namespace Company.Interface.SXXK.BangKeThanhLy
{
    partial class DSToKhaiXuatGCForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList1_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DSToKhaiXuatGCForm));
            this.dgList1 = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.dgList1);
            this.grbMain.Size = new System.Drawing.Size(501, 273);
            // 
            // dgList1
            // 
            this.dgList1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList1.AlternatingColors = true;
            this.dgList1.AutomaticSort = false;
            this.dgList1.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList1.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList1.ColumnAutoResize = true;
            dgList1_DesignTimeLayout.LayoutString = resources.GetString("dgList1_DesignTimeLayout.LayoutString");
            this.dgList1.DesignTimeLayout = dgList1_DesignTimeLayout;
            this.dgList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList1.GroupByBoxVisible = false;
            this.dgList1.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList1.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList1.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList1.ImageList = this.ImageList1;
            this.dgList1.Location = new System.Drawing.Point(0, 0);
            this.dgList1.Name = "dgList1";
            this.dgList1.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList1.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList1.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList1.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList1.Size = new System.Drawing.Size(501, 273);
            this.dgList1.TabIndex = 20;
            this.dgList1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList1.VisualStyleManager = this.vsmMain;
            this.dgList1.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList1_RowDoubleClick);
            this.dgList1.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList1_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // DSToKhaiXuatGCForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(501, 273);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "DSToKhaiXuatGCForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Danh sách tờ khai xuất gia công";
            this.Load += new System.EventHandler(this.DSToKhaiXuatGCForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX dgList1;
        private System.Windows.Forms.ImageList ImageList1;
    }
}