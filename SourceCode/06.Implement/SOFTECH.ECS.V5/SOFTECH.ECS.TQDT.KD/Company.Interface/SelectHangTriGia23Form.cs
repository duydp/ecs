﻿using System;
using System.Drawing;
//using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT;
//using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL;
//using Company.KD.BLL.Utils;
using System.Data;
#if GC_V3 || GC_V4
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif
namespace Company.Interface
{
    public partial class SelectHangTriGia23Form : BaseForm
    {
        //-----------------------------------------------------------------------------------------

        public ToKhaiMauDich TKMD;
        public HangMauDich HMD;
        public int LoaiTKTG = 1;
        //-----------------------------------------------------------------------------------------

        public SelectHangTriGia23Form()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(int STT)
        {
            foreach (ToKhaiTriGiaPP23 TKTG23 in TKMD.TKTGPP23Collection)
            {
                if (TKTG23.STTHang == STT && LoaiTKTG == TKTG23.MaToKhaiTriGia) return true;
            }
            return false;
        }

   


        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            HMD = null;
            this.Close();            
        }


        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HMD = this.TKMD.HMDCollection[i.Position];
                    if (HMD.DonGiaKB==0)
                    {
                        ShowMessage("Đây là hàng có đơn giá = 0 không khai báo trong tờ khai trị giá", false);
                        return;
                    }
                    if (checkMaHangExit(HMD.SoThuTuHang))
                    {
                        MLMessages("Mặt hàng này đã được chọn.","MSG_SEL02","goods", false);
                        return;
                    }                   
                    this.Close();
                }
                break;
            }
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKMD.HMDCollection;            
        }

    


    }
}
