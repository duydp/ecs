using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class SanPham
	{
		#region Private members.
		
		protected long _ID;
		protected string _Ma = string.Empty;
		protected string _Ten = string.Empty;
		protected string _MaHS = string.Empty;
		protected string _DVT_ID = string.Empty;
		protected int _STTHang;
		protected long _Master_ID;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		
		public string Ma
		{
			set {this._Ma = value;}
			get {return this._Ma;}
		}
		
		public string Ten
		{
			set {this._Ten = value;}
			get {return this._Ten;}
		}
		
		public string MaHS
		{
			set {this._MaHS = value;}
			get {return this._MaHS;}
		}
		
		public string DVT_ID
		{
			set {this._DVT_ID = value;}
			get {return this._DVT_ID;}
		}
		
		public int STTHang
		{
			set {this._STTHang = value;}
			get {return this._STTHang;}
		}
		
		public long Master_ID
		{
			set {this._Master_ID = value;}
			get {return this._Master_ID;}
		}
		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static SanPham Load(long iD)
		{
			string spName = "[dbo].p_KDT_SXXK_SanPham_Load";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
			SanPham entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new SanPham();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
			}
			reader.Close();
			dbCommand.Connection.Close();
			
			return entity;
		}
    
        public static SanPham Load(string ma)
        {
            SanPham entity = null;
            string where = "Ma='" + ma + "'";
            IDataReader reader = SelectReaderDynamic(where, "");
            if (reader.Read())
            {
                entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
            }
            reader.Close();            
            return entity;
        }
        public static bool checkNPLExit(string masp, long master_ID)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "select sp.id " +
                       "from t_KDT_SXXK_SanPham sp inner join t_KDT_SXXK_SanPhamDangKy spdk on " +
                       "sp.Master_ID=spdk.ID " +
                       "where sp.Ma=@Ma and sp.Master_ID<>@Master_ID and spdk.TrangThaiXuLy<>1 ";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, masp);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            return true;
        }
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Master_ID(long master_ID)
		{
			string spName = "[dbo].p_KDT_SXXK_SanPham_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		

		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderBy_Master_ID(long master_ID)
		{
			string spName = "[dbo].p_KDT_SXXK_SanPham_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            return db.ExecuteReader(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		

		public static SanPhamCollection SelectCollectionBy_Master_ID(long master_ID)
		{
			string spName = "[dbo].p_KDT_SXXK_SanPham_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            IDataReader reader = db.ExecuteReader(dbCommand);
			SanPhamCollection collection = new SanPhamCollection();			
			while (reader.Read())
			{
				SanPham entity = new SanPham();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				collection.Add(entity);
			}
			reader.Close();
			dbCommand.Connection.Close();
			return collection;			
			
		}
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            string spName = "[dbo].p_KDT_SXXK_SanPham_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public static IDataReader SelectReaderAll()
        {
            string spName = "[dbo].p_KDT_SXXK_SanPham_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static SanPhamCollection SelectCollectionAll()
		{
			SanPhamCollection collection = new SanPhamCollection();

			IDataReader reader = SelectReaderAll();
			while (reader.Read())
			{
				SanPham entity = new SanPham();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "[dbo].p_KDT_SXXK_SanPham_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "[dbo].p_KDT_SXXK_SanPham_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static SanPhamCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			SanPhamCollection collection = new SanPhamCollection();

			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				SanPham entity = new SanPham();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long Insert(string ma, string ten, string maHS, string dVT_ID, int sTTHang, long master_ID)
		{
			SanPham entity = new SanPham();			
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.STTHang = sTTHang;
			entity.Master_ID = master_ID;
			
			long returnID = entity.Insert();
						
			return returnID;
		}
		
		public bool Insert(SanPhamCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection) db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (SanPham item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "[dbo].p_KDT_SXXK_SanPham_Insert";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_SanPham_InsertUpdate";	
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(SanPhamCollection collection)
        {
            bool ret;			
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection) db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (SanPham item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int Update(long iD, string ma, string ten, string maHS, string dVT_ID, int sTTHang, long master_ID)
		{
			SanPham entity = new SanPham();			
			entity.ID = iD;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.STTHang = sTTHang;
			entity.Master_ID = master_ID;
			return entity.Update();
		}
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public bool Update(SanPhamCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection) db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (SanPham item in collection)
                    {
                        if (item.UpdateTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "[dbo].p_KDT_SXXK_SanPham_Update";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
	
	}	
}