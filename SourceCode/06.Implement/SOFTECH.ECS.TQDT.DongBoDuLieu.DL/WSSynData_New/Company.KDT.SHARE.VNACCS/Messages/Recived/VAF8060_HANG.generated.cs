using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAF8060_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute B01 { get; set; }
public PropertiesAttribute B02 { get; set; }
public PropertiesAttribute B03 { get; set; }
public PropertiesAttribute B04 { get; set; }
public PropertiesAttribute B05 { get; set; }
public PropertiesAttribute B06 { get; set; }
public PropertiesAttribute B07 { get; set; }
public PropertiesAttribute B08 { get; set; }
public PropertiesAttribute B09 { get; set; }
public PropertiesAttribute B10 { get; set; }

public VAF8060_HANG()
        {
B01 = new PropertiesAttribute(4, typeof(string));
B02 = new PropertiesAttribute(8, typeof(DateTime));
B03 = new PropertiesAttribute(6, typeof(DateTime));
B04 = new PropertiesAttribute(13, typeof(int));
B05 = new PropertiesAttribute(13, typeof(int));
B06 = new PropertiesAttribute(12, typeof(int));
B07 = new PropertiesAttribute(8, typeof(DateTime));
B08 = new PropertiesAttribute(3, typeof(string));
B09 = new PropertiesAttribute(6, typeof(string));
B10 = new PropertiesAttribute(210, typeof(string));
TongSoByte = 303;
}

}public partial class EnumGroupID
    {
}

}
