using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.BLL.SXXK.ThanhKhoan
{
	public partial class NPLHuy : EntityBase
	{
		#region Private members.
		
		protected long _ID;
		protected int _LanThanhLy;
		protected string _MaDoanhNghiep = String.Empty;
		protected int _SoToKhai;
		protected string _MaLoaiHinh = String.Empty;
		protected short _NamDangKy;
		protected string _MaHaiQuan = String.Empty;
		protected string _MaNPL = String.Empty;
		protected string _BienBanHuy = String.Empty;
		protected DateTime _NgayHuy = new DateTime(1900, 01, 01);
		protected decimal _SoLuongHuy;
		protected bool _DaXuLy;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		public int LanThanhLy
		{
			set {this._LanThanhLy = value;}
			get {return this._LanThanhLy;}
		}
		public string MaDoanhNghiep
		{
			set {this._MaDoanhNghiep = value;}
			get {return this._MaDoanhNghiep;}
		}
		public int SoToKhai
		{
			set {this._SoToKhai = value;}
			get {return this._SoToKhai;}
		}
		public string MaLoaiHinh
		{
			set {this._MaLoaiHinh = value;}
			get {return this._MaLoaiHinh;}
		}
		public short NamDangKy
		{
			set {this._NamDangKy = value;}
			get {return this._NamDangKy;}
		}
		public string MaHaiQuan
		{
			set {this._MaHaiQuan = value;}
			get {return this._MaHaiQuan;}
		}
		public string MaNPL
		{
			set {this._MaNPL = value;}
			get {return this._MaNPL;}
		}
		public string BienBanHuy
		{
			set {this._BienBanHuy = value;}
			get {return this._BienBanHuy;}
		}
		public DateTime NgayHuy
		{
			set {this._NgayHuy = value;}
			get {return this._NgayHuy;}
		}
		public decimal SoLuongHuy
		{
			set {this._SoLuongHuy = value;}
			get {return this._SoLuongHuy;}
		}
		public bool DaXuLy
		{
			set {this._DaXuLy = value;}
			get {return this._DaXuLy;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_SXXK_ThanhLy_NPLHuy_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("BienBanHuy"))) this._BienBanHuy = reader.GetString(reader.GetOrdinal("BienBanHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHuy"))) this._NgayHuy = reader.GetDateTime(reader.GetOrdinal("NgayHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHuy"))) this._SoLuongHuy = reader.GetDecimal(reader.GetOrdinal("SoLuongHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) this._DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
				return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public NPLHuyCollection SelectCollectionBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan()
		{
			string spName = "p_SXXK_ThanhLy_NPLHuy_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			
			NPLHuyCollection collection = new NPLHuyCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				NPLHuy entity = new NPLHuy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("BienBanHuy"))) entity.BienBanHuy = reader.GetString(reader.GetOrdinal("BienBanHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHuy"))) entity.NgayHuy = reader.GetDateTime(reader.GetOrdinal("NgayHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHuy"))) entity.SoLuongHuy = reader.GetDecimal(reader.GetOrdinal("SoLuongHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
				collection.Add(entity);
			}
			return collection;
		}

		//---------------------------------------------------------------------------------------------
		public NPLHuyCollection SelectCollectionBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan()
		{
			string spName = "p_SXXK_ThanhLy_NPLHuy_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			
			NPLHuyCollection collection = new NPLHuyCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				NPLHuy entity = new NPLHuy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("BienBanHuy"))) entity.BienBanHuy = reader.GetString(reader.GetOrdinal("BienBanHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHuy"))) entity.NgayHuy = reader.GetDateTime(reader.GetOrdinal("NgayHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHuy"))) entity.SoLuongHuy = reader.GetDecimal(reader.GetOrdinal("SoLuongHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
				collection.Add(entity);
			}
			return collection;
		}

		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan()
		{
			string spName = "p_SXXK_ThanhLy_NPLHuy_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
						
            return this.db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------
		public DataSet SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan()
		{
			string spName = "p_SXXK_ThanhLy_NPLHuy_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
						
            return this.db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLHuy_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLHuy_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ThanhLy_NPLHuy_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ThanhLy_NPLHuy_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public NPLHuyCollection SelectCollectionAll()
		{
			NPLHuyCollection collection = new NPLHuyCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				NPLHuy entity = new NPLHuy();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("BienBanHuy"))) entity.BienBanHuy = reader.GetString(reader.GetOrdinal("BienBanHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHuy"))) entity.NgayHuy = reader.GetDateTime(reader.GetOrdinal("NgayHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHuy"))) entity.SoLuongHuy = reader.GetDecimal(reader.GetOrdinal("SoLuongHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public NPLHuyCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			NPLHuyCollection collection = new NPLHuyCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				NPLHuy entity = new NPLHuy();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("BienBanHuy"))) entity.BienBanHuy = reader.GetString(reader.GetOrdinal("BienBanHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHuy"))) entity.NgayHuy = reader.GetDateTime(reader.GetOrdinal("NgayHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHuy"))) entity.SoLuongHuy = reader.GetDecimal(reader.GetOrdinal("SoLuongHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
				collection.Add(entity);
			}
            reader.Close();
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ThanhLy_NPLHuy_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@BienBanHuy", SqlDbType.VarChar, this._BienBanHuy);
			this.db.AddInParameter(dbCommand, "@NgayHuy", SqlDbType.DateTime, this._NgayHuy);
			this.db.AddInParameter(dbCommand, "@SoLuongHuy", SqlDbType.Decimal, this._SoLuongHuy);
			this.db.AddInParameter(dbCommand, "@DaXuLy", SqlDbType.Bit, this._DaXuLy);
			
			if (transaction != null)
			{
				this.db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				this.db.ExecuteNonQuery(dbCommand);
				this._ID = (long) this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(NPLHuyCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLHuy item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, NPLHuyCollection collection)
        {
            foreach (NPLHuy item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ThanhLy_NPLHuy_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@BienBanHuy", SqlDbType.VarChar, this._BienBanHuy);
			this.db.AddInParameter(dbCommand, "@NgayHuy", SqlDbType.DateTime, this._NgayHuy);
			this.db.AddInParameter(dbCommand, "@SoLuongHuy", SqlDbType.Decimal, this._SoLuongHuy);
			this.db.AddInParameter(dbCommand, "@DaXuLy", SqlDbType.Bit, this._DaXuLy);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(NPLHuyCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLHuy item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ThanhLy_NPLHuy_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@BienBanHuy", SqlDbType.VarChar, this._BienBanHuy);
			this.db.AddInParameter(dbCommand, "@NgayHuy", SqlDbType.DateTime, this._NgayHuy);
			this.db.AddInParameter(dbCommand, "@SoLuongHuy", SqlDbType.Decimal, this._SoLuongHuy);
			this.db.AddInParameter(dbCommand, "@DaXuLy", SqlDbType.Bit, this._DaXuLy);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(NPLHuyCollection collection, SqlTransaction transaction)
        {
            foreach (NPLHuy item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ThanhLy_NPLHuy_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(NPLHuyCollection collection, SqlTransaction transaction)
        {
            foreach (NPLHuy item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(NPLHuyCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLHuy item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion
        public NPLHuy SelectEntityBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan_AND_MaNPL(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLHuy_SelectBy_KeyPhu";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);

            IDataReader reader = null;
            if (transaction == null)
                reader = this.db.ExecuteReader(dbCommand);
            else
                reader = this.db.ExecuteReader(dbCommand, transaction);
            NPLHuy entity = null;
            if (reader.Read())
            {                
                entity = new NPLHuy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("BienBanHuy"))) entity.BienBanHuy = reader.GetString(reader.GetOrdinal("BienBanHuy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHuy"))) entity.NgayHuy = reader.GetDateTime(reader.GetOrdinal("NgayHuy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHuy"))) entity.SoLuongHuy = reader.GetDecimal(reader.GetOrdinal("SoLuongHuy"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));                
            }
            reader.Close();
            return entity;
        }
        public void InsertUpdateTransaction2(SqlTransaction transaction)
        {
            NPLHuy tmp = this.SelectEntityBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan_AND_MaNPL(transaction);
            if (tmp == null)
                this.InsertTransaction(transaction);
            else
            {
                this.ID = tmp.ID;
                this.UpdateTransaction(transaction);
            }
        }
        public static void updateDatabase(DataSet ds, SqlTransaction transaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NPLHuy npl = new NPLHuy();
                npl._DaXuLy = Convert.ToBoolean(row["DA_XL"].ToString());   
                npl._BienBanHuy=(row["BB_HUY"].ToString());
                npl._LanThanhLy = Convert.ToInt32(row["LAN_TL"].ToString());
                npl._MaDoanhNghiep = (row["MA_DV"].ToString());
                npl._MaHaiQuan = (row["MA_HQ"].ToString());
                npl._MaLoaiHinh = (row["MA_LH"].ToString());
                npl._MaNPL = (row["MA_NPL"].ToString());
                npl._NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                npl._NgayHuy = Convert.ToDateTime(row["NGAY_HUY"].ToString());
                npl._SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                npl._SoLuongHuy = Convert.ToDecimal(row["LUONG_HUY"]);
                npl.InsertUpdateTransaction2(transaction);
            }
        }

	}	
}