namespace Company.Interface.SXXK
{
    partial class ImportHSTKForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListNPLTon_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportHSTKForm));
            Janus.Windows.GridEX.GridEXLayout dgListTKXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTKNhap_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnImportTKN = new System.Windows.Forms.Button();
            this.btnImportNPLTon = new System.Windows.Forms.Button();
            this.btnImportTKX = new System.Windows.Forms.Button();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.chkSaveSetting = new Janus.Windows.EditControls.UICheckBox();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.chkOverwrite = new Janus.Windows.EditControls.UICheckBox();
            this.uiTab = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageNPLTon = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListNPLTon = new Janus.Windows.GridEX.GridEX();
            this.uiTabPageTKX = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListTKXuat = new Janus.Windows.GridEX.GridEX();
            this.uiTabPageTKN = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListTKNhap = new Janus.Windows.GridEX.GridEX();
            this.btnCapNhatTon = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab)).BeginInit();
            this.uiTab.SuspendLayout();
            this.uiTabPageNPLTon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLTon)).BeginInit();
            this.uiTabPageTKX.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKXuat)).BeginInit();
            this.uiTabPageTKN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKNhap)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab);
            this.grbMain.Controls.Add(this.chkOverwrite);
            this.grbMain.Controls.Add(this.chkSaveSetting);
            this.grbMain.Controls.Add(this.btnCapNhatTon);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(593, 422);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Excel 97 - 2003 files (*.xls)|*.xls";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnImportTKN);
            this.uiGroupBox1.Controls.Add(this.btnImportNPLTon);
            this.uiGroupBox1.Controls.Add(this.btnImportTKX);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(593, 115);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Nhập dữ liệu từ Excel";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnImportTKN
            // 
            this.btnImportTKN.Location = new System.Drawing.Point(23, 79);
            this.btnImportTKN.Name = "btnImportTKN";
            this.btnImportTKN.Size = new System.Drawing.Size(278, 23);
            this.btnImportTKN.TabIndex = 2;
            this.btnImportTKN.Text = "Import Bảng kê Tờ khai nhập";
            this.btnImportTKN.UseVisualStyleBackColor = true;
            this.btnImportTKN.Click += new System.EventHandler(this.btnImportTKN_Click);
            // 
            // btnImportNPLTon
            // 
            this.btnImportNPLTon.Location = new System.Drawing.Point(23, 20);
            this.btnImportNPLTon.Name = "btnImportNPLTon";
            this.btnImportNPLTon.Size = new System.Drawing.Size(278, 23);
            this.btnImportNPLTon.TabIndex = 0;
            this.btnImportNPLTon.Text = "Import NPL nhập - tồn";
            this.btnImportNPLTon.UseVisualStyleBackColor = true;
            this.btnImportNPLTon.Click += new System.EventHandler(this.btnImportNPLTon_Click);
            // 
            // btnImportTKX
            // 
            this.btnImportTKX.Location = new System.Drawing.Point(23, 49);
            this.btnImportTKX.Name = "btnImportTKX";
            this.btnImportTKX.Size = new System.Drawing.Size(278, 23);
            this.btnImportTKX.TabIndex = 1;
            this.btnImportTKX.Text = "Import Bảng kê Tờ khai xuất";
            this.btnImportTKX.UseVisualStyleBackColor = true;
            this.btnImportTKX.Click += new System.EventHandler(this.btnImportTKX_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(467, 395);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(391, 395);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // chkSaveSetting
            // 
            this.chkSaveSetting.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.chkSaveSetting.BackColor = System.Drawing.Color.Transparent;
            this.chkSaveSetting.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSaveSetting.Location = new System.Drawing.Point(49, 396);
            this.chkSaveSetting.Name = "chkSaveSetting";
            this.chkSaveSetting.Size = new System.Drawing.Size(104, 23);
            this.chkSaveSetting.TabIndex = 2;
            this.chkSaveSetting.Text = "Lưu thông số";
            this.chkSaveSetting.VisualStyleManager = this.vsmMain;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // chkOverwrite
            // 
            this.chkOverwrite.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.chkOverwrite.BackColor = System.Drawing.Color.Transparent;
            this.chkOverwrite.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOverwrite.Location = new System.Drawing.Point(159, 396);
            this.chkOverwrite.Name = "chkOverwrite";
            this.chkOverwrite.Size = new System.Drawing.Size(104, 23);
            this.chkOverwrite.TabIndex = 3;
            this.chkOverwrite.Text = "Ghi đè";
            this.chkOverwrite.VisualStyleManager = this.vsmMain;
            // 
            // uiTab
            // 
            this.uiTab.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiTab.Location = new System.Drawing.Point(0, 115);
            this.uiTab.Name = "uiTab";
            this.uiTab.Size = new System.Drawing.Size(593, 269);
            this.uiTab.TabIndex = 1;
            this.uiTab.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageNPLTon,
            this.uiTabPageTKX,
            this.uiTabPageTKN});
            this.uiTab.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            // 
            // uiTabPageNPLTon
            // 
            this.uiTabPageNPLTon.Controls.Add(this.dgListNPLTon);
            this.uiTabPageNPLTon.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageNPLTon.Name = "uiTabPageNPLTon";
            this.uiTabPageNPLTon.Size = new System.Drawing.Size(591, 247);
            this.uiTabPageNPLTon.TabStop = true;
            this.uiTabPageNPLTon.Text = "NPL nhập tồn";
            // 
            // dgListNPLTon
            // 
            this.dgListNPLTon.AllowCardSizing = false;
            this.dgListNPLTon.AllowColumnDrag = false;
            this.dgListNPLTon.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLTon.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPLTon.AlternatingColors = true;
            this.dgListNPLTon.AutomaticSort = false;
            this.dgListNPLTon.ColumnAutoResize = true;
            dgListNPLTon_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLTon_DesignTimeLayout.LayoutString");
            this.dgListNPLTon.DesignTimeLayout = dgListNPLTon_DesignTimeLayout;
            this.dgListNPLTon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPLTon.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNPLTon.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLTon.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLTon.GroupByBoxVisible = false;
            this.dgListNPLTon.ImageList = this.ImageList1;
            this.dgListNPLTon.Location = new System.Drawing.Point(0, 0);
            this.dgListNPLTon.Name = "dgListNPLTon";
            this.dgListNPLTon.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPLTon.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLTon.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNPLTon.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLTon.SelectedFormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            this.dgListNPLTon.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNPLTon.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPLTon.Size = new System.Drawing.Size(591, 247);
            this.dgListNPLTon.TabIndex = 0;
            this.dgListNPLTon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListNPLTon.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPageTKX
            // 
            this.uiTabPageTKX.Controls.Add(this.dgListTKXuat);
            this.uiTabPageTKX.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageTKX.Name = "uiTabPageTKX";
            this.uiTabPageTKX.Size = new System.Drawing.Size(591, 247);
            this.uiTabPageTKX.TabStop = true;
            this.uiTabPageTKX.Text = "Tờ khai xuất";
            // 
            // dgListTKXuat
            // 
            this.dgListTKXuat.AllowCardSizing = false;
            this.dgListTKXuat.AllowColumnDrag = false;
            this.dgListTKXuat.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKXuat.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTKXuat.AlternatingColors = true;
            this.dgListTKXuat.AutomaticSort = false;
            this.dgListTKXuat.ColumnAutoResize = true;
            dgListTKXuat_DesignTimeLayout.LayoutString = resources.GetString("dgListTKXuat_DesignTimeLayout.LayoutString");
            this.dgListTKXuat.DesignTimeLayout = dgListTKXuat_DesignTimeLayout;
            this.dgListTKXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTKXuat.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKXuat.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKXuat.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTKXuat.GroupByBoxVisible = false;
            this.dgListTKXuat.ImageList = this.ImageList1;
            this.dgListTKXuat.Location = new System.Drawing.Point(0, 0);
            this.dgListTKXuat.Name = "dgListTKXuat";
            this.dgListTKXuat.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTKXuat.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKXuat.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKXuat.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKXuat.SelectedFormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            this.dgListTKXuat.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKXuat.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTKXuat.Size = new System.Drawing.Size(591, 247);
            this.dgListTKXuat.TabIndex = 0;
            this.dgListTKXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTKXuat.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPageTKN
            // 
            this.uiTabPageTKN.Controls.Add(this.dgListTKNhap);
            this.uiTabPageTKN.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageTKN.Name = "uiTabPageTKN";
            this.uiTabPageTKN.Size = new System.Drawing.Size(591, 247);
            this.uiTabPageTKN.TabStop = true;
            this.uiTabPageTKN.Text = "Tờ khai nhập";
            // 
            // dgListTKNhap
            // 
            this.dgListTKNhap.AllowCardSizing = false;
            this.dgListTKNhap.AllowColumnDrag = false;
            this.dgListTKNhap.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKNhap.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTKNhap.AlternatingColors = true;
            this.dgListTKNhap.AutomaticSort = false;
            this.dgListTKNhap.ColumnAutoResize = true;
            dgListTKNhap_DesignTimeLayout.LayoutString = resources.GetString("dgListTKNhap_DesignTimeLayout.LayoutString");
            this.dgListTKNhap.DesignTimeLayout = dgListTKNhap_DesignTimeLayout;
            this.dgListTKNhap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTKNhap.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKNhap.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKNhap.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTKNhap.GroupByBoxVisible = false;
            this.dgListTKNhap.ImageList = this.ImageList1;
            this.dgListTKNhap.Location = new System.Drawing.Point(0, 0);
            this.dgListTKNhap.Name = "dgListTKNhap";
            this.dgListTKNhap.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTKNhap.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKNhap.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKNhap.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKNhap.SelectedFormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            this.dgListTKNhap.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKNhap.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTKNhap.Size = new System.Drawing.Size(591, 247);
            this.dgListTKNhap.TabIndex = 0;
            this.dgListTKNhap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTKNhap.VisualStyleManager = this.vsmMain;
            // 
            // btnCapNhatTon
            // 
            this.btnCapNhatTon.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCapNhatTon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhatTon.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCapNhatTon.Icon")));
            this.btnCapNhatTon.Location = new System.Drawing.Point(282, 395);
            this.btnCapNhatTon.Name = "btnCapNhatTon";
            this.btnCapNhatTon.Size = new System.Drawing.Size(102, 23);
            this.btnCapNhatTon.TabIndex = 4;
            this.btnCapNhatTon.Text = "Cập nhật tồn";
            this.btnCapNhatTon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnCapNhatTon.Click += new System.EventHandler(this.btnCapNhatTon_Click);
            // 
            // ImportHSTKForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(593, 422);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ImportHSTKForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Import thông tin Hồ sơ thanh khoản từ Excel";
            this.Load += new System.EventHandler(this.ImportTTDMForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab)).EndInit();
            this.uiTab.ResumeLayout(false);
            this.uiTabPageNPLTon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLTon)).EndInit();
            this.uiTabPageTKX.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKXuat)).EndInit();
            this.uiTabPageTKN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKNhap)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UICheckBox chkSaveSetting;
        private System.Windows.Forms.ErrorProvider error;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UICheckBox chkOverwrite;
        private Janus.Windows.UI.Tab.UITab uiTab;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageNPLTon;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageTKX;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageTKN;
        private System.Windows.Forms.Button btnImportTKN;
        private System.Windows.Forms.Button btnImportNPLTon;
        private System.Windows.Forms.Button btnImportTKX;
        private Janus.Windows.GridEX.GridEX dgListTKNhap;
        private Janus.Windows.GridEX.GridEX dgListTKXuat;
        private Janus.Windows.EditControls.UIButton btnCapNhatTon;
        private Janus.Windows.GridEX.GridEX dgListNPLTon;
    }
}