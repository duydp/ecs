﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.DuLieuChuan
{
    public class Nuoc : BaseClass
    {
        public static DataTable SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_Nuoc ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static string GetName(string id)
        {
            string query = string.Format("SELECT Ten FROM t_HaiQuan_Nuoc WHERE [ID] = '{0}'", id.PadRight(3));
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                return reader["Ten"].ToString();
            }
            return string.Empty;
        }
        public static void Update(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_Nuoc VALUES(@Id,@Ten)";
            string update = "UPDATE t_HaiQuan_Nuoc SET Ten = @Ten WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_Nuoc WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
    }
}