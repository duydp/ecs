﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.Interface.DanhMucChuan;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components.Messages.Send;
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.VNACCS.VNACC.PhieuKho;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.Messages;
using Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
using System.IO;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using Company.KDT.SHARE.VNACCS.VNACC.PhieuKho;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.Messages;
using Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
using System.IO;
using Company.KDT.SHARE.VNACCS.VNACC.PhieuKho;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP;
using Company.KDT.SHARE.Components.Messages;
#endif
namespace Company.Interface
{
    public class SingleMessage
    {
        static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        static string sfmtDate = "yyyy-MM-dd";
        static string sfmtVnDateTime = "dd-MM-yyyy HH:mm:ss";
        public static ObjectSend SendMessageV3(ToKhaiMauDich TKMD)
        {

            TKMD.GUIDSTR = Guid.NewGuid().ToString();
            //TKMD.Update();
            ToKhai toKhaiDto = new ToKhai();
            toKhaiDto = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferObject(TKMD) : Mapper.ToDataTransferObject(TKMD);
            bool isToKhaiSua = TKMD.SoToKhai != 0 && TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            ObjectSend objSend = new ObjectSend(new NameBase()
                           {
                               Name = Company.KDT.SHARE.Components.Globals.isKhaiDL ? TKMD.TenDaiLyTTHQ : TKMD.TenDoanhNghiep,
                               Identity = Company.KDT.SHARE.Components.Globals.isKhaiDL ? TKMD.MaDaiLyTTHQ : TKMD.MaDoanhNghiep
                           },
                            new NameBase()
                             {
                                 Name = DonViHaiQuan.GetName(TKMD.MaHaiQuan),
                                 Identity = /*TKMD.MaHaiQuan*/ Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TKMD.MaHaiQuan.Trim()) : TKMD.MaHaiQuan.Trim()
                             },
                             new SubjectBase()
                            {
                                Type = toKhaiDto.Issuer,
                                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                                Reference = TKMD.GUIDSTR
                            },
                            toKhaiDto);
            return objSend;
        }
        public static ObjectSend CancelMessageV3(ToKhaiMauDich TKMD)
        {
            return CancelMessageV3(TKMD, string.Empty);
        }
        public static ObjectSend CancelMessageV3(ToKhaiMauDich TKMD, string guidHuyTK)
        {
            bool IsToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
            TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
            bool IsHuyToKhai = (TKMD.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
            string issuer = string.Empty;
#if KD_V3 || KD_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT;
#elif SXXK_V3 || SXXK_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.SXXK_TOKHAI_NHAP : DeclarationIssuer.SXXK_TOKHAI_XUAT;
#elif GC_V3 || GC_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_NHAP : DeclarationIssuer.GC_TOKHAI_XUAT;
#endif
            DeclarationBase declaredCancel = new DeclarationBase()
            {
                Issuer = issuer,
                Reference = TKMD.GUIDSTR,
                IssueLocation = string.Empty,
                Agents = new List<Agent>(),

                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(TKMD.SoTiepNhan, 0),
                Acceptance = TKMD.NgayTiepNhan.ToString(sfmtDate),
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TKMD.MaHaiQuan.Trim()).Trim() : TKMD.MaHaiQuan.Trim(),//tkmd.MaHaiQuan.Trim(),
                AdditionalInformations = new List<AdditionalInformation>()
            };
            declaredCancel.Agents.Add(new Agent()
            {
                Name = TKMD.TenDoanhNghiep,
                Identity = TKMD.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            
            declaredCancel.AdditionalInformations.Add(
                new AdditionalInformation()
                {
                    Content = new Content() { Text = "." }
                }
                );
            if (IsHuyToKhai)
            {
                List<HuyToKhai> cancelContent = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_TKMD_ID(TKMD.ID);
                if (cancelContent.Count > 0)
                {
                    declaredCancel.AdditionalInformations[0].Content.Text = cancelContent[0].LyDoHuy;
                    declaredCancel.Reference = guidHuyTK;
                }
            }
            ObjectSend objSend = new ObjectSend(
                new NameBase()
            {
                Name = TKMD.TenDoanhNghiep,
                Identity = TKMD.MaDoanhNghiep
            },
            new NameBase()
            {
                Name = DonViHaiQuan.GetName(TKMD.MaHaiQuan),
                Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TKMD.MaHaiQuan) : TKMD.MaHaiQuan
            },
            
            new SubjectBase()
            {
                Type = issuer,
                Function = declaredCancel.Function,
                Reference = declaredCancel.Reference //TKMD.GUIDSTR
            }, declaredCancel);
            return objSend;
        }
        public static ObjectSend FeedBackMessageV3(ToKhaiMauDich TKMD)
        {
            return FeedBack(TKMD, TKMD.GUIDSTR);
        }
        public static ObjectSend FeedBack(ToKhaiMauDich TKMD, string reference)
        {
            return FeedBack(TKMD, reference, "");
        }
        public static ObjectSend FeedBack(ToKhaiMauDich TKMD, string reference, string DieuKienPhanHoi)
        {
            if (TKMD == null) return null;
            bool IsToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
            string issuer = string.Empty;

#if KD_V3 || KD_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT;
#elif SXXK_V3 || SXXK_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.SXXK_TOKHAI_NHAP : DeclarationIssuer.SXXK_TOKHAI_XUAT;
#elif GC_V3 || GC_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_NHAP : DeclarationIssuer.GC_TOKHAI_XUAT;
#endif
            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = issuer,
                Reference = reference,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = issuer,

            };
            if (!string.IsNullOrEmpty(DieuKienPhanHoi))
                subjectBase.DeclarationStatus = DieuKienPhanHoi;
            ObjectSend objSend = new ObjectSend(new NameBase()
                                        {
                                            Name = Company.KDT.SHARE.Components.Globals.isKhaiDL ? TKMD.TenDaiLyTTHQ : TKMD.TenDoanhNghiep,
                                            Identity = Company.KDT.SHARE.Components.Globals.isKhaiDL ? TKMD.MaDaiLyTTHQ : TKMD.MaDoanhNghiep
                                        },
                                        new NameBase()
                                          {
                                              Name = DonViHaiQuan.GetName(TKMD.MaHaiQuan.Trim()),
                                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TKMD.MaHaiQuan.Trim()) : TKMD.MaHaiQuan.Trim()//TKMD.MaHaiQuan
                                          },
                                          subjectBase, null);
            return objSend;
        }
        public static ObjectSend FeedBackBoSungCo(Company.KDT.SHARE.QuanLyChungTu.CO Co, ToKhaiMauDich TKMD)
        {
            return FeedBack(TKMD, Co.GuidStr);
        }
        private static ObjectSend BuildBoSungChungTu(ToKhaiMauDich TKMD, BoSungChungTu bosung)
        {
            ObjectSend objSend = new ObjectSend(new NameBase()
               {
                   Identity = TKMD.MaDoanhNghiep,
                   Name = TKMD.TenDoanhNghiep
               },
               new NameBase()
               {
                   Name = DonViHaiQuan.GetName(TKMD.MaHaiQuan),
                   Identity = TKMD.MaHaiQuan
               },
               new SubjectBase()
            {
                Type = bosung.Issuer,
                Function = bosung.Function,
                Reference = bosung.Reference
            }, bosung);
            return objSend;

        }
        public static ObjectSend BoSungCO(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.CO Co)
        {
            BoSungChungTu bosungCo = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, Co) : Mapper.ToDataTransferBoSung(TKMD, Co);
            return BuildBoSungChungTu(TKMD, bosungCo);
        }
        public static ObjectSend BoSungGiayPhep(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep)
        {
            BoSungChungTu boSungGiayPhep = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, giayphep) : Mapper.ToDataTransferBoSung(TKMD, giayphep);
            return BuildBoSungChungTu(TKMD, boSungGiayPhep);
        }
        public static ObjectSend BoSungHopDong(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong)
        {
            BoSungChungTu bosungHopDong = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, hopdong) : Mapper.ToDataTransferBoSung(TKMD, hopdong);
            return BuildBoSungChungTu(TKMD, bosungHopDong);
        }
        public static ObjectSend BoSungHoaDonTM(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon)
        {
            BoSungChungTu bosungHopDong = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, hoadon) : Mapper.ToDataTransferBoSung(TKMD, hoadon);
            return BuildBoSungChungTu(TKMD, bosungHopDong);
        }
        public static ObjectSend BoSungChungTuKem(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem)
        {
            BoSungChungTu bosungHopDong = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, chungtukem) : Mapper.ToDataTransferBoSung(TKMD, chungtukem);
            return BuildBoSungChungTu(TKMD, bosungHopDong);
        }
        public static ObjectSend BoSungXinChuyenCuaKhau(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau chuyencuakhau)
        {
            BoSungChungTu bosungXinChuyenCuaKhau = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, chuyencuakhau) : Mapper.ToDataTransferBoSung(TKMD, chuyencuakhau);
            return BuildBoSungChungTu(TKMD, bosungXinChuyenCuaKhau);
        }
        public static ObjectSend BoSungGiayNopTien(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.GiayNopTien GiayNopTien)
        {
            BoSungChungTu boSungGiayNopTien = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, GiayNopTien) : Mapper_V4.ToDataTransferBoSung(TKMD, GiayNopTien);
            return BuildBoSungChungTu(TKMD, boSungGiayNopTien);
        }
        public static ObjectSend BoSungGiayKiemTra(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.GiayKiemTra GiayKT)
        {
            BoSungChungTu BoSungGiayKT = Mapper_V4.ToDataTransferBoSung(TKMD, GiayKT);
            return BuildBoSungChungTu(TKMD, BoSungGiayKT);
        }
        public static ObjectSend BoSungChungThuGiamDinh(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.ChungThuGiamDinh ChungThuGD)
        {
            BoSungChungTu boSungChungThuGiamDinh = Mapper_V4.ToDataTransferBoSung(TKMD, ChungThuGD);
            return BuildBoSungChungTu(TKMD, boSungChungThuGiamDinh);
        }

        public static void DeleteMsgSend(string LoaiKhaiBao, long id)
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao;
            sendXML.master_id = id;
            sendXML.Delete();
        }
        public static void InsertUpdateMsgSend(string LoaiKhaiBao, long id)
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao;
            sendXML.master_id = id;
            sendXML.InsertUpdate();
        }
#if GC_V3 || GC_V4
        #region Tờ khai chuyển tiếp
        public static ObjectSend SendMessageCT(ToKhaiChuyenTiep TKCT)
        {
            TKCT.GUIDSTR = Guid.NewGuid().ToString();
            ToKhai toKhaiDto = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferToKhaiCT(TKCT, GlobalSettings.TEN_DON_VI) : Mapper.ToDataTransferToKhaiCT(TKCT, GlobalSettings.TEN_DON_VI);
            bool isToKhaiSua = TKCT.SoToKhai != 0 && TKCT.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = TKCT.MaDoanhNghiep
            },
                            new NameBase()
                            {
                                Name = DonViHaiQuan.GetName(TKCT.MaHaiQuanTiepNhan),
                                Identity =  Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TKCT.MaHaiQuanTiepNhan.Trim()): TKCT.MaHaiQuanTiepNhan
                            },
                             new SubjectBase()
                             {
                                 Type = toKhaiDto.Issuer,
                                 Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                                 Reference = TKCT.GUIDSTR
                             },
                            toKhaiDto);
            return objSend;
        }
        public static ObjectSend CancelMessageCT(ToKhaiChuyenTiep TKCT)
        {
            return CancelMessageCT(TKCT, string.Empty);
        }
        public static ObjectSend CancelMessageCT(ToKhaiChuyenTiep TKCT, string guidHTK)
        {
            bool IsToKhaiNhap = TKCT.MaLoaiHinh.StartsWith("N");
            TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
            bool IsHuyToKhai = (TKCT.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
            string issuer = string.Empty;
            issuer = TKCT.MaLoaiHinh.Substring(0, 1).ToUpper() == "N" ? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT;
            //string guid;
            DeclarationBase declaredCancel = new DeclarationBase()
            {
                Issuer = issuer,
                Reference = TKCT.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(TKCT.SoTiepNhan, 0),
                Acceptance = TKCT.NgayTiepNhan.ToString(sfmtDate),
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TKCT.MaHaiQuanTiepNhan.Trim()) : TKCT.MaHaiQuanTiepNhan.Trim(),
                AdditionalInformations = new List<AdditionalInformation>(),
                Importer = IsToKhaiNhap ? new NameBase { Name = TKCT.TenDonViDoiTac, Identity = "." } :
                new NameBase { Name = GlobalSettings.TEN_DON_VI, Identity = TKCT.MaDoanhNghiep.Trim() },
            };
            declaredCancel.AdditionalInformations.Add(
                new AdditionalInformation()
                {
                    Content = new Content() { Text = "." }
                }
                );
            if (IsHuyToKhai)
            {
                string sfmtWhere = string.Format("TKCT_ID={0}", TKCT.ID);
                List<HuyToKhaiCT> listHuyTK = (List<HuyToKhaiCT>)HuyToKhaiCT.SelectCollectionDynamic(sfmtWhere, "");
                if (listHuyTK.Count > 0)
                    declaredCancel.AdditionalInformations[0].Content.Text = listHuyTK[0].LyDoHuy;
                declaredCancel.Reference = guidHTK;
            }


            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = TKCT.MaDoanhNghiep
            },
            new NameBase()
            {
                Name = DonViHaiQuan.GetName(TKCT.MaHaiQuanTiepNhan),
                Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TKCT.MaHaiQuanTiepNhan) : TKCT.MaHaiQuanTiepNhan
            },
            new SubjectBase()
            {
                Type = issuer,
                Function = declaredCancel.Function,
                Reference = string.IsNullOrEmpty(guidHTK) ? TKCT.GUIDSTR : guidHTK
            },
            
            
            declaredCancel);
            
            return objSend;
        }
        public static ObjectSend FeedBackCT(ToKhaiChuyenTiep TKMD, string reference)
        {
            return FeedBackCT(TKMD, reference, string.Empty);
        }
        public static ObjectSend FeedBackCT(ToKhaiChuyenTiep TKMD, string reference, string DieuKienPhanHoi)
        {
            if (TKMD == null) return null;
            bool IsToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
            string issuer = IsToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = issuer,
                Reference = reference,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = issuer
            };
            if (!string.IsNullOrEmpty(DieuKienPhanHoi))
                subjectBase.DeclarationStatus = DieuKienPhanHoi;
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = TKMD.MaDoanhNghiep
            },
                                        new NameBase()
                                        {
                                            Name = DonViHaiQuan.GetName(TKMD.MaHaiQuanTiepNhan.Trim()),
                                            Identity  =Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TKMD.MaHaiQuanTiepNhan.Trim()): TKMD.MaHaiQuanTiepNhan
                                        },
                                          subjectBase, null);
            return objSend;
        }
        #endregion

        #region Bổ sung tờ khai chuyển tiếp
        public static ObjectSend BoSungChungTuGCCT(ToKhaiChuyenTiep TKCT, object ChungTuBoSung, string TenDoanhNghiep)
        {
            BoSungChungTu boSungCT = Mapper.ToDataTransferBoSungChuyenTiep(TKCT, ChungTuBoSung, TenDoanhNghiep);
            return BuildBoSungChungTuChuyenTiep(TKCT, boSungCT);
        }
        public static ObjectSend BuildBoSungChungTuChuyenTiep(ToKhaiChuyenTiep TKCT, BoSungChungTu bosung)
        {
            ObjectSend msgSend = new ObjectSend(
               new NameBase()
               {
                   Identity = TKCT.MaDoanhNghiep,
                   Name = GlobalSettings.TEN_DON_VI
               },
               new NameBase()
               {
                   Name = DonViHaiQuan.GetName(TKCT.MaHaiQuanTiepNhan),
                   Identity = TKCT.MaHaiQuanTiepNhan
               },
            new SubjectBase()
            {
                Type = bosung.Issuer,
                Function = bosung.Function,
                Reference = bosung.Reference
            },
             bosung);
            return msgSend;
        }
        #region Feedback gia công chuyển tiếp
        public static ObjectSend FeedBackGCCT(ToKhaiChuyenTiep TKCT, string reference)
        {
            return FeedBackGCCT(TKCT, reference, string.Empty);
        }
        public static ObjectSend FeedBackGCCT(ToKhaiChuyenTiep TKCT, string reference, string DieuKienPhanHoi)
        {
            if (TKCT == null) return null;
            bool IsToKhaiNhap = TKCT.MaLoaiHinh.StartsWith("N");
            string issuer = string.Empty;
            issuer = IsToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = issuer,
                Reference = reference,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = issuer
            };
            if (!string.IsNullOrEmpty(DieuKienPhanHoi))
                subjectBase.DeclarationStatus = DieuKienPhanHoi;
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = TKCT.MaDoanhNghiep
            },
                                        new NameBase()
                                        {
                                            Name = DonViHaiQuan.GetName(TKCT.MaHaiQuanTiepNhan.Trim()),
                                            Identity = TKCT.MaHaiQuanTiepNhan
                                        },
                                          subjectBase, null);
            return objSend;
        }
        #endregion
        #endregion
#endif
        #region Process Message
        public static string GetErrorContent(FeedBackContent feedbackContent)
        {
            string noidung = string.Empty;
            noidung = feedbackContent.AdditionalInformations[0].Content.Text;
            if (feedbackContent.AdditionalInformations[0].Content.ErrorInformations != null &&
                                    feedbackContent.AdditionalInformations[0].Content.ErrorInformations.Count > 0)
            {
                if (!string.IsNullOrEmpty(noidung)) noidung += "\r\n";
                foreach (ErrorInformation error in feedbackContent.AdditionalInformations[0].Content.ErrorInformations)
                {
                    if (error.ReQuired != null)
                        noidung += string.Format("{0}. {1}\r\n", error.ReQuired.SeQuence, error.ReQuired.Description);
                    else if (error.CheckNoDeNotExist != null)
                        noidung += string.Format("{0}. {1}\r\n", error.CheckNoDeNotExist.SeQuence, error.CheckNoDeNotExist.Description);
                    else if (error.MaxLength != null)
                        noidung += string.Format("{0}. {1}\r\n", error.MaxLength.SeQuence, error.MaxLength.Description);
                    else if (error.PositiveDouble != null)
                        noidung += string.Format("{0}. {1}\r\n", error.PositiveDouble.SeQuence, error.PositiveDouble.Description);
                    else if (error.IN != null)
                        noidung += string.Format("{0}. {1}\r\n", error.IN.SeQuence, error.IN.Description);
                    else if (error.DATETIME != null)
                        noidung += string.Format("{0}. {1}\r\n", error.DATETIME.SeQuence, error.DATETIME.Description);
                    else if (error.LE_CURRENT_DATETIME != null)
                        noidung += string.Format("{0}. {1}\r\n", error.LE_CURRENT_DATETIME.SeQuence, error.LE_CURRENT_DATETIME.Description);
                    else if (error.GE_CURRENT_DATETIME != null)
                        noidung += string.Format("{0}. {1}\r\n", error.GE_CURRENT_DATETIME.SeQuence, error.GE_CURRENT_DATETIME.Description);

                }
            }
            if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                noidung = "Hệ thống Hải quan từ chối với lý do:\r\n" + noidung;
            return noidung;
        }

        public static string GetErrorContentPort(FeedBackContentPort feedbackContent)
        {
            string noidung = string.Empty;
            noidung = feedbackContent.AdditionalInformation.Content.Text;
            if (feedbackContent.AdditionalInformation.Content.ErrorInformations != null &&
                                    feedbackContent.AdditionalInformation.Content.ErrorInformations.Count > 0)
            {
                if (!string.IsNullOrEmpty(noidung)) noidung += "\r\n";
                foreach (ErrorInformation error in feedbackContent.AdditionalInformation.Content.ErrorInformations)
                {
                    if (error.ReQuired != null)
                        noidung += string.Format("{0}. {1}\r\n", error.ReQuired.SeQuence, error.ReQuired.Description);
                    else if (error.CheckNoDeNotExist != null)
                        noidung += string.Format("{0}. {1}\r\n", error.CheckNoDeNotExist.SeQuence, error.CheckNoDeNotExist.Description);
                    else if (error.MaxLength != null)
                        noidung += string.Format("{0}. {1}\r\n", error.MaxLength.SeQuence, error.MaxLength.Description);
                    else if (error.PositiveDouble != null)
                        noidung += string.Format("{0}. {1}\r\n", error.PositiveDouble.SeQuence, error.PositiveDouble.Description);
                    else if (error.IN != null)
                        noidung += string.Format("{0}. {1}\r\n", error.IN.SeQuence, error.IN.Description);
                    else if (error.DATETIME != null)
                        noidung += string.Format("{0}. {1}\r\n", error.DATETIME.SeQuence, error.DATETIME.Description);
                    else if (error.LE_CURRENT_DATETIME != null)
                        noidung += string.Format("{0}. {1}\r\n", error.LE_CURRENT_DATETIME.SeQuence, error.LE_CURRENT_DATETIME.Description);
                    else if (error.GE_CURRENT_DATETIME != null)
                        noidung += string.Format("{0}. {1}\r\n", error.GE_CURRENT_DATETIME.SeQuence, error.GE_CURRENT_DATETIME.Description);

                }
            }
            if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                noidung = "Hệ thống Hải quan từ chối với lý do:\r\n" + noidung;
            return noidung;
        }

        public static void SendMail(string mahaiquan, SendEventArgs e)
        {
            SendMail("Thông báo lỗi", mahaiquan, e);
        }
        private static string Zip(string sourceFile)
        {
            try
            {
                if (System.IO.File.Exists(sourceFile))
                {
                    string zipFile = sourceFile + ".zip";
                    System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("zip.exe", "-j -r -9 -D \"" +
                                                             zipFile + "\" \"" +
                                                             sourceFile + "\"");
                    psi.CreateNoWindow = true;
                    psi.ErrorDialog = true;
                    psi.UseShellExecute = false;
                    System.Diagnostics.Process ps = System.Diagnostics.Process.Start(psi);
                    ps.WaitForExit();
                    System.IO.FileInfo fi = new System.IO.FileInfo(zipFile);
                    if (fi.Exists)
                        return zipFile;

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return string.Empty;
        }
        public static void SendMail(string caption, string mahaiquan, SendEventArgs e)
        {
            if (e.Error == null && string.IsNullOrEmpty(e.FeedBackMessage)) return;
            Company.Controls.KDTMessageBoxControl msg = new Company.Controls.KDTMessageBoxControl();
            msg.HQMessageString = caption;
            msg.ShowYesNoButton = false;
            msg.ShowErrorButton = true;
            msg.MaHQ = mahaiquan;
            if (!string.IsNullOrEmpty(e.FeedBackMessage))
            {
                string fileZip = Zip(e.FeedBackMessage);
                if (fileZip != string.Empty && System.IO.File.Exists(fileZip))
                    msg.XmlFiles.Add(fileZip);
                else
                    msg.XmlFiles.Add(e.FeedBackMessage);
            }
            msg.MessageString = e.Error.Message;
            msg.exceptionString = e.Error.StackTrace == null ? e.Error.Message : e.Error.StackTrace;
            msg.ShowDialog();
        }
        public static DateTime GetDate(string value)
        {
            return GetDate(value, DateTime.Now.ToString(sfmtDateTime), sfmtDate);
        }
        public static DateTime GetDate(string value, string defaultValue, string formatdate)
        {

            try
            {
                if (value.Length == 10)
                    return DateTime.ParseExact(value, formatdate, null);
                else if (value.Length == 19)
                    return DateTime.ParseExact(value, formatdate, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return DateTime.ParseExact(defaultValue, formatdate, null);

        }
        public static DateTime GetNgayAnDinhThue(string value)
        {
            DateTime date = new DateTime(1900, 1, 1);
            try
            {
                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out date))
                    return date;
                else if (DateTime.TryParseExact(value, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out date))
                    return date;
                else if (DateTime.TryParseExact(value, "dd-MM-yyyy", null, System.Globalization.DateTimeStyles.None, out date))
                    return date;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return date;

        }
        public static FeedBackContent ToKhaiSendHandler(ToKhaiMauDich TKMD, ref string msgInfor, object sender, SendEventArgs e)
        {
            FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContentV4(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;

                    //if (Company.KDT.SHARE.Components.Globals.FontName == "TCVN3")
                    //    noidung = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(noidung);
                    bool SaiMatKhau = false;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                        SaiMatKhau = true;
                        //feedbackContent.Function = DeclarationFunction.CHUA_XU_LY;
                    }
                    bool isDeleteMsg = false;
                    switch (feedbackContent.Function)
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {
                                if (!SaiMatKhau)
                                {
                                    isDeleteMsg = true;
                                    noidung = GetErrorContent(feedbackContent);
                                    e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && TKMD.SoToKhai > 0)
                                    {
                                        TKMD.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                                    }
                                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || TKMD.SoToKhai == 0)
                                    {
                                        TKMD.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                        TKMD.NgayTiepNhan = DateTime.Now;
                                    }
                                }
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {

                                string[] vals = noidung.Split('/');

                                TKMD.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                                TKMD.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                // TKMD.NamDK = TKMD.NgayTiepNhan.Year;

                                noidung = string.Format("Số tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nNăm tiếp nhận: {2}\r\nLoại hình: {3}\r\nHải quan: {4}", new object[] {
                                    TKMD.SoTiepNhan, TKMD.NgayTiepNhan.ToString(sfmtVnDateTime), TKMD.NgayTiepNhan.Year, TKMD.MaLoaiHinh, TKMD.MaHaiQuan });
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, noidung);
                                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                                {
                                    noidung = "Tờ khai được cấp số chờ hủy khai báo\r\n" + noidung;
                                    //TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else
                                {
                                    noidung = "Tờ khai được cấp số tiếp nhận\r\n" + noidung;
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                                }
                                break;
                            }
                        case DeclarationFunction.CAP_SO_TO_KHAI:
                            {
                                TKMD.SoToKhai = int.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                                TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                //Phiph Cập nhật lại ngày tiếp nhận thành ngày đăng ký do Message HQ trả về không còn giờ đăng ký

                                //noidung = string.Format("Tờ khai được cấp số\r\nSố tờ khai: {0} \r\nNgày tiếp nhận: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nNội dung hq: {4}",
                                //new object[] { feedbackContent.CustomsReference, feedbackContent.Issue, feedbackContent.NatureOfTransaction, feedbackContent.DeclarationOffice, noidung });
                                noidung = string.Format("Tờ khai được cấp số\r\nSố tờ khai: {0} \r\nNgày dăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nNội dung hq: {4}",
                                new object[] { feedbackContent.CustomsReference, feedbackContent.Acceptance, feedbackContent.NatureOfTransaction, feedbackContent.DeclarationOffice, noidung });
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocCapSo, noidung);
                                isDeleteMsg = true;
                                TKMD.NamDK = TKMD.NgayDangKy.Year;
                                //Bo sung: Hungtq, 4/2/2013, Cap nhat thong tin NPL vao ton thuc te
#if SXXK_V3 || SXXK_V4
                                TKMD.TransgferDataToSXXK();
#elif GC_V3 || GC_V4
                                // Loai tru reiker - Bổ sung chức năng mới cho công ty reiker
                                if (GlobalSettings.MA_DON_VI != "4000395355")
                                    TKMD.TransgferDataToNPLTonThucTe(TKMD.NgayDangKy);
#endif
                            }
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            {
                                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                                {
                                    //if (TKMD.SoTiepNhan.ToString() == feedbackContent.CustomsReference)
                                    //{
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                    noidung = "Tờ khai được chấp nhận hủy khai báo\r\n";
                                    e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.HQHuyKhaiBaoToKhai, noidung);
                                    //}
                                }
                                else //if ((TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)/* && TKMD.SoToKhai.ToString() == feedbackContent.CustomsReference*/)
                                {
                                    KhaiBaoSua.XoaSoTNCu(TKMD.SoToKhai, TKMD.NgayDangKy.Year, TKMD.MaLoaiHinh, TKMD.MaHaiQuan, TKMD.MaDoanhNghiep, LoaiKhaiBao.ToKhai);
                                    noidung = "Số tờ khai: " + feedbackContent.CustomsReference;
                                    noidung += "\r\nNgày đăng ký " + feedbackContent.Acceptance;
                                    if (!string.IsNullOrEmpty(noidung))
                                    {
                                        //TKMD.HUONGDAN = noidung;
                                        //                                         noidung = "Số tờ khai: " + feedbackContent.CustomsReference;
                                        //                                         noidung += "\r\nNgày đăng ký " + feedbackContent.Acceptance;
                                        noidung += "\r\nTờ khai được chấp nhận thông quan nội dung: " + noidung + "\r\n";
                                    }
                                    else noidung = "Tờ khai được chấp nhận thông quan";
                                    if (TKMD.SoToKhai == 0 && !string.IsNullOrEmpty(feedbackContent.CustomsReference))
                                    {
                                        if (feedbackContent.Acceptance.Length == 10)
                                            TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                        else if (feedbackContent.Acceptance.Length == 19)
                                            TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                        else TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                                        TKMD.SoToKhai = int.Parse(feedbackContent.CustomsReference.Trim());
                                    }
                                    e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.TKChapNhanThongQuan, noidung);
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                    TKMD.NamDK = TKMD.NgayDangKy.Year;
                                    //Updated by Hungtq, 24/08/2012. Cap nhat thong tin to khai sua doi bo sung trong da dang ky
#if SXXK_V3 || SXXK_V4
                                    try
                                    {
                                        if (TKMD.ActionStatus == (short)ActionStatus.ToKhaiSua)
                                            TKMD.CapNhatThongTinHangToKhaiSua();
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                        e.Error.Message.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiSuaDuocDuyet, string.Format("Tờ khai sửa:\r\nMã Hải quan: {0}\r\nMã doanh nghiệp: {1}\r\nTên doanh nghiệp: {2}\r\nSố tờ khai: {3}\r\nMã loại hình: {4}\r\nNăm đăng ký: {5}", TKMD.MaHaiQuan, TKMD.MaDoanhNghiep, TKMD.TenDoanhNghiep, TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK));
                                        SendMail("Lỗi cập nhật dữ liệu tờ khai sửa được duyệt", TKMD.MaHaiQuan, e);
                                    }
#endif
                                }
                                /*   else*/
                                //e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.None, noidung);

                                isDeleteMsg = true;
                            }
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            isDeleteMsg = true;
                            //Khanhhn - Bổ sung không cập nhật phân luồng khi statement rỗng
                            if (TKMD.ActionStatus != (short)ActionStatus.ToKhaiSua && !string.IsNullOrEmpty(feedbackContent.AdditionalInformations[0].Statement))
                                TKMD.HUONGDAN = noidung;
                            else
                            {
                                //Khanhhn - Bổ sung chuyển trạng thái tờ khai sửa sau khi phân luồng lại
                                TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                KhaiBaoSua.XoaSoTNCu(TKMD.SoToKhai, TKMD.NgayDangKy.Year, TKMD.MaLoaiHinh, TKMD.MaHaiQuan, TKMD.MaDoanhNghiep, LoaiKhaiBao.ToKhai);
                                NoiDungChinhSuaTKForm ndDieuChinh = new NoiDungChinhSuaTKForm();
                                ndDieuChinh.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
                                foreach (NoiDungDieuChinhTK item in ndDieuChinh.ListNoiDungDieuChinhTK)
                                {
                                    item.TrangThai = TrangThaiXuLy.DA_DUYET;
                                }
#if SXXK_V3 || SXXK_V4
                                try
                                {

                                    TKMD.CapNhatThongTinHangToKhaiSua();
                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                    e.Error.Message.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiSuaDuocDuyet, string.Format("Tờ khai sửa:\r\nMã Hải quan: {0}\r\nMã doanh nghiệp: {1}\r\nTên doanh nghiệp: {2}\r\nSố tờ khai: {3}\r\nMã loại hình: {4}\r\nNăm đăng ký: {5}", TKMD.MaHaiQuan, TKMD.MaDoanhNghiep, TKMD.TenDoanhNghiep, TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK));
                                    SendMail("Lỗi cập nhật dữ liệu tờ khai sửa được duyệt", TKMD.MaHaiQuan, e);
                                }
#endif
                                if (!string.IsNullOrEmpty(feedbackContent.AdditionalInformations[0].Statement))
                                    TKMD.HUONGDAN = noidung;
                            }
                            string idTax = string.Empty;
                            string tenKhobac = string.Empty;
                            foreach (AdditionalInformation item in feedbackContent.AdditionalInformations)
                            {
                                TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                if (item.Statement == AdditionalInformationStatement.LUONG_XANH)
                                    TKMD.PhanLuong = TrangThaiPhanLuong.LUONG_XANH;
                                else if (item.Statement == AdditionalInformationStatement.LUONG_VANG)
                                    TKMD.PhanLuong = TrangThaiPhanLuong.LUONG_VANG;
                                else if (item.Statement == AdditionalInformationStatement.LUONG_DO)
                                    TKMD.PhanLuong = TrangThaiPhanLuong.LUONG_DO;
                                else if (item.Statement == AdditionalInformationStatement.TAI_KHOAN_KHO_BAC)
                                {
                                    idTax = item.Content.Text;
                                }
                                else if (item.Statement == AdditionalInformationStatement.TEN_KHO_BAC)
                                {
                                    tenKhobac = item.Content.Text;
                                }
                            }
                            if (idTax != string.Empty && feedbackContent.AdditionalDocument != null)
                            {
                                noidung += "\r\nThông báo thuế của hệ thống Hải quan:\r\n";



                                AnDinhThue andinhthue = new AnDinhThue();
                                andinhthue.TaiKhoanKhoBac = idTax;
                                andinhthue.TKMD_ID = TKMD.ID;
                                andinhthue.TKMD_Ref = TKMD.GUIDSTR;
                                andinhthue.NgayQuyetDinh = GetNgayAnDinhThue(feedbackContent.AdditionalDocument.Issue);
                                andinhthue.NgayHetHan = GetNgayAnDinhThue(feedbackContent.AdditionalDocument.Expire);
                                andinhthue.SoQuyetDinh = feedbackContent.AdditionalDocument.Reference.Trim();
                                andinhthue.TenKhoBac = Company.KDT.SHARE.Components.ConvertFont.TCVN3ToUnicode(tenKhobac);
                                noidung += "Số quyết định: " + andinhthue.SoQuyetDinh;
                                noidung += "\r\nTài khoản kho bạc: " + idTax;
                                noidung += "\r\nTên kho bạc: " + tenKhobac;
                                noidung += andinhthue.NgayQuyetDinh.Year > 1900 ? "\r\nNgày quyết định: " + andinhthue.NgayQuyetDinh.ToString("dd/MM/yyyy") : "\r\nNgày quyết định: N/N";
                                noidung += andinhthue.NgayHetHan.Year > 1900 ? "\r\nNgày hết hạn: " + andinhthue.NgayHetHan.ToString("dd/MM/yyyy") : "\r\nNgày hết hạn: N/N";

                                andinhthue.Insert();


                                foreach (DutyTaxFee item in feedbackContent.AdditionalDocument.DutyTaxFees)
                                {
                                    ChiTiet chiTietThue = new ChiTiet();
                                    chiTietThue.IDAnDinhThue = andinhthue.ID;

                                    //Updated by Hungtq, 02/01/2013. Cap nhat tien thue, loai bo so thap phan
                                    string[] spl = item.AdValoremTaxBase.Split(new char[] { '.' });

                                    if (spl.Length == 2)
                                        if (System.Convert.ToDecimal(spl[1]) == 0)
                                            chiTietThue.TienThue = System.Convert.ToDecimal(spl[0]);
                                        else
                                            chiTietThue.TienThue = System.Convert.ToDecimal(item.AdValoremTaxBase);
                                    else
                                        chiTietThue.TienThue = System.Convert.ToDecimal(item.AdValoremTaxBase);
                                    switch (item.Type.Trim())
                                    {
                                        case DutyTaxFeeType.THUE_XNK:
                                            chiTietThue.SacThue = "XNK";
                                            break;
                                        case DutyTaxFeeType.THUE_BAO_VE_MOI_TRUONG:
                                            chiTietThue.SacThue = "BVMT";
                                            break;
                                        case DutyTaxFeeType.THUE_VAT:
                                            chiTietThue.SacThue = "VAT";
                                            break;
                                        case DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET:
                                            chiTietThue.SacThue = "TTDB";
                                            break;
                                        case DutyTaxFeeType.THUE_KHAC:
                                            chiTietThue.SacThue = "KHAC";
                                            break;
                                        case DutyTaxFeeType.THUE_CHENH_LECH_GIA:
                                            chiTietThue.SacThue = "CLG";
                                            break;
                                        case DutyTaxFeeType.THUE_CHONG_BAN_PHA_GIA:
                                            chiTietThue.SacThue = "TVCBPG";
                                            break;
                                        default:
                                            break;
                                    }

                                    foreach (AdditionalInformation additionalInfo in item.AdditionalInformations)
                                    {
                                        if (!string.IsNullOrEmpty(additionalInfo.Content.Text))
                                            switch (additionalInfo.Statement)
                                            {
                                                case "211"://Chuong
                                                    chiTietThue.Chuong = additionalInfo.Content.Text;
                                                    break;
                                                case "212"://Loai
                                                    chiTietThue.Loai = additionalInfo.Content.Text.Contains("N") ? 0 : Convert.ToInt32(additionalInfo.Content.Text);
                                                    break;
                                                case "213"://Khoan
                                                    chiTietThue.Khoan = additionalInfo.Content.Text.Contains("N") ? 0 : Convert.ToInt32(additionalInfo.Content.Text);
                                                    break;
                                                case "214"://Muc
                                                    chiTietThue.Muc = additionalInfo.Content.Text.Contains("N") ? 0 : Convert.ToInt32(additionalInfo.Content.Text);
                                                    break;
                                                case "215"://Tieu muc
                                                    chiTietThue.TieuMuc = additionalInfo.Content.Text.Contains("N") ? 0 : Convert.ToInt32(additionalInfo.Content.Text);
                                                    break;
                                            }
                                    }
                                    chiTietThue.Insert();
                                }
                            }
                            TKMD.NamDK = TKMD.NgayDangKy.Year;
                            e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocPhanLuong, noidung);
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", TKMD.MaHaiQuan, e);
                                break;
                            }
                    }
                    msgInfor = noidung;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    {
                        TKMD.Update();
                        if (GlobalSettings.SOTOKHAI_DONGBO > 0 && GlobalSettings.MA_DON_VI != null)
                        {
                            GlobalSettings.ISKHAIBAO = (GlobalSettings.SOTOKHAI_DONGBO > new ToKhaiMauDich().SelectCountSoTKThongQuan(GlobalSettings.MA_DON_VI));
                        }
                    }
                    if (isDeleteMsg)
                    {
                        DeleteMsgSend(LoaiKhaiBao.ToKhai, TKMD.ID);
                    }

                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.ToKhai, TKMD.ID);
                    e.Error.Message.XmlSaveMessage(TKMD.ID, MessageTitle.Error);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", TKMD.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, TKMD.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        public static Container_VNACCS ContainerVnaccsSendHandler(KDT_ContainerDangKy ContDK, ref string msgInfor, SendEventArgs e) 
        {
            Container_VNACCS cont = null;
            Container_VNACCS feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContainerVNACCS(e.FeedBackMessage);
                    string noidung = "";
                    if(feedbackContent.Function!=DeclarationFunction.THONG_QUAN)
                         noidung = feedbackContent.AdditionalInformationNew.Content;
                    e.FeedBackMessage.XmlSaveMessage(ContDK.ID, MessageTitle.ResultFeedback, noidung);
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            e.FeedBackMessage.XmlSaveMessage(ContDK.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            ContDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_CO_PHAN_HOI_QUA_KVGS;
                            ContDK.Update();
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            e.FeedBackMessage.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);
                            ContDK.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                ContDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                ContDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else ContDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                ContDK.SoTiepNhan,ContDK.NgayTiepNhan.ToString(sfmtVnDateTime)});
                            ContDK.TrangThaiXuLy = TrangThaiXuLy.DA_CO_PHAN_HOI_QUA_KVGS;
                            ContDK.Update();
                            isDeleteMsg = false;
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            ContDK.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            e.FeedBackMessage.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);
                            if (feedbackContent.Acceptance.Length == 10)
                                ContDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                ContDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else ContDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                            noidung += "Cấp số tiếp nhận khai báo";
                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                ContDK.SoTiepNhan,ContDK.NgayTiepNhan.ToString(sfmtVnDateTime)});

                            if (ContDK.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET || ContDK.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET || ContDK.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_CO_PHAN_HOI_QUA_KVGS)
                            {
                                ContDK.TrangThaiXuLy = TrangThaiXuLy.DA_CO_DANH_SACH_HANG_QUA_KVGS;
                            }
                            cont = Company.KDT.SHARE.Components.Helpers.GetFeedBackContainerVNACCS(e.FeedBackMessage);
                            ContDK.ThoiGianXuatDL = Convert.ToDateTime(feedbackContent.TimeExport);
                            ContDK.LuongTK = Convert.ToInt32(feedbackContent.Channel);
                            ContDK.CustomsStatus = feedbackContent.CustomsStatus;
                            ContDK.Note = feedbackContent.Note.ToString();
                            ContDK.LocationControl = feedbackContent.LocationControl.ToString();
                            ContDK.CargoCtrlNo = feedbackContent.CargoCtrlNo.ToString();
                            ContDK.TransportationCode = Convert.ToInt32(feedbackContent.TransportationCode);
                            ContDK.ArrivalDeparture = Convert.ToDateTime(feedbackContent.ArrivalDeparture);
                            if (!string.IsNullOrEmpty(feedbackContent.IsContainer))
                            {
                                if (feedbackContent.IsContainer=="1")
                                {
                                    foreach (TransportEquipment item in feedbackContent.TransportEquipments.TransportEquipment)
                                    {
                                        foreach (KDT_ContainerBS items in ContDK.ListCont)
                                        {
                                            if (items.SoContainer.Trim() == item.Container.Trim())
                                            {
                                                items.SoContainer = item.Container.Trim();
                                                items.SoSeal = item.Seal.Trim();
                                                items.CustomsSeal = item.CustomsSeal;
                                                items.Code = item.CodeContent;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ContDK.CargoPiece = feedbackContent.TransportEquipment_HR.CargoPiece;
                                    ContDK.PieceUnitCode = feedbackContent.TransportEquipment_HR.PieceUnitCode;
                                    ContDK.CargoWeight = feedbackContent.TransportEquipment_HR.CargoWeight;
                                    ContDK.WeightUnitCode = feedbackContent.TransportEquipment_HR.WeightUnitCode;
                                }
                                //for (int i = 0; i < feedbackContent.TransportEquipments.TransportEquipment.Count; i++)
                                //{
                                //    foreach (KDT_ContainerBS item in ContDK.ListCont)
                                //    {
                                //        if (item.SoContainer.Trim() == feedbackContent.TransportEquipments.TransportEquipment[i].Container.Trim())
                                //        {
                                //            item.SoContainer = feedbackContent.TransportEquipments.TransportEquipment[i].Container.Trim();
                                //            item.SoSeal = feedbackContent.TransportEquipments.TransportEquipment[i].Seal.Trim();
                                //            item.CustomsSeal = feedbackContent.TransportEquipments.TransportEquipment[i].CustomsSeal;
                                //            item.Code = feedbackContent.TransportEquipments.TransportEquipment[i].CodeContent;
                                //        }   
                                //    }
                                //}
                            }
                            else
                            {
                                ContDK.CargoPiece = feedbackContent.TransportEquipment_HR.CargoPiece;
                                ContDK.PieceUnitCode = feedbackContent.TransportEquipment_HR.PieceUnitCode;
                                ContDK.CargoWeight = feedbackContent.TransportEquipment_HR.CargoWeight;
                                ContDK.WeightUnitCode = feedbackContent.TransportEquipment_HR.WeightUnitCode;
                            }
                            ContDK.InsertUpdateFull();
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(ContDK.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", ContDK.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.ContainerKVGS, ContDK.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        ContDK.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.ContainerKVGS, ContDK.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", ContDK.MaHQ, e);
                    ContDK.Update();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return feedbackContent;
        }
        public static FeedBackContent BSContainerSendHandler(KDT_ContainerDangKy ContDK, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (ContDK.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                ContDK.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                ContDK.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(ContDK.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            ContDK.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            ContDK.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            if (feedbackContent.Issue == DeclarationIssuer.Container_KVGS)
                            {

                            }
                            else
                            {
                                string[] ketqua = noidung.Split('/');

                                if ((ContDK.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo";
                                    ContDK.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else if (ContDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || ContDK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || ContDK.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                                {
                                    noidung = "Cấp số tiếp nhận khai báo";
                                    ContDK.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                ContDK.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    ContDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    ContDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else ContDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                ContDK.SoTiepNhan,ContDK.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            }
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, ContDK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiPhuKien, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (ContDK.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {    
                                ContDK.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                ContDK.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);
                                ContDK.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                //ContDK.TransferDataToSXXK();
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(ContDK.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", ContDK.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.Container, ContDK.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        ContDK.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.PhuKien_TK, ContDK.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", ContDK.MaHQ, e);
                    ContDK.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    ContDK.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, ContDK.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        public static FeedBackContent DinhDanhHangHoaSendHandler(KDT_VNACCS_CapSoDinhDanh capSoDinhDanh, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    string noidung = "";
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    if (feedbackContent.CargoCtrlNo !=null)
                    {
                        noidung = "Cấp số định danh khai báo hàng hóa";
                    }
                    else
                    {
                        noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    }
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (capSoDinhDanh.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                capSoDinhDanh.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                capSoDinhDanh.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(capSoDinhDanh.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (capSoDinhDanh.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                            {
                                capSoDinhDanh.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;   
                            }
                            capSoDinhDanh.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            if (feedbackContent.Issue == DeclarationIssuer.DinhDanh)
                            {

                            }
                            else
                            {
                                string[] ketqua = noidung.Split('/');

                                if ((capSoDinhDanh.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo";
                                    capSoDinhDanh.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                }
                                else if (capSoDinhDanh.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || capSoDinhDanh.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || capSoDinhDanh.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                                {
                                    noidung = "Cấp số tiếp nhận khai báo";
                                    capSoDinhDanh.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                capSoDinhDanh.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    capSoDinhDanh.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    capSoDinhDanh.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else capSoDinhDanh.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                capSoDinhDanh.SoTiepNhan,capSoDinhDanh.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            }
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, capSoDinhDanh.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiPhuKien, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = false;
                            if (capSoDinhDanh.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                            {                                
                                capSoDinhDanh.SoDinhDanh = feedbackContent.CargoCtrlNo.Reference;
                                capSoDinhDanh.NgayCap=DateTime.ParseExact(feedbackContent.CargoCtrlNo.Issue,sfmtDateTime,null);
                                capSoDinhDanh.CodeContent = feedbackContent.CargoCtrlNo.CodeContent;

                                noidung += string.Format("\r\nSố định danh: {0}\r\nNgày cấp : {1}", new object[]{
                                capSoDinhDanh.SoDinhDanh,capSoDinhDanh.NgayCap.ToString(sfmtVnDateTime)
                            });
                             Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, capSoDinhDanh.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiPhuKien, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(capSoDinhDanh.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", capSoDinhDanh.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.Container, capSoDinhDanh.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        capSoDinhDanh.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.PhuKien_TK, capSoDinhDanh.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", capSoDinhDanh.MaHQ, e);
                    capSoDinhDanh.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    capSoDinhDanh.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, capSoDinhDanh.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        public static FeedBackContent ContractRefrenceSendHandler(KDT_VNACCS_BaoCaoQuyetToan_MMTB goodItem, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            else
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            goodItem.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((goodItem.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || goodItem.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận khai báo sửa";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET_DASUACHUA;
                            }

                            goodItem.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                goodItem.SoTN,goodItem.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, goodItem.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterContractRefrence, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", goodItem.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.ContractReference, goodItem.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        goodItem.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.ContractReference, goodItem.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", goodItem.MaHQ, e);
                    if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    goodItem.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, goodItem.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        public static FeedBackContent BillOfLadingNewSendHandler(KDT_VNACCS_BillOfLadingNew billOfLadingNew, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(billOfLadingNew.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            billOfLadingNew.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }

                            billOfLadingNew.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                billOfLadingNew.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                billOfLadingNew.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else billOfLadingNew.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                billOfLadingNew.SoTN,billOfLadingNew.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, billOfLadingNew.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterBillOfLadingNew, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (billOfLadingNew.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(billOfLadingNew.ID, MessageTitle.RegisterBillOfLadingNew, noidung);
                                billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(billOfLadingNew.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", billOfLadingNew.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.BillOfLadingNew, billOfLadingNew.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        billOfLadingNew.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.BillOfLadingNew, billOfLadingNew.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", billOfLadingNew.MaHQ, e);
                    billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    billOfLadingNew.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, billOfLadingNew.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        public static FeedBackContent StorageAreasProductionSendHandler(KDT_VNACCS_StorageAreasProduction storageAreasProduction, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            else if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(storageAreasProduction.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }
                            else if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET_DASUACHUA;
                            }

                            storageAreasProduction.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                storageAreasProduction.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                storageAreasProduction.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else storageAreasProduction.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                storageAreasProduction.SoTN,storageAreasProduction.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, storageAreasProduction.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterStorageAreasProduction, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (storageAreasProduction.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                e.FeedBackMessage.XmlSaveMessage(storageAreasProduction.ID, MessageTitle.RegisterStorageAreasProduction, noidung);
                                storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (storageAreasProduction.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                e.FeedBackMessage.XmlSaveMessage(storageAreasProduction.ID, MessageTitle.RegisterStorageAreasProduction, noidung);
                                storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (storageAreasProduction.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                e.FeedBackMessage.XmlSaveMessage(storageAreasProduction.ID, MessageTitle.RegisterStorageAreasProduction, noidung);
                                storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(storageAreasProduction.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", storageAreasProduction.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.StorageAreasProduction, storageAreasProduction.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        storageAreasProduction.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.StorageAreasProduction, storageAreasProduction.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", storageAreasProduction.MaHQ, e);
                    if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, storageAreasProduction.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent TransportEquipmentSendHandler(KDT_VNACCS_TransportEquipment transportEquipment, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (transportEquipment.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                transportEquipment.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                transportEquipment.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(transportEquipment.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (transportEquipment.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                transportEquipment.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (transportEquipment.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                transportEquipment.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            transportEquipment.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((transportEquipment.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                transportEquipment.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (transportEquipment.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || transportEquipment.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || transportEquipment.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                transportEquipment.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            transportEquipment.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                transportEquipment.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                transportEquipment.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else transportEquipment.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                transportEquipment.SoTN,transportEquipment.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, transportEquipment.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterTransportEquipment, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (transportEquipment.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                transportEquipment.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                transportEquipment.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(transportEquipment.ID, MessageTitle.RegisterTransportEquipment, noidung);
                                transportEquipment.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(transportEquipment.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", transportEquipment.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.License, transportEquipment.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        transportEquipment.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.TransportEquipment, transportEquipment.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", transportEquipment.MaHQ, e);
                    if (transportEquipment.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        transportEquipment.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (transportEquipment.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        transportEquipment.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    transportEquipment.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, transportEquipment.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent GoodItemSendHandler(KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            goodItem.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || goodItem.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            goodItem.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                goodItem.SoTN,goodItem.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, goodItem.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterGoodItem, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem , noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", goodItem.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.GoodItem, goodItem.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        goodItem.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.GoodItem, goodItem.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", goodItem.MaHQ, e);
                    if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    goodItem.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, goodItem.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent GoodItemSendHandlerNew(KDT_VNACCS_BaoCaoQuyetToan_TT39 goodItem, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if(goodItem.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            }
                            else
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO )
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            goodItem.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((goodItem.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || goodItem.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận khai báo sửa";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET_DASUACHUA;                                
                            }

                            goodItem.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                goodItem.SoTN,goodItem.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, goodItem.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterGoodItem, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;                                
                            }

                            goodItem.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                goodItem.SoTN,goodItem.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, goodItem.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterGoodItem, noidung);
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }

                            goodItem.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                goodItem.SoTN,goodItem.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, goodItem.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterGoodItem, noidung);
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", goodItem.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.GoodItem, goodItem.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        goodItem.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.GoodItem, goodItem.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", goodItem.MaHQ, e);
                    if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    goodItem.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, goodItem.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent GoodItemSendHandlerNew(KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New goodItem, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            }
                            else
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            goodItem.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((goodItem.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || goodItem.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }
                            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận khai báo sửa";
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET_DASUACHUA;
                            }

                            goodItem.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                goodItem.SoTN,goodItem.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, goodItem.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterGoodItem, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            goodItem.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                goodItem.SoTN,goodItem.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, goodItem.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterGoodItem, noidung);
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (goodItem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem, noidung);
                                goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            goodItem.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else goodItem.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                goodItem.SoTN,goodItem.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, goodItem.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterGoodItem, noidung);
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(goodItem.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", goodItem.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.License, goodItem.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        goodItem.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.GoodItem, goodItem.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", goodItem.MaHQ, e);
                    if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    goodItem.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, goodItem.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent LicenseSendHandler(KDT_VNACCS_License license, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (license.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                license.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                license.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(license.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            license.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            license.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                                string[] ketqua = noidung.Split('/');

                                if ((license.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo";
                                    license.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else if (license.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || license.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || license.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                                {
                                    noidung = "Cấp số tiếp nhận khai báo";
                                    license.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                license.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    license.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    license.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else license.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                license.SoTN,license.NgayTN.ToString(sfmtVnDateTime)
                            });
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, license.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterLicense, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (license.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                license.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                license.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(license.ID, MessageTitle.RegisterLicense, noidung);
                                license.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(license.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", license.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.License, license.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        license.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.License, license.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", license.MaHQ, e);
                    license.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    license.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, license.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent ContractDocumentSendHandler(KDT_VNACCS_ContractDocument contractDocument, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (contractDocument.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                contractDocument.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                contractDocument.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(contractDocument.ID, MessageTitle.RegisterContractDocument, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            contractDocument.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            contractDocument.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                                string[] ketqua = noidung.Split('/');

                                if ((contractDocument.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo";
                                    contractDocument.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else if (contractDocument.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || contractDocument.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || contractDocument.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                                {
                                    noidung = "Cấp số tiếp nhận khai báo";
                                    contractDocument.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                contractDocument.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    contractDocument.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    contractDocument.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else contractDocument.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                contractDocument.SoTN,contractDocument.NgayTN.ToString(sfmtVnDateTime)
                            });
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, contractDocument.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterContractDocument, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (contractDocument.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                contractDocument.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                contractDocument.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(contractDocument.ID, MessageTitle.RegisterContractDocument, noidung);
                                contractDocument.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(contractDocument.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", contractDocument.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.ContractDocument, contractDocument.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        contractDocument.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.ContractDocument, contractDocument.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", contractDocument.MaHQ, e);
                    contractDocument.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    contractDocument.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, contractDocument.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent CommercialInvoiceSendHandler(KDT_VNACCS_CommercialInvoice commercialInvoice, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(commercialInvoice.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            commercialInvoice.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                                string[] ketqua = noidung.Split('/');

                                if ((commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo";
                                    commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else if (commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                                {
                                    noidung = "Cấp số tiếp nhận khai báo";
                                    commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                commercialInvoice.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    commercialInvoice.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    commercialInvoice.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else commercialInvoice.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                commercialInvoice.SoTN,commercialInvoice.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, commercialInvoice.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterCommercialInvoicer, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (commercialInvoice.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                
                                commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(commercialInvoice.ID, MessageTitle.RegisterCommercialInvoicer, noidung);
                                commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(commercialInvoice.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", commercialInvoice.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.CommercialInvoice, commercialInvoice.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        commercialInvoice.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.CommercialInvoice, commercialInvoice.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", commercialInvoice.MaHQ, e);
                    commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    commercialInvoice.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, commercialInvoice.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent BillOfLadingSendHandler(KDT_VNACCS_BillOfLading billOfLading, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (billOfLading.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                billOfLading.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                billOfLading.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(billOfLading.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            billOfLading.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            billOfLading.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                                string[] ketqua = noidung.Split('/');

                                if ((billOfLading.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo";
                                    billOfLading.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else if (billOfLading.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || billOfLading.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || billOfLading.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                                {
                                    noidung = "Cấp số tiếp nhận khai báo";
                                    billOfLading.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                billOfLading.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    billOfLading.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    billOfLading.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else billOfLading.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                billOfLading.SoTN,billOfLading.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, billOfLading.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterBillOfLading, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (billOfLading.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                
                                billOfLading.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                billOfLading.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(billOfLading.ID, MessageTitle.RegisterBillOfLading, noidung);
                                billOfLading.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(billOfLading.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", billOfLading.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.BillOfLading, billOfLading.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        billOfLading.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.BillOfLading, billOfLading.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", billOfLading.MaHQ, e);
                    billOfLading.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    billOfLading.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, billOfLading.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent ContainersSendHandler(KDT_VNACCS_Container_Detail container, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (container.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                container.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                container.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(container.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            container.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            container.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                                string[] ketqua = noidung.Split('/');

                                if ((container.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo";
                                    container.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else if (container.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || container.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || container.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                                {
                                    noidung = "Cấp số tiếp nhận khai báo";
                                    container.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                container.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    container.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    container.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else container.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                container.SoTN,container.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, container.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterContainer, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (container.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                
                                container.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                container.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(container.ID, MessageTitle.RegisterContainer, noidung);
                                container.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(container.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", container.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.Containers, container.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        container.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.Containers, container.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", container.MaHQ, e);
                    container.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    container.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, container.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent AdditionalDocumentSendHandler(KDT_VNACCS_AdditionalDocument additionalDocument, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (additionalDocument.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                additionalDocument.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                additionalDocument.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(additionalDocument.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            additionalDocument.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            additionalDocument.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                                string[] ketqua = noidung.Split('/');

                                if ((additionalDocument.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo";
                                    additionalDocument.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else if (additionalDocument.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || additionalDocument.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || additionalDocument.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                                {
                                    noidung = "Cấp số tiếp nhận khai báo";
                                    additionalDocument.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                additionalDocument.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    additionalDocument.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    additionalDocument.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else additionalDocument.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                additionalDocument.SoTN,additionalDocument.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, additionalDocument.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterAdditionalDocumentr, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (additionalDocument.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                
                                additionalDocument.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                additionalDocument.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(additionalDocument.ID, MessageTitle.RegisterAdditionalDocumentr, noidung);
                                additionalDocument.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(additionalDocument.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", additionalDocument.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.AdditionalDocument, additionalDocument.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        additionalDocument.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.AdditionalDocument, additionalDocument.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", additionalDocument.MaHQ, e);
                    additionalDocument.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    additionalDocument.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, additionalDocument.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent OverTimeSendHandler(KDT_VNACCS_OverTime overTime, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (overTime.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                overTime.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                overTime.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(overTime.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (overTime.TrangThaiXuLy==TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                overTime.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;   
                            }
                            overTime.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                                string[] ketqua = noidung.Split('/');

                                if ((overTime.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo";
                                    overTime.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else if (overTime.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || overTime.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || overTime.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                                {
                                    noidung = "Cấp số tiếp nhận khai báo";
                                    overTime.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                                }

                                overTime.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    overTime.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    overTime.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else overTime.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                overTime.SoTN,overTime.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, overTime.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterOverTime, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (overTime.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                e.FeedBackMessage.XmlSaveMessage(overTime.ID, MessageTitle.RegisterOverTime, noidung);
                                overTime.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(overTime.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", overTime.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.OverTime, overTime.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        overTime.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.OverTime, overTime.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", overTime.MaHQ, e);
                    if (overTime.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        overTime.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    overTime.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, overTime.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent CertificateOfOriginSendHandler(KDT_VNACCS_CertificateOfOrigin certificateOfOrigin, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(certificateOfOrigin.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            certificateOfOrigin.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                                string[] ketqua = noidung.Split('/');

                                if ((certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo";
                                    certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else if (certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                                {
                                    noidung = "Cấp số tiếp nhận khai báo";
                                    certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                certificateOfOrigin.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    certificateOfOrigin.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    certificateOfOrigin.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else certificateOfOrigin.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                certificateOfOrigin.SoTN,certificateOfOrigin.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, certificateOfOrigin.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiPhuKien, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (certificateOfOrigin.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(certificateOfOrigin.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);
                                certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(certificateOfOrigin.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", certificateOfOrigin.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.Container, certificateOfOrigin.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        certificateOfOrigin.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.PhuKien_TK, certificateOfOrigin.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", certificateOfOrigin.MaHQ, e);
                    certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    certificateOfOrigin.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, certificateOfOrigin.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent PhieuKhoSendHandler(KDT_KhoCFS_DangKy PhieuKhoDK, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    //bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    //string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    string noidung = feedbackContent.MessageContent;

                    if (noidung == @"Nội dung khai báo đã được tiếp nhận.")
                    {
                        e.FeedBackMessage.XmlSaveMessage(PhieuKhoDK.ID, MessageTitle.PhieuNhapKho, noidung);
                        if (PhieuKhoDK.TrangThaiXuLy==TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        }
                        else if (PhieuKhoDK.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                        {
                            PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                        }
                        PhieuKhoDK.NgayTiepNhan = DateTime.Now;
                        msgInfor = noidung;
                        Company.Controls.KDTMessageBoxControl msg = new Company.Controls.KDTMessageBoxControl();
                        msg.HQMessageString = "Thông báo từ Kho CFS";
                        msg.ShowYesNoButton = false;
                        msg.ShowErrorButton = true;
                        msg.MaHQ = PhieuKhoDK.MaHQ;

                        msg.MessageString = msgInfor;
                        //msg.exceptionString = e.Error.StackTrace == null ? e.Error.Message : e.Error.StackTrace;
                        msg.ShowDialog();
                        
                    }
                    else if (noidung == @"Dữ liệu khai báo không hợp lệ.")
                    {
                        e.FeedBackMessage.XmlSaveMessage(PhieuKhoDK.ID, MessageTitle.PhieuNhapKho, noidung);
                        PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                        msgInfor = noidung;
                        //SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", PhieuKhoDK.MaHQ, e);

                        Company.Controls.KDTMessageBoxControl msg = new Company.Controls.KDTMessageBoxControl();
                        msg.HQMessageString = "Thông báo từ Kho CFS";
                        msg.ShowYesNoButton = false;
                        msg.ShowErrorButton = true;
                        msg.MaHQ = PhieuKhoDK.MaHQ;

                        msg.MessageString = msgInfor;
                        //msg.exceptionString = e.Error.StackTrace == null ? e.Error.Message : e.Error.StackTrace;
                        msg.ShowDialog();

                    }
                    else
                    {
                        e.FeedBackMessage.XmlSaveMessage(PhieuKhoDK.ID, MessageTitle.PhieuNhapKho, noidung);
                        if (PhieuKhoDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                        }
                        else if (PhieuKhoDK.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                        {
                            PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                        }
                        msgInfor = noidung;
                        Company.Controls.KDTMessageBoxControl msg = new Company.Controls.KDTMessageBoxControl();
                        msg.HQMessageString = "Thông báo từ Kho CFS";
                        msg.ShowYesNoButton = false;
                        msg.ShowErrorButton = true;
                        msg.MaHQ = PhieuKhoDK.MaHQ;

                        msg.MessageString = msgInfor;
                        //msg.exceptionString = e.Error.StackTrace == null ? e.Error.Message : e.Error.StackTrace;
                        msg.ShowDialog();
                    }
                    PhieuKhoDK.Update();
                    //switch (feedbackContent.Function)
                    //{
                    //    case DeclarationFunction.KHONG_CHAP_NHAN:
                    //        noidung = GetErrorContent(feedbackContent);
                    //        if (PhieuKhoDK.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                    //            PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                    //        else
                    //            PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                    //        e.FeedBackMessage.XmlSaveMessage(PhieuKhoDK.ID, MessageTitle.TuChoiTiepNhan, noidung);
                    //        isDeleteMsg = true;
                    //        break;
                    //    case DeclarationFunction.CHUA_XU_LY:
                    //        break;
                    //    case DeclarationFunction.CAP_SO_TIEP_NHAN:
                    //        if (feedbackContent.Issue == DeclarationIssuer.LayTTHangGuiKho)
                    //        {

                    //        }
                    //        else
                    //        {
                    //            string[] ketqua = noidung.Split('/');

                    //            if ((PhieuKhoDK.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                    //            {
                    //                noidung = "Cấp số tiếp nhận hủy khai báo";
                    //                PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                    //            }
                    //            else if (PhieuKhoDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || PhieuKhoDK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || PhieuKhoDK.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                    //            {
                    //                noidung = "Cấp số tiếp nhận khai báo";
                    //                PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                    //            }

                    //            PhieuKhoDK.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                    //            if (feedbackContent.Acceptance.Length == 10)
                    //                PhieuKhoDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                    //            else if (feedbackContent.Acceptance.Length == 19)
                    //                PhieuKhoDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                    //            else PhieuKhoDK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                    //            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                    //            PhieuKhoDK.SoTiepNhan,PhieuKhoDK.NgayTiepNhan.ToString(sfmtVnDateTime)
                    //        });
                    //        }
                    //        Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, PhieuKhoDK.ID, Company.KDT.SHARE.Components.MessageTitle.PhieuNhapKho, noidung);

                    //        break;
                    //    case DeclarationFunction.THONG_QUAN:
                    //        isDeleteMsg = true;
                    //        if (PhieuKhoDK.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                    //        {
                    //            
                    //            PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                    //            PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                    //            e.FeedBackMessage.XmlSaveMessage(PhieuKhoDK.ID, MessageTitle.PhieuNhapKho, noidung);
                    //            PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;

                    //        }
                    //        break;
                    //    default:
                    //        {
                    //            e.FeedBackMessage.XmlSaveMessage(PhieuKhoDK.ID, MessageTitle.Error, noidung);
                    //           // SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", PhieuKhoDK.MaHQ, e);
                    //            break;
                    //        }

                    //}
                    //if (isDeleteMsg)
                    //    DeleteMsgSend(LoaiKhaiBao.PhieuNhapKho, PhieuKhoDK.ID);
                    //if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    //    PhieuKhoDK.Update();
                    //msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.PhieuNhapKho, PhieuKhoDK.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", PhieuKhoDK.MaHQ, e);
                    if (PhieuKhoDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (PhieuKhoDK.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                    {
                        PhieuKhoDK.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    PhieuKhoDK.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                //SendMail(msgTitle, PhieuKhoDK.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        public static FeedBackContent TotalInventoryReportSendHandler(KDT_VNACCS_TotalInventoryReport totalInventoryReport, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            }
                            else
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (totalInventoryReport.TrangThaiXuLy==TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            totalInventoryReport.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET_DASUACHUA;
                            }

                            totalInventoryReport.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                totalInventoryReport.SoTiepNhan,totalInventoryReport.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, totalInventoryReport.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterTotalInventoryReport, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            totalInventoryReport.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                totalInventoryReport.SoTiepNhan,totalInventoryReport.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, totalInventoryReport.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterTotalInventoryReport, noidung);
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            totalInventoryReport.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                totalInventoryReport.SoTiepNhan,totalInventoryReport.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, totalInventoryReport.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterTotalInventoryReport, noidung);
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", totalInventoryReport.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.TotalInventoryReport, totalInventoryReport.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        totalInventoryReport.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.TotalInventoryReport, totalInventoryReport.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", totalInventoryReport.MaHaiQuan, e);
                    if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    totalInventoryReport.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, totalInventoryReport.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        public static FeedBackContent TotalInventoryReportNewSendHandler(Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport totalInventoryReport, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            }
                            else
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            totalInventoryReport.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận khai báo sửa";
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET_DASUACHUA;
                            }

                            totalInventoryReport.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                totalInventoryReport.SoTiepNhan,totalInventoryReport.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, totalInventoryReport.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterTotalInventoryReport, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            totalInventoryReport.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                totalInventoryReport.SoTiepNhan,totalInventoryReport.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, totalInventoryReport.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterTotalInventoryReport, noidung);
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            totalInventoryReport.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                totalInventoryReport.SoTiepNhan,totalInventoryReport.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, totalInventoryReport.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterTotalInventoryReport, noidung);
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", totalInventoryReport.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.TotalInventoryReport, totalInventoryReport.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        totalInventoryReport.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.TotalInventoryReport, totalInventoryReport.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", totalInventoryReport.MaHaiQuan, e);
                    if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    totalInventoryReport.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, totalInventoryReport.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        public static FeedBackContent ImportWareHouseSendHandler(T_KDT_VNACCS_WarehouseImport warehouseImport, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (warehouseImport.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else if (warehouseImport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                            {
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            }
                            else
                            {
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(warehouseImport.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (warehouseImport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (warehouseImport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (warehouseImport.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            warehouseImport.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((warehouseImport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (warehouseImport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || warehouseImport.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || warehouseImport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (warehouseImport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận sửa khai báo";
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }

                            warehouseImport.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                warehouseImport.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                warehouseImport.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else warehouseImport.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                warehouseImport.SoTN,warehouseImport.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, warehouseImport.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterWareHouseImport, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (warehouseImport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(warehouseImport.ID, MessageTitle.RegisterWareHouseImport, noidung);
                                warehouseImport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(warehouseImport.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", warehouseImport.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.License, warehouseImport.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        warehouseImport.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.TransportEquipment, warehouseImport.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", warehouseImport.MaHQ, e);
                    if (warehouseImport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        warehouseImport.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (warehouseImport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        warehouseImport.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (warehouseImport.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        warehouseImport.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    warehouseImport.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, warehouseImport.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent ExportWareHouseSendHandler(T_KDT_VNACCS_WarehouseExport warehouseExport, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (warehouseExport.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else if (warehouseExport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                            {
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            }
                            else
                            {
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(warehouseExport.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (warehouseExport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (warehouseExport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (warehouseExport.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            warehouseExport.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((warehouseExport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (warehouseExport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || warehouseExport.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || warehouseExport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (warehouseExport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận sửa khai báo";
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }

                            warehouseExport.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                warehouseExport.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                warehouseExport.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else warehouseExport.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                warehouseExport.SoTN,warehouseExport.NgayTN.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, warehouseExport.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterWareHouseExport, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            if (warehouseExport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {                                
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(warehouseExport.ID, MessageTitle.RegisterWareHouseExport, noidung);
                                warehouseExport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(warehouseExport.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", warehouseExport.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.License, warehouseExport.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        warehouseExport.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.TransportEquipment, warehouseExport.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", warehouseExport.MaHQ, e);
                    if (warehouseExport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        warehouseExport.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (warehouseExport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        warehouseExport.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (warehouseExport.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        warehouseExport.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, warehouseExport.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
#if SXXK_V3 || SXXK_V4

        public static FeedBackContent ScrapInformationSendHandler(KDT_VNACCS_ScrapInformation scrapInformation, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            scrapInformation.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(scrapInformation.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (scrapInformation.TrangThaiXuLy==TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            scrapInformation.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = noidung.Split('/');

                            if ((scrapInformation.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (scrapInformation.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || scrapInformation.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || scrapInformation.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            scrapInformation.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                scrapInformation.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                scrapInformation.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else scrapInformation.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                scrapInformation.SoTiepNhan,scrapInformation.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, scrapInformation.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterScrapInformation, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            if (scrapInformation.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (scrapInformation.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (scrapInformation.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            scrapInformation.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                scrapInformation.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                scrapInformation.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else scrapInformation.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                scrapInformation.SoTiepNhan,scrapInformation.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, scrapInformation.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterScrapInformation, noidung);
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            if (scrapInformation.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (scrapInformation.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (scrapInformation.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                            {
                                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            scrapInformation.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                scrapInformation.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                scrapInformation.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else scrapInformation.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                scrapInformation.SoTiepNhan,scrapInformation.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, scrapInformation.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterScrapInformation, noidung);
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(scrapInformation.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", scrapInformation.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.ScrapInformation, scrapInformation.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        scrapInformation.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.ScrapInformation, scrapInformation.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", scrapInformation.MaHaiQuan, e);
                    if (scrapInformation.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        scrapInformation.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    scrapInformation.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, scrapInformation.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        //public static FeedBackContent TotalInventoryReportSendHandler(KDT_VNACCS_TotalInventoryReport totalInventoryReport, ref string msgInfor, SendEventArgs e)
        //{
        //    Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
        //    try
        //    {
        //        msgInfor = string.Empty;
        //        if (e.Error == null)
        //        {
        //            bool isDeleteMsg = false;
        //            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
        //            string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
        //            if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
        //            {
        //                GlobalSettings.IsRemember = false;
        //            }
        //            switch (feedbackContent.Function.Trim())
        //            {
        //                case DeclarationFunction.KHONG_CHAP_NHAN:
        //                    noidung = GetErrorContent(feedbackContent);
        //                    if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
        //                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
        //                    else
        //                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
        //                    e.FeedBackMessage.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.TuChoiTiepNhan, noidung);
        //                    isDeleteMsg = true;
        //                    break;
        //                case DeclarationFunction.CHUA_XU_LY:
        //                    break;
        //                case DeclarationFunction.CAP_SO_TIEP_NHAN:
        //                    string[] ketqua = noidung.Split('/');

        //                    if ((totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
        //                    {
        //                        noidung = "Cấp số tiếp nhận hủy khai báo";
        //                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
        //                    }
        //                    else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
        //                    {
        //                        noidung = "Cấp số tiếp nhận khai báo";
        //                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
        //                    }

        //                    totalInventoryReport.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
        //                    if (feedbackContent.Acceptance.Length == 10)
        //                        totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
        //                    else if (feedbackContent.Acceptance.Length == 19)
        //                        totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
        //                    else totalInventoryReport.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

        //                    noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
        //                        totalInventoryReport.SoTiepNhan,totalInventoryReport.NgayTiepNhan.ToString(sfmtVnDateTime)
        //                    });
        //                    Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, totalInventoryReport.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterTotalInventoryReport, noidung);

        //                    break;
        //                case DeclarationFunction.THONG_QUAN:
        //                    isDeleteMsg = true;
        //                    if (totalInventoryReport.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
        //                    {
        //                        
        //                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
        //                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
        //                        e.FeedBackMessage.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.RegisterTotalInventoryReport, noidung);
        //                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
        //                    }
        //                    break;
        //                default:
        //                    {
        //                        e.FeedBackMessage.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.Error, noidung);
        //                        SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", totalInventoryReport.MaHaiQuan, e);
        //                        break;
        //                    }

        //            }
        //            if (isDeleteMsg)
        //                DeleteMsgSend(LoaiKhaiBao.TotalInventoryReport, totalInventoryReport.ID);
        //            if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
        //                totalInventoryReport.Update();
        //            msgInfor = noidung;
        //        }
        //        else
        //        {
        //            DeleteMsgSend(LoaiKhaiBao.TotalInventoryReport, totalInventoryReport.ID);
        //            SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", totalInventoryReport.MaHaiQuan, e);

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        string msgTitle = string.Empty;
        //        if (!string.IsNullOrEmpty(e.FeedBackMessage))
        //        {
        //            Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
        //            msgTitle = "Không hiểu thông tin trả về từ hải quan";
        //        }
        //        else
        //        {
        //            Logger.LocalLogger.Instance().WriteMessage(e.Error);
        //            msgTitle = "Hệ thống không thể xử lý thông tin";
        //        }
        //        SendMail(msgTitle, totalInventoryReport.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
        //    }
        //    return feedbackContent;
        //}
        public static FeedBackContent NguyenPhuLieuSendHandler(NguyenPhuLieuDangKy nplDangKy, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                            {
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            }
                            else
                            {
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }

                            nplDangKy.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI )
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if(nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận sửa khai báo";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }

                            nplDangKy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);


                            nplDangKy.NamDK = (short)nplDangKy.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                nplDangKy.SoTiepNhan,nplDangKy.NgayTiepNhan.ToString(sfmtVnDateTime),nplDangKy.NamDK
                            });
                            e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiNguyenPhuLieu, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;

                            if (nplDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt nguyên phụ liệu";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            }
                            else if (nplDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo nguyên phụ liệu";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoChapNhanHuyNguyenPhuLieu, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", nplDangKy.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, nplDangKy.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        nplDangKy.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, nplDangKy.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", nplDangKy.MaHaiQuan, e);
                    if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }

                    nplDangKy.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, nplDangKy.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent SanPhamSendHandler(SanPhamDangKy sanpham, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (sanpham.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            else if (sanpham.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            else
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(sanpham.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (sanpham.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (sanpham.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (sanpham.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            sanpham.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((sanpham.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (sanpham.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || sanpham.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || sanpham.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (sanpham.TrangThaiXuLy ==TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận sửa khai báo";
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }

                            sanpham.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                sanpham.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                sanpham.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else sanpham.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            sanpham.NamDK = (short)sanpham.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                sanpham.SoTiepNhan,sanpham.NgayTiepNhan.ToString(sfmtVnDateTime),sanpham.NamDK
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, sanpham.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiSanPham, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (sanpham.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt sản phẩm";
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(sanpham.ID, MessageTitle.KhaiBaoHQDuyet, noidung);

                            }
                            else if (sanpham.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo sản phẩm";
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(sanpham.ID, MessageTitle.KhaiBaoHQHuySanPham, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(sanpham.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", sanpham.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.SanPham, sanpham.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        sanpham.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.SanPham, sanpham.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", sanpham.MaHaiQuan, e);
                    if (sanpham.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        sanpham.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (sanpham.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        sanpham.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (sanpham.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        sanpham.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    sanpham.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, sanpham.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent DinhMucSendHandler(DinhMucDangKy dinhmuc, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else
                            {
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }                            
                            e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                            }
                            else if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }                            
                            dinhmuc.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((dinhmuc.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dinhmuc.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || dinhmuc.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận khai báo sửa";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                dinhmuc.DeleteDinhMucSXXK();
                            }

                            dinhmuc.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                dinhmuc.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                dinhmuc.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else dinhmuc.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            dinhmuc.NamDK = (short)dinhmuc.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                dinhmuc.SoTiepNhan,dinhmuc.NgayTiepNhan.ToString(sfmtVnDateTime),dinhmuc.NamDK
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dinhmuc.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiDinhMuc, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (dinhmuc.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.KhaiBaoHQDuyetDinhMuc, noidung);
                                dinhmuc.TransferDataToSXXK();
                            }
                            else if (dinhmuc.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo định mức";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.KhaiBaoHQHuyDinhMuc, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", dinhmuc.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.DinhMuc, dinhmuc.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        dinhmuc.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.DinhMuc, dinhmuc.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", dinhmuc.MaHaiQuan, e);
                    if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        dinhmuc.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        dinhmuc.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    dinhmuc.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, dinhmuc.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent DinhMucSendHandler(KDT_SXXK_DinhMucThucTeDangKy dinhmuc, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else
                            {
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                            }
                            else if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            dinhmuc.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((dinhmuc.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dinhmuc.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || dinhmuc.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận khai báo sửa";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                dinhmuc.DeleteFull();
                            }

                            dinhmuc.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                dinhmuc.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                dinhmuc.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else dinhmuc.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\n", new object[]{
                                dinhmuc.SoTiepNhan,dinhmuc.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dinhmuc.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiDinhMuc, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (dinhmuc.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {

                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.KhaiBaoHQDuyetDinhMuc, noidung);
                                dinhmuc.TransferDataToSXXK();
                            }
                            else if (dinhmuc.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo định mức";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.KhaiBaoHQHuyDinhMuc, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", dinhmuc.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.DinhMuc, dinhmuc.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        dinhmuc.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.DinhMuc, dinhmuc.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", dinhmuc.MaHaiQuan, e);
                    if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        dinhmuc.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        dinhmuc.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    dinhmuc.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, dinhmuc.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent PhuKienSendHandler(KDT_SXXK_PhuKienDangKy pkdk, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(pkdk.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            pkdk.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                pkdk.SoTiepNhan,pkdk.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiPhuKien, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (pkdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt phụ kiện";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(pkdk.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;

                                //pkdk.TransferDataToSXXK();
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(pkdk.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", pkdk.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.PhuKien_TK, pkdk.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        pkdk.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.PhuKien_TK, pkdk.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", pkdk.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, pkdk.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent TuCungUngSendHandler(KDT_SXXK_BKNPLCungUngDangKy TuCungUng, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (TuCungUng.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                TuCungUng.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else if (TuCungUng.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                                TuCungUng.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            else if (TuCungUng.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                                TuCungUng.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            else
                                TuCungUng.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(TuCungUng.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((TuCungUng.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY) || (TuCungUng.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET))
                            {
                                noidung = "Cấp số tiếp nhận khai báo hủy";
                                //if (TuCungUng.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)

                                TuCungUng.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (TuCungUng.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TuCungUng.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || TuCungUng.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                TuCungUng.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            TuCungUng.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                TuCungUng.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                TuCungUng.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else TuCungUng.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                TuCungUng.SoTiepNhan,TuCungUng.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, TuCungUng.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiTCU, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (TuCungUng.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt phụ kiện";
                                TuCungUng.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(TuCungUng.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);

                                //TuCungUng.TransferDataToSXXK();
                            }
                            if (TuCungUng.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Hải quan đã hủy bản kê";
                                TuCungUng.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(TuCungUng.ID, "HQ Đồng ý hủy bảng kê", noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(TuCungUng.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", TuCungUng.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.TuCungUng, TuCungUng.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        TuCungUng.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.TuCungUng, TuCungUng.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", TuCungUng.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, TuCungUng.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent TaiXuatSendHandler(KDT_CX_TaiXuatDangKy TaiXuat, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (TaiXuat.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                TaiXuat.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else if (TaiXuat.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                                TaiXuat.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            else if (TaiXuat.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                                TaiXuat.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            else
                                TaiXuat.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(TaiXuat.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((TaiXuat.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY) || (TaiXuat.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET))
                            {
                                noidung = "Cấp số tiếp nhận khai báo hủy";
                                //if (TaiXuat.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)

                                TaiXuat.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (TaiXuat.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TaiXuat.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || TaiXuat.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                TaiXuat.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            TaiXuat.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                TaiXuat.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                TaiXuat.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else TaiXuat.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                TaiXuat.SoTiepNhan,TaiXuat.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, TaiXuat.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiTaiXuat, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (TaiXuat.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt phụ kiện";
                                TaiXuat.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(TaiXuat.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);

                                //TaiXuat.TransferDataToSXXK();
                            }
                            if (TaiXuat.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Hải quan đã hủy bản kê";
                                TaiXuat.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(TaiXuat.ID, "HQ Đồng ý hủy bảng kê", noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(TaiXuat.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", TaiXuat.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.TaiXuat, TaiXuat.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        TaiXuat.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.TaiXuat, TaiXuat.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", TaiXuat.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, TaiXuat.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent TieuHuySendHandler(KDT_CX_NPLXinHuyDangKy TieuHuy, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (TieuHuy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                TieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else if (TieuHuy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                                TieuHuy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            else if (TieuHuy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                                TieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            else
                                TieuHuy.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(TieuHuy.ID, MessageTitle.TuChoiDNTieuHuy, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((TieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY) || (TieuHuy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET))
                            {
                                noidung = "Cấp số tiếp nhận khai báo hủy";
                                //if (TaiXuat.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)

                                TieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (TieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || TieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                TieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            TieuHuy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                TieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                TieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else TieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                TieuHuy.SoTiepNhan,TieuHuy.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, TieuHuy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiTaiXuat, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (TieuHuy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt phụ kiện";
                                TieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(TieuHuy.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);

                                //TaiXuat.TransferDataToSXXK();
                            }
                            if (TieuHuy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Hải quan đã hủy bản kê";
                                TieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(TieuHuy.ID, "HQ Đồng ý hủy bảng kê", noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(TieuHuy.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", TieuHuy.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.TieuHuy, TieuHuy.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        TieuHuy.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.TaiXuat, TieuHuy.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", TieuHuy.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, TieuHuy.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent BKHangTonSendHandler(KDT_SXXK_BKHangTonKhoDangKy HangTon, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (HangTon.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(HangTon.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((HangTon.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (HangTon.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || HangTon.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || HangTon.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            HangTon.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                HangTon.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                HangTon.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else HangTon.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                HangTon.SoTiepNhan,HangTon.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, HangTon.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiCX_HangTon, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (HangTon.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt phụ kiện";
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(HangTon.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;

                                //HangTon.TransferDataToSXXK();
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HangTon.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", HangTon.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.HangTon, HangTon.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        HangTon.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.HangTon, HangTon.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", HangTon.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, HangTon.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent HangTonSendHandler(KDT_SXXK_HangTonDangKy HangTon, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (HangTon.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(HangTon.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((HangTon.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (HangTon.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || HangTon.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || HangTon.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            HangTon.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                HangTon.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                HangTon.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else HangTon.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                HangTon.SoTiepNhan,HangTon.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, HangTon.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiCX_HangTon, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (HangTon.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt phụ kiện";
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(HangTon.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);
                                HangTon.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                //HangTon.TransferDataToSXXK();
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HangTon.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", HangTon.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.HangTon, HangTon.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        HangTon.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.HangTon, HangTon.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", HangTon.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, HangTon.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent HangDuaVaoCX(HangDuaVaoDangKy nplDangKy, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            else
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            nplDangKy.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận sửa khai báo";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            nplDangKy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);


                            nplDangKy.NamDK = (short)nplDangKy.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                nplDangKy.SoTiepNhan,nplDangKy.NgayTiepNhan.ToString(sfmtVnDateTime),nplDangKy.NamDK
                            });
                            e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiNguyenPhuLieu, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;

                            if (nplDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt nguyên phụ liệu";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy.TransgferHDVToSXXK(nplDangKy);
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            }
                            else if (nplDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo nguyên phụ liệu";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoChapNhanHuyNguyenPhuLieu, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", nplDangKy.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, nplDangKy.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        nplDangKy.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, nplDangKy.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", nplDangKy.MaHaiQuan, e);
                    if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    nplDangKy.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, nplDangKy.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent ThanhLyTaiSanCoDinh(Company.BLL.SXXK.KDT_SXXK_TLTSCDDangKy TLTSCD, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (TLTSCD.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                TLTSCD.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                TLTSCD.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(TLTSCD.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((TLTSCD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                // TLTSCD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (TLTSCD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TLTSCD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || TLTSCD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                TLTSCD.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            TLTSCD.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                TLTSCD.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                TLTSCD.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else TLTSCD.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);


                            TLTSCD.NamDK = (short)TLTSCD.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                TLTSCD.SoTiepNhan,TLTSCD.NgayTiepNhan.ToString(sfmtVnDateTime),TLTSCD.NamDK
                            });
                            e.FeedBackMessage.XmlSaveMessage(TLTSCD.ID, Company.KDT.SHARE.Components.MessageTitle.CapSoTiepNhanTSCD, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;

                            if (TLTSCD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt bản thanh lý";
                                TLTSCD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(TLTSCD.ID, Company.KDT.SHARE.Components.MessageTitle.DuyetTSCD, noidung);
                            }
                            else if (TLTSCD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo bản thanh lý";
                                TLTSCD.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(TLTSCD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoChapNhanHuyNguyenPhuLieu, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(TLTSCD.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", TLTSCD.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.ThanhLyTaiSanCoDinh, TLTSCD.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        TLTSCD.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.ThanhLyTaiSanCoDinh, TLTSCD.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", TLTSCD.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, TLTSCD.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

#endif

#if GC_V3 || GC_V4
        public static FeedBackContent DinhMucSendHandler(Company.GC.BLL.GC.KDT_GC_DinhMucThucTeDangKy dmDangKy, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                            {
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            }
                            else
                            {
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(dmDangKy.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            dmDangKy.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((dmDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận khai báo sửa";
                                dmDangKy.DeleteDinhMucGC();
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }

                            dmDangKy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else
                                dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\n", new object[]{
                                dmDangKy.SoTiepNhan,dmDangKy.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiDinhMuc, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            if (dmDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {

                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (dmDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo định mức";
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(dmDangKy.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", dmDangKy.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.DinhMuc, dmDangKy.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        dmDangKy.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.DinhMuc, dmdk.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", dmDangKy.MaHaiQuan, e);
                    if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        dmDangKy.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        dmDangKy.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    dmDangKy.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, dmDangKy.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent DinhMucSendHandler(DinhMucDangKy dmdk, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (dmdk.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            }
                            else if (dmdk.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                            {
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                            }
                            else
                            {
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            }
                            e.FeedBackMessage.XmlSaveMessage(dmdk.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (dmdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            else if (dmdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                            {
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                            }
                            else if (dmdk.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                            {
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                            }
                            dmdk.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((dmdk.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (dmdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dmdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || dmdk.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (dmdk.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
                            {
                                noidung = "Cấp số tiếp nhận khai báo sửa";
                                dmdk.DeleteDinhMucGC();
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }

                            dmdk.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                dmdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                dmdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else
                                dmdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            //dmdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            dmdk.NamTN = dmdk.NgayTiepNhan.Year.ToString();

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                dmdk.SoTiepNhan,dmdk.NgayTiepNhan.ToString(sfmtVnDateTime),dmdk.NamTN
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dmdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiDinhMuc, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dmdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            if (dmdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (dmdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo định mức";
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(dmdk.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", dmdk.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.DinhMuc, dmdk.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        dmdk.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.DinhMuc, dmdk.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", dmdk.MaHaiQuan, e);
                    InsertUpdateMsgSend(LoaiKhaiBao.DinhMuc, dmdk.ID);
                    if (dmdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        dmdk.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    else if (dmdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    {
                        dmdk.TrangThaiXuLy = TrangThaiXuLy.SUA_KHAI_BAO;
                    }
                    else if (dmdk.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
                    {
                        dmdk.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    dmdk.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, dmdk.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent HopDongSendHandler(HopDong hopdong, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            hopdong.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(hopdong.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            hopdong.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            hopdong.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if (hopdong.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                hopdong.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (hopdong.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || hopdong.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || hopdong.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                hopdong.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }

                            hopdong.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                hopdong.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                hopdong.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else
                                hopdong.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            //hopdong.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            hopdong.NamTN = hopdong.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                hopdong.SoTiepNhan,hopdong.NgayTiepNhan.ToString(sfmtVnDateTime),hopdong.NamTN
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, hopdong.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiHopDong, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (hopdong.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt hợp đồng";
                                hopdong.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, hopdong.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            }
                            else if (hopdong.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo hợp đồng";
                                hopdong.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, hopdong.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQHuyHopDong, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(hopdong.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", hopdong.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.HopDong, hopdong.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        hopdong.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.HopDong, hopdong.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", hopdong.MaHaiQuan, e);
                    hopdong.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    hopdong.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, hopdong.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent PhuKienSendHandler(PhuKienDangKy pkdk, ref string msgInfor, SendEventArgs e)
        {
            FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            pkdk.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(pkdk.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            pkdk.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((pkdk.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }

                            pkdk.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            pkdk.NamDK = (short)(pkdk.NgayTiepNhan.Year);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                pkdk.SoTiepNhan,pkdk.NgayTiepNhan.ToString(sfmtVnDateTime),pkdk.NamDK
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiPhuKien, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            if (pkdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (pkdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(pkdk.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", pkdk.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.PhuKien, pkdk.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        pkdk.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.HopDong, pkdk.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", pkdk.MaHaiQuan, e);
                    InsertUpdateMsgSend(LoaiKhaiBao.PhuKien, pkdk.ID);
                    pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    pkdk.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, pkdk.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent PhuKienToKhaiSendHandler(KDT_SXXK_PhuKienDangKy pkdk, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(pkdk.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            pkdk.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            pkdk.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                pkdk.SoTiepNhan,pkdk.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiPhuKien, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (pkdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(pkdk.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                //pkdk.TransferDataToSXXK();
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(pkdk.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", pkdk.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.PhuKien_TK, pkdk.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        pkdk.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.PhuKien_TK, pkdk.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", pkdk.MaHaiQuan, e);
                    pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    pkdk.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, pkdk.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent NPLCungUngSendHandler(KDT_GC_CungUngDangKy cudk, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (cudk.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                cudk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                cudk.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(cudk.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((cudk.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                cudk.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (cudk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || cudk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || cudk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                cudk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            cudk.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                cudk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                cudk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else cudk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                cudk.SoTiepNhan,cudk.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, cudk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiPhuKien, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (cudk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                
                                cudk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                cudk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(cudk.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);
                                cudk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                //cudk.TransferDataToSXXK();
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(cudk.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", cudk.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.TuCungUng, cudk.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        cudk.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.PhuKien_TK, cudk.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", cudk.MaHQ, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, cudk.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent TaiXuatSendHandler(KDT_GC_TaiXuatDangKy txdk, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (txdk.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                txdk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                txdk.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(txdk.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((txdk.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                txdk.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (txdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || txdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || txdk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                txdk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            txdk.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                txdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                txdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else txdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                txdk.SoTiepNhan,txdk.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, txdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiTaiXuat, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (txdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                
                                txdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                txdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(txdk.ID, MessageTitle.KhaiBaoPhuKienHQDuyet, noidung);
                                txdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                //txdk.TransferDataToSXXK();
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(txdk.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", txdk.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.TaiXuat, txdk.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        txdk.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.PhuKien_TK, txdk.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", txdk.MaHQ, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, txdk.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent DNGiamSatTieuHuySendHandler(GiamSatTieuHuy gsTieuHuy, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(gsTieuHuy.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            gsTieuHuy.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            gsTieuHuy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                gsTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                gsTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else
                                gsTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            //gsTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);


                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                gsTieuHuy.SoTiepNhan,gsTieuHuy.NgayTiepNhan.ToString(sfmtVnDateTime),gsTieuHuy.NgayTiepNhan.Year
                            });
                            e.FeedBackMessage.XmlSaveMessage(gsTieuHuy.ID, MessageTitle.CapSoTNDNTieuHuy, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            e.FeedBackMessage.XmlSaveMessage(gsTieuHuy.ID, MessageTitle.DuyetDNTieuHuy, noidung);
                            if (gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt giám sát tiêu hủy";
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;

                            }
                            else if (gsTieuHuy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo giám sát tiêu hủy";
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            e.FeedBackMessage.XmlSaveMessage(gsTieuHuy.ID, MessageTitle.DuyetDNTieuHuy, noidung);
                            if (feedbackContent.AdditionalInformations[0].Statement == AdditionalInformationStatement.LUONG_DO)
                            {

                            }
                            else if (feedbackContent.AdditionalInformations[0].Statement == AdditionalInformationStatement.LUONG_XANH)
                            {

                            }
                            else if (feedbackContent.AdditionalInformations[0].Statement == AdditionalInformationStatement.LUONG_VANG)
                            {

                            }
                            gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(gsTieuHuy.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", gsTieuHuy.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.GiamSatTieuHuy, gsTieuHuy.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        gsTieuHuy.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, gsTieuHuy.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", gsTieuHuy.MaHaiQuan, e);
                    InsertUpdateMsgSend(LoaiKhaiBao.GiamSatTieuHuy, gsTieuHuy.ID);
                    gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    gsTieuHuy.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, gsTieuHuy.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent ThanhKhoanHDSendHandler(ThanhKhoan thanhKhoan, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(thanhKhoan.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            thanhKhoan.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                thanhKhoan.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                thanhKhoan.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else thanhKhoan.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                thanhKhoan.SoTiepNhan,thanhKhoan.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            e.FeedBackMessage.XmlSaveMessage(thanhKhoan.ID, MessageTitle.KhaiBaoHQCapSoTiepNhan, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            e.FeedBackMessage.XmlSaveMessage(thanhKhoan.ID, MessageTitle.DuyetDNTieuHuy, noidung);
                            if (thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt yêu cầu thanh khoản";
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;

                            }
                            else if (thanhKhoan.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo yêu cầu thanh khoản";
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(thanhKhoan.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", thanhKhoan.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.ThanhKhoanHopDong, thanhKhoan.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        thanhKhoan.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.ThanhKhoanHopDong, thanhKhoan.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", thanhKhoan.MaHaiQuan, e);
                    thanhKhoan.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, thanhKhoan.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        #region Tờ khai chuyển tiếp
        public static FeedBackContent ToKhaiSendHandlerCT(ToKhaiChuyenTiep TKCT, ref string msgInfor, object sender, SendEventArgs e)
        {
            FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung="";
                    if (feedbackContent.AdditionalInformations !=null)
                    {
                        noidung = feedbackContent.AdditionalInformations[0].Content.Text;   
                    }
                    else
                    {
                        noidung = Helpers.ConvertFromBase64(feedbackContent.MessageContent.ToString());
                    }
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;

                    }
                    bool isDeleteMsg = false;
                    switch (feedbackContent.Function)
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                //isDeleteMsg = true;
                                noidung = GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && TKCT.SoToKhai > 0)
                                {
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                                }
                                else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || TKCT.SoToKhai == 0)
                                {
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                    TKCT.NgayTiepNhan = DateTime.Now;
                                }
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {

                                string[] vals = noidung.Split('/');

                                TKCT.SoTiepNhan = long.Parse(vals[0].Trim());
                                TKCT.NamDK = short.Parse(vals[1].Trim());

                                if (feedbackContent.Acceptance.Length == 10)
                                    TKCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    TKCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else TKCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                //TKCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);

                                noidung = string.Format("Số tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nNăm tiếp nhận: {2}\r\nLoại hình: {3}\r\nHải quan: {4}", new object[] {
                                    TKCT.SoTiepNhan, TKCT.NgayTiepNhan.ToString(sfmtVnDateTime), TKCT.NamDK, TKCT.MaLoaiHinh, TKCT.MaHaiQuanTiepNhan });
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, noidung);
                                if (feedbackContent.Function == "29")
                                    if (TKCT.TrangThaiXuLy != TrangThaiXuLy.HUY_KHAI_BAO)
                                    {
                                        noidung = "Tờ khai được cấp số tiếp nhận\r\n" + noidung;
                                        TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                                    }
                                    else
                                    {
                                        noidung = "Tờ khai được cấp số chờ hủy khai báo\r\n" + noidung;
                                        TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                    }
                                break;
                            }
                        case DeclarationFunction.CAP_SO_TO_KHAI:
                            {
                                TKCT.SoToKhai = int.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    TKCT.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    TKCT.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else TKCT.NgayDangKy = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                                TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                noidung = string.Format("Tờ khai được cấp số\r\nSố tờ khai: {0} \r\nNgày tiếp nhận: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nNội dung hq: {4}",
                                    new object[] { feedbackContent.CustomsReference, feedbackContent.Acceptance, feedbackContent.NatureOfTransaction, feedbackContent.DeclarationOffice, noidung });
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.ToKhaiDuocCapSo, noidung);
                                isDeleteMsg = true;

                                //Bo sung: Hungtq, 4/2/2013, Cap nhat thong tin NPL vao ton thuc te
#if GC_V3 || GC_V4
                                if (GlobalSettings.MA_DON_VI != "4000395355")
                                    TKCT.TransferToNPLTon(TKCT.NgayDangKy);
#endif
                            }
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            {
                                if (TKCT.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO || TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                                {
                                    //if (TKCT.SoTiepNhan.ToString() == feedbackContent.CustomsReference)
                                    //{
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                    noidung = "Tờ khai được chấp nhận hủy khai báo\r\n";
                                    e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.HQHuyKhaiBaoToKhai, noidung);
                                    //}
                                }
                                else if ((TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || TKCT.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET) && TKCT.SoToKhai.ToString() == feedbackContent.CustomsReference)
                                {
                                    if (!string.IsNullOrEmpty(noidung))
                                    {
                                        //TKCT.HUONGDAN = noidung;
                                        noidung = "Tờ khai được chấp nhận thông quan nội dung: " + noidung + "\r\n";
                                    }
                                    else noidung = "Tờ khai được chấp nhận thông quan";
                                    e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.ToKhaiSuaDuocDuyet, noidung);
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }
                                else
                                    e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.None, noidung);


                                isDeleteMsg = true;
                            }
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            {
                                TKCT.HUONGDAN = noidung;
                                string idTax = string.Empty;
                                string tenKhobac = string.Empty;
                                foreach (AdditionalInformation item in feedbackContent.AdditionalInformations)
                                {
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                    if (item.Statement == AdditionalInformationStatement.LUONG_XANH)
                                        TKCT.PhanLuong = TrangThaiPhanLuong.LUONG_XANH;
                                    else if (item.Statement == AdditionalInformationStatement.LUONG_VANG)
                                        TKCT.PhanLuong = TrangThaiPhanLuong.LUONG_VANG;
                                    else if (item.Statement == AdditionalInformationStatement.LUONG_DO)
                                        TKCT.PhanLuong = TrangThaiPhanLuong.LUONG_DO;
                                    else if (item.Statement == AdditionalInformationStatement.TAI_KHOAN_KHO_BAC)
                                    {
                                        idTax = item.Content.Text;
                                    }
                                    else if (item.Statement == AdditionalInformationStatement.TEN_KHO_BAC)
                                    {
                                        tenKhobac = item.Content.Text;
                                    }
                                }
                                if (idTax != string.Empty && feedbackContent.AdditionalDocument != null)
                                {
                                    noidung += "\r\nThông báo thuế của hệ thống hải quan:\r\n";
                                }
                                if (idTax != string.Empty && feedbackContent.AdditionalDocument != null)
                                {
                                    noidung += "\r\nThông báo thuế của hệ thống hải quan:\r\n";



                                    AnDinhThue andinhthue = new AnDinhThue();
                                    andinhthue.TaiKhoanKhoBac = idTax;
                                    andinhthue.TKMD_ID = TKCT.ID;
                                    andinhthue.TKMD_Ref = TKCT.GUIDSTR;
                                    andinhthue.NgayQuyetDinh = GetDate(feedbackContent.AdditionalDocument.Issue);
                                    andinhthue.NgayHetHan = GetDate(feedbackContent.AdditionalDocument.Expire);
                                    andinhthue.SoQuyetDinh = feedbackContent.AdditionalDocument.Reference.Trim();
                                    andinhthue.TenKhoBac = tenKhobac;
                                    noidung += "Số quyết định: " + andinhthue.SoQuyetDinh;
                                    noidung += "\r\nTài khoản kho bạc: " + idTax;
                                    noidung += "\r\nTên kho bạc: " + tenKhobac;
                                    noidung += "\r\nNgày quyết định: " + andinhthue.NgayQuyetDinh.ToLongDateString();
                                    noidung += "\r\nNgày hết hạn: " + andinhthue.NgayHetHan.ToLongDateString();

                                    andinhthue.Insert();


                                    foreach (DutyTaxFee item in feedbackContent.AdditionalDocument.DutyTaxFees)
                                    {
                                        ChiTiet chiTietThue = new ChiTiet();
                                        chiTietThue.IDAnDinhThue = andinhthue.ID;
                                        chiTietThue.TienThue = Convert.ToDecimal(item.AdValoremTaxBase);

                                        switch (item.Type.Trim())
                                        {
                                            case DutyTaxFeeType.THUE_XNK:
                                                chiTietThue.SacThue = "XNK";
                                                break;
                                            case DutyTaxFeeType.THUE_VAT:
                                                chiTietThue.SacThue = "VAT";
                                                break;
                                            case DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET:
                                                chiTietThue.SacThue = "TTDB";
                                                break;
                                            case DutyTaxFeeType.THUE_KHAC:
                                                chiTietThue.SacThue = "TVCBPG";
                                                break;
                                            default:
                                                break;
                                        }

                                        foreach (AdditionalInformation additionalInfo in item.AdditionalInformations)
                                        {
                                            if (!string.IsNullOrEmpty(additionalInfo.Content.Text))
                                                switch (additionalInfo.Statement)
                                                {
                                                    case "211"://Chuong
                                                        chiTietThue.Chuong = additionalInfo.Content.Text;
                                                        break;
                                                    case "212"://Loai
                                                        chiTietThue.Loai = Convert.ToInt32(additionalInfo.Content.Text);
                                                        break;
                                                    case "213"://Khoan
                                                        chiTietThue.Khoan = Convert.ToInt32(additionalInfo.Content.Text);
                                                        break;
                                                    case "214"://Muc
                                                        chiTietThue.Muc = Convert.ToInt32(additionalInfo.Content.Text);
                                                        break;
                                                    case "215"://Tieu muc
                                                        chiTietThue.TieuMuc = Convert.ToInt32(additionalInfo.Content.Text);
                                                        break;
                                                }
                                        }
                                        chiTietThue.Insert();
                                    }
                                }
                            }
                            e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.ToKhaiDuocPhanLuong, noidung);
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", TKCT.MaHaiQuanTiepNhan, e);
                                break;
                            }
                    }
                    msgInfor = noidung;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        TKCT.Update();
                    if (isDeleteMsg)
                    {
                        DeleteMsgSend(LoaiKhaiBao.ToKhaiCT, TKCT.ID);
                    }

                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.ToKhai, TKCT.ID);
                    e.Error.Message.XmlSaveMessage(TKCT.ID, MessageTitle.Error);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", TKCT.MaHaiQuanTiepNhan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, TKCT.MaHaiQuanTiepNhan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent GiaHanThanhKhoan(GiaHanThanhKhoan giaHanThanhKhoan, ref string msgInfor, SendEventArgs e)
        {
            FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    bool isDeleteMsg = false;
                    switch (feedbackContent.Function)
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                isDeleteMsg = true;
                                noidung = GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.TuChoiGiaHanTK, noidung);
                                giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {

                                string[] vals = noidung.Split('/');

                                giaHanThanhKhoan.SoTiepNhan = long.Parse(vals[0].Trim());

                                giaHanThanhKhoan.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);

                                noidung = string.Format("Số tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nNăm tiếp nhận: {2}\r\nHải quan: {3}", new object[] {
                                    giaHanThanhKhoan.SoTiepNhan, giaHanThanhKhoan.NgayTiepNhan.ToString(sfmtVnDateTime), vals[1].Trim(),giaHanThanhKhoan.MaHaiQuan });
                                e.FeedBackMessage.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.CapSoTNGiaHanTK, noidung);
                                if (giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo\r\n" + noidung;
                                    giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else
                                {
                                    noidung = "Cấp số tiếp nhận khai báo\r\n" + noidung;
                                    giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                                }
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                if (
                                    (
                                    giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO
                                    || giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY
                                    )
                                    && giaHanThanhKhoan.SoTiepNhan.ToString() == feedbackContent.CustomsReference)
                                {
                                    giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                    noidung = "Chấp nhận hủy khai báo\r\n";
                                    e.FeedBackMessage.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.HuyKhaiBaoGiaHanTK, noidung);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(noidung))
                                    {
                                        noidung = "Hải quan đã duyệt nội dung: " + noidung + "\r\n";
                                    }
                                    else noidung = "Hải quan đã duyệt";
                                    e.FeedBackMessage.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.DuyetGiaHanTK, noidung);
                                    giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                isDeleteMsg = true;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", giaHanThanhKhoan.MaHaiQuan, e);
                                break;
                            }
                    }
                    msgInfor = noidung;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        giaHanThanhKhoan.Update();
                    if (isDeleteMsg)
                    {
                        DeleteMsgSend(LoaiKhaiBao.GiaHanThanhKhoan, giaHanThanhKhoan.ID);
                    }

                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.GiaHanThanhKhoan, giaHanThanhKhoan.ID);
                    e.Error.Message.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.Error);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", giaHanThanhKhoan.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, giaHanThanhKhoan.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        #endregion
#endif
        #endregion Process Message

        #region Thu phí cảng HP
        public static FeedBackContent RegisterInformationSendHandler(T_KDT_THUPHI_DOANHNGHIEP customer, ref string msgInfor, SendEventArgs e)
        {
            FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    string noidung = "";
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (customer.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                customer.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                customer.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(customer.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.LOI_KHI_XU_LY:
                            if (customer.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                customer.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                customer.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(customer.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            if (customer.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                            {
                                customer.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            }
                            customer.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TO_KHAI:
                            if ((customer.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                customer.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            else if (customer.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || customer.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || customer.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                customer.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, customer.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterInformation, noidung);

                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = false;
                            if (customer.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                            {
                                noidung += string.Format("\r\nNgày cấp : {0}", new object[]{
                                customer.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, customer.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterInformation, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(customer.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", customer.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.RegisterInformaton, customer.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        customer.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.RegisterInformaton, customer.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", customer.MaHQ, e);
                    if (customer.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                    {
                        customer.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    }
                    customer.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, customer.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        public static FeedBackContent RegisterTKSendHandler(T_KDT_THUPHI_TOKHAI TK, T_KDT_THUPHI_THONGBAO TB, ref string msgInfor, SendEventArgs e)
        {
            FeedBackContent feedbackContent = null;
            FeedBackContentPort feedbackContentPort = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    string noidung = "";
                    string decompressedData = "";
                    bool compressed = true; 
                    try
                    {
                        decompressedData = Helpers.DecompressString(e.FeedBackMessage);
                    }
                    catch
                    {
                        compressed = false;
                    }
                    if (compressed)
                    {
                        feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(decompressedData);
                        if (TK.TrangThaiXuLy == TrangThaiToKhaiNopPhi.DUOC_CHAP_NHAN)
                        {
                            feedbackContentPort = Company.KDT.SHARE.Components.Helpers.GetFeedBackContentPort(decompressedData);
                        }
                    }
                    else
                    {
                        feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                        if (TK.TrangThaiXuLy == TrangThaiToKhaiNopPhi.DUOC_CHAP_NHAN)
                        {
                            feedbackContentPort = Company.KDT.SHARE.Components.Helpers.GetFeedBackContentPort(e.FeedBackMessage);
                        }
                    }
                    if (TK.TrangThaiXuLy != TrangThaiToKhaiNopPhi.DUOC_CHAP_NHAN)
                    {
                        noidung = feedbackContent.AdditionalInformations[0].Content.Text;   
                    }
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            TK.TrangThaiXuLy = TrangThaiToKhaiNopPhi.CHUA_KHAI_BAO;
                            e.FeedBackMessage.XmlSaveMessage(TK.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            TK.TrangThaiXuLy = TrangThaiToKhaiNopPhi.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                            TK.Update();
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            if (feedbackContent.Issuer == DeclarationIssuer.RegisterHangContainer || feedbackContent.Issuer == DeclarationIssuer.RegisterHangRoi)
                            {

                                TK.TrangThaiXuLy = TrangThaiToKhaiNopPhi.DA_CO_SO_TIEP_NHAN;
                                TK.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    TK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    TK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else TK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                                noidung += "Cấp số tiếp nhận khai báo \r\n";
                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                TK.SoTiepNhan,TK.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                            }
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, TK.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterHangContainer, noidung);
                            break;
                        case DeclarationFunction.CAP_SO_TO_KHAI:
                            isDeleteMsg = false;
                            if (TK.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiToKhaiNopPhi.DA_CO_SO_TIEP_NHAN)
                            {
                                noidung += "Khai báo tờ khai nộp phí đã được chấp nhận \r\n";
                                noidung += string.Format("\r\nNgày cấp : {0}", new object[]{
                                TK.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, TK.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterHangContainer, noidung);
                            }
                            TK.TrangThaiXuLy = TrangThaiToKhaiNopPhi.DUOC_CHAP_NHAN;
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = false;
                            if (TK.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiToKhaiNopPhi.DUOC_CHAP_NHAN)
                            {
                                noidung += "Đã có thông báo nộp phí \r\n";
                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}", new object[]{
                                TK.SoTiepNhan,TK.NgayTiepNhan.ToString(sfmtVnDateTime)
                            });
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, TK.ID, Company.KDT.SHARE.Components.MessageTitle.RegisterHangContainer, noidung);
                            }
                            TK.TrangThaiXuLy = TrangThaiToKhaiNopPhi.DA_CO_THONG_BAO_NOP_PHI;
                            //Xử lý kết quả trả về
                            TB.TK_ID = TK.ID;
                            TB.SoThongBao = feedbackContentPort.CustomsReference.ToString();
                            TB.NgayThongBao = Convert.ToDateTime(feedbackContentPort.Acceptance);
                            TB.DonViThongBao = feedbackContentPort.DeclarationOffice.ToString();
                            TB.TenDonVi = feedbackContentPort.Agent.Name;
                            TB.MaDonVi = feedbackContentPort.Agent.Identity;
                            TB.DiaChi = feedbackContentPort.Agent.Address;
                            TB.MaDonViXNK = feedbackContentPort.Agent.Identity;
                            TB.TenDonViXNK = feedbackContentPort.Agent.Name;
                            TB.DiaChiXNK = feedbackContentPort.Agent.Address;
                            TB.TrangThai = Convert.ToInt32(AgentsStatus.NGUOIKHAI_HAIQUAN);
                            TB.GuidString = feedbackContentPort.Reference;
                            CustomsGoodsItems customsGoodsItems = feedbackContentPort.CustomsGoodsItems;
                            foreach (Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.CustomsGoodsItem item in customsGoodsItems.CustomsGoodsItem)
                            {
                                T_KDT_THUPHI_THONGBAO_PHI phi = new T_KDT_THUPHI_THONGBAO_PHI();
                                phi.MaKhoanMuc = item.TariffCode;
                                phi.TenKhoanMuc = item.TariffName;
                                phi.SoVanDonOrContainer = item.BillOrContainerNo;
                                phi.MaDVT = item.UnitCode;
                                phi.TenDVT = item.UnitName;
                                phi.SoLuongOrTrongLuong = Convert.ToDecimal(item.QuantityOrGrossMass);
                                phi.DonGia = Convert.ToDecimal(item.Price);
                                phi.ThanhTien = Convert.ToDecimal(item.Amount);
                                phi.DienGiai = item.JournalMemo;
                                TB.PhiCollection.Add(phi);
                            }
                            FileAttachs fileAttachs = feedbackContentPort.FileAttachs;
                            foreach (Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.File item in fileAttachs.File)
                            {
                                T_KDT_THUPHI_THONGBAO_FILE file = new T_KDT_THUPHI_THONGBAO_FILE();
                                file.TenFile = item.Name;
                                file.Content = item.Content;
                                TB.FileCollection.Add(file);
                            }
                            TB.InsertUpdateFull();
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(TK.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", TK.MaHQ, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.RegisterHangContainer, TK.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        TK.Update();
                    msgInfor = noidung;
                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.RegisterHangContainer, TK.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", TK.MaHQ, e);
                    TK.TrangThaiXuLy = TrangThaiToKhaiNopPhi.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    TK.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, TK.MaHQ, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

        public static FeedBackContentPort SearchInformationSendHandler(T_KDT_THUPHI_BIENLAI BL, ref string msgInfor, SendEventArgs e)
        {
            FeedBackContentPort feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    string noidung = "";
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContentPort(e.FeedBackMessage);
                    if (feedbackContent.Function.Trim() == DeclarationFunction.DUYET_LUONG_CHUNG_TU)
                    {
                        noidung = feedbackContent.AdditionalInformation.Content.Text;   
                    }
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContentPort(feedbackContent);
                            e.FeedBackMessage.XmlSaveMessage(BL.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            if (feedbackContent.Issuer == DeclarationIssuer.ResultSearchInformation)
                            {
                                // Xử lý kết quả trả về
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, BL.ID, Company.KDT.SHARE.Components.MessageTitle.SearchInformation, noidung);
                                if (feedbackContent.ReceiptOfPayments.ReceiptOfPayment.Count>0)
                                {
                                    foreach (ReceiptOfPayment item in feedbackContent.ReceiptOfPayments.ReceiptOfPayment)
                                    {
                                        BL.MauBienLai = item.Receipt.InvForm;
                                        BL.SoSerial = item.Receipt.InvSeries;
                                        BL.QuyenBienLai = item.Receipt.InvNumber;
                                        BL.SoBienLai = item.Receipt.ReceiptNo;
                                        BL.NgayBienLai = Convert.ToDateTime(item.Receipt.ReceiptDate);
                                        BL.TrangThaiBienLai = item.Receipt.ReceiptStatus;
                                        BL.TongTien = Convert.ToDecimal(item.Receipt.TotalAmount);

                                        BL.MaDoanhNghiepNP = item.Partner.PartnerCode;
                                        BL.TenDoanhNghiepNP = item.Partner.PartnerNameInVN;
                                        BL.DiaChiDoanhNghiepNP = item.Partner.Address;
                                        BL.MST = item.Partner.TaxCode;
                                        BL.TenNguoiNop = item.Partner.ContactName;

                                        BL.SoToKhai = item.CustomsReference.CustomsDeclare;
                                        BL.NgayToKhai = item.CustomsReference.CustomsDeclareDate;
                                        BL.MaLoaiHinh = item.CustomsReference.CustomsDeclareType;
                                        BL.NhomLoaiHinh = item.CustomsReference.TariffTypeCode;
                                        BL.MaPTVC = item.CustomsReference.TransportCode ==String.Empty ? 0 : Convert.ToInt32(item.CustomsReference.TransportCode);
                                        BL.MaDDLK = item.CustomsReference.DestinationCode;

                                        BL.ChoPhepLayHang = Convert.ToInt32(item.AdditionalInformation.Inactive);
                                        BL.DienGiai = item.AdditionalInformation.JournalMemo;

                                        CustomsGoodsItems customsGoodsItems = item.CustomsGoodsItems;
                                        foreach (Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.CustomsGoodsItem items in customsGoodsItems.CustomsGoodsItem)
                                        {
                                            T_KDT_THUPHI_BIENLAI_PHI phi = new T_KDT_THUPHI_BIENLAI_PHI();
                                            phi.MaKhoanMuc = items.TariffCode;
                                            phi.TenKhoanMuc = items.TariffName;
                                            phi.MaDVT = items.UnitCode;
                                            phi.TenDVT = items.UnitName;
                                            phi.SoLuongOrTrongLuong = Convert.ToDecimal(items.QuantityOrGrossMass);
                                            phi.DonGia = Convert.ToDecimal(items.Price);
                                            phi.ThanhTien = Convert.ToDecimal(items.Amount);
                                            phi.DienGiai = items.JournalMemo;
                                            phi.SoVanDon = items.BillOfLading;
                                            phi.SoContainer = items.Container;
                                            phi.SoSeal = items.Seal;
                                            phi.Loai = items.ContainerType ==String.Empty ? 0 : Convert.ToInt32(items.ContainerType);
                                            phi.TinhChat = items.ContainerKind;
                                            BL.PhiCollection.Add(phi);
                                        }
                                        BL.InsertUpdateFull();
                                    }
                                }
                            }
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            isDeleteMsg = false;
                            if (feedbackContent.Issue == DeclarationIssuer.ResultSearchInformation)
                            {
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, BL.ID, Company.KDT.SHARE.Components.MessageTitle.SearchInformation, noidung);
                            }
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            isDeleteMsg = false;
                            if (feedbackContent.Issue == DeclarationIssuer.ResultSearchInformation)
                            {
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, BL.ID, Company.KDT.SHARE.Components.MessageTitle.SearchInformation, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(BL.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", GlobalSettings.MA_HAI_QUAN, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.SearchInformaton, BL.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                       BL.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.RegisterInformaton, BL.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", GlobalSettings.MA_HAI_QUAN, e);
                    BL.Update();
                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, GlobalSettings.MA_HAI_QUAN, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        #endregion
    }
}
