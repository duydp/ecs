﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.Vouchers;

namespace Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho
{
    [XmlRoot("Declaration")]
   public class GC_ImportWareHouse
    {
        /// <summary>
        /// Loại chứng từ  (Nhập khẩu=929, Xuất khẩu=930)
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// Số vận đơn
        /// 
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày vận đơn
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// </summary>
        [XmlElement("function")]
        public string Function { get; set; }

        /// <summary>
        /// Nước phát hành
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }


        /// <summary>
        /// Trạng thái của chứng từ
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }


        /// <summary>
        /// Số tờ khai 
        /// Dùng trong trường hợp khai sửa bản đăng ký
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }

        /// <summary>
        /// Ngày đăng ký YYYY-MM-DD HH:mm:ss
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }

        /// <summary>
        /// Đơn vị HQ khai báo
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }

        /// <summary>
        /// Ngày bắt đầu báo cáo
        /// </summary>
        [XmlElement("startDate")]
        public string StartDate { get; set; }

        /// <summary>
        /// Ngày kết thúc báo cáo
        /// </summary>
        [XmlElement("finishDate")]
        public string FinishDate { get; set; }

        /// <summary>
        /// Đại lý
        /// </summary>
        [XmlElement("Agent")]
        public List<Agent> Agents { get; set; }


        /// <summary>
        /// Doanh nghiệp nhập khẩu
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }

        /// <summary>
        /// Nguyên phụ liệu
        /// </summary>
        [XmlElement("Warehouse")]
        public NameBase Warehouse { get; set; }


        ///// <summary>
        /// Nguyên phụ liệu
        /// </summary>
        [XmlElement("AdditionalDocuments")]
        public PNK_AdditionalDocuments AdditionalDocuments { get; set; }



        /// <summary>
        /// Thông tin tham chiếu đến tờ khai
        /// </summary>
        [XmlElement("DeclarationDocument")]
        public PNK_DeclarationDocument DeclarationDocument { get; set; }

    }
}
