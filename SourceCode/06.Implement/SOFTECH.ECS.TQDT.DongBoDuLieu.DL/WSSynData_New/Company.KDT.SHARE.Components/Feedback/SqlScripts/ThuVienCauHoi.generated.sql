-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Insert]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Update]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Delete]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Load]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_SelectAll]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_SelectBy_LinhVuc]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_SelectBy_LinhVuc]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_DeleteBy_LinhVuc]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_DeleteBy_LinhVuc]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, September 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Insert]
	@MaCauHoi varchar(50),
	@LinhVuc varchar(50),
	@Phanloai varchar(50),
	@TieuDe nvarchar(250),
	@NoiDungCauHoi nvarchar(max),
	@TraLoi nvarchar(max),
	@NgayTao datetime,
	@NgayCapNhat datetime,
	@HienThi bit,
	@GhiChu nvarchar(255)
AS
INSERT INTO [dbo].[t_GopY_ThuVienCauHoi]
(
	[MaCauHoi],
	[LinhVuc],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[TraLoi],
	[NgayTao],
	[NgayCapNhat],
	[HienThi],
	[GhiChu]
)
VALUES
(
	@MaCauHoi,
	@LinhVuc,
	@Phanloai,
	@TieuDe,
	@NoiDungCauHoi,
	@TraLoi,
	@NgayTao,
	@NgayCapNhat,
	@HienThi,
	@GhiChu
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, September 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Update]
	@MaCauHoi varchar(50),
	@LinhVuc varchar(50),
	@Phanloai varchar(50),
	@TieuDe nvarchar(250),
	@NoiDungCauHoi nvarchar(max),
	@TraLoi nvarchar(max),
	@NgayTao datetime,
	@NgayCapNhat datetime,
	@HienThi bit,
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_GopY_ThuVienCauHoi]
SET
	[LinhVuc] = @LinhVuc,
	[Phanloai] = @Phanloai,
	[TieuDe] = @TieuDe,
	[NoiDungCauHoi] = @NoiDungCauHoi,
	[TraLoi] = @TraLoi,
	[NgayTao] = @NgayTao,
	[NgayCapNhat] = @NgayCapNhat,
	[HienThi] = @HienThi,
	[GhiChu] = @GhiChu
WHERE
	[MaCauHoi] = @MaCauHoi

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, September 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_InsertUpdate]
	@MaCauHoi varchar(50),
	@LinhVuc varchar(50),
	@Phanloai varchar(50),
	@TieuDe nvarchar(250),
	@NoiDungCauHoi nvarchar(max),
	@TraLoi nvarchar(max),
	@NgayTao datetime,
	@NgayCapNhat datetime,
	@HienThi bit,
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [MaCauHoi] FROM [dbo].[t_GopY_ThuVienCauHoi] WHERE [MaCauHoi] = @MaCauHoi)
	BEGIN
		UPDATE
			[dbo].[t_GopY_ThuVienCauHoi] 
		SET
			[LinhVuc] = @LinhVuc,
			[Phanloai] = @Phanloai,
			[TieuDe] = @TieuDe,
			[NoiDungCauHoi] = @NoiDungCauHoi,
			[TraLoi] = @TraLoi,
			[NgayTao] = @NgayTao,
			[NgayCapNhat] = @NgayCapNhat,
			[HienThi] = @HienThi,
			[GhiChu] = @GhiChu
		WHERE
			[MaCauHoi] = @MaCauHoi
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GopY_ThuVienCauHoi]
	(
			[MaCauHoi],
			[LinhVuc],
			[Phanloai],
			[TieuDe],
			[NoiDungCauHoi],
			[TraLoi],
			[NgayTao],
			[NgayCapNhat],
			[HienThi],
			[GhiChu]
	)
	VALUES
	(
			@MaCauHoi,
			@LinhVuc,
			@Phanloai,
			@TieuDe,
			@NoiDungCauHoi,
			@TraLoi,
			@NgayTao,
			@NgayCapNhat,
			@HienThi,
			@GhiChu
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, September 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Delete]
	@MaCauHoi varchar(50)
AS

DELETE FROM 
	[dbo].[t_GopY_ThuVienCauHoi]
WHERE
	[MaCauHoi] = @MaCauHoi

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_DeleteBy_LinhVuc]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, September 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_DeleteBy_LinhVuc]
	@LinhVuc varchar(50)
AS

DELETE FROM [dbo].[t_GopY_ThuVienCauHoi]
WHERE
	[LinhVuc] = @LinhVuc

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, September 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GopY_ThuVienCauHoi] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, September 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Load]
	@MaCauHoi varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaCauHoi],
	[LinhVuc],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[TraLoi],
	[NgayTao],
	[NgayCapNhat],
	[HienThi],
	[GhiChu]
FROM
	[dbo].[t_GopY_ThuVienCauHoi]
WHERE
	[MaCauHoi] = @MaCauHoi
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_SelectBy_LinhVuc]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, September 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_SelectBy_LinhVuc]
	@LinhVuc varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaCauHoi],
	[LinhVuc],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[TraLoi],
	[NgayTao],
	[NgayCapNhat],
	[HienThi],
	[GhiChu]
FROM
	[dbo].[t_GopY_ThuVienCauHoi]
WHERE
	[LinhVuc] = @LinhVuc

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, September 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaCauHoi],
	[LinhVuc],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[TraLoi],
	[NgayTao],
	[NgayCapNhat],
	[HienThi],
	[GhiChu]
FROM [dbo].[t_GopY_ThuVienCauHoi] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, September 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaCauHoi],
	[LinhVuc],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[TraLoi],
	[NgayTao],
	[NgayCapNhat],
	[HienThi],
	[GhiChu]
FROM
	[dbo].[t_GopY_ThuVienCauHoi]	

GO

