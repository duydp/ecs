﻿namespace Company.Interface
{
    partial class InToKhaiSuaDoiBoSungForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblNgayDangKy = new System.Windows.Forms.Label();
            this.lblSoToKhai = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblMaLoaiHinh = new System.Windows.Forms.Label();
            this.lblMaToKhai = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboTrang = new Janus.Windows.EditControls.UIComboBox();
            this.lblSoTrang = new System.Windows.Forms.Label();
            this.cmbSoDieuChinh = new Janus.Windows.EditControls.UIComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnThuchien = new Janus.Windows.EditControls.UIButton();
            this.btnHuybo = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnHuybo);
            this.grbMain.Controls.Add(this.btnThuchien);
            this.grbMain.Controls.Add(this.groupBox2);
            this.grbMain.Controls.Add(this.groupBox1);
            this.grbMain.Controls.Add(this.pictureEdit1);
            this.grbMain.Size = new System.Drawing.Size(584, 232);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::Company.Interface.Properties.Resources.MessageBoxIcon;
            this.pictureEdit1.Location = new System.Drawing.Point(12, 12);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(100, 96);
            this.pictureEdit1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.lblNgayDangKy);
            this.groupBox1.Controls.Add(this.lblSoToKhai);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lblMaLoaiHinh);
            this.groupBox1.Controls.Add(this.lblMaToKhai);
            this.groupBox1.Location = new System.Drawing.Point(118, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(454, 82);
            this.groupBox1.TabIndex = 273;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin tờ khai";
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.AutoSize = true;
            this.lblNgayDangKy.BackColor = System.Drawing.Color.Transparent;
            this.lblNgayDangKy.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDangKy.ForeColor = System.Drawing.Color.Black;
            this.lblNgayDangKy.Location = new System.Drawing.Point(317, 44);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Size = new System.Drawing.Size(34, 23);
            this.lblNgayDangKy.TabIndex = 270;
            this.lblNgayDangKy.Text = "00";
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.AutoSize = true;
            this.lblSoToKhai.BackColor = System.Drawing.Color.Transparent;
            this.lblSoToKhai.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhai.ForeColor = System.Drawing.Color.Red;
            this.lblSoToKhai.Location = new System.Drawing.Point(100, 44);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Size = new System.Drawing.Size(34, 23);
            this.lblSoToKhai.TabIndex = 269;
            this.lblSoToKhai.Text = "00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(233, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 268;
            this.label2.Text = "Ngày đăng ký:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(37, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 268;
            this.label1.Text = "Số tờ khai:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(25, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 268;
            this.label3.Text = "Tờ khai (ID):";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(242, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 268;
            this.label10.Text = "Mã loại hình:";
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.AutoSize = true;
            this.lblMaLoaiHinh.BackColor = System.Drawing.Color.Transparent;
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaLoaiHinh.ForeColor = System.Drawing.Color.Black;
            this.lblMaLoaiHinh.Location = new System.Drawing.Point(317, 18);
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.Size = new System.Drawing.Size(36, 23);
            this.lblMaLoaiHinh.TabIndex = 268;
            this.lblMaLoaiHinh.Text = "XK";
            // 
            // lblMaToKhai
            // 
            this.lblMaToKhai.AutoSize = true;
            this.lblMaToKhai.BackColor = System.Drawing.Color.Transparent;
            this.lblMaToKhai.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaToKhai.ForeColor = System.Drawing.Color.Black;
            this.lblMaToKhai.Location = new System.Drawing.Point(100, 18);
            this.lblMaToKhai.Name = "lblMaToKhai";
            this.lblMaToKhai.Size = new System.Drawing.Size(34, 23);
            this.lblMaToKhai.TabIndex = 268;
            this.lblMaToKhai.Text = "00";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.cboTrang);
            this.groupBox2.Controls.Add(this.lblSoTrang);
            this.groupBox2.Controls.Add(this.cmbSoDieuChinh);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(118, 100);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(454, 94);
            this.groupBox2.TabIndex = 274;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin Tờ khai Sửa đổi, bổ sung";
            // 
            // cboTrang
            // 
            this.cboTrang.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboTrang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTrang.Location = new System.Drawing.Point(272, 21);
            this.cboTrang.Name = "cboTrang";
            this.cboTrang.Size = new System.Drawing.Size(96, 21);
            this.cboTrang.TabIndex = 273;
            this.cboTrang.ValueMember = "ID";
            this.cboTrang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cboTrang.VisualStyleManager = this.vsmMain;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.AutoSize = true;
            this.lblSoTrang.BackColor = System.Drawing.Color.Transparent;
            this.lblSoTrang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTrang.Location = new System.Drawing.Point(217, 26);
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Size = new System.Drawing.Size(58, 13);
            this.lblSoTrang.TabIndex = 272;
            this.lblSoTrang.Text = "Số trang:";
            // 
            // cmbSoDieuChinh
            // 
            this.cmbSoDieuChinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbSoDieuChinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSoDieuChinh.Location = new System.Drawing.Point(115, 21);
            this.cmbSoDieuChinh.Name = "cmbSoDieuChinh";
            this.cmbSoDieuChinh.Size = new System.Drawing.Size(96, 21);
            this.cmbSoDieuChinh.TabIndex = 271;
            this.cmbSoDieuChinh.ValueMember = "ID";
            this.cmbSoDieuChinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cmbSoDieuChinh.VisualStyleManager = this.vsmMain;
            this.cmbSoDieuChinh.SelectedIndexChanged += new System.EventHandler(this.cmbSoDieuChinh_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(25, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(403, 30);
            this.label4.TabIndex = 270;
            this.label4.Text = "Hướng dẫn: Chọn số điều chỉnh của tờ khai để in tờ khai sửa đổi, bổ sung. Sau đó " +
                "nhấn nút \'Thực hiện\' bên dưới để in ấn.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(25, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 268;
            this.label7.Text = "Số điều chỉnh:";
            // 
            // btnThuchien
            // 
            this.btnThuchien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThuchien.Location = new System.Drawing.Point(430, 200);
            this.btnThuchien.Name = "btnThuchien";
            this.btnThuchien.Size = new System.Drawing.Size(67, 24);
            this.btnThuchien.TabIndex = 275;
            this.btnThuchien.Text = "Thực hiện";
            this.btnThuchien.VisualStyleManager = this.vsmMain;
            this.btnThuchien.Click += new System.EventHandler(this.btnThuchien_Click);
            // 
            // btnHuybo
            // 
            this.btnHuybo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnHuybo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuybo.Location = new System.Drawing.Point(505, 200);
            this.btnHuybo.Name = "btnHuybo";
            this.btnHuybo.Size = new System.Drawing.Size(67, 24);
            this.btnHuybo.TabIndex = 276;
            this.btnHuybo.Text = "Hủy bỏ";
            this.btnHuybo.VisualStyleManager = this.vsmMain;
            // 
            // InToKhaiSuaDoiBoSungForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 232);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InToKhaiSuaDoiBoSungForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "In tờ khai sửa đổi bổ sung Form";
            this.Load += new System.EventHandler(this.InToKhaiSuaDoiBoSungForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblSoToKhai;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblMaLoaiHinh;
        private System.Windows.Forms.Label lblMaToKhai;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblNgayDangKy;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnThuchien;
        private Janus.Windows.EditControls.UIButton btnHuybo;
        private Janus.Windows.EditControls.UIComboBox cmbSoDieuChinh;
        private Janus.Windows.EditControls.UIComboBox cboTrang;
        private System.Windows.Forms.Label lblSoTrang;

    }
}