﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KeySecurity;

namespace LicenseCode
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }


        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUser.Text))
            {
                errorProvider1.SetError(txtUser, "Không được để trống Username");
                return;
            }
            if (string.IsNullOrEmpty(txtPass.Text))
            {
                errorProvider1.SetError(txtPass, "Không được để trống Password");
                return;
            }
            ProductKey.ProductKey productServices = new ProductKey.ProductKey();
            KeyServer.User.User userLogin = new KeyServer.User.User();
            string Pass = txtPass.Text.Trim();
            try
            {
                Pass = KeyCode.HashMD5(KeyCode.Encrypt(Pass));
                Pass = KeyCode.ConvertToBase64(Pass);
                string result = productServices.Login(txtUser.Text, Pass);
                if (!string.IsNullOrEmpty(result))
                {
                    try
                    {
                        userLogin = KeyCode.Deserialize<KeyServer.User.User>(KeyCode.ConvertFromBase64(result));
                    }
                    catch
                    {
                        MessageBoxControl.ShowMessage("Lỗi services: " + result);
                    }
                    frmMain.isLoginSuccess = true;
                    frmMain.UserName = txtUser.Text;
                    frmMain.Pass = Pass;
                    frmMain.User = userLogin;
                    this.Close();
                }
                else
                {
                    MessageBoxControl.ShowMessage("Đăng nhập thất bại, sai Username hoặc Password ");
                }
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi đăng nhập: " + ex.Message);
            }
            
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnLogin_Click(null,null);
            }
        }

        private void btnLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnLogin_Click(null, null);
            }
        }
    }
}
