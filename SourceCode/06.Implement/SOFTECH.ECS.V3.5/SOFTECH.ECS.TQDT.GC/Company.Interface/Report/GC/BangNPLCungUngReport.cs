using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;

namespace Company.Interface.Report.GC
{
    public partial class BangNPLCungUngReport : DevExpress.XtraReports.UI.XtraReport
    {
        private int STT = 0;
        public ToKhaiMauDich TKMD;
        public HopDong HD;
        public BangNPLCungUngReport()
        {
            InitializeComponent();
        }
        private decimal GetSoLuong(string maSP)
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (hmd.MaPhu == maSP) return hmd.SoLuong;
                
            }
            return 0;
        }
        private string GetTenHang(string maSP)
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (hmd.MaPhu == maSP) return hmd.TenHang;

            }
            return "";
        }
        public void BindReport(string maSP)
        {
            //Lấy danh sách dữ liệu cung ứng theo từng sản phẩm của tờ khai đưa vào bảng dtTemp
            DataTable dtTemp = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCuaSanPham(this.TKMD.ID, this.HD.ID, maSP).Tables[0];
            //Xữ lý bảng dttemp để bind dữ liệu vào report theo đúng mẫu
            DataTable dt = XuLyDuLieu(dtTemp);

            //Bind dữ liệu vào report
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = dt;

            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;
            //minhnd fix for vnaccs
            if (this.TKMD.MaLoaiHinh.Contains("V"))
            {
                if (this.TKMD.SoToKhai > 0)
                    lblSoToKhai.Text = this.TKMD.LoaiVanDon + " / " + LoaiHinhMauDich.GetTenVietTac(this.TKMD.MaLoaiHinh);
                else
                    lblSoToKhai.Text = "      / " + LoaiHinhMauDich.GetTenVietTac(this.TKMD.MaLoaiHinh);
            }
            else 
            {
                if (this.TKMD.SoToKhai > 0)
                    lblSoToKhai.Text = this.TKMD.SoToKhai + " / " + LoaiHinhMauDich.GetTenVietTac(this.TKMD.MaLoaiHinh);
                else
                    lblSoToKhai.Text = "      / " + LoaiHinhMauDich.GetTenVietTac(this.TKMD.MaLoaiHinh);
            }
            
            if (this.TKMD.NgayDangKy.Year > 1900)
                lblNgayToKhai.Text = this.TKMD.NgayDangKy.ToShortDateString();
            else if (this.TKMD.NgayTiepNhan.Year > 1900)
                lblNgayToKhai.Text = this.TKMD.NgayTiepNhan.ToShortDateString();
            else
                lblNgayToKhai.Text = "";
            lblSoLuong.Text = GetSoLuong(maSP).ToString("N"+ GlobalSettings.SoThapPhan.LuongSP);
            lblMathang.Text = GetTenHang(maSP) + " / " + maSP;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lblThoiHan.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;
            if (this.TKMD.NgayTiepNhan.Year > 1900)
                xrLabel2.Text = "Ngày " + TKMD.NgayTiepNhan.Day + " tháng " + this.TKMD.NgayTiepNhan.Month + " năm " + this.TKMD.NgayTiepNhan.Year;
            else
                xrLabel2.Text = "Ngày....tháng....năm.......";
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblDonGia.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DonGiaKB", "{0:N5}");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblHTCU.DataBindings.Add("Text", this.DataSource, dt.TableName + ".HinhThucCU");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            lblTriGia.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TriGiaKB", "{0:N3}");
            lblLuongCU.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongCU", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".GhiChu");
            //xrLabel2.Text = GlobalSettings.TieudeNgay;
        }

        private DataTable XuLyDuLieu(DataTable dtTemp)
        {
            //Tạo cấu trúc bảng giống report

            DataTable dt = new DataTable();
            DataColumn[] dcCol = new DataColumn[8];
            dcCol[0] = new DataColumn("TenNPL", Type.GetType("System.String"));
            dcCol[5] = new DataColumn("MaNPL", Type.GetType("System.String"));
            dcCol[1] = new DataColumn("DVT", Type.GetType("System.String"));
            dcCol[2] = new DataColumn("LuongCU", Type.GetType("System.Decimal"));
            dcCol[3] = new DataColumn("HinhThucCU", Type.GetType("System.String"));
            dcCol[4] = new DataColumn("GhiChu", Type.GetType("System.String"));
            dcCol[6] = new DataColumn("DonGiaKB", Type.GetType("System.Decimal"));
            dcCol[7] = new DataColumn("TriGiaKB", Type.GetType("System.Decimal"));

            dt.Columns.AddRange(dcCol);
            string maNPL = "";
            foreach (DataRow drTemp in dtTemp.Rows)
            {
                //Xử lý từng dòng dữ liệu trong dtTemp và insert vào bảng dt
                if (maNPL != drTemp["MaNPL"].ToString())
                {
                    DataRow dr = dt.NewRow();
                    dr["TenNPL"] = drTemp["TenNPL"].ToString();
                    dr["MaNPL"] =  drTemp["MaNPL"].ToString();
                    dr["DVT"] = DonViTinh.GetName(drTemp["DVT_NPL"]);
                    dr["LuongCU"] = 0;
                    if (drTemp["MaLoaiHinhNhap"].ToString().Contains("NSX"))
                    {
                        dr["LuongCU"] = Convert.ToDecimal(drTemp["LuongPhanBo"]);
                        dr["HinhThucCU"] = LoaiHinhMauDich.GetTenVietTac(drTemp["MaLoaiHinhNhap"].ToString());
                        dr["GhiChu"] = "TK" + drTemp["SoToKhaiNhap"].ToString() + "/" + drTemp["NamDangKyNhap"] + ":" + Convert.ToDecimal(drTemp["LuongPhanBo"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL) + "; ";
                    }

                    if (Convert.ToDecimal(drTemp["LuongCungUng"]) > 0)
                    {
                        dr["LuongCU"] = Convert.ToDecimal(dr["LuongCU"]) + Convert.ToDecimal(drTemp["LuongCungUng"]);
                        dr["HinhThucCU"] = "Mua tại VN";
                        dr["GhiChu"] = "Mua VN: " + Convert.ToDecimal(drTemp["LuongCungUng"]).ToString("N"+GlobalSettings.SoThapPhan.LuongNPL) + "; ";
                        
                    }
                    ToKhaiMauDich TKMDNhap = new ToKhaiMauDich();
                    TKMDNhap.SoToKhai = Convert.ToInt32(drTemp["SoToKhaiNhap"]);
                    TKMDNhap.MaLoaiHinh = Convert.ToString(drTemp["MaLoaiHinhNhap"]);
                    TKMDNhap.NgayDangKy = Convert.ToDateTime(drTemp["NgayDangKyNhap"]);
                    dr["DonGiaKB"] = TKMDNhap.SelectDonGia(drTemp["MaNPL"].ToString());
                    dr["TriGiaKB"] = Convert.ToDecimal(dr["DonGiaKB"]) * Convert.ToDecimal(dr["LuongCU"]);
                    dt.Rows.Add(dr);
                    maNPL = drTemp["MaNPL"].ToString();
                }
                else
                {

                    if (drTemp["MaLoaiHinhNhap"].ToString().Contains("NSX"))
                    {
                        dt.Rows[dt.Rows.Count - 1]["LuongCU"] = Convert.ToDecimal(dt.Rows[dt.Rows.Count - 1]["LuongCU"]) + Convert.ToDecimal(drTemp["LuongPhanBo"]);

                        if (!dt.Rows[dt.Rows.Count - 1]["HinhThucCU"].ToString().Contains(LoaiHinhMauDich.GetTenVietTac(drTemp["MaLoaiHinhNhap"].ToString())))
                            dt.Rows[dt.Rows.Count - 1]["HinhThucCU"] = dt.Rows[dt.Rows.Count - 1]["HinhThucCU"].ToString() + "; " + LoaiHinhMauDich.GetTenVietTac(drTemp["MaLoaiHinhNhap"].ToString());
                        dt.Rows[dt.Rows.Count - 1]["GhiChu"] = dt.Rows[dt.Rows.Count - 1]["GhiChu"].ToString() + " ; TK" + drTemp["SoToKhaiNhap"].ToString() + "/" + drTemp["NamDangKyNhap"] + ":" + Convert.ToDecimal(drTemp["LuongPhanBo"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                    }
                    if (Convert.ToDecimal(drTemp["LuongCungUng"]) > 0)
                    {
                        dt.Rows[dt.Rows.Count - 1]["LuongCU"] = Convert.ToDecimal(dt.Rows[dt.Rows.Count - 1]["LuongCU"]) + Convert.ToDecimal(drTemp["LuongCungUng"]);
                        dt.Rows[dt.Rows.Count - 1]["HinhThucCU"] = dt.Rows[dt.Rows.Count - 1]["HinhThucCU"].ToString()+ "; " + "Mua tại VN";
                        dt.Rows[dt.Rows.Count - 1]["GhiChu"] = dt.Rows[dt.Rows.Count - 1]["GhiChu"].ToString() + "; Mua VN: " + Convert.ToDecimal(drTemp["LuongCungUng"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);

                    }
                    dt.Rows[dt.Rows.Count - 1]["TriGiaKB"] = Convert.ToDecimal(dt.Rows[dt.Rows.Count - 1]["DonGiaKB"]) * Convert.ToDecimal(dt.Rows[dt.Rows.Count - 1]["LuongCU"]);
                }

            }
            return dt;
        }
      

        private void PhanBoReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT =0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblTenNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString(); ;
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblDVT.Text = DonViTinh.GetName(GetCurrentColumnValue("DVT_ID"));
        }

        private void lblLuongCU_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (GetCurrentColumnValue("SoToKhaiNhap").ToString() == "0") 
            //    lblLuongCU.Text = Convert.ToDecimal(GetCurrentColumnValue("LuongCungUng")).ToString("N"+GlobalSettings.SoThapPhan.LuongNPL);
            //else
            //    lblLuongCU.Text = Convert.ToDecimal(GetCurrentColumnValue("LuongPhanBo")).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
        }

        private void lblHTCU_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (GetCurrentColumnValue("MaLoaiHinhNhap").ToString() == "")
            //    lblHTCU.Text = "Mua tại VN";
            //else
            //    lblHTCU.Text = LoaiHinhMauDich.SelectTenVTByMa(GetCurrentColumnValue("MaLoaiHinhNhap").ToString());
        }





    }
}
