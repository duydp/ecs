﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components;
using Company.Interface.KDT.DNCX;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class TaiSanCoDinhEditFrm : BaseForm
    {
        public KDT_SXXK_TLTSCDDangKy TLTSCD;
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public TaiSanCoDinhEditFrm()
        {
            InitializeComponent();
            dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(dgList_RowDoubleClick);
        }
        private void SetCommandStatus()
        {
            if (TLTSCD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdLuu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = InheritableBoolean.False;
                lblTrangThai.Text = "Chờ duyệt";

            }
            else if (TLTSCD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TLTSCD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || TLTSCD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdLuu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = InheritableBoolean.True;
                cmdKhaiSua.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (TLTSCD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    lblTrangThai.Text = "Chưa khai báo";
                if (TLTSCD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    lblTrangThai.Text = "Không phê duyệt";
                if (TLTSCD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                    lblTrangThai.Text = "Đang sửa";

            }
            else if (TLTSCD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdLuu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = InheritableBoolean.False;
                cmdKhaiSua.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lblTrangThai.Text = "Đã duyệt";
            }

        }
        void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            KDT_SXXK_TLTSCD hanghoa = (KDT_SXXK_TLTSCD)e.Row.DataRow;
            ThanhLyTSCD_ThemHangFrm f = new ThanhLyTSCD_ThemHangFrm();
            f.HangHoa = hanghoa;
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                dgList.DataSource = this.TLTSCD.HangThanhLyCollection;
                dgList.Refetch();
            }
        }
        private void TaiSanCoDinhEditFrm_Load(object sender, System.EventArgs e)
        {
            if (TLTSCD != null)
            {
                clcNgayThongBaoTL.Value = TLTSCD.NgayThongBaoTL;
                txtSoTiepNhan.Text = TLTSCD.SoTiepNhan.ToString();

                ctrMaHaiQuan.Ma = TLTSCD.MaHaiQuan;
                dgList.DataSource = TLTSCD.HangThanhLyCollection;
            }
            else
            {
                TLTSCD = new KDT_SXXK_TLTSCDDangKy();
                TLTSCD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                ctrMaHaiQuan.Ma = GlobalSettings.MA_HAI_QUAN;
            }
            SetCommandStatus();

        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    this.AddNew();
                    break;
                case "cmdLuu":
                    this.save();
                    break;
                case "cmdKetQua":
                    KetQuaXuLyTK();
                    break;
                case "cmdKhaiBao":
                    SendV3(TLTSCD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET);
                    break;
                case "cmdNhanDuLieu":
                    FeedBackV3();
                    break;
                case "cmdKhaiSua":
                    TLTSCD.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    SetCommandStatus();
                    break;
            }
        }
        private void save()
        {
            try
            {
                if (TLTSCD == null)
                {
                    TLTSCD = new KDT_SXXK_TLTSCDDangKy();
                }
                if (TLTSCD.ID == 0)
                {
                    TLTSCD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    TLTSCD.NgayTiepNhan = new DateTime(1900, 1, 1);
                }
                TLTSCD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                TLTSCD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                TLTSCD.MaDaiLy = GlobalSettings.MA_DAI_LY;
                TLTSCD.NgayThongBaoTL = clcNgayThongBaoTL.Value;

                if (TLTSCD.HangThanhLyCollection == null || TLTSCD.HangThanhLyCollection.Count == 0)
                {
                    ShowMessage("Chưa có thông tin hàng.", false);
                }
                else
                {
                    if (TLTSCD.InsertUpdateFull())
                        ShowMessage("Lưu thông tin thành công", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
        private void AddNew()
        {
            try
            {

                if (this.TLTSCD.HangThanhLyCollection == null)
                    this.TLTSCD.HangThanhLyCollection = new List<KDT_SXXK_TLTSCD>();
                ThanhLyTSCD_ThemHangFrm f = new ThanhLyTSCD_ThemHangFrm();
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    this.TLTSCD.HangThanhLyCollection.Add(f.HangHoa);
                    dgList.DataSource = TLTSCD.HangThanhLyCollection;
                    dgList.Refetch();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        #region khai bao

        private void SendV3(bool isSua)
        {
          
            if (TLTSCD.ID == 0)
            {
                this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                return;
            }
            try
            {
                if (TLTSCD.HangThanhLyCollection == null || TLTSCD.HangThanhLyCollection.Count == 0)
                {
                    // ShowMessage("Danh sách hàng cần thanh lý rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    MLMessages("Danh sách hàng cần thanh lý rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }
                MsgSend sendXML1 = new MsgSend();
                sendXML1.LoaiHS = LoaiKhaiBao.ThanhLyTaiSanCoDinh;
                sendXML1.master_id = TLTSCD.ID;
                if (sendXML1.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Xin kiểm tra lại", false);
                    // XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    return;
                }

                TLTSCD.GuidStr = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.CX_ThanhlyTaiSanCoDinh npl = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferObject_TLTSCD(TLTSCD, isSua, GlobalSettings.TEN_DON_VI);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = TLTSCD.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(TLTSCD.MaHaiQuan),
                                     Identity = TLTSCD.MaHaiQuan.Trim()
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.THANHLY_TSCD,
                                    Function = isSua ? Company.KDT.SHARE.Components.DeclarationFunction.SUA : Company.KDT.SHARE.Components.DeclarationFunction.KHAI_BAO,
                                    Reference = TLTSCD.GuidStr,
                                }
                                ,
                                npl);
                TLTSCD.TrangThaiXuLy = isSua ? Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET :  Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(TLTSCD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoTSCD);
                    //  XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    //  cmdAdd.Enabled = cmdSave.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    btnDelete.Enabled = false;
                    cmdKhaiSua.Enabled = cmdKhaiBao1.Enabled = cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    MsgSend sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.ThanhLyTaiSanCoDinh;
                    sendXML.master_id = TLTSCD.ID;
                    sendXML.func = 1;
                    // sendXML.msg = msgSend;
                    sendXML.InsertUpdate();
                    //   TLTSCD.TransgferDataToSXXK();
                    //SetCommandStatus();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);

            }


            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.ThanhLyTaiSanCoDinh(TLTSCD, ref msgInfor, e);
        }
        private void FeedBackV3()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
           
            while (isFeedBack)
            {
                string reference = TLTSCD.GuidStr;

                Company.KDT.SHARE.Components.SubjectBase subjectBase = new Company.KDT.SHARE.Components.SubjectBase()
                {
                    Issuer = Company.KDT.SHARE.Components.DeclarationIssuer.THANHLY_TSCD,
                    Reference = reference,
                    Function = Company.KDT.SHARE.Components.DeclarationFunction.HOI_TRANG_THAI,
                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.THANHLY_TSCD,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new Company.KDT.SHARE.Components.NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = TLTSCD.MaDoanhNghiep
                                            },
                                              new Company.KDT.SHARE.Components.NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(TLTSCD.MaHaiQuan.Trim()),
                                                  Identity = TLTSCD.MaHaiQuan.Trim()
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    //if (TLTSCD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    //{
                    //    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    //    {
                    //        isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                    //        ShowMessageTQDT(msgInfor, false);
                    //    }
                    //    count--;
                    //}
                    //else if (TLTSCD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || TLTSCD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    //{
                    //    ShowMessageTQDT(msgInfor, false);
                    //    isFeedBack = false;
                    //}
                    //else if (!string.IsNullOrEmpty(msgInfor))
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    //else isFeedBack = false;

                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) SetCommandStatus();
                }

            }
        }

        #endregion

        private void KetQuaXuLyTK()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.TLTSCD.ID;
            form.DeclarationIssuer = DeclarationIssuer.THANHLY_TSCD;
            form.ShowDialog(this);
        }
    }
}
