using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanXacNhanNoiDungKhaiSuaDoiBoSung_1 : DevExpress.XtraReports.UI.XtraReport
    {
        DataTable data = new DataTable();
        public BanXacNhanNoiDungKhaiSuaDoiBoSung_1()
        {
            InitializeComponent();
        }
        public void BindReport(VAL8000 VAL,string maNV)
        {

            lblName.Text = EnumThongBao.GetTenNV(maNV);
            try
            {
                lblSoPL.Text = "1 / " + VAL.A24.Value.ToString();
                //            lblSoThongBao.Text =VAL.ADD.Value.ToString();
                //lblNgayHoanThanhKT.Text =VAL.AD2.Value.ToString();
                //lblGioHoanThanhKT.Text =VAL.AD3.Value.ToString();
                lblSoToKhaiBoSung.Text = VAL.A03.Value.ToString();
                //lblNgayDKKhaiBoSung.Text = VAL.A02.Value.ToString();
                //lblGioDKKhaiBoSung.Text = VAL.AD1.Value.ToString();
                if (Convert.ToDateTime(VAL.A02.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                    lblNgayDKKhaiBoSung.Text = Convert.ToDateTime(VAL.A02.GetValue()).ToString("dd/MM/yyyy");
                else
                    lblNgayDKKhaiBoSung.Text = "";

                string time = VAL.AD1.Value.ToString();
                lblGioDKKhaiBoSung.Text = time.Substring(0, 2) + ":" + time.Substring(2, 2) + ":" + time.Substring(4, 2);
                lblCoQuanNhan.Text = VAL.A00.Value.ToString();
                lblNhomXuLyHoSo.Text = VAL.A01.Value.ToString();
                lblPhanLoaiXNK.Text = VAL.ADG.Value.ToString();
                lblSoToKhaiXNK.Text = VAL.A27.Value.ToString();
                lblMaLoaiHinh.Text = VAL.ADQ.Value.ToString();
                //lblNgayToKhaiXNK.Text = VAL.A29.Value.ToString();
                if (Convert.ToDateTime(VAL.A29.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                    lblNgayToKhaiXNK.Text = Convert.ToDateTime(VAL.A29.GetValue()).ToString("dd/MM/yyyy");
                else
                    lblNgayToKhaiXNK.Text = "";
                lblDauHieuBaoQua.Text = VAL.ADE.Value.ToString();
                //lblMaHetThoiHan.Text =VAL.CTO.Value.ToString();
                //lblNgayCapPhep.Text = VAL.A30.Value.ToString();
                //lblThoiHanTaiNhapXuat.Text = VAL.ADR.Value.ToString();
                if (Convert.ToDateTime(VAL.A30.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                    lblNgayCapPhep.Text = Convert.ToDateTime(VAL.A30.GetValue()).ToString("dd/MM/yyyy");
                else
                    lblNgayCapPhep.Text = "";
                if (Convert.ToDateTime(VAL.ADR.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                    lblThoiHanTaiNhapXuat.Text = Convert.ToDateTime(VAL.ADR.GetValue()).ToString("dd/MM/yyyy");
                else
                    lblThoiHanTaiNhapXuat.Text = "";
                lblMaNguoiKhai.Text = VAL.A04.Value.ToString();
                lblTenNguoiKhai.Text = VAL.A05.Value.ToString();
                lblMaBuuChinh.Text = VAL.A06.Value.ToString();
                lblDiaChi.Text = VAL.A07.Value.ToString();
                lblSoDT.Text = VAL.A11.Value.ToString();
                lblMaDaiLyHQ.Text = VAL.A12.Value.ToString();
                lblTenDaiLyHQ.Text = VAL.A13.Value.ToString();
                lblMaNhanVienHQ.Text = VAL.A14.Value.ToString();
                lblMaLyDoKhaiBS.Text = VAL.A15.Value.ToString();
                lblPhanLoaiNopThue.Text = VAL.A16.Value.ToString();
                lblMaXacDinhThoiHanNop.Text = VAL.ADL.Value.ToString();
                //lblNgayHetHieuLuc.Text = VAL.ADS.Value.ToString();
                if (Convert.ToDateTime(VAL.ADS.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                    lblNgayHetHieuLuc.Text = Convert.ToDateTime(VAL.ADS.GetValue()).ToString("dd/MM/yyyy");
                else
                    lblNgayHetHieuLuc.Text = "";
                lblSoNgayAnHan.Text = VAL.ADT.Value.ToString();
                lblSoNgayAnHanVAT.Text = VAL.ADU.Value.ToString();
                lblSoQuanLyNoiBo.Text = VAL.A20.Value.ToString();
                lblMaTienTeTruocKhaiBao.Text = VAL.ADH.Value.ToString();

                lblGhiChuSauKhaiBao.Text = VAL.A22.Value.ToString();
                lblGhiChuTruocKhaiBao.Text = VAL.ADJ.Value.ToString();
                lblTongSoTrang.Text = VAL.A24.Value.ToString();
                lblTongSoDongHang.Text = VAL.A25.Value.ToString();
                lblMaTienTeThueXNK.Text = VAL.ADN.Value.ToString();
                lblMaTienTeSauKhaiBao.Text = VAL.ADK.Value.ToString();
                lblBieuThiSoTienTangGiamThueXNK.Text = VAL.ADB.Value.ToString();
                try
                {
                    lblTyGiaHoiDoaiTruocKhaiBao.Text = string.Empty;
                    lblTyGiaHoiDoaiTruocKhaiBao.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(VAL.ADI.Value.ToString().Replace(".", ",")));
                    
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //throw;
                }
                try
                {
                    lblTyGiaHoiDoaiSauKhaiBao.Text = string.Empty;
                    lblTyGiaHoiDoaiSauKhaiBao.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(VAL.A21.Value.ToString().Replace(".", ",")));

                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //throw;
                }

                lblTongSoTienTangGiamThueXNK.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(VAL.A23.Value.ToString().Replace(".", ",")));
                //lblMaKhoanMucTiepNhan.Text = VAL.KA1.Value.ToString();

                //lblTenKhoanMucTiepNhan.Text =VAL.KB1.Value.ToString();
                //lblBieuThiSoTienTangGiamThueVaThuKhac.Text =VAL.KU1.Value.ToString();
                //lblTongSoTienTangGiamThueVaThuKhac.Text = VAL.KC1.Value.ToString();
                //lblMaTienTeThueVaThuKhac.Text =VAL.KW1.Value.ToString();
                
                //lblLyDo.Text =VAL.ADO.Value.ToString();
                //lblTenNguoiPhuTrach.Text =VAL.ADF.Value.ToString();
                //lblTenTruongDv.Text =VAL.ADP.Value.ToString();
                data = ReportViewVNACCS.ConvertListToTable(VAL.KA1, 5);
                BindReportSatThue(data);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            

        }
        private void BindReportSatThue(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblMaKhoanMucTiepNhan.DataBindings.Add("Text", DetailReport.DataSource, "VAL8000_KA1");
            lblTenKhoanMucTiepNhan.DataBindings.Add("Text", DetailReport.DataSource, "VAL8000_KB1");
            lblBieuThiSoTienTangGiamThueVaThuKhac.DataBindings.Add("Text", DetailReport.DataSource, "VAL8000_KU1");
            lblTongSoTienTangGiamThueVaThuKhac.DataBindings.Add("Text", DetailReport.DataSource, "VAL8000_KC1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaTienTeThueVaThuKhac.DataBindings.Add("Text", DetailReport.DataSource, "VAL8000_KW1");
        }

    }
}
