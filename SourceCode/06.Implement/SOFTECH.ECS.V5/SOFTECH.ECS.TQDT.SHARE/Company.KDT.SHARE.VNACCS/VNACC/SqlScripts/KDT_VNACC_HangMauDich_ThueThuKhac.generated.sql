-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]
	@Master_id bigint,
	@MaTSThueThuKhac varchar(10),
	@MaMGThueThuKhac varchar(5),
	@SoTienGiamThueThuKhac numeric(16, 4),
	@TenKhoanMucThueVaThuKhac nvarchar(9),
	@TriGiaTinhThueVaThuKhac numeric(17, 4),
	@SoLuongTinhThueVaThuKhac numeric(14, 0),
	@MaDVTDanhThueVaThuKhac varchar(4),
	@ThueSuatThueVaThuKhac varchar(24),
	@SoTienThueVaThuKhac numeric(16, 4),
	@DieuKhoanMienGiamThueVaThuKhac varchar(60),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
(
	[Master_id],
	[MaTSThueThuKhac],
	[MaMGThueThuKhac],
	[SoTienGiamThueThuKhac],
	[TenKhoanMucThueVaThuKhac],
	[TriGiaTinhThueVaThuKhac],
	[SoLuongTinhThueVaThuKhac],
	[MaDVTDanhThueVaThuKhac],
	[ThueSuatThueVaThuKhac],
	[SoTienThueVaThuKhac],
	[DieuKhoanMienGiamThueVaThuKhac]
)
VALUES 
(
	@Master_id,
	@MaTSThueThuKhac,
	@MaMGThueThuKhac,
	@SoTienGiamThueThuKhac,
	@TenKhoanMucThueVaThuKhac,
	@TriGiaTinhThueVaThuKhac,
	@SoLuongTinhThueVaThuKhac,
	@MaDVTDanhThueVaThuKhac,
	@ThueSuatThueVaThuKhac,
	@SoTienThueVaThuKhac,
	@DieuKhoanMienGiamThueVaThuKhac
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]
	@ID bigint,
	@Master_id bigint,
	@MaTSThueThuKhac varchar(10),
	@MaMGThueThuKhac varchar(5),
	@SoTienGiamThueThuKhac numeric(16, 4),
	@TenKhoanMucThueVaThuKhac nvarchar(9),
	@TriGiaTinhThueVaThuKhac numeric(17, 4),
	@SoLuongTinhThueVaThuKhac numeric(14, 0),
	@MaDVTDanhThueVaThuKhac varchar(4),
	@ThueSuatThueVaThuKhac varchar(24),
	@SoTienThueVaThuKhac numeric(16, 4),
	@DieuKhoanMienGiamThueVaThuKhac varchar(60)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
SET
	[Master_id] = @Master_id,
	[MaTSThueThuKhac] = @MaTSThueThuKhac,
	[MaMGThueThuKhac] = @MaMGThueThuKhac,
	[SoTienGiamThueThuKhac] = @SoTienGiamThueThuKhac,
	[TenKhoanMucThueVaThuKhac] = @TenKhoanMucThueVaThuKhac,
	[TriGiaTinhThueVaThuKhac] = @TriGiaTinhThueVaThuKhac,
	[SoLuongTinhThueVaThuKhac] = @SoLuongTinhThueVaThuKhac,
	[MaDVTDanhThueVaThuKhac] = @MaDVTDanhThueVaThuKhac,
	[ThueSuatThueVaThuKhac] = @ThueSuatThueVaThuKhac,
	[SoTienThueVaThuKhac] = @SoTienThueVaThuKhac,
	[DieuKhoanMienGiamThueVaThuKhac] = @DieuKhoanMienGiamThueVaThuKhac
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@MaTSThueThuKhac varchar(10),
	@MaMGThueThuKhac varchar(5),
	@SoTienGiamThueThuKhac numeric(16, 4),
	@TenKhoanMucThueVaThuKhac nvarchar(9),
	@TriGiaTinhThueVaThuKhac numeric(17, 4),
	@SoLuongTinhThueVaThuKhac numeric(14, 0),
	@MaDVTDanhThueVaThuKhac varchar(4),
	@ThueSuatThueVaThuKhac varchar(24),
	@SoTienThueVaThuKhac numeric(16, 4),
	@DieuKhoanMienGiamThueVaThuKhac varchar(60)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] 
		SET
			[Master_id] = @Master_id,
			[MaTSThueThuKhac] = @MaTSThueThuKhac,
			[MaMGThueThuKhac] = @MaMGThueThuKhac,
			[SoTienGiamThueThuKhac] = @SoTienGiamThueThuKhac,
			[TenKhoanMucThueVaThuKhac] = @TenKhoanMucThueVaThuKhac,
			[TriGiaTinhThueVaThuKhac] = @TriGiaTinhThueVaThuKhac,
			[SoLuongTinhThueVaThuKhac] = @SoLuongTinhThueVaThuKhac,
			[MaDVTDanhThueVaThuKhac] = @MaDVTDanhThueVaThuKhac,
			[ThueSuatThueVaThuKhac] = @ThueSuatThueVaThuKhac,
			[SoTienThueVaThuKhac] = @SoTienThueVaThuKhac,
			[DieuKhoanMienGiamThueVaThuKhac] = @DieuKhoanMienGiamThueVaThuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
		(
			[Master_id],
			[MaTSThueThuKhac],
			[MaMGThueThuKhac],
			[SoTienGiamThueThuKhac],
			[TenKhoanMucThueVaThuKhac],
			[TriGiaTinhThueVaThuKhac],
			[SoLuongTinhThueVaThuKhac],
			[MaDVTDanhThueVaThuKhac],
			[ThueSuatThueVaThuKhac],
			[SoTienThueVaThuKhac],
			[DieuKhoanMienGiamThueVaThuKhac]
		)
		VALUES 
		(
			@Master_id,
			@MaTSThueThuKhac,
			@MaMGThueThuKhac,
			@SoTienGiamThueThuKhac,
			@TenKhoanMucThueVaThuKhac,
			@TriGiaTinhThueVaThuKhac,
			@SoLuongTinhThueVaThuKhac,
			@MaDVTDanhThueVaThuKhac,
			@ThueSuatThueVaThuKhac,
			@SoTienThueVaThuKhac,
			@DieuKhoanMienGiamThueVaThuKhac
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[MaTSThueThuKhac],
	[MaMGThueThuKhac],
	[SoTienGiamThueThuKhac],
	[TenKhoanMucThueVaThuKhac],
	[TriGiaTinhThueVaThuKhac],
	[SoLuongTinhThueVaThuKhac],
	[MaDVTDanhThueVaThuKhac],
	[ThueSuatThueVaThuKhac],
	[SoTienThueVaThuKhac],
	[DieuKhoanMienGiamThueVaThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[MaTSThueThuKhac],
	[MaMGThueThuKhac],
	[SoTienGiamThueThuKhac],
	[TenKhoanMucThueVaThuKhac],
	[TriGiaTinhThueVaThuKhac],
	[SoLuongTinhThueVaThuKhac],
	[MaDVTDanhThueVaThuKhac],
	[ThueSuatThueVaThuKhac],
	[SoTienThueVaThuKhac],
	[DieuKhoanMienGiamThueVaThuKhac]
FROM [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[MaTSThueThuKhac],
	[MaMGThueThuKhac],
	[SoTienGiamThueThuKhac],
	[TenKhoanMucThueVaThuKhac],
	[TriGiaTinhThueVaThuKhac],
	[SoLuongTinhThueVaThuKhac],
	[MaDVTDanhThueVaThuKhac],
	[ThueSuatThueVaThuKhac],
	[SoTienThueVaThuKhac],
	[DieuKhoanMienGiamThueVaThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]	

GO

