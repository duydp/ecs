﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using System.Reflection;
using Company.Interface.Report ;
using Janus.Windows.GridEX;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using System.IO;
using System.Diagnostics;
using Company.Controls.CustomValidation;
//using Company.BLL.KDT.SXXK ;
namespace Company.Interface.GC
{
    public partial class ThongkeHangTonForm : BaseForm
    {
        private NPLNhapTonThucTe NPLTon = new NPLNhapTonThucTe();
        public IList<NPLNhapTonThucTe> Collection = new List<NPLNhapTonThucTe>();
        //ToKhaiMauDichCollection TKMDChuaPhanBoCollection = new ToKhaiMauDichCollection();
        ToKhaiMauDichCollection TKMDDaPhanBoCollection = new ToKhaiMauDichCollection();
        public HopDong HD;
        public double ValueOld = 0;
        public ThongkeHangTonForm()
        {            
            InitializeComponent();
            this.uiTabSanPham.SelectedTabChanged -= new Janus.Windows.UI.Tab.TabEventHandler(this.uiTabSanPham_SelectedTabChanged);
        }
        
        public bool check = true ;
        
        private void ThongkeHangTonForm_Load(object sender, EventArgs e)
        {
            //txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
            this.uiTabSanPham.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.uiTabSanPham_SelectedTabChanged);
            ccFrom.Value = new DateTime(DateTime.Today.Year, 1, 1);
            dgList.Tables[0].Columns["LuongTon"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListNPLTK.Tables[0].Columns["Luong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListNPLTK.Tables[0].Columns["Ton"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListPhanBo.Tables[0].Columns["DinhMucChung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgListPhanBo.Tables[0].Columns["SoLuongXuat"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
            dgListPhanBo.Tables[0].Columns["NhuCau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListPhanBo.Tables[0].Columns["LuongTonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListPhanBo.Tables[0].Columns["LuongPhanBo"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListPhanBo.Tables[0].Columns["LuongCungUng"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListPhanBo.Tables[0].Columns["LuongTonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;


            BindDataNPLTonThucTe();
            BindDataToKhaiNPLTon();
            
        }
        private void BindDataToKhaiNPLTon()
        {
            Collection = NPLNhapTonThucTe.SelectCollectionDynamic("a.MaHaiQuan='" + HD.MaHaiQuan + "' and a.MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND a.ID_HopDong = " + this.HD.ID, "");
            dgListNPLTK.DataSource = Collection;
            dgListNPLTK.Refetch();
        }
        
        private void dgListNPLTK_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT"].Text = DonViTinh_GetName(e.Row.Cells["DVT"].Value);
            }
            catch { }

        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
            }
            catch { }

        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindDataNPLTonThucTe()
        {
            dgList.DataSource = NPLNhapTonThucTe.SelectNPLTonThucTeByHopDong(HD.ID);
            dgList.Refetch();
        }
        private void saveLuongTon()
        {
            try
            {               
                NPLNhapTonThucTe.InsertUpdateCollection(Collection);
                try
                {
                    dgListNPLTK.DataSource = Collection;
                    dgListNPLTK.Refetch();
                }
                catch { dgListNPLTK.Refresh(); }
                showMsg("MSG_2702001");
                //ShowMessage("Cập nhật thành công.", false);
            }
            catch (Exception ex)
            {
                //ShowMessage(ex.Message, false);
                showMsg("MSG_2702004", ex.Message);
            }
        }        

       
        private void searchToKhaiTonThucTe()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = " ID_HopDong  = " + this.HD.ID;
            string temp = "";
            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND a.SoToKhai = " + txtSoTiepNhan.Value;
            }
            //if (txtNamTiepNhan.TextLength > 0)
            //{
            //    where += " AND a.NamDangKy = " + txtNamTiepNhan.Value;
            //}
            where += "AND a.NgayDangKy BETWEEN '" + ccFrom.Value.ToString("MM/dd/yyyy") + "' AND '" + ccTo.Value.ToString("MM/dd/yyyy") + "'";
            if (txtMaNPL.Text.Length > 0)
            {
                where += " AND a.MaNPL LIKE '%" + txtMaNPL.Text + "%'";
            }
            

            //where += " AND Cast(a.SoToKhai as varchar(10))+ a.MaLoaiHinh +  Cast(a.NamDangKy as varchar(4))+a.MaHaiQuan IN " +
            //"(SELECT Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan FROM dbo.t_KDT_ToKhaiMauDich WHERE MaLoaiHinh LIKE 'N%'" + temp + ")";

            // Thực hiện tìm kiếm.                        
            Collection= NPLNhapTonThucTe.SelectCollectionDynamic(where, "");
            dgListNPLTK.DataSource = Collection;
            dgListNPLTK.Refetch();
        }
        private void TimToKhaiTon_Click(object sender, EventArgs e)
        {
            searchToKhaiTonThucTe();
        }

        private void inDanhSachTon_Click_1(object sender, EventArgs e)
        {
            gridEXPrintDocument1.Print();
        }

       

        private void XemPhanBo_Click(object sender, EventArgs e)
        {
            long id = 0;
            try
            {
                id = Convert.ToInt64(cbbToKhai.Text);
            }
            catch
            {
                ShowMessage("Bạn hãy chọn tờ khai.", false);
                return;
            }
            string maLoaiHinh = Convert.ToString(((DataRowView)cbbToKhai.SelectedItem).Row["MaLoaiHinh"]);
            if (maLoaiHinh.Contains("XGC"))
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                if (cbbToKhai.Text == "")
                {
                    showMsg("MSG_WRN29");
                    //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                    return;
                }
                try
                {
                    TKMD.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                if (cbSanPham.Text.Trim() == "")
                    dgListPhanBo.DataSource = PhanBoToKhaiNhap.SelectCollectionPhanBoToKhaiXuatOfTKMD(TKMD.ID, GlobalSettings.SoThapPhan.LuongNPL);
                else
                    dgListPhanBo.DataSource = PhanBoToKhaiNhap.SelectCollectionPhanBoToKhaiXuatOfTKMDAndMaSP(TKMD.ID, cbSanPham.SelectedItem.Value.ToString(), GlobalSettings.SoThapPhan.LuongNPL);
                dgListPhanBo.Refetch();
                cbSanPham.Items.Clear();
                cbSanPham.Items.Add("", "");
                foreach (HangMauDich HMD in TKMD.HMDCollection)
                {
                    cbSanPham.Items.Add(HMD.TenHang + " / " + HMD.MaPhu, HMD.MaPhu);
                }
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                if (cbbToKhai.Text == "")
                {
                    showMsg("MSG_WRN29");
                    //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                    return;
                }
                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKCT.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                if (cbSanPham.Text.Trim() == "")
                    dgListPhanBo.DataSource = PhanBoToKhaiNhap.SelectCollectionPhanBoToKhaiXuatOfTKCT(TKCT.ID, GlobalSettings.SoThapPhan.LuongNPL);
                else
                    dgListPhanBo.DataSource = PhanBoToKhaiNhap.SelectCollectionPhanBoToKhaiXuatOfTKMDAndMaSPTKCT(TKCT.ID, cbSanPham.SelectedItem.Value.ToString(), GlobalSettings.SoThapPhan.LuongNPL);
                dgListPhanBo.Refetch();
                cbSanPham.Items.Clear();
                cbSanPham.Items.Add("", "");
                foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                {
                    cbSanPham.Items.Add(HCT.TenHang + " / " + HCT.MaHang, HCT.MaHang);
                }
 
            }
        }

        private void dgListTKChuaPhanBo_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string loaiHinh = (e.Row.Cells["MaLoaiHinh"].Value.ToString());
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());

            }
        }
        private void BindDataToKhaiChuaPhanBo()
        {

            dgListTKChuaPhanBo.DataSource = ToKhaiMauDich.SelectToKhaiChuaPhanBo(HD.ID);
            dgListTKChuaPhanBo.Refetch();
        }
        private void BindDataToKhaiDaPhanBo()
        {
            //TKMDDaPhanBoCollection =
            dgToKhaiDaPhanBo.DataSource = ToKhaiMauDich.SelectToKhaiDaPhanBo(HD.ID);
            dgToKhaiDaPhanBo.Refetch();
        }
        private void uiTabSanPham_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            switch (e.Page.Key)
            {
                case "tpQLNPLTon":
                    BindDataNPLTonThucTe();
                    break;
                case "tpPhanBoToKhaiXuat":
                    BindDataToKhaiChuaPhanBo();
                    break;
                case "tpDanhSachToKhaiDaPhanBo":
                    BindDataToKhaiDaPhanBo();
                    break;
                case "tpTheoDoiPhanBo":
                    BindToKhaiDaPhanBoToCombobox();
                    break;

                    
            }
        }

        private void BindToKhaiDaPhanBoToCombobox()
        {
            
            cbbToKhai.DisplayMember = "ID";
            cbbToKhai.ValueMember = "ID";
            cbbToKhai.DataSource = ToKhaiMauDich.SelectToKhaiDaPhanBo(HD.ID);
        }

        private void PhanBo_Click(object sender, EventArgs e)
        {
            if (dgListTKChuaPhanBo.GetCheckedRows().Length <= 0) 
            {
                showMsg("MSG_0203083"); 
                return;
            }
            int i = 0;
            this.Cursor = Cursors.WaitCursor;
            foreach (GridEXRow row in dgListTKChuaPhanBo.GetCheckedRows())
            {
                string maLoaiHinh = row.Cells["MaLoaiHinh"].Value.ToString();
                if (maLoaiHinh.Contains("XGC"))
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.ID = Convert.ToInt64(row.Cells["ID"].Value);
                    TKMD.Load();
                    TKMD.LoadHMDCollection();
                    try
                    {
                        if (TKMD.LoaiHangHoa == "S")
                        {
                            PhanBoToKhaiXuat.PhanBoChoToKhaiXuatGC(TKMD, GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.DinhMuc);
                        }
                        else
                        {
                            SelectNPLTaiXuatForm f = new SelectNPLTaiXuatForm();
                            f.TKMD = TKMD;
                            TKMD.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
                            foreach (HangMauDich HMD in TKMD.HMDCollection)
                            {
                                PhanBoToKhaiXuat pbToKhaiTaiXuat = new PhanBoToKhaiXuat();
                                pbToKhaiTaiXuat.ID_TKMD = TKMD.ID;
                                pbToKhaiTaiXuat.MaDoanhNghiep = TKMD.MaLoaiHinh;
                                pbToKhaiTaiXuat.MaSP = HMD.MaPhu;
                                pbToKhaiTaiXuat.SoLuongXuat = HMD.SoLuong;
                                TKMD.PhanBoToKhaiXuatCollection.Add(pbToKhaiTaiXuat);
                            }
                            f.pbToKhaiXuatCollection = TKMD.PhanBoToKhaiXuatCollection;
                            f.ShowDialog();
                            if (!f.isPhanBo)
                            {
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        showMsg("MSG_WRN28", TKMD.SoToKhai);
                        //ShowMessage("Có lỗi : "+ex.Message+" trong tờ khai "+TKMD.SoToKhai, false);
                        break;
                    }
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.ID = Convert.ToInt64(row.Cells["ID"].Value);
                    TKCT.Load();
                    TKCT.LoadHCTCollection();
                    try
                    {
                        if (TKCT.MaLoaiHinh.Trim() == "PHSPX")
                        {
                            PhanBoToKhaiXuat.PhanBoChoToKhaiChuyenTiep(TKCT, GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.DinhMuc);
                        }
                        else
                        {
                            SelectNPLTaiXuatTKCTForm f = new SelectNPLTaiXuatTKCTForm();
                            f.TKCT = TKCT;
                            TKCT.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
                            foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                            {
                                PhanBoToKhaiXuat pbToKhaiTaiXuat = new PhanBoToKhaiXuat();
                                pbToKhaiTaiXuat.ID_TKMD = TKCT.ID;
                                pbToKhaiTaiXuat.MaDoanhNghiep = TKCT.MaLoaiHinh;
                                pbToKhaiTaiXuat.MaSP = HCT.MaHang;
                                pbToKhaiTaiXuat.SoLuongXuat = HCT.SoLuong;
                                TKCT.PhanBoToKhaiXuatCollection.Add(pbToKhaiTaiXuat);
                            }
                            f.pbToKhaiXuatCollection = TKCT.PhanBoToKhaiXuatCollection;
                            f.ShowDialog();
                            if (!f.isPhanBo)
                            {
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        showMsg("MSG_WRN28", TKCT.SoToKhai);
                        //ShowMessage("Có lỗi : "+ex.Message+" trong tờ khai "+TKMD.SoToKhai, false);
                        break;
                    }
 
                }
            }
            BindDataToKhaiChuaPhanBo();
            this.Cursor = Cursors.Default;
            showMsg("MSG_240210");
            //ShowMessage("Đã thực hiện xong.", false);
        }

        private void dgListSPTON_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void dgListTKChuaPhanBo_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void dgListPhanBo_RecordUpdated(object sender, EventArgs e)
        {
            
        }

        private void dgListPhanBo_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                long TKMD_IDPhanBo = Convert.ToInt64(e.Row.Cells["TKXuat_ID"].Value);
                PhanBoToKhaiXuat pbTKXuat = PhanBoToKhaiXuat.Load(TKMD_IDPhanBo);
                Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
                SP.HopDong_ID = this.HD.ID;
                SP.Ma = pbTKXuat.MaSP;
                SP.Load();
                e.Row.Cells["TenSP"].Text = SP.Ten;
                e.Row.Cells["MaSP"].Text = pbTKXuat.MaSP;
                e.Row.Cells["SoLuongXuat"].Text = pbTKXuat.SoLuongXuat + "";
                e.Row.Cells["NhuCau"].Text = pbTKXuat.SoLuongXuat * Convert.ToDecimal(e.Row.Cells["DinhMucChung"].Text) + "";
                e.Row.Cells["TKXuat_ID"].Text = SP.Ten;
            }            
        }

        private void uiButton9_Click(object sender, EventArgs e)
        {
            
        }

        private void dgListPhanBo_EditingCell(object sender, EditingCellEventArgs e)
        {
            ValueOld = Convert.ToDouble(e.Value);
        }

        private void cmdXemBangNPLCungUng_Click(object sender, EventArgs e)
        {
            long id = 0;
            try
            {
                id = Convert.ToInt64(cbbToKhai.Text);
            }
            catch
            {
                ShowMessage("Bạn hãy chọn tờ khai.", false);
                return;
            }
            string maLoaiHinh = Convert.ToString(((DataRowView)cbbToKhai.SelectedItem).Row["MaLoaiHinh"]);
            if (maLoaiHinh.Contains("XGC"))
            {

                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                if (cbbToKhai.Text == "")
                {
                    showMsg("MSG_WRN29");
                    //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                    return;
                }
                try
                {
                    TKMD.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                ViewNPLCungUngForm f = new ViewNPLCungUngForm();

                f.TKMD = TKMD;
                f.HD = this.HD;
                f.Show();
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                if (cbbToKhai.Text == "")
                {
                    showMsg("MSG_WRN29");
                    //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                    return;
                }
                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKCT.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                ViewNPLCungUngTKCTForm f = new ViewNPLCungUngTKCTForm();

                f.TKCT = TKCT;
                f.HD = this.HD;
                f.Show();
            }
        }

        private void cmdViewBangPhanBoNPL_Click(object sender, EventArgs e)
        {
            long id = 0;
            try
            {
                id = Convert.ToInt64(cbbToKhai.Text);
            }
            catch
            {
                ShowMessage("Bạn hãy chọn tờ khai.", false);
                return;
            }
            string maLoaiHinh = Convert.ToString(((DataRowView)cbbToKhai.SelectedItem).Row["MaLoaiHinh"]);
            if (maLoaiHinh.Contains("XGC"))
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                if (cbbToKhai.Text == "")
                {
                    showMsg("MSG_WRN29");
                    //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                    return;
                }
                try
                {
                    TKMD.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                ViewPhanBoForm f = new ViewPhanBoForm();


                f.TKMD = TKMD;
                f.HD = this.HD;
                f.Show();
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                if (cbbToKhai.Text == "")
                {
                    showMsg("MSG_WRN29");
                    //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                    return;
                }
                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKCT.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                ViewPhanBoTKCTForm f = new ViewPhanBoTKCTForm();
                f.TKCT = TKCT;
                f.HD = this.HD;
                f.Show();
            }
        }

        private void btnDeletePhanBo_Click(object sender, EventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa phân bổ tờ khai này ko?", true) == "Yes")
            {
                GridEXRow row = dgToKhaiDaPhanBo.GetRow();
                string maLoaiHinh = row.Cells["MaLoaiHinh"].Value.ToString();
                if (maLoaiHinh.Contains("XGC"))
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.ID = Convert.ToInt64(row.Cells["ID"].Value);
                    TKMD.Load();
                    TKMD.DeletePhanBo();
                    showMsg("MSG_DEL02");
                    //ShowMessage("Xóa phân bổ thành công", false);
                    BindDataToKhaiDaPhanBo();
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.ID = Convert.ToInt64(row.Cells["ID"].Value);
                    TKCT.Load();
                    TKCT.DeletePhanBo();
                    showMsg("MSG_DEL02");
                    //ShowMessage("Xóa phân bổ thành công", false);
                    BindDataToKhaiDaPhanBo();
                }
            }
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            SaveFileDialog d = new SaveFileDialog();
            d.RestoreDirectory = true;
            d.InitialDirectory = Application.StartupPath;
            d.Filter = "Excel files| *.xls";
            d.ShowDialog();
            if (d.FileName != "")
            {
                Stream s = d.OpenFile();
                gridEXExporter1.Export(s);
                s.Close();
                if (showMsg("MSG_MAL08", true) == "Yes")
                //if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(d.FileName);
                }
            }
        }

        private void toolStripMenuItemBangPhanBo_Click(object sender, EventArgs e)
        {
            long id = Convert.ToInt64(dgToKhaiDaPhanBo.GetRow().Cells["ID"].Value);
            string maLoaiHinh = dgToKhaiDaPhanBo.GetRow().Cells["MaLoaiHinh"].Value.ToString();
            if (maLoaiHinh.Contains("XGC"))
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();

                try
                {
                    TKMD.ID = Convert.ToInt64(dgToKhaiDaPhanBo.GetRow().Cells["ID"].Value);
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                ViewPhanBoForm f = new ViewPhanBoForm();

                f.TKMD = TKMD;
                f.HD = this.HD;
                f.Show();
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();

                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKCT.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                ViewPhanBoTKCTForm f = new ViewPhanBoTKCTForm();

                f.TKCT = TKCT;
                f.HD = this.HD;
                f.Show();
            }

        }

        private void toolStripMenuItemBangNPLCungUng_Click(object sender, EventArgs e)
        {
            long id = Convert.ToInt64(dgToKhaiDaPhanBo.GetRow().Cells["ID"].Value);
            string maLoaiHinh = dgToKhaiDaPhanBo.GetRow().Cells["MaLoaiHinh"].Value.ToString();
            if (maLoaiHinh.Contains("XGC"))
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                try
                {
                    TKMD.ID = Convert.ToInt64(dgToKhaiDaPhanBo.GetRow().Cells["ID"].Value);
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                ViewNPLCungUngForm f = new ViewNPLCungUngForm();
                f.TKMD = TKMD;
                f.HD = this.HD;
                f.Show();
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKCT.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                ViewNPLCungUngTKCTForm f = new ViewNPLCungUngTKCTForm();
                f.TKCT = TKCT;
                f.HD = this.HD;
                f.Show();
            }
        }

        private void toolStripMenuItemPhanBo_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            GridEXRow row = dgListTKChuaPhanBo.GetRow();
            if (row == null) return;
            string maLoaiHinh = row.Cells["MaLoaiHinh"].Value.ToString();
            if (maLoaiHinh.Contains("XGC"))
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.ID = Convert.ToInt64(row.Cells["ID"].Value);
                TKMD.Load();
                TKMD.LoadHMDCollection();
                try
                {
                    if (TKMD.LoaiHangHoa == "S")
                    {
                        PhanBoToKhaiXuat.PhanBoChoToKhaiXuatGC(TKMD, GlobalSettings.SoThapPhan.LuongNPL , GlobalSettings.SoThapPhan.DinhMuc);
                    }
                    else
                    {
                        SelectNPLTaiXuatForm f = new SelectNPLTaiXuatForm();
                        f.TKMD = TKMD;
                        TKMD.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
                        foreach (HangMauDich HMD in TKMD.HMDCollection)
                        {
                            PhanBoToKhaiXuat pbToKhaiTaiXuat = new PhanBoToKhaiXuat();
                            pbToKhaiTaiXuat.ID_TKMD = TKMD.ID;
                            pbToKhaiTaiXuat.MaDoanhNghiep = TKMD.MaLoaiHinh;
                            pbToKhaiTaiXuat.MaSP = HMD.MaPhu;
                            pbToKhaiTaiXuat.SoLuongXuat = HMD.SoLuong;
                            TKMD.PhanBoToKhaiXuatCollection.Add(pbToKhaiTaiXuat);
                        }
                        f.pbToKhaiXuatCollection = TKMD.PhanBoToKhaiXuatCollection;
                        f.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    showMsg("MSG_WRN28", TKMD.SoToKhai);
                    //ShowMessage("Có lỗi : "+ex.Message+" trong tờ khai "+TKMD.SoToKhai, false);
                }
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT.ID = Convert.ToInt64(row.Cells["ID"].Value);
                TKCT.Load();
                TKCT.LoadHCTCollection();
                try
                {
                    if (TKCT.MaLoaiHinh.Trim() == "PHSPX")
                    {
                        PhanBoToKhaiXuat.PhanBoChoToKhaiChuyenTiep(TKCT, GlobalSettings.SoThapPhan.LuongNPL,  GlobalSettings.SoThapPhan.DinhMuc);
                    }
                    else
                    {
                        SelectNPLTaiXuatTKCTForm f = new SelectNPLTaiXuatTKCTForm();
                        f.TKCT = TKCT;
                        TKCT.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
                        foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                        {
                            PhanBoToKhaiXuat pbToKhaiTaiXuat = new PhanBoToKhaiXuat();
                            pbToKhaiTaiXuat.ID_TKMD = TKCT.ID;
                            pbToKhaiTaiXuat.MaDoanhNghiep = TKCT.MaLoaiHinh;
                            pbToKhaiTaiXuat.MaSP = HCT.MaHang;
                            pbToKhaiTaiXuat.SoLuongXuat = HCT.SoLuong;
                            TKCT.PhanBoToKhaiXuatCollection.Add(pbToKhaiTaiXuat);
                        }
                        f.pbToKhaiXuatCollection = TKCT.PhanBoToKhaiXuatCollection;
                        f.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    showMsg("MSG_WRN28", TKCT.SoToKhai);
                    //ShowMessage("Có lỗi : "+ex.Message+" trong tờ khai "+TKMD.SoToKhai, false);
                }
            }
            BindDataToKhaiChuaPhanBo();
            this.Cursor = Cursors.Default;
            showMsg("MSG_2702023");
            //ShowMessage("Đã thực hiện phân bổ xong.", false);
        }

        private void btnXuatExcelNPLTon_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.RestoreDirectory = true;
            sfNPL.InitialDirectory = Application.StartupPath;
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.ShowDialog();
            if (sfNPL.FileName != "")
            {
                Stream str = sfNPL.OpenFile();
                gridEXExporterNPL.Export(str);
                str.Close();
                if (showMsg("MSG_MAL08", true) == "Yes")
                //if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
        }

        private void btnInNPLTon_Click(object sender, EventArgs e)
        {
            gridEXPrintDocumentNPL.Print();
        }

        private void btnXuatExcelNPLCungUng_Click(object sender, EventArgs e)
        {

            long id = 0;
            try
            {
                id = Convert.ToInt64(cbbToKhai.Text);
            }
            catch
            {
                ShowMessage("Bạn hãy chọn tờ khai.", false);
                return;
            }
            string maLoaiHinh = Convert.ToString(((DataRowView)cbbToKhai.SelectedItem).Row["MaLoaiHinh"]);
            if (maLoaiHinh.Contains("XGC"))
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                if (cbbToKhai.Text == "")
                {
                    showMsg("MSG_WRN29");
                    //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                    return;
                }
                try
                {
                    TKMD.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                XuatExcelNPLCungUng(TKMD);
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                if (cbbToKhai.Text == "")
                {
                    showMsg("MSG_WRN29");
                    //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                    return;
                }
                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKCT.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                XuatExcelNPLCungUng(TKCT);
            }
        }
        private void XuatExcelNPLCungUng(ToKhaiMauDich TKMD)
        {
            Workbook wb = new Workbook();
            Company.GC.BLL.GC.DinhMuc DMtemp = new Company.GC.BLL.GC.DinhMuc();
            long soSheet1 = 0;
            foreach (HangMauDich HMD in TKMD.HMDCollection)
            {
                DataTable dt = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCuaSanPhamGroupByMaNPL(TKMD.ID, this.HD.ID, HMD.MaPhu).Tables[0];
                if (dt.Rows.Count == 0) continue;
                Worksheet ws = wb.Worksheets.Add(HMD.MaPhu);
                soSheet1++;
                if(HMD.MaPhu=="ULHF583")
                {

                }

                WorksheetRowCollection wsrc = ws.Rows;
                WorksheetRow wsr0 = ws.Rows[0];
                ws.Columns[0].Width = 2500;
                ws.Columns[1].Width = 2500;
                ws.Columns[2].Width = 2500;
                ws.Columns[3].Width = 2500;
                ws.Columns[3].Width = 2500;
                wsr0.Cells[0].Value = "Mã SP";
                wsr0.Cells[1].Value = "Mã NPL";
                wsr0.Cells[2].Value = "Định mức";
                wsr0.Cells[3].Value = "TLHH";
                wsr0.Cells[4].Value = "Đơn giá";

                string maNPL = "";
                decimal LuongPhanBo = 0;
                decimal TongTriGia = 0; 
                int t = 1;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];                  
                    if (maNPL.Trim().ToUpper() != dr["MaNPL"].ToString())
                    {
                       
                        LuongPhanBo = 0;
                        TongTriGia = 0;
                        WorksheetRow wsr = ws.Rows[t];
                        wsr.Cells[0].Value = DMtemp.MaSanPham = HMD.MaPhu;
                        wsr.Cells[1].Value = DMtemp.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                        DMtemp.HopDong_ID = this.HD.ID;
                        DMtemp.Load();
                        LuongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                        wsr.Cells[2].Value = DMtemp.DinhMucSuDung;
                        wsr.Cells[3].Value = DMtemp.TyLeHaoHut;
                        if (dr["MaLoaiHinhNhap"].ToString().Contains("NSX"))
                        {
                            ToKhaiMauDich TKMDNhap = new ToKhaiMauDich();
                            TKMDNhap.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                            TKMDNhap.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinhNhap"]);
                            TKMDNhap.NgayDangKy = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                            wsr.Cells[4].Value = TKMDNhap.SelectDonGia(dr["MaNPL"].ToString());
                            TongTriGia += LuongPhanBo * Convert.ToDecimal(wsr.Cells[4].Value);
                        }
                        else
                        {
                            wsr.Cells[4].Value = 0;
                        }
                        maNPL = dr["MaNPL"].ToString();
                        t++;
                    }
                    else
                    {
                        WorksheetRow wsr = ws.Rows[t-1];
                        LuongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                        wsr.Cells[2].Value = DMtemp.DinhMucSuDung;
                        if (dr["MaLoaiHinhNhap"].ToString().Contains("NSX"))
                        {
                            ToKhaiMauDich TKMDNhap = new ToKhaiMauDich();
                            TKMDNhap.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                            TKMDNhap.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinhNhap"]);
                            TKMDNhap.NgayDangKy = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                            TongTriGia += Convert.ToDecimal(dr["LuongPhanBo"]) * TKMDNhap.SelectDonGia(dr["MaNPL"].ToString());
                            wsr.Cells[4].Value = TongTriGia / LuongPhanBo;
                        }
                        else
                        {
                            if (wsr.Cells[4].Value == null || wsr.Cells[4].Value.ToString() == "")
                                wsr.Cells[4].Value = 0;          
                        }

                    }
                }


            }
            string fileName = "";

            DialogResult rs = saveFileDialog1.ShowDialog();
            if (rs == DialogResult.OK && soSheet1>0)
            {
                fileName = saveFileDialog1.FileName;
                wb.Save(fileName); 
                if (showMsg("MSG_MAL08", true) == "Yes")
                {
                    Process.Start(fileName);
                }
            }

            //wb = Workbook.Load(fileName);
           
        }
        private void XuatExcelNPLCungUng(ToKhaiChuyenTiep TKCT)
        {
            Workbook wb = new Workbook();
            Company.GC.BLL.GC.DinhMuc DMtemp = new Company.GC.BLL.GC.DinhMuc();

            foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
            {
                DataTable dt = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCuaSanPhamGroupByMaNPLTKCT(TKCT.ID, this.HD.ID, HCT.MaHang).Tables[0];
                if (dt.Rows.Count == 0) continue;
                Worksheet ws = wb.Worksheets.Add(HCT.MaHang);

                WorksheetRowCollection wsrc = ws.Rows;
                WorksheetRow wsr0 = ws.Rows[0];
                ws.Columns[0].Width = 2500;
                ws.Columns[1].Width = 2500;
                ws.Columns[2].Width = 2500;
                ws.Columns[3].Width = 2500;
                ws.Columns[3].Width = 2500;
                wsr0.Cells[0].Value = "Mã SP";
                wsr0.Cells[1].Value = "Mã NPL";
                wsr0.Cells[2].Value = "Định mức";
                wsr0.Cells[3].Value = "TLHH";
                wsr0.Cells[4].Value = "Đơn giá";

                string maNPL = "";
                decimal LuongPhanBo = 0;
                decimal TongTriGia = 0;
                int t = 1;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    if (maNPL.Trim().ToUpper() != dr["MaNPL"].ToString())
                    {
                        LuongPhanBo = 0;
                        TongTriGia = 0;
                        WorksheetRow wsr = ws.Rows[t];
                        wsr.Cells[0].Value = DMtemp.MaSanPham = HCT.MaHang;
                        wsr.Cells[1].Value = DMtemp.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                        DMtemp.HopDong_ID = this.HD.ID;
                        DMtemp.Load();
                        LuongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                        wsr.Cells[2].Value = DMtemp.DinhMucSuDung; 
                        wsr.Cells[3].Value = DMtemp.TyLeHaoHut;
                        if (dr["MaLoaiHinhNhap"].ToString().Contains("NSX"))
                        {
                            ToKhaiMauDich TKMDNhap = new ToKhaiMauDich();
                            TKMDNhap.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                            TKMDNhap.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinhNhap"]);
                            TKMDNhap.NgayDangKy = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                            wsr.Cells[4].Value = TKMDNhap.SelectDonGia(dr["MaNPL"].ToString());
                            TongTriGia += LuongPhanBo * Convert.ToDecimal(wsr.Cells[4].Value);
                        }
                        else
                        {
                            wsr.Cells[4].Value = 0;
                        }
                        maNPL = dr["MaNPL"].ToString();
                        t++;
                    }
                    else
                    {
                        WorksheetRow wsr = ws.Rows[t-1];
                        wsr.Cells[2].Value = DMtemp.DinhMucSuDung;
                        LuongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);                        
                        if (dr["MaLoaiHinhNhap"].ToString().Contains("NSX"))
                        {                            
                            ToKhaiMauDich TKMDNhap = new ToKhaiMauDich();
                            TKMDNhap.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                            TKMDNhap.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinhNhap"]);
                            TKMDNhap.NgayDangKy = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                            TongTriGia += Convert.ToDecimal(dr["LuongPhanBo"]) * TKMDNhap.SelectDonGia(dr["MaNPL"].ToString());
                            wsr.Cells[4].Value = TongTriGia / LuongPhanBo;
                        }
                        else
                        {
                            if (wsr.Cells[4].Value == null || wsr.Cells[4].Value.ToString()=="")
                                wsr.Cells[4].Value = 0;                            
                        }

                    }
                }


            }
            string fileName = "";

            DialogResult rs = saveFileDialog1.ShowDialog();
            if (rs == DialogResult.OK)
            {
                fileName = saveFileDialog1.FileName;
                wb.Save(fileName); 
                if (showMsg("MSG_MAL08", true) == "Yes")
                {
                    Process.Start(fileName);
                }
            }

            //wb = Workbook.Load(fileName);
           
        }
        private void xuatExcelNPLCungUngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgToKhaiDaPhanBo.GetRow();
            string maLoaiHinh = row.Cells["MaLoaiHinh"].Value.ToString();
            long id = Convert.ToInt64(row.Cells["ID"].Value);
            if (maLoaiHinh.Contains("XGC"))
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();

                try
                {
                    TKMD.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                XuatExcelNPLCungUng(TKMD);
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();

                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKCT.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                XuatExcelNPLCungUng(TKCT);
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void btnChuyenTonSXXK_Click(object sender, EventArgs e)
        {
            ChuyenTonSXXK f = new ChuyenTonSXXK();
            f.HD = this.HD;
            f.ShowDialog();
            BindDataToKhaiNPLTon();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa phân bổ tờ khai này ko?", true) == "Yes")
            {
                GridEXRow row = dgToKhaiDaPhanBo.GetRow();
                string maLoaiHinh = row.Cells["MaLoaiHinh"].Value.ToString();
                if (maLoaiHinh.Contains("XGC"))
                {
                    try
                    {
                        ToKhaiMauDich TKMD = new ToKhaiMauDich();
                        TKMD.ID = Convert.ToInt64(row.Cells["ID"].Value);
                        TKMD.Load();
                        DataTable dtNPLToKhaiNhap = TKMD.GetDanhSachToKhaiNhapPhanBo();
                        TKMD.DeletePhanBoVaFixData(dtNPLToKhaiNhap);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi khi thực hiện: " + ex.Message, false);
                        return;
                    }
                    ShowMessage("Xóa phân bổ và chỉnh dữ liệu phân bổ thành công", false);
                    BindDataToKhaiDaPhanBo();
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.ID = Convert.ToInt64(row.Cells["ID"].Value);
                    TKCT.Load();
                    TKCT.DeletePhanBo();
                    //showMsg("MSG_DEL02");
                    ShowMessage("Xóa phân bổ và chỉnh dữ liệu phân bổ thành công", false);
                    BindDataToKhaiDaPhanBo();
                }
            }
        }

        private void uiTabSanPham_SelectedTabChanged_1(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {

        }

        private void btnLayDuLieuCungUng_Click(object sender, EventArgs e)
        {
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                TKMD.LayDuLieuCungUng();
                ShowMessage("Cập nhật thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

      
     }
}