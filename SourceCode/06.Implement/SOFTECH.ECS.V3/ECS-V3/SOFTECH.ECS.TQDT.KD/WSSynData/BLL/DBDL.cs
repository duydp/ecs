﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Company.KDT.SHARE.Components;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;
namespace WSSynData
{
    public partial class DBDL
    {

        public void TaoDuLieu(SyncDaTa ThongTinMessagess)
        {
            this.GUIDSTR = ThongTinMessagess.Declaration[0].Reference;
            this.STATUS = 0;
            this.MSG_TO = ThongTinMessagess.Declaration[0].Business;
            this.CREATE_TIME = DateTime.Now;
            this.MSG_TYPE = ThongTinMessagess.Type;
        }
        
        public static DBDL LoadByGuiStr(string GuiStr)
        {
            const string spName = "[dbo].[p_MSG_DBDL_LoadByGuiStr]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GuiStr);
            DBDL entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new DBDL();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) entity.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
                if (!reader.IsDBNull(reader.GetOrdinal("CREATE_TIME"))) entity.CREATE_TIME = reader.GetDateTime(reader.GetOrdinal("CREATE_TIME"));
                if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) entity.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_TYPE"))) entity.MSG_TYPE = reader.GetString(reader.GetOrdinal("MSG_TYPE"));
            }
            reader.Close();
            return entity;
        }
    }
}
