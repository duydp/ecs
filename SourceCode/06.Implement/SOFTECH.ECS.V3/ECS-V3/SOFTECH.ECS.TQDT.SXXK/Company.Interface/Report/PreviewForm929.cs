using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.SXXK;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.Report
{
    public partial class PreviewForm929 : BaseForm
    {
        public BangKeToKhaiNhap_929 BKTKN = new BangKeToKhaiNhap_929();
        public BangKeToKhaiXuat_929 BKTKX = new BangKeToKhaiXuat_929();
        public BCNPLXuatNhapTon_929 BCNPLXNT = new BCNPLXuatNhapTon_929();
        public BCThueXNK_929 BCThue = new BCThueXNK_929();
        public BC03_929 BCNPLSXXK = new BC03_929();
        //public BCNPLSXXK_929 BCNPLSXXK = new BCNPLSXXK_929();
        public int LanThanhLy;
        public int SoHoSo;
        public PreviewForm929()
        {
            InitializeComponent();
        }

        private void explorerBar1_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "BKTKN":
                    printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                    this.BKTKN.CreateDocument();
                    break;
                case "BKTKX":
                    printControl1.PrintingSystem = this.BKTKX.PrintingSystem;
                    this.BKTKX.CreateDocument();
                    break;
                case "BCNPLXNT":
                    printControl1.PrintingSystem = this.BCNPLXNT.PrintingSystem;
                    this.BCNPLXNT.CreateDocument();
                    break;
                case "BCThueNK":
                    printControl1.PrintingSystem = this.BCThue.PrintingSystem;
                    this.BCThue.CreateDocument();
                    break;
                case "BCNLSXXK":
                    printControl1.PrintingSystem = this.BCNPLSXXK.PrintingSystem;
                    this.BCNPLSXXK.CreateDocument();
                    break;
            }
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {
            printPreviewBarItem4.Enabled = printPreviewBarItem5.Enabled = Program.isActivated;

            if(GlobalSettings.GiaoDien.Id == "Office2003")
                this.defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            string where = "";
            this.BKTKN.LanThanhLy = this.LanThanhLy;
            this.BKTKN.SoHoSo = this.SoHoSo;
            this.BKTKN.BindReport(where);
            this.BKTKX.SoHoSo = this.SoHoSo;
            this.BKTKX.BindReport(this.LanThanhLy);    
            this.BCNPLXNT.LanThanhLy = this.LanThanhLy;
            this.BCNPLXNT.SoHoSo = this.SoHoSo;
            this.BCNPLXNT.BindReport(where);
            this.BCThue.LanThanhLy = this.LanThanhLy;
            this.BCThue.SoHoSo = this.SoHoSo;
            this.BCThue.BindReport(where);
            this.BCNPLSXXK.LanThanhLy = this.LanThanhLy;
            this.BCNPLSXXK.SoHoSo = this.SoHoSo;
            this.BCNPLSXXK.BindReport(where);
            printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
            this.BKTKN.CreateDocument();
        }
    }
}