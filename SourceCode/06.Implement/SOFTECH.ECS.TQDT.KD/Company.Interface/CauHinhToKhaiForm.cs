﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.DuLieuChuan;

namespace Company.Interface
{
    public partial class CauHinhToKhaiForm : Company.Interface.BaseForm
    {
        public CauHinhToKhaiForm()
        {
            InitializeComponent();
        }
        private void khoitao_DuLieuChuan()
        {
            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll().Tables[0];
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            txtTenDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
//            txtTyGiaUSD.Value = GlobalSettings.TY_GIA_USD;
            cbMaHTS.SelectedIndex = GlobalSettings.MaHTS;

            ctrCuaKhauXuatHang.Ma = GlobalSettings.CUA_KHAU;
            ctrDiaDiemDoHang.Ma = GlobalSettings.DIA_DIEM_DO_HANG;

            txtMienThue1.Text = GlobalSettings.TieuDeInDinhMuc;
            txtMienThue2.Text = GlobalSettings.DonViBaoCao;
            cbTinhThue.SelectedValue = GlobalSettings.TuDongTinhThue;
        }
        private void CauHinhToKhaiForm_Load(object sender, EventArgs e)
        {
            khoitao_DuLieuChuan();
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            Properties.Settings.Default.TEN_DOI_TAC = txtTenDoiTac.Text;
            Properties.Settings.Default.PTTT_MAC_DINH = cbPTTT.SelectedValue.ToString();
            Properties.Settings.Default.PTVT_MAC_DINH = cbPTVT.SelectedValue.ToString();
            Properties.Settings.Default.DKGH_MAC_DINH = cbDKGH.SelectedValue.ToString();
            //Properties.Settings.Default.TY_GIA_USD = Convert.ToDecimal(txtTyGiaUSD.Value);
            Properties.Settings.Default.CUA_KHAU = ctrCuaKhauXuatHang.Ma;
            Properties.Settings.Default.DIA_DIEM_DO_HANG = ctrDiaDiemDoHang.Ma;
            Properties.Settings.Default.NGUYEN_TE_MAC_DINH = ctrNguyenTe.Ma;
            Properties.Settings.Default.NUOC = ctrNuocXK.Ma;
            Properties.Settings.Default.MA_HTS = Convert.ToInt32(cbMaHTS.SelectedValue);
            Properties.Settings.Default.DonViBaoCao = txtMienThue2.Text.Trim();
            Properties.Settings.Default.TuDongTinhThue = cbTinhThue.SelectedValue.ToString();
            Properties.Settings.Default.TieuDeInDinhMuc = txtMienThue1.Text.Trim();
            Properties.Settings.Default.MienThueGTGT = txtMienThue2.Text.Trim();
            Properties.Settings.Default.Save();
            GlobalSettings.RefreshKey();
            ShowMessage("Lưu cấu hình thành công.", false);
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}

