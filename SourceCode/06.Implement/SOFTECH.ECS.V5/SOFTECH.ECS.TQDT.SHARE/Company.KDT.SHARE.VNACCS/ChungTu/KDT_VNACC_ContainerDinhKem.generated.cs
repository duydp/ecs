﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ContainerDinhKem : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long ChungTuDinhKem_ID { set; get; }
		public string SoVanDon { set; get; }
		public string SoContainer { set; get; }
		public string SoSeal { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ContainerDinhKem> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ContainerDinhKem> collection = new List<KDT_VNACC_ContainerDinhKem>();
			while (reader.Read())
			{
				KDT_VNACC_ContainerDinhKem entity = new KDT_VNACC_ContainerDinhKem();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungTuDinhKem_ID"))) entity.ChungTuDinhKem_ID = reader.GetInt64(reader.GetOrdinal("ChungTuDinhKem_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer"))) entity.SoContainer = reader.GetString(reader.GetOrdinal("SoContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal"))) entity.SoSeal = reader.GetString(reader.GetOrdinal("SoSeal"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ContainerDinhKem> collection, long id)
        {
            foreach (KDT_VNACC_ContainerDinhKem item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ContainerDinhKem VALUES(@ChungTuDinhKem_ID, @SoVanDon, @SoContainer, @SoSeal, @GhiChu)";
            string update = "UPDATE t_KDT_VNACC_ContainerDinhKem SET ChungTuDinhKem_ID = @ChungTuDinhKem_ID, SoVanDon = @SoVanDon, SoContainer = @SoContainer, SoSeal = @SoSeal, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ContainerDinhKem WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChungTuDinhKem_ID", SqlDbType.BigInt, "ChungTuDinhKem_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoContainer", SqlDbType.VarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal", SqlDbType.VarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChungTuDinhKem_ID", SqlDbType.BigInt, "ChungTuDinhKem_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoContainer", SqlDbType.VarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal", SqlDbType.VarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ContainerDinhKem VALUES(@ChungTuDinhKem_ID, @SoVanDon, @SoContainer, @SoSeal, @GhiChu)";
            string update = "UPDATE t_KDT_VNACC_ContainerDinhKem SET ChungTuDinhKem_ID = @ChungTuDinhKem_ID, SoVanDon = @SoVanDon, SoContainer = @SoContainer, SoSeal = @SoSeal, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ContainerDinhKem WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChungTuDinhKem_ID", SqlDbType.BigInt, "ChungTuDinhKem_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoContainer", SqlDbType.VarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal", SqlDbType.VarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChungTuDinhKem_ID", SqlDbType.BigInt, "ChungTuDinhKem_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoContainer", SqlDbType.VarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal", SqlDbType.VarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ContainerDinhKem Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ContainerDinhKem> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ContainerDinhKem> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ContainerDinhKem> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACC_ContainerDinhKem> SelectCollectionBy_ChungTuDinhKem_ID(long chungTuDinhKem_ID)
		{
            IDataReader reader = SelectReaderBy_ChungTuDinhKem_ID(chungTuDinhKem_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_ChungTuDinhKem_ID(long chungTuDinhKem_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_SelectBy_ChungTuDinhKem_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungTuDinhKem_ID", SqlDbType.BigInt, chungTuDinhKem_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_ChungTuDinhKem_ID(long chungTuDinhKem_ID)
		{
			const string spName = "p_KDT_VNACC_ContainerDinhKem_SelectBy_ChungTuDinhKem_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungTuDinhKem_ID", SqlDbType.BigInt, chungTuDinhKem_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ContainerDinhKem(long chungTuDinhKem_ID, string soVanDon, string soContainer, string soSeal, string ghiChu)
		{
			KDT_VNACC_ContainerDinhKem entity = new KDT_VNACC_ContainerDinhKem();	
			entity.ChungTuDinhKem_ID = chungTuDinhKem_ID;
			entity.SoVanDon = soVanDon;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@ChungTuDinhKem_ID", SqlDbType.BigInt, ChungTuDinhKem_ID);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.VarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.VarChar, SoSeal);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ContainerDinhKem> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ContainerDinhKem item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ContainerDinhKem(long id, long chungTuDinhKem_ID, string soVanDon, string soContainer, string soSeal, string ghiChu)
		{
			KDT_VNACC_ContainerDinhKem entity = new KDT_VNACC_ContainerDinhKem();			
			entity.ID = id;
			entity.ChungTuDinhKem_ID = chungTuDinhKem_ID;
			entity.SoVanDon = soVanDon;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ContainerDinhKem_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ChungTuDinhKem_ID", SqlDbType.BigInt, ChungTuDinhKem_ID);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.VarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.VarChar, SoSeal);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ContainerDinhKem> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ContainerDinhKem item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ContainerDinhKem(long id, long chungTuDinhKem_ID, string soVanDon, string soContainer, string soSeal, string ghiChu)
		{
			KDT_VNACC_ContainerDinhKem entity = new KDT_VNACC_ContainerDinhKem();			
			entity.ID = id;
			entity.ChungTuDinhKem_ID = chungTuDinhKem_ID;
			entity.SoVanDon = soVanDon;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ChungTuDinhKem_ID", SqlDbType.BigInt, ChungTuDinhKem_ID);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.VarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.VarChar, SoSeal);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ContainerDinhKem> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ContainerDinhKem item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ContainerDinhKem(long id)
		{
			KDT_VNACC_ContainerDinhKem entity = new KDT_VNACC_ContainerDinhKem();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_ChungTuDinhKem_ID(long chungTuDinhKem_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_DeleteBy_ChungTuDinhKem_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungTuDinhKem_ID", SqlDbType.BigInt, chungTuDinhKem_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ContainerDinhKem_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ContainerDinhKem> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ContainerDinhKem item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}