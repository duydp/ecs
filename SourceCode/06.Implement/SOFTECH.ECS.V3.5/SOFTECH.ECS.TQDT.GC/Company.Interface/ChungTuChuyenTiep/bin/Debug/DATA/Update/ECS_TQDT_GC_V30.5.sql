  
GO
ALTER TABLE dbo.t_KDT_HangMauDich
ALTER COLUMN [MaPhu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
ALTER TABLE dbo.t_KDT_HangMauDich
ALTER COLUMN [TenHang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Insert]  
-- Database: ECS_TQDT_GC_V3.5  
-- Author: Ngo Thanh Tung  
-- Time created: Friday, December 14, 2012  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Insert]  
 @TKMD_ID bigint,  
 @SoThuTuHang int,  
 @MaHS varchar(12),  
 @MaPhu varchar(50),  
 @TenHang nvarchar(255),  
 @NuocXX_ID char(3),  
 @DVT_ID char(3),  
 @SoLuong numeric(18, 5),  
 @TrongLuong numeric(18, 5),  
 @DonGiaKB numeric(38, 15),  
 @DonGiaTT numeric(38, 15),  
 @TriGiaKB numeric(38, 15),  
 @TriGiaTT numeric(38, 15),  
 @TriGiaKB_VND numeric(38, 15),  
 @ThueSuatXNK numeric(5, 2),  
 @ThueSuatTTDB numeric(5, 2),  
 @ThueSuatGTGT numeric(5, 2),  
 @ThueXNK money,  
 @ThueTTDB money,  
 @ThueGTGT money,  
 @PhuThu money,  
 @TyLeThuKhac numeric(5, 2),  
 @TriGiaThuKhac money,  
 @MienThue tinyint,  
 @Ma_HTS varchar(50),  
 @DVT_HTS char(3),  
 @SoLuong_HTS numeric(18, 5),  
 @ThueSuatXNKGiam numeric(5, 2),  
 @ThueSuatTTDBGiam numeric(5, 2),  
 @ThueSuatVATGiam numeric(5, 2),  
 @MaHSMoRong nvarchar(12),  
 @FOC bit,  
 @NhanHieu nvarchar(256),  
 @QuyCachPhamChat nvarchar(1000),  
 @ThanhPhan nvarchar(100),  
 @Model nvarchar(35),  
 @TenHangSX nvarchar(256),  
 @MaHangSX nvarchar(30),  
 @ThueTuyetDoi bit,  
 @DonGiaTuyetDoi float,  
 @ThueBVMT money,  
 @ThueSuatBVMT numeric(5, 2),  
 @ThueSuatBVMTGiam numeric(5, 2),  
 @ThueChongPhaGia money,  
 @ThueSuatChongPhaGia numeric(5, 2),  
 @ThueSuatChongPhaGiaGiam numeric(5, 2),  
 @isHangCu bit,  
 @BieuThueXNK nvarchar(255),  
 @BieuThueTTDB nvarchar(255),  
 @BieuThueBVMT nvarchar(255),  
 @BieuThueGTGT nvarchar(255),  
 @BieuThueCBPG nvarchar(255),  
 @ThongTinKhac nvarchar(2000),  
 @ID bigint OUTPUT  
AS  
  
INSERT INTO [dbo].[t_KDT_HangMauDich]  
(  
 [TKMD_ID],  
 [SoThuTuHang],  
 [MaHS],  
 [MaPhu],  
 [TenHang],  
 [NuocXX_ID],  
 [DVT_ID],  
 [SoLuong],  
 [TrongLuong],  
 [DonGiaKB],  
 [DonGiaTT],  
 [TriGiaKB],  
 [TriGiaTT],  
 [TriGiaKB_VND],  
 [ThueSuatXNK],  
 [ThueSuatTTDB],  
 [ThueSuatGTGT],  
 [ThueXNK],  
 [ThueTTDB],  
 [ThueGTGT],  
 [PhuThu],  
 [TyLeThuKhac],  
 [TriGiaThuKhac],  
 [MienThue],  
 [Ma_HTS],  
 [DVT_HTS],  
 [SoLuong_HTS],  
 [ThueSuatXNKGiam],  
 [ThueSuatTTDBGiam],  
 [ThueSuatVATGiam],  
 [MaHSMoRong],  
 [FOC],  
 [NhanHieu],  
 [QuyCachPhamChat],  
 [ThanhPhan],  
 [Model],  
 [TenHangSX],  
 [MaHangSX],  
 [ThueTuyetDoi],  
 [DonGiaTuyetDoi],  
 [ThueBVMT],  
 [ThueSuatBVMT],  
 [ThueSuatBVMTGiam],  
 [ThueChongPhaGia],  
 [ThueSuatChongPhaGia],  
 [ThueSuatChongPhaGiaGiam],  
 [isHangCu],  
 [BieuThueXNK],  
 [BieuThueTTDB],  
 [BieuThueBVMT],  
 [BieuThueGTGT],  
 [BieuThueCBPG],  
 [ThongTinKhac]  
)  
VALUES   
(  
 @TKMD_ID,  
 @SoThuTuHang,  
 @MaHS,  
 @MaPhu,  
 @TenHang,  
 @NuocXX_ID,  
 @DVT_ID,  
 @SoLuong,  
 @TrongLuong,  
 @DonGiaKB,  
 @DonGiaTT,  
 @TriGiaKB,  
 @TriGiaTT,  
 @TriGiaKB_VND,  
 @ThueSuatXNK,  
 @ThueSuatTTDB,  
 @ThueSuatGTGT,  
 @ThueXNK,  
 @ThueTTDB,  
 @ThueGTGT,  
 @PhuThu,  
 @TyLeThuKhac,  
 @TriGiaThuKhac,  
 @MienThue,  
 @Ma_HTS,  
 @DVT_HTS,  
 @SoLuong_HTS,  
 @ThueSuatXNKGiam,  
 @ThueSuatTTDBGiam,  
 @ThueSuatVATGiam,  
 @MaHSMoRong,  
 @FOC,  
 @NhanHieu,  
 @QuyCachPhamChat,  
 @ThanhPhan,  
 @Model,  
 @TenHangSX,  
 @MaHangSX,  
 @ThueTuyetDoi,  
 @DonGiaTuyetDoi,  
 @ThueBVMT,  
 @ThueSuatBVMT,  
 @ThueSuatBVMTGiam,  
 @ThueChongPhaGia,  
 @ThueSuatChongPhaGia,  
 @ThueSuatChongPhaGiaGiam,  
 @isHangCu,  
 @BieuThueXNK,  
 @BieuThueTTDB,  
 @BieuThueBVMT,  
 @BieuThueGTGT,  
 @BieuThueCBPG,  
 @ThongTinKhac  
)  
  
SET @ID = SCOPE_IDENTITY()  
GO

  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]  
-- Database: ECS_TQDT_GC_V3.5  
-- Author: Ngo Thanh Tung  
-- Time created: Friday, December 14, 2012  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdate]  
 @ID bigint,  
 @TKMD_ID bigint,  
 @SoThuTuHang int,  
 @MaHS varchar(12),  
 @MaPhu varchar(50),  
 @TenHang nvarchar(255),  
 @NuocXX_ID char(3),  
 @DVT_ID char(3),  
 @SoLuong numeric(18, 5),  
 @TrongLuong numeric(18, 5),  
 @DonGiaKB numeric(38, 15),  
 @DonGiaTT numeric(38, 15),  
 @TriGiaKB numeric(38, 15),  
 @TriGiaTT numeric(38, 15),  
 @TriGiaKB_VND numeric(38, 15),  
 @ThueSuatXNK numeric(5, 2),  
 @ThueSuatTTDB numeric(5, 2),  
 @ThueSuatGTGT numeric(5, 2),  
 @ThueXNK money,  
 @ThueTTDB money,  
 @ThueGTGT money,  
 @PhuThu money,  
 @TyLeThuKhac numeric(5, 2),  
 @TriGiaThuKhac money,  
 @MienThue tinyint,  
 @Ma_HTS varchar(50),  
 @DVT_HTS char(3),  
 @SoLuong_HTS numeric(18, 5),  
 @ThueSuatXNKGiam numeric(5, 2),  
 @ThueSuatTTDBGiam numeric(5, 2),  
 @ThueSuatVATGiam numeric(5, 2),  
 @MaHSMoRong nvarchar(12),  
 @FOC bit,  
 @NhanHieu nvarchar(256),  
 @QuyCachPhamChat nvarchar(1000),  
 @ThanhPhan nvarchar(100),  
 @Model nvarchar(35),  
 @TenHangSX nvarchar(256),  
 @MaHangSX nvarchar(30),  
 @ThueTuyetDoi bit,  
 @DonGiaTuyetDoi float,  
 @ThueBVMT money,  
 @ThueSuatBVMT numeric(5, 2),  
 @ThueSuatBVMTGiam numeric(5, 2),  
 @ThueChongPhaGia money,  
 @ThueSuatChongPhaGia numeric(5, 2),  
 @ThueSuatChongPhaGiaGiam numeric(5, 2),  
 @isHangCu bit,  
 @BieuThueXNK nvarchar(255),  
 @BieuThueTTDB nvarchar(255),  
 @BieuThueBVMT nvarchar(255),  
 @BieuThueGTGT nvarchar(255),  
 @BieuThueCBPG nvarchar(255),  
 @ThongTinKhac nvarchar(2000)  
AS  
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich] WHERE [ID] = @ID)  
 BEGIN  
  UPDATE  
   [dbo].[t_KDT_HangMauDich]   
  SET  
   [TKMD_ID] = @TKMD_ID,  
   [SoThuTuHang] = @SoThuTuHang,  
   [MaHS] = @MaHS,  
   [MaPhu] = @MaPhu,  
   [TenHang] = @TenHang,  
   [NuocXX_ID] = @NuocXX_ID,  
   [DVT_ID] = @DVT_ID,  
   [SoLuong] = @SoLuong,  
   [TrongLuong] = @TrongLuong,  
   [DonGiaKB] = @DonGiaKB,  
   [DonGiaTT] = @DonGiaTT,  
   [TriGiaKB] = @TriGiaKB,  
   [TriGiaTT] = @TriGiaTT,  
   [TriGiaKB_VND] = @TriGiaKB_VND,  
   [ThueSuatXNK] = @ThueSuatXNK,  
   [ThueSuatTTDB] = @ThueSuatTTDB,  
   [ThueSuatGTGT] = @ThueSuatGTGT,  
   [ThueXNK] = @ThueXNK,  
   [ThueTTDB] = @ThueTTDB,  
   [ThueGTGT] = @ThueGTGT,  
   [PhuThu] = @PhuThu,  
   [TyLeThuKhac] = @TyLeThuKhac,  
   [TriGiaThuKhac] = @TriGiaThuKhac,  
   [MienThue] = @MienThue,  
   [Ma_HTS] = @Ma_HTS,  
   [DVT_HTS] = @DVT_HTS,  
   [SoLuong_HTS] = @SoLuong_HTS,  
   [ThueSuatXNKGiam] = @ThueSuatXNKGiam,  
   [ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,  
   [ThueSuatVATGiam] = @ThueSuatVATGiam,  
   [MaHSMoRong] = @MaHSMoRong,  
   [FOC] = @FOC,  
   [NhanHieu] = @NhanHieu,  
   [QuyCachPhamChat] = @QuyCachPhamChat,  
   [ThanhPhan] = @ThanhPhan,  
   [Model] = @Model,  
   [TenHangSX] = @TenHangSX,  
   [MaHangSX] = @MaHangSX,  
   [ThueTuyetDoi] = @ThueTuyetDoi,  
   [DonGiaTuyetDoi] = @DonGiaTuyetDoi,  
   [ThueBVMT] = @ThueBVMT,  
   [ThueSuatBVMT] = @ThueSuatBVMT,  
   [ThueSuatBVMTGiam] = @ThueSuatBVMTGiam,  
   [ThueChongPhaGia] = @ThueChongPhaGia,  
   [ThueSuatChongPhaGia] = @ThueSuatChongPhaGia,  
   [ThueSuatChongPhaGiaGiam] = @ThueSuatChongPhaGiaGiam,  
   [isHangCu] = @isHangCu,  
   [BieuThueXNK] = @BieuThueXNK,  
   [BieuThueTTDB] = @BieuThueTTDB,  
   [BieuThueBVMT] = @BieuThueBVMT,  
   [BieuThueGTGT] = @BieuThueGTGT,  
   [BieuThueCBPG] = @BieuThueCBPG,  
   [ThongTinKhac] = @ThongTinKhac  
  WHERE  
   [ID] = @ID  
 END  
ELSE  
 BEGIN  
    
  INSERT INTO [dbo].[t_KDT_HangMauDich]  
  (  
   [TKMD_ID],  
   [SoThuTuHang],  
   [MaHS],  
   [MaPhu],  
   [TenHang],  
   [NuocXX_ID],  
   [DVT_ID],  
   [SoLuong],  
   [TrongLuong],  
   [DonGiaKB],     [DonGiaTT],  
   [TriGiaKB],  
   [TriGiaTT],  
   [TriGiaKB_VND],  
   [ThueSuatXNK],  
   [ThueSuatTTDB],  
   [ThueSuatGTGT],  
   [ThueXNK],  
   [ThueTTDB],  
   [ThueGTGT],  
   [PhuThu],  
   [TyLeThuKhac],  
   [TriGiaThuKhac],  
   [MienThue],  
   [Ma_HTS],  
   [DVT_HTS],  
   [SoLuong_HTS],  
   [ThueSuatXNKGiam],  
   [ThueSuatTTDBGiam],  
   [ThueSuatVATGiam],  
   [MaHSMoRong],  
   [FOC],  
   [NhanHieu],  
   [QuyCachPhamChat],  
   [ThanhPhan],  
   [Model],  
   [TenHangSX],  
   [MaHangSX],  
   [ThueTuyetDoi],  
   [DonGiaTuyetDoi],  
   [ThueBVMT],  
   [ThueSuatBVMT],  
   [ThueSuatBVMTGiam],  
   [ThueChongPhaGia],  
   [ThueSuatChongPhaGia],  
   [ThueSuatChongPhaGiaGiam],  
   [isHangCu],  
   [BieuThueXNK],  
   [BieuThueTTDB],  
   [BieuThueBVMT],  
   [BieuThueGTGT],  
   [BieuThueCBPG],  
   [ThongTinKhac]  
  )  
  VALUES   
  (  
   @TKMD_ID,  
   @SoThuTuHang,  
   @MaHS,  
   @MaPhu,  
   @TenHang,  
   @NuocXX_ID,  
   @DVT_ID,  
   @SoLuong,  
   @TrongLuong,  
   @DonGiaKB,  
   @DonGiaTT,  
   @TriGiaKB,  
   @TriGiaTT,  
   @TriGiaKB_VND,  
   @ThueSuatXNK,  
   @ThueSuatTTDB,  
   @ThueSuatGTGT,  
   @ThueXNK,  
   @ThueTTDB,  
   @ThueGTGT,  
   @PhuThu,  
   @TyLeThuKhac,  
   @TriGiaThuKhac,  
   @MienThue,  
   @Ma_HTS,  
   @DVT_HTS,  
   @SoLuong_HTS,  
   @ThueSuatXNKGiam,  
   @ThueSuatTTDBGiam,  
   @ThueSuatVATGiam,  
   @MaHSMoRong,  
   @FOC,  
   @NhanHieu,  
   @QuyCachPhamChat,  
   @ThanhPhan,  
   @Model,  
   @TenHangSX,  
   @MaHangSX,  
   @ThueTuyetDoi,  
   @DonGiaTuyetDoi,  
   @ThueBVMT,  
   @ThueSuatBVMT,  
   @ThueSuatBVMTGiam,  
   @ThueChongPhaGia,  
   @ThueSuatChongPhaGia,  
   @ThueSuatChongPhaGiaGiam,  
   @isHangCu,  
   @BieuThueXNK,  
   @BieuThueTTDB,  
   @BieuThueBVMT,  
   @BieuThueGTGT,  
   @BieuThueCBPG,  
   @ThongTinKhac  
  )    
 END
 GO
 
   
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Update]  
-- Database: ECS_TQDT_GC_V3.5  
-- Author: Ngo Thanh Tung  
-- Time created: Friday, December 14, 2012  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Update]  
 @ID bigint,  
 @TKMD_ID bigint,  
 @SoThuTuHang int,  
 @MaHS varchar(12),  
 @MaPhu varchar(50),  
 @TenHang nvarchar(255),  
 @NuocXX_ID char(3),  
 @DVT_ID char(3),  
 @SoLuong numeric(18, 5),  
 @TrongLuong numeric(18, 5),  
 @DonGiaKB numeric(38, 15),  
 @DonGiaTT numeric(38, 15),  
 @TriGiaKB numeric(38, 15),  
 @TriGiaTT numeric(38, 15),  
 @TriGiaKB_VND numeric(38, 15),  
 @ThueSuatXNK numeric(5, 2),  
 @ThueSuatTTDB numeric(5, 2),  
 @ThueSuatGTGT numeric(5, 2),  
 @ThueXNK money,  
 @ThueTTDB money,  
 @ThueGTGT money,  
 @PhuThu money,  
 @TyLeThuKhac numeric(5, 2),  
 @TriGiaThuKhac money,  
 @MienThue tinyint,  
 @Ma_HTS varchar(50),  
 @DVT_HTS char(3),  
 @SoLuong_HTS numeric(18, 5),  
 @ThueSuatXNKGiam numeric(5, 2),  
 @ThueSuatTTDBGiam numeric(5, 2),  
 @ThueSuatVATGiam numeric(5, 2),  
 @MaHSMoRong nvarchar(12),  
 @FOC bit,  
 @NhanHieu nvarchar(256),  
 @QuyCachPhamChat nvarchar(1000),  
 @ThanhPhan nvarchar(100),  
 @Model nvarchar(35),  
 @TenHangSX nvarchar(256),  
 @MaHangSX nvarchar(30),  
 @ThueTuyetDoi bit,  
 @DonGiaTuyetDoi float,  
 @ThueBVMT money,  
 @ThueSuatBVMT numeric(5, 2),  
 @ThueSuatBVMTGiam numeric(5, 2),  
 @ThueChongPhaGia money,  
 @ThueSuatChongPhaGia numeric(5, 2),  
 @ThueSuatChongPhaGiaGiam numeric(5, 2),  
 @isHangCu bit,  
 @BieuThueXNK nvarchar(255),  
 @BieuThueTTDB nvarchar(255),  
 @BieuThueBVMT nvarchar(255),  
 @BieuThueGTGT nvarchar(255),  
 @BieuThueCBPG nvarchar(255),  
 @ThongTinKhac nvarchar(2000)  
AS  
  
UPDATE  
 [dbo].[t_KDT_HangMauDich]  
SET  
 [TKMD_ID] = @TKMD_ID,  
 [SoThuTuHang] = @SoThuTuHang,  
 [MaHS] = @MaHS,  
 [MaPhu] = @MaPhu,  
 [TenHang] = @TenHang,  
 [NuocXX_ID] = @NuocXX_ID,  
 [DVT_ID] = @DVT_ID,  
 [SoLuong] = @SoLuong,  
 [TrongLuong] = @TrongLuong,  
 [DonGiaKB] = @DonGiaKB,  
 [DonGiaTT] = @DonGiaTT,  
 [TriGiaKB] = @TriGiaKB,  
 [TriGiaTT] = @TriGiaTT,  
 [TriGiaKB_VND] = @TriGiaKB_VND,  
 [ThueSuatXNK] = @ThueSuatXNK,  
 [ThueSuatTTDB] = @ThueSuatTTDB,  
 [ThueSuatGTGT] = @ThueSuatGTGT,  
 [ThueXNK] = @ThueXNK,  
 [ThueTTDB] = @ThueTTDB,  
 [ThueGTGT] = @ThueGTGT,  
 [PhuThu] = @PhuThu,  
 [TyLeThuKhac] = @TyLeThuKhac,  
 [TriGiaThuKhac] = @TriGiaThuKhac,  
 [MienThue] = @MienThue,  
 [Ma_HTS] = @Ma_HTS,  
 [DVT_HTS] = @DVT_HTS,  
 [SoLuong_HTS] = @SoLuong_HTS,  
 [ThueSuatXNKGiam] = @ThueSuatXNKGiam,  
 [ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,  
 [ThueSuatVATGiam] = @ThueSuatVATGiam,  
 [MaHSMoRong] = @MaHSMoRong,  
 [FOC] = @FOC,  
 [NhanHieu] = @NhanHieu,  
 [QuyCachPhamChat] = @QuyCachPhamChat,  
 [ThanhPhan] = @ThanhPhan,  
 [Model] = @Model,  
 [TenHangSX] = @TenHangSX,  
 [MaHangSX] = @MaHangSX,  
 [ThueTuyetDoi] = @ThueTuyetDoi,  
 [DonGiaTuyetDoi] = @DonGiaTuyetDoi,  
 [ThueBVMT] = @ThueBVMT,  
 [ThueSuatBVMT] = @ThueSuatBVMT,  
 [ThueSuatBVMTGiam] = @ThueSuatBVMTGiam,  
 [ThueChongPhaGia] = @ThueChongPhaGia,  
 [ThueSuatChongPhaGia] = @ThueSuatChongPhaGia,  
 [ThueSuatChongPhaGiaGiam] = @ThueSuatChongPhaGiaGiam,  
 [isHangCu] = @isHangCu,  
 [BieuThueXNK] = @BieuThueXNK,  
 [BieuThueTTDB] = @BieuThueTTDB,  
 [BieuThueBVMT] = @BieuThueBVMT,  
 [BieuThueGTGT] = @BieuThueGTGT,  
 [BieuThueCBPG] = @BieuThueCBPG,  
 [ThongTinKhac] = @ThongTinKhac  
WHERE  
 [ID] = @ID  
  
GO
IF OBJECT_ID(N'dbo.p_KDT_ToKhaiMauDich_CheckDuplicateTK') IS NOT NULL
	DROP PROCEDURE  dbo.p_KDT_ToKhaiMauDich_CheckDuplicateTK   
GO
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_CheckDuplicateTK]  
 @HopDong_ID bigint  
AS  
  
SELECT  [LoaiVanDon], [MaLoaiHinh], [NamDK], [MaHaiQuan], [NgayDangKy]  
FROM  
 [dbo].[t_KDT_ToKhaiMauDich]  
WHERE  
 [IDHopDong] = @HopDong_ID  
GROUP BY [LoaiVanDon], [MaLoaiHinh], [NamDK], [MaHaiQuan], [NgayDangKy]
HAVING COUNT(*) > 1  
GO

IF OBJECT_ID(N'dbo.p_KDT_ToKhaiChuyenTiep_CheckDuplicateTK') IS NOT NULL
	DROP PROCEDURE  dbo.p_KDT_ToKhaiChuyenTiep_CheckDuplicateTK   
GO
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiep_CheckDuplicateTK]  
 @HopDong_ID bigint  
AS  
  
SELECT [Huongdan_PL], [MaLoaiHinh], [NamDK], MaHaiQuanTiepNhan, [NgayDangKy]  
FROM  
 [dbo].[t_KDT_GC_ToKhaiChuyenTiep]  
WHERE  
 [IDHopDong] = @HopDong_ID  
GROUP BY [Huongdan_PL], [MaLoaiHinh], [NamDK], MaHaiQuanTiepNhan, [NgayDangKy]  
HAVING COUNT(*) > 1  

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.5',GETDATE(), N'CẬP NHẬT ALTER TABLE T_KDT_HANGMAUDICH VÀ KIỂM TRA TỜ KHAI TRÙNG CỦA HĐ')
END

GO