﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.SqlClient;

namespace KeyServer.User
{
    public partial class User
    {
        private Permission _permission = new Permission();
        public Permission Permission 
        {
            set { this._permission = value; } 
            get { return this._permission; } 
        }

        public void LoadPermission()
        {
            if (this.ID > 0)
            {
                List<Permission> lisper = Permission.SelectCollectionDynamic("Master_ID = " + ID.ToString(),null);
                if (lisper == null || lisper.Count == 0)
                    this.Permission = new Permission();
                else
                    this.Permission = lisper[0];
            }
        }
        public static bool IsExit(string UserName)
        {
            List<User> listUser = User.SelectCollectionDynamic("UserName = '" + UserName + "'", null);
            if (listUser == null || listUser.Count == 0)
                return false;
            else
                return true;
        }
        public bool IsLogin(string UserName, string password)
        {
            try
            {
                List<User> listUser = User.SelectCollectionDynamic("UserName = '" + UserName + "' AND PassWord = '" + password + "'", null);
                if (listUser == null || listUser.Count == 0)
                    return false;
                else if (listUser.Count == 1)
                {
                    this.ID = listUser[0].ID;
                    this.FirstName = listUser[0].FirstName;
                    this.LastName = listUser[0].LastName;
                    this.UserName = listUser[0].UserName;
                    this.PassWord = listUser[0].PassWord;
                    this.Department = listUser[0].Department;
                    this.position = listUser[0].position;
                    this.Title = listUser[0].Title;
                    this.Email = listUser[0].Email;
                    LoadPermission();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public void InsertFull()
        {
             SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
             using (SqlConnection connection = (SqlConnection)db.CreateConnection())
             {
                 connection.Open();
                 SqlTransaction transaction = connection.BeginTransaction();
                 try
                 {
                     if (this.ID == 0)
                     {
                         this.ID = this.Insert(transaction);
                         this.Permission.Master_ID = this.ID;
                         this.Permission.Insert(transaction);
                     }
                     transaction.Commit();
                 }
                 catch
                 {
                     transaction.Rollback();

                 }
                 finally
                 {
                     connection.Close();
                 }
             }
        }
        public static List<User> LoadFull()
        {
            List<User> _listUser = User.SelectCollectionDynamic("1=1","ID");
            List<Permission> _listPermission = Permission.SelectCollectionAll();
            foreach (Permission item in _listPermission)
            {
                foreach (User userItem in _listUser)
                {
                    if (userItem.ID == item.Master_ID)
                    {
                        userItem.Permission = item;
                        break;
                    }
                }
            }
            return _listUser;
        }
        public static List<User> LoadUserByPermission(int PermissionID)
        {
            const string spName = "[dbo].p_LM_User_LoadByPermission";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@PermissionID", SqlDbType.Int, PermissionID);
            List<User> collection = new List<User>();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                User entity = new User();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("FirstName"))) entity.FirstName = reader.GetString(reader.GetOrdinal("FirstName"));
                if (!reader.IsDBNull(reader.GetOrdinal("LastName"))) entity.LastName = reader.GetString(reader.GetOrdinal("LastName"));
                if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));
                if (!reader.IsDBNull(reader.GetOrdinal("PassWord"))) entity.PassWord = reader.GetString(reader.GetOrdinal("PassWord"));
                if (!reader.IsDBNull(reader.GetOrdinal("Department"))) entity.Department = reader.GetString(reader.GetOrdinal("Department"));
                if (!reader.IsDBNull(reader.GetOrdinal("position"))) entity.position = reader.GetString(reader.GetOrdinal("position"));
                if (!reader.IsDBNull(reader.GetOrdinal("Title"))) entity.Title = reader.GetString(reader.GetOrdinal("Title"));
                if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;			
        }
    }
}
