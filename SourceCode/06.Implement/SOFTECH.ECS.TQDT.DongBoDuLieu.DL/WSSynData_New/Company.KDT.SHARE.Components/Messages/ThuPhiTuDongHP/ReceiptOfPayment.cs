﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class ReceiptOfPayment
    {
        /// <summary>
        /// Thông tin biên lai
        /// </summary>
        /// 
       [XmlElement("Receipt")]
       public Receipt Receipt { get; set; }

       /// <summary>
       /// Thông tin khách hàng
       /// </summary>
       /// 
       [XmlElement("Partner")]
       public Partner Partner { get; set; }


       /// <summary>
       /// Thông tin tờ khai
       /// </summary>
       /// 
       [XmlElement("CustomsReference")]
       public CustomsReference CustomsReference { get; set; }

       /// <summary>
       /// Thông tin ghi chú
       /// </summary>
       /// 
       [XmlElement("AdditionalInformation")]
       public AdditionalInformation AdditionalInformation { get; set; }

       /// <summary>
       /// Thông tin các khoản mục phí
       /// </summary>
       /// 
       [XmlElement("CustomsGoodsItems")]
       public CustomsGoodsItems CustomsGoodsItems { get; set; }
    }
}
