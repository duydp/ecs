﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.Interface.KDT.GC;
using Janus.Windows.ExplorerBar;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.LookAndFeel;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report;
using Company.Interface.DanhMucChuan;
using System.IO;
using System.Xml.Serialization;
using Company.QuanTri;
using Company.Interface.QuanTri;
using Company.Interface.GC;
using System.Globalization;
using System.Threading;
using System.Resources;
using Company.Interface.PhongKhai;
using System.Xml;
using Company.KDT.SHARE.Components.Utils;
using System.Collections.Generic;
using HopDong = Company.GC.BLL.KDT.GC.HopDong;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;
using Company.Interface.DongBoDuLieu;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using Company.Interface.VNACCS.PhieuKho;
using Company.Interface.GC.PhanBo;
using Company.Interface.VNACCS.Vouchers;
using IWshRuntimeLibrary;
using Company.Interface.VNACCS.WareHouse;
using Company.Interface.KDT;
using Company.Interface.VNACCS.ExportTotalData;
using Company.Interface.VNACCS;
using Company.Interface.GC.WareHouse;
using Company.Interface.ProdInfo;
using System.Drawing;
using Company.Interface.CauHinh;
using Company.Interface.VNACCS.TollManagement;
namespace Company.Interface
{
    public partial class MainForm : BaseForm
    {
        //TODO: VNACCS---------------------------------------------------------------------------------------------
        public static bool isUseSettingVNACC = true;
        public ThongBaoFormVNACCS frmThongBao = new ThongBaoFormVNACCS();
        public TrangThaiPhanHoiForm frmTrangThaiPhanHoi = new TrangThaiPhanHoiForm();

        public static int flag = 0;
        public static bool isLoginSuccess = false;
        HtmlDocument docHTML = null;
        WebBrowser wbManin = new WebBrowser();
        public static int versionHD = 0;
        public static int typeLogin = 0;
        private int soLanThucHien = 0;
        public int soLanLayTyGia = 0;
        private FrmQuerySQLUpdate frmQuery;
        private Company.Interface.VNACCS.QuanLyMessageForm quanLyMsg = new Company.Interface.VNACCS.QuanLyMessageForm();
        #region SXXK
        private static QueueForm queueForm = new QueueForm();

        // Tờ khai mậu dịch.
        private ToKhaiMauDichForm tkmdForm;
        private ToKhaiMauDichManageForm tkmdManageForm;
        Company.Interface.GC.ToKhaiMauDichRegistedForm tkmdRegisterForm = new Company.Interface.GC.ToKhaiMauDichRegistedForm();

        #endregion

        //-----------------------------------------------------------------------------------------------

        #region GiaCong
        // Hợp đồng.
        private HopDongRegistedForm hdgcRegistedForm;
        private HopDongManageForm hdgcManageForm;
        private HopDongEditForm hdgcForm;

        //Định mức
        private DinhMucGCManageForm dmgcManageForm;
        private DinhMucGCSendForm dmgcSendForm;
        private DinhMucGCRegistedForm dmgcRegistedForm;

        //Phụ kiện
        private PhuKienGCSendForm pkgcSendForm;
        private PhuKienGCManageForm pkgcManageForm;
        private PhuKienGCRegistedForm pkgcRegistedForm;

        //tờ khai phiếu chuyển tiếp gia công
        private ToKhaiGCChuyenTiepNhapForm tkgcCTNhapForm;
        private ToKhaiGCCTManagerForm theodoiTKGCCTForm;
        private ToKhaiGCCTRegistedForm tkctRegistedForm;

        //Bảng kê NPL cung ứng
        private NPLCungUngTheoTKSendForm nplCUSendForm;
        private NPLCungUngManageForm nplCUSTheoDoiForm;
        private NPLCungUngRegistedForm nplCURegisredForm;

        //Gia hạn thanh khoản

        private GiaHanHopDongSendForm ghHopDongTKSendForm;
        private GiaHanHopDongManageForm ghHopDongManageForm;
        //Đề nghị giám sát tiêu hủy

        private GiamSatThieuHuyGCSendForm gsTieuHuySendForm;
        GiamSatThieuHuyGCManageForm gsTieuHuyManagerForm;
        // Thanh khoản
        ThanhKhoanHDGCSendForm thanhKhoanSendForm;
        ThanhKhoanManageForm thanhKhoanManagerForm;
        //Phan bo to khai 
        private ThongkeHangTonForm tkHangTon;
        //Npl Cung ung
        private CungUngManagerForm cungUngManager;
        private CungUngForm cungUngFrm;
        //Tai Xuat
        private TaiXuatManagerForm taiXuatManager;
        private TaiXuatForm taiXuatFrm;
        #endregion

        #region Du lieu Chuan
        private MaHSForm maHSForm;
        private PTTTForm ptttForm;
        private PTVTForm ptvtForm;
        private DonViTinhForm dvtForm;
        private DonViHaiQuanForm dvhqForm;
        private CuaKhauForm ckForm;
        private NguyenTeForm ntForm;
        private DKGHForm dkghForm;
        private NuocForm nuocForm;

        private FrmDongBoDuLieu DBDLForm;
        private FrmDongBoDuLieu_VNACCS DBDLForm_VNACCS;
        private SyncDataForm dongBoForm;
        #endregion

        #region Quản trị
        public static ECSPrincipal EcsQuanTri;
        QuanLyNguoiDung NguoiDung;
        QuanLyNhomNguoiDung NhomNguoiDung;
        #endregion Quản trị

        #region Hien thi ngay thang nam, gio

        private System.Windows.Forms.Timer timerDateTime;

        private void SetTimerDateTimeStatus()
        {
            timerDateTime = new System.Windows.Forms.Timer();
            timerDateTime.Interval = 1000;
            timerDateTime.Tick += new EventHandler(timerDateTime_Tick);

            timerDateTime.Enabled = true;
            timerDateTime.Start();
            statusBar.Panels["DateTime"].AutoSize = StatusBarPanelAutoSize.Contents;

            statusBar.ImageList = ilSmall;
            statusBar.Panels["DateTime"].ImageIndex = 34;

            timer1 = new System.Windows.Forms.Timer();
            timer1.Interval = 1000;
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Enabled = true;
            timer1.Start();
            statusBar.Panels["Support"].AutoSize = StatusBarPanelAutoSize.Spring;

        }

        void timerDateTime_Tick(object sender, EventArgs e)
        {
            statusBar.Panels["DateTime"].ToolTipText = statusBar.Panels["DateTime"].Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt");
        }

        #endregion

        QuanLyMessage qlMess;
        private void khoitao_GiaTriMacDinh()
        {
            try
            {
                GlobalSettings.KhoiTao_GiaTriMacDinh();

                statusBar.Panels["DoanhNghiep"].AutoSize = StatusBarPanelAutoSize.Spring;
                statusBar.Panels["HaiQuan"].AutoSize = StatusBarPanelAutoSize.Contents;
                statusBar.Panels["HaiQuan"].Width = 250;
                statusBar.Panels["Service"].AutoSize = StatusBarPanelAutoSize.None;
                statusBar.Panels["Service"].Width = 250;
                statusBar.Panels["Terminal"].AutoSize = StatusBarPanelAutoSize.Contents;

                if (versionHD == 0)
                {
                    string statusStr1 = setText(string.Format("Mã/ tên người khai HQ: {0}/ {1}", ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.HO_TEN) + " - " + string.Format("Mã/ tên Doanh nghiệp: {0}/ {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI), "User: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Company : {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI));
                    //string statusStr2 = setText(string.Format("Hải quan tiếp nhận: {0}/ {1}.", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN), string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN));
                    statusBar.Panels["HaiQuan"].Text = setText(string.Format("Hải quan tiếp nhận: {0}", GlobalSettings.MA_HAI_QUAN), string.Format("Customs : {0}", GlobalSettings.MA_HAI_QUAN));
                    string statusStr3 = string.Format("Service khai báo: {0}", GlobalSettings.DiaChiWS);
                    statusBar.Panels["DoanhNghiep"].ToolTipText = statusBar.Panels["DoanhNghiep"].Text = statusStr1;
                    statusBar.Panels["HaiQuan"].ToolTipText = setText(string.Format("Hải quan tiếp nhận: {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN), string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN));
                    statusBar.Panels["Service"].ToolTipText = statusBar.Panels["Service"].Text = statusStr3;
                    statusBar.Panels["Terminal"].ToolTipText = statusBar.Panels["Terminal"].Text = string.Format("Terminal ID: {0}", GlobalVNACC.TerminalID != null && GlobalVNACC.TerminalID != "" ? GlobalVNACC.TerminalID : "?");
                }
                else
                {
                    string diaChiKhaibao = "http://" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(GlobalSettings.MA_DON_VI, "DIA_CHI_HQ").Value_Config.ToString() +
                                        "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(GlobalSettings.MA_DON_VI, "TEN_DICH_VU").Value_Config.ToString();
                    //this.Text = "Hệ thống hỗ trợ khai báo từ xa loại hình Kinh Doanh - Đầu Tư version " + Application.ProductVersion;
                    statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                    //statusBar.Panels["HaiQuan"].Text = statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Hải quan tiếp nhận: {0}/ {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN);
                    statusBar.Panels["HaiQuan"].Text = string.Format("Hải quan tiếp nhận: {0}", GlobalSettings.MA_HAI_QUAN);
                    statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Hải quan tiếp nhận: {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN);
                    statusBar.Panels["Service"].Text = statusBar.Panels["Service"].ToolTipText = string.Format("Service khai báo: {0}", diaChiKhaibao);
                    statusBar.Panels["Terminal"].ToolTipText = statusBar.Panels["Terminal"].Text = string.Format("Terminal ID: {0}", GlobalVNACC.TerminalID != null && GlobalVNACC.TerminalID != "" ? GlobalVNACC.TerminalID : "?");
                }

                statusBar.Panels["Terminal"].FormatStyle.ForeColor = GlobalVNACC.TerminalID != null && GlobalVNACC.TerminalID != "" ? System.Drawing.Color.Blue : System.Drawing.Color.Red;


                //TODO: Hungtq updaetd 7/2/2013. Khoitao_giatriMacDinh()
                string Version = "V?";
                bool CKS = false;

                CKS = Company.KDT.SHARE.Components.Globals.IsSignOnLan || Company.KDT.SHARE.Components.Globals.IsSignRemote || Company.KDT.SHARE.Components.Globals.IsSignature;

                if (Company.KDT.SHARE.Components.Globals.IsKTX)
                    Version = string.Format("V{0}", 1);
                else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                    Version = string.Format("V{0}", 2);
                else if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00" && GlobalSettings.SendV4 == false)
                    Version = string.Format("V{0}", 3);
                else if (GlobalSettings.SendV4) //Mac dinh V4
                    Version = string.Format("V{0}", 4);
                if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS) //khai báo VNACCS
                    Version = string.Format("V{0}", 5);

                statusBar.Panels["Version"].ToolTipText = statusBar.Panels["Version"].Text = Version;
                statusBar.ImageList = ilSmall;
                statusBar.Panels["CKS"].ToolTipText = (CKS ? "CKS" : "");
                statusBar.Panels["CKS"].Text = "";
                statusBar.Panels["CKS"].ImageIndex = (CKS ? 33 : -1);

                //statusBar.Panels["DoanhNghiep"].ImageIndex = 37;
                statusBar.Panels["HaiQuan"].ImageIndex = 35;
                statusBar.Panels["Terminal"].ImageIndex = 36;
                //statusBar.Panels["Service"].ImageIndex = 63;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void LoadDongBoDuLieuDN()
        {
            LoadDongBoDuLieuForm dongboDNForm = new LoadDongBoDuLieuForm();
            dongboDNForm.ShowDialog();
        }
        public static void LoadECSPrincipal(long id)
        {
            EcsQuanTri = new ECSPrincipal(id);
        }
        public static void ShowQueueForm()
        {
            queueForm.Show();
        }
        public static void AddToQueueForm(HangDoi hd)
        {
            queueForm.HDCollection.Add(hd);
            queueForm.RefreshQueue();
        }
        void wbManin_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                //docHTML = wbManin.Document;
                //HtmlElement itemTyGia = null;
                //foreach (HtmlElement item in docHTML.GetElementsByTagName("table"))
                //{
                //    if (item.GetAttribute("id") == "AutoNumber6")
                //    {
                //        itemTyGia = item;
                //        break;
                //    }
                //}
                //if (itemTyGia == null)
                //{
                //    MainForm.flag = 0;
                //    return;
                //}
                //int i = 1;
                //string stUSD = "";
                //try
                //{
                //    foreach (HtmlElement itemTD in docHTML.GetElementsByTagName("TD"))
                //    {
                //        if (itemTD.InnerText != null)
                //        {

                //        }
                //        if (itemTD.InnerText != null && itemTD.InnerText.IndexOf("1 USD=") > -1)
                //        {
                //            stUSD = itemTD.InnerText.Trim();
                //        }
                //    }
                //    if (stUSD != "")
                //    {
                //        stUSD = stUSD.Substring(6).Trim();
                //        stUSD = stUSD.Substring(0, stUSD.Length - 3).Trim();
                //        Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.Update(null, "USD", Convert.ToDecimal(stUSD));

                //    }
                //}
                //catch { }
                //XmlDocument doc = new XmlDocument();
                //XmlElement root = doc.CreateElement("Root");
                //doc.AppendChild(root);
                //foreach (HtmlElement itemTD in itemTyGia.GetElementsByTagName("TR"))
                //{
                //    if (i == 1)
                //    {
                //        i++;
                //        continue;
                //    }
                //    HtmlElementCollection collection = itemTD.GetElementsByTagName("TD");
                //    XmlElement itemNT = doc.CreateElement("NgoaiTe");
                //    XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
                //    itemMaNT.InnerText = collection[1].InnerText;

                //    XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
                //    itemTenNT.InnerText = collection[2].InnerText;

                //    XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
                //    itemTyNT.InnerText = collection[3].InnerText;

                //    itemNT.AppendChild(itemMaNT);
                //    itemNT.AppendChild(itemTenNT);
                //    itemNT.AppendChild(itemTyNT);
                //    root.AppendChild(itemNT);
                //}
                //Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.UpdateTyGia(root);
                //doc.Save("TyGia.xml");
                //timer1.Enabled = false;
            }
            catch { MainForm.flag = 0; }
            //timer1.Enabled = false;
        }
        public MainForm()
        {
            //try
            //{
            //    Company.KDT.SHARE.Components.Globals.CreateShortcut(
            //        AppDomain.CurrentDomain.BaseDirectory + "SOFTECH.ECS.TQDT.GC.exe",
            //        Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\SOFTECH.ECS.TQDT.GC v5.0 - VNACCS.lnk",
            //        "", null, "", AppDomain.CurrentDomain.BaseDirectory, "");
            //}

            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage("Lỗi tạo tự động shortcut chương trình.", ex);
            //}

            InitializeComponent();
            wbManin.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wbManin_DocumentCompleted);

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            //TODO: HUNGTQ updated 02/08/2012.
            CreateQuerySQLCommand();

            SetTimerDateTimeStatus();

            //statusBar.PanelClick += new Janus.Windows.UI.StatusBar.StatusBarEventHandler(statusBar_PanelClick);
            //statusBar.MouseHover += new EventHandler(statusBar_MouseHover);
            //statusBar.MouseLeave += new EventHandler(statusBar_MouseLeave);
        }
        private void ShowLoginForm()
        {
            // this.Hide();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                //Company.GC.BLL.GC.NguyenPhuLieu abc = new Company.GC.BLL.GC.NguyenPhuLieu();
                //abc.HopDong_ID = 507;
                //abc.Ma = "2";

                //Hungtq complemented 14/12/2010
                //timer1.Enabled = false; //Disable timer get Exchange Rate

                this.explorerBarVNACCS_TKMD.Groups[8].Visible = GlobalSettings.MA_DON_VI == "0400101556";

                bool installCASuccess = Company.KDT.SHARE.VNACCS.ImportCertNet.InstallCA();
                string cer = installCASuccess ? " - Certified" : "- Not certified";

                string strVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                string dateRelease = Company.KDT.SHARE.Components.DownloadUpdate.GetDateRelease();
                this.Text += " - Build " + strVersion + (dateRelease != "" ? string.Format(" ({0})", dateRelease) : "") + cer;
                this.Text += " - SĐT HỖ TRỢ : (0236) 3.840.888 . ĐỊA CHỈ EMAIL : ecs@softech.vn ";
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(Application.StartupPath + "\\Config.xml");
                    XmlNode node = doc.SelectSingleNode("Root/Type");
                    MainForm.versionHD = Convert.ToInt32(node.InnerText);
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }


                if (MainForm.versionHD == 2)
                {

                    cmdNhapDuLieuTuDoangNghiep.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdXuatDuLieuPhongKHai.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdDongBoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdXuatDN.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhapXuat.Visible = Janus.Windows.UI.InheritableBoolean.False;

                }
                else
                {
                    cmdXuatDuLieuPhongKHai.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdNhapDuLieuTuDoangNghiep.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdDongBoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdXuatDN.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdNhapXuat.Visible = Janus.Windows.UI.InheritableBoolean.True;
                }
                try
                {
                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        this.BackgroundImage = System.Drawing.Image.FromFile("ecse.jpg");

                    }
                    else
                    {
                        this.BackgroundImage = System.Drawing.Image.FromFile("ecsv.png");
                        this.BackgroundImageLayout = ImageLayout.Stretch;
                    }
                    this.BackgroundImageLayout = ImageLayout.Stretch;
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

                this.Hide();

                if (versionHD == 0)
                {
                    Login login = new Login();
                    login.ShowDialog();
                }
                else if (versionHD == 1)
                {
                    ThongTinDaiLy DLinfo = Company.KDT.SHARE.Components.HeThongPhongKhai.GetThongTinDL();
                    if (DLinfo == null)
                    {
                        Company.Interface.TTDaiLy.FrmInfo finfo = new Company.Interface.TTDaiLy.FrmInfo();
                        finfo.ShowDialog(this);
                        while (!finfo.isSuccess)
                        {
                            if (ShowMessage("Bạn chưa nhập thông tin đại lý. Vui lòng nhập đầy đủ thông tin đại lý khai báo trước khi sử dụng chương trình. Xin cảm ơn!\r\n\r\n Nhập lại thông tin đại lý ?", true) == "Yes")
                            {
                                finfo = new Company.Interface.TTDaiLy.FrmInfo();
                                finfo.ShowDialog(this);
                            }
                            else
                            {
                                Application.ExitThread();
                                Application.Exit();
                                return;
                            }
                        }
                    }
                    else
                    {
                        GlobalSettings.MA_DAI_LY = DLinfo.MaDL;
                        GlobalSettings.TEN_DAI_LY = DLinfo.TenDL;

                    }
                    Company.Interface.DaiLy.Login login = new Company.Interface.DaiLy.Login();
                    login.ShowDialog();
                    GlobalSettings.IsDaiLy = true;
                }
                else if (versionHD == 2)
                {
                    Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                    login.ShowDialog();
                }
                if (isLoginSuccess)
                {
                    this.Show();

                    //Hungtq updated 21/12/2011. Cau hinh Ecs Express
                    Express();

                    this.khoitao_GiaTriMacDinh();

                    //Hungtq updated 11/12/2012.
                    Company.KDT.SHARE.Components.Globals.KhoiTao_DanhMucChuanHQ();

                    if (versionHD == 0)
                    {
                        if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem_GC.Management)))
                        {
                            QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                            //ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //cmbMenu.Commands[0].Commands[0].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //cmbMenu.Commands[0].Commands[1].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //cmbMenu.Commands[0].Commands[2].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[0].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[1].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[2].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[3].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                    }
                    else if (versionHD == 2)
                    {
                        switch (typeLogin)
                        {
                            case 1: // User Da Cau Hinh
                                this.Show();
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 2: // User Chua Cau Hinh
                                this.Show();
                                DangKyForm dangKyForm = new DangKyForm();
                                dangKyForm.ShowDialog();
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 3:// Admin
                                this.Hide();
                                CreatAccountForm fAdminForm = new CreatAccountForm();
                                fAdminForm.ShowDialog();
                                this.LoginUserKhac();
                                break;
                        }
                    }
                    //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
                    //{
                    // cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //}

                    #region active key mới
                    this.requestActivate();


                    int dayInt = 7;
                    int dateTrial = KeySecurity.Active.Install.License.KeyInfo.TrialDays;
                    //try
                    //{
                    //    string day = new Company.KDT.SHARE.Components.Utils.License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ExpiredDate"));
                    //    dayInt = int.Parse(day);
                    //}
                    //catch (Exception) { }
                    if (!string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Key))
                    {
                        cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        if (dayInt > dateTrial)
                            ShowMessage("Phần mềm còn " + dateTrial + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phần SOFTECH để tiếp tục sử dụng", false);
                        //CultureInfo en = new CultureInfo("en-US");
                        //for (int k = 0; k < dayInt; k++)
                        //{
                        //    //Kiem tra Culture, neu la vi-VN -> chuyen doi ve cung 1 chuan en-US.
                        //    string dateString = DateTime.Now.Date.ToString("MM/dd/yyyy");

                        //    if (DateTime.Parse(dateString, en).AddDays(k) == DateTime.Parse(Program.lic.dayExpires, en).Date)
                        //    {
                        //        int ngayConLai = k + 1;
                        //        ShowMessage("Ph?n m?m còn " + ngayConLai + " ngày n?a là h?t h?n s? d?ng.\nVui lòng liên h? v?i Công ty c? ph?n Công Ngh? Ph?n M?m Ðà N?ng d? ti?p t?c s? d?ng", false);
                        //        break;
                        //    }
                        //}
                        this.Text += setText(" - Đã đăng ký bản quyền.", " - License register.");
                    }
                    else
                    {
                        this.Text += setText(" - Bản dùng thử", " - Trial.");
                    }
                    #endregion

                    vsmMain.DefaultColorScheme = GlobalSettings.GiaoDien.Id;
                    if (GlobalSettings.GiaoDien.Id == "Office2007")
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    }

                    //MessageBox.Show("ok");
                    //LoginForm f = new LoginForm();
                    //f.ShowDialog(this);
                    //if (!f.IsLogin)
                    //{
                    //    this.Close();
                    //}                    

                    Show_LeftPanel();

                    // linhhtn - thêm thông báo sắp hết hạn sử dụng - 20110309
                    #region active key cũ
                    //int dayInt = 7;
                    //int dateTrial = int.Parse(Program.lic.dateTrial);
                    //try
                    //{
                    //    string day = new Company.KDT.SHARE.Components.Utils.License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ExpiredDate"));
                    //    dayInt = int.Parse(day);
                    //}
                    //catch (Exception) { }
                    //if (Program.isActivated)
                    //{
                    //    cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //    CultureInfo en = new CultureInfo("en-US");
                    //    for (int k = 0; k < dayInt; k++)
                    //    {
                    //        //Kiem tra Culture, neu la vi-VN -> chuyen doi ve cung 1 chuan en-US.
                    //        // string dateString = DateTime.Now.Date.ToString("MM/dd/yyyy");

                    //        if (DateTime.Now.AddDays(k).Date == System.DateTime.Parse(System.Convert.ToDateTime(Program.lic.dayExpires).ToString("MM/dd/yyyy"), en).Date)
                    //        {
                    //            int ngayConLai = k + 1;
                    //            ShowMessage("Ph?n m?m còn " + ngayConLai + " ngày n?a là h?t h?n s? d?ng.\nVui lòng liên h? v?i Công ty c? ph?n Công Ngh? Ph?n M?m Ðà N?ng d? ti?p t?c s? d?ng", false);
                    //            break;
                    //        }
                    //    }
                    //    this.Text += setText(" - Ðã dang ký b?n quy?n.", " - License register.");
                    //}
                    //else
                    //{
                    //    this.Text += setText(" - B?n dùng th?.", " - Trial.");

                    //    if (dateTrial <= 0)
                    //        this.requestActivate_old();
                    //    else if (dateTrial <= dayInt)
                    //        ShowMessage("Ph?n m?m còn " + dateTrial + " ngày n?a là h?t h?n s? d?ng.\nVui lòng liên h? v?i Công ty c? ph?n Công Ngh? Ph?n M?m Ðà N?ng d? ti?p t?c s? d?ng", false);
                    //}
                    #endregion

                    //if (GlobalSettings.MA_DON_VI == "") //Comment by HungTQ 22/12/2010
                    if (GlobalSettings.MA_DON_VI == "" || GlobalSettings.MA_DON_VI == "?")
                    {
                        ShowThietLapThongTinDNAndHQ();
                    }

                    //Hungtq complemented 14/12/2010
                    //timer1.Enabled = true; //Enable timer get Exchange Rate

                    backgroundWorker1.RunWorkerAsync();

                    if (GlobalSettings.NGAYSAOLUU != null && GlobalSettings.NGAYSAOLUU != "")
                    {
                        string st = GlobalSettings.NGAYSAOLUU;
                        DateTime time = Convert.ToDateTime(GlobalSettings.NGAYSAOLUU);
                        int NHAC_NHO_SAO_LUU = Convert.ToInt32(GlobalSettings.NHAC_NHO_SAO_LUU);
                        TimeSpan time1 = DateTime.Today.Subtract(time);
                        int ngay = NHAC_NHO_SAO_LUU - time1.Days;
                        if (ngay <= 0)
                            ShowBackupAndRestore(true);
                    }
                    else
                        ShowBackupAndRestore(true);

                    if (!(GlobalSettings.LastBackup.Equals("")) && !(GlobalSettings.NHAC_NHO_SAO_LUU.Equals("")))
                        if (Convert.ToDateTime(GlobalSettings.LastBackup).Add(TimeSpan.Parse(GlobalSettings.NHAC_NHO_SAO_LUU)) == DateTime.Today)
                        {
                            ShowBackupAndRestore(true);
                        }

                    //TODO: VNACCS, Hungtq 15/11/2013
                    frmTrangThaiPhanHoi.Show();

                    if (!Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ThongBaoVNACCS", "false")))
                    {
                        FrmThongBaoVNACCS frmThongBaoVNACCS = new FrmThongBaoVNACCS();
                        frmThongBaoVNACCS.ShowDialog();
                        //ShowMessageTQDT("ĐỂ THUẬN LỢI KHI KHAI DỊCH VỤ CÔNG TRỰC TUYẾN TRÊN TRANG WEBSITE HẢI QUAN : http://pus.customs.gov.vn \n.SOFTECH ĐÃ TẠO PHẦN MỀM KÝ CHỮ KÝ SỐ CHO FILE ĐÍNH KÈM GỬI LÊN (HỖ TRỢ ĐỊNH DẠNG pdf,docx,xlsx) \n .ĐỂ CÀI ĐẶT PHẦN MỀM DOANH NGHIỆP VÀO : MENU TRỢ GIÚP - CÔNG CỤ HỖ TRỢ - PHẦN MỀM HỖ TRỢ KÝ CHỮ KÝ SỐ CHO FILE ĐỂ CÀI ĐẶT.", false);
                    }
                    CreateShorcut();
#if GC_V4
                    // KIỂM TRA CÁC HĐ TRONG THÁNG GẦN HẾT HẠN
                    List<KDT_GC_HopDong> HopDongCollection = new List<KDT_GC_HopDong>();
                    List<KDT_GC_HopDong> HopDongExpriedCollection = new List<KDT_GC_HopDong>();
                    HopDongCollection = KDT_GC_HopDong.SelectCollectionAll();
                    foreach (KDT_GC_HopDong item in HopDongCollection)
                    {
                        DateTime NgayHetHan = item.NgayGiaHan.Year == 1900 ? item.NgayHetHan : item.NgayGiaHan;
                        if (NgayHetHan.Year == DateTime.Now.Year && NgayHetHan.Month == DateTime.Now.Month && NgayHetHan.Day >= DateTime.Now.Day)
                        {
                            HopDongExpriedCollection.Add(item);
                        }
                    }
                    String ErrorExpried = "\n[STT]-[SỐ HỢP ĐỒNG]-[NGÀY HẾT HẠN HỢP ĐỒNG]-[THỜI GIAN HẾT HẠN (SỐ NGÀY)]\n\n";
                    int i =1;
                    foreach (KDT_GC_HopDong item in HopDongExpriedCollection)
                    {
                        DateTime NgayHetHan = item.NgayGiaHan.Year == 1900 ? item.NgayHetHan : item.NgayGiaHan;
                        ErrorExpried += "[" + i +"]-["+item.SoHopDong+"]-[" + NgayHetHan.ToString("dd/MM/yyyy") +"]-[" + (NgayHetHan-DateTime.Now).TotalDays.ToString("N0") + "]\n\n";
                        i++;
                    }
                    if (HopDongExpriedCollection.Count >=1)
                    {
                        ErrorExpried += "GIẢI PHÁP : ĐỂ TIẾP TỤC KHAI BÁO TỜ KHAI DOANH NGHIỆP HÃY THỰC HIỆN KHAI BÁO PHỤ KIỆN GIA HẠN CÁC HỢP ĐỒNG TRƯỚC KHI HỢP ĐỒNG HẾT HẠN .";
                        ShowMessageTQDT("CẢNH BÁO HẾT HẠN HỢP ĐỒNG GIA CÔNG",ErrorExpried,false);
                    }
#endif
                }
                else
                    Application.Exit();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #region Active key
        private void requestActivate_old()
        {
            //if (ShowMessage("Đã hết hạn dùng thử.\nVui lòng liên hệ với Công ty cổ phẩn SOFTECH để tiếp tục sử dụng", true) == "Yes")
            if (showMsg("MSG_0203082", true) == "Yes")
            {
                ProdInfo.frmRegister_old obj = new Company.Interface.ProdInfo.frmRegister_old();
                obj.ShowDialog();
                requestActivate_old();
            }
            else
            {
                Application.Exit();
            }
        }


        private void requestActivate()
        {

            string sfmtMsg = "{0}\nVui lòng liên hệ với Công ty cổ phẩn SOFTECH để tiếp tục sử dụng";
            bool isShowActive = false;

            if (KeySecurity.Active.Install.License.KeyInfo == null)
            {
                sfmtMsg = string.Format(sfmtMsg, "Bạn chưa đăng ký sử dụng phần mềm.");
                isShowActive = true;
            }
            //else if (Helpers.GetMD5Value(Security.Active.Install.License.KeyInfo.ProductId) != "ECS_TQDT_KD")
            //{
            //    sfmtMsg = string.Format(sfmtMsg, "Mã sản phẩm không hợp lệ.");
            //    isShowActive = true;
            //}
            else if (KeySecurity.Active.Install.License.KeyInfo.TrialDays == 0)
            {
                sfmtMsg = string.Format(sfmtMsg, "Đã hết hạn dùng phần mềm");
                isShowActive = true;
            }

            if (isShowActive)
            {
                if (ShowMessage(sfmtMsg, true) == "Yes")
                {
                    ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
                    if (obj.ShowDialog(this) == DialogResult.OK)
                    {
                        ShowMessage("Kích hoạt thành công. Chương trình sẽ tự khởi động lại", false);
                        Application.Restart();
                    }
                    else
                    {
                        ShowMessage("Kích hoạt không thành công, chương trình sẽ tự đóng", false);
                        Application.Exit();
                    }

                }
                else
                    Application.Exit();
            }
            if (KeySecurity.Active.Install.License.KeyInfo.IsShow
                && !string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Notice))
            {
                InfoOnline inf = new InfoOnline();
                inf.SetRTF(KeySecurity.Active.Install.License.KeyInfo.Notice);
                inf.ShowDialog();
            }
        }


        private string UpdateInfo(string soTK)
        {
            Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer info = new Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer();
            #region Tạo thông tin update
            if (GlobalSettings.IsDaiLy)
            {
                string ListDoanhNghiep = Company.KDT.SHARE.Components.HeThongPhongKhai.GetDoanhNghiep();
                ThongTinDaiLy DLinfo = new ThongTinDaiLy();
                DLinfo = Company.KDT.SHARE.Components.HeThongPhongKhai.GetThongTinDL();
                info.Id = DLinfo.MaDL;
                info.Name = DLinfo.TenDL;
                info.Address = DLinfo.DiaChiDL;
                info.Phone = DLinfo.SoDienThoaiDL;
                info.Contact_Person = DLinfo.NguoiLienHeDL;
                info.Email = DLinfo.EmailDL;
                info.Id_Customs = GlobalSettings.MA_HAI_QUAN;
                info.IP_Customs = GlobalSettings.DiaChiWS;
                info.ServerName = GlobalSettings.SERVER_NAME;
                info.Data_Name = GlobalSettings.DATABASE_NAME;
                info.Data_User = GlobalSettings.USER;
                info.Data_Pass = GlobalSettings.PASS;
                info.Data_Version = Company.KDT.SHARE.Components.Version.GetVersion();
                info.App_Version = Application.ProductVersion;
                info.LastCheck = DateTime.Now;
                info.temp1 = ListDoanhNghiep;
                //info.temp2 = string.Empty;
                info.MachineCode = KeySecurity.KeyCode.ProcessInfo();
                info.Product_ID = "ECS_TQDT_GC_V3";
                info.RecordCount = soTK;
            }
            else
            {
                string SignRemote = string.Empty; // Thông tin của doanh nghiệp sử dụng chữ kí số
                if (Company.KDT.SHARE.Components.Globals.IsSignRemote)
                {
                    SignRemote = "Sữ dụng chữ kí số online";
                    SignRemote += ". User: " + Company.KDT.SHARE.Components.Globals.UserNameSignRemote;
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
                {
                    SignRemote = "Sử dụng chũ kí số qua mạng nội bộ";
                    SignRemote += ". Database chữ kí số: " + Company.KDT.SHARE.Components.Globals.DataSignLan;
                }
                info.Id = GlobalSettings.MA_DON_VI;
                info.Name = GlobalSettings.TEN_DON_VI;
                info.Address = GlobalSettings.DIA_CHI;
                info.Phone = GlobalSettings.SoDienThoaiDN;
                info.Contact_Person = GlobalSettings.NguoiLienHe;
                info.Email = GlobalSettings.MailDoanhNghiep;
                info.Id_Customs = GlobalSettings.MA_HAI_QUAN;
                info.IP_Customs = GlobalSettings.DiaChiWS;
                info.ServerName = GlobalSettings.SERVER_NAME;
                info.Data_Name = GlobalSettings.DATABASE_NAME;
                info.Data_User = GlobalSettings.USER;
                info.Data_Pass = GlobalSettings.PASS;
                info.Data_Version = Company.KDT.SHARE.Components.Version.GetVersion();
                info.App_Version = Application.ProductVersion;
                info.LastCheck = DateTime.Now;
                info.temp1 = SignRemote;
                //info.temp2 = string.Empty;
                info.MachineCode = KeySecurity.KeyCode.ProcessInfo();
                info.Product_ID = "ECS_TQDT_GC_V3";
                info.RecordCount = soTK;
            }
            #endregion
            return Helpers.Serializer(info);
        }
        #endregion

        //Kiem tra phan quyen hien thi he thong
        private void Show_LeftPanel()
        {
            try
            {
                if (versionHD == 0)
                {
                    //Kiem tra permission hop dong
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.XemDuLieu)))
                    {
                        this.explorerBar1.Groups[0].Enabled = false;
                        this.explorerBar1.Groups[0].Expanded = false;
                    }
                    else
                    {
                        this.explorerBar1.Groups[0].Enabled = true;
                        this.explorerBar1.Groups[0].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.KhaiDienTu)))
                        this.explorerBar1.Groups[0].Items[0].Enabled = false;
                    else
                        this.explorerBar1.Groups[0].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.CapNhatDuLieu)))
                        this.explorerBar1.Groups[0].Items[1].Enabled = false;
                    else
                        this.explorerBar1.Groups[0].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.XemDuLieu)))
                        this.explorerBar1.Groups[0].Items[2].Enabled = false;
                    else
                        this.explorerBar1.Groups[0].Items[2].Enabled = true;
                    //Ket thuc kiem tra hop dong

                    //Kiem tra permission dinh muc
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.XemDuLieu)))
                    {
                        this.explorerBar1.Groups[1].Enabled = false;
                        this.explorerBar1.Groups[1].Expanded = false;
                    }
                    else
                    {
                        this.explorerBar1.Groups[1].Enabled = true;
                        this.explorerBar1.Groups[1].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu)))
                        this.explorerBar1.Groups[1].Items[0].Enabled = false;
                    else
                        this.explorerBar1.Groups[1].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.CapNhatDuLieu)))
                        this.explorerBar1.Groups[1].Items[1].Enabled = false;
                    else
                        this.explorerBar1.Groups[1].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.XemDuLieu)))
                        this.explorerBar1.Groups[1].Items[2].Enabled = false;
                    else
                        this.explorerBar1.Groups[1].Items[2].Enabled = true;
                    //Ket thuc kiem tra dinh muc

                    //Kiem tra permission phu kien
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.XemDuLieu)))
                    {
                        this.explorerBar1.Groups[2].Enabled = false;
                        this.explorerBar1.Groups[2].Expanded = false;
                    }
                    else
                    {
                        this.explorerBar1.Groups[2].Enabled = true;
                        this.explorerBar1.Groups[2].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.KhaiDienTu)))
                        this.explorerBar1.Groups[2].Items[0].Enabled = false;
                    else
                        this.explorerBar1.Groups[2].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.CapNhatDuLieu)))
                        this.explorerBar1.Groups[2].Items[1].Enabled = false;
                    else
                        this.explorerBar1.Groups[2].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.XemDuLieu)))
                        this.explorerBar1.Groups[2].Items[2].Enabled = false;
                    else
                        this.explorerBar1.Groups[2].Items[2].Enabled = true;
                    //Ket thuc kiem tra phu kien

                    //Kiem tra permission phu kien
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.XemDuLieu)))
                    {
                        this.explorerBar1.Groups[2].Enabled = false;
                        this.explorerBar1.Groups[2].Expanded = false;
                    }
                    else
                    {
                        this.explorerBar1.Groups[2].Enabled = true;
                        this.explorerBar1.Groups[2].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.KhaiDienTu)))
                        this.explorerBar1.Groups[2].Items[0].Enabled = false;
                    else
                        this.explorerBar1.Groups[2].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.CapNhatDuLieu)))
                        this.explorerBar1.Groups[2].Items[1].Enabled = false;
                    else
                        this.explorerBar1.Groups[2].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.XemDuLieu)))
                        this.explorerBar1.Groups[2].Items[2].Enabled = false;
                    else
                        this.explorerBar1.Groups[2].Items[2].Enabled = true;
                    //Ket thuc kiem tra phu kien

                    //Kiem tra permission to khai co HD
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.KhaiDienTuNhap)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.KhaiDienTuXuat)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.XemDuLieu)))
                    {
                        this.explorerBar1.Groups[3].Enabled = false;
                        this.explorerBar1.Groups[3].Expanded = false;
                    }
                    else
                    {
                        this.explorerBar1.Groups[3].Enabled = true;
                        this.explorerBar1.Groups[3].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.KhaiDienTuNhap)))
                        this.explorerBar1.Groups[3].Items[0].Enabled = false;
                    else
                        this.explorerBar1.Groups[3].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.KhaiDienTuXuat)))
                        this.explorerBar1.Groups[3].Items[1].Enabled = false;
                    else
                        this.explorerBar1.Groups[3].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.CapNhatDuLieu)))
                        this.explorerBar1.Groups[3].Items[2].Enabled = false;
                    else
                        this.explorerBar1.Groups[3].Items[2].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.XemDuLieu)))
                        this.explorerBar1.Groups[3].Items[3].Enabled = false;
                    else
                        this.explorerBar1.Groups[3].Items[3].Enabled = true;
                    //Ket thuc kiem tra to khai co HD

                    //Kiem tra permission to khai co GCCT
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.KhaiDienTuNhap)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.KhaiDienTuXuat)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.XemDuLieu)))
                    {
                        this.explorerBar1.Groups[4].Enabled = false;
                        this.explorerBar1.Groups[4].Expanded = false;
                    }
                    else
                    {
                        this.explorerBar1.Groups[4].Enabled = true;
                        this.explorerBar1.Groups[4].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.KhaiDienTuNhap)))
                        this.explorerBar1.Groups[4].Items[0].Enabled = false;
                    else
                        this.explorerBar1.Groups[4].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.KhaiDienTuXuat)))
                        this.explorerBar1.Groups[4].Items[1].Enabled = false;
                    else
                        this.explorerBar1.Groups[4].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.CapNhatDuLieu)))
                        this.explorerBar1.Groups[4].Items[2].Enabled = false;
                    else
                        this.explorerBar1.Groups[4].Items[2].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.XemDuLieu)))
                        this.explorerBar1.Groups[4].Items[3].Enabled = false;
                    else
                        this.explorerBar1.Groups[4].Items[3].Enabled = true;
                    //Ket thuc kiem tra to khai co GCCT

                    //Kiem tra permission NPL cung ung
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.XemDuLieu)))
                    {
                        this.explorerBar1.Groups[5].Enabled = false;
                        this.explorerBar1.Groups[5].Expanded = false;
                    }
                    else
                    {
                        this.explorerBar1.Groups[5].Enabled = true;
                        this.explorerBar1.Groups[5].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.KhaiDienTu)))
                        this.explorerBar1.Groups[5].Items[0].Enabled = false;
                    else
                        this.explorerBar1.Groups[5].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.CapNhatDuLieu)))
                        this.explorerBar1.Groups[5].Items[1].Enabled = false;
                    else
                        this.explorerBar1.Groups[5].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.XemDuLieu)))
                        this.explorerBar1.Groups[5].Items[2].Enabled = false;
                    else
                        this.explorerBar1.Groups[5].Items[2].Enabled = true;
                    //Ket thuc kiem tra NPLP cung ung

                    //Kiem tra permission to khai khong co HD
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.KhaiDienTuNhap)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.KhaiDienTuXuat)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.XemDuLieu)))
                    {
                        this.explorerBar1.Groups[6].Enabled = false;
                        this.explorerBar1.Groups[6].Expanded = false;
                    }
                    else
                    {
                        this.explorerBar1.Groups[6].Enabled = true;
                        this.explorerBar1.Groups[6].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.KhaiDienTuNhap)))
                        this.explorerBar1.Groups[6].Items[0].Enabled = false;
                    else
                        this.explorerBar1.Groups[6].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.KhaiDienTuXuat)))
                        this.explorerBar1.Groups[6].Items[1].Enabled = false;
                    else
                        this.explorerBar1.Groups[6].Items[1].Enabled = true;
                    //if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.CapNhatDuLieu)))
                    //    this.explorerBar1.Groups[6].Items[2].Enabled = false;
                    //else
                    //    this.explorerBar1.Groups[6].Items[2].Enabled = true;
                    //if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.XemDuLieu)))
                    //    this.explorerBar1.Groups[6].Items[3].Enabled = false;
                    //else
                    //    this.explorerBar1.Groups[6].Items[3].Enabled = true;
                    //Ket thuc kiem tra to khai khong co HD
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        //Ket thuc phan quyen kiem tra he thong

        private void expSXXK_ItemClick(object sender, ItemEventArgs e)
        {

        }
        private void showTheoDoiPK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhieuKhoManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            PhieuKhoManager f = new PhieuKhoManager();
            f.MdiParent = this;
            f.Show();
        }

        private void wareHouseImport()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_WareHouseImportForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_WareHouseImportForm f = new VNACC_WareHouseImportForm();
            f.MdiParent = this;
            f.Show();
        }
        private void wareHouseExport()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_WareHouseExportForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_WareHouseExportForm f = new VNACC_WareHouseExportForm();
            f.MdiParent = this;
            f.Show();
        }
        private void wareHouseManagement()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_WareHouseManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_WareHouseManagementForm f = new VNACC_WareHouseManagementForm();
            f.MdiParent = this;
            f.Show();
        }
        private void showKhaiBao_PK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhieuKho"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            PhieuKho f = new PhieuKho();
            f.MdiParent = this;
            f.Show();
        }
        private void show_ToKhaiSXXKDangKy()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegisterForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ToKhaiMauDichRegistedForm f = new ToKhaiMauDichRegistedForm();
            f.MdiParent = this;
            f.Show();

        }

        private void show_ToKhaiSXXKManage()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            tkmdManageForm.nhomLoaiHinh = "SX";
            tkmdManageForm.MdiParent = this;
            tkmdManageForm.Text = setText("Theo dõi tờ khai SXXK", "Exporting Production declaration tracking");
            tkmdManageForm.Show();
        }

        //-----------------------------------------------------------------------------------------
        private void show_ToKhaiMauDichForm(string nhomLoaiHinh)
        {
            // if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
            //{
            //   ShowMessage("Bạn không có quyền thực hiện chức năng này.", false);
            //   return;
            //}   
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals(nhomLoaiHinh))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdForm = new ToKhaiMauDichForm();
            tkmdForm.OpenType = OpenFormType.Insert;
            // tkmdForm.Name = nhomLoaiHinh;
            tkmdForm.NhomLoaiHinh = nhomLoaiHinh;
            tkmdForm.MdiParent = this;
            tkmdForm.Show();
        }

        //-----------------------------------------------------------------------------------------

        private void show_ToKhaiMauDichManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            tkmdManageForm.MdiParent = this;
            tkmdManageForm.Show();
        }

        private void expKhaiBao_TheoDoi_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "ThucHienPhanBo":
                    this.ThucHienPhanBoToKhaiXuat();
                    break;
                case "TheoDoiPhanBo":
                    showMsg("MSG_0203055");
                    //ShowMessage("Chức năng đang được xây dựng.", false);
                    break;
                case "QuanLyNPLTon":
                    NhapXuatTon();
                    //ShowMessage("Chức năng đang được xây dựng.", false);
                    break;
            }
        }
        private void show_ToKhaiMauDichRegisterForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdRegisterForm = new Company.Interface.GC.ToKhaiMauDichRegistedForm();
            tkmdRegisterForm.MdiParent = this;
            tkmdRegisterForm.Show();
        }
        private void expGiaCong_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Hơp đồng
                case "hdgcNhap":
                    this.show_HopDongForm();
                    break;
                case "hdgcRegisted":
                    this.show_HopDongRegistedForm();
                    break;
                case "hdgcManage":
                    this.show_HopDongManageForm();
                    break;
                //Kho kế toán
                case "cmdKhoKT":
                    this.show_PhieuXuatNhapKhoManageForm();
                    break;

                //Kho kế toán
                case "cmdBCQTHopDong":
                    this.BaoCaoQuyetToanHopDong();
                    break;
                //Kho kế toán
                case "cmdTheoDoiBCQTHopDong":
                    this.TheoDoiBaoCaoQuyetToanHopDong();
                    break;
                //Quyết toán TT38

                case "mau15":
                    this.showCreateHopDongQuyetToan();
                    break;
                case "cmdTheoDoiQuyetToan_Mau15":
                    this.showTheoDoiQuyetToan();
                    break;
                case "mau16":
                    this.showCreatBCQTMMTB();
                    break;
                case "cmdMMTB":
                    this.showTheoDoiBCQTMMTB();
                    break;
                case "cmdNPLTon":
                    this.showTheoDoiNPLTon();
                    break;
                //Định mức
                case "dmRegister":
                    this.show_DinhMucGCRegister();
                    break;
                case "dmSend":
                    this.show_DinhMucGCSendForm();
                    break;
                case "dmEdit":
                    this.show_DinhMucGCSendEditForm();
                    break;
                case "dmCancel":
                    this.show_DinhMucGCSendCancelForm();
                    break;
                case "dmManage":
                    this.show_DinhMucGCManageForm();
                    break;
                case "dmRegisted":
                    this.show_DinhMucGCRegistedForm();
                    break;
                    //Định mức thực tế
                case "dmTTDangKy":
                    this.show_DinhMucGCTheoGiaiDoan();
                    break;
                case "dmTTEdit":
                    this.show_DinhMucGCEdit();
                    break;
                case "dmTTCancel":
                    this.show_DinhMucGCCancel();
                    break;
                case "dmTTTheoDoi":
                    this.show_TheoDoiDinhMucGCTheoGiaiDoan();
                    break;
                    //Lệnh sản xuất
                case "lsxDangKy":
                    this.show_DangKyLenhSanXuat();
                    break;
                case "lsxCapNhat":
                    this.show_CapNhatLenhSanXuat();
                    break;
                case "lsxTheoDoi":
                    this.show_TheoDoiLenhSanXuat();
                    break;
                case "lsxQuyTac":
                    this.show_CauHinhLenhSanXuat();
                    break;  
                //Phụ kiện
                case "pkgcNhap":
                    this.show_PhuKienGCSendForm();
                    break;
                case "pkgcManage":
                    this.show_PhuKienGCManageForm();
                    break;
                case "pkgcRegisted":
                    this.show_PhuKienGCRegistedForm();
                    break;

                //Tờ khai có HĐ
                case "tkNhapKhau_GC":
                    this.show_ToKhaiMauDichForm("NGC");
                    break;
                case "tkXuatKhau_GC":
                    this.show_ToKhaiMauDichForm("XGC");
                    break;
                case "tkTheoDoi":
                    this.show_ToKhaiMauDichManageForm();
                    break;
                case "tkDaDangKy":
                    this.show_ToKhaiMauDichRegisterForm();
                    break;

                //Tờ khai GCCT
                case "tkGCCTNhap":
                    this.show_ToKhaiGCChuyenTiepNhap("N");
                    break;
                case "tkGCCTXuat":
                    this.show_ToKhaiGCChuyenTiepNhap("X");
                    break;
                case "theodoiTKCT":
                    this.show_ToKhaiGCCTManagerForm();
                    break;
                case "tkGCCTDaDangKy":
                    this.show_ToKhaiGCCTDaDangKyForm();
                    break;

                //Bảng kê NPL cung ứng
                case "dmCUKhaibao":
                    this.show_KhaiBaoDMCU();
                    break;
                case "dmCUTheoDoi":
                    this.show_TheoDoiDMCU();
                    break;
                case "dmCUDaDangKy":
                    this.show_DaDangKyDMCU();
                    break;

                //Thanh khoản
                case "thanhKhoanSend":
                    this.Show_ThanhKhoanSendForm();
                    break;
                case "thanhKhoanManager":
                    this.show_ThanhKhoanManageForm();
                    break;

                case "thanhKhoan":
                    this.show_HopDongRegistedForm();
                    break;
                case "theodoiThanhKhoan":
                    this.show_HopDongManageForm();
                    break;

                case "dngsTieuHuy":
                    show_DeNghiGSTieuHuyForm();
                    break;
                case "dngsthManage":
                    show_DNGSTieuHuyGCManageForm();
                    break;
                #region Gia hạn thanh khoản hợp đồng
                case "giaHanSend":
                    show_KhaiBaoGiaHanTK();
                    break;
                case "giaHanManager":
                    show_GiaHanThanhKhoanManageForm();
                    break;
                #endregion


                case "cmdKhaiBaoBKTX":
                    show_KhaiBaoBKTX();
                    break;
                case "cmdTheoDoiBKTX":
                    show_TheoDoiBKTX();
                    break;
                case "cmdKhaiBaoCungUng":
                    show_KhaiBaoBKNPL();
                    break;
                case "cmdTheoDoiBKNPLCungUng":
                    show_TheoDoiBKNPL();
                    break;
                case "cmdBCQTMMTB":
                    this.showCreatBCQTMMTB();
                    break;
                case "cmdTheoDoiBCQTMMTB":
                    this.showTheoDoiBCQTMMTB();
                    break;
            }
        }

        private void show_CauHinhLenhSanXuat()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuyTacTaoLenhSanXuatForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            QuyTacTaoLenhSanXuatForm f = new QuyTacTaoLenhSanXuatForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_TheoDoiDinhMucGCTheoGiaiDoan()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucThucTeGCManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhMucThucTeGCManagerForm f = new DinhMucThucTeGCManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_TheoDoiLenhSanXuat()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("LenhSanXuatSPManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            LenhSanXuatSPManagerForm f = new LenhSanXuatSPManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_CapNhatLenhSanXuat()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CapNhatLenhSanXuatGCForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            CapNhatLenhSanXuatGCForm f = new CapNhatLenhSanXuatGCForm();
            f.MdiParent = this;
            f.Show();
        }
        private void show_DangKyLenhSanXuat()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("LenhSanXuatSPForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            LenhSanXuatSPForm lenhSanXuatSPForm = new LenhSanXuatSPForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected.ID > 0)
            {
                lenhSanXuatSPForm.HD = f.HopDongSelected;
                lenhSanXuatSPForm.MdiParent = this;
                lenhSanXuatSPForm.Show();
            }
        }
        private void show_DinhMucGCEdit()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucThucTeGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            DinhMucThucTeGCSendForm lenhSanXuatGCForm = new DinhMucThucTeGCSendForm();
            f.IsBrowseForm = true;            
            f.ShowDialog();
            if (f.HopDongSelected.ID > 0)
            {
                lenhSanXuatGCForm.HD = f.HopDongSelected;
                lenhSanXuatGCForm.MdiParent = this;
                lenhSanXuatGCForm.isEdit = true;
                lenhSanXuatGCForm.Show();
            }
        }
        private void show_DinhMucGCCancel()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucThucTeGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            DinhMucThucTeGCSendForm lenhSanXuatGCForm = new DinhMucThucTeGCSendForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected.ID > 0)
            {
                lenhSanXuatGCForm.HD = f.HopDongSelected;
                lenhSanXuatGCForm.MdiParent = this;
                lenhSanXuatGCForm.isCancel = true;
                lenhSanXuatGCForm.Show();
            }
        }
        private void show_DinhMucGCTheoGiaiDoan()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucThucTeGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            DinhMucThucTeGCSendForm lenhSanXuatGCForm = new DinhMucThucTeGCSendForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected.ID > 0)
            {
                lenhSanXuatGCForm.HD = f.HopDongSelected;
                lenhSanXuatGCForm.MdiParent = this;
                lenhSanXuatGCForm.Show();
            }
        }

        private void TheoDoiBaoCaoQuyetToanHopDong()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("BaoCaoQuyetToanManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaoCaoQuyetToanManagementForm f= new BaoCaoQuyetToanManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoQuyetToanHopDong()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("BaoCaoQuyetToanHopDongForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaoCaoQuyetToanHopDongForm  f = new BaoCaoQuyetToanHopDongForm();
            f.MdiParent = this;
            f.Show();
        }
        private void show_KhaiBaoBKNPL()
        {
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
            {
                cungUngFrm = new CungUngForm();
                cungUngFrm.MdiParent = this;
                cungUngFrm.CungUngDK.HopDong_ID = f.HopDongSelected.ID;
                cungUngFrm.CungUngDK.SoHopDong = f.HopDongSelected.SoHopDong;
                cungUngFrm.CungUngDK.NgayHopDong = f.HopDongSelected.NgayKy;
                cungUngFrm.CungUngDK.NgayHetHan = f.HopDongSelected.NgayHetHan;

                cungUngFrm.Show();
            }


        }
        private void show_TheoDoiBKNPL()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CungUngManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            cungUngManager = new CungUngManagerForm();
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
            {
                cungUngManager.HD = f.HopDongSelected;
            }
            cungUngManager.MdiParent = this;
            cungUngManager.Show();
        }

        private void show_KhaiBaoBKTX()
        {
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
            {
                taiXuatFrm = new TaiXuatForm();
                taiXuatFrm.MdiParent = this;
                taiXuatFrm.TaiXuatDK.HopDong_ID = f.HopDongSelected.ID;
                taiXuatFrm.TaiXuatDK.SoHopDong = f.HopDongSelected.SoHopDong;
                taiXuatFrm.TaiXuatDK.NgayHopDong = f.HopDongSelected.NgayKy;
                taiXuatFrm.TaiXuatDK.NgayHetHan = f.HopDongSelected.NgayHetHan;

                taiXuatFrm.Show();
            }


        }
        private void show_TheoDoiBKTX()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TaiXuatManagerFrom"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            taiXuatManager = new TaiXuatManagerForm();
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
            {
                taiXuatManager.HD = f.HopDongSelected;
            }
            taiXuatManager.MdiParent = this;
            taiXuatManager.Show();
        }


        private void show_DaDangKyDMCU()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NPLCungUngRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplCURegisredForm = new NPLCungUngRegistedForm();
            nplCURegisredForm.MdiParent = this;
            nplCURegisredForm.Show();
        }
        private void show_KhaiBaoDMCU()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NPLCungUngTheoTKSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplCUSendForm = new NPLCungUngTheoTKSendForm();
            nplCUSendForm.MdiParent = this;
            nplCUSendForm.Show();
        }
        private void show_TheoDoiDMCU()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NPLCungUngManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplCUSTheoDoiForm = new NPLCungUngManageForm();
            nplCUSTheoDoiForm.MdiParent = this;
            nplCUSTheoDoiForm.Show();
        }
        #region Gia hạn thanh khoản
        private void show_KhaiBaoGiaHanTK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("GiaHanHopDongSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            dmgcSendForm = new DinhMucGCSendForm();
            f.IsBrowseForm = true;
            f.ShowDialog();

            if (f.HopDongSelected.ID > 0)
            {
                ghHopDongTKSendForm = new GiaHanHopDongSendForm();
                ghHopDongTKSendForm.HD = f.HopDongSelected;
                ghHopDongTKSendForm.OpenType = OpenFormType.Insert;
                ghHopDongTKSendForm.MdiParent = this;
                ghHopDongTKSendForm.Show();
            }



        }
        private void show_TheoDoiGiaHanTK()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("GiaHanHopDongManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplCUSTheoDoiForm = new NPLCungUngManageForm();
            nplCUSTheoDoiForm.MdiParent = this;
            nplCUSTheoDoiForm.Show();
        }
        #endregion
        private void show_CapnhatHSTK()
        {
            //HoSoThanhKhoanForm nplCUSTheoDoiForm ;
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("HoSoThanhKhoanForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            //nplCUSTheoDoiForm = new HoSoThanhKhoanForm();
            //nplCUSTheoDoiForm.MdiParent = this;
            //nplCUSTheoDoiForm.Show();
        }
        private void show_ToKhaiGCCTDaDangKyForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiGCCTRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkctRegistedForm = new ToKhaiGCCTRegistedForm();
            tkctRegistedForm.MdiParent = this;
            tkctRegistedForm.Show();
        }
        //-----------------------------------------------------------------------------------------
        private void show_ToKhaiGCCTManagerForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiGCCTManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            theodoiTKGCCTForm = new ToKhaiGCCTManagerForm();
            theodoiTKGCCTForm.MdiParent = this;
            theodoiTKGCCTForm.Show();
        }

        //-----------------------------------------------------------------------------------------


        //-----------------------------------------------------------------------------------------
        private void show_HopDongRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HopDongRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hdgcRegistedForm = new HopDongRegistedForm();
            hdgcRegistedForm.MdiParent = this;
            hdgcRegistedForm.Show();
        }
        private void show_HopDongManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HopDongManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            f.MdiParent = this;
            f.Show();
        }
        private void show_HopDongForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HopDongEditForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hdgcForm = new HopDongEditForm();
            hdgcForm.OpenType = OpenFormType.Insert;
            hdgcForm.MdiParent = this;
            hdgcForm.Show();
        }
        private void show_DinhMucGCSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            dmgcSendForm = new DinhMucGCSendForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected.ID > 0)
            {
                dmgcSendForm.dmDangKy.ID_HopDong = f.HopDongSelected.ID;
                dmgcSendForm.OpenType = OpenFormType.Insert;
                dmgcSendForm.MdiParent = this;
                dmgcSendForm.Show();
            }
        }
        private void show_DinhMucGCSendCancelForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            dmgcSendForm = new DinhMucGCSendForm();
            f.IsBrowseForm = true;            
            f.ShowDialog();
            if (f.HopDongSelected.ID > 0)
            {
                dmgcSendForm.dmDangKy.ID_HopDong = f.HopDongSelected.ID;
                dmgcSendForm.OpenType = OpenFormType.Insert;
                dmgcSendForm.MdiParent = this;
                dmgcSendForm.isCancel = true;
                dmgcSendForm.Show();
            }
        }
        private void show_DinhMucGCSendEditForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            dmgcSendForm = new DinhMucGCSendForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected.ID > 0)
            {
                dmgcSendForm.dmDangKy.ID_HopDong = f.HopDongSelected.ID;
                dmgcSendForm.OpenType = OpenFormType.Insert;
                dmgcSendForm.MdiParent = this;
                dmgcSendForm.isEdit = true;
                dmgcSendForm.Show();
            }
        }
        private void show_DinhMucGCRegister()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCRegisterForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhMucGCRegisterForm f = new DinhMucGCRegisterForm();
            f.MdiParent = this;
            f.Show();
        }
        private void show_DeNghiGSTieuHuyForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("GiamSatThieuHuyGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            gsTieuHuySendForm = new GiamSatThieuHuyGCSendForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected.ID > 0)
            {
                gsTieuHuySendForm.dnGSTieuHuy.HopDong_ID = f.HopDongSelected.ID;
                gsTieuHuySendForm.HD = f.HopDongSelected;
                gsTieuHuySendForm.OpenType = OpenFormType.Insert;
                gsTieuHuySendForm.MdiParent = this;
                gsTieuHuySendForm.Show();
            }
        }
        private void show_ToKhaiGCChuyenTiepNhap(string nhomLoaiHinh)
        {
            Form[] forms = this.MdiChildren;

            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TKCX" + nhomLoaiHinh))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkgcCTNhapForm = new ToKhaiGCChuyenTiepNhapForm();
            tkgcCTNhapForm.NhomLoaiHinh = nhomLoaiHinh;
            // tkgcCTNhapForm.Name = "TKCX"+ nhomLoaiHinh;
            tkgcCTNhapForm.OpenType = OpenFormType.Insert;
            tkgcCTNhapForm.MdiParent = this;
            tkgcCTNhapForm.Show();
        }
        //private void show_ToKhaiGCChuyenTiepXuat()
        //{
        //    Form[] forms = this.MdiChildren;
        //    for (int i = 0; i < forms.Length; i++)
        //    {
        //        if (forms[i].Name.ToString().Equals("ToKhaiGCChuyenTiepXuatForm"))
        //        {
        //            forms[i].Activate();
        //            return;
        //        }
        //    }
        //    tkgcCTXuatForm = new ToKhaiGIaCongChuyenTiepXuatForm();
        //    tkgcCTXuatForm.MdiParent = this;
        //    tkgcCTXuatForm.Show();
        //}
        private void show_DinhMucGCManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmgcManageForm = new DinhMucGCManageForm();
            dmgcManageForm.MdiParent = this;
            dmgcManageForm.Show();
        }
        private void show_DinhMucGCRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmgcRegistedForm = new DinhMucGCRegistedForm();
            dmgcRegistedForm.MdiParent = this;
            dmgcRegistedForm.Show();
        }
        private void show_PhuKienGCSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhuKienGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
            {
                pkgcSendForm = new PhuKienGCSendForm();
                pkgcSendForm.HD.ID = f.HopDongSelected.ID;
                pkgcSendForm.HD = HopDong.Load(pkgcSendForm.HD.ID);
                pkgcSendForm.OpenType = OpenFormType.Insert;
                pkgcSendForm.MdiParent = this;
                pkgcSendForm.Show();
            }

        }
        private void show_PhuKienGCManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhuKienGCManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            pkgcManageForm = new PhuKienGCManageForm();
            pkgcManageForm.MdiParent = this;
            pkgcManageForm.Show();
        }
        private void show_DNGSTieuHuyGCManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("GiamSatThieuHuyGCManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            gsTieuHuyManagerForm = new GiamSatThieuHuyGCManageForm();
            gsTieuHuyManagerForm.MdiParent = this;
            gsTieuHuyManagerForm.Show();
        }
        private void show_GiaHanThanhKhoanManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("GiaHanHopDongManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ghHopDongManageForm = new GiaHanHopDongManageForm();
            ghHopDongManageForm.MdiParent = this;
            ghHopDongManageForm.Show();
        }
        private void show_PhuKienGCRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhuKienGCRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            pkgcRegistedForm = new PhuKienGCRegistedForm();
            pkgcRegistedForm.MdiParent = this;
            pkgcRegistedForm.Show();
        }
        //Update by KHANHHN - 20/03/2012
        // Show form thanh khoản hợp đồng gia công
        private void Show_ThanhKhoanSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThanhKhoanHDGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            thanhKhoanSendForm = new ThanhKhoanHDGCSendForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected.ID > 0)
            {
                thanhKhoanSendForm.HD.ID = f.HopDongSelected.ID;
                thanhKhoanSendForm.OpenType = OpenFormType.Insert;
                thanhKhoanSendForm.MdiParent = this;
                thanhKhoanSendForm.Show();
            }
        }
        private void show_ThanhKhoanManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThanhKhoanManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            thanhKhoanManagerForm = new ThanhKhoanManageForm();
            thanhKhoanManagerForm.MdiParent = this;
            thanhKhoanManagerForm.Show();
        }
        //hangton      

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (queueForm.HDCollection.Count > 0)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(HangDoiCollection));
                FileStream fs = new FileStream("HangDoi.xml", FileMode.Create);
                serializer.Serialize(fs, queueForm.HDCollection);
            }
            queueForm.Dispose();
            //             backgroundWorker1.WorkerSupportsCancellation = true;
            //             backgroundWorker1.CancelAsync();

            Application.Exit();
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //Hungtq, 21/11/2013.
                case "cmdHDSDCKS":
                    HDSD_CKS();
                    break;

                case "cmdHelpVideo":
                    try
                    {
                        this.Cursor = Cursors.WaitCursor;
                        FrmHelpVideo frm = new FrmHelpVideo();
                        frm.Show();
                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        this.Cursor = Cursors.Default;
                    }
                    break;

                case "cmdNhomCuaKhau":
                    {
                        new NhomCuaKhauForm().ShowDialog();
                    }
                    break;
                case "cmdTLTTDN":
                    {
                        this.Show_ThietLapTTDN();
                    }
                    break;
                case "cmdThoat":
                    {
                        Application.Exit();
                    }
                    break;
                case "DongBoDuLieu":
                    {
                        this.DongBoDuLieuDN();
                    }
                    break;
                case "cmd2007":
                    if (cmd20071.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    GlobalSettings.Luu_GiaoDien("Office2007");
                    UpdateStyleForAllForm("Office2007");
                    //this.UpdateStyles();

                    break;
                case "cmd2003":
                    if (cmd20031.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    GlobalSettings.Luu_GiaoDien("Office2003");
                    UpdateStyleForAllForm("Office2003");
                    //this.UpdateStyles();
                    break;
                case "cmdEng":
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        try
                        {
                            this.BackgroundImage = System.Drawing.Image.FromFile("ecse.jpg");
                            this.BackgroundImageLayout = ImageLayout.Stretch;
                            this.Text = "Management System for outsourcing - exporting " + Application.ProductVersion;
                            statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = "User: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Enterprise: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                            statusBar.Panels["HaiQuan"].Text = statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN);
                        }
                        catch { }
                        GlobalSettings.NGON_NGU = "1";
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NGON_NGU", 1);
                        CultureInfo culture = new CultureInfo("en-US");
                        //     Thread.CurrentThread.CurrentCulture = culture;
                        //     Thread.CurrentThread.CurrentUICulture = culture;  
                        Form[] forms = this.MdiChildren;
                        for (int i = 0; i < forms.Length; i++)
                        {
                            ((BaseForm)forms[i]).InitCulture("en-US", "Company.Interface.LanguageResource.Language");
                        }
                        this.InitCulture("en-US", "Company.Interface.LanguageResource.Language");
                    }
                    break;
                case "cmdVN":
                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        if (MLMessages("Chương trình sẽ khởi động lại và chuyển giao diện sang tiếng Việt,hãy lưu lại các công việc đang làm.\nBạn muốn chuyển giao diện không?", "MSG_0203092", "", true) == "Yes")
                        {
                            GlobalSettings.NGON_NGU = "0";
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NGON_NGU", 0);
                            CultureInfo culture = new CultureInfo("vi-VN");
                            Application.Restart();
                        }
                    }
                    break;

                case "cmdHelp":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\Huong dan su dung phan mem ECS_TQDT_GC.pdf");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(setText("Hiện tại chưa có file help!", "There is not a help file!"), false);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;
                case "cmdHDSDVNACCS":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\VNACCS_TaiLieuHuongDanSuDung.pdf");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(setText("Hiện tại chưa có file help!", "There is not a help file!"), false);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;
                case "cmdBieuThueXNK2018":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\BIEU THUE XNK2018 - Update.xlsx");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(setText("Hiện tại chưa có file help!", "There is not a help file!"), false);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;                   
                case "cmdMaHS":
                    ShowMaHSForm();
                    break;
                //Cảng trong nước
                case "cmdBerth":
                    ShowBerthForm();
                    break;
                //Địa điểm lưu kho hàng chờ thông quan
                case "cmdCargo":
                    ShowCargoForm();
                    break;
                //Địa điểm xếp hàng 
                case "cmdCityUNLOCODE":
                    ShowCityUNLOCODEForm();
                    break;
                //Tổng hợp danh mục
                case "cmdCommon":
                    ShowCommonForm();
                    break;
                //Container size
                case "cmdContainerSize":
                    ShowContainerSizeForm();
                    break;
                // Đội thủ tục HQ
                case "cmdCustomsSubSection":
                    ShowCustomsSubSectionForm();
                    break;
                // Mã kiểm dịch động vật
                case "cmdOGAUser":
                    ShowOGAUserForm();
                    break;
                //ĐVT Lượng kiện
                case "cmdPackagesUnit":
                    ShowPackagesUnitForm();
                    break;
                // Kho ngoại quan
                case "cmdStations":
                    ShowStationsForm();
                    break;
                // Biểu thuế XNK
                case "cmdTaxClassificationCode":
                    ShowTaxClassificationCodeForm();
                    break;
                case "cmdHaiQuan":
                    ShowDVHQForm();
                    break;
                case "cmdNuoc":
                    ShowNuocForm();
                    break;
                case "cmdNguyenTe":
                    ShowNguyenTeForm();
                    break;
                case "cmdPTTT":
                    ShowPTTTForm();
                    break;
                case "cmdPTVT":
                    ShowPTVTForm();
                    break;
                case "cmdDKGH":
                    ShowDKGHForm();
                    break;
                case "cmdDVT":
                    ShowDVTForm();
                    break;
                case "cmdCuaKhau":
                    ShowCuaKhauForm();
                    break;
                case "cmdReloadData":
                    Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
                    ucCategory.ReLoadData();
                    Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucCategoryAllowEmpty = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
                    ucCategoryAllowEmpty.ReLoadData();
                    break;
                case "cmdRestore":
                    ShowRestoreForm();
                    break;
                case "cmdBackUp":
                    ShowBackupAndRestore(true);
                    break;
                case "ThongSoKetNoi":
                    ShowThietLapThongSoKetNoi();
                    break;
                case "TLThongTinDNHQ":
                    ShowThietLapThongTinDNAndHQ();
                    break;
                case "cmdThietLapIn":
                    ShowThietLapInBaoCao();
                    break;
                case "cmdConfigReadExcel":
                    ShowThietLapReadExcel();
                    break;                    
                case "cmdCauHinhToKhai":
                    ShowCauHinhToKhai();
                    break;
                case "QuanLyNguoiDung":
                    ShowQuanLyNguoiDung();
                    break;
                case "QuanLyNhom":
                    ShowQuanLyNhomNguoiDung();
                    break;
                case "LoginUser":
                    LoginUserKhac();
                    break;
                case "cmdChangePass":
                    ChangePassForm();
                    break;
                case "TraCuuMaHS":
                    TraCuuMaHSForm();
                    break;
                case "cmdAutoUpdate":
                    //System.Diagnostics.Process.Start(Application.StartupPath + "\\AppAutoUpdate.exe");
                    AutoUpdate("MSG");
                    break;
                case "NhapHDXML":
                    NhapHopDongXML();
                    break;
                case "NhapPKXML":
                    NhapPhuKienXML();
                    break;
                case "NhapDMXML":
                    NhapDinhMucXML();
                    break;
                case "NhapTKMD":
                    this.NhapTKMDXML();
                    break;
                case "NhapTKGCCT":
                    this.NhapTKCTXML();
                    break;
                case "cmdXuatHopDong":
                    XuatHopDongChoPhongKhai();
                    break;
                case "cmdXuatPhuKien":
                    XuatPhuKienChoPhongKhai();
                    break;
                case "cmdXuatDinhMuc":
                    XuatDinhMucChoPhongKhai();
                    break;
                case "XuatTKMD":
                    XuatToKhaiMauDichChoPhongKhai();
                    break;
                case "XuatTKGCCT":
                    XuatToKhaiGCCTChoPhongKhai();
                    break;
                case "cmdAbout":
                    this.dispInfo();
                    break;
                case "cmdActivate":
                    this.dispActivate();
                    break;
                case "cmdDMSPGC":
                    this.ShowFormDMSPGC();
                    break;
                case "NhapXML":
                    this.ShowFormChonThuMuc();
                    break;
                case "cmdCloseMe":
                    this.doCloseMe();
                    break;
                case "cmdCloseAllButMe":
                    this.doCloseAllButMe();
                    break;
                case "cmdCloseAll":
                    this.doCloseAll();
                    break;
                case "cmdXuatDN":
                    this.XuatDuLieuChoDoanhNghiep();
                    break;

                case "cmdNhapHopDong":
                    NhapHopDongTuDoanhNghiep();
                    break;
                case "cmdNhapDinhMuc":
                    NhapDinhMucTuDoanhNghiep();
                    break;
                case "cmdNhapPhuKien":
                    NhapPhuKienTuDoanhNghiep();
                    break;
                case "cmdNhapToKhaiMauDich":
                    NhapToKhaiMauDichTuDoanhNghiep();
                    break;
                case "cmdNhapToKhaiGCCT":
                    NhapToKhaiGCCTTuDoanhNghiep();
                    break;
                case "QuanLyMess":
                    WSForm2 login1 = new WSForm2();
                    login1.ShowDialog();
                    if (WSForm2.IsSuccess == true)
                    {
                        ShowQuanLyMess();
                    }
                    break;

                case "cmdLog":
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\Error.log");
                    break;
                case "cmdChuKySo":
                    ChuKySo();
                    break;
                case "cmdTimer":
                    new FrmCauHinhThoiGian().ShowDialog();
                    break;

                case "cmdDaily":
                    ShowDaiLy();
                    break;
                case "cmdThongBaoVNACCS":
                    FrmThongBaoVNACCS frmThongBaoVNACCS = new FrmThongBaoVNACCS();
                    frmThongBaoVNACCS.Show();
                    break;
                case "cmdSignFile":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\PDFSignedDigitalSetup.msi");
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;
                case "cmdHelpSignFile":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\HelpSignFile.pdf");
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;
                case "cmdUpdateCategoryOnline":
                    UpdateCategoryOnline();
                    break;
                case "cmdNhanPhanHoi":
                    var mainForm = Application.OpenForms["MainForm"] as MainForm;
                    //mainForm.ShowFormThongBao(null, null);
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate { mainForm.frmThongBao.Show(); }));

                    }
                    else
                        mainForm.frmThongBao.Show();
                    break;
                case "cmdUpdateCertificates":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\CertificateAuthority\\certVNACCS.exe");
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;
                case "cmdPerformanceDatabase":
                    PerformanceDatabase();
                    break;
                    

            }

            //TODO: HUNGTQ updated 02/08/2012.
            cmQuerySQL(e);

            //TODO: HUNGTQ updated 20/08/2012.
            cmdBoSungCommand(e);
        }

        private void PerformanceDatabase()
        {
            PerformanceDatabaseForm f = new PerformanceDatabaseForm();
            f.Show(this);
            
        }

        private void ShowThietLapReadExcel()
        {
            FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
            f.ShowDialog(this);
        }

        private void UpdateCategoryOnline()
        {
            try
            {
                //Old
                //string msg = Company.KDT.SHARE.Components.Globals.UpdateCategoryVNACCSOnline();

                //if (msg != "")
                //{
                //    if (msg.Contains("E|"))
                //        ShowMessage("Có lỗi trong quá trình thực hiện cập nhật danh mục trực tuyến:\r\n" + msg.Replace("E|", ""), true, true, "");
                //    else
                //        ShowMessage(msg, false);
                //}
                //else
                //    ShowMessage("Không có bản ghi nào được cập nhật mới. Thông tin danh mục trên hệ thống hiện tại là mới nhất.", false);

                ProcessUpdateCategoryOnline f = new ProcessUpdateCategoryOnline();
                f.Show(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ShowTaxClassificationCodeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_TaxClassificationCode"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_TaxClassificationCode f = new Company.Interface.VNACCS.Category.VNACC_Category_TaxClassificationCode();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowStationsForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Stations"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Stations f = new Company.Interface.VNACCS.Category.VNACC_Category_Stations();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowPackagesUnitForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_PackagesUnit"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_PackagesUnit f = new Company.Interface.VNACCS.Category.VNACC_Category_PackagesUnit();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowOGAUserForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_OGAUser"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_OGAUser f = new Company.Interface.VNACCS.Category.VNACC_Category_OGAUser();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCustomsSubSectionForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CustomsSubSection"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CustomsSubSection f = new Company.Interface.VNACCS.Category.VNACC_Category_CustomsSubSection();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowContainerSizeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_ContainerSize"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_ContainerSize f = new Company.Interface.VNACCS.Category.VNACC_Category_ContainerSize();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCommonForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Common"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Common f = new Company.Interface.VNACCS.Category.VNACC_Category_Common();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCityUNLOCODEForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CityUNLOCODE"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CityUNLOCODE f = new Company.Interface.VNACCS.Category.VNACC_Category_CityUNLOCODE();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCargoForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Cargo"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Cargo f = new Company.Interface.VNACCS.Category.VNACC_Category_Cargo();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowBerthForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Berth"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Berth f = new Company.Interface.VNACCS.Category.VNACC_Category_Berth();
            f.MdiParent = this;
            f.Show();
        }
        private void HDSD_CKS()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Company.KDT.SHARE.Components.Globals.HDSD_CKS();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ChuKySo()
        {
            FrmCauHinhChuKySo cks = new FrmCauHinhChuKySo();
            cks.Show(this);

            //TODO: Hungtq updated, 7/2/2013. ChuKySo(): Reload sau khi dong form cau hinh CKS
            khoitao_GiaTriMacDinh();
        }

        #region Query SQL

        //TODO: HUNGTQ updated 02/08/2012.

        /// <summary>
        ///  Bo sung dong lenh: CreateQuerySQLCommand(); vao ham khoi dung MainForm()
        /// </summary>
        private void CreateQuerySQLCommand()
        {
            try
            {
                Janus.Windows.UI.CommandBars.UICommand mnuFormSQL = new Janus.Windows.UI.CommandBars.UICommand("mnuFormSQL");
                //Janus.Windows.UI.CommandBars.UICommand mnuMiniSQL = new Janus.Windows.UI.CommandBars.UICommand("mnuMiniSQL");
                // 
                // mnuFormSQL
                // 
                mnuFormSQL.ImageIndex = 15;
                mnuFormSQL.Key = "mnuFormSQL";
                mnuFormSQL.Name = "mnuFormSQL";
                mnuFormSQL.Text = "Form SQL";
                // 
                // mnuMiniSQL
                // 
                //mnuMiniSQL.ImageIndex = 15;
                //mnuMiniSQL.Key = "mnuMiniSQL";
                //mnuMiniSQL.Name = "mnuMiniSQL";
                //mnuMiniSQL.Text = "MiniSQL";

                // 
                // mnuQuerySQL
                // 
                mnuQuerySQL.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                    mnuFormSQL});
                //mnuMiniSQL});
                mnuQuerySQL.ImageIndex = 15;
                mnuQuerySQL.Key = "mnuQuerySQL";
                mnuQuerySQL.Name = "mnuQuerySQL";
                mnuQuerySQL.Text = "Thực hiện truy vấn SQL";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage(ex.Message, false);
            }
        }

        /// <summary>
        /// Bo sung dong lenh: cmQuerySQL(e); vao cuoi dong cua ham cmMain_CommandClick(...)
        /// </summary>
        /// <param name="e"></param>
        private void cmQuerySQL(Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "mnuFormSQL":
                    FormSQL();
                    break;
                case "mnuMiniSQL":
                    MiniSQL();
                    break;
            }
        }

        private void FormSQL()
        {
            WSForm2 login = new WSForm2();
            login.ShowDialog(this);
            if (WSForm2.IsSuccess == true)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("FrmQuerySQLUpdate"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }

                frmQuery = new FrmQuerySQLUpdate();
                frmQuery.MdiParent = this;
                frmQuery.Show();
            }
        }

        private void MiniSQL()
        {
            //WSForm2 login2 = new WSForm2();
            //login2.ShowDialog(this);
            //if (WSForm2.IsSuccess == true)
            //{
            //    string fileName = Application.StartupPath + "\\MiniSQL\\MiniSqlQuery.exe";
            //    if (System.IO.File.Exists(fileName))
            //    {

            //        MiniSqlQuery.Core.DbConnectionDefinition conn = new MiniSqlQuery.Core.DbConnectionDefinition();
            //        conn.ConnectionString = "Data Source=" + GlobalSettings.SERVER_NAME + ";Initial Catalog=" + GlobalSettings.DATABASE_NAME +
            //                                ";User ID=" + GlobalSettings.USER + ";Password=" + GlobalSettings.PASS;
            //        conn.Name = GlobalSettings.DATABASE_NAME;
            //        System.Diagnostics.Process.Start(fileName);
            //    }
            //}
        }

        #endregion

        #region Bo sung command

        //TODO: HUNGTQ updated 16/08/2012.

        /// <summary>
        /// Bo sung dong lenh: cmdBoSungCommand(e); vao cuoi dong cua ham cmMain_CommandClick(...)
        /// </summary>
        /// <param name="e"></param>
        private void cmdBoSungCommand(Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            Company.KDT.SHARE.Components.Globals.OpenCommandBieuThue(e.Command.Key);

            if (e.Command.Key == "cmdGetCategoryOnline")
            {
                string msg = Company.KDT.SHARE.Components.Globals.UpdateCategoryOnline();

                if (msg != "")
                {
                    if (msg.Contains("E|"))
                        ShowMessage("Có lỗi trong quá trình thực hiện cập nhật danh mục trực tuyến:\r\n" + msg.Replace("E|", ""), true, true, "");
                    else
                        ShowMessage(msg, false);
                }
                else
                    ShowMessage("Không có bản ghi nào được cập nhật mới. Thông tin danh mục trên hệ thống hiện tại là mới nhất.", false);

            }
            else if (e.Command.Key == "cmdGopY")
            {
            }
            else if (e.Command.Key == "cmdTeamview")
            {
                string path = Application.StartupPath + "\\Support\\TeamViewerQS_vi-ckq.exe";

                System.Diagnostics.Process.Start(path);
            }
            else if (e.Command.Key == "cmdCapNhatHS8Auto")
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Bạn có chắc chắn muốn chuyển mã HS danh mục Nguyên phụ liệu, Sản phẩm thành 8 số không?");
                sb.AppendLine();
                sb.AppendLine("Quy tắc chuyển mã:");
                sb.AppendLine("   - Chương trình sẽ tự động cắt bỏ 2 số '00' bên phải của mã HS, chỉ lấy 8 số.");
                sb.AppendLine();
                sb.AppendLine("Vì vậy mã HS sau chuyển đổi có thể không đúng với danh mục chuẩn mã HS 8 số, bạn phải kiểm tra lại danh mục mã HS vừa chuyển đổi.");

                if (ShowMessageTQDT("Chuyên đổi mã Biểu thuế (HS)", sb.ToString(), true) == "Yes")
                {
                    if (Company.KDT.SHARE.Components.Globals.ConvertHS8Auto(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, "GC") == true)
                    {
                        ShowMessage("Đã thực hiện thành công.", false);
                    }
                }
            }
            else if (e.Command.Key == "cmdCapNhatHS8SoManual")
            {
                HS08SO.ChuyenMaHS08So f = new Company.Interface.HS08SO.ChuyenMaHS08So();
                f.ShowDialog();
            }
            else if (e.Command.Key == "cmdImageResize")
            {
                Cursor = Cursors.WaitCursor;

                string msg = Company.KDT.SHARE.Components.Globals.DownloadToolImageResize();

                if (msg.Length != 0)
                    ShowMessage(msg, false);
            }
            else if (e.Command.Key == "cmdImageResizeHelp")
            {
                Cursor = Cursors.WaitCursor;

                string msg = Company.KDT.SHARE.Components.Globals.HDSDImageResize();

                if (msg.Length != 0)
                    ShowMessage(msg, false);
            }
            else if (e.Command.Key == "cmdUpdateDatabase")
            {
                UpdateNewDataVersion();
            }
        }

        private void UpdateNewDataVersion()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Helper.Controls.UpdateDatabaseSQLForm.Instance(true).ShowDialog();

                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                this.cmdDataVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != "" ? dataVersion : "?"));

                //TODO: Reload lai du lieu sau khi Nang cap du lieu moi. Hungtq, 21/11/2013.
                Company.KDT.SHARE.VNACCS.Controls.ucBase.InitialData(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        #endregion

        private void Show_ThietLapTTDN()
        {
            DangKyForm fDK = new DangKyForm();
            fDK.ShowDialog();
        }
        #region Doanh nghiệp khai báo đại ly
        private void ShowDaiLy()
        {
            WSForm2 login = new WSForm2();
            login.ShowDialog(this);
            if (WSForm2.IsSuccess == true)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("FrmDaiLy"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }

                FrmDaiLy frmDaiLy = new FrmDaiLy();
                frmDaiLy.MdiParent = this;
                frmDaiLy.Show();
            }
        }
        #endregion

        private void ShowQuanLyMess()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            qlMess = new QuanLyMessage();
            qlMess.MdiParent = this;
            qlMess.Show();
        }
        /// <summary>
        /// Bốn phương thức dùng để nhập tờ khai chuyển tiếp từ doanh nghiệp
        /// </summary>
        public void NhapToKhaiGCCTTuDoanhNghiep()
        {
            List<ToKhaiChuyenTiep> TKCTCollection = new List<ToKhaiChuyenTiep>();

            XmlSerializer serializer = new XmlSerializer(typeof(List<ToKhaiChuyenTiep>));
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKCTCollection = (List<ToKhaiChuyenTiep>)serializer.Deserialize(fs);
                    fs.Close();

                    //KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    //KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    //IDHDCollection = IDHD.SelectCollectionAll();
                    //foreach (ToKhaiChuyenTiep TKCT_EDIT in TKCTCollection)
                    //{
                    //    foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                    //    {
                    //        if (TKCT_EDIT.IDHopDong == IDHD_EDIT.ID_CU)
                    //            TKCT_EDIT.IDHopDong = IDHD_EDIT.ID_MOI;
                    //    }
                    //}

                    string message = "";
                    string stTKCTHopDong = KiemTraTKCTHopDong(TKCTCollection);
                    if (stTKCTHopDong != "")
                    {
                        message += "Hợp đồng " + stTKCTHopDong + "chưa có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (message != "")
                        ShowMessage(message, false);
                    if (TKCTCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
                        {
                            if (TKCT.InsertDongBoDuLieu())
                                i++;
                        }
                        if (i > 0)
                            ShowMessage("Nhập thành công " + i + " tờ khai.", false);
                    }
                    else
                        ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }


        }

        /// <summary>
        /// Bốn phương thức dùng để nhập tờ khai mậu dịch từ doanh nghiệp
        /// </summary>        
        public void NhapToKhaiMauDichTuDoanhNghiep()
        {
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection = new Company.GC.BLL.KDT.ToKhaiMauDichCollection();

            XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.ToKhaiMauDichCollection));
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKMDCollection = (Company.GC.BLL.KDT.ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();

                    //KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    //KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    //IDHDCollection = IDHD.SelectCollectionAll();
                    //foreach (ToKhaiMauDich TKMD_EDIT in TKMDCollection)
                    //{
                    //    foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                    //    {
                    //        if (TKMD_EDIT.IDHopDong == IDHD_EDIT.ID_CU)
                    //            TKMD_EDIT.IDHopDong = IDHD_EDIT.ID_MOI;
                    //    }
                    //}

                    string message = "";
                    string stTKMDHopDong = KiemTraTKMDHopDong(TKMDCollection);
                    if (stTKMDHopDong != "")
                    {
                        message += "Hợp đồng " + stTKMDHopDong + ", chưa có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (message != "")
                        ShowMessage(message, false);
                    if (TKMDCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (ToKhaiMauDich TKMD in TKMDCollection)
                        {
                            if (TKMD.InsertDongBoDuLieu())
                                i++;
                        }
                        if (i > 0)
                            ShowMessage("Nhập thành công " + i + " tờ khai.", false);
                    }
                    else
                        ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        /// <summary>
        /// Hai phương thức dùng để nhập định mức từ doanh nghiệp
        /// </summary>
        public void NhapDinhMucTuDoanhNghiep()
        {

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    DinhMucDangKyCollection DMDKCollection = (DinhMucDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string message = "";

                    KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    IDHDCollection = IDHD.SelectCollectionAll();
                    DinhMucDangKy DMDK = new DinhMucDangKy();
                    foreach (DinhMucDangKy DM_EDIT in DMDKCollection)
                    {
                        foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                        {
                            if (DM_EDIT.ID_HopDong == IDHD_EDIT.ID_CU)
                                DM_EDIT.ID_HopDong = IDHD_EDIT.ID_MOI;
                        }
                    }


                    string stDMHopDong = KiemTraDinhMucHopDong(DMDKCollection);


                    if (stDMHopDong != "")
                    {
                        message += "Hợp đồng có mã " + stDMHopDong + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }

                    if (DMDKCollection.Count != 0)
                    {
                        if (DMDK.InsertUpdate(DMDKCollection))
                            ShowMessage("Nhập thành công " + DMDKCollection.Count.ToString() + " thông tin định mức.", false);
                    }
                    else
                        ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private string KiemTraDinhMucHopDong(DinhMucDangKyCollection DMDKCollection)
        {
            string st = "";
            //string mahopdong = "";
            DinhMucDangKyCollection DMDKCollectionHD = new DinhMucDangKyCollection();
            string where = "MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'";
            List<HopDong> HDCollection = HopDong.SelectCollectionDynamic(where, "");
            foreach (DinhMucDangKy DMDK in DMDKCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (DMDK.SoHopDong.Trim().ToUpper() == HDExist.SoHopDong.Trim().ToUpper())
                    {
                        DMDK.ID_HopDong = HDExist.ID;
                        DMDKCollectionHD.Add(DMDK);
                        break;
                    }
                }
            }
            foreach (DinhMucDangKy DMDK in DMDKCollectionHD)
            {
                DMDKCollection.Remove(DMDK);
            }
            foreach (DinhMucDangKy DMDK in DMDKCollection)
            {
                st += DMDK.SoHopDong + ", ";
            }
            DMDKCollection.Clear();
            foreach (DinhMucDangKy DMDK in DMDKCollectionHD)
            {
                DMDKCollection.Add(DMDK);
            }
            return st;
        }

        /// <summary>
        /// Bốn phương thức dùng để nhập phụ kiện từ doanh nghiệp
        /// </summary>
        private void NhapPhuKienTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.GC.PhuKienDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.GC.BLL.KDT.GC.PhuKienDangKyCollection PKCollection = (Company.GC.BLL.KDT.GC.PhuKienDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();

                    //KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    //KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    //IDHDCollection = IDHD.SelectCollectionAll();
                    //Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                    //foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy PK_EDIT in PKCollection)
                    //{
                    //    foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                    //    {
                    //        if (PK_EDIT.HopDong_ID == IDHD_EDIT.ID_CU)
                    //            PK_EDIT.HopDong_ID = IDHD_EDIT.ID_MOI;
                    //    }
                    //}

                    string message = "";
                    string stPKHopDong = KiemTraPhuKienHopDong(PKCollection);
                    if (stPKHopDong != "")
                    {
                        message += "Hợp đồng có mã " + stPKHopDong + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }

                    if (PKCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy PK in PKCollection)
                        {
                            PK.InsertPhuKienDongBoDuLieu();
                            i++;
                        }
                        if (i > 0)
                            ShowMessage("Nhập thành công " + i + " phụ kiện.", false);
                    }
                    else
                        ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        public string KiemTraPhuKienHopDong(PhuKienDangKyCollection PKCollection)
        {
            string st = "";
            string mahopdong = "";
            PhuKienDangKyCollection PKCollectionHD = new PhuKienDangKyCollection();
            string where = "MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'";
            List<HopDong> HDCollection = HopDong.SelectCollectionDynamic(where, "");
            foreach (PhuKienDangKy PK in PKCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (PK.SoHopDong.Trim().ToUpper() == HDExist.SoHopDong.Trim().ToUpper())
                    {
                        PK.HopDong_ID = HDExist.ID;
                        PKCollectionHD.Add(PK);
                        break;
                    }
                }
            }
            foreach (PhuKienDangKy PK in PKCollectionHD)
            {
                PKCollection.Remove(PK);
            }
            foreach (PhuKienDangKy PK in PKCollection)
            {
                if (mahopdong.Trim().ToUpper() != PK.HopDong_ID.ToString().Trim().ToUpper())
                {
                    mahopdong = PK.HopDong_ID.ToString();
                    st += mahopdong + ", ";
                }
            }
            PKCollection.Clear();
            foreach (PhuKienDangKy PK in PKCollectionHD)
            {
                PKCollection.Add(PK);
            }
            return st;
        }


        /// <summary>
        /// Ba phương thức dùng để nhập hợp đồng từ doanh nghiệp
        /// </summary>
        public void NhapHopDongTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Company.GC.BLL.KDT.GC.HopDong>));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    List<Company.GC.BLL.KDT.GC.HopDong> HDCollection = (List<Company.GC.BLL.KDT.GC.HopDong>)serializer.Deserialize(fs);
                    fs.Close();
                    string stExist = KiemTraTonTaiHopDongNhap(HDCollection);
                    string message = "";
                    if (stExist != "")
                    {
                        message += "Hợp đồng số " + stExist + " đã có trong hệ thống nên sẽ được bỏ qua.\n";
                    }

                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }
                    HopDong HD = new HopDong();
                    KDT_GC_IDHopDongCollection IDCollection = new KDT_GC_IDHopDongCollection();
                    foreach (HopDong HopDongDK in HDCollection)
                    {
                        KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                        IDHD.SoHopDong = HopDongDK.SoHopDong;
                        IDHD.ID_CU = HopDongDK.ID;
                        IDCollection.Add(IDHD);
                    }
                    if (HDCollection.Count != 0)
                    {
                        foreach (HopDong HDNhap in HDCollection)
                        {
                            HDNhap.InsertHopDongDongBoDuLieu(IDCollection);

                        }
                        KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                        if (IDHD.InsertUpdateNew(IDCollection))
                            ShowMessage("Nhập thành công " + HDCollection.Count.ToString() + " hợp đồng.", false);
                    }
                    else
                    {
                        ShowMessage("Không có dữ liệu để nhập", false);
                    }

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private void XuatDuLieuChoDoanhNghiep()
        {
            XuatDuLieuChoDNForm form = new XuatDuLieuChoDNForm();
            form.ShowDialog();
        }

        private void ShowFormChonThuMuc()
        {
            ChonThuMucDaiLyForm f = new ChonThuMucDaiLyForm();
            f.ShowDialog();
        }

        private void ShowFormDMSPGC()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DanhMucLoaiSPForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DanhMucLoaiSPForm dmloaisp = new DanhMucLoaiSPForm();
            dmloaisp.MdiParent = this;
            dmloaisp.Show();
        }
        private void dispActivate()
        {
            ProdInfo.frmRegister_old obj = new Company.Interface.ProdInfo.frmRegister_old();
            obj.Show();
        }

        private void dispInfo()
        {
            ProdInfo.frmInfo obj = new Company.Interface.ProdInfo.frmInfo();
            obj.ShowDialog();
        }

        private void XuatToKhaiGCCTChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiGCCTManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ToKhaiGCCTManagerForm f = new ToKhaiGCCTManagerForm();

            f.MdiParent = this;
            f.Show();
        }

        private void XuatToKhaiMauDichChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ToKhaiMauDichManageForm f = new ToKhaiMauDichManageForm();

            f.MdiParent = this;
            f.Show();
        }

        private void XuatDinhMucChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhMucGCManageForm f = new DinhMucGCManageForm();

            f.MdiParent = this;
            f.Show();
        }

        private void XuatPhuKienChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhuKienGCManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            PhuKienGCManageForm f = new PhuKienGCManageForm();

            f.MdiParent = this;
            f.Show();
        }

        private void XuatHopDongChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HopDongManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();

            f.MdiParent = this;
            f.Show();
        }

        private void NhapXuatTon()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NhapXuatTonForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            NhapXuatTonForm f = new NhapXuatTonForm();
            f.MdiParent = this;
            f.Show();
        }
        private void ThucHienPhanBoToKhaiXuat()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongkeHangTonForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (f.HopDongSelected.SoHopDong != null)
                if (f.HopDongSelected.SoHopDong.Trim() != "")
                {
                    tkHangTon = new ThongkeHangTonForm();
                    tkHangTon.HD = new HopDong();
                    tkHangTon.HD.ID = f.HopDongSelected.ID;
                    tkHangTon.HD = HopDong.Load(tkHangTon.HD.ID);
                    tkHangTon.MdiParent = this;
                    tkHangTon.Show();
                }


        }

        #region NHẬP DỮ LIỆU TỪ XML

        /// <summary>
        /// Bốn phương thức dùng để nhập tờ khai chuyển tiếp từ XML
        /// </summary>

        public void NhapTKCTXML()
        {
            List<ToKhaiChuyenTiep> TKCTCollection = new List<ToKhaiChuyenTiep>();

            XmlSerializer serializer = new XmlSerializer(typeof(List<ToKhaiChuyenTiep>));
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKCTCollection = (List<ToKhaiChuyenTiep>)serializer.Deserialize(fs);
                    fs.Close();

                    KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    IDHDCollection = IDHD.SelectCollectionAll();
                    foreach (ToKhaiChuyenTiep TKCT_EDIT in TKCTCollection)
                    {
                        foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                        {
                            if (TKCT_EDIT.IDHopDong == IDHD_EDIT.ID_CU)
                                TKCT_EDIT.IDHopDong = IDHD_EDIT.ID_MOI;
                        }
                    }

                    string message = "";
                    string stExist = KiemTraTonTaiTKCTNhap(TKCTCollection);
                    string stTKCTHopDong = KiemTraTKCTHopDong(TKCTCollection);
                    string stTKCTDoanhNghiep = KiemTraTKCTDoanhNghiep(TKCTCollection);
                    if (stExist != "")
                    {
                        message += "Tờ khai số " + stExist + ", đã có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (stTKCTHopDong != "")
                    {
                        message += "Hợp đồng " + stTKCTHopDong + ", chưa có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (stTKCTDoanhNghiep != "")
                    {
                        message += "Tờ khai số" + stTKCTDoanhNghiep + ", không phải của doanh nghiệp nên sẽ bị loại.\n";
                    }
                    if (message != "")
                        ShowMessage(message, false);
                    if (TKCTCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
                        {
                            if (TKCT.InsertDongBoDuLieu())
                                i++;
                        }
                        if (i > 0)
                            showMsg("MSG_0203056", i);
                        //ShowMessage("Nhập thành công " + i + " tờ khai.", false);
                        else
                            showMsg("MSG_0203057");
                        //ShowMessage("Lỗi trong quá trình nhập dữ liệu.", false);
                    }
                    else
                        showMsg("MSG_0203058");
                    //ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }


        }

        public string KiemTraTonTaiTKCTNhap(List<ToKhaiChuyenTiep> TKCTCollection)
        {
            string st = "";
            string sotokhai = "";
            List<ToKhaiChuyenTiep> TKCTCollectionExist = new List<ToKhaiChuyenTiep>();
            foreach (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                if (TKCT.CheckExistToKhaiChuyenTiep(TKCT.MaHaiQuanTiepNhan, TKCT.MaLoaiHinh, TKCT.SoToKhai, TKCT.NgayDangKy))
                {
                    sotokhai = TKCT.SoToKhai + setText("/Ngày ", "/Date") + TKCT.NgayDangKy;
                    st += sotokhai;
                    TKCTCollectionExist.Add(TKCT);
                }
            }
            foreach (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT in TKCTCollectionExist)
            {
                TKCTCollection.Remove(TKCT);
            }
            return st;
        }

        public string KiemTraTKCTHopDong(List<ToKhaiChuyenTiep> TKCTCollection)
        {
            string st = "";
            string mahopdong = "";
            List<ToKhaiChuyenTiep> TKCTCollectionHD = new List<ToKhaiChuyenTiep>();
            List<HopDong> HDCollection = new List<HopDong>();
            HopDong HD = new HopDong();
            HDCollection = HopDong.SelectCollectionAll();
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (TKCT.IDHopDong == HDExist.ID)
                    {
                        TKCTCollectionHD.Add(TKCT);
                        break;
                    }
                }
            }
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollectionHD)
            {
                TKCTCollection.Remove(TKCT);
            }
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                if (mahopdong.Trim().ToUpper() != TKCT.IDHopDong.ToString().Trim().ToUpper())
                {
                    mahopdong = TKCT.IDHopDong.ToString();
                    st += mahopdong;
                }
            }
            TKCTCollection.Clear();
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollectionHD)
            {
                TKCTCollection.Add(TKCT);
            }
            return st;
        }

        public string KiemTraTKCTDoanhNghiep(List<ToKhaiChuyenTiep> TKCTCollection)
        {
            string st = "";
            string sotokhai = "";
            List<ToKhaiChuyenTiep> TKCTCollectionExit = new List<ToKhaiChuyenTiep>();
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                if (TKCT.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    TKCTCollectionExit.Add(TKCT);
                    if (sotokhai.Trim().ToUpper() != TKCT.SoToKhai.ToString().ToUpper())
                    {
                        sotokhai = TKCT.SoToKhai.ToString();
                        st += sotokhai;
                    }
                }
            }
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollectionExit)
            {
                TKCTCollection.Remove(TKCT);
            }
            return st;
        }




        /// <summary>
        /// Bốn phương thức dùng để nhập tờ khai mậu dịch từ XML
        /// </summary>


        public void NhapTKMDXML()
        {
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection = new Company.GC.BLL.KDT.ToKhaiMauDichCollection();

            XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.ToKhaiMauDichCollection));
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKMDCollection = (Company.GC.BLL.KDT.ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();

                    KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    IDHDCollection = IDHD.SelectCollectionAll();
                    foreach (ToKhaiMauDich TKMD_EDIT in TKMDCollection)
                    {
                        foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                        {
                            if (TKMD_EDIT.IDHopDong == IDHD_EDIT.ID_CU)
                                TKMD_EDIT.IDHopDong = IDHD_EDIT.ID_MOI;
                        }
                    }

                    string message = "";
                    string stExist = KiemTraTonTaiTKMDNhap(TKMDCollection);
                    string stTKMDHopDong = KiemTraTKMDHopDong(TKMDCollection);
                    string stTKMDDoanhNghiep = KiemTraTKMDDoanhNghiep(TKMDCollection);
                    if (stExist != "")
                    {
                        message += "Tờ khai số " + stExist + ", đã có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (stTKMDHopDong != "")
                    {
                        message += "Hợp đồng " + stTKMDHopDong + ", chưa có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (stTKMDDoanhNghiep != "")
                    {
                        message += "Tờ khai số" + stTKMDDoanhNghiep + ", không phải của doanh nghiệp nên sẽ bị loại.\n";
                    }
                    if (message != "")
                        ShowMessage(message, false);
                    if (TKMDCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (ToKhaiMauDich TKMD in TKMDCollection)
                        {
                            if (TKMD.InsertDongBoDuLieu())
                                i++;
                        }
                        if (i > 0)
                            showMsg("MSG_0203056", i);
                        //ShowMessage("Nhập thành công " + i + " tờ khai.", false);
                        else
                            showMsg("MSG_0203057");
                        //ShowMessage("Lỗi trong quá trình nhập dữ liệu.", false);
                    }
                    else
                        showMsg("MSG_0203058");
                    //ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        public string KiemTraTonTaiTKMDNhap(Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string st = "";
            string sotokhai = "";
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollectionExist = new ToKhaiMauDichCollection();
            foreach (Company.GC.BLL.KDT.ToKhaiMauDich TKMD in TKMDCollection)
            {
                if (TKMD.CheckExistToKhaiMauDich(TKMD.MaHaiQuan, TKMD.MaLoaiHinh, TKMD.SoToKhai, TKMD.NgayDangKy))
                {
                    sotokhai = TKMD.SoToKhai + "/Ngày " + TKMD.NgayDangKy;
                    st += sotokhai;
                    TKMDCollectionExist.Add(TKMD);
                }
            }
            foreach (Company.GC.BLL.KDT.ToKhaiMauDich TKMD in TKMDCollectionExist)
            {
                TKMDCollection.Remove(TKMD);
            }
            return st;
        }

        public string KiemTraTKMDHopDong(Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string st = "";
            string mahopdong = "";
            ToKhaiMauDichCollection TKMDCollectionHD = new ToKhaiMauDichCollection();
            List<HopDong> HDCollection = new List<HopDong>();
            HopDong HD = new HopDong();
            HDCollection = HopDong.SelectCollectionAll();
            foreach (ToKhaiMauDich TKMD in TKMDCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (TKMD.IDHopDong == HDExist.ID)
                    {
                        TKMDCollectionHD.Add(TKMD);
                        break;
                    }
                }
            }
            foreach (ToKhaiMauDich TKMD in TKMDCollectionHD)
            {
                TKMDCollection.Remove(TKMD);
            }
            foreach (ToKhaiMauDich TKMD in TKMDCollection)
            {
                if (mahopdong.Trim().ToUpper() != TKMD.IDHopDong.ToString().Trim().ToUpper())
                {
                    mahopdong = TKMD.IDHopDong.ToString();
                    st += mahopdong;
                }
            }
            TKMDCollection.Clear();
            foreach (ToKhaiMauDich TKMD in TKMDCollectionHD)
            {
                TKMDCollection.Add(TKMD);
            }
            return st;
        }

        public string KiemTraTKMDDoanhNghiep(Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string st = "";
            string sotokhai = "";
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollectionExit = new ToKhaiMauDichCollection();
            foreach (ToKhaiMauDich TKMD in TKMDCollection)
            {
                if (TKMD.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    TKMDCollectionExit.Add(TKMD);
                    if (sotokhai.Trim().ToUpper() != TKMD.SoToKhai.ToString().ToUpper())
                    {
                        sotokhai = TKMD.SoToKhai.ToString();
                        st += sotokhai;
                    }
                }
            }
            foreach (ToKhaiMauDich TKMD in TKMDCollectionExit)
            {
                TKMDCollection.Remove(TKMD);
            }
            return st;
        }



        /// <summary>
        /// Ba phương thức dùng để nhập hợp đồng từ XML
        /// </summary>

        public void NhapHopDongXML()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Company.GC.BLL.KDT.GC.HopDong>));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    List<Company.GC.BLL.KDT.GC.HopDong> HDCollection = (List<Company.GC.BLL.KDT.GC.HopDong>)serializer.Deserialize(fs);
                    fs.Close();
                    string stExist = KiemTraTonTaiHopDongNhap(HDCollection);
                    string stMember = KiemTraHopDongDoanhNghiep(HDCollection);
                    string message = "";
                    if (stExist != "")
                    {
                        message += "Hợp đồng số " + stExist + " đã có trong hệ thống nên sẽ được bỏ qua.\n";
                    }
                    if (stMember != "")
                    {
                        message += "Hợp đồng số " + stMember + "không phải của doanh nghiệp nên bị loại bỏ.";
                    }
                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }
                    HopDong HD = new HopDong();
                    KDT_GC_IDHopDongCollection IDCollection = new KDT_GC_IDHopDongCollection();
                    foreach (HopDong HopDongDK in HDCollection)
                    {
                        KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                        IDHD.SoHopDong = HopDongDK.SoHopDong;
                        IDHD.ID_CU = HopDongDK.ID;
                        IDCollection.Add(IDHD);
                    }
                    if (HDCollection.Count != 0)
                    {
                        foreach (HopDong HDNhap in HDCollection)
                        {
                            HDNhap.InsertHopDongDongBoDuLieu(IDCollection);

                        }
                        KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                        if (IDHD.InsertUpdateNew(IDCollection))
                            showMsg("MSG_0203059", HDCollection.Count);
                        //ShowMessage("Nhập thành công " + HDCollection.Count.ToString() + " hợp đồng.", false);
                        else
                            showMsg("MSG_0203057");
                        //ShowMessage("Lỗi trong quá trình nhập dữ liệu", false);
                    }
                    else
                    {
                        showMsg("MSG_0203058");
                        //ShowMessage("Không có dữ liệu để nhập", false);
                    }

                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        public string KiemTraTonTaiHopDongNhap(List<HopDong> HDCollection)
        {
            string st = "";
            string sohopdong = "";
            List<HopDong> HDCollectionExit = new List<HopDong>();
            foreach (HopDong HopDongDK in HDCollection)
            {
                if (HopDongDK.checkSoHopDongExit(HopDongDK.SoHopDong, HopDongDK.MaHaiQuan, GlobalSettings.MA_DON_VI))
                {
                    HDCollectionExit.Add(HopDongDK);
                    if (sohopdong.Trim().ToUpper() != HopDongDK.SoHopDong.ToUpper())
                    {
                        sohopdong = HopDongDK.SoHopDong;
                        st += sohopdong + ", ";
                    }
                }
            }
            foreach (HopDong HopDongDK in HDCollectionExit)
            {
                HDCollection.Remove(HopDongDK);
            }
            return st;
        }

        public string KiemTraHopDongDoanhNghiep(List<HopDong> HDCollection)
        {
            string st = "";
            string sohopdong = "";
            List<HopDong> HDCollectionExit = new List<HopDong>();
            foreach (HopDong HopDongDK in HDCollection)
            {
                if (HopDongDK.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    HDCollectionExit.Add(HopDongDK);
                    if (sohopdong.Trim().ToUpper() != HopDongDK.SoHopDong.ToUpper())
                    {
                        sohopdong = HopDongDK.SoHopDong;
                        st += sohopdong + ", ";
                    }
                }
            }
            foreach (HopDong HopDongDK in HDCollectionExit)
            {
                HDCollection.Remove(HopDongDK);
            }
            return st;
        }



        /// <summary>
        /// Năm phương thức dùng để nhập định mức từ XML
        /// </summary>

        public void NhapDinhMucXML()
        {

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.GC.DinhMucCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.GC.BLL.GC.DinhMucCollection DMCollection = (Company.GC.BLL.GC.DinhMucCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string message = "";

                    KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    IDHDCollection = IDHD.SelectCollectionAll();
                    Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                    foreach (Company.GC.BLL.GC.DinhMuc DM_EDIT in DMCollection)
                    {
                        foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                        {
                            if (DM_EDIT.HopDong_ID == IDHD_EDIT.ID_CU)
                                DM_EDIT.HopDong_ID = IDHD_EDIT.ID_MOI;
                        }
                    }

                    string stExist = KiemTraTonTaiDinhMucNhap(DMCollection);
                    string stDMSanPham = KiemTraDinhMucSanPham(DMCollection);
                    string stDMHopDong = KiemTraDinhMucHopDong(DMCollection);
                    string stDMNguyenPhuLieu = KiemTraDinhMucNguyenPhuLieu(DMCollection);
                    if (stExist != "")
                    {
                        message += "Sản phẩm có mã " + stExist + "đã có định mức trong hệ thống nên sẽ được bỏ qua.\n";
                    }
                    if (stDMHopDong != "")
                    {
                        message += "Hợp đồng có mã " + stDMHopDong + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (stDMSanPham != "")
                    {
                        message += "Sản phẩm có mã " + stDMSanPham + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (stDMNguyenPhuLieu != "")
                    {
                        message += "Nguyên phụ liệu có mã " + stDMNguyenPhuLieu + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }

                    if (DMCollection.Count != 0)
                    {
                        if (DM.InsertUpdate(DMCollection))
                            showMsg("MSG_0203060", DMCollection.Count);
                        //ShowMessage("Nhập thành công "+ DMCollection.Count.ToString() + " thông tin định mức.", false);
                        else
                            showMsg("MSG_0203057");
                        //ShowMessage("Lỗi trong quá trình nhập dữ liệu.", false);
                    }
                    else
                        showMsg("MSG_0203058");
                    //ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private string KiemTraTonTaiDinhMucNhap(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string masanpham = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionExit = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (DM.CheckExitsDinhMucSanPhamNguyenPhuLieu())
                {
                    DMCollectionExit.Add(DM);
                    if (masanpham.Trim().ToUpper() != DM.MaSanPham.Trim().ToUpper())
                    {
                        masanpham = DM.MaSanPham;
                        st += masanpham + ", ";
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionExit)
            {
                DMCollection.Remove(DM);
            }
            return st;
        }

        private string KiemTraDinhMucSanPham(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string masanpham = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionSP = new Company.GC.BLL.GC.DinhMucCollection();
            Company.GC.BLL.GC.SanPhamCollection SPCollection = new Company.GC.BLL.GC.SanPhamCollection();
            Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
            SPCollection = SP.SelectCollectionAll();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                foreach (Company.GC.BLL.GC.SanPham SPExist in SPCollection)
                {
                    if (DM.MaSanPham == SPExist.Ma)
                    {
                        DMCollectionSP.Add(DM);
                        break;
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionSP)
            {
                DMCollection.Remove(DM);
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (masanpham.Trim().ToUpper() != DM.MaSanPham.Trim().ToUpper())
                {
                    masanpham = DM.MaSanPham;
                    st += masanpham + ", ";
                }
            }
            DMCollection.Clear();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionSP)
            {
                DMCollection.Add(DM);
            }
            return st;
        }

        private string KiemTraDinhMucHopDong(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string mahopdong = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionHD = new Company.GC.BLL.GC.DinhMucCollection();
            List<HopDong> HDCollection = new List<HopDong>();
            HopDong HD = new HopDong();
            HDCollection = HopDong.SelectCollectionAll();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (DM.HopDong_ID == HDExist.ID)
                    {
                        DMCollectionHD.Add(DM);
                        break;
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionHD)
            {
                DMCollection.Remove(DM);
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (mahopdong.Trim().ToUpper() != DM.HopDong_ID.ToString().Trim().ToUpper())
                {
                    mahopdong = DM.HopDong_ID.ToString();
                    st += mahopdong + ", ";
                }
            }
            DMCollection.Clear();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionHD)
            {
                DMCollection.Add(DM);
            }
            return st;
        }

        private string KiemTraDinhMucNguyenPhuLieu(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string manguyenphulieu = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionNPL = new Company.GC.BLL.GC.DinhMucCollection();
            Company.GC.BLL.GC.NguyenPhuLieuCollection NPLCollection = new Company.GC.BLL.GC.NguyenPhuLieuCollection();
            Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
            NPLCollection = NPL.SelectCollectionAll();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLExist in NPLCollection)
                {
                    if (DM.MaNguyenPhuLieu == NPLExist.Ma)
                    {
                        DMCollectionNPL.Add(DM);
                        break;
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionNPL)
            {
                DMCollection.Remove(DM);
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (manguyenphulieu.Trim().ToUpper() != DM.MaNguyenPhuLieu.Trim().ToUpper())
                {
                    manguyenphulieu = DM.MaNguyenPhuLieu;
                    st += manguyenphulieu + ", ";
                }
            }
            DMCollection.Clear();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionNPL)
            {
                DMCollection.Add(DM);
            }
            return st;
        }



        /// <summary>
        /// Bốn phương thức dùng để nhập phụ kiện từ XML
        /// </summary>

        public void NhapPhuKienXML()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.GC.PhuKienDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.GC.BLL.KDT.GC.PhuKienDangKyCollection PKCollection = (Company.GC.BLL.KDT.GC.PhuKienDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();

                    KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    IDHDCollection = IDHD.SelectCollectionAll();
                    Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                    foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy PK_EDIT in PKCollection)
                    {
                        foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                        {
                            if (PK_EDIT.HopDong_ID == IDHD_EDIT.ID_CU)
                                PK_EDIT.HopDong_ID = IDHD_EDIT.ID_MOI;
                        }
                    }

                    string message = "";
                    string stExist = KiemTraTonTaiPhuKienNhap(PKCollection);
                    string stPKHopDong = KiemTraPhuKienHopDong(PKCollection);
                    string stPKDoanhNghiep = KiemTraPhuKienDoanhNghiep(PKCollection);
                    if (stExist != "")
                    {
                        message += "Phụ kiện có mã " + stExist + "đã có trong hệ thống nên sẽ được bỏ qua.\n";
                    }
                    if (stPKHopDong != "")
                    {
                        message += "Hợp đồng có mã " + stPKHopDong + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (stPKDoanhNghiep != "")
                    {
                        message += "Phụ kiện có mã " + stPKDoanhNghiep + "không phải của doanh nghiệp nên bị bỏ.\n";
                    }
                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }

                    if (PKCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy PK in PKCollection)
                        {
                            PK.InsertPhuKienDongBoDuLieu();
                            i++;
                        }
                        if (i > 0)
                            showMsg("MSG_0203061", i);
                        //ShowMessage("Nhập thành công " + i + " phụ kiện.", false);
                        else
                            showMsg("MSG_0203057");
                        //ShowMessage("Lỗi trong quá trình nhập dữ liệu.", false);
                    }
                    else
                        showMsg("MSG_0203058");
                    //ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        public string KiemTraTonTaiPhuKienNhap(PhuKienDangKyCollection PKCollection)
        {
            string st = "";
            string sophukien = "";
            Company.GC.BLL.KDT.GC.PhuKienDangKyCollection PKCollectionExit = new PhuKienDangKyCollection();
            foreach (PhuKienDangKy PK in PKCollection)
            {
                if (PK.checkExitsSoPK(PK.SoPhuKien, pkgcManageForm.MaHaiQuan, GlobalSettings.MA_DON_VI))
                {
                    PKCollectionExit.Add(PK);
                    if (sophukien.Trim().ToUpper() != PK.SoPhuKien.ToUpper())
                    {
                        sophukien = PK.SoPhuKien;
                        st += sophukien + ", ";
                    }
                }
            }
            foreach (PhuKienDangKy PK in PKCollectionExit)
            {
                PKCollection.Remove(PK);
            }
            return st;
        }


        public string KiemTraPhuKienDoanhNghiep(PhuKienDangKyCollection PKCollection)
        {
            string st = "";
            string sophukien = "";
            PhuKienDangKyCollection PKCollectionExit = new PhuKienDangKyCollection();
            foreach (PhuKienDangKy PK in PKCollection)
            {
                if (PK.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    PKCollectionExit.Add(PK);
                    if (sophukien.Trim().ToUpper() != PK.SoPhuKien.ToUpper())
                    {
                        sophukien = PK.SoPhuKien;
                        st += sophukien + ", ";
                    }
                }
            }
            foreach (PhuKienDangKy PK in PKCollectionExit)
            {
                PKCollection.Remove(PK);
            }
            return st;
        }

        #endregion

        //----------------------------------------------------------------------------

        private void ChangePassForm()
        {
            ChangePassForm f = new ChangePassForm();
            f.ShowDialog();
        }
        private void LoginUserKhac()
        {
            this.Hide();
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
            MainForm.EcsQuanTri = null;
            isLoginSuccess = false;
            if (versionHD == 0)
            {
                Login login = new Login();
                login.ShowDialog();
            }
            else if (versionHD == 1)
            {
                Company.Interface.DaiLy.Login login = new Company.Interface.DaiLy.Login();
                login.ShowDialog();
            }
            else if (versionHD == 2)
            {
                Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                login.ShowDialog();
            }
            if (isLoginSuccess)
            {
                //statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = "Người dùng: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                this.Show();
                if (versionHD == 0)
                {
                    if (MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem_GC.Management)))
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    else
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.KhaiDienTuNhap)))
                    {
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                else if (versionHD == 1)
                {
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                }
                else if (versionHD == 2)
                {
                    switch (typeLogin)
                    {
                        case 1: // User Da Cau Hinh
                            this.Show();
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 2: // User Chua Cau Hinh
                            this.Show();
                            DangKyForm dangKyForm = new DangKyForm();
                            dangKyForm.ShowDialog();
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 3:// Admin
                            this.Hide();
                            CreatAccountForm fAdminForm = new CreatAccountForm();
                            fAdminForm.ShowDialog();
                            this.ShowLoginForm();
                            break;
                    }
                }
                khoitao_GiaTriMacDinh();
            }
            else
                Application.Exit();
        }
        private void ShowQuanLyNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            NguoiDung = new QuanLyNguoiDung();
            NguoiDung.MdiParent = this;
            NguoiDung.Show();
        }
        private void TraCuuMaHSForm()
        {
            //HaiQuan.HS.MainForm f = new HaiQuan.HS.MainForm();
            //f.Show();
            ShowMaHSForm();
        }
        private void ShowQuanLyNhomNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNhomNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            NhomNguoiDung = new QuanLyNhomNguoiDung();
            NhomNguoiDung.MdiParent = this;
            NhomNguoiDung.Show();
        }
        private void ShowCauHinhToKhai()
        {
            CauHinhToKhaiForm f = new CauHinhToKhaiForm();
            f.ShowDialog();
        }
        private void ShowThietLapInBaoCao()
        {
            CauHinhInForm f = new CauHinhInForm();
            f.ShowDialog();
        }
        private void ShowRestoreForm()
        {
            RestoreForm f = new RestoreForm();
            f.ShowDialog();
        }
        private void ShowThietLapThongTinDNAndHQ()
        {
            ThongTinDNAndHQForm f = new ThongTinDNAndHQForm();
            f.ShowDialog();
            khoitao_GiaTriMacDinh();
        }
        private void ShowThietLapThongSoKetNoi()
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog();

            //Test webservice
            string msgError = "";
            bool webServiceConnection = Company.KDT.SHARE.Components.Globals.ServiceExists(GlobalSettings.DiaChiWS, false, out msgError);
            if (webServiceConnection)
                statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Blue;
            else
                statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Red;

        }
        private void ShowBackupAndRestore(bool isBackUp)
        {
            //BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            //f.isBackUp = isBackUp;
            //f.ShowDialog();
            BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            f.isBackUp = isBackUp;
            f.ShowDialog();
        }
        private void ShowCuaKhauForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_BorderGate"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_BorderGate f = new Company.Interface.VNACCS.Category.VNACC_Category_BorderGate();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("CuaKhauForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //ckForm = new CuaKhauForm();
            //ckForm.MdiParent = this;
            //ckForm.Show();
        }

        private void ShowDVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_QuantityUnit"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_QuantityUnit f = new Company.Interface.VNACCS.Category.VNACC_Category_QuantityUnit();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("DonViTinhForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //dvtForm = new DonViTinhForm();
            //dvtForm.MdiParent = this;
            //dvtForm.Show();
        }

        private void ShowDKGHForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DKGHForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dkghForm = new DKGHForm();
            dkghForm.MdiParent = this;
            dkghForm.Show();
        }
        private void ShowPTVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_TransportMeans"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_TransportMeans f = new Company.Interface.VNACCS.Category.VNACC_Category_TransportMeans();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("PTVTForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //ptvtForm = new PTVTForm();
            //ptvtForm.MdiParent = this;
            //ptvtForm.Show();
        }
        private void ShowPTTTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PTTTForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ptttForm = new PTTTForm();
            ptttForm.MdiParent = this;
            ptttForm.Show();
        }

        private void ShowNguyenTeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CurrencyExchange"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CurrencyExchange f = new Company.Interface.VNACCS.Category.VNACC_Category_CurrencyExchange();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("NguyenTeForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //ntForm = new NguyenTeForm();
            //ntForm.MdiParent = this;
            //ntForm.Show();
        }

        private void ShowNuocForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NuocForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            nuocForm = new NuocForm();
            nuocForm.MdiParent = this;
            nuocForm.Show();
        }

        private void ShowDVHQForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CustomsOffice"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CustomsOffice f = new Company.Interface.VNACCS.Category.VNACC_Category_CustomsOffice();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("DonViHaiQuanForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //dvhqForm = new DonViHaiQuanForm();
            //dvhqForm.MdiParent = this;
            //dvhqForm.Show();
        }

        private void ShowMaHSForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_HSCode"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_HSCode f = new Company.Interface.VNACCS.Category.VNACC_Category_HSCode();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("MaHSForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //maHSForm = new MaHSForm();
            //maHSForm.MdiParent = this;
            //maHSForm.Show();
        }

        private void DongBoDuLieuDN()
        {
            //             Form[] forms = this.MdiChildren;
            //             for (int i = 0; i < forms.Length; i++)
            //             {
            //                 if (forms[i].Name.ToString().Equals("SyncDataForm"))
            //                 {
            //                     forms[i].Activate();
            //                     return;
            //                 }
            //             }
            // 
            //             dongBoForm = new SyncDataForm();
            //             dongBoForm.MdiParent = this;
            //             dongBoForm.Show();

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("FrmDongBoDuLieu_VNACCS"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DBDLForm_VNACCS = new FrmDongBoDuLieu_VNACCS();
            DBDLForm_VNACCS.MdiParent = this;
            DBDLForm_VNACCS.Show();
        }
        private void UpdateStyleForAllForm(string style)
        {
            this.vsmMain.DefaultColorScheme = style;
            queueForm.vsmMain.DefaultColorScheme = style;
            if (tkmdForm != null)
                tkmdForm.vsmMain.DefaultColorScheme = style;
            if (tkmdManageForm != null)
                tkmdManageForm.vsmMain.DefaultColorScheme = style;
            if (maHSForm != null)
                maHSForm.vsmMain.DefaultColorScheme = style;
            if (ptttForm != null)
                ptttForm.vsmMain.DefaultColorScheme = style;
            if (ptvtForm != null)
                ptvtForm.vsmMain.DefaultColorScheme = style;
            if (dvtForm != null)
                dvtForm.vsmMain.DefaultColorScheme = style;
            if (dvhqForm != null)
                dvhqForm.vsmMain.DefaultColorScheme = style;
            if (ckForm != null)
                ckForm.vsmMain.DefaultColorScheme = style;
            if (ntForm != null)
                ntForm.vsmMain.DefaultColorScheme = style;
            if (dkghForm != null)
                dkghForm.vsmMain.DefaultColorScheme = style;
            if (nuocForm != null)
                nuocForm.vsmMain.DefaultColorScheme = style;

        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            statusBar.Panels["Support"].Visible = true;
            Int32 red;
            Int32 green;
            Int32 blue;
            Random rnd = new Random();
            red = rnd.Next(0, 255);
            green = rnd.Next(0, 255);
            blue = rnd.Next(0, 255);
            Color clr = Color.FromArgb(red, green, blue);
            statusBar.Panels["Support"].Alignment = HorizontalAlignment.Center;
            statusBar.Panels["Support"].FormatStyle.ForeColor = clr;
        }

        private void cmbMenu_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void explorerBar1_ItemClick(object sender, ItemEventArgs e)
        {

        }

        private void uiPanel0_SelectedPanelChanged(object sender, Janus.Windows.UI.Dock.PanelActionEventArgs e)
        {

        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void pmMain_MdiTabMouseDown(object sender, Janus.Windows.UI.Dock.MdiTabMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                e.Tab.Form.Activate();
                if (this.MdiChildren.Length == 1)
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                mnuRightClick.Show(this, e.X + pmMain.MdiTabGroups[0].Location.X, e.Y + 6);
            }
        }

        private void doCloseMe()
        {
            Form form = pmMain.MdiTabGroups[0].SelectedTab.Form;
            form.Close();
        }

        private void doCloseAllButMe()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i] != pmMain.MdiTabGroups[0].SelectedTab.Form)
                {
                    forms[i].Close();
                }
            }
        }

        private void doCloseAll()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {

                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                this.cmdDataVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != null || dataVersion != "" ? dataVersion : "?"));

                long sotk = ToKhaiMauDich.SelectCountSoTK(GlobalSettings.MA_DON_VI);
                if ((sotk % 2 == 0 && sotk > 0) || (sotk % 10 == 0 && sotk >= 10))
                {
                    string loaihinh = "ECSGC";
                    if (versionHD == 1)
                        loaihinh = "ECSDLGC";
                    if (versionHD != 2)
                        WebServiceConnection.sendThongTinDN(GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, "SoTK=" + sotk, loaihinh);
                }
                string error = KeySecurity.Active.Install.ECS_updateInfo(UpdateInfo(sotk.ToString()));
                if (!string.IsNullOrEmpty(error))
                {
                    Logger.LocalLogger.Instance().WriteMessage(error, null);
                }

                //Hungtq updated 23/05/2013.
                Company.KDT.SHARE.Components.Globals.KhoiTao_DanhMucChuanHQ_MaHS();

                //Test webservice
                string msgError = "";
                bool webServiceConnection = Company.KDT.SHARE.Components.Globals.ServiceExists(GlobalSettings.DiaChiWS, false, out msgError);
                if (webServiceConnection)
                    statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Blue;
                else
                    statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Red;

                AutoUpdate(string.Empty);

                if (!e.Cancel)
                {
                    //Hungtq complemented 14/12/2010
                    if (Company.KDT.SHARE.Components.Globals.GetTyGia() == true)
                    {
                        //timer1.Enabled = false;
                        MainForm.flag = 1;
                    }
                    else
                    {
                        MainForm.flag = 0;
                        soLanLayTyGia = 0;
                        Thread.Sleep(15000);
                    }
                }
                //timer1.Enabled = false;
            }
            catch { }
        }

        #region AutoUpdate ONLINE
        private void CreateShorcutAutoUpdate()
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + "AUTO UPDATE " + Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = AppDomain.CurrentDomain.BaseDirectory + "\\AppAutoUpdate.exe";
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        }
        private void CreateShorcut()
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        } 
        private void AutoUpdate(string args)
        {
            try
            {
                CreateShorcutAutoUpdate();
                //Kiem tra phien ban du lieu truoc khi cap nhat phien ban chuong trinh moi
                if (Helper.Controls.UpdateDatabaseSQLForm.Instance(true).IsHasNewDataVersion)
                {
                    UpdateNewDataVersion();
                }

                Company.KDT.SHARE.Components.DownloadUpdate dl = new DownloadUpdate(args);
                dl.DoDownload();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #region TẠO COMMAND TOOLBAR

        private delegate void CreateCommandCallback(string lastestVersion);
        private void CreateCommand(string lastestVersion)
        {
            try
            {
                this.cmbMenu.CommandManager = this.cmMain;
                this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                    this.cmdNewVersion
                });

                if (!this.cmMain.Commands.Contains("cmdNewVersion"))
                    this.cmMain.Commands.Add(this.cmdNewVersion);

                // 
                // cmdNewVersion
                // 
                this.cmdNewVersion.Key = "cmdNewVersion";
                this.cmdNewVersion.Name = "cmdNewVersion";
                this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;
                this.cmdNewVersion.TextAlignment = Janus.Windows.UI.CommandBars.ContentAlignment.MiddleCenter;
                this.cmdNewVersion.TextImageRelation = Janus.Windows.UI.CommandBars.TextImageRelation.ImageBeforeText;
                this.cmdNewVersion.ImageReplaceableColor = System.Drawing.Color.Pink;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //HUNGTQ, Update 11/06/2010.

        private Janus.Windows.UI.CommandBars.UICommand cmdNewVersion;

        /// <summary>
        /// Tạo command chứng từ đính kèm bổ sung.
        /// </summary>
        private void CreateCommandBosung(string lastestVersion)
        {
            #region Initialize
            //Tao nut command tren toolbar.
            this.cmdNewVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdNewVersion");

            //this.cmbMenu.CommandManager = this.cmMain;
            //this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            //    this.cmdNewVersion
            //});

            //if (!this.cmMain.Commands.Contains("cmdNewVersion"))
            //    this.cmMain.Commands.Add(this.cmdNewVersion);

            //// 
            //// cmdNewVersion
            //// 
            //this.cmdNewVersion.Key = "cmdNewVersion";
            //this.cmdNewVersion.Name = "cmdNewVersion";
            //this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;

            // Invoke the method that updates the form's label
            this.Invoke(new CreateCommandCallback(this.CreateCommand), new object[] { lastestVersion });

            #endregion

            cmdNewVersion.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdNewVersion_Click);

        }

        private void cmdNewVersion_Click(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                switch (e.Command.Key)
                {
                    case "cmdNewVersion":
                        AutoUpdate("MSG");
                        break;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #endregion

        #endregion

        #region Begin ECS Express

        private void Express()
        {
            try
            {
                string val = KeySecurity.KeyCode.Decrypt(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Ep"));

                if (!string.IsNullOrEmpty(val) && bool.Parse(val) == true)
                {
                    SetExpress(false);
                }
                else
                    SetExpress(true);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void SetExpress(bool visible)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Janus.Windows.UI.InheritableBoolean status = visible == true ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;

                /*Menu He thong*/
                cmdDongBoPhongKhai.Visible = cmdNhapXuat.Visible = status;    //Nhap du lieu tu doanh nghiep
                cmdRestore.Visible = status;            //Phuc hoi du lieu

                /*Menu Giao dien*/
                cmdEnglish.Visible = status;

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        #endregion End ECS Express

        private void grbMain_Click_1(object sender, EventArgs e)
        {

        }

        private void statusBar_PanelClick(object sender, Janus.Windows.UI.StatusBar.StatusBarEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                switch (e.Panel.Key)
                {
                    case "DoanhNghiep":
                    case "HaiQuan":
                    case "Terminal":
                    case "Service":
                    case "Version":
                        ShowThietLapThongTinDNAndHQ();
                        break;
                    case "CKS":
                        ChuKySo();
                        break;
                    case "Support":
                        Support();
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { this.Cursor = Cursors.Default; }

        }

        private void BindText()
        {
            if (statusBar.Panels["Support"].Visible == true)
            {
                statusBar.Panels["Support"].Visible = false;
            }
            else
            {
                statusBar.Panels["Support"].Visible = true;
                Int32 red;
                Int32 green;
                Int32 blue;
                Random rnd = new Random();
                red = rnd.Next(0, 255);
                green = rnd.Next(0, 255);
                blue = rnd.Next(0, 255);
                Color clr = Color.FromArgb(red,green,blue);
                statusBar.Panels["Support"].FormatStyle.BackColor = clr;
            }
        }
        private void Support()
        {
            frmSendSupport f = new frmSendSupport();
            f.ShowDialog(this);
        }

        private void statusBar_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void statusBar_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        /***VNACCS***/

        //Hoa Don
        private VNACC_HoaDonForm khaiBaoHoaDon;
        private VNACC_GiayPhepForm_SEA khaiBaoGiayPhep;
        private VNACC_GiayPhepManageForm giayPhepManage;
        private VNACC_HoaDonManageForm hoaDonManage;
        //to khai tri gia thap
        private VNACC_TKNhapTriGiaForm toKhaiNhapTriGia;
        private VNACC_TKXuatTriGiaForm toKhaiXuatTriGia;
        private VNACC_TKTriGiaManager toKhaiTriGiaManager;
        //To khai VNACC
        private VNACC_ToKhaiMauDichXuatForm toKhaiXuat;
        private VNACC_ToKhaiMauDichNhapForm toKhaiNhap;
        private VNACC_ToKhaiManager toKhaiManager;
        //To khai chuyen cua khau
        private VNACC_ChuyenCuaKhau toKhaiVC;
        private VNACC_ChuyenCuaKhau_ManagerForm toKhaiVCManager;

        // Dang ky hang miem thue TEA
        private VNACC_TEA_ManagerForm TEAManager;
        //Dang ky hang Tam nhap tai xuat
        private VNACC_TIA_ManagerForm TIAManager;

        private PhieuXuatNhapKhoManager PhieuXuatNhapKhoManager;

        private void explorerBarVNACCS_TKMD_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //KHO CFS
                case "KhaiBao_KhoCFS":
                    this.showKhaiBao_PK();
                    break;
                case "TheoDoi_KhoCFS":
                    this.showTheoDoiPK();
                    break;
                //To khai VNACC
                case "ToKhaiNhap":
                    this.showToKhaiNhap();
                    break;
                case "ToKhaiXuat":
                    this.showToKhaiXuat();
                    break;
                case "TheoDoiToKhai":
                    this.showTheoDoiToKhai();
                    break;
                case "TheoDoiChungTu":
                    this.showTheoDoiChungTuToKhai();
                    break;                    

                //To Khai Van chuyen OLA
                case "KhaiBao_TKVC":
                    this.showToKhaiVC();
                    break;
                case "TheoDoi_TKVC":
                    this.showTheoDoiToKhaiVC();
                    break;

                // To khai tri gia thap MIC
                case "ToKhaiNhapTriGiaThap":
                    this.showToKhaiNhapTriGia();
                    break;
                case "ToKhaiXuatTriGiaThap":
                    this.showToKhaiXuatTriGia();
                    break;
                case "TheoDoiTKTG":
                    this.showTheoDoiToKhaiTriGia();
                    break;

                // Dang ky hang mien thue TEA
                case "KhaiBaoTEA":
                    this.showTEA();
                    break;
                case "TheoDoiTEA":
                    this.showManagerTEA();
                    break;

                // Dang ky hang mien thue TIA
                case "KhaiBaoTIA":
                    this.showTIA();
                    break;
                case "TheoDoiTIA":
                    this.showManagerTIA();
                    break;

                //To khai sua bo sung thue cua hang hoa AMA
                case "KhaiBao_TK_KhaiBoSung_ThueHangHoa":
                    this.show_VNACC_TKBoSung_ThueHangHoaForm();
                    break;
                case "TheoDoi_TK_KhaiBoSung_ThueHangHoa":
                    this.show_VNACC_TKBoSung_ThueHangHoaManageForm();
                    break;

                //Chung tu thanh toan
                case "KhaiBao_CTTT_IAS":
                    this.Show_VNACC_CTTT_IAS();
                    break;
                case "KhaiBao_CTTT_IBA":
                    this.Show_VNACC_CTTT_IBA();
                    break;
                case "TheoDoi_ChungTuThanhToan":
                    this.Show_VNACC_CTTTManageForm();
                    break;

                //Chung tu dinh kem HYS
                case "KhaiBao_ChungTuKem_MSB":
                    this.show_VNACC_ChungTuKem_MSBForm();
                    break;
                case "KhaiBao_ChungTuKem_HYS":
                    this.show_VNACC_ChungTuKem_HYSForm();
                    break;
                case "TheoDoi_ChungTuKem":
                    this.show_VNACC_ChungTuKemManageForm();
                    break;
                case "cmdSignFile":
                    this.show_VNACC_SignFile();
                    break;
                case "TheoDoiPhieuXNKho":
                    this.show_PhieuXuatNhapKhoManageForm();
                    break;
                //Tờ khai vận chuyển đủ điều kiện qua KVGS
                case "KhaiBao_TKVCQKVGS":
                    this.showKhaiBao_TKVC();
                    break;
                case "TheoDoi_TKVCQKVGS":
                    this.showTheoDoiTKVC();
                    break;

                //Đinh danh hàng hóa
                case "cmdKhaiBaoDinhDanh":
                    this.showKhaiBao_DinhDanhHH();
                    break;
                case "cmdTheoDoiDinhDanh":
                    this.showTheoDoiDinhDanhHH();
                    break;

                //Tách vận đơn
                case "cmdKhaiBaoTachVanDon":
                    this.showKhaiBao_TachVanDon();
                    break;
                case "cmdTheoDoiTachVanDon":
                    this.showTheoDoiTachVanDon();
                    break;

                //Quản lý thu phí
                case "cmdHangContainer":
                    this.showToKhaiNP_HangContainer();
                    break;
                case "cmdHangRoi":
                    this.showToKhaiNP_HangRoi();
                    break;
                case "cmdDanhSachTKNP":
                    this.showDanhSachTKNP();
                    break;
                case "cmdTraCuuBL":
                    this.showTraCuuBL();
                    break;
                case "cmdRegisterInformation":
                    this.showRegisterInformation();
                    break;
                case "cmdRegisterManagement":
                    this.showRegisterManagement();
                    break;
                    
            }
        }

        private void showRegisterManagement()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("RegisterManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            RegisterManagementForm f = new RegisterManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showRegisterInformation()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_Register_Information"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_Register_Information f = new HP_Register_Information();
            f.MdiParent = this;
            f.Show();
        }

        private void showTraCuuBL()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_SearchInfomationForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_SearchInfomationForm f = new HP_SearchInfomationForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showDanhSachTKNP()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_ToKhaiNopPhiManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_ToKhaiNopPhiManagementForm f = new HP_ToKhaiNopPhiManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showToKhaiNP_HangRoi()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_ToKhaiNopPhiForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_ToKhaiNopPhiForm f = new HP_ToKhaiNopPhiForm();
            f.IsContainer = false;
            f.MdiParent = this;
            f.Show();
        }

        private void showToKhaiNP_HangContainer()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_ToKhaiNopPhiForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_ToKhaiNopPhiForm f = new HP_ToKhaiNopPhiForm();
            f.IsContainer = true;
            f.MdiParent = this;
            f.Show();
        }

        private void showTheoDoiTachVanDon()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_BillOfLadingManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_BillOfLadingManagementForm f = new VNACC_BillOfLadingManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showKhaiBao_TachVanDon()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_BillOfLadingForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_BillOfLadingForm f = new VNACC_BillOfLadingForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showTheoDoiDinhDanhHH()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhDanhHangHoaManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhDanhHangHoaManagerForm f = new DinhDanhHangHoaManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showKhaiBao_DinhDanhHH()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CapSoDinhDanhHangHoaForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            CapSoDinhDanhHangHoaForm f = new CapSoDinhDanhHangHoaForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_SignFile()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SignDigitalDocumentForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            SignDigitalDocumentForm f = new SignDigitalDocumentForm();
            f.MdiParent = this;
            f.Show();
        }
        private void showKhaiBao_TKVC()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TransportEquipmentForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_TransportEquipmentForm f = new VNACC_TransportEquipmentForm();
            f.MdiParent = this;
            f.Show();
        }
        private void showTheoDoiTKVC()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TransportEquipmentManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TransportEquipmentManagerForm f = new TransportEquipmentManagerForm();
            f.MdiParent = this;
            f.Show();
        }
        private void explorerBarVNACCS_GiayPhep_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Giay phep SEA
                case "KhaiBao_GiayPhep_SEA":
                    this.show_VNACC_GiayPhepForm_SEA();
                    break;
                case "TheoDoi_GiayPhep_SEA":
                    this.show_VNACC_GiayPhepManageForm_SEA();
                    break;

                //Giay phep SFA
                case "KhaiBao_GiayPhep_SFA":
                    this.show_VNACC_GiayPhepForm_SFA();
                    break;
                case "TheoDoi_GiayPhep_SFA":
                    this.show_VNACC_GiayPhepManageForm_SFA();
                    break;

                //Giay phep SAA
                case "KhaiBao_GiayPhep_SAA":
                    this.show_VNACC_GiayPhepForm_SAA();
                    break;
                case "TheoDoi_GiayPhep_SAA":
                    this.show_VNACC_GiayPhepManageForm_SAA();
                    break;

                //Giay phep SMA
                case "KhaiBao_GiayPhep_SMA":
                    this.show_VNACC_GiayPhepForm_SMA();
                    break;
                case "TheoDoi_GiayPhep_SMA":
                    this.show_VNACC_GiayPhepManageForm_SMA();
                    break;
                case "TheoDoiPhieuXNKho":
                    this.show_PhieuXuatNhapKhoManageForm();
                    break;
            }
        }

        private void show_PhieuXuatNhapKhoManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhieuXuatNhapKhoManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.isQuyetToan = false;
            f.ShowDialog();
            if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
            {
                PhieuXuatNhapKhoManager f1 = new PhieuXuatNhapKhoManager();
                f1.MdiParent = this;
                f1.HopDong = f.HopDongSelected;
                f1.Show();
            }
            else
            {
                PhieuXuatNhapKhoManager f1 = new PhieuXuatNhapKhoManager();
                f1.MdiParent = this;
                //f1.HopDong = f.HopDongSelected;
                f1.Show();
            }

        }
        private void explorerBarVNACCS_HoaDon_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Hoa don
                case "KhaiBaoHoaDon":
                    this.show_VNACC_HoaDonForm();
                    break;
                case "TheoDoiHoaDon":
                    this.show_VNACC_HoaDonManageForm();
                    break;
            }
        }

        #region Khai bao sua doi/ bo sung thue hang hoa

        private void show_VNACC_TKBoSung_ThueHangHoaManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TKBoSung_ThueHangHoaManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_TKBoSung_ThueHangHoaManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_TKBoSung_ThueHangHoaForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TKBoSung_ThueHangHoaForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_TKBoSung_ThueHangHoaForm();
            f.MdiParent = this;
            f.Show();
        }

        #endregion
        //private void show_PhieuXuatNhapKhoManageForm()
        //{
        //    Form[] forms = this.MdiChildren;
        //    for (int i = 0; i < forms.Length; i++)
        //    {
        //        if (forms[i].Name.ToString().Equals("PhieuXuatNhapKhoManager"))
        //        {
        //            forms[i].Activate();
        //            return;
        //        }
        //    }
        //    BaseForm f = new PhieuXuatNhapKhoManager();
        //    f.MdiParent = this;
        //    f.Show();
        //}
        #region VNACC Chung tu kem
        private void show_VNACC_ChungTuKemManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuKemManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuKemManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_ChungTuKem_MSBForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("VNACC_ChungTuDinhKemForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            BaseForm f = new VNACC_ChungTuDinhKemForm(ELoaiThongTin.MSB);
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_ChungTuKem_HYSForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("VNACC_ChungTuDinhKemForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            BaseForm f = new VNACC_ChungTuDinhKemForm(ELoaiThongTin.HYS);
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        // Hoa don
        #region Hoa Don
        private void show_VNACC_HoaDonForm()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("KhaiBaoHoaDon"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            khaiBaoHoaDon = new VNACC_HoaDonForm();
            khaiBaoHoaDon.MdiParent = this;
            khaiBaoHoaDon.Show();
        }
        private void show_VNACC_HoaDonManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_HoaDonManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hoaDonManage = new VNACC_HoaDonManageForm();
            hoaDonManage.MdiParent = this;
            hoaDonManage.Show();
        }
        #endregion

        //giay phep
        #region Giay Phep
        private void show_VNACC_GiayPhepForm_SEA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepForm_SEA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            khaiBaoGiayPhep = new VNACC_GiayPhepForm_SEA();
            khaiBaoGiayPhep.MdiParent = this;
            khaiBaoGiayPhep.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SEA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            giayPhepManage = new VNACC_GiayPhepManageForm();
            giayPhepManage.MdiParent = this;
            giayPhepManage.Show();
        }

        private void show_VNACC_GiayPhepForm_SFA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepForm_SFA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepForm_SFA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SFA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm_SFA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepManageForm_SFA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepForm_SAA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepForm_SAA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepForm_SAA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SAA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm_SAA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepManageForm_SAA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepForm_SMA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepForm_SMA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepForm_SMA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SMA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm_SMA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepManageForm_SMA();
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        #region Chung tu thanh toan

        private void Show_VNACC_CTTT_IAS()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuThanhToanForm_IAS"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuThanhToanForm_IAS();
            f.MdiParent = this;
            f.Show();
        }

        private void Show_VNACC_CTTT_IBA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuThanhToanForm_IBA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuThanhToanForm_IBA();
            f.MdiParent = this;
            f.Show();
        }

        private void Show_VNACC_CTTTManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuThanhToanManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuThanhToanManageForm();
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        #region To khai VNACC
        private void showToKhaiNhap()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiNhap"))
                {
                    forms[i].Activate();
                    return;
                }
            }
#if GC_V4
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
            {
                KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                TKMD.HopDong_ID = f.HopDongSelected.ID;
                string sohopdong = string.Empty;
                if (f.HopDongSelected.ActionStatus == 999)
                    sohopdong = "1";
                else
                    sohopdong = "2";
                if (f.HopDongSelected.MaHaiQuan != "" && f.HopDongSelected.MaHaiQuan.Length >= 4)
                    sohopdong = sohopdong + f.HopDongSelected.MaHaiQuan.Substring(1, 2);
                if (f.HopDongSelected.NgayDangKy.Year > 1900)
                    sohopdong = sohopdong + f.HopDongSelected.NgayDangKy.Year.ToString().Trim();
                if (f.HopDongSelected.SoTiepNhan != 0)
                    sohopdong = sohopdong + f.HopDongSelected.SoTiepNhan.ToString().Trim();
                TKMD.HopDong_So = sohopdong;

                toKhaiNhap = new VNACC_ToKhaiMauDichNhapForm();
                toKhaiNhap.HD = f.HopDongSelected;
                toKhaiNhap.soHD = f.HopDongSelected.SoHopDong;
                toKhaiNhap.ngayHD = f.HopDongSelected.NgayKy;
                toKhaiNhap.TKMD = TKMD;
                toKhaiNhap.MdiParent = this;
                toKhaiNhap.Show();
            }
            else
            {
                toKhaiNhap = new VNACC_ToKhaiMauDichNhapForm();
                toKhaiNhap.MdiParent = this;
                toKhaiNhap.Show();
            }
#elif KD_V4 ||SXXK_V4
            toKhaiNhap = new VNACC_ToKhaiMauDichNhapForm();
            toKhaiNhap.MdiParent = this;
            toKhaiNhap.Show();
#endif
        }

        private void showToKhaiXuat()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiXuat"))
                {
                    forms[i].Activate();
                    return;
                }
            }
#if GC_V4
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
            {
                KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                TKMD.HopDong_ID = f.HopDongSelected.ID;
                string sohopdong = string.Empty;
                if (f.HopDongSelected.ActionStatus == 999)
                    sohopdong = "1";
                else
                    sohopdong = "2";
                if (f.HopDongSelected.MaHaiQuan != "" && f.HopDongSelected.MaHaiQuan.Length >= 4)
                    sohopdong = sohopdong + f.HopDongSelected.MaHaiQuan.Substring(1, 2);
                if (f.HopDongSelected.NgayDangKy.Year > 1900)
                    sohopdong = sohopdong + f.HopDongSelected.NgayDangKy.Year.ToString().Trim();
                if (f.HopDongSelected.SoTiepNhan != 0)
                    sohopdong = sohopdong + f.HopDongSelected.SoTiepNhan.ToString().Trim();
                TKMD.HopDong_So = sohopdong;

                toKhaiXuat = new VNACC_ToKhaiMauDichXuatForm();
                toKhaiXuat.HD = f.HopDongSelected;
                toKhaiXuat.TKMD = TKMD;
                toKhaiXuat.soHD = f.HopDongSelected.SoHopDong;
                toKhaiXuat.ngayHD = f.HopDongSelected.NgayKy;
                toKhaiXuat.MdiParent = this;
                toKhaiXuat.Show();
            }
            else
            {
                toKhaiXuat = new VNACC_ToKhaiMauDichXuatForm();
                toKhaiXuat.MdiParent = this;
                toKhaiXuat.Show();
            }
#elif KD_V4 ||SXXK_V4
            toKhaiXuat = new VNACC_ToKhaiMauDichXuatForm();
            toKhaiXuat.MdiParent = this;
            toKhaiXuat.Show();
#endif

        }


        private void showTheoDoiChungTuToKhai()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_VouchersToKhaiManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_VouchersToKhaiManager toKhaiManager = new VNACC_VouchersToKhaiManager();
            toKhaiManager.MdiParent = this;
            toKhaiManager.Show();

        }
        private void showTheoDoiToKhai()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ToKhaiManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiManager = new VNACC_ToKhaiManager();
            toKhaiManager.MdiParent = this;
            toKhaiManager.Show();

        }
        private void showToKhaiVC()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChuyenCuaKhau"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiVC = new VNACC_ChuyenCuaKhau();
            toKhaiVC.MdiParent = this;
            toKhaiVC.Show();
        }
        private void showTheoDoiToKhaiVC()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChuyenCuaKhau_ManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiVCManager = new VNACC_ChuyenCuaKhau_ManagerForm();
            toKhaiVCManager.MdiParent = this;
            toKhaiVCManager.Show();

        }

        private void showTEA()
        {

            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("KhaiBaoTEA"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            //TEAForm = new VNACC_TEAForm();
            //TEAForm.MdiParent = this;
            //TEAForm.Show();
            VNACC_TEAForm f = new VNACC_TEAForm();
            f.ShowDialog();
        }
        private void showManagerTEA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TEA_ManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TEAManager = new VNACC_TEA_ManagerForm();
            TEAManager.MdiParent = this;
            TEAManager.Show();
        }
        private void showTIA()
        {
            VNACC_TIAForm f = new VNACC_TIAForm();
            f.ShowDialog();
        }
        private void showManagerTIA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TIA_ManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TIAManager = new VNACC_TIA_ManagerForm();
            TIAManager.MdiParent = this;
            TIAManager.Show();
        }
        #endregion

        #region To khai tri gia thap
        private void showToKhaiNhapTriGia()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiNhapTriGiaThap"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiNhapTriGia = new VNACC_TKNhapTriGiaForm();
            toKhaiNhapTriGia.MdiParent = this;
            toKhaiNhapTriGia.Show();
        }
        private void showToKhaiXuatTriGia()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiXuat"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiXuatTriGia = new VNACC_TKXuatTriGiaForm();
            toKhaiXuatTriGia.MdiParent = this;
            toKhaiXuatTriGia.Show();
        }
        private void showTheoDoiToKhaiTriGia()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TKTriGiaManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiTriGiaManager = new VNACC_TKTriGiaManager();
            toKhaiTriGiaManager.MdiParent = this;
            toKhaiTriGiaManager.Show();
        }
        #endregion

        public void ShowFormThongBao(object sender, EventArgs e)
        {
            if (!frmThongBao.Visible)
            {
                frmThongBao.TopMost = true;
                frmThongBao.BringToFront();
                frmThongBao.Visible = true;
            }
        }
        public void show_QuanLyMessageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyMessageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            quanLyMsg = new Company.Interface.VNACCS.QuanLyMessageForm();
            quanLyMsg.MdiParent = this;
            quanLyMsg.Show();
        }
        private void showTheoDoiQuyetToan()
        {
            try
            {
                //if (GlobalSettings.MA_DON_VI == "4000395355")
                //{
                    this.Cursor = Cursors.WaitCursor;
                    Form[] forms = this.MdiChildren;
                    for (int i = 0; i < forms.Length; i++)
                    {
                        if (forms[i].Name.ToString().Equals("GoodItemsManagerForm"))
                        {
                            forms[i].Activate();
                            return;
                        }
                    }
                    GoodItemsManagerForm GoodItem = new GoodItemsManagerForm();
                    GoodItem.MdiParent = this;
                    GoodItem.Show();   
                //}
                //else
                //{
                //    this.Cursor = Cursors.WaitCursor;
                //    Form[] forms = this.MdiChildren;
                //    for (int i = 0; i < forms.Length; i++)
                //    {
                //        if (forms[i].Name.ToString().Equals("HoSoQuyetToanManagerForm"))
                //        {
                //            forms[i].Activate();
                //            return;
                //        }
                //    }
                //    HoSoQuyetToanManagerForm xnt = new HoSoQuyetToanManagerForm();
                //    xnt.MdiParent = this;
                //    xnt.Show();                
                //}



            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }
        private void showHopDongQuyetToan()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("XuatNhapTonForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                HopDongManageForm f = new HopDongManageForm();
                f.IsBrowseForm = true;
                f.IsDaDuyet = true;
                f.isQuyetToan = true;
                f.ShowDialog();
                if (!string.IsNullOrEmpty(f.ListHopDongSelected))
                {
                    XuatNhapTonForm xnt = new XuatNhapTonForm();
                    xnt.ListHD = f.ListHopDongSelected;
                    xnt.ListID_HopDong = f.ListID_HopDongSelected;
                    xnt.MdiParent = this;
                    xnt.Show();

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }
        private void showCreatBCQTMMTB()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("BaoCaoQuyetToanMMTB"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaoCaoQuyetToanMMTB form = new BaoCaoQuyetToanMMTB();
            form.MdiParent = this;
            form.IsActive = false;
            form.Show();
            #region
            //string LIST_HOPDONG_ID = "";
            //try
            //{
            //    //if (GlobalSettings.MA_DON_VI == "4000395355")
            //    //{
            //    HopDongManageForm f = new HopDongManageForm();
            //    f.isQuyetToan = true;
            //    //f.isTaoHoSo = true;
            //    f.IsBrowseForm = true;
            //    f.ShowDialog();
            //    LIST_HOPDONG_ID = f.LIST_HOPDONG_ID;
            //    if (!String.IsNullOrEmpty(LIST_HOPDONG_ID))
            //    {
            //        Form[] forms = this.MdiChildren;
            //        for (int i = 0; i < forms.Length; i++)
            //        {
            //            if (forms[i].Name.ToString().Equals("BaoCaoQuyetToanMMTB"))
            //            {
            //                forms[i].Activate();
            //                return;
            //            }
            //        }
            //        BaoCaoQuyetToanMMTB form = new BaoCaoQuyetToanMMTB();
            //        form.LIST_HOPDONG_ID = LIST_HOPDONG_ID;
            //        form.MdiParent = this;
            //        form.IsActive = false;
            //        form.Show();
            //    }
            //    else
            //    {
            //        ShowMessage("Bạn chưa chọn hợp đồng để đưa vào quyết toán .", false);
            //    }
            //}
            //catch (Exception ex)
            //{

            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            #endregion
        }
        private void showTheoDoiNPLTon()
        {
            string LIST_HOPDONG_ID = "";
            List<HopDong> Collection = new List<HopDong>();
            try
            {

                HopDongManageForm f = new HopDongManageForm();
                f.isQuyetToan = true;
                //f.isTaoHoSo = true;
                f.IsBrowseForm = true;
                f.ShowDialog();
                Collection = f.list_HopDong;
                LIST_HOPDONG_ID = f.LIST_HOPDONG_ID;
                if (Collection.Count >0)
                {
                    Form[] forms = this.MdiChildren;
                    for (int i = 0; i < forms.Length; i++)
                    {
                        if (forms[i].Name.ToString().Equals("ThongKeNPLTonForm"))
                        {
                            forms[i].Activate();
                            return;
                        }
                    }
                    ThongKeNPLTonForm TKNPL = new ThongKeNPLTonForm();
                    TKNPL.MdiParent = this;
                    TKNPL.Collection = Collection;
                    TKNPL.LIST_HOPDONG_ID = LIST_HOPDONG_ID;
                    TKNPL.Show();
                }
                else
                {
                    ShowMessage("Bạn chưa chọn hợp đồng để xử lý .", false);
                }
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void showTheoDoiBCQTMMTB()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("BaoCaoQuyetToanMMTBManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaoCaoQuyetToanMMTBManagementForm f = new BaoCaoQuyetToanMMTBManagementForm();
            f.MdiParent = this;
            f.Show();
        }
        private void showCreateHopDongQuyetToan()
        {
            string LIST_HOPDONG_ID = "";
            try
            {
                //if (GlobalSettings.MA_DON_VI == "4000395355")
                //{
                    HopDongManageForm f = new HopDongManageForm();
                    f.isQuyetToan = true;
                    f.isTaoHoSo = true;
                    f.IsBrowseForm = true;
                    f.ShowDialog();
                    LIST_HOPDONG_ID = f.LIST_HOPDONG_ID;
                    if (!String.IsNullOrEmpty(LIST_HOPDONG_ID))
                    {
                        Form[] forms = this.MdiChildren;
                        for (int i = 0; i < forms.Length; i++)
                        {
                            if (forms[i].Name.ToString().Equals("BaoCaoQuyetToanHopDong"))
                            {
                                forms[i].Activate();
                                return;
                            }
                        }
                        BaoCaoQuyetToanHopDong BCQuyeToan = new BaoCaoQuyetToanHopDong();
                        BCQuyeToan.MdiParent = this;
                        BCQuyeToan.LIST_HOPDONG_ID = LIST_HOPDONG_ID;
                        BCQuyeToan.NAMQUYETTOAN = f.NAMQUYETTOAN;
                        BCQuyeToan.IsActive = false;
                        BCQuyeToan.Show();
                    }
                    else
                    {
                        ShowMessage("Bạn chưa chọn hợp đồng để đưa vào quyết toán .", false);
                    }
                //}
                //else
                //{
                //    Form[] forms = this.MdiChildren;
                //    for (int i = 0; i < forms.Length; i++)
                //    {
                //        if (forms[i].Name.ToString().Equals("XuatNhapTonForm"))
                //        {
                //            forms[i].Activate();
                //            return;
                //        }
                //    }
                //    XuatNhapTonForm xnt = new XuatNhapTonForm();
                //    xnt.MdiParent = this;
                //    xnt.isUpdate = false;
                //    xnt.isDaQuyetToan = true;
                //    xnt.Show();
                //}
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private void explorerBarVNACCS_ChungTu_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                    // Khai báo cơ sở sản xuất
                case "Send" :
                    this.StorageAreasProduction();
                    break;
                case "Manager" :
                    this.ManagerStorageAreasProduction();
                    break;
                //Phiếu xuất nhập kho
                case "PhieuNhapKho":
                    this.wareHouseImport();
                    break;
                case "PhieuXuatKho":
                    this.wareHouseExport();
                    break;
                case "TheoDoiPhieuKho":
                    this.wareHouseManagement();
                    break;
                case "cmdBaoCaoChotTon":
                    this.BaoCaoChotTon();
                    break;
                case "cmdTheoDoiBCCT":
                    this.TheoDoiBCCT();
                    break;
                case "cmdQuanLyMessage":
                    this.QuanLyMessage();
                    break;                    
                default:
                    break;
            }
        }

        private void QuanLyMessage()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("QuanLyMessage"))
                {
                    form[i].Activate();
                    return;
                }
            }

            QuanLyMessage f = new QuanLyMessage();
            f.MdiParent = this;
            f.Show();
        }

        private void TheoDoiBCCT()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("BaoCaoChotTonManagementForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            BaoCaoChotTonManagementForm f = new BaoCaoChotTonManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoChotTon()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("BaoCaoChotTonGCForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            BaoCaoChotTonGCForm f = new BaoCaoChotTonGCForm();
            f.MdiParent = this;
            f.Show();
        }
        private void StorageAreasProduction()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("ManufactureFactories"))
                {
                    form[i].Activate();
                    return;
                }
            }

            ManufactureFactories f = new ManufactureFactories();
            f.MdiParent = this;
            f.Show();
        }
        private void ManagerStorageAreasProduction()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("StorageAreasProductionManagerForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            StorageAreasProductionManagerForm f = new StorageAreasProductionManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void explorerBarVNACC_KetXuatDuLieu_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Tờ khai VNACCS
                case "ToKhaiXuatNhapKhau":
                    this.ThongKeToKhaiXNK();
                    break;
                case "ToKhaiAMA":
                    this.ThongKeToKhaiAMA();
                    break;
                case "TongHangHoaXNK":
                    this.BaoCaoTongHopHHXNK();
                    break;
                case "ChiTietHangHoaXNK":
                    this.BaoCaoChiTietHHXNK();
                    break;
                // Báo cáo tờ khai VNACCS
                case "TongHopXNT":
                    this.BaoCaoTongHopXNT();
                    break;
                case "QuyDoiNPLSP":
                    this.BaoCaoTongHopQuyDoiNPLSP();
                    break;

                //Tờ khai mậu dịch V4
                case "ThongKeTKXNK":
                    this.ThongKeToKhaiXNKV4();
                    break;
                case "BaoCaoTongHopHHXNK":
                    this.BaoCaoTongHopHHXNKV4();
                    break;
                case "BaoCaoChiTietHHXNK":
                    this.BaoCaoChiTietHHXNKV4();
                    break;


            }
        }

        private void BaoCaoChiTietHHXNKV4()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("V4_BaoCaoChiTietHHXNKForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            V4_BaoCaoChiTietHHXNKForm f = new V4_BaoCaoChiTietHHXNKForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoTongHopHHXNKV4()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("V4_BaoCaoTongHopHHXNK"))
                {
                    form[i].Activate();
                    return;
                }
            }

            V4_BaoCaoTongHopHHXNK f = new V4_BaoCaoTongHopHHXNK();
            f.MdiParent = this;
            f.Show();
        }

        private void ThongKeToKhaiXNKV4()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("V4_ThongKeTKXNKForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            V4_ThongKeTKXNKForm f = new V4_ThongKeTKXNKForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoTongHopQuyDoiNPLSP()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_BaoCaoTongHopQuyDoiNPLSPForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_BaoCaoTongHopQuyDoiNPLSPForm f = new VNACC_BaoCaoTongHopQuyDoiNPLSPForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoTongHopXNT()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_BaoCaoTongHopXNTForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_BaoCaoTongHopXNTForm f = new VNACC_BaoCaoTongHopXNTForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoChiTietHHXNK()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_BaoCaoChiTietHHXNKForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_BaoCaoChiTietHHXNKForm f = new VNACC_BaoCaoChiTietHHXNKForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoTongHopHHXNK()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_BaoCaoTongHHXNKForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_BaoCaoTongHHXNKForm f = new VNACC_BaoCaoTongHHXNKForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ThongKeToKhaiAMA()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_ThongKeToKhaiAMAForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_ThongKeToKhaiAMAForm f = new VNACC_ThongKeToKhaiAMAForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ThongKeToKhaiXNK()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_ThongKeToKhaiXNKForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_ThongKeToKhaiXNKForm f = new VNACC_ThongKeToKhaiXNKForm();
            f.MdiParent = this;
            f.Show();
        }

        private void explorerBarKhoKeToan_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                // Cấu hình
                case "cmdCauHinh":
                    this.CauHinhKhoKeToan();
                    break;
                // Danh sách kho 
                case "cmdDanhSachKho":
                    this.DanhSachKhoKeToan();
                    break;
                case "cmdTheoDoiKho":
                    this.TheoDoiKhoKeToan();
                    break;
                // Danh mục kho
                case "cmdKhoNPL":
                    this.KhoNPL();
                    break;
                case "cmdKhoSanPham":
                    this.KhoSanPham();
                    break;
                case "cmdKhoThietBi":
                    this.KhoThietBi();
                    break;
                case "cmdKhoHangMau":
                    this.KhoHangMau();
                    break;
                // Phiếu xuất nhập kho
                case "cmdPhieuNhapKho":
                    this.PhieuNhapKhoKeToan();
                    break;
                case "cmdTheoDoiPNK":
                    this.TheoDoiPNK();
                    break;
                case "cmdPhieuXuatKho":
                    this.PhieuXuatKhoKeToan();
                    break;
                case "cmdTheoDoiPXK":
                    this.cmdTheoDoiPXK();
                    break;
                    // Định mức
                case "cmdTheoDoiDinhMuc":
                    this.TheoDoiDM();
                    break;
                case "cmdThemMoiDM":
                    this.ThemMoiDM();
                    break;
                case "cmdTinhToan":
                    this.TinhToanDinhMucSPNPL();
                    break;

                // Báo cáo 
                case "cmdTheKho":
                    this.BaoCaoTheKho();
                    break;
                case "cmdBaoCaoQT":
                    this.BaoCaoQuyetToan();
                    break;
                case "cmdCapNhatTonDK":
                    this.CapNhatTonDauKy();
                    break;

            }
        }

        private void CapNhatTonDauKy()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseUpdateQuantityForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseUpdateQuantityForm f = new WareHouseUpdateQuantityForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoQuyetToan()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseSettlementReportForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseSettlementReportForm f = new WareHouseSettlementReportForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoTheKho()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WarehouseCardReportForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WarehouseCardReportForm f = new WarehouseCardReportForm();
            f.MdiParent = this;
            f.Show();
        }

        private void TinhToanDinhMucSPNPL()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseCalculateSampleDataForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseCalculateSampleDataForm f = new WareHouseCalculateSampleDataForm();
            f.MdiParent = this;
            f.Show();
        }

        private void TheoDoiDM()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseDinhMucManagementForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseDinhMucManagementForm f = new WareHouseDinhMucManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ThemMoiDM()
        {
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected != null)
            {
                Form[] form = this.MdiChildren;
                for (int i = 0; i < form.Length; i++)
                {
                    if (form[i].Name.ToString().Equals("WareHouseDinhMucMaper"))
                    {
                        form[i].Activate();
                        return;
                    }
                }
                WareHouseDinhMucMaper frm = new WareHouseDinhMucMaper();
                frm.MdiParent = this;
                frm.HD = f.HopDongSelected;
                frm.Show();

            }
            else
            {
                ShowMessage("Bạn chưa chọn Hợp đồng .",false);
            }
        }

        private void CauHinhKhoKeToan()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("ConfigPrefix"))
                {
                    form[i].Activate();
                    return;
                }
            }

            ConfigPrefix f = new ConfigPrefix();
            f.MdiParent = this;
            f.Show();
        }

        private void DanhSachKhoKeToan()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseRegisterForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseRegisterForm f = new WareHouseRegisterForm();
            f.MdiParent = this;
            f.Show();
        }

        private void TheoDoiKhoKeToan()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseRegisterManagementForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseRegisterManagementForm f = new WareHouseRegisterManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void KhoNPL()
        {


            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseNPLMaperManagement"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseNPLMaperManagement f = new WareHouseNPLMaperManagement();
            f.MdiParent = this;
            f.Show();
        }

        private void KhoSanPham()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseSPMaperManagementForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseSPMaperManagementForm f = new WareHouseSPMaperManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void KhoThietBi()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseNPLMaperManagement"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseNPLMaperManagement f = new WareHouseNPLMaperManagement();
            f.MdiParent = this;
            f.Show();
        }

        private void KhoHangMau()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseNPLMaperManagement"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseNPLMaperManagement f = new WareHouseNPLMaperManagement();
            f.MdiParent = this;
            f.Show();
        }

        private void PhieuNhapKhoKeToan()
        {
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected != null && f.HopDongSelected.ID > 0)
            {
                Form[] form = this.MdiChildren;
                for (int i = 0; i < form.Length; i++)
                {
                    if (form[i].Name.ToString().Equals("WareHouseExportForm"))
                    {
                        form[i].Activate();
                        return;
                    }
                }

                WareHouseExportForm frm = new WareHouseExportForm();
                frm.MdiParent = this;
                frm.HD = f.HopDongSelected;
                frm.LOAICT = "N";
                frm.Show();

            }
        }

        private void TheoDoiPNK()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseManager"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseManager f = new WareHouseManager();
            f.MdiParent = this;
            f.Show();
        }

        private void PhieuXuatKhoKeToan()
        {
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected != null && f.HopDongSelected.ID > 0)
            {
                Form[] form = this.MdiChildren;
                for (int i = 0; i < form.Length; i++)
                {
                    if (form[i].Name.ToString().Equals("WareHouseExportForm"))
                    {
                        form[i].Activate();
                        return;
                    }
                }

                WareHouseExportForm frm = new WareHouseExportForm();
                frm.MdiParent = this;
                frm.HD = f.HopDongSelected;
                frm.LOAICT = "X";
                frm.Show();

            }
        }

        private void cmdTheoDoiPXK()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseManager"))
                {
                    form[i].Activate();
                    return;
                }
            }
            WareHouseManager f = new WareHouseManager();
            f.MdiParent = this;
            f.Show();
        }
    }
}