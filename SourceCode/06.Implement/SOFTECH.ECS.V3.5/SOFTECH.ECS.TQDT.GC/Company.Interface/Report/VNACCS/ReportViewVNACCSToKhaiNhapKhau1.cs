﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.LogMessages;
using System.Collections.Generic;
using System.Diagnostics;
using BarcodeLib;

namespace Company.Interface.Report.VNACCS
{
    public partial class ReportViewVNACCSToKhaiNhapKhau1 : DevExpress.XtraReports.UI.XtraReport
    {
        int count;
        public ReportViewVNACCSToKhaiNhapKhau1()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = false,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 30,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        public void BindingReport( KDT_VNACC_ToKhaiMauDich tkmd)
        {
            //lblSoTrang.Text = ;
            try
            {
                pictureBox1.Image = GenerateBarCode(tkmd.SoToKhai.ToString());
                lblTenThongTinXuat.Text = "Tờ khai hàng hóa nhập khẩu (thông quan)";
                count = tkmd.HangCollection.Count;
                lblSoToKhai.Text = tkmd.SoToKhai.ToString();
                lblSoToKhaiDauTien.Text = String.IsNullOrEmpty(tkmd.SoToKhaiDauTien) ? "" : tkmd.SoToKhaiDauTien.ToString();
                lblSoNhanhToKhaiChiaNho.Text = tkmd.SoNhanhToKhai.ToString();
                lblSoToKhaiTamNhapTaiXuat.Text = tkmd.SoToKhaiTNTX.ToString();
                lblMaPhanLoaiKiemTra.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiKiemTra) ? "" : tkmd.MaPhanLoaiKiemTra.ToString();
                lblMaLoaiHinh.Text = String.IsNullOrEmpty(tkmd.MaLoaiHinh) ? "" : tkmd.MaLoaiHinh.ToString();
                lblMaPhanLoaiHangHoa.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiHH) ? "" : tkmd.MaPhanLoaiHH.ToString();
                lblMaHieuPhuongThucVanChuyen.Text = String.IsNullOrEmpty(tkmd.MaPhuongThucVT) ? "" : tkmd.MaPhuongThucVT.ToString();
                if (!String.IsNullOrEmpty(tkmd.PhanLoaiToChuc))
                {
                    lblPhanLoaiCaNhanToChuc.Text = tkmd.PhanLoaiToChuc.ToString();
                }
                else
                {
                    lblPhanLoaiCaNhanToChuc.Text = "";
                }
                //lblMaSoHangHoaDaiDienToKhai.Text = ;
                lblTenCoQuanHaiQuanTiepNhanToKhai.Text = tkmd.CoQuanHaiQuan.ToString();
                //lblMaBoPhanXuLyToKhai.Text =
                if (tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayDangKy.Text = tkmd.NgayDangKy.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayDangKy.Text = "";

                }
                if (tkmd.NgayDangKy.Hour.ToString() != "" && tkmd.NgayDangKy.Hour.ToString() != "0")
                {
                    lblGioDangKy.Text = tkmd.NgayDangKy.Hour.ToString() + ":" + tkmd.NgayDangKy.Minute.ToString() + ":" + tkmd.NgayDangKy.Second.ToString();// vad1ac.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
                }
                else
                {
                    lblGioDangKy.Text = "";
                }
                if (tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayThayDoiDangKy.Text = tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayThayDoiDangKy.Text = "";
                }
                if (tkmd.NgayThayDoiDangKy.Hour.ToString() != "" && tkmd.NgayThayDoiDangKy.Hour.ToString().ToUpper() != "0")
                {
                    lblGioThayDoiDangKy.Text = tkmd.NgayThayDoiDangKy.Hour.ToString() + ":" + tkmd.NgayThayDoiDangKy.Minute.ToString() + ":" + tkmd.NgayThayDoiDangKy.Second.ToString();//vad1ac.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
                }
                else
                {
                    lblGioThayDoiDangKy.Text = "";
                }
                if (tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblThoiHanTaiNhapTaiXuat.Text = tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblThoiHanTaiNhapTaiXuat.Text = "";
                }
                lblBieuThiTHHetHan.Text = String.IsNullOrEmpty(tkmd.BieuThiTruongHopHetHan) ? "" : tkmd.BieuThiTruongHopHetHan.ToString().ToUpper();
                lblMaNguoiNhapKhau.Text = String.IsNullOrEmpty(tkmd.MaDonVi) ? "" : tkmd.MaDonVi.ToString().ToUpper();
                lblTenNguoiNhapKhau.Text = String.IsNullOrEmpty(tkmd.TenDonVi) ? "" : tkmd.TenDonVi.ToString().ToUpper();
                lblMaBuuChinhNhapKhau.Text = String.IsNullOrEmpty(tkmd.MaBuuChinhDonVi) ? ""  : tkmd.MaBuuChinhDonVi.ToString().ToUpper();
                lblDiaChiNguoiNhapKhau.Text = String.IsNullOrEmpty(tkmd.DiaChiDonVi)? "" : tkmd.DiaChiDonVi.ToString().ToUpper();
                lblSoDienThoaiNguoiNhapKhau.Text = String.IsNullOrEmpty(tkmd.SoDienThoaiDonVi) ? "" : tkmd.SoDienThoaiDonVi.ToString().ToUpper();
                lblMaNguoiUyThacNhapKhau.Text = String.IsNullOrEmpty(tkmd.MaUyThac) ? "" : tkmd.MaUyThac.ToString().ToUpper();
                lblTenNguoiUyThacNhapKhau.Text = String.IsNullOrEmpty(tkmd.TenUyThac) ? "" : tkmd.TenUyThac.ToString().ToUpper();
                lblMaNguoiXuatKhau.Text = String.IsNullOrEmpty(tkmd.MaDoiTac) ? "" : tkmd.MaDoiTac.ToString().ToUpper();
                lblTenNguoiXuatKhau.Text = String.IsNullOrEmpty(tkmd.TenDoiTac) ? "" : tkmd.TenDoiTac.ToString().ToUpper();
                lblMaBuuChinhXuatKhau.Text = String.IsNullOrEmpty(tkmd.MaBuuChinhDoiTac) ? "" : tkmd.MaBuuChinhDoiTac.ToString().ToUpper();
                lblDiaChiNguoiXuatKhau1.Text = String.IsNullOrEmpty(tkmd.DiaChiDoiTac1) ? "" : tkmd.DiaChiDoiTac1.ToString().ToUpper();
                lblDiaChiNguoiXuatKhau2.Text = String.IsNullOrEmpty(tkmd.DiaChiDoiTac2) ? "" : tkmd.DiaChiDoiTac2.ToString().ToUpper();
                lblDiaChiNguoiXuatKhau3.Text = String.IsNullOrEmpty(tkmd.DiaChiDoiTac3) ? "" : tkmd.DiaChiDoiTac3.ToString().ToUpper();
                lblDiaChiNguoiXuatKhau4.Text = String.IsNullOrEmpty(tkmd.DiaChiDoiTac4) ? "" : tkmd.DiaChiDoiTac4.ToString().ToUpper();
                lblMaNuoc.Text = String.IsNullOrEmpty(tkmd.MaNuocDoiTac) ? "" : tkmd.MaNuocDoiTac.ToString().ToUpper();
                if (!String.IsNullOrEmpty(tkmd.NguoiUyThacXK))
                {
                    lblNguoiUyThacXuatKhau.Text = tkmd.NguoiUyThacXK.ToString().ToUpper();
                }
                else
                {
                    lblNguoiUyThacXuatKhau.Text = "";
                }
                lblMaDaiLyHaiQuan.Text = String.IsNullOrEmpty(tkmd.MaDaiLyHQ) ? "" : tkmd.MaDaiLyHQ.ToString().ToUpper();
                lblTenDaiLyHaiQuan.Text = String.IsNullOrEmpty(tkmd.TenDaiLyHaiQuan) ? "" : tkmd.TenDaiLyHaiQuan.ToString().ToUpper();
                lblMaNhanVienHaiQuan.Text = String.IsNullOrEmpty(tkmd.MaNhanVienHaiQuan) ? "" : tkmd.MaNhanVienHaiQuan.ToString().ToUpper();

                for (int i = 0; i < tkmd.VanDonCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblSoVanDon1.Text = String.IsNullOrEmpty(tkmd.VanDonCollection[i].SoDinhDanh) ? "" : tkmd.VanDonCollection[i].SoDinhDanh.ToString().ToUpper();
                            break;
                        case 1:
                            lblSoVanDon2.Text = String.IsNullOrEmpty(tkmd.VanDonCollection[i].SoDinhDanh) ? "" : tkmd.VanDonCollection[i].SoDinhDanh.ToString().ToUpper();
                            break;
                        case 2:
                            lblSoVanDon3.Text = String.IsNullOrEmpty(tkmd.VanDonCollection[i].SoDinhDanh) ? "" : tkmd.VanDonCollection[i].SoDinhDanh.ToString().ToUpper();
                            break;
                        case 3:
                            lblSoVanDon4.Text = String.IsNullOrEmpty(tkmd.VanDonCollection[i].SoDinhDanh) ? "" : tkmd.VanDonCollection[i].SoDinhDanh.ToString().ToUpper();
                            break;
                        case 4:
                            lblSoVanDon5.Text = String.IsNullOrEmpty(tkmd.VanDonCollection[i].SoDinhDanh) ? "" : tkmd.VanDonCollection[i].SoDinhDanh.ToString().ToUpper();
                            break;
                    }
                }
                lblSoLuong.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(tkmd.SoLuong.ToString()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(vad1ac.NO.GetValue().ToString().ToUpper().ToString().Replace(".", ",")));
                lblMaDonViTinh.Text = String.IsNullOrEmpty(tkmd.MaDVTSoLuong) ? "" : tkmd.MaDVTSoLuong.ToString().ToUpper();
                //string a = vad1ac.GW.GetValue().ToString().Replace(".", ",");
                lblTongTrongLuongHang.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(tkmd.TrongLuong.ToString()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(vad1ac.GW.GetValue().ToString().Replace(".", ",")));
                lblMaDonViTinhTrongLuong.Text = String.IsNullOrEmpty(tkmd.MaDVTTrongLuong) ? "" : tkmd.MaDVTTrongLuong.ToString().ToUpper();
                lblSoLuongContainer.Text = tkmd.SoLuongCont.ToString().ToUpper();
                lblMaDiaDiemLuuKhoHang.Text = String.IsNullOrEmpty(tkmd.MaDDLuuKho) ? "" : tkmd.MaDDLuuKho.ToString().ToUpper();
                lblTenDiaDiemLuuKhoHang.Text = String.IsNullOrEmpty(tkmd.TenDDLuuKho) ? "" : tkmd.TenDDLuuKho.ToString().ToUpper();
                lblMaDiaDiemDoHang.Text = String.IsNullOrEmpty(tkmd.MaDiaDiemDoHang) ? "" : tkmd.MaDiaDiemDoHang.ToString().ToUpper();
                lblTenDiaDiemDoHang.Text = String.IsNullOrEmpty(tkmd.TenDiaDiemDohang) ? "" : tkmd.TenDiaDiemDohang.ToString().ToUpper();
                lblMaDiaDiemXepHang.Text = String.IsNullOrEmpty(tkmd.MaDiaDiemXepHang) ? "" : tkmd.MaDiaDiemXepHang.ToString().ToUpper();
                lblTenDiaDiemXepHang.Text = String.IsNullOrEmpty(tkmd.TenDiaDiemXepHang) ? "" : tkmd.TenDiaDiemXepHang.ToString().ToUpper();
                lblMaPhuongTienVanChuyen.Text = String.IsNullOrEmpty(tkmd.MaPTVC) ? "" : tkmd.MaPTVC.ToString().ToUpper();
                lblTenPhuongTienVanChuyen.Text = String.IsNullOrEmpty(tkmd.TenPTVC) ? "" : tkmd.TenPTVC.ToString().ToUpper();
                if (tkmd.NgayHangDen.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayHangDen.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayHangDen.Text = tkmd.NgayHangDen.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayHangDen.Text = "";
                }
                lblKyHieuSoHieu.Text = String.IsNullOrEmpty(tkmd.SoHieuKyHieu) ? "" : tkmd.SoHieuKyHieu.ToString().ToUpper();
                if (tkmd.NgayNhapKhoDau.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayNhapKhoDau.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayDuocPhepNhapKho.Text = tkmd.NgayNhapKhoDau.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayDuocPhepNhapKho.Text = "";
                }
                if (!String.IsNullOrEmpty(tkmd.MaVanbanPhapQuy1))
                {
                    lblMaVanBanPhapQuyKhac1.Text = tkmd.MaVanbanPhapQuy1.ToString().ToUpper();
                }
                else
                {
                    lblMaVanBanPhapQuyKhac1.Text = "";
                }
                if (!String.IsNullOrEmpty(tkmd.MaVanbanPhapQuy2))
                {
                    lblMaVanBanPhapQuyKhac2.Text = tkmd.MaVanbanPhapQuy2.ToString().ToUpper();
                }
                else
                {
                    lblMaVanBanPhapQuyKhac2.Text = "";
                }

                if (!String.IsNullOrEmpty(tkmd.MaVanbanPhapQuy3))
                {
                    lblMaVanBanPhapQuyKhac3.Text = tkmd.MaVanbanPhapQuy3.ToString().ToUpper();
                }
                else
                {
                    lblMaVanBanPhapQuyKhac3.Text = "";
                }

                if (!String.IsNullOrEmpty(tkmd.MaVanbanPhapQuy4))
                {
                    lblMaVanBanPhapQuyKhac4.Text = tkmd.MaVanbanPhapQuy4.ToString().ToUpper();
                }
                else
                {
                    lblMaVanBanPhapQuyKhac4.Text = "";
                }
 
                if (!String.IsNullOrEmpty(tkmd.MaVanbanPhapQuy5))
                {
                    lblMaVanBanPhapQuyKhac5.Text = tkmd.MaVanbanPhapQuy5.ToString().ToUpper();
                }
                else
                {
                    lblMaVanBanPhapQuyKhac5.Text = "";
                }
                lblPhanLoaiHinhThucHoaDon.Text = String.IsNullOrEmpty(tkmd.PhanLoaiHD) ? "" : tkmd.PhanLoaiHD.ToString().ToUpper();
                lblSoHoaDon.Text = String.IsNullOrEmpty(tkmd.SoHoaDon) ? "" : tkmd.SoHoaDon.ToUpper();
                lblSoTiepNhanHoaDonDienTu.Text = tkmd.SoTiepNhanHD.ToString().ToUpper();
                if (tkmd.NgayPhatHanhHD.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayPhatHanhHD.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayPhatHanh.Text = tkmd.NgayPhatHanhHD.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayPhatHanh.Text = "";
                }
                lblPhuongThucThanhToan.Text = String.IsNullOrEmpty(tkmd.PhuongThucTT) ? "" : tkmd.PhuongThucTT.ToString().ToUpper();
                lblMaPhanLoaiGiaHoaDon.Text = String.IsNullOrEmpty(tkmd.PhanLoaiGiaHD) ? "" : tkmd.PhanLoaiGiaHD.ToString().ToUpper();
                lblMaDieuKienGiaHoaDon.Text = String.IsNullOrEmpty(tkmd.MaDieuKienGiaHD) ? "" : tkmd.MaDieuKienGiaHD.ToString().ToUpper();
                lblMaDongTienHoaDon.Text = String.IsNullOrEmpty(tkmd.MaTTHoaDon) ? "" : tkmd.MaTTHoaDon.ToString().ToUpper();
                lblTongTriGiaHoaDon.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.TongTriGiaHD)); //tkmd.TongTriGiaHD.ToString();//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.IP4.GetValue().ToString().Replace(".", ",")));
                lblTongTriGiaTinhThue.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.TriGiaTinhThue)); //tkmd.TriGiaTinhThue.ToString();//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.A86.GetValue().ToString().Replace(".", ",")));
                decimal triGiaHang = 0;
                tkmd.LoadFull();
                foreach (KDT_VNACC_HangMauDich hmd in tkmd.HangCollection)
                {
                    triGiaHang += hmd.TriGiaHoaDon;
                }
                lblTongHeSoPhanBoTriGia.Text = triGiaHang.ToString("#,#.0000#;(#,#.0000#)");
                //minhnd Fix Trị giá Tờ khai
                //lblTongHeSoPhanBoTriGia.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.TP.GetValue().ToString().Replace(".", ",")));
                if (!String.IsNullOrEmpty(tkmd.MaPhanLoaiTriGia))
                {
                    lblMaPhanLoaiNhapLieu.Text = tkmd.MaPhanLoaiTriGia.ToString().ToUpper();
                }
                else
                {
                    lblMaPhanLoaiNhapLieu.Text = "";
                }
                if (!String.IsNullOrEmpty(tkmd.MaKetQuaKiemTra))
                {
                    lblMaKetQuaKiemTraNoiDung.Text = tkmd.MaKetQuaKiemTra.ToString().ToUpper();
                }
                else
                {
                    lblMaKetQuaKiemTraNoiDung.Text = "";
                }

                for (int i = 0; i < tkmd.GiayPhepCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblPhanLoaiGiayPhepNhapKhau1.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].PhanLoai) ? "" : tkmd.GiayPhepCollection[i].PhanLoai.ToString().ToUpper();
                            lblSoGiayPhep1.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].SoGiayPhep) ? "" : tkmd.GiayPhepCollection[i].SoGiayPhep.ToString().ToUpper();
                            break;
                        case 1:
                            lblPhanLoaiGiayPhepNhapKhau2.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].PhanLoai) ? "" : tkmd.GiayPhepCollection[i].PhanLoai.ToString().ToUpper();
                            lblSoGiayPhep2.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].SoGiayPhep) ? "" : tkmd.GiayPhepCollection[i].SoGiayPhep.ToString().ToUpper();
                            break;
                        case 2:
                            lblPhanLoaiGiayPhepNhapKhau3.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].PhanLoai) ? "" : tkmd.GiayPhepCollection[i].PhanLoai.ToString().ToUpper();
                            lblSoGiayPhep3.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].SoGiayPhep) ? "" : tkmd.GiayPhepCollection[i].SoGiayPhep.ToString().ToUpper();
                            break;
                        case 3:
                            lblPhanLoaiGiayPhepNhapKhau4.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].PhanLoai) ? "" : tkmd.GiayPhepCollection[i].PhanLoai.ToString().ToUpper();
                            lblSoGiayPhep4.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].SoGiayPhep) ? "" : tkmd.GiayPhepCollection[i].SoGiayPhep.ToString().ToUpper();
                            break;
                        case 4:
                            lblPhanLoaiGiayPhepNhapKhau5.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].PhanLoai) ? "" : tkmd.GiayPhepCollection[i].PhanLoai.ToString().ToUpper();
                            lblSoGiayPhep5.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].SoGiayPhep) ? "" : tkmd.GiayPhepCollection[i].SoGiayPhep.ToString().ToUpper();
                            break;
                    }
                }
                if (!String.IsNullOrEmpty(tkmd.MaPhanLoaiTriGia))
                {
                    lblMaPhanLoaiKhaiTriGia.Text = tkmd.MaPhanLoaiTriGia.ToString().ToUpper();                    
                }
                else
                {
                    lblMaPhanLoaiKhaiTriGia.Text = "";
                }
                lblSoTiepNhanKhaiTriGiaTongHop.Text = tkmd.SoTiepNhanTKTriGia.ToString();//vad1ac.VD2.GetValue().ToString().ToUpper() == "0" ? "" : vad1ac.VD2.GetValue().ToString().ToUpper();
                if (String.IsNullOrEmpty(tkmd.PhanLoaiCongThucChuan))
                {
                    lblPhanLoaiCongThucChuan.Text = "";
                }
                else
                {
                    lblPhanLoaiCongThucChuan.Text = tkmd.PhanLoaiCongThucChuan.ToString().ToUpper();
                }

                if (String.IsNullOrEmpty(tkmd.MaPhanLoaiDieuChinhTriGia))
                {
                    lblMaPhanLoaiDieuChinhTriGia.Text = "";
                }
                else
                {
                    lblMaPhanLoaiDieuChinhTriGia.Text = tkmd.MaPhanLoaiDieuChinhTriGia.ToString().ToUpper(); 
                }
                if (String.IsNullOrEmpty(tkmd.PhuongPhapDieuChinhTriGia))
                {
                    lblPhuongPhapDieuChinhTriGia.Text = "";
                }
                else
                {
                    lblPhuongPhapDieuChinhTriGia.Text = tkmd.PhuongPhapDieuChinhTriGia.ToString().ToUpper();                
                }
                //lblMaTienTe.Text = tkmd.PhanLoaiKhongQDVND.ToString().ToUpper();
                //lblGiaCoSo.Text =  ;//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VPC.GetValue().ToString().Replace(".", ",")));
                if (!String.IsNullOrEmpty(tkmd.MaPhanLoaiPhiBH))
                {
                    lblMaPhanLoaiPhiVanChuyen.Text = tkmd.MaPhanLoaiPhiBH.ToString().ToUpper();
                }
                else
                {
                    lblMaPhanLoaiPhiVanChuyen.Text = "";
                }
                //vad1ac.FR1.GetValue().ToString().ToUpper();
                if (String.IsNullOrEmpty(tkmd.MaTTPhiVC))
                {
                    lblMaTienTePhiVanChuyen.Text = "";
                }
                else
                {
                    lblMaTienTePhiVanChuyen.Text = tkmd.MaTTPhiVC.ToString().ToUpper();
                }
                lblPhiVanChuyen.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.PhiVanChuyen)); //tkmd.PhiVanChuyen.ToString();//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.FR3.GetValue().ToString().Replace(".", ",")));
                if (String.IsNullOrEmpty(tkmd.MaPhanLoaiPhiBH))
                {
                    lblMaPhanLoaiBaoHiem.Text = "";
                }
                else
                {
                    lblMaPhanLoaiBaoHiem.Text = tkmd.MaPhanLoaiPhiBH.ToString().ToUpper();
                }
                if (String.IsNullOrEmpty(tkmd.MaTTPhiBH))
                {
                    lblMaTienTeCuaTienBaoHiem.Text = "";
                }
                else
                {
                    lblMaTienTeCuaTienBaoHiem.Text = tkmd.MaTTPhiBH.ToString().ToUpper();
                }
                lblPhiBaoHiem.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.PhiBaoHiem)); //tkmd.PhiBaoHiem.ToString();//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.IN3.GetValue().ToString().Replace(".", ",")));
                if (String.IsNullOrEmpty(tkmd.SoDangKyBH))
                {
                    lblSoDangKyBaoHiemTongHop.Text = "";
                }
                else
                {
                    lblSoDangKyBaoHiemTongHop.Text = tkmd.SoDangKyBH.ToString().ToUpper();
                }
                for (int i = 0; i < tkmd.KhoanDCCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblMaTenKhoanDieuChinh1.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaTenDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaTenDieuChinh.ToString().ToUpper();
                            lblMaPhanLoaiDieuChinh1.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaPhanLoaiDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaPhanLoaiDieuChinh.ToString().ToUpper();
                            lblMaDongTienDieuChinhTriGia1.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaTTDieuChinhTriGia) ? "" : tkmd.KhoanDCCollection[i].MaTTDieuChinhTriGia.ToString().ToUpper();
                            lblTriGiaKhoanDieuChinh1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.KhoanDCCollection[i].TriGiaKhoanDieuChinh.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.KhoanDCCollection[i].TongHeSoPhanBo.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                            break;
                        case 1:
                            lblMaTenKhoanDieuChinh2.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaTenDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaTenDieuChinh.ToString().ToUpper();
                            lblMaPhanLoaiDieuChinh2.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaPhanLoaiDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaPhanLoaiDieuChinh.ToString().ToUpper();
                            lblMaDongTienDieuChinhTriGia2.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaTTDieuChinhTriGia) ? "" : tkmd.KhoanDCCollection[i].MaTTDieuChinhTriGia.ToString().ToUpper();
                            lblTriGiaKhoanDieuChinh2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.KhoanDCCollection[i].TriGiaKhoanDieuChinh.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.KhoanDCCollection[i].TongHeSoPhanBo.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                            break;
                        case 2:
                            lblMaTenKhoanDieuChinh3.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaTenDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaTenDieuChinh.ToString().ToUpper();
                            lblMaPhanLoaiDieuChinh3.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaPhanLoaiDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaPhanLoaiDieuChinh.ToString().ToUpper();
                            lblMaDongTienDieuChinhTriGia3.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaTTDieuChinhTriGia) ? "" : tkmd.KhoanDCCollection[i].MaTTDieuChinhTriGia.ToString().ToUpper();
                            lblTriGiaKhoanDieuChinh3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.KhoanDCCollection[i].TriGiaKhoanDieuChinh.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.KhoanDCCollection[i].TongHeSoPhanBo.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                            break;
                        case 3:
                            lblMaTenKhoanDieuChinh4.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaTenDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaTenDieuChinh.ToString().ToUpper();
                            lblMaPhanLoaiDieuChinh4.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaPhanLoaiDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaPhanLoaiDieuChinh.ToString().ToUpper();
                            lblMaDongTienDieuChinhTriGia4.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaTenDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaTTDieuChinhTriGia.ToString().ToUpper();
                            lblTriGiaKhoanDieuChinh4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.KhoanDCCollection[i].TriGiaKhoanDieuChinh.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.KhoanDCCollection[i].TongHeSoPhanBo.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                            break;
                        case 4:
                            lblMaTenKhoanDieuChinh5.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaTenDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaTenDieuChinh.ToString().ToUpper();
                            lblMaPhanLoaiDieuChinh5.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaPhanLoaiDieuChinh) ? "" : tkmd.KhoanDCCollection[i].MaPhanLoaiDieuChinh.ToString().ToUpper();
                            lblMaDongTienDieuChinhTriGia5.Text = String.IsNullOrEmpty(tkmd.KhoanDCCollection[i].MaTTDieuChinhTriGia) ? "" : tkmd.KhoanDCCollection[i].MaTTDieuChinhTriGia.ToString().ToUpper();
                            lblTriGiaKhoanDieuChinh5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.KhoanDCCollection[i].TriGiaKhoanDieuChinh.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.KhoanDCCollection[i].TongHeSoPhanBo.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                            break;
                    }
                }
                if (String.IsNullOrEmpty(tkmd.ChiTietKhaiTriGia))
                {
                    lblChiTietKhaiTriGia.Text = "";
                }
                else
                {
                    lblChiTietKhaiTriGia.Text = tkmd.ChiTietKhaiTriGia.ToString().ToUpper();
                }
                for (int i = 0; i < tkmd.SacThueCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblMaSacThue1.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThue1.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            lblTongTienThue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.SacThueCollection[i].TongTienThue.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblSoDongTong1.Text = tkmd.SacThueCollection[i].SoDongTongTienThue.ToString().ToUpper();
                            break;
                        case 1:
                            lblMaSacThue2.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThue2.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            lblTongTienThue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.SacThueCollection[i].TongTienThue.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblSoDongTong2.Text = tkmd.SacThueCollection[i].SoDongTongTienThue.ToString().ToUpper();
                            break;
                        case 2:
                            lblMaSacThue3.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThue3.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            lblTongTienThue3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.SacThueCollection[i].TongTienThue.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblSoDongTong3.Text = tkmd.SacThueCollection[i].SoDongTongTienThue.ToString().ToUpper();
                            break;
                        case 3:
                            lblMaSacThue4.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThue4.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            lblTongTienThue4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.SacThueCollection[i].TongTienThue.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblSoDongTong4.Text = tkmd.SacThueCollection[i].SoDongTongTienThue.ToString().ToUpper();
                            break;
                        case 4:
                            lblMaSacThue5.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThue5.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            lblTongTienThue5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.SacThueCollection[i].TongTienThue.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblSoDongTong5.Text = tkmd.SacThueCollection[i].SoDongTongTienThue.ToString().ToUpper();
                            break;
                        case 5:
                            lblMaSacThue6.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThue6.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            lblTongTienThue6.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.SacThueCollection[i].TongTienThue.ToString().ToUpper()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                            lblSoDongTong6.Text = tkmd.SacThueCollection[i].SoDongTongTienThue.ToString().ToUpper();
                            break;
                    }
                }
                lblTongTienThuePhaiNop.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.TongTienThuePhaiNop.ToString()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.B02.GetValue().ToString().Replace(".", ",")));
                lblSoTienBaoLanh.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.SoTienBaoLanh.ToString()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.B03.GetValue().ToString().Replace(".", ",")));
                for (int i = 0; i < tkmd.TyGiaCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblMaDongTienTyGiaTinhThue1.Text = String.IsNullOrEmpty(tkmd.TyGiaCollection[i].MaTTTyGiaTinhThue) ? "" : tkmd.TyGiaCollection[i].MaTTTyGiaTinhThue.ToString().ToUpper();
                            lblTyGiaTinhThue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.TyGiaCollection[i].TyGiaTinhThue.ToString()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                            break;
                        case 1:
                            lblMaDongTienTyGiaTinhThue2.Text = String.IsNullOrEmpty(tkmd.TyGiaCollection[i].MaTTTyGiaTinhThue) ? "" : tkmd.TyGiaCollection[i].MaTTTyGiaTinhThue.ToString().ToUpper();
                            lblTyGiaTinhThue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.TyGiaCollection[i].TyGiaTinhThue.ToString()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                            break;
                        case 2:
                            lblMaDongTienTyGiaTinhThue3.Text = String.IsNullOrEmpty(tkmd.TyGiaCollection[i].MaTTTyGiaTinhThue) ? "" : tkmd.TyGiaCollection[i].MaTTTyGiaTinhThue.ToString().ToUpper();
                            lblTyGiaTinhThue3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.TyGiaCollection[i].TyGiaTinhThue.ToString()));//String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                            break;
                    }
                }
                lblMaXacDinhThoiHanNopThue.Text = String.IsNullOrEmpty(tkmd.MaXDThoiHanNopThue) ? "" : tkmd.MaXDThoiHanNopThue.ToString().ToUpper();
                lblNguoiNopThue.Text = String.IsNullOrEmpty(tkmd.NguoiNopThue) ? "" : tkmd.NguoiNopThue.ToString().ToUpper();
                lblMaLyDoDeNghi.Text = String.IsNullOrEmpty(tkmd.MaLyDoDeNghiBP) ? "" : tkmd.MaLyDoDeNghiBP.ToString().ToUpper();
                lblPhanLoaiNopThue.Text = String.IsNullOrEmpty(tkmd.PhanLoaiNopThue) ? "" : tkmd.PhanLoaiNopThue.ToString().ToUpper();
                lblTongSoTrangToKhai.Text = tkmd.TongSoTrangCuaToKhai.ToString().ToUpper();
                lblTongSoDongHang.Text = tkmd.TongSoDongHangCuaToKhai.ToString().ToUpper();
            }
            catch (Exception ex)
            {
                // Get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
        }

        private void lblSoTrang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblSoTrang.Text = 1 + "/" + (count + 2);
        }
    }
}
