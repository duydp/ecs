﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAGP030 : BasicVNACC
    {

        public List<VAGP030_HANG> HangHoa { get; set; }
        public void LoadVAGP030(string strResult)
        {
            try
            {
                this.GetObject<VAGP030>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAGP030, false, VAGP030.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAGP030.TongSoByte);
                while (true)
                {
                    VAGP030_HANG ivaHang = new VAGP030_HANG();
                    ivaHang.GetObject<VAGP030_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAGP030_HANG", false, VAGP030_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAGP030_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAGP030_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAGP030_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAGP030_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
