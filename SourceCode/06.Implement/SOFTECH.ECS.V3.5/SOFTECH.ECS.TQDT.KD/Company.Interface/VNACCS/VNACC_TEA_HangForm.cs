﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TEA_HangForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_TEA TEA = new KDT_VNACCS_TEA();
        private KDT_VNACCS_TEA_HangHoa Hang = null;
        bool isAddNew = true;
        public VNACC_TEA_HangForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.TEA.ToString());
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (Hang == null)
                {
                    Hang = new KDT_VNACCS_TEA_HangHoa();
                    isAddNew = true;
                }
                if (!ValidateForm(false))
                    return;
                GetHang();
                if (isAddNew)
                    TEA.HangCollection.Add(Hang);
                grListHang.DataSource = TEA.HangCollection;
                grListHang.Refetch();
                Hang = new KDT_VNACCS_TEA_HangHoa();
                SetHang();
                Hang = null;
                isAddNew = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }
        private void SetHang()
        {
            try
            {
                txtMoTaHangHoa.Text = Hang.MoTaHangHoa;
                txtSoLuongDangKyMT.Text = Hang.SoLuongDangKyMT.ToString();
                ctrSoLuongDangKyMT.Code = Hang.DVTSoLuongDangKyMT;
                txtSoLuongDaSuDung.Text = Hang.SoLuongDaSuDung.ToString();
                ctrSoLuongDaSuDung.Code = Hang.DVTSoLuongDaSuDung;
                txtTriGia.Text = Hang.TriGia.ToString();
                txtTriGiaDuKien.Text = Hang.TriGiaDuKien.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMoTaHangHoa, errorProvider, "Mô tả hàng hóa", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongDangKyMT, errorProvider, "Số lượng đăng ký", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtSoLuongDaSuDung, errorProvider, "Số lượng đã sử dụng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrSoLuongDangKyMT, errorProvider, "ĐVT", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(ctrSoLuongDaSuDung, errorProvider, "ĐVT", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTriGia, errorProvider, "Trị giá", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtTriGiaDuKien, errorProvider, "Trị giá dự kiến", isOnlyWarning);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void GetHang()
        {
            try
            {
                Hang.MoTaHangHoa = txtMoTaHangHoa.Text;
                Hang.SoLuongDangKyMT = Convert.ToDecimal(txtSoLuongDangKyMT.Value);
                Hang.DVTSoLuongDangKyMT = ctrSoLuongDangKyMT.Code;
                Hang.SoLuongDaSuDung = Convert.ToDecimal(txtSoLuongDaSuDung.Value);
                Hang.DVTSoLuongDaSuDung = ctrSoLuongDaSuDung.Code == null ? "" : ctrSoLuongDaSuDung.Code;
                Hang.TriGia = Convert.ToDecimal(txtTriGia.Value);
                Hang.TriGiaDuKien = Convert.ToDecimal(txtTriGiaDuKien.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_TEA_HangForm_Load(object sender, EventArgs e)
        {
            try
            {
                SetIDControl();
                grListHang.DataSource = TEA.HangCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void grListHang_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (grListHang.GetRows().Length < 0) return;
                Hang = (KDT_VNACCS_TEA_HangHoa)grListHang.CurrentRow.DataRow;
                SetHang();
                isAddNew = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = grListHang.SelectedItems;
                List<KDT_VNACCS_TEA_HangHoa> ItemColl = new List<KDT_VNACCS_TEA_HangHoa>();
                if (grListHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_TEA_HangHoa)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_TEA_HangHoa item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TEA.HangCollection.Remove(item);
                    }

                    grListHang.DataSource = TEA.HangCollection;
                    try { grListHang.Refetch(); }
                    catch { grListHang.Refresh(); }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }


        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                txtMoTaHangHoa.Tag = "CMN"; //Mô tả hàng hóa
                txtSoLuongDangKyMT.Tag = "QT"; //Số lượng đăng ký miễn thuế
                ctrSoLuongDangKyMT.TagName = "QTU"; //Đơn vị (của số lượng đăng ký miễn thuế)
                txtSoLuongDaSuDung.Tag = "UT"; //Số lượng đã sử dụng
                ctrSoLuongDaSuDung.TagName = "UTU"; //Đơn vị (của số lượng đã sử dụng)
                txtTriGia.Tag = "VA"; //Trị giá
                txtTriGiaDuKien.Tag = "EVA"; //Trị giá dự kiến

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
    }
}
