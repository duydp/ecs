//using Infragistics.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Infragistics.Excel;
using Company.Interface.SXXK.BangKeThanhLy;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    public partial class CanDoiSPXuatForm : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        private DataTable dt = new DataTable();
        public CanDoiSPXuatForm()
        {
            InitializeComponent();
        }

        private void CanDoiNhapXuatForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.reloadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void reloadData()
        {
            try
            {
                this.dt = this.HSTL.GetCanDoiSanPhamXuat(this.HSTL.LanThanhLy); ;
                this.dgList.DataSource = this.dt;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "Refresh":
                    this.reloadData();
                    break;
                default: XuatFileExcel();
                    break;                 
            }
            
        }

        private void XemTKNNPLItem_Click(object sender, EventArgs e)
        {
            try
            {
                XemToKhaiNhapNPLForm f = new XemToKhaiNhapNPLForm();
                f.HSTL = this.HSTL;
                f.MaNPL = dgList.GetRow().Cells["MaNPL"].Text;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void XuatFileExcel()
        {
            try
            {
                Workbook wb = new Workbook();
                Worksheet ws = wb.Worksheets.Add("CanDoiSPXuat");
                ws.Columns[0].Width = 5000;
                ws.Columns[1].Width = 10000;
                ws.Columns[2].Width = 5000;
                ws.Columns[3].Width = 5000;
                ws.Columns[4].Width = 5000;
                WorksheetRowCollection wsrc = ws.Rows;
                WorksheetRow wsr0 = ws.Rows[0];
                wsr0.Cells[0].Value = "Mã SP";
                wsr0.Cells[1].Value = "Tên SP";
                wsr0.Cells[2].Value = "Lượng SP Xuất";
                wsr0.Cells[3].Value = "Lượng Thức Xuất";
                wsr0.Cells[4].Value = "Lượng Chênh Lệch";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    WorksheetRow wsr = ws.Rows[i + 1];
                    wsr.Cells[0].Value = dt.Rows[i]["MaSP"].ToString();
                    wsr.Cells[1].Value = dt.Rows[i]["TenSP"].ToString();
                    wsr.Cells[2].Value = Convert.ToDecimal(dt.Rows[i]["LuongSPXuat"]);
                    wsr.Cells[3].Value = Convert.ToDecimal(dt.Rows[i]["LuongThucXuat"]);
                    wsr.Cells[4].Value = Convert.ToDecimal(dt.Rows[i]["ChenhLech"]);
                }
                DialogResult rs = saveFileDialog1.ShowDialog(this);
                if (rs == DialogResult.OK)
                {
                    string fileName = saveFileDialog1.FileName;
                    wb.Save(fileName);
                }
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
//throw;
            }
            
        }
        private void xemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                XemDinhMucNPLForm f = new XemDinhMucNPLForm();
                f.HSTL = this.HSTL;
                f.MaNPL = dgList.GetRow().Cells["MaNPL"].Text;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void xemDSTKN_Click(object sender, EventArgs e)
        {
            try
            {
                frmThemTKBK01_HSTL obj = new frmThemTKBK01_HSTL();

                GridEXRow dr = dgList.SelectedItems[0].GetRow();
                obj.MaNPL = dr.Cells["MaNPL"].Text;
                obj.lblMaNPL.Text = dr.Cells["MaNPL"].Text;
                obj.lblTenNPL.Text = dr.Cells["TenNPL"].Text;
                obj.lblLuongNhap.Text = dr.Cells["LuongNPLNhap"].Text;
                obj.lblLuongXuat.Text = dr.Cells["LuongNPLXuat"].Text;
                obj.lblLuongTon.Text = dr.Cells["LuongNPLTon"].Text;
                obj.HSTL = this.HSTL;
                obj.ShowDialog(this);
                this.reloadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

    }
}

