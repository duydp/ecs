-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Insert]
	@English varchar(250),
	@Vietnamese nvarchar(250),
	@ReferenceDB varchar(4),
	@Notes nvarchar(150),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_Type]
(
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
)
VALUES 
(
	@English,
	@Vietnamese,
	@ReferenceDB,
	@Notes
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Update]
	@ID int,
	@English varchar(250),
	@Vietnamese nvarchar(250),
	@ReferenceDB varchar(4),
	@Notes nvarchar(150)
AS

UPDATE
	[dbo].[t_VNACC_Category_Type]
SET
	[English] = @English,
	[Vietnamese] = @Vietnamese,
	[ReferenceDB] = @ReferenceDB,
	[Notes] = @Notes
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_InsertUpdate]
	@ID int,
	@English varchar(250),
	@Vietnamese nvarchar(250),
	@ReferenceDB varchar(4),
	@Notes nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_Type] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Type] 
		SET
			[English] = @English,
			[Vietnamese] = @Vietnamese,
			[ReferenceDB] = @ReferenceDB,
			[Notes] = @Notes
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_Type]
		(
			[English],
			[Vietnamese],
			[ReferenceDB],
			[Notes]
		)
		VALUES 
		(
			@English,
			@Vietnamese,
			@ReferenceDB,
			@Notes
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Type]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Type] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
FROM
	[dbo].[t_VNACC_Category_Type]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
FROM [dbo].[t_VNACC_Category_Type] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
FROM
	[dbo].[t_VNACC_Category_Type]	

GO

