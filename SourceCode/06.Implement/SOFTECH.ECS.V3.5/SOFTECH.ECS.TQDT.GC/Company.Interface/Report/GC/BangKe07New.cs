﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe07New : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;
        public int toso = 0;


        public BangKe07New()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt)
        {
            this.lblNgayKi.Visible = this.Last;
            this.lblNgayKi1.Visible = this.Last;
            this.lblNgayKi2.Visible = this.Last;
            this.lblHQ.Visible = this.Last;
            this.lblHQ1.Visible = this.Last;
            this.lblXN.Visible = this.Last;
            this.lblXN1.Visible = this.Last;
            this.lblXN2.Visible = this.Last;
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = dt;


            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }
            lblSoTo.Text = toso.ToString();

            lblMathang.Text = loaiSP;
            lblAddThue.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;

            lblTK1.Text = dt.Columns[2].Caption;
            lblTK2.Text = dt.Columns[3].Caption;
            lblTK3.Text = dt.Columns[4].Caption;
            lblTK4.Text = dt.Columns[5].Caption;
            lblTK5.Text = dt.Columns[6].Caption;

            lblTB.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[0].ColumnName);
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[1].ColumnName);
            lblLuong1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[2].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[3].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong3.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[4].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong4.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[5].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong5.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[6].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");

            lblNgayKi.Text =lblXN.Text =  GlobalSettings.TieudeNgay;
            
        }

    }
}
