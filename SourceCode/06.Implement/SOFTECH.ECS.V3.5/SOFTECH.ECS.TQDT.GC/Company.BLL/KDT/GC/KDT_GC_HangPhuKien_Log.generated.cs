﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_HangPhuKien_Log : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaHang { set; get; }
		public string TenHang { set; get; }
		public string MaHS { set; get; }
		public string ThongTinCu { set; get; }
		public decimal SoLuong { set; get; }
		public double DonGia { set; get; }
		public string DVT_ID { set; get; }
		public string NuocXX_ID { set; get; }
		public double TriGia { set; get; }
		public string NguyenTe_ID { set; get; }
		public int STTHang { set; get; }
		public string TinhTrang { set; get; }
		public long Master_ID { set; get; }
		public string NhomSP { set; get; }
		public DateTime DateLog { set; get; }
		public string Status { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_HangPhuKien_Log> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_HangPhuKien_Log> collection = new List<KDT_GC_HangPhuKien_Log>();
			while (reader.Read())
			{
				KDT_GC_HangPhuKien_Log entity = new KDT_GC_HangPhuKien_Log();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) entity.ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhomSP"))) entity.NhomSP = reader.GetString(reader.GetOrdinal("NhomSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateLog"))) entity.DateLog = reader.GetDateTime(reader.GetOrdinal("DateLog"));
				if (!reader.IsDBNull(reader.GetOrdinal("Status"))) entity.Status = reader.GetString(reader.GetOrdinal("Status"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_HangPhuKien_Log> collection, long id)
        {
            foreach (KDT_GC_HangPhuKien_Log item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_HangPhuKien_Log VALUES(@MaHang, @TenHang, @MaHS, @ThongTinCu, @SoLuong, @DonGia, @DVT_ID, @NuocXX_ID, @TriGia, @NguyenTe_ID, @STTHang, @TinhTrang, @Master_ID, @NhomSP, @DateLog, @Status)";
            string update = "UPDATE t_KDT_GC_HangPhuKien_Log SET MaHang = @MaHang, TenHang = @TenHang, MaHS = @MaHS, ThongTinCu = @ThongTinCu, SoLuong = @SoLuong, DonGia = @DonGia, DVT_ID = @DVT_ID, NuocXX_ID = @NuocXX_ID, TriGia = @TriGia, NguyenTe_ID = @NguyenTe_ID, STTHang = @STTHang, TinhTrang = @TinhTrang, Master_ID = @Master_ID, NhomSP = @NhomSP, DateLog = @DateLog, Status = @Status WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_HangPhuKien_Log WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThongTinCu", SqlDbType.NVarChar, "ThongTinCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGia", SqlDbType.Float, "DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGia", SqlDbType.Float, "TriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhTrang", SqlDbType.NVarChar, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomSP", SqlDbType.VarChar, "NhomSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateLog", SqlDbType.DateTime, "DateLog", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Status", SqlDbType.NVarChar, "Status", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThongTinCu", SqlDbType.NVarChar, "ThongTinCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGia", SqlDbType.Float, "DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGia", SqlDbType.Float, "TriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhTrang", SqlDbType.NVarChar, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomSP", SqlDbType.VarChar, "NhomSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateLog", SqlDbType.DateTime, "DateLog", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Status", SqlDbType.NVarChar, "Status", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_HangPhuKien_Log VALUES(@MaHang, @TenHang, @MaHS, @ThongTinCu, @SoLuong, @DonGia, @DVT_ID, @NuocXX_ID, @TriGia, @NguyenTe_ID, @STTHang, @TinhTrang, @Master_ID, @NhomSP, @DateLog, @Status)";
            string update = "UPDATE t_KDT_GC_HangPhuKien_Log SET MaHang = @MaHang, TenHang = @TenHang, MaHS = @MaHS, ThongTinCu = @ThongTinCu, SoLuong = @SoLuong, DonGia = @DonGia, DVT_ID = @DVT_ID, NuocXX_ID = @NuocXX_ID, TriGia = @TriGia, NguyenTe_ID = @NguyenTe_ID, STTHang = @STTHang, TinhTrang = @TinhTrang, Master_ID = @Master_ID, NhomSP = @NhomSP, DateLog = @DateLog, Status = @Status WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_HangPhuKien_Log WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThongTinCu", SqlDbType.NVarChar, "ThongTinCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGia", SqlDbType.Float, "DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGia", SqlDbType.Float, "TriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhTrang", SqlDbType.NVarChar, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomSP", SqlDbType.VarChar, "NhomSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateLog", SqlDbType.DateTime, "DateLog", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Status", SqlDbType.NVarChar, "Status", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThongTinCu", SqlDbType.NVarChar, "ThongTinCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGia", SqlDbType.Float, "DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGia", SqlDbType.Float, "TriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhTrang", SqlDbType.NVarChar, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomSP", SqlDbType.VarChar, "NhomSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateLog", SqlDbType.DateTime, "DateLog", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Status", SqlDbType.NVarChar, "Status", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_HangPhuKien_Log Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_HangPhuKien_Log> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_HangPhuKien_Log> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_HangPhuKien_Log> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_GC_HangPhuKien_Log> SelectCollectionBy_Master_ID(long master_ID)
		{
            IDataReader reader = SelectReaderBy_Master_ID(master_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Master_ID(long master_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_SelectBy_Master_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_Master_ID(long master_ID)
		{
			const string spName = "p_KDT_GC_HangPhuKien_Log_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_HangPhuKien_Log(string maHang, string tenHang, string maHS, string thongTinCu, decimal soLuong, double donGia, string dVT_ID, string nuocXX_ID, double triGia, string nguyenTe_ID, int sTTHang, string tinhTrang, long master_ID, string nhomSP, DateTime dateLog, string status)
		{
			KDT_GC_HangPhuKien_Log entity = new KDT_GC_HangPhuKien_Log();	
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.ThongTinCu = thongTinCu;
			entity.SoLuong = soLuong;
			entity.DonGia = donGia;
			entity.DVT_ID = dVT_ID;
			entity.NuocXX_ID = nuocXX_ID;
			entity.TriGia = triGia;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.STTHang = sTTHang;
			entity.TinhTrang = tinhTrang;
			entity.Master_ID = master_ID;
			entity.NhomSP = nhomSP;
			entity.DateLog = dateLog;
			entity.Status = status;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.NVarChar, ThongTinCu);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, DonGia);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, TriGia);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, TinhTrang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@NhomSP", SqlDbType.VarChar, NhomSP);
			db.AddInParameter(dbCommand, "@DateLog", SqlDbType.DateTime, DateLog.Year <= 1753 ? DBNull.Value : (object) DateLog);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.NVarChar, Status);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_HangPhuKien_Log> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_HangPhuKien_Log item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_HangPhuKien_Log(long id, string maHang, string tenHang, string maHS, string thongTinCu, decimal soLuong, double donGia, string dVT_ID, string nuocXX_ID, double triGia, string nguyenTe_ID, int sTTHang, string tinhTrang, long master_ID, string nhomSP, DateTime dateLog, string status)
		{
			KDT_GC_HangPhuKien_Log entity = new KDT_GC_HangPhuKien_Log();			
			entity.ID = id;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.ThongTinCu = thongTinCu;
			entity.SoLuong = soLuong;
			entity.DonGia = donGia;
			entity.DVT_ID = dVT_ID;
			entity.NuocXX_ID = nuocXX_ID;
			entity.TriGia = triGia;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.STTHang = sTTHang;
			entity.TinhTrang = tinhTrang;
			entity.Master_ID = master_ID;
			entity.NhomSP = nhomSP;
			entity.DateLog = dateLog;
			entity.Status = status;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_HangPhuKien_Log_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.NVarChar, ThongTinCu);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, DonGia);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, TriGia);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, TinhTrang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@NhomSP", SqlDbType.VarChar, NhomSP);
			db.AddInParameter(dbCommand, "@DateLog", SqlDbType.DateTime, DateLog.Year <= 1753 ? DBNull.Value : (object) DateLog);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.NVarChar, Status);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_HangPhuKien_Log> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_HangPhuKien_Log item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_HangPhuKien_Log(long id, string maHang, string tenHang, string maHS, string thongTinCu, decimal soLuong, double donGia, string dVT_ID, string nuocXX_ID, double triGia, string nguyenTe_ID, int sTTHang, string tinhTrang, long master_ID, string nhomSP, DateTime dateLog, string status)
		{
			KDT_GC_HangPhuKien_Log entity = new KDT_GC_HangPhuKien_Log();			
			entity.ID = id;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.ThongTinCu = thongTinCu;
			entity.SoLuong = soLuong;
			entity.DonGia = donGia;
			entity.DVT_ID = dVT_ID;
			entity.NuocXX_ID = nuocXX_ID;
			entity.TriGia = triGia;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.STTHang = sTTHang;
			entity.TinhTrang = tinhTrang;
			entity.Master_ID = master_ID;
			entity.NhomSP = nhomSP;
			entity.DateLog = dateLog;
			entity.Status = status;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.NVarChar, ThongTinCu);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, DonGia);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, TriGia);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, TinhTrang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@NhomSP", SqlDbType.VarChar, NhomSP);
			db.AddInParameter(dbCommand, "@DateLog", SqlDbType.DateTime, DateLog.Year <= 1753 ? DBNull.Value : (object) DateLog);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.NVarChar, Status);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_HangPhuKien_Log> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_HangPhuKien_Log item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_HangPhuKien_Log(long id)
		{
			KDT_GC_HangPhuKien_Log entity = new KDT_GC_HangPhuKien_Log();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_Master_ID(long master_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_DeleteBy_Master_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_HangPhuKien_Log_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_HangPhuKien_Log> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_HangPhuKien_Log item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}