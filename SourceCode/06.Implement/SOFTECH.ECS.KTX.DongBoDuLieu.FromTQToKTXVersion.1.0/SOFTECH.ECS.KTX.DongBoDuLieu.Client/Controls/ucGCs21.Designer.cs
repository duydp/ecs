namespace DongBo.Controls
{
    partial class ucGCs21
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListDNNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucGCs21));
            Janus.Windows.GridEX.GridEXLayout dgListDNLSP_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDNSP_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDNTB_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDNDM_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDNPK_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDNTKMD_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDNTKCT_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDNBKCU_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListHQNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListHQLSP_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListHQSP_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListHQTB_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListHQDM_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListHQPK_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListHQTKMD_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListHQTKCT_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListHQBKCU_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.tabDN = new Janus.Windows.UI.Tab.UITab();
            this.tabDNNPL = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListDNNPL = new Janus.Windows.GridEX.GridEX();
            this.tabDNLSP = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListDNLSP = new Janus.Windows.GridEX.GridEX();
            this.tabDNSP = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListDNSP = new Janus.Windows.GridEX.GridEX();
            this.tabDNTB = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListDNTB = new Janus.Windows.GridEX.GridEX();
            this.tabDNDM = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListDNDM = new Janus.Windows.GridEX.GridEX();
            this.tabDNPK = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListDNPK = new Janus.Windows.GridEX.GridEX();
            this.tabTKMD = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListDNTKMD = new Janus.Windows.GridEX.GridEX();
            this.tabTKCT = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListDNTKCT = new Janus.Windows.GridEX.GridEX();
            this.tabDNBKCU = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListDNBKCU = new Janus.Windows.GridEX.GridEX();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbHopDong = new Janus.Windows.EditControls.UIComboBox();
            this.btnDongBoALL = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnExport2Xml = new System.Windows.Forms.Button();
            this.btnDeleteAllHDOld = new System.Windows.Forms.Button();
            this.lblHQ = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tảiDữLiệuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lấyDữLiệuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabHQ = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHQNPL = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHQLSP = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHQSP = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHQTB = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage5 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHQDM = new Janus.Windows.GridEX.GridEX();
            this.tabPaPK = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHQPK = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage6 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHQTKMD = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage7 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHQTKCT = new Janus.Windows.GridEX.GridEX();
            this.tabHQBKCU = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHQBKCU = new Janus.Windows.GridEX.GridEX();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnTimSaiLech = new System.Windows.Forms.Button();
            this.chkOverwrite = new System.Windows.Forms.CheckBox();
            this.lblMsgSoHopDong = new System.Windows.Forms.Label();
            this.lblMsgPercent = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnDongBo1HD = new System.Windows.Forms.Button();
            this.btnHopDongNew = new System.Windows.Forms.Button();
            this.btnGHiDeALL = new System.Windows.Forms.Button();
            this.btnDongBoDuLieu = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbHopDongDN = new Janus.Windows.EditControls.UIComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDN = new System.Windows.Forms.Label();
            this.btnSearchHDDN = new System.Windows.Forms.Button();
            this.btnMoveSelected = new System.Windows.Forms.Button();
            this.bgw = new System.ComponentModel.BackgroundWorker();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xuấtHợpĐồngHiệnTạiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xuấtTấtCảHợpĐồngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xóaHợpĐồngHiệnTạiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xóaTấtCảHợpĐồngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlTop.SuspendLayout();
            this.pnlContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabDN)).BeginInit();
            this.tabDN.SuspendLayout();
            this.tabDNNPL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNNPL)).BeginInit();
            this.tabDNLSP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNLSP)).BeginInit();
            this.tabDNSP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNSP)).BeginInit();
            this.tabDNTB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNTB)).BeginInit();
            this.tabDNDM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNDM)).BeginInit();
            this.tabDNPK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNPK)).BeginInit();
            this.tabTKMD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNTKMD)).BeginInit();
            this.tabTKCT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNTKCT)).BeginInit();
            this.tabDNBKCU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNBKCU)).BeginInit();
            this.panel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabHQ)).BeginInit();
            this.tabHQ.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQNPL)).BeginInit();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQLSP)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQSP)).BeginInit();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQTB)).BeginInit();
            this.uiTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQDM)).BeginInit();
            this.tabPaPK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQPK)).BeginInit();
            this.uiTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQTKMD)).BeginInit();
            this.uiTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQTKCT)).BeginInit();
            this.tabHQBKCU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQBKCU)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.Location = new System.Drawing.Point(5, 489);
            this.pnlBottom.Size = new System.Drawing.Size(973, 22);
            // 
            // pnlTop
            // 
            this.pnlTop.Size = new System.Drawing.Size(973, 28);
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.tableLayoutPanel1);
            this.pnlContent.Size = new System.Drawing.Size(973, 461);
            // 
            // lblCaption
            // 
            this.lblCaption.Size = new System.Drawing.Size(327, 22);
            this.lblCaption.Text = "Đồng bộ  dữ liệu hợp đồng  Gia Công";
            // 
            // imgList
            // 
            this.imgList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imgList.ImageSize = new System.Drawing.Size(16, 16);
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // tabDN
            // 
            this.tabDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabDN.Location = new System.Drawing.Point(3, 249);
            this.tabDN.Name = "tabDN";
            this.tabDN.Size = new System.Drawing.Size(960, 128);
            this.tabDN.TabIndex = 24;
            this.tabDN.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tabDNNPL,
            this.tabDNLSP,
            this.tabDNSP,
            this.tabDNTB,
            this.tabDNDM,
            this.tabDNPK,
            this.tabTKMD,
            this.tabTKCT,
            this.tabDNBKCU});
            // 
            // tabDNNPL
            // 
            this.tabDNNPL.Controls.Add(this.dgListDNNPL);
            this.tabDNNPL.Location = new System.Drawing.Point(1, 21);
            this.tabDNNPL.Name = "tabDNNPL";
            this.tabDNNPL.Size = new System.Drawing.Size(956, 104);
            this.tabDNNPL.TabStop = true;
            this.tabDNNPL.Text = "Nguyên phụ liệu";
            // 
            // dgListDNNPL
            // 
            this.dgListDNNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDNNPL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListDNNPL.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDNNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDNNPL.ColumnAutoResize = true;
            dgListDNNPL_DesignTimeLayout.LayoutString = resources.GetString("dgListDNNPL_DesignTimeLayout.LayoutString");
            this.dgListDNNPL.DesignTimeLayout = dgListDNNPL_DesignTimeLayout;
            this.dgListDNNPL.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNNPL.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDNNPL.GroupByBoxVisible = false;
            this.dgListDNNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNNPL.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDNNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDNNPL.Hierarchical = true;
            this.dgListDNNPL.Location = new System.Drawing.Point(0, 0);
            this.dgListDNNPL.Name = "dgListDNNPL";
            this.dgListDNNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDNNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDNNPL.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNNPL.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNNPL.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNNPL.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDNNPL.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNNPL.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNNPL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDNNPL.Size = new System.Drawing.Size(956, 104);
            this.dgListDNNPL.TabIndex = 21;
            this.dgListDNNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabDNLSP
            // 
            this.tabDNLSP.Controls.Add(this.dgListDNLSP);
            this.tabDNLSP.Location = new System.Drawing.Point(1, 21);
            this.tabDNLSP.Name = "tabDNLSP";
            this.tabDNLSP.Size = new System.Drawing.Size(956, 122);
            this.tabDNLSP.TabStop = true;
            this.tabDNLSP.Text = "Loại sản phẩm";
            // 
            // dgListDNLSP
            // 
            this.dgListDNLSP.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDNLSP.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDNLSP.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDNLSP.ColumnAutoResize = true;
            dgListDNLSP_DesignTimeLayout.LayoutString = resources.GetString("dgListDNLSP_DesignTimeLayout.LayoutString");
            this.dgListDNLSP.DesignTimeLayout = dgListDNLSP_DesignTimeLayout;
            this.dgListDNLSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDNLSP.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNLSP.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNLSP.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDNLSP.GroupByBoxVisible = false;
            this.dgListDNLSP.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNLSP.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDNLSP.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNLSP.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDNLSP.Hierarchical = true;
            this.dgListDNLSP.Location = new System.Drawing.Point(0, 0);
            this.dgListDNLSP.Name = "dgListDNLSP";
            this.dgListDNLSP.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDNLSP.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDNLSP.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNLSP.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNLSP.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNLSP.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDNLSP.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNLSP.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNLSP.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDNLSP.Size = new System.Drawing.Size(956, 122);
            this.dgListDNLSP.TabIndex = 22;
            this.dgListDNLSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabDNSP
            // 
            this.tabDNSP.Controls.Add(this.dgListDNSP);
            this.tabDNSP.Location = new System.Drawing.Point(1, 21);
            this.tabDNSP.Name = "tabDNSP";
            this.tabDNSP.Size = new System.Drawing.Size(956, 122);
            this.tabDNSP.TabStop = true;
            this.tabDNSP.Text = "Sản phẩm";
            // 
            // dgListDNSP
            // 
            this.dgListDNSP.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDNSP.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDNSP.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDNSP.ColumnAutoResize = true;
            dgListDNSP_DesignTimeLayout.LayoutString = resources.GetString("dgListDNSP_DesignTimeLayout.LayoutString");
            this.dgListDNSP.DesignTimeLayout = dgListDNSP_DesignTimeLayout;
            this.dgListDNSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDNSP.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNSP.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNSP.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDNSP.GroupByBoxVisible = false;
            this.dgListDNSP.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNSP.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDNSP.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNSP.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDNSP.Hierarchical = true;
            this.dgListDNSP.Location = new System.Drawing.Point(0, 0);
            this.dgListDNSP.Name = "dgListDNSP";
            this.dgListDNSP.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDNSP.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDNSP.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNSP.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNSP.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNSP.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDNSP.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNSP.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNSP.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDNSP.Size = new System.Drawing.Size(956, 122);
            this.dgListDNSP.TabIndex = 22;
            this.dgListDNSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabDNTB
            // 
            this.tabDNTB.Controls.Add(this.dgListDNTB);
            this.tabDNTB.Location = new System.Drawing.Point(1, 21);
            this.tabDNTB.Name = "tabDNTB";
            this.tabDNTB.Size = new System.Drawing.Size(956, 122);
            this.tabDNTB.TabStop = true;
            this.tabDNTB.Text = "Thiết bị";
            // 
            // dgListDNTB
            // 
            this.dgListDNTB.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDNTB.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDNTB.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDNTB.ColumnAutoResize = true;
            dgListDNTB_DesignTimeLayout.LayoutString = resources.GetString("dgListDNTB_DesignTimeLayout.LayoutString");
            this.dgListDNTB.DesignTimeLayout = dgListDNTB_DesignTimeLayout;
            this.dgListDNTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDNTB.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNTB.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTB.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDNTB.GroupByBoxVisible = false;
            this.dgListDNTB.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTB.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDNTB.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTB.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDNTB.Hierarchical = true;
            this.dgListDNTB.Location = new System.Drawing.Point(0, 0);
            this.dgListDNTB.Name = "dgListDNTB";
            this.dgListDNTB.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDNTB.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDNTB.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNTB.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTB.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNTB.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDNTB.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTB.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNTB.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDNTB.Size = new System.Drawing.Size(956, 122);
            this.dgListDNTB.TabIndex = 22;
            this.dgListDNTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabDNDM
            // 
            this.tabDNDM.Controls.Add(this.dgListDNDM);
            this.tabDNDM.Location = new System.Drawing.Point(1, 21);
            this.tabDNDM.Name = "tabDNDM";
            this.tabDNDM.Size = new System.Drawing.Size(956, 104);
            this.tabDNDM.TabStop = true;
            this.tabDNDM.Text = "Định mức";
            // 
            // dgListDNDM
            // 
            this.dgListDNDM.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDNDM.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDNDM.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDNDM.ColumnAutoResize = true;
            dgListDNDM_DesignTimeLayout.LayoutString = resources.GetString("dgListDNDM_DesignTimeLayout.LayoutString");
            this.dgListDNDM.DesignTimeLayout = dgListDNDM_DesignTimeLayout;
            this.dgListDNDM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDNDM.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNDM.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNDM.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDNDM.GroupByBoxVisible = false;
            this.dgListDNDM.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNDM.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDNDM.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNDM.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDNDM.Hierarchical = true;
            this.dgListDNDM.Location = new System.Drawing.Point(0, 0);
            this.dgListDNDM.Name = "dgListDNDM";
            this.dgListDNDM.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDNDM.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDNDM.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNDM.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNDM.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNDM.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDNDM.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNDM.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNDM.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDNDM.Size = new System.Drawing.Size(956, 104);
            this.dgListDNDM.TabIndex = 23;
            this.dgListDNDM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabDNPK
            // 
            this.tabDNPK.Controls.Add(this.dgListDNPK);
            this.tabDNPK.Location = new System.Drawing.Point(1, 21);
            this.tabDNPK.Name = "tabDNPK";
            this.tabDNPK.Size = new System.Drawing.Size(956, 122);
            this.tabDNPK.TabStop = true;
            this.tabDNPK.Text = "Phụ Kiện";
            // 
            // dgListDNPK
            // 
            this.dgListDNPK.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDNPK.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDNPK.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDNPK.ColumnAutoResize = true;
            dgListDNPK_DesignTimeLayout.LayoutString = resources.GetString("dgListDNPK_DesignTimeLayout.LayoutString");
            this.dgListDNPK.DesignTimeLayout = dgListDNPK_DesignTimeLayout;
            this.dgListDNPK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDNPK.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNPK.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNPK.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDNPK.GroupByBoxVisible = false;
            this.dgListDNPK.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNPK.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDNPK.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNPK.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDNPK.Hierarchical = true;
            this.dgListDNPK.Location = new System.Drawing.Point(0, 0);
            this.dgListDNPK.Name = "dgListDNPK";
            this.dgListDNPK.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDNPK.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDNPK.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNPK.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNPK.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNPK.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDNPK.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNPK.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNPK.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDNPK.Size = new System.Drawing.Size(956, 122);
            this.dgListDNPK.TabIndex = 23;
            this.dgListDNPK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabTKMD
            // 
            this.tabTKMD.Controls.Add(this.dgListDNTKMD);
            this.tabTKMD.Location = new System.Drawing.Point(1, 21);
            this.tabTKMD.Name = "tabTKMD";
            this.tabTKMD.Size = new System.Drawing.Size(956, 104);
            this.tabTKMD.TabStop = true;
            this.tabTKMD.Text = "Tờ khai MD";
            // 
            // dgListDNTKMD
            // 
            this.dgListDNTKMD.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDNTKMD.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDNTKMD.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDNTKMD.ColumnAutoResize = true;
            dgListDNTKMD_DesignTimeLayout.LayoutString = resources.GetString("dgListDNTKMD_DesignTimeLayout.LayoutString");
            this.dgListDNTKMD.DesignTimeLayout = dgListDNTKMD_DesignTimeLayout;
            this.dgListDNTKMD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDNTKMD.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNTKMD.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTKMD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDNTKMD.GroupByBoxVisible = false;
            this.dgListDNTKMD.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTKMD.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDNTKMD.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTKMD.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDNTKMD.Hierarchical = true;
            this.dgListDNTKMD.Location = new System.Drawing.Point(0, 0);
            this.dgListDNTKMD.Name = "dgListDNTKMD";
            this.dgListDNTKMD.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDNTKMD.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNTKMD.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTKMD.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNTKMD.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDNTKMD.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTKMD.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNTKMD.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDNTKMD.Size = new System.Drawing.Size(956, 104);
            this.dgListDNTKMD.TabIndex = 23;
            this.dgListDNTKMD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabTKCT
            // 
            this.tabTKCT.Controls.Add(this.dgListDNTKCT);
            this.tabTKCT.Location = new System.Drawing.Point(1, 21);
            this.tabTKCT.Name = "tabTKCT";
            this.tabTKCT.Size = new System.Drawing.Size(956, 104);
            this.tabTKCT.TabStop = true;
            this.tabTKCT.Text = "Tờ khai CT";
            // 
            // dgListDNTKCT
            // 
            this.dgListDNTKCT.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDNTKCT.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDNTKCT.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDNTKCT.ColumnAutoResize = true;
            dgListDNTKCT_DesignTimeLayout.LayoutString = resources.GetString("dgListDNTKCT_DesignTimeLayout.LayoutString");
            this.dgListDNTKCT.DesignTimeLayout = dgListDNTKCT_DesignTimeLayout;
            this.dgListDNTKCT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDNTKCT.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNTKCT.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTKCT.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDNTKCT.GroupByBoxVisible = false;
            this.dgListDNTKCT.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTKCT.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDNTKCT.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTKCT.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDNTKCT.Hierarchical = true;
            this.dgListDNTKCT.Location = new System.Drawing.Point(0, 0);
            this.dgListDNTKCT.Name = "dgListDNTKCT";
            this.dgListDNTKCT.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDNTKCT.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDNTKCT.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNTKCT.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTKCT.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNTKCT.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDNTKCT.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNTKCT.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNTKCT.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDNTKCT.Size = new System.Drawing.Size(956, 104);
            this.dgListDNTKCT.TabIndex = 23;
            this.dgListDNTKCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabDNBKCU
            // 
            this.tabDNBKCU.Controls.Add(this.dgListDNBKCU);
            this.tabDNBKCU.Location = new System.Drawing.Point(1, 21);
            this.tabDNBKCU.Name = "tabDNBKCU";
            this.tabDNBKCU.Size = new System.Drawing.Size(956, 104);
            this.tabDNBKCU.TabStop = true;
            this.tabDNBKCU.Text = "Bảng kê cung ứng";
            // 
            // dgListDNBKCU
            // 
            this.dgListDNBKCU.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDNBKCU.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDNBKCU.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDNBKCU.ColumnAutoResize = true;
            dgListDNBKCU_DesignTimeLayout.LayoutString = resources.GetString("dgListDNBKCU_DesignTimeLayout.LayoutString");
            this.dgListDNBKCU.DesignTimeLayout = dgListDNBKCU_DesignTimeLayout;
            this.dgListDNBKCU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDNBKCU.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNBKCU.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNBKCU.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDNBKCU.GroupByBoxVisible = false;
            this.dgListDNBKCU.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNBKCU.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDNBKCU.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNBKCU.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDNBKCU.Hierarchical = true;
            this.dgListDNBKCU.Location = new System.Drawing.Point(0, 0);
            this.dgListDNBKCU.Name = "dgListDNBKCU";
            this.dgListDNBKCU.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDNBKCU.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDNBKCU.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDNBKCU.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNBKCU.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNBKCU.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDNBKCU.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDNBKCU.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDNBKCU.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDNBKCU.Size = new System.Drawing.Size(956, 104);
            this.dgListDNBKCU.TabIndex = 23;
            this.dgListDNBKCU.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.flowLayoutPanel1);
            this.panel2.Controls.Add(this.lblHQ);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(960, 53);
            this.panel2.TabIndex = 21;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Thông tin Hợp đồng từ hệ thống Hải quan";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cbHopDong);
            this.flowLayoutPanel1.Controls.Add(this.btnDongBoALL);
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.btnExport2Xml);
            this.flowLayoutPanel1.Controls.Add(this.btnDeleteAllHDOld);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(246, 19);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(709, 33);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // cbHopDong
            // 
            this.cbHopDong.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cbHopDong.Location = new System.Drawing.Point(3, 10);
            this.cbHopDong.Name = "cbHopDong";
            this.cbHopDong.Size = new System.Drawing.Size(166, 21);
            this.cbHopDong.TabIndex = 10;
            this.cbHopDong.SelectedIndexChanged += new System.EventHandler(this.cbHopDong_SelectedIndexChanged);
            // 
            // btnDongBoALL
            // 
            this.btnDongBoALL.AutoSize = true;
            this.btnDongBoALL.Image = ((System.Drawing.Image)(resources.GetObject("btnDongBoALL.Image")));
            this.btnDongBoALL.Location = new System.Drawing.Point(175, 3);
            this.btnDongBoALL.Name = "btnDongBoALL";
            this.btnDongBoALL.Size = new System.Drawing.Size(105, 28);
            this.btnDongBoALL.TabIndex = 5;
            this.btnDongBoALL.Text = "1. Tải dữ liệu";
            this.btnDongBoALL.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDongBoALL.UseVisualStyleBackColor = true;
            this.btnDongBoALL.Click += new System.EventHandler(this.btnDongBoALL_Click);
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(286, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 28);
            this.button1.TabIndex = 7;
            this.button1.Text = "2. Lấy dữ liệu";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnExport2Xml
            // 
            this.btnExport2Xml.AutoSize = true;
            this.btnExport2Xml.Location = new System.Drawing.Point(396, 3);
            this.btnExport2Xml.Name = "btnExport2Xml";
            this.btnExport2Xml.Size = new System.Drawing.Size(89, 28);
            this.btnExport2Xml.TabIndex = 8;
            this.btnExport2Xml.Text = "Dữ liệu HĐ";
            this.btnExport2Xml.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExport2Xml.UseVisualStyleBackColor = true;
            this.btnExport2Xml.Click += new System.EventHandler(this.btnExport2Xml_Click);
            // 
            // btnDeleteAllHDOld
            // 
            this.btnDeleteAllHDOld.AutoSize = true;
            this.btnDeleteAllHDOld.Location = new System.Drawing.Point(491, 3);
            this.btnDeleteAllHDOld.Name = "btnDeleteAllHDOld";
            this.btnDeleteAllHDOld.Size = new System.Drawing.Size(100, 28);
            this.btnDeleteAllHDOld.TabIndex = 11;
            this.btnDeleteAllHDOld.Text = "Xóa tất cả HĐ cũ";
            this.btnDeleteAllHDOld.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDeleteAllHDOld.UseVisualStyleBackColor = true;
            this.btnDeleteAllHDOld.Click += new System.EventHandler(this.btnDeleteAllHDOld_Click);
            // 
            // lblHQ
            // 
            this.lblHQ.AutoSize = true;
            this.lblHQ.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHQ.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblHQ.Location = new System.Drawing.Point(3, 4);
            this.lblHQ.Name = "lblHQ";
            this.lblHQ.Size = new System.Drawing.Size(24, 14);
            this.lblHQ.TabIndex = 1;
            this.lblHQ.Text = "[?]";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tảiDữLiệuToolStripMenuItem,
            this.lấyDữLiệuToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(144, 48);
            // 
            // tảiDữLiệuToolStripMenuItem
            // 
            this.tảiDữLiệuToolStripMenuItem.Name = "tảiDữLiệuToolStripMenuItem";
            this.tảiDữLiệuToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.tảiDữLiệuToolStripMenuItem.Text = "1. Tải dữ liệu";
            this.tảiDữLiệuToolStripMenuItem.Click += new System.EventHandler(this.tảiDữLiệuToolStripMenuItem_Click);
            // 
            // lấyDữLiệuToolStripMenuItem
            // 
            this.lấyDữLiệuToolStripMenuItem.Name = "lấyDữLiệuToolStripMenuItem";
            this.lấyDữLiệuToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.lấyDữLiệuToolStripMenuItem.Text = "2. Lấy dữ liệu";
            this.lấyDữLiệuToolStripMenuItem.Click += new System.EventHandler(this.lấyDữLiệuToolStripMenuItem_Click);
            // 
            // tabHQ
            // 
            this.tabHQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabHQ.Location = new System.Drawing.Point(3, 62);
            this.tabHQ.Name = "tabHQ";
            this.tabHQ.Size = new System.Drawing.Size(960, 124);
            this.tabHQ.TabIndex = 23;
            this.tabHQ.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage4,
            this.uiTabPage2,
            this.uiTabPage3,
            this.uiTabPage5,
            this.tabPaPK,
            this.uiTabPage6,
            this.uiTabPage7,
            this.tabHQBKCU});
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgListHQNPL);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(956, 100);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Nguyên phụ liệu";
            // 
            // dgListHQNPL
            // 
            this.dgListHQNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHQNPL.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListHQNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHQNPL.ColumnAutoResize = true;
            dgListHQNPL_DesignTimeLayout.LayoutString = resources.GetString("dgListHQNPL_DesignTimeLayout.LayoutString");
            this.dgListHQNPL.DesignTimeLayout = dgListHQNPL_DesignTimeLayout;
            this.dgListHQNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHQNPL.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQNPL.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHQNPL.GroupByBoxVisible = false;
            this.dgListHQNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQNPL.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListHQNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHQNPL.Hierarchical = true;
            this.dgListHQNPL.Location = new System.Drawing.Point(0, 0);
            this.dgListHQNPL.Name = "dgListHQNPL";
            this.dgListHQNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListHQNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHQNPL.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQNPL.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQNPL.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQNPL.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListHQNPL.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQNPL.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQNPL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHQNPL.Size = new System.Drawing.Size(956, 100);
            this.dgListHQNPL.TabIndex = 21;
            this.dgListHQNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.dgListHQLSP);
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(956, 121);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Loại sản phẩm";
            // 
            // dgListHQLSP
            // 
            this.dgListHQLSP.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHQLSP.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListHQLSP.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHQLSP.ColumnAutoResize = true;
            dgListHQLSP_DesignTimeLayout.LayoutString = resources.GetString("dgListHQLSP_DesignTimeLayout.LayoutString");
            this.dgListHQLSP.DesignTimeLayout = dgListHQLSP_DesignTimeLayout;
            this.dgListHQLSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHQLSP.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQLSP.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQLSP.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHQLSP.GroupByBoxVisible = false;
            this.dgListHQLSP.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQLSP.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListHQLSP.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQLSP.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHQLSP.Hierarchical = true;
            this.dgListHQLSP.Location = new System.Drawing.Point(0, 0);
            this.dgListHQLSP.Name = "dgListHQLSP";
            this.dgListHQLSP.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListHQLSP.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHQLSP.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQLSP.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQLSP.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQLSP.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListHQLSP.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQLSP.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQLSP.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHQLSP.Size = new System.Drawing.Size(956, 121);
            this.dgListHQLSP.TabIndex = 22;
            this.dgListHQLSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.dgListHQSP);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(956, 121);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Sản phẩm";
            // 
            // dgListHQSP
            // 
            this.dgListHQSP.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHQSP.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListHQSP.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHQSP.ColumnAutoResize = true;
            dgListHQSP_DesignTimeLayout.LayoutString = resources.GetString("dgListHQSP_DesignTimeLayout.LayoutString");
            this.dgListHQSP.DesignTimeLayout = dgListHQSP_DesignTimeLayout;
            this.dgListHQSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHQSP.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQSP.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQSP.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHQSP.GroupByBoxVisible = false;
            this.dgListHQSP.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQSP.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListHQSP.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQSP.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHQSP.Hierarchical = true;
            this.dgListHQSP.Location = new System.Drawing.Point(0, 0);
            this.dgListHQSP.Name = "dgListHQSP";
            this.dgListHQSP.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListHQSP.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHQSP.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQSP.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQSP.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQSP.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListHQSP.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQSP.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQSP.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHQSP.Size = new System.Drawing.Size(956, 121);
            this.dgListHQSP.TabIndex = 22;
            this.dgListHQSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.dgListHQTB);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(956, 121);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Thiết bị";
            // 
            // dgListHQTB
            // 
            this.dgListHQTB.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHQTB.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListHQTB.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHQTB.ColumnAutoResize = true;
            dgListHQTB_DesignTimeLayout.LayoutString = resources.GetString("dgListHQTB_DesignTimeLayout.LayoutString");
            this.dgListHQTB.DesignTimeLayout = dgListHQTB_DesignTimeLayout;
            this.dgListHQTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHQTB.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQTB.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTB.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHQTB.GroupByBoxVisible = false;
            this.dgListHQTB.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTB.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListHQTB.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTB.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHQTB.Hierarchical = true;
            this.dgListHQTB.Location = new System.Drawing.Point(0, 0);
            this.dgListHQTB.Name = "dgListHQTB";
            this.dgListHQTB.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListHQTB.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHQTB.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQTB.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTB.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQTB.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListHQTB.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTB.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQTB.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHQTB.Size = new System.Drawing.Size(956, 121);
            this.dgListHQTB.TabIndex = 22;
            this.dgListHQTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage5
            // 
            this.uiTabPage5.Controls.Add(this.dgListHQDM);
            this.uiTabPage5.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage5.Name = "uiTabPage5";
            this.uiTabPage5.Size = new System.Drawing.Size(956, 121);
            this.uiTabPage5.TabStop = true;
            this.uiTabPage5.Text = "Định mức";
            // 
            // dgListHQDM
            // 
            this.dgListHQDM.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHQDM.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListHQDM.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHQDM.ColumnAutoResize = true;
            dgListHQDM_DesignTimeLayout.LayoutString = resources.GetString("dgListHQDM_DesignTimeLayout.LayoutString");
            this.dgListHQDM.DesignTimeLayout = dgListHQDM_DesignTimeLayout;
            this.dgListHQDM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHQDM.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQDM.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQDM.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHQDM.GroupByBoxVisible = false;
            this.dgListHQDM.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQDM.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListHQDM.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQDM.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHQDM.Hierarchical = true;
            this.dgListHQDM.Location = new System.Drawing.Point(0, 0);
            this.dgListHQDM.Name = "dgListHQDM";
            this.dgListHQDM.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListHQDM.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHQDM.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQDM.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQDM.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQDM.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListHQDM.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQDM.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQDM.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHQDM.Size = new System.Drawing.Size(956, 121);
            this.dgListHQDM.TabIndex = 22;
            this.dgListHQDM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabPaPK
            // 
            this.tabPaPK.Controls.Add(this.dgListHQPK);
            this.tabPaPK.Location = new System.Drawing.Point(1, 21);
            this.tabPaPK.Name = "tabPaPK";
            this.tabPaPK.Size = new System.Drawing.Size(956, 121);
            this.tabPaPK.TabStop = true;
            this.tabPaPK.Text = "Phụ Kiện";
            // 
            // dgListHQPK
            // 
            this.dgListHQPK.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHQPK.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListHQPK.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHQPK.ColumnAutoResize = true;
            dgListHQPK_DesignTimeLayout.LayoutString = resources.GetString("dgListHQPK_DesignTimeLayout.LayoutString");
            this.dgListHQPK.DesignTimeLayout = dgListHQPK_DesignTimeLayout;
            this.dgListHQPK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHQPK.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQPK.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQPK.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHQPK.GroupByBoxVisible = false;
            this.dgListHQPK.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQPK.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListHQPK.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQPK.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHQPK.Hierarchical = true;
            this.dgListHQPK.Location = new System.Drawing.Point(0, 0);
            this.dgListHQPK.Name = "dgListHQPK";
            this.dgListHQPK.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListHQPK.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHQPK.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQPK.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQPK.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQPK.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListHQPK.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQPK.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQPK.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHQPK.Size = new System.Drawing.Size(956, 121);
            this.dgListHQPK.TabIndex = 24;
            this.dgListHQPK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage6
            // 
            this.uiTabPage6.Controls.Add(this.dgListHQTKMD);
            this.uiTabPage6.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage6.Name = "uiTabPage6";
            this.uiTabPage6.Size = new System.Drawing.Size(956, 100);
            this.uiTabPage6.TabStop = true;
            this.uiTabPage6.Text = "Tờ khai MD";
            // 
            // dgListHQTKMD
            // 
            this.dgListHQTKMD.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHQTKMD.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListHQTKMD.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHQTKMD.ColumnAutoResize = true;
            dgListHQTKMD_DesignTimeLayout.LayoutString = resources.GetString("dgListHQTKMD_DesignTimeLayout.LayoutString");
            this.dgListHQTKMD.DesignTimeLayout = dgListHQTKMD_DesignTimeLayout;
            this.dgListHQTKMD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHQTKMD.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQTKMD.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTKMD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHQTKMD.GroupByBoxVisible = false;
            this.dgListHQTKMD.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTKMD.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListHQTKMD.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTKMD.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHQTKMD.Hierarchical = true;
            this.dgListHQTKMD.Location = new System.Drawing.Point(0, 0);
            this.dgListHQTKMD.Name = "dgListHQTKMD";
            this.dgListHQTKMD.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHQTKMD.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQTKMD.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTKMD.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQTKMD.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListHQTKMD.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTKMD.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQTKMD.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHQTKMD.Size = new System.Drawing.Size(956, 100);
            this.dgListHQTKMD.TabIndex = 22;
            this.dgListHQTKMD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage7
            // 
            this.uiTabPage7.Controls.Add(this.dgListHQTKCT);
            this.uiTabPage7.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage7.Name = "uiTabPage7";
            this.uiTabPage7.Size = new System.Drawing.Size(956, 100);
            this.uiTabPage7.TabStop = true;
            this.uiTabPage7.Text = "Tờ khai CT";
            // 
            // dgListHQTKCT
            // 
            this.dgListHQTKCT.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHQTKCT.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListHQTKCT.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHQTKCT.ColumnAutoResize = true;
            dgListHQTKCT_DesignTimeLayout.LayoutString = resources.GetString("dgListHQTKCT_DesignTimeLayout.LayoutString");
            this.dgListHQTKCT.DesignTimeLayout = dgListHQTKCT_DesignTimeLayout;
            this.dgListHQTKCT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHQTKCT.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQTKCT.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTKCT.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHQTKCT.GroupByBoxVisible = false;
            this.dgListHQTKCT.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTKCT.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListHQTKCT.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTKCT.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHQTKCT.Hierarchical = true;
            this.dgListHQTKCT.Location = new System.Drawing.Point(0, 0);
            this.dgListHQTKCT.Name = "dgListHQTKCT";
            this.dgListHQTKCT.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListHQTKCT.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHQTKCT.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQTKCT.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTKCT.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQTKCT.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListHQTKCT.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQTKCT.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQTKCT.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHQTKCT.Size = new System.Drawing.Size(956, 100);
            this.dgListHQTKCT.TabIndex = 22;
            this.dgListHQTKCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabHQBKCU
            // 
            this.tabHQBKCU.Controls.Add(this.dgListHQBKCU);
            this.tabHQBKCU.Location = new System.Drawing.Point(1, 21);
            this.tabHQBKCU.Name = "tabHQBKCU";
            this.tabHQBKCU.Size = new System.Drawing.Size(956, 100);
            this.tabHQBKCU.TabStop = true;
            this.tabHQBKCU.Text = "Bảng kê cung ứng";
            // 
            // dgListHQBKCU
            // 
            this.dgListHQBKCU.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHQBKCU.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListHQBKCU.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListHQBKCU.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHQBKCU.ColumnAutoResize = true;
            dgListHQBKCU_DesignTimeLayout.LayoutString = resources.GetString("dgListHQBKCU_DesignTimeLayout.LayoutString");
            this.dgListHQBKCU.DesignTimeLayout = dgListHQBKCU_DesignTimeLayout;
            this.dgListHQBKCU.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQBKCU.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQBKCU.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHQBKCU.GroupByBoxVisible = false;
            this.dgListHQBKCU.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQBKCU.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListHQBKCU.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQBKCU.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHQBKCU.Hierarchical = true;
            this.dgListHQBKCU.Location = new System.Drawing.Point(0, 0);
            this.dgListHQBKCU.Name = "dgListHQBKCU";
            this.dgListHQBKCU.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListHQBKCU.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHQBKCU.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListHQBKCU.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQBKCU.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQBKCU.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListHQBKCU.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHQBKCU.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListHQBKCU.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHQBKCU.Size = new System.Drawing.Size(954, 98);
            this.dgListHQBKCU.TabIndex = 22;
            this.dgListHQBKCU.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.tabDN, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tabHQ, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 134F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(963, 451);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnTimSaiLech);
            this.panel3.Controls.Add(this.chkOverwrite);
            this.panel3.Controls.Add(this.lblMsgSoHopDong);
            this.panel3.Controls.Add(this.lblMsgPercent);
            this.panel3.Controls.Add(this.progressBar);
            this.panel3.Controls.Add(this.btnDongBo1HD);
            this.panel3.Controls.Add(this.btnHopDongNew);
            this.panel3.Controls.Add(this.btnGHiDeALL);
            this.panel3.Controls.Add(this.btnDongBoDuLieu);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 383);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(960, 65);
            this.panel3.TabIndex = 26;
            // 
            // btnTimSaiLech
            // 
            this.btnTimSaiLech.AutoSize = true;
            this.btnTimSaiLech.Image = ((System.Drawing.Image)(resources.GetObject("btnTimSaiLech.Image")));
            this.btnTimSaiLech.Location = new System.Drawing.Point(100, 3);
            this.btnTimSaiLech.Name = "btnTimSaiLech";
            this.btnTimSaiLech.Size = new System.Drawing.Size(188, 29);
            this.btnTimSaiLech.TabIndex = 17;
            this.btnTimSaiLech.Text = "Tìm thông tin sai lệch tất cả HĐ";
            this.btnTimSaiLech.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTimSaiLech.UseVisualStyleBackColor = true;
            this.btnTimSaiLech.Click += new System.EventHandler(this.btnTimSaiLech_Click);
            // 
            // chkOverwrite
            // 
            this.chkOverwrite.AutoSize = true;
            this.chkOverwrite.Location = new System.Drawing.Point(3, 9);
            this.chkOverwrite.Name = "chkOverwrite";
            this.chkOverwrite.Size = new System.Drawing.Size(91, 17);
            this.chkOverwrite.TabIndex = 16;
            this.chkOverwrite.Text = "Ghi đè dữ liệu";
            this.chkOverwrite.UseVisualStyleBackColor = true;
            // 
            // lblMsgSoHopDong
            // 
            this.lblMsgSoHopDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMsgSoHopDong.AutoSize = true;
            this.lblMsgSoHopDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblMsgSoHopDong.Location = new System.Drawing.Point(271, 15);
            this.lblMsgSoHopDong.Name = "lblMsgSoHopDong";
            this.lblMsgSoHopDong.Size = new System.Drawing.Size(0, 13);
            this.lblMsgSoHopDong.TabIndex = 14;
            // 
            // lblMsgPercent
            // 
            this.lblMsgPercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMsgPercent.AutoSize = true;
            this.lblMsgPercent.Location = new System.Drawing.Point(421, 43);
            this.lblMsgPercent.Name = "lblMsgPercent";
            this.lblMsgPercent.Size = new System.Drawing.Size(60, 13);
            this.lblMsgPercent.TabIndex = 15;
            this.lblMsgPercent.Text = "0/ 0 ( 0 %)";
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(268, 38);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(147, 23);
            this.progressBar.TabIndex = 13;
            // 
            // btnDongBo1HD
            // 
            this.btnDongBo1HD.AutoSize = true;
            this.btnDongBo1HD.Image = ((System.Drawing.Image)(resources.GetObject("btnDongBo1HD.Image")));
            this.btnDongBo1HD.Location = new System.Drawing.Point(2, 32);
            this.btnDongBo1HD.Name = "btnDongBo1HD";
            this.btnDongBo1HD.Size = new System.Drawing.Size(144, 29);
            this.btnDongBo1HD.TabIndex = 10;
            this.btnDongBo1HD.Text = "Đồng bộ hợp đồng [?]";
            this.btnDongBo1HD.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDongBo1HD.UseVisualStyleBackColor = true;
            this.btnDongBo1HD.Click += new System.EventHandler(this.btnDongBo1HD_Click);
            // 
            // btnHopDongNew
            // 
            this.btnHopDongNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHopDongNew.AutoSize = true;
            this.btnHopDongNew.Image = ((System.Drawing.Image)(resources.GetObject("btnHopDongNew.Image")));
            this.btnHopDongNew.Location = new System.Drawing.Point(487, 32);
            this.btnHopDongNew.Name = "btnHopDongNew";
            this.btnHopDongNew.Size = new System.Drawing.Size(169, 29);
            this.btnHopDongNew.TabIndex = 9;
            this.btnHopDongNew.Text = "Đồng bộ các hợp đồng mới";
            this.btnHopDongNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnHopDongNew.UseVisualStyleBackColor = true;
            this.btnHopDongNew.Visible = false;
            this.btnHopDongNew.Click += new System.EventHandler(this.btnHopDongNew_Click);
            // 
            // btnGHiDeALL
            // 
            this.btnGHiDeALL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGHiDeALL.AutoSize = true;
            this.btnGHiDeALL.Image = ((System.Drawing.Image)(resources.GetObject("btnGHiDeALL.Image")));
            this.btnGHiDeALL.Location = new System.Drawing.Point(809, 32);
            this.btnGHiDeALL.Name = "btnGHiDeALL";
            this.btnGHiDeALL.Size = new System.Drawing.Size(146, 28);
            this.btnGHiDeALL.TabIndex = 2;
            this.btnGHiDeALL.Text = "Đồng bộ tất cả dữ liệu";
            this.btnGHiDeALL.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGHiDeALL.UseVisualStyleBackColor = true;
            this.btnGHiDeALL.Click += new System.EventHandler(this.btnGHiDeALL_Click);
            // 
            // btnDongBoDuLieu
            // 
            this.btnDongBoDuLieu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDongBoDuLieu.AutoSize = true;
            this.btnDongBoDuLieu.Image = ((System.Drawing.Image)(resources.GetObject("btnDongBoDuLieu.Image")));
            this.btnDongBoDuLieu.Location = new System.Drawing.Point(655, 32);
            this.btnDongBoDuLieu.Name = "btnDongBoDuLieu";
            this.btnDongBoDuLieu.Size = new System.Drawing.Size(154, 28);
            this.btnDongBoDuLieu.TabIndex = 8;
            this.btnDongBoDuLieu.Text = "Đồng bộ các dữ liệu mới";
            this.btnDongBoDuLieu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDongBoDuLieu.UseVisualStyleBackColor = true;
            this.btnDongBoDuLieu.Visible = false;
            this.btnDongBoDuLieu.Click += new System.EventHandler(this.btnDongBoDuLieu_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbHopDongDN);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblDN);
            this.panel1.Controls.Add(this.btnSearchHDDN);
            this.panel1.Controls.Add(this.btnMoveSelected);
            this.panel1.Location = new System.Drawing.Point(3, 192);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(956, 48);
            this.panel1.TabIndex = 25;
            // 
            // cbHopDongDN
            // 
            this.cbHopDongDN.Location = new System.Drawing.Point(246, 21);
            this.cbHopDongDN.Name = "cbHopDongDN";
            this.cbHopDongDN.Size = new System.Drawing.Size(205, 21);
            this.cbHopDongDN.TabIndex = 10;
            this.cbHopDongDN.SelectedIndexChanged += new System.EventHandler(this.cbHopDongDN_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(237, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Thông tin Hợp đồng từ csdl Doanh nghiệp";
            // 
            // lblDN
            // 
            this.lblDN.AutoSize = true;
            this.lblDN.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblDN.Location = new System.Drawing.Point(3, 5);
            this.lblDN.Name = "lblDN";
            this.lblDN.Size = new System.Drawing.Size(24, 14);
            this.lblDN.TabIndex = 9;
            this.lblDN.Text = "[?]";
            // 
            // btnSearchHDDN
            // 
            this.btnSearchHDDN.AutoSize = true;
            this.btnSearchHDDN.ImageList = this.imgList;
            this.btnSearchHDDN.Location = new System.Drawing.Point(467, 17);
            this.btnSearchHDDN.Name = "btnSearchHDDN";
            this.btnSearchHDDN.Size = new System.Drawing.Size(139, 28);
            this.btnSearchHDDN.TabIndex = 7;
            this.btnSearchHDDN.Text = "Tìm hợp đồng so sánh";
            this.btnSearchHDDN.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearchHDDN.UseVisualStyleBackColor = true;
            this.btnSearchHDDN.Click += new System.EventHandler(this.btnSearchHDDN_Click);
            // 
            // btnMoveSelected
            // 
            this.btnMoveSelected.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnMoveSelected.AutoSize = true;
            this.btnMoveSelected.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveSelected.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveSelected.Image")));
            this.btnMoveSelected.Location = new System.Drawing.Point(610, 17);
            this.btnMoveSelected.Name = "btnMoveSelected";
            this.btnMoveSelected.Size = new System.Drawing.Size(127, 28);
            this.btnMoveSelected.TabIndex = 3;
            this.btnMoveSelected.Text = "So sánh dữ liệu";
            this.btnMoveSelected.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMoveSelected.UseVisualStyleBackColor = true;
            this.btnMoveSelected.Click += new System.EventHandler(this.btnMoveSelected_Click);
            // 
            // bgw
            // 
            this.bgw.WorkerReportsProgress = true;
            this.bgw.WorkerSupportsCancellation = true;
            this.bgw.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_DoWork);
            this.bgw.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgw_RunWorkerCompleted);
            this.bgw.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgw_ProgressChanged);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "_GC";
            this.saveFileDialog1.Filter = "Xml|*.xml";
            this.saveFileDialog1.RestoreDirectory = true;
            this.saveFileDialog1.Title = "Lưu tệp tin";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xuấtHợpĐồngHiệnTạiToolStripMenuItem,
            this.xuấtTấtCảHợpĐồngToolStripMenuItem,
            this.xóaHợpĐồngHiệnTạiToolStripMenuItem,
            this.xóaTấtCảHợpĐồngToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(208, 92);
            // 
            // xuấtHợpĐồngHiệnTạiToolStripMenuItem
            // 
            this.xuấtHợpĐồngHiệnTạiToolStripMenuItem.Name = "xuấtHợpĐồngHiệnTạiToolStripMenuItem";
            this.xuấtHợpĐồngHiệnTạiToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.xuấtHợpĐồngHiệnTạiToolStripMenuItem.Text = "1. Xuất hợp đồng hiện tại";
            this.xuấtHợpĐồngHiệnTạiToolStripMenuItem.Click += new System.EventHandler(this.xuấtHợpĐồngHiệnTạiToolStripMenuItem_Click);
            // 
            // xuấtTấtCảHợpĐồngToolStripMenuItem
            // 
            this.xuấtTấtCảHợpĐồngToolStripMenuItem.Name = "xuấtTấtCảHợpĐồngToolStripMenuItem";
            this.xuấtTấtCảHợpĐồngToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.xuấtTấtCảHợpĐồngToolStripMenuItem.Text = "2. Xuất tất cả hợp đồng";
            this.xuấtTấtCảHợpĐồngToolStripMenuItem.Click += new System.EventHandler(this.xuấtTấtCảHợpĐồngToolStripMenuItem_Click);
            // 
            // xóaHợpĐồngHiệnTạiToolStripMenuItem
            // 
            this.xóaHợpĐồngHiệnTạiToolStripMenuItem.Name = "xóaHợpĐồngHiệnTạiToolStripMenuItem";
            this.xóaHợpĐồngHiệnTạiToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.xóaHợpĐồngHiệnTạiToolStripMenuItem.Text = "3. Xóa hợp đồng hiện tại";
            this.xóaHợpĐồngHiệnTạiToolStripMenuItem.Click += new System.EventHandler(this.xóaHợpĐồngHiệnTạiToolStripMenuItem_Click);
            // 
            // xóaTấtCảHợpĐồngToolStripMenuItem
            // 
            this.xóaTấtCảHợpĐồngToolStripMenuItem.Name = "xóaTấtCảHợpĐồngToolStripMenuItem";
            this.xóaTấtCảHợpĐồngToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.xóaTấtCảHợpĐồngToolStripMenuItem.Text = "4. Xóa tất cả hợp đồng";
            this.xóaTấtCảHợpĐồngToolStripMenuItem.Click += new System.EventHandler(this.xóaTấtCảHợpĐồngToolStripMenuItem_Click);
            // 
            // ucGCs21
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ucGCs21";
            this.Size = new System.Drawing.Size(983, 511);
            this.Load += new System.EventHandler(this.ucGCs21_Load);
            this.Controls.SetChildIndex(this.pnlBottom, 0);
            this.Controls.SetChildIndex(this.pnlTop, 0);
            this.Controls.SetChildIndex(this.pnlContent, 0);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabDN)).EndInit();
            this.tabDN.ResumeLayout(false);
            this.tabDNNPL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNNPL)).EndInit();
            this.tabDNLSP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNLSP)).EndInit();
            this.tabDNSP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNSP)).EndInit();
            this.tabDNTB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNTB)).EndInit();
            this.tabDNDM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNDM)).EndInit();
            this.tabDNPK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNPK)).EndInit();
            this.tabTKMD.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNTKMD)).EndInit();
            this.tabTKCT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNTKCT)).EndInit();
            this.tabDNBKCU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDNBKCU)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabHQ)).EndInit();
            this.tabHQ.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQNPL)).EndInit();
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQLSP)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQSP)).EndInit();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQTB)).EndInit();
            this.uiTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQDM)).EndInit();
            this.tabPaPK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQPK)).EndInit();
            this.uiTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQTKMD)).EndInit();
            this.uiTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQTKCT)).EndInit();
            this.tabHQBKCU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHQBKCU)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imgList;
        private Janus.Windows.UI.Tab.UITab tabDN;
        private Janus.Windows.UI.Tab.UITabPage tabDNNPL;
        private Janus.Windows.UI.Tab.UITabPage tabDNSP;
        private Janus.Windows.GridEX.GridEX dgListDNSP;
        private Janus.Windows.UI.Tab.UITabPage tabDNTB;
        private Janus.Windows.GridEX.GridEX dgListDNTB;
        private Janus.Windows.UI.Tab.UITabPage tabDNLSP;
        private Janus.Windows.GridEX.GridEX dgListDNLSP;
        private Janus.Windows.UI.Tab.UITabPage tabDNDM;
        private Janus.Windows.UI.Tab.UITabPage tabTKMD;
        private Janus.Windows.UI.Tab.UITabPage tabTKCT;
        private Janus.Windows.GridEX.GridEX dgListDNTKCT;
        private Janus.Windows.UI.Tab.UITabPage tabDNPK;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Janus.Windows.UI.Tab.UITab tabHQ;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.GridEX.GridEX dgListHQNPL;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.GridEX.GridEX dgListHQLSP;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.GridEX dgListHQSP;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.GridEX.GridEX dgListHQTB;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage5;
        private Janus.Windows.GridEX.GridEX dgListHQDM;
        private Janus.Windows.UI.Tab.UITabPage tabPaPK;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage6;
        private Janus.Windows.GridEX.GridEX dgListHQTKMD;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage7;
        private Janus.Windows.GridEX.GridEX dgListHQTKCT;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnDongBoALL;
        private System.Windows.Forms.Button btnGHiDeALL;
        private System.Windows.Forms.Label lblHQ;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMoveSelected;
        private Janus.Windows.GridEX.GridEX dgListDNPK;
        private System.Windows.Forms.Button btnSearchHDDN;
        private Janus.Windows.GridEX.GridEX dgListHQPK;
        private Janus.Windows.UI.Tab.UITabPage tabDNBKCU;
        private Janus.Windows.UI.Tab.UITabPage tabHQBKCU;
        private Janus.Windows.GridEX.GridEX dgListHQBKCU;
        private Janus.Windows.GridEX.GridEX dgListDNBKCU;
        private System.Windows.Forms.Button btnDongBoDuLieu;
        private System.Windows.Forms.Label lblDN;
        private System.Windows.Forms.Button btnHopDongNew;
        private System.Windows.Forms.Panel panel3;
        private Janus.Windows.EditControls.UIComboBox cbHopDongDN;
        private System.Windows.Forms.Button button1;
        private Janus.Windows.GridEX.GridEX dgListDNDM;
        private Janus.Windows.GridEX.GridEX dgListDNNPL;
        private Janus.Windows.GridEX.GridEX dgListDNTKMD;
        private System.Windows.Forms.Button btnDongBo1HD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblMsgSoHopDong;
        private System.Windows.Forms.Label lblMsgPercent;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.ComponentModel.BackgroundWorker bgw;
        private System.Windows.Forms.CheckBox chkOverwrite;
        private System.Windows.Forms.Button btnTimSaiLech;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tảiDữLiệuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lấyDữLiệuToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnExport2Xml;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem xuấtHợpĐồngHiệnTạiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xuấtTấtCảHợpĐồngToolStripMenuItem;
        private Janus.Windows.EditControls.UIComboBox cbHopDong;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem xóaHợpĐồngHiệnTạiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xóaTấtCảHợpĐồngToolStripMenuItem;
        private System.Windows.Forms.Button btnDeleteAllHDOld;
    }
}
