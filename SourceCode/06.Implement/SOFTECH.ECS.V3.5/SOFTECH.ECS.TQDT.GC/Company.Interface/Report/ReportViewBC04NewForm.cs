﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC04NewForm : BaseForm
    {
        public DataSet ds;
        public HopDong HD = new HopDong();
        private BangKe04New BK04;
        public XRLabel Label;

        public ReportViewBC04NewForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            ds = new DataSet();
            BK04 = new BangKe04New();
            Label = new XRLabel();
            this.BK04.report = this;
            ds = this.CreatSchemaDataSetRP04();
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            cbPage.SelectedIndex = 0;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BK04.HD = this.HD;
            this.BK04.First = (cbPage.SelectedIndex == 0);
            if (cbPage.SelectedIndex == cbPage.Items.Count - 1)
            {
                BK04.Last = true;
            }
            else
            {
                BK04.Last = false;
            }
            this.BK04.BindReport(this.ds.Tables[cbPage.SelectedIndex]);
            printControl1.PrintingSystem = this.BK04.PrintingSystem;
            this.BK04.CreateDocument();
        }

        public DataSet CreatSchemaDataSetRP04()
        {

            DataSet dsSTK = new DataSet();
            ToKhaiMauDichCollection tkmdColl = HD.GetTKXK();

            BKCungUngDangKyCollection bkcuDKColl = HD.GetNPLCU();

            NguyenPhuLieuCungUngCollection nplCungUngColl = new NguyenPhuLieuCungUngCollection();

            foreach (BKCungUngDangKy bkcuDK in bkcuDKColl)
            {
                bkcuDK.LoadSanPhamCungUngCollection();
                foreach (SanPhanCungUng spcu in bkcuDK.SanPhamCungUngCollection)
                {
                    spcu.LoadNPLCungUngCollection();
                    foreach (NguyenPhuLieuCungUng nplcu in spcu.NPLCungUngCollection)
                    {
                        nplCungUngColl.Add(nplcu);
                    }
                }
            }

            List<NguyenPhuLieu> nplColl = new List<NguyenPhuLieu>();

            foreach (NguyenPhuLieuCungUng nplcu2 in nplCungUngColl)
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.Ma = nplcu2.MaNguyenPhuLieu;
                npl.HopDong_ID = this.HD.ID;
                npl = NguyenPhuLieu.Load(HD.ID, npl.Ma);
                if (npl!=null)
                {
                    if (!this.inNPLCollection(npl, nplColl))
                    {
                        nplColl.Add(npl);
                    }
                }

            }

            int soTable = (nplColl.Count - 1) / 7 + 1;
            DataTable dtTemp;
            for (int i = 0; i < soTable; i++)
            {
                dtTemp = new DataTable("tbl_" + i.ToString());
                dtTemp.Columns.Add("MaTKX", Type.GetType("System.Int64"));
                dtTemp.Columns.Add("SoTKX", Type.GetType("System.String"));
                for (int j = i * 7; j < (i + 1) * 7; j++)
                {
                    DataColumn col = new DataColumn();
                    if (j < nplColl.Count)
                    {
                        col.ColumnName = nplColl[j].Ma;
                        col.Caption = "Tên: " + nplColl[j].Ten + "\nMã: " + nplColl[j].Ma + "\nĐV tính: " + DonViTinh_GetName(nplColl[j].DVT_ID);
                    }
                    else
                    {
                        col.ColumnName = "col_" + j.ToString();
                        col.Caption = "Tên: \nMã \nĐV tính: ";
                    }
                    dtTemp.Columns.Add(col);
                }
                dtTemp.Columns.Add("HTCU", Type.GetType("System.String"));

                foreach (ToKhaiMauDich tkmd in tkmdColl)
                {
                    
                    bool val = false;
                    DataRow dr = dtTemp.NewRow();
                    dr[0] = tkmd.ID;
                    dr[1] = tkmd.SoToKhai.ToString() + "-" + tkmd.MaLoaiHinh + "-" + tkmd.NgayDangKy.Year.ToString();

                    DataTable dt = tkmd.GetNPLCungUngTK(tkmd.ID).Tables[0];
                    for (int k = 2; k < dtTemp.Columns.Count; k++)
                    {
                        decimal tg = 0;

                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["MaNguyenPhuLieu"].ToString().Trim().ToUpper() == dtTemp.Columns[k].ColumnName.Trim().ToUpper())
                            {
                                tg += Decimal.Parse(row["LuongCung"].ToString());
                            }
                        }

                        if (tg != 0)
                        {
                            dr[k] = tg.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                            val = true;
                        }

                    }
                    if (val) dtTemp.Rows.Add(dr);
                }
                dsSTK.Tables.Add(dtTemp);
            }

            return dsSTK;
        }

        private bool inNPLCollection(NguyenPhuLieu npl, List<NguyenPhuLieu> nplColl)
        {
            for (int i = 0; i < nplColl.Count; i++ )
            {
                if (npl.Ma == nplColl[i].Ma)
                {
                    return true;
                }
            }
            return false;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
            }   
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BK04.setText(this.Label, txtName.Text);
            this.BK04.CreateDocument();
        }
   
    }
}