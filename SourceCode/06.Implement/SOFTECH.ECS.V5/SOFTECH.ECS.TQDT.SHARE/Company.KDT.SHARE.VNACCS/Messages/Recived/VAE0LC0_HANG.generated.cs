using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAE0LC0_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute R01 { get; set; }
public PropertiesAttribute R05 { get; set; }
public PropertiesAttribute ADS { get; set; }
public PropertiesAttribute R03 { get; set; }
public PropertiesAttribute R04 { get; set; }
public PropertiesAttribute R08 { get; set; }
public PropertiesAttribute R09 { get; set; }
public PropertiesAttribute R11 { get; set; }
public PropertiesAttribute R12 { get; set; }
public PropertiesAttribute R13 { get; set; }
public PropertiesAttribute UPR { get; set; }
public PropertiesAttribute UPC { get; set; }
public PropertiesAttribute TSC { get; set; }
public PropertiesAttribute R07 { get; set; }
public PropertiesAttribute ADC { get; set; }
public PropertiesAttribute R14 { get; set; }
public PropertiesAttribute R15 { get; set; }
public PropertiesAttribute TSQ { get; set; }
public PropertiesAttribute TSU { get; set; }
public PropertiesAttribute CVU { get; set; }
public PropertiesAttribute ADD { get; set; }
public PropertiesAttribute QCV { get; set; }
public PropertiesAttribute TRA { get; set; }
public PropertiesAttribute TRM { get; set; }
public PropertiesAttribute TAX { get; set; }
public PropertiesAttribute ADE { get; set; }
public PropertiesAttribute TRP { get; set; }
public PropertiesAttribute ADF { get; set; }
public PropertiesAttribute TDN { get; set; }
public PropertiesAttribute ADG { get; set; }
public PropertiesAttribute ADH { get; set; }
public PropertiesAttribute CUP { get; set; }
public PropertiesAttribute IUP { get; set; }
public PropertiesAttribute CQU { get; set; }
public PropertiesAttribute CQC { get; set; }
public PropertiesAttribute IQU { get; set; }
public PropertiesAttribute IQC { get; set; }
public PropertiesAttribute CPR { get; set; }
public PropertiesAttribute IPR { get; set; }
public GroupAttribute F1 { get; set; }
public PropertiesAttribute TRC { get; set; }
public PropertiesAttribute TRL { get; set; }

public VAE0LC0_HANG()
        {
R01 = new PropertiesAttribute(2, typeof(string));
R05 = new PropertiesAttribute(12, typeof(string));
ADS = new PropertiesAttribute(7, typeof(string));
R03 = new PropertiesAttribute(1, typeof(string));
R04 = new PropertiesAttribute(600, typeof(string));
R08 = new PropertiesAttribute(12, typeof(int));
R09 = new PropertiesAttribute(4, typeof(string));
R11 = new PropertiesAttribute(12, typeof(int));
R12 = new PropertiesAttribute(4, typeof(string));
R13 = new PropertiesAttribute(20, typeof(int));
UPR = new PropertiesAttribute(9, typeof(int));
UPC = new PropertiesAttribute(3, typeof(string));
TSC = new PropertiesAttribute(4, typeof(string));
R07 = new PropertiesAttribute(17, typeof(int));
ADC = new PropertiesAttribute(3, typeof(string));
R14 = new PropertiesAttribute(3, typeof(string));
R15 = new PropertiesAttribute(20, typeof(int));
TSQ = new PropertiesAttribute(12, typeof(int));
TSU = new PropertiesAttribute(4, typeof(string));
CVU = new PropertiesAttribute(18, typeof(int));
ADD = new PropertiesAttribute(3, typeof(string));
QCV = new PropertiesAttribute(4, typeof(string));
TRA = new PropertiesAttribute(30, typeof(string));
TRM = new PropertiesAttribute(1, typeof(string));
TAX = new PropertiesAttribute(16, typeof(int));
ADE = new PropertiesAttribute(3, typeof(string));
TRP = new PropertiesAttribute(16, typeof(int));
ADF = new PropertiesAttribute(3, typeof(string));
TDN = new PropertiesAttribute(2, typeof(string));
ADG = new PropertiesAttribute(12, typeof(int));
ADH = new PropertiesAttribute(3, typeof(string));
CUP = new PropertiesAttribute(21, typeof(string));
IUP = new PropertiesAttribute(21, typeof(string));
CQU = new PropertiesAttribute(12, typeof(int));
CQC = new PropertiesAttribute(4, typeof(string));
IQU = new PropertiesAttribute(12, typeof(int));
IQC = new PropertiesAttribute(4, typeof(string));
CPR = new PropertiesAttribute(16, typeof(int));
IPR = new PropertiesAttribute(16, typeof(int));
TRC = new PropertiesAttribute(5, typeof(string));
TRL = new PropertiesAttribute(60, typeof(string));
#region F1
List<PropertiesAttribute> listF1 = new List<PropertiesAttribute>();
listF1.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAE0LC0_HANG_F1, typeof(string)));
F1 = new GroupAttribute("F1", 5, listF1);
#endregion F1
TongSoByte = 1133;
}

}public partial class EnumGroupID
    {
public static readonly string VAE0LC0_HANG_F1 = "VAE0LC0_HANG_F1";
}

}
