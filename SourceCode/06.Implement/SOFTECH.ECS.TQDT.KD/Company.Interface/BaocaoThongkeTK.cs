using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.SXXK.ToKhai;
using Company.Interface.Report.SXXK;
using Company.Interface.Report;

namespace Company.Interface
{
    public partial class BaocaoThongkeTK : BaseForm
    {
        public ToKhaiMauDich tokhai = new ToKhaiMauDich();
        public ToKhaiMauDichCollection tokhaiColl = new ToKhaiMauDichCollection();
        ThongkeTK report = new ThongkeTK();
        public DataSet dsToKhai;

        public BaocaoThongkeTK()
        {
            InitializeComponent();
        }

        private void BaocaoThongkeTK_Load(object sender, EventArgs e)
        {
            CbLoaiHinh.SelectedIndex = 2;
            txtDVBC.Text = Properties.Settings.Default.DonViBaoCao;
            txtNguoiDB.Text = Properties.Settings.Default.NguoiDuyetBieu;
            txtNguoiLB.Text = Properties.Settings.Default.NguoiLapBieu;

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string loaihinh = "";
            string where = "";
            
            if (ckbLuuMacdinh.Checked)
            {
                Properties.Settings.Default.NguoiLapBieu = txtNguoiLB.Text.ToUpper();
                Properties.Settings.Default.NguoiDuyetBieu = txtNguoiDB.Text.ToUpper();
                Properties.Settings.Default.DonViBaoCao = txtDVBC.Text.ToUpper();
                Properties.Settings.Default.Save();
                GlobalSettings.RefreshKey();
            }
            loaihinh = CbLoaiHinh.SelectedItem.Value.ToString();
            where = "TrangThai IN (0,1) AND  NgayDangKy >='" + CbTuNgay.Value.ToString("MM/dd/yyyy") + "' AND NgayDangKy <='" + CbDenNgay.Value.ToString("MM/dd/yyyy") + "'";
            if(loaihinh != string.Empty){
                where += "  AND  MaLoaiHinh Like '" + loaihinh + "%' ";
            }

            dsToKhai = tokhai.SelectDynamic(where, "MaLoaiHinh,SoToKhai");

            dsToKhai.Tables[0].Columns.Add("stringTrangThai",typeof(string));
            dsToKhai.Tables[0].Columns.Add("ToKhai", typeof(string));
            foreach(DataRow dr in dsToKhai.Tables[0].Rows ){
                dr["ToKhai"] = dr["SoToKhai"].ToString()+ " " + dr["MaLoaiHinh"] + "/" + dr["MaHaiQuan"];
                if (Convert.ToInt32(dr["TrangThai"]) == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET )
                {
                    dr["stringTrangThai"] =setText("Đã duyệt","Approved");
                }
                if (Convert.ToInt32(dr["TrangThai"]) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                {
                    dr["stringTrangThai"] = setText("Chờ duyệt","Pending approval");
                }

            }                       
            grdList.DataSource = dsToKhai.Tables[0];
            if (dsToKhai.Tables[0].Rows.Count >0 )
            {  
                btnView.Enabled = true;                         
            }
            else { 
                btnView.Enabled = false;
                ShowMessage(setText("Không có kết quả thống kê cho trường hợp này", "No results for this case"), false);
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            ReportHangXKViewForm viewform = new ReportHangXKViewForm();
            viewform.report = report;                          
            report.DataSource = dsToKhai.Tables[0];
            report.tungay = CbTuNgay.Value;
            report.denngay = CbDenNgay.Value;
            report.NguoiLapBieu = txtNguoiLB.Text.ToUpper();
            report.NguoiDuyetBieu = txtNguoiDB.Text.ToUpper();
            report.DonViBaoCao = txtDVBC.Text;
            report.BindData();
           // report.ShowPreview();
            viewform.Show();

        }

        private void ckbLuuMacdinh_CheckedChanged(object sender, EventArgs e)
        {
            
        
        }

        private void txtNguoiDB_TextChanged(object sender, EventArgs e)
        {

        }

        
        

       

    }
}

