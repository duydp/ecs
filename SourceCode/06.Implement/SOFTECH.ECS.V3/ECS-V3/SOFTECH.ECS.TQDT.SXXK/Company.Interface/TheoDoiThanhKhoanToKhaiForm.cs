﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.KDT.SXXK;

namespace Company.Interface
{
    public partial class TheoDoiThanhKhoanToKhaiForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public TheoDoiThanhKhoanToKhaiForm()
        {
            InitializeComponent();
        }

        private void TheoDoiThanhKhoanToKhaiForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["TonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["TonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            this.Text = "Theo dõi thanh khoản tờ khai số " + TKMD.SoToKhai + "/" + TKMD.MaLoaiHinh.Trim() + "/" + TKMD.NgayDangKy.ToString("dd-MM-yyyy");
            NPLNhapTonCollection col = new NPLNhapTon().SelectCollectionDynamic("MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND SoToKhai =" + this.TKMD.SoToKhai + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "' AND NamDangKy =" + TKMD.NamDangKy + " AND TonCuoi < TonDau AND LanThanhLy IN (SELECT LanThanhLy FROM t_KDT_SXXK_HoSoThanhLyDangKy)", "");
            dgList.DataSource = col;
        }
    }
}