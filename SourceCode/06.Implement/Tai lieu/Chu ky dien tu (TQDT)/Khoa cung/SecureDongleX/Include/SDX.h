// ====================================================================

EXTERN_C int WINAPI SDX_Find();

// Find SecureDongle X attached to the computer
// Return value:
//	<0	Error code
//	=0	No SecureDongle X attached
//	>0	The number of attached SecureDongle X(s)

// ====================================================================

EXTERN_C int WINAPI SDX_Open(int mode, DWORD uid, DWORD* hid);

// Open specified SecureDongle X
// Input:
// mode -- This parameter indicates the way to open SDX
// mode > 0,  open the SDX according to the UID. The mode value is the SDX number,
//            for example: uid=12345678, mode=2, this will open the second SDX with UID 12345678
// mode = -1, open the SDX according to the HID, and *hid can not be 0
// Define a Constant for ease of use:
#define HID_MODE	-1

// uid -- UserID,You need to specify the SDX UID and this UID is generated with SDX_GenUID
// hid -- Hardware ID,Open SDX with HID of *hid
// The SDX HID will be returned to *hid regardless of how the SDX was opened.
// Return value:
//	>=0	Success. The opened SDX handle is returned.
//	< 0	Error code. Please refer to latter section

// ====================================================================

EXTERN_C void WINAPI SDX_Close(int handle);

// Close specified SecureDongle X
// Input:
// handle -- SDX  handle. It is the handle returned from SDX_Open
// Return value:
// N/A

// ====================================================================

EXTERN_C int WINAPI SDX_Read(int handle, int block_index, char* buffer512);

// Read SecureDongle X content
// Input:
// handle      -- SDX handle. It is the handle returned from SDX_Open.
// block_index -- Block index. Specify the block to read. The value range is 0-4.
// buffer512   -- Read buffer. The buffer must be at least 512 bytes to accommodate the 512 bytes block size.
// Return value:
// Error code. Please refer to latter section

// ====================================================================

EXTERN_C int WINAPI SDX_Write(int handle, int block_index, char* buffer512);

// Write to SecureDongle X
// Input:
// handle      -- SDX handle. It is the handle returned from SDX_Open
// block_index -- Block index. Specify the block to read. The value range is 0-4.
// buffer512   -- Read buffer. The buffer must be at least 512 bytes to accommodate the 512 bytes block size.
// Return value:
// Error code. Please refer to latter section

// ====================================================================

EXTERN_C int WINAPI SDX_Transform(int handle, int len, char* buffer);

// Data Hashing function
// Input:
// handle -- SDX handle. It is the handle returned from SDX_Open
// len    -- Length of buffer to transform. Maximum 64 characters
// buffer -- Content of data that user want to transform. 
// SDX will return the result of transform to *buffer.
// Return value:
// Error code. Please refer to latter section

// ====================================================================

EXTERN_C int WINAPI SDX_GetVersion(int handle);

// Get SecureDongle X hardware version
// Input:
// handle -- SDX handle. It is the handle returned from SDX_Open
// Return value:
//	> 0	Success. The hardware version is returned.
//	< 0	Error code. Please refer to latter section

// ====================================================================

EXTERN_C int WINAPI SDX_RSAEncrypt(int handle, int startIndex, char *bufferData, int *len, char *Key512);

// Encrypt with RSA and write to SecureDongle X
// Input:
// handle     -- SDX handle. It is the handle returned from SDX_Open
// startIndex -- Start index. Specify the start index to write cipher text into SDX. The value range is 0-2559.
// bufferData -- Plaintext that will be encrypted.
// len        -- Length of Data buffer to encrypt. On success, SDX will write the length of cipher text to len.
// Key512     -- Decryption key. The key must 512 Byte. On success, SDX will write the decryption key to Key512.
// Return value:
// Error code. Please refer to latter section

// ====================================================================

EXTERN_C int WINAPI SDX_RSADecrypt(int handle, int startIndex, char *bufferData, int *len, char *Key512);

// Decrypt with RSA and write to SecureDongle X
// Input:
// handle     -- SDX handle. It is the handle returned from SDX_Open
// startIndex -- Start index. Specify the start index to read cipher text into SDX. The value range is 0-2559.
// bufferData -- If success, SDX will write bufferData with plaintext.
// len        -- Length of cipher text to decrypt. If success SDX will write the length of plaintext to len.
// Key512     -- Key that is used to decrypt. The key size must 512 Bytes.
// Return value:
// Error code. Please refer to latter section


// Error codes ========================================================

#define SDXERR_SUCCESS									0			// Success
#define SDXERR_NO_SUCH_DEVICE							0xA0100001	// Specified SDX is not found (parameter error)
#define SDXERR_NOT_OPENED_DEVICE						0xA0100002	// Need to call SDX_Open first to open the SDX, then call this function (operation error)
#define SDXERR_WRONG_UID								0xA0100003	// Wrong UID(parameter error)
#define SDXERR_WRONG_INDEX								0xA0100004	// Block index error (parameter error)
#define SDXERR_TOO_LONG_SEED							0xA0100005	// Seed character string is longer than 64 bytes when calling GenUID (parameter error)
#define SDXERR_WRITE_PROTECT							0xA0100006	// Tried to write to write-protected dongle(operation error)
#define SDXERR_WRONG_START_INDEX						0xA0100007	// Start index wrong (parameter error)
#define SDXERR_INVALID_LEN								0xA0100008	// Invalid length (parameter error)
#define SDXERR_TOO_LONG_ENCRYPTION_DATA					0xA0100009	// Chipertext is too long (cryptography error)
#define SDXERR_GENERATE_KEY								0xA010000A	// Generate key error (cryptography error)
#define SDXERR_INVALID_KEY								0xA010000B	// Invalid key (cryptography error)
#define SDXERR_FAILED_ENCRYPTION						0xA010000C	// Failed to encrypt string (cryptography error)
#define SDXERR_FAILED_WRITE_KEY							0xA010000D	// Failed to write key (cryptography error)
#define SDXERR_FAILED_DECRYPTION						0xA010000E	// Failed to decrypt string (Cryptography error)	
#define SDXERR_OPEN_DEVICE								0xA010000F	// Open device error (Windows error)
#define SDXERR_READ_REPORT								0xA0100010	// Read record error(Windows error)
#define SDXERR_WRITE_REPORT								0xA0100011	// Write record error(Windows error)
#define SDXERR_SETUP_DI_GET_DEVICE_INTERFACE_DETAIL		0xA0100012	// Internal error (Windows error)
#define SDXERR_GET_ATTRIBUTES							0xA0100013	// Internal error (Windows error)
#define SDXERR_GET_PREPARSED_DATA						0xA0100014	// Internal error (Windows error)
#define SDXERR_GETCAPS									0xA0100015	// Internal error (Windows error)
#define SDXERR_FREE_PREPARSED_DATA						0xA0100016	// Internal error (Windows error)
#define SDXERR_FLUSH_QUEUE								0xA0100017	// Internal error (Windows error)
#define SDXERR_SETUP_DI_CLASS_DEVS						0xA0100018	// Internal error (Windows error)
#define SDXERR_GET_SERIAL								0xA0100019	// Internal error (Windows error)
#define SDXERR_GET_PRODUCT_STRING						0xA010001A	// Internal error (Windows error)
#define SDXERR_TOO_LONG_DEVICE_DETAIL					0xA010001B	// Internal error
#define SDXERR_UNKNOWN_DEVICE							0xA0100020	// Unknown device(hardware error)
#define SDXERR_VERIFY									0xA0100021	// Verification error(hardware error)
#define SDXERR_UNKNOWN_ERROR							0xA010FFFF	// Unknown error(hardware error)
