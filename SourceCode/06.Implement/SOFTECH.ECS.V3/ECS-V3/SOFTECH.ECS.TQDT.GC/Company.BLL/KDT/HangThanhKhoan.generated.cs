using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT
{
	public partial class HangThanhKhoan
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public int LoaiHang { set; get; }
		public string MaHang { set; get; }
		public string TenHang { set; get; }
		public string MaHS { set; get; }
		public decimal SoLuong { set; get; }
		public string DVT_ID { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HangThanhKhoan Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			HangThanhKhoan entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new HangThanhKhoan();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHang"))) entity.LoaiHang = reader.GetInt32(reader.GetOrdinal("LoaiHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<HangThanhKhoan> SelectCollectionAll()
		{
			List<HangThanhKhoan> collection = new List<HangThanhKhoan>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				HangThanhKhoan entity = new HangThanhKhoan();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHang"))) entity.LoaiHang = reader.GetInt32(reader.GetOrdinal("LoaiHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<HangThanhKhoan> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<HangThanhKhoan> collection = new List<HangThanhKhoan>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				HangThanhKhoan entity = new HangThanhKhoan();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHang"))) entity.LoaiHang = reader.GetInt32(reader.GetOrdinal("LoaiHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<HangThanhKhoan> SelectCollectionBy_Master_ID(long master_ID)
		{
			List<HangThanhKhoan> collection = new List<HangThanhKhoan>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_Master_ID(master_ID);
			while (reader.Read())
			{
				HangThanhKhoan entity = new HangThanhKhoan();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHang"))) entity.LoaiHang = reader.GetInt32(reader.GetOrdinal("LoaiHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Master_ID(long master_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_SelectBy_Master_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_Master_ID(long master_ID)
		{
			const string spName = "p_KDT_GC_HangThanhKhoan_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHangThanhKhoan(long master_ID, int loaiHang, string maHang, string tenHang, string maHS, decimal soLuong, string dVT_ID, string ghiChu)
		{
			HangThanhKhoan entity = new HangThanhKhoan();	
			entity.Master_ID = master_ID;
			entity.LoaiHang = loaiHang;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.Int, LoaiHang);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.NVarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HangThanhKhoan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangThanhKhoan item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHangThanhKhoan(long id, long master_ID, int loaiHang, string maHang, string tenHang, string maHS, decimal soLuong, string dVT_ID, string ghiChu)
		{
			HangThanhKhoan entity = new HangThanhKhoan();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.LoaiHang = loaiHang;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_HangThanhKhoan_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.Int, LoaiHang);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.NVarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HangThanhKhoan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangThanhKhoan item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHangThanhKhoan(long id, long master_ID, int loaiHang, string maHang, string tenHang, string maHS, decimal soLuong, string dVT_ID, string ghiChu)
		{
			HangThanhKhoan entity = new HangThanhKhoan();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.LoaiHang = loaiHang;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.Int, LoaiHang);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.NVarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HangThanhKhoan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangThanhKhoan item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHangThanhKhoan(long id)
		{
			HangThanhKhoan entity = new HangThanhKhoan();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_Master_ID(long master_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_DeleteBy_Master_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_HangThanhKhoan_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HangThanhKhoan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangThanhKhoan item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}