﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components
{
    public partial class DaiLy
    {
        public static DaiLy Load(string userName, string passWord)
        {
            string whereCondition = string.Format("USER_NAME = '{0}' AND PASSWORD = '{1}'", userName, passWord);
            IDataReader reader = SelectReaderDynamic(whereCondition, "");
            IList<DaiLy> list = ConvertToCollection(reader);

            return list.Count == 1 ? list[0] : null;
        }
        public static DaiLy Load(string userName)
        {
            string whereCondition = string.Format("USER_NAME = '{0}'", userName);
            IDataReader reader = SelectReaderDynamic(whereCondition, "");
            IList<DaiLy> list = ConvertToCollection(reader);

            return list.Count == 1 ? list[0] : null;
        }

        //---------------------------------------------------------------------------------------------

        #region Select

        public static List<DaiLy> SelectUserDaiLy(string maDoanhNghiep)
        {
            string whereCondition = string.Format("MaDoanhNghiep = '{0}'", maDoanhNghiep);
            IDataReader reader = SelectReaderDynamic(whereCondition, "");
            return (List<DaiLy>)ConvertToCollection(reader);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert
        public int InsertUpdateUserDaiLy(SqlTransaction transaction)
        {
            const string spName = "p_User_DaiLy_InsertUpdate_ByUSER_NAME";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, USER_NAME);
            db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, PASSWORD);
            db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, HO_TEN);
            db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, MO_TA);
            db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, isAdmin);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int InsertUpdateUserDaiLy(DaiLy obj)
        {
            return InsertUpdateUserDaiLy(obj.USER_NAME, obj.PASSWORD, obj.HO_TEN, obj.MO_TA, obj.isAdmin, obj.MaDoanhNghiep);
        }

        //---------------------------------------------------------------------------------------------

        public static int InsertUpdateUserDaiLy(string uSER_NAME, string pASSWORD, string hO_TEN, string mO_TA, bool isAdmin, string maDoanhNghiep)
        {
            int ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;

                        DaiLy item = DaiLy.Load(uSER_NAME, pASSWORD); 
                        if (item == null)
                            item = new DaiLy();
                        item.USER_NAME = uSER_NAME;
                        item.PASSWORD = pASSWORD;
                        item.HO_TEN = hO_TEN;
                        item.MO_TA = mO_TA;
                        item.isAdmin = isAdmin;
                        item.MaDoanhNghiep = maDoanhNghiep;
                        
                        ret = item.InsertUpdateUserDaiLy(transaction);

                        if (ret <= 0)
                        {
                            ret01 = false;
                        }

                        if (ret01)
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        public static int InsertUpdateUserDaiLy_V3(string uSER_NAME, string pASSWORD, string hO_TEN, string mO_TA, string maDoanhNghiep)
        {
            int ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;

                        DaiLy item = DaiLy.Load(uSER_NAME);
                        if (item == null)
                            item = new DaiLy();
                        item.USER_NAME = uSER_NAME;
                        item.PASSWORD = pASSWORD;
                        item.HO_TEN = hO_TEN;
                        item.MO_TA = mO_TA;
                        //item.isAdmin = isAdmin;
                        item.MaDoanhNghiep = maDoanhNghiep;

                        ret = item.InsertUpdateUserDaiLy(transaction);

                        if (ret <= 0)
                        {
                            ret01 = false;
                        }

                        if (ret01)
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete
        public int DeleteUserDaiLy(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_User_DaiLy_Delete_ByUSER_NAME]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, USER_NAME);
            db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, PASSWORD);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static bool DeleteUserDaiLy(DaiLy obj)
        {
            return DeleteUserDaiLy(obj.USER_NAME, obj.PASSWORD);
        }

        //---------------------------------------------------------------------------------------------

        public static bool DeleteUserDaiLy(string userName, string passWord)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;

                        DaiLy item = DaiLy.Load(userName, passWord);
                        if (item == null)
                            item = new DaiLy();

                        item.USER_NAME = userName;
                        item.PASSWORD = passWord;


                        if (item.DeleteUserDaiLy(transaction) <= 0)
                        {
                            ret01 = false;
                        }

                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}