using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.Common
{
	public partial class ToKhaiMauDichBoSung : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public int PhienBan { set; get; }
		public bool ChuKySo { set; get; }
		public string GhiChu { set; get; }
		public string MaNguoiNhanHang { set; get; }
		public string TenNguoiNhanHang { set; get; }
		public string MaNguoiGiaoHang { set; get; }
		public string TenNguoiGiaoHang { set; get; }
		public string MaNguoiChiDinhGiaoHang { set; get; }
		public string TenNguoiChiDinhGiaoHang { set; get; }
		public string MaNguoiKhaiHQ { set; get; }
		public string TenNguoiKhaiHQ { set; get; }
		public string NoiDungUyQuyen { set; get; }
		public decimal TongCacKhoanPhaiCong { set; get; }
		public decimal TongCacKhoanPhaiTru { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<ToKhaiMauDichBoSung> ConvertToCollection(IDataReader reader)
		{
			IList<ToKhaiMauDichBoSung> collection = new List<ToKhaiMauDichBoSung>();
			while (reader.Read())
			{
				ToKhaiMauDichBoSung entity = new ToKhaiMauDichBoSung();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhienBan"))) entity.PhienBan = reader.GetInt32(reader.GetOrdinal("PhienBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChuKySo"))) entity.ChuKySo = reader.GetBoolean(reader.GetOrdinal("ChuKySo"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiChiDinhGiaoHang"))) entity.MaNguoiChiDinhGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiChiDinhGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiChiDinhGiaoHang"))) entity.TenNguoiChiDinhGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiChiDinhGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhaiHQ"))) entity.MaNguoiKhaiHQ = reader.GetString(reader.GetOrdinal("MaNguoiKhaiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhaiHQ"))) entity.TenNguoiKhaiHQ = reader.GetString(reader.GetOrdinal("TenNguoiKhaiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungUyQuyen"))) entity.NoiDungUyQuyen = reader.GetString(reader.GetOrdinal("NoiDungUyQuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongCacKhoanPhaiCong"))) entity.TongCacKhoanPhaiCong = reader.GetDecimal(reader.GetOrdinal("TongCacKhoanPhaiCong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongCacKhoanPhaiTru"))) entity.TongCacKhoanPhaiTru = reader.GetDecimal(reader.GetOrdinal("TongCacKhoanPhaiTru"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<ToKhaiMauDichBoSung> collection, long id)
        {
            foreach (ToKhaiMauDichBoSung item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_ToKhaiMauDichBoSung VALUES(@TKMD_ID, @PhienBan, @ChuKySo, @GhiChu, @MaNguoiNhanHang, @TenNguoiNhanHang, @MaNguoiGiaoHang, @TenNguoiGiaoHang, @MaNguoiChiDinhGiaoHang, @TenNguoiChiDinhGiaoHang, @MaNguoiKhaiHQ, @TenNguoiKhaiHQ, @NoiDungUyQuyen, @TongCacKhoanPhaiCong, @TongCacKhoanPhaiTru, @NgayKhaiBao)";
            string update = "UPDATE t_KDT_ToKhaiMauDichBoSung SET TKMD_ID = @TKMD_ID, PhienBan = @PhienBan, ChuKySo = @ChuKySo, GhiChu = @GhiChu, MaNguoiNhanHang = @MaNguoiNhanHang, TenNguoiNhanHang = @TenNguoiNhanHang, MaNguoiGiaoHang = @MaNguoiGiaoHang, TenNguoiGiaoHang = @TenNguoiGiaoHang, MaNguoiChiDinhGiaoHang = @MaNguoiChiDinhGiaoHang, TenNguoiChiDinhGiaoHang = @TenNguoiChiDinhGiaoHang, MaNguoiKhaiHQ = @MaNguoiKhaiHQ, TenNguoiKhaiHQ = @TenNguoiKhaiHQ, NoiDungUyQuyen = @NoiDungUyQuyen, TongCacKhoanPhaiCong = @TongCacKhoanPhaiCong, TongCacKhoanPhaiTru = @TongCacKhoanPhaiTru, NgayKhaiBao = @NgayKhaiBao WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ToKhaiMauDichBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhienBan", SqlDbType.Int, "PhienBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuKySo", SqlDbType.Bit, "ChuKySo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, "MaNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, "MaNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, "TenNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiChiDinhGiaoHang", SqlDbType.VarChar, "MaNguoiChiDinhGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiChiDinhGiaoHang", SqlDbType.NVarChar, "TenNguoiChiDinhGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhaiHQ", SqlDbType.VarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungUyQuyen", SqlDbType.NVarChar, "NoiDungUyQuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongCacKhoanPhaiCong", SqlDbType.Decimal, "TongCacKhoanPhaiCong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongCacKhoanPhaiTru", SqlDbType.Decimal, "TongCacKhoanPhaiTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhienBan", SqlDbType.Int, "PhienBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuKySo", SqlDbType.Bit, "ChuKySo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, "MaNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, "MaNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, "TenNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiChiDinhGiaoHang", SqlDbType.VarChar, "MaNguoiChiDinhGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiChiDinhGiaoHang", SqlDbType.NVarChar, "TenNguoiChiDinhGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhaiHQ", SqlDbType.VarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungUyQuyen", SqlDbType.NVarChar, "NoiDungUyQuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongCacKhoanPhaiCong", SqlDbType.Decimal, "TongCacKhoanPhaiCong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongCacKhoanPhaiTru", SqlDbType.Decimal, "TongCacKhoanPhaiTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_ToKhaiMauDichBoSung VALUES(@TKMD_ID, @PhienBan, @ChuKySo, @GhiChu, @MaNguoiNhanHang, @TenNguoiNhanHang, @MaNguoiGiaoHang, @TenNguoiGiaoHang, @MaNguoiChiDinhGiaoHang, @TenNguoiChiDinhGiaoHang, @MaNguoiKhaiHQ, @TenNguoiKhaiHQ, @NoiDungUyQuyen, @TongCacKhoanPhaiCong, @TongCacKhoanPhaiTru, @NgayKhaiBao)";
            string update = "UPDATE t_KDT_ToKhaiMauDichBoSung SET TKMD_ID = @TKMD_ID, PhienBan = @PhienBan, ChuKySo = @ChuKySo, GhiChu = @GhiChu, MaNguoiNhanHang = @MaNguoiNhanHang, TenNguoiNhanHang = @TenNguoiNhanHang, MaNguoiGiaoHang = @MaNguoiGiaoHang, TenNguoiGiaoHang = @TenNguoiGiaoHang, MaNguoiChiDinhGiaoHang = @MaNguoiChiDinhGiaoHang, TenNguoiChiDinhGiaoHang = @TenNguoiChiDinhGiaoHang, MaNguoiKhaiHQ = @MaNguoiKhaiHQ, TenNguoiKhaiHQ = @TenNguoiKhaiHQ, NoiDungUyQuyen = @NoiDungUyQuyen, TongCacKhoanPhaiCong = @TongCacKhoanPhaiCong, TongCacKhoanPhaiTru = @TongCacKhoanPhaiTru, NgayKhaiBao = @NgayKhaiBao WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ToKhaiMauDichBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhienBan", SqlDbType.Int, "PhienBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuKySo", SqlDbType.Bit, "ChuKySo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, "MaNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, "MaNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, "TenNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiChiDinhGiaoHang", SqlDbType.VarChar, "MaNguoiChiDinhGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiChiDinhGiaoHang", SqlDbType.NVarChar, "TenNguoiChiDinhGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhaiHQ", SqlDbType.VarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungUyQuyen", SqlDbType.NVarChar, "NoiDungUyQuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongCacKhoanPhaiCong", SqlDbType.Decimal, "TongCacKhoanPhaiCong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongCacKhoanPhaiTru", SqlDbType.Decimal, "TongCacKhoanPhaiTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhienBan", SqlDbType.Int, "PhienBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuKySo", SqlDbType.Bit, "ChuKySo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, "MaNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, "MaNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, "TenNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiChiDinhGiaoHang", SqlDbType.VarChar, "MaNguoiChiDinhGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiChiDinhGiaoHang", SqlDbType.NVarChar, "TenNguoiChiDinhGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhaiHQ", SqlDbType.VarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungUyQuyen", SqlDbType.NVarChar, "NoiDungUyQuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongCacKhoanPhaiCong", SqlDbType.Decimal, "TongCacKhoanPhaiCong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongCacKhoanPhaiTru", SqlDbType.Decimal, "TongCacKhoanPhaiTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ToKhaiMauDichBoSung Load(long id)
		{
			const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<ToKhaiMauDichBoSung> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<ToKhaiMauDichBoSung> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<ToKhaiMauDichBoSung> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<ToKhaiMauDichBoSung> SelectCollectionBy_TKMD_ID(long tKMD_ID)
		{
            IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertToKhaiMauDichBoSung(long tKMD_ID, int phienBan, bool chuKySo, string ghiChu, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string maNguoiChiDinhGiaoHang, string tenNguoiChiDinhGiaoHang, string maNguoiKhaiHQ, string tenNguoiKhaiHQ, string noiDungUyQuyen, decimal tongCacKhoanPhaiCong, decimal tongCacKhoanPhaiTru, DateTime ngayKhaiBao)
		{
			ToKhaiMauDichBoSung entity = new ToKhaiMauDichBoSung();	
			entity.TKMD_ID = tKMD_ID;
			entity.PhienBan = phienBan;
			entity.ChuKySo = chuKySo;
			entity.GhiChu = ghiChu;
			entity.MaNguoiNhanHang = maNguoiNhanHang;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.MaNguoiGiaoHang = maNguoiGiaoHang;
			entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
			entity.MaNguoiChiDinhGiaoHang = maNguoiChiDinhGiaoHang;
			entity.TenNguoiChiDinhGiaoHang = tenNguoiChiDinhGiaoHang;
			entity.MaNguoiKhaiHQ = maNguoiKhaiHQ;
			entity.TenNguoiKhaiHQ = tenNguoiKhaiHQ;
			entity.NoiDungUyQuyen = noiDungUyQuyen;
			entity.TongCacKhoanPhaiCong = tongCacKhoanPhaiCong;
			entity.TongCacKhoanPhaiTru = tongCacKhoanPhaiTru;
			entity.NgayKhaiBao = ngayKhaiBao;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@PhienBan", SqlDbType.Int, PhienBan);
			db.AddInParameter(dbCommand, "@ChuKySo", SqlDbType.Bit, ChuKySo);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@MaNguoiChiDinhGiaoHang", SqlDbType.VarChar, MaNguoiChiDinhGiaoHang);
			db.AddInParameter(dbCommand, "@TenNguoiChiDinhGiaoHang", SqlDbType.NVarChar, TenNguoiChiDinhGiaoHang);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiHQ", SqlDbType.VarChar, MaNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, TenNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@NoiDungUyQuyen", SqlDbType.NVarChar, NoiDungUyQuyen);
			db.AddInParameter(dbCommand, "@TongCacKhoanPhaiCong", SqlDbType.Decimal, TongCacKhoanPhaiCong);
			db.AddInParameter(dbCommand, "@TongCacKhoanPhaiTru", SqlDbType.Decimal, TongCacKhoanPhaiTru);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ToKhaiMauDichBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ToKhaiMauDichBoSung item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateToKhaiMauDichBoSung(long id, long tKMD_ID, int phienBan, bool chuKySo, string ghiChu, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string maNguoiChiDinhGiaoHang, string tenNguoiChiDinhGiaoHang, string maNguoiKhaiHQ, string tenNguoiKhaiHQ, string noiDungUyQuyen, decimal tongCacKhoanPhaiCong, decimal tongCacKhoanPhaiTru, DateTime ngayKhaiBao)
		{
			ToKhaiMauDichBoSung entity = new ToKhaiMauDichBoSung();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.PhienBan = phienBan;
			entity.ChuKySo = chuKySo;
			entity.GhiChu = ghiChu;
			entity.MaNguoiNhanHang = maNguoiNhanHang;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.MaNguoiGiaoHang = maNguoiGiaoHang;
			entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
			entity.MaNguoiChiDinhGiaoHang = maNguoiChiDinhGiaoHang;
			entity.TenNguoiChiDinhGiaoHang = tenNguoiChiDinhGiaoHang;
			entity.MaNguoiKhaiHQ = maNguoiKhaiHQ;
			entity.TenNguoiKhaiHQ = tenNguoiKhaiHQ;
			entity.NoiDungUyQuyen = noiDungUyQuyen;
			entity.TongCacKhoanPhaiCong = tongCacKhoanPhaiCong;
			entity.TongCacKhoanPhaiTru = tongCacKhoanPhaiTru;
			entity.NgayKhaiBao = ngayKhaiBao;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_ToKhaiMauDichBoSung_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@PhienBan", SqlDbType.Int, PhienBan);
			db.AddInParameter(dbCommand, "@ChuKySo", SqlDbType.Bit, ChuKySo);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@MaNguoiChiDinhGiaoHang", SqlDbType.VarChar, MaNguoiChiDinhGiaoHang);
			db.AddInParameter(dbCommand, "@TenNguoiChiDinhGiaoHang", SqlDbType.NVarChar, TenNguoiChiDinhGiaoHang);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiHQ", SqlDbType.VarChar, MaNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, TenNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@NoiDungUyQuyen", SqlDbType.NVarChar, NoiDungUyQuyen);
			db.AddInParameter(dbCommand, "@TongCacKhoanPhaiCong", SqlDbType.Decimal, TongCacKhoanPhaiCong);
			db.AddInParameter(dbCommand, "@TongCacKhoanPhaiTru", SqlDbType.Decimal, TongCacKhoanPhaiTru);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ToKhaiMauDichBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ToKhaiMauDichBoSung item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateToKhaiMauDichBoSung(long id, long tKMD_ID, int phienBan, bool chuKySo, string ghiChu, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string maNguoiChiDinhGiaoHang, string tenNguoiChiDinhGiaoHang, string maNguoiKhaiHQ, string tenNguoiKhaiHQ, string noiDungUyQuyen, decimal tongCacKhoanPhaiCong, decimal tongCacKhoanPhaiTru, DateTime ngayKhaiBao)
		{
			ToKhaiMauDichBoSung entity = new ToKhaiMauDichBoSung();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.PhienBan = phienBan;
			entity.ChuKySo = chuKySo;
			entity.GhiChu = ghiChu;
			entity.MaNguoiNhanHang = maNguoiNhanHang;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.MaNguoiGiaoHang = maNguoiGiaoHang;
			entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
			entity.MaNguoiChiDinhGiaoHang = maNguoiChiDinhGiaoHang;
			entity.TenNguoiChiDinhGiaoHang = tenNguoiChiDinhGiaoHang;
			entity.MaNguoiKhaiHQ = maNguoiKhaiHQ;
			entity.TenNguoiKhaiHQ = tenNguoiKhaiHQ;
			entity.NoiDungUyQuyen = noiDungUyQuyen;
			entity.TongCacKhoanPhaiCong = tongCacKhoanPhaiCong;
			entity.TongCacKhoanPhaiTru = tongCacKhoanPhaiTru;
			entity.NgayKhaiBao = ngayKhaiBao;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@PhienBan", SqlDbType.Int, PhienBan);
			db.AddInParameter(dbCommand, "@ChuKySo", SqlDbType.Bit, ChuKySo);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@MaNguoiChiDinhGiaoHang", SqlDbType.VarChar, MaNguoiChiDinhGiaoHang);
			db.AddInParameter(dbCommand, "@TenNguoiChiDinhGiaoHang", SqlDbType.NVarChar, TenNguoiChiDinhGiaoHang);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiHQ", SqlDbType.VarChar, MaNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, TenNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@NoiDungUyQuyen", SqlDbType.NVarChar, NoiDungUyQuyen);
			db.AddInParameter(dbCommand, "@TongCacKhoanPhaiCong", SqlDbType.Decimal, TongCacKhoanPhaiCong);
			db.AddInParameter(dbCommand, "@TongCacKhoanPhaiTru", SqlDbType.Decimal, TongCacKhoanPhaiTru);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ToKhaiMauDichBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ToKhaiMauDichBoSung item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteToKhaiMauDichBoSung(long id)
		{
			ToKhaiMauDichBoSung entity = new ToKhaiMauDichBoSung();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ToKhaiMauDichBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ToKhaiMauDichBoSung item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}