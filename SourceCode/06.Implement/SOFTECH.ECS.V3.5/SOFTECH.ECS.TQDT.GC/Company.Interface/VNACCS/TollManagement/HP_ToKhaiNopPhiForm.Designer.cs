﻿namespace Company.Interface.VNACCS.TollManagement
{
    partial class HP_ToKhaiNopPhiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListContainer_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HP_ToKhaiNopPhiForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgListFile_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem14 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem15 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem16 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageVDN = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListContainer = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteContainer = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.grbHangContainer = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbTinhChatContainer = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoSeal = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbLoaiContainer = new Janus.Windows.EditControls.UIComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.grbHangRoi = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbDVT = new Janus.Windows.EditControls.UIComboBox();
            this.txtTongTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.btnAddContainer = new Janus.Windows.EditControls.UIButton();
            this.btnAddExcel = new Janus.Windows.EditControls.UIButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTabPageContainer = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListFile = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteFile = new Janus.Windows.EditControls.UIButton();
            this.lblTotalSize = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.grbContainer = new Janus.Windows.EditControls.UIGroupBox();
            this.btnViewFile = new Janus.Windows.EditControls.UIButton();
            this.btnSelect = new Janus.Windows.EditControls.UIButton();
            this.btnAddFile = new Janus.Windows.EditControls.UIButton();
            this.cbbLoaiFile = new Janus.Windows.EditControls.UIComboBox();
            this.txtFileSize = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtTenFile = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbDDNP = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.cbbLoaiHangHoa = new Janus.Windows.EditControls.UIComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.cbbHinhThucTT = new Janus.Windows.EditControls.UIComboBox();
            this.cbbNhomLoaiHinh = new Janus.Windows.EditControls.UIComboBox();
            this.ctrMaPhuongThucVT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaLoaiHinh = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDDLuuKho = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayCT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayToKhai = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoCT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdUpdateGuidstring1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidstring");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdUpdateGuidstring = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidstring");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPageVDN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListContainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHangContainer)).BeginInit();
            this.grbHangContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHangRoi)).BeginInit();
            this.grbHangRoi.SuspendLayout();
            this.uiTabPageContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbContainer)).BeginInit();
            this.grbContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(1305, 820);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiTab1);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 254);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1305, 521);
            this.uiGroupBox1.TabIndex = 40;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(3, 8);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1299, 510);
            this.uiTab1.TabIndex = 17;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageVDN,
            this.uiTabPageContainer});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPageVDN
            // 
            this.uiTabPageVDN.Controls.Add(this.uiGroupBox7);
            this.uiTabPageVDN.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageVDN.Name = "uiTabPageVDN";
            this.uiTabPageVDN.Size = new System.Drawing.Size(1297, 488);
            this.uiTabPageVDN.TabStop = true;
            this.uiTabPageVDN.Text = "Danh sách Container";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.uiGroupBox11);
            this.uiGroupBox7.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox7.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1297, 488);
            this.uiGroupBox7.TabIndex = 2;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.AutoScroll = true;
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.dgListContainer);
            this.uiGroupBox11.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox11.Location = new System.Drawing.Point(3, 291);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(1291, 140);
            this.uiGroupBox11.TabIndex = 3;
            this.uiGroupBox11.Text = "Danh sách Container";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListContainer
            // 
            this.dgListContainer.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListContainer.AlternatingColors = true;
            this.dgListContainer.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListContainer.ColumnAutoResize = true;
            dgListContainer_DesignTimeLayout.LayoutString = resources.GetString("dgListContainer_DesignTimeLayout.LayoutString");
            this.dgListContainer.DesignTimeLayout = dgListContainer_DesignTimeLayout;
            this.dgListContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListContainer.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListContainer.FrozenColumns = 3;
            this.dgListContainer.GroupByBoxVisible = false;
            this.dgListContainer.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListContainer.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListContainer.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListContainer.Location = new System.Drawing.Point(3, 17);
            this.dgListContainer.Margin = new System.Windows.Forms.Padding(0);
            this.dgListContainer.Name = "dgListContainer";
            this.dgListContainer.RecordNavigator = true;
            this.dgListContainer.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListContainer.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListContainer.Size = new System.Drawing.Size(1285, 120);
            this.dgListContainer.TabIndex = 15;
            this.dgListContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListContainer.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListContainer_RowDoubleClick);
            this.dgListContainer.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListContainer_LoadingRow);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.Controls.Add(this.btnDeleteContainer);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 443);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1291, 42);
            this.uiGroupBox4.TabIndex = 2;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnDeleteContainer
            // 
            this.btnDeleteContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteContainer.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteContainer.Image")));
            this.btnDeleteContainer.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteContainer.Location = new System.Drawing.Point(1188, 13);
            this.btnDeleteContainer.Name = "btnDeleteContainer";
            this.btnDeleteContainer.Size = new System.Drawing.Size(94, 23);
            this.btnDeleteContainer.TabIndex = 28;
            this.btnDeleteContainer.Text = "Xóa";
            this.btnDeleteContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteContainer.Click += new System.EventHandler(this.btnDeleteContainer_Click);
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.grbHangContainer);
            this.uiGroupBox9.Controls.Add(this.grbHangRoi);
            this.uiGroupBox9.Controls.Add(this.btnAddContainer);
            this.uiGroupBox9.Controls.Add(this.btnAddExcel);
            this.uiGroupBox9.Controls.Add(this.label10);
            this.uiGroupBox9.Controls.Add(this.label5);
            this.uiGroupBox9.Controls.Add(this.txtGhiChu);
            this.uiGroupBox9.Controls.Add(this.txtSoVanDon);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox9.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(1291, 283);
            this.uiGroupBox9.TabIndex = 0;
            this.uiGroupBox9.Text = "Thông tin Container";
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grbHangContainer
            // 
            this.grbHangContainer.Controls.Add(this.cbbTinhChatContainer);
            this.grbHangContainer.Controls.Add(this.txtSoContainer);
            this.grbHangContainer.Controls.Add(this.txtSoLuong);
            this.grbHangContainer.Controls.Add(this.label9);
            this.grbHangContainer.Controls.Add(this.txtSoSeal);
            this.grbHangContainer.Controls.Add(this.label29);
            this.grbHangContainer.Controls.Add(this.label28);
            this.grbHangContainer.Controls.Add(this.label6);
            this.grbHangContainer.Controls.Add(this.cbbLoaiContainer);
            this.grbHangContainer.Controls.Add(this.label8);
            this.grbHangContainer.Location = new System.Drawing.Point(10, 51);
            this.grbHangContainer.Name = "grbHangContainer";
            this.grbHangContainer.Size = new System.Drawing.Size(411, 184);
            this.grbHangContainer.TabIndex = 0;
            this.grbHangContainer.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbTinhChatContainer
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Container hàng khô";
            uiComboBoxItem1.Value = "KHO";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Container hàng lạnh";
            uiComboBoxItem2.Value = "LANH";
            this.cbbTinhChatContainer.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbTinhChatContainer.Location = new System.Drawing.Point(131, 122);
            this.cbbTinhChatContainer.Name = "cbbTinhChatContainer";
            this.cbbTinhChatContainer.Size = new System.Drawing.Size(257, 21);
            this.cbbTinhChatContainer.TabIndex = 21;
            this.cbbTinhChatContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtSoContainer
            // 
            this.txtSoContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoContainer.Location = new System.Drawing.Point(96, 16);
            this.txtSoContainer.Name = "txtSoContainer";
            this.txtSoContainer.Size = new System.Drawing.Size(292, 21);
            this.txtSoContainer.TabIndex = 18;
            this.txtSoContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 20;
            this.txtSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong.Location = new System.Drawing.Point(96, 152);
            this.txtSoLuong.MaxLength = 15;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(127, 21);
            this.txtSoLuong.TabIndex = 22;
            this.txtSoLuong.Text = "0";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số lượng :";
            // 
            // txtSoSeal
            // 
            this.txtSoSeal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoSeal.Location = new System.Drawing.Point(96, 51);
            this.txtSoSeal.Name = "txtSoSeal";
            this.txtSoSeal.Size = new System.Drawing.Size(292, 21);
            this.txtSoSeal.TabIndex = 19;
            this.txtSoSeal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(5, 20);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(74, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Số container :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(5, 55);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(48, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "Số seal :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Loại Container :";
            // 
            // cbbLoaiContainer
            // 
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Container 20 feet";
            uiComboBoxItem3.Value = "20";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Container 40 feet";
            uiComboBoxItem4.Value = "40";
            this.cbbLoaiContainer.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbbLoaiContainer.Location = new System.Drawing.Point(96, 86);
            this.cbbLoaiContainer.Name = "cbbLoaiContainer";
            this.cbbLoaiContainer.Size = new System.Drawing.Size(292, 21);
            this.cbbLoaiContainer.TabIndex = 20;
            this.cbbLoaiContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Tính chất Container :";
            // 
            // grbHangRoi
            // 
            this.grbHangRoi.Controls.Add(this.cbbDVT);
            this.grbHangRoi.Controls.Add(this.txtTongTrongLuong);
            this.grbHangRoi.Controls.Add(this.label14);
            this.grbHangRoi.Controls.Add(this.label22);
            this.grbHangRoi.Location = new System.Drawing.Point(429, 56);
            this.grbHangRoi.Name = "grbHangRoi";
            this.grbHangRoi.Size = new System.Drawing.Size(252, 124);
            this.grbHangRoi.TabIndex = 0;
            this.grbHangRoi.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbDVT
            // 
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Kilogam";
            uiComboBoxItem5.Value = "KG";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Tấn";
            uiComboBoxItem6.Value = "TAN";
            this.cbbDVT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbbDVT.Location = new System.Drawing.Point(108, 71);
            this.cbbDVT.Name = "cbbDVT";
            this.cbbDVT.Size = new System.Drawing.Size(127, 21);
            this.cbbDVT.TabIndex = 21;
            this.cbbDVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtTongTrongLuong
            // 
            this.txtTongTrongLuong.DecimalDigits = 20;
            this.txtTongTrongLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongTrongLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTrongLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongTrongLuong.Location = new System.Drawing.Point(108, 34);
            this.txtTongTrongLuong.MaxLength = 15;
            this.txtTongTrongLuong.Name = "txtTongTrongLuong";
            this.txtTongTrongLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTrongLuong.Size = new System.Drawing.Size(127, 21);
            this.txtTongTrongLuong.TabIndex = 23;
            this.txtTongTrongLuong.Text = "0";
            this.txtTongTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 38);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(97, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Tổng trọng lượng :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 76);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(66, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Đơn vị tính :";
            // 
            // btnAddContainer
            // 
            this.btnAddContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddContainer.Image = ((System.Drawing.Image)(resources.GetObject("btnAddContainer.Image")));
            this.btnAddContainer.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddContainer.Location = new System.Drawing.Point(112, 247);
            this.btnAddContainer.Name = "btnAddContainer";
            this.btnAddContainer.Size = new System.Drawing.Size(94, 23);
            this.btnAddContainer.TabIndex = 26;
            this.btnAddContainer.Text = "Ghi";
            this.btnAddContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddContainer.Click += new System.EventHandler(this.btnAddContainer_Click);
            // 
            // btnAddExcel
            // 
            this.btnAddExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnAddExcel.Image")));
            this.btnAddExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddExcel.Location = new System.Drawing.Point(212, 247);
            this.btnAddExcel.Name = "btnAddExcel";
            this.btnAddExcel.Size = new System.Drawing.Size(94, 23);
            this.btnAddExcel.TabIndex = 27;
            this.btnAddExcel.Text = "Nhập Excel";
            this.btnAddExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddExcel.Click += new System.EventHandler(this.btnAddExcel_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(437, 208);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Ghi chú : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số vận đơn :";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(495, 203);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(381, 21);
            this.txtGhiChu.TabIndex = 25;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon.Location = new System.Drawing.Point(104, 24);
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(292, 21);
            this.txtSoVanDon.TabIndex = 17;
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiTabPageContainer
            // 
            this.uiTabPageContainer.Controls.Add(this.uiGroupBox5);
            this.uiTabPageContainer.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageContainer.Name = "uiTabPageContainer";
            this.uiTabPageContainer.Size = new System.Drawing.Size(1091, 482);
            this.uiTabPageContainer.TabStop = true;
            this.uiTabPageContainer.Text = "Chứng từ đính kèm";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.uiGroupBox13);
            this.uiGroupBox5.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox5.Controls.Add(this.grbContainer);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1091, 482);
            this.uiGroupBox5.TabIndex = 0;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.AutoScroll = true;
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.dgListFile);
            this.uiGroupBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox13.Location = new System.Drawing.Point(3, 141);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(1085, 292);
            this.uiGroupBox13.TabIndex = 6;
            this.uiGroupBox13.Text = "Danh sách File đính kèm";
            this.uiGroupBox13.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListFile
            // 
            this.dgListFile.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListFile.AlternatingColors = true;
            this.dgListFile.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListFile.ColumnAutoResize = true;
            dgListFile_DesignTimeLayout.LayoutString = resources.GetString("dgListFile_DesignTimeLayout.LayoutString");
            this.dgListFile.DesignTimeLayout = dgListFile_DesignTimeLayout;
            this.dgListFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListFile.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListFile.FrozenColumns = 3;
            this.dgListFile.GroupByBoxVisible = false;
            this.dgListFile.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListFile.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListFile.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListFile.Location = new System.Drawing.Point(3, 17);
            this.dgListFile.Margin = new System.Windows.Forms.Padding(0);
            this.dgListFile.Name = "dgListFile";
            this.dgListFile.RecordNavigator = true;
            this.dgListFile.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListFile.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListFile.Size = new System.Drawing.Size(1079, 272);
            this.dgListFile.TabIndex = 15;
            this.dgListFile.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListFile.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListFile_RowDoubleClick);
            this.dgListFile.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListFile_LoadingRow);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.btnDeleteFile);
            this.uiGroupBox6.Controls.Add(this.lblTotalSize);
            this.uiGroupBox6.Controls.Add(this.label30);
            this.uiGroupBox6.Controls.Add(this.label26);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 437);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1085, 42);
            this.uiGroupBox6.TabIndex = 5;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnDeleteFile
            // 
            this.btnDeleteFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteFile.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteFile.Image")));
            this.btnDeleteFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteFile.Location = new System.Drawing.Point(997, 12);
            this.btnDeleteFile.Name = "btnDeleteFile";
            this.btnDeleteFile.Size = new System.Drawing.Size(80, 23);
            this.btnDeleteFile.TabIndex = 27;
            this.btnDeleteFile.Text = "Xóa";
            this.btnDeleteFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteFile.Click += new System.EventHandler(this.btnDeleteFile_Click);
            // 
            // lblTotalSize
            // 
            this.lblTotalSize.AutoSize = true;
            this.lblTotalSize.ForeColor = System.Drawing.Color.Blue;
            this.lblTotalSize.Location = new System.Drawing.Point(108, 17);
            this.lblTotalSize.Name = "lblTotalSize";
            this.lblTotalSize.Size = new System.Drawing.Size(63, 13);
            this.lblTotalSize.TabIndex = 35;
            this.lblTotalSize.Text = "{Total Size}";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 17);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(95, 13);
            this.label30.TabIndex = 35;
            this.label30.Text = "Tổng dung lượng :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(291, 17);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(336, 13);
            this.label26.TabIndex = 35;
            this.label26.Text = "Tổng dung lượng File đính kèm lên tối đa cho phép là : 2 Mb";
            // 
            // grbContainer
            // 
            this.grbContainer.AutoScroll = true;
            this.grbContainer.BackColor = System.Drawing.Color.Transparent;
            this.grbContainer.Controls.Add(this.btnViewFile);
            this.grbContainer.Controls.Add(this.btnSelect);
            this.grbContainer.Controls.Add(this.btnAddFile);
            this.grbContainer.Controls.Add(this.cbbLoaiFile);
            this.grbContainer.Controls.Add(this.txtFileSize);
            this.grbContainer.Controls.Add(this.label25);
            this.grbContainer.Controls.Add(this.label23);
            this.grbContainer.Controls.Add(this.label24);
            this.grbContainer.Controls.Add(this.txtTenFile);
            this.grbContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.grbContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbContainer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grbContainer.Location = new System.Drawing.Point(3, 8);
            this.grbContainer.Name = "grbContainer";
            this.grbContainer.Size = new System.Drawing.Size(1085, 133);
            this.grbContainer.TabIndex = 0;
            this.grbContainer.Text = "Thông tin về file đính kèm";
            this.grbContainer.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnViewFile
            // 
            this.btnViewFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewFile.Image = ((System.Drawing.Image)(resources.GetObject("btnViewFile.Image")));
            this.btnViewFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnViewFile.Location = new System.Drawing.Point(648, 31);
            this.btnViewFile.Name = "btnViewFile";
            this.btnViewFile.Size = new System.Drawing.Size(93, 23);
            this.btnViewFile.TabIndex = 19;
            this.btnViewFile.Text = "Xem File";
            this.btnViewFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnViewFile.Click += new System.EventHandler(this.btnViewFile_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
            this.btnSelect.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSelect.Location = new System.Drawing.Point(549, 31);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(93, 23);
            this.btnSelect.TabIndex = 18;
            this.btnSelect.Text = "Chọn File";
            this.btnSelect.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnAddFile
            // 
            this.btnAddFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddFile.Image = ((System.Drawing.Image)(resources.GetObject("btnAddFile.Image")));
            this.btnAddFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddFile.Location = new System.Drawing.Point(104, 97);
            this.btnAddFile.Name = "btnAddFile";
            this.btnAddFile.Size = new System.Drawing.Size(82, 23);
            this.btnAddFile.TabIndex = 21;
            this.btnAddFile.Text = "Ghi";
            this.btnAddFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddFile.Click += new System.EventHandler(this.btnAddFile_Click);
            // 
            // cbbLoaiFile
            // 
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "1: Tờ khai Hải quan";
            uiComboBoxItem7.Value = "1";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "2. Vận đơn";
            uiComboBoxItem8.Value = "2";
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "9. Chứng từ khác";
            uiComboBoxItem9.Value = "9";
            this.cbbLoaiFile.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9});
            this.cbbLoaiFile.Location = new System.Drawing.Point(104, 66);
            this.cbbLoaiFile.Name = "cbbLoaiFile";
            this.cbbLoaiFile.Size = new System.Drawing.Size(292, 21);
            this.cbbLoaiFile.TabIndex = 20;
            this.cbbLoaiFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtFileSize
            // 
            this.txtFileSize.AutoSize = true;
            this.txtFileSize.ForeColor = System.Drawing.Color.Blue;
            this.txtFileSize.Location = new System.Drawing.Point(480, 70);
            this.txtFileSize.Name = "txtFileSize";
            this.txtFileSize.Size = new System.Drawing.Size(55, 13);
            this.txtFileSize.TabIndex = 0;
            this.txtFileSize.Text = "{File Size}";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(405, 70);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(69, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Dung lượng :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 70);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(55, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Loại File  :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(13, 35);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Tên File : ";
            // 
            // txtTenFile
            // 
            this.txtTenFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenFile.Location = new System.Drawing.Point(104, 31);
            this.txtTenFile.Name = "txtTenFile";
            this.txtTenFile.Size = new System.Drawing.Size(439, 21);
            this.txtTenFile.TabIndex = 17;
            this.txtTenFile.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 775);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1305, 45);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1215, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 29;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.cbbDDNP);
            this.uiGroupBox3.Controls.Add(this.cbbLoaiHangHoa);
            this.uiGroupBox3.Controls.Add(this.label27);
            this.uiGroupBox3.Controls.Add(this.label31);
            this.uiGroupBox3.Controls.Add(this.lblStatus);
            this.uiGroupBox3.Controls.Add(this.cbbHinhThucTT);
            this.uiGroupBox3.Controls.Add(this.cbbNhomLoaiHinh);
            this.uiGroupBox3.Controls.Add(this.ctrMaPhuongThucVT);
            this.uiGroupBox3.Controls.Add(this.ctrMaLoaiHinh);
            this.uiGroupBox3.Controls.Add(this.ctrMaDDLuuKho);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox3.Controls.Add(this.clcNgayCT);
            this.uiGroupBox3.Controls.Add(this.clcNgayToKhai);
            this.uiGroupBox3.Controls.Add(this.clcNgayTiepNhan);
            this.uiGroupBox3.Controls.Add(this.txtTenDoanhNghiep);
            this.uiGroupBox3.Controls.Add(this.label13);
            this.uiGroupBox3.Controls.Add(this.txtSoCT);
            this.uiGroupBox3.Controls.Add(this.txtMaHQ);
            this.uiGroupBox3.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox3.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox3.Controls.Add(this.label12);
            this.uiGroupBox3.Controls.Add(this.label15);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Controls.Add(this.label21);
            this.uiGroupBox3.Controls.Add(this.label20);
            this.uiGroupBox3.Controls.Add(this.label19);
            this.uiGroupBox3.Controls.Add(this.label18);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1305, 254);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Thông tin chung";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // cbbDDNP
            // 
            this.cbbDDNP.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cbbDDNP.Appearance.Options.UseBackColor = true;
            this.cbbDDNP.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A205;
            this.cbbDDNP.Code = "";
            this.cbbDDNP.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbDDNP.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.cbbDDNP.IsOnlyWarning = false;
            this.cbbDDNP.IsValidate = true;
            this.cbbDDNP.Location = new System.Drawing.Point(496, 148);
            this.cbbDDNP.Name = "cbbDDNP";
            this.cbbDDNP.Name_VN = "";
            this.cbbDDNP.SetOnlyWarning = false;
            this.cbbDDNP.SetValidate = false;
            this.cbbDDNP.ShowColumnCode = true;
            this.cbbDDNP.ShowColumnName = true;
            this.cbbDDNP.Size = new System.Drawing.Size(307, 21);
            this.cbbDDNP.TabIndex = 8;
            this.cbbDDNP.TagCode = "";
            this.cbbDDNP.TagName = "";
            this.cbbDDNP.Where = null;
            this.cbbDDNP.WhereCondition = "";
            // 
            // cbbLoaiHangHoa
            // 
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Hàng container";
            uiComboBoxItem10.Value = "100";
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Hàng lỏng, hàng rời";
            uiComboBoxItem11.Value = "101";
            this.cbbLoaiHangHoa.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem10,
            uiComboBoxItem11});
            this.cbbLoaiHangHoa.Location = new System.Drawing.Point(594, 30);
            this.cbbLoaiHangHoa.Name = "cbbLoaiHangHoa";
            this.cbbLoaiHangHoa.Size = new System.Drawing.Size(209, 21);
            this.cbbLoaiHangHoa.TabIndex = 2;
            this.cbbLoaiHangHoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiHangHoa.SelectedValueChanged += new System.EventHandler(this.cbbLoaiHangHoa_SelectedValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(493, 33);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(95, 14);
            this.label27.TabIndex = 0;
            this.label27.Text = "Loại hàng hóa : ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(388, 151);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(87, 14);
            this.label31.TabIndex = 0;
            this.label31.Text = "Điểm nộp phí :";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.ForeColor = System.Drawing.Color.Blue;
            this.lblStatus.Location = new System.Drawing.Point(926, 35);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(38, 13);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "Status";
            // 
            // cbbHinhThucTT
            // 
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "Tiền mặt";
            uiComboBoxItem12.Value = "TM";
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "Chuyển khoản";
            uiComboBoxItem13.Value = "CK";
            this.cbbHinhThucTT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem12,
            uiComboBoxItem13});
            this.cbbHinhThucTT.Location = new System.Drawing.Point(967, 219);
            this.cbbHinhThucTT.Name = "cbbHinhThucTT";
            this.cbbHinhThucTT.Size = new System.Drawing.Size(123, 21);
            this.cbbHinhThucTT.TabIndex = 16;
            this.cbbHinhThucTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbNhomLoaiHinh
            // 
            uiComboBoxItem14.FormatStyle.Alpha = 0;
            uiComboBoxItem14.IsSeparator = false;
            uiComboBoxItem14.Text = "Hàng tạm nhập, tái xuất, hàng chuyển khẩu, hàng gửi kho ngoại quan";
            uiComboBoxItem14.Value = "TF001";
            uiComboBoxItem15.FormatStyle.Alpha = 0;
            uiComboBoxItem15.IsSeparator = false;
            uiComboBoxItem15.Text = "Đối với hàng quá cảnh";
            uiComboBoxItem15.Value = "TF002";
            uiComboBoxItem16.FormatStyle.Alpha = 0;
            uiComboBoxItem16.IsSeparator = false;
            uiComboBoxItem16.Text = "Đối với hàng hóa nhập khẩu, hàng hóa xuất khẩu";
            uiComboBoxItem16.Value = "TF003";
            this.cbbNhomLoaiHinh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem14,
            uiComboBoxItem15,
            uiComboBoxItem16});
            this.cbbNhomLoaiHinh.Location = new System.Drawing.Point(111, 219);
            this.cbbNhomLoaiHinh.Name = "cbbNhomLoaiHinh";
            this.cbbNhomLoaiHinh.Size = new System.Drawing.Size(482, 21);
            this.cbbNhomLoaiHinh.TabIndex = 11;
            this.cbbNhomLoaiHinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // ctrMaPhuongThucVT
            // 
            this.ctrMaPhuongThucVT.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaPhuongThucVT.Appearance.Options.UseBackColor = true;
            this.ctrMaPhuongThucVT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E005;
            this.ctrMaPhuongThucVT.Code = "";
            this.ctrMaPhuongThucVT.ColorControl = System.Drawing.Color.White;
            this.ctrMaPhuongThucVT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhuongThucVT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhuongThucVT.IsOnlyWarning = false;
            this.ctrMaPhuongThucVT.IsValidate = true;
            this.ctrMaPhuongThucVT.Location = new System.Drawing.Point(111, 185);
            this.ctrMaPhuongThucVT.Name = "ctrMaPhuongThucVT";
            this.ctrMaPhuongThucVT.Name_VN = "";
            this.ctrMaPhuongThucVT.SetOnlyWarning = false;
            this.ctrMaPhuongThucVT.SetValidate = false;
            this.ctrMaPhuongThucVT.ShowColumnCode = true;
            this.ctrMaPhuongThucVT.ShowColumnName = true;
            this.ctrMaPhuongThucVT.Size = new System.Drawing.Size(264, 21);
            this.ctrMaPhuongThucVT.TabIndex = 9;
            this.ctrMaPhuongThucVT.TagName = "";
            this.ctrMaPhuongThucVT.Where = null;
            this.ctrMaPhuongThucVT.WhereCondition = "";
            // 
            // ctrMaLoaiHinh
            // 
            this.ctrMaLoaiHinh.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaLoaiHinh.Appearance.Options.UseBackColor = true;
            this.ctrMaLoaiHinh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E001;
            this.ctrMaLoaiHinh.Code = "";
            this.ctrMaLoaiHinh.ColorControl = System.Drawing.Color.White;
            this.ctrMaLoaiHinh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaLoaiHinh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaLoaiHinh.IsOnlyWarning = false;
            this.ctrMaLoaiHinh.IsValidate = true;
            this.ctrMaLoaiHinh.Location = new System.Drawing.Point(111, 148);
            this.ctrMaLoaiHinh.Name = "ctrMaLoaiHinh";
            this.ctrMaLoaiHinh.Name_VN = "";
            this.ctrMaLoaiHinh.SetOnlyWarning = false;
            this.ctrMaLoaiHinh.SetValidate = false;
            this.ctrMaLoaiHinh.ShowColumnCode = true;
            this.ctrMaLoaiHinh.ShowColumnName = true;
            this.ctrMaLoaiHinh.Size = new System.Drawing.Size(271, 21);
            this.ctrMaLoaiHinh.TabIndex = 7;
            this.ctrMaLoaiHinh.TagName = "";
            this.ctrMaLoaiHinh.Where = null;
            this.ctrMaLoaiHinh.WhereCondition = "";
            // 
            // ctrMaDDLuuKho
            // 
            this.ctrMaDDLuuKho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDDLuuKho.Appearance.Options.UseBackColor = true;
            this.ctrMaDDLuuKho.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDDLuuKho.Code = "";
            this.ctrMaDDLuuKho.ColorControl = System.Drawing.Color.White;
            this.ctrMaDDLuuKho.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDDLuuKho.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDDLuuKho.IsOnlyWarning = false;
            this.ctrMaDDLuuKho.IsValidate = true;
            this.ctrMaDDLuuKho.Location = new System.Drawing.Point(497, 185);
            this.ctrMaDDLuuKho.Name = "ctrMaDDLuuKho";
            this.ctrMaDDLuuKho.Name_VN = "";
            this.ctrMaDDLuuKho.SetOnlyWarning = false;
            this.ctrMaDDLuuKho.SetValidate = false;
            this.ctrMaDDLuuKho.ShowColumnCode = true;
            this.ctrMaDDLuuKho.ShowColumnName = false;
            this.ctrMaDDLuuKho.Size = new System.Drawing.Size(96, 21);
            this.ctrMaDDLuuKho.TabIndex = 10;
            this.ctrMaDDLuuKho.TagName = "";
            this.ctrMaDDLuuKho.Where = null;
            this.ctrMaDDLuuKho.WhereCondition = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã đơn vị :";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.BackColor = System.Drawing.Color.White;
            this.txtSoToKhai.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(111, 105);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(218, 22);
            this.txtSoToKhai.TabIndex = 4;
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoToKhai.ButtonClick += new System.EventHandler(this.txtSoToKhai_ButtonClick);
            // 
            // clcNgayCT
            // 
            this.clcNgayCT.AutoSize = false;
            this.clcNgayCT.BackColor = System.Drawing.Color.White;
            this.clcNgayCT.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.clcNgayCT.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayCT.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcNgayCT.DropDownCalendar.Name = "";
            this.clcNgayCT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayCT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayCT.Location = new System.Drawing.Point(929, 181);
            this.clcNgayCT.Name = "clcNgayCT";
            this.clcNgayCT.Nullable = true;
            this.clcNgayCT.NullButtonText = "Xóa";
            this.clcNgayCT.ReadOnly = true;
            this.clcNgayCT.ShowNullButton = true;
            this.clcNgayCT.Size = new System.Drawing.Size(159, 22);
            this.clcNgayCT.TabIndex = 15;
            this.clcNgayCT.TodayButtonText = "Hôm nay";
            this.clcNgayCT.Value = new System.DateTime(2019, 6, 12, 0, 0, 0, 0);
            this.clcNgayCT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayToKhai
            // 
            this.clcNgayToKhai.AutoSize = false;
            this.clcNgayToKhai.BackColor = System.Drawing.Color.White;
            this.clcNgayToKhai.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.clcNgayToKhai.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayToKhai.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcNgayToKhai.DropDownCalendar.Name = "";
            this.clcNgayToKhai.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayToKhai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayToKhai.Location = new System.Drawing.Point(434, 104);
            this.clcNgayToKhai.Name = "clcNgayToKhai";
            this.clcNgayToKhai.Nullable = true;
            this.clcNgayToKhai.NullButtonText = "Xóa";
            this.clcNgayToKhai.ShowNullButton = true;
            this.clcNgayToKhai.Size = new System.Drawing.Size(159, 22);
            this.clcNgayToKhai.TabIndex = 5;
            this.clcNgayToKhai.TodayButtonText = "Hôm nay";
            this.clcNgayToKhai.Value = new System.DateTime(2019, 6, 12, 0, 0, 0, 0);
            this.clcNgayToKhai.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayTiepNhan
            // 
            this.clcNgayTiepNhan.AutoSize = false;
            this.clcNgayTiepNhan.BackColor = System.Drawing.Color.White;
            this.clcNgayTiepNhan.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.clcNgayTiepNhan.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcNgayTiepNhan.DropDownCalendar.Name = "";
            this.clcNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTiepNhan.Location = new System.Drawing.Point(929, 105);
            this.clcNgayTiepNhan.Name = "clcNgayTiepNhan";
            this.clcNgayTiepNhan.Nullable = true;
            this.clcNgayTiepNhan.NullButtonText = "Xóa";
            this.clcNgayTiepNhan.ReadOnly = true;
            this.clcNgayTiepNhan.ShowNullButton = true;
            this.clcNgayTiepNhan.Size = new System.Drawing.Size(159, 22);
            this.clcNgayTiepNhan.TabIndex = 13;
            this.clcNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.clcNgayTiepNhan.Value = new System.DateTime(2019, 6, 12, 0, 0, 0, 0);
            this.clcNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.BackColor = System.Drawing.Color.White;
            this.txtTenDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(111, 65);
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(692, 22);
            this.txtTenDoanhNghiep.TabIndex = 3;
            this.txtTenDoanhNghiep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(14, 223);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "Nhóm loại hình :";
            // 
            // txtSoCT
            // 
            this.txtSoCT.BackColor = System.Drawing.Color.White;
            this.txtSoCT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCT.Location = new System.Drawing.Point(929, 147);
            this.txtSoCT.Name = "txtSoCT";
            this.txtSoCT.ReadOnly = true;
            this.txtSoCT.Size = new System.Drawing.Size(161, 22);
            this.txtSoCT.TabIndex = 14;
            this.txtSoCT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHQ
            // 
            this.txtMaHQ.BackColor = System.Drawing.Color.White;
            this.txtMaHQ.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHQ.Location = new System.Drawing.Point(692, 105);
            this.txtMaHQ.Name = "txtMaHQ";
            this.txtMaHQ.Size = new System.Drawing.Size(111, 22);
            this.txtMaHQ.TabIndex = 6;
            this.txtMaHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.BackColor = System.Drawing.Color.White;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(929, 64);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(161, 22);
            this.txtSoTiepNhan.TabIndex = 12;
            this.txtSoTiepNhan.Tag = "0";
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.BackColor = System.Drawing.Color.White;
            this.txtMaDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(111, 30);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(370, 22);
            this.txtMaDoanhNghiep.TabIndex = 1;
            this.txtMaDoanhNghiep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(14, 188);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 14);
            this.label12.TabIndex = 0;
            this.label12.Text = "Mã loại PTVC :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(386, 188);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 14);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mã địa điểm :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(14, 151);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 14);
            this.label11.TabIndex = 0;
            this.label11.Text = "Loại hình :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(827, 223);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(134, 14);
            this.label21.TabIndex = 0;
            this.label21.Text = "Hình thức thanh toán :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(827, 185);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(98, 14);
            this.label20.TabIndex = 0;
            this.label20.Text = "Ngày chứng từ :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(827, 151);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(85, 14);
            this.label19.TabIndex = 0;
            this.label19.Text = "Số chứng từ :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(827, 109);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(98, 14);
            this.label18.TabIndex = 0;
            this.label18.Text = "Ngày tiếp nhận :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(827, 69);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 14);
            this.label17.TabIndex = 0;
            this.label17.Text = "Số tiếp nhận :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(827, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 14);
            this.label16.TabIndex = 0;
            this.label16.Text = "Trạng thái :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(606, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mã hải quan :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(345, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ngày tờ khai :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(14, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Số tờ khai HQ :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên đơn vị :";
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdSend,
            this.cmdResult,
            this.cmdPrint,
            this.cmdUpdateGuidstring,
            this.cmdFeedback});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave1,
            this.cmdSend1,
            this.cmdFeedback1,
            this.cmdResult1,
            this.cmdPrint1,
            this.cmdUpdateGuidstring1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(669, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // cmdUpdateGuidstring1
            // 
            this.cmdUpdateGuidstring1.Key = "cmdUpdateGuidstring";
            this.cmdUpdateGuidstring1.Name = "cmdUpdateGuidstring1";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu lại";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdResult
            // 
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Text = "Thông báo nộp phí";
            // 
            // cmdUpdateGuidstring
            // 
            this.cmdUpdateGuidstring.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidstring.Image")));
            this.cmdUpdateGuidstring.Key = "cmdUpdateGuidstring";
            this.cmdUpdateGuidstring.Name = "cmdUpdateGuidstring";
            this.cmdUpdateGuidstring.Text = "Cập nhật chuỗi phản hồi";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback.Image")));
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "Lấy phản hồi";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1305, 32);
            // 
            // HP_ToKhaiNopPhiForm
            // 
            this.ClientSize = new System.Drawing.Size(1305, 852);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HP_ToKhaiNopPhiForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tờ khai nộp phí";
            this.Load += new System.EventHandler(this.HP_ToKhaiNopPhiForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPageVDN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListContainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHangContainer)).EndInit();
            this.grbHangContainer.ResumeLayout(false);
            this.grbHangContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHangRoi)).EndInit();
            this.grbHangRoi.ResumeLayout(false);
            this.grbHangRoi.PerformLayout();
            this.uiTabPageContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbContainer)).EndInit();
            this.grbContainer.ResumeLayout(false);
            this.grbContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiep;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayCT;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCT;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayToKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQ;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhuongThucVT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaLoaiHinh;
        private Janus.Windows.EditControls.UIComboBox cbbHinhThucTT;
        private Janus.Windows.EditControls.UIComboBox cbbNhomLoaiHinh;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageVDN;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.GridEX.GridEX dgListContainer;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTrongLuong;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageContainer;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.GridEX.GridEX dgListFile;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox grbContainer;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSeal;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoContainer;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.EditControls.UIButton btnAddExcel;
        private Janus.Windows.EditControls.UIComboBox cbbTinhChatContainer;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiContainer;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.EditControls.UIButton btnAddContainer;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiFile;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenFile;
        private Janus.Windows.EditControls.UIButton btnDeleteContainer;
        private Janus.Windows.EditControls.UIButton btnDeleteFile;
        private Janus.Windows.EditControls.UIButton btnAddFile;
        private Janus.Windows.EditControls.UIButton btnSelect;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidstring;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidstring1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.EditControls.UIGroupBox grbHangContainer;
        private Janus.Windows.EditControls.UIGroupBox grbHangRoi;
        private Janus.Windows.EditControls.UIButton btnViewFile;
        private System.Windows.Forms.Label txtFileSize;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblTotalSize;
        private System.Windows.Forms.Label label30;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHangHoa;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label31;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDDLuuKho;
        private System.Windows.Forms.Label label15;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty cbbDDNP;
        private System.Windows.Forms.Label lblStatus;
        private Janus.Windows.EditControls.UIComboBox cbbDVT;
    }
}
