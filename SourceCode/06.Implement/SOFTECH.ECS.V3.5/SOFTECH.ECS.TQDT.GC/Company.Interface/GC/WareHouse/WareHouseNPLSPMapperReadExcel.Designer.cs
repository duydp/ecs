﻿namespace Company.Interface.GC.WareHouse
{
    partial class WareHouseNPLSPMapperReadExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WareHouseNPLSPMapperReadExcel));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnReadFile = new Janus.Windows.EditControls.UIButton();
            this.linkFileExcel = new System.Windows.Forms.LinkLabel();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnSelectFile = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbSheetName = new Janus.Windows.EditControls.UIComboBox();
            this.txtRowIndex = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTenTiengAnh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTLQD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHangMap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHangMap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDVTMap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSMap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTenHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNguoiBan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMauSac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtKichThuoc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDonHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtMaKho = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTaiKhoan = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(1119, 338);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.btnReadFile);
            this.uiGroupBox2.Controls.Add(this.linkFileExcel);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 289);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1119, 49);
            this.uiGroupBox2.TabIndex = 322;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageIndex = 4;
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(595, 17);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 23);
            this.btnClose.TabIndex = 309;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnReadFile
            // 
            this.btnReadFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReadFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadFile.Image = ((System.Drawing.Image)(resources.GetObject("btnReadFile.Image")));
            this.btnReadFile.ImageIndex = 4;
            this.btnReadFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnReadFile.Location = new System.Drawing.Point(496, 17);
            this.btnReadFile.Name = "btnReadFile";
            this.btnReadFile.Size = new System.Drawing.Size(90, 23);
            this.btnReadFile.TabIndex = 308;
            this.btnReadFile.Text = "Đọc File";
            this.btnReadFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnReadFile.Click += new System.EventHandler(this.btnReadFile_Click);
            // 
            // linkFileExcel
            // 
            this.linkFileExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkFileExcel.AutoSize = true;
            this.linkFileExcel.BackColor = System.Drawing.Color.Transparent;
            this.linkFileExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkFileExcel.Location = new System.Drawing.Point(12, 22);
            this.linkFileExcel.Name = "linkFileExcel";
            this.linkFileExcel.Size = new System.Drawing.Size(74, 13);
            this.linkFileExcel.TabIndex = 79;
            this.linkFileExcel.TabStop = true;
            this.linkFileExcel.Text = "File Excel mẫu";
            this.linkFileExcel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkFileExcel_LinkClicked);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.txtFilePath);
            this.uiGroupBox3.Controls.Add(this.btnSelectFile);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 244);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1119, 45);
            this.uiGroupBox3.TabIndex = 321;
            this.uiGroupBox3.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(11, 18);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(864, 21);
            this.txtFilePath.TabIndex = 0;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFile.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectFile.Image")));
            this.btnSelectFile.ImageIndex = 4;
            this.btnSelectFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSelectFile.Location = new System.Drawing.Point(881, 16);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(90, 23);
            this.btnSelectFile.TabIndex = 310;
            this.btnSelectFile.Text = "Chọn File";
            this.btnSelectFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox1);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1119, 244);
            this.uiGroupBox4.TabIndex = 320;
            this.uiGroupBox4.Text = "Thông số cấu hình";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbSheetName
            // 
            this.cbbSheetName.BackColor = System.Drawing.Color.White;
            this.cbbSheetName.DisplayMember = "Name";
            this.cbbSheetName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSheetName.Location = new System.Drawing.Point(12, 34);
            this.cbbSheetName.Name = "cbbSheetName";
            this.cbbSheetName.Size = new System.Drawing.Size(188, 22);
            this.cbbSheetName.TabIndex = 76;
            this.cbbSheetName.Tag = "";
            this.cbbSheetName.ValueMember = "ID";
            this.cbbSheetName.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtRowIndex
            // 
            this.txtRowIndex.BackColor = System.Drawing.Color.White;
            this.txtRowIndex.DecimalDigits = 0;
            this.txtRowIndex.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRowIndex.Location = new System.Drawing.Point(208, 35);
            this.txtRowIndex.MaxLength = 6;
            this.txtRowIndex.Name = "txtRowIndex";
            this.txtRowIndex.Size = new System.Drawing.Size(90, 21);
            this.txtRowIndex.TabIndex = 64;
            this.txtRowIndex.Text = "2";
            this.txtRowIndex.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.txtRowIndex.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(110, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 13);
            this.label14.TabIndex = 73;
            this.label14.Text = "Tên hàng";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(14, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 71;
            this.label13.Text = "Mã hàng";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(206, 12);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 63;
            this.label10.Text = "Dòng bắt đầu";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(499, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 66;
            this.label9.Text = "Ghi chú";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(411, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 75;
            this.label3.Text = "Tỷ lệ QĐ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 60;
            this.label5.Text = "Tên Sheet";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(209, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 75;
            this.label11.Text = "Mã HS";
            // 
            // txtTenTiengAnh
            // 
            this.txtTenTiengAnh.BackColor = System.Drawing.Color.White;
            this.txtTenTiengAnh.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenTiengAnh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenTiengAnh.Location = new System.Drawing.Point(400, 52);
            this.txtTenTiengAnh.MaxLength = 1;
            this.txtTenTiengAnh.Name = "txtTenTiengAnh";
            this.txtTenTiengAnh.Size = new System.Drawing.Size(90, 21);
            this.txtTenTiengAnh.TabIndex = 68;
            this.txtTenTiengAnh.Text = "E";
            this.txtTenTiengAnh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(305, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 75;
            this.label1.Text = "ĐVT";
            // 
            // txtTLQD
            // 
            this.txtTLQD.BackColor = System.Drawing.Color.White;
            this.txtTLQD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTLQD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTLQD.Location = new System.Drawing.Point(414, 52);
            this.txtTLQD.MaxLength = 1;
            this.txtTLQD.Name = "txtTLQD";
            this.txtTLQD.Size = new System.Drawing.Size(90, 21);
            this.txtTLQD.TabIndex = 68;
            this.txtTLQD.Text = "K";
            this.txtTLQD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.BackColor = System.Drawing.Color.White;
            this.txtGhiChu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(499, 52);
            this.txtGhiChu.MaxLength = 1;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(83, 21);
            this.txtGhiChu.TabIndex = 68;
            this.txtGhiChu.Text = "F";
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHangMap
            // 
            this.txtMaHangMap.BackColor = System.Drawing.Color.White;
            this.txtMaHangMap.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHangMap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangMap.Location = new System.Drawing.Point(17, 52);
            this.txtMaHangMap.MaxLength = 1;
            this.txtMaHangMap.Name = "txtMaHangMap";
            this.txtMaHangMap.Size = new System.Drawing.Size(90, 21);
            this.txtMaHangMap.TabIndex = 68;
            this.txtMaHangMap.Text = "H";
            this.txtMaHangMap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenHangMap
            // 
            this.txtTenHangMap.BackColor = System.Drawing.Color.White;
            this.txtTenHangMap.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangMap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangMap.Location = new System.Drawing.Point(113, 52);
            this.txtTenHangMap.MaxLength = 1;
            this.txtTenHangMap.Name = "txtTenHangMap";
            this.txtTenHangMap.Size = new System.Drawing.Size(90, 21);
            this.txtTenHangMap.TabIndex = 68;
            this.txtTenHangMap.Text = "G";
            this.txtTenHangMap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDVTMap
            // 
            this.txtDVTMap.BackColor = System.Drawing.Color.White;
            this.txtDVTMap.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTMap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTMap.Location = new System.Drawing.Point(308, 52);
            this.txtDVTMap.MaxLength = 1;
            this.txtDVTMap.Name = "txtDVTMap";
            this.txtDVTMap.Size = new System.Drawing.Size(90, 21);
            this.txtDVTMap.TabIndex = 68;
            this.txtDVTMap.Text = "J";
            this.txtDVTMap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHSMap
            // 
            this.txtMaHSMap.BackColor = System.Drawing.Color.White;
            this.txtMaHSMap.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHSMap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSMap.Location = new System.Drawing.Point(212, 52);
            this.txtMaHSMap.MaxLength = 1;
            this.txtMaHSMap.Name = "txtMaHSMap";
            this.txtMaHSMap.Size = new System.Drawing.Size(90, 21);
            this.txtMaHSMap.TabIndex = 68;
            this.txtMaHSMap.Text = "I";
            this.txtMaHSMap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.txtRowIndex);
            this.uiGroupBox5.Controls.Add(this.label5);
            this.uiGroupBox5.Controls.Add(this.cbbSheetName);
            this.uiGroupBox5.Controls.Add(this.label10);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1113, 65);
            this.uiGroupBox5.TabIndex = 323;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtTaiKhoan);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.txtMaKho);
            this.uiGroupBox1.Controls.Add(this.txtMaHangHoa);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.txtDonHang);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.txtKichThuoc);
            this.uiGroupBox1.Controls.Add(this.txtTenHangHoa);
            this.uiGroupBox1.Controls.Add(this.txtDVT);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Controls.Add(this.label16);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtMauSac);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.txtNguoiBan);
            this.uiGroupBox1.Controls.Add(this.txtTenTiengAnh);
            this.uiGroupBox1.Controls.Add(this.txtMaHS);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 82);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(589, 159);
            this.uiGroupBox1.TabIndex = 325;
            this.uiGroupBox1.Text = "Thông tin hàng hóa kho Kế toán";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 66;
            this.label6.Text = "Mã hàng hóa";
            // 
            // txtMaHangHoa
            // 
            this.txtMaHangHoa.BackColor = System.Drawing.Color.White;
            this.txtMaHangHoa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangHoa.Location = new System.Drawing.Point(12, 52);
            this.txtMaHangHoa.MaxLength = 1;
            this.txtMaHangHoa.Name = "txtMaHangHoa";
            this.txtMaHangHoa.Size = new System.Drawing.Size(90, 21);
            this.txtMaHangHoa.TabIndex = 68;
            this.txtMaHangHoa.Text = "A";
            this.txtMaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(107, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 66;
            this.label7.Text = "Tên hàng hóa";
            // 
            // txtTenHangHoa
            // 
            this.txtTenHangHoa.BackColor = System.Drawing.Color.White;
            this.txtTenHangHoa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangHoa.Location = new System.Drawing.Point(107, 52);
            this.txtTenHangHoa.MaxLength = 1;
            this.txtTenHangHoa.Name = "txtTenHangHoa";
            this.txtTenHangHoa.Size = new System.Drawing.Size(90, 21);
            this.txtTenHangHoa.TabIndex = 68;
            this.txtTenHangHoa.Text = "B";
            this.txtTenHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDVT
            // 
            this.txtDVT.BackColor = System.Drawing.Color.White;
            this.txtDVT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVT.Location = new System.Drawing.Point(304, 52);
            this.txtDVT.MaxLength = 1;
            this.txtDVT.Name = "txtDVT";
            this.txtDVT.Size = new System.Drawing.Size(90, 21);
            this.txtDVT.TabIndex = 68;
            this.txtDVT.Text = "D";
            this.txtDVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(205, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 69;
            this.label12.Text = "Mã HS";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(400, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 75;
            this.label4.Text = "Tên tiếng anh";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(304, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 63;
            this.label8.Text = "ĐVT";
            // 
            // txtMaHS
            // 
            this.txtMaHS.BackColor = System.Drawing.Color.White;
            this.txtMaHS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHS.Location = new System.Drawing.Point(205, 52);
            this.txtMaHS.MaxLength = 1;
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.Size = new System.Drawing.Size(90, 21);
            this.txtMaHS.TabIndex = 68;
            this.txtMaHS.Text = "C";
            this.txtMaHS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.txtMaHSMap);
            this.uiGroupBox6.Controls.Add(this.txtDVTMap);
            this.uiGroupBox6.Controls.Add(this.label14);
            this.uiGroupBox6.Controls.Add(this.txtTenHangMap);
            this.uiGroupBox6.Controls.Add(this.label13);
            this.uiGroupBox6.Controls.Add(this.txtMaHangMap);
            this.uiGroupBox6.Controls.Add(this.label3);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.label11);
            this.uiGroupBox6.Controls.Add(this.txtTLQD);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox6.Location = new System.Drawing.Point(592, 82);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(524, 159);
            this.uiGroupBox6.TabIndex = 326;
            this.uiGroupBox6.Text = "Thông tin hàng hóa VNACCS";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtNguoiBan
            // 
            this.txtNguoiBan.BackColor = System.Drawing.Color.White;
            this.txtNguoiBan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNguoiBan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiBan.Location = new System.Drawing.Point(205, 111);
            this.txtNguoiBan.MaxLength = 1;
            this.txtNguoiBan.Name = "txtNguoiBan";
            this.txtNguoiBan.Size = new System.Drawing.Size(90, 21);
            this.txtNguoiBan.TabIndex = 68;
            this.txtNguoiBan.Text = "N";
            this.txtNguoiBan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMauSac
            // 
            this.txtMauSac.BackColor = System.Drawing.Color.White;
            this.txtMauSac.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMauSac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSac.Location = new System.Drawing.Point(400, 111);
            this.txtMauSac.MaxLength = 1;
            this.txtMauSac.Name = "txtMauSac";
            this.txtMauSac.Size = new System.Drawing.Size(90, 21);
            this.txtMauSac.TabIndex = 68;
            this.txtMauSac.Text = "P";
            this.txtMauSac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(304, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 63;
            this.label2.Text = "Kích thước";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(400, 89);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 13);
            this.label15.TabIndex = 75;
            this.label15.Text = "Màu sắc";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(205, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 69;
            this.label16.Text = "Người bán";
            // 
            // txtKichThuoc
            // 
            this.txtKichThuoc.BackColor = System.Drawing.Color.White;
            this.txtKichThuoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtKichThuoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKichThuoc.Location = new System.Drawing.Point(304, 111);
            this.txtKichThuoc.MaxLength = 1;
            this.txtKichThuoc.Name = "txtKichThuoc";
            this.txtKichThuoc.Size = new System.Drawing.Size(90, 21);
            this.txtKichThuoc.TabIndex = 68;
            this.txtKichThuoc.Text = "O";
            this.txtKichThuoc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDonHang
            // 
            this.txtDonHang.BackColor = System.Drawing.Color.White;
            this.txtDonHang.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonHang.Location = new System.Drawing.Point(107, 111);
            this.txtDonHang.MaxLength = 1;
            this.txtDonHang.Name = "txtDonHang";
            this.txtDonHang.Size = new System.Drawing.Size(90, 21);
            this.txtDonHang.TabIndex = 68;
            this.txtDonHang.Text = "M";
            this.txtDonHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(107, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 13);
            this.label18.TabIndex = 66;
            this.label18.Text = "Đơn hàng";
            // 
            // txtMaKho
            // 
            this.txtMaKho.BackColor = System.Drawing.Color.White;
            this.txtMaKho.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaKho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaKho.Location = new System.Drawing.Point(12, 111);
            this.txtMaKho.MaxLength = 1;
            this.txtMaKho.Name = "txtMaKho";
            this.txtMaKho.Size = new System.Drawing.Size(90, 21);
            this.txtMaKho.TabIndex = 68;
            this.txtMaKho.Text = "L";
            this.txtMaKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(12, 89);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 66;
            this.label19.Text = "Mã kho";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(500, 89);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 13);
            this.label17.TabIndex = 66;
            this.label17.Text = "Tài khoản";
            // 
            // txtTaiKhoan
            // 
            this.txtTaiKhoan.BackColor = System.Drawing.Color.White;
            this.txtTaiKhoan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaiKhoan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaiKhoan.Location = new System.Drawing.Point(500, 111);
            this.txtTaiKhoan.MaxLength = 1;
            this.txtTaiKhoan.Name = "txtTaiKhoan";
            this.txtTaiKhoan.Size = new System.Drawing.Size(83, 21);
            this.txtTaiKhoan.TabIndex = 68;
            this.txtTaiKhoan.Text = "Q";
            this.txtTaiKhoan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // WareHouseNPLSPMapperReadExcel
            // 
            this.ClientSize = new System.Drawing.Size(1119, 338);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "WareHouseNPLSPMapperReadExcel";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đọc hàng hóa từ File Excel";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnReadFile;
        private System.Windows.Forms.LinkLabel linkFileExcel;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.EditControls.UIButton btnSelectFile;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIComboBox cbbSheetName;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRowIndex;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenTiengAnh;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTLQD;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangMap;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangMap;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTMap;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSMap;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangHoa;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangHoa;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVT;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHS;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaKho;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtKichThuoc;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMauSac;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiBan;
        private Janus.Windows.GridEX.EditControls.EditBox txtTaiKhoan;
        private System.Windows.Forms.Label label17;
    }
}
