using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class GiayPhep
    {
        #region Private members.

        protected long _ID;
        protected string _SoGiayPhep = string.Empty;
        protected DateTime _NgayGiayPhep = new DateTime(1900, 1, 1);
        protected DateTime _NgayHetHan = new DateTime(1900, 1, 1);
        protected string _NguoiCap = string.Empty;
        protected string _NoiCap = string.Empty;
        protected string _MaDonViDuocCap = string.Empty;
        protected string _TenDonViDuocCap = string.Empty;
        protected string _MaCoQuanCap = string.Empty;
        protected string _TenQuanCap = string.Empty;
        protected string _ThongTinKhac = string.Empty;
        protected string _MaDoanhNghiep = string.Empty;
        protected long _TKMD_ID;
        protected string _GuidStr = string.Empty;
        protected int _LoaiKB;
        protected long _SoTiepNhan;
        protected DateTime _NgayTiepNhan = new DateTime(1900, 1, 1);
        protected int _TrangThai;
        protected int _NamTiepNhan;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public string SoGiayPhep
        {
            set { this._SoGiayPhep = value; }
            get { return this._SoGiayPhep; }
        }

        public DateTime NgayGiayPhep
        {
            set { this._NgayGiayPhep = value; }
            get { return this._NgayGiayPhep; }
        }

        public DateTime NgayHetHan
        {
            set { this._NgayHetHan = value; }
            get { return this._NgayHetHan; }
        }

        public string NguoiCap
        {
            set { this._NguoiCap = value; }
            get { return this._NguoiCap; }
        }

        public string NoiCap
        {
            set { this._NoiCap = value; }
            get { return this._NoiCap; }
        }

        public string MaDonViDuocCap
        {
            set { this._MaDonViDuocCap = value; }
            get { return this._MaDonViDuocCap; }
        }

        public string TenDonViDuocCap
        {
            set { this._TenDonViDuocCap = value; }
            get { return this._TenDonViDuocCap; }
        }

        public string MaCoQuanCap
        {
            set { this._MaCoQuanCap = value; }
            get { return this._MaCoQuanCap; }
        }

        public string TenQuanCap
        {
            set { this._TenQuanCap = value; }
            get { return this._TenQuanCap; }
        }

        public string ThongTinKhac
        {
            set { this._ThongTinKhac = value; }
            get { return this._ThongTinKhac; }
        }

        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }

        public long TKMD_ID
        {
            set { this._TKMD_ID = value; }
            get { return this._TKMD_ID; }
        }

        public string GuidStr
        {
            set { this._GuidStr = value; }
            get { return this._GuidStr; }
        }

        public int LoaiKB
        {
            set { this._LoaiKB = value; }
            get { return this._LoaiKB; }
        }

        public long SoTiepNhan
        {
            set { this._SoTiepNhan = value; }
            get { return this._SoTiepNhan; }
        }

        public DateTime NgayTiepNhan
        {
            set { this._NgayTiepNhan = value; }
            get { return this._NgayTiepNhan; }
        }

        public int TrangThai
        {
            set { this._TrangThai = value; }
            get { return this._TrangThai; }
        }

        public int NamTiepNhan
        {
            set { this._NamTiepNhan = value; }
            get { return this._NamTiepNhan; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static GiayPhep Load(long iD)
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            GiayPhep entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new GiayPhep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiCap"))) entity.NguoiCap = reader.GetString(reader.GetOrdinal("NguoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiCap"))) entity.NoiCap = reader.GetString(reader.GetOrdinal("NoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViDuocCap"))) entity.MaDonViDuocCap = reader.GetString(reader.GetOrdinal("MaDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDuocCap"))) entity.TenDonViDuocCap = reader.GetString(reader.GetOrdinal("TenDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCoQuanCap"))) entity.MaCoQuanCap = reader.GetString(reader.GetOrdinal("MaCoQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenQuanCap"))) entity.TenQuanCap = reader.GetString(reader.GetOrdinal("TenQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------
        public static List<GiayPhep> SelectCollectionAll()
        {
            List<GiayPhep> collection = new List<GiayPhep>();
            SqlDataReader reader = SelectReaderAll();
            while (reader.Read())
            {
                GiayPhep entity = new GiayPhep();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiCap"))) entity.NguoiCap = reader.GetString(reader.GetOrdinal("NguoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiCap"))) entity.NoiCap = reader.GetString(reader.GetOrdinal("NoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViDuocCap"))) entity.MaDonViDuocCap = reader.GetString(reader.GetOrdinal("MaDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDuocCap"))) entity.TenDonViDuocCap = reader.GetString(reader.GetOrdinal("TenDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCoQuanCap"))) entity.MaCoQuanCap = reader.GetString(reader.GetOrdinal("MaCoQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenQuanCap"))) entity.TenQuanCap = reader.GetString(reader.GetOrdinal("TenQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public static List<GiayPhep> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            List<GiayPhep> collection = new List<GiayPhep>();

            SqlDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                GiayPhep entity = new GiayPhep();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiCap"))) entity.NguoiCap = reader.GetString(reader.GetOrdinal("NguoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiCap"))) entity.NoiCap = reader.GetString(reader.GetOrdinal("NoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViDuocCap"))) entity.MaDonViDuocCap = reader.GetString(reader.GetOrdinal("MaDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDuocCap"))) entity.TenDonViDuocCap = reader.GetString(reader.GetOrdinal("TenDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCoQuanCap"))) entity.MaCoQuanCap = reader.GetString(reader.GetOrdinal("MaCoQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenQuanCap"))) entity.TenQuanCap = reader.GetString(reader.GetOrdinal("TenQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static List<GiayPhep> SelectCollectionBy_TKMD_ID(long tKMD_ID)
        {
            List<GiayPhep> collection = new List<GiayPhep>();
            SqlDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
            while (reader.Read())
            {
                GiayPhep entity = new GiayPhep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiCap"))) entity.NguoiCap = reader.GetString(reader.GetOrdinal("NguoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiCap"))) entity.NoiCap = reader.GetString(reader.GetOrdinal("NoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViDuocCap"))) entity.MaDonViDuocCap = reader.GetString(reader.GetOrdinal("MaDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDuocCap"))) entity.TenDonViDuocCap = reader.GetString(reader.GetOrdinal("TenDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCoQuanCap"))) entity.MaCoQuanCap = reader.GetString(reader.GetOrdinal("MaCoQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenQuanCap"))) entity.TenQuanCap = reader.GetString(reader.GetOrdinal("TenQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static SqlDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static SqlDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static SqlDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "p_KDT_GiayPhep_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertGiayPhep(string soGiayPhep, DateTime ngayGiayPhep, DateTime ngayHetHan, string nguoiCap, string noiCap, string maDonViDuocCap, string tenDonViDuocCap, string maCoQuanCap, string tenQuanCap, string thongTinKhac, string maDoanhNghiep, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan)
        {
            GiayPhep entity = new GiayPhep();
            entity.SoGiayPhep = soGiayPhep;
            entity.NgayGiayPhep = ngayGiayPhep;
            entity.NgayHetHan = ngayHetHan;
            entity.NguoiCap = nguoiCap;
            entity.NoiCap = noiCap;
            entity.MaDonViDuocCap = maDonViDuocCap;
            entity.TenDonViDuocCap = tenDonViDuocCap;
            entity.MaCoQuanCap = maCoQuanCap;
            entity.TenQuanCap = tenQuanCap;
            entity.ThongTinKhac = thongTinKhac;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.VarChar, SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year <= 1900 ? DBNull.Value : (object)NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1900 ? DBNull.Value : (object)NgayHetHan);
            db.AddInParameter(dbCommand, "@NguoiCap", SqlDbType.NVarChar, NguoiCap);
            db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
            db.AddInParameter(dbCommand, "@MaDonViDuocCap", SqlDbType.VarChar, MaDonViDuocCap);
            db.AddInParameter(dbCommand, "@TenDonViDuocCap", SqlDbType.NVarChar, TenDonViDuocCap);
            db.AddInParameter(dbCommand, "@MaCoQuanCap", SqlDbType.VarChar, MaCoQuanCap);
            db.AddInParameter(dbCommand, "@TenQuanCap", SqlDbType.NVarChar, TenQuanCap);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1900 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<GiayPhep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (GiayPhep item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateGiayPhep(long iD, string soGiayPhep, DateTime ngayGiayPhep, DateTime ngayHetHan, string nguoiCap, string noiCap, string maDonViDuocCap, string tenDonViDuocCap, string maCoQuanCap, string tenQuanCap, string thongTinKhac, string maDoanhNghiep, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan)
        {
            GiayPhep entity = new GiayPhep();
            entity.SoGiayPhep = soGiayPhep;
            entity.NgayGiayPhep = ngayGiayPhep;
            entity.NgayHetHan = ngayHetHan;
            entity.NguoiCap = nguoiCap;
            entity.NoiCap = noiCap;
            entity.MaDonViDuocCap = maDonViDuocCap;
            entity.TenDonViDuocCap = tenDonViDuocCap;
            entity.MaCoQuanCap = maCoQuanCap;
            entity.TenQuanCap = tenQuanCap;
            entity.ThongTinKhac = thongTinKhac;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_GiayPhep_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.VarChar, SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year == 1900 ? DBNull.Value : (object)NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1900 ? DBNull.Value : (object)NgayHetHan);
            db.AddInParameter(dbCommand, "@NguoiCap", SqlDbType.NVarChar, NguoiCap);
            db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
            db.AddInParameter(dbCommand, "@MaDonViDuocCap", SqlDbType.VarChar, MaDonViDuocCap);
            db.AddInParameter(dbCommand, "@TenDonViDuocCap", SqlDbType.NVarChar, TenDonViDuocCap);
            db.AddInParameter(dbCommand, "@MaCoQuanCap", SqlDbType.VarChar, MaCoQuanCap);
            db.AddInParameter(dbCommand, "@TenQuanCap", SqlDbType.NVarChar, TenQuanCap);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1900 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<GiayPhep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (GiayPhep item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateGiayPhep(long iD, string soGiayPhep, DateTime ngayGiayPhep, DateTime ngayHetHan, string nguoiCap, string noiCap, string maDonViDuocCap, string tenDonViDuocCap, string maCoQuanCap, string tenQuanCap, string thongTinKhac, string maDoanhNghiep, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan)
        {
            GiayPhep entity = new GiayPhep();
            entity.ID = iD;
            entity.SoGiayPhep = soGiayPhep;
            entity.NgayGiayPhep = ngayGiayPhep;
            entity.NgayHetHan = ngayHetHan;
            entity.NguoiCap = nguoiCap;
            entity.NoiCap = noiCap;
            entity.MaDonViDuocCap = maDonViDuocCap;
            entity.TenDonViDuocCap = tenDonViDuocCap;
            entity.MaCoQuanCap = maCoQuanCap;
            entity.TenQuanCap = tenQuanCap;
            entity.ThongTinKhac = thongTinKhac;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.VarChar, SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year == 1900 ? DBNull.Value : (object)NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1900 ? DBNull.Value : (object)NgayHetHan);
            db.AddInParameter(dbCommand, "@NguoiCap", SqlDbType.NVarChar, NguoiCap);
            db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
            db.AddInParameter(dbCommand, "@MaDonViDuocCap", SqlDbType.VarChar, MaDonViDuocCap);
            db.AddInParameter(dbCommand, "@TenDonViDuocCap", SqlDbType.NVarChar, TenDonViDuocCap);
            db.AddInParameter(dbCommand, "@MaCoQuanCap", SqlDbType.VarChar, MaCoQuanCap);
            db.AddInParameter(dbCommand, "@TenQuanCap", SqlDbType.NVarChar, TenQuanCap);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1900 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<GiayPhep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (GiayPhep item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteGiayPhep(long iD)
        {
            GiayPhep entity = new GiayPhep();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMD_ID(long tKMD_ID)
        {
            string spName = "[dbo].[p_KDT_GiayPhep_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<GiayPhep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (GiayPhep item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}