using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_TEA : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public decimal SoDanhMucMienThue { set; get; }
		public string PhanLoaiXuatNhapKhau { set; get; }
		public string CoQuanHaiQuan { set; get; }
		public string DiaChiCuaNguoiKhai { set; get; }
		public string SDTCuaNguoiKhai { set; get; }
		public DateTime ThoiHanMienThue { set; get; }
		public string TenDuAnDauTu { set; get; }
		public string DiaDiemXayDungDuAn { set; get; }
		public string MucTieuDuAn { set; get; }
		public string MaMienGiam { set; get; }
		public string PhamViDangKyDMMT { set; get; }
		public DateTime NgayDuKienXNK { set; get; }
		public string GP_GCNDauTuSo { set; get; }
		public DateTime NgayChungNhan { set; get; }
		public string CapBoi { set; get; }
		public string GhiChu { set; get; }
		public string CamKetSuDung { set; get; }
		public string MaNguoiKhai { set; get; }
		public DateTime KhaiBaoTuNgay { set; get; }
		public DateTime KhaiBaoDenNgay { set; get; }
		public string MaSoQuanLyDSMT { set; get; }
		public string PhanLoaiCapPhep { set; get; }
		public string TrangThaiXuLy { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_TEA> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_TEA> collection = new List<KDT_VNACCS_TEA>();
			while (reader.Read())
			{
				KDT_VNACCS_TEA entity = new KDT_VNACCS_TEA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDanhMucMienThue"))) entity.SoDanhMucMienThue = reader.GetDecimal(reader.GetOrdinal("SoDanhMucMienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiXuatNhapKhau"))) entity.PhanLoaiXuatNhapKhau = reader.GetString(reader.GetOrdinal("PhanLoaiXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHaiQuan"))) entity.CoQuanHaiQuan = reader.GetString(reader.GetOrdinal("CoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiCuaNguoiKhai"))) entity.DiaChiCuaNguoiKhai = reader.GetString(reader.GetOrdinal("DiaChiCuaNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SDTCuaNguoiKhai"))) entity.SDTCuaNguoiKhai = reader.GetString(reader.GetOrdinal("SDTCuaNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanMienThue"))) entity.ThoiHanMienThue = reader.GetDateTime(reader.GetOrdinal("ThoiHanMienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDuAnDauTu"))) entity.TenDuAnDauTu = reader.GetString(reader.GetOrdinal("TenDuAnDauTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXayDungDuAn"))) entity.DiaDiemXayDungDuAn = reader.GetString(reader.GetOrdinal("DiaDiemXayDungDuAn"));
				if (!reader.IsDBNull(reader.GetOrdinal("MucTieuDuAn"))) entity.MucTieuDuAn = reader.GetString(reader.GetOrdinal("MucTieuDuAn"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaMienGiam"))) entity.MaMienGiam = reader.GetString(reader.GetOrdinal("MaMienGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhamViDangKyDMMT"))) entity.PhamViDangKyDMMT = reader.GetString(reader.GetOrdinal("PhamViDangKyDMMT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDuKienXNK"))) entity.NgayDuKienXNK = reader.GetDateTime(reader.GetOrdinal("NgayDuKienXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("GP_GCNDauTuSo"))) entity.GP_GCNDauTuSo = reader.GetString(reader.GetOrdinal("GP_GCNDauTuSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungNhan"))) entity.NgayChungNhan = reader.GetDateTime(reader.GetOrdinal("NgayChungNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("CapBoi"))) entity.CapBoi = reader.GetString(reader.GetOrdinal("CapBoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("CamKetSuDung"))) entity.CamKetSuDung = reader.GetString(reader.GetOrdinal("CamKetSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhai"))) entity.MaNguoiKhai = reader.GetString(reader.GetOrdinal("MaNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhaiBaoTuNgay"))) entity.KhaiBaoTuNgay = reader.GetDateTime(reader.GetOrdinal("KhaiBaoTuNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhaiBaoDenNgay"))) entity.KhaiBaoDenNgay = reader.GetDateTime(reader.GetOrdinal("KhaiBaoDenNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoQuanLyDSMT"))) entity.MaSoQuanLyDSMT = reader.GetString(reader.GetOrdinal("MaSoQuanLyDSMT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiCapPhep"))) entity.PhanLoaiCapPhep = reader.GetString(reader.GetOrdinal("PhanLoaiCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetString(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_TEA> collection, long id)
        {
            foreach (KDT_VNACCS_TEA item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TEA VALUES(@SoDanhMucMienThue, @PhanLoaiXuatNhapKhau, @CoQuanHaiQuan, @DiaChiCuaNguoiKhai, @SDTCuaNguoiKhai, @ThoiHanMienThue, @TenDuAnDauTu, @DiaDiemXayDungDuAn, @MucTieuDuAn, @MaMienGiam, @PhamViDangKyDMMT, @NgayDuKienXNK, @GP_GCNDauTuSo, @NgayChungNhan, @CapBoi, @GhiChu, @CamKetSuDung, @MaNguoiKhai, @KhaiBaoTuNgay, @KhaiBaoDenNgay, @MaSoQuanLyDSMT, @PhanLoaiCapPhep, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_TEA SET SoDanhMucMienThue = @SoDanhMucMienThue, PhanLoaiXuatNhapKhau = @PhanLoaiXuatNhapKhau, CoQuanHaiQuan = @CoQuanHaiQuan, DiaChiCuaNguoiKhai = @DiaChiCuaNguoiKhai, SDTCuaNguoiKhai = @SDTCuaNguoiKhai, ThoiHanMienThue = @ThoiHanMienThue, TenDuAnDauTu = @TenDuAnDauTu, DiaDiemXayDungDuAn = @DiaDiemXayDungDuAn, MucTieuDuAn = @MucTieuDuAn, MaMienGiam = @MaMienGiam, PhamViDangKyDMMT = @PhamViDangKyDMMT, NgayDuKienXNK = @NgayDuKienXNK, GP_GCNDauTuSo = @GP_GCNDauTuSo, NgayChungNhan = @NgayChungNhan, CapBoi = @CapBoi, GhiChu = @GhiChu, CamKetSuDung = @CamKetSuDung, MaNguoiKhai = @MaNguoiKhai, KhaiBaoTuNgay = @KhaiBaoTuNgay, KhaiBaoDenNgay = @KhaiBaoDenNgay, MaSoQuanLyDSMT = @MaSoQuanLyDSMT, PhanLoaiCapPhep = @PhanLoaiCapPhep, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TEA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDanhMucMienThue", SqlDbType.Decimal, "SoDanhMucMienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, "PhanLoaiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiCuaNguoiKhai", SqlDbType.NVarChar, "DiaChiCuaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SDTCuaNguoiKhai", SqlDbType.VarChar, "SDTCuaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanMienThue", SqlDbType.DateTime, "ThoiHanMienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDuAnDauTu", SqlDbType.NVarChar, "TenDuAnDauTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemXayDungDuAn", SqlDbType.NVarChar, "DiaDiemXayDungDuAn", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MucTieuDuAn", SqlDbType.NVarChar, "MucTieuDuAn", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMienGiam", SqlDbType.VarChar, "MaMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhamViDangKyDMMT", SqlDbType.NVarChar, "PhamViDangKyDMMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDuKienXNK", SqlDbType.DateTime, "NgayDuKienXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GP_GCNDauTuSo", SqlDbType.VarChar, "GP_GCNDauTuSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungNhan", SqlDbType.DateTime, "NgayChungNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CapBoi", SqlDbType.NVarChar, "CapBoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CamKetSuDung", SqlDbType.NVarChar, "CamKetSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhaiBaoTuNgay", SqlDbType.DateTime, "KhaiBaoTuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhaiBaoDenNgay", SqlDbType.DateTime, "KhaiBaoDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoQuanLyDSMT", SqlDbType.VarChar, "MaSoQuanLyDSMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiCapPhep", SqlDbType.VarChar, "PhanLoaiCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDanhMucMienThue", SqlDbType.Decimal, "SoDanhMucMienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, "PhanLoaiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiCuaNguoiKhai", SqlDbType.NVarChar, "DiaChiCuaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SDTCuaNguoiKhai", SqlDbType.VarChar, "SDTCuaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanMienThue", SqlDbType.DateTime, "ThoiHanMienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDuAnDauTu", SqlDbType.NVarChar, "TenDuAnDauTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemXayDungDuAn", SqlDbType.NVarChar, "DiaDiemXayDungDuAn", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MucTieuDuAn", SqlDbType.NVarChar, "MucTieuDuAn", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMienGiam", SqlDbType.VarChar, "MaMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhamViDangKyDMMT", SqlDbType.NVarChar, "PhamViDangKyDMMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDuKienXNK", SqlDbType.DateTime, "NgayDuKienXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GP_GCNDauTuSo", SqlDbType.VarChar, "GP_GCNDauTuSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungNhan", SqlDbType.DateTime, "NgayChungNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CapBoi", SqlDbType.NVarChar, "CapBoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CamKetSuDung", SqlDbType.NVarChar, "CamKetSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhaiBaoTuNgay", SqlDbType.DateTime, "KhaiBaoTuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhaiBaoDenNgay", SqlDbType.DateTime, "KhaiBaoDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoQuanLyDSMT", SqlDbType.VarChar, "MaSoQuanLyDSMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiCapPhep", SqlDbType.VarChar, "PhanLoaiCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TEA VALUES(@SoDanhMucMienThue, @PhanLoaiXuatNhapKhau, @CoQuanHaiQuan, @DiaChiCuaNguoiKhai, @SDTCuaNguoiKhai, @ThoiHanMienThue, @TenDuAnDauTu, @DiaDiemXayDungDuAn, @MucTieuDuAn, @MaMienGiam, @PhamViDangKyDMMT, @NgayDuKienXNK, @GP_GCNDauTuSo, @NgayChungNhan, @CapBoi, @GhiChu, @CamKetSuDung, @MaNguoiKhai, @KhaiBaoTuNgay, @KhaiBaoDenNgay, @MaSoQuanLyDSMT, @PhanLoaiCapPhep, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_TEA SET SoDanhMucMienThue = @SoDanhMucMienThue, PhanLoaiXuatNhapKhau = @PhanLoaiXuatNhapKhau, CoQuanHaiQuan = @CoQuanHaiQuan, DiaChiCuaNguoiKhai = @DiaChiCuaNguoiKhai, SDTCuaNguoiKhai = @SDTCuaNguoiKhai, ThoiHanMienThue = @ThoiHanMienThue, TenDuAnDauTu = @TenDuAnDauTu, DiaDiemXayDungDuAn = @DiaDiemXayDungDuAn, MucTieuDuAn = @MucTieuDuAn, MaMienGiam = @MaMienGiam, PhamViDangKyDMMT = @PhamViDangKyDMMT, NgayDuKienXNK = @NgayDuKienXNK, GP_GCNDauTuSo = @GP_GCNDauTuSo, NgayChungNhan = @NgayChungNhan, CapBoi = @CapBoi, GhiChu = @GhiChu, CamKetSuDung = @CamKetSuDung, MaNguoiKhai = @MaNguoiKhai, KhaiBaoTuNgay = @KhaiBaoTuNgay, KhaiBaoDenNgay = @KhaiBaoDenNgay, MaSoQuanLyDSMT = @MaSoQuanLyDSMT, PhanLoaiCapPhep = @PhanLoaiCapPhep, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TEA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDanhMucMienThue", SqlDbType.Decimal, "SoDanhMucMienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, "PhanLoaiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiCuaNguoiKhai", SqlDbType.NVarChar, "DiaChiCuaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SDTCuaNguoiKhai", SqlDbType.VarChar, "SDTCuaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanMienThue", SqlDbType.DateTime, "ThoiHanMienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDuAnDauTu", SqlDbType.NVarChar, "TenDuAnDauTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemXayDungDuAn", SqlDbType.NVarChar, "DiaDiemXayDungDuAn", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MucTieuDuAn", SqlDbType.NVarChar, "MucTieuDuAn", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMienGiam", SqlDbType.VarChar, "MaMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhamViDangKyDMMT", SqlDbType.NVarChar, "PhamViDangKyDMMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDuKienXNK", SqlDbType.DateTime, "NgayDuKienXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GP_GCNDauTuSo", SqlDbType.VarChar, "GP_GCNDauTuSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungNhan", SqlDbType.DateTime, "NgayChungNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CapBoi", SqlDbType.NVarChar, "CapBoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CamKetSuDung", SqlDbType.NVarChar, "CamKetSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhaiBaoTuNgay", SqlDbType.DateTime, "KhaiBaoTuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhaiBaoDenNgay", SqlDbType.DateTime, "KhaiBaoDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoQuanLyDSMT", SqlDbType.VarChar, "MaSoQuanLyDSMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiCapPhep", SqlDbType.VarChar, "PhanLoaiCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDanhMucMienThue", SqlDbType.Decimal, "SoDanhMucMienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, "PhanLoaiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiCuaNguoiKhai", SqlDbType.NVarChar, "DiaChiCuaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SDTCuaNguoiKhai", SqlDbType.VarChar, "SDTCuaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanMienThue", SqlDbType.DateTime, "ThoiHanMienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDuAnDauTu", SqlDbType.NVarChar, "TenDuAnDauTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemXayDungDuAn", SqlDbType.NVarChar, "DiaDiemXayDungDuAn", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MucTieuDuAn", SqlDbType.NVarChar, "MucTieuDuAn", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMienGiam", SqlDbType.VarChar, "MaMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhamViDangKyDMMT", SqlDbType.NVarChar, "PhamViDangKyDMMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDuKienXNK", SqlDbType.DateTime, "NgayDuKienXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GP_GCNDauTuSo", SqlDbType.VarChar, "GP_GCNDauTuSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungNhan", SqlDbType.DateTime, "NgayChungNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CapBoi", SqlDbType.NVarChar, "CapBoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CamKetSuDung", SqlDbType.NVarChar, "CamKetSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhaiBaoTuNgay", SqlDbType.DateTime, "KhaiBaoTuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhaiBaoDenNgay", SqlDbType.DateTime, "KhaiBaoDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoQuanLyDSMT", SqlDbType.VarChar, "MaSoQuanLyDSMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiCapPhep", SqlDbType.VarChar, "PhanLoaiCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_TEA Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TEA_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_TEA> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_TEA> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_TEA> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TEA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TEA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
        public static DataSet SelectDynamicFull(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TEA_SelectDynamicFull]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TEA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TEA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_TEA(decimal soDanhMucMienThue, string phanLoaiXuatNhapKhau, string coQuanHaiQuan, string diaChiCuaNguoiKhai, string sDTCuaNguoiKhai, DateTime thoiHanMienThue, string tenDuAnDauTu, string diaDiemXayDungDuAn, string mucTieuDuAn, string maMienGiam, string phamViDangKyDMMT, DateTime ngayDuKienXNK, string gP_GCNDauTuSo, DateTime ngayChungNhan, string capBoi, string ghiChu, string camKetSuDung, string maNguoiKhai, DateTime khaiBaoTuNgay, DateTime khaiBaoDenNgay, string maSoQuanLyDSMT, string phanLoaiCapPhep, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TEA entity = new KDT_VNACCS_TEA();	
			entity.SoDanhMucMienThue = soDanhMucMienThue;
			entity.PhanLoaiXuatNhapKhau = phanLoaiXuatNhapKhau;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.DiaChiCuaNguoiKhai = diaChiCuaNguoiKhai;
			entity.SDTCuaNguoiKhai = sDTCuaNguoiKhai;
			entity.ThoiHanMienThue = thoiHanMienThue;
			entity.TenDuAnDauTu = tenDuAnDauTu;
			entity.DiaDiemXayDungDuAn = diaDiemXayDungDuAn;
			entity.MucTieuDuAn = mucTieuDuAn;
			entity.MaMienGiam = maMienGiam;
			entity.PhamViDangKyDMMT = phamViDangKyDMMT;
			entity.NgayDuKienXNK = ngayDuKienXNK;
			entity.GP_GCNDauTuSo = gP_GCNDauTuSo;
			entity.NgayChungNhan = ngayChungNhan;
			entity.CapBoi = capBoi;
			entity.GhiChu = ghiChu;
			entity.CamKetSuDung = camKetSuDung;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.KhaiBaoTuNgay = khaiBaoTuNgay;
			entity.KhaiBaoDenNgay = khaiBaoDenNgay;
			entity.MaSoQuanLyDSMT = maSoQuanLyDSMT;
			entity.PhanLoaiCapPhep = phanLoaiCapPhep;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_TEA_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoDanhMucMienThue", SqlDbType.Decimal, SoDanhMucMienThue);
			db.AddInParameter(dbCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, PhanLoaiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@DiaChiCuaNguoiKhai", SqlDbType.NVarChar, DiaChiCuaNguoiKhai);
			db.AddInParameter(dbCommand, "@SDTCuaNguoiKhai", SqlDbType.VarChar, SDTCuaNguoiKhai);
			db.AddInParameter(dbCommand, "@ThoiHanMienThue", SqlDbType.DateTime, ThoiHanMienThue.Year <= 1753 ? DBNull.Value : (object) ThoiHanMienThue);
			db.AddInParameter(dbCommand, "@TenDuAnDauTu", SqlDbType.NVarChar, TenDuAnDauTu);
			db.AddInParameter(dbCommand, "@DiaDiemXayDungDuAn", SqlDbType.NVarChar, DiaDiemXayDungDuAn);
			db.AddInParameter(dbCommand, "@MucTieuDuAn", SqlDbType.NVarChar, MucTieuDuAn);
			db.AddInParameter(dbCommand, "@MaMienGiam", SqlDbType.VarChar, MaMienGiam);
			db.AddInParameter(dbCommand, "@PhamViDangKyDMMT", SqlDbType.NVarChar, PhamViDangKyDMMT);
			db.AddInParameter(dbCommand, "@NgayDuKienXNK", SqlDbType.DateTime, NgayDuKienXNK.Year <= 1753 ? DBNull.Value : (object) NgayDuKienXNK);
			db.AddInParameter(dbCommand, "@GP_GCNDauTuSo", SqlDbType.VarChar, GP_GCNDauTuSo);
			db.AddInParameter(dbCommand, "@NgayChungNhan", SqlDbType.DateTime, NgayChungNhan.Year <= 1753 ? DBNull.Value : (object) NgayChungNhan);
			db.AddInParameter(dbCommand, "@CapBoi", SqlDbType.NVarChar, CapBoi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@CamKetSuDung", SqlDbType.NVarChar, CamKetSuDung);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@KhaiBaoTuNgay", SqlDbType.DateTime, KhaiBaoTuNgay.Year <= 1753 ? DBNull.Value : (object) KhaiBaoTuNgay);
			db.AddInParameter(dbCommand, "@KhaiBaoDenNgay", SqlDbType.DateTime, KhaiBaoDenNgay.Year <= 1753 ? DBNull.Value : (object) KhaiBaoDenNgay);
			db.AddInParameter(dbCommand, "@MaSoQuanLyDSMT", SqlDbType.VarChar, MaSoQuanLyDSMT);
			db.AddInParameter(dbCommand, "@PhanLoaiCapPhep", SqlDbType.VarChar, PhanLoaiCapPhep);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_TEA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TEA item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_TEA(long id, decimal soDanhMucMienThue, string phanLoaiXuatNhapKhau, string coQuanHaiQuan, string diaChiCuaNguoiKhai, string sDTCuaNguoiKhai, DateTime thoiHanMienThue, string tenDuAnDauTu, string diaDiemXayDungDuAn, string mucTieuDuAn, string maMienGiam, string phamViDangKyDMMT, DateTime ngayDuKienXNK, string gP_GCNDauTuSo, DateTime ngayChungNhan, string capBoi, string ghiChu, string camKetSuDung, string maNguoiKhai, DateTime khaiBaoTuNgay, DateTime khaiBaoDenNgay, string maSoQuanLyDSMT, string phanLoaiCapPhep, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TEA entity = new KDT_VNACCS_TEA();			
			entity.ID = id;
			entity.SoDanhMucMienThue = soDanhMucMienThue;
			entity.PhanLoaiXuatNhapKhau = phanLoaiXuatNhapKhau;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.DiaChiCuaNguoiKhai = diaChiCuaNguoiKhai;
			entity.SDTCuaNguoiKhai = sDTCuaNguoiKhai;
			entity.ThoiHanMienThue = thoiHanMienThue;
			entity.TenDuAnDauTu = tenDuAnDauTu;
			entity.DiaDiemXayDungDuAn = diaDiemXayDungDuAn;
			entity.MucTieuDuAn = mucTieuDuAn;
			entity.MaMienGiam = maMienGiam;
			entity.PhamViDangKyDMMT = phamViDangKyDMMT;
			entity.NgayDuKienXNK = ngayDuKienXNK;
			entity.GP_GCNDauTuSo = gP_GCNDauTuSo;
			entity.NgayChungNhan = ngayChungNhan;
			entity.CapBoi = capBoi;
			entity.GhiChu = ghiChu;
			entity.CamKetSuDung = camKetSuDung;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.KhaiBaoTuNgay = khaiBaoTuNgay;
			entity.KhaiBaoDenNgay = khaiBaoDenNgay;
			entity.MaSoQuanLyDSMT = maSoQuanLyDSMT;
			entity.PhanLoaiCapPhep = phanLoaiCapPhep;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_TEA_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoDanhMucMienThue", SqlDbType.Decimal, SoDanhMucMienThue);
			db.AddInParameter(dbCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, PhanLoaiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@DiaChiCuaNguoiKhai", SqlDbType.NVarChar, DiaChiCuaNguoiKhai);
			db.AddInParameter(dbCommand, "@SDTCuaNguoiKhai", SqlDbType.VarChar, SDTCuaNguoiKhai);
			db.AddInParameter(dbCommand, "@ThoiHanMienThue", SqlDbType.DateTime, ThoiHanMienThue.Year <= 1753 ? DBNull.Value : (object) ThoiHanMienThue);
			db.AddInParameter(dbCommand, "@TenDuAnDauTu", SqlDbType.NVarChar, TenDuAnDauTu);
			db.AddInParameter(dbCommand, "@DiaDiemXayDungDuAn", SqlDbType.NVarChar, DiaDiemXayDungDuAn);
			db.AddInParameter(dbCommand, "@MucTieuDuAn", SqlDbType.NVarChar, MucTieuDuAn);
			db.AddInParameter(dbCommand, "@MaMienGiam", SqlDbType.VarChar, MaMienGiam);
			db.AddInParameter(dbCommand, "@PhamViDangKyDMMT", SqlDbType.NVarChar, PhamViDangKyDMMT);
			db.AddInParameter(dbCommand, "@NgayDuKienXNK", SqlDbType.DateTime, NgayDuKienXNK.Year <= 1753 ? DBNull.Value : (object) NgayDuKienXNK);
			db.AddInParameter(dbCommand, "@GP_GCNDauTuSo", SqlDbType.VarChar, GP_GCNDauTuSo);
			db.AddInParameter(dbCommand, "@NgayChungNhan", SqlDbType.DateTime, NgayChungNhan.Year <= 1753 ? DBNull.Value : (object) NgayChungNhan);
			db.AddInParameter(dbCommand, "@CapBoi", SqlDbType.NVarChar, CapBoi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@CamKetSuDung", SqlDbType.NVarChar, CamKetSuDung);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@KhaiBaoTuNgay", SqlDbType.DateTime, KhaiBaoTuNgay.Year <= 1753 ? DBNull.Value : (object) KhaiBaoTuNgay);
			db.AddInParameter(dbCommand, "@KhaiBaoDenNgay", SqlDbType.DateTime, KhaiBaoDenNgay.Year <= 1753 ? DBNull.Value : (object) KhaiBaoDenNgay);
			db.AddInParameter(dbCommand, "@MaSoQuanLyDSMT", SqlDbType.VarChar, MaSoQuanLyDSMT);
			db.AddInParameter(dbCommand, "@PhanLoaiCapPhep", SqlDbType.VarChar, PhanLoaiCapPhep);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_TEA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TEA item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_TEA(long id, decimal soDanhMucMienThue, string phanLoaiXuatNhapKhau, string coQuanHaiQuan, string diaChiCuaNguoiKhai, string sDTCuaNguoiKhai, DateTime thoiHanMienThue, string tenDuAnDauTu, string diaDiemXayDungDuAn, string mucTieuDuAn, string maMienGiam, string phamViDangKyDMMT, DateTime ngayDuKienXNK, string gP_GCNDauTuSo, DateTime ngayChungNhan, string capBoi, string ghiChu, string camKetSuDung, string maNguoiKhai, DateTime khaiBaoTuNgay, DateTime khaiBaoDenNgay, string maSoQuanLyDSMT, string phanLoaiCapPhep, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TEA entity = new KDT_VNACCS_TEA();			
			entity.ID = id;
			entity.SoDanhMucMienThue = soDanhMucMienThue;
			entity.PhanLoaiXuatNhapKhau = phanLoaiXuatNhapKhau;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.DiaChiCuaNguoiKhai = diaChiCuaNguoiKhai;
			entity.SDTCuaNguoiKhai = sDTCuaNguoiKhai;
			entity.ThoiHanMienThue = thoiHanMienThue;
			entity.TenDuAnDauTu = tenDuAnDauTu;
			entity.DiaDiemXayDungDuAn = diaDiemXayDungDuAn;
			entity.MucTieuDuAn = mucTieuDuAn;
			entity.MaMienGiam = maMienGiam;
			entity.PhamViDangKyDMMT = phamViDangKyDMMT;
			entity.NgayDuKienXNK = ngayDuKienXNK;
			entity.GP_GCNDauTuSo = gP_GCNDauTuSo;
			entity.NgayChungNhan = ngayChungNhan;
			entity.CapBoi = capBoi;
			entity.GhiChu = ghiChu;
			entity.CamKetSuDung = camKetSuDung;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.KhaiBaoTuNgay = khaiBaoTuNgay;
			entity.KhaiBaoDenNgay = khaiBaoDenNgay;
			entity.MaSoQuanLyDSMT = maSoQuanLyDSMT;
			entity.PhanLoaiCapPhep = phanLoaiCapPhep;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TEA_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoDanhMucMienThue", SqlDbType.Decimal, SoDanhMucMienThue);
			db.AddInParameter(dbCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, PhanLoaiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@DiaChiCuaNguoiKhai", SqlDbType.NVarChar, DiaChiCuaNguoiKhai);
			db.AddInParameter(dbCommand, "@SDTCuaNguoiKhai", SqlDbType.VarChar, SDTCuaNguoiKhai);
			db.AddInParameter(dbCommand, "@ThoiHanMienThue", SqlDbType.DateTime, ThoiHanMienThue.Year <= 1753 ? DBNull.Value : (object) ThoiHanMienThue);
			db.AddInParameter(dbCommand, "@TenDuAnDauTu", SqlDbType.NVarChar, TenDuAnDauTu);
			db.AddInParameter(dbCommand, "@DiaDiemXayDungDuAn", SqlDbType.NVarChar, DiaDiemXayDungDuAn);
			db.AddInParameter(dbCommand, "@MucTieuDuAn", SqlDbType.NVarChar, MucTieuDuAn);
			db.AddInParameter(dbCommand, "@MaMienGiam", SqlDbType.VarChar, MaMienGiam);
			db.AddInParameter(dbCommand, "@PhamViDangKyDMMT", SqlDbType.NVarChar, PhamViDangKyDMMT);
			db.AddInParameter(dbCommand, "@NgayDuKienXNK", SqlDbType.DateTime, NgayDuKienXNK.Year <= 1753 ? DBNull.Value : (object) NgayDuKienXNK);
			db.AddInParameter(dbCommand, "@GP_GCNDauTuSo", SqlDbType.VarChar, GP_GCNDauTuSo);
			db.AddInParameter(dbCommand, "@NgayChungNhan", SqlDbType.DateTime, NgayChungNhan.Year <= 1753 ? DBNull.Value : (object) NgayChungNhan);
			db.AddInParameter(dbCommand, "@CapBoi", SqlDbType.NVarChar, CapBoi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@CamKetSuDung", SqlDbType.NVarChar, CamKetSuDung);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@KhaiBaoTuNgay", SqlDbType.DateTime, KhaiBaoTuNgay.Year <= 1753 ? DBNull.Value : (object) KhaiBaoTuNgay);
			db.AddInParameter(dbCommand, "@KhaiBaoDenNgay", SqlDbType.DateTime, KhaiBaoDenNgay.Year <= 1753 ? DBNull.Value : (object) KhaiBaoDenNgay);
			db.AddInParameter(dbCommand, "@MaSoQuanLyDSMT", SqlDbType.VarChar, MaSoQuanLyDSMT);
			db.AddInParameter(dbCommand, "@PhanLoaiCapPhep", SqlDbType.VarChar, PhanLoaiCapPhep);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_TEA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TEA item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_TEA(long id)
		{
			KDT_VNACCS_TEA entity = new KDT_VNACCS_TEA();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TEA_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_TEA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TEA item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}