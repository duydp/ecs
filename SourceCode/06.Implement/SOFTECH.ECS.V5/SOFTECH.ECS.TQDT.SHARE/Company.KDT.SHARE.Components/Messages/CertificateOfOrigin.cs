﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class CertificateOfOrigin : CertificateOfOriginBase
    {
        /// <summary>
        /// Người xuất khẩu (ID,Name)
        /// </summary>
        [XmlElement("Exporter")]
        public NameBase ExporterEx { get; set; }
        
        /// <summary>
        /// Nước Xuất khẩu (Code,Name)
        /// </summary>
        [XmlElement("ExportationCountry")]
        public LocationNameBase ExportationCountryEx { get; set; }
        
        /// <summary>
        /// người nhập khẩu (ID,Name)
        /// </summary>
        [XmlElement("Importer")]
        public NameBase ImporterEx { get; set; }
        
        /// <summary>
        /// Nước nhập khẩu(Code,Name)
        /// </summary>
        [XmlElement("ImportationCountry")]
        public LocationNameBase ImportationCountryEx { get; set; }
        
        /// <summary>
        /// Cảng xếp hàng
        /// </summary>
        [XmlElement("LoadingLocation")]
        public LoadingLocation LoadingLocation { get; set; }
        /// <summary>
        /// Cảng dở hàng
        /// </summary>
        [XmlElement("UnloadingLocation")]
        public UnloadingLocation UnloadingLocation { get; set; }
        
        /// <summary>
        /// Thông Tin Thêm
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        /// <summary>
        /// Hàng khai báo
        /// </summary>
        [XmlElement("GoodsItem")]
        public List<GoodsItem> GoodsItems { get; set; }

        /// <summary>
        /// Mo ta hang hoa
        /// </summary>
        [XmlElement("description")]
        public string Description { get; set; }

        /// <summary>
        /// Ty le phan tram ham luong xuat xu ghi tren C/O
        /// </summary>
        [XmlElement("percentOrigin")]
        public string PercentOrigin { get; set; }

      

    }
}
