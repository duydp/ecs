﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
using Logger;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
using Logger;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Logger;
#endif

namespace Company.Interface
{
    public partial class ListChungTuTruocDoForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        /*public ToKhaiMauDich TKMD;*/
        public long Master_ID = 0;
        public string Type;
        public ThuTucHQTruocDo ThuTucTruoc;
        public List<ChungTuHQTruocDo> listChungTuDelete = new List<ChungTuHQTruocDo>();
        //-----------------------------------------------------------------------------------------

        public ListChungTuTruocDoForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {   

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BinData()
        {
            if (ThuTucTruoc != null && ThuTucTruoc.ListChungTu != null)
            {
                dgList.DataSource = ThuTucTruoc.ListChungTu;
             
            }
            else
            {
              
            }

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

        }
        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            if (ThuTucTruoc == null)
                ThuTucTruoc = ThuTucHQTruocDo.LoadFullChungTu(this.Master_ID, Type);
            if (ThuTucTruoc == null)
                ThuTucTruoc = new ThuTucHQTruocDo();
            else
                txtnoiDungThuTuc.Text = ThuTucTruoc.NoiDungThuTuc;
            BinData();
          
            System.Data.DataView view = LoaiChungTu.SelectAll().Tables[0].DefaultView;
            view.Sort = "ID ASC";


        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<ChungTuHQTruocDo> listChungTu = new List<ChungTuHQTruocDo>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuHQTruocDo gp = (ChungTuHQTruocDo)i.GetRow().DataRow;
                        //ThuTucTruoc.ListChungTu.Remove(gp);
                        listChungTuDelete.Add(gp);
                       /* listChungTu.Add(gp);*/
                    }
                }
            }
            foreach (ChungTuHQTruocDo item in listChungTuDelete)
            {
                ThuTucTruoc.ListChungTu.Remove(item);
            }


            BinData();

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            try
            {
                if (ThuTucTruoc.ListChungTu == null)
                    ThuTucTruoc.ListChungTu = new List<ChungTuHQTruocDo>();
                ChungTuHQTruocDoForm ctForm = new ChungTuHQTruocDoForm();

                ctForm.Type = Type;
                ctForm.Master_Id = this.Master_ID;
                ctForm.ShowDialog(this);
                if (ctForm.DialogResult == System.Windows.Forms.DialogResult.Yes)
                    ThuTucTruoc.ListChungTu.Add(ctForm.ChungTuTruoc);
                BinData();
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
           
        }
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChungTuHQTruocDoForm f = new ChungTuHQTruocDoForm();
      
            f.ChungTuTruoc = (ChungTuHQTruocDo)e.Row.DataRow;
  
            f.Type = Type;
            f.Master_Id = this.Master_ID;
            f.ShowDialog(this);
            
            BinData();
           
        }

      
        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
           
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                ThuTucTruoc.NoiDungThuTuc = txtnoiDungThuTuc.Text.Trim();
                ThuTucTruoc.Type = this.Type;
                ThuTucTruoc.Master_ID = this.Master_ID;
                foreach (ChungTuHQTruocDo item in listChungTuDelete)
                {
                    if(item!=null)
                    {
                        ChungTuHQTruocDo del = ChungTuHQTruocDo.Load(item.ID);
                        if (del != null)
                            del.Delete();
                    }
                }
                if (Master_ID > 0)
                {
                    if (ThuTucTruoc.InsertUpdateFull() == 1)
                    {
                        ShowMessage("Lưu thông tin thành công", false);
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                    else
                        ShowMessage("Lưu thông tin bị lỗi dữ liệu", false);
                }
                else
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
            }
            catch (System.Exception ex)
            {
                ShowMessage(ex.Message, false);
                LocalLogger.Instance().WriteMessage(ex);
            }
           
        }

        private void uiGroupBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
