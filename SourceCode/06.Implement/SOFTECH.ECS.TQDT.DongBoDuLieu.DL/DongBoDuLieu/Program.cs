using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
//using System.Management;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using Company.BLL.Utils;

namespace DongBoDuLieu
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CultureInfo culture = culture = new CultureInfo("vi-VN");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            //Application.Run(new Admin.QuanLyNguoiDung());// Chay form tao nguoi dung
            Application.Run(new MainForm());

            //Company.KDT.SHARE.Components.DaiLy user = Company.KDT.SHARE.Components.DaiLy.Load("dragon", "1");
        }
    }
}
