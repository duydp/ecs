namespace Company.Interface
{
    partial class ChuyenTrangThaiTK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.ccNgayDayKy = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMaToKhai = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnThuchien = new Janus.Windows.EditControls.UIButton();
            this.btnHuybo = new Janus.Windows.EditControls.UIButton();
            this.lblCaption = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblMaLoaiHinh = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbPL = new Janus.Windows.EditControls.UIComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtHuongDan = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.groupBox2);
            this.grbMain.Controls.Add(this.groupBox1);
            this.grbMain.Controls.Add(this.btnHuybo);
            this.grbMain.Controls.Add(this.btnThuchien);
            this.grbMain.Controls.Add(this.lblCaption);
            this.grbMain.Size = new System.Drawing.Size(499, 397);
            // 
            // ccNgayDayKy
            // 
            // 
            // 
            // 
            this.ccNgayDayKy.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDayKy.DropDownCalendar.Name = "";
            this.ccNgayDayKy.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDayKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDayKy.Location = new System.Drawing.Point(322, 44);
            this.ccNgayDayKy.Name = "ccNgayDayKy";
            this.ccNgayDayKy.Nullable = true;
            this.ccNgayDayKy.NullButtonText = "Xóa";
            this.ccNgayDayKy.ShowNullButton = true;
            this.ccNgayDayKy.Size = new System.Drawing.Size(96, 21);
            this.ccNgayDayKy.TabIndex = 4;
            this.ccNgayDayKy.TodayButtonText = "Hôm nay";
            this.ccNgayDayKy.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDayKy.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(233, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 268;
            this.label2.Text = "Ngày đăng ký:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 268;
            this.label1.Text = "Số tờ khai:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(25, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 268;
            this.label3.Text = "Tờ khai (ID):";
            // 
            // lblMaToKhai
            // 
            this.lblMaToKhai.AutoSize = true;
            this.lblMaToKhai.BackColor = System.Drawing.Color.Transparent;
            this.lblMaToKhai.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaToKhai.ForeColor = System.Drawing.Color.Red;
            this.lblMaToKhai.Location = new System.Drawing.Point(100, 18);
            this.lblMaToKhai.Name = "lblMaToKhai";
            this.lblMaToKhai.Size = new System.Drawing.Size(34, 23);
            this.lblMaToKhai.TabIndex = 268;
            this.lblMaToKhai.Text = "00";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(25, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(423, 30);
            this.label4.TabIndex = 268;
            this.label4.Text = "     - Để chuyển trạng thái của tờ khai này, bạn cần phải hoàn thành thông tin bắ" +
                "t buột bên trên";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(203, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 268;
            this.label5.Text = "*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(421, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 268;
            this.label6.Text = "*";
            // 
            // btnThuchien
            // 
            this.btnThuchien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThuchien.Location = new System.Drawing.Point(348, 361);
            this.btnThuchien.Name = "btnThuchien";
            this.btnThuchien.Size = new System.Drawing.Size(67, 24);
            this.btnThuchien.TabIndex = 270;
            this.btnThuchien.Text = "Thực hiện";
            this.btnThuchien.VisualStyleManager = this.vsmMain;
            this.btnThuchien.Click += new System.EventHandler(this.btnThuchien_Click);
            // 
            // btnHuybo
            // 
            this.btnHuybo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnHuybo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuybo.Location = new System.Drawing.Point(421, 361);
            this.btnHuybo.Name = "btnHuybo";
            this.btnHuybo.Size = new System.Drawing.Size(67, 24);
            this.btnHuybo.TabIndex = 271;
            this.btnHuybo.Text = "Hủy bỏ";
            this.btnHuybo.VisualStyleManager = this.vsmMain;
            this.btnHuybo.Click += new System.EventHandler(this.btnHuybo_Click);
            // 
            // lblCaption
            // 
            this.lblCaption.AutoSize = true;
            this.lblCaption.BackColor = System.Drawing.Color.Transparent;
            this.lblCaption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.ForeColor = System.Drawing.Color.Red;
            this.lblCaption.Location = new System.Drawing.Point(22, 9);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(252, 25);
            this.lblCaption.TabIndex = 268;
            this.lblCaption.Text = "Chuyển trạng thái tờ khai";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(25, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(423, 30);
            this.label7.TabIndex = 268;
            this.label7.Text = "     - Sau khi nhập thông tin về \"Số tờ khai\" và \"Ngày đăng ký\", Click vào nút \"T" +
                "hực hiện\" bên dưới để chuyển trạng thái.";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Location = new System.Drawing.Point(25, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(423, 30);
            this.label8.TabIndex = 268;
            this.label8.Text = "     - Sau khi Click vào nút \"Thực hiện\", tờ khai này sẽ được chuyển sang trạng t" +
                "hái đã duyệt.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(242, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 268;
            this.label10.Text = "Mã loại hình:";
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.AutoSize = true;
            this.lblMaLoaiHinh.BackColor = System.Drawing.Color.Transparent;
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaLoaiHinh.ForeColor = System.Drawing.Color.Red;
            this.lblMaLoaiHinh.Location = new System.Drawing.Point(317, 18);
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.Size = new System.Drawing.Size(36, 23);
            this.lblMaLoaiHinh.TabIndex = 268;
            this.lblMaLoaiHinh.Text = "XK";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.txtHuongDan);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cbPL);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtSoToKhai);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.ccNgayDayKy);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lblMaLoaiHinh);
            this.groupBox1.Controls.Add(this.lblMaToKhai);
            this.groupBox1.Location = new System.Drawing.Point(27, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(454, 200);
            this.groupBox1.TabIndex = 272;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin tờ khai";
            // 
            // cbPL
            // 
            this.cbPL.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPL.DisplayMember = "Name";
            this.cbPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Luồng Xanh";
            uiComboBoxItem4.Value = 1;
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Luồng Vàng";
            uiComboBoxItem5.Value = 2;
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Luồng Đỏ";
            uiComboBoxItem6.Value = 3;
            this.cbPL.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbPL.Location = new System.Drawing.Point(106, 77);
            this.cbPL.Name = "cbPL";
            this.cbPL.Size = new System.Drawing.Size(312, 21);
            this.cbPL.TabIndex = 272;
            this.cbPL.ValueMember = "ID";
            this.cbPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbPL.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(32, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 271;
            this.label11.Text = "Phân luồng:";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 0;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.FormatString = "#####";
            this.txtSoToKhai.Location = new System.Drawing.Point(107, 44);
            this.txtSoToKhai.MaxLength = 10;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(93, 21);
            this.txtSoToKhai.TabIndex = 270;
            this.txtSoToKhai.Text = "1";
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.Value = ((ulong)(1ul));
            this.txtSoToKhai.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.DimGray;
            this.label9.Location = new System.Drawing.Point(37, 160);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(398, 27);
            this.label9.TabIndex = 268;
            this.label9.Text = "\"Số tờ khai\" và \"Ngày đăng ký\" của tờ khai này phải chính xác với thông tin trên " +
                "Hải quan";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(34, 244);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(454, 111);
            this.groupBox2.TabIndex = 273;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lưu ý";
            // 
            // txtHuongDan
            // 
            this.txtHuongDan.Location = new System.Drawing.Point(107, 104);
            this.txtHuongDan.Multiline = true;
            this.txtHuongDan.Name = "txtHuongDan";
            this.txtHuongDan.Size = new System.Drawing.Size(304, 53);
            this.txtHuongDan.TabIndex = 276;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(32, 107);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 13);
            this.label12.TabIndex = 275;
            this.label12.Text = "Hướng dẫn:";
            // 
            // ChuyenTrangThaiTK
            // 
            this.AcceptButton = this.btnThuchien;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnHuybo;
            this.ClientSize = new System.Drawing.Size(499, 397);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChuyenTrangThaiTK";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chuyển trạng thái tờ khai";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayDayKy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblMaToKhai;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnHuybo;
        private Janus.Windows.EditControls.UIButton btnThuchien;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblMaLoaiHinh;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private Janus.Windows.EditControls.UIComboBox cbPL;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtHuongDan;
        private System.Windows.Forms.Label label12;
    }
}