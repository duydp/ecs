﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Convert64
{
    public partial class frmMain : Form
    {
        string _connectionString = "Server=192.168.72.100\\ecsexpress; database=ECS_TrapService; uid=sa; pwd=123456";

        public frmMain()
        {
            InitializeComponent();
        }
        public static string ConvertFromBase64(string strSource)
        {
            byte[] bArr = Convert.FromBase64String(strSource);
            return Encoding.Unicode.GetString(bArr);
        }
        public static string ConvertToBase64(string strSource)
        {
            byte[] bArr = Encoding.Unicode.GetBytes(strSource);
            return Convert.ToBase64String(bArr);
        }
        private XmlDocument doc = null;

        private void btConvert_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkBox1.Checked == true)
                    rtbDestination.Text = ConvertToBase64(rtbSource.Text);
                else
                    rtbDestination.Text = ConvertFromBase64(rtbSource.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    doc = new XmlDocument();

                    try
                    {
                        doc.Load(openFileDialog1.FileName);
                        XmlNode node = doc.SelectSingleNode("Envelope/Body/Content");
                        if (node != null)
                            rtbSource.Text = node.InnerXml;
                        txtFileName.Text = openFileDialog1.FileName;
                    }
                    catch
                    {
                        MessageBox.Show("Noi dung file khong dung dinh dang XML");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter wText = new StreamWriter(saveFileDialog1.FileName);
                wText.Write(rtbDestination.Text.Trim());
                wText.Close();

                if (MessageBox.Show("Da luu thanh cong. Ban co muon mo file khong?.") == DialogResult.OK)
                {
                    System.Diagnostics.Process.Start(saveFileDialog1.FileName);
                }
            }
        }

        private object CreateCommand(string queryString)
        {
            return CreateCommand(queryString, _connectionString);
        }
        private object CreateCommand(string queryString, string connectionstring)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(queryString);

                return db.ExecuteScalar(dbCommand);

                using (SqlConnection connection = new SqlConnection(
                           connectionstring))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    return command.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private void btnLayNoiDungXml_Click(object sender, EventArgs e)
        {
            try
            {
                string content = null;
                object objContent = null;

                if (chTrapDb.Checked)
                {
                    List<cMSG> filters = cMSG.SelectCollectionDynamic("ID = " + cboIDMsg.Text, "");
                    //objContent = CreateCommand(@"SELECT  [MSG] FROM dbo.[MSG] where ID=" + cboIDMsg.Text);
                    objContent = filters.Count > 0 ? filters[0].MSG : null;
                }
                else
                {
                    objContent = CreateCommand("select MessageContent from t_KDT_Messages where ID=" + cboIDMsg.Text + "");
                }

                if (objContent != null)
                {
                    content = objContent.ToString();
                }
                if (content == null) return;

                doc = new XmlDocument();
                rtbSource.Text = "";
                rtbDestination.Text = "";
                try
                {
                    doc.LoadXml(content);
                    XmlNode node = doc.SelectSingleNode("Envelope/Body/Content");
                    if (node != null)
                        rtbSource.Text = node.InnerXml;
                }
                catch
                {
                    rtbSource.Text = content;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnLayHetXml_Click(object sender, EventArgs e)
        {
            try
            {
                string content = null;
                object objContent = null;

                if (chTrapDb.Checked)
                {
                    List<cMSG> filters = cMSG.SelectCollectionDynamic("ID = " + cboIDMsg.Text, "");
                    //objContent = CreateCommand(@"SELECT  [MSG] FROM dbo.[MSG] where ID=" + cboIDMsg.Text);
                    objContent = filters.Count > 0 ? filters[0].MSG : null;
                }
                else
                {
                    objContent = CreateCommand("select MessageContent from t_KDT_Messages where ID=" + cboIDMsg.Text + "");
                }

                if (objContent != null)
                {
                    content = objContent.ToString();
                }
                if (content == null) return;

                doc = new XmlDocument();
                rtbDestination.Text = "";
                rtbSource.Text = "";
                try
                {
                    doc.LoadXml(content);
                    rtbSource.Text = doc.InnerXml;
                }
                catch
                {
                    rtbSource.Text = content;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            GetDates();
        }

        private void GetID(string ngayKB)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                DataTable dtID = cMSG.SelectID(ngayKB);

                cboIDMsg.DataSource = dtID;
                cboIDMsg.DisplayMember = "ID";
                cboIDMsg.ValueMember = "ID";

                cboIDMsg.SelectedIndex = cboIDMsg.Items.Count > 0 ? 0 : -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetDates()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                DataTable dt = cMSG.SelectDates();

                cboDate.DataSource = dt;
                cboDate.DisplayMember = "NgayKB";
                cboDate.ValueMember = "NgayKB";

                cboDate.SelectedIndex = cboDate.Items.Count > 0 ? 0 : -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnConvertFull_Click(object sender, EventArgs e)
        {
            try
            {
                if (doc != null)
                {
                    XmlNode node = doc.SelectSingleNode("Envelope/Body/Content");
                    string value = string.Empty;
                    if (node != null)
                    {
                        value = ConvertFromBase64(node.InnerText);
                        node.InnerXml = value;
                        rtbDestination.Text = doc.InnerXml;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cboDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetID(cboDate.Text);
        }
    }
}
