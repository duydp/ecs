
UPDATE [dbo].[t_HaiQuan_CuaKhau]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_CuaKhau]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL
     
UPDATE [dbo].[t_HaiQuan_Cuc]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_Cuc]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL
     
UPDATE [dbo].[t_HaiQuan_DieuKienGiaoHang]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_DieuKienGiaoHang]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL
     
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL
     
UPDATE [dbo].[t_HaiQuan_DonViTinh]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_DonViTinh]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL
     
UPDATE [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL
     
UPDATE [dbo].[t_HaiQuan_MaHS]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_MaHS]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL
     
UPDATE [dbo].[t_HaiQuan_NguyenTe]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_NguyenTe]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL
     
UPDATE [dbo].[t_HaiQuan_Nuoc]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_Nuoc]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL
     
UPDATE [dbo].[t_HaiQuan_PhuongThucThanhToan]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_PhuongThucThanhToan]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL
     
UPDATE [dbo].[t_HaiQuan_PhuongThucVanTai]  SET	DateCreated = '1900-01-01'  WHERE DateCreated IS NULL  
UPDATE [dbo].[t_HaiQuan_PhuongThucVanTai]  SET	DateModified = '1900-01-01'  WHERE DateModified IS NULL

go

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='4.0', Date = getdate(), Notes = N''
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('4.0', getdate(), N'')
	end 



