-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_HeThongPhongKhai_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HeThongPhongKhai_Insert]

IF OBJECT_ID(N'[dbo].[p_HeThongPhongKhai_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HeThongPhongKhai_Update]

IF OBJECT_ID(N'[dbo].[p_HeThongPhongKhai_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HeThongPhongKhai_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_HeThongPhongKhai_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HeThongPhongKhai_Delete]

IF OBJECT_ID(N'[dbo].[p_HeThongPhongKhai_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HeThongPhongKhai_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_HeThongPhongKhai_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HeThongPhongKhai_Load]

IF OBJECT_ID(N'[dbo].[p_HeThongPhongKhai_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HeThongPhongKhai_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_HeThongPhongKhai_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HeThongPhongKhai_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, September 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_Insert]
	@MaDoanhNghiep char(15),
	@PassWord varchar(50),
	@TenDoanhNghiep nvarchar(500),
	@Key_Config varchar(30),
	@Value_Config nvarchar(1000),
	@Role smallint
AS
INSERT INTO [dbo].[t_HeThongPhongKhai]
(
	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
)
VALUES
(
	@MaDoanhNghiep,
	@PassWord,
	@TenDoanhNghiep,
	@Key_Config,
	@Value_Config,
	@Role
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, September 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_Update]
	@MaDoanhNghiep char(15),
	@PassWord varchar(50),
	@TenDoanhNghiep nvarchar(500),
	@Key_Config varchar(30),
	@Value_Config nvarchar(1000),
	@Role smallint
AS

UPDATE
	[dbo].[t_HeThongPhongKhai]
SET
	[PassWord] = @PassWord,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[Value_Config] = @Value_Config,
	[Role] = @Role
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Key_Config] = @Key_Config

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, September 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_InsertUpdate]
	@MaDoanhNghiep char(15),
	@PassWord varchar(50),
	@TenDoanhNghiep nvarchar(500),
	@Key_Config varchar(30),
	@Value_Config nvarchar(1000),
	@Role smallint
AS
IF EXISTS(SELECT [MaDoanhNghiep], [Key_Config] FROM [dbo].[t_HeThongPhongKhai] WHERE [MaDoanhNghiep] = @MaDoanhNghiep AND [Key_Config] = @Key_Config)
	BEGIN
		UPDATE
			[dbo].[t_HeThongPhongKhai] 
		SET
			[PassWord] = @PassWord,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[Value_Config] = @Value_Config,
			[Role] = @Role
		WHERE
			[MaDoanhNghiep] = @MaDoanhNghiep
			AND [Key_Config] = @Key_Config
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HeThongPhongKhai]
	(
			[MaDoanhNghiep],
			[PassWord],
			[TenDoanhNghiep],
			[Key_Config],
			[Value_Config],
			[Role]
	)
	VALUES
	(
			@MaDoanhNghiep,
			@PassWord,
			@TenDoanhNghiep,
			@Key_Config,
			@Value_Config,
			@Role
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, September 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_Delete]
	@MaDoanhNghiep char(15),
	@Key_Config varchar(30)
AS

DELETE FROM 
	[dbo].[t_HeThongPhongKhai]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Key_Config] = @Key_Config

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, September 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HeThongPhongKhai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, September 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_Load]
	@MaDoanhNghiep char(15),
	@Key_Config varchar(30)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
FROM
	[dbo].[t_HeThongPhongKhai]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Key_Config] = @Key_Config
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, September 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
FROM [dbo].[t_HeThongPhongKhai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, September 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
FROM
	[dbo].[t_HeThongPhongKhai]	

GO

