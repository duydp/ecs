﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.Components;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class SoContainer
    {

        public void LoadSoContFromVanDon(VanDon vd)
        {
            if (vd!=null )
            {
                if (vd.ContainerCollection == null || vd.ContainerCollection.Count == 0)
                    vd.LoadContainerCollection();
                if (vd.ContainerCollection != null && vd.ContainerCollection.Count > 0)
                {
                    Cont20 = Cont40 = Cont45 = ContKhac = 0;
                    foreach (Container item in vd.ContainerCollection)
                    {
                        switch (item.LoaiContainer)
                        {
                            case "2":
                                this.Cont20++;
                                break;
                            case "4":
                                this.Cont40++;
                                break;
                            case "5":
                                this.Cont45++;
                                break;
                            case "9":
                                this.ContKhac++;
                                break;
                        }
                    }
                }
            }
        }
        public int TongSoCont()
        {
            return this.Cont20 + this.Cont40 + this.Cont45 + this.ContKhac;
        }

    }
}
