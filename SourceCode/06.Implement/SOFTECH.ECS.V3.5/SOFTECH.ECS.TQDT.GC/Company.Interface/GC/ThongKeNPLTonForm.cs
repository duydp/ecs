﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.GC
{
    public partial class ThongKeNPLTonForm : BaseForm
    {
        public string LIST_HOPDONG_ID = "";
        public List<HopDong> Collection = new List<HopDong>();
        public ThongKeNPLTonForm()
        {
            InitializeComponent();
        }
        private void BindDataProcess()
        {
            try
            {
                String Last = LIST_HOPDONG_ID.Substring(LIST_HOPDONG_ID.Length - 1, 1);
                if (Last == ",")
                {
                    LIST_HOPDONG_ID = LIST_HOPDONG_ID.Substring(0, LIST_HOPDONG_ID.Length - 1);
                }
                dgListDetail.Refetch();
                dgListDetail.DataSource = KDT_GC_QuyetToan_NguyenPhuLieu.SelectDynamicBy(" HopDong_ID IN (" + LIST_HOPDONG_ID + ") ", "").Tables[0];
                dgListDetail.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindataTotal()
        {
            string Last= LIST_HOPDONG_ID.Substring(LIST_HOPDONG_ID.Length -1,1);
            if (Last ==",")
            {
                LIST_HOPDONG_ID = LIST_HOPDONG_ID.Substring(0, LIST_HOPDONG_ID.Length - 1);
            }
            dgListTotal.Refetch();
            dgListTotal.DataSource = KDT_GC_QuyetToan_NguyenPhuLieu.SelectDynamicGroupBy(" HopDong_ID IN (" + LIST_HOPDONG_ID + ") ", "").Tables[0];
            dgListTotal.Refresh();
        }
        private void btnProcess_Click(object sender, EventArgs e)
        {
            foreach (HopDong item in Collection)
            {
                try
                {
                    XuLyHopDongFrm fx = new XuLyHopDongFrm();
                    fx.dateFrom = dtpDateFrom.Value;
                    fx.dateTo = dtpDateTo.Value;
                    fx.StartPosition = FormStartPosition.CenterScreen;
                    fx.HD = item;
                    fx.isQuyetoan = true;
                    fx.ShowDialog(this);
                    //if (fx.DialogResult == DialogResult.OK)
                    //    showMsg("MSG_2702010");
                    //else
                    //{
                    //    this.ShowMessage("Lỗi xử lý hợp đồng + " + fx.ExceptionForm.Message, false);
                    //}
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            BindataTotal();
            BindDataProcess();
        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = tbNPLTon.SelectedTab.Text + "_" + LIST_HOPDONG_ID + "_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel(dgListTotal);
        }

        private void mnuExport_Click(object sender, EventArgs e)
        {
            ExportExcel(dgListDetail);
        }

        private void tbNPLTon_Selected(object sender, TabControlEventArgs e)
        {
            switch (tbNPLTon.SelectedTab.Name)
            {
                case "tpTotal":
                    BindataTotal();
                    break;
                case "tpDetail":
                    BindDataProcess();
                    break;
                default:
                    break;
            }
        }
    }
}
