﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.KDT.SXXK;
using System.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class ReportDinhMuc : DevExpress.XtraReports.UI.XtraReport
    {
        public string MaSP;
        public string TenSP;
        public string DVT;
        public string HopDong;
        public DinhMucDangKy DMDangKy;
        private int STT = 0;
        public delegate void GetObjectInfo(string hodong, string tokhai);
        public GetObjectInfo GetText;
        public bool inNKvaND = false;
        public ReportDinhMuc()
        {
            InitializeComponent();
        }


        public void BindReport()
        {
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI + " - " + GlobalSettings.DIA_CHI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (!string.IsNullOrEmpty(HopDong))
            {
                lblHopDong.Text = this.HopDong;
                //lblHopDong.Visible = true;
                //lblHopDongLable.Visible = true;
            }

            //else
            //{
            //lblHopDong.Visible = false;
            //lblHopDongLable.Visible = false;
            //lblSanPham.Location = lblHopDongLable.Location;
            //}
            if (this.DMDangKy.SoTiepNhan > 0)
                lblSoTN.Text = "STNĐM: " + this.DMDangKy.SoTiepNhan;
            lblSanPham.Text = "Mã sản phẩm : " + this.MaSP + "        - Tên sản phẩm : " + TenSP + "  -ĐVT : " + DVT;
            this.DataSource = DMDangKy.getDinhMucOfSanPham(this.MaSP, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

            lblMaNPL.DataBindings.Add("Text", this.DataSource, "MaNguyenPhuLieu");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT_ID");
            lblDM.DataBindings.Add("Text", this.DataSource, "DinhMucSuDung", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTyLeHH.DataBindings.Add("Text", this.DataSource, "TyLeHaoHut", "{0:n0}");
            lblDMChung.DataBindings.Add("Text", this.DataSource, "DinhMucChung", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, "TenNPL");
            if (!inNKvaND)
                lblNguonCung.DataBindings.Add("Text", this.DataSource, "FromVietNam");
            else
                lblNguonCung.Text = "Nhập khẩu và mua tại VN";
            lblNgay.Text = string.Format("{0}, ngày .. tháng .. năm ....", string.IsNullOrEmpty(GlobalSettings.DiaPhuong) ? "............" : GlobalSettings.DiaPhuong);
            lblNgay2.Text = string.Format("{0}, ngày {1} tháng {2} năm {3}",
                new object[] { string.IsNullOrEmpty(GlobalSettings.DiaPhuong) ? "............" : GlobalSettings.DiaPhuong, 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Day.ToString() : "..", 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Month.ToString():"..", 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Year.ToString():"...." });
            lbTitleSoToKhai.Visible = lbSoToKhai.Visible = GlobalSettings.M07DMDK;
            lbSoToKhai.Text = string.Format("{0}/XK/SXXK/{1} ngày {2}",
                     new object[] { ".......", GlobalSettings.MA_HAI_QUAN, ".../.../....." });
        }
        public void BindReportDinhMucDaDangKy()
        {
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI + " - " + GlobalSettings.DIA_CHI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (HopDong != null && HopDong != "")
                lblHopDong.Text = this.HopDong;

            lblSanPham.Text = "Mã sản phẩm : " + this.MaSP + "        - Tên sản phẩm : " + TenSP + "  -ĐVT : " + DVT;
            DataSet ds = BLL.SXXK.DinhMuc.getDinhMucOfSanPham(this.MaSP, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            if (ds.Tables.Count == 0) return;
            ds.Tables[0].Columns.Add("STTHang");
            int i = 1;
            foreach (DataRow row in ds.Tables[0].Rows)
                row["STTHang"] = i++;
            this.DataSource = ds.Tables[0];
            lblMaNPL.DataBindings.Add("Text", this.DataSource, "MaNguyenPhuLieu");
            lblSTT.DataBindings.Add("Text", this.DataSource, "STTHang");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT_ID");
            lblDM.DataBindings.Add("Text", this.DataSource, "DinhMucSuDung", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTyLeHH.DataBindings.Add("Text", this.DataSource, "TyLeHaoHut", "{0:n0}");
            lblDMChung.DataBindings.Add("Text", this.DataSource, "DinhMucChung", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, "TenNPL");

            lblNgay.Text = string.Format("{0}, ngày .. tháng .. năm ....", string.IsNullOrEmpty(GlobalSettings.DiaPhuong) ? "............" : GlobalSettings.DiaPhuong);
            lblNgay2.Text = string.Format("{0}, ngày {1} tháng {2} năm {3}",
                new object[] { string.IsNullOrEmpty(GlobalSettings.DiaPhuong) ? "............" : GlobalSettings.DiaPhuong, 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Day.ToString() : "..", 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Month.ToString():"..", 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Year.ToString():"...." });
            if (!inNKvaND)
                lblNguonCung.DataBindings.Add("Text", this.DataSource, "FromVietNam");
            else
                lblNguonCung.Text = "Nhập khẩu và mua tại VN";

            lbTitleSoToKhai.Visible = lbSoToKhai.Visible = GlobalSettings.M07DMDK;

            lbSoToKhai.Text = string.Format("{0}/XK/SXXK/{1} ngày {2}",
                              new object[] { ".......", GlobalSettings.MA_HAI_QUAN, ".../.../....." });

        }
        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
        }

        private void lblTyLeHH_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void ReportDinhMuc_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }
        public void SetText(string hopdong, string tokhai)
        {
            if (!string.IsNullOrEmpty(hopdong))
                lblHopDong.Text = hopdong;
            if (!string.IsNullOrEmpty(tokhai))
            {
                lbSoToKhai.Text = tokhai;
            }
            this.CreateDocument();
        }


        private void lbSoToKhai_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            if (GetText != null)
                GetText(lblHopDong.Text, lbSoToKhai.Text);
        }

    }
}
