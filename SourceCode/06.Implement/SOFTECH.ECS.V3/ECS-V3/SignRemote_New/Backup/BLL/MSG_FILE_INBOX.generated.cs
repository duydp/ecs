﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SignRemote
{
	public partial class MSG_FILE_INBOX : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MSG_FROM { set; get; }
		public string MSG_TO { set; get; }
		public string FILE_NAME { set; get; }
		public string FILE_DATA { set; get; }
		public DateTime CREATE_TIME { set; get; }
		public int STATUS { set; get; }
		public string GUIDSTR { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<MSG_FILE_INBOX> ConvertToCollection(IDataReader reader)
		{
			List<MSG_FILE_INBOX> collection = new List<MSG_FILE_INBOX>();
			while (reader.Read())
			{
				MSG_FILE_INBOX entity = new MSG_FILE_INBOX();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
				if (!reader.IsDBNull(reader.GetOrdinal("FILE_NAME"))) entity.FILE_NAME = reader.GetString(reader.GetOrdinal("FILE_NAME"));
				if (!reader.IsDBNull(reader.GetOrdinal("FILE_DATA"))) entity.FILE_DATA = reader.GetString(reader.GetOrdinal("FILE_DATA"));
				if (!reader.IsDBNull(reader.GetOrdinal("CREATE_TIME"))) entity.CREATE_TIME = reader.GetDateTime(reader.GetOrdinal("CREATE_TIME"));
				if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) entity.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<MSG_FILE_INBOX> collection, long id)
        {
            foreach (MSG_FILE_INBOX item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_MSG_FILE_INBOX VALUES(@MSG_FROM, @MSG_TO, @FILE_NAME, @FILE_DATA, @CREATE_TIME, @STATUS, @GUIDSTR)";
            string update = "UPDATE t_MSG_FILE_INBOX SET MSG_FROM = @MSG_FROM, MSG_TO = @MSG_TO, FILE_NAME = @FILE_NAME, FILE_DATA = @FILE_DATA, CREATE_TIME = @CREATE_TIME, STATUS = @STATUS, GUIDSTR = @GUIDSTR WHERE ID = @ID";
            string delete = "DELETE FROM t_MSG_FILE_INBOX WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MSG_FROM", SqlDbType.VarChar, "MSG_FROM", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MSG_TO", SqlDbType.VarChar, "MSG_TO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FILE_NAME", SqlDbType.NVarChar, "FILE_NAME", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FILE_DATA", SqlDbType.NVarChar, "FILE_DATA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CREATE_TIME", SqlDbType.DateTime, "CREATE_TIME", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STATUS", SqlDbType.Int, "STATUS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MSG_FROM", SqlDbType.VarChar, "MSG_FROM", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MSG_TO", SqlDbType.VarChar, "MSG_TO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FILE_NAME", SqlDbType.NVarChar, "FILE_NAME", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FILE_DATA", SqlDbType.NVarChar, "FILE_DATA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CREATE_TIME", SqlDbType.DateTime, "CREATE_TIME", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STATUS", SqlDbType.Int, "STATUS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_MSG_FILE_INBOX VALUES(@MSG_FROM, @MSG_TO, @FILE_NAME, @FILE_DATA, @CREATE_TIME, @STATUS, @GUIDSTR)";
            string update = "UPDATE t_MSG_FILE_INBOX SET MSG_FROM = @MSG_FROM, MSG_TO = @MSG_TO, FILE_NAME = @FILE_NAME, FILE_DATA = @FILE_DATA, CREATE_TIME = @CREATE_TIME, STATUS = @STATUS, GUIDSTR = @GUIDSTR WHERE ID = @ID";
            string delete = "DELETE FROM t_MSG_FILE_INBOX WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MSG_FROM", SqlDbType.VarChar, "MSG_FROM", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MSG_TO", SqlDbType.VarChar, "MSG_TO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FILE_NAME", SqlDbType.NVarChar, "FILE_NAME", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FILE_DATA", SqlDbType.NVarChar, "FILE_DATA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CREATE_TIME", SqlDbType.DateTime, "CREATE_TIME", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STATUS", SqlDbType.Int, "STATUS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MSG_FROM", SqlDbType.VarChar, "MSG_FROM", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MSG_TO", SqlDbType.VarChar, "MSG_TO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FILE_NAME", SqlDbType.NVarChar, "FILE_NAME", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FILE_DATA", SqlDbType.NVarChar, "FILE_DATA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CREATE_TIME", SqlDbType.DateTime, "CREATE_TIME", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STATUS", SqlDbType.Int, "STATUS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static MSG_FILE_INBOX Load(long id)
		{
			const string spName = "[dbo].[p_MSG_FILE_INBOX_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<MSG_FILE_INBOX> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<MSG_FILE_INBOX> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<MSG_FILE_INBOX> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
        public static MSG_FILE_INBOX SelectCollectionDynamicBy(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            List<MSG_FILE_INBOX> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_MSG_FILE_INBOX_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_MSG_FILE_INBOX_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_MSG_FILE_INBOX_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_MSG_FILE_INBOX_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertMSG_FILE_INBOX(string mSG_FROM, string mSG_TO, string fILE_NAME, string fILE_DATA, DateTime cREATE_TIME, int sTATUS, string gUIDSTR)
		{
			MSG_FILE_INBOX entity = new MSG_FILE_INBOX();	
			entity.MSG_FROM = mSG_FROM;
			entity.MSG_TO = mSG_TO;
			entity.FILE_NAME = fILE_NAME;
			entity.FILE_DATA = fILE_DATA;
			entity.CREATE_TIME = cREATE_TIME;
			entity.STATUS = sTATUS;
			entity.GUIDSTR = gUIDSTR;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_MSG_FILE_INBOX_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MSG_FROM", SqlDbType.VarChar, MSG_FROM);
			db.AddInParameter(dbCommand, "@MSG_TO", SqlDbType.VarChar, MSG_TO);
			db.AddInParameter(dbCommand, "@FILE_NAME", SqlDbType.NVarChar, FILE_NAME);
			db.AddInParameter(dbCommand, "@FILE_DATA", SqlDbType.NVarChar, FILE_DATA);
			db.AddInParameter(dbCommand, "@CREATE_TIME", SqlDbType.DateTime, CREATE_TIME.Year <= 1753 ? DBNull.Value : (object) CREATE_TIME);
			db.AddInParameter(dbCommand, "@STATUS", SqlDbType.Int, STATUS);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<MSG_FILE_INBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MSG_FILE_INBOX item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateMSG_FILE_INBOX(long id, string mSG_FROM, string mSG_TO, string fILE_NAME, string fILE_DATA, DateTime cREATE_TIME, int sTATUS, string gUIDSTR)
		{
			MSG_FILE_INBOX entity = new MSG_FILE_INBOX();			
			entity.ID = id;
			entity.MSG_FROM = mSG_FROM;
			entity.MSG_TO = mSG_TO;
			entity.FILE_NAME = fILE_NAME;
			entity.FILE_DATA = fILE_DATA;
			entity.CREATE_TIME = cREATE_TIME;
			entity.STATUS = sTATUS;
			entity.GUIDSTR = gUIDSTR;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_MSG_FILE_INBOX_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MSG_FROM", SqlDbType.VarChar, MSG_FROM);
			db.AddInParameter(dbCommand, "@MSG_TO", SqlDbType.VarChar, MSG_TO);
			db.AddInParameter(dbCommand, "@FILE_NAME", SqlDbType.NVarChar, FILE_NAME);
			db.AddInParameter(dbCommand, "@FILE_DATA", SqlDbType.NVarChar, FILE_DATA);
			db.AddInParameter(dbCommand, "@CREATE_TIME", SqlDbType.DateTime, CREATE_TIME.Year <= 1753 ? DBNull.Value : (object) CREATE_TIME);
			db.AddInParameter(dbCommand, "@STATUS", SqlDbType.Int, STATUS);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<MSG_FILE_INBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MSG_FILE_INBOX item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateMSG_FILE_INBOX(long id, string mSG_FROM, string mSG_TO, string fILE_NAME, string fILE_DATA, DateTime cREATE_TIME, int sTATUS, string gUIDSTR)
		{
			MSG_FILE_INBOX entity = new MSG_FILE_INBOX();			
			entity.ID = id;
			entity.MSG_FROM = mSG_FROM;
			entity.MSG_TO = mSG_TO;
			entity.FILE_NAME = fILE_NAME;
			entity.FILE_DATA = fILE_DATA;
			entity.CREATE_TIME = cREATE_TIME;
			entity.STATUS = sTATUS;
			entity.GUIDSTR = gUIDSTR;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_MSG_FILE_INBOX_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MSG_FROM", SqlDbType.VarChar, MSG_FROM);
			db.AddInParameter(dbCommand, "@MSG_TO", SqlDbType.VarChar, MSG_TO);
			db.AddInParameter(dbCommand, "@FILE_NAME", SqlDbType.NVarChar, FILE_NAME);
			db.AddInParameter(dbCommand, "@FILE_DATA", SqlDbType.NVarChar, FILE_DATA);
			db.AddInParameter(dbCommand, "@CREATE_TIME", SqlDbType.DateTime, CREATE_TIME.Year <= 1753 ? DBNull.Value : (object) CREATE_TIME);
			db.AddInParameter(dbCommand, "@STATUS", SqlDbType.Int, STATUS);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<MSG_FILE_INBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MSG_FILE_INBOX item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteMSG_FILE_INBOX(long id)
		{
			MSG_FILE_INBOX entity = new MSG_FILE_INBOX();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_MSG_FILE_INBOX_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_MSG_FILE_INBOX_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<MSG_FILE_INBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MSG_FILE_INBOX item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}