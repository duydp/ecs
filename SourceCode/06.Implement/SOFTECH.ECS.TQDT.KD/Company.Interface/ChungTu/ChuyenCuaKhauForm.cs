﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Janus.Windows.GridEX;

using Company.KD.BLL.KDT;
using Company.KD.BLL;

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class ChuyenCuaKhauForm : Company.Interface.BaseForm
    {
        public bool isKhaiBoSung = false;

        public ToKhaiMauDich TKMD;
        public DeNghiChuyenCuaKhau deNghiChuyen = new DeNghiChuyenCuaKhau();
        public List<DeNghiChuyenCuaKhau> ListDeNghiChuyen = new List<DeNghiChuyenCuaKhau>();
        public ChuyenCuaKhauForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            //cvError.Validate();
            //if (!cvError.IsValid)
            //    return;

            deNghiChuyen.DiaDiemKiemTra = txtDiaDiemKiemTra.Text.Trim();
            if (isKhaiBoSung)
                deNghiChuyen.LoaiKB = 1;
            else
                deNghiChuyen.LoaiKB = 0;

            deNghiChuyen.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            deNghiChuyen.NgayVanDon = ccNgayVanDon.Value;
            deNghiChuyen.SoVanDon = txtSoVanDon.Text;
            deNghiChuyen.ThoiGianDen = ccThoiHanDen.Value;
            deNghiChuyen.ThongTinKhac = txtThongTinKhac.Text.Trim();
            deNghiChuyen.TuyenDuong = txtTuyenDuong.Text.Trim();
            deNghiChuyen.TKMD_ID = TKMD.ID;
            if (string.IsNullOrEmpty(deNghiChuyen.GuidStr))
                deNghiChuyen.GuidStr = Guid.NewGuid().ToString();
            bool isUpdate = false;
            try
            {
                if (deNghiChuyen.ID == 0)
                    deNghiChuyen.Insert();
                else
                {
                    deNghiChuyen.Update();
                    isUpdate = true;
                }
                if (!isUpdate)
                    TKMD.listChuyenCuaKhau.Add(deNghiChuyen);
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.ID > 0)
            {
                deNghiChuyen.Delete();
                this.Close();
            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                bool thanhcong = deNghiChuyen.WSKhaiBaoChuyenCK(password, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK, TKMD.ID, TKMD.MaDoanhNghiep, ListDeNghiChuyen, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao);

                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }

            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Thông báo từ hệ thống ECS", "Chưa có thông tin phản hồi từ hệ thống thông quan điện tử.", false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                if (this.deNghiChuyen.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.deNghiChuyen.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = deNghiChuyen.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.deNghiChuyen.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungDeNghiChuyenCKThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.deNghiChuyen.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.deNghiChuyen.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.deNghiChuyen.NgayTiepNhan.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = deNghiChuyen.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.deNghiChuyen.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungDeNghiChuyenCKDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.deNghiChuyen.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    this.ShowMessage(message, false);

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form CHUYENCUAKHAU.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCHUYENCUAKHAU(Company.KD.BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau chuyenCuaKhau)
        {
            if (chuyenCuaKhau == null)
                return false;

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    status = true;

                    btnXoa.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXuLy.Enabled = true;
                    //Neu chua co so tiep nhan -> phai khai bao
                    if (chuyenCuaKhau.SoTiepNhan == 0)
                    {
                        btnKhaiBao.Enabled = true;
                        btnLayPhanHoi.Enabled = false;
                    }
                    //Neu da co so tiep nhan -> co the lay phan hoi
                    else
                    {
                        btnKhaiBao.Enabled = false;
                        btnLayPhanHoi.Enabled = true;
                    }
                }
            }

            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.GuidStr != null && deNghiChuyen.GuidStr != "")
                Globals.ShowKetQuaXuLyBoSung(deNghiChuyen.GuidStr);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        private void ChuyenCuaKhauForm_Load(object sender, EventArgs e)
        {
            //DATLMQ bổ sung Số Vận Đơn 17/01/2011
            if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("X"))
            {
                txtSoVanDon.Text = "";
                ccNgayVanDon.Enabled = true;
                ccNgayVanDon.Text = deNghiChuyen.NgayVanDon.ToShortDateString();
                ccThoiHanDen.Enabled = true;
                ccThoiHanDen.Text = deNghiChuyen.ThoiGianDen.ToShortDateString();
            }
            else
            {
                txtSoVanDon.Text = TKMD.SoVanDon;
                ccNgayVanDon.Text = TKMD.NgayVanDon.ToShortDateString();
                ccThoiHanDen.Text = TKMD.NgayDenPTVT.ToShortDateString();
            }
            txtDiaDiemKiemTra.Text = deNghiChuyen.DiaDiemKiemTra.Trim();
            txtTuyenDuong.Text = deNghiChuyen.TuyenDuong.Trim();
            txtThongTinKhac.Text = deNghiChuyen.ThongTinKhac.Trim();

            SetButtonStateCHUYENCUAKHAU(TKMD, isKhaiBoSung, deNghiChuyen);
        }
    }
}

