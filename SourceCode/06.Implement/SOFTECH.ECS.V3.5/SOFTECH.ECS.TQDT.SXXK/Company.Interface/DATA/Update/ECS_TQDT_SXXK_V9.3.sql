

/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]    Script Date: 06/28/2013 15:25:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]
GO


/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]    Script Date: 06/28/2013 15:25:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]
    @MaDoanhNghiep VARCHAR(14) ,
    @MaHaiQuan CHAR(6) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
IF(@MaHaiQuan IS not NULL AND @MaHaiQuan <> '')
BEGIN
    SELECT  A.MaHaiQuan ,
            A.SoToKhai ,
            A.MaLoaiHinh ,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan        
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'XSX%'
                  OR A.MaLoaiHinh LIKE 'XGC%'
                  OR A.MaLoaiHinh LIKE 'XCX%'
                ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL         
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   MaHaiQuan = @MaHaiQuan
                    AND MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy != 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh       
            
END
ELSE
	BEGIN
		  SELECT  A.MaHaiQuan ,
            A.SoToKhai ,
            A.MaLoaiHinh ,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        --WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan        
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            --AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'XSX%'
                  OR A.MaLoaiHinh LIKE 'XGC%'
                  OR A.MaLoaiHinh LIKE 'XCX%'
                ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL         
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   --MaHaiQuan = @MaHaiQuan AND
                     MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy != 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh       
	END 
        
/* Comment Script        
        
SELECT           
 A.MaHaiQuan,          
 A.SoToKhai,          
 A.MaLoaiHinh,          
 A.NamDangKy,          
 A.NgayDangKy,          
 A.NGAY_THN_THX          
FROM         
(        
 --HungTQ Updated 22/06/2011: Bo sung loc du lieu theo TrangThaiXuLy = 1        
 SELECT   DISTINCT        
  A.MaHaiQuan,         
  A.MaDoanhNghiep,         
  A.SoToKhai,          
  A.MaLoaiHinh,          
  A.NamDangKy,          
  A.NgayDangKy,          
  A.NGAY_THN_THX, tkmd.TrangThaiXuLy         
 FROM t_SXXK_ToKhaiMauDich A  LEFT JOIN  dbo.t_KDT_ToKhaiMauDich tkmd        
 ON a.SoToKhai = tkmd.SoToKhai AND a.MaLoaiHinh = tkmd.MaLoaiHinh AND a.NamDangKy = YEAR(tkmd.NgayDangKy) AND A.MaHaiQuan = tkmd.MaHaiQuan AND A.MaDoanhNghiep = tkmd.MaDoanhNghiep        
) A          
 LEFT JOIN (          
   SELECT DISTINCT          
    SoToKhai,          
    MaLoaiHinh,          
    NamDangKy,          
    MaHaiQuan          
   FROM t_KDT_SXXK_BKToKhaiXuat          
   WHERE          
    MaHaiQuan = @MaHaiQuan) D ON          
  A.SoToKhai=D.SoToKhai AND          
  A.MaLoaiHinh=D.MaLoaiHinh AND          
  A.NamDangKy=D.NamDangKy AND          
  A.MaHaiQuan=D.MaHaiQuan          
WHERE          
 A.MaDoanhNghiep = @MaDoanhNghiep AND          
 A.MaHaiQuan = @MaHaiQuan AND          
 (A.MaLoaiHinh LIKE 'XSX%' OR A.MaLoaiHinh LIKE 'XGC%')          
 AND A.NgayDangKy between @TuNgay and @DenNgay          
 AND (A.NGAY_THN_THX IS NOT NULL OR year(A.NGAY_THN_THX)!=1900 )          
 AND D.SoToKhai IS NULL           
 AND A.TrangThaiXuLy = 1        
           
ORDER BY A.NgayDangKy , A.SoToKhai, A.MaLoaiHinh          
*/          
          
          
          

GO




/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]    Script Date: 06/28/2013 15:27:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]
GO



/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]    Script Date: 06/28/2013 15:27:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]    
@MaDoanhNghiep VARCHAR (14),              
@MaHaiQuan char(6),              
@TuNgay datetime,              
@DenNgay datetime,              
@UserName varchar(50),              
@TenChuHang nvarchar(200)              
              
AS
IF (@MaHaiQuan IS NOT NULL AND @MaHaiQuan <> '')
BEGIN
SELECT               
  A.MaHaiQuan,              
     A.SoToKhai,              
  A.MaLoaiHinh,              
  A.NamDangKy,              
  A.NgayDangKy,              
  A.NGAY_THN_THX,     
   A.NgayHoanThanh 
   ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.      
FROM t_SXXK_ToKhaiMauDich A              
 LEFT JOIN (SELECT DISTINCT              
    SoToKhai,              
    MaLoaiHinh,              
    NamDangKy,              
    MaHaiQuan              
   FROM t_KDT_SXXK_BKToKhaiNhap              
   WHERE              
    UserName != @UserName) D ON              
  A.SoToKhai=D.SoToKhai AND              
  A.MaLoaiHinh=D.MaLoaiHinh AND              
  A.NamDangKy=D.NamDangKy AND              
  A.MaHaiQuan=D.MaHaiQuan
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
  LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)
              
WHERE              
 A.MaDoanhNghiep = @MaDoanhNghiep AND              
 A.MaHaiQuan = @MaHaiQuan AND              
-- D.SoToKhai IS NULL AND              
 A.MaLoaiHinh LIKE 'N%'AND              
 A.TenChuHang LIKE '%' + @TenChuHang + '%' AND              
 A.NgayDangKy between @TuNgay and @DenNgay AND               
 A.THANHLY <> 'H'     
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy.   
 AND CAST(a.SoToKhai AS VARCHAR(50)) + CAST(a.MaLoaiHinh AS VARCHAR(50)) + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (SELECT  CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50)) + CAST(YEAR(NgayDangKy) AS VARCHAR(50)) FROM dbo.t_KDT_ToKhaiMauDich WHERE MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep AND TrangThaiXuLy != 1 AND SoToKhai != 0)
ORDER BY A.NgayDangKy  DESC, A.SoToKhai, A.MaLoaiHinh
END
ELSE
	BEGIN
		SELECT               
  A.MaHaiQuan,              
     A.SoToKhai,              
  A.MaLoaiHinh,              
  A.NamDangKy,              
  A.NgayDangKy,              
  A.NGAY_THN_THX,     
   A.NgayHoanThanh 
   ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.      
FROM t_SXXK_ToKhaiMauDich A              
 LEFT JOIN (SELECT DISTINCT              
    SoToKhai,              
    MaLoaiHinh,              
    NamDangKy,              
    MaHaiQuan              
   FROM t_KDT_SXXK_BKToKhaiNhap              
   WHERE              
    UserName != @UserName) D ON              
  A.SoToKhai=D.SoToKhai AND              
  A.MaLoaiHinh=D.MaLoaiHinh AND              
  A.NamDangKy=D.NamDangKy              
  AND A.MaHaiQuan=D.MaHaiQuan
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
  LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy) AND a.MaHaiQuan = e.MaHaiQuan
              
WHERE              
 A.MaDoanhNghiep = @MaDoanhNghiep AND              
 --A.MaHaiQuan = @MaHaiQuan AND              
-- D.SoToKhai IS NULL AND              
 A.MaLoaiHinh LIKE 'N%'AND              
 A.TenChuHang LIKE '%' + @TenChuHang + '%' AND              
 A.NgayDangKy between @TuNgay and @DenNgay AND               
 A.THANHLY <> 'H'     
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy.   
 AND CAST(a.SoToKhai AS VARCHAR(50)) + CAST(a.MaLoaiHinh AS VARCHAR(50)) + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (SELECT  CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50)) + CAST(YEAR(NgayDangKy) AS VARCHAR(50)) FROM dbo.t_KDT_ToKhaiMauDich WHERE /*MaHaiQuan = @MaHaiQuan AND*/ MaDoanhNghiep = @MaDoanhNghiep AND TrangThaiXuLy != 1 AND SoToKhai != 0)
ORDER BY A.NgayDangKy  DESC, A.SoToKhai, A.MaLoaiHinh
	END
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '9.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('9.3',GETDATE(), N'Cập nhật thanh khoản nhiều chi cục')
END