﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

namespace SOFTECH.ECS.V3.Components.KD
{//AiNPV
    public partial class HuyToKhai
    {
        public static string ConfigHuyToKhai( ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)
        {
            string doc = "";
            string path = "../../../SOFTECH.ECS.V3.Components/XML/KD/HuyKhaiBao.xml";
            XDocument docXML = XDocument.Load(path);
            if (docXML != null)
            {
                XElement Root = docXML.Root;
                if (Root != null && tkmd != null)
                {
                    XElement AdditionalInformation = Root.Element(NodeName.AdditionalInformation);
                    IList<HuyToKhai> l_Htk = HuyToKhai.SelectCollectionBy_TKMD_ID(tkmd.ID);
                    if (l_Htk != null)
                    {
                        Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, status, declarationOffice);
                        foreach (XElement element in AdditionalInformation.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodeName.statement.ToUpper()))
                                element.SetValue("");//(Not Found)
                            if (element.Name.ToString().ToUpper().Equals(NodeName.content.ToUpper()))
                                element.SetValue(l_Htk[0].LyDoHuy);
                        }
                        Root.Add(AdditionalInformation);
                    }
                    else
                    {
                        return doc; //return "";
                    }
                }
                else
                {
                    return doc; //return "";
                }
            }
            //docXML.Save(@"D:\temp.xml");
            doc = docXML.ToString();
            return doc;
        }
    }
}