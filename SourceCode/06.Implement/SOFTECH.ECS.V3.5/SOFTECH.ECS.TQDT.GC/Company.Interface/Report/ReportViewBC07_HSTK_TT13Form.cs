﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC07_HSTK_TT13Form : BaseForm
    {
        public DataSet ds = new DataSet();
        private BangKe07_HSTK_TT13 BC07 = new BangKe07_HSTK_TT13();
        public HopDong HD = new HopDong();
        public DataSet dsBK = new DataSet();
        public DataSet dsBKX = new DataSet();
        public decimal luongTamNhap ;
        public decimal luongTaiXuat;
        public int luongTon;

        public ReportViewBC07_HSTK_TT13Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            this.BC07.HD = this.HD;
            foreach (DataRow drnTB in dsBK.Tables[0].Rows)
            {
                foreach (DataRow drxTB in dsBKX.Tables[0].Rows )
                {
                    if (drnTB["TenHang"].ToString() == drxTB["TenHang"].ToString())
                    {
                        luongTamNhap = Convert.ToDecimal(drnTB["SoLuongTamNhap"].ToString());
                        luongTaiXuat = Convert.ToDecimal(drxTB["SoLuongTaiXuat"].ToString());
                        luongTon = Convert.ToInt32(luongTamNhap - luongTaiXuat);
                        drnTB["SoLuongTaiXuat"] = luongTaiXuat;
                        drnTB["ConLai"] = luongTon.ToString();
                    }
                    continue;
                }
            }
            this.BC07.BindReport(dsBK.Tables[0]);
            printControl1.PrintingSystem = this.BC07.PrintingSystem;
            this.BC07.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }


    }
}