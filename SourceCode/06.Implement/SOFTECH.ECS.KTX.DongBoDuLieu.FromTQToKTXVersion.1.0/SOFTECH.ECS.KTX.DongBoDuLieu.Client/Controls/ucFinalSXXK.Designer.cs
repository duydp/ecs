﻿namespace DongBo.Controls
{
    partial class ucFinalSXXK
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkTiepTuc = new System.Windows.Forms.CheckBox();
            this.txtFinal = new System.Windows.Forms.RichTextBox();
            this.pnlBottom.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.chkTiepTuc);
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.txtFinal);
            // 
            // lblCaption
            // 
            this.lblCaption.Size = new System.Drawing.Size(362, 22);
            this.lblCaption.Text = "Đồng bộ dữ liệu doanh nghiệp thành công";
            // 
            // chkTiepTuc
            // 
            this.chkTiepTuc.AutoSize = true;
            this.chkTiepTuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTiepTuc.Location = new System.Drawing.Point(8, 17);
            this.chkTiepTuc.Name = "chkTiepTuc";
            this.chkTiepTuc.Size = new System.Drawing.Size(220, 17);
            this.chkTiepTuc.TabIndex = 0;
            this.chkTiepTuc.Text = "Tiếp tục đồng bộ cho phân hệ khác";
            this.chkTiepTuc.UseVisualStyleBackColor = true;
            this.chkTiepTuc.CheckedChanged += new System.EventHandler(this.chkTiepTuc_CheckedChanged);
            // 
            // txtFinal
            // 
            this.txtFinal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFinal.Location = new System.Drawing.Point(5, 5);
            this.txtFinal.Name = "txtFinal";
            this.txtFinal.Size = new System.Drawing.Size(478, 265);
            this.txtFinal.TabIndex = 0;
            this.txtFinal.Text = "";
            // 
            // ucFinalSXXK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ucFinalSXXK";
            this.Load += new System.EventHandler(this.ucFinalSXXK_Load);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlContent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkTiepTuc;
        private System.Windows.Forms.RichTextBox txtFinal;
    }
}
