using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class MaHS : BaseClass
    {
        public static bool Validate(string maHS, int numberOfCharacter)
        {
            if (maHS.Trim().Length > numberOfCharacter) return false;
            foreach (char c in maHS.ToCharArray())
            {
                if (!char.IsNumber(c)) return false;
            }
            return true;
        }
        public static DataTable SelectAll()
        {
            Database db = DatabaseFactory.CreateDatabase();
            string query = "SELECT * FROM t_HaiQuan_MaHS";
            DbCommand dbCommand =  db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static DataSet GetAllMaHS()
        {
            Database db = DatabaseFactory.CreateDatabase();
            string query = "SELECT * FROM t_HaiQuan_MaHS";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }

        public static string CheckExist(string maHS)
        {
            //Database db = DatabaseFactory.CreateDatabase();

            //string query = "";
            //if (maHS.Length == 10)
            //    query = "SELECT Mo_Ta FROM t_HaiQuan_MaHS WHERE HS10SO = '" + maHS + "'";
            //else
            //    query = "SELECT Mo_Ta FROM t_HaiQuan_MaHS WHERE HS10SO like '" + maHS + "%'";

            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //string t = "";
            //try
            //{
            //    t = System.Convert.ToString(db.ExecuteScalar(dbCommand));
            //}
            //catch
            //{ }
            //return t;

            try
            {
                string query = "";
                if (maHS.Length == 10)
                    query = string.Format("[HS10SO] = '{0}'", maHS);
                else
                    query = string.Format("[HS10SO] like '{0}%'", maHS);

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["MaHS"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["Mo_Ta"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(maHS, ex); }

            return string.Empty;
        }

        public static void Update(DataSet ds, string _nhom, string _pn1, string _pn2, string _pn3)
        {
            string insert = "Insert INTO t_HaiQuan_MaHS VALUES(@Nhom, @PN1, @PN2, @Pn3, @Mo_ta, @HS10SO,@DateCreated,@DateModified)";
            string update = "UPDATE t_HaiQuan_MaHS SET Nhom = @Nhom, PN1 = @PN1, PN2 = @PN2, Pn3 = @Pn3, Mo_ta=@Mo_ta, DateModified = @DateModified WHERE HS10SO = @HS10SO";
            string delete = "DELETE FROM t_HaiQuan_MaHS WHERE HS10SO = @HS10SO";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Nhom", SqlDbType.NVarChar, _nhom);
            db.AddInParameter(InsertCommand, "@PN1", SqlDbType.NVarChar, _pn1);
            db.AddInParameter(InsertCommand, "@PN2", SqlDbType.NVarChar, _pn2);
            db.AddInParameter(InsertCommand, "@Pn3", SqlDbType.NVarChar, _pn3);
            db.AddInParameter(InsertCommand, "@Mo_ta", SqlDbType.NVarChar, "Mo_ta", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@HS10SO", SqlDbType.NVarChar, "HS10SO", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Nhom", SqlDbType.NVarChar, _nhom);
            db.AddInParameter(UpdateCommand, "@PN1", SqlDbType.NVarChar, _pn1);
            db.AddInParameter(UpdateCommand, "@PN2", SqlDbType.NVarChar, _pn2);
            db.AddInParameter(UpdateCommand, "@Pn3", SqlDbType.NVarChar, _pn3);
            db.AddInParameter(UpdateCommand, "@Mo_ta", SqlDbType.NVarChar, "Mo_ta", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@HS10SO", SqlDbType.NVarChar, "HS10SO", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@HS10SO", SqlDbType.NVarChar, "HS10SO", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
    }
}