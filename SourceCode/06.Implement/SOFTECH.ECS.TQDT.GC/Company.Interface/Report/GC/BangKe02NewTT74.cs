﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe02NewTT74 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewBC02TT74Form report;
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;


        public BangKe02NewTT74()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt)
        {
            this.ReportHeader.Visible = this.First;
            this.lblNgayKi.Visible = this.Last;
            this.lblNgayKi1.Visible = this.Last;
            this.lblNgayKi2.Visible = this.Last;
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = dt;


            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }

            lblMathang.Text = loaiSP;
            lblAddThue.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;

            lblNPL1.Text = dt.Columns[2].Caption;
            lblNPL2.Text = dt.Columns[3].Caption;
            lblNPL3.Text = dt.Columns[4].Caption;
            lblNPL4.Text = dt.Columns[5].Caption;
            lblNPL5.Text = dt.Columns[6].Caption;
            //lblNPL6.Text = dt.Columns[6].Caption;
            lblTKNK.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[0].ColumnName);
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[1].ColumnName);
            lblLuong1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[2].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[3].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong3.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[4].ColumnName, "{0:N"+ GlobalSettings.SoThapPhan.LuongNPL +"}");
            lblLuong4.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[5].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong5.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[6].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongCong.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[7].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblNgayKi.Text = GlobalSettings.TieudeNgay;
            //decimal[] Tong = {0,0,0,0,0,0,0,0,0};

            //foreach (DataRow dr in dt.Rows)
            //{
            //    for (int i = 1; i <= 6; i++)
            //        if (dr[i].ToString() != "")
            //            Tong[i] += Decimal.Parse(dr[i].ToString());
            //}
            
            //lblTong1.Text = Tong[1].ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            //lblTong2.Text = Tong[2].ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            //lblTong3.Text = Tong[3].ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            //lblTong4.Text = Tong[4].ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            //lblTong5.Text = Tong[5].ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            //lblTong6.Text = Tong[6].ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);


        }

        private void lblLuong1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblLuong1.Text == null || lblLuong1.Text == "")
                lblLuong1.Text = "-";
        }

        private void lblLuong2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblLuong2.Text == null || lblLuong2.Text == "")
                lblLuong2.Text = "-";
        }

        private void lblLuong3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblLuong3.Text == null || lblLuong3.Text == "")
                lblLuong3.Text = "-";
        }

        private void lblLuong4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblLuong4.Text == null || lblLuong4.Text == "")
                lblLuong4.Text = "-";
        }

        private void lblLuong5_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblLuong5.Text == null || lblLuong5.Text == "")
                lblLuong5.Text = "-";
        }

        private void lblLuong6_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTongCong.Text == null || lblTongCong.Text == "")
                lblTongCong.Text = "-";
        }

        private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel label = (XRLabel)sender;
            report.label = label;
            report.txtName.Text = label.Text;
            report.lblName.Text = label.Tag.ToString();
        }
        public void setText(XRLabel label, string text)
        {
            label.Text = text;
        }

    }
}
