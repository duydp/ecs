using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmGroupEmployeeInfo2 : DevExpress.XtraEditors.XtraForm
    {
        private Employee Empl;
        public Boolean isEdit = false;

        public FrmGroupEmployeeInfo2(string ID)
        {
            InitializeComponent();
            if (ID == null)
            {
                txtEmplID.Text = Math.Abs(DateTime.Now.GetHashCode()).ToString();
                this.Text = "Th�m nh�n vi�n: " + txtEmplID.Text;
                lblCaption.Text = "Th�m nh�n vi�n: " + txtEmplID.Text;
                Empl = new Employee();
                this.Name = "Empl_New";
                this.isEdit = true;
            }
            else
            {
                txtEmplID.Text = ID;
                this.Text = "Nh�n vi�n: " + txtEmplID.Text;
                lblCaption.Text = "Nh�n vi�n: " + txtEmplID.Text;
                Empl = Employee.Load(ID);
                this.Name = "Empl_" + ID;
                this.FillData();
            }
        }

        #region Add Methods.
        private void GetData()
        {
            Empl.ID = txtEmplID.Text;
            Empl.FirstName = txtFirstName.Text;
            Empl.LastName = txtLastName.Text;
        }

        private void FillData()
        {
            txtEmplID.Text = Empl.ID;
            txtFirstName.Text = Empl.FirstName;
            txtLastName.Text = Empl.LastName;
        }

        private void DisableButton(Boolean val)
        {
            btnCancel.Enabled = val;
            btnSave.Enabled = val;
            groupEmplInfo.Enabled = val;
            btnEdit.Enabled = !val;
        }
        #endregion

        private void btnEdit_Click(object sender, EventArgs e)
        {
            this.isEdit = true;
            this.DisableButton(true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.GetData();
            if (this.Name == "Empl_New")
            {
                Empl.Insert();
                this.Text = "Nh�n vi�n: " + Empl.ID;
                lblCaption.Text = "Nh�n vi�n: " + txtEmplID.Text;
                this.Name = "Empl_" + Empl.ID;
            }
            else
            {
                Empl.Update();
            }
            this.DisableButton(false);

            if (ClassLib.IsFormShowed("FrmEmployeeManager"))
            {
                FrmEmployeeManager obj = (FrmEmployeeManager)Application.OpenForms["FrmEmployeeManager"];
                obj.RefreshData();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.Name == "Empl_New")
            {
                this.Close();
            }
            else
            {
                this.FillData();
                this.DisableButton(false);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmEmployeeInfo_Load(object sender, EventArgs e)
        {
            this.DisableButton(this.isEdit);
        }

    }
}