﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL.SXXK;

namespace Company.Interface.KDT.GC
{
    partial class ThietBiGCBoSungForm
    {
        private UIGroupBox uiGroupBox1;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private EditBox txtMa;
        private EditBox txtTen;
        private UIGroupBox uiGroupBox2;
        private UIComboBox cbDonViTinh;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgThietBi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgThietBi_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThietBiGCBoSungForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgThietBi = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMaTB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNuoc = new Company.Interface.Controls.NuocHControl();
            this.ctrNguyenTe = new Company.Interface.Controls.NguyenTeControl();
            this.lblCu = new System.Windows.Forms.Label();
            this.lblTenCu = new System.Windows.Forms.Label();
            this.txtMaCu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chTinhTrang = new Janus.Windows.EditControls.UICheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.bntAddExcel = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDonGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbDonViTinh = new Janus.Windows.EditControls.UIComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTen = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoLuong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvdongia = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvtrigia = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rvDonGia = new Company.Controls.CustomValidation.RangeValidator();
            this.rvSoLuong = new Company.Controls.CustomValidation.RangeValidator();
            this.revMaHS = new Company.Controls.CustomValidation.RegularExpressionValidator();
            this.label18 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvdongia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvtrigia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvDonGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.revMaHS)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 578), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 578);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 554);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 554);
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(927, 578);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.dgThietBi);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(203, 3);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(927, 578);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // dgThietBi
            // 
            this.dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThietBi.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThietBi.AlternatingColors = true;
            this.dgThietBi.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgThietBi.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThietBi.ColumnAutoResize = true;
            dgThietBi_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgThietBi_DesignTimeLayout_Reference_0.Instance")));
            dgThietBi_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgThietBi_DesignTimeLayout_Reference_0});
            dgThietBi_DesignTimeLayout.LayoutString = resources.GetString("dgThietBi_DesignTimeLayout.LayoutString");
            this.dgThietBi.DesignTimeLayout = dgThietBi_DesignTimeLayout;
            this.dgThietBi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThietBi.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThietBi.GroupByBoxVisible = false;
            this.dgThietBi.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThietBi.ImageList = this.ImageList1;
            this.dgThietBi.Location = new System.Drawing.Point(0, 351);
            this.dgThietBi.Name = "dgThietBi";
            this.dgThietBi.RecordNavigator = true;
            this.dgThietBi.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgThietBi.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThietBi.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgThietBi.Size = new System.Drawing.Size(927, 182);
            this.dgThietBi.TabIndex = 288;
            this.dgThietBi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgThietBi.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgThietBi_RowDoubleClick);
            this.dgThietBi.SelectionChanged += new System.EventHandler(this.dgThietBi_SelectionChanged);
            this.dgThietBi.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgThietBi_DeletingRecords);
            this.dgThietBi.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgThietBi_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnSearch);
            this.uiGroupBox6.Controls.Add(this.label16);
            this.uiGroupBox6.Controls.Add(this.txtMaTB);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 305);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(927, 46);
            this.uiGroupBox6.TabIndex = 287;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(399, 16);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(109, 23);
            this.btnSearch.TabIndex = 87;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(21, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 86;
            this.label16.Text = "Mã TB";
            // 
            // txtMaTB
            // 
            this.txtMaTB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaTB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaTB.BackColor = System.Drawing.Color.White;
            this.txtMaTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaTB.Location = new System.Drawing.Point(104, 17);
            this.txtMaTB.Name = "txtMaTB";
            this.txtMaTB.Size = new System.Drawing.Size(280, 21);
            this.txtMaTB.TabIndex = 85;
            this.txtMaTB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaTB.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnExportExcel);
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnXoa);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 533);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(927, 45);
            this.uiGroupBox3.TabIndex = 8;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(12, 15);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(90, 23);
            this.btnExportExcel.TabIndex = 71;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(840, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 17;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(759, 15);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 16;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label18);
            this.uiGroupBox2.Controls.Add(this.ctrNuoc);
            this.uiGroupBox2.Controls.Add(this.ctrNguyenTe);
            this.uiGroupBox2.Controls.Add(this.lblCu);
            this.uiGroupBox2.Controls.Add(this.lblTenCu);
            this.uiGroupBox2.Controls.Add(this.txtMaCu);
            this.uiGroupBox2.Controls.Add(this.chTinhTrang);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.label17);
            this.uiGroupBox2.Controls.Add(this.label19);
            this.uiGroupBox2.Controls.Add(this.label20);
            this.uiGroupBox2.Controls.Add(this.label15);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.bntAddExcel);
            this.uiGroupBox2.Controls.Add(this.btnAdd);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.txtTriGia);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.txtDonGia);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtSoLuong);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.txtMaHS);
            this.uiGroupBox2.Controls.Add(this.cbDonViTinh);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtMa);
            this.uiGroupBox2.Controls.Add(this.txtTen);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(927, 305);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            this.uiGroupBox2.Click += new System.EventHandler(this.uiGroupBox2_Click);
            // 
            // ctrNuoc
            // 
            this.ctrNuoc.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuoc.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrNuoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuoc.Location = new System.Drawing.Point(107, 213);
            this.ctrNuoc.Ma = "";
            this.ctrNuoc.Name = "ctrNuoc";
            this.ctrNuoc.ReadOnly = false;
            this.ctrNuoc.Size = new System.Drawing.Size(245, 22);
            this.ctrNuoc.TabIndex = 12;
            this.ctrNuoc.VisualStyleManager = null;
            // 
            // ctrNguyenTe
            // 
            this.ctrNguyenTe.BackColor = System.Drawing.Color.Transparent;
            this.ctrNguyenTe.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.ctrNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNguyenTe.Location = new System.Drawing.Point(653, 179);
            this.ctrNguyenTe.Ma = "";
            this.ctrNguyenTe.Name = "ctrNguyenTe";
            this.ctrNguyenTe.ReadOnly = false;
            this.ctrNguyenTe.Size = new System.Drawing.Size(262, 22);
            this.ctrNguyenTe.TabIndex = 10;
            this.ctrNguyenTe.VisualStyleManager = null;
            // 
            // lblCu
            // 
            this.lblCu.AutoSize = true;
            this.lblCu.BackColor = System.Drawing.Color.Transparent;
            this.lblCu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCu.ForeColor = System.Drawing.Color.Red;
            this.lblCu.Location = new System.Drawing.Point(520, 26);
            this.lblCu.Name = "lblCu";
            this.lblCu.Size = new System.Drawing.Size(14, 14);
            this.lblCu.TabIndex = 0;
            this.lblCu.Text = "*";
            // 
            // lblTenCu
            // 
            this.lblTenCu.AutoSize = true;
            this.lblTenCu.BackColor = System.Drawing.Color.Transparent;
            this.lblTenCu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCu.Location = new System.Drawing.Point(20, 23);
            this.lblTenCu.Name = "lblTenCu";
            this.lblTenCu.Size = new System.Drawing.Size(82, 14);
            this.lblTenCu.TabIndex = 0;
            this.lblTenCu.Text = "Mã thiết bị cũ";
            // 
            // txtMaCu
            // 
            this.txtMaCu.BackColor = System.Drawing.Color.White;
            this.txtMaCu.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaCu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaCu.Location = new System.Drawing.Point(108, 18);
            this.txtMaCu.MaxLength = 50;
            this.txtMaCu.Name = "txtMaCu";
            this.txtMaCu.Size = new System.Drawing.Size(405, 22);
            this.txtMaCu.TabIndex = 1;
            this.txtMaCu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaCu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaCu.ButtonClick += new System.EventHandler(this.btnChon_Click);
            // 
            // chTinhTrang
            // 
            this.chTinhTrang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chTinhTrang.Location = new System.Drawing.Point(651, 213);
            this.chTinhTrang.Name = "chTinhTrang";
            this.chTinhTrang.Size = new System.Drawing.Size(244, 20);
            this.chTinhTrang.TabIndex = 11;
            this.chTinhTrang.Text = "Còn mới/Đã cũ";
            this.chTinhTrang.ToolTipText = "Nếu chọn là tình trạng đã cũ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(542, 216);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 14);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tình trạng";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(901, 147);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(14, 14);
            this.label17.TabIndex = 0;
            this.label17.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(901, 117);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(14, 14);
            this.label19.TabIndex = 0;
            this.label19.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(901, 90);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(14, 14);
            this.label20.TabIndex = 0;
            this.label20.Text = "*";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(361, 219);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 14);
            this.label15.TabIndex = 0;
            this.label15.Text = "*";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(361, 184);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 14);
            this.label14.TabIndex = 0;
            this.label14.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(361, 150);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(520, 105);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 14);
            this.label12.TabIndex = 0;
            this.label12.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(519, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 14);
            this.label11.TabIndex = 0;
            this.label11.Text = "*";
            // 
            // bntAddExcel
            // 
            this.bntAddExcel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntAddExcel.Image = ((System.Drawing.Image)(resources.GetObject("bntAddExcel.Image")));
            this.bntAddExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.bntAddExcel.Location = new System.Drawing.Point(206, 251);
            this.bntAddExcel.Name = "bntAddExcel";
            this.bntAddExcel.Size = new System.Drawing.Size(120, 22);
            this.bntAddExcel.TabIndex = 14;
            this.bntAddExcel.Text = "Thêm từ Excel";
            this.bntAddExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.bntAddExcel.Click += new System.EventHandler(this.bntAddExcel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAdd.Location = new System.Drawing.Point(108, 250);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(92, 23);
            this.btnAdd.TabIndex = 15;
            this.btnAdd.Text = "Ghi";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(542, 183);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 14);
            this.label9.TabIndex = 0;
            this.label9.Text = "Nguyên tệ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "Xuất xứ";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // txtTriGia
            // 
            this.txtTriGia.BackColor = System.Drawing.Color.White;
            this.txtTriGia.DecimalDigits = 3;
            this.txtTriGia.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGia.Location = new System.Drawing.Point(653, 142);
            this.txtTriGia.Name = "txtTriGia";
            this.txtTriGia.ReadOnly = true;
            this.txtTriGia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTriGia.Size = new System.Drawing.Size(242, 22);
            this.txtTriGia.TabIndex = 9;
            this.txtTriGia.Text = "0.000";
            this.txtTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(542, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Trị giá";
            // 
            // txtDonGia
            // 
            this.txtDonGia.BackColor = System.Drawing.Color.White;
            this.txtDonGia.DecimalDigits = 3;
            this.txtDonGia.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGia.Location = new System.Drawing.Point(653, 113);
            this.txtDonGia.Name = "txtDonGia";
            this.txtDonGia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDonGia.Size = new System.Drawing.Size(242, 22);
            this.txtDonGia.TabIndex = 8;
            this.txtDonGia.Text = "0.000";
            this.txtDonGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtDonGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtDonGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDonGia.Leave += new System.EventHandler(this.txtTriGia_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(542, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Đơn giá";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.BackColor = System.Drawing.Color.White;
            this.txtSoLuong.DecimalDigits = 3;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.Location = new System.Drawing.Point(653, 85);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSoLuong.Size = new System.Drawing.Size(242, 22);
            this.txtSoLuong.TabIndex = 7;
            this.txtSoLuong.Text = "0.000";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuong.Leave += new System.EventHandler(this.txtTriGia_Leave);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(542, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số lượng đăng ký";
            // 
            // txtMaHS
            // 
            this.txtMaHS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHS.BackColor = System.Drawing.Color.White;
            this.txtMaHS.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHS.Location = new System.Drawing.Point(108, 146);
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.Size = new System.Drawing.Size(244, 22);
            this.txtMaHS.TabIndex = 4;
            this.txtMaHS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHS.Leave += new System.EventHandler(this.txtMaHS_Leave);
            // 
            // cbDonViTinh
            // 
            this.cbDonViTinh.BackColor = System.Drawing.Color.White;
            this.cbDonViTinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDonViTinh.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDonViTinh.Location = new System.Drawing.Point(108, 179);
            this.cbDonViTinh.Name = "cbDonViTinh";
            this.cbDonViTinh.Size = new System.Drawing.Size(244, 22);
            this.cbDonViTinh.TabIndex = 5;
            this.cbDonViTinh.ValueMember = "ID";
            this.cbDonViTinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mã HS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên thiết bị";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã thiết bị";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "Đơn vị tính";
            // 
            // txtMa
            // 
            this.txtMa.BackColor = System.Drawing.Color.White;
            this.txtMa.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMa.Location = new System.Drawing.Point(108, 50);
            this.txtMa.MaxLength = 50;
            this.txtMa.Name = "txtMa";
            this.txtMa.Size = new System.Drawing.Size(405, 22);
            this.txtMa.TabIndex = 2;
            this.txtMa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMa.ButtonClick += new System.EventHandler(this.btnChon_Click);
            // 
            // txtTen
            // 
            this.txtTen.BackColor = System.Drawing.Color.White;
            this.txtTen.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.Location = new System.Drawing.Point(108, 85);
            this.txtTen.MaxLength = 254;
            this.txtTen.Multiline = true;
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(405, 50);
            this.txtTen.TabIndex = 3;
            this.txtTen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMa;
            this.rfvMa.ErrorMessage = "\"Mã thiết bị\" bắt buộc phải nhập.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.txtTen;
            this.rfvTen.ErrorMessage = "\"Tên thiết bị\" bắt buộc phải nhập.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            this.rfvTen.Tag = "rfvTen";
            // 
            // rfvDVT
            // 
            this.rfvDVT.ControlToValidate = this.cbDonViTinh;
            this.rfvDVT.ErrorMessage = "\"Đơn vị tính\" bắt buộc phải chọn.";
            this.rfvDVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDVT.Icon")));
            this.rfvDVT.Tag = "rfvDVT";
            // 
            // error
            // 
            this.error.ContainerControl = this;
            this.error.Icon = ((System.Drawing.Icon)(resources.GetObject("error.Icon")));
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMaHS;
            this.rfvMaHS.ErrorMessage = "\"Mã HS\" bắt buộc phải nhập.";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // rfvSoLuong
            // 
            this.rfvSoLuong.ControlToValidate = this.txtSoLuong;
            this.rfvSoLuong.ErrorMessage = "\"Số lượng đắng ký\" bắt buộc phải chọn.";
            this.rfvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoLuong.Icon")));
            this.rfvSoLuong.Tag = "rfvSoLuong";
            // 
            // rfvdongia
            // 
            this.rfvdongia.ControlToValidate = this.txtDonGia;
            this.rfvdongia.ErrorMessage = "\"Đơn giá\" bắt buộc phải chọn.";
            this.rfvdongia.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvdongia.Icon")));
            this.rfvdongia.Tag = "rfvdongia";
            // 
            // rfvtrigia
            // 
            this.rfvtrigia.ControlToValidate = this.txtTriGia;
            this.rfvtrigia.ErrorMessage = "Giá trị không hợp lệ";
            this.rfvtrigia.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvtrigia.Icon")));
            this.rfvtrigia.Tag = "rfvtrigia";
            // 
            // rvDonGia
            // 
            this.rvDonGia.ControlToValidate = this.txtDonGia;
            this.rvDonGia.ErrorMessage = "\"Đơn giá \" không hợp lệ";
            this.rvDonGia.Icon = ((System.Drawing.Icon)(resources.GetObject("rvDonGia.Icon")));
            this.rvDonGia.MaximumValue = "10000000000";
            this.rvDonGia.MinimumValue = "1";
            this.rvDonGia.Tag = "rvDonGia";
            this.rvDonGia.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvSoLuong
            // 
            this.rvSoLuong.ControlToValidate = this.txtSoLuong;
            this.rvSoLuong.ErrorMessage = "\"Số lượng \" không hợp lệ";
            this.rvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rvSoLuong.Icon")));
            this.rvSoLuong.MaximumValue = "10000000000";
            this.rvSoLuong.MinimumValue = "1";
            this.rvSoLuong.Tag = "rvSoLuong";
            this.rvSoLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // revMaHS
            // 
            this.revMaHS.ControlToValidate = this.txtMaHS;
            this.revMaHS.ErrorMessage = "\"Mã HS\" không hợp lệ.";
            this.revMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("revMaHS.Icon")));
            this.revMaHS.Tag = "revMaHS";
            this.revMaHS.ValidationExpression = "\\d{8,12}";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(20, 283);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(760, 14);
            this.label18.TabIndex = 16;
            this.label18.Text = " Lưu ý Mã thiết bị không nên sử dụng các ký tự này để tránh các lỗi phát sinh sau" +
                " này : ! \" # % & ` + \' @ : ; < = > ? $ [ ] ^";
            // 
            // ThietBiGCBoSungForm
            // 
            this.AcceptButton = this.btnAdd;
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(1133, 584);
            this.Controls.Add(this.uiGroupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ThietBiGCBoSungForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin thiết bị gia công";
            this.Load += new System.EventHandler(this.ThietBiGCEditForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ThietBiGCBoSungForm_FormClosing);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvdongia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvtrigia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvDonGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.revMaHS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDVT;
        private UIButton btnClose;
        private EditBox txtMaHS;
        private IContainer components;
        private ErrorProvider error;
        private Label label5;
        private NumericEditBox txtSoLuong;
        private UIButton btnAdd;
        private Label label6;
        private Label label7;
        private NumericEditBox txtDonGia;
        private Label label9;
        private Label label8;
        private NumericEditBox txtTriGia;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoLuong;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvdongia;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvtrigia;
        private ImageList ImageList1;
        private Label label17;
        private Label label19;
        private Label label20;
        private Label label15;
        private Label label14;
        private Label label13;
        private Label label12;
        private Label label11;
        private UIButton btnXoa;
        private Company.Controls.CustomValidation.RangeValidator rvDonGia;
        private Company.Controls.CustomValidation.RangeValidator rvSoLuong;
        private Company.Controls.CustomValidation.RegularExpressionValidator revMaHS;
        private UICheckBox chTinhTrang;
        private Label label10;
        private Label lblCu;
        private Label lblTenCu;
        private EditBox txtMaCu;
        private UIButton bntAddExcel;
        private Company.Interface.Controls.NguyenTeControl ctrNguyenTe;
        private Company.Interface.Controls.NuocHControl ctrNuoc;
        private UIGroupBox uiGroupBox3;
        private UIButton btnExportExcel;
        private Janus.Windows.GridEX.GridEX dgThietBi;
        private UIGroupBox uiGroupBox6;
        private UIButton btnSearch;
        private Label label16;
        private EditBox txtMaTB;
        private Label label18;
    }
}
