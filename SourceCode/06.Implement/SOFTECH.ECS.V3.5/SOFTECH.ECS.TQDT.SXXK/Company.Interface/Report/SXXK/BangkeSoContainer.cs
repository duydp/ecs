﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT;

namespace Company.Interface.Report.SXXK
{
    public partial class BangkeSoContainer : DevExpress.XtraReports.UI.XtraReport
    {
        public string phieu;
        public string soTN;
        public string ngayTN;
        public string maHaiQuan;
        int STT = 0;
        public ToKhaiMauDich tkmd = new ToKhaiMauDich();

        public BangkeSoContainer()
        {
            InitializeComponent();
        }
        
        public void BindReport()
        {
            DataTable dt = tkmd.GetContainerInfo(tkmd.ID);
            this.DataSource = dt;
            lblnguoikhaihq.Text = ":" + GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (tkmd.NgayDangKy.Year > 1900)
                lblNgayKy.Text = tkmd.NgayDangKy.ToString("dd/MM/yyy");
            else
                lblNgayKy.Text = "";
            if (tkmd.SoToKhai > 0)
                lblSoToKhai.Text = tkmd.SoToKhai + "";
            else
                lblSoToKhai.Text = "";
            lblHieuKien.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoHieu");
            lblSoseal.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Seal_No");
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

    }
}
