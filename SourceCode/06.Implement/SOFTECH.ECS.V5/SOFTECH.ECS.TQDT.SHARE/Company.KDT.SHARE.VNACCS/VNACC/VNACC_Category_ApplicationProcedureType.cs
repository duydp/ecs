using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_ApplicationProcedureType
    {
        protected static List<VNACC_Category_ApplicationProcedureType> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_ApplicationProcedureType> collection = new List<VNACC_Category_ApplicationProcedureType>();
            while (reader.Read())
            {
                VNACC_Category_ApplicationProcedureType entity = new VNACC_Category_ApplicationProcedureType();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ApplicationProcedureType"))) entity.ApplicationProcedureType = reader.GetString(reader.GetOrdinal("ApplicationProcedureType"));
                if (!reader.IsDBNull(reader.GetOrdinal("ApplicationProcedureName"))) entity.ApplicationProcedureName = reader.GetString(reader.GetOrdinal("ApplicationProcedureName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_ApplicationProcedureType> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [ApplicationProcedureType], [ApplicationProcedureName] FROM [dbo].[t_VNACC_Category_ApplicationProcedureType]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_ApplicationProcedureType> SelectCollectionBy(List<VNACC_Category_ApplicationProcedureType> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_ApplicationProcedureType o)
            {
                return o.TableID.Contains(tableID);
            });
        }
    }
}