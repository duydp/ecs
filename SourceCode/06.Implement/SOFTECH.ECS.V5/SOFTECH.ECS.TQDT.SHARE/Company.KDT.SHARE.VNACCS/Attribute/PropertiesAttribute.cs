﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;


namespace Company.KDT.SHARE.VNACCS
{
   public  class PropertiesAttribute
    {
        public Type OfType { get; set; }
        public int Digit { get; set; }
        public int Repetition { get; set; }
        public string GroupID { get; set; }
        public object Value { get; set; }
        public List<object> ListValue { get; set; }
        public int decimalNumber { get; set; }
        public bool isRequired { get; set; }
        public PropertiesAttribute(Type type, int digit, int repetition, string loopID, object value)
        {
            this.OfType = type;
            this.Digit = digit;
            this.Repetition = repetition;
            this.GroupID = loopID;
            this.Value = value;
        }
        public PropertiesAttribute(int digit, int repetition, string loopID, Type type)
        {
            this.Digit = digit;
            this.Repetition = repetition;
            this.GroupID = loopID;
            this.OfType = type;
            this.ListValue = new List<object>();
            for (int i = 0; i < Repetition; i++ )
            {
                ListValue.Add("");
            }
            this.decimalNumber = 0;
        }
        public PropertiesAttribute(int digit, int repetition, string loopID, Type type , int DecimalNo)
        {
            this.Digit = digit;
            this.Repetition = repetition;
            this.GroupID = loopID;
            this.OfType = type;
            this.ListValue = new List<object>();
            for (int i = 0; i < Repetition; i++)
            {
                ListValue.Add(new object());
            }
            this.decimalNumber = DecimalNo;
        }
        public PropertiesAttribute (int digit, Type type)
        {
            this.Digit = digit;
            this.Repetition = 0;
            this.OfType = type;
            this.decimalNumber = 0;
            this.GroupID = EnumGroupID.None;
        }
       public PropertiesAttribute (int digit, Type type, int decimalNo)
        {
            this.Digit = digit;
            this.Repetition = 0;
            this.OfType = type;
            this.decimalNumber = decimalNo;
            this.GroupID = EnumGroupID.None;
            
        }
       /// <summary>
       /// default :  Typeof(string)
       /// </summary>
       /// <param name="digit"></param>
        public PropertiesAttribute(int digit)
        {
            this.Digit = digit;
            this.Repetition = 0;
            this.OfType = typeof(string);
            this.decimalNumber = 0;
            this.GroupID = EnumGroupID.None;
        }
        public void SetValue( object value )
        {
            SetValue(value, false);
        }
        public void SetValue(object value, bool isRequired)
        {
            this.Value = value;
            this.isRequired = isRequired;
        }
        public object GetValue()
        {
            return GetValue(true);
        }
        public object GetValue(bool isTrim)
        {
            try
            {
                if (this.OfType == typeof(DateTime))
                {
                    if (this.Value != null && !string.IsNullOrEmpty(this.Value.ToString().Trim()))
                    {
                        return DateTime.ParseExact(this.Value.ToString(), "ddMMyyyy", null);
                    }
                    else
                        return new DateTime(1900,1,1);
                }
                else if (this.OfType == typeof(decimal) || this.OfType == typeof(int))
                {
                    if (Value == null || string.IsNullOrEmpty(Value.ToString().Trim()))
                        return 0;
                    else
                        return Value;
                }
                else
                {
                    if (Value == null)
                        return string.Empty;
                    else
                    {
                        if (isTrim)
                            return Value.ToString().TrimEnd();
                        else
                            return Value.ToString();
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public object GetValueCollection(int index)
        {
            if (this.ListValue != null && this.ListValue.Count > 0 && this.ListValue.Count -1 >= index)
            {
                object t = this.ListValue[index];
                try
                {
                    if (this.OfType == typeof(DateTime))
                    {
                        if (t != null && !string.IsNullOrEmpty(t.ToString().Trim()))
                        {
                            return DateTime.ParseExact(t.ToString(), "ddMMyyyy", null);
                        }
                        else
                            return new DateTime(1900,1,1);
                    }
                    else if (this.OfType == typeof(decimal) || this.OfType == typeof(int))
                    {
                        if (t == null || string.IsNullOrEmpty(t.ToString().Trim()))
                            return 0;
                        else
                            return t;
                    }
                    else
                    {
                        if (t == null || string.IsNullOrEmpty(t.ToString().Trim()))
                            return string.Empty;
                        else
                            return t.ToString().TrimEnd();
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
            return null;
        }
        public void AddListValue(object value)
        {
            ListValue.Add(value);

        }
        public void SetValueList(object value, int index)
        {
            SetValueList(value, index, false);

        }
        public void SetValueList(object value, int index , bool isRequired)
        {
            if (this.ListValue.Count <= index)
                this.AddListValue(value);
            else
                this.ListValue[index] = value;
            this.isRequired = isRequired;

        }
        public StringBuilder ToEDI(StringBuilder Edi, bool AppendLine)
        {
            
            StringBuilder EdiProperties = Edi;
            try
            {
            
            if (this.OfType == typeof(int))
            {
                EdiProperties.Append(HelperVNACCS.FormatNumber(this.Value, this.Digit, 0, this.isRequired));
            }
            else if (this.OfType == typeof(double) || this.OfType == typeof(decimal))
            {
                EdiProperties.Append(HelperVNACCS.FormatNumber(this.Value, this.Digit, this.decimalNumber, this.isRequired));
            }
            else if (this.OfType == typeof(string))
            {
                EdiProperties.Append(HelperVNACCS.FormatString(this.Value, this.Digit));
                
            }
            else if(this.OfType == typeof(DateTime))
            {
                EdiProperties.Append(HelperVNACCS.FormatDateTime(this.Value));
            }
            if (AppendLine)
                EdiProperties.AppendLine();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
            return EdiProperties;
        }
        public StringBuilder ToEDI(StringBuilder Edi, object value, bool AppendLine)
        {
            StringBuilder EdiProperties = Edi;
            try
            {
            if (this.OfType == typeof(int))
            {
                if (this.GroupID == "AMA_HANGHOA_MB_" || this.GroupID == "AMA_HANGHOA_MQ_")
                {
                    if (value.ToString().Trim()=="0")
                    {
                        this.isRequired = true;   
                    }
                }
                EdiProperties.Append(HelperVNACCS.FormatNumber(value, this.Digit, 0, this.isRequired));
            }
            else if (this.OfType == typeof(double) || this.OfType == typeof(decimal))
            {
                EdiProperties.Append(HelperVNACCS.FormatNumber(value, this.Digit, this.decimalNumber, this.isRequired));
            }
            else if (this.OfType == typeof(string))
            {
                EdiProperties.Append(HelperVNACCS.FormatString(value, this.Digit));
            }
            else if (this.OfType == typeof(DateTime))
            {
                EdiProperties.Append(HelperVNACCS.FormatDateTime(value));
            }
            if (AppendLine)
                EdiProperties.AppendLine();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
            return EdiProperties;
        }
    }
}
