﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Company.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Controls;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.GridEX;
using Janus.Windows.UI.Dock;
using Janus.Windows.ExplorerBar;
using System;
using Janus.Windows.FilterEditor;
using Janus.Windows.EditControls;
using Janus.Windows.UI.StatusBar;
using Company.Controls.CustomValidation;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class BaseForm : Form
    {
        public string MaDoanhNghiep;
        public string MaHaiQuan;
        public OpenFormType OpenType;
        public string CalledForm = string.Empty;
        public int langCheck = 0;

        protected MessageBoxControl _MsgBox;
        protected DataTable _DonViTinh;
        protected DataTable _DonViHaiQuan;
        protected DataTable _LoaiHinhMauDich;
        protected DataTable _NguyenTe;
        protected DataTable _Nuoc;
        protected DataTable _PhuongThucThanhToan;
        protected DataTable _PhuongThucVanTai;
        protected DataTable _DieuKienGiaoHang;
        protected DataTable _NhomSanPham;
        protected DataTable _LoaiPhuKien;
        protected DataTable _LoaiCO;
        private IContainer components = null;
        ResourceManager resource = null;

        protected KDTMessageBoxControl _KDTMsgBox;

        public string ShowMessageTQDT(string messageHQ, string messageContent, bool showYesNoButton)
        {
            this._KDTMsgBox = new KDTMessageBoxControl();
            this._KDTMsgBox.ShowYesNoButton = showYesNoButton;
            this._KDTMsgBox.HQMessageString = messageHQ;
            this._KDTMsgBox.MessageString = messageContent;
            this._KDTMsgBox.ShowDialog(this);
            string st = this._KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        
        public string ShowMessageTQDT(string messageContent, long MessageID)
        {
            string messageHQ = "Thông báo trả về từ hệ thống Hải quan";
            this._KDTMsgBox = new KDTMessageBoxControl();
            this._KDTMsgBox.ShowYesNoButton = false;
            this._KDTMsgBox.MessageID = MessageID;
            this._KDTMsgBox.HQMessageString = messageHQ;
            this._KDTMsgBox.MessageString = messageContent;
            this._KDTMsgBox.ShowDialog(this);
            string st = this._KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        public string ShowMessageTQDT(string messageContent, bool showYesNoButton)
        {
            string messageHQ = "Thông báo trả về từ hệ thống Hải quan";
            this._KDTMsgBox = new KDTMessageBoxControl();
            this._KDTMsgBox.ShowYesNoButton = showYesNoButton;
            this._KDTMsgBox.HQMessageString = messageHQ;
            this._KDTMsgBox.MessageString = messageContent;
            this._KDTMsgBox.ShowDialog(this);
            string st = this._KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            string st = string.Empty;
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        public string ShowMessage(string message, bool showYesNoButton, bool showErrorButton, string exceptionString)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowErrorButton = showErrorButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.exceptionString = exceptionString;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }

        public string Message(string key, string value, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            string message = "";
            if (resource.GetString(key) != null && resource.GetString(key).Trim() != "")
            {
                // resource asign in InitCulture() 
                message = resource.GetString(key);
                message = message.Replace("$", value);
            }
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }

        public string MLMessages(string msg, string key, string value, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            string message = msg;
            string st = "";
            if (GlobalSettings.NGON_NGU == "0")
            {
                this._MsgBox.MessageString = msg;
                this._MsgBox.ShowDialog(this);
                st = this._MsgBox.ReturnValue;

            }
            else if (GlobalSettings.NGON_NGU == "1")
            {
                if (resource.GetString(key) != null && resource.GetString(key).Trim() != "")
                {
                    // resource asign in InitCulture() 
                    message = resource.GetString(key);
                    message = message.Replace("$", value);
                }
                this._MsgBox.MessageString = message;
                this._MsgBox.ShowDialog(this);
                st = this._MsgBox.ReturnValue;
            }

            return st;
        }

        protected override void OnLoad(System.EventArgs e)
        {
            this.helpProvider1.SetHelpKeyword(this, this.Name + ".htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);

            this.helpProvider1.SetShowHelp(this, true);
            this.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            this.MaDoanhNghiep = GlobalSettings.MA_DON_VI;

            //string strResource = "";
            //try
            //{
            //    strResource = Application.StartupPath + "\\Resources\\Language.resx";
            //}
            //catch { }
            if (Thread.CurrentThread.CurrentCulture.Equals(new CultureInfo("en-US")))
            {

                this.InitCulture("en-US", "Company.Interface.Resources.Language");
                //this.InitCulture("en-US",strResource);

            }
            else
            {
                // Init control:
                ResourceManager rm = new ResourceManager("Company.Interface.Resources.Language", typeof(BaseForm).Assembly);
                this.resource = rm;
                // this.InitCulture("vi-VN", "Company.Interface.Resources.Language");
                //this.InitCulture("vi-VN", strResource);
            }
            base.OnLoad(e);
        }

        public BaseForm()
        {

            GlobalSettings.KhoiTao_GiaTriMacDinh();
            InitializeComponent();
        }

        #region Dữ liệu chuẩn.

        public string Nuoc_GetName(object id)
        {
            //if (id == null || string.IsNullOrEmpty(id.ToString().Trim()))
            //    return "";

            //if (_Nuoc == null) _Nuoc = Nuoc.SelectAll().Tables[0];
            //return this._Nuoc.Select(string.Format("ID = '{0}'", id.ToString().PadRight(3)))[0][1].ToString();

            return Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(id.ToString());
        }
        public string DVT_VNACC(object codeV4)
        {
            return Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(codeV4.ToString());
        }
        protected string DonViTinh_GetName(object id)
        {
            //if (id == null || string.IsNullOrEmpty(id.ToString().Trim()))
            //    return "";

            //if (this._DonViTinh == null) this._DonViTinh = DonViTinh.SelectAll().Tables[0];

            //DataRow[] rows = this._DonViTinh.Select(string.Format("ID = '{0}'", id.ToString().PadRight(3)));

            //return rows.Length > 0 ? rows[0][1].ToString() : "";

            return Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(id);
        }
        protected string DonViTinh_GetID(string ten)
        {
            //int val = DonViTinhID(ten);
            //if (val == -1) return string.Empty;
            //else return val.ToString();

            return Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetID(ten);
        }
        protected int DonViTinhID(string ten)
        {
            //if (string.IsNullOrEmpty(ten.Trim()))
            //    return -1;

            //if (this._DonViTinh == null) this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            //DataRow[] items = this._DonViTinh.Select(string.Format("Ten = '{0}'", ten));
            //if (items.Length == 0) return -1;
            //return Convert.ToInt32(items[0][0]);

            string id = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetID(ten);

            return id != "" ? Convert.ToInt32(id) : -1;
        }
        protected string DonViHaiQuan_GetName(object id)
        {
            //if (id == null || string.IsNullOrEmpty(id.ToString().Trim()))
            //    return "";

            //if (_DonViHaiQuan == null) _DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            //return this._DonViHaiQuan.Select(string.Format("ID = '{0}'", id.ToString().PadRight(6)))[0][1].ToString();

            return Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(id.ToString());
        }
        protected string LoaiHinhMauDich_GetTenVT(object id)
        {
            //if (id == null || string.IsNullOrEmpty(id.ToString().Trim()))
            //    return "";

            //if (_LoaiHinhMauDich == null) _LoaiHinhMauDich = LoaiHinhMauDich.SelectAll();
            //return this._LoaiHinhMauDich.Select(string.Format("ID = '{0}'", id.ToString().PadRight(5)))[0][2].ToString();

            return Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetTenVietTac(id.ToString());
        }
        protected string LoaiHinhMauDich_GetName(object id)
        {
            //if (id == null || string.IsNullOrEmpty(id.ToString().Trim()))
            //    return "";

            //if (_LoaiHinhMauDich == null) _LoaiHinhMauDich = LoaiHinhMauDich.SelectAll();
            //return this._LoaiHinhMauDich.Select(string.Format("ID = '{0}'", id.ToString().PadRight(5)))[0][1].ToString();

            return Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(id.ToString());
        }
        protected string NhomSanPham_GetName(object id)
        {
            //if (id == null || string.IsNullOrEmpty(id.ToString().Trim()))
            //    return "";

            //if (_NhomSanPham == null) _NhomSanPham = NhomSanPham.SelectAll().Tables[0];
            //return this._NhomSanPham.Select(string.Format("MaSanPham = '{0}'", id.ToString().PadRight(5)))[0][2].ToString();

            return Company.KDT.SHARE.Components.DuLieuChuan.NhomSanPham.getTenSanPham(id.ToString());
        }
        protected string LoaiPhuKien_GetName(object id)
        {
            //if (id == null || string.IsNullOrEmpty(id.ToString().Trim()))
            //    return "";

            //if (_LoaiPhuKien == null) _LoaiPhuKien = Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhuKien.SelectAll();
            //return this._LoaiPhuKien.Select(string.Format("ID_LoaiPhuKien = '{0}'", id.ToString().PadRight(3)))[0][1].ToString();

            return Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhuKien.GetName(id.ToString());
        }
        protected string NguyenTe_GetName(object id)
        {
            //if (id == null || string.IsNullOrEmpty(id.ToString().Trim()))
            //    return "";

            //if (_NguyenTe == null) _NguyenTe = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.SelectAll().Tables[0];
            //return this._NguyenTe.Select(string.Format("ID = '{0}'", id.ToString().PadRight(3)))[0][1].ToString();

            return Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.SelectName(id.ToString());
        }
        public string LoaiCO_GetName(object id)
        {
            //if (_LoaiCO == null) _LoaiCO = LoaiCO.SelectAll().Tables[0];
            //return this._LoaiCO.Select(string.Format("Ma = '{0}'", id.ToString()))[0][1].ToString();

            return Company.KDT.SHARE.Components.DuLieuChuan.LoaiCO.GetName(id.ToString());
        }
        #endregion

        #region Multi Languge

        public void DefaultLanguge()
        {

            CultureInfo c = CultureInfo.CurrentCulture;
            if (!c.ToString().Equals("en-US"))
            {

                this.InitCulture("en-US", "Company.Interface.Resources.Language");

            }

        }

        public void VietNameseLanguge()
        {
            CultureInfo c = CultureInfo.CurrentCulture;
            if (!c.ToString().Equals("vi-VN"))
            {

                this.InitCulture("vi-VN", "Company.Interface.Resources.Language");
            }
        }
        public void EnglishLanguge()
        {
            CultureInfo c = CultureInfo.CurrentCulture;
            if (!c.ToString().Equals("en-US"))
            {

                this.InitCulture("en-US", "Company.Interface.Resources.Language");

            }
        }
        // Get all controls in form.
        public List<Control> GetAllControls(IList controls)
        {
            List<Control> allControls = new List<Control>();
            foreach (Control control in controls)
            {
                allControls.Add(control);
                List<Control> subControls = GetAllControls(control.Controls);
                allControls.AddRange(subControls);
            }

            return allControls;
        }
        public List<Component> GetAllComponent()
        {
            List<Component> allComponent = new List<Component>();
            int i = this.components.Components.Count;
            foreach (Component component in this.components.Components)
            {
                allComponent.Add(component);
            }
            //int j = this.
            return allComponent;
        }
        public void forGrid(Control c, ResourceManager rm, string formName)
        {
            GridEX grid = (GridEX)c;
            GridEXColumnCollection columnColl = grid.RootTable.Columns;
            string columnKey = "";
            string key = String.Empty;
            foreach (GridEXColumn column in columnColl)
            {
                key = columnKey = "";
                columnKey = column.Key;
                key = formName + "_" + c.Name + "_" + columnKey;
                if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                {
                    column.Caption = rm.GetString(key);
                }
            }
            if (grid.RootTable.Groups != null)
            {
                foreach (GridEXGroup group in grid.RootTable.Groups)
                {

                    key = "";
                    key = formName + "_" + grid.Name + "_group" + group.Index.ToString();
                    if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                    {
                        group.HeaderCaption = rm.GetString(key);
                    }
                }
            }
            if (grid.RootTable.GroupHeaderTotals != null)
            {
                foreach (GridEXGroupHeaderTotal groupHeaderTotal in grid.RootTable.GroupHeaderTotals)
                {

                    key = "";
                    key = formName + "_" + grid.Name + "_" + groupHeaderTotal.Key;
                    if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                    {
                        groupHeaderTotal.TotalSuffix = rm.GetString(key);
                    }
                }
            }
            if (grid.ContextMenuStrip != null)
            {
                forToolStripMenuItem(grid.ContextMenuStrip.Items, rm, formName, grid.ContextMenuStrip.Name);
            }
        }
        public List<UIPanelBase> getUIPanelChild(Janus.Windows.UI.Dock.UIPanelCollection panelColl, UIPanelGroup panelGroup)
        {
            List<UIPanelBase> allPanels = new List<UIPanelBase>();
            List<UIPanelBase> allPanels2 = new List<UIPanelBase>();
            if (panelColl != null)//coll
            {
                foreach (UIPanelGroup panelG in panelColl)
                {
                    string n = panelG.Name;
                    allPanels.Add(panelG);
                    getUIPanelChild(null, panelG);
                }
            }
            if (panelGroup != null)//group
            {
                if (panelGroup.Panels.Count > 0)
                {
                    for (int i = 0; i < (int)panelGroup.Panels.Count; i++)
                    {
                        if (panelGroup.Panels[i].GetType() == typeof(UIPanelGroup))
                        {
                            UIPanelGroup tmppanelGroup = (UIPanelGroup)panelGroup.Panels[i];
                            string n = tmppanelGroup.Name;
                            allPanels.Add(tmppanelGroup);
                            allPanels2 = this.getUIPanelChild(null, tmppanelGroup);
                            allPanels.AddRange(allPanels2);
                        }
                        else
                        {
                            string panelName = panelGroup.Panels[i].Name;
                            allPanels.Add(panelGroup.Panels[i]);
                        }
                    }
                }
            }
            return allPanels;
        }
        public List<UIPanelBase> getUIPanelChild2(Janus.Windows.UI.Dock.UIPanelCollection panelC, UIPanelGroup panelG)
        {
            List<UIPanelBase> allPanels = new List<UIPanelBase>();
            List<UIPanelBase> allPanels2 = new List<UIPanelBase>();
            UIPanelGroup panelGroup;
            if (panelC != null)//coll
            {
                panelGroup = (UIPanelGroup)panelC[0];
                allPanels.Add(panelGroup);
            }
            else
            {
                panelGroup = panelG;
            }
            string na = panelGroup.Name;
            if (panelGroup.Panels.Count > 0)
            {
                for (int i = 0; i < (int)panelGroup.Panels.Count; i++)
                {
                    if (panelGroup.Panels[i].GetType() == typeof(UIPanelGroup))
                    {
                        UIPanelGroup tmppanelGroup = (UIPanelGroup)panelGroup.Panels[i];
                        string n = tmppanelGroup.Name;
                        allPanels.Add(tmppanelGroup);
                        allPanels2 = this.getUIPanelChild(null, tmppanelGroup);
                        allPanels.AddRange(allPanels2);
                    }
                    else
                    {
                        string panelName = panelGroup.Panels[i].Name;
                        allPanels.Add(panelGroup.Panels[i]);
                    }
                }
            }
            return allPanels;
        }
        public void forPanel(Component c, ResourceManager rm, string formName)
        {
            if (c.GetType().ToString().Contains("PanelManager"))
            {
                Janus.Windows.UI.Dock.UIPanelManager p = (Janus.Windows.UI.Dock.UIPanelManager)c;
                Janus.Windows.UI.Dock.UIPanelCollection panelColl = p.Panels;
                List<UIPanelBase> panels = getUIPanelChild2(panelColl, null);
                foreach (Janus.Windows.UI.Dock.UIPanelBase panel in panels)
                {
                    string panelName = panel.Name;
                    string panelText = panel.Text;
                    string key = formName + "_" + panelName;
                    if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                    {
                        panel.Text = rm.GetString(key);
                    }
                }

            }
            else
            {

            }


        }// no need
        public void forExplore(Component c, ResourceManager rm, string formName)
        {

            ExplorerBar e = (ExplorerBar)c;
            string eName = e.Name;
            foreach (ExplorerBarGroup eGroup in e.Groups)
            {
                string groupKey = eGroup.Key;
                // allExploreGroup.Add(eGroup);                               
                if (rm.GetString(formName + "_" + groupKey) != null && rm.GetString(formName + "_" + groupKey).Trim() != "")
                {
                    eGroup.Text = rm.GetString(formName + "_" + groupKey);
                }

                foreach (ExplorerBarItem eItem in eGroup.Items)
                {
                    string itemKey = eItem.Key;
                    //  allExploreItem.Add(eItem);
                    string key = formName + "_" + groupKey + "_" + itemKey;
                    if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                    {
                        eItem.Text = rm.GetString(key);
                    }

                }


            }
        }
        public List<UICommand> forCommandBar(Control c, UICommand cm, ResourceManager rm, string formName)
        {
            List<UICommand> allCommand = new List<UICommand>();
            List<UICommand> suballCommand = new List<UICommand>();
            UICommandBar cmdBar = (UICommandBar)c;
            if (c != null)
            {
                //contextMenu
                UICommandManager cmdMangager = cmdBar.CommandManager;
                UIContextMenuCollection contextMenuColl = cmdMangager.ContextMenus;
                if (contextMenuColl != null)
                {
                    for (int i = 0; i < cmdMangager.ContextMenus.Count; i++)
                    {
                        UICommandCollection cmdColl = cmdMangager.ContextMenus[i].Commands;
                        if (cmdColl != null && cmdColl.Count > 0)
                        {
                            foreach (UICommand cmd in cmdColl)
                            {
                                string key = formName + "_" + cmd.Key;
                                if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                                {
                                    cmd.Text = rm.GetString(key);

                                }
                            }
                        }
                    }
                }
                //if (cmdMangager.Commands != null)
                //{
                //    foreach (UICommand cmd in cmdMangager.Commands)
                //    {
                //        string key = formName + "_" + cmd.Key;
                //        if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                //        {
                //            cmd.Text = rm.GetString(key);

                //        }
                //    }

                //}
                //Command
                string cmdBarKey = cmdBar.Name;
                foreach (UICommand cmd in cmdBar.Commands)
                {
                    string cmdKey = cmd.Key;
                    if (rm.GetString(formName + "_" + cmdKey) != null && rm.GetString(formName + "_" + cmdKey).Trim() != "")
                    {
                        cmd.Text = rm.GetString(formName + "_" + cmdKey);
                    }
                    if (cmd.Commands.Count > 0)
                    {
                        suballCommand = forCommandBar(null, cmd, rm, formName);
                        allCommand.AddRange(suballCommand);
                    }
                    else
                    {
                        allCommand.Add(cmd);
                    }
                }
            }
            if (cm != null)
            {
                string cmdKey = cm.Key;
                allCommand.Add(cm);
                foreach (UICommand cmd in cm.Commands)
                {
                    string Key = cmd.Key;
                    //cmd.Text;
                    if (rm.GetString(formName + "_" + Key) != null && rm.GetString(formName + "_" + Key) != "")
                    {
                        cmd.Text = rm.GetString(formName + "_" + Key);
                    }
                    if (cmd.Commands.Count > 0)
                    {
                        suballCommand = forCommandBar(null, cmd, rm, formName);
                        allCommand.AddRange(suballCommand);
                    }
                    else
                    {
                        allCommand.Add(cmd);
                    }
                }
            }
            return allCommand;

        }
        public void forToolStripMenuItem(ToolStripItemCollection itemColl, ResourceManager rm, string formName, string contextMenuName)
        {
            try
            {
                foreach (ToolStripMenuItem menuItem in itemColl)
                {
                    string key = "";
                    key = formName + "_" + contextMenuName + "_" + menuItem.Name;
                    try
                    {
                        if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                        {
                            menuItem.Text = rm.GetString(key);

                        }
                    }
                    catch { }
                    try
                    {
                        if (menuItem.DropDownItems != null && menuItem.DropDownItems.Count > 0)
                            try { forToolStripMenuItem(menuItem.DropDownItems, rm, formName, contextMenuName); }
                            catch { }
                    }
                    catch { }
                }
            }
            catch { }
        }
        // Init culture and language.
        public void InitCulture(string language, string resourcefile)
        {
            CultureInfo culture = new CultureInfo(language);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            // Init control:
            ResourceManager rm = new ResourceManager(resourcefile, typeof(BaseForm).Assembly);
            this.resource = rm;
            List<Control> allControls = GetAllControls(this.Controls);
            if (this.components != null)
            {
                List<Component> allComponents = this.GetAllComponent();
            }

            int i = allControls.Count;
            string name = "";
            string key = "";
            foreach (Control c in allControls)
            {
                Form f = c.FindForm();

                Type ctrType = c.GetType();
                // if (ctrTypeName == "Janus.Window.GridEX.GridEX" )                
                if (ctrType == typeof(GridEX))
                {
                    this.forGrid(c, rm, f.Name);
                }
                else if (ctrType == typeof(Janus.Windows.ExplorerBar.ExplorerBar))
                {
                    this.forExplore(c, rm, f.Name);
                }
                else if (c.GetType() == typeof(UICommandBar))
                {
                    List<UICommand> cmdList = this.forCommandBar(c, null, rm, f.Name);
                }
                else if (c.GetType() == typeof(ContextMenuStrip))
                {

                }

                else if (c.GetType() == typeof(UIComboBox))
                {
                    UIComboBox cbo = (UIComboBox)c;
                    if (cbo.ValueMember == "" && cbo.Items.Count > 0)
                    {
                        foreach (UIComboBoxItem item in cbo.Items)
                        {
                            key = "";
                            string value = item.Text.ToString();
                            key = value;
                            switch (key.Trim())
                            {
                                //case "-1": key = "status_No_Declared"; break;
                                //case "0": key = "status_Wait_Approved"; break;
                                //case "1": key = "status_Approved"; break;
                                //case "2": key = "status_No_Approved"; break;
                                //case "N": key = "status_Import"; break;
                                //case "X": key = "status_Export"; break;
                                case "Chưa khai báo": key = "status_No_Declared"; break;
                                case "Chờ duyệt": key = "status_Wait_Approved"; break;
                                case "Đã duyệt": key = "status_Approved"; break;
                                case "Không phê duyệt": key = "status_No_Approved"; break;
                                case "Nhập khẩu": key = "status_Import"; break;
                                case "Xuất khẩu": key = "status_Export"; break;
                                case "Tờ khai nhập": key = "type_Import"; break;
                                case "Tờ khai xuất": key = "type_Export"; break;
                                //
                                case "Tất cả": key = "method_all"; break;
                                case "Nguyên phụ liệu": key = "module_NPL"; break;
                                case "Sản phẩm": key = "module_SP"; break;
                                case "Tờ khai": key = "module_TK"; break;
                                case "Thanh khoản": key = "module_TKh"; break;
                                case "Quản trị hệ thống": key = "module_QT"; break;
                                case "Không": key = "thanhkhoan_No"; break;
                                case "Có": key = "thanhkhoan_Yes"; break;
                                case "Theo mã NPL": key = "sort_material"; break;
                                case "Theo ngày ĐK tờ khai": key = "sort_declaration"; break;

                                //
                                case "Thuế NK không thu": key = "thuekhongthu"; break;
                                case "Thuế NK phải nộp": key = "thuephainop"; break;
                                case "Tách làm 2": key = "tachlam2"; break;
                                case "Không tách làm 2": key = "khongtachlam2"; break;
                                case "Ngày hoàn thành": key = "ngayhoanthanh"; break;
                                case "Ngày thực xuất": key = "ngaythucxuat"; break;
                                case "Định mức": key = "dinhmuc"; break;
                                case "Trang": key = "page"; break;
                                //
                                case "Thanh Khoản 1 phần": key = "thanhkhoanmotphan"; break;
                                case "Thanh khoản hết": key = "thanhkhoanhet"; break;
                                case "Chưa thanh khoản": key = "chuatk"; break;



                            }
                            if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                            {
                                item.Text = rm.GetString(key);
                            }

                        }

                    }

                }
                else if (c.GetType() == typeof(FilterEditor))
                {
                    FilterEditor filter = (FilterEditor)c;
                    key = "";
                    string columnKey = "";
                    Janus.Windows.GridEX.GridEX grid = (Janus.Windows.GridEX.GridEX)filter.SourceControl;
                    foreach (GridEXColumn column in filter.Table.Fields)
                    {
                        key = columnKey = "";
                        columnKey = column.Key;
                        key = f.Name + "_" + grid.Name + "_" + columnKey;
                        if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                        {
                            column.Caption = rm.GetString(key);
                        }
                    }
                    //filter.Refresh();
                    filter.Reload();

                }
                else if (c.GetType() == typeof(UIAutoHideStrip))
                {
                    UIAutoHideStrip hideCtr = (UIAutoHideStrip)c;
                    UIPanelBase[] panelbase = hideCtr.Panels;
                    foreach (UIPanelBase panel in panelbase)
                    {
                        key = String.Empty;
                        key = f.Name + "_" + panel.Name;
                        key = key.Trim();
                        try
                        {
                            if (rm.GetString(key) != null && rm.GetString(key).Length >= 1)
                            {
                                panel.Text = rm.GetString(key);
                            }
                        }
                        catch
                        {
                        }
                        if (panel.HasChildren)
                        {
                            forChildControlInpanel((UIPanelBase)panel, rm, f.Name);
                        }
                    }
                }
                else if (c.GetType() == typeof(UIStatusBar))
                {
                    UIStatusBar statusBar = (UIStatusBar)c;
                    if (statusBar.Panels != null)
                    {
                        foreach (UIStatusBarPanel statusP in statusBar.Panels)
                        {
                            key = "";
                            key = f.Name + "_" + c.Name + "_" + statusP.Key;
                            if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                            {
                                statusP.Text = statusP.ToolTipText = rm.GetString(key);

                            }
                        }
                    }
                }
                else
                {
                    key = String.Empty;
                    if (c.Name.Trim() != "")
                    {
                        key = f.Name + "_" + c.Name;
                        try
                        {
                            if (rm.GetString(key) != null && rm.GetString(key).Trim() != "") { c.Text = rm.GetString(key); }
                        }
                        catch
                        {
                        }
                    }
                }
            }
            // Init form title:                 
            try
            {
                string text = this.Name.Trim();
                if (rm.GetString(text) != null && rm.GetString(text).Trim() != "")
                {
                    this.Text = rm.GetString(text);
                }
            }
            catch
            {

            }
            if (ValidatorManager.GetValidators(this) != null)
            {
                foreach (BaseValidator baseVali in ValidatorManager.GetValidators(this))
                {
                    baseVali.ErrorMessage = rm.GetString(this.Name + "_" + baseVali.Tag);
                    baseVali.ErrorMessageEnglish = rm.GetString(this.Name + "_" + baseVali.Tag);
                    string st = baseVali.ErrorMessage;
                }
            }
        }
        public void forChildControlInpanel(UIPanelBase panel, ResourceManager rm, string formname)
        {
            if (panel.GetType() == typeof(UIPanelGroup))
            {
                UIPanelGroup panelG = (UIPanelGroup)panel;
                foreach (UIPanelBase pBase in panelG.Panels)
                {
                    forChildControlInpanel(pBase, rm, formname);
                }
            }
            else//UIPanel
            {
                string key = String.Empty;
                Control tmpCtr = panel.GetNextControl(new Control(), true);
                if (tmpCtr.GetType() == typeof(UIPanelInnerContainer))
                {
                    UIPanelInnerContainer panelContainer = (UIPanelInnerContainer)tmpCtr;
                    if (panelContainer.Controls != null)
                    {
                        foreach (Control control in panelContainer.Controls)
                        {
                            if (control.GetType() == typeof(GridEX))
                            {
                                this.forGrid(control, rm, formname);
                            }
                            else
                            {
                                key = String.Empty;
                                key = formname + "_" + panel.Name;
                                try
                                {
                                    if (rm.GetString(key) != null && rm.GetString(key) != "")
                                    {
                                        control.Text = rm.GetString(key);
                                    }
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
                else//UiPanel 
                {
                    forChildControlInpanel((UIPanel)tmpCtr, rm, formname);
                }
            }

        }
        public string setText(string vnText, string enText)
        {
            if (GlobalSettings.NGON_NGU == "1")
            {
                return enText;
            }
            else
            {
                return vnText;
            }
        }
        #endregion

        # region XX

        //Lấy toàn bộ control trên form
        public List<Control> GetAllControls111(IList controls)
        {
            List<Control> allControls = new List<Control>();
            foreach (Control control in controls)
            {
                allControls.Add(control);
                List<Control> subControls = GetAllControls(control.Controls);
                allControls.AddRange(subControls);
            }
            //int i = this.components.Components.Count;
            //foreach (Component c in components.Components)
            //{

            //}
            //int j = this.
            return allControls;
        }
        //Lấy toàn bộ các subcontrol trên form.
        public List<Component> GetAllComponent111()
        {
            List<Component> allComponent = new List<Component>();
            int i = this.components.Components.Count;
            foreach (Component component in this.components.Components)
            {
                allComponent.Add(component);
            }
            return allComponent;
        }
        private void BaseForm_Load(object sender, System.EventArgs e)
        {
            //this.InitCulture("en-US");
        }
        public void InitCulture1111(string language)
        {
            CultureInfo culture = new CultureInfo(language);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            // Init control:
            ResourceManager rm = new ResourceManager("Company.Interface.Language", typeof(BaseForm).Assembly);
            List<Control> allControls = GetAllControls(this.Controls);
            // List<Control> allControls = GetAllControls(this.Controls);
            if (this.components != null)
            {
                List<Component> allCmpt = GetAllComponent();
            }

            foreach (Control c in allControls)
            {
                Form f = c.FindForm();
                System.Type stype = c.GetType();
                if (stype == typeof(GridEX))
                {
                    IsGrid(c, rm, f.Name);
                }
                //Subcontrol                 
                //foreach (Component cmp in allCmpt)
                //{                     
                if (stype == typeof(Janus.Windows.UI.Dock.UIPanelManager))
                    IsPanel(c, rm, f.Name);
                else if (stype == typeof(Janus.Windows.ExplorerBar.ExplorerBar))
                    IsExplore(c, rm, f.Name);
                else if (stype == typeof(UICommandBar))
                {
                    List<UICommand> cmdList = IsCmdToolBar(c, null, rm, f.Name);
                }
                else
                {
                    string key = f.Name + "_" + c.Name;
                    try
                    {
                        c.Text = rm.GetString(key);
                    }
                    catch
                    {

                    }
                }
            }
            // Init form title:
            try
            {
                string text = this.Name;
                this.Text = rm.GetString(text);
            }
            catch
            {

            }

        }

        public List<UIPanelBase> SubUIPanel(Janus.Windows.UI.Dock.UIPanelCollection panelColl, UIPanelGroup panelGroup)
        {
            List<UIPanelBase> morePanel = new List<UIPanelBase>();
            List<UIPanelBase> copyPanel = new List<UIPanelBase>();
            if (panelColl != null)
            {
                foreach (UIPanelGroup panelG in panelColl)
                {
                    string n = panelG.Name;
                    morePanel.Add(panelG);
                    SubUIPanel(null, panelG);
                }

            }
            if (panelGroup != null)//group
            {
                if (panelGroup.Panels.Count > 0)
                {
                    for (int i = 0; i < (int)panelGroup.Panels.Count; i++)
                    {
                        if (panelGroup.Panels[i].GetType() == typeof(UIPanelGroup))
                        {
                            UIPanelGroup tmppanelGroup = (UIPanelGroup)panelGroup.Panels[i];
                            copyPanel = this.SubUIPanel(null, tmppanelGroup);
                            morePanel.AddRange(copyPanel);
                        }
                        else
                        {
                            string panelName = panelGroup.Panels[i].Name;
                            morePanel.Add(panelGroup.Panels[i]);
                        }
                    }
                }
            }
            return morePanel;
        }
        public void IsPanel(Component c, ResourceManager rm, string formName)
        {
            //if (c.GetType().ToString().Contains("Janus.Windows.UI.Dock.UIPanelManager"))
            if (c.GetType().ToString().Contains("UIPanelManager"))
            {
                Janus.Windows.UI.Dock.UIPanelManager p = (Janus.Windows.UI.Dock.UIPanelManager)c;
                Janus.Windows.UI.Dock.UIPanelCollection panelColl = p.Panels;
                List<UIPanelBase> panels = SubUIPanel(panelColl, null);
                foreach (Janus.Windows.UI.Dock.UIPanelBase panel in panels)
                {
                    string pText = panel.Text;
                    string key = formName + "_" + panel.Name;
                    panel.Text = rm.GetString(key);
                }

            }
            else
            {

            }


        }
        public void IsExplore(Component c, ResourceManager rm, string formName)
        {

            ExplorerBar e = (ExplorerBar)c;
            string eName = e.Name;
            foreach (ExplorerBarGroup eBarG in e.Groups)
            {
                string groupKey = eBarG.Key;
                if (rm.GetString(formName.Trim() + "_" + groupKey.Trim()) != null)
                {
                    eBarG.Text = rm.GetString(formName + "_" + groupKey);
                }

                foreach (ExplorerBarItem eBarItem in eBarG.Items)
                {
                    string itemKey = eBarItem.Key;
                    //  allExploreBarItem.Add(eBarItem);
                    string key = formName + "_" + groupKey + "_" + itemKey;
                    if (rm.GetString(key) != null)
                    {
                        eBarItem.Text = rm.GetString(key);
                    }

                }


            }
        }
        public void IsGrid(Control c, ResourceManager rm, string formName)
        {
            GridEX grid = (GridEX)c;
            GridEXColumnCollection colColl = grid.RootTable.Columns;

            foreach (GridEXColumn col in colColl)
            {
                try
                {
                    col.Caption = rm.GetString(formName + "_" + c.Name + "_" + col.Key);
                }
                catch { }
            }
        }
        public List<UICommand> IsCmdToolBar(Control c, UICommand cm, ResourceManager rm, string formName)
        {
            List<UICommand> allCommand = new List<UICommand>();
            List<UICommand> suballCommand = new List<UICommand>();
            UICommandBar cmdBar = (UICommandBar)c;
            if (c != null)
            {
                string cmdBarKey = cmdBar.Name;
                foreach (UICommand cmd in cmdBar.Commands)
                {
                    string cmdKey = cmd.Key;
                    if (rm.GetString(formName + "_" + cmdKey) != null)
                    {
                        cmd.Text = rm.GetString(formName + "_" + cmdKey);
                    }
                    if (cmd.Commands.Count > 0)
                    {
                        suballCommand = IsCmdToolBar(null, cmd, rm, formName);
                        allCommand.AddRange(suballCommand);
                    }
                    else
                    {
                        allCommand.Add(cmd);
                    }
                }
            }
            if (cm != null)
            {
                string cmdKey = cm.Key;
                allCommand.Add(cm);
                foreach (UICommand cmd in cm.Commands)
                {
                    string Key = cmd.Key;
                    //cmd.Text;
                    if (rm.GetString(formName + "_" + Key) != null)
                    {
                        cmd.Text = rm.GetString(formName + "_" + Key);
                    }
                    if (cmd.Commands.Count > 0)
                    {
                        suballCommand = IsCmdToolBar(null, cmd, rm, formName);
                        allCommand.AddRange(suballCommand);
                    }
                    else
                    {
                        allCommand.Add(cmd);
                    }
                }
            }
            return allCommand;

        }

        # endregion XX
    }
}