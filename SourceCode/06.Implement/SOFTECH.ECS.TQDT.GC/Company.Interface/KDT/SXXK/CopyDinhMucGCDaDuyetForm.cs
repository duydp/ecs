﻿using System;
using Janus.Windows.GridEX;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;

namespace Company.Interface.KDT.SXXK
{
    public partial class CopyDinhMucGCDaDuyetForm : BaseForm
    {
        public Company.GC.BLL.KDT.GC.HopDong HD;
        public SanPham SP;
        public CopyDinhMucGCDaDuyetForm()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll();           
        }

    
        private void DinhMucGCEditForm_Load(object sender, EventArgs e)
        {
            try
            {
                dgSanPham.DataSource = DinhMuc.GetSanPhamCoDinhMuc(HD.ID).Tables[0];
                dgSanPham.Refetch();
            }
            catch
            {
                dgSanPham.Refresh();
            }
        }
      

        private void btnClose_Click(object sender, EventArgs e)
        {           
            this.Close();
        }

        private void dgSanPham_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                SP=new SanPham();
                SP.Ma = e.Row.Cells["Ma"].Text;
                this.Close();
            }
        }

      
    }
}