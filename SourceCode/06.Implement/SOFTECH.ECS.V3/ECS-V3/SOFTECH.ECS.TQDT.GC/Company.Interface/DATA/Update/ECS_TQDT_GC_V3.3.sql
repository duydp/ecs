/*
Run this script on:

        192.168.72.151.ECS_TQDT_GC_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.151.ECS_TQDT_GC_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 06/29/2012 9:42:40 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_GC_BC05TT74]'
GO
-- =============================================  
-- Author:  LanNT  
-- Create date:   
-- Description:   
-- =============================================  
ALTER PROCEDURE [dbo].[p_GC_BC05TT74]   
 -- Add the parameters for the stored procedure here  
 @HopDong_ID BIGINT,  
 @MaHaiQuan CHAR(6),  
 @MaDoanhNghiep VARCHAR(14)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
/*ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ma) AS STT*/  
  
SELECT   ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS STT,  t_GC_NguyenPhuLieu.Ten AS TenNPL, t_GC_NguyenPhuLieu.Ma AS MaNPL, t_GC_SanPham.Ten AS TenSP, t_GC_SanPham.Ma AS MaSP, 
                      t_HaiQuan_DonViTinh.Ten AS DVT, SUM(t_GC_SanPham.SoLuongDaXuat) AS SoLuongDaXuat, t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) 
                      / 100 AS DinhMucGomTiLeHH, t_GC_SanPham.SoLuongDaXuat * (t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) / 100) AS LuongSuDung, 
                      SUM(t_GC_NguyenPhuLieu.SoLuongDaDung) AS TongSoLuongDaDung, t_GC_DinhMuc.TyLeHaoHut, t_GC_DinhMuc.DinhMucSuDung
FROM         t_GC_NguyenPhuLieu INNER JOIN
                      t_KDT_GC_HopDong ON t_GC_NguyenPhuLieu.HopDong_ID = t_KDT_GC_HopDong.ID INNER JOIN
                      t_GC_DinhMuc ON t_KDT_GC_HopDong.ID = t_GC_DinhMuc.HopDong_ID AND t_GC_NguyenPhuLieu.Ma = t_GC_DinhMuc.MaNguyenPhuLieu INNER JOIN
                      t_GC_SanPham ON t_KDT_GC_HopDong.ID = t_GC_SanPham.HopDong_ID AND t_GC_DinhMuc.MaSanPham = t_GC_SanPham.Ma INNER JOIN
                      t_HaiQuan_DonViTinh ON t_GC_SanPham.DVT_ID = t_HaiQuan_DonViTinh.ID
WHERE     (t_GC_NguyenPhuLieu.HopDong_ID = @HopDong_ID)
GROUP BY t_KDT_GC_HopDong.MaHaiQuan, t_KDT_GC_HopDong.MaDoanhNghiep, t_GC_NguyenPhuLieu.Ten, t_GC_NguyenPhuLieu.Ma, t_GC_SanPham.Ten, 
                      t_GC_SanPham.Ma, t_HaiQuan_DonViTinh.Ten, t_GC_SanPham.SoLuongDaXuat * (t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) / 100), 
                      t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) / 100, t_GC_DinhMuc.TyLeHaoHut, t_GC_DinhMuc.DinhMucSuDung
HAVING      (SUM(t_GC_SanPham.SoLuongDaXuat) > 0) AND (t_KDT_GC_HopDong.MaHaiQuan = @MaHaiQuan) AND (t_KDT_GC_HopDong.MaDoanhNghiep = @MaDoanhNghiep)
ORDER BY TenNPL
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_GC_BC01HSTK_GC_TT117_Tong]'
GO
  -- =============================================  
-- Author:  LanNT  
-- Create date:   
-- Description:   Modify 20/06/2012      
-- =============================================  
  
ALTER PROCEDURE [dbo].[p_GC_BC01HSTK_GC_TT117_Tong]   
 -- Add the parameters for the stored procedure here  
 @IDHopDong BIGINT,  
 @MaHaiQuan CHAR(6),  
 @MaDoanhNghiep VARCHAR(14)   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;    
  --  ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS 'STT',  
--DECLARE @IDHopDong BIGINT;DECLARE @MaHaiQuan CHAR(6);DECLARE @MaDoanhNghiep VARCHAR(14) ; SET @IDHopDong=710;SET @MaDoanhNghiep = '0400101556';SET @MaHaiQuan='C34C'  
--ROW_NUMBER() OVER (ORDER BY  ToKhai.MaNguyenPhuLieu) AS 'STT',

SELECT     ToKhai.IDHopDong AS ID, ToKhai.MaDoanhNghiep, ToKhai.MaHaiQuan, '-' AS SoToKhai, '-' AS NgayDangKy, ToKhai.TenHang AS TenNguyenPhuLieu, 
                      ToKhai.MaNguyenPhuLieu, '-' AS SoLuong, t_HaiQuan_DonViTinh.Ten AS DVT, SUM(ToKhai.SoLuong) AS TongLuong
FROM         (SELECT     CONVERT(varchar(20), t_KDT_ToKhaiMauDich.SoToKhai) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, 
                                              t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, t_KDT_ToKhaiMauDich.MaHaiQuan, 
                                              t_KDT_HangMauDich.MaPhu AS MaNguyenPhuLieu, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.DVT_ID, 
                                              t_KDT_ToKhaiMauDich.MaDoanhNghiep
                       FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN
                                              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
                       WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                              (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'NGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N')
                       UNION
                       SELECT     CONVERT(varchar(20), t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) + '/' + t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh AS SoToKhai, 
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, 
                                             t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, t_KDT_GC_HangChuyenTiep.MaHang AS MaNguyenPhuLieu, t_KDT_GC_HangChuyenTiep.SoLuong, 
                                             t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.ID_DVT, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                       FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                             t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                       WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                             (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh IN ('PHPLN', 'PHSPN', 'NGC18', 'NGC19', 'PHTBN')) AND (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N')) 
                      AS ToKhai INNER JOIN
                      t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID
GROUP BY ToKhai.IDHopDong, ToKhai.MaDoanhNghiep, ToKhai.MaHaiQuan, ToKhai.TenHang, ToKhai.MaNguyenPhuLieu, t_HaiQuan_DonViTinh.Ten
HAVING      (ToKhai.IDHopDong = @IDHopDong) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan)
  
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_GC_BC01HSTK_GC_TT117]'
GO
-- =============================================      
-- Author:  LanNT      
-- Create date:       
-- Description: Modify 19/06/2012      
-- =============================================      
      
ALTER PROCEDURE [dbo].[p_GC_BC01HSTK_GC_TT117]       
 -- Add the parameters for the stored procedure here      
 @IDHopDong BIGINT,      
 @MaHaiQuan CHAR(6),      
 @MaDoanhNghiep VARCHAR(14)       
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
-- SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS 'STT',  
-- ROW_NUMBER() OVER (ORDER BY  ToKhai.MaNguyenPhuLieu) AS 'STT'

SELECT    ROW_NUMBER() OVER (ORDER BY  ToKhai.MaNguyenPhuLieu) AS 'STT',  ToKhai.IDHopDong AS ID, ToKhai.MaDoanhNghiep, ToKhai.MaHaiQuan, ToKhai.SoToKhai, ToKhai.NgayDangKy, ToKhai.TenHang AS TenNguyenPhuLieu, 
                      ToKhai.MaNguyenPhuLieu, ToKhai.SoLuong, t_HaiQuan_DonViTinh.Ten AS DVT, '-' AS TongLuong
FROM         (SELECT     CONVERT(varchar(20), t_KDT_ToKhaiMauDich.SoToKhai) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, 
                                              t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, t_KDT_ToKhaiMauDich.MaHaiQuan, 
                                              t_KDT_HangMauDich.MaPhu AS MaNguyenPhuLieu, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.DVT_ID, 
                                              t_KDT_ToKhaiMauDich.MaDoanhNghiep
                       FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN
                                              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
                       WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                              (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'NGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N')
                       UNION
                       SELECT     CONVERT(varchar(20), t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) + '/' + t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh AS SoToKhai, 
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, 
                                             t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, t_KDT_GC_HangChuyenTiep.MaHang AS MaNguyenPhuLieu, t_KDT_GC_HangChuyenTiep.SoLuong, 
                                             t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.ID_DVT, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                       FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                             t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                       WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                             (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh IN ('PHPLN', 'PHSPN', 'NGC18', 'NGC19', 'PHTBN')) AND (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N')) 
                      AS ToKhai INNER JOIN
                      t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID
WHERE     (ToKhai.IDHopDong = @IDHopDong) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan)
END   
  
 GO
 if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.3', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.3', getdate(), null)
	end  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
