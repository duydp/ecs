﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

namespace SOFTECH.ECS.V3.Components.KD
{

    //NAMNTH
    public class ChapNhanVaCapSoTK
    {

        /// <summary>
        /// ConfigChapnhanvacapsoTK
        /// </summary> 
        /// <returns>string</returns>
        public static string ConfigChapNhanVaCapSoTK(ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/KD/ChapNhanVaCapSoTK.xml");
                if (tkmd != null && docXML != null)
                {
                    XElement Root = docXML.Root;                    
                    if (Root != null && Root.Elements().Count() > 0)
                    {
                        Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, status, declarationOffice);
                        
                        XElement AdditionalInformation = Root.Element(NodeChapNhanVaCapSoTK.AdditionalInformation);
                        foreach (XElement element in AdditionalInformation.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodeChapNhanVaCapSoTK.content.ToUpper()))
                                element.SetValue("");//Thông tin hướng dẫn doanh nghiệp
                            if (element.Name.ToString().ToUpper().Equals(NodeChapNhanVaCapSoTK.statement.ToUpper()))
                                element.SetValue("");//Mã nội dung phản hồi
                        }


                        //docXML.Save(@"D:\NewChapNhanVaCapSoTK.xml");
                        //docXML.Save(@"../../../SOFTECH.ECS.V3.Components/XML/LoadKD/NewChapNhanVaCapSoTK.xml");
                        return docXML.ToString();
                    }

                }
                return "";
            }
            catch
            {
                return "";
            }

        }
    }
}
