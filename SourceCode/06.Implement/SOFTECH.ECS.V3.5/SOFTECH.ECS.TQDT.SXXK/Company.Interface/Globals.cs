﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.DanhMucChuan;
using System.Windows.Forms;
using Company.BLL;
using System.Xml;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;
using System.IO;
using System.Net.Mail;
using System.Net;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public class Globals
    {
        #region Vnaccs

        public static void SetControlTrangThaiXuLy(Janus.Windows.EditControls.UIComboBox cboTrangThai)
        {
            cboTrangThai.Items.Clear();

            cboTrangThai.Items.Add("Tất cả", "-1");
            cboTrangThai.Items.Add("Chưa khai báo", Company.KDT.SHARE.VNACCS.EnumTrangThaiXuLy.ChuaKhaiBao);
            cboTrangThai.Items.Add("Đã khai báo", Company.KDT.SHARE.VNACCS.EnumTrangThaiXuLy.KhaiBaoTam);
            cboTrangThai.Items.Add("Đã xác nhận khai báo", Company.KDT.SHARE.VNACCS.EnumTrangThaiXuLy.KhaiBaoChinhThuc);
            cboTrangThai.Items.Add("Thông quan", Company.KDT.SHARE.VNACCS.EnumTrangThaiXuLy.ThongQuan);
            cboTrangThai.Items.Add("Đang sửa", Company.KDT.SHARE.VNACCS.EnumTrangThaiXuLy.KhaiBaoSua);
            cboTrangThai.Items.Add("Không phê duyệt", Company.KDT.SHARE.VNACCS.EnumTrangThaiXuLy.TuChoi);

            cboTrangThai.SelectedIndex = 0;
        }

        #endregion

        #region Get Password
        public bool getPassword(ref string password)
        {

            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog();
                if (!wsForm.IsReady) return false;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            return true;
        }


        #endregion

        #region MESSAGE
        private static Company.Controls.KDTMessageBoxControl _KDTMsgBox;
        private static Company.Controls.MessageBoxControl _MsgBox;

        public static string ShowMessageTQDT(string messageHQ, string messageContent, bool showYesNoButton)
        {
            _KDTMsgBox = new Company.Controls.KDTMessageBoxControl();
            _KDTMsgBox.ShowYesNoButton = showYesNoButton;
            _KDTMsgBox.HQMessageString = messageHQ;
            _KDTMsgBox.MessageString = messageContent;
            _KDTMsgBox.ShowDialog();
            string st = _KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        public static string ShowMessageTQDT(string messageContent, bool showYesNoButton)
        {
            string messageHQ = "Thông báo trả về từ hệ thống Hải quan";
            _KDTMsgBox = new Company.Controls.KDTMessageBoxControl();
            _KDTMsgBox.ShowYesNoButton = showYesNoButton;
            _KDTMsgBox.HQMessageString = messageHQ;
            _KDTMsgBox.MessageString = messageContent;
            _KDTMsgBox.ShowDialog();
            string st = _KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        public static string ShowMessage(string message, bool showYesNoButton)
        {
            _MsgBox = new Company.Controls.MessageBoxControl();
            _MsgBox.ShowYesNoButton = showYesNoButton;
            _MsgBox.MessageString = message;
            _MsgBox.ShowDialog();
            string st = _MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }

        #endregion

        #region DANH MUC DOI TAC
        /// <summary>
        /// Tra ve 1 Object Doi tac
        /// </summary>
        /// <returns></returns>
        public static DoiTac GetMaDonViObject()
        {
            DoiTac dt = new DoiTac();

            try
            {
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog();

                if (f.doiTac != null && f.doiTac.TenCongTy != "")
                {
                    dt = f.doiTac;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return dt;
        }

        /// <summary>
        /// Tra ve chuoi: ten va dia chi doi tac.
        /// </summary>
        /// <returns></returns>
        public static string GetMaDonViString()
        {
            try
            {
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog();
                if (f.doiTac != null && f.doiTac.TenCongTy != "")
                {
                    return f.doiTac.TenCongTy + ". " + f.doiTac.DiaChi;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }

        #endregion

        #region TO KHAI //Hien tai 'Chua su dung'

        /// <summary>
        /// Hủy thông tin đã đăng ký.
        /// </summary>
        public static bool HuyKhaiBao(Janus.Windows.GridEX.GridEXRow activeROW)
        {
            if (activeROW != null && activeROW.RowType == Janus.Windows.GridEX.RowType.Record)
            {
                Company.BLL.KDT.ToKhaiMauDich tkmd = (Company.BLL.KDT.ToKhaiMauDich)activeROW.DataRow;

                return HuyKhaiBao(tkmd);
            }
            else
            {
                ShowMessage("Chưa chọn thông tin để hủy.", false);
                return false;
            }
        }

        /// <summary>
        /// Hủy thông tin đã đăng ký.
        /// </summary>
        public static bool HuyKhaiBao(Company.BLL.KDT.ToKhaiMauDich toKhaiMauDich)
        {
            string password = "";
            string xmlCurrent = "";
            Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
            WSForm wsForm = new WSForm();
            Company.BLL.KDT.SXXK.MsgSend sendXML = new Company.BLL.KDT.SXXK.MsgSend();

            if (toKhaiMauDich != null)
            {
                tkmd = toKhaiMauDich;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;

                sendXML.Load();
            }
            else
            {
                ShowMessage("Chưa chọn thông tin để hủy.", false);
                return false;
            }

            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog();
                if (!wsForm.IsReady) return false;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();

            try
            {
                if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                    xmlCurrent = tkmd.WSCancelXMLNhap(password);
                else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
                    xmlCurrent = tkmd.WSCancelXMLXuat(password);

                sendXML = new Company.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();

                return LayPhanHoi(toKhaiMauDich, password); ;

            }
            catch (Exception ex)
            {
                #region FPTService
                string[] msg = ex.Message.Split('|');

                if (msg.Length == 2)
                {
                    if (msg[1] == "DOTNET_LEVEL")
                    {
                        //if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                        if (ShowMessage("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", true) == "Yes")
                        {
                            //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                            //hd.ID = tkmd.ID;
                            //hd.LoaiToKhai = Company.KDT.SHARE.Components.LoaiToKhai.TO_KHAI_MAU_DICH;
                            //hd.TrangThai = tkmd.TrangThaiXuLy;
                            //hd.ChucNang = Company.KDT.SHARE.Components.ChucNang.HUY_KHAI_BAO;
                            //hd.PassWord = password;
                            //MainForm.AddToQueueForm(hd);
                            //MainForm.ShowQueueForm();
                        }
                    }
                    else
                    {
                        ShowMessage("Có lỗi trong khai báo : " + msg[0], false);

                        if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            GlobalSettings.PassWordDT = "";
                        }
                    }
                }
                else
                {
                    ShowMessage("Xảy ra lỗi không xác định. ", false);

                    GlobalSettings.PassWordDT = "";
                }
                #endregion FPTService

                //Lỗi xảy ra trong khi 'Hủy khai báo'
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return false;
        }

        public static bool LayPhanHoi(Company.BLL.KDT.ToKhaiMauDich toKhaiMauDich, string pass)
        {
            Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
            Company.BLL.KDT.SXXK.MsgSend sendXML = new Company.BLL.KDT.SXXK.MsgSend();
            string xmlCurrent = "";

            try
            {
                if (toKhaiMauDich != null)
                {
                    tkmd = toKhaiMauDich;
                    sendXML.LoaiHS = "TK";
                    sendXML.master_id = tkmd.ID;
                    sendXML.Load();

                    xmlCurrent = tkmd.LayPhanHoiTQDTKhaiBao(pass, sendXML.msg);

                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);

                        if (kq == "Yes")
                        {
                            LayPhanHoi(toKhaiMauDich, pass);
                        }
                        return false;
                    }

                    string mess = "";
                    if (sendXML.func == 1)
                    {
                        mess = "Khai báo thông tin thành công ! \r\n------------------------------------- \r\n "
                            + "Số tiếp nhận : " + tkmd.SoTiepNhan.ToString() + "\r\n"
                            + " Loại hình khai báo : " + tkmd.MaLoaiHinh.ToString()
                            + "\r\n Ngày tiếp nhận : " + tkmd.NgayTiepNhan.ToShortDateString();
                        ShowMessageTQDT(mess, false);

                        return true; //this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                        {
                            ShowMessageTQDT("Trạng thái chứng từ đã thay đổi:\n\nTrạng thái : Đã duyệt chính thức.\nSố tờ khai : " + tkmd.SoToKhai.ToString()
                                + "\nNgày đăng ký : " + tkmd.NgayDangKy.ToString("dd/MM/yyyy"), false);

                            return true; //this.search();
                        }
                        else if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                        {
                            ShowMessage("Hải quan chưa xử lý tờ khai này!", false);
                        }
                        else if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);

                            return true; //this.search();
                        }
                    }
                    else if (sendXML.func == 3)
                    {
                        ShowMessageTQDT("Hủy tờ khai thành công", false);
                        sendXML.Delete();
                        return true; //this.search();
                    }

                    //xoa thông tin msg nay trong database
                    sendXML.Delete();

                    //setCommandStatus();
                }
            }
            catch (Exception ex)
            {
                #region FPTService

                string[] msg = ex.Message.Split('|');

                if (msg.Length == 2)
                {
                    if (msg[1] == "DOTNET_LEVEL")
                    {
                        // if (ShowMessage("Khai báo không thành công.Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                        if (ShowMessage("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", true) == "Yes")
                        {
                            //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                            //hd.ID = tkmd.ID;
                            //hd.LoaiToKhai = Company.KDT.SHARE.Components.LoaiToKhai.TO_KHAI_MAU_DICH;
                            //hd.TrangThai = tkmd.TrangThaiXuLy;
                            //hd.ChucNang = Company.KDT.SHARE.Components.ChucNang.XAC_NHAN_THONG_TIN;
                            //hd.PassWord = pass;
                            //MainForm.AddToQueueForm(hd);
                            //MainForm.ShowQueueForm();
                        }
                    }
                    else if (msg[1] == "DATA_LEVEL")
                    {
                        ShowMessageTQDT(msg[0], false);
                        return false;
                    }
                    else
                    {
                        ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        sendXML.Delete();
                    }
                }
                else
                {
                    ShowMessage("Xảy ra lỗi không xác định. \n\n" + ex.Message, false);
                }
                #endregion FPTService

                //Lỗi khi khai báo danh sách Tờ khai
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return false;
        }

        public static bool KhaiBaoToKhai() { return false; }

        public static bool LayThongTinPhanHoi() { return false; }

        public static bool KhaiBao() { return false; }

        #endregion

        #region KHAI SUA TO KHAI
        /*
        public string ConfigPhongBi(Company.BLL.KDT.ToKhaiMauDich tkmd, string maHaiQuan, string maDoanhNghiep, string tenDoanhNghiep, MessageTypes messageType, MessageFunctions messageFunction)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(maHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = messageType.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = messageFunction.ToString();


            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            tkmd.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = tkmd.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = tenDoanhNghiep;
            nodeFrom.ChildNodes[1].InnerText = maDoanhNghiep;

            tkmd.Update();

            return doc.InnerXml;
        }

        private string ConvertToXMLToKhaiSua(Company.BLL.KDT.ToKhaiMauDich tkmd)
        {
            //load du lieu
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            System.Globalization.NumberFormatInfo f = new System.Globalization.NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            if (tkmd.MaLoaiHinh.StartsWith("N"))
                docNPL.Load(path + "\\TemplateXML\\SuaToKhaiNhapDaDuyet.xml");
            else
                docNPL.Load(path + "\\TemplateXML\\SuaToKhaiXuatDaDuyet.xml");


            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = tkmd.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(tkmd.MaHaiQuan);
            
            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = tkmd.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = tkmd.TenDoanhNghiep;

            //thong tin to khai
            XmlNode nodeToKhai = docNPL.GetElementsByTagName("TO_KHAI")[0];

            //Bo sung cho to khai sua
            nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = tkmd.DeXuatKhac;
            nodeToKhai.Attributes["SOTK"].Value = tkmd.SoToKhai.ToString();
            nodeToKhai.Attributes["NGAY_DK"].Value = tkmd.NgayDangKy.ToString("yyyy-MM-dd");
            nodeToKhai.Attributes["LY_DO_SUA"].Value = tkmd.LyDoSua;

            nodeToKhai.Attributes["MA_LH"].Value = tkmd.MaLoaiHinh;
            nodeToKhai.Attributes["MA_HQ"].Value = tkmd.MaHaiQuan;
            nodeToKhai.Attributes["MA_DV_NK"].Value = tkmd.MaDoanhNghiep;
            nodeToKhai.Attributes["MA_DV_UT"].Value = tkmd.MaDonViUT;
            nodeToKhai.Attributes["MA_DV_XK"].Value = "";

            //nodeToKhai.Attributes["MA_DV_KT"].Value = tkmd.MaDaiLyTTHQ; // Commnent 29/04/2010 Hungtq

            //if (tkmd.TenDonViDoiTac.Length <= 30)
            nodeToKhai.Attributes["TEN_DV_XK"].Value = tkmd.TenDonViDoiTac;
            //else
            //nodeToKhai.Attributes["TEN_DV_XK"].Value = tkmd.TenDonViDoiTac.Substring(0, 30);

            if (tkmd.SoGiayPhep.Length <= 35)
                nodeToKhai.Attributes["SO_GP"].Value = tkmd.SoGiayPhep;
            else
                nodeToKhai.Attributes["SO_GP"].Value = tkmd.SoGiayPhep.Substring(0, 35);
            if (tkmd.NgayGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_GP"].Value = tkmd.NgayGiayPhep.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_GP"].Value = null;
            if (tkmd.NgayHetHanGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHGP"].Value = tkmd.NgayHetHanGiayPhep.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HHGP"].Value = null;
            if (tkmd.SoHopDong.Length <= 50)
                nodeToKhai.Attributes["SO_HD"].Value = tkmd.SoHopDong;
            else
                nodeToKhai.Attributes["SO_HD"].Value = tkmd.SoHopDong.Substring(0, 50);
            if (tkmd.NgayHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HD"].Value = tkmd.NgayHopDong.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HD"].Value = null;
            if (tkmd.NgayHetHanHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHHD"].Value = tkmd.NgayHetHanHopDong.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HHHD"].Value = null;
            if (tkmd.SoHoaDonThuongMai.Length <= 50)
                nodeToKhai.Attributes["SO_HDTM"].Value = tkmd.SoHoaDonThuongMai;
            else
                nodeToKhai.Attributes["SO_HDTM"].Value = tkmd.SoHoaDonThuongMai.Substring(0, 50);
            if (tkmd.NgayHoaDonThuongMai.Year > 1900)
                nodeToKhai.Attributes["NGAY_HDTM"].Value = tkmd.NgayHoaDonThuongMai.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HDTM"].Value = null;
            nodeToKhai.Attributes["MA_PTVT"].Value = tkmd.PTVT_ID;
            if (tkmd.SoHieuPTVT.Length <= 20)
                nodeToKhai.Attributes["TEN_PTVT"].Value = tkmd.SoHieuPTVT;
            else
                nodeToKhai.Attributes["TEN_PTVT"].Value = tkmd.SoHieuPTVT.Substring(0, 20);
            if (tkmd.NgayDenPTVT.Year > 1900)
                nodeToKhai.Attributes["NGAY_DEN"].Value = tkmd.NgayDenPTVT.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_DEN"].Value = null;
            if (tkmd.SoVanDon.Length <= 20)
                nodeToKhai.Attributes["SO_VANDON"].Value = tkmd.SoVanDon;
            else
                nodeToKhai.Attributes["SO_VANDON"].Value = tkmd.SoVanDon.Substring(0, 20);
            if (tkmd.NgayVanDon.Year > 1900)
                nodeToKhai.Attributes["NGAY_VANDON"].Value = tkmd.NgayVanDon.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_VANDON"].Value = null;
            nodeToKhai.Attributes["NUOC_XK"].Value = tkmd.NuocXK_ID;
            if (tkmd.DiaDiemXepHang.Length <= 40)
                nodeToKhai.Attributes["CANG_XUAT"].Value = tkmd.DiaDiemXepHang;
            else
                nodeToKhai.Attributes["CANG_XUAT"].Value = tkmd.DiaDiemXepHang.Substring(0, 40);
            nodeToKhai.Attributes["MA_CK_NHAP"].Value = tkmd.CuaKhau_ID;
            nodeToKhai.Attributes["MA_DKGH"].Value = tkmd.DKGH_ID;
            nodeToKhai.Attributes["MA_NGTE"].Value = tkmd.NguyenTe_ID;
            nodeToKhai.Attributes["TY_GIA_VND"].Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(tkmd.TyGiaTinhThue, 9);
            nodeToKhai.Attributes["MA_PTTT"].Value = tkmd.PTTT_ID;
            if (tkmd.GiayTo.Length > 40)
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = tkmd.GiayTo.Substring(0, 40);
            else
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = tkmd.GiayTo;
            nodeToKhai.Attributes["SO_CONT20"].Value = Convert.ToUInt32(tkmd.SoContainer20).ToString();
            nodeToKhai.Attributes["SO_CONT40"].Value = Convert.ToUInt32(tkmd.SoContainer40).ToString();
            nodeToKhai.Attributes["SO_PLTK"].Value = Convert.ToUInt32(tkmd.SoLuongPLTK).ToString();
            nodeToKhai.Attributes["SO_KIEN"].Value = Convert.ToUInt32(tkmd.SoKien).ToString();
            nodeToKhai.Attributes["TRONG_LUONG"].Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(tkmd.TrongLuong, 9);
            nodeToKhai.Attributes["PHI_BH"].Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(tkmd.PhiBaoHiem, 9);
            decimal PhiKhacNH = tkmd.PhiKhac + tkmd.PhiVanChuyen;
            nodeToKhai.Attributes["PHI_VC"].Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(PhiKhacNH, 9);
            nodeToKhai.Attributes["LE_PHI_HQ"].Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(tkmd.LePhiHaiQuan, 9);
            if (tkmd.TenChuHang.Length > 30)
                nodeToKhai.Attributes["CHU_HANG"].Value = tkmd.TenChuHang.Substring(0, 30);
            else
                nodeToKhai.Attributes["CHU_HANG"].Value = tkmd.TenChuHang;

            //hang hoa
            if (tkmd.HMDCollection == null || tkmd.HMDCollection.Count == 0)
            {
                Company.BLL.KDT.HangMauDich hmd = new Company.BLL.KDT.HangMauDich();
                hmd.TKMD_ID = tkmd.ID;
                tkmd.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
            }
            XmlNode nodeHang = docNPL.GetElementsByTagName("HANG")[0];
            foreach (Company.BLL.KDT.HangMauDich hmd in tkmd.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("HANG.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT_HANG");
                sttAtt.Value = hmd.SoThuTuHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_HANG");

                maAtt.Value = hmd.MaPhu;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = hmd.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_HANG");
                TenAtt.Value = hmd.TenHang;
                node.Attributes.Append(TenAtt);

                XmlAttribute NuocAtt = docNPL.CreateAttribute("NUOC_XX");
                NuocAtt.Value = hmd.NuocXX_ID;
                node.Attributes.Append(NuocAtt);

                XmlAttribute SoLuongAtt = docNPL.CreateAttribute("LUONG");
                SoLuongAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.SoLuong, 9);
                node.Attributes.Append(SoLuongAtt);

                XmlAttribute Ma_DVTAtt = docNPL.CreateAttribute("MA_DVT");
                Ma_DVTAtt.Value = hmd.DVT_ID;
                node.Attributes.Append(Ma_DVTAtt);

                XmlAttribute DonGiaAtt = docNPL.CreateAttribute("DGIA_NT");
                DonGiaAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.DonGiaKB, 9);
                node.Attributes.Append(DonGiaAtt);

                XmlAttribute TriGiaAtt = docNPL.CreateAttribute("TGIA_NT");
                TriGiaAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.TriGiaKB, 9);
                node.Attributes.Append(TriGiaAtt);

                XmlAttribute DonGiaVNDAtt = docNPL.CreateAttribute("DGIA_TT_VND");
                decimal dongiatt = hmd.TriGiaTT / hmd.SoLuong;
                DonGiaVNDAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(dongiatt, 9);
                node.Attributes.Append(DonGiaVNDAtt);

                XmlAttribute TriGiaVNDAtt = docNPL.CreateAttribute("TGIA_TT_VND");
                TriGiaVNDAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.TriGiaTT, 9);
                node.Attributes.Append(TriGiaVNDAtt);

                XmlAttribute TS_XNKAtt = docNPL.CreateAttribute("TS_XNK");
                TS_XNKAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.Attributes.Append(TS_XNKAtt);

                XmlAttribute TS_TTDBAtt = docNPL.CreateAttribute("TS_TTDB");
                TS_TTDBAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.ThueSuatTTDB, 9);
                node.Attributes.Append(TS_TTDBAtt);

                XmlAttribute TS_VSTAtt = docNPL.CreateAttribute("TS_VAT");
                TS_VSTAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(TS_VSTAtt);

                XmlAttribute TL_PhuThuAtt = docNPL.CreateAttribute("TL_PHU_THU");
                TL_PhuThuAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.Attributes.Append(TL_PhuThuAtt);

                XmlAttribute ThueXNKAtt = docNPL.CreateAttribute("THUE_XNK");
                ThueXNKAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.ThueXNK, 9);
                node.Attributes.Append(ThueXNKAtt);

                XmlAttribute ThueTTDBAtt = docNPL.CreateAttribute("THUE_TTDB");
                ThueTTDBAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.ThueTTDB, 9);
                node.Attributes.Append(ThueTTDBAtt);

                XmlAttribute ThueVATAtt = docNPL.CreateAttribute("THUE_VAT");
                ThueVATAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(ThueVATAtt);

                XmlAttribute TPhuThuAtt = docNPL.CreateAttribute("PHU_THU");
                TPhuThuAtt.Value = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hmd.PhuThu, 9);
                node.Attributes.Append(TPhuThuAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        public static bool WSKhaiBaoSuaToKhai(Company.BLL.KDT.ToKhaiMauDich tkmd, MessageTypes messageType, MessageFunctions messageFunction)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.ToKhaiNhap, MessgaseFunction.KhaiBao));

            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXMLSuaToKhai());

            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.Attributes["SOTN"].Value = this.SoTiepNhan.ToString();
            root.Attributes["NAMTN"].Value = this.NamDK.ToString();

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            //--------Khai dinh kem --------------
            XmlNode DU_LEU = doc.GetElementsByTagName("DU_LIEU")[0];

            XmlNode node = CO.ConvertCollectionCOToXML(doc, this.COCollection, this.ID);

            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            listHang = ConvertHMDKDToHangMauDich();
            if (node != null)
                DU_LEU.AppendChild(node);

            node = GiayPhep.ConvertCollectionGiayPhepToXML(doc, this.GiayPhepCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            node = HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML(doc, this.HoaDonThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            node = HopDongThuongMai.ConvertCollectionHopDongToXML(doc, this.HopDongThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);


            if (VanTaiDon == null)
            {
                List<VanDon> VanDonCollection = VanDon.SelectCollectionBy_TKMD_ID(this.ID);
                if (VanDonCollection != null && VanDonCollection.Count > 0)
                {
                    VanTaiDon = VanDonCollection[0];
                    VanTaiDon.LoadContainerCollection();
                }
            }
            if (VanTaiDon != null)
            {
                node = this.VanTaiDon.ConvertVanDonToXML(doc);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            node = DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(doc, this.listChuyenCuaKhau, this.ID);
            if (node != null)
                DU_LEU.AppendChild(node);

            //tkem
            ChungTuKem ctct = new ChungTuKem();

            List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet> listCTCT = new List<ChungTuKemChiTiet>();
            listCTCT = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(ctct.LoadCT(this.ID));
            if (listCTCT != null && listCTCT.Count != 0)
            {
                node = ChungTuKem.ConvertCollectionCTDinhKemToXML(doc, ChungTuKemCollection, this.ID, listCTCT);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            //----------------End Khai dinh kem --------------------

            //Service
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            
            string kq = "";
            try
            {
                // Lưu message.
                Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                message.ItemID = chungTuKem.ID;
                message.ReferenceID = new Guid(chungTuKem.GUIDSTR);
                message.MessageType = MessageTypes.ToKhaiNhap;
                message.MessageFunction = MessageFunctions.KhaiBao;
                message.MessageFrom = maDoanhNghiep;
                message.MessageTo = maHaiQuan.Trim();
                message.MessageContent = xmlDocument.InnerXml;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKem;
                message.NoiDungThongBao = string.Empty;
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);

                    // Lưu message.
                    Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                    message.ItemID = chungTuKem.ID;
                    message.ReferenceID = new Guid(chungTuKem.GUIDSTR);
                    message.MessageType = MessageTypes.ThongTin;
                    message.MessageFunction = MessageFunctions.PhanHoi;
                    message.MessageFrom = maHaiQuan.Trim();
                    message.MessageTo = maDoanhNghiep;
                    message.MessageContent = xmlDocument.InnerXml;
                    message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKem;
                    message.NoiDungThongBao = string.Empty;
                    message.CreatedTime = DateTime.Now;
                    message.Insert();

                    return true;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return false;

        }
 */
        #endregion
        
        #region KHAI BO SUNG CHUNG TU dang anh

        public bool WSKhaiBaoChungTuDinhKem(ChungTuKem chungTuKem, string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long TKMD_ID, string maDoanhNghiep, List<ChungTuKemChiTiet> list, MessageTypes messageType, MessageFunctions messageFunction)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep;
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep;

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan;
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan;

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            chungTuKem.GUIDSTR = nodeReference.InnerText = System.Guid.NewGuid().ToString();
            chungTuKem.Update();
            if (chungTuKem.GUIDSTR == "")
            {
                chungTuKem.GUIDSTR = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = chungTuKem.GUIDSTR;
                chungTuKem.Update();
            }

            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep;
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep;

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan;
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan;

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";


            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<ChungTuKem> listChungTuKem = new List<ChungTuKem>();
            listChungTuKem.Add(chungTuKem);
            xmlNodeDulieu.AppendChild(ChungTuKem.ConvertCollectionCTDinhKemToXML(xmlDocument, listChungTuKem, TKMD_ID, list));

            //Service
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                // Lưu message.
                Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                message.ItemID = chungTuKem.ID;
                message.ReferenceID = new Guid(chungTuKem.GUIDSTR);
                message.MessageType = MessageTypes.ToKhaiNhap;
                message.MessageFunction = MessageFunctions.KhaiBao;
                message.MessageFrom = maDoanhNghiep;
                message.MessageTo = maHaiQuan.Trim();
                message.MessageContent = xmlDocument.InnerXml;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKem;
                message.NoiDungThongBao = string.Empty;
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);

                    // Lưu message.
                    Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                    message.ItemID = chungTuKem.ID;
                    message.ReferenceID = new Guid(chungTuKem.GUIDSTR);
                    message.MessageType = MessageTypes.ThongTin;
                    message.MessageFunction = MessageFunctions.PhanHoi;
                    message.MessageFrom = maHaiQuan.Trim();
                    message.MessageTo = maDoanhNghiep;
                    message.MessageContent = xmlDocument.InnerXml;
                    message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKem;
                    message.NoiDungThongBao = string.Empty;
                    message.CreatedTime = DateTime.Now;
                    message.Insert();

                    return true;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return false;

        }

        /// <summary>
        /// Lấy số tiếp nhận khai báo.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLaySoTiepNhan(ChungTuKem chungTukem, string password)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(chungTukem.GUIDSTR), MessageTypes.ThongTin, MessageFunctions.PhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                chungTukem.SOTN = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                chungTukem.NGAYTN = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                chungTukem.Update();

                Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                message.ItemID = chungTukem.ID;
                message.ReferenceID = new Guid(chungTukem.GUIDSTR);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.PhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKemThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", chungTukem.SOTN, chungTukem.NGAYTN.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Nhận phản hồi thông quan điện tử sau khi khai báo đã có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLayPhanHoi(ChungTuKem chungTukem, string password)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(chungTukem.GUIDSTR), MessageTypes.ThongTin, MessageFunctions.PhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                chungTukem.SOTN = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                chungTukem.NGAYTN = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                chungTukem.Update();

                Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                message.ItemID = chungTukem.ID;
                message.ReferenceID = new Guid(chungTukem.GUIDSTR);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.PhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKemThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", chungTukem.SOTN, chungTukem.NGAYTN.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }
            else if (xmlNodeResult.Attributes["SOTN"] != null)
            {
                Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                message.ItemID = chungTukem.ID;
                message.ReferenceID = new Guid(chungTukem.GUIDSTR);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.PhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKemDuocChapNhan;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã được Hải quan chấp nhận.", chungTukem.SOTN, chungTukem.NGAYTN.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }
            else if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
            {
                Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                message.ItemID = chungTukem.ID;
                message.ReferenceID = new Guid(chungTukem.GUIDSTR);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.PhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.TuChoiTiepNhan;
                string lydo = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", chungTukem.SOTN, chungTukem.NGAYTN.ToShortDateString(), lydo);
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }

            return false;
        }


        #endregion

        #region Ket qua xu ly
        public static void KetQuaXuLy(BLL.KDT.ToKhaiMauDich tkmd)
        {
            KetQuaXuLyForm form = new KetQuaXuLyForm();
            form.ItemID = tkmd.ID;
            form.ShowDialog();
        }
        #endregion

        #region Ket qua xu ly bo sung

        public static void ShowKetQuaXuLyBoSung(string guidStr)
        {
            KetQuaXuLyBoSungForm form = new KetQuaXuLyBoSungForm();
            form.refId = guidStr;
            form.ShowDialog();
        }

        #endregion

        #region Ket qua xu ly San pham
        public static void KetQuaXuLySanPham(BLL.KDT.SXXK.SanPhamDangKySUA spdkSUA)
        {
            KetQuaXuLyForm form = new KetQuaXuLyForm();
            form.ItemID = spdkSUA.ID;
            form.ShowDialog();
        }

        public static void KetQuaXuLyNguyenPhuLieu(BLL.KDT.SXXK.NguyenPhuLieuDangKySUA npldkSUA)
        {
            KetQuaXuLyForm form = new KetQuaXuLyForm();
            form.ItemID = npldkSUA.ID;
            form.ShowDialog();
        }

        public static void KetQuaXuLyChung(long id)
        {
            KetQuaXuLyForm form = new KetQuaXuLyForm();
            form.ItemID = id;
            form.ShowDialog();
        }
        #endregion

        public static decimal tinhDinhMucChung(decimal dm, decimal tylehaohut)
        {
            decimal dmc = dm + (dm * tylehaohut / 100);
            return dmc;
        }
        
        #region VALUE LIST

        private static System.Data.DataTable dtHS;
        private static System.Data.DataTable dtNguyenTe;
        private static System.Data.DataTable dtDonViTinh;
        private static System.Data.DataTable dtNuoc;

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillHSValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;

                valueList = col.ValueList;

                if (dtHS == null)
                    dtHS = MaHS.SelectAll();

                System.Data.DataView view = dtHS.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["HS10So"], (string)row["HS10So"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillNguyenTeValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;
                valueList = col.ValueList;

                if (dtNguyenTe == null)
                    dtNguyenTe = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.SelectAll().Tables[0];

                System.Data.DataView view = dtNguyenTe.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"], (string)row["Ten"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillDonViTinhValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;
                valueList = col.ValueList;

                if (dtDonViTinh == null)
                    dtDonViTinh = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];

                System.Data.DataView view = dtDonViTinh.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"], (string)row["Ten"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillNuocXXValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;
                valueList = col.ValueList;

                if (dtNuoc == null)
                    dtNuoc = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.SelectAll().Tables[0];

                System.Data.DataView view = dtNuoc.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"], (string)row["Ten"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        #endregion

        #region File, Folder

        /// <summary>
        /// Đọc file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>byte[]</returns>
        public static byte[] ReadFile(string filePath)
        {
            byte[] filedata = new byte[0];

            try
            {
                // provide read access to the file
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);

                // Create a byte array of file stream length
                filedata = new byte[fs.Length];

                //Read block of bytes from stream into the byte array
                fs.Read(filedata, 0, System.Convert.ToInt32(fs.Length));

                //Close the File Stream
                fs.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return filedata;
        }

        /// <summary>
        /// Ghi file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="data"></param>
        public static bool WriteFile(string filePath, byte[] data)
        {
            try
            {
                // provide read access to the file
                FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite);

                //Read block of bytes from stream into the byte array
                fs.Write(data, 0, data.Length);

                //Close the File Stream
                fs.Close();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

        }


        #endregion

        static System.Drawing.Image _ResourceWait;
        public static System.Drawing.Image ResourceWait
        {
            get
            {
                return _ResourceWait;
            }
        }
        static Globals()
        {
            try
            {
                System.IO.Stream stream;
                System.Reflection.Assembly assembly;

                assembly = System.Reflection.Assembly.LoadFrom(Application.ExecutablePath);
                stream = assembly.GetManifestResourceStream("Company.Interface.Image.wait.gif");
                _ResourceWait = System.Drawing.Image.FromStream(stream);
            }
            catch { }
        }
        public static string ConfigPhongBiPhanHoi(int type, int function, string maHaiQuan, string maDoanhNghiep, string GUIDSTR)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                string path = Company.BLL.EntityBase.GetPathProgram();
                doc.Load(path + "\\TemplateXML\\PhongBi.xml");
                //set thong tin hai quan den
                XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
                XmlNode nodeName = nodeTo.ChildNodes[0];
                nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(maHaiQuan);
                XmlNode nodeIdentity = nodeTo.ChildNodes[1];
                nodeIdentity.InnerText = maHaiQuan.Trim();

                //set thong so gui di subject
                XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
                XmlNode nodeType = nodeSubject.ChildNodes[0];
                nodeType.InnerText = type.ToString();

                XmlNode nodeFunction = nodeSubject.ChildNodes[1];
                nodeFunction.InnerText = function.ToString();

                XmlNode nodeReference = nodeSubject.ChildNodes[2];
                nodeReference.InnerText = GUIDSTR;

                XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
                nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

                XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
                nodeFrom.ChildNodes[1].InnerText = maDoanhNghiep.Trim();

                return doc.InnerXml;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        public static string GetStatus(int Status)
        {
            if (Status == TrangThaiXuLy.DA_DUYET)
                return "Đã duyệt";
            else if (Status == TrangThaiXuLy.CHO_DUYET)
                return "Chờ duyệt";
            if (Status == TrangThaiXuLy.CHUA_KHAI_BAO)
                return "Chưa khai báo";
            if (Status == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                return "Đã khai báo";
            if (Status == TrangThaiXuLy.SUATKDADUYET)
                return "Đang sửa";
            if (Status == TrangThaiXuLy.SUA_KHAI_BAO)
                return "Đã khai báo sửa";
            if (Status == TrangThaiXuLy.CHO_HUY)
                return "Chờ hủy";
            if (Status == TrangThaiXuLy.HUY_KHAI_BAO)
                return "Khai báo hủy";
            if (Status == TrangThaiXuLy.DA_HUY)
                return "Đã hủy";
            if (Status == TrangThaiXuLy.KHONG_PHE_DUYET)
                return "Không phê duyệt";
            else
                return "";
        }
        #region Export Excel
        public enum ToKhaiType
        {
            Nhap,
            Xuat,
            PhucLucTKN,
            PhuLucTKX,
            BCXuatNhapTon
        }

        public static void ExportExcel(ToKhaiType typeTK, long tkmdID, bool banLuuHaiQuan, bool inMaHang, bool mienThueXuatKhau, bool mienThueGTGT, string pls)
        {
            Company.BLL.KDT.ToKhaiMauDich TKMD = new Company.BLL.KDT.ToKhaiMauDich();
            TKMD.ID = tkmdID;
            TKMD.Load();
            TKMD.LoadHMDCollection();
            TKMD.LoadChungTuHaiQuan();

            if (TKMD == null)
            {
                Globals.ShowMessage("Không tồn tại thông tin của tờ khai ID = " + tkmdID + ".", false);
                return;
            }

            if (typeTK == ToKhaiType.Nhap)
                ExportExcelTKN(TKMD, banLuuHaiQuan, inMaHang, mienThueXuatKhau, mienThueGTGT);
            else if (typeTK == ToKhaiType.Xuat)
                ExportExcelTKX(TKMD, banLuuHaiQuan, inMaHang, mienThueXuatKhau, mienThueGTGT);
            else if (typeTK == ToKhaiType.PhucLucTKN)
                ExportExcelPhuLucTKN(TKMD, banLuuHaiQuan, inMaHang, mienThueXuatKhau, mienThueGTGT, pls);
            else if (typeTK == ToKhaiType.PhuLucTKX)
                ExportExcelPhuLucTKX(TKMD, banLuuHaiQuan, inMaHang, mienThueXuatKhau, mienThueGTGT, pls);
        }

        private static void ExportExcelTKN(Company.BLL.KDT.ToKhaiMauDich TKMD, bool banLuuHaiQuan, bool inMaHang, bool mienThueXuatKhau, bool mienThueGTGT)
        {
            bool ok = true;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            Infragistics.Excel.WorksheetMergedCellsRegion mergedRegion;
            string tenHang = "";
            /*Số thập phân*/
            int soThapPhanSoLuong = 0;
            int soThapPhanDonGia = 10;
            int soThapPhanTriGiaNguyenTe = 2;
            int soThapPhanTriGiaTinhThue = 0;
            int soThapPhanThueSuat = 0;
            int soThapPhanTienThue = 0;
            int soThapPhanTyLethuKhac = 0;
            int soThapPhanTienThuKhac = 0;
            int soThapPhanTongTienThue = 0;
            int soThapPhanTyGia = 2;

            if (ok)
            {
                //Lấy đường dẫn file gốc
                string destFile = "";
                string fileName = "TKN_TQDT_2009_TT222.xls";

                string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

                //Kiem tra file Excel mau co ton tai?
                if (!System.IO.File.Exists(sourcePath))
                {
                    ShowMessage("Không tồn tại file mẫu Excel.", false);
                    return;
                }

                //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
                try
                {
                    Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                    Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];
                    DateTime minDate = new DateTime(1900, 1, 1);

                    //Chi cục Hải quan
                    workSheet.GetCell("E5").Value = GlobalSettings.TEN_HAI_QUAN;
                    //Chi cục Hải quan cửa khẩu
                    workSheet.GetCell("H6").Value = TKMD.CuaKhau_ID + "-" + Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);

                    //TODO: Cao Hữu Tú:updated:09-09-2011
                    // nội dung: lấy số ID của tờ khai làm số tham chiếu
                    if (TKMD.ID > 0)
                        workSheet.GetCell("R5").Value = TKMD.ID;
                    else
                    {

                        MessageBox.Show("ID của Tờ Khai <=0 sẽ được gán =0");
                        workSheet.GetCell("R5").Value = 0;
                    }


                    //Ngày, giờ gửi
                    if (TKMD.NgayTiepNhan > minDate)
                        workSheet.GetCell("Q6").Value = TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("Q6").Value = "";

                    //Số tờ khai
                    if (TKMD.SoToKhai > 0)
                        workSheet.GetCell("AB5").Value = TKMD.SoToKhai + "";
                    else
                        workSheet.GetCell("AB5").Value = "";
                    //Ngày, giờ đăng ký
                    if (TKMD.NgayDangKy > minDate)
                        workSheet.GetCell("AB6").Value = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("AB6").Value = "";

                    if (TKMD.MaLoaiHinh.Substring(0, 1) == "X")
                    {
                        //1.Người xuất khẩu A8
                        workSheet.GetCell("A8").Value = (TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI).ToUpper();
                        //2.Người nhập khẩu A10
                        workSheet.GetCell("A10").Value = TKMD.TenDonViDoiTac.ToUpper();
                    }
                    else if (TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                    {
                        //1.Người xuất khẩu A8
                        workSheet.GetCell("A8").Value = TKMD.TenDonViDoiTac.ToUpper();
                        //2.Người nhập khẩu A10
                        workSheet.GetCell("A10").Value = (TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI).ToUpper();
                    }

                    //3.Người ủy thác A12
                    workSheet.GetCell("A12").Value = TKMD.TenDonViUT;
                    //4.Đại lý làm thủ tục Hải quan A14
                    workSheet.GetCell("A14").Value = TKMD.MaDaiLyTTHQ + "." + TKMD.TenDaiLyTTHQ;
                    //5. Loại hình P7
                    string stlh = "";
                    stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                    workSheet.GetCell("P7").Value = TKMD.MaLoaiHinh + stlh;
                    //6. Hóa đơn thương mại M9, Ngày O10
                    workSheet.GetCell("M9").Value = TKMD.SoHoaDonThuongMai;
                    if (TKMD.NgayHoaDonThuongMai > minDate)
                        workSheet.GetCell("O10").Value = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("O10").Value = "";
                    //7. Giấy phép U8, Ngày U9, Ngày hết hạn U10
                    if (TKMD.SoGiayPhep != "")
                        workSheet.GetCell("U8").Value = "" + TKMD.SoGiayPhep;
                    else
                        workSheet.GetCell("U8").Value = "";

                    if (TKMD.NgayGiayPhep > minDate)
                        workSheet.GetCell("U9").Value = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("U9").Value = "";

                    if (TKMD.NgayHetHanGiayPhep > minDate)
                        workSheet.GetCell("U10").Value = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("U10").Value = "";
                    //8. Hợp đồng AA8, Ngày AB9, Ngày hết hạn AB10
                    workSheet.GetCell("AA8").Value = "" + TKMD.SoHopDong;

                    if (TKMD.NgayHopDong > minDate)
                        workSheet.GetCell("AB9").Value = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("AB9").Value = "";

                    if (TKMD.NgayHetHanHopDong > minDate)
                        workSheet.GetCell("AB10").Value = "" + TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("AB10").Value = "";

                    //9. Vận tải đơn P11, Ngày N12
                    workSheet.GetCell("P11").Value = TKMD.SoVanDon;
                    if (TKMD.NgayVanDon > minDate)
                        workSheet.GetCell("N12").Value = TKMD.NgayVanDon.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("N12").Value = "";
                    //10. Cảng xếp hàng R12
                    workSheet.GetCell("R12").Value = TKMD.DiaDiemXepHang;
                    //11 Cảng dỡ hàng AB11, Tên cảng X12
                    workSheet.GetCell("AB11").Value = TKMD.CuaKhau_ID;
                    workSheet.GetCell("X12").Value = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);
                    //12. Phương tiện vận tải R13, Tên M14
                    workSheet.GetCell("R13").Value = " " + TKMD.SoHieuPTVT;
                    workSheet.GetCell("M14").Value = Company.KDT.SHARE.Components.DuLieuChuan.PhuongThucVanTai.getName(TKMD.PTVT_ID);
                    //13. Nước xuất khẩu AC13, Tên X14
                    workSheet.GetCell("AC13").Value = TKMD.NuocXK_ID;
                    workSheet.GetCell("X14").Value = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(TKMD.NuocXK_ID);
                    //14. Điều kiện giao hàng R15
                    workSheet.GetCell("R15").Value = TKMD.DKGH_ID;
                    //15. Phương thức thanh toán AA15
                    workSheet.GetCell("AA15").Value = TKMD.PTTT_ID;
                    //16. Đồng tiền thanh toán S16
                    workSheet.GetCell("S16").Value = TKMD.NguyenTe_ID;
                    //17. Tỷ giá tính thuế Z16
                    workSheet.GetCell("Z16").Value = TKMD.TyGiaTinhThue.ToString("G10");
                    //18. Kết quả phân luồng và hướng dẫn làm thủ tục Hải quan: A18
                    workSheet.GetCell("A18").Value = TKMD.HUONGDAN;
                    //19. Chứng từ Hải quan trước: X18

                    /*THÔNG TIN HÀNG*/
                    //20. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                    //21. Mã số hàng hóa: K22, K23, K24
                    //22. Xuất xứ: O22, O23, O24
                    //23. Số lượng: R22, R23, R24
                    //24. Đơn vị tính: U22, U23, U24
                    //25. Đơn giá nguyên tệ: X22, X23, X24
                    //26. Trị giá nguyên tệ: AB22, AN23, AB24
                    //Cộng: AD25

                    //Truoc 26. Phi bao hiem B25
                    string st = "";
                    if (TKMD.PhiBaoHiem > 0)
                        st = "I = " + TKMD.PhiBaoHiem.ToString("N2");
                    if (TKMD.PhiVanChuyen > 0)
                        st += " F = " + TKMD.PhiVanChuyen.ToString("N2");
                    if (TKMD.PhiKhac > 0)
                        st += " Phí khác = " + TKMD.PhiKhac.ToString("N2");
                    workSheet.GetCell("B25").Value = st;

                    /*THÔNG TIN THUẾ*/
                    //27. Thuế nhập khẩu
                    //Trị giá tính thuế: B28, B29, B30
                    //Thuế suất (%): F28, F29, F30
                    //Tiền thuế: I28, I29, I30
                    //Cộng: K31
                    //28. Thuế GTGT (hoặc TTĐB)
                    //Trị giá tính thuế: L28, L29, L30
                    //Thuế suất (%): P28, P29, P30
                    //Tiền thuế: S28, S29, S30
                    //29. Thu khác
                    //Tỷ lệ (%): W28, W29, W30
                    //Số tiền: AA28, AA29, AA30
                    //30. Tổng số tiền thuế và thu khác (ô 27+28 + 29) bằng số: A32, Bằng chữ E33

                    if (TKMD.HMDCollection.Count > 0 && TKMD.HMDCollection.Count <= 3)
                    {
                        #region Chi tiet hang hoa

                        Company.BLL.KDT.HangMauDich hmd = null;
                        for (int i = 0; i < TKMD.HMDCollection.Count; i++)
                        {
                            hmd = TKMD.HMDCollection[i];

                            #region THÔNG TIN HÀNG
                            //20. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                            if (!inMaHang)
                            {
                                if (hmd.Ma_HTS.Trim().Length > 0)
                                    tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                                else
                                    tenHang = hmd.TenHang;
                            }
                            else
                            {
                                if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                                    tenHang = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                                else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                                    tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                                else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                                    tenHang = hmd.TenHang + "/" + hmd.MaPhu;
                                else
                                    tenHang = hmd.TenHang;
                            }
                            workSheet.Rows[workSheet.GetCell("B22").RowIndex + i].Cells[1].Value = tenHang;

                            //21. Mã số hàng hóa: K22, K23, K24
                            // Create a merged region
                            mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("K22").RowIndex + i, 11, workSheet.GetCell("K22").RowIndex + i, 13);
                            mergedRegion.Value = hmd.MaHS;
                            mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                            //19. Xuất xứ: O22, O23, O24
                            mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("O22").RowIndex + i, 14, workSheet.GetCell("O22").RowIndex + i, 16);
                            mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                            mergedRegion.Value = hmd.NuocXX_ID;
                            //20. Số lượng: R22, R23, R24 hoac T22
                            workSheet.Rows[workSheet.GetCell("T22").RowIndex + i].Cells[workSheet.GetCell("T22").ColumnIndex].Value = hmd.SoLuong.ToString("N" + soThapPhanSoLuong);
                            workSheet.Rows[workSheet.GetCell("T22").RowIndex + i].Cells[workSheet.GetCell("T22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //21. Đơn vị tính: U22, U23, U24
                            mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("U22").RowIndex + i, 20, workSheet.GetCell("U22").RowIndex + i, 22);
                            mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                            mergedRegion.Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(hmd.DVT_ID);
                            //22. Đơn giá nguyên tệ: X22, X23, X24 hoac AA22
                            workSheet.Rows[workSheet.GetCell("AA22").RowIndex + i].Cells[workSheet.GetCell("AA22").ColumnIndex].Value = hmd.DonGiaKB.ToString("G" + soThapPhanDonGia);
                            workSheet.Rows[workSheet.GetCell("AA22").RowIndex + i].Cells[workSheet.GetCell("AA22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //23. Trị giá nguyên tệ: AB22, AN23, AB24 hoac AD22
                            workSheet.Rows[workSheet.GetCell("AD22").RowIndex + i].Cells[workSheet.GetCell("AD22").ColumnIndex].Value = hmd.TriGiaKB.ToString("N" + soThapPhanTriGiaNguyenTe);
                            workSheet.Rows[workSheet.GetCell("AD22").RowIndex + i].Cells[workSheet.GetCell("AD22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                            tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);

                            #endregion

                            #region THÔNG TIN THUẾ

                            if (!mienThueXuatKhau)
                            {
                                //27. Thuế nhập khẩu
                                //Trị giá tính thuế: E28
                                workSheet.Rows[workSheet.GetCell("E28").RowIndex + i].Cells[workSheet.GetCell("E28").ColumnIndex].Value = hmd.TriGiaTT.ToString("N" + soThapPhanTriGiaTinhThue);
                                workSheet.Rows[workSheet.GetCell("E28").RowIndex + i].Cells[workSheet.GetCell("E28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Thuế suất (%): H28
                                workSheet.Rows[workSheet.GetCell("H28").RowIndex + i].Cells[workSheet.GetCell("H28").ColumnIndex].Value = hmd.ThueSuatXNK.ToString("N" + soThapPhanThueSuat);
                                workSheet.Rows[workSheet.GetCell("H28").RowIndex + i].Cells[workSheet.GetCell("H28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Tiền thuế: K28
                                workSheet.Rows[workSheet.GetCell("K28").RowIndex + i].Cells[workSheet.GetCell("K28").ColumnIndex].Value = hmd.ThueXNK.ToString("N" + soThapPhanTienThue);
                                workSheet.Rows[workSheet.GetCell("K28").RowIndex + i].Cells[workSheet.GetCell("K28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            }

                            if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                            {
                                decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                if (!mienThueXuatKhau)
                                {
                                    //28. Thuế GTGT
                                    //Trị giá tính thuế: O28
                                    workSheet.Rows[workSheet.GetCell("O28").RowIndex + i].Cells[workSheet.GetCell("O28").ColumnIndex].Value = TriGiaTTGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("O28").RowIndex + i].Cells[workSheet.GetCell("O28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                    //Tỷ lệ (%): R28
                                    workSheet.Rows[workSheet.GetCell("R28").RowIndex + i].Cells[workSheet.GetCell("R28").ColumnIndex].Value = hmd.ThueSuatGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("R28").RowIndex + i].Cells[workSheet.GetCell("R28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                    //Tiền thuế: V28
                                    workSheet.Rows[workSheet.GetCell("V28").RowIndex + i].Cells[workSheet.GetCell("V28").ColumnIndex].Value = hmd.ThueGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("V28").RowIndex + i].Cells[workSheet.GetCell("V28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                                    //29. Thu khác
                                    //Tỷ lệ (%): Z28
                                    workSheet.Rows[workSheet.GetCell("Z28").RowIndex + i].Cells[workSheet.GetCell("Z28").ColumnIndex].Value = hmd.TyLeThuKhac.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("Z28").RowIndex + i].Cells[workSheet.GetCell("Z28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                    //Số tiền: AD28
                                    workSheet.Rows[workSheet.GetCell("AD28").RowIndex + i].Cells[workSheet.GetCell("AD28").ColumnIndex].Value = hmd.TriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                                    workSheet.Rows[workSheet.GetCell("AD28").RowIndex + i].Cells[workSheet.GetCell("AD28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                }

                                tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                            {
                                decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                            {
                                decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                            }

                            //Tiền thuế Xuất nhập khẩu
                            tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                            //Tổng tiền thuế và thu khác
                            tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);

                            #endregion
                        }

                        #endregion
                    }
                    //CHI TIET DINH KEM
                    else
                    {
                        #region CHI TIET DINH KEM

                        //17. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                        workSheet.GetCell("B22").Value = "HÀNG HÓA NHẬP";
                        workSheet.GetCell("B23").Value = "(Chi tiết theo phụ lục đính kèm)";

                        foreach (Company.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
                        {
                            tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);
                            tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);
                            tongTriGiaTT += Convert.ToDecimal(hmd.TriGiaTT);
                            if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                            {
                                tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                            {
                                tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                            {
                                tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                            }
                            //Tiền thuế Xuất nhập khẩu
                            //tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                            //Tổng tiền thuế và thu khác
                            tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);
                        }

                        #endregion
                    }

                    //Linhhtn - Không công phí VC và các phí khác vào Tổng trị giá nguyên tệ
                    //tongTriGiaNT += Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);

                    //23. Trị giá nguyên tệ: Cộng: AD25
                    workSheet.GetCell("AD25").Value = tongTriGiaNT.ToString("N" + soThapPhanTriGiaNguyenTe);
                    workSheet.GetCell("AD25").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //24. Thuế nhập khẩu: Cộng: K31
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("K31").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("K31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    workSheet.GetCell("K31").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("K31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế GTGT
                    //if (tongThueGTGT > 0)
                    //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                    //else
                    //    lblTongTienThueGTGT.Text = "";
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("V31").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("V31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    workSheet.GetCell("V31").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("V31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế thu khác
                    if (tongTriGiaThuKhac > 0 && TKMD.HMDCollection.Count <= 3)
                    {
                        workSheet.GetCell("AD31").Value = tongTriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                        workSheet.GetCell("AD31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    }

                    //26. Tổng số tiền thuế và thu khác (ô 24 + 25) bằng số: A32, Bằng chữ E33
                    //mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("A32").RowIndex, 1, workSheet.GetCell("A32").RowIndex, 20);
                    //mergedRegion.Value = tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);
                    workSheet.GetCell("A32").Value += " " + tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);

                    //Bằng chữ
                    string s = Company.BLL.Utils.VNCurrency.ToString(tongTienThueTatCa).Trim();
                    s = s[0].ToString().ToUpper() + s.Substring(1);
                    workSheet.GetCell("E33").Value = s.Replace("  ", " ");
                    workSheet.GetCell("E33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Left;

                    //31. Tổng trọng lượng F34, Số hiệu kiện, cont: F35, Tổng số container: W35
                    if (TKMD.TrongLuong > 0)
                        workSheet.GetCell("F34").Value = TKMD.TrongLuong + " kg ";
                    else
                        workSheet.GetCell("F34").Value = "";
                    //Số hiệu kiện, cont: F35
                    if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
                        workSheet.GetCell("F35").Value = "danh sách container theo bảng kê đính kèm";
                    else
                    {
                        string soHieuKien = "";
                        Company.KDT.SHARE.QuanLyChungTu.Container objContainer = null;

                        if (TKMD.VanTaiDon != null)
                        {
                            for (int cnt = 0; cnt < TKMD.VanTaiDon.ContainerCollection.Count; cnt++)
                            {
                                objContainer = TKMD.VanTaiDon.ContainerCollection[cnt];

                                soHieuKien += objContainer.SoHieu + "/" + objContainer.Seal_No;

                                if (cnt < TKMD.VanTaiDon.ContainerCollection.Count - 1)
                                {
                                    soHieuKien += "; ";
                                }
                            }
                        }
                        workSheet.GetCell("F35").Value = soHieuKien;
                    }
                    //Tổng số container: W35
                    string tsContainer = "";
                    string cont20 = "", cont40 = "", soKien = "";
                    if (TKMD.SoContainer20 > 0)
                    {
                        cont20 = "Cont20: " + Convert.ToInt32(TKMD.SoContainer20);

                        tsContainer += cont20 + "; ";
                    }
                    else
                        cont20 = "";

                    if (TKMD.SoContainer40 > 0)
                    {
                        cont40 = "Cont40: " + Convert.ToInt32(TKMD.SoContainer40);

                        tsContainer += cont40 + "; ";
                    }
                    else
                        cont40 = "";

                    if (TKMD.SoKien > 0)
                    {
                        //TODO: Cao Hữu Tú: updated 09-09-2011
                        //contents: xóa đơn vị Kg của số kiện khi xuất ra excel
                        soKien = "Tổng số kiện: " + TKMD.SoKien;
                        tsContainer += soKien;
                    }
                    else
                        soKien = "";

                    workSheet.GetCell("W35").Value = tsContainer;

                    //32. Ghi chép khác: A37
                    workSheet.GetCell("A37").Value = TKMD.DeXuatKhac;
                    if (TKMD.TrongLuongNet > 0 && TKMD.DeXuatKhac.Length > 0)
                        workSheet.GetCell("A37").Value += "; " + "Trọng lượng tịnh: " + TKMD.TrongLuongNet.ToString();
                    else if (TKMD.TrongLuongNet > 0 && TKMD.DeXuatKhac.Equals(""))
                        workSheet.GetCell("A37").Value += "Trọng lượng tịnh: " + TKMD.TrongLuongNet.ToString();
                    //33. Ngày tháng năm: A42
                    if (TKMD.NgayDangKy == new DateTime(1900, 1, 1))
                        workSheet.GetCell("A42").Value = "Ngày " + DateTime.Today.Day + " tháng " + DateTime.Today.Month + " năm " + DateTime.Today.Year;
                    else
                        workSheet.GetCell("A42").Value = "Ngày " + TKMD.NgayTiepNhan.Day + " tháng " + TKMD.NgayTiepNhan.Month + " năm " + TKMD.NgayTiepNhan.Year;

                    //Chọn đường dẫn file cần lưu
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                    string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                    saveFileDialog1.FileName = "TK" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + TKMD.NamDK + "_" + dateNow;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        destFile = saveFileDialog1.FileName;
                        //Ghi nội dung file gốc vào file cần lưu
                        try
                        {
                            byte[] sourceFile = Globals.ReadFile(sourcePath);
                            Globals.WriteFile(destFile, sourceFile);
                            workBook.Save(destFile);
                            ShowMessage("Lưu tập tin thành công.\n" + destFile, false);
                            System.Diagnostics.Process.Start(destFile);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message, false);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + ex.Message, false);
                    return;
                }
            }
        }

        private static void ExportExcelTKX(Company.BLL.KDT.ToKhaiMauDich TKMD, bool banLuuHaiQuan, bool inMaHang, bool mienThueXuatKhau, bool mienThueGTGT)
        {
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            Infragistics.Excel.WorksheetMergedCellsRegion mergedRegion;
            string tenHang = "";
            /*Số thập phân*/
            int soThapPhanSoLuong = 0;
            int soThapPhanDonGia = 10;
            int soThapPhanTriGiaNguyenTe = 2;
            int soThapPhanTriGiaTinhThue = 0;
            int soThapPhanThueSuat = 0;
            int soThapPhanTienThue = 0;
            int soThapPhanTyLethuKhac = 0;
            int soThapPhanTienThuKhac = 0;
            int soThapPhanTongTienThue = 0;
            int soThapPhanTyGia = 2;


            //Lấy đường dẫn file gốc
            string destFile = "";
            string fileName = "TKX_TQDT_2009_TT222.xls";

            string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

            //Kiem tra file Excel mau co ton tai?
            if (!System.IO.File.Exists(sourcePath))
            {
                ShowMessage("Không tồn tại file mẫu Excel.", false);
                return;
            }

            //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
            try
            {
                Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];
                DateTime minDate = new DateTime(1900, 1, 1);

                #region Ô số 1 -> 16

                //Chi cục Hải quan
                mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("E5").RowIndex, 4, workSheet.GetCell("E5").RowIndex, 11);
                mergedRegion.Value = GlobalSettings.TEN_HAI_QUAN;
                mergedRegion.CellFormat.ShrinkToFit = Infragistics.Excel.ExcelDefaultableBoolean.True;

                //Chi cục Hải quan cửa khẩu
                workSheet.GetCell("H6").Value = TKMD.CuaKhau_ID + "-" + Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);

                //Số tham chiếu
                if (TKMD.ID > 0)
                    workSheet.GetCell("R5").Value = TKMD.ID + "";
                else
                    workSheet.GetCell("R5").Value = "";
                //Ngày, giờ gửi
                if (TKMD.NgayTiepNhan > minDate)
                    workSheet.GetCell("Q6").Value = TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("Q6").Value = "";

                //Số tờ khai
                if (TKMD.SoToKhai > 0)
                    workSheet.GetCell("AB5").Value = TKMD.SoToKhai + "";
                else
                    workSheet.GetCell("AB5").Value = "";
                //Ngày, giờ đăng ký
                if (TKMD.NgayDangKy > minDate)
                    workSheet.GetCell("AB6").Value = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("AB6").Value = "";

                if (TKMD.MaLoaiHinh.Substring(0, 1) == "X")
                {
                    //1.Người xuất khẩu A8
                    workSheet.GetCell("A8").Value = (TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI).ToUpper();
                    //2.Người nhập khẩu A10
                    workSheet.GetCell("A10").Value = TKMD.TenDonViDoiTac.ToUpper();
                }
                else if (TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                {
                    //1.Người xuất khẩu A8
                    workSheet.GetCell("A8").Value = TKMD.TenDonViDoiTac.ToUpper();
                    //2.Người nhập khẩu A10
                    workSheet.GetCell("A10").Value = (TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI).ToUpper();
                }

                //3.Người ủy thác A12
                workSheet.GetCell("A12").Value = TKMD.TenDonViUT;
                //4.Đại lý làm thủ tục Hải quan A14
                workSheet.GetCell("A14").Value = TKMD.MaDaiLyTTHQ + "." + TKMD.TenDaiLyTTHQ;
                //5. Loại hình P7
                string stlh = "";
                stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                workSheet.GetCell("P7").Value = TKMD.MaLoaiHinh + " - " + stlh;
                //6. Giấy phép: P8, Ngày P9, Ngày hết hạn P10
                if (TKMD.SoGiayPhep != "")
                    workSheet.GetCell("P8").Value = TKMD.SoGiayPhep;
                else
                    workSheet.GetCell("P8").Value = "";

                if (TKMD.NgayGiayPhep > minDate)
                    workSheet.GetCell("P9").Value = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("P9").Value = "";

                if (TKMD.NgayHetHanGiayPhep > minDate)
                    workSheet.GetCell("P10").Value = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("P10").Value = "";
                //7. Hợp đồng: X8, Ngày X9, Ngày hết hạn X10
                workSheet.GetCell("X8").Value = TKMD.SoHopDong;
                if (TKMD.NgayHopDong > minDate)
                    workSheet.GetCell("X9").Value = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("X9").Value = "";
                if (TKMD.NgayHetHanHopDong > minDate)
                    workSheet.GetCell("X10").Value = TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("X10").Value = "";
                //8. Hóa đơn thương mại R11, Ngày M12
                workSheet.GetCell("R11").Value = TKMD.SoHoaDonThuongMai;
                if (TKMD.NgayHoaDonThuongMai > minDate)
                    workSheet.GetCell("M12").Value = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("M12").Value = "";
                //9. Cảng xếp hàng: Y11, Tên cảng U12
                workSheet.GetCell("Y11").Value = TKMD.CuaKhau_ID;
                workSheet.GetCell("U12").Value = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);
                //10. Nước nhập khẩu: R13, Tên nước N14
                workSheet.GetCell("R13").Value = TKMD.NuocNK_ID;
                workSheet.GetCell("N14").Value = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(TKMD.NuocNK_ID);
                //11. Điều kiện giao hàng: S15
                workSheet.GetCell("S15").Value = TKMD.DKGH_ID;
                //12. Phương thức thanh toán AA15
                workSheet.GetCell("AA15").Value = TKMD.PTTT_ID;
                //13. Đồng tiền thanh toán S16
                workSheet.GetCell("S16").Value = TKMD.NguyenTe_ID;
                //14. Tỷ giá tính thuế AA16
                workSheet.GetCell("AA16").Value = TKMD.TyGiaTinhThue.ToString("N" + soThapPhanTyGia);
                //15. Kết quả phân luồng và hướng dẫn làm thủ tục Hải quan: A18
                workSheet.GetCell("A18").Value = TKMD.HUONGDAN;
                //16. Chứng từ Hải quan trước: X18

                #endregion

                /*THÔNG TIN HÀNG*/
                //17. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                //18. Mã số hàng hóa: L22, L23, L24
                //19. Xuất xứ: O22, O23, O24
                //20. Số lượng: T22
                //21. Đơn vị tính: U22, U23, U24
                //22. Đơn giá nguyên tệ: AA22
                //23. Trị giá nguyên tệ: AD22
                //Cộng: AD25
                /*THÔNG TIN THUẾ*/
                //24. Thuế nhập khẩu
                //Trị giá tính thuế: H28
                //Thuế suất (%): N28
                //Tiền thuế: T28
                //Cộng: T31
                //25. Thu khác
                //Tỷ lệ (%): X28
                //Số tiền: AD28

                if (TKMD.HMDCollection.Count > 0 && TKMD.HMDCollection.Count <= 3)
                {
                    #region Chi tiet hang hoa

                    Company.BLL.KDT.HangMauDich hmd = null;
                    for (int i = 0; i < TKMD.HMDCollection.Count; i++)
                    {
                        hmd = TKMD.HMDCollection[i];

                        #region THÔNG TIN HÀNG
                        //17. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                        if (!inMaHang)
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else
                                tenHang = hmd.TenHang;
                        }
                        else
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                            else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                tenHang = hmd.TenHang;
                        }
                        workSheet.Rows[workSheet.GetCell("B22").RowIndex + i].Cells[1].Value = tenHang;

                        //18. Mã số hàng hóa: L22, L23, L24
                        // Create a merged region
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("L22").RowIndex + i, 11, workSheet.GetCell("L22").RowIndex + i, 13);
                        mergedRegion.Value = hmd.MaHS;
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        //19. Xuất xứ: O22, O23, O24
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("O22").RowIndex + i, 14, workSheet.GetCell("O22").RowIndex + i, 16);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = hmd.NuocXX_ID;
                        //20. Số lượng: R22, R23, R24 hoac T22
                        workSheet.Rows[workSheet.GetCell("T22").RowIndex + i].Cells[workSheet.GetCell("T22").ColumnIndex].Value = hmd.SoLuong.ToString("N" + soThapPhanSoLuong);
                        workSheet.Rows[workSheet.GetCell("T22").RowIndex + i].Cells[workSheet.GetCell("T22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //21. Đơn vị tính: U22, U23, U24
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("U22").RowIndex + i, 20, workSheet.GetCell("U22").RowIndex + i, 22);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(hmd.DVT_ID);
                        //22. Đơn giá nguyên tệ: X22, X23, X24 hoac AA22
                        workSheet.Rows[workSheet.GetCell("AA22").RowIndex + i].Cells[workSheet.GetCell("AA22").ColumnIndex].Value = hmd.DonGiaKB.ToString("G" + soThapPhanDonGia);
                        workSheet.Rows[workSheet.GetCell("AA22").RowIndex + i].Cells[workSheet.GetCell("AA22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //23. Trị giá nguyên tệ: AB22, AN23, AB24 hoac AD22
                        workSheet.Rows[workSheet.GetCell("AD22").RowIndex + i].Cells[workSheet.GetCell("AD22").ColumnIndex].Value = hmd.TriGiaKB.ToString("N" + soThapPhanTriGiaNguyenTe);
                        workSheet.Rows[workSheet.GetCell("AD22").RowIndex + i].Cells[workSheet.GetCell("AD22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                        tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);

                        #endregion

                        #region THÔNG TIN THUẾ

                        if (!mienThueXuatKhau)
                        {
                            //24. Thuế nhập khẩu
                            //Trị giá tính thuế: H28
                            workSheet.Rows[workSheet.GetCell("H28").RowIndex + i].Cells[workSheet.GetCell("H28").ColumnIndex].Value = hmd.TriGiaTT.ToString("N" + soThapPhanTriGiaTinhThue);
                            workSheet.Rows[workSheet.GetCell("H28").RowIndex + i].Cells[workSheet.GetCell("H28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Thuế suất (%): N28
                            workSheet.Rows[workSheet.GetCell("N28").RowIndex + i].Cells[workSheet.GetCell("N28").ColumnIndex].Value = hmd.ThueSuatXNK.ToString("N" + soThapPhanThueSuat);
                            workSheet.Rows[workSheet.GetCell("N28").RowIndex + i].Cells[workSheet.GetCell("N28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Tiền thuế: T28
                            workSheet.Rows[workSheet.GetCell("T28").RowIndex + i].Cells[workSheet.GetCell("T28").ColumnIndex].Value = hmd.ThueXNK.ToString("N" + soThapPhanTienThue);
                            workSheet.Rows[workSheet.GetCell("T28").RowIndex + i].Cells[workSheet.GetCell("T28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        }


                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            if (!mienThueXuatKhau)
                            {
                                //25. Thu khác
                                //Tỷ lệ (%): X28
                                workSheet.Rows[workSheet.GetCell("X28").RowIndex + i].Cells[workSheet.GetCell("X28").ColumnIndex].Value = hmd.TyLeThuKhac.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("X28").RowIndex + i].Cells[workSheet.GetCell("X28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Số tiền: AD28
                                workSheet.Rows[workSheet.GetCell("AD28").RowIndex + i].Cells[workSheet.GetCell("AD28").ColumnIndex].Value = hmd.TriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                                workSheet.Rows[workSheet.GetCell("AD28").RowIndex + i].Cells[workSheet.GetCell("AD28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            }

                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }

                        //Tiền thuế Xuất nhập khẩu
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                        //Tổng tiền thuế và thu khác
                        tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);

                        #endregion
                    }

                    #endregion
                }
                //CHI TIET DINH KEM
                else
                {
                    #region CHI TIET DINH KEM

                    //17. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                    workSheet.GetCell("B22").Value = "HÀNG HÓA XUẤT";
                    workSheet.GetCell("B23").Value = "(Chi tiết theo phụ lục đính kèm)";

                    foreach (Company.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
                    {
                        tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);
                        tongTriGiaTT += Convert.ToDecimal(hmd.TriGiaTT);
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }
                    }

                    #endregion
                }

                //Linhhtn - Không công phí VC và các phí khác vào Tổng trị giá nguyên tệ
                //tongTriGiaNT += Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);

                //23. Trị giá nguyên tệ: Cộng: AD25
                workSheet.GetCell("AD25").Value = tongTriGiaNT.ToString("N" + soThapPhanTriGiaNguyenTe);
                workSheet.GetCell("AD25").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                //24. Thuế nhập khẩu: Cộng: T31
                if (TKMD.HMDCollection.Count <= 3)
                {
                    workSheet.GetCell("T31").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("T31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                }

                //Thuế GTGT
                //if (tongThueGTGT > 0)
                //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                //else
                //    lblTongTienThueGTGT.Text = "";

                //Thuế thu khác
                if (tongTriGiaThuKhac > 0 && TKMD.HMDCollection.Count <= 3)
                {
                    workSheet.GetCell("AD31").Value = tongTriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                    workSheet.GetCell("AD31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                }

                //26. Tổng số tiền thuế và thu khác (ô 24 + 25) bằng số: A32, Bằng chữ E33
                //mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("A32").RowIndex, 1, workSheet.GetCell("A32").RowIndex, 20);
                //mergedRegion.Value = tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);
                workSheet.GetCell("A32").Value += " " + tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);

                //Bằng chữ
                string s = Company.BLL.Utils.VNCurrency.ToString(tongTienThueTatCa).Trim();
                s = s[0].ToString().ToUpper() + s.Substring(1);
                workSheet.GetCell("E33").Value = s.Replace("  ", " ");
                workSheet.GetCell("E33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Left;


                //Truoc 23. Phi bao hiem B25
                string st = "";
                if (TKMD.PhiBaoHiem > 0)
                    st = "I = " + TKMD.PhiBaoHiem.ToString("N2");
                if (TKMD.PhiVanChuyen > 0)
                    st += " F = " + TKMD.PhiVanChuyen.ToString("N2");
                if (TKMD.PhiKhac > 0)
                    st += " Phí khác = " + TKMD.PhiKhac.ToString("N2");
                //workSheet.GetCell("B25").Value = st;

                //27. Tổng trọng lượng F34, Số hiệu kiện, cont: F35, Tổng số container: W35
                if (TKMD.TrongLuong > 0)
                    workSheet.GetCell("F34").Value = TKMD.TrongLuong + " kg ";
                else
                    workSheet.GetCell("F34").Value = "";
                //Số hiệu kiện, cont: F35
                if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
                    workSheet.GetCell("F35").Value = "danh sách container theo bảng kê đính kèm";
                else
                {
                    string soHieuKien = "";
                    Company.KDT.SHARE.QuanLyChungTu.Container objContainer = null;

                    if (TKMD.VanTaiDon != null)
                    {
                        for (int cnt = 0; cnt < TKMD.VanTaiDon.ContainerCollection.Count; cnt++)
                        {
                            objContainer = TKMD.VanTaiDon.ContainerCollection[cnt];

                            soHieuKien += objContainer.SoHieu + "/" + objContainer.Seal_No;

                            if (cnt < TKMD.VanTaiDon.ContainerCollection.Count - 1)
                            {
                                soHieuKien += "; ";
                            }
                        }
                    }
                    workSheet.GetCell("F35").Value = soHieuKien;
                }
                //Tổng số container: W35
                string tsContainer = "";
                string cont20 = "", cont40 = "", soKien = "";
                if (TKMD.SoContainer20 > 0)
                {
                    cont20 = "Cont20: " + Convert.ToInt32(TKMD.SoContainer20);

                    //tsContainer += cont20 + "; ";
                }
                else
                    cont20 = "";

                if (TKMD.SoContainer40 > 0)
                {
                    cont40 = "Cont40: " + Convert.ToInt32(TKMD.SoContainer40);

                    tsContainer += cont40 + "; ";
                }
                else
                    cont40 = "";

                if (TKMD.SoKien > 0)
                {
                    soKien = "Tổng số kiện: " + TKMD.SoKien;

                    tsContainer += soKien;
                }
                else
                    soKien = "";

                workSheet.GetCell("AB34").Value = cont20;
                workSheet.GetCell("AB34").CellFormat.Font.Height = 160; // Size: 160 / 20 = 8
                workSheet.GetCell("W35").Value = tsContainer;

                //28. Ghi chép khác: A37
                workSheet.GetCell("A37").Value = TKMD.DeXuatKhac;
                //29. Xác nhận giải phóng hàng/đưa hàng về bảo quản Y38
                //30. Ngày tháng năm: A43
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1))
                    workSheet.GetCell("A43").Value = "Ngày " + DateTime.Today.Day + " tháng " + DateTime.Today.Month + " năm " + DateTime.Today.Year;
                else
                    workSheet.GetCell("A43").Value = "Ngày " + TKMD.NgayTiepNhan.Day + " tháng " + TKMD.NgayTiepNhan.Month + " năm " + TKMD.NgayTiepNhan.Year;


                //Chọn đường dẫn file cần lưu
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                saveFileDialog1.FileName = "TK" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + (TKMD.NamDK != 0 ? TKMD.NamDK : TKMD.NgayDangKy.Year) + "_" + dateNow;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    destFile = saveFileDialog1.FileName;
                    //Ghi nội dung file gốc vào file cần lưu
                    try
                    {
                        byte[] sourceFile = Globals.ReadFile(sourcePath);
                        Globals.WriteFile(destFile, sourceFile);
                        workBook.Save(destFile);
                        ShowMessage("Lưu tập tin thành công.\n" + destFile, false);
                        System.Diagnostics.Process.Start(destFile);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(ex.Message, false);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + ex.Message, false);
                return;
            }
        }

        //DATLMQ Export Excel Phụ lục Tờ khai Nhập 28/12/2010
        private static void ExportExcelPhuLucTKN(Company.BLL.KDT.ToKhaiMauDich TKMD, bool banLuuHaiQuan, bool inMaHang, bool mienThueXuatKhau, bool mienThueGTGT, string _pls)
        {
            bool ok = true;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            Infragistics.Excel.WorksheetMergedCellsRegion mergedRegion;
            string tenHang = "";
            /*Số thập phân*/
            int soThapPhanSoLuong = 0;
            int soThapPhanDonGia = 10;
            int soThapPhanTriGiaNguyenTe = 2;
            int soThapPhanTriGiaTinhThue = 0;
            int soThapPhanThueSuat = 0;
            int soThapPhanTienThue = 0;
            int soThapPhanTyLethuKhac = 0;
            int soThapPhanTienThuKhac = 0;
            int soThapPhanTongTienThue = 0;
            int soThapPhanTyGia = 2;
            int index = 0;
            int j = -1;

            if (ok)
            {
                //Lấy đường dẫn file gốc
                string destFile = "";
                string fileName = "PhuLuc_TKN_TQDT_2009_TT22.xls";

                string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

                //Kiem tra file Excel mau co ton tai?
                if (!System.IO.File.Exists(sourcePath))
                {
                    ShowMessage("Không tồn tại file mẫu Excel.", false);
                    return;
                }

                //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
                try
                {
                    Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                    Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];
                    DateTime minDate = new DateTime(1900, 1, 1);

                    //Chi cục Hải quan: F5
                    workSheet.GetCell("F5").Value = GlobalSettings.TEN_HAI_QUAN;
                    //Chi cục Hải quan cửa khẩu: H6
                    workSheet.GetCell("H6").Value = TKMD.CuaKhau_ID + "-" + Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);
                    //Phụ lục số: S5
                    workSheet.GetCell("S5").Value = _pls;
                    //Số tờ khai: AC5
                    if (TKMD.SoToKhai > 0)
                        workSheet.GetCell("AC5").Value = TKMD.SoToKhai + "";
                    else
                        workSheet.GetCell("AC5").Value = "";
                    //Ngày, giờ đăng ký: U6
                    if (TKMD.NgayDangKy > minDate)
                        workSheet.GetCell("U6").Value = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("U6").Value = "";
                    //Loại hình: AB6
                    string stlh = "";
                    stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                    workSheet.GetCell("AB6").Value = TKMD.MaLoaiHinh + " - " + stlh;

                    /*THÔNG TIN HÀNG*/
                    /*THÔNG TIN THUẾ*/
                    //27. Thuế nhập khẩu
                    //Trị giá tính thuế: F23 -> F32
                    //Thuế suất (%): H23 -> H32
                    //Tiền thuế: M23 -> M32
                    //Cộng: M33
                    //28. Thuế GTGT (hoặc TTĐB)
                    //Trị giá tính thuế: R23 -> R32
                    //Thuế suất (%): T23 -> T32
                    //Tiền thuế: Y23 -> Y32
                    //29. Thu khác
                    //Tỷ lệ (%): AB23 -> AB32
                    //Số tiền: AH23 -> AH32
                    //30. Tổng số tiền thuế và thu khác (ô 27+28 + 29) bằng số: O34, Bằng chữ D35

                    #region CHI TIẾT THÔNG TIN HÀNG
                    Company.BLL.KDT.HangMauDich hmd = null;
                    List<Company.BLL.KDT.HangMauDich> HMDReportCollection = new List<Company.BLL.KDT.HangMauDich>();
                    int begin = (Convert.ToInt32(_pls) - 1) * 10;
                    int end = Convert.ToInt32(_pls) * 10;
                    if (end > TKMD.HMDCollection.Count) end = TKMD.HMDCollection.Count;
                    for (int i = begin; i < end; i++)
                    {
                        HMDReportCollection.Add(TKMD.HMDCollection[i]);
                        //for (int i = 0; i < TKMD.HMDCollection.Count; i++)
                        //{
                        hmd = TKMD.HMDCollection[i];
                        j++;
                        #region THÔNG TIN HÀNG
                        try
                        {
                            index = (j + 1) + (Convert.ToInt32(_pls) - 1) * 10;
                        }
                        catch
                        {
                            ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.", false);
                            return;
                        }
                        workSheet.GetCell("A" + (10 + j).ToString()).Value = index;
                        workSheet.GetCell("A" + (23 + j).ToString()).Value = index;
                        //20. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                        if (!inMaHang)
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else
                                tenHang = hmd.TenHang;
                        }
                        else
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                            else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                tenHang = hmd.TenHang;
                        }

                        //20. Tên hàng, quy cách phẩm chất: B10
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("B10").RowIndex + j, 1, workSheet.GetCell("B10").RowIndex + j, 8);
                        mergedRegion.Value = tenHang;
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;

                        //21. Mã số hàng hóa: J10 -> J19
                        // Create a merged region
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("J10").RowIndex + j, 9, workSheet.GetCell("J10").RowIndex + j, 12);
                        mergedRegion.Value = hmd.MaHS;
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        //22. Xuất xứ: N10 -> N19
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("N10").RowIndex + j, 13, workSheet.GetCell("N10").RowIndex + j, 15);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = hmd.NuocXX_ID;
                        //23. Số lượng: T10 -> T19
                        workSheet.Rows[workSheet.GetCell("T10").RowIndex + j].Cells[workSheet.GetCell("T10").ColumnIndex].Value = hmd.SoLuong.ToString("N" + soThapPhanSoLuong);
                        workSheet.Rows[workSheet.GetCell("T10").RowIndex + j].Cells[workSheet.GetCell("T10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //24. Đơn vị tính: U10 -> U19
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("U10").RowIndex + j, 20, workSheet.GetCell("U10").RowIndex + j, 22);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(hmd.DVT_ID);
                        //25. Đơn giá nguyên tệ: AA10 -> AA19
                        workSheet.Rows[workSheet.GetCell("AA10").RowIndex + j].Cells[workSheet.GetCell("AA10").ColumnIndex].Value = hmd.DonGiaKB.ToString("G" + soThapPhanDonGia);
                        workSheet.Rows[workSheet.GetCell("AA10").RowIndex + j].Cells[workSheet.GetCell("AA10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //26. Trị giá nguyên tệ: AE10 -> AE19
                        workSheet.Rows[workSheet.GetCell("AE10").RowIndex + j].Cells[workSheet.GetCell("AE10").ColumnIndex].Value = hmd.TriGiaKB.ToString("N" + soThapPhanTriGiaNguyenTe);
                        workSheet.Rows[workSheet.GetCell("AE10").RowIndex + j].Cells[workSheet.GetCell("AE10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                        tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);

                        #endregion

                        #region THÔNG TIN THUẾ

                        if (!mienThueXuatKhau)
                        {
                            //27. Thuế nhập khẩu
                            //Trị giá tính thuế: F23
                            workSheet.Rows[workSheet.GetCell("F23").RowIndex + j].Cells[workSheet.GetCell("F23").ColumnIndex].Value = hmd.TriGiaTT.ToString("N" + soThapPhanTriGiaTinhThue);
                            workSheet.Rows[workSheet.GetCell("F23").RowIndex + j].Cells[workSheet.GetCell("F23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Thuế suất (%): H23
                            workSheet.Rows[workSheet.GetCell("H23").RowIndex + j].Cells[workSheet.GetCell("H23").ColumnIndex].Value = hmd.ThueSuatXNK.ToString("N" + soThapPhanThueSuat);
                            workSheet.Rows[workSheet.GetCell("H23").RowIndex + j].Cells[workSheet.GetCell("H23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Tiền thuế: M23
                            workSheet.Rows[workSheet.GetCell("M23").RowIndex + j].Cells[workSheet.GetCell("M23").ColumnIndex].Value = hmd.ThueXNK.ToString("N" + soThapPhanTienThue);
                            workSheet.Rows[workSheet.GetCell("M23").RowIndex + j].Cells[workSheet.GetCell("M23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        }


                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            if (!mienThueXuatKhau)
                            {
                                //28. Thuế GTGT
                                //Trị giá tính thuế: R23
                                workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].Value = TriGiaTTGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Tỷ lệ (%): T23
                                workSheet.Rows[workSheet.GetCell("T23").RowIndex + j].Cells[workSheet.GetCell("T23").ColumnIndex].Value = hmd.ThueSuatGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("T23").RowIndex + j].Cells[workSheet.GetCell("T23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Tiền thuế: Y23
                                workSheet.Rows[workSheet.GetCell("Y23").RowIndex + j].Cells[workSheet.GetCell("Y23").ColumnIndex].Value = hmd.ThueGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("Y23").RowIndex + j].Cells[workSheet.GetCell("Y23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                                //29. Thu khác
                                //Tỷ lệ (%): AB23
                                workSheet.Rows[workSheet.GetCell("AB23").RowIndex + j].Cells[workSheet.GetCell("AB23").ColumnIndex].Value = hmd.TyLeThuKhac.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("AB23").RowIndex + j].Cells[workSheet.GetCell("AB23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Số tiền: AH23
                                workSheet.Rows[workSheet.GetCell("AH23").RowIndex + j].Cells[workSheet.GetCell("AH23").ColumnIndex].Value = hmd.TriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                                workSheet.Rows[workSheet.GetCell("AH23").RowIndex + j].Cells[workSheet.GetCell("AH23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            }

                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }

                        //Tiền thuế Xuất nhập khẩu
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                        //Tổng tiền thuế và thu khác
                        tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);

                        #endregion

                    }
                    #endregion
                    //}

                    //Linhhtn - Không công phí VC và các phí khác vào Tổng trị giá nguyên tệ
                    //tongTriGiaNT += Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);

                    //23. Trị giá nguyên tệ: Cộng: AE20
                    workSheet.GetCell("AE20").Value = tongTriGiaNT.ToString("N" + soThapPhanTriGiaNguyenTe);
                    workSheet.GetCell("AE20").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //24. Thuế nhập khẩu: Cộng: K31
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("K31").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("K31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    workSheet.GetCell("M33").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("M33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế GTGT
                    //if (tongThueGTGT > 0)
                    //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                    //else
                    //    lblTongTienThueGTGT.Text = "";
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("V31").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("V31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    workSheet.GetCell("Y33").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("Y33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế thu khác
                    //if (tongTriGiaThuKhac > 0 && TKMD.HMDCollection.Count <= 3)
                    //{
                    workSheet.GetCell("AH33").Value = tongTriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                    workSheet.GetCell("AH33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}

                    //26. Tổng số tiền thuế và thu khác (ô 24 + 25) bằng số: O34, Bằng chữ D35
                    //mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("A32").RowIndex, 1, workSheet.GetCell("A32").RowIndex, 20);
                    //mergedRegion.Value = tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);
                    workSheet.GetCell("O34").Value += " " + tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);

                    //Bằng chữ
                    string s = Company.BLL.Utils.VNCurrency.ToString(tongTienThueTatCa).Trim();
                    s = s[0].ToString().ToUpper() + s.Substring(1);
                    workSheet.GetCell("D35").Value = s.Replace("  ", " ");
                    workSheet.GetCell("D35").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Left;

                    //Chọn đường dẫn file cần lưu
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                    string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                    saveFileDialog1.FileName = "TK" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + TKMD.NamDK + "_" + dateNow + "PL_So_" + _pls;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        destFile = saveFileDialog1.FileName;
                        //Ghi nội dung file gốc vào file cần lưu
                        try
                        {
                            byte[] sourceFile = Globals.ReadFile(sourcePath);
                            Globals.WriteFile(destFile, sourceFile);
                            workBook.Save(destFile);
                            ShowMessage("Save file thành công", false);
                            System.Diagnostics.Process.Start(destFile);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message, false);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + ex.Message, false);
                    return;
                }
            }
        }

        private static void ExportExcelPhuLucTKX(Company.BLL.KDT.ToKhaiMauDich TKMD, bool banLuuHaiQuan, bool inMaHang, bool mienThueXuatKhau, bool mienThueGTGT, string _pls)
        {
            bool ok = true;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            Infragistics.Excel.WorksheetMergedCellsRegion mergedRegion;
            string tenHang = "";
            /*Số thập phân*/
            int soThapPhanSoLuong = 0;
            int soThapPhanDonGia = 10;
            int soThapPhanTriGiaNguyenTe = 2;
            int soThapPhanTriGiaTinhThue = 0;
            int soThapPhanThueSuat = 0;
            int soThapPhanTienThue = 0;
            int soThapPhanTyLethuKhac = 0;
            int soThapPhanTienThuKhac = 0;
            int soThapPhanTongTienThue = 0;
            int soThapPhanTyGia = 2;
            int index = 0;
            int j = -1;

            if (ok)
            {
                //Lấy đường dẫn file gốc
                string destFile = "";
                string fileName = "PhuLuc_TKX_TQDT_2009_TT22.xls";

                string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

                //Kiem tra file Excel mau co ton tai?
                if (!System.IO.File.Exists(sourcePath))
                {
                    ShowMessage("Không tồn tại file mẫu Excel.", false);
                    return;
                }

                //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
                try
                {
                    Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                    Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];
                    DateTime minDate = new DateTime(1900, 1, 1);

                    //Chi cục Hải quan: F5
                    workSheet.GetCell("F5").Value = GlobalSettings.TEN_HAI_QUAN;
                    //Chi cục Hải quan cửa khẩu: H6
                    workSheet.GetCell("H6").Value = TKMD.CuaKhau_ID + "-" + Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);
                    //Phụ lục số: S5
                    workSheet.GetCell("S5").Value = _pls;
                    //Số tờ khai: AC5
                    if (TKMD.SoToKhai > 0)
                        workSheet.GetCell("AC5").Value = TKMD.SoToKhai + "";
                    else
                        workSheet.GetCell("AC5").Value = "";
                    //Ngày, giờ đăng ký: U6
                    if (TKMD.NgayDangKy > minDate)
                        workSheet.GetCell("U6").Value = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("U6").Value = "";
                    //Loại hình: AB6
                    string stlh = "";
                    stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                    workSheet.GetCell("AB6").Value = TKMD.MaLoaiHinh + " - " + stlh;

                    /*THÔNG TIN HÀNG*/
                    /*THÔNG TIN THUẾ*/
                    //27. Thuế nhập khẩu
                    //Trị giá tính thuế: F23 -> F32
                    //Thuế suất (%): H23 -> H32
                    //Tiền thuế: M23 -> M32
                    //Cộng: M33
                    //28. Thuế GTGT (hoặc TTĐB)
                    //Trị giá tính thuế: R23 -> R32
                    //Thuế suất (%): T23 -> T32
                    //Tiền thuế: Y23 -> Y32
                    //29. Thu khác
                    //Tỷ lệ (%): AB23 -> AB32
                    //Số tiền: AH23 -> AH32
                    //30. Tổng số tiền thuế và thu khác (ô 27+28 + 29) bằng số: O34, Bằng chữ D35

                    #region CHI TIẾT THÔNG TIN HÀNG
                    Company.BLL.KDT.HangMauDich hmd = null;
                    List<Company.BLL.KDT.HangMauDich> HMDReportCollection = new List<Company.BLL.KDT.HangMauDich>();
                    int begin = (Convert.ToInt32(_pls) - 1) * 10;
                    int end = Convert.ToInt32(_pls) * 10;
                    if (end > TKMD.HMDCollection.Count) end = TKMD.HMDCollection.Count;
                    for (int i = begin; i < end; i++)
                    {
                        HMDReportCollection.Add(TKMD.HMDCollection[i]);
                        //for (int i = 0; i < TKMD.HMDCollection.Count; i++)
                        //{
                        hmd = TKMD.HMDCollection[i];
                        j++;
                        #region THÔNG TIN HÀNG
                        try
                        {
                            index = (j + 1) + (Convert.ToInt32(_pls) - 1) * 10;
                        }
                        catch
                        {
                            ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.", false);
                            return;
                        }
                        workSheet.GetCell("A" + (10 + j).ToString()).Value = index;
                        workSheet.GetCell("A" + (23 + j).ToString()).Value = index;
                        //20. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                        if (!inMaHang)
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else
                                tenHang = hmd.TenHang;
                        }
                        else
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                            else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                tenHang = hmd.TenHang;
                        }

                        //20. Tên hàng, quy cách phẩm chất: B10
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("B10").RowIndex + j, 1, workSheet.GetCell("B10").RowIndex + j, 8);
                        mergedRegion.Value = tenHang;
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;

                        //21. Mã số hàng hóa: J10 -> J19
                        // Create a merged region
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("J10").RowIndex + j, 9, workSheet.GetCell("J10").RowIndex + j, 12);
                        mergedRegion.Value = hmd.MaHS;
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        //22. Xuất xứ: N10 -> N19
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("N10").RowIndex + j, 13, workSheet.GetCell("N10").RowIndex + j, 15);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = hmd.NuocXX_ID;
                        //23. Số lượng: T10 -> T19
                        workSheet.Rows[workSheet.GetCell("T10").RowIndex + j].Cells[workSheet.GetCell("T10").ColumnIndex].Value = hmd.SoLuong.ToString("N" + soThapPhanSoLuong);
                        workSheet.Rows[workSheet.GetCell("T10").RowIndex + j].Cells[workSheet.GetCell("T10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //24. Đơn vị tính: U10 -> U19
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("U10").RowIndex + j, 20, workSheet.GetCell("U10").RowIndex + j, 22);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(hmd.DVT_ID);
                        //25. Đơn giá nguyên tệ: AA10 -> AA19
                        workSheet.Rows[workSheet.GetCell("AA10").RowIndex + j].Cells[workSheet.GetCell("AA10").ColumnIndex].Value = hmd.DonGiaKB.ToString("G" + soThapPhanDonGia);
                        workSheet.Rows[workSheet.GetCell("AA10").RowIndex + j].Cells[workSheet.GetCell("AA10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //26. Trị giá nguyên tệ: AE10 -> AE19
                        workSheet.Rows[workSheet.GetCell("AE10").RowIndex + j].Cells[workSheet.GetCell("AE10").ColumnIndex].Value = hmd.TriGiaKB.ToString("N" + soThapPhanTriGiaNguyenTe);
                        workSheet.Rows[workSheet.GetCell("AE10").RowIndex + j].Cells[workSheet.GetCell("AE10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                        tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);

                        #endregion

                        #region THÔNG TIN THUẾ

                        if (!mienThueXuatKhau)
                        {
                            //27. Thuế nhập khẩu
                            //Trị giá tính thuế: G23
                            workSheet.Rows[workSheet.GetCell("G23").RowIndex + j].Cells[workSheet.GetCell("G23").ColumnIndex].Value = hmd.TriGiaTT.ToString("N" + soThapPhanTriGiaTinhThue);
                            workSheet.Rows[workSheet.GetCell("G23").RowIndex + j].Cells[workSheet.GetCell("G23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Thuế suất (%): L23
                            workSheet.Rows[workSheet.GetCell("L23").RowIndex + j].Cells[workSheet.GetCell("L23").ColumnIndex].Value = hmd.ThueSuatXNK.ToString("N" + soThapPhanThueSuat);
                            workSheet.Rows[workSheet.GetCell("L23").RowIndex + j].Cells[workSheet.GetCell("L23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Tiền thuế: R23
                            workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].Value = hmd.ThueXNK.ToString("N" + soThapPhanTienThue);
                            workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        }


                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            if (!mienThueXuatKhau)
                            {
                                //28. Thuế GTGT
                                //Trị giá tính thuế: R23
                                //workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].Value = TriGiaTTGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                //workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                ////Tỷ lệ (%): T23
                                //workSheet.Rows[workSheet.GetCell("T23").RowIndex + j].Cells[workSheet.GetCell("T23").ColumnIndex].Value = hmd.ThueSuatGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                //workSheet.Rows[workSheet.GetCell("T23").RowIndex + j].Cells[workSheet.GetCell("T23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                ////Tiền thuế: Y23
                                //workSheet.Rows[workSheet.GetCell("Y23").RowIndex + j].Cells[workSheet.GetCell("Y23").ColumnIndex].Value = hmd.ThueGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                //workSheet.Rows[workSheet.GetCell("Y23").RowIndex + j].Cells[workSheet.GetCell("Y23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                                //29. Thu khác
                                //Tỷ lệ (%): AB23
                                workSheet.Rows[workSheet.GetCell("AB23").RowIndex + j].Cells[workSheet.GetCell("AB23").ColumnIndex].Value = hmd.TyLeThuKhac.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("AB23").RowIndex + j].Cells[workSheet.GetCell("AB23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Số tiền: AH23
                                workSheet.Rows[workSheet.GetCell("AH23").RowIndex + j].Cells[workSheet.GetCell("AH23").ColumnIndex].Value = hmd.TriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                                workSheet.Rows[workSheet.GetCell("AH23").RowIndex + j].Cells[workSheet.GetCell("AH23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            }

                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }

                        //Tiền thuế Xuất nhập khẩu
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                        //Tổng tiền thuế và thu khác
                        tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);

                        #endregion

                    }
                    #endregion
                    //}

                    //Linhhtn - Không công phí VC và các phí khác vào Tổng trị giá nguyên tệ
                    //tongTriGiaNT += Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);

                    //23. Trị giá nguyên tệ: Cộng: AE20
                    workSheet.GetCell("AE20").Value = tongTriGiaNT.ToString("N" + soThapPhanTriGiaNguyenTe);
                    workSheet.GetCell("AE20").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //24. Thuế nhập khẩu: Cộng: R33
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("K31").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("K31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    workSheet.GetCell("R33").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("R33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế GTGT
                    //if (tongThueGTGT > 0)
                    //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                    //else
                    //    lblTongTienThueGTGT.Text = "";
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("V31").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("V31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    //workSheet.GetCell("Y33").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    //workSheet.GetCell("Y33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế thu khác
                    //if (tongTriGiaThuKhac > 0 && TKMD.HMDCollection.Count <= 3)
                    //{
                    workSheet.GetCell("AH33").Value = tongTriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                    workSheet.GetCell("AH33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}

                    //26. Tổng số tiền thuế và thu khác (ô 24 + 25) bằng số: N34, Bằng chữ D35
                    //mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("A32").RowIndex, 1, workSheet.GetCell("A32").RowIndex, 20);
                    //mergedRegion.Value = tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);
                    workSheet.GetCell("N34").Value += " " + tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);

                    //Bằng chữ
                    string s = Company.BLL.Utils.VNCurrency.ToString(tongTienThueTatCa).Trim();
                    s = s[0].ToString().ToUpper() + s.Substring(1);
                    workSheet.GetCell("D35").Value = s.Replace("  ", " ");
                    workSheet.GetCell("D35").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Left;

                    //Chọn đường dẫn file cần lưu
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                    string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                    saveFileDialog1.FileName = "TK" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + TKMD.NamDK + "_" + dateNow + "PL_So_" + _pls;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        destFile = saveFileDialog1.FileName;
                        //Ghi nội dung file gốc vào file cần lưu
                        try
                        {
                            byte[] sourceFile = Globals.ReadFile(sourcePath);
                            Globals.WriteFile(destFile, sourceFile);
                            workBook.Save(destFile);
                            ShowMessage("Save file thành công", false);
                            System.Diagnostics.Process.Start(destFile);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message, false);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + ex.Message, false);
                    return;
                }
            }
        }

        //DATLMQ Export Excel Thông báo Thuế 06/08/2011
        public static void ExportExcelThongBaoThue(Company.BLL.KDT.ToKhaiMauDich TKMD, string _soQD, DateTime _ngayQD, DateTime _ngayHH, string _TKKB, string _TenKB, string _chuongThueXNK,
                                                    string _loaiThueXNK, string _khoanThueXNK, string _mucThueXNK, string _tieuMucThueXNK, double _soTienThueXNK, string _chuongThueVAT,
                                                    string _loaiThueVAT, string _khoanThueVAT, string _mucThueVAT, string _tieuMucThueVAT, double _soTienThueVAT, string _chuongThueTTDB,
                                                    string _loaiThueTTDB, string _khoanThueTTDB, string _mucThueTTDB, string _tieuMucThueTTDB, double _soTienThueTTDB, string _chuongThueTVCBPG,
                                                    string _loaiThueTVCBPG, string _khoanThueTVCBPG, string _mucThueTVCBPG, string _tieuMucThueTVCBPG, double _soTienThueTVCBPG, int soThapPhan)
        {
            //Lấy đường dẫn file gốc
            string destFile = "";
            string fileName = "Thong_Bao_Thue.xls";

            string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

            //Kiem tra file Excel mau co ton tai?
            if (!System.IO.File.Exists(sourcePath))
            {
                ShowMessage("Không tồn tại file mẫu Excel.", false);
                return;
            }

            //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
            try
            {
                Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];

                //Nội dung xuất Excel
                workSheet.GetCell("A1").Value = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
                //1. Tên chi cục Hải quan: A2
                workSheet.GetCell("A2").Value = GlobalSettings.TEN_HAI_QUAN;
                //2. Số QD: D3
                workSheet.GetCell("D3").Value = TKMD.SoQD + "/TBT";
                //3. Người Xuất khẩu/Nhập khẩu: F9
                workSheet.GetCell("F9").Value = TKMD.TenDoanhNghiep;
                //4. Mã số thuế: F10
                workSheet.GetCell("F10").Value = TKMD.MaDoanhNghiep;
                //5. Người khai Hải quan: F11
                workSheet.GetCell("F11").Value = TKMD.TenDoanhNghiep;
                //6. Mã số thuế: F12
                workSheet.GetCell("F12").Value = TKMD.MaDoanhNghiep;
                //7. Số tờ khai: C13
                workSheet.GetCell("C13").Value = TKMD.SoToKhai.ToString();
                //8. Mã loại hình: I13
                workSheet.GetCell("I13").Value = TKMD.MaLoaiHinh;
                //9. Ngày đăng ký: O13
                workSheet.GetCell("O13").Value = TKMD.NgayDangKy.ToShortDateString();
                //10. Số Vận Đơn: D15
                workSheet.GetCell("D15").Value = TKMD.SoVanDon;
                //11. Ngày Vận Đơn: J15
                workSheet.GetCell("J15").Value = TKMD.NgayVanDon.ToShortDateString();
                //12. Số Hóa Đơn Thương Mại: F16
                workSheet.GetCell("F16").Value = TKMD.SoHoaDonThuongMai;
                //13. Ngày Hóa Đơn Thương Mại: J16
                workSheet.GetCell("J16").Value = TKMD.NgayHoaDonThuongMai.ToShortDateString();
                //14. Kết quả xử lý: E18
                string tenLuong = "";
                if (TKMD.PhanLuong == "1")
                    tenLuong = "Xanh";
                else if (TKMD.PhanLuong == "2")
                    tenLuong = "Vàng";
                else
                    tenLuong = "Đỏ";
                workSheet.GetCell("E18").Value = "Tờ khai được phân luồng " + tenLuong;
                //15. Chi tiết Thuế
                //Thuế XNK: Chương: F24
                workSheet.GetCell("F24").Value = _chuongThueXNK;
                //Loại: H24
                workSheet.GetCell("H24").Value = _loaiThueXNK;
                //Khoản: J24
                workSheet.GetCell("J24").Value = _khoanThueXNK;
                //Mục: L24
                workSheet.GetCell("L24").Value = _mucThueXNK;
                //Tiểu Mục: N24
                workSheet.GetCell("N24").Value = _tieuMucThueXNK;
                //Số Tiền: P24
                workSheet.GetCell("P24").Value = _soTienThueXNK.ToString("N" + soThapPhan);
                //Thuế VAT: Chương: F26
                workSheet.GetCell("F26").Value = _chuongThueVAT;
                //Loại: H24
                workSheet.GetCell("H26").Value = _loaiThueVAT;
                //Khoản: J24
                workSheet.GetCell("J26").Value = _khoanThueVAT;
                //Mục: L24
                workSheet.GetCell("L26").Value = _mucThueVAT;
                //Tiểu Mục: N24
                workSheet.GetCell("N26").Value = _tieuMucThueVAT;
                //Số Tiền: P24
                workSheet.GetCell("P26").Value = _soTienThueVAT.ToString("N" + soThapPhan);
                //Thuế TTDB: Chương: F28
                workSheet.GetCell("F28").Value = _chuongThueTTDB;
                //Loại: H24
                workSheet.GetCell("H28").Value = _loaiThueTTDB;
                //Khoản: J24
                workSheet.GetCell("J28").Value = _khoanThueTTDB;
                //Mục: L24
                workSheet.GetCell("L28").Value = _mucThueTTDB;
                //Tiểu Mục: N24
                workSheet.GetCell("N28").Value = _tieuMucThueTTDB;
                //Số Tiền: P24
                workSheet.GetCell("P28").Value = _soTienThueTTDB.ToString("N" + soThapPhan);
                //Thuế TVCBPG: Chương: F30
                workSheet.GetCell("F30").Value = _chuongThueTVCBPG;
                //Loại: H24
                workSheet.GetCell("H30").Value = _loaiThueTVCBPG;
                //Khoản: J24
                workSheet.GetCell("J30").Value = _khoanThueTVCBPG;
                //Mục: L24
                workSheet.GetCell("L30").Value = _mucThueTVCBPG;
                //Tiểu Mục: N24
                workSheet.GetCell("N30").Value = _tieuMucThueTVCBPG;
                //Số Tiền: P24
                workSheet.GetCell("P30").Value = _soTienThueTVCBPG.ToString("N" + soThapPhan);
                //Tổng số Tiền Thuế: P32
                double tongTienThue = _soTienThueXNK + _soTienThueVAT + _soTienThueTTDB + _soTienThueTVCBPG;
                workSheet.GetCell("P32").Value = tongTienThue.ToString("N" + soThapPhan);
                //Bằng chữ: C35
                workSheet.GetCell("C35").Value = Company.BLL.Utils.VNCurrency.ToString(tongTienThue);
                //Thời hạn: D37
                workSheet.GetCell("D37").Value = _ngayHH.Subtract(_ngayQD).Days + 1;
                //Kể từ ngày: H37
                workSheet.GetCell("H37").Value = _ngayQD.ToShortDateString();
                //Tài khoản Kho Bạc: L38
                workSheet.GetCell("L38").Value = _TKKB;
                //Tên Kho Bạc: A39
                workSheet.GetCell("A39").Value = _TenKB;

                //Chọn đường dẫn file cần lưu
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                saveFileDialog1.FileName = "ThongBaoThue_TK_" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + TKMD.NamDK + "_" + dateNow;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    destFile = saveFileDialog1.FileName;
                    //Ghi nội dung file gốc vào file cần lưu
                    try
                    {
                        byte[] sourceFile = Globals.ReadFile(sourcePath);
                        Globals.WriteFile(destFile, sourceFile);
                        workBook.Save(destFile);
                        ShowMessage("Lưu file thành công", false);
                        System.Diagnostics.Process.Start(destFile);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(ex.Message, false);
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LocalLogger.Instance().WriteMessage(e);
                ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + e.Message, false);
                return;
            }
        }

        private static void ExportExcelBCXuatNhapTon(System.Data.DataTable dtXuatNhapTon, bool banLuuHaiQuan, int fontSize)
        {
            bool ok = true;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            Infragistics.Excel.WorksheetMergedCellsRegion mergedRegion;
            string tenHang = "";

            /*Số thập phân*/
            int soThapPhanTongLuongNPLSuDung = GlobalSettings.SoThapPhan.LuongNPL;
            int soThapPhanLuongNPLSuDung = GlobalSettings.SoThapPhan.LuongNPL;
            int soThapPhanLuongNhap = GlobalSettings.SoThapPhan.LuongNPL;
            int soThapPhanTongLuongNhap = GlobalSettings.SoThapPhan.LuongNPL;
            int soThapPhanLuongTonDau = GlobalSettings.SoThapPhan.LuongNPL;
            int soThapPhanLuongTonCuoi = GlobalSettings.SoThapPhan.LuongNPL;
            int soThapPhanLuongSPXuat = GlobalSettings.SoThapPhan.LuongSP;
            int soThapPhanDinhMuc = GlobalSettings.SoThapPhan.DinhMuc;
            int soThapPhanLuongNPLTaiXuat = GlobalSettings.SoThapPhan.LuongNPL;

            if (ok)
            {
                //Lấy đường dẫn file gốc
                string destFile = "";
                string fileName = "Mau10_HSTK_KCX_XuatNhapTon_194.xls";

                string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

                //Kiem tra file Excel mau co ton tai?
                if (!System.IO.File.Exists(sourcePath))
                {
                    ShowMessage("Không tồn tại file mẫu Excel.", false);
                    return;
                }

                //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
                try
                {
                    Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                    Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];
                    DateTime minDate = new DateTime(1900, 1, 1);

                    /*
                    //Tên doanh nghiệp
                    if (TKMD.NgayTiepNhan > minDate)
                        workSheet.GetCell("Q6").Value = TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("Q6").Value = "";

                    //Mã doanh nghiệp
                    if (TKMD.SoToKhai > 0)
                        workSheet.GetCell("AB5").Value = TKMD.SoToKhai + "";
                    else
                        workSheet.GetCell("AB5").Value = "";
                    //Ngày, giờ đăng ký
                    if (TKMD.NgayDangKy > minDate)
                        workSheet.GetCell("AB6").Value = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("AB6").Value = "";

                    if (TKMD.MaLoaiHinh.Substring(0, 1) == "X")
                    {
                        //1.Người xuất khẩu A8
                        workSheet.GetCell("A8").Value = (TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI).ToUpper();
                        //2.Người nhập khẩu A10
                        workSheet.GetCell("A10").Value = TKMD.TenDonViDoiTac.ToUpper();
                    }
                    else if (TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                    {
                        //1.Người xuất khẩu A8
                        workSheet.GetCell("A8").Value = TKMD.TenDonViDoiTac.ToUpper();
                        //2.Người nhập khẩu A10
                        workSheet.GetCell("A10").Value = (TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI).ToUpper();
                    }

                    //3.Người ủy thác A12
                    workSheet.GetCell("A12").Value = TKMD.TenDonViUT;
                    //4.Đại lý làm thủ tục Hải quan A14
                    workSheet.GetCell("A14").Value = TKMD.MaDaiLyTTHQ + "." + TKMD.TenDaiLyTTHQ;
                    //5. Loại hình P7
                    string stlh = "";
                    stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                    workSheet.GetCell("P7").Value = TKMD.MaLoaiHinh + stlh;
                    //6. Hóa đơn thương mại M9, Ngày O10
                    workSheet.GetCell("M9").Value = TKMD.SoHoaDonThuongMai;
                    if (TKMD.NgayHoaDonThuongMai > minDate)
                        workSheet.GetCell("O10").Value = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("O10").Value = "";
                    //7. Giấy phép U8, Ngày U9, Ngày hết hạn U10
                    if (TKMD.SoGiayPhep != "")
                        workSheet.GetCell("U8").Value = "" + TKMD.SoGiayPhep;
                    else
                        workSheet.GetCell("U8").Value = "";

                    if (TKMD.NgayGiayPhep > minDate)
                        workSheet.GetCell("U9").Value = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("U9").Value = "";

                    if (TKMD.NgayHetHanGiayPhep > minDate)
                        workSheet.GetCell("U10").Value = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("U10").Value = "";
                    //8. Hợp đồng AA8, Ngày AB9, Ngày hết hạn AB10
                    workSheet.GetCell("AA8").Value = "" + TKMD.SoHopDong;

                    if (TKMD.NgayHopDong > minDate)
                        workSheet.GetCell("AB9").Value = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("AB9").Value = "";

                    if (TKMD.NgayHetHanHopDong > minDate)
                        workSheet.GetCell("AB10").Value = "" + TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("AB10").Value = "";

                    //9. Vận tải đơn P11, Ngày N12
                    workSheet.GetCell("P11").Value = TKMD.SoVanDon;
                    if (TKMD.NgayVanDon > minDate)
                        workSheet.GetCell("N12").Value = TKMD.NgayVanDon.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("N12").Value = "";
                    //10. Cảng xếp hàng R12
                    workSheet.GetCell("R12").Value = TKMD.DiaDiemXepHang;
                    //11 Cảng dỡ hàng AB11, Tên cảng X12
                    workSheet.GetCell("AB11").Value = TKMD.CuaKhau_ID;
                    workSheet.GetCell("X12").Value = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);
                    //12. Phương tiện vận tải R13, Tên M14
                    workSheet.GetCell("R13").Value = " " + TKMD.SoHieuPTVT;
                    workSheet.GetCell("M14").Value = Company.KDT.SHARE.Components.DuLieuChuan.PhuongThucVanTai.getName(TKMD.PTVT_ID);
                    //13. Nước xuất khẩu AC13, Tên X14
                    workSheet.GetCell("AC13").Value = TKMD.NuocXK_ID;
                    workSheet.GetCell("X14").Value = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(TKMD.NuocXK_ID);
                    //14. Điều kiện giao hàng R15
                    workSheet.GetCell("R15").Value = TKMD.DKGH_ID;
                    //15. Phương thức thanh toán AA15
                    workSheet.GetCell("AA15").Value = TKMD.PTTT_ID;
                    //16. Đồng tiền thanh toán S16
                    workSheet.GetCell("S16").Value = TKMD.NguyenTe_ID;
                    //17. Tỷ giá tính thuế Z16
                    workSheet.GetCell("Z16").Value = TKMD.TyGiaTinhThue.ToString("G10");
                    //18. Kết quả phân luồng và hướng dẫn làm thủ tục Hải quan: A18
                    workSheet.GetCell("A18").Value = TKMD.HUONGDAN;
                    //19. Chứng từ Hải quan trước: X18

                    /*THÔNG TIN HÀNG
                    //20. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                    //21. Mã số hàng hóa: K22, K23, K24
                    //22. Xuất xứ: O22, O23, O24
                    //23. Số lượng: R22, R23, R24
                    //24. Đơn vị tính: U22, U23, U24
                    //25. Đơn giá nguyên tệ: X22, X23, X24
                    //26. Trị giá nguyên tệ: AB22, AN23, AB24
                    //Cộng: AD25

                    //Truoc 26. Phi bao hiem B25
                    string st = "";
                    if (TKMD.PhiBaoHiem > 0)
                        st = "I = " + TKMD.PhiBaoHiem.ToString("N2");
                    if (TKMD.PhiVanChuyen > 0)
                        st += " F = " + TKMD.PhiVanChuyen.ToString("N2");
                    if (TKMD.PhiKhac > 0)
                        st += " Phí khác = " + TKMD.PhiKhac.ToString("N2");
                    workSheet.GetCell("B25").Value = st;

                    /*THÔNG TIN THUẾ
                    //27. Thuế nhập khẩu
                    //Trị giá tính thuế: B28, B29, B30
                    //Thuế suất (%): F28, F29, F30
                    //Tiền thuế: I28, I29, I30
                    //Cộng: K31
                    //28. Thuế GTGT (hoặc TTĐB)
                    //Trị giá tính thuế: L28, L29, L30
                    //Thuế suất (%): P28, P29, P30
                    //Tiền thuế: S28, S29, S30
                    //29. Thu khác
                    //Tỷ lệ (%): W28, W29, W30
                    //Số tiền: AA28, AA29, AA30
                    //30. Tổng số tiền thuế và thu khác (ô 27+28 + 29) bằng số: A32, Bằng chữ E33

                    if (dtXuatNhapTon.Rows.Count > 0)
                    {
                        #region Chi tiet hang hoa

                        Company.BLL.KDT.HangMauDich hmd = null;
                        for (int i = 0; i < TKMD.HMDCollection.Count; i++)
                        {
                            hmd = TKMD.HMDCollection[i];

                            #region THÔNG TIN HÀNG
                            //20. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                            if (!inMaHang)
                            {
                                if (hmd.Ma_HTS.Trim().Length > 0)
                                    tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                                else
                                    tenHang = hmd.TenHang;
                            }
                            else
                            {
                                if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                                    tenHang = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                                else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                                    tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                                else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                                    tenHang = hmd.TenHang + "/" + hmd.MaPhu;
                                else
                                    tenHang = hmd.TenHang;
                            }
                            workSheet.Rows[workSheet.GetCell("B22").RowIndex + i].Cells[1].Value = tenHang;

                            //21. Mã số hàng hóa: K22, K23, K24
                            // Create a merged region
                            mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("K22").RowIndex + i, 11, workSheet.GetCell("K22").RowIndex + i, 13);
                            mergedRegion.Value = hmd.MaHS;
                            mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                            //19. Xuất xứ: O22, O23, O24
                            mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("O22").RowIndex + i, 14, workSheet.GetCell("O22").RowIndex + i, 16);
                            mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                            mergedRegion.Value = hmd.NuocXX_ID;
                            //20. Số lượng: R22, R23, R24 hoac T22
                            workSheet.Rows[workSheet.GetCell("T22").RowIndex + i].Cells[workSheet.GetCell("T22").ColumnIndex].Value = hmd.SoLuong.ToString("N" + soThapPhanSoLuong);
                            workSheet.Rows[workSheet.GetCell("T22").RowIndex + i].Cells[workSheet.GetCell("T22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //21. Đơn vị tính: U22, U23, U24
                            mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("U22").RowIndex + i, 20, workSheet.GetCell("U22").RowIndex + i, 22);
                            mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                            mergedRegion.Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(hmd.DVT_ID);
                            //22. Đơn giá nguyên tệ: X22, X23, X24 hoac AA22
                            workSheet.Rows[workSheet.GetCell("AA22").RowIndex + i].Cells[workSheet.GetCell("AA22").ColumnIndex].Value = hmd.DonGiaKB.ToString("G" + soThapPhanDonGia);
                            workSheet.Rows[workSheet.GetCell("AA22").RowIndex + i].Cells[workSheet.GetCell("AA22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //23. Trị giá nguyên tệ: AB22, AN23, AB24 hoac AD22
                            workSheet.Rows[workSheet.GetCell("AD22").RowIndex + i].Cells[workSheet.GetCell("AD22").ColumnIndex].Value = hmd.TriGiaKB.ToString("N" + soThapPhanTriGiaNguyenTe);
                            workSheet.Rows[workSheet.GetCell("AD22").RowIndex + i].Cells[workSheet.GetCell("AD22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                            tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);

                            #endregion

                            #region THÔNG TIN THUẾ

                            if (!mienThueXuatKhau)
                            {
                                //27. Thuế nhập khẩu
                                //Trị giá tính thuế: E28
                                workSheet.Rows[workSheet.GetCell("E28").RowIndex + i].Cells[workSheet.GetCell("E28").ColumnIndex].Value = hmd.TriGiaTT.ToString("N" + soThapPhanTriGiaTinhThue);
                                workSheet.Rows[workSheet.GetCell("E28").RowIndex + i].Cells[workSheet.GetCell("E28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Thuế suất (%): H28
                                workSheet.Rows[workSheet.GetCell("H28").RowIndex + i].Cells[workSheet.GetCell("H28").ColumnIndex].Value = hmd.ThueSuatXNK.ToString("N" + soThapPhanThueSuat);
                                workSheet.Rows[workSheet.GetCell("H28").RowIndex + i].Cells[workSheet.GetCell("H28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Tiền thuế: K28
                                workSheet.Rows[workSheet.GetCell("K28").RowIndex + i].Cells[workSheet.GetCell("K28").ColumnIndex].Value = hmd.ThueXNK.ToString("N" + soThapPhanTienThue);
                                workSheet.Rows[workSheet.GetCell("K28").RowIndex + i].Cells[workSheet.GetCell("K28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            }

                            if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                            {
                                decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                if (!mienThueXuatKhau)
                                {
                                    //28. Thuế GTGT
                                    //Trị giá tính thuế: O28
                                    workSheet.Rows[workSheet.GetCell("O28").RowIndex + i].Cells[workSheet.GetCell("O28").ColumnIndex].Value = TriGiaTTGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("O28").RowIndex + i].Cells[workSheet.GetCell("O28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                    //Tỷ lệ (%): R28
                                    workSheet.Rows[workSheet.GetCell("R28").RowIndex + i].Cells[workSheet.GetCell("R28").ColumnIndex].Value = hmd.ThueSuatGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("R28").RowIndex + i].Cells[workSheet.GetCell("R28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                    //Tiền thuế: V28
                                    workSheet.Rows[workSheet.GetCell("V28").RowIndex + i].Cells[workSheet.GetCell("V28").ColumnIndex].Value = hmd.ThueGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("V28").RowIndex + i].Cells[workSheet.GetCell("V28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                                    //29. Thu khác
                                    //Tỷ lệ (%): Z28
                                    workSheet.Rows[workSheet.GetCell("Z28").RowIndex + i].Cells[workSheet.GetCell("Z28").ColumnIndex].Value = hmd.TyLeThuKhac.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("Z28").RowIndex + i].Cells[workSheet.GetCell("Z28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                    //Số tiền: AD28
                                    workSheet.Rows[workSheet.GetCell("AD28").RowIndex + i].Cells[workSheet.GetCell("AD28").ColumnIndex].Value = hmd.TriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                                    workSheet.Rows[workSheet.GetCell("AD28").RowIndex + i].Cells[workSheet.GetCell("AD28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                }

                                tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                            {
                                decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                            {
                                decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                            }

                            //Tiền thuế Xuất nhập khẩu
                            tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                            //Tổng tiền thuế và thu khác
                            tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);

                            #endregion
                        }

                        #endregion
                    }
                    
                    //Bằng chữ
                    string s = Company.BLL.Utils.VNCurrency.ToString(tongTienThueTatCa).Trim();
                    s = s[0].ToString().ToUpper() + s.Substring(1);
                    workSheet.GetCell("E33").Value = s.Replace("  ", " ");
                    workSheet.GetCell("E33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Left;
                                        

                    //Chọn đường dẫn file cần lưu
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                    string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                    saveFileDialog1.FileName = "TK" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + TKMD.NamDK + "_" + dateNow;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        destFile = saveFileDialog1.FileName;
                        //Ghi nội dung file gốc vào file cần lưu
                        try
                        {
                            byte[] sourceFile = Globals.ReadFile(sourcePath);
                            Globals.WriteFile(destFile, sourceFile);
                            workBook.Save(destFile);
                            ShowMessage("Lưu tập tin thành công.\n" + destFile, false);
                            System.Diagnostics.Process.Start(destFile);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message, false);
                            return;
                        }
                    }
                    */
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + ex.Message, false);
                    return;
                }
            }
        }

        #endregion

        #region Send email

        public static void sendEmail(string fromEmail, string toEmail, string subject, string body)
        {
            try
            {
                //MailMessage mail = new MailMessage();

                //mail.From = new MailAddress(fromEmail, fromEmail);
                //mail.To.Add(toEmail);

                //mail.Subject = subject;
                //mail.Body = body;

                //SmtpClient smtp = new SmtpClient();
                //smtp.Send(mail);
                ////smtp.SendAsync(mail, "");

                System.Net.Mail.SmtpClient objSMTP = new System.Net.Mail.SmtpClient("mail.softech.vn");

                System.Net.Mail.MailMessage objMessage = new System.Net.Mail.MailMessage(fromEmail, toEmail);

                //System.Net.Mail.MailMessage objMessage = new System.Net.Mail.MailMessage("softech@softech.vn", "huypvt@softech.vn");

                objMessage.IsBodyHtml = true;

                objMessage.Subject = subject;

                objMessage.Body = body;


                objSMTP.Send(objMessage);



                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());

            }
            catch (Exception ex) { }
        }

        #endregion

        #region ẤN ĐỊNH THUẾ

        /// <summary>
        /// Lấy thông tin XML phản hồi từ Hải quan lưu trong kết quả xử lý và cập nhật lại thông tin Ấn định thuế.
        /// </summary>
        /// <param name="TKMD"></param>
        public static void AnDinhThue_InsertFromXML(Company.BLL.KDT.ToKhaiMauDich TKMD)
        {
            try
            {
                //Thong Bao Thue By DATLMQ 24/01/2011
                bool IsThongBaoThue = false;
                string SoQD = "";
                DateTime NgayQD = new DateTime();
                DateTime NgayHetHan = new DateTime();
                string TaiKhoanKhoBac = "";
                string TenKhoBac = "";
                //Thue XNK
                string ChuongThueXNK = "";
                string LoaiThueXNK = "";
                string KhoanThueXNK = "";
                string MucThueXNK = "";
                string TieuMucThueXNK = "";
                double SoTienThueXNK = 0;
                //Thue VAT
                string ChuongThueVAT = "";
                string LoaiThueVAT = "";
                string KhoanThueVAT = "";
                string MucThueVAT = "";
                string TieuMucThueVAT = "";
                double SoTienThueVAT = 0;
                //Thue TTDB
                string ChuongThueTTDB = "";
                string LoaiThueTTDB = "";
                string KhoanThueTTDB = "";
                string MucThueTTDB = "";
                string TieuMucThueTTDB = "";
                double SoTienThueTTDB = 0;
                //Thue TVCBPG
                string ChuongThueTVCBPG = "";
                string LoaiThueTVCBPG = "";
                string KhoanThueTVCBPG = "";
                string MucThueTVCBPG = "";
                string TieuMucThueTVCBPG = "";
                double SoTienThueTVCBPG = 0;

                XmlDocument docNPL = new XmlDocument();

                //1. Lấy thông tin Ấn định thuế từ XML trả về từ Hải quan.
                List<Company.KDT.SHARE.Components.Message> listMsg = Company.KDT.SHARE.Components.Message.SelectCollectionBy_ItemID(TKMD.ID, string.Empty);

                //2. Lọc thông tin Ấn định thuế
                foreach (Company.KDT.SHARE.Components.Message msg in listMsg)
                {
                    if (msg.MessageFrom != null && msg.MessageFrom.Trim().ToLower() == GlobalSettings.MA_HAI_QUAN.Trim().ToLower())
                    {
                        docNPL.LoadXml(msg.MessageContent);

                        #region BEGIN AN DINH THUE

                        //Convert noi dung da ma hoa cua V3
                        if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        {
                            FeedBackContent feedbackContent = Helpers.GetFeedBackContent(msg.MessageContent);
                            string noidung = feedbackContent.AdditionalInformations[0].Content.Text;

                            switch (feedbackContent.Function)
                            {
                                case DeclarationFunction.DUYET_LUONG_CHUNG_TU:

                                    string idTax = string.Empty;
                                    string tenKhobac = string.Empty;

                                    foreach (AdditionalInformation item in feedbackContent.AdditionalInformations)
                                    {
                                        if (item.Statement == AdditionalInformationStatement.TAI_KHOAN_KHO_BAC)
                                        {
                                            idTax = item.Content.Text;
                                        }
                                        else if (item.Statement == AdditionalInformationStatement.TEN_KHO_BAC)
                                        {
                                            tenKhobac = item.Content.Text;
                                        }
                                    }

                                    if (idTax != string.Empty && feedbackContent.AdditionalDocument != null)
                                    {
                                        noidung += "\r\nThông báo thuế của hệ thống Hải quan:\r\n";

                                        Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue andinhthue = new Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue();
                                        andinhthue.TaiKhoanKhoBac = idTax;
                                        andinhthue.TKMD_ID = TKMD.ID;
                                        andinhthue.TKMD_Ref = TKMD.GUIDSTR;
                                        andinhthue.NgayQuyetDinh = SingleMessage.GetDate(feedbackContent.AdditionalDocument.Issue);
                                        andinhthue.NgayHetHan = SingleMessage.GetDate(feedbackContent.AdditionalDocument.Expire);
                                        andinhthue.SoQuyetDinh = feedbackContent.AdditionalDocument.Reference.Trim();
                                        andinhthue.TenKhoBac = tenKhobac;
                                        noidung += "Số quyết định: " + andinhthue.SoQuyetDinh;
                                        noidung += "\r\nTài khoản kho bạc: " + idTax;
                                        noidung += "\r\nTên kho bạc: " + tenKhobac;
                                        noidung += "\r\nNgày quyết định: " + andinhthue.NgayQuyetDinh.ToString("dd-MM-yyyy HH:mm:ss");
                                        noidung += "\r\nNgày hết hạn: " + andinhthue.NgayHetHan.ToLongDateString();

                                        andinhthue.Insert();

                                        foreach (DutyTaxFee item in feedbackContent.AdditionalDocument.DutyTaxFees)
                                        {
                                            Company.KDT.SHARE.Components.AnDinhThue.ChiTiet chiTietThue = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();
                                            chiTietThue.IDAnDinhThue = andinhthue.ID;

                                            string[] spl = item.AdValoremTaxBase.Split(new char[] { '.' });

                                            if (spl.Length == 2)
                                                if (System.Convert.ToDecimal(spl[1]) == 0)
                                                    chiTietThue.TienThue = System.Convert.ToDecimal(spl[0]);
                                                else
                                                    chiTietThue.TienThue = System.Convert.ToDecimal(item.AdValoremTaxBase);

                                            switch (item.Type.Trim())
                                            {
                                                case DutyTaxFeeType.THUE_XNK:
                                                    chiTietThue.SacThue = "XNK";
                                                    break;
                                                case DutyTaxFeeType.THUE_VAT:
                                                    chiTietThue.SacThue = "VAT";
                                                    break;
                                                case DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET:
                                                    chiTietThue.SacThue = "TTDB";
                                                    break;
                                                case DutyTaxFeeType.THUE_KHAC:
                                                    chiTietThue.SacThue = "TVCBPG";
                                                    break;
                                                default:
                                                    break;
                                            }

                                            foreach (AdditionalInformation additionalInfo in item.AdditionalInformations)
                                            {
                                                if (!string.IsNullOrEmpty(additionalInfo.Content.Text))
                                                    switch (additionalInfo.Statement)
                                                    {
                                                        case "211"://Chuong
                                                            chiTietThue.Chuong = additionalInfo.Content.Text;
                                                            break;
                                                        case "212"://Loai
                                                            chiTietThue.Loai = Convert.ToInt32(additionalInfo.Content.Text);
                                                            break;
                                                        case "213"://Khoan
                                                            chiTietThue.Khoan = Convert.ToInt32(additionalInfo.Content.Text);
                                                            break;
                                                        case "214"://Muc
                                                            chiTietThue.Muc = Convert.ToInt32(additionalInfo.Content.Text);
                                                            break;
                                                        case "215"://Tieu muc
                                                            chiTietThue.TieuMuc = Convert.ToInt32(additionalInfo.Content.Text);
                                                            break;
                                                    }
                                            }
                                            chiTietThue.Insert();
                                        }

                                        Company.KDT.SHARE.Components.Globals.SaveMessage(msg.MessageContent, TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiAnDinhThue, noidung);

                                    }
                                    break;
                            }
                        }
                        else
                        {
                            //DATLMQ bổ sung In ra thông báo thuế 06/08/2011
                            XmlNode xmlNodeAnDinhThue = docNPL.SelectSingleNode("Envelope/Body/Content/Root/AN_DINH_THUE");
                            if (xmlNodeAnDinhThue != null)
                            {
                                IsThongBaoThue = true;
                                SoQD = xmlNodeAnDinhThue.Attributes["SOQD"].Value;
                                NgayQD = Convert.ToDateTime(xmlNodeAnDinhThue.Attributes["NGAYQD"].Value);
                                NgayHetHan = Convert.ToDateTime(xmlNodeAnDinhThue.Attributes["NGAYHH"].Value);
                                TaiKhoanKhoBac = xmlNodeAnDinhThue.Attributes["TKKB"].Value;
                                TenKhoBac = xmlNodeAnDinhThue.Attributes["KHOBAC"].Value;

                                //Kiem tra thong tin an dinh thue cua to khai
                                Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue anDinhThueObj = new Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue();
                                List<Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue> dsADT = (List<Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                                if (dsADT.Count > 0)
                                {
                                    anDinhThueObj.ID = dsADT[0].ID;
                                }

                                //Luu an dinh thue
                                anDinhThueObj.SoQuyetDinh = SoQD;
                                anDinhThueObj.NgayQuyetDinh = NgayQD;
                                anDinhThueObj.NgayHetHan = NgayHetHan;
                                anDinhThueObj.TaiKhoanKhoBac = TaiKhoanKhoBac;
                                anDinhThueObj.TenKhoBac = TenKhoBac;
                                anDinhThueObj.GhiChu = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}",
                                    TKMD.SoToKhai, TKMD.NgayDangKy.ToString(), TKMD.MaLoaiHinh.Trim(), TKMD.MaHaiQuan.Trim(), Company.KDT.SHARE.Components.Globals.GetPhanLuong(TKMD.PhanLuong), TKMD.HUONGDAN);
                                anDinhThueObj.TKMD_ID = TKMD.ID;
                                anDinhThueObj.TKMD_Ref = TKMD.GUIDSTR;
                                if (anDinhThueObj.ID == 0)
                                    anDinhThueObj.Insert();
                                else
                                    anDinhThueObj.InsertUpdate();

                                #region Begin Danh sach thue chi tiet
                                Company.KDT.SHARE.Components.AnDinhThue.ChiTiet thueObj = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();

                                XmlNodeList xmlNodeThue = docNPL.SelectNodes("Envelope/Body/Content/Root/AN_DINH_THUE/THUE");
                                foreach (XmlNode node in xmlNodeThue)
                                {
                                    if (node.Attributes["SACTHUE"].Value.Equals("XNK"))
                                    {
                                        ChuongThueXNK = node.Attributes["CHUONG"].Value;
                                        SoTienThueXNK = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                                        LoaiThueXNK = node.Attributes["LOAI"].Value;
                                        KhoanThueXNK = node.Attributes["KHOAN"].Value;
                                        MucThueXNK = node.Attributes["MUC"].Value;
                                        TieuMucThueXNK = node.Attributes["TIEUMUC"].Value;

                                        //Tao doi tuong thue XNK
                                        thueObj = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();
                                        thueObj.SacThue = "XNK";
                                        thueObj.Chuong = ChuongThueXNK;
                                        thueObj.TienThue = (decimal)SoTienThueXNK;
                                        thueObj.Loai = int.Parse(LoaiThueXNK);
                                        thueObj.Khoan = int.Parse(KhoanThueXNK);
                                        thueObj.Muc = int.Parse(MucThueXNK);
                                        thueObj.TieuMuc = int.Parse(TieuMucThueXNK);
                                        thueObj.IDAnDinhThue = anDinhThueObj.ID;
                                        thueObj.InsertUpdateBy(null);
                                    }
                                    if (node.Attributes["SACTHUE"].Value.Equals("VAT"))
                                    {
                                        ChuongThueVAT = node.Attributes["CHUONG"].Value;
                                        SoTienThueVAT = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                                        LoaiThueVAT = node.Attributes["LOAI"].Value;
                                        KhoanThueVAT = node.Attributes["KHOAN"].Value;
                                        MucThueVAT = node.Attributes["MUC"].Value;
                                        TieuMucThueVAT = node.Attributes["TIEUMUC"].Value;

                                        //Tao doi tuong thue VAT
                                        thueObj = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();
                                        thueObj.SacThue = "VAT";
                                        thueObj.Chuong = ChuongThueVAT;
                                        thueObj.TienThue = (decimal)SoTienThueVAT;
                                        thueObj.Loai = int.Parse(LoaiThueVAT);
                                        thueObj.Khoan = int.Parse(KhoanThueVAT);
                                        thueObj.Muc = int.Parse(MucThueVAT);
                                        thueObj.TieuMuc = int.Parse(TieuMucThueVAT);
                                        thueObj.IDAnDinhThue = anDinhThueObj.ID;
                                        thueObj.InsertUpdateBy(null);
                                    }
                                    if (node.Attributes["SACTHUE"].Value.Equals("TTDB"))
                                    {
                                        ChuongThueTTDB = node.Attributes["CHUONG"].Value;
                                        SoTienThueTTDB = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                                        LoaiThueTTDB = node.Attributes["LOAI"].Value;
                                        KhoanThueTTDB = node.Attributes["KHOAN"].Value;
                                        MucThueTTDB = node.Attributes["MUC"].Value;
                                        TieuMucThueTTDB = node.Attributes["TIEUMUC"].Value;

                                        //Tao doi tuong thue Tieu thu dac biet
                                        thueObj = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();
                                        thueObj.SacThue = "TTDB";
                                        thueObj.Chuong = ChuongThueTTDB;
                                        thueObj.TienThue = (decimal)SoTienThueTTDB;
                                        thueObj.Loai = int.Parse(LoaiThueTTDB);
                                        thueObj.Khoan = int.Parse(KhoanThueTTDB);
                                        thueObj.Muc = int.Parse(MucThueTTDB);
                                        thueObj.TieuMuc = int.Parse(TieuMucThueTTDB);
                                        thueObj.IDAnDinhThue = anDinhThueObj.ID;
                                        thueObj.InsertUpdateBy(null);
                                    }
                                    if (node.Attributes["SACTHUE"].Value.Equals("TVCBPG"))
                                    {
                                        ChuongThueTVCBPG = node.Attributes["CHUONG"].Value;
                                        SoTienThueTVCBPG = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                                        LoaiThueTVCBPG = node.Attributes["LOAI"].Value;
                                        KhoanThueTVCBPG = node.Attributes["KHOAN"].Value;
                                        MucThueTVCBPG = node.Attributes["MUC"].Value;
                                        TieuMucThueTVCBPG = node.Attributes["TIEUMUC"].Value;

                                        //Tao doi tuong thue Thu chenh lech gia
                                        thueObj = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();
                                        thueObj.SacThue = "TVCBPG";
                                        thueObj.Chuong = ChuongThueTVCBPG;
                                        thueObj.TienThue = (decimal)SoTienThueTVCBPG;
                                        thueObj.Loai = int.Parse(LoaiThueTVCBPG);
                                        thueObj.Khoan = int.Parse(KhoanThueTVCBPG);
                                        thueObj.Muc = int.Parse(MucThueTVCBPG);
                                        thueObj.TieuMuc = int.Parse(TieuMucThueTVCBPG);
                                        thueObj.IDAnDinhThue = anDinhThueObj.ID;
                                        thueObj.InsertUpdateBy(null);
                                    }
                                }


                                #endregion End Danh sach thue chi tiet

                                KetQuaXuLy kqxlAnDinhThue = new KetQuaXuLy();
                                kqxlAnDinhThue.ItemID = TKMD.ID;
                                kqxlAnDinhThue.ReferenceID = new Guid(TKMD.GUIDSTR);
                                kqxlAnDinhThue.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                                kqxlAnDinhThue.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_AnDinhThueToKhai;
                                kqxlAnDinhThue.NoiDung = string.Format("Ấn định thuế:\r\nSố quyết định: {0}\r\nNgày quyết định: {1}\r\nNgày hết hạn: {2}\r\nTài khoản kho bạc: {3}\r\nKho bạc: {4}", SoQD, NgayQD.ToString(), NgayHetHan.ToString(), TaiKhoanKhoBac, TenKhoBac);
                                kqxlAnDinhThue.Ngay = DateTime.Now;
                                kqxlAnDinhThue.Insert();

                                Company.KDT.SHARE.Components.Globals.SaveMessage(msg.MessageContent, TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiAnDinhThue, kqxlAnDinhThue.NoiDung);
                            }
                        }
                        #endregion END AN DINH THUE
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(string.Format("Ấn định thuế tờ khai số: {0}, ngày đăng ký: {1}, mã loại hình: {2}, ID: {3}", TKMD.SoToKhai, TKMD.NgayDangKy.ToString(), TKMD.MaLoaiHinh, TKMD.GUIDSTR), ex); }
        }

        #endregion

    }
}
