﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
#if GC_V4
using Company.GC.BLL.GC;
#endif
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.IO;
using Company.KDT.SHARE.VNACCS;
namespace Company.Interface.GC
{
    public partial class ReadExcelFormNPLSP : BaseForm
    {
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem;
#if GC_V4
        public T_GC_NPL_QUYETOAN NPLQT;
        public List<T_GC_NPL_QUYETOAN> ListNPLQuyetToan = new List<T_GC_NPL_QUYETOAN>();
#endif
        public string MaNPL;
        public ReadExcelFormNPLSP()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.ShowDialog(this);
                txtFilePath.Text = sf.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                // Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //showMsg("MSG_0203008");
                    //ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                //Dictionary<string, Worksheet> dict = new Dictionary<string, Worksheet>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                    //dict.Add(worksheet.Name, worksheet);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnReadFile_Click(object sender, EventArgs e)
        {
            #region Old
            //cvError.Validate();
            //if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRowIndex.Value) -1 ;
            if (beginRow < 0)
            {
                //error.SetError(txtRowIndex, "Dòng bắt đầu phải lớn hơn 0");
                //error.SetIconPadding(txtRowIndex, 8);
                return;

            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                ShowMessage("Không tồn tại sheet \"" + cbbSheetName.Text + "", false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char STTColumn = Convert.ToChar(txtSTT.Text.Trim());
            int STTCol = ConvertCharToInt(STTColumn);

            char LoaiHHHColumn = Convert.ToChar(txtLoaiHangHoa.Text.Trim());
            int LoaiHHCol = ConvertCharToInt(LoaiHHHColumn);

            char TaiKhoanColumn = Convert.ToChar(txtTaiKhoan.Text.Trim());
            int TaiKhoanCol = ConvertCharToInt(TaiKhoanColumn);

            char MaHHColumn = Convert.ToChar(txtMaHangHoa.Text.Trim());
            int MaHHCol = ConvertCharToInt(MaHHColumn);

            char TenHHColumn = Convert.ToChar(txtTenHangHoa.Text.Trim());
            int TenHHCol = ConvertCharToInt(TenHHColumn);

            char DVTColumn = Convert.ToChar(txtDVT.Text.Trim());
            int DVTCol = ConvertCharToInt(DVTColumn);

            char TonDKColumn = Convert.ToChar(txtTonDauKy.Text.Trim());
            int TonDKCol = ConvertCharToInt(TonDKColumn);

            char NhapTKColumn = Convert.ToChar(txtNhapTrongKy.Text.Trim());
            int NhapTKCol = ConvertCharToInt(NhapTKColumn);

            char XuatTKColumn = Convert.ToChar(txtXuatTrongKy.Text.Trim());
            int XuatTKCol = ConvertCharToInt(XuatTKColumn);

            char TonCKColumn = Convert.ToChar(txtTonCuoiKy.Text.Trim());
            int TonCKCol = ConvertCharToInt(TonCKColumn);

            char GhiChuColumn = Convert.ToChar(txtGhiChu.Text.Trim());
            int GhiChuCol = ConvertCharToInt(GhiChuColumn);

            string errorTotal = "";
            string errorSTT = "";
            string errorLoaiHangHoa = "";
            string errorLoaiHangHoaValid = "";
            string errorTaiKhoan = "";
            string errorTaiKhoanValid = "";
            string errorTenHangHoa = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorTonDauKy = "";
            string errorNhapTrongKy = "";
            string errorXuatTrongKy = "";
            string errorTonCuoiKy = "";

            string errorTonDauKyValid = "";
            string errorNhapTrongKyValid = "";
            string errorXuatTrongKyValid = "";
            string errorTonCuoiKyValid = "";
            string errorToTalTonCuoiKy = "";

            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        bool isAdd = true;
                        KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail GoodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
                        GoodItem.STT = Convert.ToInt32(wsr.Cells[STTCol].Value);
                        if (GoodItem.STT.ToString().Length == 0)
                        {
                            errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.STT + "],";
                            isAdd = false;
                        }
                        try
                        {
                            GoodItem.LoaiHangHoa = Convert.ToInt32(wsr.Cells[LoaiHHCol].Value);
                            if (GoodItem.LoaiHangHoa.ToString().Trim().Length == 0)
                            {
                                errorLoaiHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LoaiHangHoa + "],";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorLoaiHangHoaValid += "[" + (wsr.Index + 1) + "]-[" + Convert.ToString(wsr.Cells[LoaiHHCol].Value) + "],";
                            isAdd = false;
                        }
                        try
                        {
                            GoodItem.TaiKhoan = Convert.ToInt32(wsr.Cells[TaiKhoanCol].Value);
                            if (GoodItem.TaiKhoan.ToString().Trim().Length == 0)
                            {
                                errorTaiKhoan += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TaiKhoan + "],";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTaiKhoanValid += "[" + (wsr.Index + 1) + "]-[" + Convert.ToString(wsr.Cells[TaiKhoanCol].Value) + "],";
                            isAdd = false;
                        }
                        GoodItem.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                        GoodItem.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                        if (GoodItem.TenHangHoa.Trim().Length == 0)
                        {
                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TenHangHoa + "],";
                            isAdd = false;
                        }
                        GoodItem.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                        bool isExits = false;
                        if (GoodItem.DVT.Trim().Length == 0)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "],";
                            isAdd = false;
                        }
                        else
                        {
                            List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
                            foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                            {
                                if (item.Code == GoodItem.DVT)
                                {
                                    isExits = true;
                                }
                            }
                        }
                        if (!isExits)
                        {
                            errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "],";
                            isAdd = false;
                        }
                        isExits = false;
                        GoodItem.TonDauKy = Convert.ToDecimal(wsr.Cells[TonDKCol].Value);
                        if (GoodItem.TonDauKy.ToString().Trim().Length == 0)
                        {
                            errorTonDauKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonDauKy + "],";
                            isAdd = false;
                        }
                        else
                        {
                            if (Decimal.Round(GoodItem.TonDauKy, 4) != GoodItem.TonDauKy)
                            {
                                errorTonDauKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonDauKy + "],";
                                isAdd = false;
                            }
                        }
                        GoodItem.NhapTrongKy = Convert.ToDecimal(wsr.Cells[NhapTKCol].Value);
                        if (GoodItem.NhapTrongKy.ToString().Trim().Length == 0)
                        {
                            errorNhapTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.NhapTrongKy + "],";
                            isAdd = false;
                        }
                        else
                        {
                            if (Decimal.Round(GoodItem.NhapTrongKy, 4) != GoodItem.NhapTrongKy)
                            {
                                errorNhapTrongKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.NhapTrongKy + "],";
                                isAdd = false;
                            }
                        }
                        GoodItem.XuatTrongKy = Convert.ToDecimal(wsr.Cells[XuatTKCol].Value);
                        if (GoodItem.XuatTrongKy.ToString().Trim().Length == 0)
                        {
                            errorXuatTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.XuatTrongKy + "],";
                            isAdd = false;
                        }
                        else
                        {
                            if (Decimal.Round(GoodItem.XuatTrongKy, 4) != GoodItem.XuatTrongKy)
                            {
                                errorXuatTrongKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.XuatTrongKy + "],";
                                isAdd = false;
                            }
                        }
                        GoodItem.TonCuoiKy = Convert.ToDecimal(wsr.Cells[TonCKCol].Value);
                        if (GoodItem.TonCuoiKy.ToString().Trim().Length == 0)
                        {
                            errorTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonCuoiKy + "],";
                            isAdd = false;
                        }
                        else
                        {
                            if (Decimal.Round(GoodItem.TonCuoiKy, 4) != GoodItem.TonCuoiKy)
                            {
                                errorTonCuoiKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonCuoiKy + "],";
                                isAdd = false;
                            }
                            if (GoodItem.TonDauKy + GoodItem.NhapTrongKy - GoodItem.XuatTrongKy != GoodItem.TonCuoiKy)
                            {
                                errorToTalTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonDauKy + "]-[" + GoodItem.NhapTrongKy + "]-[" + GoodItem.XuatTrongKy + "]-[" + GoodItem.TonCuoiKy + "],";
                                isAdd = false;
                            }
                        }
                        GoodItem.GhiChu = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();
                        if(isAdd)
                            goodItem.GoodItemsCollection.Add(GoodItem);
                    }
                    catch (Exception ex)
                    {
                        return;
                    }

                    if (!String.IsNullOrEmpty(errorSTT))
                        errorTotal += "\n - [STT] -[SỐ THỨ TỰ] : " + errorSTT + "không được để trống";
                    if (!String.IsNullOrEmpty(errorLoaiHangHoa))
                        errorTotal += "\n - [STT] -[LOẠI HÀNG HÓA] : " + errorLoaiHangHoa + "không được để trống";
                    if (!String.IsNullOrEmpty(errorLoaiHangHoaValid))
                        errorTotal += "\n - [STT] -[LOẠI HÀNG HÓA] : " + errorLoaiHangHoaValid + "không đúng (Nếu hàng hóa là NPL thì nhập : 1 . Nếu hàng hóa là Thành phẩm thì nhập : 2)";
                    if (!String.IsNullOrEmpty(errorTaiKhoan))
                        errorTotal += "\n - [STT] -[TÀI KHOẢN] : " + errorTaiKhoan + "không được để trống";
                    if (!String.IsNullOrEmpty(errorTaiKhoanValid))
                        errorTotal += "\n - [STT] -[TÀI KHOẢN] : " + errorTaiKhoanValid + "không đúng (Nếu hàng hóa là NPL thì nhập : 152 . Nếu hàng hóa là Thành phẩm thì nhập : 155)";
                    if (!String.IsNullOrEmpty(errorTenHangHoa))
                        errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : " + errorTenHangHoa + "không được để trống";
                    if (!String.IsNullOrEmpty(errorDVT))
                        errorTotal += "\n - [STT] -[ĐVT] : " + errorDVT + "không được để trống";
                    if (!String.IsNullOrEmpty(errorDVTExits))
                        errorTotal += "\n - [STT] -[ĐVT] : " + errorDVTExits + "không hợp lệ. ĐVT phải là ĐVT VNACCS (Ví dụ : Cái/Chiếc là : PCE)";
                    if (!String.IsNullOrEmpty(errorTonDauKy))
                        errorTotal += "\n - [STT] -[TỒN ĐẦU KỲ] : " + errorTonDauKy + "không được để trống";
                    if (!String.IsNullOrEmpty(errorTonDauKyValid))
                        errorTotal += "\n - [STT] -[TỒN ĐẦU KỲ] : " + errorTonDauKyValid + "chỉ được nhập tối đa 4 số thập phân (Ví dụ : ****,1234)";
                    if (!String.IsNullOrEmpty(errorNhapTrongKy))
                        errorTotal += "\n - [STT] -[NHẬP TRONG KỲ] : " + errorNhapTrongKy + "không được để trống";
                    if (!String.IsNullOrEmpty(errorNhapTrongKyValid))
                        errorTotal += "\n - [STT] -[NHẬP TRONG KỲ] : " + errorNhapTrongKyValid + "chỉ được nhập tối đa 4 số thập phân (Ví dụ : ****,1234)";
                    if (!String.IsNullOrEmpty(errorXuatTrongKy))
                        errorTotal += "\n - [STT] -[XUẤT TRONG KỲ] : " + errorXuatTrongKy + "không được để trống";
                    if (!String.IsNullOrEmpty(errorXuatTrongKyValid))
                        errorTotal += "\n - [STT] -[XUẤT TRONG KỲ] : " + errorXuatTrongKyValid + "chỉ được nhập tối đa 4 số thập phân (Ví dụ : ****,1234)";
                    if (!String.IsNullOrEmpty(errorTonCuoiKy))
                        errorTotal += "\n - [STT] -[TỒN CUỐI KỲ] : " + errorTonCuoiKy + "không được để trống";
                    if (!String.IsNullOrEmpty(errorTonCuoiKyValid))
                        errorTotal += "\n - [STT] -[TỒN CUỐI KỲ] : " + errorTonCuoiKyValid + "chỉ được nhập tối đa 4 số thập phân (Ví dụ : ****,1234)";
                    if (!String.IsNullOrEmpty(errorToTalTonCuoiKy))
                        errorTotal += "\n - [STT] -[ [TỒN ĐẦU KỲ] + [NHẬP TRONG KỲ] - [XUẤT TRONG KỲ] KHÔNG BẰNG [TỒN CUỐI KỲ] ] : " + errorTonCuoiKyValid;
                }
            }
            if (!String.IsNullOrEmpty(errorTotal))
            {
                ShowMessageTQDT("Nhập hàng từ Excel thành công .", false);
            }
            else
            {
                ShowMessageTQDT("Nhập từ Excel không thành công ." + errorTotal, false);
                return;
            }
            this.Close();
            #endregion
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkFileExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GoodItem();
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
