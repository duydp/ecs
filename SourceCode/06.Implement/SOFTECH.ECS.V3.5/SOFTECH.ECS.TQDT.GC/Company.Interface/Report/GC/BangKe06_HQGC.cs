﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;
 

namespace Company.Interface.Report.GC
{
    public partial class BangKe06_HQGC : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewBC06Form report = new ReportViewBC06Form();
        public HopDong HD = new HopDong();
        public DataSet dsBK = new DataSet();
        public BangKe06_HQGC()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
           // DataTable dt = new Company.GC.BLL.GC.ThanhKhoanHDGC().getDuLieuThanhKhoan(this.HD.ID).Tables[0];

            DataTable dt = dsBK.Tables[0]; 
            dt.TableName = "t_GC_ThanhKhoanHDGC";
            this.DataSource = dt;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                }
            }
            lblMathang.Text = loaiSP; 

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblTenNPL.DataBindings.Add("Text", this.DataSource,dt.TableName + ".Ten");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblTongNK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TongLuongNK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongCU.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TongLuongCU", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongXK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TongLuongXK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblChenhLech.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChenhLech", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblKLXLCL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".KetLuanXLCL");
            xrLabel19.Text  = xrLabel11.Text = xrLabel13.Text= GlobalSettings.TieudeNgay;
            
        }

        private void xrLabel40_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string ten = GetCurrentColumnValue("Ten").ToString();
            
            //lblTenNPL.Text = ten.Substring(0,ten.LastIndexOf("(") - 1);

            string ten = GetCurrentColumnValue("Ten").ToString();
            if (!ten.Contains("(&#!"))
                lblTenNPL.Text = ten.Substring(0, ten.LastIndexOf("(") - 1);
            else
            {
                lblTenNPL.Text = ten.Substring(0, ten.LastIndexOf("(&#!") - 1);
            }
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
//             string ten = GetCurrentColumnValue("Ten").ToString();
//             string[] arr = ten.Split(new char[1] {'('});
//             lblMaNPL.Text = arr[arr.Length - 1].Replace(")","");

            string ten = GetCurrentColumnValue("Ten").ToString();
            if (!ten.Contains("&#!"))
            {
                string[] arr = ten.Split(new char[1] { '(' });
                lblMaNPL.Text = arr[arr.Length - 1].Replace(")", "");
            }
            else
            {
                string[] arr = ten.Split(new string[] { "(&#!" }, StringSplitOptions.None);
                lblMaNPL.Text = arr[arr.Length - 1].Replace("&#!)", "");
            }
        }

        private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel label = (XRLabel)sender;
            report.Label = label;
            report.txtName.Text = label.Text;
            report.lblName.Text = label.Tag.ToString();
        }
        public void setText(XRLabel label, string text)
        {
            label.Text = text;
        }
    }
}
