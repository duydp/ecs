-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_Insert]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_Update]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_Delete]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_Load]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_Insert]
	@Ma varchar(50),
	@Ten nvarchar(250),
	@HienThi bit,
	@MacDinh bit,
	@ThuTu int,
	@ParentID int,
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_GopY_LinhVuc]
(
	[Ma],
	[Ten],
	[HienThi],
	[MacDinh],
	[ThuTu],
	[ParentID]
)
VALUES 
(
	@Ma,
	@Ten,
	@HienThi,
	@MacDinh,
	@ThuTu,
	@ParentID
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_Update]
	@ID int,
	@Ma varchar(50),
	@Ten nvarchar(250),
	@HienThi bit,
	@MacDinh bit,
	@ThuTu int,
	@ParentID int
AS

UPDATE
	[dbo].[t_GopY_LinhVuc]
SET
	[Ma] = @Ma,
	[Ten] = @Ten,
	[HienThi] = @HienThi,
	[MacDinh] = @MacDinh,
	[ThuTu] = @ThuTu,
	[ParentID] = @ParentID
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_InsertUpdate]
	@ID int,
	@Ma varchar(50),
	@Ten nvarchar(250),
	@HienThi bit,
	@MacDinh bit,
	@ThuTu int,
	@ParentID int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_GopY_LinhVuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_GopY_LinhVuc] 
		SET
			[Ma] = @Ma,
			[Ten] = @Ten,
			[HienThi] = @HienThi,
			[MacDinh] = @MacDinh,
			[ThuTu] = @ThuTu,
			[ParentID] = @ParentID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_GopY_LinhVuc]
		(
			[Ma],
			[Ten],
			[HienThi],
			[MacDinh],
			[ThuTu],
			[ParentID]
		)
		VALUES 
		(
			@Ma,
			@Ten,
			@HienThi,
			@MacDinh,
			@ThuTu,
			@ParentID
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_GopY_LinhVuc]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GopY_LinhVuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[HienThi],
	[MacDinh],
	[ThuTu],
	[ParentID]
FROM
	[dbo].[t_GopY_LinhVuc]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ma],
	[Ten],
	[HienThi],
	[MacDinh],
	[ThuTu],
	[ParentID]
FROM [dbo].[t_GopY_LinhVuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[HienThi],
	[MacDinh],
	[ThuTu],
	[ParentID]
FROM
	[dbo].[t_GopY_LinhVuc]	

GO

