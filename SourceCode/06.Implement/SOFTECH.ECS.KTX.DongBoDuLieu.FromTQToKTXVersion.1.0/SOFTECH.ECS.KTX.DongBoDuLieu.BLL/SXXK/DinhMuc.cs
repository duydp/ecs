﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Company.BLL.SXXK;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DongBoDuLieu.BLL.SXXK
{
    public class DM
    {
        public static DataSet GetALLDanhSachDM(string MaHaiQuan, string MaDoanhNghiep, bool isPhongKhai, string MatKhau)
        {
            string query = "SELECT Ma_HQ, Ma_DV, Ma_SP, Ma_NPL, DM_SD, TL_HH, DM_CHUNG, GHI_CHU  FROM DDINHMUC WHERE MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDoanhNghiep + "'";
            DataSet ds = null;
            if (!isPhongKhai)
                ds = WebServiceDB.getDataBySQL(query, MaHaiQuan, "SXXK", MaDoanhNghiep, MatKhau);
            else
                ds = WebServiceDB.getDataBySQLDatabase(query, MaHaiQuan, "SXXK");
            return ds;
        }

        public static DataSet GetALLDanhSachDMLocal(string MaHaiQuan, string MaDoanhNghiep, DateTime tuNgay, DateTime denNgay, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = "SELECT DM.MaHaiQuan AS Ma_HQ, DM.MaDoanhNghiep AS MA_DV,DM.MaSanPham AS Ma_SP,DM.MaNguyenPhuLieu AS Ma_NPL,DM.DinhMucSuDung AS DM_SD, DM.TyLeHaoHut AS TL_HH,DM.DinhMucChung AS DM_CHUNG,DM.GhiChu AS GHI_CHU FROM dbo.t_SXXK_DinhMuc DM INNER JOIN dbo.t_SXXK_ThongTinDinhMuc dmdk ON DM.MaSanPham = dmdk.MaSanPham AND DM.MaDoanhNghiep = dmdk.MaDoanhNghiep AND DM.MaHaiQuan = dmdk.MaHaiQuan WHERE DM.MaHaiQuan = '" + MaHaiQuan + "' AND DM.MaDoanhNghiep = '" + MaDoanhNghiep + "'";
            if (tuNgay > DateTime.MinValue)
                query += " and  DMDK.NgayDangKy >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND  DMDK.NgayDangKy <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.MaSanPham, temp.MaNguyenPhuLieu FROM [" + databaseNameTarget + "].dbo.t_SXXK_DinhMuc temp WHERE temp.MaSanPham = dm.MaSanPham AND temp.MaNguyenPhuLieu = dm.MaNguyenPhuLieu )";
            query += " ORDER BY  DMDK.NgayDangKy";

            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);
            return ds;
        }

        public static DataSet GetALLDanhSachDMLocalSimple(string MaHaiQuan, string MaDoanhNghiep, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = "SELECT DM.MaHaiQuan AS Ma_HQ, DM.MaDoanhNghiep AS MA_DV,DM.MaSanPham AS Ma_SP,DM.MaNguyenPhuLieu AS Ma_NPL,DM.DinhMucSuDung AS DM_SD, DM.TyLeHaoHut AS TL_HH,DM.DinhMucChung AS DM_CHUNG,DM.GhiChu AS GHI_CHU FROM dbo.t_SXXK_DinhMuc DM WHERE DM.MaHaiQuan = '" + MaHaiQuan + "' AND DM.MaDoanhNghiep = '" + MaDoanhNghiep + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.MaSanPham, temp.MaNguyenPhuLieu FROM [" + databaseNameTarget + "].dbo.t_SXXK_DinhMuc temp WHERE temp.MaSanPham = dm.MaSanPham AND temp.MaNguyenPhuLieu = dm.MaNguyenPhuLieu )";

            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);
            return ds;
        }

        public static DataSet GetALLDanhSachDMLocalTQDT(string MaHaiQuan, string MaDoanhNghiep, bool trangThaiDaDuyet, DateTime tuNgay, DateTime denNgay, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = "SELECT  DM.Master_ID,  " +
                            "DMDK.SoTiepNhan, DMDK.NgayTiepNhan, DMDK.TrangThaiXuLy,  " +
                            "DM.MaSanPham AS Ma_SP,  " +
                            "DM.MaNguyenPhuLieu AS Ma_NPL,  " +
                            "DM.DinhMucSuDung AS DM_SD,  " +
                            "DM.TyLeHaoHut AS TL_HH , " +
                            "DM.GhiChu AS GHI_CHU " +
                            "FROM    dbo.t_KDT_SXXK_DinhMuc DM " +
                            "INNER JOIN dbo.t_KDT_SXXK_DinhMucDangKy DMDK ON DM.Master_ID = DMDK.ID " +
                            "WHERE   DMDK.MaHaiQuan = '" + MaHaiQuan + "' " +
                            "AND DMDK.MaDoanhNghiep = '" + MaDoanhNghiep + "' ";
            if (trangThaiDaDuyet)
                query += " AND DMDK.TrangThaiXuLy = 1";
            if (tuNgay > DateTime.MinValue)
                query += " and  DMDK.NgayTiepNhan >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND  DMDK.NgayTiepNhan <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.MaSanPham, temp.MaNguyenPhuLieu FROM [" + databaseNameTarget + "].dbo.t_KDT_SXXK_DinhMuc temp WHERE temp.MaSanPham = dm.MaSanPham AND temp.MaNguyenPhuLieu = dm.MaNguyenPhuLieu )";
            query += " ORDER BY  DMDK.NgayTiepNhan";

            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);
            return ds;
        }

        public static DataSet GetDinhMucCuaSanPhams(string MaHaiQuan, string MaDoanhNghiep, string MatKhau, params string[] MaSP)
        {
            string query = "SELECT Ma_HQ, Ma_DV, Ma_SP, Ma_NPL, DM_SD, TL_HH, DM_CHUNG, GHI_CHU  FROM DDINHMUC WHERE MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDoanhNghiep + "'";
            int i = 0;
            query += " and Ma_SP in (";
            foreach (string ma in MaSP)
            {
                query += "'" + ma + "',";
            }
            query += "'')";
            DataSet ds = WebServiceDB.getDataBySQL(query, MaHaiQuan, "SXXK", MaDoanhNghiep, MatKhau);

            return ds;
        }
        /// <summary>
        /// nhung du lieu da co thi bo qua cua doanh nghiep
        /// </summary>
        /// <param name="MaHaiQuan"></param>
        /// <param name="MaDoanhNghiep"></param>
        /// <param name="ds"></param>
        public static void InsertDuLieuMoi(string MaHaiQuan, string MaDoanhNghiep, DataSet ds)
        {
            DinhMucCollection collection = new DinhMucCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string masp = "";
                    bool ok = false;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DinhMuc dm = new DinhMuc();
                        dm.MaHaiQuan = MaHaiQuan;
                        dm.MaDoanhNghiep = MaDoanhNghiep;
                        dm.MaSanPHam = row["Ma_SP"].ToString().Trim();
                        if (masp != dm.MaSanPHam)
                        {
                            masp = dm.MaSanPHam;
                            ok = false;
                            ThongTinDinhMuc ttdm = new ThongTinDinhMuc();
                            ttdm.MaHaiQuan = MaHaiQuan;
                            ttdm.MaDoanhNghiep = MaDoanhNghiep;
                            ttdm.MaSanPham = masp;
                            if (!ttdm.Load(transaction))
                            {
                                ttdm.InsertTransaction(transaction);
                                ok = true;
                            }
                            else
                                ok = false;
                        }
                        if (ok)
                        {
                            dm.MaNguyenPhuLieu = row["Ma_NPL"].ToString().Trim();
                            dm.DinhMucSuDung = Convert.ToDecimal(row["DM_SD"].ToString());
                            dm.DinhMucChung = Convert.ToDecimal(row["DM_Chung"].ToString());
                            dm.TyLeHaoHut = Convert.ToDecimal(row["TL_HH"].ToString());
                            dm.GhiChu = row["GHI_CHU"].ToString();
                            dm.InsertTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public static void InsertDuLieuMoi(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, string connectionString)
        {
            DinhMucCollection collection = new DinhMucCollection();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    int i = 0;
                    string masp = "";
                    bool ok = false;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DinhMuc dm = new DinhMuc();
                        dm.MaHaiQuan = MaHaiQuan;
                        dm.MaDoanhNghiep = MaDoanhNghiep;
                        dm.MaSanPHam = row["Ma_SP"].ToString().Trim();
                        if (masp != dm.MaSanPHam)
                        {
                            masp = dm.MaSanPHam;
                            ThongTinDinhMuc ttdm = new ThongTinDinhMuc();
                            ttdm.MaHaiQuan = MaHaiQuan;
                            ttdm.MaDoanhNghiep = MaDoanhNghiep;
                            ttdm.MaSanPham = masp;
                            ttdm.Load(transaction, db);
                            ttdm.InsertUpdateTransaction(transaction, db);
                            ok = true;
                            /*if (!ttdm.Load(transaction, db))
                            {
                                ttdm.InsertUpdateTransaction(transaction, db);
                                ok = true;
                            }
                            else
                                ok = false;*/
                        }
                        if (ok)
                        {
                            dm.MaNguyenPhuLieu = row["Ma_NPL"].ToString().Trim();
                            dm.DinhMucSuDung = Convert.ToDecimal(row["DM_SD"].ToString());
                            dm.DinhMucChung = Convert.ToDecimal(row["DM_Chung"].ToString());
                            dm.TyLeHaoHut = Convert.ToDecimal(row["TL_HH"].ToString());
                            dm.GhiChu = row["GHI_CHU"].ToString();
                            dm.InsertUpdateTransactionKTX(transaction, db);

                            i += 1;
                        }

                        //TODO: HungTQ, Update 27/04/2010.
                        Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("{0}.SXXK Dinh muc: MaHaiQuan={1}, MaDoanhNghiep={2}, MaSanPHam={3}, MaNguyenPhuLieu={4}, TenNPL={5}, DinhMucSuDung={6}, DinhMucChung={7}, TyLeHaoHut={8} ", i, dm.MaHaiQuan, dm.MaDoanhNghiep, dm.MaSanPHam, dm.MaNguyenPhuLieu, dm.TenNPL, dm.DinhMucSuDung, dm.DinhMucChung, dm.TyLeHaoHut)));

                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public static void InsertDuLieuMoiTQDT(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, string connectionString)
        {
            DinhMucCollection collection = new DinhMucCollection();
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string masp = "";
                    bool ok = false;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DinhMuc dm = new DinhMuc();
                        dm.MaHaiQuan = MaHaiQuan;
                        dm.MaDoanhNghiep = MaDoanhNghiep;
                        dm.MaSanPHam = row["Ma_SP"].ToString().Trim();
                        if (masp != dm.MaSanPHam)
                        {
                            masp = dm.MaSanPHam;
                            ok = false;
                            ThongTinDinhMuc ttdm = new ThongTinDinhMuc();
                            ttdm.MaHaiQuan = MaHaiQuan;
                            ttdm.MaDoanhNghiep = MaDoanhNghiep;
                            ttdm.MaSanPham = masp;
                            if (!ttdm.Load(transaction))
                            {
                                ttdm.InsertTransaction(transaction);
                                ok = true;
                            }
                            else
                                ok = false;
                        }
                        if (ok)
                        {
                            dm.MaNguyenPhuLieu = row["Ma_NPL"].ToString().Trim();
                            dm.DinhMucSuDung = Convert.ToDecimal(row["DM_SD"].ToString());
                            dm.DinhMucChung = Convert.ToDecimal(row["DM_Chung"].ToString());
                            dm.TyLeHaoHut = Convert.ToDecimal(row["TL_HH"].ToString());
                            dm.GhiChu = row["GHI_CHU"].ToString();
                            dm.InsertTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public static void InsertGhiDeTatCaDuLieu(string MaHaiQuan, string MaDoanhNghiep, DataSet ds)
        {
            DinhMucCollection collection = new DinhMucCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DeleteAll(MaHaiQuan, MaDoanhNghiep, transaction);
                    string masp = "";
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DinhMuc dm = new DinhMuc();
                        dm.MaHaiQuan = MaHaiQuan;
                        dm.MaDoanhNghiep = MaDoanhNghiep;
                        dm.MaSanPHam = row["Ma_SP"].ToString().Trim();
                        if (masp != dm.MaSanPHam)
                        {
                            masp = dm.MaSanPHam;
                            ThongTinDinhMuc ttdm = new ThongTinDinhMuc();
                            ttdm.MaHaiQuan = MaHaiQuan;
                            ttdm.MaDoanhNghiep = MaDoanhNghiep;
                            ttdm.MaSanPham = masp;
                            if (!ttdm.Load(transaction))
                            {
                                ttdm.InsertTransaction(transaction);
                            }

                        }
                        dm.MaNguyenPhuLieu = row["Ma_NPL"].ToString().Trim();
                        dm.DinhMucSuDung = Convert.ToDecimal(row["DM_SD"].ToString());
                        dm.DinhMucChung = Convert.ToDecimal(row["DM_Chung"].ToString());
                        dm.TyLeHaoHut = Convert.ToDecimal(row["TL_HH"].ToString());
                        dm.GhiChu = row["GHI_CHU"].ToString();
                        dm.InsertTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public static void InsertGhiDeTatCaDuLieu(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, string connectionString)
        {
            DinhMucCollection collection = new DinhMucCollection();
            SqlDatabase db = new SqlDatabase(connectionString);

            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DeleteAll(MaHaiQuan, MaDoanhNghiep, transaction, connectionString);
                    string masp = "";
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DinhMuc dm = new DinhMuc();
                        dm.MaHaiQuan = MaHaiQuan;
                        dm.MaDoanhNghiep = MaDoanhNghiep;
                        dm.MaSanPHam = row["Ma_SP"].ToString().Trim();
                        if (masp != dm.MaSanPHam)
                        {
                            masp = dm.MaSanPHam;
                            ThongTinDinhMuc ttdm = new ThongTinDinhMuc();
                            ttdm.MaHaiQuan = MaHaiQuan;
                            ttdm.MaDoanhNghiep = MaDoanhNghiep;
                            ttdm.MaSanPham = masp;
                            if (!ttdm.Load(transaction, db))
                            {
                                ttdm.InsertUpdateTransaction(transaction, db);
                            }

                        }
                        dm.MaNguyenPhuLieu = row["Ma_NPL"].ToString().Trim();
                        dm.DinhMucSuDung = Convert.ToDecimal(row["DM_SD"].ToString());
                        dm.DinhMucChung = Convert.ToDecimal(row["DM_Chung"].ToString());
                        dm.TyLeHaoHut = Convert.ToDecimal(row["TL_HH"].ToString());
                        dm.GhiChu = row["GHI_CHU"].ToString();
                        dm.InsertUpdateTransaction(transaction, db);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public static void InsertGhiDeTatCaDuLieuTQDT(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, string connectionString)
        {
            DinhMucCollection collection = new DinhMucCollection();
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DeleteAll(MaHaiQuan, MaDoanhNghiep, transaction);
                    string masp = "";
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DinhMuc dm = new DinhMuc();
                        dm.MaHaiQuan = MaHaiQuan;
                        dm.MaDoanhNghiep = MaDoanhNghiep;
                        dm.MaSanPHam = row["Ma_SP"].ToString().Trim();
                        if (masp != dm.MaSanPHam)
                        {
                            masp = dm.MaSanPHam;
                            ThongTinDinhMuc ttdm = new ThongTinDinhMuc();
                            ttdm.MaHaiQuan = MaHaiQuan;
                            ttdm.MaDoanhNghiep = MaDoanhNghiep;
                            ttdm.MaSanPham = masp;
                            if (!ttdm.Load(transaction))
                            {
                                ttdm.InsertTransaction(transaction);
                            }

                        }
                        dm.MaNguyenPhuLieu = row["Ma_NPL"].ToString().Trim();
                        dm.DinhMucSuDung = Convert.ToDecimal(row["DM_SD"].ToString());
                        dm.DinhMucChung = Convert.ToDecimal(row["DM_Chung"].ToString());
                        dm.TyLeHaoHut = Convert.ToDecimal(row["TL_HH"].ToString());
                        dm.GhiChu = row["GHI_CHU"].ToString();
                        dm.InsertTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public static void InsertDuLieuTQDT(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, string connectionString)
        {
            try
            {
                Company.BLL.KDT.SXXK.DinhMucDangKyCollection kdtDMDKCollection = new Company.BLL.KDT.SXXK.DinhMucDangKyCollection();

                //Create collection DinhMucDangKy
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Company.BLL.KDT.SXXK.DinhMucDangKy dmDK = Company.BLL.KDT.SXXK.DinhMucDangKy.Load(long.Parse(row["Master_ID"].ToString()));

                    if (dmDK != null)
                    {
                        //Load Dinh muc cua DM DK.
                        dmDK.LoadDMCollection();

                        kdtDMDKCollection.Add(dmDK);
                    }
                }

                //Insert data DinhMucDangKy
                foreach (Company.BLL.KDT.SXXK.DinhMucDangKy item in kdtDMDKCollection)
                {
                    item.InsertUpdateFull(connectionString);
                }

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// Xóa hết dữ liệu định mức trừ những sản phẩm đã tham gia thanh khoản.
        /// </summary>
        /// <param name="MaHaiQuan"></param>
        /// <param name="MaDoanhNghiep"></param>
        /// <param name="transaction"></param>
        private static void DeleteAll(string MaHaiQuan, string MaDoanhNghiep, SqlTransaction transaction)
        {
            string sql = "delete from t_SXXK_ThongTinDinhMuc where MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan " +
                "and MaSanPham not in ( SELECT distinct tksnt.MaSP " +
                "FROM v_KDT_SXXK_NPLXuatTon tksnt " +
                "INNER JOIN t_SXXK_ToKhaiMauDich tstkmd ON tstkmd.SoToKhai = tksnt.SoToKhai " +
                "AND tstkmd.MaLoaiHinh = tksnt.MaLoaiHinh AND tstkmd.NamDangKy = tksnt.NamDangKy AND tstkmd.MaHaiQuan = tksnt.MaHaiQuan " +
                "WHERE tstkmd.MaDoanhNghiep=@MaDoanhNghiep)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.ExecuteNonQuery(dbCommand, transaction);
        }


        private static void DeleteAll(string MaHaiQuan, string MaDoanhNghiep, SqlTransaction transaction, string connectionString)
        {
            string sql = "delete from t_SXXK_ThongTinDinhMuc where MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan " +
                "and MaSanPham not in ( SELECT distinct tksnt.MaSP " +
                "FROM v_KDT_SXXK_NPLXuatTon tksnt " +
                "INNER JOIN t_SXXK_ToKhaiMauDich tstkmd ON tstkmd.SoToKhai = tksnt.SoToKhai " +
                "AND tstkmd.MaLoaiHinh = tksnt.MaLoaiHinh AND tstkmd.NamDangKy = tksnt.NamDangKy AND tstkmd.MaHaiQuan = tksnt.MaHaiQuan " +
                "WHERE tstkmd.MaDoanhNghiep=@MaDoanhNghiep)";
            SqlDatabase db = new SqlDatabase(connectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.ExecuteNonQuery(dbCommand, transaction);
        }
    }
}
