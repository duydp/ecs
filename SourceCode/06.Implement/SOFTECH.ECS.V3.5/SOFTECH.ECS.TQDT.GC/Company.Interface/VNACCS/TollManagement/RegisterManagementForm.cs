﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.TollManagement
{
    public partial class RegisterManagementForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public RegisterManagementForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private string GetSearchWhere()
        {
            try
            {
                string where = " 1= 1 ";
                if (!String.IsNullOrEmpty(txtMa.Text.ToString()))
                    where += " AND MaDoanhNghiep ='" + txtMa.Text.ToString() + "'";
                if (!String.IsNullOrEmpty(txtTen.Text.ToString()))
                    where += " AND TenDoanhNghiep LIKE '%" + txtTen.Text.ToString() + "%'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1";
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgListVDN.Refetch();
                dgListVDN.DataSource = T_KDT_THUPHI_DOANHNGHIEP.SelectCollectionDynamic(GetSearchWhere(), "");
                dgListVDN.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListVDN_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    T_KDT_THUPHI_DOANHNGHIEP customer = new T_KDT_THUPHI_DOANHNGHIEP();
                    customer = (T_KDT_THUPHI_DOANHNGHIEP)e.Row.DataRow;
                    HP_Register_Information f = new HP_Register_Information();
                    f.customer = customer;
                    f.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void RegisterManagementForm_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }
    }
}
