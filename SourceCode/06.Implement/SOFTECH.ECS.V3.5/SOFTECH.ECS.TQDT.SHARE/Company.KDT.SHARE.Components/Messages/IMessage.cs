﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
namespace Company.KDT.SHARE.Components
{
    public class MessageEventArgs : EventArgs
    {
        internal MessageInfo MessageInfo { get; set; }
    }
    public interface IMessage
    {
        MessageInfo Send(string pass);
        MessageInfo Request(string pass);
    }
    public class MesssageProcess
    {
        /// <summary>
        ///  
        /// </summary>
        /// <param name="sfmtMessage"></param>
        /// <returns></returns>
        private bool ProcessMessage(string sfmtMessage)
        {
            //pattern format header is ERROR:Content=
            string pattern = @"(?<Information>\w+):Content=(?<content>(\w|\.)+)";
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(pattern);
            string msgContent = string.Empty;
            string info = string.Empty;
            foreach (Match item in regex.Matches(sfmtMessage))
            {
                msgContent += item.Result("${content}");
                info = item.Result("${Information}");
            }

            //// Thực hiện kiểm tra. 
            //if (info.Equals("NoInfo"))
            //{
            //    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
            //    if (kq == "Yes")
            //    {
            //        return true;
            //    }

            //}
            //else if (info.Equals("TuChoi"))
            //{
            //    msgContent = "Hải quan từ chối với thông báo:\r\n " + msgContent;
            //   // ShowMessage(msg, false);
            //}
            //else if (info.Equals("ERROR"))
            //{
            //    msgContent = "Lỗi từ hệ thống hải quan trả về :\r\n" + msgContent;
            //  //  ShowMessage(msg, false);
            //}
            //else
            //{
            //    string kq = MLMessages(msgContent + "\r\nnBạn có muốn tiếp tục lấy phân luồng thông tin không?", "MSG_STN02", "", true);
            //    if (kq == "Yes")
            //    {
            //        return true;
            //    }
            //}
            return false;
        }
    }
    public enum StandardType
    {
        VietNamese=0,
        English,
    }
}
