﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.BLL.KDT;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamManageForm : BaseForm
    {
        /// <summary>
        /// Dùng cho kết quả tìm kiếm.
        /// </summary>
        private SanPhamDangKyCollection spDangKyCollection = new SanPhamDangKyCollection();
        private SanPhamDangKyCollection tmpCollection = new SanPhamDangKyCollection();
        /// <summary>
        /// Thông tin sản phẩm đang dược chọn.
        /// </summary>
        private readonly SanPhamDangKy currentSPDangKy = new SanPhamDangKy();
        private SanPhamDangKy spdk = new SanPhamDangKy();
        private string xmlCurrent = "";

        public SanPhamManageForm()
        {
            InitializeComponent();

        }

        //-----------------------------------------------------------------------------------------
        private void SanPhamManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                cbStatus.SelectedIndex = 0;

                //An nut Xac nhan
                XacNhan.Visible = InheritableBoolean.False;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            setCommandStatus();
            saveFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        //-----------------------------------------------------------------------------------------

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
            where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
            }

            if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                }
            }

            where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

            // Thực hiện tìm kiếm.            
            this.spDangKyCollection = SanPhamDangKy.SelectCollectionDynamic(where, "");
            dgList.DataSource = this.spDangKyCollection;

            this.setCommandStatus();

            this.currentSPDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }

        //-----------------------------------------------------------------------------------------

        private void setCommandStatus()
        {
            DongBoDuLieu.Enabled = InheritableBoolean.True;
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = false;
                cmdSuaSP.Enabled = InheritableBoolean.False;//Sua sp
                btnSuaSP.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = InheritableBoolean.False;
                cmdXoa.Enabled = InheritableBoolean.False;
                XacNhan.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                btnDelete.Enabled = false;
                cmdSuaSP.Enabled = InheritableBoolean.True;//Sua sp
                btnSuaSP.Enabled = true;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = false;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = false;
                cmdSend.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = InheritableBoolean.True;
                cmdSingleDownload.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = true;
                cmdSuaSP.Enabled = InheritableBoolean.False;//Sua sp
                btnSuaSP.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.False;
                cmdXoa.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = InheritableBoolean.True;
                DongBoDuLieu.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                btnDelete.Enabled = false;
                cmdSuaSP.Enabled = InheritableBoolean.True;//Sua sp
                btnSuaSP.Enabled = true;
            }
        }


        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case -1:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not declared yet";

                        }
                        break;
                    case 0:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for approval";

                        }
                        break;
                    case 1:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Approved";

                        }
                        break;
                    case 2:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not Approved";

                        }
                        break;
                }


                //TODO: Cao Hữu Tú updated:15-09-2011
                //Contents: bổ sung cột mã sản phẩm vào Grid Danh sách sản phẩm


                Company.BLL.KDT.SXXK.SanPhamCollection SanPhamCollection = new SanPhamCollection();
                int ValueIdCell = Int32.Parse(e.Row.Cells["ID"].Value.ToString());
                string TatCaMaNPL = "";

                SanPhamCollection = Company.BLL.KDT.SXXK.SanPham.SelectCollectionBy_Master_ID(ValueIdCell);
                Company.BLL.KDT.SXXK.SanPham entitySanPham = new SanPham();

                if (SanPhamCollection.Count > 0)
                {
                    //lấy mã đầu tiên trong NguyenPhuLieuCollection đưa vào Cột mã nguyên phụ liệu
                    entitySanPham = SanPhamCollection[0];
                    e.Row.Cells["MaSanPham"].Text = entitySanPham.Ma;

                    //lấy mã sản phẩm bắt đầu từ phần tử thứ 2 đưa vào 
                    foreach (Company.BLL.KDT.SXXK.SanPham EntitySP in SanPhamCollection)
                    {

                        TatCaMaNPL = TatCaMaNPL + EntitySP.Ma + "\n";

                    }
                    e.Row.Cells["MaSanPham"].ToolTipText = TatCaMaNPL;
                }

            }
        }

        //-----------------------------------------------------------------------------------------

        private void download()
        {
            SanPhamDangKy spDK = new SanPhamDangKy();
            MsgSend sendXML = new MsgSend();
            WSForm wsForm = new WSForm();
            string password = "";
            if (dgList.GetRow() != null)
            {
                spDK = (SanPhamDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "SP";
                sendXML.master_id = spDK.ID;
                if (!sendXML.Load())
                {
                    // ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    //MLMessages("Sản phẩm chưa được gửi thông tin tới hải quan.\nHãy chọn chức năng 'Khai báo' cho sản phẩm này.", "MSG_SEN03", "", false);
                    //return;
                }
            }
            else
            {
                //  ShowMessage("Chưa chọn thông tin nhận trạng thái.", false);
                MLMessages("Chưa chọn thông tin nhận trạng thái.", "MSG_REC01", "", false);
                return;
            }

            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                //DATLMQ comment theo chuan moi cua TNTT 25/11/2010
                //xmlCurrent = spDK.WSRequestXML(password);

                sendXML = new MsgSend();
                sendXML.LoaiHS = "SP";
                sendXML.master_id = spDK.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không kết nối được tới  hệ thống phía hải quan.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = spDK.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                                hd.TrangThai = spDK.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong hủy thông tin : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo danh sách SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSingleDownload":
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    {
                        LayPhanHoiToKhaiV3();
                    }
                    else
                    {
                        this.download();
                    }
                    break;
                case "cmdCancel":
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        cancelV3();
                    else
                    this.cancel();
                    break;
                case "cmdSend":
                    if (this.ShowMessage("Bạn có muốn khai báo thông tin tờ khai này không?", true) == "Yes")
                    {
                        if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        {
                            SendV3();
                        }
                        else
                        {
                            this.send();
                        }
                    }
                    break;
                case "XacNhan":
                    this.LaySoTiepNhanDT();
                    break;
                case "Export":
                    this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "cmdXoa":
                    this.Delete();
                    break;
                case "cmdCSDaDuyet":
                    this.ChuyenTrangThai();
                    break;
                case "cmdXuatSanPhamChoPhongKhai":
                    XuatSanPhamChoPhongKhai();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdSuaSP":
                    this.SuaSPDaDuyet();
                    break;
            }
        }
        private void SuaSPDaDuyet()
        {
            SanPhamDangKy spdk = new SanPhamDangKy();
            spdk = (SanPhamDangKy)dgList.GetRow().DataRow;
            spdk.LoadSPCollection();
            if (SanPhamDangKySUA.SelectCollectionBy_IDSPDK(spdk.ID).Count == 0)
            {
                string msg = "Bạn có muốn chuyển sang trạng thái Sửa sản phẩm không?";
                msg += "\n\nSố tiếp nhận: " + spdk.SoTiepNhan.ToString();
                msg += "\n----------------------";
                msg += "\nCó " + spdk.SPCollection.Count.ToString() + " sản phẩm đăng ký đã được duyệt";
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    SanPhamSuaForm f = new SanPhamSuaForm();
                    f.SPDangKy = spdk;
                    f.masterId = spdk.ID;
                    f.spdkSUA = new SanPhamDangKySUA();
                    f.ShowDialog();
                }
            }
            else
            {
                SanPhamSuaListForm listForm = new SanPhamSuaListForm();
                listForm.spdk = spdk;
                listForm.ShowDialog();
            }
        }
        private void inPhieuTN()
        {
            if (dgList.GetRows().Length < 1) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "SẢN PHẨM";
            Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    SanPhamDangKy spDangKySelected = (SanPhamDangKy)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = spDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = spDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();

        }
        private void XuatSanPhamChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                // ShowMessage("Chưa chọn danh sách sản phẩm cần xuất.", false);
                MLMessages("Chưa chọn danh sách sản phẩm cần xuất.", "MSG_PUB45", "", false);
                return;
            }
            try
            {
                SanPhamDangKyCollection col = new SanPhamDangKyCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SanPhamDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sosp = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            SanPhamDangKy spSelected = (SanPhamDangKy)i.GetRow().DataRow;
                            spSelected.LoadSPCollection();
                            col.Add(spSelected);
                            sosp++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    //ShowMessage("Xuất thành công " + sosp + " sản phẩm.", false);
                    MLMessages("Xuất thành công " + sosp + " sản phẩm.", "MSG_NPL04", "" + sosp, false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }

        private void ChuyenTrangThai()
        {
            //ShowMessage("aa", false);
            if (dgList.SelectedItems.Count > 0)
            {
                SanPhamDangKyCollection spDKColl = new SanPhamDangKyCollection();

                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    spDKColl.Add((SanPhamDangKy)grItem.GetRow().DataRow);
                }

                for (int i = 0; i < spDKColl.Count; i++)
                {
                    string msg = "";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        msg = "Bạn có muốn chuyển trạng thái của danh sách được chọn sang đã duyệt không?";
                        msg += "\n\nSố thứ tự danh sách: " + spDKColl[i].ID.ToString();
                        //msg += "\n----------------------";
                        spDKColl[i].LoadSPCollection();
                        msg += "\nCó " + spDKColl[i].SPCollection.Count.ToString() + " sản phẩm đăng ký";
                        msg += "\nCác mã sản phẩm: ";
                    }
                    else
                    {
                        msg = "Do you want to change status of selection list into Approved status?";
                        msg += "\n\n Order number of list: " + spDKColl[i].ID.ToString();
                        //msg += "\n----------------------";
                        spDKColl[i].LoadSPCollection();
                        msg += "\nThere are " + spDKColl[i].SPCollection.Count.ToString() + "registered product";
                        msg += "\nCode of product: ";

                    }
                    for (int j = 0; j < spDKColl[i].SPCollection.Count; j++)
                    {
                        msg += spDKColl[i].SPCollection[j].Ma.ToString();
                        if (j == spDKColl[i].SPCollection.Count - 1)
                        {
                            msg += ".";
                        }
                        else
                        {
                            msg += ", ";
                        }
                    }
                    if (ShowMessage(msg, true) == "Yes")
                    {
                        if (spDKColl[i].TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            spDKColl[i].NgayTiepNhan = DateTime.Now;
                        }

                        string fmtMsg = string.Format("Thông tin trước khi chuyển-->GUIDSTR:{0},Mã hải quan: {1},Mã doanh nghiệp: {2}," +
                                                      "ID: {3},Năm đăng ký:{4},Ngày tiếp nhận:{5},Số tiếp nhận:{6}",
                                                   new object[]{
                                                       spDKColl[i].GUIDSTR,
                                                       spDKColl[i].MaHaiQuan,                                                       
                                                       spDKColl[i].MaDoanhNghiep,
                                                       spDKColl[i].ID,
                                                       spDKColl[i].NamDK,
                                                       spDKColl[i].NgayTiepNhan,                                                       
                                                       spDKColl[i].SoTiepNhan});
                        Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty, spDKColl[i].ID, spDKColl[i].GUIDSTR,
                        Company.KDT.SHARE.Components.MessageTypes.DanhMucSanPham,
                        Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                        Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay, fmtMsg);

                        spDKColl[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;

                        spDKColl[i].TransgferDataToSXXK();
                    }
                }
                this.search();
            }
            else
            {
                //  ShowMessage("Chưa có dữ liệu được chọn", false);
                MLMessages("Chưa có dữ liệu được chọn", "MSG_PUB45", "", false);
            }
        }

        public void XoaSanPhamDangKy(GridEXSelectedItemCollection items)
        {

            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            MsgSend sendXML = new MsgSend();

            List<SanPhamDangKy> itemsDelete = new List<SanPhamDangKy>();

            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    SanPhamDangKy npldk = (SanPhamDangKy)i.GetRow().DataRow;
                    sendXML.LoaiHS = "SP";
                    sendXML.master_id = npldk.ID;
                    if (!sendXML.Load())
                    {
                        msgWarning += string.Format(" ID ={0} [Chấp nhận xóa]\r\n", npldk.ID);
                        itemsDelete.Add(npldk);
                    }
                    else
                    {
                        msgWarning += string.Format(" ID ={0} [Không thể xóa]\r\n", npldk.ID);
                    }
                }
            }
            msgWarning += "* Ghi chú: Những sản phẩm [Không thể xóa] vì đã gửi thông tin đến hải quan\r\n";
            if (itemsDelete.Count == 0)
            {
                Globals.ShowMessage(msgWarning, false);
                return;
            }
            else
            {
                msgWarning += "Bạn đồng ý thực hiện không?";
                if (Globals.ShowMessage(msgWarning, true) != "Yes") return;
            }
            try
            {
                msgWarning = string.Empty;
                foreach (SanPhamDangKy item in itemsDelete)
                {
                    if (item.CloneToDB(null))
                        item.Delete();
                    else msgWarning += string.Format("ID={0} [thực hiện không thành công]", item.ID);
                }
                if (msgWarning != string.Empty)

                    Globals.ShowMessage(msgWarning, false);
            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Delete()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaSanPhamDangKy(items);
            search();

            //LanNT
            #region Mã cũ
            //try
            //{
            //    if (items.Count > 0)
            //        // if (ShowMessage("Bạn có muốn xóa danh sách sản phẩm này không?", true) == "Yes")
            //        if (MLMessages("Bạn có muốn xóa danh sách sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
            //        {
            //            this.Cursor = Cursors.WaitCursor;

            //            foreach (GridEXSelectedItem i in items)
            //            {
            //                if (i.RowType == RowType.Record)
            //                {
            //                    SanPhamDangKy spDangKySelected = (SanPhamDangKy)i.GetRow().DataRow;                               
            //                    sendXML.LoaiHS = "SP";
            //                    sendXML.master_id = spDangKySelected.ID;

            //                    if (sendXML.Load())
            //                    {
            //                        if (spDangKySelected.SoTiepNhan != 0)
            //                            // ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
            //                            MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_NPL02", "" + i.Position + "", false);
            //                    }
            //                    else
            //                    {
            //                        if (spDangKySelected.ID > 0)
            //                        {
            //                            spDangKySelected.Delete();
            //                        }
            //                    }

            //                    if (spDangKySelected.SoTiepNhan == 0 && spDangKySelected.ID > 0)
            //                    {
            //                        spDangKySelected.CloneToDB(null);
            //                        spDangKySelected.Delete();
            //                    }
            //                    #region Log
            //                    //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
            //                    try
            //                    {
            //                        string where = "1 = 1";
            //                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", spDangKySelected.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.SanPham);
            //                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
            //                        if (listLog.Count > 0)
            //                        {
            //                            long idLog = listLog[0].IDLog;
            //                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
            //                            long idDK = listLog[0].ID_DK;
            //                            string guidstr = listLog[0].GUIDSTR_DK;
            //                            string userKhaiBao = listLog[0].UserNameKhaiBao;
            //                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
            //                            string userSuaDoi = GlobalSettings.UserLog;
            //                            DateTime ngaySuaDoi = DateTime.Now;
            //                            string ghiChu = listLog[0].GhiChu;
            //                            bool isDelete = true;
            //                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
            //                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
            //                        }
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
            //                        return;
            //                    }
            //                    #endregion
            //                }
            //            }
            //            search();
            //        }

            //}
            //catch (Exception ex)
            //{
            //    ShowMessage(ex.Message, false);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            #endregion Mã cũ

        }
        private string checkDataHangImport(SanPhamDangKy spDangky)
        {
            string st = "";
            foreach (SanPham sp in spDangky.SPCollection)
            {
                BLL.SXXK.SanPham spDaDuyet = new Company.BLL.SXXK.SanPham();
                spDaDuyet.Ma = sp.Ma;
                spDaDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                spDaDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (spDaDuyet.Load())
                {
                    //if (sp.STTHang == 1)
                    //    st = "Danh sách có ID='" + spDangky.ID + "'\n";
                    //st += "Sản phẩm có mã '" + sp.Ma + "' đã có trong hệ thống.\n";
                    if (sp.STTHang == 1)
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st = "Danh sách có ID='" + spDangky.ID + "'\n";

                        }
                        else
                        {
                            st = "List has ID='" + spDangky.ID + "'\n";

                        }
                    }
                    if (GlobalSettings.NGON_NGU == "0")
                    {

                        st += "Sản phẩm có mã '" + sp.Ma + "' đã có trong hệ thống.\n";
                    }
                    else
                    {

                        st += "Product has code '" + sp.Ma + "' already exist in system.\n";
                    }
                }
            }
            return st;
        }
        private string checkDataImport(SanPhamDangKyCollection collection)
        {
            string st = "";
            foreach (SanPhamDangKy spDangky in collection)
            {
                SanPhamDangKy spInDatabase = SanPhamDangKy.Load(spDangky.ID);
                if (spInDatabase != null)
                {
                    if (spInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        // st += "Danh sách có ID=" + spDangky.ID + " đã được duyệt.\n";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st += "Danh sách có ID=" + spDangky.ID + " đã được duyệt.\n";
                        }
                        else
                        {
                            st += "List has ID=" + spDangky.ID + " already approved.\n";
                        }
                    }
                    else
                    {
                        string tmp = checkDataHangImport(spDangky);
                        st += tmp;
                        if (tmp == "")
                            tmpCollection.Add(spDangky);
                    }
                }
                else
                {
                    if (spInDatabase.ID > 0)
                        spInDatabase.ID = 0;
                    tmpCollection.Add(spDangky);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    tmpCollection.Clear();
                    XmlSerializer serializer = new XmlSerializer(typeof(SanPhamDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    SanPhamDangKyCollection spDKCollection = (SanPhamDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(spDKCollection);
                    if (st != "")
                    {
                        // if (ShowMessage("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", true) == "Yes")
                        if (MLMessages("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "MSG_PUB04", "", true) == "Yes")
                        {
                            SanPhamDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                            //ShowMessage("Import thành công", false);
                            MLMessages("Import thành công", "MSG_PUB02", "", false);
                        }
                    }
                    else
                    {
                        SanPhamDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                        //ShowMessage("Import thành công", false);
                        MLMessages("Import thành công", "MSG_PUB02", "", false);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(" " + ex.Message, false);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                ShowMessage("Chưa chọn danh sách sản phẩm", false);
                return;
            }
            try
            {
                SanPhamDangKyCollection col = new SanPhamDangKyCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SanPhamDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            SanPhamDangKy spDangKySelected = (SanPhamDangKy)i.GetRow().DataRow;
                            spDangKySelected.LoadSPCollection();
                            col.Add(spDangKySelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }

        }
        private void LaySoTiepNhanDT()
        {
            SanPhamDangKy spDangKy = new SanPhamDangKy();
            MsgSend sendXML = new MsgSend();
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {

                if (dgList.GetRow() != null)
                {
                    spDangKy = (SanPhamDangKy)dgList.GetRow().DataRow;
                    sendXML.LoaiHS = "SP";
                    sendXML.master_id = spDangKy.ID;
                    if (!sendXML.Load())
                    {
                        // ShowMessage("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                        //MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_SEN03", "", false);
                        //return;
                    }
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    this.Cursor = Cursors.WaitCursor;
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    {
                        xmlCurrent = spDangKy.LayPhanHoi(password, sendXML.msg);
                    }
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        // string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                        string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                        if (kq == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        return;
                    }

                    if (sendXML.func == 1)
                    {
                        //  ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + spDangKy.SoTiepNhan, false);
                        MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + spDangKy.SoTiepNhan, "MSG_SEN05", "" + spDangKy.SoTiepNhan, false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        // ShowMessage("Đã hủy danh sách sản phẩm này", false);
                        MLMessages("Đã hủy danh sách nguyên phụ liệu này", "MSG_SEN06", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            // ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                            MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN07", "", false);
                            this.search();
                        }
                        else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            //ShowMessage("Hải quan chưa xử lý danh sách sản phẩm này!", false);
                            MLMessages("Hải quan chưa xử lý danh sách sản phẩm này!", "MSG_SEN08", "", false);
                        }
                        else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            // ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                            MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_STN09", "", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = spDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                                hd.TrangThai = spDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                                sendXML.Delete();
                        }
                    }
                    else
                    {
                        ShowMessage("Xảy ra lỗi không xác định.", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            //Wait.StartWait();
            SanPhamDangKy spDangKy = new SanPhamDangKy();
            MsgSend sendXML = new MsgSend();
            try
            {
                if (dgList.GetRow() != null)
                {
                    spDangKy = (SanPhamDangKy)dgList.GetRow().DataRow;
                    sendXML.LoaiHS = "SP";
                    sendXML.master_id = spDangKy.ID;
                    sendXML.Load();
                    this.Cursor = Cursors.WaitCursor;

                    //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 30/11/2010
                    //Tao XML Header
                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(Globals.ConfigPhongBiPhanHoi(MessgaseType.DanhMucSanPham, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, spDangKy.GUIDSTR));

                    //Tao Body XML
                    XmlDocument docNPL = new XmlDocument();
                    string path = EntityBase.GetPathProram();
                    docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                    XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                    //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
                    root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI.Trim();
                    root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI.Trim();

                    root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN.Trim();
                    root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));
                    root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = spDangKy.GUIDSTR.Trim();

                    XmlNode Content = xml.GetElementsByTagName("Content")[0];
                    Content.AppendChild(root);

                    xmlCurrent = spDangKy.LayPhanHoi(pass, xml.InnerXml);
                    this.Cursor = Cursors.Default;
                    //Wait.Close();
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        // string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                        string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                        if (kq == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        return;
                    }

                    if (sendXML.func == 1)
                    {
                        // ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + spDangKy.SoTiepNhan, false);
                        MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + spDangKy.SoTiepNhan, "MSG_SEN05", "" + spDangKy.SoTiepNhan, false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        //ShowMessage("Đã hủy danh sách sản phẩm này", false);
                        MLMessages("Đã hủy danh sách sản phẩm này", "MSG_SEN06", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                            MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN18", "", false);
                            this.search();
                        }
                        else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            // ShowMessage("Hải quan chưa xử lý danh sách sản phẩm này!", false);
                            MLMessages("Hải quan chưa xử lý!", "MSG_SEN08", "", false);
                        }
                        else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                            MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN09", "", false);
                            this.search();
                        }
                        //DATLMQ bổ sung thông báo SP bị Hải quan từ chối ngày 08/08/2011
                        else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO && !(spDangKy.HUONGDAN.Equals("")))
                        {
                            MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN09", "", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = spDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                                hd.TrangThai = spDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                SanPhamDangKyDetailForm f = new SanPhamDangKyDetailForm();
                f.SPDangKy = (SanPhamDangKy)e.Row.DataRow;
                int ttxl = f.SPDangKy.TrangThaiXuLy;
                if (f.SPDangKy.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                {
                    f.OpenType = OpenFormType.Edit;
                }
                else
                {
                    f.OpenType = OpenFormType.View;
                }
                if (f.SPDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    f.OpenType = OpenFormType.View;
                }
                f.ShowDialog(this);
                if (ttxl != f.SPDangKy.TrangThaiXuLy) this.search();
            }
        }

        /// <summary>
        /// Hủy thông tin đã đăng ký.
        /// </summary>
        private void cancel()
        {
            SanPhamDangKy spDangKy = new SanPhamDangKy();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null)
            {
                spDangKy = (SanPhamDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "SP";
                sendXML.master_id = spDangKy.ID;
                if (sendXML.Load())
                {
                    //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                    //return;
                }
            }
            else
            {
                //ShowMessage("Chưa chọn thông tin để hủy.", false);
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                xmlCurrent = spDangKy.WSCancelXML(password);
                sendXML = new MsgSend();
                sendXML.LoaiHS = "SP";
                sendXML.master_id = spDangKy.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = spDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                                hd.TrangThai = spDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.HUY_KHAI_BAO;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {

                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi Hủy danh sách SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
            SanPhamDangKy spDangKy = new SanPhamDangKy();
            MsgSend sendXML = new MsgSend();
            string password = "";
            if (dgList.GetRow() != null)
            {
                spDangKy = (SanPhamDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "SP";
                sendXML.master_id = spDangKy.ID;
                if (sendXML.Load())
                {
                    // ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    MLMessages("Sản phẩm đã gửi thông tin tới hải quan nhưng chưa có thông tin phản hồi.\nHãy chọn chức năng 'Nhận dữ liệu' cho sản phẩm này.", "MSG_SEN03", "", false);
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    return;
                }
            }
            else
            {
                //ShowMessage("Chưa chọn thông tin để gửi.", false);
                MLMessages("Chưa chọn thông tin để gửi.", "MSG_PUB45", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                spDangKy.LoadSPCollection();
                if(spDangKy.SPCollection.Count == 0)
                {
                    //ShowMessage("Danh sách sản phẩm rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    MLMessages("Danh sách sản phẩm rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }
                string[] danhsachDaDangKy = new string[0];

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                xmlCurrent = spDangKy.WSSendXML(password);
                this.Cursor = Cursors.Default;

                sendXML = new MsgSend();
                sendXML.LoaiHS = "SP";
                sendXML.master_id = spDangKy.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();

                LayPhanHoi(password); ;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = spDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                                hd.TrangThai = spDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.KHAI_BAO;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                txtNamTiepNhan.Text = string.Empty;
                txtNamTiepNhan.Value = 0;
                txtNamTiepNhan.Enabled = false;
                txtSoTiepNhan.Value = 0;
                txtSoTiepNhan.Text = string.Empty;
                txtSoTiepNhan.Enabled = false;
            }
            else
            {
                txtNamTiepNhan.Value = DateTime.Today.Year;
                txtNamTiepNhan.Enabled = true;
                txtSoTiepNhan.Enabled = true;
            }
            this.setCommandStatus();
            this.search();
        }

        //-----------------------------------------------------------------------------------------


        private void SanPhamManageForm_Shown(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Delete();
            #region Old
            //try
            //{
            //    //if (ShowMessage("Bạn có muốn xóa danh sách sản phẩm này không?", true) == "Yes")
            //    if (MLMessages("Bạn có muốn xóa danh sách sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        GridEXSelectedItemCollection items = dgList.SelectedItems;
            //        foreach (GridEXSelectedItem i in items)
            //        {
            //            if (i.RowType == RowType.Record)
            //            {
            //                SanPhamDangKy spDangKySelected = (SanPhamDangKy)i.GetRow().DataRow;
            //                MsgSend sendXML = new MsgSend();
            //                sendXML.LoaiHS = "SP";
            //                sendXML.master_id = spDangKySelected.ID;
            //                if (sendXML.Load())
            //                {
            //                    ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
            //                }
            //                else
            //                {
            //                    if (spDangKySelected.ID > 0)
            //                    {
            //                        spDangKySelected.Delete();
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage(ex.Message, false);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            #endregion Old
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Delete();
        }

        private void uiContextMenu1_CommandClick(object sender, CommandEventArgs e)
        {

        }

        private void btnSuaSP_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() != null)
            {
                this.SuaSPDaDuyet();
            }
            else
            {
                ShowMessage("Chưa chọn thông tin để sửa.", false);
                return;
            }
        }

        private void uiMessage_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;

            SanPhamDangKy nplSanPhamDangKy = (SanPhamDangKy)dgList.CurrentRow.DataRow;
            Globals.KetQuaXuLyChung(nplSanPhamDangKy.ID);
        }

        //-----------------------------------------------------------------------------------------
        #region Send V3 Create by LANNT
        private void SendV3()
        {

            if (dgList.GetRow() != null)
                spdk = (SanPhamDangKy)dgList.GetRow().DataRow;
            else
                return;
            try
            {
                if (spdk.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                spdk.LoadSPCollection();
                // Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu npl = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_NPL(nplDangKy);
                // string stringdemo = Company.KDT.SHARE.Components.Helpers.Serializer(npl);

                if (spdk.SPCollection.Count == 0)
                {
                    // ShowMessage("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }

                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = "SP";
                sendXML.master_id = spdk.ID;
                if (sendXML.Load())
                {
                    MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", "MSG_SEN03", "", false);
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    //NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }


                if (spdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                {
                    //Khai báo sửa tờ khai
                }
                //else if (spdk. != 0)
                //{
                //    string msg = "Tờ khai này đã có số tờ khai, không thể khai báo mới tờ khai.\r\nBạn hãy chuyển tờ khai sang trạng thái khác.";
                //    ShowMessage(msg, false);
                //    return;
                //}
                else
                {
                    //Khai báo mới tờ khai.

                    //Tờ khai nhập

                    spdk.GUIDSTR = Guid.NewGuid().ToString();
                    spdk.Update();

                    Company.KDT.SHARE.Components.SXXK_SanPham sanpham = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_SP(spdk,false);
                    string msgSend = Company.KDT.SHARE.Components.Helpers.BuildSend(
                                   new Company.KDT.SHARE.Components.NameBase()
                                   {
                                       Name = GlobalSettings.TEN_DON_VI,
                                       Identity = spdk.MaDoanhNghiep,
                                   }
                                     , new Company.KDT.SHARE.Components.NameBase()
                                     {
                                         Name = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(spdk.MaHaiQuan),
                                         Identity = spdk.MaHaiQuan,
                                     }
                                  ,
                                    new Company.KDT.SHARE.Components.SubjectBase()
                                    {
                                        Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_SP,
                                        Function = Company.KDT.SHARE.Components.DeclarationFunction.KHAI_BAO,
                                        Reference = spdk.GUIDSTR,
                                    }
                                    ,
                                    sanpham, null,true);
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    sendForm.DoSend(msgSend);
                    Company.KDT.SHARE.Components.Globals.XmlSaveMessage(msgSend, spdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoNguyenPhuLieu);
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = "SP";
                    sendXML.master_id = spdk.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    Company.KDT.SHARE.Components.FeedBackContent feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);

                    switch (feedbackContent.AdditionalInformation.Statement.Trim())
                    {
                        case Company.KDT.SHARE.Components.DeclarationFunction.KHONG_CHAP_NHAN:
                            this.ShowMessageTQDT(feedbackContent.AdditionalInformation.Content.Text, false);
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.CHUA_XU_LY:
                            this.ShowMessageTQDT(feedbackContent.AdditionalInformation.Content.Text, false);
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.CAP_SO_TIEP_NHAN:
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, spdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiSanPham, "Cap So Tiep Nhan");
                            string[] ketqua = feedbackContent.AdditionalInformation.Content.Text.Split('/');
                            spdk.SoTiepNhan = long.Parse(ketqua[0].Trim());
                            spdk.NamDK = short.Parse(ketqua[1].Trim());
                            spdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null); ;
                            spdk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            break;
                        //case Company.KDT.SHARE.Components.DeclarationFunction.CAP_SO_TO_KHAI:
                        //    Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage,spdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQCapSoTiepNhan,"Cap so to khai");
                        //   break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.THONG_QUAN:
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, spdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDaDuyetSanPham);
                            break;
                        default:
                            break;
                    }
                    spdk.Update();
                }
                else
                {
                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }
        private void LayPhanHoiToKhaiV3()
        {
            if (dgList.GetRow() != null)
                spdk = (SanPhamDangKy)dgList.GetRow().DataRow;
            else
                return;
            string reference = spdk.GUIDSTR;

            Company.KDT.SHARE.Components.SubjectBase subjectBase = new Company.KDT.SHARE.Components.SubjectBase()
            {
                Issuer = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_NPL,
                Reference = reference,
                Function = Company.KDT.SHARE.Components.DeclarationFunction.HOI_TRANG_THAI,
                Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_NPL,

            };

            string msgSend = Company.KDT.SHARE.Components.Helpers.BuildFeedBack(
                                        new Company.KDT.SHARE.Components.NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = spdk.MaDoanhNghiep
                                        },
                                          new Company.KDT.SHARE.Components.NameBase()
                                          {
                                              Name = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(spdk.MaHaiQuan.Trim()),
                                              Identity = spdk.MaHaiQuan
                                          }, subjectBase, null);
            SendMessageForm sendForm = new SendMessageForm();
            sendForm.Send += SendMessage;
            sendForm.DoSend(msgSend);

        }
        private void cancelV3()
        {
            spdk = new SanPhamDangKy();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null)
            {
                spdk = (SanPhamDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = spdk.ID;
            }
            else
            {
                //ShowMessage("Chưa chọn thông tin để hủy.", false);
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            spdk.LoadSPCollection();
            Company.KDT.SHARE.Components.DeclarationBase sp = Company.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_SP,spdk.GUIDSTR,spdk.SoTiepNhan,spdk.MaHaiQuan, spdk.NgayTiepNhan);
            
            string msgSend = Company.KDT.SHARE.Components.Helpers.BuildSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = spdk.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = GlobalSettings.TEN_HAI_QUAN,
                                 Identity = spdk.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_SP,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = spdk.GUIDSTR,
                            }
                            ,
                            sp, null, true);
            SendMessageForm sendForm = new SendMessageForm();
            sendForm.Send += SendMessage;
            sendForm.DoSend(msgSend);
            Company.KDT.SHARE.Components.Globals.XmlSaveMessage(msgSend, spdk.ID, Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoToKhai);
        }
        #endregion
    }
}