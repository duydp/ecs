﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Janus.Windows.GridEX;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseDinhMucManagementForm : Company.Interface.BaseForm
    {
        public T_KHOKETOAN_DINHMUC DM = new T_KHOKETOAN_DINHMUC();
        public T_KHOKETOAN_SANPHAM_MAP SPMap = new T_KHOKETOAN_SANPHAM_MAP();
        public WareHouseDinhMucManagementForm()
        {
            InitializeComponent();
            cbHopDong.DataSource = HopDong.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "ID");
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            cbHopDong.SelectedIndex = -1;
        }
        private void BindDataSPMap()
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("HOPDONG_ID = " + cbHopDong.Value , "");
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataDMMap(String MASP)
        {
            try
            {
                grListDM.Refetch();
                grListDM.DataSource = T_KHOKETOAN_DINHMUC.SelectCollectionDynamic("HOPDONG_ID = " + cbHopDong.Value + "AND MASP ='" + MASP + "' AND MANPL LIKE '%" + txtNPL.Text.ToString() + "%'", "");
                grListDM.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void WareHouseDinhMucManagementForm_Load(object sender, EventArgs e)
        {

        }

        private void grListSP_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SPMap = (T_KHOKETOAN_SANPHAM_MAP)i.GetRow().DataRow;                        
                    }
                }
                BindDataDMMap(SPMap.MASP);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListDM_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void grListDM_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                DM = new T_KHOKETOAN_DINHMUC();
                DM = (T_KHOKETOAN_DINHMUC)e.Row.DataRow;
                HopDong HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value));
                WareHouseDinhMucMaper f = new WareHouseDinhMucMaper();
                f.HD = HD;
                f.DM = DM;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            BindDataSPMap();
        }

        private void btnSearchSP_Click(object sender, EventArgs e)
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASP LIKE '%"+txtMaSP.Text.ToString()+"%' AND HOPDONG_ID = " + cbHopDong.Value, "");
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearchNPL_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SPMap = (T_KHOKETOAN_SANPHAM_MAP)i.GetRow().DataRow;
                    }
                }
                BindDataDMMap(SPMap.MASP);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            WareHouseDinhMucAutoMapperForm f = new WareHouseDinhMucAutoMapperForm();
            HopDong HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value));
            f.HD = HD;
            f.ShowDialog(this);
            BindDataSPMap();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            WareHouseDinhMucMaper f = new WareHouseDinhMucMaper();
            HopDong HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value));
            f.HD = HD;
            f.ShowDialog(this);
            BindDataSPMap();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListDM.SelectedItems;
                List<T_KHOKETOAN_DINHMUC> ItemColl = new List<T_KHOKETOAN_DINHMUC>();
                if (grListDM.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_DINHMUC)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KHOKETOAN_DINHMUC item in ItemColl)
                    {
                        if (item.ID > 0)
                        {
                            item.Delete();
                        }
                    }
                    ShowMessageTQDT("Xóa thành công !", false);
                    btnSearchNPL_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
