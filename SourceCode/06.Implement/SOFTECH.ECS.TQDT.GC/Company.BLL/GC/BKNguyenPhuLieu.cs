using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace Company.GC.BLL.GC
{
    public partial class BKNguyenPhuLieu 
    {
        public DataSet getNPL( long  dk)
        {
            Database db =(SqlDatabase) DatabaseFactory.CreateDatabase();
            string strSQL = "select * from t_GC_BKNguyenPhuLieu Where OldHD_ID =@OldHD_ID";            
            DbCommand dbcommand = db.GetSqlStringCommand(strSQL);
            db.AddInParameter(dbcommand, "@OldHD_ID", DbType.Int64, dk); 
            return db.ExecuteDataSet(dbcommand);
        }

        public int DeleteOldBK( long HopDong_ID)
        {
            string strSQL = " Delete from t_GC_BKNguyenPhuLieu where OldHD_ID = @OldHD_ID ";
            SqlCommand cmd = (SqlCommand)db.GetSqlStringCommand(strSQL);
            db.AddInParameter(cmd, "@OldHD_ID", DbType.Int64, HopDong_ID); 
            return db.ExecuteNonQuery(cmd);
        }
        public bool UpdateGrid(BKNguyenPhuLieuCollection   collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BKNguyenPhuLieu   item in collection)
                    {
                        BKNguyenPhuLieu npl = new BKNguyenPhuLieu();
                        npl.DonGia = item.DonGia;
                        npl.DVT = item.DVT;
                        npl.GhiChu = item.GhiChu;
                        npl.HinhThucCU = item.HinhThucCU;
                        npl.ID = item.ID;
                        npl.MaNPL = item.MaNPL;
                        npl.OldHD_ID = item.OldHD_ID;
                        npl.STT = item.STT;
                        npl.TenNPL = item.TenNPL;
                        npl.TongNPLCU = item.TongNPLCU;
                        npl.TriGia = item.TriGia;                       

                        if (npl.UpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool UpdateDataSetFromGrid(DataSet  ds)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DataRow dr in ds.Tables[0].Rows )
                    {
                        BKNguyenPhuLieu npl = new BKNguyenPhuLieu();
                        npl.DonGia = Convert.ToDecimal(dr["DonGia"]);
                        npl.DVT = dr["DVT"].ToString();
                        npl.GhiChu = dr["GhiChu"].ToString();
                        npl.HinhThucCU = dr["HinhThucCU"].ToString();
                        npl.ID = Convert.ToInt64(dr["ID"]);
                        npl.MaNPL = dr["MaNPL"].ToString();
                        npl.OldHD_ID = Convert.ToInt64(dr["OldHD_ID"]);
                        npl.STT = Convert.ToInt32(dr["STT"]);
                        npl.TenNPL = dr["TenNPL"].ToString();
                        npl.TongNPLCU = Convert.ToDecimal(dr["TongNPLCU"]);
                        npl.TriGia = Convert.ToDecimal(dr["TriGia"]);  

                        if (npl.UpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }
}