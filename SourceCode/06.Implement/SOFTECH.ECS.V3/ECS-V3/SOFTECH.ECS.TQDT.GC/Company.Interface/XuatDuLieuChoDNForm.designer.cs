﻿namespace Company.Interface
{
    partial class XuatDuLieuChoDNForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XuatDuLieuChoDNForm));
            this.btnXuat = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.chkDinhMuc = new Janus.Windows.EditControls.UICheckBox();
            this.chkPhuKien = new Janus.Windows.EditControls.UICheckBox();
            this.chkTKMD = new Janus.Windows.EditControls.UICheckBox();
            this.chkTKGCCT = new Janus.Windows.EditControls.UICheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkDefault = new Janus.Windows.EditControls.UICheckBox();
            this.txtThuMucLuu = new System.Windows.Forms.TextBox();
            this.btnChon = new Janus.Windows.EditControls.UIButton();
            this.FolderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.chkBKCU = new Janus.Windows.EditControls.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.txtThuMucLuu);
            this.grbMain.Controls.Add(this.chkTKGCCT);
            this.grbMain.Controls.Add(this.chkBKCU);
            this.grbMain.Controls.Add(this.chkTKMD);
            this.grbMain.Controls.Add(this.chkPhuKien);
            this.grbMain.Controls.Add(this.chkDinhMuc);
            this.grbMain.Controls.Add(this.chkDefault);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.btnChon);
            this.grbMain.Controls.Add(this.btnXuat);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Size = new System.Drawing.Size(349, 210);
            // 
            // btnXuat
            // 
            this.btnXuat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuat.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXuat.Icon")));
            this.btnXuat.Location = new System.Drawing.Point(167, 173);
            this.btnXuat.Name = "btnXuat";
            this.btnXuat.Size = new System.Drawing.Size(86, 25);
            this.btnXuat.TabIndex = 36;
            this.btnXuat.Text = "Thực hiện";
            this.btnXuat.VisualStyleManager = this.vsmMain;
            this.btnXuat.Click += new System.EventHandler(this.btnXuat_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(259, 173);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 25);
            this.btnClose.TabIndex = 35;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(14, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(227, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Các danh mục cần xuất theo hợp đồng :";
            // 
            // chkDinhMuc
            // 
            this.chkDinhMuc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDinhMuc.BackColor = System.Drawing.Color.Transparent;
            this.chkDinhMuc.Checked = true;
            this.chkDinhMuc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDinhMuc.Location = new System.Drawing.Point(42, 32);
            this.chkDinhMuc.Name = "chkDinhMuc";
            this.chkDinhMuc.Size = new System.Drawing.Size(63, 18);
            this.chkDinhMuc.TabIndex = 38;
            this.chkDinhMuc.Text = "Định mức";
            this.chkDinhMuc.VisualStyleManager = this.vsmMain;
            // 
            // chkPhuKien
            // 
            this.chkPhuKien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkPhuKien.BackColor = System.Drawing.Color.Transparent;
            this.chkPhuKien.Checked = true;
            this.chkPhuKien.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPhuKien.Location = new System.Drawing.Point(42, 56);
            this.chkPhuKien.Name = "chkPhuKien";
            this.chkPhuKien.Size = new System.Drawing.Size(63, 18);
            this.chkPhuKien.TabIndex = 38;
            this.chkPhuKien.Text = "Phụ kiện";
            this.chkPhuKien.VisualStyleManager = this.vsmMain;
            // 
            // chkTKMD
            // 
            this.chkTKMD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkTKMD.BackColor = System.Drawing.Color.Transparent;
            this.chkTKMD.Checked = true;
            this.chkTKMD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTKMD.Location = new System.Drawing.Point(157, 32);
            this.chkTKMD.Name = "chkTKMD";
            this.chkTKMD.Size = new System.Drawing.Size(96, 18);
            this.chkTKMD.TabIndex = 38;
            this.chkTKMD.Text = "Tờ khai mậu dịch";
            this.chkTKMD.VisualStyleManager = this.vsmMain;
            this.chkTKMD.CheckedChanged += new System.EventHandler(this.chkTKMD_CheckedChanged);
            // 
            // chkTKGCCT
            // 
            this.chkTKGCCT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkTKGCCT.BackColor = System.Drawing.Color.Transparent;
            this.chkTKGCCT.Checked = true;
            this.chkTKGCCT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTKGCCT.Location = new System.Drawing.Point(157, 56);
            this.chkTKGCCT.Name = "chkTKGCCT";
            this.chkTKGCCT.Size = new System.Drawing.Size(153, 18);
            this.chkTKGCCT.TabIndex = 38;
            this.chkTKGCCT.Text = "Tờ khai gia công chuyển tiếp";
            this.chkTKGCCT.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label5.Location = new System.Drawing.Point(14, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "Chọn thư mục lưu :";
            // 
            // chkDefault
            // 
            this.chkDefault.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDefault.BackColor = System.Drawing.Color.Transparent;
            this.chkDefault.Checked = true;
            this.chkDefault.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDefault.Location = new System.Drawing.Point(132, 116);
            this.chkDefault.Name = "chkDefault";
            this.chkDefault.Size = new System.Drawing.Size(63, 18);
            this.chkDefault.TabIndex = 38;
            this.chkDefault.Text = "Mặc định";
            this.chkDefault.VisualStyleManager = this.vsmMain;
            this.chkDefault.CheckedChanged += new System.EventHandler(this.chkDefault_CheckedChanged);
            // 
            // txtThuMucLuu
            // 
            this.txtThuMucLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtThuMucLuu.BackColor = System.Drawing.Color.White;
            this.txtThuMucLuu.Enabled = false;
            this.txtThuMucLuu.Location = new System.Drawing.Point(42, 140);
            this.txtThuMucLuu.Name = "txtThuMucLuu";
            this.txtThuMucLuu.Size = new System.Drawing.Size(271, 21);
            this.txtThuMucLuu.TabIndex = 40;
            // 
            // btnChon
            // 
            this.btnChon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChon.Enabled = false;
            this.btnChon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChon.Location = new System.Drawing.Point(319, 140);
            this.btnChon.Name = "btnChon";
            this.btnChon.Size = new System.Drawing.Size(20, 21);
            this.btnChon.TabIndex = 36;
            this.btnChon.Text = "...";
            this.btnChon.VisualStyleManager = this.vsmMain;
            this.btnChon.Click += new System.EventHandler(this.btnChon_Click);
            // 
            // chkBKCU
            // 
            this.chkBKCU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkBKCU.BackColor = System.Drawing.Color.Transparent;
            this.chkBKCU.Checked = true;
            this.chkBKCU.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBKCU.Location = new System.Drawing.Point(42, 80);
            this.chkBKCU.Name = "chkBKCU";
            this.chkBKCU.Size = new System.Drawing.Size(110, 18);
            this.chkBKCU.TabIndex = 38;
            this.chkBKCU.Text = "Bảng kê cung ứng";
            this.chkBKCU.VisualStyleManager = this.vsmMain;
            // 
            // XuatDuLieuChoDNForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(349, 210);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(357, 387);
            this.MinimizeBox = false;
            this.Name = "XuatDuLieuChoDNForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Xuất dữ liệu đã đăng ký cho doanh nghiệp";
            this.Load += new System.EventHandler(this.XuatDuLieuChoDNForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnXuat;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UICheckBox chkTKGCCT;
        private Janus.Windows.EditControls.UICheckBox chkTKMD;
        private Janus.Windows.EditControls.UICheckBox chkPhuKien;
        private Janus.Windows.EditControls.UICheckBox chkDinhMuc;
        private Janus.Windows.EditControls.UICheckBox chkDefault;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtThuMucLuu;
        private Janus.Windows.EditControls.UIButton btnChon;
        private System.Windows.Forms.FolderBrowserDialog FolderBrowser;
        private Janus.Windows.EditControls.UICheckBox chkBKCU;
    }
}