﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class BCKhaiBoSungThue : DevExpress.XtraReports.UI.XtraReport
    {
       // public SanPhamCollection SPCollection = new SanPhamCollection();
        public DataTable dt = new DataTable();
        private int STT = 0;
        public BCKhaiBoSungThue()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblTokhai.Text = GlobalSettings.DIA_CHI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            this.DataSource = this.dt;


            dt.TableName = "BaoCaoThueTon";
            lblSoHopDong.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoHopDong");
            lblTokhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaLoaiHinh"); 
           
            lblSoLuongDK1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoLuong","{0:N0}");
            lblSoLuongDK2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoLuong", "{0:N0}");

            lblTenhang1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenHang");
            lblTenhang2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenHang");  

            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblDVT2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");            

            lblDGKB1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DonGiaKB", "{0:N3}");
            lblDonGiaDK2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DonGiaKB", "{0:N3}");

            lblTGKB1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NguyenTe_ID");

            lblTokhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL");
           

           lblTokhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL1");
           lblMaSoHH.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaPhu");
           lblMasoHH2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaPhu");

           lblMucThueSuat.Text = "Hang khong chui thue GTGT";
           lblMucThueSuat2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL1");

           lblTyGia1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyGiaUSD");
           lblTyGia2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyGiaUSD");

           lblXuatXu1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NuocXX_ID");
           lblXuatXu2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NuocXX_ID");
            
          //lblSoTienDaKhai1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL1");

            lblngay.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKy","{0:dd/MM/yyyy}");
            lblNgay2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKy", "{0:dd/MM/yyyy}");

            lblSoLuongDK2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoLuong","{0:N0}");           
           lblSoTienNop2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL1");
           
           lblThueChenhLech2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyLeThuKhac");

          lblThueGTGT1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueGTGT", "{0:N3}");
          lblThueGTGT2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueGTGT", "{0:N3}");
          lblThueGTGT3.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueGTGT", "{0:N3}");
          if(lblTokhai.Text.Contains("NK")){
              lblThueNK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueXNK", "{0:N3}");
              lblThueNK2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueXNK", "{0:N3}");
              lblThueNK3.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueXNK", "{0:N3}");          
          }
          if (lblTokhai.Text.Contains("XK"))
          {
              lblThueXK2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueXNK", "{0:N3}");
              lblThueXK3.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueXNK", "{0:N3}");
              lblThueXuatKhau1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueXNK", "{0:N3}");
          }
          lblThueTieuDB.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueSuatTTDB");
          lblThueTTDB2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueSuatTTDB");

         


           
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            //lblSTT.Text = this.STT + "";
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblDVT.Text = DonViTinh.GetName(GetCurrentColumnValue("DVT_ID"));
        }

    }
}
