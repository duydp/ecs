﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class ThanhKhoanHDGC
    {
        #region Properties.

        public long HopDong_ID { set; get; }
        public string Ten { set; get; }
        public string DVT { set; get; }
        public decimal TongLuongNK { set; get; }
        public decimal TongLuongCU { set; get; }
        public decimal TongLuongXK { set; get; }
        public decimal ChenhLech { set; get; }
        public string KetLuanXLCL { set; get; }
        public long OldHD_ID { set; get; }
        public int STT { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<ThanhKhoanHDGC> ConvertToCollection(IDataReader reader)
        {
            IList<ThanhKhoanHDGC> collection = new List<ThanhKhoanHDGC>();
            while (reader.Read())
            {
                ThanhKhoanHDGC entity = new ThanhKhoanHDGC();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongNK"))) entity.TongLuongNK = reader.GetDecimal(reader.GetOrdinal("TongLuongNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongCU"))) entity.TongLuongCU = reader.GetDecimal(reader.GetOrdinal("TongLuongCU"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongXK"))) entity.TongLuongXK = reader.GetDecimal(reader.GetOrdinal("TongLuongXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChenhLech"))) entity.ChenhLech = reader.GetDecimal(reader.GetOrdinal("ChenhLech"));
                if (!reader.IsDBNull(reader.GetOrdinal("KetLuanXLCL"))) entity.KetLuanXLCL = reader.GetString(reader.GetOrdinal("KetLuanXLCL"));
                if (!reader.IsDBNull(reader.GetOrdinal("OldHD_ID"))) entity.OldHD_ID = reader.GetInt64(reader.GetOrdinal("OldHD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static ThanhKhoanHDGC Load(long hopDong_ID)
        {
            const string spName = "[dbo].[p_GC_ThanhKhoanHDGC_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<ThanhKhoanHDGC> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<ThanhKhoanHDGC> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<ThanhKhoanHDGC> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		


        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_GC_ThanhKhoanHDGC_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_GC_ThanhKhoanHDGC_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_GC_ThanhKhoanHDGC_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_GC_ThanhKhoanHDGC_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertThanhKhoanHDGC(string ten, string dVT, decimal tongLuongNK, decimal tongLuongCU, decimal tongLuongXK, decimal chenhLech, string ketLuanXLCL, long oldHD_ID, int sTT)
        {
            ThanhKhoanHDGC entity = new ThanhKhoanHDGC();
            entity.Ten = ten;
            entity.DVT = dVT;
            entity.TongLuongNK = tongLuongNK;
            entity.TongLuongCU = tongLuongCU;
            entity.TongLuongXK = tongLuongXK;
            entity.ChenhLech = chenhLech;
            entity.KetLuanXLCL = ketLuanXLCL;
            entity.OldHD_ID = oldHD_ID;
            entity.STT = sTT;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_GC_ThanhKhoanHDGC_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
            db.AddInParameter(dbCommand, "@TongLuongNK", SqlDbType.Decimal, TongLuongNK);
            db.AddInParameter(dbCommand, "@TongLuongCU", SqlDbType.Decimal, TongLuongCU);
            db.AddInParameter(dbCommand, "@TongLuongXK", SqlDbType.Decimal, TongLuongXK);
            db.AddInParameter(dbCommand, "@ChenhLech", SqlDbType.Decimal, ChenhLech);
            db.AddInParameter(dbCommand, "@KetLuanXLCL", SqlDbType.NVarChar, KetLuanXLCL);
            db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, OldHD_ID);
            db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                HopDong_ID = (long)db.GetParameterValue(dbCommand, "@HopDong_ID");
                return HopDong_ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                HopDong_ID = (long)db.GetParameterValue(dbCommand, "@HopDong_ID");
                return HopDong_ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<ThanhKhoanHDGC> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ThanhKhoanHDGC item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateThanhKhoanHDGC(long hopDong_ID, string ten, string dVT, decimal tongLuongNK, decimal tongLuongCU, decimal tongLuongXK, decimal chenhLech, string ketLuanXLCL, long oldHD_ID, int sTT)
        {
            ThanhKhoanHDGC entity = new ThanhKhoanHDGC();
            entity.HopDong_ID = hopDong_ID;
            entity.Ten = ten;
            entity.DVT = dVT;
            entity.TongLuongNK = tongLuongNK;
            entity.TongLuongCU = tongLuongCU;
            entity.TongLuongXK = tongLuongXK;
            entity.ChenhLech = chenhLech;
            entity.KetLuanXLCL = ketLuanXLCL;
            entity.OldHD_ID = oldHD_ID;
            entity.STT = sTT;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_GC_ThanhKhoanHDGC_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
            db.AddInParameter(dbCommand, "@TongLuongNK", SqlDbType.Decimal, TongLuongNK);
            db.AddInParameter(dbCommand, "@TongLuongCU", SqlDbType.Decimal, TongLuongCU);
            db.AddInParameter(dbCommand, "@TongLuongXK", SqlDbType.Decimal, TongLuongXK);
            db.AddInParameter(dbCommand, "@ChenhLech", SqlDbType.Decimal, ChenhLech);
            db.AddInParameter(dbCommand, "@KetLuanXLCL", SqlDbType.NVarChar, KetLuanXLCL);
            db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, OldHD_ID);
            db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<ThanhKhoanHDGC> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ThanhKhoanHDGC item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateThanhKhoanHDGC(long hopDong_ID, string ten, string dVT, decimal tongLuongNK, decimal tongLuongCU, decimal tongLuongXK, decimal chenhLech, string ketLuanXLCL, long oldHD_ID, int sTT)
        {
            ThanhKhoanHDGC entity = new ThanhKhoanHDGC();
            entity.HopDong_ID = hopDong_ID;
            entity.Ten = ten;
            entity.DVT = dVT;
            entity.TongLuongNK = tongLuongNK;
            entity.TongLuongCU = tongLuongCU;
            entity.TongLuongXK = tongLuongXK;
            entity.ChenhLech = chenhLech;
            entity.KetLuanXLCL = ketLuanXLCL;
            entity.OldHD_ID = oldHD_ID;
            entity.STT = sTT;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_GC_ThanhKhoanHDGC_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
            db.AddInParameter(dbCommand, "@TongLuongNK", SqlDbType.Decimal, TongLuongNK);
            db.AddInParameter(dbCommand, "@TongLuongCU", SqlDbType.Decimal, TongLuongCU);
            db.AddInParameter(dbCommand, "@TongLuongXK", SqlDbType.Decimal, TongLuongXK);
            db.AddInParameter(dbCommand, "@ChenhLech", SqlDbType.Decimal, ChenhLech);
            db.AddInParameter(dbCommand, "@KetLuanXLCL", SqlDbType.NVarChar, KetLuanXLCL);
            db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, OldHD_ID);
            db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<ThanhKhoanHDGC> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ThanhKhoanHDGC item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteThanhKhoanHDGC(long hopDong_ID)
        {
            ThanhKhoanHDGC entity = new ThanhKhoanHDGC();
            entity.HopDong_ID = hopDong_ID;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_GC_ThanhKhoanHDGC_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_GC_ThanhKhoanHDGC_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<ThanhKhoanHDGC> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ThanhKhoanHDGC item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}