
/****** Object:  StoredProcedure [dbo].[p_GC_BC03HSTK_GC_TT117]    Script Date: 07/30/2014 09:41:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  LanNT          
-- Create date:           
-- Description:   Modify 18/06/2012    
-- =============================================          
          
ALTER PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117]           
 -- Add the parameters for the stored procedure here          
 @IDHopDong BIGINT,          
 @MaHaiQuan CHAR(6),          
 @MaDoanhNghiep VARCHAR(14)           
           
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
 -- p_GC_BC03HSTK_GC_TT117 219, 'C34C','0400101556'          
-- SET @IDHopDong=306 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',          
-- declare @IDHopDong BIGINT, @MaHaiQuan CHAR(6), @MaDoanhNghiep VARCHAR(14);  set @IDHopDong=  306;set @MaHaiQuan='C34C'; Set @MaDoanhNghiep='0400101556';          
          
    --      
     /*ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT'*/    
     -- CONVERT(varchar(50), ToKhai.SoToKhai) + '/' + v_HaiQuan_MaLoaiHinh.Ten_VT AS SoToKhai  
SELECT    ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT', ToKhai.SoToKhai, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, '-' AS TongCong, ToKhai.IDHopDong  
FROM         (  
          
      SELECT     CONVERT(varchar(50), 
      (case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_ToKhaiMauDich.SoToKhai) else t_KDT_ToKhaiMauDich.SoToKhai end )) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai,   
              LTRIM(RTRIM(t_KDT_ToKhaiMauDich.MaLoaiHinh)) AS MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong,   
              t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.DVT_ID  
       FROM         t_KDT_ToKhaiMauDich INNER JOIN  
              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN  
              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID  
       WHERE     (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND   
        (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND  
        (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%') AND   
        (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N') AND  
        (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND   
        (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep) AND  
        (NOT (t_KDT_ToKhaiMauDich.MaLoaiHinh IN ('XGC18,XGC19,XGC20','XVE54')))  
                         
                                                                                                                        
                       UNION  
                         
       SELECT     CONVERT(varchar(50), 
       (case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) else t_KDT_GC_ToKhaiChuyenTiep.SoToKhai end )) + '/' + t_HaiQuan_LoaiPhieuChuyenTiep.ID AS SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,   
                      t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_HangChuyenTiep.MaHang, t_KDT_GC_HangChuyenTiep.TenHang,   
                      t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_HangChuyenTiep.ID_DVT  
FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN  
                      t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID INNER JOIN  
                      t_HaiQuan_LoaiPhieuChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = t_HaiQuan_LoaiPhieuChuyenTiep.ID  
WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND   
                      (LTRIM(RTRIM(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh)) IN ('PHPLX', 'XGC18', 'PHSPX','PHTBX','XVE54')) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N')           
       )   
                      AS ToKhai INNER JOIN  
                      t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID  
WHERE     (ToKhai.IDHopDong = @IDHopDong)  
ORDER BY ToKhai.MaHang  
  
END     

GO


/****** Object:  StoredProcedure [dbo].[p_GC_BC03HSTK_GC_TT117_New]    Script Date: 07/30/2014 09:52:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:  LanNT          
-- Create date:           
-- Description:   Modify 18/06/2012    
-- =============================================          
          
ALTER PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117_New]           
 -- Add the parameters for the stored procedure here          
 @IDHopDong BIGINT,          
 @MaHaiQuan CHAR(6),          
 @MaDoanhNghiep VARCHAR(14)           
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
 -- p_GC_BC03HSTK_GC_TT117 219, 'C34C','0400101556'          
-- SET @IDHopDong=306 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',          
-- declare @IDHopDong BIGINT, @MaHaiQuan CHAR(6), @MaDoanhNghiep VARCHAR(14);  set @IDHopDong=  306;set @MaHaiQuan='C34C'; Set @MaDoanhNghiep='0400101556';          
          
    --      
     /*ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT'*/    
     -- CONVERT(varchar(50), ToKhai.SoToKhai) + '/' + v_HaiQuan_MaLoaiHinh.Ten_VT AS SoToKhai  

--DECLARE    
-- @IDHopDong BIGINT,          
-- @MaHaiQuan CHAR(6),          
-- @MaDoanhNghiep VARCHAR(14)       

--SET
--@IDHopDong = 721
--SET
--@MaHaiQuan = 'N60C'
--SET
--@MaDoanhNghiep = '4000395355' 
   
if OBJECT_ID('tempdb..#BC02') is not null drop table #BC02
CREATE TABLE #BC03
(STT BIGINT
 ,SoToKhai VARCHAR(50)
 ,MaHang VARCHAR(35)
 ,TenHang NVARCHAR(255)
 ,DVT VARCHAR(6)
 ,SoLuong DECIMAL(18,6)
 ,TongCong DECIMAL(18,6)
 ,IDHopDong BIGINT
)


 INSERT INTO #BC03
 (
 	STT,
 	SoToKhai,
 	MaHang,
 	TenHang,
 	DVT,
 	SoLuong,
 	TongCong,
 	IDHopDong
 )
SELECT    ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang, ToKhai.NgayDangKy) AS 'STT', ToKhai.SoToKhai, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, 0 AS TongCong, ToKhai.IDHopDong  
FROM         (  
          
       SELECT     CONVERT(varchar(50), 
      (case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_ToKhaiMauDich.SoToKhai) else t_KDT_ToKhaiMauDich.SoToKhai end )) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai,   
              LTRIM(RTRIM(t_KDT_ToKhaiMauDich.MaLoaiHinh)) AS MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong,   
              t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.DVT_ID  
       FROM         t_KDT_ToKhaiMauDich INNER JOIN  
              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN  
              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID  
       WHERE     (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND   
        (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND  
        (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%') AND   
        (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N') AND  
        (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND   
        (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep) AND  
        (NOT (t_KDT_ToKhaiMauDich.MaLoaiHinh IN ('XGC18,XGC19,XGC20','XVE54')))  
                         
                                                                                                                        
                       UNION  
                         
       SELECT     CONVERT(varchar(50), 
       (case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) else t_KDT_GC_ToKhaiChuyenTiep.SoToKhai end )) + '/' + t_HaiQuan_LoaiPhieuChuyenTiep.ID AS SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,   
                      t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_HangChuyenTiep.MaHang, t_KDT_GC_HangChuyenTiep.TenHang,   
                      t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_HangChuyenTiep.ID_DVT  
FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN  
                      t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID INNER JOIN  
                      t_HaiQuan_LoaiPhieuChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = t_HaiQuan_LoaiPhieuChuyenTiep.ID  
WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND   
                      (LTRIM(RTRIM(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh)) IN ('PHPLX', 'XGC18', 'PHSPX','PHTBX','XVE54')) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N')           
       )   
                      AS ToKhai INNER JOIN  
                      t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID  
WHERE     (ToKhai.IDHopDong = @IDHopDong)  
--ORDER BY ToKhai.MaHang  

	

    
UPDATE t1
SET
	TongCong = t2.RunningTotal
FROM #BC03 t1 INNER JOIN
(SELECT temp1.STT AS STT,temp1.MaHang AS MaHang,SUM(temp2.SoLuong) AS RunningTotal
   FROM #BC03 temp1 CROSS JOIN #BC03 temp2
 WHERE temp2.STT <= temp1.STT AND Temp2.MaHang = temp1.MaHang 
 GROUP BY temp1.STT, temp1.MaHang) t2
 ON t1.STT = t2.STT
 
 
 
SELECT * FROM #BC03 
DROP TABLE #BC03  
  
END
     

GO


/****** Object:  StoredProcedure [dbo].[p_GC_BC03HSTK_GC_TT117_Tong]    Script Date: 07/30/2014 09:52:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  LanNT      
-- Create date:       
-- Description:   Modify 18/06/2012    
-- =============================================      
      
ALTER PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117_Tong]       
 -- Add the parameters for the stored procedure here      
 @IDHopDong BIGINT,      
 @MaHaiQuan CHAR(6),      
 @MaDoanhNghiep VARCHAR(14)       
       
AS      
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
 -- p_GC_BC03HSTK_GC_TT117 219, 'C34C','0400101556'          
-- SET @IDHopDong=306 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',          
-- declare @IDHopDong BIGINT, @MaHaiQuan CHAR(6), @MaDoanhNghiep VARCHAR(14);  set @IDHopDong=  306;set @MaHaiQuan='C34C'; Set @MaDoanhNghiep='0400101556';          
          
    --      
     /*ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT'*/    
     -- CONVERT(varchar(50), ToKhai.SoToKhai) + '/' + v_HaiQuan_MaLoaiHinh.Ten_VT AS SoToKhai  
SELECT   ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT',  ToKhai.SoToKhai, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, '-' AS SoLuong, SUM(ToKhai.SoLuong) AS TongCong,   
                      ToKhai.IDHopDong  
FROM         (SELECT     CONVERT(varchar(50),
			(case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_ToKhaiMauDich.SoToKhai) else t_KDT_ToKhaiMauDich.SoToKhai end )) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai,
                                              LTRIM(RTRIM(t_KDT_ToKhaiMauDich.MaLoaiHinh)) AS MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong,   
                                              t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.DVT_ID  
                       FROM          t_KDT_ToKhaiMauDich INNER JOIN  
                                              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN  
                                              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID  
                       WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND   
                                              (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N') AND   
                                              (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep) AND   
                                              (NOT (t_KDT_ToKhaiMauDich.MaLoaiHinh IN ('XGC18,XGC19,XGC20','XVE54')))  
                       UNION  
                       SELECT     CONVERT(varchar(50), 
                       (case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) else t_KDT_GC_ToKhaiChuyenTiep.SoToKhai end )) + '/' + t_HaiQuan_LoaiPhieuChuyenTiep.ID AS SoToKhai,
                       t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,   
                      t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_HangChuyenTiep.MaHang, t_KDT_GC_HangChuyenTiep.TenHang,   
                      t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_HangChuyenTiep.ID_DVT  
FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN  
                      t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID INNER JOIN  
                      t_HaiQuan_LoaiPhieuChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = t_HaiQuan_LoaiPhieuChuyenTiep.ID  
WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND   
                      (LTRIM(RTRIM(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh)) IN ('PHPLX', 'XGC18', 'PHSPX', 'PHTBX','XVE54')) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N')  
                                             )   
                      AS ToKhai INNER JOIN  
    t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID  
GROUP BY ToKhai.SoToKhai, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten, ToKhai.IDHopDong  
HAVING      (ToKhai.IDHopDong = @IDHopDong)  
ORDER BY ToKhai.MaHang  
  
END     
GO



---------Bao cao 09------------------------------------------------------------------------------------------------




/****** Object:  StoredProcedure [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]    Script Date: 07/30/2014 09:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  Huynh Ngoc Khanh    
-- Create date:     18/04/2013
-- Description:     View BC 09/HSTK-TT117
-- =============================================    
ALTER PROCEDURE [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;   
  
 --DECLARE
 --@IDHopDong BIGINT,    
 --@MaHaiQuan CHAR(6),    
 --@MaDoanhNghiep VARCHAR(14) 
 --SET @IDHopDong = 444
 --SET @MaHaiQuan = 'C34C'
 --SET @MaDoanhNghiep = '0400101556'
 SELECT  
 ROW_NUMBER() OVER (ORDER BY BangKe.SoNgayToKhai) AS 'STT',
 BangKe.SoNgayToKhai, BangKe.TenHang, BangKe.SoLuong, BangKe.TriGia, t_KDT_GC_HopDong.SoHopDong + ', ' + CONVERT(NVARCHAR(20),     
                      t_KDT_GC_HopDong.NgayKy, 103) + ', ' + CONVERT(NVARCHAR(20), t_KDT_GC_HopDong.NgayHetHan, 103) AS HdPhuLuc, BangKe.CuaKhau,     
                      BangKe.SoNgayBL, t_KDT_GC_HopDong.ID AS IDHopDong, t_KDT_GC_HopDong.MaHaiQuan, t_KDT_GC_HopDong.MaDoanhNghiep     FROM
 (
 (SELECT  CONVERT(NVARCHAR(20), 
 (case when TKMD.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = TKMD.SoToKhai) else TKMD.SoToKhai end )) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  TKMD.NgayDangKy, 103) AS SoNgayToKhai, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
								  t_KDT_HangMauDich.TriGiaKB AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau,
								   CASE PTVT_ID 
								  WHEN '001' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103) 
								  WHEN '005' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103) 
								  ELSE CASE TKMD.SoVanDon 
								  WHEN '' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103)
								  ELSE
								   TKMD.SoVanDon + ', ' + CONVERT(NVARCHAR(20), TKMD.NgayVanDon, 103) 
								       END 
								   END AS SoNgayBL
			    FROM (SELECT ID,SoToKhai,NgayDangKy,MaLoaiHinh,CuaKhau_ID,SoVanDon,NgayVanDon,PTVT_ID
			            FROM t_KDT_ToKhaiMauDich  
				WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%' 
				AND t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong AND t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep 
				AND t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AS TKMD
	   LEFT JOIN t_HaiQuan_CuaKhau ON t_HaiQuan_CuaKhau.ID = TKMD.CuaKhau_ID
	   INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_HaiQuan_LoaiHinhMauDich.ID = TKMD.MaLoaiHinh
	   INNER JOIN t_KDT_HangMauDich ON TKMD.ID = t_KDT_HangMauDich.TKMD_ID
	   LEFT  JOIN ( select * from t_GC_BC08TT74_VanDonToKhaiXuat WHERE t_GC_BC08TT74_VanDonToKhaiXuat.IDHopDong = @IDHopDong) t_GC_BC08TT74_VanDonToKhaiXuat  ON t_GC_BC08TT74_VanDonToKhaiXuat.TKMD_ID = TKMD.ID)
 UNION
 (SELECT CONVERT(NVARCHAR(20),
  (case when TKCT.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = TKCT.SoToKhai) else TKCT.SoToKhai end )) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  TKCT.NgayDangKy, 103) AS SoNgayToKhai,t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, 
								  t_KDT_GC_HangChuyenTiep.TriGia AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau, '' AS SoNgayBL 
  FROM (SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep 
                WHERE  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE 'X%' OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE '%X')
						AND  t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep AND t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AS TKCT
	   LEFT JOIN t_HaiQuan_CuaKhau ON t_HaiQuan_CuaKhau.ID = TKCT.CuaKhau_ID
	   INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_HaiQuan_LoaiHinhMauDich.ID = TKCT.MaLoaiHinh
	   INNER JOIN t_KDT_GC_HangChuyenTiep ON TKCT.ID = t_KDT_GC_HangChuyenTiep.Master_ID)
)	
AS BangKe    
INNER JOIN t_KDT_GC_HopDong ON t_KDT_GC_HopDong.ID = @IDHopDong
		 
END

GO


/****** Object:  StoredProcedure [dbo].[p_GC_BC09HSTK_GC_TT117]    Script Date: 07/30/2014 09:21:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Name    
-- Create date:     
-- Description:     
-- =============================================    
ALTER PROCEDURE [dbo].[p_GC_BC09HSTK_GC_TT117]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
    SELECT  
    ROW_NUMBER() OVER (ORDER BY BangKe.SoNgayToKhai) AS 'STT',
   BangKe.SoNgayToKhai, BangKe.TenHang, BangKe.SoLuong, BangKe.TriGia, t_KDT_GC_HopDong.SoHopDong + ', ' + CONVERT(NVARCHAR(20),     
                      t_KDT_GC_HopDong.NgayDangKy, 103) + ', ' + CONVERT(NVARCHAR(20), t_KDT_GC_HopDong.NgayHetHan, 103) AS HdPhuLuc, BangKe.CuaKhau,     
                      BangKe.SoNgayBL, BangKe.IDHopDong, BangKe.MaHaiQuan, BangKe.MaDoanhNghiep    
			FROM         (SELECT     CONVERT(NVARCHAR(20), t_KDT_ToKhaiMauDich.SoToKhai) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  t_KDT_ToKhaiMauDich.NgayDangKy, 103) AS SoNgayToKhai, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
								  t_KDT_HangMauDich.TriGiaKB AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau, 
								  CASE PTVT_ID WHEN '001' THEN '' WHEN '005' THEN '' ELSE CASE t_KDT_ToKhaiMauDich.NgayVanDon WHEN '1900-1-1' THEN t_KDT_ToKhaiMauDich.SoVanDon ELSE
								   t_KDT_ToKhaiMauDich.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_KDT_ToKhaiMauDich.NgayVanDon, 103) END END AS SoNgayBL, 
								  t_KDT_ToKhaiMauDich.IDHopDong, t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_ToKhaiMauDich.MaDoanhNghiep
			FROM         t_KDT_ToKhaiMauDich INNER JOIN
								  t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN
								  t_HaiQuan_CuaKhau ON t_KDT_ToKhaiMauDich.CuaKhau_ID = t_HaiQuan_CuaKhau.ID INNER JOIN
								  t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
			WHERE     (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
								  (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep)   
                       UNION    
                       SELECT     CONVERT(NVARCHAR(20), t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
                      t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 103) AS SoNgayToKhai, t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, 
                      t_KDT_GC_HangChuyenTiep.TriGia, '' AS CuaKhau, '' AS SoNgayBL, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, 
                      t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan AS MaHaiQuan, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                      t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID INNER JOIN
                      t_HaiQuan_LoaiHinhMauDich ON t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE 'X%' OR
                      t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                      (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep))     
                      AS BangKe INNER JOIN    
                      t_KDT_GC_HopDong ON BangKe.IDHopDong = t_KDT_GC_HopDong.ID    
WHERE     (t_KDT_GC_HopDong.ID = @IDHopDong)    

END 
GO
IF NOT EXISTS (SELECT * FROM t_HaiQuan_LoaiPhieuChuyenTiep WHERE ID='XVE54')
INSERT INTO t_HaiQuan_LoaiPhieuChuyenTiep VALUES ('XVE54',N'Xuất nguyên phụ liệu gia công cho hợp đồng khác')
IF NOT EXISTS (SELECT * FROM t_HaiQuan_LoaiPhieuChuyenTiep WHERE ID='NVE23')
INSERT INTO t_HaiQuan_LoaiPhieuChuyenTiep VALUES ('NVE23',N'Nhập nguyên liệu gia công từ hợp đồng khác chuyển sang')

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.7',GETDATE(), N' Cập nhật bao cao 03,09')
END