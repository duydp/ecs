﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class ContractDocument
    {
        /// <summary>
        /// Số hợp đồng
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày hợp đồng
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Thời hạn thanh toán
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }

        ///// <summary>
        ///// Tổng trị giá
        ///// </summary>
        //[XmlElement("totalValue")]
        //public decimal TotalValue { get; set; }

        ///// <summary>
        ///// AttachedFile
        ///// </summary>
        //[XmlElement("AttachedFile")]
        //public Company.KDT.SHARE.Components.AttachedFile AttachedFile { get; set; }
    }
}
