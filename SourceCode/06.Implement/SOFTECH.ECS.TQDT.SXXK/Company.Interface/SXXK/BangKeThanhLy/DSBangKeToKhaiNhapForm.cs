using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.SXXK.BangKeThanhLy
{
    public partial class DSBangKeToKhaiNhapForm : BaseForm
    {
        public int SoToKhai = 0;
        public string MaLoaiHinh = "";
        public DateTime NgayDangKy = new DateTime(1900, 1, 1);
        public string MaNPL = "";
        public string TenNPL = "";
        public decimal SoLuong = 0;
        public long BangKeHSTL_ID;
        public string DVT_ID;
        public DSBangKeToKhaiNhapForm()
        {
            InitializeComponent();
        }

        private void DSBangKeToKhaiNhap_Load(object sender, EventArgs e)
        {
            dgList1.DataSource = new BKToKhaiNhap().getNPL_BKToKhaiNhap(this.BangKeHSTL_ID).Tables[0];
        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
            }
        }

        private void dgList1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
                this.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.TenNPL = e.Row.Cells["TenHang"].Text;
                this.SoLuong = Convert.ToDecimal(e.Row.Cells["SoLuong"].Value);
                this.MaHaiQuan = Convert.ToString(e.Row.Cells["MaHaiQuan"].Value);
                this.MaNPL = e.Row.Cells["Ma"].Text;
                this.DVT_ID = Convert.ToString(e.Row.Cells["DVT_ID"].Value);
                this.Close();
            }
        }
    }
}