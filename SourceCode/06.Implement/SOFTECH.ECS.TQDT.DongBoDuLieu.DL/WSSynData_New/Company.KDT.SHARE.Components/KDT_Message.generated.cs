﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components
{
	public partial class KDT_Message : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public Guid ReferenceID { set; get; }
		public long ItemID { set; get; }
		public string MessageFrom { set; get; }
		public string MessageTo { set; get; }
		public int MessageType { set; get; }
		public int MessageFunction { set; get; }
		public string MessageContent { set; get; }
		public DateTime CreatedTime { set; get; }
		public string TieuDeThongBao { set; get; }
		public string NoiDungThongBao { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_Message> ConvertToCollection(IDataReader reader)
		{
			List<KDT_Message> collection = new List<KDT_Message>();
			while (reader.Read())
			{
				KDT_Message entity = new KDT_Message();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ReferenceID"))) entity.ReferenceID = reader.GetGuid(reader.GetOrdinal("ReferenceID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ItemID"))) entity.ItemID = reader.GetInt64(reader.GetOrdinal("ItemID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageFrom"))) entity.MessageFrom = reader.GetString(reader.GetOrdinal("MessageFrom"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTo"))) entity.MessageTo = reader.GetString(reader.GetOrdinal("MessageTo"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageType"))) entity.MessageType = reader.GetInt32(reader.GetOrdinal("MessageType"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageFunction"))) entity.MessageFunction = reader.GetInt32(reader.GetOrdinal("MessageFunction"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageContent"))) entity.MessageContent = reader.GetString(reader.GetOrdinal("MessageContent"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatedTime"))) entity.CreatedTime = reader.GetDateTime(reader.GetOrdinal("CreatedTime"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDeThongBao"))) entity.TieuDeThongBao = reader.GetString(reader.GetOrdinal("TieuDeThongBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungThongBao"))) entity.NoiDungThongBao = reader.GetString(reader.GetOrdinal("NoiDungThongBao"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_Message> collection, long id)
        {
            foreach (KDT_Message item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_Messages VALUES(@ReferenceID, @ItemID, @MessageFrom, @MessageTo, @MessageType, @MessageFunction, @MessageContent, @CreatedTime, @TieuDeThongBao, @NoiDungThongBao)";
            string update = "UPDATE t_KDT_Messages SET ReferenceID = @ReferenceID, ItemID = @ItemID, MessageFrom = @MessageFrom, MessageTo = @MessageTo, MessageType = @MessageType, MessageFunction = @MessageFunction, MessageContent = @MessageContent, CreatedTime = @CreatedTime, TieuDeThongBao = @TieuDeThongBao, NoiDungThongBao = @NoiDungThongBao WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_Messages WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, "ReferenceID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ItemID", SqlDbType.BigInt, "ItemID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageFrom", SqlDbType.VarChar, "MessageFrom", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTo", SqlDbType.VarChar, "MessageTo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageType", SqlDbType.Int, "MessageType", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageFunction", SqlDbType.Int, "MessageFunction", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageContent", SqlDbType.NVarChar, "MessageContent", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedTime", SqlDbType.DateTime, "CreatedTime", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDeThongBao", SqlDbType.NVarChar, "TieuDeThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungThongBao", SqlDbType.NVarChar, "NoiDungThongBao", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, "ReferenceID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ItemID", SqlDbType.BigInt, "ItemID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageFrom", SqlDbType.VarChar, "MessageFrom", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTo", SqlDbType.VarChar, "MessageTo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageType", SqlDbType.Int, "MessageType", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageFunction", SqlDbType.Int, "MessageFunction", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageContent", SqlDbType.NVarChar, "MessageContent", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedTime", SqlDbType.DateTime, "CreatedTime", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDeThongBao", SqlDbType.NVarChar, "TieuDeThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungThongBao", SqlDbType.NVarChar, "NoiDungThongBao", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_Messages VALUES(@ReferenceID, @ItemID, @MessageFrom, @MessageTo, @MessageType, @MessageFunction, @MessageContent, @CreatedTime, @TieuDeThongBao, @NoiDungThongBao)";
            string update = "UPDATE t_KDT_Messages SET ReferenceID = @ReferenceID, ItemID = @ItemID, MessageFrom = @MessageFrom, MessageTo = @MessageTo, MessageType = @MessageType, MessageFunction = @MessageFunction, MessageContent = @MessageContent, CreatedTime = @CreatedTime, TieuDeThongBao = @TieuDeThongBao, NoiDungThongBao = @NoiDungThongBao WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_Messages WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, "ReferenceID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ItemID", SqlDbType.BigInt, "ItemID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageFrom", SqlDbType.VarChar, "MessageFrom", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTo", SqlDbType.VarChar, "MessageTo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageType", SqlDbType.Int, "MessageType", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageFunction", SqlDbType.Int, "MessageFunction", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageContent", SqlDbType.NVarChar, "MessageContent", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedTime", SqlDbType.DateTime, "CreatedTime", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDeThongBao", SqlDbType.NVarChar, "TieuDeThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungThongBao", SqlDbType.NVarChar, "NoiDungThongBao", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, "ReferenceID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ItemID", SqlDbType.BigInt, "ItemID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageFrom", SqlDbType.VarChar, "MessageFrom", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTo", SqlDbType.VarChar, "MessageTo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageType", SqlDbType.Int, "MessageType", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageFunction", SqlDbType.Int, "MessageFunction", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageContent", SqlDbType.NVarChar, "MessageContent", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedTime", SqlDbType.DateTime, "CreatedTime", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDeThongBao", SqlDbType.NVarChar, "TieuDeThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungThongBao", SqlDbType.NVarChar, "NoiDungThongBao", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_Message Load(long id)
		{
			const string spName = "[dbo].[p_KDT_Message_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_Message> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_Message> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_Message> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_Message_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_Message_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_Message_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_Message_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_Message(Guid referenceID, long itemID, string messageFrom, string messageTo, int messageType, int messageFunction, string messageContent, DateTime createdTime, string tieuDeThongBao, string noiDungThongBao)
		{
			KDT_Message entity = new KDT_Message();	
			entity.ReferenceID = referenceID;
			entity.ItemID = itemID;
			entity.MessageFrom = messageFrom;
			entity.MessageTo = messageTo;
			entity.MessageType = messageType;
			entity.MessageFunction = messageFunction;
			entity.MessageContent = messageContent;
			entity.CreatedTime = createdTime;
			entity.TieuDeThongBao = tieuDeThongBao;
			entity.NoiDungThongBao = noiDungThongBao;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_Message_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, ReferenceID);
			db.AddInParameter(dbCommand, "@ItemID", SqlDbType.BigInt, ItemID);
			db.AddInParameter(dbCommand, "@MessageFrom", SqlDbType.VarChar, MessageFrom);
			db.AddInParameter(dbCommand, "@MessageTo", SqlDbType.VarChar, MessageTo);
			db.AddInParameter(dbCommand, "@MessageType", SqlDbType.Int, MessageType);
			db.AddInParameter(dbCommand, "@MessageFunction", SqlDbType.Int, MessageFunction);
			db.AddInParameter(dbCommand, "@MessageContent", SqlDbType.NVarChar, MessageContent);
			db.AddInParameter(dbCommand, "@CreatedTime", SqlDbType.DateTime, CreatedTime.Year <= 1753 ? DBNull.Value : (object) CreatedTime);
			db.AddInParameter(dbCommand, "@TieuDeThongBao", SqlDbType.NVarChar, TieuDeThongBao);
			db.AddInParameter(dbCommand, "@NoiDungThongBao", SqlDbType.NVarChar, NoiDungThongBao);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_Message> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_Message item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_Message(long id, Guid referenceID, long itemID, string messageFrom, string messageTo, int messageType, int messageFunction, string messageContent, DateTime createdTime, string tieuDeThongBao, string noiDungThongBao)
		{
			KDT_Message entity = new KDT_Message();			
			entity.ID = id;
			entity.ReferenceID = referenceID;
			entity.ItemID = itemID;
			entity.MessageFrom = messageFrom;
			entity.MessageTo = messageTo;
			entity.MessageType = messageType;
			entity.MessageFunction = messageFunction;
			entity.MessageContent = messageContent;
			entity.CreatedTime = createdTime;
			entity.TieuDeThongBao = tieuDeThongBao;
			entity.NoiDungThongBao = noiDungThongBao;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_Message_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, ReferenceID);
			db.AddInParameter(dbCommand, "@ItemID", SqlDbType.BigInt, ItemID);
			db.AddInParameter(dbCommand, "@MessageFrom", SqlDbType.VarChar, MessageFrom);
			db.AddInParameter(dbCommand, "@MessageTo", SqlDbType.VarChar, MessageTo);
			db.AddInParameter(dbCommand, "@MessageType", SqlDbType.Int, MessageType);
			db.AddInParameter(dbCommand, "@MessageFunction", SqlDbType.Int, MessageFunction);
			db.AddInParameter(dbCommand, "@MessageContent", SqlDbType.NVarChar, MessageContent);
			db.AddInParameter(dbCommand, "@CreatedTime", SqlDbType.DateTime, CreatedTime.Year <= 1753 ? DBNull.Value : (object) CreatedTime);
			db.AddInParameter(dbCommand, "@TieuDeThongBao", SqlDbType.NVarChar, TieuDeThongBao);
			db.AddInParameter(dbCommand, "@NoiDungThongBao", SqlDbType.NVarChar, NoiDungThongBao);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_Message> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_Message item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_Message(long id, Guid referenceID, long itemID, string messageFrom, string messageTo, int messageType, int messageFunction, string messageContent, DateTime createdTime, string tieuDeThongBao, string noiDungThongBao)
		{
			KDT_Message entity = new KDT_Message();			
			entity.ID = id;
			entity.ReferenceID = referenceID;
			entity.ItemID = itemID;
			entity.MessageFrom = messageFrom;
			entity.MessageTo = messageTo;
			entity.MessageType = messageType;
			entity.MessageFunction = messageFunction;
			entity.MessageContent = messageContent;
			entity.CreatedTime = createdTime;
			entity.TieuDeThongBao = tieuDeThongBao;
			entity.NoiDungThongBao = noiDungThongBao;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_Message_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, ReferenceID);
			db.AddInParameter(dbCommand, "@ItemID", SqlDbType.BigInt, ItemID);
			db.AddInParameter(dbCommand, "@MessageFrom", SqlDbType.VarChar, MessageFrom);
			db.AddInParameter(dbCommand, "@MessageTo", SqlDbType.VarChar, MessageTo);
			db.AddInParameter(dbCommand, "@MessageType", SqlDbType.Int, MessageType);
			db.AddInParameter(dbCommand, "@MessageFunction", SqlDbType.Int, MessageFunction);
			db.AddInParameter(dbCommand, "@MessageContent", SqlDbType.NVarChar, MessageContent);
			db.AddInParameter(dbCommand, "@CreatedTime", SqlDbType.DateTime, CreatedTime.Year <= 1753 ? DBNull.Value : (object) CreatedTime);
			db.AddInParameter(dbCommand, "@TieuDeThongBao", SqlDbType.NVarChar, TieuDeThongBao);
			db.AddInParameter(dbCommand, "@NoiDungThongBao", SqlDbType.NVarChar, NoiDungThongBao);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_Message> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_Message item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_Message(long id)
		{
			KDT_Message entity = new KDT_Message();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_Message_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_Message_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_Message> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_Message item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}