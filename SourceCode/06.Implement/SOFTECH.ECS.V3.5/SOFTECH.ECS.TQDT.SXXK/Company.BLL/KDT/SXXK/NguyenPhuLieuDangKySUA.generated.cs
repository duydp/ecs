using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class NguyenPhuLieuDangKySUA
	{
		#region Properties.
		
		public long ID { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public string MaHaiQuan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string MaDaiLy { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string GUIDSTR { set; get; }
		public string DeXuatKhac { set; get; }
		public string LyDoSua { set; get; }
		public int ActionStatus { set; get; }
		public string GuidReference { set; get; }
		public short NamDK { set; get; }
		public string HUONGDAN { set; get; }
		public string PhanLuong { set; get; }
		public string Huongdan_PL { set; get; }
		public long IDNPLDK { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static NguyenPhuLieuDangKySUA Load(long id)
		{
			const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			NguyenPhuLieuDangKySUA entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new NguyenPhuLieuDangKySUA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDNPLDK"))) entity.IDNPLDK = reader.GetInt64(reader.GetOrdinal("IDNPLDK"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<NguyenPhuLieuDangKySUA> SelectCollectionAll()
		{
			List<NguyenPhuLieuDangKySUA> collection = new List<NguyenPhuLieuDangKySUA>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				NguyenPhuLieuDangKySUA entity = new NguyenPhuLieuDangKySUA();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDNPLDK"))) entity.IDNPLDK = reader.GetInt64(reader.GetOrdinal("IDNPLDK"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<NguyenPhuLieuDangKySUA> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<NguyenPhuLieuDangKySUA> collection = new List<NguyenPhuLieuDangKySUA>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				NguyenPhuLieuDangKySUA entity = new NguyenPhuLieuDangKySUA();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDNPLDK"))) entity.IDNPLDK = reader.GetInt64(reader.GetOrdinal("IDNPLDK"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<NguyenPhuLieuDangKySUA> SelectCollectionBy_IDNPLDK(long iDNPLDK)
		{
			List<NguyenPhuLieuDangKySUA> collection = new List<NguyenPhuLieuDangKySUA>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_IDNPLDK(iDNPLDK);
			while (reader.Read())
			{
				NguyenPhuLieuDangKySUA entity = new NguyenPhuLieuDangKySUA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDNPLDK"))) entity.IDNPLDK = reader.GetInt64(reader.GetOrdinal("IDNPLDK"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_IDNPLDK(long iDNPLDK)
		{
			const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_SelectBy_IDNPLDK]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDNPLDK", SqlDbType.BigInt, iDNPLDK);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_IDNPLDK(long iDNPLDK)
		{
			const string spName = "p_KDT_SXXK_NguyenPhuLieuDangKySUA_SelectBy_IDNPLDK";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDNPLDK", SqlDbType.BigInt, iDNPLDK);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertNguyenPhuLieuDangKySUA(long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, string maDoanhNghiep, string maDaiLy, int trangThaiXuLy, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL, long iDNPLDK)
		{
			NguyenPhuLieuDangKySUA entity = new NguyenPhuLieuDangKySUA();	
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaDaiLy = maDaiLy;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			entity.IDNPLDK = iDNPLDK;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			db.AddInParameter(dbCommand, "@IDNPLDK", SqlDbType.BigInt, IDNPLDK);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<NguyenPhuLieuDangKySUA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NguyenPhuLieuDangKySUA item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateNguyenPhuLieuDangKySUA(long id, long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, string maDoanhNghiep, string maDaiLy, int trangThaiXuLy, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL, long iDNPLDK)
		{
			NguyenPhuLieuDangKySUA entity = new NguyenPhuLieuDangKySUA();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaDaiLy = maDaiLy;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			entity.IDNPLDK = iDNPLDK;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_NguyenPhuLieuDangKySUA_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			db.AddInParameter(dbCommand, "@IDNPLDK", SqlDbType.BigInt, IDNPLDK);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<NguyenPhuLieuDangKySUA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NguyenPhuLieuDangKySUA item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateNguyenPhuLieuDangKySUA(long id, long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, string maDoanhNghiep, string maDaiLy, int trangThaiXuLy, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL, long iDNPLDK)
		{
			NguyenPhuLieuDangKySUA entity = new NguyenPhuLieuDangKySUA();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaDaiLy = maDaiLy;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			entity.IDNPLDK = iDNPLDK;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			db.AddInParameter(dbCommand, "@IDNPLDK", SqlDbType.BigInt, IDNPLDK);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<NguyenPhuLieuDangKySUA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NguyenPhuLieuDangKySUA item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteNguyenPhuLieuDangKySUA(long id)
		{
			NguyenPhuLieuDangKySUA entity = new NguyenPhuLieuDangKySUA();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_IDNPLDK(long iDNPLDK)
		{
			const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_DeleteBy_IDNPLDK]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDNPLDK", SqlDbType.BigInt, iDNPLDK);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKySUA_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<NguyenPhuLieuDangKySUA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NguyenPhuLieuDangKySUA item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}