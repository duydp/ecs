﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.SXXK;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.Report
{
    public partial class PreviewForm : BaseForm
    {
        public BangKeToKhaiNhap BKTKN = new BangKeToKhaiNhap();
        public BangKeToKhaiXuat BKTKX = new BangKeToKhaiXuat();
        public BCNPLXuatNhapTon BCNPLXNT = new BCNPLXuatNhapTon();
        public Company.Interface.Report.SXXK.BCThueXNK BCThue = new Company.Interface.Report.SXXK.BCThueXNK();
        public int LanThanhLy;
        public int SoHoSo;
        public PreviewForm()
        {
            InitializeComponent();
        }

        private void explorerBar1_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "BKTKN":
                    printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                    this.BKTKN.CreateDocument();
                    break;
                case "BKTKX":
                    printControl1.PrintingSystem = this.BKTKX.PrintingSystem;
                    this.BKTKX.CreateDocument();
                    break;
                case "BCNPLXNT":
                    printControl1.PrintingSystem = this.BCNPLXNT.PrintingSystem;
                    this.BCNPLXNT.CreateDocument();
                    break;
                case "BCThueNK":
                    printControl1.PrintingSystem = this.BCThue.PrintingSystem;
                    this.BCThue.CreateDocument();
                    break;
            }
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {
            printPreviewBarItem4.Enabled = printPreviewBarItem5.Enabled = Program.isActivated; 
            if (GlobalSettings.GiaoDien.Id == "Office2003")
                this.defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            string where = "";
            try
            {
                this.BKTKN.LanThanhLy = this.LanThanhLy;
                this.BKTKN.SoHoSo = this.SoHoSo;
                this.BKTKN.BindReport(where);
                this.BKTKX.SoHoSo = this.SoHoSo;
                this.BKTKX.BindReport(this.LanThanhLy);
                this.BCNPLXNT.LanThanhLy = this.LanThanhLy;
                this.BCNPLXNT.SoHoSo = this.SoHoSo;
                this.BCNPLXNT.BindReport(where);
                this.BCThue.LanThanhLy = this.LanThanhLy;
                this.BCThue.SoHoSo = this.SoHoSo;
                this.BCThue.BindReport(where);
                printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                this.BKTKN.CreateDocument();
            }
            catch
            {
                ShowMessage("Có lỗi khi nạp dữ liệu báo cáo.",false);
            }
        }
    }
}