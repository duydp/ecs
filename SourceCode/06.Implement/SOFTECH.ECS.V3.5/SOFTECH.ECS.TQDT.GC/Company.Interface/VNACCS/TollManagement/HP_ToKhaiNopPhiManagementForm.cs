﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.VNACCS.TollManagement
{
    public partial class HP_ToKhaiNopPhiManagementForm : Company.Interface.BaseForm
    {
        public HP_ToKhaiNopPhiManagementForm()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgListVDN.Refetch();
                dgListVDN.DataSource = T_KDT_THUPHI_TOKHAI.SelectCollectionDynamic(GetSearchWhere(), "");
                dgListVDN.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string GetSearchWhere()
        {
            try
            {
                string where = " 1= 1 ";
                if (!String.IsNullOrEmpty(txtMa.Text.ToString()))
                    where += " AND MaDoanhNghiep ='" + txtMa.Text.ToString() + "'";
                if (!String.IsNullOrEmpty(txtSoToKhai.Text.ToString()))
                    where += " AND SoTK LIKE '%" + txtSoToKhai.Text.ToString() + "%'";
                if (!String.IsNullOrEmpty(txtSoCT.Text.ToString()))
                    where += " AND SoTKNP LIKE '%" + txtSoCT.Text.ToString() + "%'";
                if (!String.IsNullOrEmpty(cbbDDNP.Code.ToString()))
                    where += " AND DiemThuPhi ='" + cbbDDNP.Code.ToString() + "'";
                if (cbbLoaiHangHoa.SelectedValue !=null)
                    where += " AND LoaiHangHoa  ='" + cbbLoaiHangHoa.SelectedValue.ToString() + "'";
                if (cbbNhomLoaiHinh.SelectedValue !=null)
                    where += " AND NhomLoaiHinh  ='" + cbbNhomLoaiHinh.SelectedValue.ToString() + "'";
                if (cbStatus.SelectedValue !=null)
                    where += " AND TrangThaiXuLy  ='" + cbStatus.SelectedValue.ToString() + "'";
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1";
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }

        private void dgListVDN_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    T_KDT_THUPHI_TOKHAI tokhai = new T_KDT_THUPHI_TOKHAI();
                    tokhai = (T_KDT_THUPHI_TOKHAI)e.Row.DataRow;
                    HP_ToKhaiNopPhiForm f = new HP_ToKhaiNopPhiForm();
                    f.tokhai = tokhai;
                    f.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListVDN_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    int LoaiHangHoa = Convert.ToInt32(e.Row.Cells["LoaiHangHoa"].Value);
                    switch (LoaiHangHoa)
                    {
                        case 100:
                            e.Row.Cells["LoaiHangHoa"].Text = "Hàng Container";
                            break;
                        case 101:
                            e.Row.Cells["LoaiHangHoa"].Text = "Hàng rời , lỏng";
                            break;
                        default:
                            break;
                    }
                    if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiToKhaiNopPhi.DA_CO_THONG_BAO_NOP_PHI)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã có thông báo nộp phí";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiToKhaiNopPhi.CHUA_KHAI_BAO)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiToKhaiNopPhi.DA_CO_SO_TIEP_NHAN)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã có số tiếp nhận";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiToKhaiNopPhi.DUOC_CHAP_NHAN)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Được chấp nhận";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiToKhaiNopPhi.DA_THUC_HIEN_NOP_PHI)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã thực hiện nộp phí";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiToKhaiNopPhi.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
