using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACCS_Mapper
    {
        private List<VNACCS_Mapper> _listVNACC_Mapper = new List<VNACCS_Mapper>();

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    //if (this.ID == 0)
                        //this.ID = this.Insert();
                    //else
                        //this.Update();

                    //foreach (KDT_VNACC_ChungTuDinhKem_ChiTiet item in this.ChungTuDinhKemChiTietCollection)
                    //{
                    //    if (item.ID == 0)
                    //    {
                    //        item.ChungTuKemID = this.ID;
                    //        item.ID = item.Insert();
                    //    }
                    //    else
                    //    {
                    //        item.Update();
                    //    }
                    //}
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    // this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }
}