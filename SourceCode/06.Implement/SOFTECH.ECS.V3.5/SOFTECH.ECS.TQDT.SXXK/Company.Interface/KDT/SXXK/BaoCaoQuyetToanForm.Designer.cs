﻿namespace Company.Interface.KDT.SXXK
{
    partial class BaoCaoQuyetToanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaoCaoQuyetToanForm));
            Janus.Windows.GridEX.GridEXLayout dgTongNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgChiTietNPLXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTongNPLXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgNPLCU_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgNPLCU_Total_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListNPLXuatDetails_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTotal_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.clcNgayKetThucBC = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayBatDauBC = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayTN = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbbType = new Janus.Windows.EditControls.UIComboBox();
            this.cbbLoaiHinhBaoCao = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoTN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThaiXuLy = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnPrint = new Janus.Windows.EditControls.UIButton();
            this.btnProcess = new Janus.Windows.EditControls.UIButton();
            this.clcDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdAdd1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAdd");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdEdit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdEdit");
            this.cmdCancel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdSuaBCQT2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaBCQT");
            this.cmdFeedBack1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedBack");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedBack = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedBack");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdEdit");
            this.cmdAddExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdAddExcel");
            this.cmdAdd = new Janus.Windows.UI.CommandBars.UICommand("cmdAdd");
            this.cmdSuaBCQT = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaBCQT");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTongNPL = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgChiTietNPLXuat = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTongNPLXuat = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage6 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNPLCU = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage5 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNPLCU_Total = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage8 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListNPLXuatDetails = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage7 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListTotal = new Janus.Windows.GridEX.GridEX();
            this.cmdSuaBCQT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaBCQT");
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTongNPL)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgChiTietNPLXuat)).BeginInit();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTongNPLXuat)).BeginInit();
            this.uiTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCU)).BeginInit();
            this.uiTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCU_Total)).BeginInit();
            this.uiTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLXuatDetails)).BeginInit();
            this.uiTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 345), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 282);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 345);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 321);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 321);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(203, 282);
            this.grbMain.Size = new System.Drawing.Size(1042, 345);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Controls.Add(this.ctrCoQuanHaiQuan);
            this.uiGroupBox2.Controls.Add(this.linkLabel1);
            this.uiGroupBox2.Controls.Add(this.clcNgayKetThucBC);
            this.uiGroupBox2.Controls.Add(this.clcNgayBatDauBC);
            this.uiGroupBox2.Controls.Add(this.clcNgayTN);
            this.uiGroupBox2.Controls.Add(this.cbbType);
            this.uiGroupBox2.Controls.Add(this.cbbLoaiHinhBaoCao);
            this.uiGroupBox2.Controls.Add(this.txtSoTN);
            this.uiGroupBox2.Controls.Add(this.txtGhiChuKhac);
            this.uiGroupBox2.Controls.Add(this.lblTrangThaiXuLy);
            this.uiGroupBox2.Controls.Add(this.label17);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label18);
            this.uiGroupBox2.Controls.Add(this.label20);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label21);
            this.uiGroupBox2.Controls.Add(this.label22);
            this.uiGroupBox2.Controls.Add(this.label23);
            this.uiGroupBox2.Controls.Add(this.label24);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 32);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1248, 247);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.Text = "Thông tin khai báo";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ctrCoQuanHaiQuan
            // 
            this.ctrCoQuanHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrCoQuanHaiQuan.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCoQuanHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrCoQuanHaiQuan.Appearance.Options.UseFont = true;
            this.ctrCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrCoQuanHaiQuan.Code = "";
            this.ctrCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrCoQuanHaiQuan.IsOnlyWarning = false;
            this.ctrCoQuanHaiQuan.IsValidate = true;
            this.ctrCoQuanHaiQuan.Location = new System.Drawing.Point(148, 53);
            this.ctrCoQuanHaiQuan.Name = "ctrCoQuanHaiQuan";
            this.ctrCoQuanHaiQuan.Name_VN = "";
            this.ctrCoQuanHaiQuan.SetOnlyWarning = false;
            this.ctrCoQuanHaiQuan.SetValidate = false;
            this.ctrCoQuanHaiQuan.ShowColumnCode = true;
            this.ctrCoQuanHaiQuan.ShowColumnName = true;
            this.ctrCoQuanHaiQuan.Size = new System.Drawing.Size(336, 21);
            this.ctrCoQuanHaiQuan.TabIndex = 10;
            this.ctrCoQuanHaiQuan.TagCode = "";
            this.ctrCoQuanHaiQuan.TagName = "";
            this.ctrCoQuanHaiQuan.Where = null;
            this.ctrCoQuanHaiQuan.WhereCondition = "";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(145, 217);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(339, 16);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Mẫu số 15/BCQT-NVL/GSQL - Mẫu số 15a/BCQT-SP/GSQL";
            this.linkLabel1.Leave += new System.EventHandler(this.linkLabel1_Leave);
            // 
            // clcNgayKetThucBC
            // 
            this.clcNgayKetThucBC.CustomFormat = "dd/MM/yyyy";
            this.clcNgayKetThucBC.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayKetThucBC.DropDownCalendar.Name = "";
            this.clcNgayKetThucBC.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayKetThucBC.Location = new System.Drawing.Point(148, 121);
            this.clcNgayKetThucBC.Name = "clcNgayKetThucBC";
            this.clcNgayKetThucBC.Size = new System.Drawing.Size(158, 21);
            this.clcNgayKetThucBC.TabIndex = 5;
            this.clcNgayKetThucBC.Value = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            this.clcNgayKetThucBC.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayBatDauBC
            // 
            this.clcNgayBatDauBC.CustomFormat = "dd/MM/yyyy";
            this.clcNgayBatDauBC.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayBatDauBC.DropDownCalendar.Name = "";
            this.clcNgayBatDauBC.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayBatDauBC.Location = new System.Drawing.Point(148, 89);
            this.clcNgayBatDauBC.Name = "clcNgayBatDauBC";
            this.clcNgayBatDauBC.Size = new System.Drawing.Size(158, 21);
            this.clcNgayBatDauBC.TabIndex = 4;
            this.clcNgayBatDauBC.Value = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.clcNgayBatDauBC.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayTN
            // 
            this.clcNgayTN.CustomFormat = "dd/MM/yyyy";
            this.clcNgayTN.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayTN.DropDownCalendar.Name = "";
            this.clcNgayTN.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTN.Location = new System.Drawing.Point(401, 20);
            this.clcNgayTN.Name = "clcNgayTN";
            this.clcNgayTN.ReadOnly = true;
            this.clcNgayTN.Size = new System.Drawing.Size(158, 21);
            this.clcNgayTN.TabIndex = 2;
            this.clcNgayTN.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // cbbType
            // 
            this.cbbType.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbType.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "1: Bổ sung thông tin";
            uiComboBoxItem1.Value = "1";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "2: Sửa thông tin";
            uiComboBoxItem2.Value = "2";
            this.cbbType.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbType.Location = new System.Drawing.Point(540, 151);
            this.cbbType.Name = "cbbType";
            this.cbbType.Size = new System.Drawing.Size(145, 21);
            this.cbbType.TabIndex = 7;
            this.cbbType.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbLoaiHinhBaoCao
            // 
            this.cbbLoaiHinhBaoCao.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiHinhBaoCao.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbLoaiHinhBaoCao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "1: Nguyên liệu, vật liệu nhập khẩu ";
            uiComboBoxItem3.Value = "1";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "2. Thành phẩm được sản xuất từ nguồn nhập khẩu";
            uiComboBoxItem4.Value = "2";
            this.cbbLoaiHinhBaoCao.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbbLoaiHinhBaoCao.Location = new System.Drawing.Point(148, 151);
            this.cbbLoaiHinhBaoCao.Name = "cbbLoaiHinhBaoCao";
            this.cbbLoaiHinhBaoCao.Size = new System.Drawing.Size(303, 21);
            this.cbbLoaiHinhBaoCao.TabIndex = 6;
            this.cbbLoaiHinhBaoCao.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiHinhBaoCao.SelectedValueChanged += new System.EventHandler(this.cbbLoaiHinhBaoCao_SelectedValueChanged);
            // 
            // txtSoTN
            // 
            this.txtSoTN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTN.Location = new System.Drawing.Point(148, 20);
            this.txtSoTN.Name = "txtSoTN";
            this.txtSoTN.ReadOnly = true;
            this.txtSoTN.Size = new System.Drawing.Size(129, 21);
            this.txtSoTN.TabIndex = 1;
            this.txtSoTN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChuKhac
            // 
            this.txtGhiChuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChuKhac.Location = new System.Drawing.Point(148, 183);
            this.txtGhiChuKhac.Name = "txtGhiChuKhac";
            this.txtGhiChuKhac.Size = new System.Drawing.Size(835, 21);
            this.txtGhiChuKhac.TabIndex = 8;
            this.txtGhiChuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTrangThaiXuLy
            // 
            this.lblTrangThaiXuLy.AutoSize = true;
            this.lblTrangThaiXuLy.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThaiXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThaiXuLy.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThaiXuLy.Location = new System.Drawing.Point(650, 24);
            this.lblTrangThaiXuLy.Name = "lblTrangThaiXuLy";
            this.lblTrangThaiXuLy.Size = new System.Drawing.Size(76, 13);
            this.lblTrangThaiXuLy.TabIndex = 0;
            this.lblTrangThaiXuLy.Text = "Chưa khai báo";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(578, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Trạng thái : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(701, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Bắt buộc khi khai sửa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(469, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Loại sửa :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(305, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(90, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Ngày tiếp nhận : ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(22, 155);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Loại báo cáo :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ngày kết thúc báo cáo :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(22, 93);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(120, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Ngày bắt đầu báo cáo :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(22, 187);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Ghi chú :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(22, 57);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Hải quan :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(22, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(74, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Số tiếp nhận :";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnPrint);
            this.uiGroupBox1.Controls.Add(this.btnProcess);
            this.uiGroupBox1.Controls.Add(this.clcDenNgay);
            this.uiGroupBox1.Controls.Add(this.clcTuNgay);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 295);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1042, 50);
            this.uiGroupBox1.TabIndex = 6;
            this.uiGroupBox1.Text = "Xử lý dữ liệu";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(942, 19);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(84, 21);
            this.btnClose.TabIndex = 123;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageSize = new System.Drawing.Size(20, 20);
            this.btnPrint.Location = new System.Drawing.Point(629, 19);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(301, 21);
            this.btnPrint.TabIndex = 123;
            this.btnPrint.Text = "Mẫu số 15/BCQT-NVL/GSQL - 15a/BCQT-SP/GSQL - ";
            this.btnPrint.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnProcess.Image")));
            this.btnProcess.ImageSize = new System.Drawing.Size(20, 20);
            this.btnProcess.Location = new System.Drawing.Point(510, 19);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(113, 21);
            this.btnProcess.TabIndex = 123;
            this.btnProcess.Text = "Xử lý dữ liệu";
            this.btnProcess.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnProcess.VisualStyleManager = this.vsmMain;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // clcDenNgay
            // 
            this.clcDenNgay.CustomFormat = "dd/MM/yyyy";
            this.clcDenNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDenNgay.DropDownCalendar.Name = "";
            this.clcDenNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.Location = new System.Drawing.Point(336, 19);
            this.clcDenNgay.Name = "clcDenNgay";
            this.clcDenNgay.Size = new System.Drawing.Size(158, 21);
            this.clcDenNgay.TabIndex = 28;
            this.clcDenNgay.Value = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            this.clcDenNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.CustomFormat = "dd/MM/yyyy";
            this.clcTuNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcTuNgay.DropDownCalendar.Name = "";
            this.clcTuNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.Location = new System.Drawing.Point(91, 19);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.Size = new System.Drawing.Size(158, 21);
            this.clcTuNgay.TabIndex = 28;
            this.clcTuNgay.Value = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.clcTuNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Từ ngày :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(264, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Đến ngày :";
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSave,
            this.cmdFeedBack,
            this.cmdResult,
            this.cmdUpdateGuidString,
            this.cmdCancel,
            this.cmdEdit,
            this.cmdAddExcel,
            this.cmdAdd,
            this.cmdSuaBCQT});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("db3f14b4-f472-45b2-95e5-5df0dc360a22");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.LockCommandBars = true;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAdd1,
            this.cmdSave1,
            this.cmdSend1,
            this.cmdEdit1,
            this.cmdCancel1,
            this.cmdSuaBCQT2,
            this.cmdFeedBack1,
            this.cmdResult1,
            this.cmdUpdateGuidString1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1022, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdAdd1
            // 
            this.cmdAdd1.Key = "cmdAdd";
            this.cmdAdd1.Name = "cmdAdd1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdEdit1
            // 
            this.cmdEdit1.Key = "cmdEdit";
            this.cmdEdit1.Name = "cmdEdit1";
            // 
            // cmdCancel1
            // 
            this.cmdCancel1.Key = "cmdCancel";
            this.cmdCancel1.Name = "cmdCancel1";
            // 
            // cmdSuaBCQT2
            // 
            this.cmdSuaBCQT2.Key = "cmdSuaBCQT";
            this.cmdSuaBCQT2.Name = "cmdSuaBCQT2";
            // 
            // cmdFeedBack1
            // 
            this.cmdFeedBack1.Key = "cmdFeedBack";
            this.cmdFeedBack1.Name = "cmdFeedBack1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdUpdateGuidString1
            // 
            this.cmdUpdateGuidString1.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString1.Name = "cmdUpdateGuidString1";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu lại";
            // 
            // cmdFeedBack
            // 
            this.cmdFeedBack.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedBack.Image")));
            this.cmdFeedBack.Key = "cmdFeedBack";
            this.cmdFeedBack.Name = "cmdFeedBack";
            this.cmdFeedBack.Text = "Nhận phản hồi";
            // 
            // cmdResult
            // 
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdUpdateGuidString
            // 
            this.cmdUpdateGuidString.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidString.Image")));
            this.cmdUpdateGuidString.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Name = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Text = "Cập nhật chuỗi phản hồi";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Image = ((System.Drawing.Image)(resources.GetObject("cmdCancel.Image")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Text = "Khai báo hủy";
            // 
            // cmdEdit
            // 
            this.cmdEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdEdit.Image")));
            this.cmdEdit.Key = "cmdEdit";
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Text = "Khai báo sửa";
            // 
            // cmdAddExcel
            // 
            this.cmdAddExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdAddExcel.Image")));
            this.cmdAddExcel.IsEditableControl = Janus.Windows.UI.InheritableBoolean.True;
            this.cmdAddExcel.Key = "cmdAddExcel";
            this.cmdAddExcel.Name = "cmdAddExcel";
            this.cmdAddExcel.Text = "Nhập hàng hóa từ File Excel";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Image = ((System.Drawing.Image)(resources.GetObject("cmdAdd.Image")));
            this.cmdAdd.Key = "cmdAdd";
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Text = "Thêm hàng hóa";
            // 
            // cmdSuaBCQT
            // 
            this.cmdSuaBCQT.Image = ((System.Drawing.Image)(resources.GetObject("cmdSuaBCQT.Image")));
            this.cmdSuaBCQT.Key = "cmdSuaBCQT";
            this.cmdSuaBCQT.Name = "cmdSuaBCQT";
            this.cmdSuaBCQT.Text = "Chuyển Trạng thái sửa";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1248, 32);
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1042, 295);
            this.uiTab1.TabIndex = 7;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage3,
            this.uiTabPage2,
            this.uiTabPage4,
            this.uiTabPage6,
            this.uiTabPage5,
            this.uiTabPage8,
            this.uiTabPage7});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgList);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1129, 295);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.TabVisible = false;
            this.uiTabPage1.Text = "CHI TIẾT THEO TỜ KHAI";
            // 
            // dgList
            // 
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 5;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1129, 295);
            this.dgList.TabIndex = 1;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.dgTongNPL);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1058, 165);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.TabVisible = false;
            this.uiTabPage3.Text = "TỔNG NGUYÊN PHỤ LIỆU";
            // 
            // dgTongNPL
            // 
            this.dgTongNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgTongNPL_DesignTimeLayout.LayoutString = resources.GetString("dgTongNPL_DesignTimeLayout.LayoutString");
            this.dgTongNPL.DesignTimeLayout = dgTongNPL_DesignTimeLayout;
            this.dgTongNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTongNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTongNPL.FrozenColumns = 4;
            this.dgTongNPL.GroupByBoxVisible = false;
            this.dgTongNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTongNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTongNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTongNPL.Location = new System.Drawing.Point(0, 0);
            this.dgTongNPL.Name = "dgTongNPL";
            this.dgTongNPL.RecordNavigator = true;
            this.dgTongNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTongNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTongNPL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTongNPL.Size = new System.Drawing.Size(1058, 165);
            this.dgTongNPL.TabIndex = 2;
            this.dgTongNPL.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTongNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTongNPL.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.dgChiTietNPLXuat);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1058, 165);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.TabVisible = false;
            this.uiTabPage2.Text = "DANH SÁCH NGUYÊN PHỤ LIỆU XUẤT THEO ĐỊNH MỨC SẢN PHẨM";
            // 
            // dgChiTietNPLXuat
            // 
            this.dgChiTietNPLXuat.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgChiTietNPLXuat_DesignTimeLayout.LayoutString = resources.GetString("dgChiTietNPLXuat_DesignTimeLayout.LayoutString");
            this.dgChiTietNPLXuat.DesignTimeLayout = dgChiTietNPLXuat_DesignTimeLayout;
            this.dgChiTietNPLXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgChiTietNPLXuat.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgChiTietNPLXuat.FrozenColumns = 5;
            this.dgChiTietNPLXuat.GroupByBoxVisible = false;
            this.dgChiTietNPLXuat.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChiTietNPLXuat.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChiTietNPLXuat.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgChiTietNPLXuat.Location = new System.Drawing.Point(0, 0);
            this.dgChiTietNPLXuat.Name = "dgChiTietNPLXuat";
            this.dgChiTietNPLXuat.RecordNavigator = true;
            this.dgChiTietNPLXuat.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowPosition;
            this.dgChiTietNPLXuat.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgChiTietNPLXuat.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgChiTietNPLXuat.Size = new System.Drawing.Size(1058, 165);
            this.dgChiTietNPLXuat.TabIndex = 2;
            this.dgChiTietNPLXuat.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgChiTietNPLXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgChiTietNPLXuat.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.dgTongNPLXuat);
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1058, 165);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.TabVisible = false;
            this.uiTabPage4.Text = "TỔNG NGUYÊN PHỤ LIỆU XUẤT";
            // 
            // dgTongNPLXuat
            // 
            this.dgTongNPLXuat.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTongNPLXuat.ColumnAutoResize = true;
            dgTongNPLXuat_DesignTimeLayout.LayoutString = resources.GetString("dgTongNPLXuat_DesignTimeLayout.LayoutString");
            this.dgTongNPLXuat.DesignTimeLayout = dgTongNPLXuat_DesignTimeLayout;
            this.dgTongNPLXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTongNPLXuat.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTongNPLXuat.FrozenColumns = 5;
            this.dgTongNPLXuat.GroupByBoxVisible = false;
            this.dgTongNPLXuat.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTongNPLXuat.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTongNPLXuat.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTongNPLXuat.Location = new System.Drawing.Point(0, 0);
            this.dgTongNPLXuat.Name = "dgTongNPLXuat";
            this.dgTongNPLXuat.RecordNavigator = true;
            this.dgTongNPLXuat.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowPosition;
            this.dgTongNPLXuat.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTongNPLXuat.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTongNPLXuat.Size = new System.Drawing.Size(1058, 165);
            this.dgTongNPLXuat.TabIndex = 3;
            this.dgTongNPLXuat.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTongNPLXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTongNPLXuat.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage6
            // 
            this.uiTabPage6.Controls.Add(this.dgNPLCU);
            this.uiTabPage6.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage6.Name = "uiTabPage6";
            this.uiTabPage6.Size = new System.Drawing.Size(1058, 165);
            this.uiTabPage6.TabStop = true;
            this.uiTabPage6.TabVisible = false;
            this.uiTabPage6.Text = "NGUYÊN PHỤ LIỆU CUNG ỨNG";
            // 
            // dgNPLCU
            // 
            this.dgNPLCU.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNPLCU.ColumnAutoResize = true;
            dgNPLCU_DesignTimeLayout.LayoutString = resources.GetString("dgNPLCU_DesignTimeLayout.LayoutString");
            this.dgNPLCU.DesignTimeLayout = dgNPLCU_DesignTimeLayout;
            this.dgNPLCU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNPLCU.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNPLCU.FrozenColumns = 5;
            this.dgNPLCU.GroupByBoxVisible = false;
            this.dgNPLCU.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCU.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCU.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNPLCU.Location = new System.Drawing.Point(0, 0);
            this.dgNPLCU.Name = "dgNPLCU";
            this.dgNPLCU.RecordNavigator = true;
            this.dgNPLCU.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNPLCU.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCU.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNPLCU.Size = new System.Drawing.Size(1058, 165);
            this.dgNPLCU.TabIndex = 5;
            this.dgNPLCU.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCU.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgNPLCU.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage5
            // 
            this.uiTabPage5.Controls.Add(this.dgNPLCU_Total);
            this.uiTabPage5.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage5.Name = "uiTabPage5";
            this.uiTabPage5.Size = new System.Drawing.Size(1058, 165);
            this.uiTabPage5.TabStop = true;
            this.uiTabPage5.TabVisible = false;
            this.uiTabPage5.Text = "TỔNG NPL CUNG ỨNG";
            // 
            // dgNPLCU_Total
            // 
            this.dgNPLCU_Total.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNPLCU_Total.ColumnAutoResize = true;
            dgNPLCU_Total_DesignTimeLayout.LayoutString = resources.GetString("dgNPLCU_Total_DesignTimeLayout.LayoutString");
            this.dgNPLCU_Total.DesignTimeLayout = dgNPLCU_Total_DesignTimeLayout;
            this.dgNPLCU_Total.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNPLCU_Total.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNPLCU_Total.FrozenColumns = 5;
            this.dgNPLCU_Total.GroupByBoxVisible = false;
            this.dgNPLCU_Total.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCU_Total.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCU_Total.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNPLCU_Total.Location = new System.Drawing.Point(0, 0);
            this.dgNPLCU_Total.Name = "dgNPLCU_Total";
            this.dgNPLCU_Total.RecordNavigator = true;
            this.dgNPLCU_Total.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCU_Total.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNPLCU_Total.Size = new System.Drawing.Size(1058, 165);
            this.dgNPLCU_Total.TabIndex = 4;
            this.dgNPLCU_Total.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCU_Total.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgNPLCU_Total.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage8
            // 
            this.uiTabPage8.Controls.Add(this.dgListNPLXuatDetails);
            this.uiTabPage8.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage8.Name = "uiTabPage8";
            this.uiTabPage8.Size = new System.Drawing.Size(1040, 273);
            this.uiTabPage8.TabStop = true;
            this.uiTabPage8.Text = "CHI TIẾT NGUYÊN PHỤ LIỆU XUẤT THEO THANH KHOẢN (SỐ LIỆU THEO DÕI KHÔNG GỬI LÊN HQ" +
                ")";
            // 
            // dgListNPLXuatDetails
            // 
            this.dgListNPLXuatDetails.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPLXuatDetails.AlternatingColors = true;
            this.dgListNPLXuatDetails.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPLXuatDetails.ColumnAutoResize = true;
            dgListNPLXuatDetails_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLXuatDetails_DesignTimeLayout.LayoutString");
            this.dgListNPLXuatDetails.DesignTimeLayout = dgListNPLXuatDetails_DesignTimeLayout;
            this.dgListNPLXuatDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPLXuatDetails.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLXuatDetails.FrozenColumns = 3;
            this.dgListNPLXuatDetails.GroupByBoxVisible = false;
            this.dgListNPLXuatDetails.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLXuatDetails.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLXuatDetails.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPLXuatDetails.Location = new System.Drawing.Point(0, 0);
            this.dgListNPLXuatDetails.Margin = new System.Windows.Forms.Padding(0);
            this.dgListNPLXuatDetails.Name = "dgListNPLXuatDetails";
            this.dgListNPLXuatDetails.RecordNavigator = true;
            this.dgListNPLXuatDetails.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPLXuatDetails.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLXuatDetails.Size = new System.Drawing.Size(1040, 273);
            this.dgListNPLXuatDetails.TabIndex = 12;
            this.dgListNPLXuatDetails.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage7
            // 
            this.uiTabPage7.Controls.Add(this.dgListTotal);
            this.uiTabPage7.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage7.Name = "uiTabPage7";
            this.uiTabPage7.Size = new System.Drawing.Size(1040, 273);
            this.uiTabPage7.TabStop = true;
            this.uiTabPage7.Text = "TỔNG HỢP SỐ LIỆU BÁO CÁO (SỐ LIỆU GỬI LÊN HQ)";
            // 
            // dgListTotal
            // 
            this.dgListTotal.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotal.AlternatingColors = true;
            this.dgListTotal.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotal.ColumnAutoResize = true;
            dgListTotal_DesignTimeLayout.LayoutString = resources.GetString("dgListTotal_DesignTimeLayout.LayoutString");
            this.dgListTotal.DesignTimeLayout = dgListTotal_DesignTimeLayout;
            this.dgListTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotal.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTotal.FrozenColumns = 3;
            this.dgListTotal.GroupByBoxVisible = false;
            this.dgListTotal.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotal.Location = new System.Drawing.Point(0, 0);
            this.dgListTotal.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTotal.Name = "dgListTotal";
            this.dgListTotal.RecordNavigator = true;
            this.dgListTotal.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotal.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotal.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTotal.Size = new System.Drawing.Size(1040, 273);
            this.dgListTotal.TabIndex = 11;
            this.dgListTotal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTotal.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListTotal_RowDoubleClick);
            // 
            // cmdSuaBCQT1
            // 
            this.cmdSuaBCQT1.Key = "cmdSuaBCQT";
            this.cmdSuaBCQT1.Name = "cmdSuaBCQT1";
            // 
            // BaoCaoQuyetToanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1248, 630);
            this.Controls.Add(this.uiGroupBox2);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BaoCaoQuyetToanForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Báo cáo quyết toán sản xuất xuất khẩu";
            this.Load += new System.EventHandler(this.BaoCaoQuyetToanForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiGroupBox2, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTongNPL)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgChiTietNPLXuat)).EndInit();
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTongNPLXuat)).EndInit();
            this.uiTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCU)).EndInit();
            this.uiTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCU_Total)).EndInit();
            this.uiTabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLXuatDetails)).EndInit();
            this.uiTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHinhBaoCao;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTN;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuKhac;
        private System.Windows.Forms.Label lblTrangThaiXuLy;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayBatDauBC;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTN;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayKetThucBC;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDenNgay;
        private Janus.Windows.CalendarCombo.CalendarCombo clcTuNgay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIButton btnProcess;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnPrint;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedBack1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedBack;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.UI.CommandBars.UICommand cmdEdit;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddExcel;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.GridEX.GridEX dgTongNPL;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.GridEX dgChiTietNPLXuat;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.GridEX.GridEX dgTongNPLXuat;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage6;
        private Janus.Windows.GridEX.GridEX dgNPLCU;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage5;
        private Janus.Windows.GridEX.GridEX dgNPLCU_Total;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage8;
        private Janus.Windows.GridEX.GridEX dgListNPLXuatDetails;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage7;
        private Janus.Windows.GridEX.GridEX dgListTotal;
        private Janus.Windows.EditControls.UIComboBox cbbType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private Janus.Windows.UI.CommandBars.UICommand cmdAdd;
        private Janus.Windows.UI.CommandBars.UICommand cmdAdd1;
        private Janus.Windows.UI.CommandBars.UICommand cmdEdit1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaBCQT;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaBCQT2;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaBCQT1;
    }
}