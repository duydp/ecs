﻿using System;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using System.Collections.Generic;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface
{
    public partial class ToKhaiMauDichDaDuyetForm : BaseForm
    {
        private ToKhaiMauDichCollection _tkmdCollection = new ToKhaiMauDichCollection();
        private List<ToKhaiChuyenTiep> _tkctCollection = new List<ToKhaiChuyenTiep>();
        public long IdHopDong;
        public ToKhaiMauDich Tkmd;
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep Tkct;
        public ToKhaiMauDichDaDuyetForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------


        private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Text = DateTime.Now.Year.ToString();
            try
            {
                cbStatus.SelectedIndexChanged -= CbStatusSelectedIndexChanged;
                cbLoaiTK.SelectedIndexChanged -= CbLoaiTkSelectedIndexChanged;

                cbStatus.SelectedIndex = 1;
                cbLoaiTK.SelectedIndex = 0;                
                Search();
                cbStatus.SelectedIndexChanged += CbStatusSelectedIndexChanged;
                cbLoaiTK.SelectedIndexChanged += CbLoaiTkSelectedIndexChanged;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private ToKhaiMauDich GetTkmdbyId(long id)
        {
            foreach (var tk in _tkmdCollection)
            {
                if (tk.ID == id) return tk;
            }
            return null;
        }

        private void DgListRowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                if (_tkmdCollection.Count > 0)
                {
                    Tkmd = new ToKhaiMauDich();
                    Tkmd = GetTkmdbyId(id);
                    Tkct = null;
                }
                else
                {
                    Tkct = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep {ID = id};
                    Tkct = Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.Load(id);
                    Tkmd = null;
                }
                Close();
            }
        }

        //-----------------------------------------------------------------------------------------
        //Tìm kiếm dữ liệu.
        
        private void Search()
        {
            // Xây dựng điều kiện tìm kiếm.

            string where = "1=1 ";
            if (cbLoaiTK.SelectedValue.ToString() != "0")
            {
                where += " and MaLoaiHinh LIKE 'XGC%' ";
                where += " AND LoaiHangHoa = 'S'";
                //where += " and MaHaiQuan ='" + GlobalS1ettings.MA_HAI_QUAN + "' ";
            }
            else
            {
                where += " and MaLoaiHinh = 'PHSPX' ";
                //where += " and MaHaiQuanTiepNhan ='" + GlobalS1ettings.MA_HAI_QUAN + "' ";
            }
            where += string.Format(" AND MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan = " + txtSoTiepNhan.Value;
            }


            if (txtNamTiepNhan.TextLength > 0 && cbStatus.SelectedValue.ToString() != "-1")
            {
                where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
            }
            where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;
            where += " and IDHopDong=" + IdHopDong;
            // Thực hiện tìm kiếm.     
            if (cbLoaiTK.SelectedValue.ToString() != "0")
            {
                
                _tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "");
                dgList.DataSource = _tkmdCollection;
                _tkctCollection.Clear();
            }
            else
            {
                _tkmdCollection.Clear();
                
                _tkctCollection = Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic(where, "");
                dgList.DataSource = _tkctCollection;
            }




            //this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }


        private void BtnSearchClick(object sender, EventArgs e)
        {
            Search();
        }

        private void DgListLoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaLoaiHinh"].Text = LoaiHinhMauDich_GetName(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                if (e.Row.Cells["NgayTiepNhan"].Text != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }
            }
        }

        private void CbStatusSelectedIndexChanged(object sender, EventArgs e)
        {
            Search();
        }

        private void CbLoaiTkSelectedIndexChanged(object sender, EventArgs e)
        {
            Search();
        }
    }
}