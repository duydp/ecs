﻿namespace Company.Interface.Controls
{
    partial class DonViTinhQuyDoiControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.txtTyLeQuyDoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.cmbDonViTinh = new Janus.Windows.EditControls.UIComboBox();
            this.chkIsEnable = new Janus.Windows.EditControls.UICheckBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTyLeQuyDoi
            // 
            this.txtTyLeQuyDoi.DecimalDigits = 4;
            this.txtTyLeQuyDoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyLeQuyDoi.FormatString = "#####";
            this.txtTyLeQuyDoi.Location = new System.Drawing.Point(112, 51);
            this.txtTyLeQuyDoi.MaxLength = 10;
            this.txtTyLeQuyDoi.Name = "txtTyLeQuyDoi";
            this.txtTyLeQuyDoi.Size = new System.Drawing.Size(92, 20);
            this.txtTyLeQuyDoi.TabIndex = 3;
            this.txtTyLeQuyDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.txtTyLeQuyDoi.Value = 0;
            this.txtTyLeQuyDoi.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTyLeQuyDoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTyLeQuyDoi.Leave += new System.EventHandler(this.cmbDonViTinh_Leave);
            // 
            // cmbDonViTinh
            // 
            this.cmbDonViTinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbDonViTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Chưa khai báo";
            uiComboBoxItem1.Value = -1;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.FormatStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Chờ duyệt";
            uiComboBoxItem2.Value = 0;
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Đã duyệt";
            uiComboBoxItem3.Value = 1;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Không phê duyệt";
            uiComboBoxItem4.Value = "2";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Sửa tờ khai";
            uiComboBoxItem5.Value = "5";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Đã hủy";
            uiComboBoxItem6.Value = "10";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Chờ hủy";
            uiComboBoxItem7.Value = "11";
            this.cmbDonViTinh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7});
            this.cmbDonViTinh.Location = new System.Drawing.Point(112, 24);
            this.cmbDonViTinh.Name = "cmbDonViTinh";
            this.cmbDonViTinh.Size = new System.Drawing.Size(92, 21);
            this.cmbDonViTinh.TabIndex = 4;
            this.cmbDonViTinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmbDonViTinh.Leave += new System.EventHandler(this.cmbDonViTinh_Leave);
            // 
            // chkIsEnable
            // 
            this.chkIsEnable.BackColor = System.Drawing.Color.Transparent;
            this.chkIsEnable.Location = new System.Drawing.Point(0, -4);
            this.chkIsEnable.Name = "chkIsEnable";
            this.chkIsEnable.Size = new System.Drawing.Size(204, 23);
            this.chkIsEnable.TabIndex = 5;
            this.chkIsEnable.Text = "Sử dụng đơn vị tính khác để khai báo";
            this.chkIsEnable.CheckedChanged += new System.EventHandler(this.chkIsEnable_CheckedChanged);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.chkIsEnable);
            this.uiGroupBox1.Controls.Add(this.cmbDonViTinh);
            this.uiGroupBox1.Controls.Add(this.txtTyLeQuyDoi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(213, 86);
            this.uiGroupBox1.TabIndex = 6;
            this.uiGroupBox1.Text = "                                                                  ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Tỷ lệ quy đổi";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Đơn vị tính khai báo";
            // 
            // DonViTinhQuyDoiControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "DonViTinhQuyDoiControl";
            this.Size = new System.Drawing.Size(213, 86);
            this.Load += new System.EventHandler(this.DonViTinhQuyDoiControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTyLeQuyDoi;
        private Janus.Windows.EditControls.UIComboBox cmbDonViTinh;
        private Janus.Windows.EditControls.UICheckBox chkIsEnable;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}
