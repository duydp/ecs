﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.VNACCS.TollManagement
{
    public partial class HP_ThongBaoNoPhiForm : Company.Interface.BaseForm
    {
        public T_KDT_THUPHI_THONGBAO TB = new T_KDT_THUPHI_THONGBAO();
        public List<T_KDT_THUPHI_THONGBAO> TBCollection = new List<T_KDT_THUPHI_THONGBAO>();
        public T_KDT_THUPHI_TOKHAI TK = new T_KDT_THUPHI_TOKHAI();
        public HP_ThongBaoNoPhiForm()
        {
            InitializeComponent();
        }

        private void HP_ThongBaoNoPhiForm_Load(object sender, EventArgs e)
        {
            try
            {
                TBCollection = T_KDT_THUPHI_THONGBAO.SelectCollectionBy_TK_ID(TK.ID);
                TB = TBCollection[0];
                TB.PhiCollection = T_KDT_THUPHI_THONGBAO_PHI.SelectCollectionBy_TB_ID(TB.ID);
                TB.FileCollection = T_KDT_THUPHI_THONGBAO_FILE.SelectCollectionBy_TB_ID(TB.ID);
                txtSoCT.Text = TK.SoTKNP;
                clcNgayCT.Value = TK.NgayTKNP;
                txtSoTB.Text = TB.SoThongBao;
                clcNgayTB.Value = TB.NgayThongBao;
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgListPhi.Refresh();
                dgListPhi.DataSource = TB.PhiCollection;
                dgListPhi.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnViewFile_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                string path = Application.StartupPath + "\\Temp";
                System.IO.FileStream fs;
                byte[] bytes;
                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                T_KDT_THUPHI_THONGBAO_FILE fileData = TB.FileCollection[0];
                string fileName = path + "\\" + fileData.TenFile;

                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
                fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                bytes = System.Convert.FromBase64String(fileData.Content);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
                System.Diagnostics.Process.Start(fileName);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
