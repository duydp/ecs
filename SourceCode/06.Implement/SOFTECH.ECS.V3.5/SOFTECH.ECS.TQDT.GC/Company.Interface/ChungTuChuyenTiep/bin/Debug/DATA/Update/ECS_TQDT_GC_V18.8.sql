
/****** Object:  Table [dbo].[t_KDT_SXXK_PhuKienDangKy]    Script Date: 07/28/2014 15:03:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_PhuKienDangKy]') AND type in (N'U'))
Begin
CREATE TABLE [dbo].[t_KDT_SXXK_PhuKienDangKy](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoTiepNhan] [bigint] NULL,
	[NgayTiepNhan] [datetime] NULL,
	[TKMD_ID] [bigint] NULL,
	[SoToKhai] [varchar](50) NULL,
	[NgayDangKy] [datetime] NULL,
	[MaLoaiHinh] [varchar](6) NULL,
	[TrangThaiXuLy] [int] NOT NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[MaDoanhNghiep] [varchar](14) NULL,
	[SoPhuKien] [varchar](50) NOT NULL,
	[NgayPhuKien] [datetime] NOT NULL,
	[DeXuatKhac] [nvarchar](2000) NULL,
	[NguoiDuyet] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](200) NULL,
	[LoaiPhuKien] [int] NOT NULL,
	[GUIDSTR] [nvarchar](500) NULL,
 CONSTRAINT [PK_t_KDT_SXXK_PhuKienDangKy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end 
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[t_KDT_SXXK_PhuKienDetail]    Script Date: 07/28/2014 15:04:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_PhuKienDetail]') AND type in (N'U'))
Begin
CREATE TABLE [dbo].[t_KDT_SXXK_PhuKienDetail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[NgayTKHeThong] [datetime] NULL,
	[NgayTKSuaDoi] [datetime] NULL,
	[SoHopDongCu] [varchar](80) NULL,
	[SoHopDongMoi] [varchar](80) NULL,
	[NgayHopDongCu] [datetime] NULL,
	[NgayHopDongMoi] [datetime] NULL,
	[STTHang] [int] NULL,
	[MaHangCu] [varchar](50) NULL,
	[LoaiHangCu] [varchar](2) NULL,
	[SoLuongCu] [numeric](18, 4) NULL,
	[DVTCu] [varchar](10) NULL,
	[MaHQDangKyCu] [varchar](6) NULL,
	[MaHangMoi] [varchar](50) NULL,
	[LoaiHangMoi] [varchar](2) NULL,
	[SoLuongMoi] [numeric](18, 4) NULL,
	[DVTMoi] [varchar](10) NULL,
	[MaHQDangKyMoi] [varchar](6) NULL,
 CONSTRAINT [PK_t_KDT_SXXK_PhuKienDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[t_KDT_SXXK_LoaiPhuKien]    Script Date: 07/28/2014 15:04:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_LoaiPhuKien]') AND type in (N'U'))
Begin
CREATE TABLE [dbo].[t_KDT_SXXK_LoaiPhuKien](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MaPhuKien] [char](4) NOT NULL,
	[NoiDung] [nvarchar](255) NULL,
	[ThongTinMoi] [varchar](50) NULL,
	[Master_ID] [bigint] NULL,
	[ThongTinCu] [varchar](50) NULL,
 CONSTRAINT [PK_t_KDT_SXXK_LoaiPhuKien] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 28, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@SoToKhai varchar(50),
	@NgayDangKy datetime,
	@MaLoaiHinh varchar(6),
	@TrangThaiXuLy int,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(14),
	@SoPhuKien varchar(50),
	@NgayPhuKien datetime,
	@DeXuatKhac nvarchar(2000),
	@NguoiDuyet nvarchar(50),
	@GhiChu nvarchar(200),
	@LoaiPhuKien int,
	@GUIDSTR nvarchar(500),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_PhuKienDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[SoToKhai],
	[NgayDangKy],
	[MaLoaiHinh],
	[TrangThaiXuLy],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[SoPhuKien],
	[NgayPhuKien],
	[DeXuatKhac],
	[NguoiDuyet],
	[GhiChu],
	[LoaiPhuKien],
	[GUIDSTR]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@TKMD_ID,
	@SoToKhai,
	@NgayDangKy,
	@MaLoaiHinh,
	@TrangThaiXuLy,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@SoPhuKien,
	@NgayPhuKien,
	@DeXuatKhac,
	@NguoiDuyet,
	@GhiChu,
	@LoaiPhuKien,
	@GUIDSTR
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDangKy_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 28, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@SoToKhai varchar(50),
	@NgayDangKy datetime,
	@MaLoaiHinh varchar(6),
	@TrangThaiXuLy int,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(14),
	@SoPhuKien varchar(50),
	@NgayPhuKien datetime,
	@DeXuatKhac nvarchar(2000),
	@NguoiDuyet nvarchar(50),
	@GhiChu nvarchar(200),
	@LoaiPhuKien int,
	@GUIDSTR nvarchar(500)
AS

UPDATE
	[dbo].[t_KDT_SXXK_PhuKienDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TKMD_ID] = @TKMD_ID,
	[SoToKhai] = @SoToKhai,
	[NgayDangKy] = @NgayDangKy,
	[MaLoaiHinh] = @MaLoaiHinh,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[SoPhuKien] = @SoPhuKien,
	[NgayPhuKien] = @NgayPhuKien,
	[DeXuatKhac] = @DeXuatKhac,
	[NguoiDuyet] = @NguoiDuyet,
	[GhiChu] = @GhiChu,
	[LoaiPhuKien] = @LoaiPhuKien,
	[GUIDSTR] = @GUIDSTR
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 28, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@SoToKhai varchar(50),
	@NgayDangKy datetime,
	@MaLoaiHinh varchar(6),
	@TrangThaiXuLy int,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(14),
	@SoPhuKien varchar(50),
	@NgayPhuKien datetime,
	@DeXuatKhac nvarchar(2000),
	@NguoiDuyet nvarchar(50),
	@GhiChu nvarchar(200),
	@LoaiPhuKien int,
	@GUIDSTR nvarchar(500)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_PhuKienDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_PhuKienDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TKMD_ID] = @TKMD_ID,
			[SoToKhai] = @SoToKhai,
			[NgayDangKy] = @NgayDangKy,
			[MaLoaiHinh] = @MaLoaiHinh,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[SoPhuKien] = @SoPhuKien,
			[NgayPhuKien] = @NgayPhuKien,
			[DeXuatKhac] = @DeXuatKhac,
			[NguoiDuyet] = @NguoiDuyet,
			[GhiChu] = @GhiChu,
			[LoaiPhuKien] = @LoaiPhuKien,
			[GUIDSTR] = @GUIDSTR
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_PhuKienDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[TKMD_ID],
			[SoToKhai],
			[NgayDangKy],
			[MaLoaiHinh],
			[TrangThaiXuLy],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[SoPhuKien],
			[NgayPhuKien],
			[DeXuatKhac],
			[NguoiDuyet],
			[GhiChu],
			[LoaiPhuKien],
			[GUIDSTR]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@TKMD_ID,
			@SoToKhai,
			@NgayDangKy,
			@MaLoaiHinh,
			@TrangThaiXuLy,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@SoPhuKien,
			@NgayPhuKien,
			@DeXuatKhac,
			@NguoiDuyet,
			@GhiChu,
			@LoaiPhuKien,
			@GUIDSTR
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDangKy_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 28, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_PhuKienDangKy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDangKy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 28, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_PhuKienDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDangKy_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 28, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[SoToKhai],
	[NgayDangKy],
	[MaLoaiHinh],
	[TrangThaiXuLy],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[SoPhuKien],
	[NgayPhuKien],
	[DeXuatKhac],
	[NguoiDuyet],
	[GhiChu],
	[LoaiPhuKien],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_SXXK_PhuKienDangKy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 28, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[SoToKhai],
	[NgayDangKy],
	[MaLoaiHinh],
	[TrangThaiXuLy],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[SoPhuKien],
	[NgayPhuKien],
	[DeXuatKhac],
	[NguoiDuyet],
	[GhiChu],
	[LoaiPhuKien],
	[GUIDSTR]
FROM [dbo].[t_KDT_SXXK_PhuKienDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 28, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[SoToKhai],
	[NgayDangKy],
	[MaLoaiHinh],
	[TrangThaiXuLy],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[SoPhuKien],
	[NgayPhuKien],
	[DeXuatKhac],
	[NguoiDuyet],
	[GhiChu],
	[LoaiPhuKien],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_SXXK_PhuKienDangKy]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Insert]
	@Master_ID bigint,
	@NgayTKHeThong datetime,
	@NgayTKSuaDoi datetime,
	@SoHopDongCu varchar(80),
	@SoHopDongMoi varchar(80),
	@NgayHopDongCu datetime,
	@NgayHopDongMoi datetime,
	@STTHang int,
	@MaHangCu varchar(50),
	@LoaiHangCu varchar(2),
	@SoLuongCu numeric(18, 4),
	@DVTCu varchar(10),
	@MaHQDangKyCu varchar(6),
	@MaHangMoi varchar(50),
	@LoaiHangMoi varchar(2),
	@SoLuongMoi numeric(18, 4),
	@DVTMoi varchar(10),
	@MaHQDangKyMoi varchar(6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_PhuKienDetail]
(
	[Master_ID],
	[NgayTKHeThong],
	[NgayTKSuaDoi],
	[SoHopDongCu],
	[SoHopDongMoi],
	[NgayHopDongCu],
	[NgayHopDongMoi],
	[STTHang],
	[MaHangCu],
	[LoaiHangCu],
	[SoLuongCu],
	[DVTCu],
	[MaHQDangKyCu],
	[MaHangMoi],
	[LoaiHangMoi],
	[SoLuongMoi],
	[DVTMoi],
	[MaHQDangKyMoi]
)
VALUES 
(
	@Master_ID,
	@NgayTKHeThong,
	@NgayTKSuaDoi,
	@SoHopDongCu,
	@SoHopDongMoi,
	@NgayHopDongCu,
	@NgayHopDongMoi,
	@STTHang,
	@MaHangCu,
	@LoaiHangCu,
	@SoLuongCu,
	@DVTCu,
	@MaHQDangKyCu,
	@MaHangMoi,
	@LoaiHangMoi,
	@SoLuongMoi,
	@DVTMoi,
	@MaHQDangKyMoi
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Update]
	@ID bigint,
	@Master_ID bigint,
	@NgayTKHeThong datetime,
	@NgayTKSuaDoi datetime,
	@SoHopDongCu varchar(80),
	@SoHopDongMoi varchar(80),
	@NgayHopDongCu datetime,
	@NgayHopDongMoi datetime,
	@STTHang int,
	@MaHangCu varchar(50),
	@LoaiHangCu varchar(2),
	@SoLuongCu numeric(18, 4),
	@DVTCu varchar(10),
	@MaHQDangKyCu varchar(6),
	@MaHangMoi varchar(50),
	@LoaiHangMoi varchar(2),
	@SoLuongMoi numeric(18, 4),
	@DVTMoi varchar(10),
	@MaHQDangKyMoi varchar(6)
AS

UPDATE
	[dbo].[t_KDT_SXXK_PhuKienDetail]
SET
	[Master_ID] = @Master_ID,
	[NgayTKHeThong] = @NgayTKHeThong,
	[NgayTKSuaDoi] = @NgayTKSuaDoi,
	[SoHopDongCu] = @SoHopDongCu,
	[SoHopDongMoi] = @SoHopDongMoi,
	[NgayHopDongCu] = @NgayHopDongCu,
	[NgayHopDongMoi] = @NgayHopDongMoi,
	[STTHang] = @STTHang,
	[MaHangCu] = @MaHangCu,
	[LoaiHangCu] = @LoaiHangCu,
	[SoLuongCu] = @SoLuongCu,
	[DVTCu] = @DVTCu,
	[MaHQDangKyCu] = @MaHQDangKyCu,
	[MaHangMoi] = @MaHangMoi,
	[LoaiHangMoi] = @LoaiHangMoi,
	[SoLuongMoi] = @SoLuongMoi,
	[DVTMoi] = @DVTMoi,
	[MaHQDangKyMoi] = @MaHQDangKyMoi
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@NgayTKHeThong datetime,
	@NgayTKSuaDoi datetime,
	@SoHopDongCu varchar(80),
	@SoHopDongMoi varchar(80),
	@NgayHopDongCu datetime,
	@NgayHopDongMoi datetime,
	@STTHang int,
	@MaHangCu varchar(50),
	@LoaiHangCu varchar(2),
	@SoLuongCu numeric(18, 4),
	@DVTCu varchar(10),
	@MaHQDangKyCu varchar(6),
	@MaHangMoi varchar(50),
	@LoaiHangMoi varchar(2),
	@SoLuongMoi numeric(18, 4),
	@DVTMoi varchar(10),
	@MaHQDangKyMoi varchar(6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_PhuKienDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_PhuKienDetail] 
		SET
			[Master_ID] = @Master_ID,
			[NgayTKHeThong] = @NgayTKHeThong,
			[NgayTKSuaDoi] = @NgayTKSuaDoi,
			[SoHopDongCu] = @SoHopDongCu,
			[SoHopDongMoi] = @SoHopDongMoi,
			[NgayHopDongCu] = @NgayHopDongCu,
			[NgayHopDongMoi] = @NgayHopDongMoi,
			[STTHang] = @STTHang,
			[MaHangCu] = @MaHangCu,
			[LoaiHangCu] = @LoaiHangCu,
			[SoLuongCu] = @SoLuongCu,
			[DVTCu] = @DVTCu,
			[MaHQDangKyCu] = @MaHQDangKyCu,
			[MaHangMoi] = @MaHangMoi,
			[LoaiHangMoi] = @LoaiHangMoi,
			[SoLuongMoi] = @SoLuongMoi,
			[DVTMoi] = @DVTMoi,
			[MaHQDangKyMoi] = @MaHQDangKyMoi
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_PhuKienDetail]
		(
			[Master_ID],
			[NgayTKHeThong],
			[NgayTKSuaDoi],
			[SoHopDongCu],
			[SoHopDongMoi],
			[NgayHopDongCu],
			[NgayHopDongMoi],
			[STTHang],
			[MaHangCu],
			[LoaiHangCu],
			[SoLuongCu],
			[DVTCu],
			[MaHQDangKyCu],
			[MaHangMoi],
			[LoaiHangMoi],
			[SoLuongMoi],
			[DVTMoi],
			[MaHQDangKyMoi]
		)
		VALUES 
		(
			@Master_ID,
			@NgayTKHeThong,
			@NgayTKSuaDoi,
			@SoHopDongCu,
			@SoHopDongMoi,
			@NgayHopDongCu,
			@NgayHopDongMoi,
			@STTHang,
			@MaHangCu,
			@LoaiHangCu,
			@SoLuongCu,
			@DVTCu,
			@MaHQDangKyCu,
			@MaHangMoi,
			@LoaiHangMoi,
			@SoLuongMoi,
			@DVTMoi,
			@MaHQDangKyMoi
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_PhuKienDetail]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_PhuKienDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[NgayTKHeThong],
	[NgayTKSuaDoi],
	[SoHopDongCu],
	[SoHopDongMoi],
	[NgayHopDongCu],
	[NgayHopDongMoi],
	[STTHang],
	[MaHangCu],
	[LoaiHangCu],
	[SoLuongCu],
	[DVTCu],
	[MaHQDangKyCu],
	[MaHangMoi],
	[LoaiHangMoi],
	[SoLuongMoi],
	[DVTMoi],
	[MaHQDangKyMoi]
FROM
	[dbo].[t_KDT_SXXK_PhuKienDetail]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[NgayTKHeThong],
	[NgayTKSuaDoi],
	[SoHopDongCu],
	[SoHopDongMoi],
	[NgayHopDongCu],
	[NgayHopDongMoi],
	[STTHang],
	[MaHangCu],
	[LoaiHangCu],
	[SoLuongCu],
	[DVTCu],
	[MaHQDangKyCu],
	[MaHangMoi],
	[LoaiHangMoi],
	[SoLuongMoi],
	[DVTMoi],
	[MaHQDangKyMoi]
FROM [dbo].[t_KDT_SXXK_PhuKienDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[NgayTKHeThong],
	[NgayTKSuaDoi],
	[SoHopDongCu],
	[SoHopDongMoi],
	[NgayHopDongCu],
	[NgayHopDongMoi],
	[STTHang],
	[MaHangCu],
	[LoaiHangCu],
	[SoLuongCu],
	[DVTCu],
	[MaHQDangKyCu],
	[MaHangMoi],
	[LoaiHangMoi],
	[SoLuongMoi],
	[DVTMoi],
	[MaHQDangKyMoi]
FROM
	[dbo].[t_KDT_SXXK_PhuKienDetail]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LoaiPhuKien_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LoaiPhuKien_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LoaiPhuKien_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LoaiPhuKien_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LoaiPhuKien_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LoaiPhuKien_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LoaiPhuKien_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LoaiPhuKien_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LoaiPhuKien_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_Insert]
	@MaPhuKien char(4),
	@NoiDung nvarchar(255),
	@ThongTinMoi varchar(50),
	@Master_ID bigint,
	@ThongTinCu varchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_LoaiPhuKien]
(
	[MaPhuKien],
	[NoiDung],
	[ThongTinMoi],
	[Master_ID],
	[ThongTinCu]
)
VALUES 
(
	@MaPhuKien,
	@NoiDung,
	@ThongTinMoi,
	@Master_ID,
	@ThongTinCu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LoaiPhuKien_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_Update]
	@ID bigint,
	@MaPhuKien char(4),
	@NoiDung nvarchar(255),
	@ThongTinMoi varchar(50),
	@Master_ID bigint,
	@ThongTinCu varchar(50)
AS

UPDATE
	[dbo].[t_KDT_SXXK_LoaiPhuKien]
SET
	[MaPhuKien] = @MaPhuKien,
	[NoiDung] = @NoiDung,
	[ThongTinMoi] = @ThongTinMoi,
	[Master_ID] = @Master_ID,
	[ThongTinCu] = @ThongTinCu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LoaiPhuKien_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_InsertUpdate]
	@ID bigint,
	@MaPhuKien char(4),
	@NoiDung nvarchar(255),
	@ThongTinMoi varchar(50),
	@Master_ID bigint,
	@ThongTinCu varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_LoaiPhuKien] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_LoaiPhuKien] 
		SET
			[MaPhuKien] = @MaPhuKien,
			[NoiDung] = @NoiDung,
			[ThongTinMoi] = @ThongTinMoi,
			[Master_ID] = @Master_ID,
			[ThongTinCu] = @ThongTinCu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_LoaiPhuKien]
		(
			[MaPhuKien],
			[NoiDung],
			[ThongTinMoi],
			[Master_ID],
			[ThongTinCu]
		)
		VALUES 
		(
			@MaPhuKien,
			@NoiDung,
			@ThongTinMoi,
			@Master_ID,
			@ThongTinCu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LoaiPhuKien_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_LoaiPhuKien]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LoaiPhuKien_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_LoaiPhuKien] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LoaiPhuKien_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaPhuKien],
	[NoiDung],
	[ThongTinMoi],
	[Master_ID],
	[ThongTinCu]
FROM
	[dbo].[t_KDT_SXXK_LoaiPhuKien]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LoaiPhuKien_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaPhuKien],
	[NoiDung],
	[ThongTinMoi],
	[Master_ID],
	[ThongTinCu]
FROM [dbo].[t_KDT_SXXK_LoaiPhuKien] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LoaiPhuKien_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LoaiPhuKien_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaPhuKien],
	[NoiDung],
	[ThongTinMoi],
	[Master_ID],
	[ThongTinCu]
FROM
	[dbo].[t_KDT_SXXK_LoaiPhuKien]	

GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.8',GETDATE(), N' Cập nhật table và store cho khai báo phu kien tk')
END	



