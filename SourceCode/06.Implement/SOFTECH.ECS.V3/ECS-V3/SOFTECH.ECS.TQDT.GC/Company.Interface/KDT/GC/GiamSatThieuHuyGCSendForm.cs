﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using System.Data;
using Company.GC.BLL.KDT;
using System.IO;
using Company.Interface.KDT.SXXK;
using Company.Interface.GC;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.Send;
namespace Company.Interface.KDT.GC
{
    public partial class GiamSatThieuHuyGCSendForm : BaseForm
    {

        public HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        private List<HangGSTieuHuy> HangGsTieuHuys = new List<HangGSTieuHuy>();
        public GiamSatTieuHuy dnGSTieuHuy = new GiamSatTieuHuy() { TrangThaiXuLy = -1 };

        public GiamSatThieuHuyGCSendForm()
        {
            InitializeComponent();


        }

        private void Set(GiamSatTieuHuy gsTieuHuy)
        {
            txtSoGiayPhep.Text = gsTieuHuy.SoGiayPhep;
            ccNgayGiayPhep.Value = gsTieuHuy.NgayGiayPhep.Year > 1900 ? gsTieuHuy.NgayGiayPhep : DateTime.Now;
            ccNgayHetHan.Value = gsTieuHuy.NgayHetHan.Year > 1900 ? gsTieuHuy.NgayHetHan : DateTime.Now;
            txtToChucCap.Text = gsTieuHuy.ToChucCap;
            txtCacbenthamgia.Text = gsTieuHuy.CacBenThamGia;
            ccThoiGianTieuHuy.Value = gsTieuHuy.ThoiGianTieuHuy.Year > 1900 ? gsTieuHuy.ThoiGianTieuHuy : DateTime.Now;
            txtDiaDiemTieuHuy.Text = gsTieuHuy.DiaDiemTieuHuy;
            txtGhiChuKhac.Text = gsTieuHuy.GhiChuKhac;
        }
        private void Get(GiamSatTieuHuy gsTieuHuy)
        {
            gsTieuHuy.SoGiayPhep = txtSoGiayPhep.Text;
            gsTieuHuy.NgayGiayPhep = Convert.ToDateTime(ccNgayGiayPhep.Value);
            gsTieuHuy.NgayHetHan = Convert.ToDateTime(ccNgayHetHan.Value);
            gsTieuHuy.ToChucCap = txtToChucCap.Text;
            gsTieuHuy.CacBenThamGia = txtCacbenthamgia.Text;
            gsTieuHuy.ThoiGianTieuHuy = Convert.ToDateTime(ccThoiGianTieuHuy.Value);
            gsTieuHuy.DiaDiemTieuHuy = txtDiaDiemTieuHuy.Text;
            gsTieuHuy.GhiChuKhac = txtGhiChuKhac.Text;
            gsTieuHuy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            gsTieuHuy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            gsTieuHuy.HopDong_ID = HD.ID;

            //gsTieuHuy.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
            if (gsTieuHuy.NgayTiepNhan.Year < 1900) gsTieuHuy.NgayTiepNhan = new DateTime(1900, 1, 1);
        }
        private void GiamSatThieuHuyGCSendForm_Load(object sender, EventArgs e)
        {
            txtSoGiayPhep.Focus();
            if (dnGSTieuHuy == null)
                dnGSTieuHuy = new GiamSatTieuHuy();
            Set(dnGSTieuHuy);
            HangGsTieuHuys = HangGSTieuHuy.SelectCollectionDynamic("Master_ID=" + dnGSTieuHuy.ID, "");
            dgList.DataSource = HangGsTieuHuys;
            if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
                sendXML.master_id = dnGSTieuHuy.ID;
                if (sendXML.Load())
                {
                    cmdSend.Enabled = cmdSend1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdDelete.Enabled = cmdDelete1.Enabled =
                        cmdChonHang.Enabled = cmdChonHang1.Enabled = HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    lblTrangThai.Text = "Đã khai báo nhưng chưa nhận được phản hồi";
                }
                else
                    setCommandStatus();
            }
            else
            {
                if (OpenType == OpenFormType.Insert)
                {
                    dnGSTieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }
                setCommandStatus();
            }
        }
        public void setCommandStatus()
        {
            txtSoHopDong.Text = HD.SoHopDong;
            txtSoTiepNhan.Text = dnGSTieuHuy.SoTiepNhan != 0 ? dnGSTieuHuy.SoTiepNhan.ToString() : string.Empty;
            if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {

                cmdChonHang.Enabled = cmdChonHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdDelete.Enabled = cmdDelete1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
            }
            else if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                HuyKhaiBao.Enabled  = HuyKhaiBao1.Enabled= Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdDelete.Enabled = cmdDelete1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChonHang.Enabled = cmdChonHang1.Enabled= Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

            }
            else if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET
                || dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO
                || dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {

                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChonHang.Enabled = cmdChonHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdDelete.Enabled = cmdDelete1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
            }
            else if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                lblTrangThai.Text = "Không phê duyệt";
            }
            else if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                lblTrangThai.Text = "Đã hủy";
        }

        private void ChonHang()
        {
            CheckValid();
            HangTieuHuyForm f = new HangTieuHuyForm();
            f.HangTieuHuys = HangGsTieuHuys;
            f.IdHopDong = HD.ID;
            f.ShowDialog();
            HangGsTieuHuys = f.HangTieuHuys;
            dgList.DataSource = HangGsTieuHuys;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void Xoa()
        {
            try
            {
                if (ShowMessage("Bạn có thật sự muốn xóa không?", true) != "Yes") return;
                dnGSTieuHuy.Delete();
                HangGSTieuHuy.DeleteCollection(HangGsTieuHuys);
                dnGSTieuHuy = new GiamSatTieuHuy();
                HangGsTieuHuys = new List<HangGSTieuHuy>();
                ShowMessage("Xóa thành công", false);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdDelete":
                    Xoa();
                    break;
                case "cmdSave":
                    this.save();
                    break;
                case "cmdChonHang":
                    this.ChonHang();
                    break;
                case "cmdSend":
                    this.SendV3();
                    break;
                case "HuyKhaiBao":
                    this.CanceldV3();
                    break;
                case "NhanDuLieu":
                    this.FeedBackV3();
                    break;
                case "XacNhan":
                    // this.FeedBackV3();
                    //this.LaySoTiepNhanDT();
                    break;
                case "TaoMoiDM":
                    //  this.TaoDanhSachDMMoi();
                    break;

            }
        }
        private bool CheckValid()
        {
            bool isValid = false;
            isValid = Globals.ValidateNull(txtSoGiayPhep, error, "Số giấy phép");
            isValid &= Globals.ValidateDate(ccNgayGiayPhep, error, "Ngày giấy phép");
            isValid &= Globals.ValidateDate(ccNgayHetHan, error, "Ngày hết hạn");
            isValid &= Globals.ValidateNull(txtToChucCap, error, "Tổ chức cấp");
            isValid &= Globals.ValidateNull(txtCacbenthamgia, error, "Các bên tham gia");
            isValid &= Globals.ValidateNull(ccThoiGianTieuHuy, error, "Thời gian tiêu hủy");
            isValid &= Globals.ValidateNull(txtDiaDiemTieuHuy, error, "Địa điểm tiêu hủy");
            return isValid;
        }
        private void save()
        {

            if (!CheckValid()) return;
            Get(dnGSTieuHuy);
            try
            {
                if (string.IsNullOrEmpty(dnGSTieuHuy.GUIDSTR))
                    dnGSTieuHuy.GUIDSTR = Guid.NewGuid().ToString();

                if (dnGSTieuHuy.ID == 0)
                    dnGSTieuHuy.Insert();
                else dnGSTieuHuy.Update();
                HangGSTieuHuy.DeleteDynamic("Master_ID=" + dnGSTieuHuy.ID);
                int index = 1;
                foreach (HangGSTieuHuy item in HangGsTieuHuys)
                {
                    item.STTHang = index++;
                    item.Master_ID = dnGSTieuHuy.ID;
                    item.InsertUpdate();
                }

                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {

            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = dnGSTieuHuy.ID;
            form.DeclarationIssuer = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY;
            form.ShowDialog(this);
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            HangTieuHuyForm f = new HangTieuHuyForm();
            f.HangTieuHuys = HangGsTieuHuys;
            f.hangTieuHuy = (HangGSTieuHuy)e.Row.DataRow;
            f.IsEnable = dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ||
                        dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET ||
                        dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            int i = Convert.ToInt32(e.Row.Cells["LoaiHang"].Value);
            string loaiHang = string.Empty;
            if (i == 1) loaiHang = "Nguyên phụ liệu";
            else if (i == 2) loaiHang = "Sản phẩm";
            else if (i == 3) loaiHang = "Thiết bị";
            else if (i == 4) loaiHang = "Hàng mẫu";
            e.Row.Cells["LoaiHang"].Text = loaiHang;
        }
        #region V3
        private void SendV3()
        {
            if (dnGSTieuHuy.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            if (HangGsTieuHuys.Count == 0)
            {
                ShowMessage("Bạn chưa nhập hàng", false);
                return;
            }
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
            sendXML.master_id = dnGSTieuHuy.ID;

            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            dnGSTieuHuy.HangGSTieuHuys = HangGsTieuHuys;
            HopDong HD = new HopDong();
            HD.ID = dnGSTieuHuy.HopDong_ID;
            HD = HopDong.Load(dnGSTieuHuy.HopDong_ID);
            try
            {
                dnGSTieuHuy.GUIDSTR = Guid.NewGuid().ToString();
                PhuKienDangKyCollection pkHD = new PhuKienDangKyCollection();
                pkHD = HD.GetPK();
                if (pkHD.Count <= 0)
                {
                    ShowMessage("Hợp đồng không có phụ kiện", false);
                    return;
                }

                GC_DNGiamSatTieuHuy dnGiamSatTieuHuy = Mapper.ToDataTransferGSTieuHuy(dnGSTieuHuy, HD);
                ObjectSend msgSend = new ObjectSend(
                               new NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = dnGSTieuHuy.MaDoanhNghiep
                               }
                                 , new NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dnGSTieuHuy.MaHaiQuan),
                                     Identity = dnGSTieuHuy.MaHaiQuan
                                 }
                              ,
                                new SubjectBase()
                                {
                                    Type = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                                    Function = dnGiamSatTieuHuy.Function,
                                    Reference = dnGSTieuHuy.GUIDSTR,
                                }
                                ,
                                dnGiamSatTieuHuy);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(dnGSTieuHuy.ID, MessageTitle.KhaiBaoDNTieuHuy);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = cmdSend1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdDelete.Enabled = cmdDelete1.Enabled = cmdChonHang.Enabled = cmdChonHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
                    sendXML.master_id = dnGSTieuHuy.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    dnGSTieuHuy.Update();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(ex.Message, dnGSTieuHuy.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.DNGiamSatTieuHuySendHandler(dnGSTieuHuy, ref msgInfor, e);

        }
        private void FeedBackV3()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                Reference = dnGSTieuHuy.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,

            };
            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = dnGSTieuHuy.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dnGSTieuHuy.MaHaiQuan.Trim()),
                                              Identity = dnGSTieuHuy.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                }
                if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    this.setCommandStatus();
            }
            if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                this.setCommandStatus();
        }
        private void CanceldV3()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
            sendXML.master_id = dnGSTieuHuy.ID;
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            DeclarationBase CancelDnGSTieuHuy =
                Mapper.HuyKhaiBao(DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                                  dnGSTieuHuy.GUIDSTR,
                                  dnGSTieuHuy.SoTiepNhan,
                                  dnGSTieuHuy.MaHaiQuan,
                                  dnGSTieuHuy.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = dnGSTieuHuy.MaDoanhNghiep
                           }
                             , new NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dnGSTieuHuy.MaHaiQuan),
                                 Identity = dnGSTieuHuy.MaHaiQuan
                             }
                          ,
                            new SubjectBase()
                            {
                                Type = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                                Function = DeclarationFunction.HUY,
                                Reference = dnGSTieuHuy.GUIDSTR,
                            }
                            ,
                            CancelDnGSTieuHuy);
            SendMessageForm sendForm = new SendMessageForm();
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
            {
                dnGSTieuHuy.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                sendForm.Message.XmlSaveMessage(dnGSTieuHuy.ID, MessageTitle.HQHuyKhaiBaoToKhai);
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dnGSTieuHuy.Update();
                FeedBackV3();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }

        #endregion

    }
}