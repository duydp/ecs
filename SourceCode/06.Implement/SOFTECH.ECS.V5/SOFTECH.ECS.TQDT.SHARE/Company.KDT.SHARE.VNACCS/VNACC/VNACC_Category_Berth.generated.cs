using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_Berth : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string ReferenceDB { set; get; }
		public string CodeHQ { set; get; }
		public string Code { set; get; }
		public string MoTa_HeThong { set; get; }
		public string TenCang { set; get; }
		public string DiaChi { set; get; }
		public string Note { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACC_Category_Berth> ConvertToCollection(IDataReader reader)
		{
			List<VNACC_Category_Berth> collection = new List<VNACC_Category_Berth>();
			while (reader.Read())
			{
				VNACC_Category_Berth entity = new VNACC_Category_Berth();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ReferenceDB"))) entity.ReferenceDB = reader.GetString(reader.GetOrdinal("ReferenceDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("CodeHQ"))) entity.CodeHQ = reader.GetString(reader.GetOrdinal("CodeHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("Code"))) entity.Code = reader.GetString(reader.GetOrdinal("Code"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTa_HeThong"))) entity.MoTa_HeThong = reader.GetString(reader.GetOrdinal("MoTa_HeThong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCang"))) entity.TenCang = reader.GetString(reader.GetOrdinal("TenCang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
				if (!reader.IsDBNull(reader.GetOrdinal("Note"))) entity.Note = reader.GetString(reader.GetOrdinal("Note"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<VNACC_Category_Berth> collection, long id)
        {
            foreach (VNACC_Category_Berth item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACC_Category_Berth VALUES(@ReferenceDB, @CodeHQ, @Code, @MoTa_HeThong, @TenCang, @DiaChi, @Note)";
            string update = "UPDATE t_VNACC_Category_Berth SET ReferenceDB = @ReferenceDB, CodeHQ = @CodeHQ, Code = @Code, MoTa_HeThong = @MoTa_HeThong, TenCang = @TenCang, DiaChi = @DiaChi, Note = @Note WHERE ID = @ID";
            string delete = "DELETE FROM t_VNACC_Category_Berth WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ReferenceDB", SqlDbType.VarChar, "ReferenceDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CodeHQ", SqlDbType.VarChar, "CodeHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Code", SqlDbType.VarChar, "Code", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTa_HeThong", SqlDbType.VarChar, "MoTa_HeThong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCang", SqlDbType.NVarChar, "TenCang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Note", SqlDbType.VarChar, "Note", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ReferenceDB", SqlDbType.VarChar, "ReferenceDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CodeHQ", SqlDbType.VarChar, "CodeHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Code", SqlDbType.VarChar, "Code", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTa_HeThong", SqlDbType.VarChar, "MoTa_HeThong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCang", SqlDbType.NVarChar, "TenCang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Note", SqlDbType.VarChar, "Note", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACC_Category_Berth VALUES(@ReferenceDB, @CodeHQ, @Code, @MoTa_HeThong, @TenCang, @DiaChi, @Note)";
            string update = "UPDATE t_VNACC_Category_Berth SET ReferenceDB = @ReferenceDB, CodeHQ = @CodeHQ, Code = @Code, MoTa_HeThong = @MoTa_HeThong, TenCang = @TenCang, DiaChi = @DiaChi, Note = @Note WHERE ID = @ID";
            string delete = "DELETE FROM t_VNACC_Category_Berth WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ReferenceDB", SqlDbType.VarChar, "ReferenceDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CodeHQ", SqlDbType.VarChar, "CodeHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Code", SqlDbType.VarChar, "Code", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTa_HeThong", SqlDbType.VarChar, "MoTa_HeThong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCang", SqlDbType.NVarChar, "TenCang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Note", SqlDbType.VarChar, "Note", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ReferenceDB", SqlDbType.VarChar, "ReferenceDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CodeHQ", SqlDbType.VarChar, "CodeHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Code", SqlDbType.VarChar, "Code", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTa_HeThong", SqlDbType.VarChar, "MoTa_HeThong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCang", SqlDbType.NVarChar, "TenCang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Note", SqlDbType.VarChar, "Note", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACC_Category_Berth Load(long id)
		{
			const string spName = "[dbo].[p_VNACC_Category_Berth_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACC_Category_Berth> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACC_Category_Berth> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACC_Category_Berth> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_Berth_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_Berth_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_Berth_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_Berth_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertVNACC_Category_Berth(string referenceDB, string codeHQ, string code, string moTa_HeThong, string tenCang, string diaChi, string note)
		{
			VNACC_Category_Berth entity = new VNACC_Category_Berth();	
			entity.ReferenceDB = referenceDB;
			entity.CodeHQ = codeHQ;
			entity.Code = code;
			entity.MoTa_HeThong = moTa_HeThong;
			entity.TenCang = tenCang;
			entity.DiaChi = diaChi;
			entity.Note = note;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACC_Category_Berth_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@ReferenceDB", SqlDbType.VarChar, ReferenceDB);
			db.AddInParameter(dbCommand, "@CodeHQ", SqlDbType.VarChar, CodeHQ);
			db.AddInParameter(dbCommand, "@Code", SqlDbType.VarChar, Code);
			db.AddInParameter(dbCommand, "@MoTa_HeThong", SqlDbType.VarChar, MoTa_HeThong);
			db.AddInParameter(dbCommand, "@TenCang", SqlDbType.NVarChar, TenCang);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@Note", SqlDbType.VarChar, Note);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACC_Category_Berth> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Berth item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACC_Category_Berth(long id, string referenceDB, string codeHQ, string code, string moTa_HeThong, string tenCang, string diaChi, string note)
		{
			VNACC_Category_Berth entity = new VNACC_Category_Berth();			
			entity.ID = id;
			entity.ReferenceDB = referenceDB;
			entity.CodeHQ = codeHQ;
			entity.Code = code;
			entity.MoTa_HeThong = moTa_HeThong;
			entity.TenCang = tenCang;
			entity.DiaChi = diaChi;
			entity.Note = note;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACC_Category_Berth_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ReferenceDB", SqlDbType.VarChar, ReferenceDB);
			db.AddInParameter(dbCommand, "@CodeHQ", SqlDbType.VarChar, CodeHQ);
			db.AddInParameter(dbCommand, "@Code", SqlDbType.VarChar, Code);
			db.AddInParameter(dbCommand, "@MoTa_HeThong", SqlDbType.VarChar, MoTa_HeThong);
			db.AddInParameter(dbCommand, "@TenCang", SqlDbType.NVarChar, TenCang);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@Note", SqlDbType.VarChar, Note);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACC_Category_Berth> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Berth item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACC_Category_Berth(long id, string referenceDB, string codeHQ, string code, string moTa_HeThong, string tenCang, string diaChi, string note)
		{
			VNACC_Category_Berth entity = new VNACC_Category_Berth();			
			entity.ID = id;
			entity.ReferenceDB = referenceDB;
			entity.CodeHQ = codeHQ;
			entity.Code = code;
			entity.MoTa_HeThong = moTa_HeThong;
			entity.TenCang = tenCang;
			entity.DiaChi = diaChi;
			entity.Note = note;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_Berth_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ReferenceDB", SqlDbType.VarChar, ReferenceDB);
			db.AddInParameter(dbCommand, "@CodeHQ", SqlDbType.VarChar, CodeHQ);
			db.AddInParameter(dbCommand, "@Code", SqlDbType.VarChar, Code);
			db.AddInParameter(dbCommand, "@MoTa_HeThong", SqlDbType.VarChar, MoTa_HeThong);
			db.AddInParameter(dbCommand, "@TenCang", SqlDbType.NVarChar, TenCang);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@Note", SqlDbType.VarChar, Note);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACC_Category_Berth> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Berth item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACC_Category_Berth(long id)
		{
			VNACC_Category_Berth entity = new VNACC_Category_Berth();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_Berth_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACC_Category_Berth_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACC_Category_Berth> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Berth item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}