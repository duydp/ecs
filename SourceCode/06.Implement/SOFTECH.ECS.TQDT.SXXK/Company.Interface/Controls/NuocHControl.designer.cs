﻿namespace Company.Interface.Controls
{
    partial class NuocHControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout gridEXLayout1 = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NuocHControl));
            this.txtMa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbTen = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.ds = new System.Data.DataSet();
            this.dtNuoc = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.cbTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNuoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            this.SuspendLayout();
            // 
            // txtMa
            // 
            this.txtMa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa.Location = new System.Drawing.Point(0, 0);
            this.txtMa.Name = "txtMa";
            this.txtMa.ReadOnly = true;
            this.txtMa.Size = new System.Drawing.Size(49, 21);
            this.txtMa.TabIndex = 0;
            this.txtMa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMa.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // cbTen
            // 
            this.cbTen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTen.ComboStyle = Janus.Windows.GridEX.ComboStyle.DropDownList;
            this.cbTen.DataMember = "Nuoc";
            this.cbTen.DataSource = this.ds;
            gridEXLayout1.LayoutString = resources.GetString("gridEXLayout1.LayoutString");
            this.cbTen.DesignTimeLayout = gridEXLayout1;
            this.cbTen.DisplayMember = "Ten";
            this.cbTen.Location = new System.Drawing.Point(55, 0);
            this.cbTen.Name = "cbTen";
            this.cbTen.SaveSettings = false;
            this.cbTen.SelectedIndex = -1;
            this.cbTen.SelectedItem = null;
            this.cbTen.Size = new System.Drawing.Size(204, 21);
            this.cbTen.TabIndex = 1;
            this.cbTen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbTen.ValueMember = "ID";
            this.cbTen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.cbTen.ValueChanged += new System.EventHandler(this.cbTen_ValueChanged);
            // 
            // ds
            // 
            this.ds.DataSetName = "DuLieuChuan";
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtNuoc});
            // 
            // dtNuoc
            // 
            this.dtNuoc.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2});
            this.dtNuoc.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "ID"}, true)});
            this.dtNuoc.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
            this.dtNuoc.TableName = "Nuoc";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "ID";
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "Ten";
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.cbTen;
            this.rfvTen.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            // 
            // NuocHControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.cbTen);
            this.Controls.Add(this.txtMa);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "NuocHControl";
            this.Size = new System.Drawing.Size(273, 22);
            this.Load += new System.EventHandler(this.NuocControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNuoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtMa;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbTen;
        private System.Data.DataSet ds;
        private System.Data.DataTable dtNuoc;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
    }
}