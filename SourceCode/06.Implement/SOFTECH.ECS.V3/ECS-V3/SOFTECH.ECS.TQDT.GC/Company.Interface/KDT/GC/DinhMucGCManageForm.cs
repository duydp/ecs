using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;


namespace Company.Interface.KDT.GC
{
    public partial class DinhMucGCManageForm : BaseForm
    {
        public bool IsBrowseForm = false;
        private Company.GC.BLL.KDT.GC.DinhMucDangKy dmdkSelect;
        private DinhMucDangKyCollection dmdkCollection = new DinhMucDangKyCollection();
        List<HopDong> hdcoll = new List<HopDong>();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public DinhMucGCManageForm()
        {
            InitializeComponent();
            cbStatus.SelectedIndex = 0;
        }


        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }

        //-----------------------------------------------------------------------------------------

        private void DinhMucGCManageForm_Load(object sender, EventArgs e)
        {
            //An nut Xac nhan
            XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;

            this.khoitao_DuLieuChuan();
            cbStatus.SelectedIndex = 0;
            //BindData(Convert.ToInt16(cbStatus.SelectedItem.Value));
            BindHopDong();
            btnSearch_Click(null, null);

        }

        //-----------------------------------------------------------------------------------------



        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //hopdong =HopDong.Load(e.Row.Cells[3].Text);                      
            long ID = (long)Convert.ToInt64(e.Row.Cells["ID"].Text);

            Form[] forms = this.ParentForm.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DM" + ID.ToString()))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhMucDangKy dmdk = new DinhMucDangKy();
            dmdk.ID = ID;
            dmdk.Load();
            dmdk.LoadCollection();
            #region code dm
            //Company.GC.BLL.KDT.GC.DinhMucCollection dmcol = Company.GC.BLL.KDT.GC.DinhMuc.SelectCollectionBy_Master_ID(ID);
            //DinhMucCollection dmCollection = new DinhMucCollection();
            //foreach(Company.GC.BLL.KDT.GC.DinhMuc dm in dmcol)
            //{
            //    DinhMuc d = new DinhMuc();
            //    d.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
            //    d.SoHopDong = dmdk.SoHopDong;
            //    d.NgayKy = dmdk.NgayKy;
            //    d.MaHaiQuan = dmdk.MaHaiQuan;
            //    d.MaDoanhNghiep = dmdk.MaDoanhNghiep;
            //    d.MaNguyenPhuLieu = dm.MaNguyenPhuLieu;
            //    d.MaSanPham = dm.MaSanPham;
            //    d.NPL_TuCungUng = dm.NPL_TuCungUng;
            //    d.TyLeHaoHut = dm.TyLeHaoHut;
            //    d.GhiChu = dm.GhiChu;
            //    d.DinhMucSuDung = dm.DinhMucSuDung;
            //    dmCollection.Add(d);                
            //}
            //if (dmCollection.Count > 0)
            //{
            //    DinhMuc d = new DinhMuc();
            //    d.Insert(dmCollection);
            //}
            #endregion code dm
            DinhMucGCSendForm f = new DinhMucGCSendForm();
            f.dmDangKy = dmdk;
            f.Name = "DM" + ID.ToString();
            f.OpenType = OpenFormType.Edit;
            f.ShowDialog();
        }
        private void BindHopDong()
        {
            HopDong hd = new HopDong();
            hd.SoHopDong = "";
            hd.ID = 0;
            List<HopDong> collection;
            {
                string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
            }
            collection.Add(hd);
            cbHopDong.DataSource = collection;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            cbHopDong.SelectedIndex = collection.Count - 1;
        }
        //private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        //{
        //    //this.BindData();
        //    BindHopDong();
        //    cbHopDong.Text = "";
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["NgayTiepNhan"].Value != DBNull.Value)
                {
                    if (Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Value).Year <= 1900)
                    {
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                    }
                }
                if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_DUYET)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHUA_KHAI_BAO)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHO_DUYET)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHO_HUY)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_HUY)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.KHONG_PHE_DUYET)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                HopDong hd = new HopDong();
                hd.ID = (long)Convert.ToInt64(e.Row.Cells["ID_HopDong"].Value);
                hd = HopDong.Load(hd.ID);
                e.Row.Cells["SoHopDong"].Text = hd.SoHopDong;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
            if (txtSoTiepNhan.Text.Trim().Length > 0)
            {
                where += " and sotiepnhan like '%" + txtSoTiepNhan.Text + "%'";
            }

            try
            {
                long idHopDong = Convert.ToInt64(cbHopDong.Value);
            }
            catch
            {
                showMsg("MSG_240205");
                //ShowMessage("Không có hợp đồng này trong hệ thống.", false);
                return;
            }
            if (cbHopDong.Text.Length > 0)
            {
                where += " and id_HopDong like '%" + cbHopDong.Value.ToString() + "%'";
            }

            if (txtNamTiepNhan.Text.Trim().Length > 0)
            {
                where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text;
            }
            if (cbStatus.Text.Length > 0)
                where += " and TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();
            dmdkCollection = (new Company.GC.BLL.KDT.GC.DinhMucDangKy()).SelectCollectionDynamic(where, "");
            dgList.DataSource = dmdkCollection;
            setCommandStatus();
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            long ID = (long)Convert.ToInt64(e.Row.Cells[4].Text);
            DinhMucDangKy dmdk = new DinhMucDangKy();
            dmdk.ID = ID;
            dmdk.Load();
            if (e.Row.RowType == RowType.Record)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa định mức này không?", true) == "Yes")
                {
                    if (dmdk.ID > 0)
                    {
                        dmdk.Delete();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void setCommandStatus()
        {
            if (cbStatus.SelectedValue.ToString() == "-1" || cbStatus.SelectedValue.ToString() == "2" || cbStatus.SelectedValue.ToString() == "10")
            {
                Huy.Enabled = Huy1.Enabled = Huy2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = NhanDuLieu2.Enabled = cbStatus.SelectedValue.ToString() == "-1" ? Janus.Windows.UI.InheritableBoolean.True :
                     Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdXuatDinhMuc.Enabled = cmdXuatDinhMuc1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.False;

            }
            else if (cbStatus.SelectedValue.ToString() == "0" || cbStatus.SelectedValue.ToString() == "11")
            {
                Huy.Enabled = Huy1.Enabled = Huy2.Enabled = cbStatus.SelectedValue.ToString() == "0" ? Janus.Windows.UI.InheritableBoolean.True :
                     Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatDinhMuc.Enabled = cmdXuatDinhMuc1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;

            }
            else
            {
                Huy.Enabled = Huy1.Enabled = Huy2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = NhanDuLieu2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatDinhMuc.Enabled = cmdXuatDinhMuc1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;

            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "KhaiBao": this.SendV3();/* khaibaoHD()*/ ; break;
                case "NhanDuLieu": this.FeedBackV3();/* nhandulieuHD()*/ ; break;
                case "Huy": this.CanceldV3();/* HuyNhieuHD()*/ ; break;
                //DATLMQ comment ngày 24/03/2011
                //case "XacNhan": LaySoTiepNhanDT(); break;
                case "XacNhan": this.FeedBackV3() /*nhandulieuHD()*/; break;
                case "cmdCSDaDuyet": ChuyenTrangThai(); break;
                case "cmdXuatDinhMuc":
                    XuatDinhMucChoPhongKhai();
                    break;
                case "InPhieuTN": this.inPhieuTN(); break;
            }
        }
        private void inPhieuTN()
        {
            if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "ĐỊNH MỨC";
            Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = dmDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = dmDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                    break;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();

        }
        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count > 0)
            {
                DinhMucDangKyCollection dmdkColl = new DinhMucDangKyCollection();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    dmdkColl.Add((DinhMucDangKy)grItem.GetRow().DataRow);
                }
                for (int i = 0; i < dmdkColl.Count; i++)
                {
                    //string msg = "Bạn có muốn chuyển trạng thái của định mức đăng ký được chọn sang đã duyệt không?";
                    //msg += "\n\nSố thứ tự của định mức đăng ký: " + dmdkColl[i].ID.ToString();
                    //dmdkColl[i].LoadCollection();
                    //msg += "\nCó tổng số " + dmdkColl[i].DMCollection.Count.ToString() + " định mức.";

                    dmdkColl[i].LoadCollection();

                    string[] args = new string[2];
                    args[0] = dmdkColl[i].ID.ToString();
                    args[1] = dmdkColl[i].DMCollection.Count.ToString();

                    if (showMsg("MSG_0203071", args, true) == "Yes")
                    {
                        if (dmdkColl[i].TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            dmdkColl[i].NgayTiepNhan = DateTime.Today;
                        }
                        Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty,
                            dmdkColl[i].ID,
                            dmdkColl[i].GUIDSTR,
                            Company.KDT.SHARE.Components.MessageTypes.DinhMuc,
                            Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                             Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay,
                             string.Format("Trước khi chuyển ID={0},GUIDSTR={1}", dmdkColl[i].ID, dmdkColl[i].GUIDSTR));
                        dmdkColl[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        dmdkColl[i].TransferGC();
                    }
                }
                this.btnSearch_Click(null, null);
            }
            else
            {
                showMsg("MSG_2702040", false);
                //ShowMessage("Chưa có dữ liệu được chọn!", false);
            }
        }

        private void XuatDinhMucChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                showMsg("MSG_2702038");
                //ShowMessage("Chưa có danh sách định mức cần xuất ra file.", false);
                return;
            }
            try
            {
                DinhMucDangKyCollection col = new DinhMucDangKyCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sodinhmuc = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMucDangKy DM = (DinhMucDangKy)i.GetRow().DataRow;
                            HopDong HD = new HopDong();
                            HD.ID = DM.ID_HopDong;

                            HD = HopDong.Load(HD.ID);
                            DM.SoHopDong = HD.SoHopDong;
                            DM.LoadCollection();
                            col.Add(DM);
                            sodinhmuc++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    showMsg("MSG_2702039", sodinhmuc);
                    //ShowMessage("Xuất ra file thành công " + sodinhmuc + " định mức.", false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void nhandulieuHD()
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702040");
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            dmdkSelect = (DinhMucDangKy)dgList.GetRow().DataRow;
            dmdkSelect.Load();
            dmdkSelect.LoadCollection();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "DMHD";
                sendXML.master_id = dmdkSelect.ID;
                //if (sendXML.Load())
                //{
                //    showMsg("MSG_WRN05");
                //    //ShowMessage("Định mức đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                //    return;
                //}
                //if (!sendXML.Load())
                //{
                //    showMsg("MSG_STN01");
                //    //ShowMessage("Định mức không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                //    return;
                //}
            }

            WSForm wsForm = new WSForm();
            string password = "";
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                {
                    //DATLMQ comment ngày 24/03/2011
                    //xmlCurrent = dmdkSelect.WSDownLoad(password);
                    //xmlCurrent = dmdkSelect.TQDTLayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;


                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "DMHD";
                sendXML.master_id = dmdkSelect.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận thông tin định mức hợp đồng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void khaibaoHD()
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702040");
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            dmdkSelect = (DinhMucDangKy)dgList.GetRow().DataRow;
            dmdkSelect.Load();
            dmdkSelect.LoadCollection();

            HopDong HD = new HopDong();
            HD.ID = dmdkSelect.ID_HopDong;
            HD = HopDong.Load(HD.ID);
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                showMsg("MSG_240235");
                //ShowMessage("Hợp đồng này chưa được khai báo", false);
                return;
            }
            WSForm wsForm = new WSForm();
            string password = "";
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = dmdkSelect.WSSend(password);
                this.Cursor = Cursors.Default;

                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "DMHD";
                sendXML.master_id = dmdkSelect.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo định mức . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void HuyNhieuHD()
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702040");
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            dmdkSelect = (DinhMucDangKy)dgList.GetRow().DataRow;
            dmdkSelect.Load();

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "DMHD";
            sendXML.master_id = dmdkSelect.ID;
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                //ShowMessage("Định mức đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            WSForm wsForm = new WSForm();
            string password = "";
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = dmdkSelect.WSCancel(password);
                this.Cursor = Cursors.Default;

                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "DMHD";
                sendXML.master_id = dmdkSelect.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy thông tin định mức . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa danh sách định mức của sản phẩm này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.DinhMucDangKy dm = (Company.GC.BLL.KDT.GC.DinhMucDangKy)row.GetRow().DataRow;
                    Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "DMGC";
                    sendXML.master_id = dm.ID;
                    if (sendXML.Load())
                    {
                        int so = row.Position + 1;
                        string st = showMsg("MSG_240236", so.ToString(), true);
                        //string st = ShowMessage("Danh sách thứ " + (row.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Bạn có muốn xóa không?", true);
                        if (st == "Yes")
                        {
                            if (dm.ID > 0)
                            {
                                try
                                {

                                    dm.Delete();

                                }
                                catch (Exception ex)
                                {
                                    showMsg("MSG_2702004", ex.Message);
                                    //ShowMessage("Lỗi: " + ex, false);
                                }
                            }
                        }
                    }
                    if (dm.ID > 0)
                    {
                        try
                        {
                            dm.Delete();
                        }
                        catch (Exception ex)
                        {
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage("Lỗi: " + ex, false);
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        //-----------------------------------------------------------------------------------------
        private void LayPhanHoi(string pass)
        {
            dmdkSelect = (DinhMucDangKy)dgList.GetRow().DataRow;
            dmdkSelect.Load();
            dmdkSelect.LoadCollection();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {

                sendXML.LoaiHS = "DMHD";
                sendXML.master_id = dmdkSelect.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                //DATLMQ bổ sung ngày 24/03/2011
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, dmdkSelect.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI.Trim();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI");

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN + " ";
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = dmdkSelect.GUIDSTR.Trim().ToUpper();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                xmlCurrent = dmdkSelect.LayPhanHoi(pass, xml.InnerXml);
                //xmlCurrent = dmdkSelect.LayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", dmdkSelect.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmdkSelect.SoTiepNhan, false);                  
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702041");
                    //ShowMessage("Đã hủy danh sách định mức này", false);                  
                }
                else if (sendXML.func == 2)
                {
                    if (dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);                     
                    }
                    else if (dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        //showMsg("MSG_SEN04", mess);
                        //ShowMessage("Hải quan chưa xử lý danh sách sản phẩm này!", false);

                        if (mess != "")
                            ShowMessage(mess, false);
                        else
                            ShowMessage("Hải quan chưa xử lý danh sách định mức này!", false);
                    }
                    else if (dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);                       
                    }
                    else if (dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        showMsg("MSG_SEN05");
                    }

                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message.Trim());
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LaySoTiepNhanDT()
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702040");
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            dmdkSelect = (DinhMucDangKy)dgList.GetRow().DataRow;
            dmdkSelect.Load();
            dmdkSelect.LoadCollection();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "DMHD";
                sendXML.master_id = dmdkSelect.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //ShowMessage("Định mức không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    return;
                }
            }

            WSForm wsForm = new WSForm();
            string password = "";
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                {
                    //xmlCurrent = dmdkSelect.LayPhanHoi(password, sendXML.msg);
                    xmlCurrent = dmdkSelect.TQDTLayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", dmdkSelect.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmdkSelect.SoTiepNhan, false);                   
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702042");
                    //ShowMessage("Đã hủy hợp đồng này", false);                 
                }
                else if (sendXML.func == 2)
                {
                    if (dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";

                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);                       
                    }
                    else if (dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                        //ShowMessage("Hải quan chưa xử lý danh sách sản phẩm này!", false);
                    }
                    else if (dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);                      
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        private void btnInDMSP_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row == null || row.RowType != RowType.Record) return;
            DinhMucDangKy DMDK = (DinhMucDangKy)row.DataRow;
            Report.ReportViewBC03_DMDKForm reportForm = new Company.Interface.Report.ReportViewBC03_DMDKForm();
            reportForm.DMDangKy = DMDK;
            reportForm.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Company.GC.BLL.KDT.GC.DinhMucDangKyCollection dmdkColl = new DinhMucDangKyCollection();
            if (dgList.GetRows().Length < 1) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count < 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa danh sách định mức của sản phẩm này không?", true) == "Yes")
            {

                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.DinhMucDangKy dm = (Company.GC.BLL.KDT.GC.DinhMucDangKy)row.GetRow().DataRow;
                    Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "DMGC";
                    sendXML.master_id = dm.ID;
                    if (sendXML.Load())
                    {
                        string st = showMsg("MSG_240236", row.Position + 1, true);
                        //string st = ShowMessage("Danh sách thứ " + (row.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Bạn có muốn xóa không?", true);
                        if (st == "Yes")
                        {
                            if (dm.ID > 0)
                            {
                                dm.Delete();
                            }
                        }
                    }
                    if (dm.ID > 0)
                    {
                        dm.Delete();
                    }

                    //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                    try
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", dm.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.DinhMuc);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = true;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                        return;
                    }

                    dmdkColl.Add(dm);

                }
                foreach (Company.GC.BLL.KDT.GC.DinhMucDangKy dmm in dmdkColl)
                {
                    dmdkCollection.Remove(dmm);
                }
                dgList.DataSource = dmdkCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
                //this.BindData(Convert.ToInt16(this.cbStatus.SelectedItem.Value));
            }
        }

        private void uiContextMenu1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702040");
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            dmdkSelect = (DinhMucDangKy)dgList.GetRow().DataRow;
            //Globals.ShowKetQuaXuLyBoSung(dmdkSelect.GUIDSTR);
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = dmdkSelect.ID;
            form.DeclarationIssuer = DeclarationIssuer.GC_DINH_MUC;
            form.ShowDialog(this);
        }
        #region V3 - 13/03/2012
        private void SendV3()
        {

            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702040");
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            dmdkSelect = (DinhMucDangKy)dgList.GetRow().DataRow;
            dmdkSelect.Load();
            dmdkSelect.LoadCollection();

            HopDong HD = new HopDong();
            HD.ID = dmdkSelect.ID_HopDong;
            HD = HopDong.Load(HD.ID);
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                showMsg("MSG_240235");
                //ShowMessage("Hợp đồng này chưa được khai báo", false);
                return;
            }
            sendXML.master_id = dmdkSelect.ID;
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            if (sendXML.Load())
            {
                ShowMessage("Định mức đã được gửi đến hải quan. Nhấn nút [Lấy phản hồi] để nhận thông tin", false);
                XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = NhanDuLieu.Enabled = NhanDuLieu1.Enabled = NhanDuLieu2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            try
            {
                dmdkSelect.GUIDSTR = Guid.NewGuid().ToString();
                if (dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    dmdkSelect.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                Company.KDT.SHARE.Components.GC_DinhMuc dm = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferDinhMuc(dmdkSelect, HD);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = dmdkSelect.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmdkSelect.MaHaiQuan),
                                     Identity = dmdkSelect.MaHaiQuan
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_DINH_MUC,
                                    Function = dm.Function,
                                    Reference = dmdkSelect.GUIDSTR,
                                }
                                ,
                                dm);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(dmdkSelect.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoDinhMuc);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = NhanDuLieu.Enabled = NhanDuLieu1.Enabled = NhanDuLieu2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                    sendXML.master_id = dmdkSelect.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    dmdkSelect.Update();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhMucSendHandler(dmdkSelect, ref msgInfor, e);

        }
        private void FeedBackV3()
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702040");
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            dmdkSelect = (DinhMucDangKy)dgList.GetRow().DataRow;
            dmdkSelect.Load();
            dmdkSelect.LoadCollection();
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_DINH_MUC,
                Reference = dmdkSelect.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_DINH_MUC,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = dmdkSelect.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmdkSelect.MaHaiQuan.Trim()),
                                              Identity = dmdkSelect.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    {
                        setCommandStatus();
                    }
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else if (dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                }
            }
        }
        private void CanceldV3()
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702040");
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            dmdkSelect = (DinhMucDangKy)dgList.GetRow().DataRow;
            dmdkSelect.Load();

            //Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            //sendXML.master_id = dmdkSelect.ID;
            //if (sendXML.Load())
            //{
            //    showMsg("MSG_WRN05");
            //    XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = NhanDuLieu.Enabled = NhanDuLieu1.Enabled = NhanDuLieu2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    //ShowMessage("Định mức đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
            //    return;
            //}
            Company.KDT.SHARE.Components.DeclarationBase dinhmuc = Company.GC.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.GC_DINH_MUC, dmdkSelect.GUIDSTR, dmdkSelect.SoTiepNhan, dmdkSelect.MaHaiQuan, dmdkSelect.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = dmdkSelect.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmdkSelect.MaHaiQuan),
                                 Identity = dmdkSelect.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_DINH_MUC,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = dmdkSelect.GUIDSTR,
                            }
                            ,
                            dinhmuc);
            SendMessageForm sendForm = new SendMessageForm();
            dmdkSelect.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && dmdkSelect.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                sendForm.Message.XmlSaveMessage(dmdkSelect.ID, Company.KDT.SHARE.Components.MessageTitle.HQHuyKhaiBaoToKhai);
               // XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = NhanDuLieu.Enabled = NhanDuLieu1.Enabled = NhanDuLieu2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //sendXML.func = 3;
                //sendXML.msg = msgSend;
                //sendXML.InsertUpdate();
                dmdkSelect.Update();
                FeedBackV3();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }

        #endregion
    }
}
