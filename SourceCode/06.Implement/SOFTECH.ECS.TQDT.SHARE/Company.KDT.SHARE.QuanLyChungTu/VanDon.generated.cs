using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class VanDon
    {
        #region Private members.

        protected long _ID;
        protected string _SoVanDon = string.Empty;
        protected DateTime _NgayVanDon = new DateTime(1753, 1, 1);
        protected bool _HangRoi;
        protected string _SoHieuPTVT = string.Empty;
        protected DateTime _NgayDenPTVT = new DateTime(1753, 1, 1);
        protected string _MaHangVT = string.Empty;
        protected string _TenHangVT = string.Empty;
        protected string _TenPTVT = string.Empty;
        protected string _QuocTichPTVT = string.Empty;
        protected string _NuocXuat_ID = string.Empty;
        protected string _MaNguoiNhanHang = string.Empty;
        protected string _TenNguoiNhanHang = string.Empty;
        protected string _MaNguoiGiaoHang = string.Empty;
        protected string _TenNguoiGiaoHang = string.Empty;
        protected string _CuaKhauNhap_ID = string.Empty;
        protected string _CuaKhauXuat = string.Empty;
        protected string _MaNguoiNhanHangTrungGian = string.Empty;
        protected string _TenNguoiNhanHangTrungGian = string.Empty;
        protected string _MaCangXepHang = string.Empty;
        protected string _TenCangXepHang = string.Empty;
        protected string _MaCangDoHang = string.Empty;
        protected string _TenCangDoHang = string.Empty;
        protected long _TKMD_ID;
        protected string _DKGH_ID = string.Empty;
        protected string _DiaDiemGiaoHang = string.Empty;
        protected string _NoiDi = string.Empty;
        protected string _SoHieuChuyenDi = string.Empty;
        protected DateTime _NgayKhoiHanh = new DateTime(1753, 1, 1);

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public string SoVanDon
        {
            set { this._SoVanDon = value; }
            get { return this._SoVanDon; }
        }

        public DateTime NgayVanDon
        {
            set { this._NgayVanDon = value; }
            get { return this._NgayVanDon; }
        }

        public bool HangRoi
        {
            set { this._HangRoi = value; }
            get { return this._HangRoi; }
        }

        public string SoHieuPTVT
        {
            set { this._SoHieuPTVT = value; }
            get { return this._SoHieuPTVT; }
        }

        public DateTime NgayDenPTVT
        {
            set { this._NgayDenPTVT = value; }
            get { return this._NgayDenPTVT; }
        }

        public string MaHangVT
        {
            set { this._MaHangVT = value; }
            get { return this._MaHangVT; }
        }

        public string TenHangVT
        {
            set { this._TenHangVT = value; }
            get { return this._TenHangVT; }
        }

        public string TenPTVT
        {
            set { this._TenPTVT = value; }
            get { return this._TenPTVT; }
        }

        public string QuocTichPTVT
        {
            set { this._QuocTichPTVT = value; }
            get { return this._QuocTichPTVT; }
        }

        public string NuocXuat_ID
        {
            set { this._NuocXuat_ID = value; }
            get { return this._NuocXuat_ID; }
        }

        public string MaNguoiNhanHang
        {
            set { this._MaNguoiNhanHang = value; }
            get { return this._MaNguoiNhanHang; }
        }

        public string TenNguoiNhanHang
        {
            set { this._TenNguoiNhanHang = value; }
            get { return this._TenNguoiNhanHang; }
        }

        public string MaNguoiGiaoHang
        {
            set { this._MaNguoiGiaoHang = value; }
            get { return this._MaNguoiGiaoHang; }
        }

        public string TenNguoiGiaoHang
        {
            set { this._TenNguoiGiaoHang = value; }
            get { return this._TenNguoiGiaoHang; }
        }

        public string CuaKhauNhap_ID
        {
            set { this._CuaKhauNhap_ID = value; }
            get { return this._CuaKhauNhap_ID; }
        }

        public string CuaKhauXuat
        {
            set { this._CuaKhauXuat = value; }
            get { return this._CuaKhauXuat; }
        }

        public string MaNguoiNhanHangTrungGian
        {
            set { this._MaNguoiNhanHangTrungGian = value; }
            get { return this._MaNguoiNhanHangTrungGian; }
        }

        public string TenNguoiNhanHangTrungGian
        {
            set { this._TenNguoiNhanHangTrungGian = value; }
            get { return this._TenNguoiNhanHangTrungGian; }
        }

        public string MaCangXepHang
        {
            set { this._MaCangXepHang = value; }
            get { return this._MaCangXepHang; }
        }

        public string TenCangXepHang
        {
            set { this._TenCangXepHang = value; }
            get { return this._TenCangXepHang; }
        }

        public string MaCangDoHang
        {
            set { this._MaCangDoHang = value; }
            get { return this._MaCangDoHang; }
        }

        public string TenCangDoHang
        {
            set { this._TenCangDoHang = value; }
            get { return this._TenCangDoHang; }
        }

        public long TKMD_ID
        {
            set { this._TKMD_ID = value; }
            get { return this._TKMD_ID; }
        }

        public string DKGH_ID
        {
            set { this._DKGH_ID = value; }
            get { return this._DKGH_ID; }
        }

        public string DiaDiemGiaoHang
        {
            set { this._DiaDiemGiaoHang = value; }
            get { return this._DiaDiemGiaoHang; }
        }

        public string NoiDi
        {
            set { this._NoiDi = value; }
            get { return this._NoiDi; }
        }

        public string SoHieuChuyenDi
        {
            set { this._SoHieuChuyenDi = value; }
            get { return this._SoHieuChuyenDi; }
        }

        public DateTime NgayKhoiHanh
        {
            set { this._NgayKhoiHanh = value; }
            get { return this._NgayKhoiHanh; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static VanDon Load(long iD)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            VanDon entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new VanDon();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("HangRoi"))) entity.HangRoi = reader.GetBoolean(reader.GetOrdinal("HangRoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangVT"))) entity.MaHangVT = reader.GetString(reader.GetOrdinal("MaHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangVT"))) entity.TenHangVT = reader.GetString(reader.GetOrdinal("TenHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenPTVT"))) entity.TenPTVT = reader.GetString(reader.GetOrdinal("TenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT"))) entity.QuocTichPTVT = reader.GetString(reader.GetOrdinal("QuocTichPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXuat_ID"))) entity.NuocXuat_ID = reader.GetString(reader.GetOrdinal("NuocXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauNhap_ID"))) entity.CuaKhauNhap_ID = reader.GetString(reader.GetOrdinal("CuaKhauNhap_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauXuat"))) entity.CuaKhauXuat = reader.GetString(reader.GetOrdinal("CuaKhauXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHangTrungGian"))) entity.MaNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("MaNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHangTrungGian"))) entity.TenNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("TenNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangXepHang"))) entity.MaCangXepHang = reader.GetString(reader.GetOrdinal("MaCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangXepHang"))) entity.TenCangXepHang = reader.GetString(reader.GetOrdinal("TenCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangDoHang"))) entity.TenCangDoHang = reader.GetString(reader.GetOrdinal("TenCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDi"))) entity.NoiDi = reader.GetString(reader.GetOrdinal("NoiDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------
        public static List<VanDon> SelectCollectionAll()
        {
            List<VanDon> collection = new List<VanDon>();
            SqlDataReader reader = SelectReaderAll();
            while (reader.Read())
            {
                VanDon entity = new VanDon();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("HangRoi"))) entity.HangRoi = reader.GetBoolean(reader.GetOrdinal("HangRoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangVT"))) entity.MaHangVT = reader.GetString(reader.GetOrdinal("MaHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangVT"))) entity.TenHangVT = reader.GetString(reader.GetOrdinal("TenHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenPTVT"))) entity.TenPTVT = reader.GetString(reader.GetOrdinal("TenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT"))) entity.QuocTichPTVT = reader.GetString(reader.GetOrdinal("QuocTichPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXuat_ID"))) entity.NuocXuat_ID = reader.GetString(reader.GetOrdinal("NuocXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauNhap_ID"))) entity.CuaKhauNhap_ID = reader.GetString(reader.GetOrdinal("CuaKhauNhap_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauXuat"))) entity.CuaKhauXuat = reader.GetString(reader.GetOrdinal("CuaKhauXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHangTrungGian"))) entity.MaNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("MaNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHangTrungGian"))) entity.TenNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("TenNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangXepHang"))) entity.MaCangXepHang = reader.GetString(reader.GetOrdinal("MaCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangXepHang"))) entity.TenCangXepHang = reader.GetString(reader.GetOrdinal("TenCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangDoHang"))) entity.TenCangDoHang = reader.GetString(reader.GetOrdinal("TenCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDi"))) entity.NoiDi = reader.GetString(reader.GetOrdinal("NoiDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public static List<VanDon> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            List<VanDon> collection = new List<VanDon>();

            SqlDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                VanDon entity = new VanDon();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("HangRoi"))) entity.HangRoi = reader.GetBoolean(reader.GetOrdinal("HangRoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangVT"))) entity.MaHangVT = reader.GetString(reader.GetOrdinal("MaHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangVT"))) entity.TenHangVT = reader.GetString(reader.GetOrdinal("TenHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenPTVT"))) entity.TenPTVT = reader.GetString(reader.GetOrdinal("TenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT"))) entity.QuocTichPTVT = reader.GetString(reader.GetOrdinal("QuocTichPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXuat_ID"))) entity.NuocXuat_ID = reader.GetString(reader.GetOrdinal("NuocXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauNhap_ID"))) entity.CuaKhauNhap_ID = reader.GetString(reader.GetOrdinal("CuaKhauNhap_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauXuat"))) entity.CuaKhauXuat = reader.GetString(reader.GetOrdinal("CuaKhauXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHangTrungGian"))) entity.MaNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("MaNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHangTrungGian"))) entity.TenNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("TenNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangXepHang"))) entity.MaCangXepHang = reader.GetString(reader.GetOrdinal("MaCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangXepHang"))) entity.TenCangXepHang = reader.GetString(reader.GetOrdinal("TenCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangDoHang"))) entity.TenCangDoHang = reader.GetString(reader.GetOrdinal("TenCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDi"))) entity.NoiDi = reader.GetString(reader.GetOrdinal("NoiDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static List<VanDon> SelectCollectionBy_TKMD_ID(long tKMD_ID)
        {
            List<VanDon> collection = new List<VanDon>();
            SqlDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
            while (reader.Read())
            {
                VanDon entity = new VanDon();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("HangRoi"))) entity.HangRoi = reader.GetBoolean(reader.GetOrdinal("HangRoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangVT"))) entity.MaHangVT = reader.GetString(reader.GetOrdinal("MaHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangVT"))) entity.TenHangVT = reader.GetString(reader.GetOrdinal("TenHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenPTVT"))) entity.TenPTVT = reader.GetString(reader.GetOrdinal("TenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT"))) entity.QuocTichPTVT = reader.GetString(reader.GetOrdinal("QuocTichPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXuat_ID"))) entity.NuocXuat_ID = reader.GetString(reader.GetOrdinal("NuocXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauNhap_ID"))) entity.CuaKhauNhap_ID = reader.GetString(reader.GetOrdinal("CuaKhauNhap_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauXuat"))) entity.CuaKhauXuat = reader.GetString(reader.GetOrdinal("CuaKhauXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHangTrungGian"))) entity.MaNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("MaNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHangTrungGian"))) entity.TenNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("TenNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangXepHang"))) entity.MaCangXepHang = reader.GetString(reader.GetOrdinal("MaCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangXepHang"))) entity.TenCangXepHang = reader.GetString(reader.GetOrdinal("TenCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangDoHang"))) entity.TenCangDoHang = reader.GetString(reader.GetOrdinal("TenCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDi"))) entity.NoiDi = reader.GetString(reader.GetOrdinal("NoiDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static SqlDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static SqlDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static SqlDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "p_KDT_VanDon_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertVanDon(string soVanDon, DateTime ngayVanDon, bool hangRoi, string soHieuPTVT, DateTime ngayDenPTVT, string maHangVT, string tenHangVT, string tenPTVT, string quocTichPTVT, string nuocXuat_ID, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string cuaKhauNhap_ID, string cuaKhauXuat, string maNguoiNhanHangTrungGian, string tenNguoiNhanHangTrungGian, string maCangXepHang, string tenCangXepHang, string maCangDoHang, string tenCangDoHang, long tKMD_ID, string dKGH_ID, string diaDiemGiaoHang, string noiDi, string soHieuChuyenDi, DateTime ngayKhoiHanh)
        {
            VanDon entity = new VanDon();
            entity.SoVanDon = soVanDon;
            entity.NgayVanDon = ngayVanDon;
            entity.HangRoi = hangRoi;
            entity.SoHieuPTVT = soHieuPTVT;
            entity.NgayDenPTVT = ngayDenPTVT;
            entity.MaHangVT = maHangVT;
            entity.TenHangVT = tenHangVT;
            entity.TenPTVT = tenPTVT;
            entity.QuocTichPTVT = quocTichPTVT;
            entity.NuocXuat_ID = nuocXuat_ID;
            entity.MaNguoiNhanHang = maNguoiNhanHang;
            entity.TenNguoiNhanHang = tenNguoiNhanHang;
            entity.MaNguoiGiaoHang = maNguoiGiaoHang;
            entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
            entity.CuaKhauNhap_ID = cuaKhauNhap_ID;
            entity.CuaKhauXuat = cuaKhauXuat;
            entity.MaNguoiNhanHangTrungGian = maNguoiNhanHangTrungGian;
            entity.TenNguoiNhanHangTrungGian = tenNguoiNhanHangTrungGian;
            entity.MaCangXepHang = maCangXepHang;
            entity.TenCangXepHang = tenCangXepHang;
            entity.MaCangDoHang = maCangDoHang;
            entity.TenCangDoHang = tenCangDoHang;
            entity.TKMD_ID = tKMD_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.DiaDiemGiaoHang = diaDiemGiaoHang;
            entity.NoiDi = noiDi;
            entity.SoHieuChuyenDi = soHieuChuyenDi;
            entity.NgayKhoiHanh = ngayKhoiHanh;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object)NgayKhoiHanh);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<VanDon> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (VanDon item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateVanDon(long iD, string soVanDon, DateTime ngayVanDon, bool hangRoi, string soHieuPTVT, DateTime ngayDenPTVT, string maHangVT, string tenHangVT, string tenPTVT, string quocTichPTVT, string nuocXuat_ID, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string cuaKhauNhap_ID, string cuaKhauXuat, string maNguoiNhanHangTrungGian, string tenNguoiNhanHangTrungGian, string maCangXepHang, string tenCangXepHang, string maCangDoHang, string tenCangDoHang, long tKMD_ID, string dKGH_ID, string diaDiemGiaoHang, string noiDi, string soHieuChuyenDi, DateTime ngayKhoiHanh)
        {
            VanDon entity = new VanDon();
            entity.SoVanDon = soVanDon;
            entity.NgayVanDon = ngayVanDon;
            entity.HangRoi = hangRoi;
            entity.SoHieuPTVT = soHieuPTVT;
            entity.NgayDenPTVT = ngayDenPTVT;
            entity.MaHangVT = maHangVT;
            entity.TenHangVT = tenHangVT;
            entity.TenPTVT = tenPTVT;
            entity.QuocTichPTVT = quocTichPTVT;
            entity.NuocXuat_ID = nuocXuat_ID;
            entity.MaNguoiNhanHang = maNguoiNhanHang;
            entity.TenNguoiNhanHang = tenNguoiNhanHang;
            entity.MaNguoiGiaoHang = maNguoiGiaoHang;
            entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
            entity.CuaKhauNhap_ID = cuaKhauNhap_ID;
            entity.CuaKhauXuat = cuaKhauXuat;
            entity.MaNguoiNhanHangTrungGian = maNguoiNhanHangTrungGian;
            entity.TenNguoiNhanHangTrungGian = tenNguoiNhanHangTrungGian;
            entity.MaCangXepHang = maCangXepHang;
            entity.TenCangXepHang = tenCangXepHang;
            entity.MaCangDoHang = maCangDoHang;
            entity.TenCangDoHang = tenCangDoHang;
            entity.TKMD_ID = tKMD_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.DiaDiemGiaoHang = diaDiemGiaoHang;
            entity.NoiDi = noiDi;
            entity.SoHieuChuyenDi = soHieuChuyenDi;
            entity.NgayKhoiHanh = ngayKhoiHanh;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_VanDon_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year == 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year == 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year == 1753 ? DBNull.Value : (object)NgayKhoiHanh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<VanDon> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (VanDon item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateVanDon(long iD, string soVanDon, DateTime ngayVanDon, bool hangRoi, string soHieuPTVT, DateTime ngayDenPTVT, string maHangVT, string tenHangVT, string tenPTVT, string quocTichPTVT, string nuocXuat_ID, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string cuaKhauNhap_ID, string cuaKhauXuat, string maNguoiNhanHangTrungGian, string tenNguoiNhanHangTrungGian, string maCangXepHang, string tenCangXepHang, string maCangDoHang, string tenCangDoHang, long tKMD_ID, string dKGH_ID, string diaDiemGiaoHang, string noiDi, string soHieuChuyenDi, DateTime ngayKhoiHanh)
        {
            VanDon entity = new VanDon();
            entity.ID = iD;
            entity.SoVanDon = soVanDon;
            entity.NgayVanDon = ngayVanDon;
            entity.HangRoi = hangRoi;
            entity.SoHieuPTVT = soHieuPTVT;
            entity.NgayDenPTVT = ngayDenPTVT;
            entity.MaHangVT = maHangVT;
            entity.TenHangVT = tenHangVT;
            entity.TenPTVT = tenPTVT;
            entity.QuocTichPTVT = quocTichPTVT;
            entity.NuocXuat_ID = nuocXuat_ID;
            entity.MaNguoiNhanHang = maNguoiNhanHang;
            entity.TenNguoiNhanHang = tenNguoiNhanHang;
            entity.MaNguoiGiaoHang = maNguoiGiaoHang;
            entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
            entity.CuaKhauNhap_ID = cuaKhauNhap_ID;
            entity.CuaKhauXuat = cuaKhauXuat;
            entity.MaNguoiNhanHangTrungGian = maNguoiNhanHangTrungGian;
            entity.TenNguoiNhanHangTrungGian = tenNguoiNhanHangTrungGian;
            entity.MaCangXepHang = maCangXepHang;
            entity.TenCangXepHang = tenCangXepHang;
            entity.MaCangDoHang = maCangDoHang;
            entity.TenCangDoHang = tenCangDoHang;
            entity.TKMD_ID = tKMD_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.DiaDiemGiaoHang = diaDiemGiaoHang;
            entity.NoiDi = noiDi;
            entity.SoHieuChuyenDi = soHieuChuyenDi;
            entity.NgayKhoiHanh = ngayKhoiHanh;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year == 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year == 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year == 1753 ? DBNull.Value : (object)NgayKhoiHanh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<VanDon> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (VanDon item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteVanDon(long iD)
        {
            VanDon entity = new VanDon();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMD_ID(long tKMD_ID)
        {
            string spName = "[dbo].[p_KDT_VanDon_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<VanDon> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (VanDon item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}