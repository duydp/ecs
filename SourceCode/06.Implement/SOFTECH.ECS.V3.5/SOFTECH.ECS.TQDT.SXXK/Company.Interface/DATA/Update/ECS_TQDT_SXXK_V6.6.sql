


/****** Object:  Table [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]    Script Date: 01/28/2013 14:34:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]') AND type in (N'U'))
DROP TABLE [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]
GO


/****** Object:  Table [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]    Script Date: 01/28/2013 14:34:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra](
	[ID] [varchar](3) NOT NULL,
	[TenChungTu] [nvarchar](255) NULL,
	[MoTaKhac] [nvarchar](2000) NULL,
 CONSTRAINT [PK_t_HaiQuan_LoaiChungTuGiayKiemTra] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO





-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Insert]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Update]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Delete]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Load]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, January 28, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Insert]
	@ID varchar(3),
	@TenChungTu nvarchar(255),
	@MoTaKhac nvarchar(2000)
AS
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]
(
	[ID],
	[TenChungTu],
	[MoTaKhac]
)
VALUES
(
	@ID,
	@TenChungTu,
	@MoTaKhac
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, January 28, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Update]
	@ID varchar(3),
	@TenChungTu nvarchar(255),
	@MoTaKhac nvarchar(2000)
AS

UPDATE
	[dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]
SET
	[TenChungTu] = @TenChungTu,
	[MoTaKhac] = @MoTaKhac
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, January 28, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_InsertUpdate]
	@ID varchar(3),
	@TenChungTu nvarchar(255),
	@MoTaKhac nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] 
		SET
			[TenChungTu] = @TenChungTu,
			[MoTaKhac] = @MoTaKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]
	(
			[ID],
			[TenChungTu],
			[MoTaKhac]
	)
	VALUES
	(
			@ID,
			@TenChungTu,
			@MoTaKhac
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, January 28, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Delete]
	@ID varchar(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, January 28, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, January 28, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_Load]
	@ID varchar(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenChungTu],
	[MoTaKhac]
FROM
	[dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, January 28, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TenChungTu],
	[MoTaKhac]
FROM [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, January 28, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiChungTuGiayKiemTra_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenChungTu],
	[MoTaKhac]
FROM
	[dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]	

GO

/*
Run this script on:

192.168.72.100.ECS_TQDT_KD_V4_test    -  This database will be modified

to synchronize it with:

192.168.72.100.ECS_TQDT_KD_V4

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.1.0 from Red Gate Software Ltd at 01/28/2013 2:38:23 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Add 28 rows to [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra]
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('007', N'Giấy chứng nhận xuất xứ hàng hóa (C/O - Form D)', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('008', N'Giấy chứng nhận xuất xứ hàng hóa (C/O - Form E)', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('315', N'Hợp đồng', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('380', N'Hóa đơn thương mại (Commercial invoice)', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('400', N'Giấy đăng ký kiểm tra chất lượng, kiểm tra vệ sinh an toàn thực phẩm, kiểm dịch động thực vật', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('401', N'Giấy thông báo kết quả kiểm tra chất lượng, kiểm tra vệ sinh an toàn thực phẩm, kiểm dịch động thực vật', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('402', N'Giấy thông báo kết quả kiểm tra', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('403', N'Chứng thư giám định', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('404', N'Giấy nộp tiền', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('405', N'Đề nghị chuyển cửa khẩu', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('704', N'Master bill of lading', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('706', N'Bill of lading bản gốc original', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('707', N'Bill of lading bản copy', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('708', N'Bill container rỗng (Empty container bill)', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('709', N'Bill hàng xá, hàng rời (Tanker bill of lading)', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('710', N'Sea waybill', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('714', N'House bill of lading', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('740', N'Air waybill', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('741', N'Master air waybill', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('743', N'Substitute air waybill', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('750', N'Packing list', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('811', N'Giấy phép xuất khẩu (Export licence)', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('861', N'Giấy chứng nhận xuất xứ hàng hóa (C/O - Form D)', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('865', N'Giấy chứng nhận xuất xứ hàng hóa (C/O - Form A)', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('911', N'Giấy phép nhập khẩu (Import licence)', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('929', N'Tờ khai nhập khẩu thương mại', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('930', N'Tờ khai xuất khẩu thương mại', NULL)
INSERT INTO [dbo].[t_HaiQuan_LoaiChungTuGiayKiemTra] ([ID], [TenChungTu], [MoTaKhac]) VALUES ('999', N'Loại khác', NULL)
COMMIT TRANSACTION
GO
UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '6.6'
      ,[Date] = getdate()
      ,[Notes] = ''