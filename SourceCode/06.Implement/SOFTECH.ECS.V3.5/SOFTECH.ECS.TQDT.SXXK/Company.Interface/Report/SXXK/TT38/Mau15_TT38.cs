﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class Mau15_TT38 : DevExpress.XtraReports.UI.XtraReport
    {
        public DataTable dtTable = new DataTable();
        public Mau15_TT38()
        {
            InitializeComponent();
        }
        public void BindReport(bool isLuong, DateTime ngayChotTon)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();

            lbMaSo.Text = GlobalSettings.MA_DON_VI;
            lbDiaChi.Text = GlobalSettings.DIA_CHI;
            lbTenToChuc.Text = GlobalSettings.TEN_DON_VI;

            DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontBCXNT);
            Int32 index = 0;
            string MaHang = "";
            string TaiKhoan = "152";
            if (!dtTable.Columns.Contains("STT"))
            {
                dtTable.Columns.Add(new DataColumn("STT", typeof(System.Int32)));
            }
            for (int i = 0; i < dtTable.Rows.Count; i++)
            {
                if (!dtTable.Rows[i]["TenNPL"].ToString().Contains("/" + dtTable.Rows[i]["MaNPL"].ToString()))
                    dtTable.Rows[i]["TenNPL"] = dtTable.Rows[i]["TenNPL"] + "/" + dtTable.Rows[i]["MaNPL"].ToString();

                if (dtTable.Rows[i]["SoTaiKhoan"].ToString() != TaiKhoan)
                    index = 0;
                TaiKhoan = dtTable.Rows[i]["SoTaiKhoan"].ToString();
                if (dtTable.Rows[i]["MaNPL"].ToString() != MaHang)
                {
                    index++;
                }

                MaHang = dtTable.Rows[i]["MaNPL"].ToString();
                dtTable.Rows[i]["STT"] = index;
            }
            //for (int i = 0; i < dtTable.Rows.Count; i++)
            //{
            //    decimal a = Convert.ToDecimal(dtTable.Rows[i]["LuongNhapTrongKy"]);
            //    decimal b = Convert.ToDecimal(dtTable.Rows[i]["TonDauKy"]);
            //    decimal c = Convert.ToDecimal(dtTable.Rows[i]["LuongXuatTrongKy"]);
            //    decimal d = a + b - c;
            //    dtTable.Rows[i]["TonCuoiKy"] = d.ToString();// (Convert.ToDecimal(dtTable.Rows[i]["LuongNhapTrongKy"]) + Convert.ToDecimal(dtTable.Rows[i]["TonDauKy"]) - Convert.ToDecimal(dtTable.Rows[i]["LuongXuatTrongKy"])).ToString();
            //    dtTable.Rows[i]["TriGiaTTTonCuoiKy"] = (Convert.ToDecimal(dtTable.Rows[i]["TriGiaTTTonDauKy"]) + Convert.ToDecimal(dtTable.Rows[i]["TriGiaTTNhapTrongKy"]) - Convert.ToDecimal(dtTable.Rows[i]["TriGiaTTXuatTrongKy"])).ToString();
            //}
            //dtTable.AcceptChanges();
            DetailReport.DataSource = dtTable;


            lbTenToChuc.Text = GlobalSettings.TEN_DON_VI;
            lbMaSo.Text = GlobalSettings.MA_DON_VI;
            lbDiaChi.Text = GlobalSettings.DIA_CHI;

            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("SoTaiKhoan")
            });
            lblSoTaiKhoan.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");
            //lbNam.DataBindings.Add("Text",DetailReport.DataSource,"NamDangKy");
            txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            //txtTaiKhoan.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");
            txtTen.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
            //txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MaNPL");
            if (isLuong)
            {
                lblTonDauKy_TieuDe.Text = "Tồn Đầu Kỳ (" + ngayChotTon.ToString("dd/MM/yyyy") + ")";
                lblSoTien.Text = "Số Lượng";
                txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "TonDauKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongNhapTrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongXuatTrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TonCuoiKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
            }
            else
            {
                lblTonDauKy_TieuDe.Text = "Tồn Đầu Kỳ (" + ngayChotTon.ToString("dd/MM/yyyy") + ")";
                lblSoTien.Text = "Số Lượng";
                txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTTTonDauKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTTNhapTrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTTXuatTrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTTTonCuoiKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
            }

        }

        private void lblSoTaiKhoan_AfterPrint(object sender, EventArgs e)
        {
            
            
        }

        private void lblSoTaiKhoan_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblSoTaiKhoan.Text == "152")
            {
                lblSoTaiKhoan.Text = "I.152";
                lblTenTaiKhoan.Text = "Nguyên liệu, vật liệu nhập khẩu";
                
            }
            else
            {
                lblSoTaiKhoan.Text = "II.155";
                lblTenTaiKhoan.Text = "Thành phẩm được sản xuất từ nguồn nhập khẩu";
            }
        }

        private void lblTenTaiKhoan_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblSoTaiKhoan.Text == "I.152")
            {
                lblTenTaiKhoan.Text = "Nguyên liệu, vật liệu nhập khẩu";
            }
            else
            {
                lblTenTaiKhoan.Text = "Thành phẩm được sản xuất từ nguồn nhập khẩu";
            }
        }

        private void STT_TK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblTenTaiKhoan_AfterPrint(object sender, EventArgs e)
        {

        }

    }
}
