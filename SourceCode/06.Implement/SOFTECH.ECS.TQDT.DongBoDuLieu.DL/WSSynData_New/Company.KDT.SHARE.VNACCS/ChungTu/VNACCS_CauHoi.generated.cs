using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACCS_CauHoi : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string TieuDe { set; get; }
		public string NoiDung { set; get; }
		public string TraLoi { set; get; }
		public int IDCauHoi { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACCS_CauHoi> ConvertToCollection(IDataReader reader)
		{
			List<VNACCS_CauHoi> collection = new List<VNACCS_CauHoi>();
			while (reader.Read())
			{
				VNACCS_CauHoi entity = new VNACCS_CauHoi();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDe"))) entity.TieuDe = reader.GetString(reader.GetOrdinal("TieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TraLoi"))) entity.TraLoi = reader.GetString(reader.GetOrdinal("TraLoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDCauHoi"))) entity.IDCauHoi = reader.GetInt32(reader.GetOrdinal("IDCauHoi"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<VNACCS_CauHoi> collection, long id)
        {
            foreach (VNACCS_CauHoi item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACCS_CauHoi VALUES(@MaDoanhNghiep, @TieuDe, @NoiDung, @TraLoi, @IDCauHoi)";
            string update = "UPDATE t_VNACCS_CauHoi SET MaDoanhNghiep = @MaDoanhNghiep, TieuDe = @TieuDe, NoiDung = @NoiDung, TraLoi = @TraLoi, IDCauHoi = @IDCauHoi WHERE ID = @ID";
            string delete = "DELETE FROM t_VNACCS_CauHoi WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDung", SqlDbType.NVarChar, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TraLoi", SqlDbType.NVarChar, "TraLoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IDCauHoi", SqlDbType.Int, "IDCauHoi", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDung", SqlDbType.NVarChar, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TraLoi", SqlDbType.NVarChar, "TraLoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IDCauHoi", SqlDbType.Int, "IDCauHoi", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACCS_CauHoi VALUES(@MaDoanhNghiep, @TieuDe, @NoiDung, @TraLoi, @IDCauHoi)";
            string update = "UPDATE t_VNACCS_CauHoi SET MaDoanhNghiep = @MaDoanhNghiep, TieuDe = @TieuDe, NoiDung = @NoiDung, TraLoi = @TraLoi, IDCauHoi = @IDCauHoi WHERE ID = @ID";
            string delete = "DELETE FROM t_VNACCS_CauHoi WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDung", SqlDbType.NVarChar, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TraLoi", SqlDbType.NVarChar, "TraLoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IDCauHoi", SqlDbType.Int, "IDCauHoi", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDung", SqlDbType.NVarChar, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TraLoi", SqlDbType.NVarChar, "TraLoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IDCauHoi", SqlDbType.Int, "IDCauHoi", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACCS_CauHoi Load(long id)
		{
			const string spName = "[dbo].[p_VNACCS_CauHoi_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACCS_CauHoi> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACCS_CauHoi> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACCS_CauHoi> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACCS_CauHoi_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACCS_CauHoi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACCS_CauHoi_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACCS_CauHoi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertVNACCS_CauHoi(string maDoanhNghiep, string tieuDe, string noiDung, string traLoi, int iDCauHoi)
		{
			VNACCS_CauHoi entity = new VNACCS_CauHoi();	
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TieuDe = tieuDe;
			entity.NoiDung = noiDung;
			entity.TraLoi = traLoi;
			entity.IDCauHoi = iDCauHoi;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACCS_CauHoi_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
			db.AddInParameter(dbCommand, "@TraLoi", SqlDbType.NVarChar, TraLoi);
			db.AddInParameter(dbCommand, "@IDCauHoi", SqlDbType.Int, IDCauHoi);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACCS_CauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_CauHoi item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACCS_CauHoi(long id, string maDoanhNghiep, string tieuDe, string noiDung, string traLoi, int iDCauHoi)
		{
			VNACCS_CauHoi entity = new VNACCS_CauHoi();			
			entity.ID = id;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TieuDe = tieuDe;
			entity.NoiDung = noiDung;
			entity.TraLoi = traLoi;
			entity.IDCauHoi = iDCauHoi;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACCS_CauHoi_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
			db.AddInParameter(dbCommand, "@TraLoi", SqlDbType.NVarChar, TraLoi);
			db.AddInParameter(dbCommand, "@IDCauHoi", SqlDbType.Int, IDCauHoi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACCS_CauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_CauHoi item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACCS_CauHoi(long id, string maDoanhNghiep, string tieuDe, string noiDung, string traLoi, int iDCauHoi)
		{
			VNACCS_CauHoi entity = new VNACCS_CauHoi();			
			entity.ID = id;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TieuDe = tieuDe;
			entity.NoiDung = noiDung;
			entity.TraLoi = traLoi;
			entity.IDCauHoi = iDCauHoi;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACCS_CauHoi_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
			db.AddInParameter(dbCommand, "@TraLoi", SqlDbType.NVarChar, TraLoi);
			db.AddInParameter(dbCommand, "@IDCauHoi", SqlDbType.Int, IDCauHoi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACCS_CauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_CauHoi item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACCS_CauHoi(long id)
		{
			VNACCS_CauHoi entity = new VNACCS_CauHoi();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACCS_CauHoi_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACCS_CauHoi_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACCS_CauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_CauHoi item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}