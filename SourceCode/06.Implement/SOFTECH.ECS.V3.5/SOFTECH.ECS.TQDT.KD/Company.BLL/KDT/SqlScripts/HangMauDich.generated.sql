-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_HangMauDich_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangMauDich_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_HangMauDich_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangMauDich_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_HangMauDich_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_HangMauDich_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangMauDich_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_HangMauDich_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangMauDich_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_HangMauDich_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangMauDich_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_HangMauDich_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangMauDich_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_HangMauDich_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangMauDich_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_Insert]
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueXNK float,
	@ThueSuatXNK float,
	@ThueSuatXNKGiam float,
	@ThueTTDB float,
	@ThueSuatTTDB float,
	@ThueSuatTTDBGiam float,
	@ThueGTGT float,
	@ThueSuatGTGT float,
	@ThueSuatVATGiam float,
	@ThueBVMT float,
	@ThueSuatBVMT float,
	@ThueSuatBVMTGiam float,
	@ThueChongPhaGia float,
	@ThueSuatChongPhaGia float,
	@ThueSuatChongPhaGiaGiam float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@FOC bit,
	@ThueTuyetDoi bit,
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@isHangCu bit,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HangMauDich]
(
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueXNK],
	[ThueSuatXNK],
	[ThueSuatXNKGiam],
	[ThueTTDB],
	[ThueSuatTTDB],
	[ThueSuatTTDBGiam],
	[ThueGTGT],
	[ThueSuatGTGT],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[isHangCu]
)
VALUES 
(
	@TKMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@TrongLuong,
	@DonGiaKB,
	@DonGiaTT,
	@TriGiaKB,
	@TriGiaTT,
	@TriGiaKB_VND,
	@ThueXNK,
	@ThueSuatXNK,
	@ThueSuatXNKGiam,
	@ThueTTDB,
	@ThueSuatTTDB,
	@ThueSuatTTDBGiam,
	@ThueGTGT,
	@ThueSuatGTGT,
	@ThueSuatVATGiam,
	@ThueBVMT,
	@ThueSuatBVMT,
	@ThueSuatBVMTGiam,
	@ThueChongPhaGia,
	@ThueSuatChongPhaGia,
	@ThueSuatChongPhaGiaGiam,
	@PhuThu,
	@TyLeThuKhac,
	@TriGiaThuKhac,
	@MienThue,
	@Ma_HTS,
	@DVT_HTS,
	@SoLuong_HTS,
	@FOC,
	@ThueTuyetDoi,
	@DonGiaTuyetDoi,
	@MaHSMoRong,
	@NhanHieu,
	@QuyCachPhamChat,
	@ThanhPhan,
	@Model,
	@MaHangSX,
	@TenHangSX,
	@isHangCu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueXNK float,
	@ThueSuatXNK float,
	@ThueSuatXNKGiam float,
	@ThueTTDB float,
	@ThueSuatTTDB float,
	@ThueSuatTTDBGiam float,
	@ThueGTGT float,
	@ThueSuatGTGT float,
	@ThueSuatVATGiam float,
	@ThueBVMT float,
	@ThueSuatBVMT float,
	@ThueSuatBVMTGiam float,
	@ThueChongPhaGia float,
	@ThueSuatChongPhaGia float,
	@ThueSuatChongPhaGiaGiam float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@FOC bit,
	@ThueTuyetDoi bit,
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@isHangCu bit
AS

UPDATE
	[dbo].[t_KDT_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[TrongLuong] = @TrongLuong,
	[DonGiaKB] = @DonGiaKB,
	[DonGiaTT] = @DonGiaTT,
	[TriGiaKB] = @TriGiaKB,
	[TriGiaTT] = @TriGiaTT,
	[TriGiaKB_VND] = @TriGiaKB_VND,
	[ThueXNK] = @ThueXNK,
	[ThueSuatXNK] = @ThueSuatXNK,
	[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
	[ThueTTDB] = @ThueTTDB,
	[ThueSuatTTDB] = @ThueSuatTTDB,
	[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
	[ThueGTGT] = @ThueGTGT,
	[ThueSuatGTGT] = @ThueSuatGTGT,
	[ThueSuatVATGiam] = @ThueSuatVATGiam,
	[ThueBVMT] = @ThueBVMT,
	[ThueSuatBVMT] = @ThueSuatBVMT,
	[ThueSuatBVMTGiam] = @ThueSuatBVMTGiam,
	[ThueChongPhaGia] = @ThueChongPhaGia,
	[ThueSuatChongPhaGia] = @ThueSuatChongPhaGia,
	[ThueSuatChongPhaGiaGiam] = @ThueSuatChongPhaGiaGiam,
	[PhuThu] = @PhuThu,
	[TyLeThuKhac] = @TyLeThuKhac,
	[TriGiaThuKhac] = @TriGiaThuKhac,
	[MienThue] = @MienThue,
	[Ma_HTS] = @Ma_HTS,
	[DVT_HTS] = @DVT_HTS,
	[SoLuong_HTS] = @SoLuong_HTS,
	[FOC] = @FOC,
	[ThueTuyetDoi] = @ThueTuyetDoi,
	[DonGiaTuyetDoi] = @DonGiaTuyetDoi,
	[MaHSMoRong] = @MaHSMoRong,
	[NhanHieu] = @NhanHieu,
	[QuyCachPhamChat] = @QuyCachPhamChat,
	[ThanhPhan] = @ThanhPhan,
	[Model] = @Model,
	[MaHangSX] = @MaHangSX,
	[TenHangSX] = @TenHangSX,
	[isHangCu] = @isHangCu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueXNK float,
	@ThueSuatXNK float,
	@ThueSuatXNKGiam float,
	@ThueTTDB float,
	@ThueSuatTTDB float,
	@ThueSuatTTDBGiam float,
	@ThueGTGT float,
	@ThueSuatGTGT float,
	@ThueSuatVATGiam float,
	@ThueBVMT float,
	@ThueSuatBVMT float,
	@ThueSuatBVMTGiam float,
	@ThueChongPhaGia float,
	@ThueSuatChongPhaGia float,
	@ThueSuatChongPhaGiaGiam float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@FOC bit,
	@ThueTuyetDoi bit,
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@isHangCu bit
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueXNK] = @ThueXNK,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
			[ThueTTDB] = @ThueTTDB,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
			[ThueGTGT] = @ThueGTGT,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueSuatVATGiam] = @ThueSuatVATGiam,
			[ThueBVMT] = @ThueBVMT,
			[ThueSuatBVMT] = @ThueSuatBVMT,
			[ThueSuatBVMTGiam] = @ThueSuatBVMTGiam,
			[ThueChongPhaGia] = @ThueChongPhaGia,
			[ThueSuatChongPhaGia] = @ThueSuatChongPhaGia,
			[ThueSuatChongPhaGiaGiam] = @ThueSuatChongPhaGiaGiam,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue,
			[Ma_HTS] = @Ma_HTS,
			[DVT_HTS] = @DVT_HTS,
			[SoLuong_HTS] = @SoLuong_HTS,
			[FOC] = @FOC,
			[ThueTuyetDoi] = @ThueTuyetDoi,
			[DonGiaTuyetDoi] = @DonGiaTuyetDoi,
			[MaHSMoRong] = @MaHSMoRong,
			[NhanHieu] = @NhanHieu,
			[QuyCachPhamChat] = @QuyCachPhamChat,
			[ThanhPhan] = @ThanhPhan,
			[Model] = @Model,
			[MaHangSX] = @MaHangSX,
			[TenHangSX] = @TenHangSX,
			[isHangCu] = @isHangCu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangMauDich]
		(
			[TKMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[TrongLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueXNK],
			[ThueSuatXNK],
			[ThueSuatXNKGiam],
			[ThueTTDB],
			[ThueSuatTTDB],
			[ThueSuatTTDBGiam],
			[ThueGTGT],
			[ThueSuatGTGT],
			[ThueSuatVATGiam],
			[ThueBVMT],
			[ThueSuatBVMT],
			[ThueSuatBVMTGiam],
			[ThueChongPhaGia],
			[ThueSuatChongPhaGia],
			[ThueSuatChongPhaGiaGiam],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue],
			[Ma_HTS],
			[DVT_HTS],
			[SoLuong_HTS],
			[FOC],
			[ThueTuyetDoi],
			[DonGiaTuyetDoi],
			[MaHSMoRong],
			[NhanHieu],
			[QuyCachPhamChat],
			[ThanhPhan],
			[Model],
			[MaHangSX],
			[TenHangSX],
			[isHangCu]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@TrongLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueXNK,
			@ThueSuatXNK,
			@ThueSuatXNKGiam,
			@ThueTTDB,
			@ThueSuatTTDB,
			@ThueSuatTTDBGiam,
			@ThueGTGT,
			@ThueSuatGTGT,
			@ThueSuatVATGiam,
			@ThueBVMT,
			@ThueSuatBVMT,
			@ThueSuatBVMTGiam,
			@ThueChongPhaGia,
			@ThueSuatChongPhaGia,
			@ThueSuatChongPhaGiaGiam,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue,
			@Ma_HTS,
			@DVT_HTS,
			@SoLuong_HTS,
			@FOC,
			@ThueTuyetDoi,
			@DonGiaTuyetDoi,
			@MaHSMoRong,
			@NhanHieu,
			@QuyCachPhamChat,
			@ThanhPhan,
			@Model,
			@MaHangSX,
			@TenHangSX,
			@isHangCu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HangMauDich]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HangMauDich]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HangMauDich] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueXNK],
	[ThueSuatXNK],
	[ThueSuatXNKGiam],
	[ThueTTDB],
	[ThueSuatTTDB],
	[ThueSuatTTDBGiam],
	[ThueGTGT],
	[ThueSuatGTGT],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[isHangCu]
FROM
	[dbo].[t_KDT_HangMauDich]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueXNK],
	[ThueSuatXNK],
	[ThueSuatXNKGiam],
	[ThueTTDB],
	[ThueSuatTTDB],
	[ThueSuatTTDBGiam],
	[ThueGTGT],
	[ThueSuatGTGT],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[isHangCu]
FROM
	[dbo].[t_KDT_HangMauDich]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueXNK],
	[ThueSuatXNK],
	[ThueSuatXNKGiam],
	[ThueTTDB],
	[ThueSuatTTDB],
	[ThueSuatTTDBGiam],
	[ThueGTGT],
	[ThueSuatGTGT],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[isHangCu]
FROM [dbo].[t_KDT_HangMauDich] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueXNK],
	[ThueSuatXNK],
	[ThueSuatXNKGiam],
	[ThueTTDB],
	[ThueSuatTTDB],
	[ThueSuatTTDBGiam],
	[ThueGTGT],
	[ThueSuatGTGT],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[isHangCu]
FROM
	[dbo].[t_KDT_HangMauDich]	

GO

