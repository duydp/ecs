﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class DinhDanhHangHoaManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem25 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem26 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem27 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem28 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem29 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem30 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem31 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem32 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem33 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem34 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem35 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem36 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DinhDanhHangHoaManagerForm));
            this.uiGroupBox25 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbLoaiHH = new Janus.Windows.EditControls.UIComboBox();
            this.cbbLoaiDoiTuong = new Janus.Windows.EditControls.UIComboBox();
            this.clcDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.cbStatus = new Janus.Windows.EditControls.UIComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.ctrCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSoTN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox25)).BeginInit();
            this.uiGroupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox25);
            this.grbMain.Size = new System.Drawing.Size(1108, 599);
            // 
            // uiGroupBox25
            // 
            this.uiGroupBox25.AutoScroll = true;
            this.uiGroupBox25.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox25.Controls.Add(this.cbbLoaiHH);
            this.uiGroupBox25.Controls.Add(this.cbbLoaiDoiTuong);
            this.uiGroupBox25.Controls.Add(this.clcDenNgay);
            this.uiGroupBox25.Controls.Add(this.clcTuNgay);
            this.uiGroupBox25.Controls.Add(this.label5);
            this.uiGroupBox25.Controls.Add(this.label1);
            this.uiGroupBox25.Controls.Add(this.cbStatus);
            this.uiGroupBox25.Controls.Add(this.label4);
            this.uiGroupBox25.Controls.Add(this.label3);
            this.uiGroupBox25.Controls.Add(this.label2);
            this.uiGroupBox25.Controls.Add(this.btnSearch);
            this.uiGroupBox25.Controls.Add(this.ctrCoQuanHaiQuan);
            this.uiGroupBox25.Controls.Add(this.label15);
            this.uiGroupBox25.Controls.Add(this.label14);
            this.uiGroupBox25.Controls.Add(this.txtSoTN);
            this.uiGroupBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox25.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox25.Name = "uiGroupBox25";
            this.uiGroupBox25.Size = new System.Drawing.Size(1108, 159);
            this.uiGroupBox25.TabIndex = 18;
            this.uiGroupBox25.Text = "Tìm kiếm";
            this.uiGroupBox25.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbLoaiHH
            // 
            uiComboBoxItem25.FormatStyle.Alpha = 0;
            uiComboBoxItem25.IsSeparator = false;
            uiComboBoxItem25.Text = "Nhập khẩu";
            uiComboBoxItem25.Value = "1";
            uiComboBoxItem26.FormatStyle.Alpha = 0;
            uiComboBoxItem26.IsSeparator = false;
            uiComboBoxItem26.Text = "Xuất khẩu";
            uiComboBoxItem26.Value = "2";
            uiComboBoxItem27.FormatStyle.Alpha = 0;
            uiComboBoxItem27.IsSeparator = false;
            uiComboBoxItem27.Text = "Hàng nội địa";
            uiComboBoxItem27.Value = "3";
            this.cbbLoaiHH.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem25,
            uiComboBoxItem26,
            uiComboBoxItem27});
            this.cbbLoaiHH.Location = new System.Drawing.Point(392, 124);
            this.cbbLoaiHH.Name = "cbbLoaiHH";
            this.cbbLoaiHH.Size = new System.Drawing.Size(149, 21);
            this.cbbLoaiHH.TabIndex = 71;
            this.cbbLoaiHH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiHH.SelectedValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // cbbLoaiDoiTuong
            // 
            uiComboBoxItem28.FormatStyle.Alpha = 0;
            uiComboBoxItem28.IsSeparator = false;
            uiComboBoxItem28.Text = "Doanh nghiệp xuất nhập khẩu";
            uiComboBoxItem28.Value = "1";
            uiComboBoxItem29.FormatStyle.Alpha = 0;
            uiComboBoxItem29.IsSeparator = false;
            uiComboBoxItem29.Text = "Doanh nghiệp kho, bãi, cảng";
            uiComboBoxItem29.Value = "2";
            this.cbbLoaiDoiTuong.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem28,
            uiComboBoxItem29});
            this.cbbLoaiDoiTuong.Location = new System.Drawing.Point(102, 124);
            this.cbbLoaiDoiTuong.Name = "cbbLoaiDoiTuong";
            this.cbbLoaiDoiTuong.Size = new System.Drawing.Size(149, 21);
            this.cbbLoaiDoiTuong.TabIndex = 70;
            this.cbbLoaiDoiTuong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiDoiTuong.SelectedValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // clcDenNgay
            // 
            this.clcDenNgay.AutoSize = false;
            this.clcDenNgay.BackColor = System.Drawing.Color.White;
            this.clcDenNgay.CustomFormat = "dd/MM/yyyy";
            this.clcDenNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDenNgay.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcDenNgay.DropDownCalendar.Name = "";
            this.clcDenNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcDenNgay.Location = new System.Drawing.Point(392, 89);
            this.clcDenNgay.Name = "clcDenNgay";
            this.clcDenNgay.Nullable = true;
            this.clcDenNgay.NullButtonText = "Xóa";
            this.clcDenNgay.ShowNullButton = true;
            this.clcDenNgay.Size = new System.Drawing.Size(149, 22);
            this.clcDenNgay.TabIndex = 69;
            this.clcDenNgay.TodayButtonText = "Hôm nay";
            this.clcDenNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.AutoSize = false;
            this.clcTuNgay.BackColor = System.Drawing.Color.White;
            this.clcTuNgay.CustomFormat = "dd/MM/yyyy";
            this.clcTuNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcTuNgay.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcTuNgay.DropDownCalendar.Name = "";
            this.clcTuNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcTuNgay.Location = new System.Drawing.Point(392, 57);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.Nullable = true;
            this.clcTuNgay.NullButtonText = "Xóa";
            this.clcTuNgay.ShowNullButton = true;
            this.clcTuNgay.Size = new System.Drawing.Size(149, 22);
            this.clcTuNgay.TabIndex = 68;
            this.clcTuNgay.TodayButtonText = "Hôm nay";
            this.clcTuNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(332, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 66;
            this.label5.Text = "Đến ngày";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(332, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 65;
            this.label1.Text = "Từ ngày";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(12, 12);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(90, 23);
            this.btnExportExcel.TabIndex = 67;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // cbStatus
            // 
            this.cbStatus.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem30.FormatStyle.Alpha = 0;
            uiComboBoxItem30.IsSeparator = false;
            uiComboBoxItem30.Text = "Chưa khai báo";
            uiComboBoxItem30.Value = "-1";
            uiComboBoxItem31.FormatStyle.Alpha = 0;
            uiComboBoxItem31.IsSeparator = false;
            uiComboBoxItem31.Text = "Đã khai báo";
            uiComboBoxItem31.Value = "-3";
            uiComboBoxItem32.FormatStyle.Alpha = 0;
            uiComboBoxItem32.IsSeparator = false;
            uiComboBoxItem32.Text = "Chờ duyệt";
            uiComboBoxItem32.Value = "0";
            uiComboBoxItem33.FormatStyle.Alpha = 0;
            uiComboBoxItem33.IsSeparator = false;
            uiComboBoxItem33.Text = "Đã duyệt";
            uiComboBoxItem33.Value = 1;
            uiComboBoxItem34.FormatStyle.Alpha = 0;
            uiComboBoxItem34.IsSeparator = false;
            uiComboBoxItem34.Text = "Không phê duyệt";
            uiComboBoxItem34.Value = "2";
            uiComboBoxItem35.FormatStyle.Alpha = 0;
            uiComboBoxItem35.IsSeparator = false;
            uiComboBoxItem35.Text = "Chờ hủy";
            uiComboBoxItem35.Value = "11";
            uiComboBoxItem36.FormatStyle.Alpha = 0;
            uiComboBoxItem36.IsSeparator = false;
            uiComboBoxItem36.Text = "Đã hủy";
            uiComboBoxItem36.Value = "10";
            this.cbStatus.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem30,
            uiComboBoxItem31,
            uiComboBoxItem32,
            uiComboBoxItem33,
            uiComboBoxItem34,
            uiComboBoxItem35,
            uiComboBoxItem36});
            this.cbStatus.Location = new System.Drawing.Point(102, 90);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(149, 21);
            this.cbStatus.TabIndex = 64;
            this.cbStatus.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbStatus.SelectedValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(260, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 13);
            this.label4.TabIndex = 63;
            this.label4.Text = "Loại thông tin hàng hóa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 63;
            this.label3.Text = "Loại đối tượng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 63;
            this.label2.Text = "Trạng thái";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(556, 123);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(90, 23);
            this.btnSearch.TabIndex = 62;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // ctrCoQuanHaiQuan
            // 
            this.ctrCoQuanHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrCoQuanHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrCoQuanHaiQuan.Code = "";
            this.ctrCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrCoQuanHaiQuan.IsOnlyWarning = false;
            this.ctrCoQuanHaiQuan.IsValidate = true;
            this.ctrCoQuanHaiQuan.Location = new System.Drawing.Point(102, 22);
            this.ctrCoQuanHaiQuan.Name = "ctrCoQuanHaiQuan";
            this.ctrCoQuanHaiQuan.Name_VN = "";
            this.ctrCoQuanHaiQuan.SetOnlyWarning = false;
            this.ctrCoQuanHaiQuan.SetValidate = false;
            this.ctrCoQuanHaiQuan.ShowColumnCode = true;
            this.ctrCoQuanHaiQuan.ShowColumnName = true;
            this.ctrCoQuanHaiQuan.Size = new System.Drawing.Size(416, 20);
            this.ctrCoQuanHaiQuan.TabIndex = 27;
            this.ctrCoQuanHaiQuan.TagCode = "";
            this.ctrCoQuanHaiQuan.TagName = "";
            this.ctrCoQuanHaiQuan.Where = null;
            this.ctrCoQuanHaiQuan.WhereCondition = "";
            this.ctrCoQuanHaiQuan.Leave += new System.EventHandler(this.btnSearch_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 62);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Số định danh";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Hải quan";
            // 
            // txtSoTN
            // 
            this.txtSoTN.Location = new System.Drawing.Point(102, 58);
            this.txtSoTN.Name = "txtSoTN";
            this.txtSoTN.Size = new System.Drawing.Size(149, 21);
            this.txtSoTN.TabIndex = 20;
            this.txtSoTN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTN.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.btnXoa);
            this.uiGroupBox5.Controls.Add(this.btnClose);
            this.uiGroupBox5.Controls.Add(this.btnExportExcel);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 558);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1108, 41);
            this.uiGroupBox5.TabIndex = 346;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(944, 12);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(74, 23);
            this.btnXoa.TabIndex = 71;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1024, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.Location = new System.Drawing.Point(0, 159);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.dgList.Size = new System.Drawing.Size(1108, 399);
            this.dgList.TabIndex = 347;
            this.dgList.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // DinhDanhHangHoaManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1108, 599);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "DinhDanhHangHoaManagerForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Theo dõi định danh hàng hóa";
            this.Load += new System.EventHandler(this.DinhDanhHangHoaManagerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox25)).EndInit();
            this.uiGroupBox25.ResumeLayout(false);
            this.uiGroupBox25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox25;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDenNgay;
        private Janus.Windows.CalendarCombo.CalendarCombo clcTuNgay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private Janus.Windows.EditControls.UIComboBox cbStatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTN;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHH;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiDoiTuong;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
    }
}