﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class DeclarationPhuKien
    {
        /// <summary>
        /// Mã nước thuê gia công
        /// </summary>
       [XmlElement("exportationCountry")]
       public string ExportationCountry { get; set; }
       /// <summary>
       /// Mã nước được thuê gia công
       /// </summary>
       [XmlElement("importationCountry")]
       public string ImportationCountry { get; set; }
       /// <summary>
       /// Tổng giá trị hàng và tổng giá trị tiền công
       /// </summary>
       [XmlElement("CustomsValue")]
       public CustomsValue CustomsValue { get; set; }
       /// <summary>
       /// Bên thuê gia công
       /// </summary>
       [XmlElement("Exporter")]
       public NameBase Exporter { get; set; }
       /// <summary>
       /// Bên được thuê gia công
       /// </summary>
       [XmlElement("Importer")]
       public NameBase Importer { get; set; }
       /// <summary>
       /// nguyên tệ
       /// </summary>
       [XmlElement("CurrencyExchange")]
       public CurrencyExchange CurrencyExchange { get; set; }
       /// <summary>
       /// Phương thức thanh toán
       /// </summary>
        [XmlElement("Payment")]
       public Payment Payment { get; set; }
       /// <summary>
       /// Sản phẩm
       /// </summary>
        [XmlElement("Product")]
       public List<Product> Products { get; set; }
       /// <summary>
       /// Nguyên phụ liệu
       /// </summary>
       [XmlElement("Material")]
        public List<Product> Materials { get; set; }
       /// <summary>
       /// Thiết bị
       /// </summary>
       [XmlElement("Equipment")]
       public List<Equipment> Equipments { get; set; }
       /// <summary>
       /// Hàng mẫu
       /// </summary>
       [XmlElement("SampleProduct")]
       public List<Equipment> SampleProducts { get; set; }
       #region Gia hạn hợp đồng
       /// <summary>
       /// Ngày hợp đồng cũ yyyy-MM-dd
       /// </summary>
       [XmlElement("oldExpire")]
       public string OldExpire { get; set; }
       /// <summary>
       /// Ngày hợp đồng mới yyyy-MM-dd
       /// </summary>
       [XmlElement("newExpire")]       
       public string NewExpire { get; set; }
       #endregion Gia hạn hợp đồng

       [XmlElement("AdditionalInformation")]
       public AdditionalInformation AdditionalInformation { get; set; }
    }
}
