using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.Utils;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.Utils;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.Utils;
#endif
using System.Collections.Generic;
using System.Linq;
namespace Company.Interface.Report
{
    public partial class TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public List<Company.KDT.SHARE.QuanLyChungTu.Container> ContCollection = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        //public Company.Interface.Report.ReportViewTKNTQDTFormTT196 report;
        public int SoToKhai;
        public DateTime NgayDangKy;
        public bool ToKhaiTaiCho = false;
        public bool BanLuuHaiQuan = false;
        public bool MienThueNK = false;
        public bool MienThueGTGT = false;
        public bool InMaHang = false;
        public int SoDongHang;
        public int Phulucso;
        public bool isCuaKhau = true;
        public bool inTriGiaTT = false;
        public bool inThueBVMT = false;
        public bool IsToKhaiTaiCho = false;
        public TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK()
        {
            InitializeComponent();

        }
        public string GetChiCucHQCK()
        {
            if (isCuaKhau)
            {
                string chicuc = NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                if (chicuc == string.Empty)
                    chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                return chicuc;
            }
            else
                return string.Empty;
        }
        public void BindReport(string pls)
        {

            try
            {
                if (IsToKhaiTaiCho)
                {
                    lblLoaiToKhai.Text = "Nhập khẩu tại chỗ";
                }
                ResetEmpty();
                float sizeFont = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8")) == 0 ? 8 : float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8"));

                this.PrintingSystem.ShowMarginsWarning = false;
                //int pluc = report.index;

                //Set ngay mac dinh
                DateTime minDate = new DateTime(1900, 1, 1);

                #region Thong tin chinh
                //Chi cục HD đăng ký
                lblChiCucHQDangKy.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                lblChiCucHQDangKy.Font = new Font("Times New Roman", sizeFont);

                //Chi cục Hải quan cửa khẩu nhập
                /*lblChiCucHQCuaKhau.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);*/
                //DonViHaiQuan.GetName(this.TKMD.CuaKhau_ID);
                if (!ToKhaiTaiCho)
                {
                    lblChiCucHQCuaKhau.Text = GetChiCucHQCK();
                    lblChiCucHQCuaKhau.Font = new Font("Times New Roman", sizeFont);
                }
                else
                {
                    lblTieuDeGocPhai.Text = "HQ/2012 - PLTKDTNK-TC";
                    lblChiCuc.Text = "";
                    lblChiCucHQCuaKhau.Text = "";
                }


                //So luong phu luc to khai
                lblSoPhuLucToKhai.Text = string.Format("{00}", Phulucso);

                //Số tờ khai
                if (this.TKMD.SoToKhai > 0)
                    lblSoToKhai.Text = this.TKMD.SoToKhai.ToString();

                //Ngày giờ đăng ký
                if (this.TKMD.NgayDangKy > minDate)
                    lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy HH:mm");
                else
                    lblNgayDangKy.Text = "";
                // Ma Loai Hinh
                string stlh = "";
                stlh = LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                lblLoaiHinh.Text = TKMD.MaLoaiHinh + " - " + stlh;
                lblLoaiHinh.Font = new Font("Times New Roman", sizeFont);

                #endregion


                #region Thong tin hang
                if (this.HMDCollection != null && this.HMDCollection.Count > 0)
                {
                    for (int j = 0; j < this.HMDCollection.Count; j++)
                    {
                        if (j == 0)
                        {
                            fillHang(xrTableHang1, xrTableThue1, j);
                        }
                        else if (j == 1)
                        {
                            fillHang(xrTableHang2, xrTableThue2, j);
                        }
                        else if (j == 2)
                            fillHang(xrTableHang3, xrTableThue3, j);
                        else if (j == 3)
                            fillHang(xrTableHang4, xrTableThue4, j);
                        //else if (j == 4)
                        //    fillHang(xrTableHang5, xrTableThue5, j);
                    }
                }

                #endregion

                #region Container
                //                 if (!GlobalSettings.IsKhongDungBangKeCont)
                //                 {
                //                     if (TKMD.VanTaiDon != null && TKMD.VanTaiDon.ContainerCollection != null && TKMD.VanTaiDon.ContainerCollection.Count <= 4)
                //                     {
                //                         for (int i = 0; i < this.ContCollection.Count; i++)
                //                         {
                //                             fillContainer(xrTableCont, i);
                //                         }
                // #if KD_V4 || SXXK_V4 || GC_V4
                //                         //Tong trong luong container
                //                         double sumTrongLuong = TKMD.VanTaiDon.ContainerCollection.Sum(x => x.TrongLuong);
                //                         lblTongContainer.Text = string.Format("{0:N3}", sumTrongLuong);
                // #endif
                //                     }
                //                 }
                //                 else
                //                 {
                //                     if (ContCollection != null & ContCollection.Count > 0)
                //                         for (int i = 0; i < this.ContCollection.Count; i++)
                //                         {
                //                             fillContainer(xrTableCont, i);
                // #if KD_V4 || SXXK_V4 || GC_V4
                //                             double sumTrongLuong = ContCollection.Sum(x => x.TrongLuong);
                //                             lblTongContainer.Text = string.Format("{0:N3}", sumTrongLuong);
                // #endif
                //                         }
                //                 }

                #endregion

                //Ngay thang nam in to khai
#if SXXK_V4
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1)|| GlobalSettings.NgayHeThong)
                    lblNgayThangNam.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
                else
                    lblNgayThangNam.Text = "Ngày " + TKMD.NgayDangKy.Day.ToString("00") + " tháng " + TKMD.NgayDangKy.Month.ToString("00") + " năm " + TKMD.NgayDangKy.Year;
#else
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1) || GlobalSettings.NgayHeThong)
                    lblNgayThangNam.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
                else
                    lblNgayThangNam.Text = "Ngày " + TKMD.NgayDangKy.Day.ToString("00") + " tháng " + TKMD.NgayDangKy.Month.ToString("00") + " năm " + TKMD.NgayDangKy.Year;
#endif

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }


        }
        #region dien thong tin container
        private void fillContainer(XRTable tableCont, int contItem)
        {
            //int phulucso = report.index;
            XRControl control = new XRControl();
            Company.KDT.SHARE.QuanLyChungTu.Container cont = this.ContCollection[contItem];

            control = tableCont.Rows[contItem].Controls["lblSTTCont" + (contItem + 1)];
            if (!GlobalSettings.IsKhongDungBangKeCont)
                control.Text = (contItem + 1 + ((Phulucso - 1) * 4)).ToString();
            else
                control.Text = (contItem + 1 + (Phulucso * 4)).ToString();
            control = tableCont.Rows[contItem].Controls["lblSoHieuCont" + (contItem + 1)];
            control.Text = cont.SoHieu + " / " + cont.Seal_No;//TKMD.VanTaiDon.ContainerCollection[contItem].SoHieu;
#if KD_V4 || SXXK_V4 || GC_V4
            int countSoLuongKien = TKMD.VanTaiDon.ListHangOfVanDon.Where(x => x.SoHieuContainer == cont.SoHieu).GroupBy(x => x.SoHieuKien).Count();
            control = tableCont.Rows[contItem].Controls["lblSoLuongKienCont" + (contItem + 1)];
            control.Text = string.Format("{0:00}", cont.SoKien);
            double countTrongLuong = TKMD.VanTaiDon.ListHangOfVanDon.Where(x => x.SoHieuContainer == cont.SoHieu).Sum(x => x.TrongLuong);
            control = tableCont.Rows[contItem].Controls["lblTrongLuongCont" + (contItem + 1)];
            control.Text = string.Format("{0:0}", cont.TrongLuong);
#endif

        }

        #endregion

        #region Dien thong tin hang và thue phu luc
        private void fillHang(XRTable tableHang, XRTable tableThue, int hangItem)
        {
            float sizeFont = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8")) == 0 ? 8 : float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8"));
#if KD_V3 || KD_V4
            double tongTriGiaNT = 0;
            double tongTienThueXNK = 0;
            double tongTienThueTatCa = 0;
            double tongThueGTGT = 0;
            //double tongThueBVMT = 0;
            //double TriGiaTTTTDB;
            //double TriGiaTTBVMT;
            double TriGiaTTGTGT;

            int formatSoLuong = GlobalSettings.SoThapPhan.SoLuongHMD;
            int formatDonGiaNT = GlobalSettings.SoThapPhan.DonGiaNT;
            int formatTriGiaNT = GlobalSettings.SoThapPhan.TriGiaNT;
#elif GC_V3 || GC_V4
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongThueGTGT = 0;
            //decimal tongTriGiaThuKhac = 0;
            //decimal tongThueBVMT = 0;
            //decimal TriGiaTTTTDB;
            decimal TriGiaTTGTGT;
            //decimal TriGiaTTBVMT;
             int formatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
        int formatDonGiaNT = GlobalSettings.DonGiaNT;
        int formatTriGiaNT = GlobalSettings.TriGiaNT;
#elif SXXK_V3 || SXXK_V4
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongThueGTGT = 0;
            //decimal tongThueBVMT = 0;
            decimal tongTriGiaThuKhac = 0;
            //decimal TriGiaTTTTDB;
            decimal TriGiaTTGTGT;
            //decimal TriGiaTTBVMT;
            int formatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
            int formatDonGiaNT = GlobalSettings.DonGiaNT;
            int formatTriGiaNT = GlobalSettings.TriGiaNT;
#endif
            // int phulucso = report.index;
            XRControl control = new XRControl();
            HangMauDich hmd = this.HMDCollection[hangItem];
            // STT hang
            control = tableHang.Rows[0].Controls["lblSTTHang" + (hangItem + 1)];
            control.Text = (hangItem + 1 + ((Phulucso - 1) * 4)).ToString();
            //18. Mo ta hang hoa - Ten hang
            control = tableHang.Rows[0].Controls["lblMoTaHang" + (hangItem + 1)];
            if (!InMaHang)
                control.Text = hmd.TenHang;
            else
            {
                if (hmd.MaPhu.Trim().Length > 0)
                    control.Text = hmd.TenHang + "/" + hmd.MaPhu;
                else
                    control.Text = hmd.TenHang;
            }
            control.WordWrap = true;
            control.Font = new Font("Times New Roman", sizeFont);
            //19. Ma HS hang
            control = tableHang.Rows[0].Controls["lblMaHSHang" + (hangItem + 1)];
            control.Text = hmd.MaHS;
            //20. Xuat xu
            control = tableHang.Rows[0].Controls["lblXuatXuHang" + (hangItem + 1)];
            control.Text = hmd.NuocXX_ID;
#if KD_V3 || KD_V4

            //21. Che do uu dai
            control = tableHang.Rows[0].Controls["lblCheDoUuDaiHang" + (hangItem + 1)];
            control.Text = LoaiCO.GetName(hmd.CheDoUuDai);
#endif
            //22. luong hang
            control = tableHang.Rows[0].Controls["lblLuongHang" + (hangItem + 1)];
            control.Text = hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, true));
            //23.Don vi tinh
            control = tableHang.Rows[0].Controls["lblDVTHang" + (hangItem + 1)];
            control.Text = DonViTinh.GetName(hmd.DVT_ID);
            //24. Don gia nguyen te
            control = tableHang.Rows[0].Controls["lblDonGiaHang" + (hangItem + 1)];
            control.Text = hmd.DonGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatDonGiaNT, true));
            //25. Tri gia nguyen te
            control = tableHang.Rows[0].Controls["lblTGNTHang" + (hangItem + 1)];
            control.Text = hmd.TriGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatTriGiaNT, true));

            //26. Thue Nhap khau - NK
            //Tri gia tinh thue
            control = tableThue.Rows[1].Controls["lblTGTTNKHang" + (hangItem + 1)];
            control.Text = inTriGiaTT ? hmd.TriGiaTT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
            if (hmd.ThueXNK > 0)
            {
                control = tableThue.Rows[1].Controls["lblTGTTNKHang" + (hangItem + 1)];
                control.Text = hmd.TriGiaTT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false)));
                //Thue suat
                control = tableThue.Rows[1].Controls["lblThueSuatNKHang" + (hangItem + 1)];
                if (hmd.ThueTuyetDoi)
                {
                    control.Text = "";
                }
                else
                {
                    /*if (hmd.ThueSuatXNKGiam == 0)*/
                    control.Text = hmd.ThueSuatXNK.ToString();
                    //                 else
                    //                     control.Text = hmd.ThueSuatXNKGiam.ToString();
                }
                //Tien thue
                control = tableThue.Rows[1].Controls["lblTienThueNKHang" + (hangItem + 1)];
                control.Text = hmd.ThueXNK.ToString("N0");
            }
            #region Tieu Thu Dac Biet Va Bao Ve Moi Truong
            //             //27. Thue thieu thu dac biet - TTDB
            //             control = tableThue.Rows[2].Controls["lblTGTTTTDBHang" + (hangItem + 1)];
            //             TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
            //             control.Text = inTriGiaTT ? TriGiaTTTTDB.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
            //             if (hmd.ThueTTDB > 0)
            //             {
            // 
            //                 control = tableThue.Rows[2].Controls["lblTGTTTTDBHang" + (hangItem + 1)];
            //                 control.Text = TriGiaTTTTDB.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false)));
            //                 //thue suat
            //                 control = tableThue.Rows[2].Controls["lblThueSuatTTDBHang" + (hangItem + 1)];
            //                 /*if (hmd.ThueSuatTTDBGiam == 0)*/
            //                 control.Text = hmd.ThueSuatTTDB.ToString("N0");
            //                 //             else
            //                 //                 control.Text = hmd.ThueSuatTTDBGiam.ToString();
            //                 //tien thue TTDB
            //                 control = tableThue.Rows[2].Controls["lblTienThueTTDBHang" + (hangItem + 1)];
            //                 control.Text = hmd.ThueTTDB.ToString("N0");
            //             }
            //             //Tri gia tinh thue BVMT
            //             control = tableThue.Rows[3].Controls["lblTGTTBVMTHang" + (hangItem + 1)];
            //             TriGiaTTBVMT = hmd.TriGiaTT;
            //             control.Text = (inThueBVMT && inTriGiaTT) ? TriGiaTTBVMT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
            // #if KD_V4 || GC_V4 || SXXK_V4
            //             if (hmd.ThueBVMT > 0)
            //             {
            // #else
            //            
            //             if (hmd.TriGiaThuKhac > 0)
            //             {
            // #endif
            //                 //28. Thue Bao ve moi truong - BVMT
            //                 control = tableThue.Rows[3].Controls["lblTGTTBVMTHang" + (hangItem + 1)];
            //                 control.Text = inThueBVMT ? TriGiaTTBVMT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
            //                 //thue suat
            //                 control = tableThue.Rows[3].Controls["lblThueSuatBVMTHang" + (hangItem + 1)];
            // #if KD_V4 || GC_V4 || SXXK_V4
            //                 if (hmd.ThueSuatBVMT != 0)
            //                     control.Text = hmd.ThueSuatBVMT.ToString("N0");
            //                 else
            //                     control.Text = "";
            // #else
            //                 if (hmd.TyLeThuKhac != 0)
            //                     control.Text = hmd.TyLeThuKhac.ToString("N0");
            //                 else
            //                     control.Text = "";
            // #endif
            //             
            //                 //tien thue BVMT
            //                 control = tableThue.Rows[3].Controls["lblTienThueBVMTHang" + (hangItem + 1)];
            // #if KD_V4 || GC_V4 || SXXK_V4
            //                 control.Text = hmd.ThueBVMT.ToString("N0");
            // #else
            //                 control.Text = hmd.TriGiaThuKhac.ToString("N0");
            // #endif
            //             }
            #endregion
            //29. Thue gia tri gia tang - VAT
            //tri gia tinh thue
            control = tableThue.Rows[2].Controls["lblTGTTVATHang" + (hangItem + 1)];
#if  KD_V4 || GC_V4 || SXXK_V4
            TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueBVMT;
#else
            TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB + hmd.TriGiaThuKhac;
#endif
            control.Text = inTriGiaTT ? TriGiaTTGTGT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
            if (hmd.ThueGTGT > 0)
            {
                control = tableThue.Rows[2].Controls["lblTGTTVATHang" + (hangItem + 1)];
                control.Text = TriGiaTTGTGT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false)));
                //thue suat
                control = tableThue.Rows[2].Controls["lblThueSuatVATHang" + (hangItem + 1)];
                /* if (hmd.ThueSuatVATGiam == 0)*/
                control.Text = hmd.ThueSuatGTGT.ToString("N0");
                //             else
                //                 control.Text = hmd.ThueSuatVATGiam.ToString();
                //tien thue
                control = tableThue.Rows[2].Controls["lblTienThueVATHang" + (hangItem + 1)];
                control.Text = hmd.ThueGTGT.ToString("N0");
            }
            // tong thue
            tongTienThueXNK += hmd.ThueXNK;
            tongThueGTGT += hmd.ThueGTGT;
            // #if  KD_V4 || GC_V4 || SXXK_V4
            //             tongThueBVMT += hmd.ThueBVMT;
            // #else
            //             tongThueBVMT += hmd.TriGiaThuKhac;
            // #endif
            tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
            //Tong thue
#if  KD_V4 || GC_V4 || SXXK_V4
            tongTienThueTatCa = hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.ThueBVMT;
#else
            tongTienThueTatCa = hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
#endif

            control = tableThue.Rows[3].Controls["lblTongThueHang" + (hangItem + 1)];
            control.Text = tongTienThueTatCa.ToString("N0");

        }
        #endregion

        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        private void ResetEmpty()
        {
            try
            {

                //Chi cuc Hai quan dang ky
                lblChiCucHQDangKy.Text = "";

                //Chi cuc Hai quan cua khau
                lblChiCucHQCuaKhau.Text = "";

                //Số tờ khai
                lblSoToKhai.Text = "";

                //Ngày giờ đăng ký
                lblNgayDangKy.Text = "";

                //So luong phu luc to khai
                lblSoPhuLucToKhai.Text = "";

                // Ma Loai Hinh
                lblLoaiHinh.Text = "";


            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}
