﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.GC
{
    public partial class PhieuTN : DevExpress.XtraReports.UI.XtraReport
    {
        public string phieu;
        public string soTN ;
        public string ngayTN ;
        public string maHaiQuan;
        public PhieuTN()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            lblPhieu.Text+=phieu;
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblHaiQuan.Text = maHaiQuan;
            lblSoTN.Text = soTN;
            lblNgayTN.Text = ngayTN;
        }       
       

    }
}
