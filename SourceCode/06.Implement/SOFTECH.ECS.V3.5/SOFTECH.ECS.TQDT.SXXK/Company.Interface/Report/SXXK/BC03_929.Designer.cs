namespace Company.Interface.Report.SXXK
{
    partial class BC03_929
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblL = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSP = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDangKyXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHoanThanhXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongSPXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDVT_SP = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDinhMuc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongNPLSuDung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiTaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayTaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongNPLTaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongTonCuoi = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThanhKhoanTiep = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNPL = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDangKyNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHoanThanhNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongTonDau = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDVT_NPL = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblToKhaiNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongLuongNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTonDau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongLuongNPLSuDung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiTaiXuat1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongNPLTaiXuat1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTonCuoi = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThanhKhoanTiep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSHSTK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Height = 20;
            this.Detail.Name = "Detail";
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(0, 0);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.ParentStyleUsing.UseBorders = false;
            this.xrTable1.ParentStyleUsing.UseBorderWidth = false;
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.Size = new System.Drawing.Size(1125, 20);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell6,
            this.lblL,
            this.xrTableCell3,
            this.xrTableCell4,
            this.lblMaSP,
            this.lblSoToKhaiXuat,
            this.lblNgayDangKyXuat,
            this.lblNgayHoanThanhXuat,
            this.lblLuongSPXuat,
            this.lblTenDVT_SP,
            this.lblDinhMuc,
            this.lblLuongNPLSuDung,
            this.lblSoToKhaiTaiXuat,
            this.lblNgayTaiXuat,
            this.lblLuongNPLTaiXuat,
            this.lblLuongTonCuoi,
            this.lblThanhKhoanTiep});
            this.xrTableRow1.Font = new System.Drawing.Font("Times New Roman", 36F);
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.ParentStyleUsing.UseBorders = false;
            this.xrTableRow1.ParentStyleUsing.UseFont = false;
            this.xrTableRow1.Size = new System.Drawing.Size(1125, 20);
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.ParentStyleUsing.UseBorders = false;
            this.xrTableCell1.Size = new System.Drawing.Size(33, 20);
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Location = new System.Drawing.Point(33, 0);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.ParentStyleUsing.UseBorders = false;
            this.xrTableCell2.Size = new System.Drawing.Size(92, 20);
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Location = new System.Drawing.Point(125, 0);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.ParentStyleUsing.UseBorders = false;
            this.xrTableCell7.Size = new System.Drawing.Size(33, 20);
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Location = new System.Drawing.Point(158, 0);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.ParentStyleUsing.UseBorders = false;
            this.xrTableCell8.Size = new System.Drawing.Size(42, 20);
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.ParentStyleUsing.UseBorders = false;
            this.xrTableCell6.Size = new System.Drawing.Size(42, 20);
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblL
            // 
            this.lblL.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblL.Location = new System.Drawing.Point(242, 0);
            this.lblL.Name = "lblL";
            this.lblL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblL.ParentStyleUsing.UseBorders = false;
            this.lblL.Size = new System.Drawing.Size(75, 20);
            this.lblL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Location = new System.Drawing.Point(317, 0);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.ParentStyleUsing.UseBorders = false;
            this.xrTableCell3.Size = new System.Drawing.Size(75, 20);
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.ParentStyleUsing.UseBorders = false;
            this.xrTableCell4.Size = new System.Drawing.Size(33, 20);
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaSP
            // 
            this.lblMaSP.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaSP.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSP.Location = new System.Drawing.Point(425, 0);
            this.lblMaSP.Name = "lblMaSP";
            this.lblMaSP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSP.ParentStyleUsing.UseBorders = false;
            this.lblMaSP.ParentStyleUsing.UseFont = false;
            this.lblMaSP.Size = new System.Drawing.Size(92, 20);
            this.lblMaSP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMaSP.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblMaSP_BeforePrint);
            // 
            // lblSoToKhaiXuat
            // 
            this.lblSoToKhaiXuat.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoToKhaiXuat.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXuat.Location = new System.Drawing.Point(517, 0);
            this.lblSoToKhaiXuat.Name = "lblSoToKhaiXuat";
            this.lblSoToKhaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiXuat.ParentStyleUsing.UseBorders = false;
            this.lblSoToKhaiXuat.ParentStyleUsing.UseFont = false;
            this.lblSoToKhaiXuat.Size = new System.Drawing.Size(33, 20);
            this.lblSoToKhaiXuat.Text = "lblSoToKhaiXuat";
            this.lblSoToKhaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSoToKhaiXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoToKhaiXuat_BeforePrint);
            // 
            // lblNgayDangKyXuat
            // 
            this.lblNgayDangKyXuat.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayDangKyXuat.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDangKyXuat.Location = new System.Drawing.Point(550, 0);
            this.lblNgayDangKyXuat.Name = "lblNgayDangKyXuat";
            this.lblNgayDangKyXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKyXuat.ParentStyleUsing.UseBorders = false;
            this.lblNgayDangKyXuat.ParentStyleUsing.UseFont = false;
            this.lblNgayDangKyXuat.Size = new System.Drawing.Size(42, 20);
            this.lblNgayDangKyXuat.Text = "22/04/09";
            this.lblNgayDangKyXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgayDangKyXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoToKhaiXuat_BeforePrint);
            // 
            // lblNgayHoanThanhXuat
            // 
            this.lblNgayHoanThanhXuat.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayHoanThanhXuat.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHoanThanhXuat.Location = new System.Drawing.Point(592, 0);
            this.lblNgayHoanThanhXuat.Name = "lblNgayHoanThanhXuat";
            this.lblNgayHoanThanhXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHoanThanhXuat.ParentStyleUsing.UseBorders = false;
            this.lblNgayHoanThanhXuat.ParentStyleUsing.UseFont = false;
            this.lblNgayHoanThanhXuat.Size = new System.Drawing.Size(41, 20);
            this.lblNgayHoanThanhXuat.Text = "lblNgayHoanThanhXuat";
            this.lblNgayHoanThanhXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgayHoanThanhXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoToKhaiXuat_BeforePrint);
            // 
            // lblLuongSPXuat
            // 
            this.lblLuongSPXuat.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongSPXuat.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuongSPXuat.Location = new System.Drawing.Point(633, 0);
            this.lblLuongSPXuat.Name = "lblLuongSPXuat";
            this.lblLuongSPXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongSPXuat.ParentStyleUsing.UseBorders = false;
            this.lblLuongSPXuat.ParentStyleUsing.UseFont = false;
            this.lblLuongSPXuat.Size = new System.Drawing.Size(50, 20);
            this.lblLuongSPXuat.Text = "lblLuongSPXuat";
            this.lblLuongSPXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongSPXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoToKhaiXuat_BeforePrint);
            // 
            // lblTenDVT_SP
            // 
            this.lblTenDVT_SP.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenDVT_SP.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDVT_SP.Location = new System.Drawing.Point(683, 0);
            this.lblTenDVT_SP.Name = "lblTenDVT_SP";
            this.lblTenDVT_SP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDVT_SP.ParentStyleUsing.UseBorders = false;
            this.lblTenDVT_SP.ParentStyleUsing.UseFont = false;
            this.lblTenDVT_SP.Size = new System.Drawing.Size(34, 20);
            this.lblTenDVT_SP.Text = "lblTenDVT_SP";
            this.lblTenDVT_SP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTenDVT_SP.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoToKhaiXuat_BeforePrint);
            // 
            // lblDinhMuc
            // 
            this.lblDinhMuc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDinhMuc.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDinhMuc.Location = new System.Drawing.Point(717, 0);
            this.lblDinhMuc.Name = "lblDinhMuc";
            this.lblDinhMuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDinhMuc.ParentStyleUsing.UseBorders = false;
            this.lblDinhMuc.ParentStyleUsing.UseFont = false;
            this.lblDinhMuc.Size = new System.Drawing.Size(58, 20);
            this.lblDinhMuc.Text = "lblDinhMuc";
            this.lblDinhMuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDinhMuc.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoToKhaiXuat_BeforePrint);
            // 
            // lblLuongNPLSuDung
            // 
            this.lblLuongNPLSuDung.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongNPLSuDung.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuongNPLSuDung.Location = new System.Drawing.Point(775, 0);
            this.lblLuongNPLSuDung.Name = "lblLuongNPLSuDung";
            this.lblLuongNPLSuDung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblLuongNPLSuDung.ParentStyleUsing.UseBorders = false;
            this.lblLuongNPLSuDung.ParentStyleUsing.UseFont = false;
            this.lblLuongNPLSuDung.Size = new System.Drawing.Size(67, 20);
            this.lblLuongNPLSuDung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongNPLSuDung.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoToKhaiXuat_BeforePrint);
            // 
            // lblSoToKhaiTaiXuat
            // 
            this.lblSoToKhaiTaiXuat.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoToKhaiTaiXuat.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiTaiXuat.Location = new System.Drawing.Point(842, 0);
            this.lblSoToKhaiTaiXuat.Name = "lblSoToKhaiTaiXuat";
            this.lblSoToKhaiTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiTaiXuat.ParentStyleUsing.UseBorders = false;
            this.lblSoToKhaiTaiXuat.ParentStyleUsing.UseFont = false;
            this.lblSoToKhaiTaiXuat.Size = new System.Drawing.Size(33, 20);
            this.lblSoToKhaiTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSoToKhaiTaiXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoToKhaiTaiXuat_BeforePrint);
            // 
            // lblNgayTaiXuat
            // 
            this.lblNgayTaiXuat.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayTaiXuat.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayTaiXuat.Location = new System.Drawing.Point(875, 0);
            this.lblNgayTaiXuat.Name = "lblNgayTaiXuat";
            this.lblNgayTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayTaiXuat.ParentStyleUsing.UseBorders = false;
            this.lblNgayTaiXuat.ParentStyleUsing.UseFont = false;
            this.lblNgayTaiXuat.Size = new System.Drawing.Size(42, 20);
            this.lblNgayTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgayTaiXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblNgayTaiXuat_BeforePrint);
            // 
            // lblLuongNPLTaiXuat
            // 
            this.lblLuongNPLTaiXuat.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongNPLTaiXuat.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuongNPLTaiXuat.Location = new System.Drawing.Point(917, 0);
            this.lblLuongNPLTaiXuat.Name = "lblLuongNPLTaiXuat";
            this.lblLuongNPLTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblLuongNPLTaiXuat.ParentStyleUsing.UseBorders = false;
            this.lblLuongNPLTaiXuat.ParentStyleUsing.UseFont = false;
            this.lblLuongNPLTaiXuat.Size = new System.Drawing.Size(58, 20);
            this.lblLuongNPLTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongNPLTaiXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblLuongNPLTaiXuat_BeforePrint);
            // 
            // lblLuongTonCuoi
            // 
            this.lblLuongTonCuoi.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongTonCuoi.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuongTonCuoi.Location = new System.Drawing.Point(975, 0);
            this.lblLuongTonCuoi.Name = "lblLuongTonCuoi";
            this.lblLuongTonCuoi.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblLuongTonCuoi.ParentStyleUsing.UseBorders = false;
            this.lblLuongTonCuoi.ParentStyleUsing.UseFont = false;
            this.lblLuongTonCuoi.Size = new System.Drawing.Size(67, 20);
            this.lblLuongTonCuoi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblThanhKhoanTiep
            // 
            this.lblThanhKhoanTiep.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblThanhKhoanTiep.Font = new System.Drawing.Font("Tahoma", 6F);
            this.lblThanhKhoanTiep.Location = new System.Drawing.Point(1042, 0);
            this.lblThanhKhoanTiep.Name = "lblThanhKhoanTiep";
            this.lblThanhKhoanTiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThanhKhoanTiep.ParentStyleUsing.UseBorders = false;
            this.lblThanhKhoanTiep.ParentStyleUsing.UseFont = false;
            this.lblThanhKhoanTiep.Size = new System.Drawing.Size(83, 20);
            this.lblThanhKhoanTiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblThanhKhoanTiep.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblThanhKhoanTiep_BeforePrint);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel68,
            this.xrLabel67,
            this.xrLabel47,
            this.xrLabel45,
            this.xrLabel44,
            this.xrLabel40,
            this.xrLabel38,
            this.xrLabel36,
            this.xrLabel34,
            this.xrLabel33,
            this.xrLabel32,
            this.xrLabel27,
            this.xrLabel24,
            this.xrLabel23,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel20,
            this.xrLabel14,
            this.xrLabel11});
            this.PageHeader.Height = 25;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrLabel68
            // 
            this.xrLabel68.BackColor = System.Drawing.Color.Azure;
            this.xrLabel68.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel68.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel68.Location = new System.Drawing.Point(1083, 0);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.ParentStyleUsing.UseBackColor = false;
            this.xrLabel68.ParentStyleUsing.UseBorders = false;
            this.xrLabel68.ParentStyleUsing.UseFont = false;
            this.xrLabel68.Size = new System.Drawing.Size(42, 25);
            this.xrLabel68.Text = "(19)";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel67
            // 
            this.xrLabel67.BackColor = System.Drawing.Color.Azure;
            this.xrLabel67.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel67.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel67.Location = new System.Drawing.Point(1042, 0);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.ParentStyleUsing.UseBackColor = false;
            this.xrLabel67.ParentStyleUsing.UseBorders = false;
            this.xrLabel67.ParentStyleUsing.UseFont = false;
            this.xrLabel67.Size = new System.Drawing.Size(41, 25);
            this.xrLabel67.Text = "(18)";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel47
            // 
            this.xrLabel47.BackColor = System.Drawing.Color.Azure;
            this.xrLabel47.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel47.Location = new System.Drawing.Point(975, 0);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.ParentStyleUsing.UseBackColor = false;
            this.xrLabel47.ParentStyleUsing.UseBorders = false;
            this.xrLabel47.ParentStyleUsing.UseFont = false;
            this.xrLabel47.Size = new System.Drawing.Size(67, 25);
            this.xrLabel47.Text = "(17)";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel45
            // 
            this.xrLabel45.BackColor = System.Drawing.Color.Azure;
            this.xrLabel45.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel45.Location = new System.Drawing.Point(917, 0);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.ParentStyleUsing.UseBackColor = false;
            this.xrLabel45.ParentStyleUsing.UseBorders = false;
            this.xrLabel45.ParentStyleUsing.UseFont = false;
            this.xrLabel45.Size = new System.Drawing.Size(58, 25);
            this.xrLabel45.Text = "(16)";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel44
            // 
            this.xrLabel44.BackColor = System.Drawing.Color.Azure;
            this.xrLabel44.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel44.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel44.Location = new System.Drawing.Point(842, 0);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.ParentStyleUsing.UseBackColor = false;
            this.xrLabel44.ParentStyleUsing.UseBorders = false;
            this.xrLabel44.ParentStyleUsing.UseFont = false;
            this.xrLabel44.Size = new System.Drawing.Size(75, 25);
            this.xrLabel44.Text = "(15)";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel40
            // 
            this.xrLabel40.BackColor = System.Drawing.Color.Azure;
            this.xrLabel40.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel40.Location = new System.Drawing.Point(775, 0);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.ParentStyleUsing.UseBackColor = false;
            this.xrLabel40.ParentStyleUsing.UseBorders = false;
            this.xrLabel40.ParentStyleUsing.UseFont = false;
            this.xrLabel40.Size = new System.Drawing.Size(67, 25);
            this.xrLabel40.Text = "(14)";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel38
            // 
            this.xrLabel38.BackColor = System.Drawing.Color.Azure;
            this.xrLabel38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel38.Location = new System.Drawing.Point(717, 0);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.ParentStyleUsing.UseBackColor = false;
            this.xrLabel38.ParentStyleUsing.UseBorders = false;
            this.xrLabel38.ParentStyleUsing.UseFont = false;
            this.xrLabel38.Size = new System.Drawing.Size(58, 25);
            this.xrLabel38.Text = "(13)";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel36
            // 
            this.xrLabel36.BackColor = System.Drawing.Color.Azure;
            this.xrLabel36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel36.Location = new System.Drawing.Point(683, 0);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.ParentStyleUsing.UseBackColor = false;
            this.xrLabel36.ParentStyleUsing.UseBorders = false;
            this.xrLabel36.ParentStyleUsing.UseFont = false;
            this.xrLabel36.Size = new System.Drawing.Size(34, 25);
            this.xrLabel36.Text = "(12)";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel34
            // 
            this.xrLabel34.BackColor = System.Drawing.Color.Azure;
            this.xrLabel34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel34.Location = new System.Drawing.Point(633, 0);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.ParentStyleUsing.UseBackColor = false;
            this.xrLabel34.ParentStyleUsing.UseBorders = false;
            this.xrLabel34.ParentStyleUsing.UseFont = false;
            this.xrLabel34.Size = new System.Drawing.Size(50, 25);
            this.xrLabel34.Text = "(11)";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel33
            // 
            this.xrLabel33.BackColor = System.Drawing.Color.Azure;
            this.xrLabel33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel33.Location = new System.Drawing.Point(592, 0);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.ParentStyleUsing.UseBackColor = false;
            this.xrLabel33.ParentStyleUsing.UseBorders = false;
            this.xrLabel33.ParentStyleUsing.UseFont = false;
            this.xrLabel33.Size = new System.Drawing.Size(41, 25);
            this.xrLabel33.Text = "(10)";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel32
            // 
            this.xrLabel32.BackColor = System.Drawing.Color.Azure;
            this.xrLabel32.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel32.Location = new System.Drawing.Point(517, 0);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.ParentStyleUsing.UseBackColor = false;
            this.xrLabel32.ParentStyleUsing.UseBorders = false;
            this.xrLabel32.ParentStyleUsing.UseFont = false;
            this.xrLabel32.Size = new System.Drawing.Size(75, 25);
            this.xrLabel32.Text = "(9)";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel27
            // 
            this.xrLabel27.BackColor = System.Drawing.Color.Azure;
            this.xrLabel27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel27.Location = new System.Drawing.Point(425, 0);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.ParentStyleUsing.UseBackColor = false;
            this.xrLabel27.ParentStyleUsing.UseBorders = false;
            this.xrLabel27.ParentStyleUsing.UseFont = false;
            this.xrLabel27.Size = new System.Drawing.Size(92, 25);
            this.xrLabel27.Text = "(8)";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.BackColor = System.Drawing.Color.Azure;
            this.xrLabel24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel24.Location = new System.Drawing.Point(392, 0);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.ParentStyleUsing.UseBackColor = false;
            this.xrLabel24.ParentStyleUsing.UseBorders = false;
            this.xrLabel24.ParentStyleUsing.UseFont = false;
            this.xrLabel24.Size = new System.Drawing.Size(33, 25);
            this.xrLabel24.Text = "(7)";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel23
            // 
            this.xrLabel23.BackColor = System.Drawing.Color.Azure;
            this.xrLabel23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.Location = new System.Drawing.Point(317, 0);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.ParentStyleUsing.UseBackColor = false;
            this.xrLabel23.ParentStyleUsing.UseBorders = false;
            this.xrLabel23.ParentStyleUsing.UseFont = false;
            this.xrLabel23.Size = new System.Drawing.Size(75, 25);
            this.xrLabel23.Text = "(6)";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel22
            // 
            this.xrLabel22.BackColor = System.Drawing.Color.Azure;
            this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel22.Location = new System.Drawing.Point(242, 0);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.ParentStyleUsing.UseBackColor = false;
            this.xrLabel22.ParentStyleUsing.UseBorders = false;
            this.xrLabel22.ParentStyleUsing.UseFont = false;
            this.xrLabel22.Size = new System.Drawing.Size(75, 25);
            this.xrLabel22.Text = "(5)";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.BackColor = System.Drawing.Color.Azure;
            this.xrLabel21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel21.Location = new System.Drawing.Point(200, 0);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.ParentStyleUsing.UseBackColor = false;
            this.xrLabel21.ParentStyleUsing.UseBorders = false;
            this.xrLabel21.ParentStyleUsing.UseFont = false;
            this.xrLabel21.Size = new System.Drawing.Size(42, 25);
            this.xrLabel21.Text = "(4)";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel20
            // 
            this.xrLabel20.BackColor = System.Drawing.Color.Azure;
            this.xrLabel20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel20.Location = new System.Drawing.Point(125, 0);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.ParentStyleUsing.UseBackColor = false;
            this.xrLabel20.ParentStyleUsing.UseBorders = false;
            this.xrLabel20.ParentStyleUsing.UseFont = false;
            this.xrLabel20.Size = new System.Drawing.Size(75, 25);
            this.xrLabel20.Text = "(3)";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel14
            // 
            this.xrLabel14.BackColor = System.Drawing.Color.Azure;
            this.xrLabel14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel14.Location = new System.Drawing.Point(33, 0);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.ParentStyleUsing.UseBackColor = false;
            this.xrLabel14.ParentStyleUsing.UseBorders = false;
            this.xrLabel14.ParentStyleUsing.UseFont = false;
            this.xrLabel14.Size = new System.Drawing.Size(92, 25);
            this.xrLabel14.Text = "(2)";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.BackColor = System.Drawing.Color.Azure;
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.Location = new System.Drawing.Point(0, 0);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.ParentStyleUsing.UseBackColor = false;
            this.xrLabel11.ParentStyleUsing.UseBorders = false;
            this.xrLabel11.ParentStyleUsing.UseFont = false;
            this.xrLabel11.Size = new System.Drawing.Size(33, 25);
            this.xrLabel11.Text = "(1)";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5,
            this.xrPageInfo1});
            this.PageFooter.Height = 42;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.ForeColor = System.Drawing.Color.Olive;
            this.xrLabel5.Location = new System.Drawing.Point(1058, 0);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.ParentStyleUsing.UseFont = false;
            this.xrLabel5.ParentStyleUsing.UseForeColor = false;
            this.xrLabel5.Size = new System.Drawing.Size(67, 25);
            this.xrLabel5.Text = "Softech ECS";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "Trang {0}";
            this.xrPageInfo1.Location = new System.Drawing.Point(0, 0);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.ParentStyleUsing.UseFont = false;
            this.xrPageInfo1.Size = new System.Drawing.Size(1058, 25);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel77,
            this.xrLabel76,
            this.xrLabel75,
            this.xrLabel74});
            this.ReportFooter.Height = 201;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel77
            // 
            this.xrLabel77.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel77.Location = new System.Drawing.Point(792, 17);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.ParentStyleUsing.UseBackColor = false;
            this.xrLabel77.ParentStyleUsing.UseBorders = false;
            this.xrLabel77.ParentStyleUsing.UseFont = false;
            this.xrLabel77.Size = new System.Drawing.Size(308, 25);
            this.xrLabel77.Text = "................, ngày ... tháng ... năm......";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel76.Location = new System.Drawing.Point(50, 17);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.ParentStyleUsing.UseBackColor = false;
            this.xrLabel76.ParentStyleUsing.UseBorders = false;
            this.xrLabel76.ParentStyleUsing.UseFont = false;
            this.xrLabel76.Size = new System.Drawing.Size(308, 25);
            this.xrLabel76.Text = "................, ngày ... tháng ... năm......";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel75.Location = new System.Drawing.Point(792, 42);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.ParentStyleUsing.UseFont = false;
            this.xrLabel75.Size = new System.Drawing.Size(308, 25);
            this.xrLabel75.Text = "Giám đốc doanh nghiệp";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel74
            // 
            this.xrLabel74.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel74.Location = new System.Drawing.Point(50, 42);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.ParentStyleUsing.UseFont = false;
            this.xrLabel74.Size = new System.Drawing.Size(308, 25);
            this.xrLabel74.Text = "Công chức Hải quan kiểm tra, đối chiếu";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.xrLabel1.Location = new System.Drawing.Point(900, 0);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.ParentStyleUsing.UseFont = false;
            this.xrLabel1.Size = new System.Drawing.Size(216, 25);
            this.xrLabel1.Text = "Mẫu: 03/BCNL SXXK/2006";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.Location = new System.Drawing.Point(0, 25);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.ParentStyleUsing.UseFont = false;
            this.xrLabel2.Size = new System.Drawing.Size(1117, 42);
            this.xrLabel2.Text = "BÁO CÁO NGUYÊN VẬT LIỆU DÙNG ĐỂ SẢN XUẤT HÀNG XUẤT KHẨU";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.GroupHeader1.Height = 25;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupHeader1_BeforePrint);
            // 
            // xrTable2
            // 
            this.xrTable2.Location = new System.Drawing.Point(0, 0);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.ParentStyleUsing.UseBorders = false;
            this.xrTable2.ParentStyleUsing.UseBorderWidth = false;
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.Size = new System.Drawing.Size(1125, 25);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTT,
            this.lblMaNPL,
            this.lblSoToKhaiNhap,
            this.lblNgayDangKyNhap,
            this.lblNgayHoanThanhNhap,
            this.lblLuongNhap,
            this.lblLuongTonDau,
            this.lblTenDVT_NPL,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell5,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.ParentStyleUsing.UseBorders = false;
            this.xrTableRow2.ParentStyleUsing.UseFont = false;
            this.xrTableRow2.Size = new System.Drawing.Size(1125, 25);
            // 
            // lblSTT
            // 
            this.lblSTT.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTT.Font = new System.Drawing.Font("Tahoma", 7F);
            this.lblSTT.Location = new System.Drawing.Point(0, 0);
            this.lblSTT.Name = "lblSTT";
            this.lblSTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT.ParentStyleUsing.UseBorders = false;
            this.lblSTT.ParentStyleUsing.UseFont = false;
            this.lblSTT.Size = new System.Drawing.Size(33, 25);
            this.lblSTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSTT_BeforePrint);
            // 
            // lblMaNPL
            // 
            this.lblMaNPL.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaNPL.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNPL.Location = new System.Drawing.Point(33, 0);
            this.lblMaNPL.Name = "lblMaNPL";
            this.lblMaNPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNPL.ParentStyleUsing.UseBorders = false;
            this.lblMaNPL.ParentStyleUsing.UseFont = false;
            this.lblMaNPL.Size = new System.Drawing.Size(92, 25);
            this.lblMaNPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMaNPL.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblMaNPL_BeforePrint);
            // 
            // lblSoToKhaiNhap
            // 
            this.lblSoToKhaiNhap.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoToKhaiNhap.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiNhap.Location = new System.Drawing.Point(125, 0);
            this.lblSoToKhaiNhap.Name = "lblSoToKhaiNhap";
            this.lblSoToKhaiNhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiNhap.ParentStyleUsing.UseBorders = false;
            this.lblSoToKhaiNhap.ParentStyleUsing.UseFont = false;
            this.lblSoToKhaiNhap.Size = new System.Drawing.Size(33, 25);
            this.lblSoToKhaiNhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayDangKyNhap
            // 
            this.lblNgayDangKyNhap.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayDangKyNhap.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDangKyNhap.Location = new System.Drawing.Point(158, 0);
            this.lblNgayDangKyNhap.Name = "lblNgayDangKyNhap";
            this.lblNgayDangKyNhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKyNhap.ParentStyleUsing.UseBorders = false;
            this.lblNgayDangKyNhap.ParentStyleUsing.UseFont = false;
            this.lblNgayDangKyNhap.Size = new System.Drawing.Size(42, 25);
            this.lblNgayDangKyNhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHoanThanhNhap
            // 
            this.lblNgayHoanThanhNhap.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayHoanThanhNhap.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHoanThanhNhap.Location = new System.Drawing.Point(200, 0);
            this.lblNgayHoanThanhNhap.Name = "lblNgayHoanThanhNhap";
            this.lblNgayHoanThanhNhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHoanThanhNhap.ParentStyleUsing.UseBorders = false;
            this.lblNgayHoanThanhNhap.ParentStyleUsing.UseFont = false;
            this.lblNgayHoanThanhNhap.Size = new System.Drawing.Size(42, 25);
            this.lblNgayHoanThanhNhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblLuongNhap
            // 
            this.lblLuongNhap.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongNhap.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuongNhap.Location = new System.Drawing.Point(242, 0);
            this.lblLuongNhap.Name = "lblLuongNhap";
            this.lblLuongNhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongNhap.ParentStyleUsing.UseBorders = false;
            this.lblLuongNhap.ParentStyleUsing.UseFont = false;
            this.lblLuongNhap.Size = new System.Drawing.Size(75, 25);
            this.lblLuongNhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblLuongTonDau
            // 
            this.lblLuongTonDau.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongTonDau.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuongTonDau.Location = new System.Drawing.Point(317, 0);
            this.lblLuongTonDau.Name = "lblLuongTonDau";
            this.lblLuongTonDau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongTonDau.ParentStyleUsing.UseBorders = false;
            this.lblLuongTonDau.ParentStyleUsing.UseFont = false;
            this.lblLuongTonDau.Size = new System.Drawing.Size(75, 25);
            this.lblLuongTonDau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTenDVT_NPL
            // 
            this.lblTenDVT_NPL.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenDVT_NPL.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDVT_NPL.Location = new System.Drawing.Point(392, 0);
            this.lblTenDVT_NPL.Name = "lblTenDVT_NPL";
            this.lblTenDVT_NPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDVT_NPL.ParentStyleUsing.UseBorders = false;
            this.lblTenDVT_NPL.ParentStyleUsing.UseFont = false;
            this.lblTenDVT_NPL.Size = new System.Drawing.Size(33, 25);
            this.lblTenDVT_NPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Location = new System.Drawing.Point(425, 0);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.ParentStyleUsing.UseBorders = false;
            this.xrTableCell30.ParentStyleUsing.UseFont = false;
            this.xrTableCell30.Size = new System.Drawing.Size(92, 25);
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrTableCell31.Location = new System.Drawing.Point(517, 0);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell31.ParentStyleUsing.UseBorders = false;
            this.xrTableCell31.ParentStyleUsing.UseFont = false;
            this.xrTableCell31.Size = new System.Drawing.Size(33, 25);
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell32.Location = new System.Drawing.Point(550, 0);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell32.ParentStyleUsing.UseBorders = false;
            this.xrTableCell32.Size = new System.Drawing.Size(42, 25);
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.Location = new System.Drawing.Point(592, 0);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell33.ParentStyleUsing.UseBorders = false;
            this.xrTableCell33.Size = new System.Drawing.Size(41, 25);
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Location = new System.Drawing.Point(633, 0);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell34.ParentStyleUsing.UseBorders = false;
            this.xrTableCell34.Size = new System.Drawing.Size(50, 25);
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell35.Location = new System.Drawing.Point(683, 0);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell35.ParentStyleUsing.UseBorders = false;
            this.xrTableCell35.Size = new System.Drawing.Size(34, 25);
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell36.Location = new System.Drawing.Point(717, 0);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell36.ParentStyleUsing.UseBorders = false;
            this.xrTableCell36.Size = new System.Drawing.Size(58, 25);
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Location = new System.Drawing.Point(775, 0);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell37.ParentStyleUsing.UseBorders = false;
            this.xrTableCell37.Size = new System.Drawing.Size(67, 25);
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Location = new System.Drawing.Point(842, 0);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell38.ParentStyleUsing.UseBorders = false;
            this.xrTableCell38.Size = new System.Drawing.Size(33, 25);
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Location = new System.Drawing.Point(875, 0);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.ParentStyleUsing.UseBorders = false;
            this.xrTableCell5.Size = new System.Drawing.Size(42, 25);
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell39.Location = new System.Drawing.Point(917, 0);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell39.ParentStyleUsing.UseBorders = false;
            this.xrTableCell39.Size = new System.Drawing.Size(58, 25);
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell40.Location = new System.Drawing.Point(975, 0);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell40.ParentStyleUsing.UseBorders = false;
            this.xrTableCell40.Size = new System.Drawing.Size(67, 25);
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell41.Location = new System.Drawing.Point(1042, 0);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell41.ParentStyleUsing.UseBorders = false;
            this.xrTableCell41.Size = new System.Drawing.Size(83, 25);
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupFooter1.Height = 25;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupFooter1_BeforePrint);
            // 
            // xrTable3
            // 
            this.xrTable3.Location = new System.Drawing.Point(0, 0);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.ParentStyleUsing.UseBorders = false;
            this.xrTable3.ParentStyleUsing.UseBorderWidth = false;
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.Size = new System.Drawing.Size(1125, 25);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.lblToKhaiNhap,
            this.lblTongLuongNhap,
            this.lblTongTonDau,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell57,
            this.lblTongLuongNPLSuDung,
            this.lblSoToKhaiTaiXuat1,
            this.xrTableCell10,
            this.lblLuongNPLTaiXuat1,
            this.lblTongTonCuoi,
            this.lblTongThanhKhoanTiep});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.ParentStyleUsing.UseBackColor = false;
            this.xrTableRow3.ParentStyleUsing.UseBorders = false;
            this.xrTableRow3.Size = new System.Drawing.Size(1125, 25);
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell43.ParentStyleUsing.UseBorders = false;
            this.xrTableCell43.Size = new System.Drawing.Size(33, 25);
            // 
            // lblToKhaiNhap
            // 
            this.lblToKhaiNhap.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblToKhaiNhap.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToKhaiNhap.Location = new System.Drawing.Point(33, 0);
            this.lblToKhaiNhap.Name = "lblToKhaiNhap";
            this.lblToKhaiNhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblToKhaiNhap.ParentStyleUsing.UseBorders = false;
            this.lblToKhaiNhap.ParentStyleUsing.UseFont = false;
            this.lblToKhaiNhap.Size = new System.Drawing.Size(209, 25);
            this.lblToKhaiNhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblToKhaiNhap.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblToKhaiNhap_BeforePrint);
            // 
            // lblTongLuongNhap
            // 
            this.lblTongLuongNhap.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongLuongNhap.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongLuongNhap.Location = new System.Drawing.Point(242, 0);
            this.lblTongLuongNhap.Name = "lblTongLuongNhap";
            this.lblTongLuongNhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongLuongNhap.ParentStyleUsing.UseBorders = false;
            this.lblTongLuongNhap.ParentStyleUsing.UseFont = false;
            this.lblTongLuongNhap.Size = new System.Drawing.Size(75, 25);
            this.lblTongLuongNhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTongTonDau
            // 
            this.lblTongTonDau.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTonDau.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTonDau.Location = new System.Drawing.Point(317, 0);
            this.lblTongTonDau.Name = "lblTongTonDau";
            this.lblTongTonDau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTonDau.ParentStyleUsing.UseBorders = false;
            this.lblTongTonDau.ParentStyleUsing.UseFont = false;
            this.lblTongTonDau.Size = new System.Drawing.Size(75, 25);
            this.lblTongTonDau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell50.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell50.ParentStyleUsing.UseBorders = false;
            this.xrTableCell50.ParentStyleUsing.UseFont = false;
            this.xrTableCell50.Size = new System.Drawing.Size(33, 25);
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell51.Location = new System.Drawing.Point(425, 0);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell51.ParentStyleUsing.UseBorders = false;
            this.xrTableCell51.Size = new System.Drawing.Size(92, 25);
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell52.Location = new System.Drawing.Point(517, 0);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell52.ParentStyleUsing.UseBorders = false;
            this.xrTableCell52.Size = new System.Drawing.Size(33, 25);
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell53.Location = new System.Drawing.Point(550, 0);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell53.ParentStyleUsing.UseBorders = false;
            this.xrTableCell53.Size = new System.Drawing.Size(42, 25);
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell54.Location = new System.Drawing.Point(592, 0);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell54.ParentStyleUsing.UseBorders = false;
            this.xrTableCell54.Size = new System.Drawing.Size(41, 25);
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell55.Location = new System.Drawing.Point(633, 0);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell55.ParentStyleUsing.UseBorders = false;
            this.xrTableCell55.Size = new System.Drawing.Size(50, 25);
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell56.Location = new System.Drawing.Point(683, 0);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell56.ParentStyleUsing.UseBorders = false;
            this.xrTableCell56.Size = new System.Drawing.Size(34, 25);
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell57.Location = new System.Drawing.Point(717, 0);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell57.ParentStyleUsing.UseBorders = false;
            this.xrTableCell57.Size = new System.Drawing.Size(58, 25);
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblTongLuongNPLSuDung
            // 
            this.lblTongLuongNPLSuDung.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongLuongNPLSuDung.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongLuongNPLSuDung.Location = new System.Drawing.Point(775, 0);
            this.lblTongLuongNPLSuDung.Name = "lblTongLuongNPLSuDung";
            this.lblTongLuongNPLSuDung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblTongLuongNPLSuDung.ParentStyleUsing.UseBorders = false;
            this.lblTongLuongNPLSuDung.ParentStyleUsing.UseFont = false;
            this.lblTongLuongNPLSuDung.Size = new System.Drawing.Size(67, 25);
            this.lblTongLuongNPLSuDung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblSoToKhaiTaiXuat1
            // 
            this.lblSoToKhaiTaiXuat1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoToKhaiTaiXuat1.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiTaiXuat1.Location = new System.Drawing.Point(842, 0);
            this.lblSoToKhaiTaiXuat1.Name = "lblSoToKhaiTaiXuat1";
            this.lblSoToKhaiTaiXuat1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiTaiXuat1.ParentStyleUsing.UseBorders = false;
            this.lblSoToKhaiTaiXuat1.ParentStyleUsing.UseFont = false;
            this.lblSoToKhaiTaiXuat1.Size = new System.Drawing.Size(33, 25);
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Max;
            this.lblSoToKhaiTaiXuat1.Summary = xrSummary1;
            this.lblSoToKhaiTaiXuat1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSoToKhaiTaiXuat1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoToKhaiTaiXuat_BeforePrint_1);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Location = new System.Drawing.Point(875, 0);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.ParentStyleUsing.UseBorders = false;
            this.xrTableCell10.Size = new System.Drawing.Size(42, 25);
            // 
            // lblLuongNPLTaiXuat1
            // 
            this.lblLuongNPLTaiXuat1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongNPLTaiXuat1.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuongNPLTaiXuat1.Location = new System.Drawing.Point(917, 0);
            this.lblLuongNPLTaiXuat1.Name = "lblLuongNPLTaiXuat1";
            this.lblLuongNPLTaiXuat1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblLuongNPLTaiXuat1.ParentStyleUsing.UseBorders = false;
            this.lblLuongNPLTaiXuat1.ParentStyleUsing.UseFont = false;
            this.lblLuongNPLTaiXuat1.Size = new System.Drawing.Size(58, 25);
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Max;
            this.lblLuongNPLTaiXuat1.Summary = xrSummary2;
            this.lblLuongNPLTaiXuat1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongNPLTaiXuat1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblLuongNPLTaiXuat_BeforePrint_1);
            // 
            // lblTongTonCuoi
            // 
            this.lblTongTonCuoi.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTonCuoi.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTonCuoi.Location = new System.Drawing.Point(975, 0);
            this.lblTongTonCuoi.Name = "lblTongTonCuoi";
            this.lblTongTonCuoi.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblTongTonCuoi.ParentStyleUsing.UseBorders = false;
            this.lblTongTonCuoi.ParentStyleUsing.UseFont = false;
            this.lblTongTonCuoi.Size = new System.Drawing.Size(67, 25);
            xrSummary3.FormatString = "{0:n5}";
            xrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.Min;
            this.lblTongTonCuoi.Summary = xrSummary3;
            this.lblTongTonCuoi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTongThanhKhoanTiep
            // 
            this.lblTongThanhKhoanTiep.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongThanhKhoanTiep.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongThanhKhoanTiep.Location = new System.Drawing.Point(1042, 0);
            this.lblTongThanhKhoanTiep.Name = "lblTongThanhKhoanTiep";
            this.lblTongThanhKhoanTiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThanhKhoanTiep.ParentStyleUsing.UseBorders = false;
            this.lblTongThanhKhoanTiep.ParentStyleUsing.UseFont = false;
            this.lblTongThanhKhoanTiep.Size = new System.Drawing.Size(83, 25);
            this.lblTongThanhKhoanTiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTongThanhKhoanTiep.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblTongThanhKhoanTiep_BeforePrint);
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel3.Location = new System.Drawing.Point(0, 67);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(125, 25);
            this.xrLabel3.Text = "Tên Doanh nghiệp:";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel4.Location = new System.Drawing.Point(0, 92);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.ParentStyleUsing.UseFont = false;
            this.xrLabel4.Size = new System.Drawing.Size(125, 25);
            this.xrLabel4.Text = "Mã Doanh nghiệp:";
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lblTenDoanhNghiep.Location = new System.Drawing.Point(125, 67);
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblTenDoanhNghiep.Size = new System.Drawing.Size(708, 25);
            this.lblTenDoanhNghiep.Text = "lblTenDoanhNghiep";
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lblMaDoanhNghiep.Location = new System.Drawing.Point(125, 92);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblMaDoanhNghiep.Size = new System.Drawing.Size(141, 25);
            this.lblMaDoanhNghiep.Text = "lblMaDoanhNghiep";
            // 
            // lblSHSTK
            // 
            this.lblSHSTK.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lblSHSTK.Location = new System.Drawing.Point(142, 117);
            this.lblSHSTK.Name = "lblSHSTK";
            this.lblSHSTK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSHSTK.ParentStyleUsing.UseFont = false;
            this.lblSHSTK.Size = new System.Drawing.Size(166, 25);
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel8.Location = new System.Drawing.Point(0, 117);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.ParentStyleUsing.UseFont = false;
            this.xrLabel8.Size = new System.Drawing.Size(142, 25);
            this.xrLabel8.Text = "Số hồ sơ thanh khoản:";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9,
            this.xrLabel10,
            this.xrLabel12,
            this.xrLabel13,
            this.xrLabel15,
            this.xrLabel16,
            this.xrLabel17,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel25,
            this.xrLabel26,
            this.xrLabel28,
            this.xrLabel29,
            this.xrLabel30,
            this.xrLabel31,
            this.xrLabel35,
            this.xrLabel37,
            this.xrLabel39,
            this.xrLabel41,
            this.xrLabel42,
            this.xrLabel43,
            this.xrLabel46,
            this.xrLabel54,
            this.xrLabel65,
            this.xrLabel66,
            this.xrLabel1,
            this.xrLabel8,
            this.xrLabel3,
            this.xrLabel4,
            this.lblTenDoanhNghiep,
            this.lblMaDoanhNghiep,
            this.lblSHSTK,
            this.xrLabel2});
            this.ReportHeader.Height = 266;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel9
            // 
            this.xrLabel9.BackColor = System.Drawing.Color.Azure;
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.Location = new System.Drawing.Point(0, 158);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.ParentStyleUsing.UseBackColor = false;
            this.xrLabel9.ParentStyleUsing.UseBorders = false;
            this.xrLabel9.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel9.ParentStyleUsing.UseFont = false;
            this.xrLabel9.Size = new System.Drawing.Size(33, 108);
            this.xrLabel9.Text = "STT";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BackColor = System.Drawing.Color.Azure;
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.Location = new System.Drawing.Point(33, 158);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.ParentStyleUsing.UseBackColor = false;
            this.xrLabel10.ParentStyleUsing.UseBorders = false;
            this.xrLabel10.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel10.ParentStyleUsing.UseFont = false;
            this.xrLabel10.Size = new System.Drawing.Size(392, 25);
            this.xrLabel10.Text = "Nguyên liệu vật tư nhập khẩu";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.BackColor = System.Drawing.Color.Azure;
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel12.Location = new System.Drawing.Point(33, 183);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.ParentStyleUsing.UseBackColor = false;
            this.xrLabel12.ParentStyleUsing.UseBorders = false;
            this.xrLabel12.ParentStyleUsing.UseFont = false;
            this.xrLabel12.Size = new System.Drawing.Size(92, 83);
            this.xrLabel12.Text = "Tên/mã nguyên liệu, vật tư";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel13
            // 
            this.xrLabel13.BackColor = System.Drawing.Color.Azure;
            this.xrLabel13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.Location = new System.Drawing.Point(125, 183);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.ParentStyleUsing.UseBackColor = false;
            this.xrLabel13.ParentStyleUsing.UseBorders = false;
            this.xrLabel13.ParentStyleUsing.UseFont = false;
            this.xrLabel13.Size = new System.Drawing.Size(267, 25);
            this.xrLabel13.Text = "Tơ khai nhập khẩu";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BackColor = System.Drawing.Color.Azure;
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel15.Location = new System.Drawing.Point(125, 208);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.ParentStyleUsing.UseBackColor = false;
            this.xrLabel15.ParentStyleUsing.UseBorders = false;
            this.xrLabel15.ParentStyleUsing.UseFont = false;
            this.xrLabel15.Size = new System.Drawing.Size(75, 58);
            this.xrLabel15.Text = "Số/ ký hiệu ngày đăng ký";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.BackColor = System.Drawing.Color.Azure;
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel16.Location = new System.Drawing.Point(200, 208);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.ParentStyleUsing.UseBackColor = false;
            this.xrLabel16.ParentStyleUsing.UseBorders = false;
            this.xrLabel16.ParentStyleUsing.UseFont = false;
            this.xrLabel16.Size = new System.Drawing.Size(42, 58);
            this.xrLabel16.Text = "Ngày hòan thành";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BackColor = System.Drawing.Color.Azure;
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.Location = new System.Drawing.Point(242, 208);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.ParentStyleUsing.UseBackColor = false;
            this.xrLabel17.ParentStyleUsing.UseBorders = false;
            this.xrLabel17.ParentStyleUsing.UseFont = false;
            this.xrLabel17.Size = new System.Drawing.Size(75, 58);
            this.xrLabel17.Text = "Lượng Nliệu, V/tư NK";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.Azure;
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.Location = new System.Drawing.Point(317, 208);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.ParentStyleUsing.UseBackColor = false;
            this.xrLabel18.ParentStyleUsing.UseBorders = false;
            this.xrLabel18.ParentStyleUsing.UseFont = false;
            this.xrLabel18.Size = new System.Drawing.Size(75, 58);
            this.xrLabel18.Text = "Lượng NL tồn đầu kỳ";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.BackColor = System.Drawing.Color.Azure;
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.Location = new System.Drawing.Point(392, 183);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.ParentStyleUsing.UseBackColor = false;
            this.xrLabel19.ParentStyleUsing.UseBorders = false;
            this.xrLabel19.ParentStyleUsing.UseFont = false;
            this.xrLabel19.Size = new System.Drawing.Size(33, 83);
            this.xrLabel19.Text = "Đơn vị tính";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.BackColor = System.Drawing.Color.Azure;
            this.xrLabel25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.Location = new System.Drawing.Point(425, 158);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.ParentStyleUsing.UseBackColor = false;
            this.xrLabel25.ParentStyleUsing.UseBorders = false;
            this.xrLabel25.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel25.ParentStyleUsing.UseFont = false;
            this.xrLabel25.Size = new System.Drawing.Size(417, 25);
            this.xrLabel25.Text = "Nguyên liệu, vật tư đã sử dụng sản xuất sản phẩm xuất khẩu";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.BackColor = System.Drawing.Color.Azure;
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel26.Location = new System.Drawing.Point(425, 183);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.ParentStyleUsing.UseBackColor = false;
            this.xrLabel26.ParentStyleUsing.UseBorders = false;
            this.xrLabel26.ParentStyleUsing.UseFont = false;
            this.xrLabel26.Size = new System.Drawing.Size(92, 83);
            this.xrLabel26.Text = "Tên/mã SP xuất khẩu";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel28
            // 
            this.xrLabel28.BackColor = System.Drawing.Color.Azure;
            this.xrLabel28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel28.Location = new System.Drawing.Point(517, 183);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.ParentStyleUsing.UseBackColor = false;
            this.xrLabel28.ParentStyleUsing.UseBorders = false;
            this.xrLabel28.ParentStyleUsing.UseFont = false;
            this.xrLabel28.Size = new System.Drawing.Size(166, 25);
            this.xrLabel28.Text = "Tờ khai xuất khẩu";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel29
            // 
            this.xrLabel29.BackColor = System.Drawing.Color.Azure;
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.Location = new System.Drawing.Point(517, 208);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.ParentStyleUsing.UseBackColor = false;
            this.xrLabel29.ParentStyleUsing.UseBorders = false;
            this.xrLabel29.ParentStyleUsing.UseFont = false;
            this.xrLabel29.Size = new System.Drawing.Size(75, 58);
            this.xrLabel29.Text = "Số/ ký hiệu ngày đăng ký";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel30
            // 
            this.xrLabel30.BackColor = System.Drawing.Color.Azure;
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel30.Location = new System.Drawing.Point(592, 208);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.ParentStyleUsing.UseBackColor = false;
            this.xrLabel30.ParentStyleUsing.UseBorders = false;
            this.xrLabel30.ParentStyleUsing.UseFont = false;
            this.xrLabel30.Size = new System.Drawing.Size(41, 58);
            this.xrLabel30.Text = "Ngày hoàn thành";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel31
            // 
            this.xrLabel31.BackColor = System.Drawing.Color.Azure;
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel31.Location = new System.Drawing.Point(633, 208);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.ParentStyleUsing.UseBackColor = false;
            this.xrLabel31.ParentStyleUsing.UseBorders = false;
            this.xrLabel31.ParentStyleUsing.UseFont = false;
            this.xrLabel31.Size = new System.Drawing.Size(50, 58);
            this.xrLabel31.Text = "Lượng SP xuất khẩu";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel35
            // 
            this.xrLabel35.BackColor = System.Drawing.Color.Azure;
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel35.Location = new System.Drawing.Point(683, 183);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.ParentStyleUsing.UseBackColor = false;
            this.xrLabel35.ParentStyleUsing.UseBorders = false;
            this.xrLabel35.ParentStyleUsing.UseFont = false;
            this.xrLabel35.Size = new System.Drawing.Size(34, 83);
            this.xrLabel35.Text = "Đơn vị tính";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel37
            // 
            this.xrLabel37.BackColor = System.Drawing.Color.Azure;
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel37.Location = new System.Drawing.Point(717, 183);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.ParentStyleUsing.UseBackColor = false;
            this.xrLabel37.ParentStyleUsing.UseBorders = false;
            this.xrLabel37.ParentStyleUsing.UseFont = false;
            this.xrLabel37.Size = new System.Drawing.Size(58, 83);
            this.xrLabel37.Text = "Định mức NL VT (kể cả hao hụt)";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel39
            // 
            this.xrLabel39.BackColor = System.Drawing.Color.Azure;
            this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel39.Location = new System.Drawing.Point(775, 183);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.ParentStyleUsing.UseBackColor = false;
            this.xrLabel39.ParentStyleUsing.UseBorders = false;
            this.xrLabel39.ParentStyleUsing.UseFont = false;
            this.xrLabel39.Size = new System.Drawing.Size(67, 83);
            this.xrLabel39.Text = "Lượng NL, VT sử dụng SXXK";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel41
            // 
            this.xrLabel41.BackColor = System.Drawing.Color.Azure;
            this.xrLabel41.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel41.Location = new System.Drawing.Point(842, 158);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
            this.xrLabel41.ParentStyleUsing.UseBackColor = false;
            this.xrLabel41.ParentStyleUsing.UseBorders = false;
            this.xrLabel41.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel41.ParentStyleUsing.UseFont = false;
            this.xrLabel41.Size = new System.Drawing.Size(133, 25);
            this.xrLabel41.Text = "NL xuất trả lại";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel42
            // 
            this.xrLabel42.BackColor = System.Drawing.Color.Azure;
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel42.Location = new System.Drawing.Point(842, 183);
            this.xrLabel42.Multiline = true;
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.ParentStyleUsing.UseBackColor = false;
            this.xrLabel42.ParentStyleUsing.UseBorders = false;
            this.xrLabel42.ParentStyleUsing.UseFont = false;
            this.xrLabel42.Size = new System.Drawing.Size(75, 83);
            this.xrLabel42.Text = "Tờ khai XK (số, ký hiệu; ngày đăng ký)";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel43
            // 
            this.xrLabel43.BackColor = System.Drawing.Color.Azure;
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel43.Location = new System.Drawing.Point(917, 183);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.ParentStyleUsing.UseBackColor = false;
            this.xrLabel43.ParentStyleUsing.UseBorders = false;
            this.xrLabel43.ParentStyleUsing.UseFont = false;
            this.xrLabel43.Size = new System.Drawing.Size(58, 83);
            this.xrLabel43.Text = "Lượng NL,VT xuất trả lại";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel46
            // 
            this.xrLabel46.BackColor = System.Drawing.Color.Azure;
            this.xrLabel46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel46.Location = new System.Drawing.Point(975, 158);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.ParentStyleUsing.UseBackColor = false;
            this.xrLabel46.ParentStyleUsing.UseBorders = false;
            this.xrLabel46.ParentStyleUsing.UseFont = false;
            this.xrLabel46.Size = new System.Drawing.Size(67, 108);
            this.xrLabel46.Text = "Lượng NL, VT tồn cuối kỳ";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel54
            // 
            this.xrLabel54.BackColor = System.Drawing.Color.Azure;
            this.xrLabel54.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel54.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel54.Location = new System.Drawing.Point(1042, 158);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.ParentStyleUsing.UseBackColor = false;
            this.xrLabel54.ParentStyleUsing.UseBorders = false;
            this.xrLabel54.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel54.ParentStyleUsing.UseFont = false;
            this.xrLabel54.Size = new System.Drawing.Size(83, 25);
            this.xrLabel54.Text = "Xử lý NL tồn ";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel65
            // 
            this.xrLabel65.BackColor = System.Drawing.Color.Azure;
            this.xrLabel65.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel65.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel65.Location = new System.Drawing.Point(1042, 183);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.ParentStyleUsing.UseBackColor = false;
            this.xrLabel65.ParentStyleUsing.UseBorders = false;
            this.xrLabel65.ParentStyleUsing.UseFont = false;
            this.xrLabel65.Size = new System.Drawing.Size(41, 83);
            this.xrLabel65.Text = "Thanh khoản tiếp";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel66
            // 
            this.xrLabel66.BackColor = System.Drawing.Color.Azure;
            this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel66.Location = new System.Drawing.Point(1083, 183);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.ParentStyleUsing.UseBackColor = false;
            this.xrLabel66.ParentStyleUsing.UseBorders = false;
            this.xrLabel66.ParentStyleUsing.UseFont = false;
            this.xrLabel66.Size = new System.Drawing.Size(42, 83);
            this.xrLabel66.Text = "Chuyển mục đích khác";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.Name = "xrControlStyle2";
            // 
            // BC03_929
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.ReportFooter,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportHeader});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(21, 21, 48, 39);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.BCNPLXuatNhapTon_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lblSHSTK;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell lblL;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSP;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDangKyXuat;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHoanThanhXuat;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongSPXuat;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDVT_SP;
        private DevExpress.XtraReports.UI.XRTableCell lblDinhMuc;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongNPLSuDung;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongTonCuoi;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiTaiXuat;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongNPLTaiXuat;
        private DevExpress.XtraReports.UI.XRTableCell lblThanhKhoanTiep;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNPL;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiNhap;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDangKyNhap;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHoanThanhNhap;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongNhap;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongTonDau;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDVT_NPL;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell lblToKhaiNhap;
        private DevExpress.XtraReports.UI.XRTableCell lblTongLuongNhap;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTonDau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell lblTongLuongNPLSuDung;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiTaiXuat1;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongNPLTaiXuat1;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTonCuoi;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThanhKhoanTiep;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayTaiXuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
    }
}
