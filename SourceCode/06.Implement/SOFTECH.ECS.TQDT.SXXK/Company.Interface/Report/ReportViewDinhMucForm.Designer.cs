﻿namespace Company.Interface.Report
{
    partial class ReportViewDinhMucForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportViewDinhMucForm));
            this.printControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnXuatExcel = new Janus.Windows.EditControls.UIButton();
            this.txtHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPrint = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.cboSanPham = new Janus.Windows.EditControls.UIComboBox();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvSP = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.btnApply = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSP)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.printControl1);
            this.grbMain.Size = new System.Drawing.Size(963, 373);
            // 
            // printControl1
            // 
            this.printControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.printControl1.BackColor = System.Drawing.Color.Empty;
            this.printControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.printControl1.ForeColor = System.Drawing.Color.Empty;
            this.printControl1.IsMetric = false;
            this.printControl1.Location = new System.Drawing.Point(1, 59);
            this.printControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.printControl1.Name = "printControl1";
            this.printControl1.PrintingSystem = this.printingSystem1;
            this.printControl1.ShowPageMargins = false;
            this.printControl1.Size = new System.Drawing.Size(960, 312);
            this.printControl1.TabIndex = 1;
            this.printControl1.TooltipFont = new System.Drawing.Font("Tahoma", 8.25F);
            // 
            // printingSystem1
            // 
            this.printingSystem1.ShowMarginsWarning = false;
            this.printingSystem1.ShowPrintStatusDialog = false;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnApply);
            this.uiGroupBox1.Controls.Add(this.txtToKhai);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.btnXuatExcel);
            this.uiGroupBox1.Controls.Add(this.txtHopDong);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.btnPrint);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.cboSanPham);
            this.uiGroupBox1.Location = new System.Drawing.Point(1, 3);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(960, 50);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtToKhai
            // 
            this.txtToKhai.Location = new System.Drawing.Point(245, 18);
            this.txtToKhai.Name = "txtToKhai";
            this.txtToKhai.Size = new System.Drawing.Size(132, 21);
            this.txtToKhai.TabIndex = 7;
            this.txtToKhai.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(205, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tờ khai";
            // 
            // btnXuatExcel
            // 
            this.btnXuatExcel.Location = new System.Drawing.Point(814, 17);
            this.btnXuatExcel.Name = "btnXuatExcel";
            this.btnXuatExcel.Size = new System.Drawing.Size(84, 23);
            this.btnXuatExcel.TabIndex = 5;
            this.btnXuatExcel.Text = "&Xuất Excel";
            this.btnXuatExcel.VisualStyleManager = this.vsmMain;
            this.btnXuatExcel.Click += new System.EventHandler(this.btnXuatExcel_Click);
            // 
            // txtHopDong
            // 
            this.txtHopDong.Location = new System.Drawing.Point(70, 17);
            this.txtHopDong.Name = "txtHopDong";
            this.txtHopDong.Size = new System.Drawing.Size(132, 21);
            this.txtHopDong.TabIndex = 1;
            this.txtHopDong.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Hợp đồng";
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(733, 17);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 4;
            this.btnPrint.Text = "&In";
            this.btnPrint.VisualStyleManager = this.vsmMain;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(453, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Chọn sản phẩm";
            // 
            // cboSanPham
            // 
            this.cboSanPham.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Tờ khai chính";
            uiComboBoxItem2.Value = 0;
            this.cboSanPham.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem2});
            this.cboSanPham.Location = new System.Drawing.Point(538, 18);
            this.cboSanPham.Name = "cboSanPham";
            this.cboSanPham.Size = new System.Drawing.Size(189, 21);
            this.cboSanPham.TabIndex = 3;
            this.cboSanPham.VisualStyleManager = this.vsmMain;
            this.cboSanPham.SelectedIndexChanged += new System.EventHandler(this.cboToKhai_SelectedIndexChanged);
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvSP
            // 
            this.rfvSP.ControlToValidate = this.cboSanPham;
            this.rfvSP.ErrorMessage = "\"Chưa chọn sản phẩm\"";
            this.rfvSP.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSP.Icon")));
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(382, 17);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(67, 23);
            this.btnApply.TabIndex = 6;
            this.btnApply.Text = "Áp dụng";
            this.btnApply.VisualStyleManager = this.vsmMain;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // ReportViewDinhMucForm
            // 
            this.AcceptButton = this.btnPrint;
            this.ClientSize = new System.Drawing.Size(963, 373);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ReportViewDinhMucForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "In định mức sản phẩm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReportViewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPrinting.Control.PrintControl printControl1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cboSanPham;
        private Janus.Windows.EditControls.UIButton btnPrint;
        private System.Windows.Forms.Label label2;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSP;
        private Janus.Windows.GridEX.EditControls.EditBox txtHopDong;
        private Janus.Windows.EditControls.UIButton btnXuatExcel;
        private Janus.Windows.GridEX.EditControls.EditBox txtToKhai;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIButton btnApply;
    }
}