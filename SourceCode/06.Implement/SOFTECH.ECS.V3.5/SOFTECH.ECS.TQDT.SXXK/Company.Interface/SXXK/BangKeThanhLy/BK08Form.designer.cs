namespace Company.Interface.SXXK.BangKe
{
    partial class BK08Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout gridEX2_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference gridEX2_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK08Form));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.grpThongTinNPL = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNamDKXGC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamDKNhap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cmdAdd = new Janus.Windows.EditControls.UIButton();
            this.txtSoLuongXuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtLoaiHinhXGC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLoaiHinhNhap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtToKhaiXGC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtToKhaiNhap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gridEX2 = new Janus.Windows.GridEX.GridEX();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cmdSave = new Janus.Windows.EditControls.UIButton();
            this.rfvToKhaiXuat = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cvSoLuong = new Company.Controls.CustomValidation.CompareValidator();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvToKhaiNhap = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinNPL)).BeginInit();
            this.grpThongTinNPL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhaiXuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhaiNhap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.grpThongTinNPL);
            this.grbMain.Size = new System.Drawing.Size(913, 523);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // grpThongTinNPL
            // 
            this.grpThongTinNPL.AutoScroll = true;
            this.grpThongTinNPL.BackColor = System.Drawing.Color.Transparent;
            this.grpThongTinNPL.Controls.Add(this.txtNamDKXGC);
            this.grpThongTinNPL.Controls.Add(this.txtNamDKNhap);
            this.grpThongTinNPL.Controls.Add(this.label11);
            this.grpThongTinNPL.Controls.Add(this.label10);
            this.grpThongTinNPL.Controls.Add(this.cmdAdd);
            this.grpThongTinNPL.Controls.Add(this.txtSoLuongXuat);
            this.grpThongTinNPL.Controls.Add(this.label14);
            this.grpThongTinNPL.Controls.Add(this.label12);
            this.grpThongTinNPL.Controls.Add(this.label9);
            this.grpThongTinNPL.Controls.Add(this.txtLoaiHinhXGC);
            this.grpThongTinNPL.Controls.Add(this.txtTenNPL);
            this.grpThongTinNPL.Controls.Add(this.txtLoaiHinhNhap);
            this.grpThongTinNPL.Controls.Add(this.label8);
            this.grpThongTinNPL.Controls.Add(this.txtToKhaiXGC);
            this.grpThongTinNPL.Controls.Add(this.txtMaNPL);
            this.grpThongTinNPL.Controls.Add(this.label7);
            this.grpThongTinNPL.Controls.Add(this.txtToKhaiNhap);
            this.grpThongTinNPL.Controls.Add(this.label4);
            this.grpThongTinNPL.Controls.Add(this.label6);
            this.grpThongTinNPL.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpThongTinNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpThongTinNPL.Location = new System.Drawing.Point(0, 0);
            this.grpThongTinNPL.Name = "grpThongTinNPL";
            this.grpThongTinNPL.Size = new System.Drawing.Size(913, 147);
            this.grpThongTinNPL.TabIndex = 0;
            this.grpThongTinNPL.Text = "Thông tin NPL xuất theo loại hình XGC";
            this.grpThongTinNPL.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grpThongTinNPL.VisualStyleManager = this.vsmMain;
            // 
            // txtNamDKXGC
            // 
            this.txtNamDKXGC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamDKXGC.Location = new System.Drawing.Point(588, 75);
            this.txtNamDKXGC.Name = "txtNamDKXGC";
            this.txtNamDKXGC.ReadOnly = true;
            this.txtNamDKXGC.Size = new System.Drawing.Size(84, 21);
            this.txtNamDKXGC.TabIndex = 15;
            this.txtNamDKXGC.VisualStyleManager = this.vsmMain;
            // 
            // txtNamDKNhap
            // 
            this.txtNamDKNhap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamDKNhap.Location = new System.Drawing.Point(588, 20);
            this.txtNamDKNhap.Name = "txtNamDKNhap";
            this.txtNamDKNhap.ReadOnly = true;
            this.txtNamDKNhap.Size = new System.Drawing.Size(84, 21);
            this.txtNamDKNhap.TabIndex = 5;
            this.txtNamDKNhap.VisualStyleManager = this.vsmMain;
            this.txtNamDKNhap.TextChanged += new System.EventHandler(this.txtNamDKNhap_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(502, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Ngày đăng ký";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(502, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Ngày đăng ký";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAdd.Image = ((System.Drawing.Image)(resources.GetObject("cmdAdd.Image")));
            this.cmdAdd.Location = new System.Drawing.Point(392, 108);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(100, 23);
            this.cmdAdd.TabIndex = 18;
            this.cmdAdd.Text = "Thêm";
            this.cmdAdd.VisualStyleManager = this.vsmMain;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // txtSoLuongXuat
            // 
            this.txtSoLuongXuat.DecimalDigits = 3;
            this.txtSoLuongXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongXuat.Location = new System.Drawing.Point(122, 110);
            this.txtSoLuongXuat.Name = "txtSoLuongXuat";
            this.txtSoLuongXuat.Size = new System.Drawing.Size(200, 21);
            this.txtSoLuongXuat.TabIndex = 17;
            this.txtSoLuongXuat.Text = "0.000";
            this.txtSoLuongXuat.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoLuongXuat.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(33, 115);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Lượng NPL xuất";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(329, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Tên NPL";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(329, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Loại hình";
            // 
            // txtLoaiHinhXGC
            // 
            this.txtLoaiHinhXGC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoaiHinhXGC.Location = new System.Drawing.Point(392, 75);
            this.txtLoaiHinhXGC.Name = "txtLoaiHinhXGC";
            this.txtLoaiHinhXGC.ReadOnly = true;
            this.txtLoaiHinhXGC.Size = new System.Drawing.Size(100, 21);
            this.txtLoaiHinhXGC.TabIndex = 13;
            this.txtLoaiHinhXGC.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNPL.Location = new System.Drawing.Point(392, 48);
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.ReadOnly = true;
            this.txtTenNPL.Size = new System.Drawing.Size(280, 21);
            this.txtTenNPL.TabIndex = 9;
            this.txtTenNPL.VisualStyleManager = this.vsmMain;
            // 
            // txtLoaiHinhNhap
            // 
            this.txtLoaiHinhNhap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoaiHinhNhap.Location = new System.Drawing.Point(392, 20);
            this.txtLoaiHinhNhap.Name = "txtLoaiHinhNhap";
            this.txtLoaiHinhNhap.ReadOnly = true;
            this.txtLoaiHinhNhap.Size = new System.Drawing.Size(100, 21);
            this.txtLoaiHinhNhap.TabIndex = 3;
            this.txtLoaiHinhNhap.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(329, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Loại hình";
            // 
            // txtToKhaiXGC
            // 
            this.txtToKhaiXGC.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtToKhaiXGC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToKhaiXGC.Location = new System.Drawing.Point(122, 80);
            this.txtToKhaiXGC.Name = "txtToKhaiXGC";
            this.txtToKhaiXGC.Size = new System.Drawing.Size(200, 21);
            this.txtToKhaiXGC.TabIndex = 11;
            this.txtToKhaiXGC.VisualStyleManager = this.vsmMain;
            this.txtToKhaiXGC.ButtonClick += new System.EventHandler(this.txtToKhaiXGC_ButtonClick);
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNPL.Location = new System.Drawing.Point(122, 53);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.ReadOnly = true;
            this.txtMaNPL.Size = new System.Drawing.Size(200, 21);
            this.txtMaNPL.TabIndex = 7;
            this.txtMaNPL.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(32, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Tờ khai XGC";
            // 
            // txtToKhaiNhap
            // 
            this.txtToKhaiNhap.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtToKhaiNhap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToKhaiNhap.Location = new System.Drawing.Point(122, 26);
            this.txtToKhaiNhap.Name = "txtToKhaiNhap";
            this.txtToKhaiNhap.ReadOnly = true;
            this.txtToKhaiNhap.Size = new System.Drawing.Size(200, 21);
            this.txtToKhaiNhap.TabIndex = 1;
            this.txtToKhaiNhap.VisualStyleManager = this.vsmMain;
            this.txtToKhaiNhap.ButtonClick += new System.EventHandler(this.txtToKhaiNhap_ButtonClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "NPL tái xuất";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(32, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tờ khai nhập";
            // 
            // gridEX2
            // 
            this.gridEX2.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX2.AlternatingColors = true;
            this.gridEX2.AutomaticSort = false;
            this.gridEX2.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.gridEX2.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.gridEX2.ColumnAutoResize = true;
            gridEX2_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("gridEX2_DesignTimeLayout_Reference_0.Instance")));
            gridEX2_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            gridEX2_DesignTimeLayout_Reference_0});
            gridEX2_DesignTimeLayout.LayoutString = resources.GetString("gridEX2_DesignTimeLayout.LayoutString");
            this.gridEX2.DesignTimeLayout = gridEX2_DesignTimeLayout;
            this.gridEX2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEX2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX2.GroupByBoxVisible = false;
            this.gridEX2.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX2.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX2.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX2.ImageList = this.ImageList1;
            this.gridEX2.Location = new System.Drawing.Point(3, 8);
            this.gridEX2.Name = "gridEX2";
            this.gridEX2.RecordNavigator = true;
            this.gridEX2.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridEX2.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX2.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridEX2.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX2.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.gridEX2.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridEX2.Size = new System.Drawing.Size(907, 302);
            this.gridEX2.TabIndex = 0;
            this.gridEX2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX2.VisualStyleManager = this.vsmMain;
            this.gridEX2.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.gridEX2_LoadingRow);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(808, 17);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(95, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Location = new System.Drawing.Point(683, 17);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(116, 23);
            this.cmdSave.TabIndex = 3;
            this.cmdSave.Text = "Lưu bảng kê";
            this.cmdSave.VisualStyleManager = this.vsmMain;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // rfvToKhaiXuat
            // 
            this.rfvToKhaiXuat.ControlToValidate = this.txtToKhaiXGC;
            this.rfvToKhaiXuat.ErrorMessage = "\"Tờ khai XGC\" không được để trống.";
            this.rfvToKhaiXuat.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvToKhaiXuat.Icon")));
            // 
            // cvSoLuong
            // 
            this.cvSoLuong.ControlToValidate = this.txtSoLuongXuat;
            this.cvSoLuong.ErrorMessage = "\"Lượng NPL xuất\" không hợp lệ.";
            this.cvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoLuong.Icon")));
            this.cvSoLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSoLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoLuong.ValueToCompare = "0";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            this.epError.Icon = ((System.Drawing.Icon)(resources.GetObject("epError.Icon")));
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.lvsError.SetErrorCaption(this.cvError, "Thông báo");
            this.lvsError.SetErrorMessage(this.cvError, "Có một số lỗi sau:");
            this.cvError.HostingForm = this;
            // 
            // rfvToKhaiNhap
            // 
            this.rfvToKhaiNhap.ControlToValidate = this.txtToKhaiNhap;
            this.rfvToKhaiNhap.ErrorMessage = "\"Tờ khai nhập\" không được để trống.";
            this.rfvToKhaiNhap.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvToKhaiNhap.Icon")));
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(0, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(322, 14);
            this.label5.TabIndex = 5;
            this.label5.Text = "Danh sách nguyên phụ liệu xuất theo loại hình XGC";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.cmdSave);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 474);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(913, 49);
            this.uiGroupBox1.TabIndex = 6;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.gridEX2);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 161);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(913, 313);
            this.uiGroupBox3.TabIndex = 7;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // BK08Form
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(913, 523);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK08Form";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Bảng kê nguyên phụ liệu xuất theo loại hình XGC";
            this.Load += new System.EventHandler(this.BK08Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinNPL)).EndInit();
            this.grpThongTinNPL.ResumeLayout(false);
            this.grpThongTinNPL.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhaiXuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhaiNhap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.GridEX.GridEX gridEX1;
        private Janus.Windows.EditControls.UIGroupBox grpThongTinNPL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtToKhaiXGC;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtToKhaiNhap;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiHinhXGC;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiHinhNhap;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongXuat;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.EditControls.UIButton cmdAdd;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamDKXGC;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamDKNhap;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.GridEX gridEX2;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton cmdSave;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvToKhaiXuat;
        private Company.Controls.CustomValidation.CompareValidator cvSoLuong;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvToKhaiNhap;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
    }
}