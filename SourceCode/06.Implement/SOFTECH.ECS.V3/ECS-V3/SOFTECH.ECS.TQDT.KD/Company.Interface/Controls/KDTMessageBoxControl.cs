using System;
using System.Windows.Forms;
using Company.Interface;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using Company.KDT.SHARE.Components;

namespace Company.Controls
{

    public partial class KDTMessageBoxControl : BaseForm
    {
        public string ReturnValue = "Cancel";
        public string exceptionString = string.Empty;
        public string MaHQ = string.Empty;
        private string PhanHe = "KD";
        public long MessageID = 0;
        public bool ShowYesNoButton
        {
            set
            {
                this.btnNo.Visible = this.btnYes.Visible = value;
                this.btnCancel.Visible = !value;
                //if (!value) this.CancelButton = btnCancel;
                //else this.CancelButton = btnNo;
            }
        }
        public List<string> XmlFiles = new List<string>();
        public bool ShowErrorButton
        {
            set
            {
                this.btnError.Visible = value;
            }
        }
        public string MessageString
        {
            set
            {
                this.txtMessage.Text = value;

                string[] giaiThich = Company.KDT.SHARE.Components.Globals.TimThongBaoLoi(txtMessage.Text);
                if (giaiThich != null)
                {
                    txtGiaiThich.Text = giaiThich[1];
                }
            }
        }
        public string HQMessageString
        {
            set { this.rtxtHQMess.Text = value; }
        }

        public void dispEnglish()
        {
            this.Text = "Announcement";
            this.btnYes.Text = "&Yes";
            this.btnNo.Text = "&No";
            this.btnCancel.Text = "&Close";
        }
        public KDTMessageBoxControl()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "Yes";
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "No";
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MessageBoxControl_Load(object sender, EventArgs e)
        {
#if KD_V3
            if (Company.Interface.Properties.Settings.Default.NgonNgu == "1") this.dispEnglish();
            PhanHe = "KD";
#elif SXXK_V3
            if (Company.Interface.GlobalSettings.NGON_NGU == "1") this.dispEnglish();
            PhanHe = "SXXK";
#elif GC_V3
            PhanHe = "GC";
#endif
            if (MessageID == 0)
                mnItemExportXml.Visible = false;
        }

        private void btnError_Click(object sender, EventArgs e)
        {
            try
            {
                #region Tệp tin đính kèm (Hình Ảnh)
                if (System.IO.Directory.Exists("Errors") == false)
                {
                    System.IO.Directory.CreateDirectory("Errors");
                }
                string fileName = Application.StartupPath + string.Format("\\Errors\\{0} - {1}.jpg", GlobalSettings.MA_DON_VI.Trim(), DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss"));
                if (!Company.KDT.SHARE.Components.Globals.SaveAsImage(fileName))
                    fileName = "";
                #endregion Hình Ảnh

                #region Nội dung
                string subject = string.Format("{0} - ECS.TQDT.{1}.V3", GlobalSettings.TEN_DON_VI, PhanHe);
#if GC_V3
                subject = string.Format("{0} - ECS.TQDT.GC.V3", GlobalSettings.TEN_DON_VI, PhanHe);
#elif SXXK_V3
                subject = string.Format("{0} - ECS.TQDT.SXXK.V3", GlobalSettings.TEN_DON_VI, PhanHe);
#endif
                string tenDN = GlobalSettings.TEN_DON_VI;
                string maDN = GlobalSettings.MA_DON_VI;
                string soDT = GlobalSettings.SoDienThoaiDN;
                string soFax = GlobalSettings.SoFaxDN;
                string nguoiLH = GlobalSettings.NguoiLienHe;
                if (MessageID != 0)
                {
                    Company.KDT.SHARE.Components.Message msg = Company.KDT.SHARE.Components.Message.Load((int)MessageID);
                    if (msg != null)
                    {
                        string file = Company.KDT.SHARE.Components.Globals.Message2File(msg.MessageContent);
                        if (!string.IsNullOrEmpty(file))
                            XmlFiles.Add(file);
                    }
                }

                //Hungtq update 16/07/2012.
                string loaiHinh = PhanHe;
                string appVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(Application.ExecutablePath).ProductVersion.ToString();
                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                dataVersion = (dataVersion != "" ? dataVersion : "?");

                bool isSend = Company.KDT.SHARE.Components.Globals.sendEmail(subject, txtMessage.Text, fileName, tenDN, maDN, MaHQ, soDT, soFax, nguoiLH, GlobalSettings.DIA_CHI, string.Empty, loaiHinh, appVersion, dataVersion, this.exceptionString, XmlFiles);
                #endregion Nội dung
                if (isSend)
                {
                    ShowMessage("Cảm ơn bạn đã gửi thông tin đến chúng tôi\r\nChúng tôi sẽ sớm trả lời bạn trong thời gian nhanh nhất", false);
                    DialogResult = DialogResult.OK;
                }
                else ShowMessage("Hệ thống không thể gửi email lỗi của bạn", false);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Xảy ra lỗi khi gởi thông tin lỗi tới nhà cung cấp phần mềm", false);
            }
        }

        private void txtMessage_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process p = new Process();
            Process.Start("IExplore.exe", e.LinkText);

        }

        private void mnItemSelectAll_Click(object sender, EventArgs e)
        {
            txtMessage.SelectAll();
        }

        private void mnitemCopy_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtMessage.SelectedText)) return;
            Clipboard.Clear();
            Clipboard.SetText(txtMessage.SelectedText);
        }

        private void mnItemExportXml_Click(object sender, EventArgs e)
        {
            if (MessageID == 0) return;
            else
            {
                try
                {
                    Company.KDT.SHARE.Components.Message msg = Company.KDT.SHARE.Components.Message.Load((int)MessageID);
                    if (msg != null && !string.IsNullOrEmpty(msg.MessageContent))
                    {
                        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                        saveFileDialog1.FileName = string.Format("{0}_{1}_{2}", new object[]{
                            GlobalSettings.MA_HAI_QUAN,GlobalSettings.MA_DON_VI,msg.ReferenceID});
                        saveFileDialog1.Filter = "Mẫu xml|*.xml";
                        saveFileDialog1.FilterIndex = 2;
                        saveFileDialog1.RestoreDirectory = true;
                        saveFileDialog1.Title = "Lưu dữ liệu mẫu";

                        Stream myStream;
                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            if ((myStream = saveFileDialog1.OpenFile()) != null)
                            {
                                // Code to write the stream goes here.

                                byte[] byteArray = System.Text.Encoding.ASCII.GetBytes(msg.MessageContent);
                                myStream.Write(byteArray, 0, byteArray.Length);
                                myStream.Close();
                            }
                            ShowMessage("Đã xuất dữ liệu", false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }




    }
}