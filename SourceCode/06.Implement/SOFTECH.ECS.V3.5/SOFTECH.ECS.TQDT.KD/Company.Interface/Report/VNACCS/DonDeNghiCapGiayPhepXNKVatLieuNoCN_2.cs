using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class DonDeNghiCapGiayPhepXNKVatLieuNoCN_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public DonDeNghiCapGiayPhepXNKVatLieuNoCN_2()
        {
            InitializeComponent();
        }
        public void BindingReport(VAHP010 vahp)
        {
            int HangHoa = vahp.HangHoa.Count;
            if (HangHoa <= 10)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 10, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            DataTable dt = new DataTable();
            if (vahp != null && vahp.HangHoa.Count > 0)
            {
                int STT = 0;
                dt.Columns.Add("STT", typeof(string));
                dt.Columns.Add("TenHang", typeof(string));
                dt.Columns.Add("MaSoHH", typeof(string));
                dt.Columns.Add("SoLuong", typeof(string));
                dt.Columns.Add("DonVi", typeof(string));
                for (int i = 0; i < vahp.HangHoa.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["STT"] = STT.ToString().Trim();
                    dr["TenHang"] = vahp.HangHoa[i].B01.GetValue().ToString().Trim();
                    dr["MaSoHH"] = vahp.HangHoa[i].B02.GetValue().ToString().Trim();
                    dr["SoLuong"] = vahp.HangHoa[i].B03.GetValue().ToString().Trim();
                    dr["DonVi"] = vahp.HangHoa[i].B04.GetValue().ToString().Trim();
                    dt.Rows.Add(dr);
                }
                BindingReportHang(dt);
            }
        }
        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblTenHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "TenHang");
            lblMaSoHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaSoHH");
            lblSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong");
            lblDVTSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "DonVi");
        }

        private void lblSoTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 2).ToString();
        }


        


    }
}
