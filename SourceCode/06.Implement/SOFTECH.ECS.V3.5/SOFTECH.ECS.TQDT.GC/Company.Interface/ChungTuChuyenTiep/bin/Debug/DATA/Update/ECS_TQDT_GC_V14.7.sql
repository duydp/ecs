Alter TABLE t_KDT_VNACC_HangMauDich
alter column SoLuong1 numeric(12,4) null

Alter TABLE t_KDT_VNACC_HangMauDich
alter column SoLuong2 numeric(12,4) null


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO




ALTER PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Insert]
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 0),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 0),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 0),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(13, 6),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue varchar(12),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60),
	@MaHangHoa varchar(48),
	@Templ_1 varchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich]
(
	[TKMD_ID],
	[MaSoHang],
	[MaQuanLy],
	[TenHang],
	[ThueSuat],
	[ThueSuatTuyetDoi],
	[MaDVTTuyetDoi],
	[MaTTTuyetDoi],
	[NuocXuatXu],
	[SoLuong1],
	[DVTLuong1],
	[SoLuong2],
	[DVTLuong2],
	[TriGiaHoaDon],
	[DonGiaHoaDon],
	[MaTTDonGia],
	[DVTDonGia],
	[MaBieuThueNK],
	[MaHanNgach],
	[MaThueNKTheoLuong],
	[MaMienGiamThue],
	[SoTienGiamThue],
	[TriGiaTinhThue],
	[MaTTTriGiaTinhThue],
	[SoMucKhaiKhoanDC],
	[SoTTDongHangTKTNTX],
	[SoDMMienThue],
	[SoDongDMMienThue],
	[MaMienGiam],
	[SoTienMienGiam],
	[MaTTSoTienMienGiam],
	[MaVanBanPhapQuyKhac1],
	[MaVanBanPhapQuyKhac2],
	[MaVanBanPhapQuyKhac3],
	[MaVanBanPhapQuyKhac4],
	[MaVanBanPhapQuyKhac5],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam1],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam],
	[MaHangHoa],
	[Templ_1]
)
VALUES 
(
	@TKMD_ID,
	@MaSoHang,
	@MaQuanLy,
	@TenHang,
	@ThueSuat,
	@ThueSuatTuyetDoi,
	@MaDVTTuyetDoi,
	@MaTTTuyetDoi,
	@NuocXuatXu,
	@SoLuong1,
	@DVTLuong1,
	@SoLuong2,
	@DVTLuong2,
	@TriGiaHoaDon,
	@DonGiaHoaDon,
	@MaTTDonGia,
	@DVTDonGia,
	@MaBieuThueNK,
	@MaHanNgach,
	@MaThueNKTheoLuong,
	@MaMienGiamThue,
	@SoTienGiamThue,
	@TriGiaTinhThue,
	@MaTTTriGiaTinhThue,
	@SoMucKhaiKhoanDC,
	@SoTTDongHangTKTNTX,
	@SoDMMienThue,
	@SoDongDMMienThue,
	@MaMienGiam,
	@SoTienMienGiam,
	@MaTTSoTienMienGiam,
	@MaVanBanPhapQuyKhac1,
	@MaVanBanPhapQuyKhac2,
	@MaVanBanPhapQuyKhac3,
	@MaVanBanPhapQuyKhac4,
	@MaVanBanPhapQuyKhac5,
	@SoDong,
	@MaPhanLoaiTaiXacNhanGia,
	@TenNoiXuatXu,
	@SoLuongTinhThue,
	@MaDVTDanhThue,
	@DonGiaTinhThue,
	@DV_SL_TrongDonGiaTinhThue,
	@MaTTDonGiaTinhThue,
	@TriGiaTinhThueS,
	@MaTTTriGiaTinhThueS,
	@MaTTSoTienMienGiam1,
	@MaPhanLoaiThueSuatThue,
	@ThueSuatThue,
	@PhanLoaiThueSuatThue,
	@SoTienThue,
	@MaTTSoTienThueXuatKhau,
	@TienLePhi_DonGia,
	@TienBaoHiem_DonGia,
	@TienLePhi_SoLuong,
	@TienLePhi_MaDVSoLuong,
	@TienBaoHiem_SoLuong,
	@TienBaoHiem_MaDVSoLuong,
	@TienLePhi_KhoanTien,
	@TienBaoHiem_KhoanTien,
	@DieuKhoanMienGiam,
	@MaHangHoa,
	@Templ_1
)

SET @ID = SCOPE_IDENTITY()




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 0),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 4),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(13, 6),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue varchar(12),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60),
	@MaHangHoa varchar(48),
	@Templ_1 varchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaSoHang] = @MaSoHang,
			[MaQuanLy] = @MaQuanLy,
			[TenHang] = @TenHang,
			[ThueSuat] = @ThueSuat,
			[ThueSuatTuyetDoi] = @ThueSuatTuyetDoi,
			[MaDVTTuyetDoi] = @MaDVTTuyetDoi,
			[MaTTTuyetDoi] = @MaTTTuyetDoi,
			[NuocXuatXu] = @NuocXuatXu,
			[SoLuong1] = @SoLuong1,
			[DVTLuong1] = @DVTLuong1,
			[SoLuong2] = @SoLuong2,
			[DVTLuong2] = @DVTLuong2,
			[TriGiaHoaDon] = @TriGiaHoaDon,
			[DonGiaHoaDon] = @DonGiaHoaDon,
			[MaTTDonGia] = @MaTTDonGia,
			[DVTDonGia] = @DVTDonGia,
			[MaBieuThueNK] = @MaBieuThueNK,
			[MaHanNgach] = @MaHanNgach,
			[MaThueNKTheoLuong] = @MaThueNKTheoLuong,
			[MaMienGiamThue] = @MaMienGiamThue,
			[SoTienGiamThue] = @SoTienGiamThue,
			[TriGiaTinhThue] = @TriGiaTinhThue,
			[MaTTTriGiaTinhThue] = @MaTTTriGiaTinhThue,
			[SoMucKhaiKhoanDC] = @SoMucKhaiKhoanDC,
			[SoTTDongHangTKTNTX] = @SoTTDongHangTKTNTX,
			[SoDMMienThue] = @SoDMMienThue,
			[SoDongDMMienThue] = @SoDongDMMienThue,
			[MaMienGiam] = @MaMienGiam,
			[SoTienMienGiam] = @SoTienMienGiam,
			[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
			[MaVanBanPhapQuyKhac1] = @MaVanBanPhapQuyKhac1,
			[MaVanBanPhapQuyKhac2] = @MaVanBanPhapQuyKhac2,
			[MaVanBanPhapQuyKhac3] = @MaVanBanPhapQuyKhac3,
			[MaVanBanPhapQuyKhac4] = @MaVanBanPhapQuyKhac4,
			[MaVanBanPhapQuyKhac5] = @MaVanBanPhapQuyKhac5,
			[SoDong] = @SoDong,
			[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
			[TenNoiXuatXu] = @TenNoiXuatXu,
			[SoLuongTinhThue] = @SoLuongTinhThue,
			[MaDVTDanhThue] = @MaDVTDanhThue,
			[DonGiaTinhThue] = @DonGiaTinhThue,
			[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
			[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
			[TriGiaTinhThueS] = @TriGiaTinhThueS,
			[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
			[MaTTSoTienMienGiam1] = @MaTTSoTienMienGiam1,
			[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
			[ThueSuatThue] = @ThueSuatThue,
			[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
			[SoTienThue] = @SoTienThue,
			[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
			[TienLePhi_DonGia] = @TienLePhi_DonGia,
			[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
			[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
			[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
			[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
			[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
			[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
			[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
			[DieuKhoanMienGiam] = @DieuKhoanMienGiam,
			[MaHangHoa] = @MaHangHoa,
			[Templ_1] = @Templ_1
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich]
		(
			[TKMD_ID],
			[MaSoHang],
			[MaQuanLy],
			[TenHang],
			[ThueSuat],
			[ThueSuatTuyetDoi],
			[MaDVTTuyetDoi],
			[MaTTTuyetDoi],
			[NuocXuatXu],
			[SoLuong1],
			[DVTLuong1],
			[SoLuong2],
			[DVTLuong2],
			[TriGiaHoaDon],
			[DonGiaHoaDon],
			[MaTTDonGia],
			[DVTDonGia],
			[MaBieuThueNK],
			[MaHanNgach],
			[MaThueNKTheoLuong],
			[MaMienGiamThue],
			[SoTienGiamThue],
			[TriGiaTinhThue],
			[MaTTTriGiaTinhThue],
			[SoMucKhaiKhoanDC],
			[SoTTDongHangTKTNTX],
			[SoDMMienThue],
			[SoDongDMMienThue],
			[MaMienGiam],
			[SoTienMienGiam],
			[MaTTSoTienMienGiam],
			[MaVanBanPhapQuyKhac1],
			[MaVanBanPhapQuyKhac2],
			[MaVanBanPhapQuyKhac3],
			[MaVanBanPhapQuyKhac4],
			[MaVanBanPhapQuyKhac5],
			[SoDong],
			[MaPhanLoaiTaiXacNhanGia],
			[TenNoiXuatXu],
			[SoLuongTinhThue],
			[MaDVTDanhThue],
			[DonGiaTinhThue],
			[DV_SL_TrongDonGiaTinhThue],
			[MaTTDonGiaTinhThue],
			[TriGiaTinhThueS],
			[MaTTTriGiaTinhThueS],
			[MaTTSoTienMienGiam1],
			[MaPhanLoaiThueSuatThue],
			[ThueSuatThue],
			[PhanLoaiThueSuatThue],
			[SoTienThue],
			[MaTTSoTienThueXuatKhau],
			[TienLePhi_DonGia],
			[TienBaoHiem_DonGia],
			[TienLePhi_SoLuong],
			[TienLePhi_MaDVSoLuong],
			[TienBaoHiem_SoLuong],
			[TienBaoHiem_MaDVSoLuong],
			[TienLePhi_KhoanTien],
			[TienBaoHiem_KhoanTien],
			[DieuKhoanMienGiam],
			[MaHangHoa],
			[Templ_1]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaSoHang,
			@MaQuanLy,
			@TenHang,
			@ThueSuat,
			@ThueSuatTuyetDoi,
			@MaDVTTuyetDoi,
			@MaTTTuyetDoi,
			@NuocXuatXu,
			@SoLuong1,
			@DVTLuong1,
			@SoLuong2,
			@DVTLuong2,
			@TriGiaHoaDon,
			@DonGiaHoaDon,
			@MaTTDonGia,
			@DVTDonGia,
			@MaBieuThueNK,
			@MaHanNgach,
			@MaThueNKTheoLuong,
			@MaMienGiamThue,
			@SoTienGiamThue,
			@TriGiaTinhThue,
			@MaTTTriGiaTinhThue,
			@SoMucKhaiKhoanDC,
			@SoTTDongHangTKTNTX,
			@SoDMMienThue,
			@SoDongDMMienThue,
			@MaMienGiam,
			@SoTienMienGiam,
			@MaTTSoTienMienGiam,
			@MaVanBanPhapQuyKhac1,
			@MaVanBanPhapQuyKhac2,
			@MaVanBanPhapQuyKhac3,
			@MaVanBanPhapQuyKhac4,
			@MaVanBanPhapQuyKhac5,
			@SoDong,
			@MaPhanLoaiTaiXacNhanGia,
			@TenNoiXuatXu,
			@SoLuongTinhThue,
			@MaDVTDanhThue,
			@DonGiaTinhThue,
			@DV_SL_TrongDonGiaTinhThue,
			@MaTTDonGiaTinhThue,
			@TriGiaTinhThueS,
			@MaTTTriGiaTinhThueS,
			@MaTTSoTienMienGiam1,
			@MaPhanLoaiThueSuatThue,
			@ThueSuatThue,
			@PhanLoaiThueSuatThue,
			@SoTienThue,
			@MaTTSoTienThueXuatKhau,
			@TienLePhi_DonGia,
			@TienBaoHiem_DonGia,
			@TienLePhi_SoLuong,
			@TienLePhi_MaDVSoLuong,
			@TienBaoHiem_SoLuong,
			@TienBaoHiem_MaDVSoLuong,
			@TienLePhi_KhoanTien,
			@TienBaoHiem_KhoanTien,
			@DieuKhoanMienGiam,
			@MaHangHoa,
			@Templ_1
		)		
	END



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 0),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 4),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(13, 6),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue varchar(12),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60),
	@MaHangHoa varchar(48),
	@Templ_1 varchar(250)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaSoHang] = @MaSoHang,
	[MaQuanLy] = @MaQuanLy,
	[TenHang] = @TenHang,
	[ThueSuat] = @ThueSuat,
	[ThueSuatTuyetDoi] = @ThueSuatTuyetDoi,
	[MaDVTTuyetDoi] = @MaDVTTuyetDoi,
	[MaTTTuyetDoi] = @MaTTTuyetDoi,
	[NuocXuatXu] = @NuocXuatXu,
	[SoLuong1] = @SoLuong1,
	[DVTLuong1] = @DVTLuong1,
	[SoLuong2] = @SoLuong2,
	[DVTLuong2] = @DVTLuong2,
	[TriGiaHoaDon] = @TriGiaHoaDon,
	[DonGiaHoaDon] = @DonGiaHoaDon,
	[MaTTDonGia] = @MaTTDonGia,
	[DVTDonGia] = @DVTDonGia,
	[MaBieuThueNK] = @MaBieuThueNK,
	[MaHanNgach] = @MaHanNgach,
	[MaThueNKTheoLuong] = @MaThueNKTheoLuong,
	[MaMienGiamThue] = @MaMienGiamThue,
	[SoTienGiamThue] = @SoTienGiamThue,
	[TriGiaTinhThue] = @TriGiaTinhThue,
	[MaTTTriGiaTinhThue] = @MaTTTriGiaTinhThue,
	[SoMucKhaiKhoanDC] = @SoMucKhaiKhoanDC,
	[SoTTDongHangTKTNTX] = @SoTTDongHangTKTNTX,
	[SoDMMienThue] = @SoDMMienThue,
	[SoDongDMMienThue] = @SoDongDMMienThue,
	[MaMienGiam] = @MaMienGiam,
	[SoTienMienGiam] = @SoTienMienGiam,
	[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
	[MaVanBanPhapQuyKhac1] = @MaVanBanPhapQuyKhac1,
	[MaVanBanPhapQuyKhac2] = @MaVanBanPhapQuyKhac2,
	[MaVanBanPhapQuyKhac3] = @MaVanBanPhapQuyKhac3,
	[MaVanBanPhapQuyKhac4] = @MaVanBanPhapQuyKhac4,
	[MaVanBanPhapQuyKhac5] = @MaVanBanPhapQuyKhac5,
	[SoDong] = @SoDong,
	[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
	[TenNoiXuatXu] = @TenNoiXuatXu,
	[SoLuongTinhThue] = @SoLuongTinhThue,
	[MaDVTDanhThue] = @MaDVTDanhThue,
	[DonGiaTinhThue] = @DonGiaTinhThue,
	[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
	[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
	[TriGiaTinhThueS] = @TriGiaTinhThueS,
	[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
	[MaTTSoTienMienGiam1] = @MaTTSoTienMienGiam1,
	[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
	[ThueSuatThue] = @ThueSuatThue,
	[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
	[SoTienThue] = @SoTienThue,
	[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
	[TienLePhi_DonGia] = @TienLePhi_DonGia,
	[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
	[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
	[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
	[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
	[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
	[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
	[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
	[DieuKhoanMienGiam] = @DieuKhoanMienGiam,
	[MaHangHoa] = @MaHangHoa,
	[Templ_1] = @Templ_1
WHERE
	[ID] = @ID


---------------------------------------------------------------------------------Alter t_kdt_VNACC_toKhaiVanChuyen -------------
alter Table t_KDT_VNACC_ToKhaiVanChuyen
alter column GioDuKienBatDauVC varchar(2) null 

alter Table t_KDT_VNACC_ToKhaiVanChuyen
alter column GioDuKienKetThucVC varchar(2) null
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@TenThongTinXuat nvarchar(43),
	@MaPhanLoaiXuLy varchar(1),
	@MaCoBaoNiemPhong varchar(1),
	@TenCoBaoNiemPhong nvarchar(35),
	@CoQuanHaiQuan varchar(10),
	@SoToKhaiVC numeric(12, 0),
	@CoBaoXuatNhapKhau varchar(1),
	@NgayLapToKhai datetime,
	@MaNguoiKhai varchar(5),
	@TenNguoiKhai varchar(50),
	@DiaChiNguoiKhai nvarchar(100),
	@MaNguoiVC varchar(13),
	@TenNguoiVC nvarchar(100),
	@DiaChiNguoiVC nvarchar(100),
	@SoHopDongVC varchar(11),
	@NgayHopDongVC datetime,
	@NgayHetHanHopDongVC datetime,
	@MaPhuongTienVC varchar(2),
	@TenPhuongTieVC varchar(12),
	@MaMucDichVC varchar(3),
	@TenMucDichVC nvarchar(70),
	@LoaiHinhVanTai varchar(2),
	@TenLoaiHinhVanTai nvarchar(70),
	@MaDiaDiemXepHang varchar(7),
	@MaViTriXepHang varchar(6),
	@MaCangCuaKhauGaXepHang varchar(6),
	@MaCangXHKhongCo_HT varchar(1),
	@DiaDiemXepHang nvarchar(35),
	@NgayDenDiaDiem_XH datetime,
	@MaDiaDiemDoHang varchar(7),
	@MaViTriDoHang varchar(6),
	@MaCangCuaKhauGaDoHang varchar(6),
	@MaCangDHKhongCo_HT varchar(1),
	@DiaDiemDoHang nvarchar(35),
	@NgayDenDiaDiem_DH datetime,
	@TuyenDuongVC varchar(35),
	@LoaiBaoLanh varchar(1),
	@SoTienBaoLanh numeric(11, 0),
	@SoLuongCot_TK int,
	@SoLuongContainer int,
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh int,
	@KyHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@MaVach numeric(12, 0),
	@NgayPheDuyetVC datetime,
	@NgayDuKienBatDauVC datetime,
	@GioDuKienBatDauVC varchar(2),
	@NgayDuKienKetThucVC datetime,
	@GioDuKienKetThucVC varchar(2),
	@MaBuuChinhHQ varchar(7),
	@DiaChiBuuChinhHQ nvarchar(54),
	@TenBuuChinhHQ nvarchar(34),
	@GhiChu nvarchar(255),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
(
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@NgayDangKy,
	@TenThongTinXuat,
	@MaPhanLoaiXuLy,
	@MaCoBaoNiemPhong,
	@TenCoBaoNiemPhong,
	@CoQuanHaiQuan,
	@SoToKhaiVC,
	@CoBaoXuatNhapKhau,
	@NgayLapToKhai,
	@MaNguoiKhai,
	@TenNguoiKhai,
	@DiaChiNguoiKhai,
	@MaNguoiVC,
	@TenNguoiVC,
	@DiaChiNguoiVC,
	@SoHopDongVC,
	@NgayHopDongVC,
	@NgayHetHanHopDongVC,
	@MaPhuongTienVC,
	@TenPhuongTieVC,
	@MaMucDichVC,
	@TenMucDichVC,
	@LoaiHinhVanTai,
	@TenLoaiHinhVanTai,
	@MaDiaDiemXepHang,
	@MaViTriXepHang,
	@MaCangCuaKhauGaXepHang,
	@MaCangXHKhongCo_HT,
	@DiaDiemXepHang,
	@NgayDenDiaDiem_XH,
	@MaDiaDiemDoHang,
	@MaViTriDoHang,
	@MaCangCuaKhauGaDoHang,
	@MaCangDHKhongCo_HT,
	@DiaDiemDoHang,
	@NgayDenDiaDiem_DH,
	@TuyenDuongVC,
	@LoaiBaoLanh,
	@SoTienBaoLanh,
	@SoLuongCot_TK,
	@SoLuongContainer,
	@MaNganHangBaoLanh,
	@NamPhatHanhBaoLanh,
	@KyHieuChungTuBaoLanh,
	@SoChungTuBaoLanh,
	@MaVach,
	@NgayPheDuyetVC,
	@NgayDuKienBatDauVC,
	@GioDuKienBatDauVC,
	@NgayDuKienKetThucVC,
	@GioDuKienKetThucVC,
	@MaBuuChinhHQ,
	@DiaChiBuuChinhHQ,
	@TenBuuChinhHQ,
	@GhiChu,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@TenThongTinXuat nvarchar(43),
	@MaPhanLoaiXuLy varchar(1),
	@MaCoBaoNiemPhong varchar(1),
	@TenCoBaoNiemPhong nvarchar(35),
	@CoQuanHaiQuan varchar(10),
	@SoToKhaiVC numeric(12, 0),
	@CoBaoXuatNhapKhau varchar(1),
	@NgayLapToKhai datetime,
	@MaNguoiKhai varchar(5),
	@TenNguoiKhai varchar(50),
	@DiaChiNguoiKhai nvarchar(100),
	@MaNguoiVC varchar(13),
	@TenNguoiVC nvarchar(100),
	@DiaChiNguoiVC nvarchar(100),
	@SoHopDongVC varchar(11),
	@NgayHopDongVC datetime,
	@NgayHetHanHopDongVC datetime,
	@MaPhuongTienVC varchar(2),
	@TenPhuongTieVC varchar(12),
	@MaMucDichVC varchar(3),
	@TenMucDichVC nvarchar(70),
	@LoaiHinhVanTai varchar(2),
	@TenLoaiHinhVanTai nvarchar(70),
	@MaDiaDiemXepHang varchar(7),
	@MaViTriXepHang varchar(6),
	@MaCangCuaKhauGaXepHang varchar(6),
	@MaCangXHKhongCo_HT varchar(1),
	@DiaDiemXepHang nvarchar(35),
	@NgayDenDiaDiem_XH datetime,
	@MaDiaDiemDoHang varchar(7),
	@MaViTriDoHang varchar(6),
	@MaCangCuaKhauGaDoHang varchar(6),
	@MaCangDHKhongCo_HT varchar(1),
	@DiaDiemDoHang nvarchar(35),
	@NgayDenDiaDiem_DH datetime,
	@TuyenDuongVC varchar(35),
	@LoaiBaoLanh varchar(1),
	@SoTienBaoLanh numeric(11, 0),
	@SoLuongCot_TK int,
	@SoLuongContainer int,
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh int,
	@KyHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@MaVach numeric(12, 0),
	@NgayPheDuyetVC datetime,
	@NgayDuKienBatDauVC datetime,
	@GioDuKienBatDauVC varchar(2),
	@NgayDuKienKetThucVC datetime,
	@GioDuKienKetThucVC varchar(2),
	@MaBuuChinhHQ varchar(7),
	@DiaChiBuuChinhHQ nvarchar(54),
	@TenBuuChinhHQ nvarchar(34),
	@GhiChu nvarchar(255),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
SET
	[TKMD_ID] = @TKMD_ID,
	[NgayDangKy] = @NgayDangKy,
	[TenThongTinXuat] = @TenThongTinXuat,
	[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
	[MaCoBaoNiemPhong] = @MaCoBaoNiemPhong,
	[TenCoBaoNiemPhong] = @TenCoBaoNiemPhong,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[SoToKhaiVC] = @SoToKhaiVC,
	[CoBaoXuatNhapKhau] = @CoBaoXuatNhapKhau,
	[NgayLapToKhai] = @NgayLapToKhai,
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
	[MaNguoiVC] = @MaNguoiVC,
	[TenNguoiVC] = @TenNguoiVC,
	[DiaChiNguoiVC] = @DiaChiNguoiVC,
	[SoHopDongVC] = @SoHopDongVC,
	[NgayHopDongVC] = @NgayHopDongVC,
	[NgayHetHanHopDongVC] = @NgayHetHanHopDongVC,
	[MaPhuongTienVC] = @MaPhuongTienVC,
	[TenPhuongTieVC] = @TenPhuongTieVC,
	[MaMucDichVC] = @MaMucDichVC,
	[TenMucDichVC] = @TenMucDichVC,
	[LoaiHinhVanTai] = @LoaiHinhVanTai,
	[TenLoaiHinhVanTai] = @TenLoaiHinhVanTai,
	[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
	[MaViTriXepHang] = @MaViTriXepHang,
	[MaCangCuaKhauGaXepHang] = @MaCangCuaKhauGaXepHang,
	[MaCangXHKhongCo_HT] = @MaCangXHKhongCo_HT,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[NgayDenDiaDiem_XH] = @NgayDenDiaDiem_XH,
	[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
	[MaViTriDoHang] = @MaViTriDoHang,
	[MaCangCuaKhauGaDoHang] = @MaCangCuaKhauGaDoHang,
	[MaCangDHKhongCo_HT] = @MaCangDHKhongCo_HT,
	[DiaDiemDoHang] = @DiaDiemDoHang,
	[NgayDenDiaDiem_DH] = @NgayDenDiaDiem_DH,
	[TuyenDuongVC] = @TuyenDuongVC,
	[LoaiBaoLanh] = @LoaiBaoLanh,
	[SoTienBaoLanh] = @SoTienBaoLanh,
	[SoLuongCot_TK] = @SoLuongCot_TK,
	[SoLuongContainer] = @SoLuongContainer,
	[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
	[NamPhatHanhBaoLanh] = @NamPhatHanhBaoLanh,
	[KyHieuChungTuBaoLanh] = @KyHieuChungTuBaoLanh,
	[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
	[MaVach] = @MaVach,
	[NgayPheDuyetVC] = @NgayPheDuyetVC,
	[NgayDuKienBatDauVC] = @NgayDuKienBatDauVC,
	[GioDuKienBatDauVC] = @GioDuKienBatDauVC,
	[NgayDuKienKetThucVC] = @NgayDuKienKetThucVC,
	[GioDuKienKetThucVC] = @GioDuKienKetThucVC,
	[MaBuuChinhHQ] = @MaBuuChinhHQ,
	[DiaChiBuuChinhHQ] = @DiaChiBuuChinhHQ,
	[TenBuuChinhHQ] = @TenBuuChinhHQ,
	[GhiChu] = @GhiChu,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@TenThongTinXuat nvarchar(43),
	@MaPhanLoaiXuLy varchar(1),
	@MaCoBaoNiemPhong varchar(1),
	@TenCoBaoNiemPhong nvarchar(35),
	@CoQuanHaiQuan varchar(10),
	@SoToKhaiVC numeric(12, 0),
	@CoBaoXuatNhapKhau varchar(1),
	@NgayLapToKhai datetime,
	@MaNguoiKhai varchar(5),
	@TenNguoiKhai varchar(50),
	@DiaChiNguoiKhai nvarchar(100),
	@MaNguoiVC varchar(13),
	@TenNguoiVC nvarchar(100),
	@DiaChiNguoiVC nvarchar(100),
	@SoHopDongVC varchar(11),
	@NgayHopDongVC datetime,
	@NgayHetHanHopDongVC datetime,
	@MaPhuongTienVC varchar(2),
	@TenPhuongTieVC varchar(12),
	@MaMucDichVC varchar(3),
	@TenMucDichVC nvarchar(70),
	@LoaiHinhVanTai varchar(2),
	@TenLoaiHinhVanTai nvarchar(70),
	@MaDiaDiemXepHang varchar(7),
	@MaViTriXepHang varchar(6),
	@MaCangCuaKhauGaXepHang varchar(6),
	@MaCangXHKhongCo_HT varchar(1),
	@DiaDiemXepHang nvarchar(35),
	@NgayDenDiaDiem_XH datetime,
	@MaDiaDiemDoHang varchar(7),
	@MaViTriDoHang varchar(6),
	@MaCangCuaKhauGaDoHang varchar(6),
	@MaCangDHKhongCo_HT varchar(1),
	@DiaDiemDoHang nvarchar(35),
	@NgayDenDiaDiem_DH datetime,
	@TuyenDuongVC varchar(35),
	@LoaiBaoLanh varchar(1),
	@SoTienBaoLanh numeric(11, 0),
	@SoLuongCot_TK int,
	@SoLuongContainer int,
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh int,
	@KyHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@MaVach numeric(12, 0),
	@NgayPheDuyetVC datetime,
	@NgayDuKienBatDauVC datetime,
	@GioDuKienBatDauVC varchar(2),
	@NgayDuKienKetThucVC datetime,
	@GioDuKienKetThucVC varchar(2),
	@MaBuuChinhHQ varchar(7),
	@DiaChiBuuChinhHQ nvarchar(54),
	@TenBuuChinhHQ nvarchar(34),
	@GhiChu nvarchar(255),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiVanChuyen] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiVanChuyen] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[NgayDangKy] = @NgayDangKy,
			[TenThongTinXuat] = @TenThongTinXuat,
			[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
			[MaCoBaoNiemPhong] = @MaCoBaoNiemPhong,
			[TenCoBaoNiemPhong] = @TenCoBaoNiemPhong,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[SoToKhaiVC] = @SoToKhaiVC,
			[CoBaoXuatNhapKhau] = @CoBaoXuatNhapKhau,
			[NgayLapToKhai] = @NgayLapToKhai,
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
			[MaNguoiVC] = @MaNguoiVC,
			[TenNguoiVC] = @TenNguoiVC,
			[DiaChiNguoiVC] = @DiaChiNguoiVC,
			[SoHopDongVC] = @SoHopDongVC,
			[NgayHopDongVC] = @NgayHopDongVC,
			[NgayHetHanHopDongVC] = @NgayHetHanHopDongVC,
			[MaPhuongTienVC] = @MaPhuongTienVC,
			[TenPhuongTieVC] = @TenPhuongTieVC,
			[MaMucDichVC] = @MaMucDichVC,
			[TenMucDichVC] = @TenMucDichVC,
			[LoaiHinhVanTai] = @LoaiHinhVanTai,
			[TenLoaiHinhVanTai] = @TenLoaiHinhVanTai,
			[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
			[MaViTriXepHang] = @MaViTriXepHang,
			[MaCangCuaKhauGaXepHang] = @MaCangCuaKhauGaXepHang,
			[MaCangXHKhongCo_HT] = @MaCangXHKhongCo_HT,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[NgayDenDiaDiem_XH] = @NgayDenDiaDiem_XH,
			[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
			[MaViTriDoHang] = @MaViTriDoHang,
			[MaCangCuaKhauGaDoHang] = @MaCangCuaKhauGaDoHang,
			[MaCangDHKhongCo_HT] = @MaCangDHKhongCo_HT,
			[DiaDiemDoHang] = @DiaDiemDoHang,
			[NgayDenDiaDiem_DH] = @NgayDenDiaDiem_DH,
			[TuyenDuongVC] = @TuyenDuongVC,
			[LoaiBaoLanh] = @LoaiBaoLanh,
			[SoTienBaoLanh] = @SoTienBaoLanh,
			[SoLuongCot_TK] = @SoLuongCot_TK,
			[SoLuongContainer] = @SoLuongContainer,
			[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
			[NamPhatHanhBaoLanh] = @NamPhatHanhBaoLanh,
			[KyHieuChungTuBaoLanh] = @KyHieuChungTuBaoLanh,
			[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
			[MaVach] = @MaVach,
			[NgayPheDuyetVC] = @NgayPheDuyetVC,
			[NgayDuKienBatDauVC] = @NgayDuKienBatDauVC,
			[GioDuKienBatDauVC] = @GioDuKienBatDauVC,
			[NgayDuKienKetThucVC] = @NgayDuKienKetThucVC,
			[GioDuKienKetThucVC] = @GioDuKienKetThucVC,
			[MaBuuChinhHQ] = @MaBuuChinhHQ,
			[DiaChiBuuChinhHQ] = @DiaChiBuuChinhHQ,
			[TenBuuChinhHQ] = @TenBuuChinhHQ,
			[GhiChu] = @GhiChu,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
		(
			[TKMD_ID],
			[NgayDangKy],
			[TenThongTinXuat],
			[MaPhanLoaiXuLy],
			[MaCoBaoNiemPhong],
			[TenCoBaoNiemPhong],
			[CoQuanHaiQuan],
			[SoToKhaiVC],
			[CoBaoXuatNhapKhau],
			[NgayLapToKhai],
			[MaNguoiKhai],
			[TenNguoiKhai],
			[DiaChiNguoiKhai],
			[MaNguoiVC],
			[TenNguoiVC],
			[DiaChiNguoiVC],
			[SoHopDongVC],
			[NgayHopDongVC],
			[NgayHetHanHopDongVC],
			[MaPhuongTienVC],
			[TenPhuongTieVC],
			[MaMucDichVC],
			[TenMucDichVC],
			[LoaiHinhVanTai],
			[TenLoaiHinhVanTai],
			[MaDiaDiemXepHang],
			[MaViTriXepHang],
			[MaCangCuaKhauGaXepHang],
			[MaCangXHKhongCo_HT],
			[DiaDiemXepHang],
			[NgayDenDiaDiem_XH],
			[MaDiaDiemDoHang],
			[MaViTriDoHang],
			[MaCangCuaKhauGaDoHang],
			[MaCangDHKhongCo_HT],
			[DiaDiemDoHang],
			[NgayDenDiaDiem_DH],
			[TuyenDuongVC],
			[LoaiBaoLanh],
			[SoTienBaoLanh],
			[SoLuongCot_TK],
			[SoLuongContainer],
			[MaNganHangBaoLanh],
			[NamPhatHanhBaoLanh],
			[KyHieuChungTuBaoLanh],
			[SoChungTuBaoLanh],
			[MaVach],
			[NgayPheDuyetVC],
			[NgayDuKienBatDauVC],
			[GioDuKienBatDauVC],
			[NgayDuKienKetThucVC],
			[GioDuKienKetThucVC],
			[MaBuuChinhHQ],
			[DiaChiBuuChinhHQ],
			[TenBuuChinhHQ],
			[GhiChu],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@NgayDangKy,
			@TenThongTinXuat,
			@MaPhanLoaiXuLy,
			@MaCoBaoNiemPhong,
			@TenCoBaoNiemPhong,
			@CoQuanHaiQuan,
			@SoToKhaiVC,
			@CoBaoXuatNhapKhau,
			@NgayLapToKhai,
			@MaNguoiKhai,
			@TenNguoiKhai,
			@DiaChiNguoiKhai,
			@MaNguoiVC,
			@TenNguoiVC,
			@DiaChiNguoiVC,
			@SoHopDongVC,
			@NgayHopDongVC,
			@NgayHetHanHopDongVC,
			@MaPhuongTienVC,
			@TenPhuongTieVC,
			@MaMucDichVC,
			@TenMucDichVC,
			@LoaiHinhVanTai,
			@TenLoaiHinhVanTai,
			@MaDiaDiemXepHang,
			@MaViTriXepHang,
			@MaCangCuaKhauGaXepHang,
			@MaCangXHKhongCo_HT,
			@DiaDiemXepHang,
			@NgayDenDiaDiem_XH,
			@MaDiaDiemDoHang,
			@MaViTriDoHang,
			@MaCangCuaKhauGaDoHang,
			@MaCangDHKhongCo_HT,
			@DiaDiemDoHang,
			@NgayDenDiaDiem_DH,
			@TuyenDuongVC,
			@LoaiBaoLanh,
			@SoTienBaoLanh,
			@SoLuongCot_TK,
			@SoLuongContainer,
			@MaNganHangBaoLanh,
			@NamPhatHanhBaoLanh,
			@KyHieuChungTuBaoLanh,
			@SoChungTuBaoLanh,
			@MaVach,
			@NgayPheDuyetVC,
			@NgayDuKienBatDauVC,
			@GioDuKienBatDauVC,
			@NgayDuKienKetThucVC,
			@GioDuKienKetThucVC,
			@MaBuuChinhHQ,
			@DiaChiBuuChinhHQ,
			@TenBuuChinhHQ,
			@GhiChu,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiVanChuyen] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_ToKhaiVanChuyen] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]	

GO

---------------------------------------------------cap nhat funtion TCVN2Unicode----------------------------------------------------------
IF OBJECT_ID(N'[dbo].[TCVN2Unicode]') IS NOT NULL
	drop  FUNCTION [dbo].[TCVN2Unicode]
/****** Object:  UserDefinedFunction [dbo].[TCVN2Unicode]    Script Date: 03/13/2014 14:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[TCVN2Unicode] (@strInput VARCHAR(4000))
RETURNS NVARCHAR(4000)
AS
BEGIN
    DECLARE @TCVN CHAR(671)
    DECLARE @UNICODE CHAR(671)
    SET @TCVN = ',184 ,181 ,182 ,183 ,185 ,168 ,190 ,187 ,188 ,189 ,198 ,169 ,202 ,199 ,200 ,201 ,203 ,208 ,204 ,206 ,207 ,209 ,170 ,213 ,210 ,211 ,212 ,214 ,221 ,215 ,216 ,220 ,222 ,227 ,223 ,225 ,226 ,228 ,171 ,232 ,229 ,230 ,231 ,233 ,172 ,237 ,234 ,235 ,236 ,238 ,243 ,239 ,241 ,242 ,244 ,173 ,248 ,245 ,246 ,247 ,249 ,253 ,250 ,251 ,252 ,254 ,174 ,184 ,181 ,182 ,183 ,185 ,161 ,190 ,187 ,188 ,189 ,198 ,162 ,202 ,199 ,200 ,201 ,203 ,208 ,204 ,206 ,207 ,209 ,163 ,213 ,210 ,211 ,212 ,214 ,221 ,215 ,216 ,220 ,222 ,227 ,223 ,225 ,226 ,228 ,164 ,232 ,229 ,230 ,231 ,233 ,165 ,237 ,234 ,235 ,236 ,238 ,243 ,239 ,241 ,242 ,244 ,166 ,248 ,245 ,246 ,247 ,249 ,253 ,250 ,251 ,252 ,254 ,167 ,'
    SET @UNICODE = ',225 ,224 ,7843,227 ,7841,259 ,7855,7857,7859,7861,7863,226 ,7845,7847,7849,7851,7853,233 ,232 ,7867,7869,7865,234 ,7871,7873,7875,7877,7879,237 ,236 ,7881,297 ,7883,243 ,242 ,7887,245 ,7885,244 ,7889,7891,7893,7895,7897,417 ,7899,7901,7903,7905,7907,250 ,249 ,7911,361 ,7909,432 ,7913,7915,7917,7919,7921,253 ,7923,7927,7929,7925,273 ,193 ,192 ,7842,195 ,7840,258 ,7854,7856,7858,7860,7862,194 ,7844,7846,7848,7850,7852,201 ,200 ,7866,7868,7864,202 ,7870,7872,7874,7876,7878,205 ,204 ,7880,296 ,7882,211 ,210 ,7886,213 ,7884,212 ,7888,7890,7892,7894,7896,416 ,7898,7900,7902,7904,7906,218 ,217 ,7910,360 ,7908,431 ,7912,7914,7916,7918,7920,221 ,7922,7926,7928,7924,272 ,'
    IF @strInput IS NULL RETURN NULL
    IF @strInput = '' RETURN NULL
    DECLARE @strOutput NVARCHAR(4000)
    DECLARE @COUNTER INT
    DECLARE @POSITION INT
    SET @COUNTER = 1
    SET @strOutput = ''
    WHILE (@COUNTER <= LEN(@strInput))
    BEGIN
        SET @POSITION = CHARINDEX(','+CONVERT(CHAR(4),ASCII(SUBSTRING(@strInput, @COUNTER, 1)))+',', @TCVN, 1)
        IF @POSITION > 0
            SET @strOutput = @strOutput + NCHAR(CONVERT(INT,SUBSTRING(@UNICODE, @POSITION+1, 4)))
        ELSE
            SET @strOutput = @strOutput + SUBSTRING(@strInput, @COUNTER, 1)
        SET @COUNTER = @COUNTER + 1
    END
    RETURN @strOutput
END

alter Table t_KDT_VNACC_ChungTuDinhKem
alter column sotiepnhan numeric(12) null

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(11, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 0),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(11, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem]
(
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
)
VALUES 
(
	@LoaiChungTu,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@TrangThaiXuLy,
	@CoQuanHaiQuan,
	@NhomXuLyHoSo,
	@TieuDe,
	@SoToKhai,
	@GhiChu,
	@PhanLoaiThuTucKhaiBao,
	@SoDienThoaiNguoiKhaiBao,
	@SoQuanLyTrongNoiBoDoanhNghiep,
	@MaKetQuaXuLy,
	@TenThuTucKhaiBao,
	@NgayKhaiBao,
	@TrangThaiKhaiBao,
	@NgaySuaCuoiCung,
	@TenNguoiKhaiBao,
	@DiaChiNguoiKhaiBao,
	@SoDeLayTepDinhKem,
	@NgayHoanThanhKiemTraHoSo,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TongDungLuong,
	@PhanLuong,
	@HuongDan,
	@TKMD_ID,
	@NgayBatDau,
	@NgayCapNhatCuoiCung,
	@CanCuPhapLenh,
	@CoBaoXoa,
	@GhiChuHaiQuan,
	@HinhThucTimKiem,
	@LyDoChinhSua,
	@MaDiaDiemDen,
	@MaNhaVanChuyen,
	@MaPhanLoaiDangKy,
	@MaPhanLoaiXuLy,
	@MaThongTinXuat,
	@NgayCapPhep,
	@NgayThongBao,
	@NguoiGui,
	@NoiDungChinhSua,
	@PhuongTienVanChuyen,
	@SoChuyenDiBien,
	@SoSeri,
	@SoTiepNhanToKhaiPhoThong,
	@TrangThaiXuLyHaiQuan,
	@NguoiCapNhatCuoiCung,
	@NhomXuLyHoSoID
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(11, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 0),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(11, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
SET
	[LoaiChungTu] = @LoaiChungTu,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[NhomXuLyHoSo] = @NhomXuLyHoSo,
	[TieuDe] = @TieuDe,
	[SoToKhai] = @SoToKhai,
	[GhiChu] = @GhiChu,
	[PhanLoaiThuTucKhaiBao] = @PhanLoaiThuTucKhaiBao,
	[SoDienThoaiNguoiKhaiBao] = @SoDienThoaiNguoiKhaiBao,
	[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[TenThuTucKhaiBao] = @TenThuTucKhaiBao,
	[NgayKhaiBao] = @NgayKhaiBao,
	[TrangThaiKhaiBao] = @TrangThaiKhaiBao,
	[NgaySuaCuoiCung] = @NgaySuaCuoiCung,
	[TenNguoiKhaiBao] = @TenNguoiKhaiBao,
	[DiaChiNguoiKhaiBao] = @DiaChiNguoiKhaiBao,
	[SoDeLayTepDinhKem] = @SoDeLayTepDinhKem,
	[NgayHoanThanhKiemTraHoSo] = @NgayHoanThanhKiemTraHoSo,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TongDungLuong] = @TongDungLuong,
	[PhanLuong] = @PhanLuong,
	[HuongDan] = @HuongDan,
	[TKMD_ID] = @TKMD_ID,
	[NgayBatDau] = @NgayBatDau,
	[NgayCapNhatCuoiCung] = @NgayCapNhatCuoiCung,
	[CanCuPhapLenh] = @CanCuPhapLenh,
	[CoBaoXoa] = @CoBaoXoa,
	[GhiChuHaiQuan] = @GhiChuHaiQuan,
	[HinhThucTimKiem] = @HinhThucTimKiem,
	[LyDoChinhSua] = @LyDoChinhSua,
	[MaDiaDiemDen] = @MaDiaDiemDen,
	[MaNhaVanChuyen] = @MaNhaVanChuyen,
	[MaPhanLoaiDangKy] = @MaPhanLoaiDangKy,
	[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
	[MaThongTinXuat] = @MaThongTinXuat,
	[NgayCapPhep] = @NgayCapPhep,
	[NgayThongBao] = @NgayThongBao,
	[NguoiGui] = @NguoiGui,
	[NoiDungChinhSua] = @NoiDungChinhSua,
	[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
	[SoChuyenDiBien] = @SoChuyenDiBien,
	[SoSeri] = @SoSeri,
	[SoTiepNhanToKhaiPhoThong] = @SoTiepNhanToKhaiPhoThong,
	[TrangThaiXuLyHaiQuan] = @TrangThaiXuLyHaiQuan,
	[NguoiCapNhatCuoiCung] = @NguoiCapNhatCuoiCung,
	[NhomXuLyHoSoID] = @NhomXuLyHoSoID
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(11, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 0),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(11, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChungTuDinhKem] 
		SET
			[LoaiChungTu] = @LoaiChungTu,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[NhomXuLyHoSo] = @NhomXuLyHoSo,
			[TieuDe] = @TieuDe,
			[SoToKhai] = @SoToKhai,
			[GhiChu] = @GhiChu,
			[PhanLoaiThuTucKhaiBao] = @PhanLoaiThuTucKhaiBao,
			[SoDienThoaiNguoiKhaiBao] = @SoDienThoaiNguoiKhaiBao,
			[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[TenThuTucKhaiBao] = @TenThuTucKhaiBao,
			[NgayKhaiBao] = @NgayKhaiBao,
			[TrangThaiKhaiBao] = @TrangThaiKhaiBao,
			[NgaySuaCuoiCung] = @NgaySuaCuoiCung,
			[TenNguoiKhaiBao] = @TenNguoiKhaiBao,
			[DiaChiNguoiKhaiBao] = @DiaChiNguoiKhaiBao,
			[SoDeLayTepDinhKem] = @SoDeLayTepDinhKem,
			[NgayHoanThanhKiemTraHoSo] = @NgayHoanThanhKiemTraHoSo,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TongDungLuong] = @TongDungLuong,
			[PhanLuong] = @PhanLuong,
			[HuongDan] = @HuongDan,
			[TKMD_ID] = @TKMD_ID,
			[NgayBatDau] = @NgayBatDau,
			[NgayCapNhatCuoiCung] = @NgayCapNhatCuoiCung,
			[CanCuPhapLenh] = @CanCuPhapLenh,
			[CoBaoXoa] = @CoBaoXoa,
			[GhiChuHaiQuan] = @GhiChuHaiQuan,
			[HinhThucTimKiem] = @HinhThucTimKiem,
			[LyDoChinhSua] = @LyDoChinhSua,
			[MaDiaDiemDen] = @MaDiaDiemDen,
			[MaNhaVanChuyen] = @MaNhaVanChuyen,
			[MaPhanLoaiDangKy] = @MaPhanLoaiDangKy,
			[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
			[MaThongTinXuat] = @MaThongTinXuat,
			[NgayCapPhep] = @NgayCapPhep,
			[NgayThongBao] = @NgayThongBao,
			[NguoiGui] = @NguoiGui,
			[NoiDungChinhSua] = @NoiDungChinhSua,
			[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
			[SoChuyenDiBien] = @SoChuyenDiBien,
			[SoSeri] = @SoSeri,
			[SoTiepNhanToKhaiPhoThong] = @SoTiepNhanToKhaiPhoThong,
			[TrangThaiXuLyHaiQuan] = @TrangThaiXuLyHaiQuan,
			[NguoiCapNhatCuoiCung] = @NguoiCapNhatCuoiCung,
			[NhomXuLyHoSoID] = @NhomXuLyHoSoID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem]
		(
			[LoaiChungTu],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[TrangThaiXuLy],
			[CoQuanHaiQuan],
			[NhomXuLyHoSo],
			[TieuDe],
			[SoToKhai],
			[GhiChu],
			[PhanLoaiThuTucKhaiBao],
			[SoDienThoaiNguoiKhaiBao],
			[SoQuanLyTrongNoiBoDoanhNghiep],
			[MaKetQuaXuLy],
			[TenThuTucKhaiBao],
			[NgayKhaiBao],
			[TrangThaiKhaiBao],
			[NgaySuaCuoiCung],
			[TenNguoiKhaiBao],
			[DiaChiNguoiKhaiBao],
			[SoDeLayTepDinhKem],
			[NgayHoanThanhKiemTraHoSo],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TongDungLuong],
			[PhanLuong],
			[HuongDan],
			[TKMD_ID],
			[NgayBatDau],
			[NgayCapNhatCuoiCung],
			[CanCuPhapLenh],
			[CoBaoXoa],
			[GhiChuHaiQuan],
			[HinhThucTimKiem],
			[LyDoChinhSua],
			[MaDiaDiemDen],
			[MaNhaVanChuyen],
			[MaPhanLoaiDangKy],
			[MaPhanLoaiXuLy],
			[MaThongTinXuat],
			[NgayCapPhep],
			[NgayThongBao],
			[NguoiGui],
			[NoiDungChinhSua],
			[PhuongTienVanChuyen],
			[SoChuyenDiBien],
			[SoSeri],
			[SoTiepNhanToKhaiPhoThong],
			[TrangThaiXuLyHaiQuan],
			[NguoiCapNhatCuoiCung],
			[NhomXuLyHoSoID]
		)
		VALUES 
		(
			@LoaiChungTu,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@TrangThaiXuLy,
			@CoQuanHaiQuan,
			@NhomXuLyHoSo,
			@TieuDe,
			@SoToKhai,
			@GhiChu,
			@PhanLoaiThuTucKhaiBao,
			@SoDienThoaiNguoiKhaiBao,
			@SoQuanLyTrongNoiBoDoanhNghiep,
			@MaKetQuaXuLy,
			@TenThuTucKhaiBao,
			@NgayKhaiBao,
			@TrangThaiKhaiBao,
			@NgaySuaCuoiCung,
			@TenNguoiKhaiBao,
			@DiaChiNguoiKhaiBao,
			@SoDeLayTepDinhKem,
			@NgayHoanThanhKiemTraHoSo,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TongDungLuong,
			@PhanLuong,
			@HuongDan,
			@TKMD_ID,
			@NgayBatDau,
			@NgayCapNhatCuoiCung,
			@CanCuPhapLenh,
			@CoBaoXoa,
			@GhiChuHaiQuan,
			@HinhThucTimKiem,
			@LyDoChinhSua,
			@MaDiaDiemDen,
			@MaNhaVanChuyen,
			@MaPhanLoaiDangKy,
			@MaPhanLoaiXuLy,
			@MaThongTinXuat,
			@NgayCapPhep,
			@NgayThongBao,
			@NguoiGui,
			@NoiDungChinhSua,
			@PhuongTienVanChuyen,
			@SoChuyenDiBien,
			@SoSeri,
			@SoTiepNhanToKhaiPhoThong,
			@TrangThaiXuLyHaiQuan,
			@NguoiCapNhatCuoiCung,
			@NhomXuLyHoSoID
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]	

GO





           --Cập nhật version
     IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '14.7') = 0
BEGIN	INSERT INTO dbo.t_HaiQuan_Version VALUES('14.7', GETDATE(), N'Cap nhat soluong Table T_KDT_VANCC_HangMauDich, alter t_kdt_VNACC_toKhaiVanChuyen,funtion TCVN2Unicode, t_kdt_VNACC_ChungTuDinhKem ')
END	