﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using System.Linq;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;


namespace SOFTECH.ECS.V3.Components.KD
{
    // NAMNTH
    public partial class CO
    {

        /// <summary>
        /// ConfigGoodsItem
        /// </summary>
        /// <param name="CertificateOfOrigin">XElement</param>
        /// <param name="co">CO</param>
        /// <returns>XElement</returns>
        public static XElement ConfigGoodsItem(XElement GoodsItem, CO co)
        {
            
            IList<HangCODetail> LCO_detail = HangCODetail.SelectCollectionBy_CO_ID(co.ID);
            HangCODetail HangCODetail1;
            if (LCO_detail != null && LCO_detail.Count > 0)
            {
                HangCODetail1 = LCO_detail[0];
            }
            else
            {
                HangCODetail1 = new HangCODetail();
            }
            //GoodsItem            
            if (GoodsItem != null&& GoodsItem.Elements().Count()>0)
            {                    
                foreach (XElement element in GoodsItem.Elements())
                {

                    if (HangCODetail1 != null)        
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeCO.sequence.ToUpper()))
                            element.SetValue(HangCODetail1.SoThuTuHang);//Số thứ tự hàng sequence
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.statisticalValue.ToUpper()))
                            element.SetValue(HangCODetail1.TriGiaTinhThue);//Trị giá tính thuế statisticalValue    

                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.Commodity.ToUpper()))//Commodity
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.description.ToUpper()))//Mô tả hàng hóa
                                    elementChild.SetValue(HangCODetail1.MoTaHangHoa);
                                else if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.identification.ToUpper()))//Mã hàng do doanh nghiệp khai
                                    elementChild.SetValue(HangCODetail1.MaHangDoanhNghiepKhai);
                                else if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.tariffClassification.ToUpper()))//Mã hs
                                    elementChild.SetValue(HangCODetail1.MaHS);
                            }

                        }

                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.Origin.ToUpper()))//Orgin
                        {
                            foreach(XElement elementChild in element.Elements())
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.originCountry.ToUpper()))
                                    elementChild.SetValue(HangCODetail1.NuocXX_ID);//Mã nước xuất xứ hàng
                        }
                            
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.Invoice.ToUpper()))//Invoice Hóa đơn thương mại
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.reference.ToUpper()))  //Số hóa đơn	1	an..50	
                                {
                                    elementChild.SetValue(HangCODetail1.SoHoaDon);
                                }
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.issue.ToUpper()))      //Ngày hóa đơn	1	an10	YYYY-MM-DD
                                {
                                    elementChild.SetValue(HangCODetail1.NgayHoaDon);
                                }
                                
                            }
                        }

                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.CurrencyExchange.ToUpper()))//CurrencyExchange
                        {
                            foreach (XElement elementChild in element.Elements())
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.currencyType.ToUpper()))
                                    elementChild.SetValue(HangCODetail1.MaNguyenTe);//Mã nguyên tệ
                        }


                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.ConsignmentItemPackaging.ToUpper()))//ConsignmentItemPackaging Thông tin kiện
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.quantity.ToUpper()))
                                    elementChild.SetValue(HangCODetail1.SoKien.ToString());//số kiện
                                else if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.markNumber.ToUpper()))
                                    elementChild.SetValue(HangCODetail1.NhanMac);//Nhãn mác
                            }

                        }

                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.GoodsMeasure.ToUpper()))//GoodsMeasure			
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.grossMass.ToUpper()))
                                    elementChild.SetValue(HangCODetail1.TrongLuong);//Trong luong
                                else if (elementChild.Name.ToString().ToUpper().Equals(NodeCO.measureUnit.ToUpper()))
                                    elementChild.SetValue(HangCODetail1.MaDonViTinh);//Mã đơn vị tính
                            }

                        }

                    }

                    
                }
            }
                
            return GoodsItem;
        }

        /// <summary>
        /// ConfigCertificateOfOrigin
        /// </summary>
        /// <param name="CertificateOfOrigin">XElement</param>
        /// <param name="co">CO</param>
        /// <returns>XElement</returns>
        public static XElement ConfigCertificateOfOrigin(XElement CertificateOfOrigin, CO co)
        {
            try
            {
                ToKhaiMauDich tkmd = ToKhaiMauDich.Load(co.TKMD_ID);
                Nuoc nuoc;


                if (CertificateOfOrigin != null && CertificateOfOrigin.Elements().Count() > 0 && co != null)
                {
                    XElement GoodsItem = new XElement(NodeCO.GoodsItem);
                    foreach (XElement element in CertificateOfOrigin.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeCO.reference.ToUpper()))
                            element.SetValue(co.SoCO);  //Số C/O
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.type.ToUpper()))
                            element.SetValue(co.LoaiCO);    //Loại C/O (Form A, D, E, AK)
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.issuer.ToUpper()))
                            element.SetValue(co.ToChucCap);        //Tổ chức cấp C/O
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.issue.ToUpper()))
                            element.SetValue(co.NgayCO.ToString("yyyy/MM/dd"));//Ngày cấp C/O
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.issueLocation.ToUpper()))
                            element.SetValue(co.NuocCapCO); //Nước cấp C/O
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.representative.ToUpper()))
                            element.SetValue(co.NguoiKy);  //Người cấp C/O  
                        //exporter
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.exporter.ToUpper()))
                        {
                            foreach (XElement elementChil in element.Elements())
                            {
                                if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.name.ToUpper()))
                                    elementChil.SetValue(co.TenDiaChiNguoiXK);//Tên, địa chỉ người xuất khẩu trên                
                                if (tkmd != null)
                                    if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.Identity.ToUpper()))
                                        elementChil.SetValue(tkmd.TenDonViDoiTac);//Mã người xuất khẩu
                            }

                        }


                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.exportationCountry.ToUpper()))
                        {
                            //exportationCountry
                            nuoc = Nuoc.Load(co.MaNuocXKTrenCO);
                            foreach (XElement elementChil in element.Elements())
                            {
                                if (nuoc != null)
                                    if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.name.ToUpper()))
                                        elementChil.SetValue(nuoc.Ten);//Tên Nước xuất khẩu trên C/O
                                if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.code.ToUpper()))
                                    elementChil.SetValue(co.MaNuocXKTrenCO);//Nước xuất khẩu trên C/O
                            }
                        }

                        //importer
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.importer.ToUpper()))
                        {
                            foreach (XElement elementChil in element.Elements())
                            {
                                if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.name.ToUpper()))
                                    elementChil.SetValue(co.TenDiaChiNguoiNK);//Tên, địa chỉ người nhập khẩu  
                                if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.Identity.ToUpper()))
                                    elementChil.SetValue(co.MaDoanhNghiep);//Tên, địa chỉ người nhập khẩu  
                            }
                        }

                        //importationCountry

                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.importationCountry.ToUpper()))
                        {
                            nuoc = Nuoc.Load(co.MaNuocNKTrenCO);
                            foreach (XElement elementChil in element.Elements())
                            {
                                if (nuoc != null)
                                    if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.name.ToUpper()))
                                        elementChil.SetValue(nuoc.Ten);//Tên Nước nhập khẩu trên C/O
                                if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.code.ToUpper()))
                                    elementChil.SetValue(co.MaNuocNKTrenCO);//Nước nhập khẩu trên C/O
                            }
                        }

                        //LoadingLocation
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.LoadingLocation.ToUpper()))
                        {
                            foreach (XElement elementChil in element.Elements())
                            {
                                if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.name.ToUpper()))//Tên cảng xếp hàng
                                    elementChil.SetValue("");
                                if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.code.ToUpper()))//Mã cảng xếp hàng
                                    elementChil.SetValue("");
                                if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.loading.ToUpper()))//Mã cảng xếp hàng
                                    elementChil.SetValue("");//Ngày khởi hành	1	an10	YYYY-MM-DD	
                            }
                        }
                        //UnLoadingLocation	
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.UnLoadingLocation.ToUpper()))
                        {
                            foreach (XElement elementChil in element.Elements())
                            {
                                if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.name.ToUpper()))//Tên cảng dỡ hàng
                                    elementChil.SetValue("");
                                if (elementChil.Name.ToString().ToUpper().Equals(NodeCO.code.ToUpper()))//Mã cảng dỡ hàng
                                    elementChil.SetValue("");
                            }
                        }

                        //AdditionalInformation						
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.AdditionalInformation.ToUpper()))
                        {
                            if (element.Element(NodeCO.content).Name.ToString().ToUpper().Equals(NodeCO.content.ToUpper()))
                                element.SetValue(co.ThongTinMoTaChiTiet);//Các thông tin khác                            
                        }
                        //GoodsItem
                        else if (element.Name.ToString().ToUpper().Equals(NodeCO.GoodsItem.ToUpper()))
                            ConfigGoodsItem(element, co);

                    }
                }
                return CertificateOfOrigin;
            }
            catch(Exception ex) {
                string tt = ex.Message;
                return CertificateOfOrigin;
            }
        }


        /// <summary>
        /// ConfigCO
        /// </summary>        
        /// <returns>string</returns>
        public static string ConfigCO(ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)
        {
            try
            {
                IList<CO> lCO = CO.SelectCollectionBy_TKMD_ID(tkmd.ID);
                if (lCO != null && lCO.Count > 0)
                {
                    XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/KD/CO.xml");
                    if (tkmd != null && docXML != null)
                    {
                        XElement Root = docXML.Root;

                        if (Root != null && Root.Elements().Count() > 0)
                        {
                            XElement CertificateOfOrigins = Root.Element(NodeCO.CertificateOfOrigins);
                            XElement CertificateOfOrigin = Root.Element(NodeCO.CertificateOfOrigin);

                            // Check Upper
                            foreach (XElement element in Root.Elements())
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodeCO.CertificateOfOrigins.ToUpper()))
                                    CertificateOfOrigins = element;
                            }
                            foreach (XElement element in CertificateOfOrigins.Elements())
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodeCO.CertificateOfOrigin.ToUpper()))
                                    CertificateOfOrigin = element;
                            }
                            Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, status, declarationOffice);
                            // Add CertificateOfOrigins

                            // CertificateOfOrigin > 1
                            
                            if (lCO.Count == 1)
                            {
                                ConfigCertificateOfOrigin(CertificateOfOrigin, lCO[0]);
                            }
                            else if (lCO.Count > 1)
                            {
                                XElement TempCertificateOfOrigin = new XElement(CertificateOfOrigin);
                                ConfigCertificateOfOrigin(CertificateOfOrigin, lCO[0]);
                                // Add new license
                                for (int i = 1; i < lCO.Count; i++)
                                {
                                    XElement NewCertificateOfOrigin = new XElement(TempCertificateOfOrigin);
                                    CertificateOfOrigins.Add(ConfigCertificateOfOrigin(NewCertificateOfOrigin, lCO[i]));
                                }
                            }                            

                            //docXML.Save(@"D:\NewCO.xml");
                            //docXML.Save(@"../../../SOFTECH.ECS.V3.Components/XML/LoadKD/NewCO.xml");
                            return docXML.ToString();
                        }

                    }
                    return "";
                }                
                return "";
            }
            catch(Exception ex)
            {
                string tt = ex.Message;
                return "";
            }
            
        }
        
    }
}
