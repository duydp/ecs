﻿namespace Company.Interface
{
    partial class VNACC_HoaDon_PhanLoaiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grdChungTu_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_HoaDon_PhanLoaiForm));
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.lable2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMaPhanLoai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.grdChungTu = new Janus.Windows.GridEX.GridEX();
            this.clcNgayChungTu = new Janus.Windows.CalendarCombo.CalendarCombo();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdChungTu)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(574, 322);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(487, 60);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(487, 31);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 3;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.clcNgayChungTu);
            this.uiGroupBox1.Controls.Add(this.lable2);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtMaPhanLoai);
            this.uiGroupBox1.Controls.Add(this.txtSoChungTu);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(574, 86);
            this.uiGroupBox1.TabIndex = 4;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // lable2
            // 
            this.lable2.AutoSize = true;
            this.lable2.Location = new System.Drawing.Point(34, 11);
            this.lable2.Name = "lable2";
            this.lable2.Size = new System.Drawing.Size(67, 13);
            this.lable2.TabIndex = 6;
            this.lable2.Text = "Mã phân loại";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(365, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ngày chứng từ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(204, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Số chứng từ";
            // 
            // txtMaPhanLoai
            // 
            this.txtMaPhanLoai.Location = new System.Drawing.Point(34, 31);
            this.txtMaPhanLoai.Name = "txtMaPhanLoai";
            this.txtMaPhanLoai.Size = new System.Drawing.Size(151, 21);
            this.txtMaPhanLoai.TabIndex = 0;
            this.txtMaPhanLoai.VisualStyleManager = this.vsmMain;
            // 
            // txtSoChungTu
            // 
            this.txtSoChungTu.Location = new System.Drawing.Point(204, 31);
            this.txtSoChungTu.Name = "txtSoChungTu";
            this.txtSoChungTu.Size = new System.Drawing.Size(135, 21);
            this.txtSoChungTu.TabIndex = 1;
            this.txtSoChungTu.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.grdChungTu);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 86);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(574, 236);
            this.uiGroupBox2.TabIndex = 5;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // grdChungTu
            // 
            this.grdChungTu.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdChungTu.ColumnAutoResize = true;
            grdChungTu_DesignTimeLayout.LayoutString = resources.GetString("grdChungTu_DesignTimeLayout.LayoutString");
            this.grdChungTu.DesignTimeLayout = grdChungTu_DesignTimeLayout;
            this.grdChungTu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdChungTu.GroupByBoxVisible = false;
            this.grdChungTu.Location = new System.Drawing.Point(0, 0);
            this.grdChungTu.Name = "grdChungTu";
            this.grdChungTu.RecordNavigator = true;
            this.grdChungTu.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdChungTu.Size = new System.Drawing.Size(574, 236);
            this.grdChungTu.TabIndex = 0;
            this.grdChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdChungTu.VisualStyleManager = this.vsmMain;
            this.grdChungTu.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grdChungTu_DeletingRecord);
            this.grdChungTu.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdChungTu_RowDoubleClick);
            // 
            // clcNgayChungTu
            // 
            // 
            // 
            // 
            this.clcNgayChungTu.DropDownCalendar.Name = "";
            this.clcNgayChungTu.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayChungTu.IsNullDate = true;
            this.clcNgayChungTu.Location = new System.Drawing.Point(368, 31);
            this.clcNgayChungTu.Name = "clcNgayChungTu";
            this.clcNgayChungTu.Nullable = true;
            this.clcNgayChungTu.Size = new System.Drawing.Size(113, 21);
            this.clcNgayChungTu.TabIndex = 11;
            this.clcNgayChungTu.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // VNACC_HoaDon_PhanLoaiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 322);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_HoaDon_PhanLoaiForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "VNACC_HoaDon_PhanLoaiForm";
            this.Load += new System.EventHandler(this.VNACC_HoaDon_PhanLoaiForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdChungTu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX grdChungTu;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label lable2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaPhanLoai;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTu;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayChungTu;
    }
}