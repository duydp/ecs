using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;

namespace Company.Interface.Report
{
    public partial class CauHinhNhomHangForm : BaseForm
    {
        public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        public CauHinhNhomHangForm()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            GridEXRow [] rows = dgList1.GetCheckedRows();
            foreach (GridEXRow row in rows)
            {
                HangMauDich hmd = (HangMauDich)row.DataRow;
                hmd.NhomHang = txtNhomHang.Text;
            }
            dgList1.Refetch();
        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }

        private void CauHinhNhomHangForm_Load(object sender, EventArgs e)
        {
            dgList1.DataSource = this.HMDCollection;
        }
    }
}