using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.GC.BLL.DuLieuChuan;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.ToKhai
{
    public partial class SanPhamChuyenTiepRegistedForm : BaseForm
    {
        public SanPham SanPhamSelected = new SanPham();
        //public Company.GC.BLL.KDT.ToKhaiMauDich TKMD;
       // public Company.GC.BLL.KDT.HangMauDichCollection HMDCollection;

        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep  TKCT;
        public Company.GC.BLL.KDT.GC.HangChuyenTiepCollection HCTCollection;

        public SanPhamChuyenTiepRegistedForm()
        {
            InitializeComponent();
            SanPhamSelected = new SanPham();
        }

        public void BindData()
        {
            SanPhamSelected.HopDong_ID = TKCT.IDHopDong;
            dgList.DataSource = SanPhamSelected.SelectCollectionBy_HopDong_ID();
        }

        //-----------------------------------------------------------------------------------------
        private bool KiemTraMaHang(string MaHang)
        {
            foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep  HCT in HCTCollection)
            {
                if (HCT.MaHang.Trim().ToUpper() == MaHang.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll();
            lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------

        private void SanPhamRegistedForm_Load(object sender, EventArgs e)
        {
            // Sản phẩm đã đăng ký.            
            BindData();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                e.Row.Cells["NhomSanPham_ID"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSanPham_ID"].Value.ToString());
            }

        }

        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                Company.GC.BLL.GC.SanPham SP = (Company.GC.BLL.GC.SanPham)row.DataRow;
                if (KiemTraMaHang(SP.Ma))
                {
                    string st = showMsg("MSG_WRN31", SP.Ma, true);
                    //string st = MLMessages("Mặt hàng " + SP.Ma + " đã có nên sẽ được bỏ qua.Bạn có muốn tiếp tục không ?","MSG_WRN31",SP.Ma, true);
                    if (st == "Yes")
                    {
                        return;
                    }
                    else
                        continue;
                }
                else
                {
                    Company.GC.BLL.KDT.GC.HangChuyenTiep HCT = new Company.GC.BLL.KDT.GC.HangChuyenTiep();
                    HCT.MaHang  = SP.Ma;
                    HCT.TenHang = SP.Ten;
                    HCT.MaHS = SP.MaHS;
                    HCT.ID_DVT  = SP.DVT_ID;
                    HCTCollection.Add(HCT);
                }
            }
            this.Close();
        }

        //-----------------------------------------------------------------------------------------
    }
}