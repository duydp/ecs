using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ChungTuThanhToan_ChiTiet : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long ChungTuThanhToan_ID { set; get; }
		public decimal SoThuTuGiaoDich { set; get; }
		public DateTime NgayTaoDuLieu { set; get; }
		public DateTime ThoiGianTaoDuLieu { set; get; }
		public decimal SoTienTruLui { set; get; }
		public decimal SoTienTangLen { set; get; }
		public decimal SoToKhai { set; get; }
		public DateTime NgayDangKyToKhai { set; get; }
		public string MaLoaiHinh { set; get; }
		public string CoQuanHaiQuan { set; get; }
		public string TenCucHaiQuan { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ChungTuThanhToan_ChiTiet> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ChungTuThanhToan_ChiTiet> collection = new List<KDT_VNACC_ChungTuThanhToan_ChiTiet>();
			while (reader.Read())
			{
				KDT_VNACC_ChungTuThanhToan_ChiTiet entity = new KDT_VNACC_ChungTuThanhToan_ChiTiet();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungTuThanhToan_ID"))) entity.ChungTuThanhToan_ID = reader.GetInt64(reader.GetOrdinal("ChungTuThanhToan_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuGiaoDich"))) entity.SoThuTuGiaoDich = reader.GetDecimal(reader.GetOrdinal("SoThuTuGiaoDich"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTaoDuLieu"))) entity.NgayTaoDuLieu = reader.GetDateTime(reader.GetOrdinal("NgayTaoDuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianTaoDuLieu"))) entity.ThoiGianTaoDuLieu = reader.GetDateTime(reader.GetOrdinal("ThoiGianTaoDuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienTruLui"))) entity.SoTienTruLui = reader.GetDecimal(reader.GetOrdinal("SoTienTruLui"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienTangLen"))) entity.SoTienTangLen = reader.GetDecimal(reader.GetOrdinal("SoTienTangLen"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyToKhai"))) entity.NgayDangKyToKhai = reader.GetDateTime(reader.GetOrdinal("NgayDangKyToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHaiQuan"))) entity.CoQuanHaiQuan = reader.GetString(reader.GetOrdinal("CoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCucHaiQuan"))) entity.TenCucHaiQuan = reader.GetString(reader.GetOrdinal("TenCucHaiQuan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ChungTuThanhToan_ChiTiet> collection, long id)
        {
            foreach (KDT_VNACC_ChungTuThanhToan_ChiTiet item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ChungTuThanhToan_ChiTiet VALUES(@ChungTuThanhToan_ID, @SoThuTuGiaoDich, @NgayTaoDuLieu, @ThoiGianTaoDuLieu, @SoTienTruLui, @SoTienTangLen, @SoToKhai, @NgayDangKyToKhai, @MaLoaiHinh, @CoQuanHaiQuan, @TenCucHaiQuan)";
            string update = "UPDATE t_KDT_VNACC_ChungTuThanhToan_ChiTiet SET ChungTuThanhToan_ID = @ChungTuThanhToan_ID, SoThuTuGiaoDich = @SoThuTuGiaoDich, NgayTaoDuLieu = @NgayTaoDuLieu, ThoiGianTaoDuLieu = @ThoiGianTaoDuLieu, SoTienTruLui = @SoTienTruLui, SoTienTangLen = @SoTienTangLen, SoToKhai = @SoToKhai, NgayDangKyToKhai = @NgayDangKyToKhai, MaLoaiHinh = @MaLoaiHinh, CoQuanHaiQuan = @CoQuanHaiQuan, TenCucHaiQuan = @TenCucHaiQuan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ChungTuThanhToan_ChiTiet WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChungTuThanhToan_ID", SqlDbType.BigInt, "ChungTuThanhToan_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuGiaoDich", SqlDbType.Decimal, "SoThuTuGiaoDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTaoDuLieu", SqlDbType.DateTime, "NgayTaoDuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianTaoDuLieu", SqlDbType.DateTime, "ThoiGianTaoDuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienTruLui", SqlDbType.Decimal, "SoTienTruLui", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienTangLen", SqlDbType.Decimal, "SoTienTangLen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, "NgayDangKyToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, "TenCucHaiQuan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChungTuThanhToan_ID", SqlDbType.BigInt, "ChungTuThanhToan_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuGiaoDich", SqlDbType.Decimal, "SoThuTuGiaoDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTaoDuLieu", SqlDbType.DateTime, "NgayTaoDuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianTaoDuLieu", SqlDbType.DateTime, "ThoiGianTaoDuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienTruLui", SqlDbType.Decimal, "SoTienTruLui", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienTangLen", SqlDbType.Decimal, "SoTienTangLen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, "NgayDangKyToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, "TenCucHaiQuan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ChungTuThanhToan_ChiTiet VALUES(@ChungTuThanhToan_ID, @SoThuTuGiaoDich, @NgayTaoDuLieu, @ThoiGianTaoDuLieu, @SoTienTruLui, @SoTienTangLen, @SoToKhai, @NgayDangKyToKhai, @MaLoaiHinh, @CoQuanHaiQuan, @TenCucHaiQuan)";
            string update = "UPDATE t_KDT_VNACC_ChungTuThanhToan_ChiTiet SET ChungTuThanhToan_ID = @ChungTuThanhToan_ID, SoThuTuGiaoDich = @SoThuTuGiaoDich, NgayTaoDuLieu = @NgayTaoDuLieu, ThoiGianTaoDuLieu = @ThoiGianTaoDuLieu, SoTienTruLui = @SoTienTruLui, SoTienTangLen = @SoTienTangLen, SoToKhai = @SoToKhai, NgayDangKyToKhai = @NgayDangKyToKhai, MaLoaiHinh = @MaLoaiHinh, CoQuanHaiQuan = @CoQuanHaiQuan, TenCucHaiQuan = @TenCucHaiQuan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ChungTuThanhToan_ChiTiet WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChungTuThanhToan_ID", SqlDbType.BigInt, "ChungTuThanhToan_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuGiaoDich", SqlDbType.Decimal, "SoThuTuGiaoDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTaoDuLieu", SqlDbType.DateTime, "NgayTaoDuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianTaoDuLieu", SqlDbType.DateTime, "ThoiGianTaoDuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienTruLui", SqlDbType.Decimal, "SoTienTruLui", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienTangLen", SqlDbType.Decimal, "SoTienTangLen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, "NgayDangKyToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, "TenCucHaiQuan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChungTuThanhToan_ID", SqlDbType.BigInt, "ChungTuThanhToan_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuGiaoDich", SqlDbType.Decimal, "SoThuTuGiaoDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTaoDuLieu", SqlDbType.DateTime, "NgayTaoDuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianTaoDuLieu", SqlDbType.DateTime, "ThoiGianTaoDuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienTruLui", SqlDbType.Decimal, "SoTienTruLui", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienTangLen", SqlDbType.Decimal, "SoTienTangLen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, "NgayDangKyToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, "TenCucHaiQuan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ChungTuThanhToan_ChiTiet Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ChungTuThanhToan_ChiTiet> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ChungTuThanhToan_ChiTiet> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ChungTuThanhToan_ChiTiet> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACC_ChungTuThanhToan_ChiTiet> SelectCollectionBy_ChungTuThanhToan_ID(long chungTuThanhToan_ID)
		{
            IDataReader reader = SelectReaderBy_ChungTuThanhToan_ID(chungTuThanhToan_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_ChungTuThanhToan_ID(long chungTuThanhToan_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungTuThanhToan_ID", SqlDbType.BigInt, chungTuThanhToan_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_ChungTuThanhToan_ID(long chungTuThanhToan_ID)
		{
			const string spName = "p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungTuThanhToan_ID", SqlDbType.BigInt, chungTuThanhToan_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ChungTuThanhToan_ChiTiet(long chungTuThanhToan_ID, decimal soThuTuGiaoDich, DateTime ngayTaoDuLieu, DateTime thoiGianTaoDuLieu, decimal soTienTruLui, decimal soTienTangLen, decimal soToKhai, DateTime ngayDangKyToKhai, string maLoaiHinh, string coQuanHaiQuan, string tenCucHaiQuan)
		{
			KDT_VNACC_ChungTuThanhToan_ChiTiet entity = new KDT_VNACC_ChungTuThanhToan_ChiTiet();	
			entity.ChungTuThanhToan_ID = chungTuThanhToan_ID;
			entity.SoThuTuGiaoDich = soThuTuGiaoDich;
			entity.NgayTaoDuLieu = ngayTaoDuLieu;
			entity.ThoiGianTaoDuLieu = thoiGianTaoDuLieu;
			entity.SoTienTruLui = soTienTruLui;
			entity.SoTienTangLen = soTienTangLen;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKyToKhai = ngayDangKyToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.TenCucHaiQuan = tenCucHaiQuan;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@ChungTuThanhToan_ID", SqlDbType.BigInt, ChungTuThanhToan_ID);
			db.AddInParameter(dbCommand, "@SoThuTuGiaoDich", SqlDbType.Decimal, SoThuTuGiaoDich);
			db.AddInParameter(dbCommand, "@NgayTaoDuLieu", SqlDbType.DateTime, NgayTaoDuLieu.Year <= 1753 ? DBNull.Value : (object) NgayTaoDuLieu);
			db.AddInParameter(dbCommand, "@ThoiGianTaoDuLieu", SqlDbType.DateTime, ThoiGianTaoDuLieu.Year <= 1753 ? DBNull.Value : (object) ThoiGianTaoDuLieu);
			db.AddInParameter(dbCommand, "@SoTienTruLui", SqlDbType.Decimal, SoTienTruLui);
			db.AddInParameter(dbCommand, "@SoTienTangLen", SqlDbType.Decimal, SoTienTangLen);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, NgayDangKyToKhai.Year <= 1753 ? DBNull.Value : (object) NgayDangKyToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, TenCucHaiQuan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ChungTuThanhToan_ChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuThanhToan_ChiTiet item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ChungTuThanhToan_ChiTiet(long id, long chungTuThanhToan_ID, decimal soThuTuGiaoDich, DateTime ngayTaoDuLieu, DateTime thoiGianTaoDuLieu, decimal soTienTruLui, decimal soTienTangLen, decimal soToKhai, DateTime ngayDangKyToKhai, string maLoaiHinh, string coQuanHaiQuan, string tenCucHaiQuan)
		{
			KDT_VNACC_ChungTuThanhToan_ChiTiet entity = new KDT_VNACC_ChungTuThanhToan_ChiTiet();			
			entity.ID = id;
			entity.ChungTuThanhToan_ID = chungTuThanhToan_ID;
			entity.SoThuTuGiaoDich = soThuTuGiaoDich;
			entity.NgayTaoDuLieu = ngayTaoDuLieu;
			entity.ThoiGianTaoDuLieu = thoiGianTaoDuLieu;
			entity.SoTienTruLui = soTienTruLui;
			entity.SoTienTangLen = soTienTangLen;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKyToKhai = ngayDangKyToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.TenCucHaiQuan = tenCucHaiQuan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ChungTuThanhToan_ID", SqlDbType.BigInt, ChungTuThanhToan_ID);
			db.AddInParameter(dbCommand, "@SoThuTuGiaoDich", SqlDbType.Decimal, SoThuTuGiaoDich);
			db.AddInParameter(dbCommand, "@NgayTaoDuLieu", SqlDbType.DateTime, NgayTaoDuLieu.Year <= 1753 ? DBNull.Value : (object) NgayTaoDuLieu);
			db.AddInParameter(dbCommand, "@ThoiGianTaoDuLieu", SqlDbType.DateTime, ThoiGianTaoDuLieu.Year <= 1753 ? DBNull.Value : (object) ThoiGianTaoDuLieu);
			db.AddInParameter(dbCommand, "@SoTienTruLui", SqlDbType.Decimal, SoTienTruLui);
			db.AddInParameter(dbCommand, "@SoTienTangLen", SqlDbType.Decimal, SoTienTangLen);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, NgayDangKyToKhai.Year <= 1753 ? DBNull.Value : (object) NgayDangKyToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, TenCucHaiQuan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ChungTuThanhToan_ChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuThanhToan_ChiTiet item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ChungTuThanhToan_ChiTiet(long id, long chungTuThanhToan_ID, decimal soThuTuGiaoDich, DateTime ngayTaoDuLieu, DateTime thoiGianTaoDuLieu, decimal soTienTruLui, decimal soTienTangLen, decimal soToKhai, DateTime ngayDangKyToKhai, string maLoaiHinh, string coQuanHaiQuan, string tenCucHaiQuan)
		{
			KDT_VNACC_ChungTuThanhToan_ChiTiet entity = new KDT_VNACC_ChungTuThanhToan_ChiTiet();			
			entity.ID = id;
			entity.ChungTuThanhToan_ID = chungTuThanhToan_ID;
			entity.SoThuTuGiaoDich = soThuTuGiaoDich;
			entity.NgayTaoDuLieu = ngayTaoDuLieu;
			entity.ThoiGianTaoDuLieu = thoiGianTaoDuLieu;
			entity.SoTienTruLui = soTienTruLui;
			entity.SoTienTangLen = soTienTangLen;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKyToKhai = ngayDangKyToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.TenCucHaiQuan = tenCucHaiQuan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ChungTuThanhToan_ID", SqlDbType.BigInt, ChungTuThanhToan_ID);
			db.AddInParameter(dbCommand, "@SoThuTuGiaoDich", SqlDbType.Decimal, SoThuTuGiaoDich);
			db.AddInParameter(dbCommand, "@NgayTaoDuLieu", SqlDbType.DateTime, NgayTaoDuLieu.Year <= 1753 ? DBNull.Value : (object) NgayTaoDuLieu);
			db.AddInParameter(dbCommand, "@ThoiGianTaoDuLieu", SqlDbType.DateTime, ThoiGianTaoDuLieu.Year <= 1753 ? DBNull.Value : (object) ThoiGianTaoDuLieu);
			db.AddInParameter(dbCommand, "@SoTienTruLui", SqlDbType.Decimal, SoTienTruLui);
			db.AddInParameter(dbCommand, "@SoTienTangLen", SqlDbType.Decimal, SoTienTangLen);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, NgayDangKyToKhai.Year <= 1753 ? DBNull.Value : (object) NgayDangKyToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, TenCucHaiQuan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ChungTuThanhToan_ChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuThanhToan_ChiTiet item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ChungTuThanhToan_ChiTiet(long id)
		{
			KDT_VNACC_ChungTuThanhToan_ChiTiet entity = new KDT_VNACC_ChungTuThanhToan_ChiTiet();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_ChungTuThanhToan_ID(long chungTuThanhToan_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungTuThanhToan_ID", SqlDbType.BigInt, chungTuThanhToan_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ChungTuThanhToan_ChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuThanhToan_ChiTiet item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}