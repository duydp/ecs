using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
    public partial class BCThueXNK
    {
 
       //---------------------------------------------------------------------------------------------

        #region Update methods.


        //---------------------------------------------------------------------------------------------

        public int UpdateNgayThucNhapXuatTransaction(int soToKhai, string maLoaiHinh, DateTime ngayDangKy, DateTime ngayThuc, SqlTransaction transaction)
        {
            string sql = "";
            if (maLoaiHinh.Contains("NSX"))
                sql = "UPDATE t_KDT_SXXK_BCThueXNK SET NgayThucNhap = @NgayThuc WHERE SoToKhaiNhap = @SoToKhai AND MaLoaiHinhNhap = @MaLoaiHinh AND NgayDangKyNhap = @NgayDangKy";
            else
                sql = "UPDATE t_KDT_SXXK_BCThueXNK SET NgayThucXuat = @NgayThuc WHERE SoToKhaiXuat = @SoToKhai AND MaLoaiHinhXuat = @MaLoaiHinh AND NgayDangKyXuat = @NgayDangKy";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, ngayDangKy);
            db.AddInParameter(dbCommand, "@NgayThuc", SqlDbType.DateTime, ngayThuc);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------



        #endregion

    }
}