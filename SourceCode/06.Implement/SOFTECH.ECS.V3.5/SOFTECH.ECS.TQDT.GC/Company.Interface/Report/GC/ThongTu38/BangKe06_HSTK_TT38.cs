﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;
 

namespace Company.Interface.Report.GC
{
    public partial class BangKe06_HSTK_TT38 : DevExpress.XtraReports.UI.XtraReport
    {
        //public Report.ReportViewBC06Form report = new ReportViewBC06Form();
        public HopDong HD = new HopDong();
        public DataTable dsBK = new DataTable();
        decimal tongluong = 0;
        decimal tongLuongXuatTra = 0;
        decimal tongLuongNK = 0;
        decimal tongLuongXK = 0;
        decimal tongLuongChenhLech = 0;
        int index = 1;
        public BangKe06_HSTK_TT38()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
           // DataTable dt = new Company.GC.BLL.GC.ThanhKhoanHDGC().getDuLieuThanhKhoan(this.HD.ID).Tables[0];
            DataTable dt = dsBK;
            dt.TableName = "t_GC_ThanhKhoanHDGC";
            this.DataSource = dt;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();
                }
                else
                {
                    loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                }
            }
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField(".Ten"),
                new DevExpress.XtraReports.UI.GroupField(".Ma"),
                new DevExpress.XtraReports.UI.GroupField(".TongLuongNK"),
                new DevExpress.XtraReports.UI.GroupField(".TongLuongCU"),
                new DevExpress.XtraReports.UI.GroupField(".TongLuongXK"),
                new DevExpress.XtraReports.UI.GroupField(".ChenhLech"),
                });
            lblMathang.Text = loaiSP;
            
            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;

            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblMaNPL_H.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ma");
            lblTenNPL_H.DataBindings.Add("Text", this.DataSource,dt.TableName + ".Ten");
            lblMaNPL_F.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ma");
            lblTenNPL_F.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ten");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKy");
            lblDVT_H.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblDVT_F.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT","");
            lblTongLuongNK_H.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TongLuongNK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongCU_H.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TongLuongCU", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongXuat_H.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TongLuongXK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoToKhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoTKVnacc");
            lblMaLoaiHinh.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaLoaiHinh");
            lblLuongSD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSD_TK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongNK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSD_TK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongXK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSD_TK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblChenhLech.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTon_TK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblChenhLech_H.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChenhLech", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongXuatTra.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongXuatTra", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            


            lblKLXLCL_H.DataBindings.Add("Text", this.DataSource, dt.TableName + ".KetLuanXLCL");
            lblNgayThangNam.Text= GlobalSettings.TieudeNgay;
            
        }

        private void xrLabel40_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string ten = GetCurrentColumnValue("Ten").ToString();
            //if (!ten.Contains("(&#!"))
            //    lblTenNPL.Text = ten.Substring(0, ten.LastIndexOf("(") - 1);
            //else
            //{
            //    lblTenNPL.Text = ten.Substring(0, ten.LastIndexOf("(&#!") - 1);
            //}
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string ten = GetCurrentColumnValue("Ten").ToString();
            //if(!ten.Contains("&#!"))
            //    {
            //string[] arr = ten.Split(new char[1] {'('});
            //lblMaNPL.Text = arr[arr.Length - 1].Replace(")","");
            //    }
            //else
            //{
            //    string[] arr = ten.Split(new string[] { "(&#!" },StringSplitOptions.None);
            //    lblMaNPL.Text = arr[arr.Length - 1].Replace("&#!)", "");
            //}
        }

        //private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        //{
        //    XRLabel label = (XRLabel)sender;
        //    report.Label = label;
        //    report.txtName.Text = label.Text;
        //    report.lblName.Text = label.Tag.ToString();
        //}
        public void setText(XRLabel label, string text)
        {
            //label.Text = text;
        }

        private void lblMaLoaiHinh_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblMaLoaiHinh.Text = lblMaLoaiHinh.Text.Substring(2, 3);
        }

        private void lblLuongSD_AfterPrint(object sender, EventArgs e)
        {
            tongluong += Convert.ToDecimal(lblLuongSD.Text);
        }

        private void xrTableCell27_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongLuongXuat_F.Text = tongluong.ToString("N4");
            tongluong = 0;
        }

        private void xrTableCell4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell4.Text = index.ToString();
            index++;
        }

        private void lblTongXuatTra_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                tongLuongXuatTra += Convert.ToDecimal(lblTongXuatTra.Text);
            }
            catch (Exception ex) 
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }

        private void lblTongLuongXuatTra_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongLuongXuatTra_F.Text = tongLuongXuatTra.ToString("N4");
            tongLuongXuatTra = 0;
        }

        private void lblTongNK_AfterPrint(object sender, EventArgs e)
        {
            tongLuongNK += Convert.ToDecimal(lblTongNK.Text);
        }

        private void lblTongXK_AfterPrint(object sender, EventArgs e)
        {
            tongLuongXK += Convert.ToDecimal(lblTongXK.Text);
        }

        private void lblTongLuongNK_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongLuongNK_F.Text = tongLuongNK.ToString("N4");
            tongLuongNK = 0;
        }

        private void lblTongLuongXuat_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongLuongXuat_F.Text = tongLuongXK.ToString("N4");
            tongLuongXK = 0;
        }

        private void lblTongChenhLech_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblChenhLech_AfterPrint(object sender, EventArgs e)
        {

        }

        private void lblNgayDangKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblNgayDangKy.Text = lblNgayDangKy.Text.Substring(0,10);
        }

        private void lblSTT_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblSTT_F.Text = xrTableCell4.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
