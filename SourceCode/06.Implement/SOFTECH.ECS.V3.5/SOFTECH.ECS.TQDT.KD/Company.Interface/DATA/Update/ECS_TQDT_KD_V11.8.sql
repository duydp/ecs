
/****** Object:  StoredProcedure [dbo].[p_KDT_NoiDungDieuChinhTK_SelectMax_SoDieuChinh]    Script Date: 11/25/2013 09:07:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTK_SelectMax_SoDieuChinh]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_SelectMax_SoDieuChinh]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_NoiDungDieuChinhTK_SelectMax_SoDieuChinh]    Script Date: 11/25/2013 09:07:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_SelectBy_TKMD_ID]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_SelectMax_SoDieuChinh]
	@TKMD_ID bigint,
	@MaLoaiHinh varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoDieuChinh]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTK]
WHERE
	[TKMD_ID] = @TKMD_ID
	and [MaLoaiHinh] = @MaLoaiHinh



GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '11.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('11.8', GETDATE(), N'cap nhat store noi dung dieu chinh TK')
END	
