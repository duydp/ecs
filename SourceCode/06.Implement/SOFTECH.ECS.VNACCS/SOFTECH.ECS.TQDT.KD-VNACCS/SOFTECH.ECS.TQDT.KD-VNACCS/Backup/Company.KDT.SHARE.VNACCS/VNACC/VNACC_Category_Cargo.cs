using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_Cargo
	{
        protected static List<VNACC_Category_Cargo> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_Cargo> collection = new List<VNACC_Category_Cargo>();
            while (reader.Read())
            {
                VNACC_Category_Cargo entity = new VNACC_Category_Cargo();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("BondedAreaCode"))) entity.BondedAreaCode = reader.GetString(reader.GetOrdinal("BondedAreaCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("BondedAreaName"))) entity.BondedAreaName = reader.GetString(reader.GetOrdinal("BondedAreaName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_Cargo> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [BondedAreaCode], [BondedAreaName] FROM [dbo].[t_VNACC_Category_Cargo]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }
      
        public static List<VNACC_Category_Cargo> SelectCollectionBy(List<VNACC_Category_Cargo> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_Cargo o)
            {
                return o.TableID.Contains(tableID);
            });
        }
	}	
}