﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using SOFTECH.ECS.TQDT.GC.KDT.GC;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
namespace Company.Interface.KDT.GC
{
    public partial class TaiXuatForm : BaseForm
    {
        public KDT_GC_TaiXuatDangKy TaiXuatDK = new KDT_GC_TaiXuatDangKy();
        private KDT_GC_TaiXuat TaiXuat = new KDT_GC_TaiXuat();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        bool isEdit = false;
        
        public TaiXuatForm()
        {
            InitializeComponent();
        }

   

        private void CungUngForm_Load(object sender, EventArgs e)
        {

            LoadData();
            SetCommand();
        }
        private void LoadData()
        {
            if (TaiXuatDK.ID == 0)
                TaiXuatDK.TrangThaiXuLy = -1;
            txtSoHopDong.Text = TaiXuatDK.SoHopDong;
            clcNgayHD.Value = TaiXuatDK.NgayHopDong;
            clcNgayHetHan.Value = TaiXuatDK.NgayHetHan;
            txtSoTiepNhan.Text = TaiXuatDK.SoTiepNhan.ToString();
            clcNgayTiepNhan.Value = TaiXuatDK.NgayTiepNhan;
            ctrMaHaiQuan.Code = TaiXuatDK.MaHQ;
           
            switch (TaiXuatDK.TrangThaiXuLy.ToString())
            {
                case "0":
                    lblTrangThai.Text = "Chờ duyệt";
                    break;
                case "-1":
                    lblTrangThai.Text = "Chưa khai báo";
                    break;
                case "1":
                    lblTrangThai.Text = "Đã duyệt";
                    break;
                case "2":
                    lblTrangThai.Text = "Không phê duyệt";
                    break;
            }
            

            dgList.DataSource = TaiXuatDK.TaiXuat_List;
            dgList.Refetch();
        }
        private void SetCommand()
        {
            int TrangThai = TaiXuatDK.TrangThaiXuLy;
            if (TaiXuatDK.TrangThaiXuLy == -1 || TaiXuatDK.TrangThaiXuLy == 2)
            {
                cmdThemTKTX.Enabled = cmdThemTKTX1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = true;
            }
            else
            {
                cmdThemTKTX.Enabled = cmdThemTKTX1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;
            }

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<KDT_GC_TaiXuat> ItemColl = new List<KDT_GC_TaiXuat>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa NPL này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_GC_TaiXuat)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_GC_TaiXuat item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TaiXuatDK.TaiXuat_List.Remove(item);
                    }
                    dgList.Refetch();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
           
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemTKTX":
                    ThemNPL();
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdSend":
                    SendV5();
                    break;
                case "cmdFeedBack":
                    FeedBackV5();
                    break;
                case "cmdKetQuaXuLy":
                    KetQuaXuLyPK();
                    break;
            }
        }
        private void ThemNPL()
        {
            TaiXuatSelectTKForm f = new TaiXuatSelectTKForm();
            f.TaiXuatDangky = TaiXuatDK;
           // f.IDHopDong = CungUngDK.HopDong_ID;
            f.ShowDialog();
            LoadData();


        }
        private void Save()
        {
            try
            {
                TaiXuatDK.SoHopDong = txtSoHopDong.Text;
                TaiXuatDK.NgayHopDong = clcNgayHD.Value;
                TaiXuatDK.NgayHetHan = clcNgayHetHan.Value;
                TaiXuatDK.MaHQ = ctrMaHaiQuan.Code;
                TaiXuatDK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                TaiXuatDK.InsertUpdateFull();
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lưu không thành công : " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void KetQuaXuLyPK()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.TaiXuatDK.ID;
            form.DeclarationIssuer = DeclarationIssuer.GC_TaiXuat;
            form.ShowDialog(this);
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            //MsgSend sendXML = new MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
            //sendXML.master_id = TaiXuatDK.ID;
            //if (!sendXML.Load())
            //{
            //    ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
            //    XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    return;
            //}
            while (isFeedBack)
            {
                string reference = TaiXuatDK.GuidStr;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.GC_TaiXuat,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.GC_TaiXuat,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = TaiXuatDK.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TaiXuatDK.MaHQ.Trim())),
                                                  //Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TaiXuatDK.MaHQ).Trim() : TaiXuatDK.MaHQ
                                                  Identity = TaiXuatDK.MaHQ
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }

            }
        }

        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.TaiXuatSendHandler(TaiXuatDK, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendV5()
        {
            if (ShowMessage("Bạn có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {
                if (TaiXuatDK.ID == 0)
                {
                    this.ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    TaiXuatDK = KDT_GC_TaiXuatDangKy.LoadFull(TaiXuatDK.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.TaiXuat;
                sendXML.master_id = TaiXuatDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    if (this.TaiXuatDK.TaiXuat_List.Count > 0)
                    {

                        string returnMessage = string.Empty;
                        TaiXuatDK.GuidStr = Guid.NewGuid().ToString();
                        GC_TKTaiXuat taiXuat  = Mapper_V4.ToDataTransferTaiXuat(TaiXuatDK, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity = TaiXuatDK.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TaiXuatDK.MaHQ)),
                                  // Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TaiXuatDK.MaHQ).Trim() : TaiXuatDK.MaHQ
                                  Identity = TaiXuatDK.MaHQ
                              },
                              new SubjectBase()
                              {

                                  Type = taiXuat.Issuer,
                                  Function = DeclarationFunction.KHAI_BAO,
                                  Reference = TaiXuatDK.GuidStr,
                              },
                              taiXuat
                            );
                        TaiXuatDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(TaiXuatDK.ID, MessageTitle.KhaiBaoTaiXuat);
                            cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdThemTKTX.Enabled = cmdThemTKTX1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            btnXoa.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.TaiXuat;
                            sendXML.master_id = TaiXuatDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            TaiXuatDK.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {

            if(e.Row.RowType == RowType.Record)
            {
                TaiXuat = (KDT_GC_TaiXuat)e.Row.DataRow;
                TaiXuatDetailForm frm = new TaiXuatDetailForm();
                frm.taiXuat = TaiXuat;
                frm.TrangThai = TaiXuatDK.TrangThaiXuLy;
                frm.ShowDialog();
                dgList.DataSource = TaiXuatDK.TaiXuat_List;
                dgList.Refetch();
            }
        }
     
        
    }
}
