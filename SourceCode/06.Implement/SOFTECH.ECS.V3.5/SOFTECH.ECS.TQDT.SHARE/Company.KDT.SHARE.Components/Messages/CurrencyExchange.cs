﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Nguyên tệ
    /// </summary>
    public class CurrencyExchange
    {
        /// <summary>
        /// Mã nguyên tệ
        /// </summary>
        [XmlElement("currencyType")]
        public string CurrencyType { get; set; }
        /// <summary>
        /// Tỷ giá nguyên tệ -Do Hải quan quy định
        /// </summary>
        [XmlElement("rate")]
        public string Rate { get; set; }
       
    }
}
