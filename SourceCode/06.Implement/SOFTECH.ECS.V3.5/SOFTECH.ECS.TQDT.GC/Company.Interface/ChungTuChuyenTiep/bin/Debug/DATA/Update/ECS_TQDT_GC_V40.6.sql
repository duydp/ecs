
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_GC_SanPham_Update_New]    
-- Database: ECS.TQDT.GC    
-- Author: Ngo Thanh Tung    
-- Time created: Friday, June 25, 2010    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_GC_SanPham_Update_New]    
 @HopDong_ID bigint,    
 @Ma varchar(50),  
 @MaMoi varchar(50),    
 @Ten nvarchar(255),    
 @MaHS varchar(12),    
 @DVT_ID char(3),    
 @SoLuongDangKy numeric(18, 5),    
 @SoLuongDaXuat numeric(18, 5),    
 @NhomSanPham_ID varchar(12),    
 @STTHang int,    
 @TrangThai int,    
 @DonGia numeric(18, 5)    
AS    
    
UPDATE    
 [dbo].[t_GC_SanPham]    
SET    
 [Ten] = @Ten,  
 [Ma]= @MaMoi,    
 [MaHS] = @MaHS,    
 [DVT_ID] = @DVT_ID,    
 [SoLuongDangKy] = @SoLuongDangKy,    
 [SoLuongDaXuat] = @SoLuongDaXuat,    
 [NhomSanPham_ID] = @NhomSanPham_ID,    
 [STTHang] = @STTHang,    
 [TrangThai] = @TrangThai,    
 [DonGia] = @DonGia    
WHERE    
 [HopDong_ID] = @HopDong_ID    
 AND [Ma] = @Ma    


 GO

 ------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Update_New]    
-- Database: ECS.TQDT.GC    
-- Author: Ngo Thanh Tung    
-- Time created: Friday, June 25, 2010    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_KDT_GC_SanPham_Update_New]    
 @HopDong_ID bigint,    
 @Ma varchar(50),  
 @MaMoi varchar(50),     
 @Ten nvarchar(255),    
 @MaHS varchar(12),    
 @DVT_ID char(3),    
 @SoLuongDangKy numeric(18, 6),    
 @NhomSanPham_ID varchar(12),    
 @STTHang int,    
 @DonGia numeric(18, 5)    
AS    
    
UPDATE    
 [dbo].[t_KDT_GC_SanPham]    
SET    
 [Ten] = @Ten,  
 [Ma]= @MaMoi,      
 [MaHS] = @MaHS,    
 [DVT_ID] = @DVT_ID,    
 [SoLuongDangKy] = @SoLuongDangKy,    
 [NhomSanPham_ID] = @NhomSanPham_ID,    
 [STTHang] = @STTHang,    
 [DonGia] = @DonGia    
WHERE    
 [HopDong_ID] = @HopDong_ID    
 AND [Ma] = @Ma    
  GO

------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_Update_New]    
-- Database: ECS_GC    
-- Author: Ngo Thanh Tung    
-- Time created: Tuesday, September 08, 2009    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Update_New]    
 @HopDong_ID bigint,    
 @Ma varchar(50),  
 @MaMoi varchar(50),     
 @Ten nvarchar(255),    
 @MaHS varchar(12),    
 @DVT_ID char(3),    
 @SoLuongDangKy numeric(18, 6),    
 @SoLuongDaNhap numeric(18, 6),    
 @SoLuongDaDung numeric(18, 6),    
 @SoLuongCungUng numeric(18, 6),    
 @TongNhuCau numeric(18, 6),    
 @STTHang int,    
 @TrangThai int,    
 @DonGia numeric(18, 5)    
AS    
    
UPDATE    
 [dbo].[t_GC_NguyenPhuLieu]    
SET    
 [Ten] = @Ten,  
 [Ma] = @MaMoi ,    
 [MaHS] = @MaHS,    
 [DVT_ID] = @DVT_ID,    
 [SoLuongDangKy] = @SoLuongDangKy,    
 [SoLuongDaNhap] = @SoLuongDaNhap,    
 [SoLuongDaDung] = @SoLuongDaDung,    
 [SoLuongCungUng] = @SoLuongCungUng,    
 [TongNhuCau] = @TongNhuCau,    
 [STTHang] = @STTHang,    
 [TrangThai] = @TrangThai,    
 [DonGia] = @DonGia    
WHERE    
 [HopDong_ID] = @HopDong_ID    
 AND [Ma] = @Ma    
  
  GO
    
     
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Update_New]    
-- Database: ECS_TQDT_GC_V3    
-- Author: Ngo Thanh Tung    
-- Time created: Tuesday, March 06, 2012    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Update_New]    
 @HopDong_ID bigint,    
 @Ma varchar(50),  
 @MaMoi varchar(50),    
 @Ten nvarchar(255),    
 @MaHS varchar(12),    
 @DVT_ID char(3),    
 @SoLuongDangKy numeric(18, 6),    
 @STTHang int,    
 @DonGia numeric(18, 5),    
 @GhiChu nvarchar(256)    
AS    
    
UPDATE    
 [dbo].[t_KDT_GC_NguyenPhuLieu]    
SET    
 [Ten] = @Ten,  
 [Ma] = @MaMoi ,    
 [MaHS] = @MaHS,    
 [DVT_ID] = @DVT_ID,    
 [SoLuongDangKy] = @SoLuongDangKy,    
 [STTHang] = @STTHang,    
 [DonGia] = @DonGia,    
 [GhiChu] = @GhiChu    
WHERE    
 [HopDong_ID] = @HopDong_ID    
 AND [Ma] = @Ma    
    
GO
   
     
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_GC_ThietBi_Update_New]    
-- Database: GiaCongECS    
-- Author: Ngo Thanh Tung    
-- Time created: Friday, September 19, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_GC_ThietBi_Update_New]    
 @HopDong_ID bigint,    
 @Ma varchar(50),  
 @MaMoi varchar(50),     
 @Ten nvarchar(255),    
 @MaHS varchar(12),    
 @DVT_ID char(3),    
 @SoLuongDangKy numeric(18, 5),    
 @SoLuongDaNhap numeric(18, 5),    
 @NuocXX_ID char(3),    
 @TinhTrang nvarchar(255),    
 @DonGia float,    
 @TriGia float,    
 @NguyenTe_ID char(3),    
 @STTHang int,    
 @TrangThai int    
AS    
UPDATE    
 [dbo].[t_GC_ThietBi]    
SET    
 [Ten] = @Ten,  
 [Ma] = @MaMoi ,    
 [MaHS] = @MaHS,    
 [DVT_ID] = @DVT_ID,    
 [SoLuongDangKy] = @SoLuongDangKy,    
 [SoLuongDaNhap] = @SoLuongDaNhap,    
 [NuocXX_ID] = @NuocXX_ID,    
 [TinhTrang] = @TinhTrang,    
 [DonGia] = @DonGia,    
 [TriGia] = @TriGia,    
 [NguyenTe_ID] = @NguyenTe_ID,    
 [STTHang] = @STTHang,    
 [TrangThai] = @TrangThai    
WHERE    
 [HopDong_ID] = @HopDong_ID    
 AND [Ma] = @Ma    
 GO
 ------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Update_New]      
-- Database: ECS_TQDT_GC_V3      
-- Author: Ngo Thanh Tung      
-- Time created: Tuesday, March 06, 2012      
------------------------------------------------------------------------------------------------------------------------      
      
ALTER PROCEDURE [dbo].[p_KDT_GC_ThietBi_Update_New]      
 @HopDong_ID bigint,      
 @Ma varchar(50),    
 @MaMoi varchar(50),      
 @Ten nvarchar(255),      
 @MaHS varchar(12),      
 @DVT_ID char(3),      
 @SoLuongDangKy numeric(18, 5),      
 @NuocXX_ID char(3),      
 @DonGia float,      
 @TriGia float,      
 @NguyenTe_ID char(3),      
 @STTHang int,      
 @TinhTrang nvarchar(50),      
 @GhiChu nvarchar(256)      
AS      
      
UPDATE      
 [dbo].[t_KDT_GC_ThietBi]      
SET      
 [Ten] = @Ten,    
 [Ma] = @MaMoi ,       
 [MaHS] = @MaHS,      
 [DVT_ID] = @DVT_ID,      
 [SoLuongDangKy] = @SoLuongDangKy,      
 [NuocXX_ID] = @NuocXX_ID,      
 [DonGia] = @DonGia,      
 [TriGia] = @TriGia,      
 [NguyenTe_ID] = @NguyenTe_ID,      
 [STTHang] = @STTHang,      
 [TinhTrang] = @TinhTrang,      
 [GhiChu] = @GhiChu      
WHERE      
 [HopDong_ID] = @HopDong_ID      
 AND [Ma] = @Ma      

GO       
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_GC_HangMau_Update_New]      
-- Database: ECS_TQDT_GC_V3      
-- Author: Ngo Thanh Tung      
-- Time created: Wednesday, March 14, 2012      
------------------------------------------------------------------------------------------------------------------------      
      
ALTER PROCEDURE [dbo].[p_GC_HangMau_Update_New]      
 @HopDong_ID bigint,      
 @Ma varchar(50),    
 @MaMoi varchar(50),     
 @Ten nvarchar(255),      
 @MaHS varchar(12),      
 @DVT_ID char(3),      
 @SoLuongDangKy numeric(18, 6),      
 @SoLuongDaNhap numeric(18, 6),      
 @SoLuongDaDung numeric(18, 6),      
 @SoLuongCungUng numeric(18, 6),      
 @TongNhuCau numeric(18, 6),      
 @STTHang int,      
 @TrangThai int,      
 @DonGia numeric(18, 5)      
AS      
      
UPDATE      
 [dbo].[t_GC_HangMau]      
SET      
 [Ten] = @Ten,      
 [Ma] = @MaMoi ,     
 [MaHS] = @MaHS,      
 [DVT_ID] = @DVT_ID,      
 [SoLuongDangKy] = @SoLuongDangKy,      
 [SoLuongDaNhap] = @SoLuongDaNhap,      
 [SoLuongDaDung] = @SoLuongDaDung,      
 [SoLuongCungUng] = @SoLuongCungUng,      
 [TongNhuCau] = @TongNhuCau,      
 [STTHang] = @STTHang,      
 [TrangThai] = @TrangThai,      
 [DonGia] = @DonGia      
WHERE      
 [HopDong_ID] = @HopDong_ID      
 AND [Ma] = @Ma      
GO
  
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Update_New]      
-- Database: ECS_TQDT_GC_V3      
-- Author: Ngo Thanh Tung      
-- Time created: Monday, March 05, 2012      
------------------------------------------------------------------------------------------------------------------------      
      
ALTER PROCEDURE [dbo].[p_KDT_GC_HangMau_Update_New]      
 @HopDong_ID bigint,      
 @Ma varchar(50),    
 @MaMoi varchar(50),      
 @Ten nvarchar(255),      
 @MaHS varchar(12),      
 @DVT_ID char(3),      
 @SoLuongDangKy numeric(18, 5),      
 @STTHang int,      
 @GhiChu nvarchar(1000)      
AS      
      
UPDATE      
 [dbo].[t_KDT_GC_HangMau]      
SET      
 [Ten] = @Ten,    
 [Ma] = @MaMoi ,      
 [MaHS] = @MaHS,      
 [DVT_ID] = @DVT_ID,      
 [SoLuongDangKy] = @SoLuongDangKy,      
 [STTHang] = @STTHang,      
 [GhiChu] = @GhiChu      
WHERE      
 [HopDong_ID] = @HopDong_ID      
 AND [Ma] = @Ma      
 GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.6',GETDATE(), N'CẬP NHẬT PROCDEDURE PHỤ KIỆN SỬA SP ,NPL,TB,HM')
END