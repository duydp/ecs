﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{
    
    public partial class ReportViewDinhMucKDTForm : BaseForm
    {
        public DinhMucDangKy DMDangKy;
        public DinhMucKDTReport DMReport = new DinhMucKDTReport();
        HopDong HD = new HopDong();
        public ReportViewDinhMucKDTForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            HD.ID = DMDangKy.ID_HopDong;
            HD.Load();
            if (DMDangKy.DMCollection.Count == 0 && DMDangKy.ID > 0)
                this.DMDangKy.LoadCollection();
            foreach (DinhMuc dm in this.DMDangKy.DMCollection)
            {
                if (!CheckMaSPExist(dm.MaSanPham))
                    cboSanPham.Items.Add(dm.MaSanPham, dm.MaSanPham);
            }
            cboSanPham.SelectedIndex = 0;
            printControl1.PrintingSystem = this.DMReport.PrintingSystem;
        }
        private bool CheckMaSPExist(string maSP)
        {
            foreach (UIComboBoxItem item in cboSanPham.Items)
                if (item.Text == maSP) return true;
            return false;
        }
        private DinhMucCollection GetDinhMucSP(string maSP)
        {
            DinhMucCollection temp = new DinhMucCollection();
            foreach (DinhMuc dm in this.DMDangKy.DMCollection)
            {
                if (dm.MaSanPham == maSP) temp.Add(dm);
            }
            return temp;
        }
        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.DMReport.DMCollection = GetDinhMucSP(cboSanPham.SelectedValue.ToString());
            this.DMReport.HD = this.HD;
            this.DMReport.BindReport(cboSanPham.SelectedValue.ToString());
            this.DMReport.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (DMDangKy.ID < 0)
            {
                ShowMessage("Thông tin định mức chưa được lưu ", false);
                return;
            }
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            try
            {
                this.DMReport.CreateDocument();
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            try
            {
                this.DMReport.CreateDocument();
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            } 
        }

       

    

  

    
    }
}