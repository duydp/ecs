/*
Run this script on:

        192.168.72.100.ECS_TQDT_KD_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_KD_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 07/11/2012 4:45:24 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_AnDinhThue_ChiTiet]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet] ALTER COLUMN [SacThue] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet] ALTER COLUMN [TienThue] [numeric] (18, 4) NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet] ALTER COLUMN [Loai] [int] NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet] ALTER COLUMN [Khoan] [int] NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet] ALTER COLUMN [Muc] [int] NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet] ALTER COLUMN [TieuMuc] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.4', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.4', getdate(), null)
	end 