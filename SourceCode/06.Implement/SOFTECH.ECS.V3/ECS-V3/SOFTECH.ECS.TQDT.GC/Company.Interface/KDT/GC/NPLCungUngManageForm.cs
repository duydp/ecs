﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean=Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.GC.BLL.KDT.SXXK;
using System.Xml.Serialization;
using Company.Interface.KDT.GC;
using Company.GC.BLL.KDT.GC;
using System.Xml;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
	public partial class NPLCungUngManageForm : BaseForm
	{
        private BKCungUngDangKyCollection BKCungUngCollection = new BKCungUngDangKyCollection();
        private BKCungUngDangKy BKCU = new BKCungUngDangKy();
        public long IDHopDong = 0;
        public NPLCungUngManageForm()
		{
			InitializeComponent();
		}

		//-----------------------------------------------------------------------------------------

        
		private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
		{
            try
			{
                //An nut xac nhan
                cmdXacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False ;

                txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
                this.Cursor = Cursors.WaitCursor;
                cbStatus.SelectedIndex = 1;
                this.search();
			}
			catch (Exception ex)
			{
                showMsg("MSG_2702004", ex.Message);
				//MessageBox.Show(ex.Message);
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            BKCungUngDangKy BKCU = (BKCungUngDangKy)e.Row.DataRow;
            NPLCungUngTheoTKSendForm f = new NPLCungUngTheoTKSendForm();
            BKCU.Load();
            if (BKCU.TKMD_ID > 0)
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.ID = BKCU.TKMD_ID;
                TKMD.Load();                
                f.TKMD = TKMD;
                //HopDong HD = new HopDong();
                //HD.ID = TKMD.IDHopDong;
                //HD.Load();
                //f.HD = HD;
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT.ID = BKCU.TKCT_ID;
                TKCT = ToKhaiChuyenTiep.Load(BKCU.TKCT_ID);
                f.TKCT = TKCT;
                //HopDong HD = new HopDong();
                //HD.ID = TKCT.IDHopDong;
                //HD.Load();
                //f.HD = HD;
            }
            f.BKCU = BKCU;
            f.ShowDialog();
            search();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
          
            string where = string.Format("MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan = " + txtSoTiepNhan.Value;
            }


            if (txtNamTiepNhan.TextLength > 0 && cbStatus.SelectedIndex>0)
            {
                where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
            }
            where += " AND TrangThaiXuLy = "+cbStatus.SelectedValue.ToString();                       
            // Thực hiện tìm kiếm.            
            this.BKCungUngCollection = new  BKCungUngDangKy().SelectCollectionDynamic(where, "");
            dgList.DataSource = this.BKCungUngCollection;
            SetCommandStatus();
           
            //this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();            
        }
        private void SetCommandStatus()
        {
            if (cbStatus.SelectedValue.ToString() == "-1")
            {
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdXacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled= InheritableBoolean.False;
                btnXoa.Enabled = true;
            }
            else if (cbStatus.SelectedValue.ToString() == "0")
            {
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.True;
                btnXoa.Enabled = false;
            }
            else
            {
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                btnXoa.Enabled = false;
            }
        }
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                if (e.Row.Cells["NgayTiepNhan"].Text != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }
            }
        }

      
        
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
            //HopDongManageForm f = new HopDongManageForm();
            //f.IsBrowseForm = true;
            //f.IsDaDuyet = true;
            //f.ShowDialog();
            //if (f.HopDongSelected.ID > 0)
            //{
            //    txtSoHopDong.Text = f.HopDongSelected.SoHopDong + "(" + f.HopDongSelected.NgayKy.ToString("dd/MM/yyyy") + ")";
            //    this.IDHopDong = f.HopDongSelected.ID;
            //}
        }

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdHuyKhaiBao":
                    Huy();
                    break;
                case "cmdSend":
                    khaibao();
                    break;
                case "cmdXacNhan":
                    LaySoTiepNhanDT();
                    break;
                case "cmdNhanDuLieu":
                    NhanDuLieu();
                    break;
                case "cmdCSDaDuyet":
                    ChuyenTrangThai();
                    break;
                 case "InPhieuTN": this.inPhieuTN();break;
            }
          
        }
        private void inPhieuTN()
        {
            if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;             
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "NGUYÊN PHỤ LIỆU CUNG ỨNG";
            Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    BKCungUngDangKy bkDangKySelected = (BKCungUngDangKy)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = bkDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = bkDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                    break;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();
        }

        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count > 0)
            {
                BKCungUngDangKyCollection bkcuColl = new BKCungUngDangKyCollection();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    object a11 = grItem.GetRow().DataRow;
                    if (grItem.RowType == RowType.Record)
                    {
                        bkcuColl.Add((BKCungUngDangKy)grItem.GetRow().DataRow);
                    }
                }
                for (int i = 0; i < bkcuColl.Count; i++)
                {
                    //string msg = "Bạn có muốn chuyển trạng thái của bảng kê được chọn sang đã duyệt không?";
                    //msg += "\n\nSố thứ tự bảng kê: " + bkcuColl[i].ID.ToString();
                    if (showMsg("MSG_0203074", bkcuColl[i].ID, true) == "Yes")
                    {
                        ChuyenTrangThaiTKNPLCU obj = new ChuyenTrangThaiTKNPLCU(bkcuColl[i]);
                        obj.ShowDialog();
                    }
                }
                this.search();
            }
            else
            {
                showMsg("MSG_0203068");
            }
        }

        private void khaibao()
        {
            string xmlCurrent = "";
            string password = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            WSForm wsForm = new WSForm();
            try
            {
                if (dgList.GetRow() != null)
                {
                    BKCU = (BKCungUngDangKy)dgList.GetRow().DataRow;
                    {
                        sendXML.LoaiHS = "DMCU";
                        sendXML.master_id = BKCU.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_240203");
                            //MLMessages("Định mức cung ứng này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_STN03","supply norm", false);
                            return;
                        }
                    }

                    BKCU.Load();
                    BKCU.LoadSanPhamCungUngCollection();
                    foreach (Company.GC.BLL.KDT.GC.SanPhanCungUng SPCungUng in BKCU.SanPhamCungUngCollection)
                    {
                        SPCungUng.Load();
                        SPCungUng.LoadNPLCungUngCollection();
                    }
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    xmlCurrent = BKCU.WSSend(password);
                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "DMCU";
                    sendXML.master_id = BKCU.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 1;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    Company.GC.BLL.KDT.HangDoi hd = new Company.GC.BLL.KDT.HangDoi();
                            //    hd.ID = BKCU.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.DINH_MUC_CUNG_UNG;
                            //    hd.TrangThai = BKCU.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = wsForm.txtMatKhau.Text.Trim();
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Có lỗi từ hệ thống phía hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13",ex.Message);
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        SetCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo định mức cung ứng . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        private void NhanDuLieu()
        {
            string xmlCurrent = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            WSForm wsForm = new WSForm();
            string password = "";
            try
            {
                if (dgList.GetRow() != null)
                {
                    BKCU = (BKCungUngDangKy)dgList.GetRow().DataRow;
                    {
                        sendXML.LoaiHS = "DMCU";
                        sendXML.master_id = BKCU.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_240203");
                            //MLMessages("Định mức cung ứng này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_STN03","supply norm", false);
                            return;
                        }
                        if (GlobalSettings.PassWordDT == "")
                        {
                            wsForm.ShowDialog(this);
                            if (!wsForm.IsReady) return;
                        }
                        if (GlobalSettings.PassWordDT != "")
                            password = GlobalSettings.PassWordDT;
                        else
                            password = wsForm.txtMatKhau.Text.Trim();
                        this.Cursor = Cursors.WaitCursor;
                        xmlCurrent = BKCU.WSRequest(password);
                        this.Cursor = Cursors.Default;
                        sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "DMCU";
                        sendXML.master_id = BKCU.ID;
                        sendXML.msg = xmlCurrent;
                        sendXML.func = 2;
                        xmlCurrent = "";
                        sendXML.InsertUpdate();
                        LayPhanHoi(password);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Nhận trạng thái không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    Company.GC.BLL.KDT.HangDoi hd = new Company.GC.BLL.KDT.HangDoi();
                            //    hd.ID = BKCU.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.DINH_MUC_CUNG_UNG;
                            //    hd.TrangThai = BKCU.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Có lỗi từ hệ thống phía hải quan.","MSG_WRN12","", false);
                            return;
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13",ex.Message);
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN12", ex.Message, false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu định mức cung ứng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void Huy()
        {
            WSForm wsForm = new WSForm();
            string password = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {
                if (dgList.GetRow() != null)
                {
                    BKCU = (BKCungUngDangKy)dgList.GetRow().DataRow;

                    {
                        sendXML.LoaiHS = "DMCU";
                        sendXML.master_id = BKCU.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_240203");
                            //MLMessages("Định mức cung ứng này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_STN03","supply norm", false);
                            return;
                        }
                    }

                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    string xmlCurrent = BKCU.WSCancel(password);
                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "DMCU";
                    sendXML.master_id = BKCU.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Hủy khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    Company.GC.BLL.KDT.HangDoi hd = new Company.GC.BLL.KDT.HangDoi();
                            //    hd.ID = BKCU.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.DINH_MUC_CUNG_UNG;
                            //    hd.TrangThai = BKCU.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.HUY_KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Có lỗi từ hệ thống phía hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13",ex.Message);
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", "", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        SetCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy định mức cung ứng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void LaySoTiepNhanDT()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            if (dgList.GetRow() != null)
            {
                BKCU = (BKCungUngDangKy)dgList.GetRow().DataRow;
                Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                {
                    sendXML.LoaiHS = "DMCU";
                    sendXML.master_id = BKCU.ID;
                    if (!sendXML.Load())
                    {
                        showMsg("MSG_240203");
                        //MLMessages("Định mức cung ứng này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.", "MSG_STN03", "supply norm", false);
                        return;
                    }
                }
                string xmlCurrent = "";
                WSForm wsForm = new WSForm();
                string password = "";
                try
                {
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    {
                        xmlCurrent = BKCU.LayPhanHoi(password, sendXML.msg);
                    }
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        doc.LoadXml(xmlCurrent);
                        node = doc.SelectSingleNode("Megs");
                        if (node == null)
                        {
                            if (showMsg("MSG_STN02", true) == "Yes")
                            //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                            {
                                this.Refresh();
                                LayPhanHoi(password);
                            }
                            return;
                        }
                    }

                    if (sendXML.func == 1)
                    {
                        showMsg("MSG_SEN02", BKCU.SoTiepNhan);
                        //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + BKCU.SoTiepNhan,"MSG_SEN02",BKCU.SoTiepNhan.ToString(), false);
                    }
                    else if (sendXML.func == 3)
                    {
                        showMsg("MSG_2702058");
                        //MLMessages("Đã hủy định mức cung ứng này","MSG_CAN01","supply norm", false);
                    }
                    else if (sendXML.func == 2)
                    {
                        if (BKCU.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            showMsg("MSG_SEN11", new string[] { BKCU.SoBangKe.ToString(), mess });
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số chứng từ : " + BKCU.SoBangKe.ToString(),"MSG_SEN03",BKCU.SoBangKe.ToString(), false);
                        }
                        else if (BKCU.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            showMsg("MSG_SEN04", mess);
                            //MLMessages("Hải quan chưa xử lý !","MSG_STN06","", false);
                        }                      
                    }
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                    this.btnSearch_Click(null, null);
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    //if (loaiWS == "1")
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                //if (ShowMessage("Xác nhận không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                //{
                                //    Company.GC.BLL.KDT.HangDoi hd = new Company.GC.BLL.KDT.HangDoi();
                                //    hd.ID = BKCU.ID;
                                //    hd.LoaiToKhai = LoaiToKhai.DINH_MUC_CUNG_UNG;
                                //    hd.TrangThai = BKCU.TrangThaiXuLy;
                                //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //    hd.PassWord = password;
                                //    MainForm.AddToQueueForm(hd);
                                //    MainForm.ShowQueueForm();
                                //}
                                showMsg("MSG_WRN12");
                                //MLMessages("Có lỗi từ hệ thống phía hải quan.","MSG_WRN12","", false);
                                return;
                            }
                            else
                            {
                                showMsg("MSG_2702016", msg[0]);
                                //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                                if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                                {
                                    sendXML.Delete();
                                    SetCommandStatus();
                                }
                                else
                                {
                                    GlobalSettings.PassWordDT = "";
                                }
                            }
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0],"MSG_SEN09",msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                SetCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                        #endregion FPTService
                    }

                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi xác nhận thông tin định mức cung ứng. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }
        private void LayPhanHoi(string pass)
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            BKCU = (BKCungUngDangKy)dgList.GetRow().DataRow;
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            string xmlCurrent = "";
            try
            {
                sendXML.LoaiHS = "DMCU";
                sendXML.master_id = BKCU.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = BKCU.LayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", BKCU.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + BKCU.SoTiepNhan,"MSG_SEN02",BKCU.SoTiepNhan.ToString(), false);
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702058");
                    //MLMessages("Đã hủy định mức cung ứng này","MSG_CAN01","", false);
                }
                else if (sendXML.func == 2)
                {
                    if (BKCU.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN11", new string[] { BKCU.SoBangKe.ToString(), mess });
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số chứng từ: " + BKCU.SoBangKe.ToString(), "MSG_SEN03", BKCU.SoBangKe.ToString(), false);
                    }
                    else if (BKCU.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        //showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa xử lý danh sách sản phẩm này!","MSG_SEN04","", false);
                        
                        if (mess != "")
                            ShowMessage(mess, false);
                        else
                            ShowMessage("Hải quan chưa xử lý danh sách Nguyên phụ liệu cung ứng này!", false);
                    }                   
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                this.btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Xác nhận thông tin không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    Company.GC.BLL.KDT.HangDoi hd = new Company.GC.BLL.KDT.HangDoi();
                            //    hd.ID = BKCU.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.DINH_MUC_CUNG_UNG;
                            //    hd.TrangThai = BKCU.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Có lỗi từ hệ thống phía hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0],"MSG_SEN09",msg[0], false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13",ex.Message);
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin định mức cung ứng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            search();           
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            // if (MLMessages("Bạn có muốn xóa không ?","MSG_DEL01","", true) == "Yes")
            {                
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                 int j=0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        BKCungUngDangKy bkCungUng = (BKCungUngDangKy)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "DMCU";
                        sendXML.master_id = bkCungUng.ID;
                        if (sendXML.Load())
                        {
                            j=i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                            //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Không xóa được?","MSG_STN09",j.ToString(), true);
                            //if (st == "Yes")
                            //{
                            //    if (bkCungUng.ID > 0)
                            //    {
                            //        bkCungUng.Delete();
                            //    }
                            //}
                        }
                        else
                        {
                            if (bkCungUng.ID > 0)
                            {
                                bkCungUng.Delete();
                            }
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (this.BKCungUngCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            int j = 0;
            if (items.Count < 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa không ?", true) == "Yes")
            {
                
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        BKCungUngDangKy bkCungUng = (BKCungUngDangKy)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "DMCU";
                        sendXML.master_id = bkCungUng.ID;
                        if (sendXML.Load())
                        {
                            j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                            //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Không xóa được?", "MSG_STN09", j.ToString(), true);
                            //if (st == "Yes")
                            //{
                            //    if (bkCungUng.ID > 0)
                            //    {
                            //        bkCungUng.Delete();
                            //    }
                            //}
                        }
                        else
                        {
                            if (bkCungUng.ID > 0)
                            {
                                bkCungUng.Delete();
                            }
                        }
                    }
                }
                this.search();
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            if(dgList.GetRow()==null) return;
              BKCungUngDangKy BKCU = (BKCungUngDangKy)dgList.GetRow().DataRow;
              Globals.ShowKetQuaXuLyBoSung(BKCU.GUIDSTR);
        }
	}
}