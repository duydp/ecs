namespace Company.Interface
{
    partial class ReadExcelFormVNACCS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadExcelFormVNACCS));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblXuatXu = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grbThuKhac = new Janus.Windows.EditControls.UIGroupBox();
            this.lblMaMienGiam = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblMucThue = new System.Windows.Forms.Label();
            this.txtSoTienMienGiamThueKhac4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoTienMienGiamThueKhac3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoTienMienGiamThueKhac2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSoTienMienGiamThueKhac1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueKhac1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtThueKhac2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtThueKhac3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtThueKhac4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTienMG = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaMienGiamThueKhac4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtMaMienGiamThueKhac3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtMaMienGiamThueKhac2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaMienGiamThueKhac1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaMienGiam = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.cbbSheetName = new Janus.Windows.EditControls.UIComboBox();
            this.txtXuatXuColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuongColumn2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtTriGiaTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtDVTColumn2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDVTColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuongColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblThue = new System.Windows.Forms.Label();
            this.txtThueSuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBieuThueXNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDonGiaColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.rfvDVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.rfvTenSP = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSheet = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDonGia = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoLuong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTSXNK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.requiredFieldValidator4 = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.ckTuDongTT = new Janus.Windows.EditControls.UICheckBox();
            this.ckTCVN3 = new Janus.Windows.EditControls.UICheckBox();
            this.checkRead = new Janus.Windows.EditControls.UICheckBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSelectFile = new Janus.Windows.EditControls.UIButton();
            this.lblLinkExcelTemplate = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbThuKhac)).BeginInit();
            this.grbThuKhac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDonGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTSXNK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requiredFieldValidator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(763, 564);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Excel 97-2003|*.xls";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(63, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cột mã hàng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(63, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 14);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cột tên hàng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(63, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cột mã HS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(344, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 14);
            this.label5.TabIndex = 12;
            this.label5.Text = "Cột ĐVT";
            // 
            // txtRow
            // 
            this.txtRow.DecimalDigits = 0;
            this.txtRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRow.Location = new System.Drawing.Point(156, 42);
            this.txtRow.MaxLength = 6;
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(151, 22);
            this.txtRow.TabIndex = 2;
            this.txtRow.Text = "2";
            this.txtRow.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.txtRow.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(63, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên Sheet";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(63, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 14);
            this.label7.TabIndex = 14;
            this.label7.Text = "Cột số lượng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(344, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 14);
            this.label8.TabIndex = 16;
            this.label8.Text = "Cột đơn giá";
            // 
            // lblXuatXu
            // 
            this.lblXuatXu.AutoSize = true;
            this.lblXuatXu.BackColor = System.Drawing.Color.Transparent;
            this.lblXuatXu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXuatXu.Location = new System.Drawing.Point(344, 16);
            this.lblXuatXu.Name = "lblXuatXu";
            this.lblXuatXu.Size = new System.Drawing.Size(72, 14);
            this.lblXuatXu.TabIndex = 10;
            this.lblXuatXu.Text = "Cột xuất xứ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(63, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 14);
            this.label11.TabIndex = 2;
            this.label11.Text = "Dòng bắt đầu";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.grbThuKhac);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(763, 445);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông số cấu hình";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grbThuKhac
            // 
            this.grbThuKhac.Controls.Add(this.lblMaMienGiam);
            this.grbThuKhac.Controls.Add(this.label20);
            this.grbThuKhac.Controls.Add(this.lblMucThue);
            this.grbThuKhac.Controls.Add(this.txtSoTienMienGiamThueKhac4);
            this.grbThuKhac.Controls.Add(this.label9);
            this.grbThuKhac.Controls.Add(this.txtSoTienMienGiamThueKhac3);
            this.grbThuKhac.Controls.Add(this.label13);
            this.grbThuKhac.Controls.Add(this.txtSoTienMienGiamThueKhac2);
            this.grbThuKhac.Controls.Add(this.label17);
            this.grbThuKhac.Controls.Add(this.txtSoTienMienGiamThueKhac1);
            this.grbThuKhac.Controls.Add(this.txtThueKhac1);
            this.grbThuKhac.Controls.Add(this.label19);
            this.grbThuKhac.Controls.Add(this.txtThueKhac2);
            this.grbThuKhac.Controls.Add(this.label16);
            this.grbThuKhac.Controls.Add(this.txtThueKhac3);
            this.grbThuKhac.Controls.Add(this.label12);
            this.grbThuKhac.Controls.Add(this.txtThueKhac4);
            this.grbThuKhac.Controls.Add(this.lblTienMG);
            this.grbThuKhac.Controls.Add(this.label10);
            this.grbThuKhac.Controls.Add(this.txtMaMienGiamThueKhac4);
            this.grbThuKhac.Controls.Add(this.label14);
            this.grbThuKhac.Controls.Add(this.txtMaMienGiamThueKhac3);
            this.grbThuKhac.Controls.Add(this.label18);
            this.grbThuKhac.Controls.Add(this.txtMaMienGiamThueKhac2);
            this.grbThuKhac.Controls.Add(this.txtMaMienGiamThueKhac1);
            this.grbThuKhac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbThuKhac.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbThuKhac.Location = new System.Drawing.Point(3, 249);
            this.grbThuKhac.Name = "grbThuKhac";
            this.grbThuKhac.Size = new System.Drawing.Size(757, 193);
            this.grbThuKhac.TabIndex = 33;
            this.grbThuKhac.Text = "Các thuế khác ngoài thuế XNK";
            this.grbThuKhac.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // lblMaMienGiam
            // 
            this.lblMaMienGiam.AutoSize = true;
            this.lblMaMienGiam.BackColor = System.Drawing.Color.Transparent;
            this.lblMaMienGiam.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaMienGiam.Location = new System.Drawing.Point(184, 56);
            this.lblMaMienGiam.Name = "lblMaMienGiam";
            this.lblMaMienGiam.Size = new System.Drawing.Size(151, 14);
            this.lblMaMienGiam.TabIndex = 24;
            this.lblMaMienGiam.Text = "Mã miễn giảm thuế khác 1";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(142, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(488, 14);
            this.label20.TabIndex = 22;
            this.label20.Text = "Các loại thuế khác bao gồm Thuế  VAT,Thuế  TTĐB, Thuế  BVMT, Thuế Tự Vệ, v.v....";
            // 
            // lblMucThue
            // 
            this.lblMucThue.AutoSize = true;
            this.lblMucThue.BackColor = System.Drawing.Color.Transparent;
            this.lblMucThue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMucThue.Location = new System.Drawing.Point(10, 56);
            this.lblMucThue.Name = "lblMucThue";
            this.lblMucThue.Size = new System.Drawing.Size(76, 14);
            this.lblMucThue.TabIndex = 22;
            this.lblMucThue.Text = "Thuế khác 1";
            // 
            // txtSoTienMienGiamThueKhac4
            // 
            this.txtSoTienMienGiamThueKhac4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoTienMienGiamThueKhac4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienMienGiamThueKhac4.Location = new System.Drawing.Point(608, 151);
            this.txtSoTienMienGiamThueKhac4.MaxLength = 1;
            this.txtSoTienMienGiamThueKhac4.Name = "txtSoTienMienGiamThueKhac4";
            this.txtSoTienMienGiamThueKhac4.Size = new System.Drawing.Size(78, 22);
            this.txtSoTienMienGiamThueKhac4.TabIndex = 26;
            this.txtSoTienMienGiamThueKhac4.Text = "V";
            this.txtSoTienMienGiamThueKhac4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 14);
            this.label9.TabIndex = 22;
            this.label9.Text = "Thuế khác 2";
            // 
            // txtSoTienMienGiamThueKhac3
            // 
            this.txtSoTienMienGiamThueKhac3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoTienMienGiamThueKhac3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienMienGiamThueKhac3.Location = new System.Drawing.Point(608, 118);
            this.txtSoTienMienGiamThueKhac3.MaxLength = 1;
            this.txtSoTienMienGiamThueKhac3.Name = "txtSoTienMienGiamThueKhac3";
            this.txtSoTienMienGiamThueKhac3.Size = new System.Drawing.Size(78, 22);
            this.txtSoTienMienGiamThueKhac3.TabIndex = 23;
            this.txtSoTienMienGiamThueKhac3.Text = "S";
            this.txtSoTienMienGiamThueKhac3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(10, 122);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 14);
            this.label13.TabIndex = 22;
            this.label13.Text = "Thuế khác 3";
            // 
            // txtSoTienMienGiamThueKhac2
            // 
            this.txtSoTienMienGiamThueKhac2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoTienMienGiamThueKhac2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienMienGiamThueKhac2.Location = new System.Drawing.Point(608, 85);
            this.txtSoTienMienGiamThueKhac2.MaxLength = 1;
            this.txtSoTienMienGiamThueKhac2.Name = "txtSoTienMienGiamThueKhac2";
            this.txtSoTienMienGiamThueKhac2.Size = new System.Drawing.Size(78, 22);
            this.txtSoTienMienGiamThueKhac2.TabIndex = 20;
            this.txtSoTienMienGiamThueKhac2.Text = "P";
            this.txtSoTienMienGiamThueKhac2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(10, 155);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 14);
            this.label17.TabIndex = 22;
            this.label17.Text = "Thuế khác 4";
            // 
            // txtSoTienMienGiamThueKhac1
            // 
            this.txtSoTienMienGiamThueKhac1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoTienMienGiamThueKhac1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienMienGiamThueKhac1.Location = new System.Drawing.Point(608, 52);
            this.txtSoTienMienGiamThueKhac1.MaxLength = 1;
            this.txtSoTienMienGiamThueKhac1.Name = "txtSoTienMienGiamThueKhac1";
            this.txtSoTienMienGiamThueKhac1.Size = new System.Drawing.Size(78, 22);
            this.txtSoTienMienGiamThueKhac1.TabIndex = 17;
            this.txtSoTienMienGiamThueKhac1.Text = "M";
            this.txtSoTienMienGiamThueKhac1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueKhac1
            // 
            this.txtThueKhac1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueKhac1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueKhac1.Location = new System.Drawing.Point(91, 52);
            this.txtThueKhac1.MaxLength = 1;
            this.txtThueKhac1.Name = "txtThueKhac1";
            this.txtThueKhac1.Size = new System.Drawing.Size(78, 22);
            this.txtThueKhac1.TabIndex = 15;
            this.txtThueKhac1.Text = "K";
            this.txtThueKhac1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(433, 155);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(175, 14);
            this.label19.TabIndex = 26;
            this.label19.Text = "Số tiền miễn giảm thuế khác 4";
            // 
            // txtThueKhac2
            // 
            this.txtThueKhac2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueKhac2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueKhac2.Location = new System.Drawing.Point(91, 85);
            this.txtThueKhac2.MaxLength = 1;
            this.txtThueKhac2.Name = "txtThueKhac2";
            this.txtThueKhac2.Size = new System.Drawing.Size(78, 22);
            this.txtThueKhac2.TabIndex = 18;
            this.txtThueKhac2.Text = "N";
            this.txtThueKhac2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(433, 122);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(175, 14);
            this.label16.TabIndex = 26;
            this.label16.Text = "Số tiền miễn giảm thuế khác 3";
            // 
            // txtThueKhac3
            // 
            this.txtThueKhac3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueKhac3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueKhac3.Location = new System.Drawing.Point(91, 118);
            this.txtThueKhac3.MaxLength = 1;
            this.txtThueKhac3.Name = "txtThueKhac3";
            this.txtThueKhac3.Size = new System.Drawing.Size(78, 22);
            this.txtThueKhac3.TabIndex = 21;
            this.txtThueKhac3.Text = "Q";
            this.txtThueKhac3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(433, 89);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(175, 14);
            this.label12.TabIndex = 26;
            this.label12.Text = "Số tiền miễn giảm thuế khác 2";
            // 
            // txtThueKhac4
            // 
            this.txtThueKhac4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueKhac4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueKhac4.Location = new System.Drawing.Point(91, 151);
            this.txtThueKhac4.MaxLength = 1;
            this.txtThueKhac4.Name = "txtThueKhac4";
            this.txtThueKhac4.Size = new System.Drawing.Size(78, 22);
            this.txtThueKhac4.TabIndex = 24;
            this.txtThueKhac4.Text = "T";
            this.txtThueKhac4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTienMG
            // 
            this.lblTienMG.AutoSize = true;
            this.lblTienMG.BackColor = System.Drawing.Color.Transparent;
            this.lblTienMG.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTienMG.Location = new System.Drawing.Point(433, 56);
            this.lblTienMG.Name = "lblTienMG";
            this.lblTienMG.Size = new System.Drawing.Size(175, 14);
            this.lblTienMG.TabIndex = 26;
            this.lblTienMG.Text = "Số tiền miễn giảm thuế khác 1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(184, 89);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(151, 14);
            this.label10.TabIndex = 24;
            this.label10.Text = "Mã miễn giảm thuế khác 2";
            // 
            // txtMaMienGiamThueKhac4
            // 
            this.txtMaMienGiamThueKhac4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaMienGiamThueKhac4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaMienGiamThueKhac4.Location = new System.Drawing.Point(340, 151);
            this.txtMaMienGiamThueKhac4.MaxLength = 1;
            this.txtMaMienGiamThueKhac4.Name = "txtMaMienGiamThueKhac4";
            this.txtMaMienGiamThueKhac4.Size = new System.Drawing.Size(78, 22);
            this.txtMaMienGiamThueKhac4.TabIndex = 25;
            this.txtMaMienGiamThueKhac4.Text = "U";
            this.txtMaMienGiamThueKhac4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(184, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(151, 14);
            this.label14.TabIndex = 24;
            this.label14.Text = "Mã miễn giảm thuế khác 3";
            // 
            // txtMaMienGiamThueKhac3
            // 
            this.txtMaMienGiamThueKhac3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaMienGiamThueKhac3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaMienGiamThueKhac3.Location = new System.Drawing.Point(340, 118);
            this.txtMaMienGiamThueKhac3.MaxLength = 1;
            this.txtMaMienGiamThueKhac3.Name = "txtMaMienGiamThueKhac3";
            this.txtMaMienGiamThueKhac3.Size = new System.Drawing.Size(78, 22);
            this.txtMaMienGiamThueKhac3.TabIndex = 22;
            this.txtMaMienGiamThueKhac3.Text = "R";
            this.txtMaMienGiamThueKhac3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(184, 155);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(151, 14);
            this.label18.TabIndex = 24;
            this.label18.Text = "Mã miễn giảm thuế khác 4";
            // 
            // txtMaMienGiamThueKhac2
            // 
            this.txtMaMienGiamThueKhac2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaMienGiamThueKhac2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaMienGiamThueKhac2.Location = new System.Drawing.Point(340, 85);
            this.txtMaMienGiamThueKhac2.MaxLength = 1;
            this.txtMaMienGiamThueKhac2.Name = "txtMaMienGiamThueKhac2";
            this.txtMaMienGiamThueKhac2.Size = new System.Drawing.Size(78, 22);
            this.txtMaMienGiamThueKhac2.TabIndex = 19;
            this.txtMaMienGiamThueKhac2.Text = "O";
            this.txtMaMienGiamThueKhac2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaMienGiamThueKhac1
            // 
            this.txtMaMienGiamThueKhac1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaMienGiamThueKhac1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaMienGiamThueKhac1.Location = new System.Drawing.Point(340, 52);
            this.txtMaMienGiamThueKhac1.MaxLength = 1;
            this.txtMaMienGiamThueKhac1.Name = "txtMaMienGiamThueKhac1";
            this.txtMaMienGiamThueKhac1.Size = new System.Drawing.Size(78, 22);
            this.txtMaMienGiamThueKhac1.TabIndex = 16;
            this.txtMaMienGiamThueKhac1.Text = "L";
            this.txtMaMienGiamThueKhac1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.ctrMaMienGiam);
            this.uiGroupBox4.Controls.Add(this.cbbSheetName);
            this.uiGroupBox4.Controls.Add(this.txtXuatXuColumn);
            this.uiGroupBox4.Controls.Add(this.txtSoLuongColumn2);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label22);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtRow);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.txtTriGiaTT);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.label21);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.label11);
            this.uiGroupBox4.Controls.Add(this.txtDVTColumn2);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Controls.Add(this.txtDVTColumn);
            this.uiGroupBox4.Controls.Add(this.lblXuatXu);
            this.uiGroupBox4.Controls.Add(this.txtSoLuongColumn);
            this.uiGroupBox4.Controls.Add(this.lblThue);
            this.uiGroupBox4.Controls.Add(this.txtThueSuat);
            this.uiGroupBox4.Controls.Add(this.label15);
            this.uiGroupBox4.Controls.Add(this.txtBieuThueXNK);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.txtDonGiaColumn);
            this.uiGroupBox4.Controls.Add(this.txtMaHangColumn);
            this.uiGroupBox4.Controls.Add(this.txtMaHSColumn);
            this.uiGroupBox4.Controls.Add(this.txtTenHangColumn);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 18);
            this.uiGroupBox4.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(757, 231);
            this.uiGroupBox4.TabIndex = 32;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ctrMaMienGiam
            // 
            this.ctrMaMienGiam.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaMienGiam.Appearance.Options.UseBackColor = true;
            this.ctrMaMienGiam.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A520;
            this.ctrMaMienGiam.Code = "";
            this.ctrMaMienGiam.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaMienGiam.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaMienGiam.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaMienGiam.IsOnlyWarning = false;
            this.ctrMaMienGiam.IsValidate = true;
            this.ctrMaMienGiam.Location = new System.Drawing.Point(620, 197);
            this.ctrMaMienGiam.Name = "ctrMaMienGiam";
            this.ctrMaMienGiam.Name_VN = "";
            this.ctrMaMienGiam.SetOnlyWarning = false;
            this.ctrMaMienGiam.SetValidate = false;
            this.ctrMaMienGiam.ShowColumnCode = true;
            this.ctrMaMienGiam.ShowColumnName = false;
            this.ctrMaMienGiam.Size = new System.Drawing.Size(116, 21);
            this.ctrMaMienGiam.TabIndex = 32;
            this.ctrMaMienGiam.TagName = "";
            this.ctrMaMienGiam.Where = null;
            this.ctrMaMienGiam.WhereCondition = "";
            // 
            // cbbSheetName
            // 
            this.cbbSheetName.DisplayMember = "Name";
            this.cbbSheetName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSheetName.Location = new System.Drawing.Point(156, 11);
            this.cbbSheetName.Name = "cbbSheetName";
            this.cbbSheetName.Size = new System.Drawing.Size(151, 22);
            this.cbbSheetName.TabIndex = 31;
            this.cbbSheetName.Tag = "PhuongThucThanhToan";
            this.cbbSheetName.ValueMember = "ID";
            this.cbbSheetName.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtXuatXuColumn
            // 
            this.txtXuatXuColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtXuatXuColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXuatXuColumn.Location = new System.Drawing.Point(458, 12);
            this.txtXuatXuColumn.MaxLength = 1;
            this.txtXuatXuColumn.Name = "txtXuatXuColumn";
            this.txtXuatXuColumn.Size = new System.Drawing.Size(149, 22);
            this.txtXuatXuColumn.TabIndex = 8;
            this.txtXuatXuColumn.Text = "E";
            this.txtXuatXuColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtXuatXuColumn.TextChanged += new System.EventHandler(this.txtXuatXuColumn_TextChanged);
            // 
            // txtSoLuongColumn2
            // 
            this.txtSoLuongColumn2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongColumn2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongColumn2.Location = new System.Drawing.Point(157, 197);
            this.txtSoLuongColumn2.MaxLength = 1;
            this.txtSoLuongColumn2.Name = "txtSoLuongColumn2";
            this.txtSoLuongColumn2.Size = new System.Drawing.Size(150, 22);
            this.txtSoLuongColumn2.TabIndex = 7;
            this.txtSoLuongColumn2.Text = "D";
            this.txtSoLuongColumn2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(63, 201);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 14);
            this.label22.TabIndex = 30;
            this.label22.Text = "Cột số lượng 2";
            // 
            // txtTriGiaTT
            // 
            this.txtTriGiaTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTriGiaTT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTT.Location = new System.Drawing.Point(458, 73);
            this.txtTriGiaTT.MaxLength = 1;
            this.txtTriGiaTT.Name = "txtTriGiaTT";
            this.txtTriGiaTT.Size = new System.Drawing.Size(149, 22);
            this.txtTriGiaTT.TabIndex = 10;
            this.txtTriGiaTT.Text = "H";
            this.txtTriGiaTT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(344, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 14);
            this.label1.TabIndex = 18;
            this.label1.Text = "Cột trị giá";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(617, 170);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(137, 14);
            this.label21.TabIndex = 12;
            this.label21.Text = "Mã miễn giảm thuế XNK";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(344, 201);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 14);
            this.label23.TabIndex = 12;
            this.label23.Text = "Cột ĐVT2";
            // 
            // txtDVTColumn2
            // 
            this.txtDVTColumn2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTColumn2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTColumn2.Location = new System.Drawing.Point(458, 197);
            this.txtDVTColumn2.MaxLength = 1;
            this.txtDVTColumn2.Name = "txtDVTColumn2";
            this.txtDVTColumn2.Size = new System.Drawing.Size(149, 22);
            this.txtDVTColumn2.TabIndex = 14;
            this.txtDVTColumn2.Text = "F";
            this.txtDVTColumn2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDVTColumn2.TextChanged += new System.EventHandler(this.editBox8_TextChanged);
            // 
            // txtDVTColumn
            // 
            this.txtDVTColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTColumn.Location = new System.Drawing.Point(458, 166);
            this.txtDVTColumn.MaxLength = 1;
            this.txtDVTColumn.Name = "txtDVTColumn";
            this.txtDVTColumn.Size = new System.Drawing.Size(149, 22);
            this.txtDVTColumn.TabIndex = 13;
            this.txtDVTColumn.Text = "F";
            this.txtDVTColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDVTColumn.TextChanged += new System.EventHandler(this.editBox8_TextChanged);
            // 
            // txtSoLuongColumn
            // 
            this.txtSoLuongColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongColumn.Location = new System.Drawing.Point(157, 166);
            this.txtSoLuongColumn.MaxLength = 1;
            this.txtSoLuongColumn.Name = "txtSoLuongColumn";
            this.txtSoLuongColumn.Size = new System.Drawing.Size(149, 22);
            this.txtSoLuongColumn.TabIndex = 6;
            this.txtSoLuongColumn.Text = "D";
            this.txtSoLuongColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblThue
            // 
            this.lblThue.AutoSize = true;
            this.lblThue.BackColor = System.Drawing.Color.Transparent;
            this.lblThue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThue.Location = new System.Drawing.Point(344, 139);
            this.lblThue.Name = "lblThue";
            this.lblThue.Size = new System.Drawing.Size(109, 14);
            this.lblThue.TabIndex = 20;
            this.lblThue.Text = "Cột biểu thuế XNK";
            // 
            // txtThueSuat
            // 
            this.txtThueSuat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueSuat.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuat.Location = new System.Drawing.Point(458, 135);
            this.txtThueSuat.MaxLength = 1;
            this.txtThueSuat.Name = "txtThueSuat";
            this.txtThueSuat.Size = new System.Drawing.Size(149, 22);
            this.txtThueSuat.TabIndex = 12;
            this.txtThueSuat.Text = "J";
            this.txtThueSuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(344, 108);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 14);
            this.label15.TabIndex = 20;
            this.label15.Text = "Cột thuế suât";
            // 
            // txtBieuThueXNK
            // 
            this.txtBieuThueXNK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBieuThueXNK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBieuThueXNK.Location = new System.Drawing.Point(458, 104);
            this.txtBieuThueXNK.MaxLength = 1;
            this.txtBieuThueXNK.Name = "txtBieuThueXNK";
            this.txtBieuThueXNK.Size = new System.Drawing.Size(149, 22);
            this.txtBieuThueXNK.TabIndex = 11;
            this.txtBieuThueXNK.Text = "I";
            this.txtBieuThueXNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDonGiaColumn
            // 
            this.txtDonGiaColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonGiaColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaColumn.Location = new System.Drawing.Point(458, 42);
            this.txtDonGiaColumn.MaxLength = 1;
            this.txtDonGiaColumn.Name = "txtDonGiaColumn";
            this.txtDonGiaColumn.Size = new System.Drawing.Size(149, 22);
            this.txtDonGiaColumn.TabIndex = 9;
            this.txtDonGiaColumn.Text = "G";
            this.txtDonGiaColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHangColumn
            // 
            this.txtMaHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHangColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangColumn.Location = new System.Drawing.Point(156, 73);
            this.txtMaHangColumn.MaxLength = 1;
            this.txtMaHangColumn.Name = "txtMaHangColumn";
            this.txtMaHangColumn.Size = new System.Drawing.Size(151, 22);
            this.txtMaHangColumn.TabIndex = 3;
            this.txtMaHangColumn.Text = "A";
            this.txtMaHangColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHSColumn
            // 
            this.txtMaHSColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHSColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSColumn.Location = new System.Drawing.Point(157, 135);
            this.txtMaHSColumn.MaxLength = 1;
            this.txtMaHSColumn.Name = "txtMaHSColumn";
            this.txtMaHSColumn.Size = new System.Drawing.Size(149, 22);
            this.txtMaHSColumn.TabIndex = 5;
            this.txtMaHSColumn.Text = "C";
            this.txtMaHSColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenHangColumn
            // 
            this.txtTenHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangColumn.Location = new System.Drawing.Point(156, 104);
            this.txtTenHangColumn.MaxLength = 1;
            this.txtTenHangColumn.Name = "txtTenHangColumn";
            this.txtTenHangColumn.Size = new System.Drawing.Size(151, 22);
            this.txtTenHangColumn.TabIndex = 4;
            this.txtTenHangColumn.Text = "B";
            this.txtTenHangColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(392, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(68, 23);
            this.btnClose.TabIndex = 34;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSave.Location = new System.Drawing.Point(318, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(68, 23);
            this.btnSave.TabIndex = 33;
            this.btnSave.Text = "Ghi";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // rfvDVT
            // 
            this.rfvDVT.ControlToValidate = this.txtDVTColumn;
            this.rfvDVT.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvDVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDVT.Icon")));
            this.rfvDVT.Tag = "rfvDVT";
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "Chưa chọn file Excel cần đọc";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            this.rfvFilePath.Tag = "rfvFilePath";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFilePath.Location = new System.Drawing.Point(13, 19);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(572, 22);
            this.txtFilePath.TabIndex = 27;
            this.txtFilePath.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // rfvTenSP
            // 
            this.rfvTenSP.ControlToValidate = this.txtTenHangColumn;
            this.rfvTenSP.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvTenSP.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSP.Icon")));
            this.rfvTenSP.Tag = "rfvTenSP";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMaHSColumn;
            this.rfvMaHS.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // cvError
            // 
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            this.error.Icon = ((System.Drawing.Icon)(resources.GetObject("error.Icon")));
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaHangColumn;
            this.rfvMa.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvTenSheet
            // 
            this.rfvTenSheet.ControlToValidate = this.cbbSheetName;
            this.rfvTenSheet.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvTenSheet.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSheet.Icon")));
            this.rfvTenSheet.Tag = "rfvTenSheet";
            // 
            // rfvDonGia
            // 
            this.rfvDonGia.ControlToValidate = this.txtDonGiaColumn;
            this.rfvDonGia.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvDonGia.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDonGia.Icon")));
            this.rfvDonGia.Tag = "rfvDonGia";
            // 
            // rfvSoLuong
            // 
            this.rfvSoLuong.ControlToValidate = this.txtSoLuongColumn;
            this.rfvSoLuong.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoLuong.Icon")));
            this.rfvSoLuong.Tag = "rfvSoLuong";
            // 
            // rfvTSXNK
            // 
            this.rfvTSXNK.ControlToValidate = this.txtBieuThueXNK;
            this.rfvTSXNK.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvTSXNK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTSXNK.Icon")));
            this.rfvTSXNK.Tag = "rfvTSXNK";
            // 
            // requiredFieldValidator4
            // 
            this.requiredFieldValidator4.ControlToValidate = this.txtMaHangColumn;
            this.requiredFieldValidator4.ErrorMessage = "Thông tin này  không được để trống";
            this.requiredFieldValidator4.Icon = ((System.Drawing.Icon)(resources.GetObject("requiredFieldValidator4.Icon")));
            this.requiredFieldValidator4.Tag = "requiredFieldValidator4";
            // 
            // ckTuDongTT
            // 
            this.ckTuDongTT.BackColor = System.Drawing.Color.Transparent;
            this.ckTuDongTT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckTuDongTT.ForeColor = System.Drawing.Color.Red;
            this.ckTuDongTT.Location = new System.Drawing.Point(248, 46);
            this.ckTuDongTT.Name = "ckTuDongTT";
            this.ckTuDongTT.Size = new System.Drawing.Size(131, 23);
            this.ckTuDongTT.TabIndex = 31;
            this.ckTuDongTT.Text = "Tự động tính thuế";
            this.ckTuDongTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // ckTCVN3
            // 
            this.ckTCVN3.BackColor = System.Drawing.Color.Transparent;
            this.ckTCVN3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckTCVN3.ForeColor = System.Drawing.Color.Red;
            this.ckTCVN3.Location = new System.Drawing.Point(148, 46);
            this.ckTCVN3.Name = "ckTCVN3";
            this.ckTCVN3.Size = new System.Drawing.Size(94, 23);
            this.ckTCVN3.TabIndex = 30;
            this.ckTCVN3.Text = "Font TCVN3";
            this.ckTCVN3.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // checkRead
            // 
            this.checkRead.BackColor = System.Drawing.Color.Transparent;
            this.checkRead.Checked = true;
            this.checkRead.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkRead.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkRead.ForeColor = System.Drawing.Color.Red;
            this.checkRead.Location = new System.Drawing.Point(385, 46);
            this.checkRead.Name = "checkRead";
            this.checkRead.Size = new System.Drawing.Size(354, 23);
            this.checkRead.TabIndex = 32;
            this.checkRead.Text = "Tự động Nhập Thông tin hàng hóa theo Mã hàng hóa";
            this.checkRead.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnSave);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 523);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(763, 41);
            this.uiGroupBox3.TabIndex = 279;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnSelectFile);
            this.uiGroupBox2.Controls.Add(this.checkRead);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Controls.Add(this.lblLinkExcelTemplate);
            this.uiGroupBox2.Controls.Add(this.ckTCVN3);
            this.uiGroupBox2.Controls.Add(this.ckTuDongTT);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 445);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(763, 78);
            this.uiGroupBox2.TabIndex = 280;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFile.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectFile.Image")));
            this.btnSelectFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSelectFile.Location = new System.Drawing.Point(591, 18);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(96, 22);
            this.btnSelectFile.TabIndex = 28;
            this.btnSelectFile.Text = "Chọn file";
            this.btnSelectFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // lblLinkExcelTemplate
            // 
            this.lblLinkExcelTemplate.AutoSize = true;
            this.lblLinkExcelTemplate.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkExcelTemplate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkExcelTemplate.Location = new System.Drawing.Point(10, 50);
            this.lblLinkExcelTemplate.Name = "lblLinkExcelTemplate";
            this.lblLinkExcelTemplate.Size = new System.Drawing.Size(123, 14);
            this.lblLinkExcelTemplate.TabIndex = 29;
            this.lblLinkExcelTemplate.TabStop = true;
            this.lblLinkExcelTemplate.Text = "Mở tệp tin mẫu Excel";
            this.lblLinkExcelTemplate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLinkExcelTemplate_LinkClicked);
            // 
            // ReadExcelFormVNACCS
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(763, 564);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReadExcelFormVNACCS";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Đọc danh sách hàng hóa từ Excel";
            this.Load += new System.EventHandler(this.ReadExcelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbThuKhac)).EndInit();
            this.grbThuKhac.ResumeLayout(false);
            this.grbThuKhac.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDonGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTSXNK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requiredFieldValidator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label lblXuatXu;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtXuatXuColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonGiaColumn;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.GridEX.EditControls.EditBox txtBieuThueXNK;
        private System.Windows.Forms.Label lblThue;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDVT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSP;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDonGia;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoLuong;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTSXNK;
        private Company.Controls.CustomValidation.RequiredFieldValidator requiredFieldValidator4;
        private Janus.Windows.GridEX.EditControls.EditBox txtTriGiaTT;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UICheckBox ckTuDongTT;
        private Janus.Windows.EditControls.UICheckBox ckTCVN3;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueSuat;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongColumn2;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTColumn2;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.EditControls.UICheckBox checkRead;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox grbThuKhac;
        private System.Windows.Forms.Label lblMaMienGiam;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblMucThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTienMienGiamThueKhac4;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTienMienGiamThueKhac3;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTienMienGiamThueKhac2;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTienMienGiamThueKhac1;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueKhac1;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueKhac2;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueKhac3;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueKhac4;
        private System.Windows.Forms.Label lblTienMG;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMienGiamThueKhac4;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMienGiamThueKhac3;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMienGiamThueKhac2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMienGiamThueKhac1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnSelectFile;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private System.Windows.Forms.LinkLabel lblLinkExcelTemplate;
        private Janus.Windows.EditControls.UIComboBox cbbSheetName;
        private System.Windows.Forms.Label label21;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaMienGiam;
    }
}