using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.BLL.SXXK.ThanhKhoan
{
    public partial class NPLXGC : EntityBase
    {
        #region Private members.

        protected int _LanThanhLy;
        protected string _MaDoanhNghiep = String.Empty;
        protected int _SoToKhai;
        protected string _MaLoaiHinh = String.Empty;
        protected short _NamDangKy;
        protected string _MaHaiQuan = String.Empty;
        protected string _MaNPL = String.Empty;
        protected int _SoToKhaiXuat;
        protected string _MaLoaiHinhXuat = String.Empty;
        protected short _NamDangKyXuat;
        protected string _MaHaiQuanXuat = String.Empty;
        protected decimal _SoLuongXuat;
        protected bool _DaXuLy;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public int LanThanhLy
        {
            set { this._LanThanhLy = value; }
            get { return this._LanThanhLy; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }
        public int SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public short NamDangKy
        {
            set { this._NamDangKy = value; }
            get { return this._NamDangKy; }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value; }
            get { return this._MaHaiQuan; }
        }
        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }
        public int SoToKhaiXuat
        {
            set { this._SoToKhaiXuat = value; }
            get { return this._SoToKhaiXuat; }
        }
        public string MaLoaiHinhXuat
        {
            set { this._MaLoaiHinhXuat = value; }
            get { return this._MaLoaiHinhXuat; }
        }
        public short NamDangKyXuat
        {
            set { this._NamDangKyXuat = value; }
            get { return this._NamDangKyXuat; }
        }
        public string MaHaiQuanXuat
        {
            set { this._MaHaiQuanXuat = value; }
            get { return this._MaHaiQuanXuat; }
        }
        public decimal SoLuongXuat
        {
            set { this._SoLuongXuat = value; }
            get { return this._SoLuongXuat; }
        }
        public bool DaXuLy
        {
            set { this._DaXuLy = value; }
            get { return this._DaXuLy; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            this.db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) this._SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) this._MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) this._NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) this._MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) this._SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) this._DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
                return true;
            }
            return false;
        }

        //---------------------------------------------------------------------------------------------

    
        //---------------------------------------------------------------------------------------------
        public NPLXGCCollection SelectCollectionBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan()
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            NPLXGCCollection collection = new NPLXGCCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NPLXGC entity = new NPLXGC();
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------
        public DataSet SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan()
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public NPLXGCCollection SelectCollectionAll()
        {
            NPLXGCCollection collection = new NPLXGCCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                NPLXGC entity = new NPLXGC();

                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public NPLXGCCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            NPLXGCCollection collection = new NPLXGCCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                NPLXGC entity = new NPLXGC();

                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            this.db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);
            this.db.AddInParameter(dbCommand, "@SoLuongXuat", SqlDbType.Decimal, this._SoLuongXuat);
            this.db.AddInParameter(dbCommand, "@DaXuLy", SqlDbType.Bit, this._DaXuLy);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(NPLXGCCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLXGC item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, NPLXGCCollection collection)
        {
            foreach (NPLXGC item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            this.db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);
            this.db.AddInParameter(dbCommand, "@SoLuongXuat", SqlDbType.Decimal, this._SoLuongXuat);
            this.db.AddInParameter(dbCommand, "@DaXuLy", SqlDbType.Bit, this._DaXuLy);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(NPLXGCCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLXGC item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            this.db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);
            this.db.AddInParameter(dbCommand, "@SoLuongXuat", SqlDbType.Decimal, this._SoLuongXuat);
            this.db.AddInParameter(dbCommand, "@DaXuLy", SqlDbType.Bit, this._DaXuLy);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(NPLXGCCollection collection, SqlTransaction transaction)
        {
            foreach (NPLXGC item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLXGC_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            this.db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(NPLXGCCollection collection, SqlTransaction transaction)
        {
            foreach (NPLXGC item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(NPLXGCCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLXGC item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion
        public static void updateDatabase(DataSet ds, SqlTransaction transaction)
        {
            int i = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NPLXGC npl = new NPLXGC();
                npl._DaXuLy = Convert.ToBoolean(row["DA_XL"].ToString());
                npl._LanThanhLy = Convert.ToInt32(row["LAN_TL"].ToString());
                npl._MaDoanhNghiep = (row["MA_DV"].ToString());
                npl._MaHaiQuan = (row["MA_HQ"].ToString());
                npl._MaHaiQuanXuat = (row["MA_HQX"].ToString());
                npl._MaLoaiHinh = (row["MA_LH"].ToString());
                npl._MaLoaiHinhXuat = (row["MA_LHX"].ToString());
                npl._MaNPL = (row["MA_NPL"].ToString());
                npl._NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                npl._NamDangKyXuat = Convert.ToInt16(row["NAMDKX"].ToString());
                npl._SoLuongXuat = Convert.ToDecimal(row["LUONG_XGC"]);
                npl._SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                npl._SoToKhaiXuat = Convert.ToInt32(row["SOTKX"].ToString());
                npl.InsertUpdateTransaction(transaction);
            }
        }
    }
}
  
        
        
