using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.KDT.GC
{
    public partial class ChonSPCungUngForm : BaseForm
    {
        public ToKhaiMauDich TKMD = null;
        public ToKhaiChuyenTiep TKCT = null;
        public HangChuyenTiep HCTSelect = new HangChuyenTiep();
        public HangMauDich HMDSelect = new HangMauDich();
        public BKCungUngDangKy BKCU = new BKCungUngDangKy();
        public ChonSPCungUngForm()
        {
            InitializeComponent();
        }

        private void ChonSPCungUngForm_Load(object sender, EventArgs e)
        {
            //this.BKCU.LoadSanPhamCungUngCollection();
            if (TKMD != null)
            {
                dgList.RootTable.Columns["MaHang"].Visible = false;
                dgList.RootTable.Columns["ID_NuocXX"].Visible = false;
                dgList.RootTable.Columns["ID_DVT"].Visible = false;
                this.TKMD.LoadHMDCollection();
                HangMauDichCollection HMDCollection = new HangMauDichCollection();
                foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                {
                    foreach (SanPhanCungUng spCU in this.BKCU.SanPhamCungUngCollection)
                    {
                        if (hmd.MaPhu.Trim().ToLower() == spCU.MaSanPham.Trim().ToLower())
                        {
                            HMDCollection.Add(hmd);
                            break;
                        }

                    }
                }
                foreach (HangMauDich hmd in HMDCollection)
                    this.TKMD.HMDCollection.Remove(hmd);
                dgList.DataSource = this.TKMD.HMDCollection;
            }
            else
            {
                dgList.RootTable.Columns["MaPhu"].Visible = false;
                dgList.RootTable.Columns["NuocXX_ID"].Visible = false;
                dgList.RootTable.Columns["DVT_ID"].Visible = false;
                this.TKCT.LoadHCTCollection();
                HangChuyenTiepCollection HCTCollection = new HangChuyenTiepCollection();
                foreach (HangChuyenTiep hct in this.TKCT.HCTCollection)
                {
                    foreach (SanPhanCungUng spCU in this.BKCU.SanPhamCungUngCollection)
                    {
                        if (hct.MaHang.Trim().ToLower() == spCU.MaSanPham.Trim().ToLower())
                        {
                            HCTCollection.Add(hct);
                            break;
                        }

                    }
                }
                foreach (HangChuyenTiep hct in HCTCollection)
                    this.TKCT.HCTCollection.Remove(hct);
                dgList.DataSource = TKCT.HCTCollection;
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (TKMD != null)
                {
                    this.HMDSelect = (HangMauDich)e.Row.DataRow;
                }
                else
                    this.HCTSelect = (HangChuyenTiep)e.Row.DataRow;
                this.Close();
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (TKMD != null)
                {
                    e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
                    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
                }
                else
                {
                    e.Row.Cells["ID_NuocXX"].Text = this.Nuoc_GetName(e.Row.Cells["ID_NuocXX"].Value.ToString());
                    e.Row.Cells["ID_DVT"].Text = this.DonViTinh_GetName(e.Row.Cells["ID_DVT"].Text);
                }
            }
            catch
            {               
            }

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.HMDSelect = null;
            this.HCTSelect = null;
            this.Close();
        }
    }
}

