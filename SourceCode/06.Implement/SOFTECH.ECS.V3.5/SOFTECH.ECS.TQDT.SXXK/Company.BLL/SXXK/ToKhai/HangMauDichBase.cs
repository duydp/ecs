﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Company.BLL.SXXK.ToKhai
{
    public partial class HangMauDich : EntityBase
    {
        #region Private members.

        protected int _SoToKhai;
        protected string _MaLoaiHinh = String.Empty;
        protected string _MaHaiQuan = String.Empty;
        protected short _NamDangKy;
        protected short _SoThuTuHang;
        protected string _MaHS = String.Empty;
        protected string _MaPhu = String.Empty;
        protected string _TenHang = String.Empty;
        protected string _NuocXX_ID = String.Empty;
        protected string _DVT_ID = String.Empty;
        protected decimal _SoLuong;
        protected decimal _DonGiaKB;
        protected decimal _DonGiaTT;
        protected decimal _TriGiaKB;
        protected decimal _TriGiaTT;
        protected decimal _TriGiaKB_VND;
        protected decimal _ThueSuatXNK;
        protected decimal _ThueSuatTTDB;
        protected decimal _ThueSuatGTGT;
        protected decimal _ThueXNK;
        protected decimal _ThueTTDB;
        protected decimal _ThueGTGT;
        protected decimal _PhuThu;
        protected decimal _TyLeThuKhac;
        protected decimal _TriGiaThuKhac;
        protected byte _MienThue;
        protected decimal _Ton;
        protected int _IsExsit;
        protected string _NhomHang = string.Empty;
        protected string _MaTMP = string.Empty;
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.
        public string MaTMP
        {
            set { this._MaTMP = value; }
            get { return this._MaTMP; }
        }
        public int SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value; }
            get { return this._MaHaiQuan; }
        }
        public short NamDangKy
        {
            set { this._NamDangKy = value; }
            get { return this._NamDangKy; }
        }
        public short SoThuTuHang
        {
            set { this._SoThuTuHang = value; }
            get { return this._SoThuTuHang; }
        }
        public string MaHS
        {
            set { this._MaHS = value; }
            get { return this._MaHS; }
        }
        public string MaPhu
        {
            set { this._MaPhu = value; }
            get { return this._MaPhu; }
        }
        public string TenHang
        {
            set { this._TenHang = value; }
            get { return this._TenHang; }
        }
        public string NuocXX_ID
        {
            set { this._NuocXX_ID = value; }
            get { return this._NuocXX_ID; }
        }
        public string DVT_ID
        {
            set { this._DVT_ID = value; }
            get { return this._DVT_ID; }
        }
        public decimal SoLuong
        {
            set { this._SoLuong = value; }
            get { return this._SoLuong; }
        }
        public decimal DonGiaKB
        {
            set { this._DonGiaKB = value; }
            get { return this._DonGiaKB; }
        }
        public decimal DonGiaTT
        {
            set { this._DonGiaTT = value; }
            get { return this._DonGiaTT; }
        }
        public decimal TriGiaKB
        {
            set { this._TriGiaKB = value; }
            get { return this._TriGiaKB; }
        }
        public decimal TriGiaTT
        {
            set { this._TriGiaTT = value; }
            get { return this._TriGiaTT; }
        }
        public decimal TriGiaKB_VND
        {
            set { this._TriGiaKB_VND = value; }
            get { return this._TriGiaKB_VND; }
        }
        public decimal ThueSuatXNK
        {
            set { this._ThueSuatXNK = value; }
            get { return this._ThueSuatXNK; }
        }
        public decimal ThueSuatTTDB
        {
            set { this._ThueSuatTTDB = value; }
            get { return this._ThueSuatTTDB; }
        }
        public decimal ThueSuatGTGT
        {
            set { this._ThueSuatGTGT = value; }
            get { return this._ThueSuatGTGT; }
        }
        public decimal ThueXNK
        {
            set { this._ThueXNK = value; }
            get { return this._ThueXNK; }
        }
        public decimal ThueTTDB
        {
            set { this._ThueTTDB = value; }
            get { return this._ThueTTDB; }
        }
        public decimal ThueGTGT
        {
            set { this._ThueGTGT = value; }
            get { return this._ThueGTGT; }
        }
        public decimal PhuThu
        {
            set { this._PhuThu = value; }
            get { return this._PhuThu; }
        }
        public decimal TyLeThuKhac
        {
            set { this._TyLeThuKhac = value; }
            get { return this._TyLeThuKhac; }
        }
        public decimal TriGiaThuKhac
        {
            set { this._TriGiaThuKhac = value; }
            get { return this._TriGiaThuKhac; }
        }
        public byte MienThue
        {
            set { this._MienThue = value; }
            get { return this._MienThue; }
        }

        public decimal Ton
        {
            set { this._Ton = value; }
            get { return this._Ton; }
        }
        //---------------------------------------------------------------------------------------------

        public int IsExist
        {
            set { this._IsExsit = value; }
            get { return this._IsExsit; }
        }
        public string NhomHang
        {
            set { this._NhomHang = value; }
            get { return this._NhomHang; }
        }
        #endregion


        //---------------------------------------------------------------------------------------------
        public void TinhThue(decimal tygiaTT)
        {
            //decimal dongia_TT = this._DonGiaKB * tygiaTT;
            //decimal trigiaNT = this._DonGiaKB * this._SoLuong;
            ////decimal trigiaTT_XNK = (dongia_TT*this._SoLuong) + (this._ASEAN_KhoanPhaiCong*tygiaTT) - (this._ASEAN_KhoanPhaiTru*tygiaTT);
            //decimal trigiaTT_XNK = (dongia_TT * this._SoLuong);
            //decimal tienthue_XNK = trigiaTT_XNK * this._ThueSuatXNK / 100;
            //decimal tienthue_TTDB = (trigiaTT_XNK + tienthue_XNK) * this._ThueSuatTTDB / 100;
            //decimal tienthue_GTGT = (trigiaTT_XNK + tienthue_XNK + tienthue_TTDB) * this._ThueSuatGTGT / 100;
            //decimal tienthukhac = this._TyLeThuKhac * trigiaTT_XNK / 100;

            //this._DonGiaTT = dongia_TT;
            //this._TriGiaKB = trigiaNT;
            //this._TriGiaTT = trigiaTT_XNK;
            //this._TriGiaKB_VND = trigiaNT * tygiaTT;
            //this._ThueXNK = tienthue_XNK;
            //this._ThueTTDB = tienthue_TTDB;
            //this._ThueGTGT = tienthue_GTGT;
            //this._TriGiaThuKhac = tienthukhac;
        }
        #region Select methods.

        public bool Load()
        {
            string spName = "p_SXXK_HangMauDich_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, this.SoThuTuHang);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) this._SoThuTuHang = reader.GetInt16(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) this._MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) this._DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) this._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) this._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) this._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) this._ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) this._ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) this._ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this._ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this._ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) this._ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this._PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) this._TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) this._TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) this._MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                this.MaTMP = this.MaPhu;
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------
        public List<HangMauDich> SelectCollectionBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy()
        {
            string spName = "p_SXXK_HangMauDich_SelectBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 300;

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            List<HangMauDich> collection = new List<HangMauDich>();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt16(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                entity.MaTMP = entity.MaPhu;
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy()
        {
            string spName = "p_SXXK_HangMauDich_SelectBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            return this.db.ExecuteDataSet(dbCommand);
        }


        public DataSet SelectAll()
        {
            string spName = "p_SXXK_HangMauDich_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_HangMauDich_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_HangMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_HangMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public HangMauDichCollection SelectCollectionAll()
        {
            HangMauDichCollection collection = new HangMauDichCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                HangMauDich entity = new HangMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt16(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                entity.MaTMP = entity.MaPhu;
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public HangMauDichCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            HangMauDichCollection collection = new HangMauDichCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                HangMauDich entity = new HangMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt16(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                entity.MaTMP = entity.MaPhu;
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_HangMauDich_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, this._SoThuTuHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this._DonGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, this._TriGiaKB);
            this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, this._TriGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, this._TriGiaKB_VND);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(HangMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangMauDich item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, HangMauDichCollection collection)
        {
            foreach (HangMauDich item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_HangMauDich_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, this._SoThuTuHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this._DonGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, this._TriGiaKB);
            this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, this._TriGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, this._TriGiaKB_VND);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);

            dbCommand.CommandTimeout = 1000;
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(HangMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangMauDich item in collection)
                    {
                        try
                        {
                            item.InsertUpdateTransaction(transaction);

                        }
                        catch
                        {
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public void InsertUpdate(HangMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (HangMauDich item in collection)
            {
                item.InsertUpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_HangMauDich_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, this._SoThuTuHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this._DonGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, this._TriGiaKB);
            this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, this._TriGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, this._TriGiaKB_VND);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(HangMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (HangMauDich item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }
        public int UpdateSTTHangTransaction(SqlTransaction transaction)
        {
            string sql = "update t_SXXK_HangMauDich set SoThuTuHang=@SoThuTuHang where SoToKhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh " +
                            " and MaHaiQuan=@MaHaiQuan and NamDangKy=@NamDangKy and MaPhu=@MaPhu";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, this._SoThuTuHang);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_HangMauDich_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan.PadRight(6));
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.VarChar, this._SoThuTuHang);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int DeleteByForeigKeyTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_HangMauDich_DeleteBy_MaHaiQuan_And_SoToKhai_And_MaLoaiHinh_And_NamDangKy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(HangMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (HangMauDich item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(HangMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangMauDich item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public void DeleteHangTransaction()
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string spName = "delete t_SXXK_HangMauDich where sotokhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and SoThuTuHang=@SoThuTuHang and NamDangKy=@NamDangKy";
                    SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);
                    dbCommand.CommandTimeout = 300;
                    this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
                    this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
                    this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
                    this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
                    this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.VarChar, this._SoThuTuHang);

                    this.db.ExecuteNonQuery(dbCommand, transaction);

                    spName = "delete t_SXXK_NPLNhapTonThucTe where sotokhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and MaNPL=@MaNPL and NamDangKy=@NamDangKy";
                    dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);
                    dbCommand.CommandTimeout = 300;
                    this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
                    this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
                    this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
                    this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
                    this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaPhu);

                    this.db.ExecuteNonQuery(dbCommand, transaction);

                    spName = "delete t_SXXK_ThanhLy_NPLNhapTon where sotokhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and MaNPL=@MaNPL and NamDangKy=@NamDangKy";
                    dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);
                    dbCommand.CommandTimeout = 300;
                    this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
                    this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
                    this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
                    this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
                    this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaPhu);

                    this.db.ExecuteNonQuery(dbCommand, transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void DeleteHangTransaction(SqlTransaction transaction)
        {
            string spName = "delete t_SXXK_HangMauDich where sotokhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and SoThuTuHang=@SoThuTuHang and NamDangKy=@NamDangKy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 300;
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.VarChar, this._SoThuTuHang);

            this.db.ExecuteNonQuery(dbCommand, transaction);

            spName = "delete t_SXXK_NPLNhapTonThucTe where sotokhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and MaNPL=@MaNPL and NamDangKy=@NamDangKy";
            dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 300;
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaPhu);

            this.db.ExecuteNonQuery(dbCommand, transaction);

            spName = "delete t_SXXK_ThanhLy_NPLNhapTon where sotokhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and MaNPL=@MaNPL and NamDangKy=@NamDangKy";
            dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 300;
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaPhu);
            this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        #endregion


    }
}