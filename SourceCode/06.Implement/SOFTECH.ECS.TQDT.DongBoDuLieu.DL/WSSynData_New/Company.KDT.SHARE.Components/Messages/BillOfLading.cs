﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class BillOfLading : IssueBase
    {

        /// <summary>
        /// Phương tiện vận tải
        /// </summary>
        [XmlElement("BorderTransportMeans")]
        public BorderTransportMeans BorderTransportMeans { get; set; }
        /// <summary>
        /// Tên vận tải
        /// </summary>
        [XmlElement("Carrier")]
        public NameBase Carrier { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("Consignment")]
        public Consignment Consignment { get; set; }

        /// <summary>
        /// Có đóng trong container không
        /// </summary>
        [XmlElement("isContainer")]
        public string IsContainer { get; set; }

        /// <summary>
        /// Địa điểm chuyển tải / quá cảnh
        /// </summary>
        [XmlElement("transitLocation")]
        public string TransitLocation { get; set; }

        /// <summary>
        /// Loại vận đơn
        /// </summary>
        [XmlElement("category")]
        public string Category { get; set; }

        /// <summary>
        /// AttachedFile
        /// </summary>
        [XmlElement("AttachedFile")]
        public Company.KDT.SHARE.Components.AttachedFile AttachedFile { get; set; }
    }
}
