-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NumberOfPackagesUnitCode varchar(5),
	@NumberOfPackagesUnitName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_PackagesUnit]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@NumberOfPackagesUnitCode,
	@NumberOfPackagesUnitName,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NumberOfPackagesUnitCode varchar(5),
	@NumberOfPackagesUnitName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_PackagesUnit]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[NumberOfPackagesUnitName] = @NumberOfPackagesUnitName,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NumberOfPackagesUnitCode varchar(5),
	@NumberOfPackagesUnitName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [NumberOfPackagesUnitCode] FROM [dbo].[t_VNACC_Category_PackagesUnit] WHERE [NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_PackagesUnit] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[NumberOfPackagesUnitName] = @NumberOfPackagesUnitName,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_PackagesUnit]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[NumberOfPackagesUnitCode],
			[NumberOfPackagesUnitName],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@NumberOfPackagesUnitCode,
			@NumberOfPackagesUnitName,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Delete]
	@NumberOfPackagesUnitCode varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_PackagesUnit]
WHERE
	[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_PackagesUnit] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Load]
	@NumberOfPackagesUnitCode varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_PackagesUnit]
WHERE
	[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_PackagesUnit] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_PackagesUnit]	

GO

