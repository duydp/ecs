﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using CMSActivexLib;
using VNPTCrypt;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;

namespace Project_SignatureXML
{
    public partial class frMain : Form
    {
        string strFilename = @"SX_NPL.xml";
        string indata = "Ha Noi mua thu";
        string outData = "";
        string CertificateData = "";
        byte[] Data = null;

        public frMain()
        {
            InitializeComponent();
        }

        private void btBrowerSign_Click(object sender, EventArgs e)
        {
        }

        private void btBrowerVerify_Click(object sender, EventArgs e)
        {
        }

        private void btSign_Click(object sender, EventArgs e)
        {
            try
            {
                //Khao báo
                //CMSActivex lib = new CMSActivex();
                //Convert data:
                Data = VNPTCrypt.Utils.ConvertStringToByte(indata);
                string sData = VNPTCrypt.Utils.ArrayFormatString(Data);
                //Ký
                outData = axCMSActivex1.SignedDataStr(sData, false, false);
                //Lây thông tin chứng thư
                CertificateData = axCMSActivex1.GetCurrentCertStr();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btVerify_Click(object sender, EventArgs e)
        {
            byte[] bCertificateData = null;
            byte[] SignatureData = null;
            string SignData = "";
            string CertData = "";
            if (outData != "")
            {
                //Check Signature
                SignatureData = VNPTCrypt.Utils.StringFormatArray(outData);
                bCertificateData = VNPTCrypt.Utils.StringFormatArray(CertificateData);

                string MessageError = "";
                bool bCheck = false;
                bCheck = VNPTCrypt.CryptoLib.VerifySign(bCertificateData, Data, SignatureData);
                if (bCheck == true)
                {
                    MessageBox.Show("Ok");
                }
                else
                {
                    MessageBox.Show(MessageError);
                }
            }
            else
            {
                MessageBox.Show("Chưa ký dữ liệu");
            }
        }

        private void frMain_Load(object sender, EventArgs e)
        {

        }
    }
}
