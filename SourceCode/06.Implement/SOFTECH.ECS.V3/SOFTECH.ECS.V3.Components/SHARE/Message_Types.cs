﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.SHARE
{
    public class Message_Types
    {
        public const int Hop_Dong = 315;
        public const int Hoa_don_thuong_mai = 380;
        public const int Master_bill_of_lading = 704;
        public const int Bill_of_lading_ban_goc_original = 706;
        public const int Bill_of_lading_ban_copy = 707;
        public const int Bill_container_rong = 708;//_(Empty container bill)
        public const int Bill_hang_xa_hang_roi = 709;//(Tanker bill of lading)	
        public const int Sea_waybill = 710;
        public const int House_bill_of_lading = 714;
        public const int Air_waybill = 740;
        public const int Master_air_waybill = 741;
        public const int Substitute_air_waybill =743;
        public const int Packing_list = 750;
        public const int Giay_phep_xuat_khau = 811;// (Export licence)	
        public const int Giay_chung_nhan_xuat_xu_hang_hoa_Form_D = 861;// (C/O - Form D)	
        public const int Giay_chung_nhan_xuat_xu_hang_hoa_From_A = 865;// (C/O - Form A)	
        public const int Giay_phep_nhap_khau = 911;// (Import licence)	
        //public const int Giay_chung_nhan_xuat_xu_hang_hoa = 007;// (C/O - Form D)	 ????????????????? Trung voi 861
        public const int Giay_chung_nhan_xuat_xu_hang_hoa_From_E = 008;// (C/O - Form E)	
        public const int Giay_dang_ky_kiem_tra_chat_luong_VSANTT_KĐTV = 400;
        public const int Giay_thong_bao_kiem_tra_chat_luong_VSANTT_KĐTV = 401;
        public const int Giay_thong_bao_ket_qua_kiem_tra = 402;
        public const int Chung_thu_giam_dinh = 403;
        public const int Giay_nop_tien = 404;
        public const int De_nghi_chuyen_cua_khau = 405;
        public const string Khong_co_TKTG = "";//	<xâu rỗng>;
        public const int TKTG_PP1 =	1;
        public const int TKTG_PP2 =	2;
        public const int TKTG_PP3 =	3;
        public const int TKTG_PP4 =	4;
        public const int TKTG_PP5 =	5;
        public const int TKTG_PP6 =	6;
        public const int To_khai_nhap_khau_thuong_mai =	929;
        public const int To_khai_xuat_khau_thuonng_mai = 930;
        public const int To_khai_nhap_khau_SXXK = 931;
        public const int To_khai_xuat_khau_SXXK = 932;
        public const int To_khai_nhap_khau_Gia_cong = 935;
        public const int To_khai_xuat_khau_Gia_cong = 936;
        public const int To_khai_nhap_khau_Gia_cong_Chuyen_Tiep = 968;
        public const int To_khai_xuat_khau_Gia_cong_Chuyen_Tiep = 986;
        public const int To_khai_dien_tu_don_gian =	939;
        public const int To_khai_dien_tu_thang = 940;
        public const int Chung_tu_dua_hang_vao_doanh_nghiep_che_xuat = 937;
        public const int Chung_tu_dua_hang_ra_khoi_doanh_nghiep_che_xuat = 938;
        public const int Danh_muc_NPL_SXXK = 100;
        public const int Danh_muc_SP_SXXK =	101;
        public const int Dinh_muc_SP_SXXK =	102;
        public const int Ho_so_thanh_khoan = 103;
        public const int Hop_dong_Gia_cong = 601;
        public const int Phu__kien_hop_dong_Gia_cong = 602;
        public const int Dinh_muc_san_pham_Gia_cong = 603;
        public const int Yeu_cau_thanh_khoan_hop_dong_gia_cong = 605;
        public const int De_nghi_giam_sat_tieu_huy_nguyen_phu_lieu_san_pham_gia_cong = 609;
        public const int Danh_muc_hang_hoa_dua_vao_doanh_nghiep_che_xuat = 501;
        public const int Danh_muc_hang_hoa_dua_ra_doanh_nghiep_che_xuat = 502;
        public const int Dịnh_muc_san_pham_doanh_nghiep_che_xuat = 508;
        public const int Bao_cao_hang_ton_kho =	510;
        public const int Quyet_dinh_kiem_tra_thuc_te_hang_ton_kho =	511;
        public const int Thong_tin_huy_nguyen_lieu_San_pham_Phe_lieu_Phe_pham =	512;
        public const int Thong_tin_thanh_ly_tai_san_co_dinh = 513;
        public const int Hop_Dong_Giao = 315;
        public const int Hop_Dong_Nhan = 316;

    }
    public class Enum
    {
        public const string NGC18 = "NGC18";
        public const string NGC19 = "NGC19";
        public const string NGC20 = "NGC20";
        public const string XGC18 = "XGC18";
        public const string XGC19 = "XGC19";
        public const string XGC20 = "XGC20";
        public const string PHPLN = "PHPLN";
        public const string PHPLX = "PHPLX";
        public const string PHSPN = "PHSPN";
        public const string PHSPX = "PHSPX";
        public const string PHTBN = "PHTBN";
        public const string PHTBX = "PHTBX";
    }
}
