using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class IDB : BasicVNACC
    {
        public PropertiesAttribute ICN { get; set; }
        public PropertiesAttribute TOM { get; set; }
        public PropertiesAttribute BLN { get; set; }
        public PropertiesAttribute IVU { get; set; }

        public IDB()
        {
            ICN = new PropertiesAttribute(12, typeof(int));
            TOM = new PropertiesAttribute(1, typeof(string));
            BLN = new PropertiesAttribute(35, typeof(string));
            IVU = new PropertiesAttribute(12, typeof(int));
        }

    }
    public partial class EnumGroupID
    {
    }

}
