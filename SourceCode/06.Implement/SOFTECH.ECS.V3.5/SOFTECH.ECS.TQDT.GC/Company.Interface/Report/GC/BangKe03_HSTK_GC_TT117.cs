﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe03_HSTK_GC_TT117 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewBC03_HSTK_GC_TT117Form report;
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;

        public BangKe03_HSTK_GC_TT117()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt)
        {
            lblNgayKi.Text =  GlobalSettings.TieudeNgay;
            
            bool isLast = dt.Rows[0]["SoToKhai"].ToString() == "-";
            //TODO: Comment by Hungtq 06/08/2012.
            //this.ReportHeader.Visible = !isLast;
            //lblNgayKi.Visible = isLast;
            //lblNgayKi1.Visible = isLast;
            //lblNgayKi2.Visible = isLast;

            this.ReportHeader.Visible = true;
            lblNgayKi.Visible = true;
            lblNgayKi1.Visible = true;
            lblNgayKi2.Visible = true;

            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = dt;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }

            lblMathang.Text = loaiSP;
            lblAddThue.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;


            lblSTT.DataBindings.Add("Text", this.DataSource,"STT");
            lblToKhaiNhap.DataBindings.Add("Text", this.DataSource, "SoToKhai");            
            lblTenNguyenLieuVT.DataBindings.Add("Text", this.DataSource, "TenHang");
            lblMaNLVT.DataBindings.Add("Text", this.DataSource, "MaHang");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT");
            lblLuonghang.DataBindings.Add("Text", this.DataSource, "SoLuong", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuong.DataBindings.Add("Text", this.DataSource, "TongCong", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, "GhiChu");

        }
        private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            //XRLabel label = (XRLabel)sender;  
            //report.txtName.Text = label.Text;
            //report.lblName.Text = label.Tag.ToString();
        }
        public void setText(XRLabel label, string text)
        {
            //label.Text = text;
        }
    }
}
