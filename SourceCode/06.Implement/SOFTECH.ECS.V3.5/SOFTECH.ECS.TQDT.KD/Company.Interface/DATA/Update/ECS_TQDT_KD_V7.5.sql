/*
Run this script on:

        192.168.72.100.ECS_TQDT_KD_V4_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_KD_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 03/18/2013 10:58:15 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_ToKhaiMauDichBoSung]'
GO
ALTER TABLE [dbo].[t_KDT_ToKhaiMauDichBoSung] ADD
[MaNguoiNhanHang] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNguoiNhanHang] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNguoiGiaoHang] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNguoiGiaoHang] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNguoiChiDinhGiaoHang] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNguoiChiDinhGiaoHang] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNguoiKhaiHQ] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNguoiKhaiHQ] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiDungUyQuyen] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongCacKhoanPhaiCong] [decimal] (35, 18) NULL,
[TongCacKhoanPhaiTru] [decimal] (35, 18) NULL,
[NgayKhaiBao] [datetime] NULL CONSTRAINT [DF_t_KDT_ToKhaiMauDichBoSung_NgayKhaiBao] DEFAULT (((1900)-(1))-(1))
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang varchar(255),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang varchar(255),
	@MaNguoiChiDinhGiaoHang varchar(50),
	@TenNguoiChiDinhGiaoHang varchar(255),
	@MaNguoiKhaiHQ varchar(50),
	@TenNguoiKhaiHQ varchar(255),
	@NoiDungUyQuyen varchar(50),
	@TongCacKhoanPhaiCong decimal(35, 18),
	@TongCacKhoanPhaiTru decimal(35, 18),
	@NgayKhaiBao datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
(
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[MaNguoiChiDinhGiaoHang],
	[TenNguoiChiDinhGiaoHang],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[NoiDungUyQuyen],
	[TongCacKhoanPhaiCong],
	[TongCacKhoanPhaiTru],
	[NgayKhaiBao]
)
VALUES 
(
	@TKMD_ID,
	@PhienBan,
	@ChuKySo,
	@GhiChu,
	@MaNguoiNhanHang,
	@TenNguoiNhanHang,
	@MaNguoiGiaoHang,
	@TenNguoiGiaoHang,
	@MaNguoiChiDinhGiaoHang,
	@TenNguoiChiDinhGiaoHang,
	@MaNguoiKhaiHQ,
	@TenNguoiKhaiHQ,
	@NoiDungUyQuyen,
	@TongCacKhoanPhaiCong,
	@TongCacKhoanPhaiTru,
	@NgayKhaiBao
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang varchar(255),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang varchar(255),
	@MaNguoiChiDinhGiaoHang varchar(50),
	@TenNguoiChiDinhGiaoHang varchar(255),
	@MaNguoiKhaiHQ varchar(50),
	@TenNguoiKhaiHQ varchar(255),
	@NoiDungUyQuyen varchar(50),
	@TongCacKhoanPhaiCong decimal(35, 18),
	@TongCacKhoanPhaiTru decimal(35, 18),
	@NgayKhaiBao datetime
AS

UPDATE
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
SET
	[TKMD_ID] = @TKMD_ID,
	[PhienBan] = @PhienBan,
	[ChuKySo] = @ChuKySo,
	[GhiChu] = @GhiChu,
	[MaNguoiNhanHang] = @MaNguoiNhanHang,
	[TenNguoiNhanHang] = @TenNguoiNhanHang,
	[MaNguoiGiaoHang] = @MaNguoiGiaoHang,
	[TenNguoiGiaoHang] = @TenNguoiGiaoHang,
	[MaNguoiChiDinhGiaoHang] = @MaNguoiChiDinhGiaoHang,
	[TenNguoiChiDinhGiaoHang] = @TenNguoiChiDinhGiaoHang,
	[MaNguoiKhaiHQ] = @MaNguoiKhaiHQ,
	[TenNguoiKhaiHQ] = @TenNguoiKhaiHQ,
	[NoiDungUyQuyen] = @NoiDungUyQuyen,
	[TongCacKhoanPhaiCong] = @TongCacKhoanPhaiCong,
	[TongCacKhoanPhaiTru] = @TongCacKhoanPhaiTru,
	[NgayKhaiBao] = @NgayKhaiBao
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang varchar(255),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang varchar(255),
	@MaNguoiChiDinhGiaoHang varchar(50),
	@TenNguoiChiDinhGiaoHang varchar(255),
	@MaNguoiKhaiHQ varchar(50),
	@TenNguoiKhaiHQ varchar(255),
	@NoiDungUyQuyen varchar(50),
	@TongCacKhoanPhaiCong decimal(35, 18),
	@TongCacKhoanPhaiTru decimal(35, 18),
	@NgayKhaiBao datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDichBoSung] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[PhienBan] = @PhienBan,
			[ChuKySo] = @ChuKySo,
			[GhiChu] = @GhiChu,
			[MaNguoiNhanHang] = @MaNguoiNhanHang,
			[TenNguoiNhanHang] = @TenNguoiNhanHang,
			[MaNguoiGiaoHang] = @MaNguoiGiaoHang,
			[TenNguoiGiaoHang] = @TenNguoiGiaoHang,
			[MaNguoiChiDinhGiaoHang] = @MaNguoiChiDinhGiaoHang,
			[TenNguoiChiDinhGiaoHang] = @TenNguoiChiDinhGiaoHang,
			[MaNguoiKhaiHQ] = @MaNguoiKhaiHQ,
			[TenNguoiKhaiHQ] = @TenNguoiKhaiHQ,
			[NoiDungUyQuyen] = @NoiDungUyQuyen,
			[TongCacKhoanPhaiCong] = @TongCacKhoanPhaiCong,
			[TongCacKhoanPhaiTru] = @TongCacKhoanPhaiTru,
			[NgayKhaiBao] = @NgayKhaiBao
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
		(
			[TKMD_ID],
			[PhienBan],
			[ChuKySo],
			[GhiChu],
			[MaNguoiNhanHang],
			[TenNguoiNhanHang],
			[MaNguoiGiaoHang],
			[TenNguoiGiaoHang],
			[MaNguoiChiDinhGiaoHang],
			[TenNguoiChiDinhGiaoHang],
			[MaNguoiKhaiHQ],
			[TenNguoiKhaiHQ],
			[NoiDungUyQuyen],
			[TongCacKhoanPhaiCong],
			[TongCacKhoanPhaiTru],
			[NgayKhaiBao]
		)
		VALUES 
		(
			@TKMD_ID,
			@PhienBan,
			@ChuKySo,
			@GhiChu,
			@MaNguoiNhanHang,
			@TenNguoiNhanHang,
			@MaNguoiGiaoHang,
			@TenNguoiGiaoHang,
			@MaNguoiChiDinhGiaoHang,
			@TenNguoiChiDinhGiaoHang,
			@MaNguoiKhaiHQ,
			@TenNguoiKhaiHQ,
			@NoiDungUyQuyen,
			@TongCacKhoanPhaiCong,
			@TongCacKhoanPhaiTru,
			@NgayKhaiBao
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[MaNguoiChiDinhGiaoHang],
	[TenNguoiChiDinhGiaoHang],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[NoiDungUyQuyen],
	[TongCacKhoanPhaiCong],
	[TongCacKhoanPhaiTru],
	[NgayKhaiBao]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[MaNguoiChiDinhGiaoHang],
	[TenNguoiChiDinhGiaoHang],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[NoiDungUyQuyen],
	[TongCacKhoanPhaiCong],
	[TongCacKhoanPhaiTru],
	[NgayKhaiBao]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[MaNguoiChiDinhGiaoHang],
	[TenNguoiChiDinhGiaoHang],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[NoiDungUyQuyen],
	[TongCacKhoanPhaiCong],
	[TongCacKhoanPhaiTru],
	[NgayKhaiBao]
FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[MaNguoiChiDinhGiaoHang],
	[TenNguoiChiDinhGiaoHang],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[NoiDungUyQuyen],
	[TongCacKhoanPhaiCong],
	[TongCacKhoanPhaiTru],
	[NgayKhaiBao]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]	

GO


UPDATE dbo.t_HaiQuan_Version SET [Version] = '7.5', [Date] = GETDATE(), Notes = N''