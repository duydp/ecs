﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class ProductionNorm
    {
        /// <summary>
        /// Sản Phẩm
        /// </summary>
        [XmlElement("Product")]
        public Product Product { get; set; }
        /// <summary>
        /// Nguyên Phụ Liệu
        /// </summary>
        [XmlElement("MaterialsNorm")]
        public List<MaterialsNorm> MaterialsNorm { get; set; }

    }
}
