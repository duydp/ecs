﻿namespace Company.Interface
{
    partial class ToKhaiTriGiaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiTriGiaForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGhiChep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtChucDanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ccNgayKhaiBao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.rbQuanHeDB = new System.Windows.Forms.RadioButton();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.rbAnhHuongQH = new System.Windows.Forms.RadioButton();
            this.txtKieuQH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.rbKhongXacDinh = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.rbTraThem = new System.Windows.Forms.RadioButton();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.rbTienTra = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.rbQuyenSuDung = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.gbGiayPhep = new Janus.Windows.EditControls.UIGroupBox();
            this.txtToSo = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ccNgayXuatKhau = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.Luu1 = new Janus.Windows.UI.CommandBars.UICommand("Luu");
            this.addHang1 = new Janus.Windows.UI.CommandBars.UICommand("addHang");
            this.cmdInTKTG1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTG");
            this.Luu = new Janus.Windows.UI.CommandBars.UICommand("Luu");
            this.addHang = new Janus.Windows.UI.CommandBars.UICommand("addHang");
            this.cmdInTKTG = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTG");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvXuatKhau = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rvToSo = new Company.Controls.CustomValidation.RangeValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).BeginInit();
            this.gbGiayPhep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvXuatKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvToSo)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(715, 380);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.gbGiayPhep);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(715, 380);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(8, 305);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(698, 72);
            this.dgList.TabIndex = 6;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.txtGhiChep);
            this.uiGroupBox6.Controls.Add(this.label13);
            this.uiGroupBox6.Controls.Add(this.txtChucDanh);
            this.uiGroupBox6.Controls.Add(this.label10);
            this.uiGroupBox6.Controls.Add(this.txtNguoiKhai);
            this.uiGroupBox6.Controls.Add(this.label8);
            this.uiGroupBox6.Controls.Add(this.label9);
            this.uiGroupBox6.Controls.Add(this.ccNgayKhaiBao);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(8, 254);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(698, 45);
            this.uiGroupBox6.TabIndex = 5;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChep
            // 
            this.txtGhiChep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChep.Location = new System.Drawing.Point(593, 13);
            this.txtGhiChep.Name = "txtGhiChep";
            this.txtGhiChep.Size = new System.Drawing.Size(98, 21);
            this.txtGhiChep.TabIndex = 7;
            this.txtGhiChep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtGhiChep.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(533, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Ghi chép";
            // 
            // txtChucDanh
            // 
            this.txtChucDanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChucDanh.Location = new System.Drawing.Point(431, 14);
            this.txtChucDanh.Name = "txtChucDanh";
            this.txtChucDanh.Size = new System.Drawing.Size(98, 21);
            this.txtChucDanh.TabIndex = 5;
            this.txtChucDanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChucDanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtChucDanh.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(370, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Chức danh";
            // 
            // txtNguoiKhai
            // 
            this.txtNguoiKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiKhai.Location = new System.Drawing.Point(268, 14);
            this.txtNguoiKhai.Name = "txtNguoiKhai";
            this.txtNguoiKhai.Size = new System.Drawing.Size(96, 21);
            this.txtNguoiKhai.TabIndex = 3;
            this.txtNguoiKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Ngày khai báo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(205, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Người khai";
            // 
            // ccNgayKhaiBao
            // 
            // 
            // 
            // 
            this.ccNgayKhaiBao.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayKhaiBao.DropDownCalendar.Name = "";
            this.ccNgayKhaiBao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKhaiBao.IsNullDate = true;
            this.ccNgayKhaiBao.Location = new System.Drawing.Point(99, 14);
            this.ccNgayKhaiBao.Name = "ccNgayKhaiBao";
            this.ccNgayKhaiBao.Nullable = true;
            this.ccNgayKhaiBao.NullButtonText = "Xóa";
            this.ccNgayKhaiBao.ShowNullButton = true;
            this.ccNgayKhaiBao.Size = new System.Drawing.Size(96, 21);
            this.ccNgayKhaiBao.TabIndex = 1;
            this.ccNgayKhaiBao.TodayButtonText = "Hôm nay";
            this.ccNgayKhaiBao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKhaiBao.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.uiGroupBox11);
            this.uiGroupBox5.Controls.Add(this.uiGroupBox10);
            this.uiGroupBox5.Controls.Add(this.txtKieuQH);
            this.uiGroupBox5.Controls.Add(this.label7);
            this.uiGroupBox5.Controls.Add(this.label5);
            this.uiGroupBox5.Controls.Add(this.label6);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(360, 134);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(346, 114);
            this.uiGroupBox5.TabIndex = 4;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.BorderColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.radioButton7);
            this.uiGroupBox11.Controls.Add(this.rbQuanHeDB);
            this.uiGroupBox11.Location = new System.Drawing.Point(232, 11);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(123, 34);
            this.uiGroupBox11.TabIndex = 1;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Checked = true;
            this.radioButton7.Location = new System.Drawing.Point(51, 11);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(60, 17);
            this.radioButton7.TabIndex = 1;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "Không";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // rbQuanHeDB
            // 
            this.rbQuanHeDB.AutoSize = true;
            this.rbQuanHeDB.Location = new System.Drawing.Point(9, 11);
            this.rbQuanHeDB.Name = "rbQuanHeDB";
            this.rbQuanHeDB.Size = new System.Drawing.Size(39, 17);
            this.rbQuanHeDB.TabIndex = 0;
            this.rbQuanHeDB.Text = "Có";
            this.rbQuanHeDB.UseVisualStyleBackColor = true;
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.BorderColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.radioButton9);
            this.uiGroupBox10.Controls.Add(this.rbAnhHuongQH);
            this.uiGroupBox10.Location = new System.Drawing.Point(229, 72);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(123, 29);
            this.uiGroupBox10.TabIndex = 5;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Checked = true;
            this.radioButton9.Location = new System.Drawing.Point(54, 11);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(60, 17);
            this.radioButton9.TabIndex = 1;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "Không";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // rbAnhHuongQH
            // 
            this.rbAnhHuongQH.AutoSize = true;
            this.rbAnhHuongQH.Location = new System.Drawing.Point(12, 11);
            this.rbAnhHuongQH.Name = "rbAnhHuongQH";
            this.rbAnhHuongQH.Size = new System.Drawing.Size(39, 17);
            this.rbAnhHuongQH.TabIndex = 0;
            this.rbAnhHuongQH.Text = "Có";
            this.rbAnhHuongQH.UseVisualStyleBackColor = true;
            // 
            // txtKieuQH
            // 
            this.txtKieuQH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKieuQH.Location = new System.Drawing.Point(179, 45);
            this.txtKieuQH.Name = "txtKieuQH";
            this.txtKieuQH.Size = new System.Drawing.Size(149, 21);
            this.txtKieuQH.TabIndex = 3;
            this.txtKieuQH.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKieuQH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtKieuQH.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(227, 30);
            this.label7.TabIndex = 0;
            this.label7.Text = "5. Người mua và người bán có mối quan hệ đặc biệt hay không ?";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(223, 36);
            this.label5.TabIndex = 4;
            this.label5.Text = "Mối quan hệ đặc biệt có ảnh hưởng đến trị giá giao dịch không ?";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 15);
            this.label6.TabIndex = 2;
            this.label6.Text = "Nếu có, nêu rõ quan hệ đó";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.radioButton5);
            this.uiGroupBox4.Controls.Add(this.rbKhongXacDinh);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(360, 68);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(346, 60);
            this.uiGroupBox4.TabIndex = 2;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Checked = true;
            this.radioButton5.Location = new System.Drawing.Point(283, 18);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(60, 17);
            this.radioButton5.TabIndex = 2;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Không";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // rbKhongXacDinh
            // 
            this.rbKhongXacDinh.AutoSize = true;
            this.rbKhongXacDinh.Location = new System.Drawing.Point(241, 18);
            this.rbKhongXacDinh.Name = "rbKhongXacDinh";
            this.rbKhongXacDinh.Size = new System.Drawing.Size(39, 17);
            this.rbKhongXacDinh.TabIndex = 1;
            this.rbKhongXacDinh.Text = "Có";
            this.rbKhongXacDinh.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(238, 46);
            this.label3.TabIndex = 0;
            this.label3.Text = "3. Việc mua bán hàng hay giá cả có phụ thuộc vào điều kiện nào dẫn đến việc không" +
                " xác định được trị giá của hàng hóa nhập khẩu không ?";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(8, 134);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(346, 114);
            this.uiGroupBox3.TabIndex = 3;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.BorderColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.radioButton3);
            this.uiGroupBox9.Controls.Add(this.rbTraThem);
            this.uiGroupBox9.Location = new System.Drawing.Point(231, 11);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(123, 45);
            this.uiGroupBox9.TabIndex = 1;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(52, 11);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(60, 17);
            this.radioButton3.TabIndex = 1;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Không";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // rbTraThem
            // 
            this.rbTraThem.AutoSize = true;
            this.rbTraThem.Location = new System.Drawing.Point(10, 11);
            this.rbTraThem.Name = "rbTraThem";
            this.rbTraThem.Size = new System.Drawing.Size(39, 17);
            this.rbTraThem.TabIndex = 0;
            this.rbTraThem.Text = "Có";
            this.rbTraThem.UseVisualStyleBackColor = true;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BorderColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.radioButton11);
            this.uiGroupBox8.Controls.Add(this.rbTienTra);
            this.uiGroupBox8.Location = new System.Drawing.Point(234, 59);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(106, 41);
            this.uiGroupBox8.TabIndex = 3;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Checked = true;
            this.radioButton11.Location = new System.Drawing.Point(49, 11);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(60, 17);
            this.radioButton11.TabIndex = 1;
            this.radioButton11.TabStop = true;
            this.radioButton11.Text = "Không";
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // rbTienTra
            // 
            this.rbTienTra.AutoSize = true;
            this.rbTienTra.Location = new System.Drawing.Point(7, 11);
            this.rbTienTra.Name = "rbTienTra";
            this.rbTienTra.Size = new System.Drawing.Size(39, 17);
            this.rbTienTra.TabIndex = 0;
            this.rbTienTra.Text = "Có";
            this.rbTienTra.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(232, 36);
            this.label4.TabIndex = 2;
            this.label4.Text = "Nếu có, có phải là khoản tiền khai báo tại tiêu thức 15 không ?";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 50);
            this.label1.TabIndex = 0;
            this.label1.Text = "4. Người mua có phải trả thêm khoản tiền nào từ số tiền thu được do việc định đoạ" +
                "t, sử dụng hàng hóa nhập khẩu không ?";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.radioButton2);
            this.uiGroupBox2.Controls.Add(this.rbQuyenSuDung);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(8, 68);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(346, 60);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(283, 18);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(60, 17);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.Text = "Không";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // rbQuyenSuDung
            // 
            this.rbQuyenSuDung.AutoSize = true;
            this.rbQuyenSuDung.Checked = true;
            this.rbQuyenSuDung.Location = new System.Drawing.Point(241, 18);
            this.rbQuyenSuDung.Name = "rbQuyenSuDung";
            this.rbQuyenSuDung.Size = new System.Drawing.Size(39, 17);
            this.rbQuyenSuDung.TabIndex = 1;
            this.rbQuyenSuDung.TabStop = true;
            this.rbQuyenSuDung.Text = "Có";
            this.rbQuyenSuDung.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(229, 36);
            this.label2.TabIndex = 0;
            this.label2.Text = "2. Người mua có đầy đủ quyền định đoạt, quyền sử dụng hàng hóa sau khi nhập khẩu " +
                "không ?";
            // 
            // gbGiayPhep
            // 
            this.gbGiayPhep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbGiayPhep.BackColor = System.Drawing.Color.Transparent;
            this.gbGiayPhep.Controls.Add(this.txtToSo);
            this.gbGiayPhep.Controls.Add(this.label11);
            this.gbGiayPhep.Controls.Add(this.label12);
            this.gbGiayPhep.Controls.Add(this.ccNgayXuatKhau);
            this.gbGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGiayPhep.Location = new System.Drawing.Point(8, 5);
            this.gbGiayPhep.Name = "gbGiayPhep";
            this.gbGiayPhep.Size = new System.Drawing.Size(698, 57);
            this.gbGiayPhep.TabIndex = 0;
            this.gbGiayPhep.Text = "Thông tin chung";
            this.gbGiayPhep.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.gbGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtToSo
            // 
            this.txtToSo.Location = new System.Drawing.Point(207, 20);
            this.txtToSo.MaxLength = 6;
            this.txtToSo.Name = "txtToSo";
            this.txtToSo.Size = new System.Drawing.Size(100, 21);
            this.txtToSo.TabIndex = 1;
            this.txtToSo.Text = "0";
            this.txtToSo.Value = ((ulong)(0ul));
            this.txtToSo.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(124, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số tờ khai";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(341, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Ngày xuất";
            // 
            // ccNgayXuatKhau
            // 
            // 
            // 
            // 
            this.ccNgayXuatKhau.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayXuatKhau.DropDownCalendar.Name = "";
            this.ccNgayXuatKhau.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayXuatKhau.IsNullDate = true;
            this.ccNgayXuatKhau.Location = new System.Drawing.Point(421, 20);
            this.ccNgayXuatKhau.Name = "ccNgayXuatKhau";
            this.ccNgayXuatKhau.Nullable = true;
            this.ccNgayXuatKhau.NullButtonText = "Xóa";
            this.ccNgayXuatKhau.ShowNullButton = true;
            this.ccNgayXuatKhau.Size = new System.Drawing.Size(96, 21);
            this.ccNgayXuatKhau.TabIndex = 3;
            this.ccNgayXuatKhau.TodayButtonText = "Hôm nay";
            this.ccNgayXuatKhau.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.Luu,
            this.addHang,
            this.cmdInTKTG});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.Luu1,
            this.addHang1,
            this.cmdInTKTG1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(344, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // Luu1
            // 
            this.Luu1.Key = "Luu";
            this.Luu1.Name = "Luu1";
            this.Luu1.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.Luu1.Text = "Lưu thông tin";
            // 
            // addHang1
            // 
            this.addHang1.Key = "addHang";
            this.addHang1.Name = "addHang1";
            this.addHang1.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            this.addHang1.Text = "Thêm hàng";
            // 
            // cmdInTKTG1
            // 
            this.cmdInTKTG1.Key = "cmdInTKTG";
            this.cmdInTKTG1.Name = "cmdInTKTG1";
            // 
            // Luu
            // 
            this.Luu.Icon = ((System.Drawing.Icon)(resources.GetObject("Luu.Icon")));
            this.Luu.Key = "Luu";
            this.Luu.Name = "Luu";
            this.Luu.Text = "Lưu thông tin";
            // 
            // addHang
            // 
            this.addHang.Icon = ((System.Drawing.Icon)(resources.GetObject("addHang.Icon")));
            this.addHang.Key = "addHang";
            this.addHang.Name = "addHang";
            this.addHang.Text = "Thêm hàng";
            // 
            // cmdInTKTG
            // 
            this.cmdInTKTG.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdInTKTG.Icon")));
            this.cmdInTKTG.Key = "cmdInTKTG";
            this.cmdInTKTG.Name = "cmdInTKTG";
            this.cmdInTKTG.Text = "In tờ khai Trị Giá";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(715, 32);
            this.TopRebar1.Click += new System.EventHandler(this.TopRebar1_Click);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.HostingForm = this;
            // 
            // rfvXuatKhau
            // 
            this.rfvXuatKhau.ControlToValidate = this.ccNgayXuatKhau;
            this.rfvXuatKhau.ErrorMessage = "\"Ngày xuất khẩu\" không được bỏ trống.";
            this.rfvXuatKhau.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvXuatKhau.Icon")));
            this.rfvXuatKhau.Tag = "rfvXuatKhau";
            // 
            // rvToSo
            // 
            this.rvToSo.ControlToValidate = this.txtToSo;
            this.rvToSo.ErrorMessage = "\"Tờ số \" không hợp lệ.";
            this.rvToSo.Icon = ((System.Drawing.Icon)(resources.GetObject("rvToSo.Icon")));
            this.rvToSo.MaximumValue = "999999";
            this.rvToSo.MinimumValue = "1";
            this.rvToSo.Tag = "rvToSo";
            this.rvToSo.Type = Company.Controls.CustomValidation.ValidationDataType.Integer;
            // 
            // ToKhaiTriGiaForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(715, 412);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiTriGiaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin tờ khai trị giá";
            this.Load += new System.EventHandler(this.ToKhaiTriGiaForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).EndInit();
            this.gbGiayPhep.ResumeLayout(false);
            this.gbGiayPhep.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvXuatKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvToSo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.EditControls.EditBox txtChucDanh;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiKhai;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKhaiBao;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtKieuQH;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton rbKhongXacDinh;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton rbTienTra;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton rbQuyenSuDung;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox gbGiayPhep;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayXuatKhau;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton rbQuanHeDB;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton rbAnhHuongQH;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton rbTraThem;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtToSo;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand Luu;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand Luu1;
        private Janus.Windows.UI.CommandBars.UICommand addHang1;
        private Janus.Windows.UI.CommandBars.UICommand addHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChep;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvXuatKhau;
        private Company.Controls.CustomValidation.RangeValidator rvToSo;
        private Janus.Windows.UI.CommandBars.UICommand cmdInTKTG1;
        private Janus.Windows.UI.CommandBars.UICommand cmdInTKTG;


    }
}