﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Người khai HQ
    /// </summary>
    public class GC_Materials 
    {

        [XmlElement("Material")]
        public List<GC_Material> Material { get; set; }
    

    }
    public class GC_Material
    {

        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }

        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }

        [XmlElement("DeclarationDocuments")]
        public GC_DeclarationDocuments DeclarationDocuments { get; set; }

    }
}
