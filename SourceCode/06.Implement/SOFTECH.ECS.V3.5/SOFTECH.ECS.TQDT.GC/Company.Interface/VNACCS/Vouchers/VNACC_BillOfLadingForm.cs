﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
using Janus.Windows.GridEX;
#if GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
#endif
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.Messages.Vouchers;
#if KD_V4
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
#endif
#if SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
#endif

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class VNACC_BillOfLadingForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_BillOfLadingNew billOfLadingNew = new KDT_VNACCS_BillOfLadingNew();
        public KDT_VNACCS_BillOfLadings_Detail billOfLadingsDetail = null;
        public KDT_VNACCS_BranchDetail branchDetail = null;
        public KDT_VNACCS_BranchDetail_TransportEquipment transportEquipment = null;
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public String Caption;
        public bool IsChange;
        public VNACC_BillOfLadingForm()
        {
            InitializeComponent();

            base.SetHandler(this);

            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.SBL.ToString());
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void VNACC_BillOfLadingForm_Load(object sender, EventArgs e)
        {
            SetIDControl();

            ctrCoQuanHaiQuan.TextChanged -= new EventHandler(txt_TextChanged);
            ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            ctrCoQuanHaiQuan.TextChanged += new EventHandler(txt_TextChanged);

            dateNgayTN.Value = DateTime.Now;

            txtTenDoanhNghiep.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            txtTenDoanhNghiep.TextChanged += new EventHandler(txt_TextChanged);

            txtMaDoanhNghiep.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            txtMaDoanhNghiep.TextChanged += new EventHandler(txt_TextChanged);

            Caption = this.Text;
            if (billOfLadingNew.ID > 0)
            {
                SetBillOfLadingNew();
                billOfLadingNew.Collection = KDT_VNACCS_BillOfLadings_Detail.SelectCollectionBy_BillOfLadings_ID(billOfLadingNew.ID);
                foreach (KDT_VNACCS_BillOfLadings_Detail item in billOfLadingNew.Collection)
                {
                    item.Collection = KDT_VNACCS_BranchDetail.SelectCollectionBy_BillOfLadings_Details_ID(item.ID);
                    foreach (KDT_VNACCS_BranchDetail branchDetail in item.Collection)
                    {
                        branchDetail.Collection = KDT_VNACCS_BranchDetail_TransportEquipment.SelectCollectionBy_BranchDetail_ID(branchDetail.ID);
                    }
                }
                BinDataBillOfLadingsDetail();
                //BinDataBranchDetail();
                //BinDataContainer();
            }
        }
        private void BinDataBillOfLadingsDetail()
        {
            try
            {
                dgListVDG.Refresh();
                dgListVDG.DataSource = billOfLadingNew.Collection;
                dgListVDG.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private bool ValidateFormBillOfLadingsDetail(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoVanDonGoc, errorProvider, "SỐ VẬN ĐƠN GỐC", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaNguoiPH, errorProvider, "MÃ NGƯỜI PHÁT HÀNH VẬN ĐƠN GỐC", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongVDN, errorProvider, "SỐ LƯỢNG VẬN ĐƠN NHÁNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbPhanLoai, errorProvider, "PHÂN LOẠI TÁCH VẬN ĐƠN", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHH, errorProvider, "LOẠI HÀNG HÓA", isOnlyWarning);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void SetData()
        {
            try
            {
                txtSoVanDonGoc.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDonGoc.Text = billOfLadingsDetail.SoVanDonGoc;
                txtSoVanDonGoc.TextChanged += new EventHandler(txt_TextChanged);

                clcNgay.TextChanged -= new EventHandler(txt_TextChanged);
                clcNgay.Value = billOfLadingsDetail.NgayVanDonGoc;
                clcNgay.TextChanged += new EventHandler(txt_TextChanged);

                txtMaNguoiPH.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaNguoiPH.Text = billOfLadingsDetail.MaNguoiPhatHanh;
                txtMaNguoiPH.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuongVDN.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuongVDN.Text = billOfLadingsDetail.SoLuongVDN.ToString();
                txtSoLuongVDN.TextChanged += new EventHandler(txt_TextChanged);

                cbbPhanLoai.TextChanged -= new EventHandler(txt_TextChanged);
                cbbPhanLoai.SelectedValue = billOfLadingsDetail.PhanLoaiTachVD;
                cbbPhanLoai.TextChanged += new EventHandler(txt_TextChanged);

                cbbLoaiHH.TextChanged -= new EventHandler(txt_TextChanged);
                cbbLoaiHH.SelectedValue = billOfLadingsDetail.LoaiHang;
                cbbLoaiHH.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAddVDG_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormBillOfLadingsDetail(false))
                    return;
                if (billOfLadingsDetail == null)
                {
                    billOfLadingsDetail = new KDT_VNACCS_BillOfLadings_Detail();
                    isAddNew = true;
                }
                if (billOfLadingsDetail.Collection.Count == 0)
                {
                    if (!String.IsNullOrEmpty(txtSoVanDonGoc.Text.ToString()) && Convert.ToInt32(txtSoLuongVDN.Text.ToString()) >= 1)
                    {
                        //if (ShowMessage("Phần mềm Sẽ tự động Tách thành : " + txtSoLuongVDN.Text.ToString() + " vận đơn nhánh . Bạn có muốn phần mềm  Tách tự động không ?", true) == "Yes")
                        //{
                            int count = Convert.ToInt32(txtSoLuongVDN.Text.ToString());
                            for (int i = 1; i <= count; i++)
                            {
                                branchDetail = new KDT_VNACCS_BranchDetail();
                                branchDetail.STT = i;
                                branchDetail.SoVanDon = txtSoVanDonGoc.Text.ToString().Trim() + "-0" + i;
                                billOfLadingsDetail.Collection.Add(branchDetail);
                            }
                        //}
                        //else
                        //{
                        //    if (Convert.ToInt32(txtSoLuongVDN.Text) != billOfLadingsDetail.Collection.Count)
                        //    {
                        //        ShowMessage("Tổng số lượng Vận đơn nhánh bạn nhập vào và Số lượng Vận đơn nhánh ở Danh sách phía dưới là không bằng nhau .", false);
                        //        return;
                        //    }
                        //}
                    }
                }
                else
                {
                    if (Convert.ToInt32(txtSoLuongVDN.Text) != billOfLadingsDetail.Collection.Count)
                    {
                        ShowMessageTQDT(" Thông báo từ hệ thống ","TỔNG SỐ LƯỢNG VẬN ĐƠN NHÁNH DOANH NGHIỆP NHẬP VÀO VÀ SỐ LƯỢNG VẬN ĐƠN NHÁNH Ở DANH SÁCH PHÍA DƯỚI LÀ KHÔNG BẰNG NHAU .", false);
                        return;
                    }
                }
                if(billOfLadingsDetail.SoVanDonGoc==null)
                    isAddNew = true;
                billOfLadingsDetail.SoVanDonGoc = txtSoVanDonGoc.Text.ToString().Trim();
                billOfLadingsDetail.NgayVanDonGoc = clcNgay.Value;
                billOfLadingsDetail.MaNguoiPhatHanh = txtMaNguoiPH.Text;
                billOfLadingsDetail.SoLuongVDN = Convert.ToDecimal(txtSoLuongVDN.Text);
                billOfLadingsDetail.PhanLoaiTachVD = Convert.ToDecimal(cbbPhanLoai.SelectedValue);
                billOfLadingsDetail.LoaiHang = Convert.ToDecimal(cbbLoaiHH.SelectedValue);
                if (isAddNew)
                    billOfLadingNew.Collection.Add(billOfLadingsDetail);
                billOfLadingsDetail = new KDT_VNACCS_BillOfLadings_Detail();

                txtSoVanDonGoc.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDonGoc.Text = String.Empty;
                txtSoVanDonGoc.TextChanged += new EventHandler(txt_TextChanged);

                txtMaNguoiPH.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaNguoiPH.Text = String.Empty;
                txtMaNguoiPH.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuongVDN.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuongVDN.Text = String.Empty;
                txtSoLuongVDN.TextChanged += new EventHandler(txt_TextChanged);

                cbbPhanLoai.TextChanged -= new EventHandler(txt_TextChanged);
                cbbPhanLoai.SelectedValue = String.Empty;
                cbbPhanLoai.TextChanged += new EventHandler(txt_TextChanged);

                cbbLoaiHH.TextChanged -= new EventHandler(txt_TextChanged);
                cbbLoaiHH.SelectedValue = String.Empty;
                cbbLoaiHH.TextChanged += new EventHandler(txt_TextChanged);

                BinDataBillOfLadingsDetail();
                //BinDataBranchDetail();
                //BinDataContainer();
                branchDetail = null;
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteVDG_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessageTQDT(" Thông báo từ hệ thống ","DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListVDG.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            billOfLadingsDetail = (KDT_VNACCS_BillOfLadings_Detail)i.GetRow().DataRow;
                            billOfLadingNew.Collection.Remove(billOfLadingsDetail);
                            if (billOfLadingsDetail.ID > 0)
                            {
                                billOfLadingsDetail.DeleteFull();
                            }
                        }
                    }
                    txtSoVanDonGoc.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoVanDonGoc.Text = String.Empty;
                    txtSoVanDonGoc.TextChanged += new EventHandler(txt_TextChanged);

                    txtMaNguoiPH.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaNguoiPH.Text = String.Empty;
                    txtMaNguoiPH.TextChanged += new EventHandler(txt_TextChanged);

                    txtSoLuongVDN.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoLuongVDN.Text = String.Empty;
                    txtSoLuongVDN.TextChanged += new EventHandler(txt_TextChanged);

                    cbbPhanLoai.TextChanged -= new EventHandler(txt_TextChanged);
                    cbbPhanLoai.SelectedValue = String.Empty;
                    cbbPhanLoai.TextChanged += new EventHandler(txt_TextChanged);

                    cbbLoaiHH.TextChanged -= new EventHandler(txt_TextChanged);
                    cbbLoaiHH.SelectedValue = String.Empty;
                    cbbLoaiHH.TextChanged += new EventHandler(txt_TextChanged);

                    ShowMessageTQDT(" Thông báo từ hệ thống ","XÓA THÀNH CÔNG", false);
                    BinDataBillOfLadingsDetail();
                    //BinDataBranchDetail();
                    //BinDataContainer();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        //private bool ValidateFormBranchDetail(bool isOnlyWarning)
        //{
        //    bool isValid = true;
        //    try
        //    {
        //        isValid &= ValidateControl.ValidateNull(txtSTT, errorProvider, "Số thứ tự vận đơn nhánh", isOnlyWarning);
        //        isValid &= ValidateControl.ValidateNull(txtSoVanDonNhanh, errorProvider, "Số vận đơn nhánh", isOnlyWarning);
        //        isValid &= ValidateControl.ValidateNull(txtTenNguoiGuiHang, errorProvider, "Tên người gửi hàng của vận đơn nhánh", isOnlyWarning);
        //        isValid &= ValidateControl.ValidateNull(txtDiaChiNguoiGuiHang, errorProvider, "Địa chỉ người gửi hàng của vận đơn nhánh", isOnlyWarning);
        //        isValid &= ValidateControl.ValidateNull(txtTenNguoiNhanHang, errorProvider, "Tên người nhận hàng của vận đơn nhánh", isOnlyWarning);
        //        isValid &= ValidateControl.ValidateNull(txtDiaChiNguoiNhanHang, errorProvider, "Địa chỉ người nhận hàng của vận đơn nhánh", isOnlyWarning);
        //        isValid &= ValidateControl.ValidateNull(txtSoLuongHang, errorProvider, "Số lượng hàng", isOnlyWarning);
        //        isValid &= ValidateControl.ValidateNull(txtTongTrongLuongHang, errorProvider, "Tổng trọng lượng hàng", isOnlyWarning);
        //        if(cbbLoaiHH.SelectedValue.ToString()=="1")
        //            isValid &= ValidateControl.ValidateNull(txtSoLuongContainer, errorProvider, "Tổng số lượng container của vận đơn nhánh", isOnlyWarning);

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
        //    }
        //    finally { Cursor = Cursors.Default; }

        //    return isValid;
        //}
        //private void BinDataBranchDetail()
        //{
        //    try
        //    {
        //        dgListVDN.Refresh();
        //        dgListVDN.DataSource = billOfLadingsDetail.Collection;
        //        dgListVDN.Refetch();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //
        //    }
        //}
        //private void btnAddVDN_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        bool isAddNew = false;
        //        if (!ValidateFormBranchDetail(false))
        //            return;
        //        if (billOfLadingsDetail==null)
        //        {
        //            billOfLadingsDetail = new KDT_VNACCS_BillOfLadings_Detail();
        //            isAddNew = true;
        //        }
        //        if (branchDetail == null)
        //        {
        //            branchDetail = new KDT_VNACCS_BranchDetail();
        //            isAddNew = true;
        //        }
        //        if(billOfLadingsDetail.SoVanDonGoc == null)
        //            isAddNew = true;
        //        if (Convert.ToInt32(txtSoLuongContainer.Text) != branchDetail.Collection.Count)
        //        {
        //            ShowMessage("Tổng số lượng Container bạn nhập vào và Số lượng Container ở Danh sách phía dưới là không bằng nhau .",false);
        //            return;
        //        }
        //        branchDetail.STT = Convert.ToDecimal(txtSTT.Text.ToString());
        //        branchDetail.SoVanDon = txtSoVanDonNhanh.Text;
        //        branchDetail.TenNguoiGuiHang = txtTenNguoiGuiHang.Text;
        //        branchDetail.DiaChiNguoiGuiHang = txtDiaChiNguoiGuiHang.Text;
        //        branchDetail.TenNguoiNhanHang = txtTenNguoiNhanHang.Text;
        //        branchDetail.DiaChiNguoiNhanHang = txtDiaChiNguoiNhanHang.Text;
        //        branchDetail.TongSoLuongContainer = Convert.ToDecimal(txtSoLuongContainer.Text);
        //        branchDetail.SoLuongHang = Convert.ToDecimal(txtSoLuongHang.Text);
        //        branchDetail.DVTSoLuong = ctrDVTLuong1.Code;
        //        branchDetail.TongTrongLuongHang = Convert.ToDecimal(txtTongTrongLuongHang.Text.ToString());
        //        branchDetail.DVTTrongLuong = ctrMaDVTTrongLuong.Code;
        //        if (isAddNew)
        //            billOfLadingsDetail.Collection.Add(branchDetail);
        //        branchDetail = new KDT_VNACCS_BranchDetail();
        //        txtSTT.Text = String.Empty;
        //        txtSoVanDonNhanh.Text = String.Empty;
        //        txtTenNguoiGuiHang.Text = String.Empty;
        //        txtDiaChiNguoiGuiHang.Text = String.Empty;
        //        txtTenNguoiNhanHang.Text = String.Empty;
        //        txtDiaChiNguoiNhanHang.Text = String.Empty;
        //        txtSoLuongContainer.Text = String.Empty;
        //        //txtSoLuongHang.Text = String.Empty;
        //        //txtTongTrongLuongHang.Text = String.Empty;

        //        BinDataBranchDetail();
        //        BinDataContainer();
        //        branchDetail = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //
        //    }
        //}

        //private void btnDeleteVDN_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
        //        {
        //            GridEXSelectedItemCollection items = dgListVDN.SelectedItems;
        //            foreach (GridEXSelectedItem i in items)
        //            {
        //                if (i.RowType == RowType.Record)
        //                {
        //                    branchDetail = (KDT_VNACCS_BranchDetail)i.GetRow().DataRow;
        //                    billOfLadingsDetail.Collection.Remove(branchDetail);
        //                    int index = 1;
        //                    foreach (KDT_VNACCS_BranchDetail item in billOfLadingsDetail.Collection)
        //                    {
        //                        item.STT = index;
        //                        item.SoVanDon = txtSoVanDonGoc.Text.ToString() + "-0" + index;
        //                        index++;
        //                    }
        //                    if (branchDetail.ID > 0)
        //                    {
        //                        branchDetail.DeleteFull();
        //                    }
        //                }
        //            }
        //            txtSTT.Text = String.Empty;
        //            txtSoVanDonNhanh.Text = String.Empty;
        //            txtTenNguoiGuiHang.Text = String.Empty;
        //            txtDiaChiNguoiGuiHang.Text = String.Empty;
        //            txtTenNguoiNhanHang.Text = String.Empty;
        //            txtDiaChiNguoiNhanHang.Text = String.Empty;
        //            txtSoLuongContainer.Text = String.Empty;
        //            txtSoLuongHang.Text = String.Empty;
        //            txtTongTrongLuongHang.Text = String.Empty;
        //            ShowMessage("Xóa thành công", false);
        //            BinDataBranchDetail();
        //            BinDataContainer();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //
        //    }
        //}
        //private void BinDataContainer()
        //{
        //    try
        //    {
        //        dgListContainer.Refresh();
        //        dgListContainer.DataSource = branchDetail.Collection;
        //        dgListContainer.Refetch();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //
        //    }
        //}
        //private bool ValidateFormContainer(bool isOnlyWarning)
        //{
        //    bool isValid = true;
        //    try
        //    {
        //        isValid &= ValidateControl.ValidateNull(txtSoContainer, errorProvider, "Số Container", isOnlyWarning);
        //        isValid &= ValidateControl.ValidateNull(txtSoSeal, errorProvider, "Số Seal", isOnlyWarning);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
        //    }
        //    finally { Cursor = Cursors.Default; }

        //    return isValid;
        //}
        //private void btnAddContainer_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        bool isAddNew = false;
        //        if (txtSoContainer.Text.ToString() == "0")
        //        {
        //            ShowMessage("Tổng số lượng Container đang nhập vào 0 nên không thể nhập danh sách Container .", false);
        //            return;
        //        }
        //        if (cbbLoaiHH.SelectedValue.ToString()=="0")
        //        {
        //            ShowMessage("Loại hàng hóa đã chọn là : Hàng rời nên không thể nhập danh sách Container .", false);
        //            return;
        //        }
        //        if (!ValidateFormContainer(false))
        //            return;
        //        if (branchDetail ==null)
        //        {
        //            branchDetail = new KDT_VNACCS_BranchDetail();
        //            isAddNew = true;
        //        }
        //        if (transportEquipment == null)
        //        {
        //            transportEquipment = new KDT_VNACCS_BranchDetail_TransportEquipment();
        //            isAddNew = true;
        //        }
        //        transportEquipment.SoContainer = txtSoContainer.Text;
        //        transportEquipment.SoSeal = txtSoSeal.Text;
        //        if (isAddNew)
        //            branchDetail.Collection.Add(transportEquipment);
        //        transportEquipment = new KDT_VNACCS_BranchDetail_TransportEquipment();
        //        txtSoContainer.Text = String.Empty;
        //        txtSoSeal.Text = String.Empty;
        //        BinDataContainer();
        //        transportEquipment = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //
        //    }
        //}

        //private void btnDeleteContainer_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
        //        {
        //            GridEXSelectedItemCollection items = dgListContainer.SelectedItems;
        //            foreach (GridEXSelectedItem i in items)
        //            {
        //                if (i.RowType == RowType.Record)
        //                {
        //                    transportEquipment = (KDT_VNACCS_BranchDetail_TransportEquipment)i.GetRow().DataRow;
        //                    branchDetail.Collection.Remove(transportEquipment);
        //                    if (transportEquipment.ID > 0)
        //                    {
        //                        transportEquipment.Delete();
        //                    }
        //                }
        //            }
        //            txtSoContainer.Text = String.Empty;
        //            txtSoSeal.Text = String.Empty;
        //            ShowMessage("Xóa thành công", false);
        //            BinDataContainer();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //
        //    }
        //}

        //private void dgListContainer_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        //{
        //    try
        //    {
        //        transportEquipment = (KDT_VNACCS_BranchDetail_TransportEquipment)dgListContainer.GetRow().DataRow;
        //        txtSoContainer.Text = transportEquipment.SoContainer;
        //        txtSoSeal.Text = transportEquipment.SoSeal;
        //        BinDataContainer();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //
        //    }
        //}

        //private void dgListVDN_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        //{
        //    try
        //    {
        //        branchDetail = (KDT_VNACCS_BranchDetail)dgListVDN.GetRow().DataRow;
        //        txtSTT.Text = branchDetail.STT.ToString();
        //        txtSoVanDonNhanh.Text = branchDetail.SoVanDon;
        //        txtTenNguoiGuiHang.Text = branchDetail.TenNguoiGuiHang;
        //        txtDiaChiNguoiGuiHang.Text = branchDetail.DiaChiNguoiGuiHang;
        //        txtTenNguoiNhanHang.Text = branchDetail.TenNguoiNhanHang;
        //        txtDiaChiNguoiNhanHang.Text = branchDetail.DiaChiNguoiNhanHang;
        //        txtSoLuongContainer.Text = branchDetail.TongSoLuongContainer.ToString();
        //        txtSoLuongHang.Text = branchDetail.SoLuongHang.ToString();
        //        ctrDVTLuong1.Code = branchDetail.DVTSoLuong;
        //        txtTongTrongLuongHang.Text = branchDetail.TongTrongLuongHang.ToString();
        //        ctrMaDVTTrongLuong.Code = branchDetail.DVTTrongLuong;
        //        BinDataContainer();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //
        //    }
        //}

        private void dgListVDG_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                billOfLadingsDetail = (KDT_VNACCS_BillOfLadings_Detail)dgListVDG.GetRow().DataRow;
                SetData();
                VNACC_BranchDetailsForm f = new VNACC_BranchDetailsForm();
                f.billOfLadingsDetail = billOfLadingsDetail;
                f.billOfLadingNew = billOfLadingNew;
                f.ShowDialog(this);
                //BinDataBranchDetail();
                //BinDataContainer();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListVDG_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    int PhanLoai = Convert.ToInt32(e.Row.Cells["PhanLoaiTachVD"].Value);
                    int LoaiHang = Convert.ToInt32(e.Row.Cells["LoaiHang"].Value);
                    if (PhanLoai == 1)
                    {
                        e.Row.Cells["PhanLoaiTachVD"].Text = "Tách vận đơn cơ học ";
                    }
                    else
                    {
                        e.Row.Cells["PhanLoaiTachVD"].Text = "Tách vận đơn lý thuyết ";
                    }

                    if (LoaiHang == 1)
                    {
                        e.Row.Cells["LoaiHang"].Text = "Hàng container ";
                    }
                    else
                    {
                        e.Row.Cells["LoaiHang"].Text = "Hàng rời";
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send();
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedBack":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                default:
                    break;
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaDoanhNghiep, errorProvider, "MÃ DOANH NGHIỆP", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenDoanhNghiep, errorProvider, "TÊN DOANH NGHIỆP", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void GetBillOfLadingNew()
        {
            billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            billOfLadingNew.MaDoanhNghiep = txtMaDoanhNghiep.Text;
            billOfLadingNew.TenDoanhNghiep = txtTenDoanhNghiep.Text;
            billOfLadingNew.SoTN = Convert.ToInt64(txtSoTN.Text);
            billOfLadingNew.NgayTN = dateNgayTN.Value;
            billOfLadingNew.MaHQ = ctrCoQuanHaiQuan.Code;
        }
        public void SetBillOfLadingNew()
        {
            txtTenDoanhNghiep.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaDoanhNghiep.Text = billOfLadingNew.MaDoanhNghiep;
            txtTenDoanhNghiep.TextChanged += new EventHandler(txt_TextChanged);

            txtMaDoanhNghiep.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenDoanhNghiep.Text = billOfLadingNew.TenDoanhNghiep;
            txtMaDoanhNghiep.TextChanged += new EventHandler(txt_TextChanged);

            txtSoTN.Text = billOfLadingNew.SoTN.ToString();
            dateNgayTN.Value = billOfLadingNew.NgayTN;
            ctrCoQuanHaiQuan.Code = billOfLadingNew.MaHQ;
        }
        public void Save()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (billOfLadingNew.Collection.Count < 1)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ","DOANH NGHIỆP CHƯA THÊM DANH SÁCH VẬN ĐƠN GỐC ", false);
                    return;
                }
                GetBillOfLadingNew();
                billOfLadingNew.InsertUpdateFull();
                ShowMessageTQDT(" Thông báo từ hệ thống ","Lưu thành công", false);
                this.SetChange(false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        public void Send()
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ","Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (billOfLadingNew.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ","Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.BillOfLadingNew;
                sendXML.master_id = billOfLadingNew.ID;

                if (sendXML.Load())
                {

                        ShowMessageTQDT(" Thông báo từ hệ thống ","Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    billOfLadingNew.GuidStr = Guid.NewGuid().ToString();
                    BillOfLadingNew_VNACCS billOfLadingNew_VNACCS = new BillOfLadingNew_VNACCS();
                    billOfLadingNew_VNACCS = Mapper_V4.ToDataTransferBillOfLadingNew(billOfLadingNew);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = billOfLadingNew.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(billOfLadingNew.MaHQ)),
                              Identity = billOfLadingNew.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = billOfLadingNew_VNACCS.Issuer,
                              //Function = DeclarationFunction.KHAI_BAO,
                              Reference = billOfLadingNew.GuidStr,
                          },
                          billOfLadingNew_VNACCS
                        );

                    msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                    billOfLadingNew.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(billOfLadingNew.ID, MessageTitle.RegisterBillOfLadingNew);
                        cmdFeedBack.Enabled = cmdFeedBack.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.BillOfLadingNew;
                        sendXML.master_id = billOfLadingNew.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(billOfLadingNew_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            txtSoTN.Text = billOfLadingNew.SoTN.ToString();
                            clcNgay.Value = billOfLadingNew.NgayTN;
                            lblTrangThai.Text = "Đã duyệt";
                            billOfLadingNew.Update();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            Feedback();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = billOfLadingNew.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.BillOfLadingNew,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.BillOfLadingNew,
                };
                subjectBase.Type = DeclarationIssuer.BillOfLadingNew;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = billOfLadingNew.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(billOfLadingNew.MaHQ.Trim())),
                                                  Identity = billOfLadingNew.MaHQ
                                              }, subjectBase, null);
                if (billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(billOfLadingNew.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    SetBillOfLadingNew();
                }
            }
        }
        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = billOfLadingNew.ID;
                form.DeclarationIssuer = DeclarationIssuer.BillOfLadingNew;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.BillOfLadingNewSendHandler(billOfLadingNew, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_BillOfLadingNew", "", Convert.ToInt32(billOfLadingNew.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://youtu.be/IyLblq7FRCo");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;                
                ctrCoQuanHaiQuan.TagName = "CHS"; //Cơ quan Hải quan
                txtSoVanDonGoc.Tag = "CHB"; //Số vận đơn gốc
                txtSoLuongVDN.Tag = "YNK"; //Số lượng vận đơn nhánh
                clcNgay.Tag = "ICN"; //Ngày khai báo vận đơn
                cbbPhanLoai.Tag = "ICB"; //Phân loại tách vận đơn
                txtMaNguoiPH.Tag = "IDD"; //Mã người phát hành vận đơn
                cbbLoaiHH.Tag = "IPD"; //Loại hàng hóa
                //txt.TagName = "RED"; //Ghi chú
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi khi xử lý chương trình :\r\n" + ex.Message, false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private void VNACC_BillOfLadingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Khai báo Tách vận đơn có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.Save();
                }
            }
        }
    }
}
