-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Insert]
	@Master_ID bigint,
	@SoToKhaiXuat numeric(12, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TKVC_TKXuat]
(
	[Master_ID],
	[SoToKhaiXuat]
)
VALUES 
(
	@Master_ID,
	@SoToKhaiXuat
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Update]
	@ID bigint,
	@Master_ID bigint,
	@SoToKhaiXuat numeric(12, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TKVC_TKXuat]
SET
	[Master_ID] = @Master_ID,
	[SoToKhaiXuat] = @SoToKhaiXuat
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SoToKhaiXuat numeric(12, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TKVC_TKXuat] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TKVC_TKXuat] 
		SET
			[Master_ID] = @Master_ID,
			[SoToKhaiXuat] = @SoToKhaiXuat
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TKVC_TKXuat]
		(
			[Master_ID],
			[SoToKhaiXuat]
		)
		VALUES 
		(
			@Master_ID,
			@SoToKhaiXuat
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TKVC_TKXuat]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TKVC_TKXuat] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoToKhaiXuat]
FROM
	[dbo].[t_KDT_VNACC_TKVC_TKXuat]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SoToKhaiXuat]
FROM [dbo].[t_KDT_VNACC_TKVC_TKXuat] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoToKhaiXuat]
FROM
	[dbo].[t_KDT_VNACC_TKVC_TKXuat]	

GO

