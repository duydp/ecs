

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_GC_BC08TT74_VanDonToKhaiXuat_t_KDT_ToKhaiMauDich]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]'))
ALTER TABLE [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat] DROP CONSTRAINT [FK_t_GC_BC08TT74_VanDonToKhaiXuat_t_KDT_ToKhaiMauDich]
GO


/****** Object:  Table [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]    Script Date: 04/20/2013 15:25:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]') AND type in (N'U'))
DROP TABLE [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]
GO


/****** Object:  Table [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]    Script Date: 04/20/2013 15:25:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[SoVanDon] [varchar](50) NOT NULL,
	[NgayVanDon] [datetime] NULL,
	[IDHopDong] [bigint] NOT NULL,
	[Temp2] [nvarchar](255) NULL,
	[Temp3] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_GC_BC08TT74_VanDonToKhaiXuat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]  WITH CHECK ADD  CONSTRAINT [FK_t_GC_BC08TT74_VanDonToKhaiXuat_t_KDT_ToKhaiMauDich] FOREIGN KEY([TKMD_ID])
REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID])
GO

ALTER TABLE [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat] CHECK CONSTRAINT [FK_t_GC_BC08TT74_VanDonToKhaiXuat_t_KDT_ToKhaiMauDich]
GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Insert]

IF OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Update]

IF OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Delete]

IF OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Load]

IF OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectAll]

IF OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectBy_TKMD_ID]

IF OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_DeleteBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_DeleteBy_TKMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Insert]
	@TKMD_ID bigint,
	@SoVanDon varchar(50),
	@NgayVanDon datetime,
	@IDHopDong bigint,
	@Temp2 nvarchar(255),
	@Temp3 nvarchar(255)
AS
INSERT INTO [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]
(
	[TKMD_ID],
	[SoVanDon],
	[NgayVanDon],
	[IDHopDong],
	[Temp2],
	[Temp3]
)
VALUES
(
	@TKMD_ID,
	@SoVanDon,
	@NgayVanDon,
	@IDHopDong,
	@Temp2,
	@Temp3
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Update]
	@TKMD_ID bigint,
	@SoVanDon varchar(50),
	@NgayVanDon datetime,
	@IDHopDong bigint,
	@Temp2 nvarchar(255),
	@Temp3 nvarchar(255)
AS

UPDATE
	[dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]
SET
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[IDHopDong] = @IDHopDong,
	[Temp2] = @Temp2,
	[Temp3] = @Temp3
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_InsertUpdate]
	@TKMD_ID bigint,
	@SoVanDon varchar(50),
	@NgayVanDon datetime,
	@IDHopDong bigint,
	@Temp2 nvarchar(255),
	@Temp3 nvarchar(255)
AS
IF EXISTS(SELECT [TKMD_ID] FROM [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat] WHERE [TKMD_ID] = @TKMD_ID)
	BEGIN
		UPDATE
			[dbo].[t_GC_BC08TT74_VanDonToKhaiXuat] 
		SET
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[IDHopDong] = @IDHopDong,
			[Temp2] = @Temp2,
			[Temp3] = @Temp3
		WHERE
			[TKMD_ID] = @TKMD_ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]
	(
			[TKMD_ID],
			[SoVanDon],
			[NgayVanDon],
			[IDHopDong],
			[Temp2],
			[Temp3]
	)
	VALUES
	(
			@TKMD_ID,
			@SoVanDon,
			@NgayVanDon,
			@IDHopDong,
			@Temp2,
			@Temp3
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Delete]
	@TKMD_ID bigint
AS

DELETE FROM 
	[dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Load]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TKMD_ID],
	[SoVanDon],
	[NgayVanDon],
	[IDHopDong],
	[Temp2],
	[Temp3]
FROM
	[dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]
WHERE
	[TKMD_ID] = @TKMD_ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TKMD_ID],
	[SoVanDon],
	[NgayVanDon],
	[IDHopDong],
	[Temp2],
	[Temp3]
FROM
	[dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[TKMD_ID],
	[SoVanDon],
	[NgayVanDon],
	[IDHopDong],
	[Temp2],
	[Temp3]
FROM [dbo].[t_GC_BC08TT74_VanDonToKhaiXuat] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 18, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TKMD_ID],
	[SoVanDon],
	[NgayVanDon],
	[IDHopDong],
	[Temp2],
	[Temp3]
FROM
	[dbo].[t_GC_BC08TT74_VanDonToKhaiXuat]	

GO


/****** Object:  StoredProcedure [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]    Script Date: 04/20/2013 15:27:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_BC09HSTK_GC_TT117_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]
GO


/****** Object:  StoredProcedure [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]    Script Date: 04/20/2013 15:27:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Huynh Ngoc Khanh    
-- Create date:     18/04/2013
-- Description:     View BC 09/HSTK-TT117
-- =============================================    
CREATE PROCEDURE [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;   
  
 --DECLARE
 --@IDHopDong BIGINT,    
 --@MaHaiQuan CHAR(6),    
 --@MaDoanhNghiep VARCHAR(14) 
 --SET @IDHopDong = 721
 --SET @MaHaiQuan = 'N60C'
 --SET @MaDoanhNghiep = '4000395355'
 SELECT  BangKe.SoNgayToKhai, BangKe.TenHang, BangKe.SoLuong, BangKe.TriGia, t_KDT_GC_HopDong.SoHopDong + ', ' + CONVERT(NVARCHAR(20),     
                      t_KDT_GC_HopDong.NgayDangKy, 103) + ', ' + CONVERT(NVARCHAR(20), t_KDT_GC_HopDong.NgayHetHan, 103) AS HdPhuLuc, BangKe.CuaKhau,     
                      BangKe.SoNgayBL, t_KDT_GC_HopDong.ID AS IDHopDong, t_KDT_GC_HopDong.MaHaiQuan, t_KDT_GC_HopDong.MaDoanhNghiep     FROM
 (
 (SELECT  CONVERT(NVARCHAR(20), TKMD.SoToKhai) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  TKMD.NgayDangKy, 103) AS SoNgayToKhai, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
								  t_KDT_HangMauDich.TriGiaKB AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau,
								   CASE PTVT_ID 
								  WHEN '001' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103) 
								  WHEN '005' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103) 
								  ELSE CASE TKMD.SoVanDon 
								  WHEN '' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103)
								  ELSE
								   TKMD.SoVanDon + ', ' + CONVERT(NVARCHAR(20), TKMD.NgayVanDon, 103) 
								       END 
								   END AS SoNgayBL
			    FROM (SELECT ID,SoToKhai,NgayDangKy,MaLoaiHinh,CuaKhau_ID,SoVanDon,NgayVanDon,PTVT_ID
			            FROM t_KDT_ToKhaiMauDich  
				WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%' 
				AND t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong AND t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep 
				AND t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AS TKMD
	   INNER JOIN t_HaiQuan_CuaKhau ON t_HaiQuan_CuaKhau.ID = TKMD.CuaKhau_ID
	   INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_HaiQuan_LoaiHinhMauDich.ID = TKMD.MaLoaiHinh
	   INNER JOIN t_KDT_HangMauDich ON TKMD.ID = t_KDT_HangMauDich.TKMD_ID
	   LEFT  JOIN ( select * from t_GC_BC08TT74_VanDonToKhaiXuat WHERE t_GC_BC08TT74_VanDonToKhaiXuat.IDHopDong = @IDHopDong) t_GC_BC08TT74_VanDonToKhaiXuat  ON t_GC_BC08TT74_VanDonToKhaiXuat.TKMD_ID = TKMD.ID)
 UNION
 (SELECT CONVERT(NVARCHAR(20), TKCT.SoToKhai) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  TKCT.NgayDangKy, 103) AS SoNgayToKhai,t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, 
								  t_KDT_GC_HangChuyenTiep.TriGia AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau, '' AS SoNgayBL 
  FROM (SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep 
                WHERE  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = 721 AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE 'XGC%' OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE '%X')
						AND  t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep AND t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AS TKCT
	   INNER JOIN t_HaiQuan_CuaKhau ON t_HaiQuan_CuaKhau.ID = TKCT.CuaKhau_ID
	   INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_HaiQuan_LoaiHinhMauDich.ID = TKCT.MaLoaiHinh
	   INNER JOIN t_KDT_GC_HangChuyenTiep ON TKCT.ID = t_KDT_GC_HangChuyenTiep.Master_ID)
)	
AS BangKe    
INNER JOIN t_KDT_GC_HopDong ON t_KDT_GC_HopDong.ID = @IDHopDong
		

 
END
GO



/****** Object:  StoredProcedure [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectToKhai]    Script Date: 04/20/2013 15:29:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectToKhai]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectToKhai]
GO


/****** Object:  StoredProcedure [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectToKhai]    Script Date: 04/20/2013 15:29:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 16, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectToKhai]
	@HopDongID bigint
AS

SELECT TKMD.TKMD_ID,TKMD.SoToKhai,TKMD.MaLoaiHinh,TKMD.NgayDangKy,VanDonXuat.SoVanDon,VanDonXuat.NgayVanDon
FROM
	(SELECT * FROM  t_GC_BC08TT74_VanDonToKhaiXuat WHERE IDHopDong = @HopDongID)  VanDonXuat 
	RIGHT JOIN
	(SELECT t_KDT_ToKhaiMauDich.ID AS TKMD_ID, t_KDT_ToKhaiMauDich.SoToKhai AS SoToKhai,t_KDT_ToKhaiMauDich.MaLoaiHinh AS MaLoaiHinh,t_KDT_ToKhaiMauDich.NgayDangKy AS NgayDangKy
	   FROM t_KDT_ToKhaiMauDich  WHERE t_KDT_ToKhaiMauDich.IDHopDong = @HopDongID AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%') TKMD   
	ON TKMD.TKMD_ID = VanDonXuat.TKMD_ID 
	 

GO

UPDATE dbo.t_HaiQuan_Version SET [Version] = '9.5', [Date] = GETDATE(), Notes = N'Cap nhat mau bao cao 05/117'


