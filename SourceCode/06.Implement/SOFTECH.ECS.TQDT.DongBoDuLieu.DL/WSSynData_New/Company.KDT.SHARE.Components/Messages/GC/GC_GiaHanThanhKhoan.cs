﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;


namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class GC_GiaHanThanhKhoan : DeclarationBase
    {
        /// <summary>
        /// Hợp đồng
        /// </summary>
        [XmlElement("ContractReference")]
        public ContractDocument ContractReference { get; set; }


    }
}
