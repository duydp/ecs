﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.GC
{
    public partial class VanDonToKhaiXuat
    {

        public static DataTable SelectToKhai (long IDHongDong)
        {
            const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectToKhai]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDongID", SqlDbType.NVarChar, IDHongDong);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        



    }
}
