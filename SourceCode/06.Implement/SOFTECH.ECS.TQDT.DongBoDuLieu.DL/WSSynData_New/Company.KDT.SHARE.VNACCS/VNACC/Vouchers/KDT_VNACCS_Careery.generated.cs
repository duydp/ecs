﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_Careery : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public decimal LoaiNganhNghe { set; get; }
		public string ChuKySanXuat { set; get; }
		public long ManufactureFactory_ID { set; get; }

        public List<KDT_VNACCS_Careeries_Product> CareeriesProductCollection = new List<KDT_VNACCS_Careeries_Product>();
        public List<KDT_VNACCS_ProductionCapacity_Product> ProductionCapacityProductCollection = new List<KDT_VNACCS_ProductionCapacity_Product>();

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_Careery> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_Careery> collection = new List<KDT_VNACCS_Careery>();
			while (reader.Read())
			{
				KDT_VNACCS_Careery entity = new KDT_VNACCS_Careery();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiNganhNghe"))) entity.LoaiNganhNghe = reader.GetDecimal(reader.GetOrdinal("LoaiNganhNghe"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChuKySanXuat"))) entity.ChuKySanXuat = reader.GetString(reader.GetOrdinal("ChuKySanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ManufactureFactory_ID"))) entity.ManufactureFactory_ID = reader.GetInt64(reader.GetOrdinal("ManufactureFactory_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_Careery> collection, long id)
        {
            foreach (KDT_VNACCS_Careery item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_Careeries VALUES(@LoaiNganhNghe, @ChuKySanXuat, @ManufactureFactory_ID)";
            string update = "UPDATE t_KDT_VNACCS_Careeries SET LoaiNganhNghe = @LoaiNganhNghe, ChuKySanXuat = @ChuKySanXuat, ManufactureFactory_ID = @ManufactureFactory_ID WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_Careeries WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiNganhNghe", SqlDbType.Decimal, "LoaiNganhNghe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuKySanXuat", SqlDbType.NVarChar, "ChuKySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ManufactureFactory_ID", SqlDbType.BigInt, "ManufactureFactory_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiNganhNghe", SqlDbType.Decimal, "LoaiNganhNghe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuKySanXuat", SqlDbType.NVarChar, "ChuKySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ManufactureFactory_ID", SqlDbType.BigInt, "ManufactureFactory_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_Careeries VALUES(@LoaiNganhNghe, @ChuKySanXuat, @ManufactureFactory_ID)";
            string update = "UPDATE t_KDT_VNACCS_Careeries SET LoaiNganhNghe = @LoaiNganhNghe, ChuKySanXuat = @ChuKySanXuat, ManufactureFactory_ID = @ManufactureFactory_ID WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_Careeries WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiNganhNghe", SqlDbType.Decimal, "LoaiNganhNghe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuKySanXuat", SqlDbType.NVarChar, "ChuKySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ManufactureFactory_ID", SqlDbType.BigInt, "ManufactureFactory_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiNganhNghe", SqlDbType.Decimal, "LoaiNganhNghe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuKySanXuat", SqlDbType.NVarChar, "ChuKySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ManufactureFactory_ID", SqlDbType.BigInt, "ManufactureFactory_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_Careery Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_Careery_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_Careery> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_Careery> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_Careery> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_Careery> SelectCollectionBy_ManufactureFactory_ID(long manufactureFactory_ID)
		{
            IDataReader reader = SelectReaderBy_ManufactureFactory_ID(manufactureFactory_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_ManufactureFactory_ID(long manufactureFactory_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_Careery_SelectBy_ManufactureFactory_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ManufactureFactory_ID", SqlDbType.BigInt, manufactureFactory_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_Careery_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_Careery_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_Careery_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_Careery_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_ManufactureFactory_ID(long manufactureFactory_ID)
		{
			const string spName = "p_KDT_VNACCS_Careery_SelectBy_ManufactureFactory_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ManufactureFactory_ID", SqlDbType.BigInt, manufactureFactory_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_Careery(decimal loaiNganhNghe, string chuKySanXuat, long manufactureFactory_ID)
		{
			KDT_VNACCS_Careery entity = new KDT_VNACCS_Careery();	
			entity.LoaiNganhNghe = loaiNganhNghe;
			entity.ChuKySanXuat = chuKySanXuat;
			entity.ManufactureFactory_ID = manufactureFactory_ID;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_Careery_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LoaiNganhNghe", SqlDbType.Decimal, LoaiNganhNghe);
			db.AddInParameter(dbCommand, "@ChuKySanXuat", SqlDbType.NVarChar, ChuKySanXuat);
			db.AddInParameter(dbCommand, "@ManufactureFactory_ID", SqlDbType.BigInt, ManufactureFactory_ID);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_Careery> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_Careery item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_Careery(long id, decimal loaiNganhNghe, string chuKySanXuat, long manufactureFactory_ID)
		{
			KDT_VNACCS_Careery entity = new KDT_VNACCS_Careery();			
			entity.ID = id;
			entity.LoaiNganhNghe = loaiNganhNghe;
			entity.ChuKySanXuat = chuKySanXuat;
			entity.ManufactureFactory_ID = manufactureFactory_ID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_Careery_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LoaiNganhNghe", SqlDbType.Decimal, LoaiNganhNghe);
			db.AddInParameter(dbCommand, "@ChuKySanXuat", SqlDbType.NVarChar, ChuKySanXuat);
			db.AddInParameter(dbCommand, "@ManufactureFactory_ID", SqlDbType.BigInt, ManufactureFactory_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_Careery> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_Careery item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_Careery(long id, decimal loaiNganhNghe, string chuKySanXuat, long manufactureFactory_ID)
		{
			KDT_VNACCS_Careery entity = new KDT_VNACCS_Careery();			
			entity.ID = id;
			entity.LoaiNganhNghe = loaiNganhNghe;
			entity.ChuKySanXuat = chuKySanXuat;
			entity.ManufactureFactory_ID = manufactureFactory_ID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_Careery_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LoaiNganhNghe", SqlDbType.Decimal, LoaiNganhNghe);
			db.AddInParameter(dbCommand, "@ChuKySanXuat", SqlDbType.NVarChar, ChuKySanXuat);
			db.AddInParameter(dbCommand, "@ManufactureFactory_ID", SqlDbType.BigInt, ManufactureFactory_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_Careery> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_Careery item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int DeleteKDT_VNACCS_Careery(long id)
		{
			KDT_VNACCS_Careery entity = new KDT_VNACCS_Careery();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_Careery_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public  int DeleteBy_ManufactureFactory_ID(long manufactureFactory_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_Careery_DeleteBy_ManufactureFactory_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ManufactureFactory_ID", SqlDbType.BigInt, manufactureFactory_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_Careery_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_Careery> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_Careery item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert();
                    }
                    else
                    {
                        this.Update();
                    }

                    foreach (KDT_VNACCS_Careeries_Product item in CareeriesProductCollection)
                    {
                        item.Careeries_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                    foreach (KDT_VNACCS_ProductionCapacity_Product item in ProductionCapacityProductCollection)
                    {
                        item.Careeries_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                }

        }
        public void DeleteFull()
        {
            try
            {
                foreach (KDT_VNACCS_Careeries_Product item in CareeriesProductCollection)
                {
                    item.Careeries_ID = this.ID;
                    item.DeleteBy_Careeries_ID(this.ID);
                }
                foreach (KDT_VNACCS_ProductionCapacity_Product item in ProductionCapacityProductCollection)
                {
                    item.Careeries_ID = this.ID;
                    item.DeleteBy_Careeries_ID(this.ID);
                }
                this.DeleteBy_ManufactureFactory_ID(this.ID);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
            }
        }
	}	
}