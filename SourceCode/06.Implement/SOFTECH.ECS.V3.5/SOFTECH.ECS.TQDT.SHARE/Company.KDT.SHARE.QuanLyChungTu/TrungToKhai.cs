﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public class TrungToKhai
    {
        public static long CheckToKhaiTrung(long ID,string MaLoaiHinh, string SoGiayPhep,string SoVD, string SoHoaDon, string soHopDong,  decimal SoKien, decimal TrongLuong, decimal SoLuongHang, int SoMatHang)
        {
            long IDtrung = 0;
            //const string spName = "[dbo].CheckTrungSoTK";
            string sql = @"
                        SELECT ToKhai.ID AS ID
                        FROM t_kdt_hangmaudich AS HangMauDich INNER JOIN
                        (SELECT tktkmd.ID AS ID FROM t_KDT_ToKhaiMauDich tktkmd WHERE
                        tktkmd.SoVanDon = @SoVanDon AND
                        tktkmd.SoHoaDonThuongMai = @SoHoaDonThuongMai AND
                        tktkmd.SoHopDong = @SoHopDongThuongMai AND
                        tktkmd.SoGiayPhep = @SoGiayPhep AND 
                        tktkmd.SoKien = @SoKien AND
                        tktkmd.MaLoaiHinh = @MaLoaiHinh AND
                        tktkmd.ID <> @ID AND
                        tktkmd.TrongLuong = @TrongLuong) AS ToKhai
                        ON HangMauDich.TKMD_ID = ToKhai.ID
                        GROUP BY ToKhai.ID 
                        HAVING SUM(HangMauDich.SoLuong) = @SoLuongHang AND COUNT(HangMauDich.ID) = @SoMatHang";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVD);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, SoHoaDon);
            db.AddInParameter(dbCommand, "@SoHopDongThuongMai", SqlDbType.NVarChar, soHopDong);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, SoGiayPhep);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Int, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Real, TrongLuong);
            db.AddInParameter(dbCommand, "@SoLuongHang", SqlDbType.BigInt, SoLuongHang);
            db.AddInParameter(dbCommand, "@SoMatHang", SqlDbType.Int, SoMatHang);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) IDtrung = reader.GetInt64(reader.GetOrdinal("ID"));
                
            }
            return IDtrung;
        }
        public static long CheckSoToKhaiTrung(long SoToKhai,string MaHaiQuan,string MaLoaiHinh, string SoGiayPhep, string SoVD, string SoHoaDon, string soHopDong, decimal SoKien, decimal TrongLuong, decimal SoLuongHang, int SoMatHang)
        {
            long _SoToKhai = 0;
            //const string spName = "[dbo].CheckTrungSoTK";
            string sql = @"
                        SELECT MAX(ToKhai.SoToKhai) AS SoToKhai 
                        FROM t_kdt_hangmaudich AS HangMauDich INNER JOIN
                        (SELECT tktkmd.SoToKhai AS SoToKhai, tktkmd.ID AS ID FROM t_KDT_ToKhaiMauDich tktkmd WHERE
                        tktkmd.SoVanDon = @SoVanDon AND
                        tktkmd.SoHoaDonThuongMai = @SoHoaDonThuongMai AND
                        tktkmd.SoHopDong = @SoHopDongThuongMai AND
                        tktkmd.SoGiayPhep = @SoGiayPhep AND 
                        tktkmd.SoKien = @SoKien AND
                        tktkmd.MaLoaiHinh = @MaLoaiHinh AND
                        tktkmd.MaHaiQuan = @MaHaiQuan AND
                        tktkmd.SoToKhai > 0 AND
                        tktkmd.SoToKhai <> @SoToKhai AND
                        tktkmd.TrongLuong = @TrongLuong) AS ToKhai
                        ON HangMauDich.TKMD_ID = ToKhai.ID
                        GROUP BY ToKhai.ID 
                        HAVING SUM(HangMauDich.SoLuong) = @SoLuongHang AND COUNT(HangMauDich.ID) = @SoMatHang";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVD);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, SoHoaDon);
            db.AddInParameter(dbCommand, "@SoHopDongThuongMai", SqlDbType.NVarChar, soHopDong);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, SoGiayPhep);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Int, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Real, TrongLuong);
            db.AddInParameter(dbCommand, "@SoLuongHang", SqlDbType.BigInt, SoLuongHang);
            db.AddInParameter(dbCommand, "@SoMatHang", SqlDbType.Int, SoMatHang);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);

            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) _SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));

            }
            return _SoToKhai;
        }



    }
}
