﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using System.Data;
using Company.GC.BLL.KDT;
using System.IO;
using Company.Interface.KDT.SXXK;
using Company.Interface.GC;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using Company.Interface.Report.GC.TT39;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.QuanTri;
namespace Company.Interface.KDT.GC
{
    public partial class DinhMucGCSendForm : BaseForm
    {
        public DinhMucDangKy dmDangKy = new DinhMucDangKy() { TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO };
        HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public string LoaiDinhMuc;
        public bool isEdit = false;
        public bool isCancel = false;
        public LogHistory Log = new LogHistory();
        public DinhMucCollection DMCollectionCompare = new DinhMucCollection();
        public DinhMucCollection DMCollectionLog = new DinhMucCollection();
        public String Caption;
        public bool IsChange;
        //-----------------------------------------------------------------------------------------
        public DinhMucGCSendForm()
        {
            InitializeComponent();

        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            try
            {
                this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];

                cbbIsGC.TextChanged -= new EventHandler(txt_TextChanged);
                cbbIsGC.SelectedIndex = 0;
                cbbIsGC.TextChanged += new EventHandler(txt_TextChanged);

                ctrCoQuanHQ.Ma = GlobalSettings.MA_HAI_QUAN;
                if (dmDangKy.ID > 0)
                {
                    txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                    dateNgayTiepNhan.Value = dmDangKy.NgayTiepNhan;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        //-----------------------------------------------------------------------------------------
        private void DinhMucGCSendForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.khoitao_DuLieuChuan();

                dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
                dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
                //dgList.Tables[0].Columns["NPL_TuCungUng"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                Caption = this.Text;
                if (HD != null)
                {
                    HD.ID = dmDangKy.ID_HopDong;
                    HD = HopDong.Load(HD.ID);
                    dateNgayTiepNhan.Value = dmDangKy.NgayTiepNhan;
                    txtSoHopDong.Text = HD.SoHopDong;

                    cbbIsGC.TextChanged -= new EventHandler(txt_TextChanged);
                    cbbIsGC.SelectedValue = dmDangKy.LoaiDinhMuc;
                    cbbIsGC.TextChanged += new EventHandler(txt_TextChanged);

                    dmDangKy.LoadCollection();
                }
                if (dmDangKy.ID == 0)
                {
                    dateNgayTiepNhan.Value = DateTime.Now;

                    cbbIsGC.TextChanged -= new EventHandler(txt_TextChanged);
                    cbbIsGC.SelectedValue = "1";
                    cbbIsGC.TextChanged += new EventHandler(txt_TextChanged);

                    cbbIsGC.SelectedValue = "1";
                    if (isEdit)
                    {
                        dmDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else if (isCancel)
                    {
                        dmDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    }
                    else
                    {
                        dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    txtSoTiepNhan.Text = "100000000000";
                }
                else
                {
                    dateNgayTiepNhan.Value = dmDangKy.NgayTiepNhan;
                    cbbIsGC.TextChanged -= new EventHandler(txt_TextChanged);
                    cbbIsGC.SelectedValue = dmDangKy.LoaiDinhMuc;
                    cbbIsGC.TextChanged += new EventHandler(txt_TextChanged);
                    txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                }

                setCommandStatus();
                BindData();
                LoaiDinhMuc = cbbIsGC.SelectedValue.ToString();
                DMCollectionCompare = dmDangKy.DMCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void BindData()
        {
            dgList.Refresh();
            dgList.DataSource = dmDangKy.DMCollection;
            dgList.Refetch();
        }
        private void setCommandStatus()
        {
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemExcel.Enabled = cmdThemExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGUIDSTR.Enabled = cmdUpdateGUIDSTR1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                dateNgayTiepNhan.Value = dmDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdThemExcel.Enabled = cmdThemExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGUIDSTR.Enabled = cmdUpdateGUIDSTR1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = true;

                dateNgayTiepNhan.Value = DateTime.Now;
                lblTrangThai.Text = dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemExcel.Enabled = cmdThemExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGUIDSTR.Enabled = cmdUpdateGUIDSTR1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;

                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                dateNgayTiepNhan.Value = dmDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemExcel.Enabled = cmdThemExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;

                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGUIDSTR.Enabled = cmdUpdateGUIDSTR1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Đã khai báo", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdThemExcel.Enabled = cmdThemExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGUIDSTR.Enabled = cmdUpdateGUIDSTR1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = true;

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                dateNgayTiepNhan.Value = dmDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdThemExcel.Enabled = cmdThemExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGUIDSTR.Enabled = cmdUpdateGUIDSTR1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = true;

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                dateNgayTiepNhan.Value = dmDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }

        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Bind dữ liệu vào combobox hợp đồng
        /// </summary>
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới định mức.
        /// </summary>
        private void add()
        {
            try
            {
                DinhMucGCEditForm f = new DinhMucGCEditForm();
                f.dmdk = dmDangKy;
                f.LoaiDinhMuc = LoaiDinhMuc;
                f.OpenType = this.OpenType;
                f.isEdit = isEdit;
                f.isCancel = isCancel;
                f.ShowDialog();
                this.SetChange(f.IsChange);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }


        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                //e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    Company.GC.BLL.KDT.GC.DinhMuc dm = (Company.GC.BLL.KDT.GC.DinhMuc)e.Row.DataRow;
                    DinhMucGCEditForm f = new DinhMucGCEditForm();
                    f.dmdk = dmDangKy;
                    f.LoaiDinhMuc = LoaiDinhMuc;
                    f.isEdit = isEdit;
                    f.isCancel = isCancel;
                    f.DMDetail = dm;
                    f.OpenType = this.OpenType;
                    f.ShowDialog();
                    this.SetChange(f.IsChange);
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoHopDong, errorProvider, "SỐ HỢP ĐỒNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbIsGC, errorProvider, "LOẠI ĐỊNH MỨC", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void Save()
        {
            try
            {
                HD = HopDong.Load(HD.ID);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return;
            }
            if (!ValidateForm(false))
                return;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (dmDangKy.DMCollection.Count > 0)
                {
                    dmDangKy.SoTiepNhan = long.Parse(txtSoTiepNhan.Text);
                    this.dmDangKy.MaHaiQuan = HD.MaHaiQuan;
                    this.dmDangKy.NgayTiepNhan = dateNgayTiepNhan.Value;
                    this.dmDangKy.NamTN = dateNgayTiepNhan.Value.Year.ToString();
                    this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    dmDangKy.LoaiDinhMuc = Convert.ToInt32(cbbIsGC.SelectedValue.ToString());
                    dmDangKy.InsertUpdateFull();
                    setCommandStatus();
                    ShowMessageTQDT("Thông báo từ hệ thống", "Lưu thành công", false);
                    Log.LogDinhMuc(dmDangKy, MessageTitle.LuuThongTin, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    this.SetChange(false);
                }
                else
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "Danh sách định mức của sản phẩm chưa được nhập liệu", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void btnInDMSP_Click()
        {
            try
            {
                if (dmDangKy.DMCollection.Count > 0 && dmDangKy.ID > 0)
                {
                    Report.ReportViewBC03_DMDKForm reportForm = new Company.Interface.Report.ReportViewBC03_DMDKForm();
                    reportForm.DMDangKy = dmDangKy;
                    reportForm.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdSend":
                    this.SendV5(false);
                    break;
                case "cmdEdit":
                    this.SendV5(false);
                    break;
                case "HuyKhaiBao":
                    if (ShowMessage("Doanh nghiệp có chắc chắn muốn khai báo hủy định mức  này đến HQ không ? ", true) == "Yes")
                    {
                        dmDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        dmDangKy.Update();
                        setCommandStatus();
                        this.SendV5(true);
                    }
                    break;
                case "NhanDuLieu":
                    this.FeedBackV5();
                    break;
                case "XacNhan":
                    this.FeedBackV5();
                    break;
                case "cmdThemExcel":
                    this.AddExcel();
                    break;
                case "cmdCopyDM":
                    this.CopyDM();
                    break;
                case "DinhMuc":
                    this.btnInDMSP_Click();
                    break;
                case "TaoMoiDM":
                    this.TaoDanhSachDMMoi();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdSuaDM":
                    this.ChuyenTrangThaiSua();
                    break;
                case "cmdUpdateGUIDSTR":
                    this.UpdateGUIDSTR();
                    break;
                case "cmdResult":
                    this.btnResultHistory_Click(null, null);
                    break;
                case "cmdPrint":
                    this.Print();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
            }
        }

        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = dmDangKy.ID;
                f.loaiKhaiBao = LoaiKhaiBao.DinhMuc;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", dmDangKy.ID, LoaiKhaiBao.DinhMuc), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageDinhMuc(dmDangKy);
                    dmDangKy.InsertUpdateFull();
                    if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                    {
                        dmDangKy.TransferGC();
                    }
                    Log.LogDinhMuc(dmDangKy, MessageTitle.CapNhatKetQuaXuLy, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    ShowMessage("Cập nhật thông tin thành công .", false);
                    setCommandStatus();
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Print()
        {
            try
            {
                Report_16_ĐMTT_GSQL f = new Report_16_ĐMTT_GSQL();
                List<DinhMuc> DinhMucCollection = new List<DinhMuc>();
                List<KDT_GC_SanPham> SPCollection = KDT_GC_SanPham.SelectCollectionBy_HopDong_ID(dmDangKy.ID_HopDong);
                List<Company.GC.BLL.GC.KDT_GC_NguyenPhuLieu> NPLCollection = Company.GC.BLL.GC.KDT_GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(dmDangKy.ID_HopDong);
                foreach (DinhMuc dm in dmDangKy.DMCollection)
                {
                    dm.HopDong_ID = dmDangKy.ID_HopDong;
                    foreach (KDT_GC_SanPham SP in SPCollection)
                    {
                        if (SP.Ma == dm.MaSanPham)
                        {
                            dm.DVT = SP.DVT_ID;
                            dm.TenSanPham = SP.Ten;
                        }
                    }
                    foreach (Company.GC.BLL.GC.KDT_GC_NguyenPhuLieu NPL in NPLCollection)
                    {
                        if (NPL.Ma == dm.MaNguyenPhuLieu)
                        {
                            dm.TenNPL = NPL.Ten;
                            dm.DVT_ID = NPL.DVT_ID;
                        }
                    }
                    DinhMucCollection.Add(dm);
                }
                f.BindReport(DinhMucCollection, dmDangKy.NgayTiepNhan, dmDangKy.NgayTiepNhan);
                f.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                sendXML.master_id = dmDangKy.ID;
                if (!sendXML.Load())
                {
                    sendXML.msg = String.Empty;
                    sendXML.func = Convert.ToInt32(DeclarationFunction.KHAI_BAO);
                    sendXML.InsertUpdate();
                }
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_GC_DinhMucDangKy", "", Convert.ToInt32(dmDangKy.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void inPhieuTN()
        {
            try
            {
                if (this.dmDangKy.SoTiepNhan == 0) return;
                Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
                phieuTN.phieu = "ĐỊNH MỨC";
                phieuTN.soTN = this.dmDangKy.SoTiepNhan.ToString();
                phieuTN.ngayTN = this.dmDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
                phieuTN.maHaiQuan = dmDangKy.MaHaiQuan;
                phieuTN.BindReport();
                phieuTN.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void TaoDanhSachDMMoi()
        {
            try
            {
                foreach (DinhMuc dm in dmDangKy.DMCollection)
                {
                    if (dm.ID == 0)
                    {
                        dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        dmDangKy.MaHaiQuan = HD.MaHaiQuan;
                        if (this.ShowMessageTQDT("Thông báo từ hệ thống", "Doanh nghiệp có muốn Lưu định mức hiện tại lại không?", true) == "Yes")
                        {
                            try
                            {
                                dmDangKy.InsertUpdateFull();
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                                //showMsg("MSG_2702004", ex.Message);
                            }
                        }
                        else
                            break;
                    }
                }
                dmDangKy.DMCollection.Clear();
                dmDangKy.ID = 0;
                dmDangKy.NgayTiepNhan = new DateTime(1900, 1, 1);
                dmDangKy.SoTiepNhan = 0;
                dmDangKy.TrangThaiXuLy = -1;
                txtSoTiepNhan.Text = "0";
                BindData();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void CopyDM()
        {
            try
            {
                DinhMucGCRegistedForm f = new DinhMucGCRegistedForm();
                f.ShowDialog();
                if (f.idHD != 0)
                {
                    long idHopDong = f.idHD;
                    DinhMuc dm = new DinhMuc();
                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                    sp.HopDong_ID = HD.ID;
                    Company.GC.BLL.GC.SanPhamCollection spcol = new Company.GC.BLL.GC.SanPhamCollection();
                    spcol = sp.SelectCollectionBy_HopDong_ID_ChuaCoDM();
                    DinhMucCollection dmCollection = new DinhMucCollection();
                    dmCollection = dm.copyDinhMuc(idHopDong, spcol);
                    if (dmCollection.Count == 0)
                    {
                        ShowMessageTQDT("Thông báo từ hệ thống", "Danh sách khai báo định mức rỗng?", true);
                        //showMsg("MSG_2702045");
                    }
                    else
                    {
                        dmDangKy.DMCollection = dmCollection;
                        BindData();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void AddExcel()
        {
            try
            {
                ImportKDTDMForm f = new ImportKDTDMForm();
                f.HD = this.HD;
                f.dmDangKy = this.dmDangKy;
                f.ShowDialog();
                this.SetChange(f.ImportExcelSucces);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                btnXoa_Click_1(null, null);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void DinhMucGCSendForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Định mức có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.Save();
                }
            }
        }

        private void btnXoa_Click_1(object sender, EventArgs e)
        {
            try
            {
                Company.GC.BLL.KDT.GC.DinhMucCollection dmDeleteColl = new DinhMucCollection();
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count == 0) return;
                if (ShowMessage("Doanh nghiệp có muốn xóa định mức này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        if (item.RowType == RowType.Record)
                        {
                            Company.GC.BLL.KDT.GC.DinhMuc dmDelte = (Company.GC.BLL.KDT.GC.DinhMuc)item.GetRow().DataRow;
                            Company.GC.BLL.GC.GC_DinhMuc dmRegister = new Company.GC.BLL.GC.GC_DinhMuc();
                            if (dmDelte.ID > 0)
                            {
                                dmDelte.Delete(HD.ID);
                            }
                            dmDeleteColl.Add(dmDelte);
                        }

                    }
                    foreach (DinhMuc dm in dmDeleteColl)
                    {
                        if (dm.ID > 0)
                            dm.DeleteByMasterID(dmDangKy.ID);
                        dmDangKy.DMCollection.Remove(dm);
                    }
                    Log.LogDinhMuc(dmDangKy, dmDeleteColl, MessageTitle.XoaDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    BindData();
                    ShowMessage("Xóa thành công", false);
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void DeleteDM()
        {
            try
            {
                //if (showMsg("MSG_DEL01", true) == "Yes")
                //{
                if (ShowMessage("Doanh nghiệp có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXRow[] items = dgList.GetCheckedRows();
                    foreach (GridEXRow row in items)
                    {
                        if (row.RowType == RowType.Record)
                        {
                            Company.GC.BLL.KDT.GC.DinhMuc dmDelte = (Company.GC.BLL.KDT.GC.DinhMuc)row.DataRow;
                            Company.GC.BLL.GC.GC_DinhMuc dmRegister = new Company.GC.BLL.GC.GC_DinhMuc();
                            if (dmDelte.ID > 0)
                            {
                                dmDelte.Delete(HD.ID);
                            }
                            dmDangKy.DMCollection.Remove(dmDelte);
                        }
                    }
                    BindData();
                    ShowMessage("Xóa thành công", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = dmDangKy.ID;
                form.DeclarationIssuer = DeclarationIssuer.GC_DINH_MUC;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        #region EIDT - 06/09/2017
        private void SendV5(bool isCancel)
        {
            if (dmDangKy.DMCollection.Count == 0)
            {
                ShowMessage("Doanh nghiệp chưa nhập định mức", false);
                //showMsg("MSG_2702043");
                return;
            }
            HD = HopDong.Load(HD.ID);
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                ShowMessage("Hợp đồng nay chưa được khai báo đến hải quan . Doanh nghiệp hãy khai báo hợp đồng trước khi khai định mức", false);
                //showMsg("MSG_240235");
                return;
            }
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (sendXML.Load())
            {
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "Khai báo đã được gửi đến hải quan. nhấn nút [Lấy phản hồi] để nhận thông tin từ hải quan", false);
                    return;
                }
            }
            try
            {
                dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                switch (dmDangKy.TrangThaiXuLy)
                {
                    case -1:
                        Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        break;
                    case -5:
                        Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHuyDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        break;
                    case 5:
                        Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoDinhMucSua, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        break;
                    default:
                        break;
                }
                Company.KDT.SHARE.Components.GC_DinhMuc dm = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferDinhMuc(dmDangKy, HD, GlobalSettings.TEN_DON_VI, isCancel);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = dmDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan.Trim()

                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_DINH_MUC,
                                    Function = dm.Function,
                                    Reference = dmDangKy.GUIDSTR,
                                }
                                ,
                                dm);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoDinhMuc);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                    sendXML.master_id = dmDangKy.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.func = Convert.ToInt32(dm.Function);
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQHuyDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            if (dmDangKy.LoaiDinhMuc != 0 && GlobalSettings.MA_DON_VI != "4000395355")
                            {
                                dmDangKy.DeleteDinhMucGC();
                            }
                        }
                        else
                        {
                            Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQDuyetDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            if (dmDangKy.LoaiDinhMuc != 0 && GlobalSettings.MA_DON_VI != "4000395355")
                            {
                                dmDangKy.TransferGC();
                            }
                        }
                        dmDangKy.Update();
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        setCommandStatus();
                        FeedBackV5();
                    }
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                {
                    Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQTuChoiDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    sendForm.Message.XmlSaveMessage(dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQTuChoiDinhMuc);
                    ShowMessageTQDT(msgInfor, false);
                    setCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(dmDangKy.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhMucSendHandler(dmDangKy, ref msgInfor, e);

        }
        private void FeedBackV5()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (!sendXML.Load())
            {
                if (ShowMessageTQDT("Thông báo từ hệ thống", "Thông tin chưa được gửi đến hải quan hoặc Hệ thống HQ bị quá tải không nhận được phản hồi . Nếu trường hợp hệ thống HQ bị quá tải Bạn chọn Yes để tạo Message nhận phản hồi . Chọn No để bỏ qua", true) == "Yes")
                {
                    sendXML.msg = String.Empty;
                    sendXML.func = Convert.ToInt32(DeclarationFunction.KHAI_BAO);
                    sendXML.InsertUpdate();
                }
                else
                {
                    return;
                }
            }
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_DINH_MUC,
                Reference = dmDangKy.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_DINH_MUC,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = dmDangKy.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan.Trim()),
                                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan.Trim()
                                          }, subjectBase, null);
            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoLayPhanHoiDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQHuyDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            if (dmDangKy.LoaiDinhMuc != 0 && GlobalSettings.MA_DON_VI != "4000395355")
                            {
                                dmDangKy.DeleteDinhMucGC();
                            }
                        }
                        else
                        {
                            Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQDuyetDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            if (dmDangKy.LoaiDinhMuc != 0 && GlobalSettings.MA_DON_VI != "4000395355")
                            {
                                dmDangKy.TransferGC();
                            }
                        }
                        dmDangKy.Update();
                        isFeedBack = false;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQTuChoiDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        dmDangKy.Update();
                        isFeedBack = false;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }
        private void CanceldV5()
        {

            if (dmDangKy.DMCollection.Count == 0)
            {
                ShowMessage("Doanh nghiệp chưa nhập thông tin định mức", false);
                //showMsg("MSG_2702043");
                return;
            }
            HD = HopDong.Load(HD.ID);

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                ShowMessage("Hợp đồng nay chưa được khai báo đến hải quan . Doanh nghiệp hãy khai báo hợp đồng trước khi khai định mức", false);
                //showMsg("MSG_240235");
                return;
            }
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            try
            {
                dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                Company.KDT.SHARE.Components.GC_DinhMuc dm = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferDinhMuc(dmDangKy, HD, GlobalSettings.TEN_DON_VI, true);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = dmDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan.Trim()

                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_DINH_MUC,
                                    Function = dm.Function,
                                    Reference = dmDangKy.GUIDSTR,
                                }
                                ,
                                dm);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                        sendForm.Message.XmlSaveMessage(dmDangKy.ID, MessageTitle.KhaiBaoDinhMucSua);
                    //minhnd 20/03/2015
                    else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        sendForm.Message.XmlSaveMessage(dmDangKy.ID, MessageTitle.HuyToKhai);
                    //minhnd 20/03/2015
                    else
                        sendForm.Message.XmlSaveMessage(dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoDinhMuc);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                    sendXML.master_id = dmDangKy.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    setCommandStatus();
                    dmDangKy.Update();
                    FeedBackV5();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                {
                    setCommandStatus();
                    sendForm.Message.XmlSaveMessage(dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQTuChoiDinhMuc);
                    ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(dmDangKy.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
            //minhnd 20/03/2015
        }

        #endregion
        private bool checkQuyetToan(DinhMucDangKy dmdk)
        {
            try
            {
                DinhMucDangKy dm = new DinhMucDangKy();
                if (dm.CheckQuyetToan(dmDangKy.ID).Rows.Count > 0)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return true;

        }
        private void ChuyenTrangThaiSua()
        {
            try
            {
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    if (checkQuyetToan(dmDangKy))
                    {
                        string msg = "";
                        msg += "---------THÔNG TIN ĐỊNH MỨC ĐÃ KHAI BÁO---------";
                        msg += "\nSỐ TIẾP NHẬN : " + dmDangKy.SoTiepNhan.ToString();
                        msg += "\nNGÀY TIẾP NHẬN : " + dmDangKy.NgayTiepNhan.ToString("dd/MM/yy hh:mm:ss");
                        msg += "\nHẢI QUAN TIẾP NHẬN : " + dmDangKy.MaHaiQuan.ToString();
                        msg += "\n----------------THÔNG TIN XÁC NHẬN----------------";
                        msg += "\nDOANH NGHIỆP CÓ MUỐN CHUYỂN SANG KHAI BÁO SỬA KHÔNG ?";
                        if (ShowMessageTQDT(" Thông báo từ hệ thống ", msg, true) == "Yes")
                        {
                            dmDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                            dmDangKy.InsertUpdate();
                            dmDangKy.DeleteDinhMucGC();
                            setCommandStatus();
                            Log.LogDinhMuc(dmDangKy, MessageTitle.ChuyenKhaiBaoSua, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            this.SetChange(true);
                        }
                    }
                    else
                    {
                        ShowMessageTQDT(" Thông báo từ hệ thống ", "Định mức của hợp đồng này đã được quyết toán.(không được phép sửa)", false);
                    }

                }
                else
                    ShowMessageTQDT(" Thông báo từ hệ thống ", "Định mức chưa được duyệt", false);
                this.setCommandStatus();
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void cbbIsGC_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbbIsGC.SelectedValue.ToString() == "1")
            {
                LoaiDinhMuc = "1";
            }
            else
            {
                LoaiDinhMuc = "0";
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearchNPL_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["MaNguyenPhuLieu"], ConditionOperator.Contains, txtMaNPL.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

    }
}