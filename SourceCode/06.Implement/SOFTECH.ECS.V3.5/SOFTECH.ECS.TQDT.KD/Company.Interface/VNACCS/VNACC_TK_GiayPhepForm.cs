﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TK_GiayPhepForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        DataTable dt = new DataTable();
        DataSet dsPhanLoai = new DataSet();
        public String Caption;
        public bool IsChange;
        public bool IsChanged;
        public VNACC_TK_GiayPhepForm()
        {
            InitializeComponent();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void VNACC_TK_GiayPhepForm_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                LoadData();
                Caption = this.Text;
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
                {
                    btnSave.Enabled = false;
                    btnXoa.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
            
        }
        private void LoadData()
        {
            try
            {
                //A528: Mã phân loại giấy phép
                dsPhanLoai = VNACC_Category_Common.SelectDynamic("ReferenceDB='A528'", "");
                grList.DropDowns["drpPhanLoai"].DataSource = dsPhanLoai.Tables[0];
                dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.GiayPhepCollection);
                grList.DataSource = dt;

                //grList.DropDowns["drpPhanLoai"].TextChanged -= new EventHandler(txt_TextChanged);
                //grList.DropDowns["drpPhanLoai"].AddingRecord += new CancelEventHandler(txt_TextChanged);
                grList.AddingRecord += new CancelEventHandler(txt_TextChanged);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int sott = 1;
                TKMD.GiayPhepCollection.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow i in dt.Rows)
                {
                    KDT_VNACC_TK_GiayPhep gp = new KDT_VNACC_TK_GiayPhep();
                    if (i["ID"].ToString().Length != 0)
                    {
                        gp.ID = Convert.ToInt32(i["ID"].ToString());
                        gp.TKMD_ID = Convert.ToInt32(i["TKMD_ID"].ToString());
                    }
                    gp.SoTT = sott;
                    gp.PhanLoai = i["PhanLoai"].ToString();
                    gp.SoGiayPhep = i["SoGiayPhep"].ToString();
                    TKMD.GiayPhepCollection.Add(gp);
                    sott++;
                }
                ShowMessage("Lưu thành công", false);
                this.SetChange(true);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                this.Cursor = Cursors.Default;
            }

        }

        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa giấy phép này không?", true) == "Yes")
                    {
                        KDT_VNACC_TK_GiayPhep.DeleteDynamic("ID=" + ID);
                        grList.CurrentRow.Delete();
                        this.SetChange(true);
                    }
                }
                else
                    grList.CurrentRow.Delete();
                grList.Refetch();
                this.SetChange(true);

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                this.Cursor = Cursors.Default;
            }

        }

        private void VNACC_TK_GiayPhepForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Giấy phép có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    btnSave_Click(null,null);
                }
            }
        }

    }
}
