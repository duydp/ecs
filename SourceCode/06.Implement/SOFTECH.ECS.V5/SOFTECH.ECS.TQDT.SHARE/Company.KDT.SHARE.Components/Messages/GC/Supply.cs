﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class Supply
    {
        /// <summary>
        /// Nguyên phụ liệu
        /// </summary>
        [XmlElement("Materials")]
        public Product Material { get; set; }
        /// <summary>
        /// Chi tiết nguyên phụ liệu cung ứng
        /// </summary>
        [XmlElement("SupplyItem")]
        public List<SupplyItem> SupplyItems { get; set; }

    }
}
