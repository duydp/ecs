﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using KetQuaXuLy = Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3.KetQuaXuLy;

namespace Company.Interface.DongBoDuLieu
{
    public partial class KetQuaXuLyForm : BaseForm
    {
        private List<KetQuaXuLy> ListKetQua = new List<KetQuaXuLy>();
        public KetQuaXuLyForm()
        {
            InitializeComponent();
        }

        private void KetQuaXuLyForm_Load(object sender, EventArgs e)
        {
            ccFromDate.Text = DateTime.Now.ToString();
            ccToDate.Text = DateTime.Now.ToString();
            string where = string.Empty;
            where = "MaDoanhNghiep = " + GlobalSettings.MA_DON_VI;
            ListKetQua = KetQuaXuLy.SelectCollectionDynamic(where, "NgayDongBo desc");
            dgList.DataSource = ListKetQua;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refetch();
            }
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string where = string.Empty;
            where = "NgayDongBo BETWEEN " + ccFromDate.Value.ToShortDateString() + " AND " + ccToDate.Value.ToShortDateString();
            ListKetQua = KetQuaXuLy.SelectCollectionDynamic(where, "NgayDongBo desc");
            dgList.DataSource = ListKetQua;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refetch();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TrangThai"].Value == "0")
                    e.Row.Cells["TrangThai"].Text = "Tìm Kiếm";
                else if (e.Row.Cells["TrangThai"].Text == "1")
                    e.Row.Cells["TrangThai"].Text = "Gửi";
                else if (e.Row.Cells["TrangThai"].Text == "2")
                    e.Row.Cells["TrangThai"].Text = "Nhận";
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            string thongbao = string.Empty;
            thongbao = string.Format("Số tờ khai: {0}\nNgày đồng bộ: {1}\nTrạng thái gửi: {2}\nChi tiết lỗi: {3}",
                e.Row.Cells["SoToKhai"].Text,e.Row.Cells["NgayDongBo"].Text,e.Row.Cells["TrangThai"].Text,e.Row.Cells["GhiChu"].Text);
            ShowMessageTQDT(thongbao, false);
        }

       

    }
}
