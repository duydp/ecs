﻿using System;
using Company.BLL.KDT;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class NoiDungChinhSuaTKForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        public NoiDungDieuChinhTK noiDungDCTK = new NoiDungDieuChinhTK();
        public List<NoiDungDieuChinhTK> ListNoiDungDieuChinhTK = new List<NoiDungDieuChinhTK>();
        public List<NoiDungDieuChinhTKDetail> ListNoiDungDieuChinhTKChiTiet = new List<NoiDungDieuChinhTKDetail>();
        int i;
        //-----------------------------------------------------------------------------------------

        public NoiDungChinhSuaTKForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        public void BindData()
        {
            dgList.DataSource = ListNoiDungDieuChinhTK;

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                switch (Convert.ToInt32(e.Row.Cells["TrangThai"].Value))
                {
                    case -1:
                        e.Row.Cells["TrangThai"].Text = setText("Chưa khai báo", "Not declared yet");
                        break;
                    case 0:
                        e.Row.Cells["TrangThai"].Text = setText("Chờ duyệt", "Wait for approval");
                        break;
                    case 1:
                        e.Row.Cells["TrangThai"].Text = setText("Đã duyệt", "Approved");
                        break;
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa nội dung này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NoiDungDieuChinhTK nddctk = new NoiDungDieuChinhTK();
                        nddctk = (NoiDungDieuChinhTK)i.GetRow().DataRow;
                        if (nddctk == null) continue;

                        if (nddctk.ID > 0)
                        {
                            NoiDungDieuChinhTKDetail.DeleteBy_Id_DieuChinh(nddctk.ID);
                            nddctk.Delete();
                        }

                        TKMD.NoiDungDieuChinhTKCollection.Remove(nddctk);
                    }
                }
            }
            //BindData();
            dgList.DataSource = TKMD.NoiDungDieuChinhTKCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            //NoiDungDieuChinhTK noiDungDieuChinhTK = new NoiDungDieuChinhTK();
            //noiDungDieuChinhTK.SoDieuChinh = ++i;
            //noiDungDieuChinhTK.NgaySua = DateTime.Now;
            //noiDungDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
            //ListNoiDungDieuChinhTK.Add(noiDungDieuChinhTK);
            //dgList.DataSource = ListNoiDungDieuChinhTK;
            //try
            //{
            //    dgList.Refetch();
            //}
            //catch
            //{
            //    dgList.Refresh();
            //}
            i++;
            NoiDungChinhSuaTKDetailForm ndForm = new NoiDungChinhSuaTKDetailForm();
            ndForm.TKMD = TKMD;
            ndForm.isKhaiBoSung = this.isKhaiBoSung;
            ndForm.noiDungDieuChinhTK.SoDieuChinh = i;
            ndForm.ShowDialog(this);

            dgList.DataSource = TKMD.NoiDungDieuChinhTKCollection;
            try
            { dgList.Refetch(); }
            catch
            { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            NoiDungChinhSuaTKDetailForm noiDungChinhSuaTKDetailForm = new NoiDungChinhSuaTKDetailForm();
            noiDungChinhSuaTKDetailForm.TKMD = TKMD;
            noiDungChinhSuaTKDetailForm.noiDungDieuChinhTK.ID = Convert.ToInt32(e.Row.Cells["ID"].Value);
            noiDungChinhSuaTKDetailForm.noiDungDieuChinhTK.TrangThai = Convert.ToInt32(e.Row.Cells["TrangThai"].Value);
            noiDungChinhSuaTKDetailForm.noiDungDieuChinhTK.SoDieuChinh = Convert.ToInt32(e.Row.Cells["SoDieuChinh"].Value);
            noiDungChinhSuaTKDetailForm.ShowDialog(this);

            dgList.DataSource = TKMD.NoiDungDieuChinhTKCollection;
            try
            { dgList.Refetch(); }
            catch
            { dgList.Refresh(); }
        }

        private void NoiDungChinhSuaTKForm_Load(object sender, EventArgs e)
        {
            i = NoiDungDieuChinhTK.SelectMaxSoDieuChinh2(TKMD.ID, TKMD.MaLoaiHinh);
            string where = "TKMD_ID = " + TKMD.ID + " AND MALOAIHINH = '" + TKMD.MaLoaiHinh.Trim() + "' ";
            TKMD.NoiDungDieuChinhTKCollection = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
            dgList.DataSource = TKMD.NoiDungDieuChinhTKCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            if (noiDungDCTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET || noiDungDCTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                btnTaoMoi.Enabled = false;
                btnXoa.Enabled = false;
            }
            //BindData();
        }



    }
}
