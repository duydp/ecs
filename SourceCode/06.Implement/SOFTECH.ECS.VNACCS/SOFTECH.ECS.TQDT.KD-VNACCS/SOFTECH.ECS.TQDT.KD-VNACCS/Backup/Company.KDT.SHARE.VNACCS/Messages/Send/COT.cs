﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.Messages.Send;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class COT : BasicVNACC
    {
        public List<COT_HANG> listCont { get; set; }

        public StringBuilder BuilEdiMessagesOLA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<COT>(StrBuild, true, GlobalVNACC.PathConfig, EnumNghiepVu.COT);
            foreach (COT_HANG item in listCont)
            {
                item.BuildEdiMessages<COT_HANG>(StrBuild, true, GlobalVNACC.PathConfig, "COT_HANG");
            }
            return str;
        }

    }
}
