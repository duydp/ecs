﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.Interface.Report.SXXK;
using Company.BLL.SXXK.ThanhKhoan;

namespace Company.Interface.SXXK
{
    public partial class TriGiaHangToKhaiNhapForm : Company.Interface.BaseForm
    {
        private  DataTable dt = new DataTable();
        public TriGiaHangToKhaiNhapForm()
        {
            InitializeComponent();
        }

        private void ToKhaiSapHetHanForm_Load(object sender, EventArgs e)
        {

            ccFromDate.Value = new DateTime(DateTime.Today.Year, 1, 1);
            ccToDate.Value = DateTime.Today;
            this.Search();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.Search();
        }

        private void Search()
        {
            DateTime fromDate = new DateTime(1900, 1, 1);
            DateTime toDate = new DateTime(2100, 1, 1);
            if (ccFromDate.Text.Trim() != "") fromDate = ccFromDate.Value;
            if (ccToDate.Text.Trim() != "") toDate = ccToDate.Value;
            if (fromDate > toDate)
            {
                ShowMessage("Từ ngày phải nhỏ hơn đến ngày.", false);
                return;
            }
            this.dt = new NPLNhapTon().SelectTriGiaHangToKhaiNK(fromDate, toDate, txtTenChuHang.Text, GlobalSettings.MA_DON_VI).Tables[0];
            dgList.DataSource = this.dt;
        }

        private void btnXuatBaoCao_Click(object sender, EventArgs e)
        {
            ReportTriGiaToKhaiNhap report = new ReportTriGiaToKhaiNhap();
            report.BindReport(this.dt, ccFromDate.Value, ccToDate.Value);
            report.ShowPreview();
        }



   }
}

