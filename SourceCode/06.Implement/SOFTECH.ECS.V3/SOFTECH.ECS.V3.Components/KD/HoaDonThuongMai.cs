﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

namespace SOFTECH.ECS.V3.Components.KD
{//AiNPV
    public partial class HoaDonThuongMai
    {
        public static string ConfigHDThuongMai(ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)
        {
            string doc = "";
            string path = "../../../SOFTECH.ECS.V3.Components/XML/KD/HoaDonThuongMai.xml";
            XDocument docXML = XDocument.Load(path);
            if (docXML != null)
            {
                XElement Root = docXML.Root;
                if (Root != null && tkmd != null)
                {
                    IList<HoaDonThuongMai> list_Hdtm = HoaDonThuongMai.SelectCollectionBy_TKMD_ID(tkmd.ID);
                    if (list_Hdtm.Count > 0)
                    {
                        Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, status, declarationOffice);
                        XElement CommercialInvoices = Root.Element(NodeHoaDonThuongMai.CommercialInvoices);
                        XElement CommercialInvoice = CommercialInvoices.Element(NodeHoaDonThuongMai.CommercialInvoice);//Nếu mẫu xml chỉ có 1 CommercialInvoice
                        if (list_Hdtm.Count == 1)
                        {
                            if (CommercialInvoice != null)
                            {
                                CommercialInvoice = Config_CommercialInvoice_HDTM(CommercialInvoice, list_Hdtm[0]);
                            }
                        }
                        else if (list_Hdtm.Count > 1)
                        {
                            if (CommercialInvoice != null)
                            {
                                XElement TempCommercialInvoice = new XElement(CommercialInvoice);
                                CommercialInvoice = Config_CommercialInvoice_HDTM(CommercialInvoice, list_Hdtm[0]);
                                for (int i = 1; i < list_Hdtm.Count; i++)
                                {
                                    XElement NewCommercialInvoice = new XElement(TempCommercialInvoice);
                                    Config_CommercialInvoice_HDTM(NewCommercialInvoice, list_Hdtm[i]);
                                    CommercialInvoices.Add(NewCommercialInvoice);
                                }
                            }
                        }
                    }
                    else
                    {
                        return doc;// return "";
                    }
                }
                else
                {
                    return doc;// return "";
                }
            }
            //docXML.Save(@"D:\temp.xml");
            doc = docXML.ToString();
            return doc;
        }
        public static XElement Config_CommercialInvoice_HDTM(XElement CommercialInvoice, HoaDonThuongMai hd)
        {
            if (hd != null)
            {
                ToKhaiMauDich tk = ToKhaiMauDich.Load(hd.TKMD_ID);
                XElement CommercialInvoiceItem = CommercialInvoice.Element(NodeHoaDonThuongMai.CommercialInvoiceItem);
                IList<HoaDonThuongMaiDetail> lhdtm_Detail = HoaDonThuongMaiDetail.SelectCollectionBy_HoaDonTM_ID(hd.ID);
                foreach (XElement element in CommercialInvoice.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.reference.ToUpper()))
                        element.SetValue(hd.SoHoaDon);
                    else if (element.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.issue.ToUpper()))
                        element.SetValue(hd.NgayHoaDon);
                    else if (element.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.Seller.ToUpper()))
                    {
                        foreach (XElement sellerchild in element.Elements())
                        {
                            if (sellerchild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.name.ToUpper()))
                                sellerchild.SetValue(hd.TenDonViBan);
                            else if (sellerchild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.identity.ToUpper()))
                                sellerchild.SetValue(hd.MaDonViBan);
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.Buyer.ToUpper()))
                    {
                        foreach (XElement buyerchild in element.Elements())
                        {
                            if (buyerchild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.name.ToUpper()))
                                buyerchild.SetValue(hd.TenDonViMua);
                            else if (buyerchild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.identity.ToUpper()))
                                buyerchild.SetValue(hd.MaDonViMua);
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.AdditionalDocument.ToUpper()))
                    {
                        if (tk != null)
                        {
                            ChungTuKem ct = ChungTuKem.Load(tk.ID);
                            if (ct != null)
                            {
                                foreach (XElement additionalDocChild in element.Elements())
                                {
                                    if (additionalDocChild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.reference.ToUpper()))
                                        additionalDocChild.SetValue(ct.SO_CT);
                                    else if (additionalDocChild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.issue.ToUpper()))
                                        additionalDocChild.SetValue(ct.NGAY_CT);
                                }
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.Payment.ToUpper()))
                    {
                        PhuongThucThanhToan pt = PhuongThucThanhToan.Load(hd.PTTT_ID);
                        if (pt != null && pt.GhiChu != null)
                        {
                            foreach (XElement methodpay in element.Elements())
                            {
                                if (methodpay.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.method.ToUpper()))
                                    methodpay.SetValue(pt.GhiChu);
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.CurrencyExchange.ToUpper()))
                    {
                        NguyenTe ng_te = NguyenTe.Load(hd.NguyenTe_ID);
                        if (ng_te != null && ng_te.Ten != null)
                        {
                            foreach (XElement currency in element.Elements())
                            {
                                if (currency.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.currencyType.ToUpper()))
                                    currency.SetValue(ng_te.Ten);
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.TradeTerm.ToUpper()))
                    {
                        DieuKienGiaoHang dk = DieuKienGiaoHang.Load(hd.DKGH_ID);
                        if (dk != null && dk.MoTa != null)
                        {
                            foreach (XElement trade in element.Elements())
                            {
                                if (trade.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.condition.ToUpper()))
                                    trade.SetValue(dk.MoTa);
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.CommercialInvoiceItem.ToUpper()))
                    {
                        CommercialInvoiceItem = element;
                    }
                }
                if (lhdtm_Detail != null && lhdtm_Detail.Count > 0 && CommercialInvoiceItem != null)
                {
                    XElement TempCommercialInvoiceItem = new XElement(CommercialInvoiceItem);
                    if (lhdtm_Detail.Count == 1)
                    {
                        Config_CommercialInvoiceItem_HDTM(CommercialInvoiceItem, lhdtm_Detail[0]);
                    }
                    else if (lhdtm_Detail.Count > 1)
                    {

                        Config_CommercialInvoiceItem_HDTM(CommercialInvoiceItem, lhdtm_Detail[0]);
                        for (int i = 1; i < lhdtm_Detail.Count; i++)
                        {
                            XElement NewCommercialInvoiceItem = new XElement(TempCommercialInvoiceItem);
                            Config_CommercialInvoiceItem_HDTM(NewCommercialInvoiceItem, lhdtm_Detail[i]);
                            CommercialInvoice.Add(NewCommercialInvoiceItem);
                        }
                    }
                }
            }

            return CommercialInvoice;
        }
        public static XElement Config_CommercialInvoiceItem_HDTM(XElement CommercialInvoiceItem, HoaDonThuongMaiDetail hdtmd)
        {
            if (hdtmd != null)
            {
                foreach (XElement elementd in CommercialInvoiceItem.Elements())
                {
                    if (elementd.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.sequence.ToUpper()))
                        elementd.SetValue(hdtmd.SoThuTuHang);
                    else if (elementd.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.unitPrice.ToUpper()))
                        elementd.SetValue(hdtmd.DonGiaKB);
                    else if (elementd.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.statisticalValue.ToUpper()))
                        elementd.SetValue(hdtmd.TriGiaKB);
                    else if (elementd.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.Origin.ToUpper()))
                    {
                        Nuoc nuoc = Nuoc.Load(hdtmd.NuocXX_ID);
                        if (nuoc != null && nuoc.Ten != null)
                        {
                            foreach (XElement originchild in elementd.Elements())
                            {
                                if (originchild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.originCountry.ToUpper()))
                                    originchild.SetValue(nuoc.Ten);
                            }
                        }
                    }
                    else if (elementd.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.Commodity.ToUpper()))
                    {
                        foreach (XElement CommodityChild in elementd.Elements())
                        {
                            if (CommodityChild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.description.ToUpper()))
                                CommodityChild.SetValue(hdtmd.TenHang);
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.identification.ToUpper()))
                                CommodityChild.SetValue(hdtmd.MaPhu);
                        }
                    }
                    else if (elementd.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.GoodsMeasure.ToUpper()))
                    {
                        foreach (XElement GoodsMeasureChild in elementd.Elements())
                        {
                            if (GoodsMeasureChild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.quantity.ToUpper()))
                                GoodsMeasureChild.SetValue(hdtmd.SoLuong);
                            else if (GoodsMeasureChild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.measureUnit.ToUpper()))
                                GoodsMeasureChild.SetValue(hdtmd.DVT_ID);
                        }
                    }
                    else if (elementd.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.ValuationAdjustment.ToUpper()))
                    {
                        foreach (XElement ValuationAdjustmentChild in elementd.Elements())
                        {
                            if (ValuationAdjustmentChild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.addition.ToUpper()))
                            {
                                if (hdtmd.GiaTriDieuChinhTang < 0)
                                {
                                    ValuationAdjustmentChild.SetValue(0);
                                }
                                else
                                {
                                    ValuationAdjustmentChild.SetValue(hdtmd.GiaTriDieuChinhTang);
                                }
                            }
                            else if (ValuationAdjustmentChild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.deduction.ToUpper()))
                            {
                                if (hdtmd.GiaTriDieuChinhTang < 0)
                                {
                                    ValuationAdjustmentChild.SetValue(0);
                                }
                                else
                                {
                                    ValuationAdjustmentChild.SetValue(hdtmd.GiaiTriDieuChinhGiam);
                                }
                            }
                        }
                    }
                    else if (elementd.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.AdditionalInformation.ToUpper()))
                    {
                        foreach (XElement AdditionalInformationChild in elementd.Elements())
                        {
                            if (AdditionalInformationChild.Name.ToString().ToUpper().Equals(NodeHoaDonThuongMai.content.ToUpper()))
                                AdditionalInformationChild.SetValue(hdtmd.GhiChu);
                        }
                    }
                }
            }
            return CommercialInvoiceItem;
        }
    }
}
