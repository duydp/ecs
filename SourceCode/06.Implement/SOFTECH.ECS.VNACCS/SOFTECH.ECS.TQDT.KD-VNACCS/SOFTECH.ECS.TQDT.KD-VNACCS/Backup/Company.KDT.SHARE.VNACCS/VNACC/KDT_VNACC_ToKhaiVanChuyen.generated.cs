using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ToKhaiVanChuyen : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public DateTime NgayDangKy { set; get; }
		public string TenThongTinXuat { set; get; }
		public string MaPhanLoaiXuLy { set; get; }
		public string MaCoBaoNiemPhong { set; get; }
		public string TenCoBaoNiemPhong { set; get; }
		public string CoQuanHaiQuan { set; get; }
		public decimal SoToKhaiVC { set; get; }
		public string CoBaoXuatNhapKhau { set; get; }
		public DateTime NgayLapToKhai { set; get; }
		public string MaNguoiKhai { set; get; }
		public string TenNguoiKhai { set; get; }
		public string DiaChiNguoiKhai { set; get; }
		public string MaNguoiVC { set; get; }
		public string TenNguoiVC { set; get; }
		public string DiaChiNguoiVC { set; get; }
		public string SoHopDongVC { set; get; }
		public DateTime NgayHopDongVC { set; get; }
		public DateTime NgayHetHanHopDongVC { set; get; }
		public string MaPhuongTienVC { set; get; }
		public string TenPhuongTieVC { set; get; }
		public string MaMucDichVC { set; get; }
		public string TenMucDichVC { set; get; }
		public string LoaiHinhVanTai { set; get; }
		public string TenLoaiHinhVanTai { set; get; }
		public string MaDiaDiemXepHang { set; get; }
		public string MaViTriXepHang { set; get; }
		public string MaCangCuaKhauGaXepHang { set; get; }
		public string MaCangXHKhongCo_HT { set; get; }
		public string DiaDiemXepHang { set; get; }
		public DateTime NgayDenDiaDiem_XH { set; get; }
		public string MaDiaDiemDoHang { set; get; }
		public string MaViTriDoHang { set; get; }
		public string MaCangCuaKhauGaDoHang { set; get; }
		public string MaCangDHKhongCo_HT { set; get; }
		public string DiaDiemDoHang { set; get; }
		public DateTime NgayDenDiaDiem_DH { set; get; }
		public string TuyenDuongVC { set; get; }
		public string LoaiBaoLanh { set; get; }
		public decimal SoTienBaoLanh { set; get; }
		public int SoLuongCot_TK { set; get; }
		public int SoLuongContainer { set; get; }
		public string MaNganHangBaoLanh { set; get; }
		public int NamPhatHanhBaoLanh { set; get; }
		public string KyHieuChungTuBaoLanh { set; get; }
		public string SoChungTuBaoLanh { set; get; }
		public decimal MaVach { set; get; }
		public DateTime NgayPheDuyetVC { set; get; }
		public DateTime NgayDuKienBatDauVC { set; get; }
		public string GioDuKienBatDauVC { set; get; }
		public DateTime NgayDuKienKetThucVC { set; get; }
		public string GioDuKienKetThucVC { set; get; }
		public string MaBuuChinhHQ { set; get; }
		public string DiaChiBuuChinhHQ { set; get; }
		public string TenBuuChinhHQ { set; get; }
		public string GhiChu { set; get; }
		public string TrangThaiXuLy { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ToKhaiVanChuyen> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ToKhaiVanChuyen> collection = new List<KDT_VNACC_ToKhaiVanChuyen>();
			while (reader.Read())
			{
				KDT_VNACC_ToKhaiVanChuyen entity = new KDT_VNACC_ToKhaiVanChuyen();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenThongTinXuat"))) entity.TenThongTinXuat = reader.GetString(reader.GetOrdinal("TenThongTinXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiXuLy"))) entity.MaPhanLoaiXuLy = reader.GetString(reader.GetOrdinal("MaPhanLoaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCoBaoNiemPhong"))) entity.MaCoBaoNiemPhong = reader.GetString(reader.GetOrdinal("MaCoBaoNiemPhong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCoBaoNiemPhong"))) entity.TenCoBaoNiemPhong = reader.GetString(reader.GetOrdinal("TenCoBaoNiemPhong"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHaiQuan"))) entity.CoQuanHaiQuan = reader.GetString(reader.GetOrdinal("CoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVC"))) entity.SoToKhaiVC = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoBaoXuatNhapKhau"))) entity.CoBaoXuatNhapKhau = reader.GetString(reader.GetOrdinal("CoBaoXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayLapToKhai"))) entity.NgayLapToKhai = reader.GetDateTime(reader.GetOrdinal("NgayLapToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhai"))) entity.MaNguoiKhai = reader.GetString(reader.GetOrdinal("MaNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhai"))) entity.TenNguoiKhai = reader.GetString(reader.GetOrdinal("TenNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiKhai"))) entity.DiaChiNguoiKhai = reader.GetString(reader.GetOrdinal("DiaChiNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiVC"))) entity.MaNguoiVC = reader.GetString(reader.GetOrdinal("MaNguoiVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiVC"))) entity.TenNguoiVC = reader.GetString(reader.GetOrdinal("TenNguoiVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiVC"))) entity.DiaChiNguoiVC = reader.GetString(reader.GetOrdinal("DiaChiNguoiVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongVC"))) entity.SoHopDongVC = reader.GetString(reader.GetOrdinal("SoHopDongVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDongVC"))) entity.NgayHopDongVC = reader.GetDateTime(reader.GetOrdinal("NgayHopDongVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDongVC"))) entity.NgayHetHanHopDongVC = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDongVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhuongTienVC"))) entity.MaPhuongTienVC = reader.GetString(reader.GetOrdinal("MaPhuongTienVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenPhuongTieVC"))) entity.TenPhuongTieVC = reader.GetString(reader.GetOrdinal("TenPhuongTieVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaMucDichVC"))) entity.MaMucDichVC = reader.GetString(reader.GetOrdinal("MaMucDichVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenMucDichVC"))) entity.TenMucDichVC = reader.GetString(reader.GetOrdinal("TenMucDichVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHinhVanTai"))) entity.LoaiHinhVanTai = reader.GetString(reader.GetOrdinal("LoaiHinhVanTai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenLoaiHinhVanTai"))) entity.TenLoaiHinhVanTai = reader.GetString(reader.GetOrdinal("TenLoaiHinhVanTai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemXepHang"))) entity.MaDiaDiemXepHang = reader.GetString(reader.GetOrdinal("MaDiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaViTriXepHang"))) entity.MaViTriXepHang = reader.GetString(reader.GetOrdinal("MaViTriXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCangCuaKhauGaXepHang"))) entity.MaCangCuaKhauGaXepHang = reader.GetString(reader.GetOrdinal("MaCangCuaKhauGaXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCangXHKhongCo_HT"))) entity.MaCangXHKhongCo_HT = reader.GetString(reader.GetOrdinal("MaCangXHKhongCo_HT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenDiaDiem_XH"))) entity.NgayDenDiaDiem_XH = reader.GetDateTime(reader.GetOrdinal("NgayDenDiaDiem_XH"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemDoHang"))) entity.MaDiaDiemDoHang = reader.GetString(reader.GetOrdinal("MaDiaDiemDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaViTriDoHang"))) entity.MaViTriDoHang = reader.GetString(reader.GetOrdinal("MaViTriDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCangCuaKhauGaDoHang"))) entity.MaCangCuaKhauGaDoHang = reader.GetString(reader.GetOrdinal("MaCangCuaKhauGaDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCangDHKhongCo_HT"))) entity.MaCangDHKhongCo_HT = reader.GetString(reader.GetOrdinal("MaCangDHKhongCo_HT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemDoHang"))) entity.DiaDiemDoHang = reader.GetString(reader.GetOrdinal("DiaDiemDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenDiaDiem_DH"))) entity.NgayDenDiaDiem_DH = reader.GetDateTime(reader.GetOrdinal("NgayDenDiaDiem_DH"));
				if (!reader.IsDBNull(reader.GetOrdinal("TuyenDuongVC"))) entity.TuyenDuongVC = reader.GetString(reader.GetOrdinal("TuyenDuongVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiBaoLanh"))) entity.LoaiBaoLanh = reader.GetString(reader.GetOrdinal("LoaiBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienBaoLanh"))) entity.SoTienBaoLanh = reader.GetDecimal(reader.GetOrdinal("SoTienBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCot_TK"))) entity.SoLuongCot_TK = reader.GetInt32(reader.GetOrdinal("SoLuongCot_TK"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongContainer"))) entity.SoLuongContainer = reader.GetInt32(reader.GetOrdinal("SoLuongContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangBaoLanh"))) entity.MaNganHangBaoLanh = reader.GetString(reader.GetOrdinal("MaNganHangBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamPhatHanhBaoLanh"))) entity.NamPhatHanhBaoLanh = reader.GetInt32(reader.GetOrdinal("NamPhatHanhBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("KyHieuChungTuBaoLanh"))) entity.KyHieuChungTuBaoLanh = reader.GetString(reader.GetOrdinal("KyHieuChungTuBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTuBaoLanh"))) entity.SoChungTuBaoLanh = reader.GetString(reader.GetOrdinal("SoChungTuBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVach"))) entity.MaVach = reader.GetDecimal(reader.GetOrdinal("MaVach"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPheDuyetVC"))) entity.NgayPheDuyetVC = reader.GetDateTime(reader.GetOrdinal("NgayPheDuyetVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDuKienBatDauVC"))) entity.NgayDuKienBatDauVC = reader.GetDateTime(reader.GetOrdinal("NgayDuKienBatDauVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("GioDuKienBatDauVC"))) entity.GioDuKienBatDauVC = reader.GetString(reader.GetOrdinal("GioDuKienBatDauVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDuKienKetThucVC"))) entity.NgayDuKienKetThucVC = reader.GetDateTime(reader.GetOrdinal("NgayDuKienKetThucVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("GioDuKienKetThucVC"))) entity.GioDuKienKetThucVC = reader.GetString(reader.GetOrdinal("GioDuKienKetThucVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhHQ"))) entity.MaBuuChinhHQ = reader.GetString(reader.GetOrdinal("MaBuuChinhHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiBuuChinhHQ"))) entity.DiaChiBuuChinhHQ = reader.GetString(reader.GetOrdinal("DiaChiBuuChinhHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenBuuChinhHQ"))) entity.TenBuuChinhHQ = reader.GetString(reader.GetOrdinal("TenBuuChinhHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetString(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ToKhaiVanChuyen> collection, long id)
        {
            foreach (KDT_VNACC_ToKhaiVanChuyen item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiVanChuyen VALUES(@TKMD_ID, @NgayDangKy, @TenThongTinXuat, @MaPhanLoaiXuLy, @MaCoBaoNiemPhong, @TenCoBaoNiemPhong, @CoQuanHaiQuan, @SoToKhaiVC, @CoBaoXuatNhapKhau, @NgayLapToKhai, @MaNguoiKhai, @TenNguoiKhai, @DiaChiNguoiKhai, @MaNguoiVC, @TenNguoiVC, @DiaChiNguoiVC, @SoHopDongVC, @NgayHopDongVC, @NgayHetHanHopDongVC, @MaPhuongTienVC, @TenPhuongTieVC, @MaMucDichVC, @TenMucDichVC, @LoaiHinhVanTai, @TenLoaiHinhVanTai, @MaDiaDiemXepHang, @MaViTriXepHang, @MaCangCuaKhauGaXepHang, @MaCangXHKhongCo_HT, @DiaDiemXepHang, @NgayDenDiaDiem_XH, @MaDiaDiemDoHang, @MaViTriDoHang, @MaCangCuaKhauGaDoHang, @MaCangDHKhongCo_HT, @DiaDiemDoHang, @NgayDenDiaDiem_DH, @TuyenDuongVC, @LoaiBaoLanh, @SoTienBaoLanh, @SoLuongCot_TK, @SoLuongContainer, @MaNganHangBaoLanh, @NamPhatHanhBaoLanh, @KyHieuChungTuBaoLanh, @SoChungTuBaoLanh, @MaVach, @NgayPheDuyetVC, @NgayDuKienBatDauVC, @GioDuKienBatDauVC, @NgayDuKienKetThucVC, @GioDuKienKetThucVC, @MaBuuChinhHQ, @DiaChiBuuChinhHQ, @TenBuuChinhHQ, @GhiChu, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiVanChuyen SET TKMD_ID = @TKMD_ID, NgayDangKy = @NgayDangKy, TenThongTinXuat = @TenThongTinXuat, MaPhanLoaiXuLy = @MaPhanLoaiXuLy, MaCoBaoNiemPhong = @MaCoBaoNiemPhong, TenCoBaoNiemPhong = @TenCoBaoNiemPhong, CoQuanHaiQuan = @CoQuanHaiQuan, SoToKhaiVC = @SoToKhaiVC, CoBaoXuatNhapKhau = @CoBaoXuatNhapKhau, NgayLapToKhai = @NgayLapToKhai, MaNguoiKhai = @MaNguoiKhai, TenNguoiKhai = @TenNguoiKhai, DiaChiNguoiKhai = @DiaChiNguoiKhai, MaNguoiVC = @MaNguoiVC, TenNguoiVC = @TenNguoiVC, DiaChiNguoiVC = @DiaChiNguoiVC, SoHopDongVC = @SoHopDongVC, NgayHopDongVC = @NgayHopDongVC, NgayHetHanHopDongVC = @NgayHetHanHopDongVC, MaPhuongTienVC = @MaPhuongTienVC, TenPhuongTieVC = @TenPhuongTieVC, MaMucDichVC = @MaMucDichVC, TenMucDichVC = @TenMucDichVC, LoaiHinhVanTai = @LoaiHinhVanTai, TenLoaiHinhVanTai = @TenLoaiHinhVanTai, MaDiaDiemXepHang = @MaDiaDiemXepHang, MaViTriXepHang = @MaViTriXepHang, MaCangCuaKhauGaXepHang = @MaCangCuaKhauGaXepHang, MaCangXHKhongCo_HT = @MaCangXHKhongCo_HT, DiaDiemXepHang = @DiaDiemXepHang, NgayDenDiaDiem_XH = @NgayDenDiaDiem_XH, MaDiaDiemDoHang = @MaDiaDiemDoHang, MaViTriDoHang = @MaViTriDoHang, MaCangCuaKhauGaDoHang = @MaCangCuaKhauGaDoHang, MaCangDHKhongCo_HT = @MaCangDHKhongCo_HT, DiaDiemDoHang = @DiaDiemDoHang, NgayDenDiaDiem_DH = @NgayDenDiaDiem_DH, TuyenDuongVC = @TuyenDuongVC, LoaiBaoLanh = @LoaiBaoLanh, SoTienBaoLanh = @SoTienBaoLanh, SoLuongCot_TK = @SoLuongCot_TK, SoLuongContainer = @SoLuongContainer, MaNganHangBaoLanh = @MaNganHangBaoLanh, NamPhatHanhBaoLanh = @NamPhatHanhBaoLanh, KyHieuChungTuBaoLanh = @KyHieuChungTuBaoLanh, SoChungTuBaoLanh = @SoChungTuBaoLanh, MaVach = @MaVach, NgayPheDuyetVC = @NgayPheDuyetVC, NgayDuKienBatDauVC = @NgayDuKienBatDauVC, GioDuKienBatDauVC = @GioDuKienBatDauVC, NgayDuKienKetThucVC = @NgayDuKienKetThucVC, GioDuKienKetThucVC = @GioDuKienKetThucVC, MaBuuChinhHQ = @MaBuuChinhHQ, DiaChiBuuChinhHQ = @DiaChiBuuChinhHQ, TenBuuChinhHQ = @TenBuuChinhHQ, GhiChu = @GhiChu, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiVanChuyen WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThongTinXuat", SqlDbType.NVarChar, "TenThongTinXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, "MaPhanLoaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCoBaoNiemPhong", SqlDbType.VarChar, "MaCoBaoNiemPhong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoBaoNiemPhong", SqlDbType.NVarChar, "TenCoBaoNiemPhong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiVC", SqlDbType.Decimal, "SoToKhaiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoBaoXuatNhapKhau", SqlDbType.VarChar, "CoBaoXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayLapToKhai", SqlDbType.DateTime, "NgayLapToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.VarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiVC", SqlDbType.VarChar, "MaNguoiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiVC", SqlDbType.NVarChar, "TenNguoiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiVC", SqlDbType.NVarChar, "DiaChiNguoiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongVC", SqlDbType.VarChar, "SoHopDongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongVC", SqlDbType.DateTime, "NgayHopDongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHanHopDongVC", SqlDbType.DateTime, "NgayHetHanHopDongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhuongTienVC", SqlDbType.VarChar, "MaPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPhuongTieVC", SqlDbType.VarChar, "TenPhuongTieVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMucDichVC", SqlDbType.VarChar, "MaMucDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenMucDichVC", SqlDbType.NVarChar, "TenMucDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHinhVanTai", SqlDbType.VarChar, "LoaiHinhVanTai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenLoaiHinhVanTai", SqlDbType.NVarChar, "TenLoaiHinhVanTai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaViTriXepHang", SqlDbType.VarChar, "MaViTriXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.VarChar, "MaCangCuaKhauGaXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangXHKhongCo_HT", SqlDbType.VarChar, "MaCangXHKhongCo_HT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, "DiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDenDiaDiem_XH", SqlDbType.DateTime, "NgayDenDiaDiem_XH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaViTriDoHang", SqlDbType.VarChar, "MaViTriDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.VarChar, "MaCangCuaKhauGaDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangDHKhongCo_HT", SqlDbType.VarChar, "MaCangDHKhongCo_HT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemDoHang", SqlDbType.NVarChar, "DiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDenDiaDiem_DH", SqlDbType.DateTime, "NgayDenDiaDiem_DH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuyenDuongVC", SqlDbType.VarChar, "TuyenDuongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiBaoLanh", SqlDbType.VarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCot_TK", SqlDbType.Int, "SoLuongCot_TK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongContainer", SqlDbType.Int, "SoLuongContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanhBaoLanh", SqlDbType.Int, "NamPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KyHieuChungTuBaoLanh", SqlDbType.VarChar, "KyHieuChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, "SoChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVach", SqlDbType.Decimal, "MaVach", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPheDuyetVC", SqlDbType.DateTime, "NgayPheDuyetVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDuKienBatDauVC", SqlDbType.DateTime, "NgayDuKienBatDauVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioDuKienBatDauVC", SqlDbType.VarChar, "GioDuKienBatDauVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDuKienKetThucVC", SqlDbType.DateTime, "NgayDuKienKetThucVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioDuKienKetThucVC", SqlDbType.VarChar, "GioDuKienKetThucVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhHQ", SqlDbType.VarChar, "MaBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiBuuChinhHQ", SqlDbType.NVarChar, "DiaChiBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenBuuChinhHQ", SqlDbType.NVarChar, "TenBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThongTinXuat", SqlDbType.NVarChar, "TenThongTinXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, "MaPhanLoaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCoBaoNiemPhong", SqlDbType.VarChar, "MaCoBaoNiemPhong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoBaoNiemPhong", SqlDbType.NVarChar, "TenCoBaoNiemPhong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiVC", SqlDbType.Decimal, "SoToKhaiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoBaoXuatNhapKhau", SqlDbType.VarChar, "CoBaoXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayLapToKhai", SqlDbType.DateTime, "NgayLapToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.VarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiVC", SqlDbType.VarChar, "MaNguoiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiVC", SqlDbType.NVarChar, "TenNguoiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiVC", SqlDbType.NVarChar, "DiaChiNguoiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongVC", SqlDbType.VarChar, "SoHopDongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongVC", SqlDbType.DateTime, "NgayHopDongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHanHopDongVC", SqlDbType.DateTime, "NgayHetHanHopDongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhuongTienVC", SqlDbType.VarChar, "MaPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPhuongTieVC", SqlDbType.VarChar, "TenPhuongTieVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMucDichVC", SqlDbType.VarChar, "MaMucDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenMucDichVC", SqlDbType.NVarChar, "TenMucDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHinhVanTai", SqlDbType.VarChar, "LoaiHinhVanTai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenLoaiHinhVanTai", SqlDbType.NVarChar, "TenLoaiHinhVanTai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaViTriXepHang", SqlDbType.VarChar, "MaViTriXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.VarChar, "MaCangCuaKhauGaXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangXHKhongCo_HT", SqlDbType.VarChar, "MaCangXHKhongCo_HT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, "DiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDenDiaDiem_XH", SqlDbType.DateTime, "NgayDenDiaDiem_XH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaViTriDoHang", SqlDbType.VarChar, "MaViTriDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.VarChar, "MaCangCuaKhauGaDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangDHKhongCo_HT", SqlDbType.VarChar, "MaCangDHKhongCo_HT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemDoHang", SqlDbType.NVarChar, "DiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDenDiaDiem_DH", SqlDbType.DateTime, "NgayDenDiaDiem_DH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuyenDuongVC", SqlDbType.VarChar, "TuyenDuongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiBaoLanh", SqlDbType.VarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCot_TK", SqlDbType.Int, "SoLuongCot_TK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongContainer", SqlDbType.Int, "SoLuongContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanhBaoLanh", SqlDbType.Int, "NamPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KyHieuChungTuBaoLanh", SqlDbType.VarChar, "KyHieuChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, "SoChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVach", SqlDbType.Decimal, "MaVach", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPheDuyetVC", SqlDbType.DateTime, "NgayPheDuyetVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDuKienBatDauVC", SqlDbType.DateTime, "NgayDuKienBatDauVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioDuKienBatDauVC", SqlDbType.VarChar, "GioDuKienBatDauVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDuKienKetThucVC", SqlDbType.DateTime, "NgayDuKienKetThucVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioDuKienKetThucVC", SqlDbType.VarChar, "GioDuKienKetThucVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhHQ", SqlDbType.VarChar, "MaBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiBuuChinhHQ", SqlDbType.NVarChar, "DiaChiBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenBuuChinhHQ", SqlDbType.NVarChar, "TenBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiVanChuyen VALUES(@TKMD_ID, @NgayDangKy, @TenThongTinXuat, @MaPhanLoaiXuLy, @MaCoBaoNiemPhong, @TenCoBaoNiemPhong, @CoQuanHaiQuan, @SoToKhaiVC, @CoBaoXuatNhapKhau, @NgayLapToKhai, @MaNguoiKhai, @TenNguoiKhai, @DiaChiNguoiKhai, @MaNguoiVC, @TenNguoiVC, @DiaChiNguoiVC, @SoHopDongVC, @NgayHopDongVC, @NgayHetHanHopDongVC, @MaPhuongTienVC, @TenPhuongTieVC, @MaMucDichVC, @TenMucDichVC, @LoaiHinhVanTai, @TenLoaiHinhVanTai, @MaDiaDiemXepHang, @MaViTriXepHang, @MaCangCuaKhauGaXepHang, @MaCangXHKhongCo_HT, @DiaDiemXepHang, @NgayDenDiaDiem_XH, @MaDiaDiemDoHang, @MaViTriDoHang, @MaCangCuaKhauGaDoHang, @MaCangDHKhongCo_HT, @DiaDiemDoHang, @NgayDenDiaDiem_DH, @TuyenDuongVC, @LoaiBaoLanh, @SoTienBaoLanh, @SoLuongCot_TK, @SoLuongContainer, @MaNganHangBaoLanh, @NamPhatHanhBaoLanh, @KyHieuChungTuBaoLanh, @SoChungTuBaoLanh, @MaVach, @NgayPheDuyetVC, @NgayDuKienBatDauVC, @GioDuKienBatDauVC, @NgayDuKienKetThucVC, @GioDuKienKetThucVC, @MaBuuChinhHQ, @DiaChiBuuChinhHQ, @TenBuuChinhHQ, @GhiChu, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiVanChuyen SET TKMD_ID = @TKMD_ID, NgayDangKy = @NgayDangKy, TenThongTinXuat = @TenThongTinXuat, MaPhanLoaiXuLy = @MaPhanLoaiXuLy, MaCoBaoNiemPhong = @MaCoBaoNiemPhong, TenCoBaoNiemPhong = @TenCoBaoNiemPhong, CoQuanHaiQuan = @CoQuanHaiQuan, SoToKhaiVC = @SoToKhaiVC, CoBaoXuatNhapKhau = @CoBaoXuatNhapKhau, NgayLapToKhai = @NgayLapToKhai, MaNguoiKhai = @MaNguoiKhai, TenNguoiKhai = @TenNguoiKhai, DiaChiNguoiKhai = @DiaChiNguoiKhai, MaNguoiVC = @MaNguoiVC, TenNguoiVC = @TenNguoiVC, DiaChiNguoiVC = @DiaChiNguoiVC, SoHopDongVC = @SoHopDongVC, NgayHopDongVC = @NgayHopDongVC, NgayHetHanHopDongVC = @NgayHetHanHopDongVC, MaPhuongTienVC = @MaPhuongTienVC, TenPhuongTieVC = @TenPhuongTieVC, MaMucDichVC = @MaMucDichVC, TenMucDichVC = @TenMucDichVC, LoaiHinhVanTai = @LoaiHinhVanTai, TenLoaiHinhVanTai = @TenLoaiHinhVanTai, MaDiaDiemXepHang = @MaDiaDiemXepHang, MaViTriXepHang = @MaViTriXepHang, MaCangCuaKhauGaXepHang = @MaCangCuaKhauGaXepHang, MaCangXHKhongCo_HT = @MaCangXHKhongCo_HT, DiaDiemXepHang = @DiaDiemXepHang, NgayDenDiaDiem_XH = @NgayDenDiaDiem_XH, MaDiaDiemDoHang = @MaDiaDiemDoHang, MaViTriDoHang = @MaViTriDoHang, MaCangCuaKhauGaDoHang = @MaCangCuaKhauGaDoHang, MaCangDHKhongCo_HT = @MaCangDHKhongCo_HT, DiaDiemDoHang = @DiaDiemDoHang, NgayDenDiaDiem_DH = @NgayDenDiaDiem_DH, TuyenDuongVC = @TuyenDuongVC, LoaiBaoLanh = @LoaiBaoLanh, SoTienBaoLanh = @SoTienBaoLanh, SoLuongCot_TK = @SoLuongCot_TK, SoLuongContainer = @SoLuongContainer, MaNganHangBaoLanh = @MaNganHangBaoLanh, NamPhatHanhBaoLanh = @NamPhatHanhBaoLanh, KyHieuChungTuBaoLanh = @KyHieuChungTuBaoLanh, SoChungTuBaoLanh = @SoChungTuBaoLanh, MaVach = @MaVach, NgayPheDuyetVC = @NgayPheDuyetVC, NgayDuKienBatDauVC = @NgayDuKienBatDauVC, GioDuKienBatDauVC = @GioDuKienBatDauVC, NgayDuKienKetThucVC = @NgayDuKienKetThucVC, GioDuKienKetThucVC = @GioDuKienKetThucVC, MaBuuChinhHQ = @MaBuuChinhHQ, DiaChiBuuChinhHQ = @DiaChiBuuChinhHQ, TenBuuChinhHQ = @TenBuuChinhHQ, GhiChu = @GhiChu, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiVanChuyen WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThongTinXuat", SqlDbType.NVarChar, "TenThongTinXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, "MaPhanLoaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCoBaoNiemPhong", SqlDbType.VarChar, "MaCoBaoNiemPhong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoBaoNiemPhong", SqlDbType.NVarChar, "TenCoBaoNiemPhong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiVC", SqlDbType.Decimal, "SoToKhaiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoBaoXuatNhapKhau", SqlDbType.VarChar, "CoBaoXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayLapToKhai", SqlDbType.DateTime, "NgayLapToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.VarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiVC", SqlDbType.VarChar, "MaNguoiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiVC", SqlDbType.NVarChar, "TenNguoiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiVC", SqlDbType.NVarChar, "DiaChiNguoiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongVC", SqlDbType.VarChar, "SoHopDongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongVC", SqlDbType.DateTime, "NgayHopDongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHanHopDongVC", SqlDbType.DateTime, "NgayHetHanHopDongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhuongTienVC", SqlDbType.VarChar, "MaPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPhuongTieVC", SqlDbType.VarChar, "TenPhuongTieVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMucDichVC", SqlDbType.VarChar, "MaMucDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenMucDichVC", SqlDbType.NVarChar, "TenMucDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHinhVanTai", SqlDbType.VarChar, "LoaiHinhVanTai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenLoaiHinhVanTai", SqlDbType.NVarChar, "TenLoaiHinhVanTai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaViTriXepHang", SqlDbType.VarChar, "MaViTriXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.VarChar, "MaCangCuaKhauGaXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangXHKhongCo_HT", SqlDbType.VarChar, "MaCangXHKhongCo_HT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, "DiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDenDiaDiem_XH", SqlDbType.DateTime, "NgayDenDiaDiem_XH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaViTriDoHang", SqlDbType.VarChar, "MaViTriDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.VarChar, "MaCangCuaKhauGaDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangDHKhongCo_HT", SqlDbType.VarChar, "MaCangDHKhongCo_HT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemDoHang", SqlDbType.NVarChar, "DiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDenDiaDiem_DH", SqlDbType.DateTime, "NgayDenDiaDiem_DH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuyenDuongVC", SqlDbType.VarChar, "TuyenDuongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiBaoLanh", SqlDbType.VarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCot_TK", SqlDbType.Int, "SoLuongCot_TK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongContainer", SqlDbType.Int, "SoLuongContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanhBaoLanh", SqlDbType.Int, "NamPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KyHieuChungTuBaoLanh", SqlDbType.VarChar, "KyHieuChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, "SoChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVach", SqlDbType.Decimal, "MaVach", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPheDuyetVC", SqlDbType.DateTime, "NgayPheDuyetVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDuKienBatDauVC", SqlDbType.DateTime, "NgayDuKienBatDauVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioDuKienBatDauVC", SqlDbType.VarChar, "GioDuKienBatDauVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDuKienKetThucVC", SqlDbType.DateTime, "NgayDuKienKetThucVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioDuKienKetThucVC", SqlDbType.VarChar, "GioDuKienKetThucVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhHQ", SqlDbType.VarChar, "MaBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiBuuChinhHQ", SqlDbType.NVarChar, "DiaChiBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenBuuChinhHQ", SqlDbType.NVarChar, "TenBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThongTinXuat", SqlDbType.NVarChar, "TenThongTinXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, "MaPhanLoaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCoBaoNiemPhong", SqlDbType.VarChar, "MaCoBaoNiemPhong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoBaoNiemPhong", SqlDbType.NVarChar, "TenCoBaoNiemPhong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiVC", SqlDbType.Decimal, "SoToKhaiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoBaoXuatNhapKhau", SqlDbType.VarChar, "CoBaoXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayLapToKhai", SqlDbType.DateTime, "NgayLapToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.VarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiVC", SqlDbType.VarChar, "MaNguoiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiVC", SqlDbType.NVarChar, "TenNguoiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiVC", SqlDbType.NVarChar, "DiaChiNguoiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongVC", SqlDbType.VarChar, "SoHopDongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongVC", SqlDbType.DateTime, "NgayHopDongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHanHopDongVC", SqlDbType.DateTime, "NgayHetHanHopDongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhuongTienVC", SqlDbType.VarChar, "MaPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPhuongTieVC", SqlDbType.VarChar, "TenPhuongTieVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMucDichVC", SqlDbType.VarChar, "MaMucDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenMucDichVC", SqlDbType.NVarChar, "TenMucDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHinhVanTai", SqlDbType.VarChar, "LoaiHinhVanTai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenLoaiHinhVanTai", SqlDbType.NVarChar, "TenLoaiHinhVanTai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaViTriXepHang", SqlDbType.VarChar, "MaViTriXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.VarChar, "MaCangCuaKhauGaXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangXHKhongCo_HT", SqlDbType.VarChar, "MaCangXHKhongCo_HT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, "DiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDenDiaDiem_XH", SqlDbType.DateTime, "NgayDenDiaDiem_XH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaViTriDoHang", SqlDbType.VarChar, "MaViTriDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.VarChar, "MaCangCuaKhauGaDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangDHKhongCo_HT", SqlDbType.VarChar, "MaCangDHKhongCo_HT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemDoHang", SqlDbType.NVarChar, "DiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDenDiaDiem_DH", SqlDbType.DateTime, "NgayDenDiaDiem_DH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuyenDuongVC", SqlDbType.VarChar, "TuyenDuongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiBaoLanh", SqlDbType.VarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCot_TK", SqlDbType.Int, "SoLuongCot_TK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongContainer", SqlDbType.Int, "SoLuongContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanhBaoLanh", SqlDbType.Int, "NamPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KyHieuChungTuBaoLanh", SqlDbType.VarChar, "KyHieuChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, "SoChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVach", SqlDbType.Decimal, "MaVach", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPheDuyetVC", SqlDbType.DateTime, "NgayPheDuyetVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDuKienBatDauVC", SqlDbType.DateTime, "NgayDuKienBatDauVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioDuKienBatDauVC", SqlDbType.VarChar, "GioDuKienBatDauVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDuKienKetThucVC", SqlDbType.DateTime, "NgayDuKienKetThucVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioDuKienKetThucVC", SqlDbType.VarChar, "GioDuKienKetThucVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhHQ", SqlDbType.VarChar, "MaBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiBuuChinhHQ", SqlDbType.NVarChar, "DiaChiBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenBuuChinhHQ", SqlDbType.NVarChar, "TenBuuChinhHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ToKhaiVanChuyen Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ToKhaiVanChuyen> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ToKhaiVanChuyen> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ToKhaiVanChuyen> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ToKhaiVanChuyen(long tKMD_ID, DateTime ngayDangKy, string tenThongTinXuat, string maPhanLoaiXuLy, string maCoBaoNiemPhong, string tenCoBaoNiemPhong, string coQuanHaiQuan, decimal soToKhaiVC, string coBaoXuatNhapKhau, DateTime ngayLapToKhai, string maNguoiKhai, string tenNguoiKhai, string diaChiNguoiKhai, string maNguoiVC, string tenNguoiVC, string diaChiNguoiVC, string soHopDongVC, DateTime ngayHopDongVC, DateTime ngayHetHanHopDongVC, string maPhuongTienVC, string tenPhuongTieVC, string maMucDichVC, string tenMucDichVC, string loaiHinhVanTai, string tenLoaiHinhVanTai, string maDiaDiemXepHang, string maViTriXepHang, string maCangCuaKhauGaXepHang, string maCangXHKhongCo_HT, string diaDiemXepHang, DateTime ngayDenDiaDiem_XH, string maDiaDiemDoHang, string maViTriDoHang, string maCangCuaKhauGaDoHang, string maCangDHKhongCo_HT, string diaDiemDoHang, DateTime ngayDenDiaDiem_DH, string tuyenDuongVC, string loaiBaoLanh, decimal soTienBaoLanh, int soLuongCot_TK, int soLuongContainer, string maNganHangBaoLanh, int namPhatHanhBaoLanh, string kyHieuChungTuBaoLanh, string soChungTuBaoLanh, decimal maVach, DateTime ngayPheDuyetVC, DateTime ngayDuKienBatDauVC, string gioDuKienBatDauVC, DateTime ngayDuKienKetThucVC, string gioDuKienKetThucVC, string maBuuChinhHQ, string diaChiBuuChinhHQ, string tenBuuChinhHQ, string ghiChu, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_ToKhaiVanChuyen entity = new KDT_VNACC_ToKhaiVanChuyen();	
			entity.TKMD_ID = tKMD_ID;
			entity.NgayDangKy = ngayDangKy;
			entity.TenThongTinXuat = tenThongTinXuat;
			entity.MaPhanLoaiXuLy = maPhanLoaiXuLy;
			entity.MaCoBaoNiemPhong = maCoBaoNiemPhong;
			entity.TenCoBaoNiemPhong = tenCoBaoNiemPhong;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.SoToKhaiVC = soToKhaiVC;
			entity.CoBaoXuatNhapKhau = coBaoXuatNhapKhau;
			entity.NgayLapToKhai = ngayLapToKhai;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.MaNguoiVC = maNguoiVC;
			entity.TenNguoiVC = tenNguoiVC;
			entity.DiaChiNguoiVC = diaChiNguoiVC;
			entity.SoHopDongVC = soHopDongVC;
			entity.NgayHopDongVC = ngayHopDongVC;
			entity.NgayHetHanHopDongVC = ngayHetHanHopDongVC;
			entity.MaPhuongTienVC = maPhuongTienVC;
			entity.TenPhuongTieVC = tenPhuongTieVC;
			entity.MaMucDichVC = maMucDichVC;
			entity.TenMucDichVC = tenMucDichVC;
			entity.LoaiHinhVanTai = loaiHinhVanTai;
			entity.TenLoaiHinhVanTai = tenLoaiHinhVanTai;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.MaViTriXepHang = maViTriXepHang;
			entity.MaCangCuaKhauGaXepHang = maCangCuaKhauGaXepHang;
			entity.MaCangXHKhongCo_HT = maCangXHKhongCo_HT;
			entity.DiaDiemXepHang = diaDiemXepHang;
			entity.NgayDenDiaDiem_XH = ngayDenDiaDiem_XH;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.MaViTriDoHang = maViTriDoHang;
			entity.MaCangCuaKhauGaDoHang = maCangCuaKhauGaDoHang;
			entity.MaCangDHKhongCo_HT = maCangDHKhongCo_HT;
			entity.DiaDiemDoHang = diaDiemDoHang;
			entity.NgayDenDiaDiem_DH = ngayDenDiaDiem_DH;
			entity.TuyenDuongVC = tuyenDuongVC;
			entity.LoaiBaoLanh = loaiBaoLanh;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.SoLuongCot_TK = soLuongCot_TK;
			entity.SoLuongContainer = soLuongContainer;
			entity.MaNganHangBaoLanh = maNganHangBaoLanh;
			entity.NamPhatHanhBaoLanh = namPhatHanhBaoLanh;
			entity.KyHieuChungTuBaoLanh = kyHieuChungTuBaoLanh;
			entity.SoChungTuBaoLanh = soChungTuBaoLanh;
			entity.MaVach = maVach;
			entity.NgayPheDuyetVC = ngayPheDuyetVC;
			entity.NgayDuKienBatDauVC = ngayDuKienBatDauVC;
			entity.GioDuKienBatDauVC = gioDuKienBatDauVC;
			entity.NgayDuKienKetThucVC = ngayDuKienKetThucVC;
			entity.GioDuKienKetThucVC = gioDuKienKetThucVC;
			entity.MaBuuChinhHQ = maBuuChinhHQ;
			entity.DiaChiBuuChinhHQ = diaChiBuuChinhHQ;
			entity.TenBuuChinhHQ = tenBuuChinhHQ;
			entity.GhiChu = ghiChu;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@TenThongTinXuat", SqlDbType.NVarChar, TenThongTinXuat);
			db.AddInParameter(dbCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, MaPhanLoaiXuLy);
			db.AddInParameter(dbCommand, "@MaCoBaoNiemPhong", SqlDbType.VarChar, MaCoBaoNiemPhong);
			db.AddInParameter(dbCommand, "@TenCoBaoNiemPhong", SqlDbType.NVarChar, TenCoBaoNiemPhong);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@SoToKhaiVC", SqlDbType.Decimal, SoToKhaiVC);
			db.AddInParameter(dbCommand, "@CoBaoXuatNhapKhau", SqlDbType.VarChar, CoBaoXuatNhapKhau);
			db.AddInParameter(dbCommand, "@NgayLapToKhai", SqlDbType.DateTime, NgayLapToKhai.Year <= 1753 ? DBNull.Value : (object) NgayLapToKhai);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.VarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaNguoiVC", SqlDbType.VarChar, MaNguoiVC);
			db.AddInParameter(dbCommand, "@TenNguoiVC", SqlDbType.NVarChar, TenNguoiVC);
			db.AddInParameter(dbCommand, "@DiaChiNguoiVC", SqlDbType.NVarChar, DiaChiNguoiVC);
			db.AddInParameter(dbCommand, "@SoHopDongVC", SqlDbType.VarChar, SoHopDongVC);
			db.AddInParameter(dbCommand, "@NgayHopDongVC", SqlDbType.DateTime, NgayHopDongVC.Year <= 1753 ? DBNull.Value : (object) NgayHopDongVC);
			db.AddInParameter(dbCommand, "@NgayHetHanHopDongVC", SqlDbType.DateTime, NgayHetHanHopDongVC.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHopDongVC);
			db.AddInParameter(dbCommand, "@MaPhuongTienVC", SqlDbType.VarChar, MaPhuongTienVC);
			db.AddInParameter(dbCommand, "@TenPhuongTieVC", SqlDbType.VarChar, TenPhuongTieVC);
			db.AddInParameter(dbCommand, "@MaMucDichVC", SqlDbType.VarChar, MaMucDichVC);
			db.AddInParameter(dbCommand, "@TenMucDichVC", SqlDbType.NVarChar, TenMucDichVC);
			db.AddInParameter(dbCommand, "@LoaiHinhVanTai", SqlDbType.VarChar, LoaiHinhVanTai);
			db.AddInParameter(dbCommand, "@TenLoaiHinhVanTai", SqlDbType.NVarChar, TenLoaiHinhVanTai);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@MaViTriXepHang", SqlDbType.VarChar, MaViTriXepHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.VarChar, MaCangCuaKhauGaXepHang);
			db.AddInParameter(dbCommand, "@MaCangXHKhongCo_HT", SqlDbType.VarChar, MaCangXHKhongCo_HT);
			db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
			db.AddInParameter(dbCommand, "@NgayDenDiaDiem_XH", SqlDbType.DateTime, NgayDenDiaDiem_XH.Year <= 1753 ? DBNull.Value : (object) NgayDenDiaDiem_XH);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@MaViTriDoHang", SqlDbType.VarChar, MaViTriDoHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.VarChar, MaCangCuaKhauGaDoHang);
			db.AddInParameter(dbCommand, "@MaCangDHKhongCo_HT", SqlDbType.VarChar, MaCangDHKhongCo_HT);
			db.AddInParameter(dbCommand, "@DiaDiemDoHang", SqlDbType.NVarChar, DiaDiemDoHang);
			db.AddInParameter(dbCommand, "@NgayDenDiaDiem_DH", SqlDbType.DateTime, NgayDenDiaDiem_DH.Year <= 1753 ? DBNull.Value : (object) NgayDenDiaDiem_DH);
			db.AddInParameter(dbCommand, "@TuyenDuongVC", SqlDbType.VarChar, TuyenDuongVC);
			db.AddInParameter(dbCommand, "@LoaiBaoLanh", SqlDbType.VarChar, LoaiBaoLanh);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoLuongCot_TK", SqlDbType.Int, SoLuongCot_TK);
			db.AddInParameter(dbCommand, "@SoLuongContainer", SqlDbType.Int, SoLuongContainer);
			db.AddInParameter(dbCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, MaNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanhBaoLanh", SqlDbType.Int, NamPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@KyHieuChungTuBaoLanh", SqlDbType.VarChar, KyHieuChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, SoChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@MaVach", SqlDbType.Decimal, MaVach);
			db.AddInParameter(dbCommand, "@NgayPheDuyetVC", SqlDbType.DateTime, NgayPheDuyetVC.Year <= 1753 ? DBNull.Value : (object) NgayPheDuyetVC);
			db.AddInParameter(dbCommand, "@NgayDuKienBatDauVC", SqlDbType.DateTime, NgayDuKienBatDauVC.Year <= 1753 ? DBNull.Value : (object) NgayDuKienBatDauVC);
			db.AddInParameter(dbCommand, "@GioDuKienBatDauVC", SqlDbType.VarChar, GioDuKienBatDauVC);
			db.AddInParameter(dbCommand, "@NgayDuKienKetThucVC", SqlDbType.DateTime, NgayDuKienKetThucVC.Year <= 1753 ? DBNull.Value : (object) NgayDuKienKetThucVC);
			db.AddInParameter(dbCommand, "@GioDuKienKetThucVC", SqlDbType.VarChar, GioDuKienKetThucVC);
			db.AddInParameter(dbCommand, "@MaBuuChinhHQ", SqlDbType.VarChar, MaBuuChinhHQ);
			db.AddInParameter(dbCommand, "@DiaChiBuuChinhHQ", SqlDbType.NVarChar, DiaChiBuuChinhHQ);
			db.AddInParameter(dbCommand, "@TenBuuChinhHQ", SqlDbType.NVarChar, TenBuuChinhHQ);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ToKhaiVanChuyen> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiVanChuyen item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ToKhaiVanChuyen(long id, long tKMD_ID, DateTime ngayDangKy, string tenThongTinXuat, string maPhanLoaiXuLy, string maCoBaoNiemPhong, string tenCoBaoNiemPhong, string coQuanHaiQuan, decimal soToKhaiVC, string coBaoXuatNhapKhau, DateTime ngayLapToKhai, string maNguoiKhai, string tenNguoiKhai, string diaChiNguoiKhai, string maNguoiVC, string tenNguoiVC, string diaChiNguoiVC, string soHopDongVC, DateTime ngayHopDongVC, DateTime ngayHetHanHopDongVC, string maPhuongTienVC, string tenPhuongTieVC, string maMucDichVC, string tenMucDichVC, string loaiHinhVanTai, string tenLoaiHinhVanTai, string maDiaDiemXepHang, string maViTriXepHang, string maCangCuaKhauGaXepHang, string maCangXHKhongCo_HT, string diaDiemXepHang, DateTime ngayDenDiaDiem_XH, string maDiaDiemDoHang, string maViTriDoHang, string maCangCuaKhauGaDoHang, string maCangDHKhongCo_HT, string diaDiemDoHang, DateTime ngayDenDiaDiem_DH, string tuyenDuongVC, string loaiBaoLanh, decimal soTienBaoLanh, int soLuongCot_TK, int soLuongContainer, string maNganHangBaoLanh, int namPhatHanhBaoLanh, string kyHieuChungTuBaoLanh, string soChungTuBaoLanh, decimal maVach, DateTime ngayPheDuyetVC, DateTime ngayDuKienBatDauVC, string gioDuKienBatDauVC, DateTime ngayDuKienKetThucVC, string gioDuKienKetThucVC, string maBuuChinhHQ, string diaChiBuuChinhHQ, string tenBuuChinhHQ, string ghiChu, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_ToKhaiVanChuyen entity = new KDT_VNACC_ToKhaiVanChuyen();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayDangKy = ngayDangKy;
			entity.TenThongTinXuat = tenThongTinXuat;
			entity.MaPhanLoaiXuLy = maPhanLoaiXuLy;
			entity.MaCoBaoNiemPhong = maCoBaoNiemPhong;
			entity.TenCoBaoNiemPhong = tenCoBaoNiemPhong;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.SoToKhaiVC = soToKhaiVC;
			entity.CoBaoXuatNhapKhau = coBaoXuatNhapKhau;
			entity.NgayLapToKhai = ngayLapToKhai;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.MaNguoiVC = maNguoiVC;
			entity.TenNguoiVC = tenNguoiVC;
			entity.DiaChiNguoiVC = diaChiNguoiVC;
			entity.SoHopDongVC = soHopDongVC;
			entity.NgayHopDongVC = ngayHopDongVC;
			entity.NgayHetHanHopDongVC = ngayHetHanHopDongVC;
			entity.MaPhuongTienVC = maPhuongTienVC;
			entity.TenPhuongTieVC = tenPhuongTieVC;
			entity.MaMucDichVC = maMucDichVC;
			entity.TenMucDichVC = tenMucDichVC;
			entity.LoaiHinhVanTai = loaiHinhVanTai;
			entity.TenLoaiHinhVanTai = tenLoaiHinhVanTai;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.MaViTriXepHang = maViTriXepHang;
			entity.MaCangCuaKhauGaXepHang = maCangCuaKhauGaXepHang;
			entity.MaCangXHKhongCo_HT = maCangXHKhongCo_HT;
			entity.DiaDiemXepHang = diaDiemXepHang;
			entity.NgayDenDiaDiem_XH = ngayDenDiaDiem_XH;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.MaViTriDoHang = maViTriDoHang;
			entity.MaCangCuaKhauGaDoHang = maCangCuaKhauGaDoHang;
			entity.MaCangDHKhongCo_HT = maCangDHKhongCo_HT;
			entity.DiaDiemDoHang = diaDiemDoHang;
			entity.NgayDenDiaDiem_DH = ngayDenDiaDiem_DH;
			entity.TuyenDuongVC = tuyenDuongVC;
			entity.LoaiBaoLanh = loaiBaoLanh;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.SoLuongCot_TK = soLuongCot_TK;
			entity.SoLuongContainer = soLuongContainer;
			entity.MaNganHangBaoLanh = maNganHangBaoLanh;
			entity.NamPhatHanhBaoLanh = namPhatHanhBaoLanh;
			entity.KyHieuChungTuBaoLanh = kyHieuChungTuBaoLanh;
			entity.SoChungTuBaoLanh = soChungTuBaoLanh;
			entity.MaVach = maVach;
			entity.NgayPheDuyetVC = ngayPheDuyetVC;
			entity.NgayDuKienBatDauVC = ngayDuKienBatDauVC;
			entity.GioDuKienBatDauVC = gioDuKienBatDauVC;
			entity.NgayDuKienKetThucVC = ngayDuKienKetThucVC;
			entity.GioDuKienKetThucVC = gioDuKienKetThucVC;
			entity.MaBuuChinhHQ = maBuuChinhHQ;
			entity.DiaChiBuuChinhHQ = diaChiBuuChinhHQ;
			entity.TenBuuChinhHQ = tenBuuChinhHQ;
			entity.GhiChu = ghiChu;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@TenThongTinXuat", SqlDbType.NVarChar, TenThongTinXuat);
			db.AddInParameter(dbCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, MaPhanLoaiXuLy);
			db.AddInParameter(dbCommand, "@MaCoBaoNiemPhong", SqlDbType.VarChar, MaCoBaoNiemPhong);
			db.AddInParameter(dbCommand, "@TenCoBaoNiemPhong", SqlDbType.NVarChar, TenCoBaoNiemPhong);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@SoToKhaiVC", SqlDbType.Decimal, SoToKhaiVC);
			db.AddInParameter(dbCommand, "@CoBaoXuatNhapKhau", SqlDbType.VarChar, CoBaoXuatNhapKhau);
			db.AddInParameter(dbCommand, "@NgayLapToKhai", SqlDbType.DateTime, NgayLapToKhai.Year <= 1753 ? DBNull.Value : (object) NgayLapToKhai);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.VarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaNguoiVC", SqlDbType.VarChar, MaNguoiVC);
			db.AddInParameter(dbCommand, "@TenNguoiVC", SqlDbType.NVarChar, TenNguoiVC);
			db.AddInParameter(dbCommand, "@DiaChiNguoiVC", SqlDbType.NVarChar, DiaChiNguoiVC);
			db.AddInParameter(dbCommand, "@SoHopDongVC", SqlDbType.VarChar, SoHopDongVC);
			db.AddInParameter(dbCommand, "@NgayHopDongVC", SqlDbType.DateTime, NgayHopDongVC.Year <= 1753 ? DBNull.Value : (object) NgayHopDongVC);
			db.AddInParameter(dbCommand, "@NgayHetHanHopDongVC", SqlDbType.DateTime, NgayHetHanHopDongVC.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHopDongVC);
			db.AddInParameter(dbCommand, "@MaPhuongTienVC", SqlDbType.VarChar, MaPhuongTienVC);
			db.AddInParameter(dbCommand, "@TenPhuongTieVC", SqlDbType.VarChar, TenPhuongTieVC);
			db.AddInParameter(dbCommand, "@MaMucDichVC", SqlDbType.VarChar, MaMucDichVC);
			db.AddInParameter(dbCommand, "@TenMucDichVC", SqlDbType.NVarChar, TenMucDichVC);
			db.AddInParameter(dbCommand, "@LoaiHinhVanTai", SqlDbType.VarChar, LoaiHinhVanTai);
			db.AddInParameter(dbCommand, "@TenLoaiHinhVanTai", SqlDbType.NVarChar, TenLoaiHinhVanTai);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@MaViTriXepHang", SqlDbType.VarChar, MaViTriXepHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.VarChar, MaCangCuaKhauGaXepHang);
			db.AddInParameter(dbCommand, "@MaCangXHKhongCo_HT", SqlDbType.VarChar, MaCangXHKhongCo_HT);
			db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
			db.AddInParameter(dbCommand, "@NgayDenDiaDiem_XH", SqlDbType.DateTime, NgayDenDiaDiem_XH.Year <= 1753 ? DBNull.Value : (object) NgayDenDiaDiem_XH);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@MaViTriDoHang", SqlDbType.VarChar, MaViTriDoHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.VarChar, MaCangCuaKhauGaDoHang);
			db.AddInParameter(dbCommand, "@MaCangDHKhongCo_HT", SqlDbType.VarChar, MaCangDHKhongCo_HT);
			db.AddInParameter(dbCommand, "@DiaDiemDoHang", SqlDbType.NVarChar, DiaDiemDoHang);
			db.AddInParameter(dbCommand, "@NgayDenDiaDiem_DH", SqlDbType.DateTime, NgayDenDiaDiem_DH.Year <= 1753 ? DBNull.Value : (object) NgayDenDiaDiem_DH);
			db.AddInParameter(dbCommand, "@TuyenDuongVC", SqlDbType.VarChar, TuyenDuongVC);
			db.AddInParameter(dbCommand, "@LoaiBaoLanh", SqlDbType.VarChar, LoaiBaoLanh);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoLuongCot_TK", SqlDbType.Int, SoLuongCot_TK);
			db.AddInParameter(dbCommand, "@SoLuongContainer", SqlDbType.Int, SoLuongContainer);
			db.AddInParameter(dbCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, MaNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanhBaoLanh", SqlDbType.Int, NamPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@KyHieuChungTuBaoLanh", SqlDbType.VarChar, KyHieuChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, SoChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@MaVach", SqlDbType.Decimal, MaVach);
			db.AddInParameter(dbCommand, "@NgayPheDuyetVC", SqlDbType.DateTime, NgayPheDuyetVC.Year <= 1753 ? DBNull.Value : (object) NgayPheDuyetVC);
			db.AddInParameter(dbCommand, "@NgayDuKienBatDauVC", SqlDbType.DateTime, NgayDuKienBatDauVC.Year <= 1753 ? DBNull.Value : (object) NgayDuKienBatDauVC);
			db.AddInParameter(dbCommand, "@GioDuKienBatDauVC", SqlDbType.VarChar, GioDuKienBatDauVC);
			db.AddInParameter(dbCommand, "@NgayDuKienKetThucVC", SqlDbType.DateTime, NgayDuKienKetThucVC.Year <= 1753 ? DBNull.Value : (object) NgayDuKienKetThucVC);
			db.AddInParameter(dbCommand, "@GioDuKienKetThucVC", SqlDbType.VarChar, GioDuKienKetThucVC);
			db.AddInParameter(dbCommand, "@MaBuuChinhHQ", SqlDbType.VarChar, MaBuuChinhHQ);
			db.AddInParameter(dbCommand, "@DiaChiBuuChinhHQ", SqlDbType.NVarChar, DiaChiBuuChinhHQ);
			db.AddInParameter(dbCommand, "@TenBuuChinhHQ", SqlDbType.NVarChar, TenBuuChinhHQ);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ToKhaiVanChuyen> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiVanChuyen item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ToKhaiVanChuyen(long id, long tKMD_ID, DateTime ngayDangKy, string tenThongTinXuat, string maPhanLoaiXuLy, string maCoBaoNiemPhong, string tenCoBaoNiemPhong, string coQuanHaiQuan, decimal soToKhaiVC, string coBaoXuatNhapKhau, DateTime ngayLapToKhai, string maNguoiKhai, string tenNguoiKhai, string diaChiNguoiKhai, string maNguoiVC, string tenNguoiVC, string diaChiNguoiVC, string soHopDongVC, DateTime ngayHopDongVC, DateTime ngayHetHanHopDongVC, string maPhuongTienVC, string tenPhuongTieVC, string maMucDichVC, string tenMucDichVC, string loaiHinhVanTai, string tenLoaiHinhVanTai, string maDiaDiemXepHang, string maViTriXepHang, string maCangCuaKhauGaXepHang, string maCangXHKhongCo_HT, string diaDiemXepHang, DateTime ngayDenDiaDiem_XH, string maDiaDiemDoHang, string maViTriDoHang, string maCangCuaKhauGaDoHang, string maCangDHKhongCo_HT, string diaDiemDoHang, DateTime ngayDenDiaDiem_DH, string tuyenDuongVC, string loaiBaoLanh, decimal soTienBaoLanh, int soLuongCot_TK, int soLuongContainer, string maNganHangBaoLanh, int namPhatHanhBaoLanh, string kyHieuChungTuBaoLanh, string soChungTuBaoLanh, decimal maVach, DateTime ngayPheDuyetVC, DateTime ngayDuKienBatDauVC, string gioDuKienBatDauVC, DateTime ngayDuKienKetThucVC, string gioDuKienKetThucVC, string maBuuChinhHQ, string diaChiBuuChinhHQ, string tenBuuChinhHQ, string ghiChu, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_ToKhaiVanChuyen entity = new KDT_VNACC_ToKhaiVanChuyen();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayDangKy = ngayDangKy;
			entity.TenThongTinXuat = tenThongTinXuat;
			entity.MaPhanLoaiXuLy = maPhanLoaiXuLy;
			entity.MaCoBaoNiemPhong = maCoBaoNiemPhong;
			entity.TenCoBaoNiemPhong = tenCoBaoNiemPhong;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.SoToKhaiVC = soToKhaiVC;
			entity.CoBaoXuatNhapKhau = coBaoXuatNhapKhau;
			entity.NgayLapToKhai = ngayLapToKhai;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.MaNguoiVC = maNguoiVC;
			entity.TenNguoiVC = tenNguoiVC;
			entity.DiaChiNguoiVC = diaChiNguoiVC;
			entity.SoHopDongVC = soHopDongVC;
			entity.NgayHopDongVC = ngayHopDongVC;
			entity.NgayHetHanHopDongVC = ngayHetHanHopDongVC;
			entity.MaPhuongTienVC = maPhuongTienVC;
			entity.TenPhuongTieVC = tenPhuongTieVC;
			entity.MaMucDichVC = maMucDichVC;
			entity.TenMucDichVC = tenMucDichVC;
			entity.LoaiHinhVanTai = loaiHinhVanTai;
			entity.TenLoaiHinhVanTai = tenLoaiHinhVanTai;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.MaViTriXepHang = maViTriXepHang;
			entity.MaCangCuaKhauGaXepHang = maCangCuaKhauGaXepHang;
			entity.MaCangXHKhongCo_HT = maCangXHKhongCo_HT;
			entity.DiaDiemXepHang = diaDiemXepHang;
			entity.NgayDenDiaDiem_XH = ngayDenDiaDiem_XH;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.MaViTriDoHang = maViTriDoHang;
			entity.MaCangCuaKhauGaDoHang = maCangCuaKhauGaDoHang;
			entity.MaCangDHKhongCo_HT = maCangDHKhongCo_HT;
			entity.DiaDiemDoHang = diaDiemDoHang;
			entity.NgayDenDiaDiem_DH = ngayDenDiaDiem_DH;
			entity.TuyenDuongVC = tuyenDuongVC;
			entity.LoaiBaoLanh = loaiBaoLanh;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.SoLuongCot_TK = soLuongCot_TK;
			entity.SoLuongContainer = soLuongContainer;
			entity.MaNganHangBaoLanh = maNganHangBaoLanh;
			entity.NamPhatHanhBaoLanh = namPhatHanhBaoLanh;
			entity.KyHieuChungTuBaoLanh = kyHieuChungTuBaoLanh;
			entity.SoChungTuBaoLanh = soChungTuBaoLanh;
			entity.MaVach = maVach;
			entity.NgayPheDuyetVC = ngayPheDuyetVC;
			entity.NgayDuKienBatDauVC = ngayDuKienBatDauVC;
			entity.GioDuKienBatDauVC = gioDuKienBatDauVC;
			entity.NgayDuKienKetThucVC = ngayDuKienKetThucVC;
			entity.GioDuKienKetThucVC = gioDuKienKetThucVC;
			entity.MaBuuChinhHQ = maBuuChinhHQ;
			entity.DiaChiBuuChinhHQ = diaChiBuuChinhHQ;
			entity.TenBuuChinhHQ = tenBuuChinhHQ;
			entity.GhiChu = ghiChu;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@TenThongTinXuat", SqlDbType.NVarChar, TenThongTinXuat);
			db.AddInParameter(dbCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, MaPhanLoaiXuLy);
			db.AddInParameter(dbCommand, "@MaCoBaoNiemPhong", SqlDbType.VarChar, MaCoBaoNiemPhong);
			db.AddInParameter(dbCommand, "@TenCoBaoNiemPhong", SqlDbType.NVarChar, TenCoBaoNiemPhong);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@SoToKhaiVC", SqlDbType.Decimal, SoToKhaiVC);
			db.AddInParameter(dbCommand, "@CoBaoXuatNhapKhau", SqlDbType.VarChar, CoBaoXuatNhapKhau);
			db.AddInParameter(dbCommand, "@NgayLapToKhai", SqlDbType.DateTime, NgayLapToKhai.Year <= 1753 ? DBNull.Value : (object) NgayLapToKhai);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.VarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaNguoiVC", SqlDbType.VarChar, MaNguoiVC);
			db.AddInParameter(dbCommand, "@TenNguoiVC", SqlDbType.NVarChar, TenNguoiVC);
			db.AddInParameter(dbCommand, "@DiaChiNguoiVC", SqlDbType.NVarChar, DiaChiNguoiVC);
			db.AddInParameter(dbCommand, "@SoHopDongVC", SqlDbType.VarChar, SoHopDongVC);
			db.AddInParameter(dbCommand, "@NgayHopDongVC", SqlDbType.DateTime, NgayHopDongVC.Year <= 1753 ? DBNull.Value : (object) NgayHopDongVC);
			db.AddInParameter(dbCommand, "@NgayHetHanHopDongVC", SqlDbType.DateTime, NgayHetHanHopDongVC.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHopDongVC);
			db.AddInParameter(dbCommand, "@MaPhuongTienVC", SqlDbType.VarChar, MaPhuongTienVC);
			db.AddInParameter(dbCommand, "@TenPhuongTieVC", SqlDbType.VarChar, TenPhuongTieVC);
			db.AddInParameter(dbCommand, "@MaMucDichVC", SqlDbType.VarChar, MaMucDichVC);
			db.AddInParameter(dbCommand, "@TenMucDichVC", SqlDbType.NVarChar, TenMucDichVC);
			db.AddInParameter(dbCommand, "@LoaiHinhVanTai", SqlDbType.VarChar, LoaiHinhVanTai);
			db.AddInParameter(dbCommand, "@TenLoaiHinhVanTai", SqlDbType.NVarChar, TenLoaiHinhVanTai);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@MaViTriXepHang", SqlDbType.VarChar, MaViTriXepHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.VarChar, MaCangCuaKhauGaXepHang);
			db.AddInParameter(dbCommand, "@MaCangXHKhongCo_HT", SqlDbType.VarChar, MaCangXHKhongCo_HT);
			db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
			db.AddInParameter(dbCommand, "@NgayDenDiaDiem_XH", SqlDbType.DateTime, NgayDenDiaDiem_XH.Year <= 1753 ? DBNull.Value : (object) NgayDenDiaDiem_XH);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@MaViTriDoHang", SqlDbType.VarChar, MaViTriDoHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.VarChar, MaCangCuaKhauGaDoHang);
			db.AddInParameter(dbCommand, "@MaCangDHKhongCo_HT", SqlDbType.VarChar, MaCangDHKhongCo_HT);
			db.AddInParameter(dbCommand, "@DiaDiemDoHang", SqlDbType.NVarChar, DiaDiemDoHang);
			db.AddInParameter(dbCommand, "@NgayDenDiaDiem_DH", SqlDbType.DateTime, NgayDenDiaDiem_DH.Year <= 1753 ? DBNull.Value : (object) NgayDenDiaDiem_DH);
			db.AddInParameter(dbCommand, "@TuyenDuongVC", SqlDbType.VarChar, TuyenDuongVC);
			db.AddInParameter(dbCommand, "@LoaiBaoLanh", SqlDbType.VarChar, LoaiBaoLanh);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoLuongCot_TK", SqlDbType.Int, SoLuongCot_TK);
			db.AddInParameter(dbCommand, "@SoLuongContainer", SqlDbType.Int, SoLuongContainer);
			db.AddInParameter(dbCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, MaNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanhBaoLanh", SqlDbType.Int, NamPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@KyHieuChungTuBaoLanh", SqlDbType.VarChar, KyHieuChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, SoChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@MaVach", SqlDbType.Decimal, MaVach);
			db.AddInParameter(dbCommand, "@NgayPheDuyetVC", SqlDbType.DateTime, NgayPheDuyetVC.Year <= 1753 ? DBNull.Value : (object) NgayPheDuyetVC);
			db.AddInParameter(dbCommand, "@NgayDuKienBatDauVC", SqlDbType.DateTime, NgayDuKienBatDauVC.Year <= 1753 ? DBNull.Value : (object) NgayDuKienBatDauVC);
			db.AddInParameter(dbCommand, "@GioDuKienBatDauVC", SqlDbType.VarChar, GioDuKienBatDauVC);
			db.AddInParameter(dbCommand, "@NgayDuKienKetThucVC", SqlDbType.DateTime, NgayDuKienKetThucVC.Year <= 1753 ? DBNull.Value : (object) NgayDuKienKetThucVC);
			db.AddInParameter(dbCommand, "@GioDuKienKetThucVC", SqlDbType.VarChar, GioDuKienKetThucVC);
			db.AddInParameter(dbCommand, "@MaBuuChinhHQ", SqlDbType.VarChar, MaBuuChinhHQ);
			db.AddInParameter(dbCommand, "@DiaChiBuuChinhHQ", SqlDbType.NVarChar, DiaChiBuuChinhHQ);
			db.AddInParameter(dbCommand, "@TenBuuChinhHQ", SqlDbType.NVarChar, TenBuuChinhHQ);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ToKhaiVanChuyen> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiVanChuyen item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ToKhaiVanChuyen(long id)
		{
			KDT_VNACC_ToKhaiVanChuyen entity = new KDT_VNACC_ToKhaiVanChuyen();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ToKhaiVanChuyen> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiVanChuyen item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}