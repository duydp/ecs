﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.Utils;
using Janus.Windows.GridEX;


namespace Company.Interface.KDT.GC.PCTFormTemplate
{
    public partial class FormDieuChinhSLNPL : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();         
        private string dvt = string.Empty;
        public bool boolFlag;
        public FormDieuChinhSLNPL()
        {
            InitializeComponent();
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (txtMaNPL.Text.Trim() == "")
                return;
            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
            npl.HopDong_ID = HD.ID;
            npl.Ma = txtMaNPL.Text.Trim();
            if (npl.Load())
            {
                if (npl.TrangThai ==0)
                {
                    txtMaNPL.Text = npl.Ma.Trim();
                    txtTenNPL.Text = npl.Ten.Trim();
                    txtMaHSNPL.Text = npl.MaHS.Trim();
                    dvt = npl.DVT_ID;
                    txtDonViTinhNPL.Text = DonViTinh.GetName(npl.DVT_ID).Trim();
                    txtSoLuongCu.Text = npl.SoLuongDangKy.ToString();
                    decimal LuongDaSuDung = npl.SoLuongDaNhap + npl.SoLuongCungUng;
                    txtLuongDaNhap.Text = LuongDaSuDung.ToString();
                    txtSoLuongMoi.Focus();
                }
                else if (npl.TrangThai != 0 && HangPK.ID>0)
                {
                    txtMaNPL.Text = npl.Ma.Trim();
                    txtTenNPL.Text = npl.Ten.Trim();
                    txtMaHSNPL.Text = npl.MaHS.Trim();
                    dvt = npl.DVT_ID;
                    txtDonViTinhNPL.Text = DonViTinh.GetName(npl.DVT_ID).Trim();
                    //txtSoLuongCu.Text = npl.SoLuongDangKy.ToString();
                    decimal LuongDaSuDung = npl.SoLuongDaNhap + npl.SoLuongCungUng;
                    txtLuongDaNhap.Text = LuongDaSuDung.ToString();
                    error.SetError(txtMaNPL, null);
                    txtSoLuongMoi.Focus();
                }
                else
                {
                    error.SetError(txtMaNPL, setText("Nguyên phụ liệu đang được điều chỉnh nhưng chưa được hải quan duyệt nên không điều chỉnh số lượng được.", "This raw material have been updated and is pending approval so can not update quanlity now"));
                    txtMaNPL.Focus();
                    return;
                }
            }
            else
            {
                error.SetError(txtMaNPL,setText( "Không tồn tại nguyên phụ liệu này.","This value is not exist"));
                txtMaNPL.Focus();
                return;
            }
        }
       

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {          
            Company.Interface.GC.NguyenPhuLieuRegistedGCForm f = new Company.Interface.GC.NguyenPhuLieuRegistedGCForm();
            f.NguyenPhuLieuSelected.HopDong_ID = this.HD.ID;
            f.isBrower = true;
            f.isDisplayAll = 2;
            f.ShowDialog();
            if (f.NguyenPhuLieuSelected != null && f.NguyenPhuLieuSelected.Ma != "")            
            {
                txtMaNPL.Text = f.NguyenPhuLieuSelected.Ma.Trim();
                txtTenNPL.Text = f.NguyenPhuLieuSelected.Ten.Trim();
                txtMaHSNPL.Text = f.NguyenPhuLieuSelected.MaHS.Trim();
                dvt = f.NguyenPhuLieuSelected.DVT_ID;
                txtDonViTinhNPL.Text = DonViTinh.GetName(f.NguyenPhuLieuSelected.DVT_ID).Trim();
                txtSoLuongCu.Text = f.NguyenPhuLieuSelected.SoLuongDangKy.ToString();
                decimal LuongDaSuDung = f.NguyenPhuLieuSelected.SoLuongDaNhap + f.NguyenPhuLieuSelected.SoLuongCungUng;
                txtLuongDaNhap.Text = LuongDaSuDung.ToString();
                txtSoLuongMoi.Focus();
                error.SetError(txtMaNPL, null);
            }
            else
            {
                error.SetError(txtMaNPL, setText("Không tồn tại nguyên phụ liệu này.","This value is not exist"));
                txtMaNPL.Focus();
                return;
            }
        }
        private bool CheckNPLExit(string maNPL)
        {
            foreach(HangPhuKien pk1 in LoaiPK.HPKCollection)
            {
                if (pk1.MaHang.Trim() == maNPL.Trim())
                {
                    return true;
                }
            }
            return false; ;
        }
             

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                txtMaNPL.Focus();
                cvError.Validate();
                if (!cvError.IsValid) return;
                if (Convert.ToDecimal(txtSoLuongMoi.Text) == 0)
                {
                    error.SetError(txtSoLuongMoi,setText("Số lượng phải lớn hơn 0.","This value must be greater than 0"));
                    txtSoLuongMoi.Focus();
                    return;
                }
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = txtMaNPL.Text.Trim();
                if (!npl.Load())
                {
                    showMsg("MSG_240217");
                    //ShowMessage("Không có nguyên phụ liệu này trong hệ thống.", false);
                    return;
                }
                LoaiPK.HPKCollection.Remove(HangPK);
                if (CheckNPLExit(txtMaNPL.Text))
                {
                    showMsg("MSG_240218");
                    //ShowMessage("Đã điều chỉnh nguyên phụ liệu này rồi", false);
                    if (HangPK.MaHang.Trim().Length > 0)
                        LoaiPK.HPKCollection.Add(HangPK);
                    return;
                }

                HangPK.MaHang = txtMaNPL.Text.Trim();
                HangPK.TenHang = txtTenNPL.Text.Trim();
                HangPK.SoLuong = Convert.ToDecimal(txtSoLuongMoi.Text);
                HangPK.MaHS = txtMaHSNPL.Text.Trim();
                HangPK.DVT_ID = dvt;
                HangPK.ThongTinCu = Convert.ToDecimal(txtSoLuongCu.Text.Trim()).ToString();
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                if (boolFlag == true)
                {
                    LoaiPK.InsertUpdateDieuChinhSoLuongNPLDaDangKy(pkdk.HopDong_ID);
                    boolFlag = false;
                }
                else 
                {
                    LoaiPK.InsertUpdateDieuChinhSoLuongNPL(pkdk.HopDong_ID);
                }
                //PhuKienGCSendForm.isEdit = true;
                reset();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
        private void reset()
        {
            txtMaNPL.Text = "";
            txtTenNPL.Text = "";
            txtDonViTinhNPL.Text = "";
            txtMaHSNPL.Text = "";
            txtSoLuongCu.Text = "0.000";
            txtSoLuongMoi.Text = "0.000";
            txtLuongDaNhap.Text = "0.000";

            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            HangPK = new HangPhuKien();
        }     

        private void DieuChinhSoLuongNPLForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["ThongTinCu"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            txtLuongDaNhap.DecimalDigits= GlobalSettings.SoThapPhan.LuongNPL;
            txtSoLuongCu.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtSoLuongMoi.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;

            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "N05")
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnUpdate.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                LoaiPK.LoadCollection();
                btnXoa.Enabled = false;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                btnXoa.Enabled = true;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {

                btnUpdate.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                btnXoa.Enabled = false;
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            //Registered
            if (boolFlag == true)
            {
                btnUpdate.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            LoaiPK.MaPhuKien = "N05";
            dgList.DataSource = LoaiPK.HPKCollection;
        }      

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            {

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien,pkdk.HopDong_ID);
                            //pkDelete.DeleteTransaction(null);
                        }
                    }
                }

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            HangPK = (HangPhuKien)e.Row.DataRow;
            txtMaNPL.Text = HangPK.MaHang;
            txtMaNPL_Leave(null, null);            
            txtSoLuongMoi.Text = HangPK.SoLuong.ToString();
            txtSoLuongCu.Text = HangPK.ThongTinCu;
            txtSoLuongMoi.Focus();          
        }

        private void FormDieuChinhSLNPL_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (LoaiPK.HPKCollection.Count > 0)
            {
                LoaiPK.MaPhuKien = "N05";
                LoaiPK.NoiDung = LoaiPhuKien_GetName("N05");
                pkdk.PKCollection.Remove(LoaiPK);
                pkdk.PKCollection.Add(LoaiPK);
            }
            else
            {
                pkdk.PKCollection.Remove(LoaiPK);
                LoaiPK.Delete(pkdk.HopDong_ID);;
                //LoaiPK.DeleteTransaction(null);
            }
        }

        private void txtSoLuongMoi_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtLuongDaNhap.Text) > Convert.ToDecimal(txtSoLuongMoi.Text))
            {
                if (showMsg("MSG_240222", true) == "Yes")
                //if (ShowMessage("Lượng điều chỉnh không được nhỏ hơn lượng đã nhập.Bạn có muốn tiếp tục không?", true) == "Yes")
                {
                    ;
                }
                else
                {
                    txtSoLuongMoi.Text = "0";
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            HangPhuKienCollection hpkColl = new HangPhuKienCollection();
            if (LoaiPK.HPKCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count < 0) return;

            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            {
                
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        try
                        {
                            if (pkDelete.ID > 0)
                            {
                                pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                                //pkDelete.DeleteTransaction(null);
                            }
                            hpkColl.Add(pkDelete);
                            //LoaiPK.HPKCollection.Remove(pkDelete);
                        }
                        catch { }
                      
                    }

                }
                foreach (Company.GC.BLL.KDT.GC.HangPhuKien hpkk in hpkColl)
                {
                    try
                    {
                        LoaiPK.HPKCollection.Remove(hpkk);
                    }
                    catch { }

                }
                dgList.DataSource = LoaiPK.HPKCollection;
                txtDonViTinhNPL.Text = "";
                txtMaHSNPL.Text = "";
                txtMaNPL.Text = "";
                txtTenNPL.Text = "";
                txtSoLuongCu.Text = "0";
                txtSoLuongMoi.Text = "0";
                txtLuongDaNhap.Text = "0";
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            
            }
        }

    
    }
}