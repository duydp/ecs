﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.Report.SXXK
{
    public partial class DoiNoiDung : Company.Interface.BaseForm
    {
        public string NoiDungMoi = "";
        public DoiNoiDung(string NoiDungCu)
        {
            InitializeComponent();
            txtNoiDungCu.Text = NoiDungCu;
        }

        private void CauHinhInForm_Load(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            NoiDungMoi = txtNoiDungMoi.Text;
            this.Close();
            this.Dispose();
        }

    }
}

