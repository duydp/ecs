
/****** Object:  StoredProcedure [dbo].[p_GC_NguyenPhuLieu_GetNPLDinhMuc]    Script Date: 09/08/2014 14:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[[p_GC_NguyenPhuLieu_GetNPLDinhMuc]]
-- Database: TQDT_SXXK_V3
-- Author: Huynh Ngoc Khanh
-- Time created: Monday, July 14, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_NguyenPhuLieu_GetNPLDinhMuc]
	@IDDinhMuc INT,
	@MaSanPham nvarchar(250),
	@MaDoanhNghiep nvarchar(20),
	@MaHaiQuan nvarchar(20)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT     t_SXXK_NguyenPhuLieu.MaHS, 
           t_SXXK_NguyenPhuLieu.DVT_ID, 
           t_SXXK_NguyenPhuLieu.Ma, 
           t_SXXK_NguyenPhuLieu.Ten, 
           t_KDT_SXXK_DinhMuc.DinhMucSuDung, 
           t_KDT_SXXK_DinhMuc.TyLeHaoHut,
           t_KDT_SXXK_DinhMuc.GhiChu
           
           FROM    t_SXXK_NguyenPhuLieu INNER JOIN
           t_KDT_SXXK_DinhMuc ON t_SXXK_NguyenPhuLieu.Ma = t_KDT_SXXK_DinhMuc.MaNguyenPhuLieu INNER JOIN
           t_KDT_SXXK_DinhMucDangKy ON t_KDT_SXXK_DinhMuc.Master_ID = t_KDT_SXXK_DinhMucDangKy.ID
WHERE    (t_KDT_SXXK_DinhMucDangKy.ID = @IDDinhMuc) AND (t_KDT_SXXK_DinhMuc.MaSanPham = @MaSanPham) AND (dbo.t_SXXK_NguyenPhuLieu.MaDoanhNghiep = @MaDoanhNghiep) AND (dbo.t_SXXK_NguyenPhuLieu.MaHaiQuan = @MaHaiQuan)
ORDER BY t_SXXK_NguyenPhuLieu.Ma
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.6',GETDATE(), N' Cap nhat p_GC_NguyenPhuLieu_GetNPLDinhMuc')
END