﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAHP030 : BasicVNACC
    {

        public List<VAHP030_HANG> HangHoa { get; set; }
        public void LoadVAHP030(string strResult)
        {
            try
            {
                this.GetObject<VAHP030>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAHP030, false, VAHP030.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAHP030.TongSoByte);
                while (true)
                {
                    VAHP030_HANG ivaHang = new VAHP030_HANG();
                    ivaHang.GetObject<VAHP030_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAHP030_HANG", false, VAHP030_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAHP030_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAHP030_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAHP030_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAHP030_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
