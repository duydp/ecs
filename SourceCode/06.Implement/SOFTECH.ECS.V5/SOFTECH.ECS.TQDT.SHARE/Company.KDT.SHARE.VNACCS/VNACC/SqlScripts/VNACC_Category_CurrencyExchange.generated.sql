-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Insert]
	@ErrorCode char(25),
	@PGNumber char(2),
	@TableID char(5),
	@ProcessClassification char(1),
	@MakerClassification char(1),
	@NumberOfKeyItems char(2),
	@CurrencyCode char(3),
	@CurrencyName nvarchar(250),
	@GenerationManagementIndication char(1),
	@DateOfMaintenanceUpdated datetime,
	@EndDate datetime,
	@StartDate_1 datetime,
	@ExchangeRate_1 decimal(9, 2),
	@StartDate_2 datetime,
	@ExchangeRate_2 decimal(9, 2),
	@StartDate_3 datetime,
	@ExchangeRate_3 decimal(9, 2),
	@StartDate_4 datetime,
	@ExchangeRate_4 decimal(9, 2),
	@StartDate_5 datetime,
	@ExchangeRate_5 decimal(9, 2),
	@StartDate_6 datetime,
	@ExchangeRate_6 decimal(9, 2),
	@StartDate_7 datetime,
	@ExchangeRate_7 decimal(9, 2),
	@StartDate_8 datetime,
	@ExchangeRate_8 decimal(9, 2),
	@StartDate_9 datetime,
	@ExchangeRate_9 decimal(9, 2),
	@StartDate_10 datetime,
	@ExchangeRate_10 decimal(9, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_CurrencyExchange]
(
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ErrorCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@MakerClassification,
	@NumberOfKeyItems,
	@CurrencyCode,
	@CurrencyName,
	@GenerationManagementIndication,
	@DateOfMaintenanceUpdated,
	@EndDate,
	@StartDate_1,
	@ExchangeRate_1,
	@StartDate_2,
	@ExchangeRate_2,
	@StartDate_3,
	@ExchangeRate_3,
	@StartDate_4,
	@ExchangeRate_4,
	@StartDate_5,
	@ExchangeRate_5,
	@StartDate_6,
	@ExchangeRate_6,
	@StartDate_7,
	@ExchangeRate_7,
	@StartDate_8,
	@ExchangeRate_8,
	@StartDate_9,
	@ExchangeRate_9,
	@StartDate_10,
	@ExchangeRate_10,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Update]
	@ErrorCode char(25),
	@PGNumber char(2),
	@TableID char(5),
	@ProcessClassification char(1),
	@MakerClassification char(1),
	@NumberOfKeyItems char(2),
	@CurrencyCode char(3),
	@CurrencyName nvarchar(250),
	@GenerationManagementIndication char(1),
	@DateOfMaintenanceUpdated datetime,
	@EndDate datetime,
	@StartDate_1 datetime,
	@ExchangeRate_1 decimal(9, 2),
	@StartDate_2 datetime,
	@ExchangeRate_2 decimal(9, 2),
	@StartDate_3 datetime,
	@ExchangeRate_3 decimal(9, 2),
	@StartDate_4 datetime,
	@ExchangeRate_4 decimal(9, 2),
	@StartDate_5 datetime,
	@ExchangeRate_5 decimal(9, 2),
	@StartDate_6 datetime,
	@ExchangeRate_6 decimal(9, 2),
	@StartDate_7 datetime,
	@ExchangeRate_7 decimal(9, 2),
	@StartDate_8 datetime,
	@ExchangeRate_8 decimal(9, 2),
	@StartDate_9 datetime,
	@ExchangeRate_9 decimal(9, 2),
	@StartDate_10 datetime,
	@ExchangeRate_10 decimal(9, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CurrencyExchange]
SET
	[ErrorCode] = @ErrorCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[MakerClassification] = @MakerClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[CurrencyName] = @CurrencyName,
	[GenerationManagementIndication] = @GenerationManagementIndication,
	[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
	[EndDate] = @EndDate,
	[StartDate_1] = @StartDate_1,
	[ExchangeRate_1] = @ExchangeRate_1,
	[StartDate_2] = @StartDate_2,
	[ExchangeRate_2] = @ExchangeRate_2,
	[StartDate_3] = @StartDate_3,
	[ExchangeRate_3] = @ExchangeRate_3,
	[StartDate_4] = @StartDate_4,
	[ExchangeRate_4] = @ExchangeRate_4,
	[StartDate_5] = @StartDate_5,
	[ExchangeRate_5] = @ExchangeRate_5,
	[StartDate_6] = @StartDate_6,
	[ExchangeRate_6] = @ExchangeRate_6,
	[StartDate_7] = @StartDate_7,
	[ExchangeRate_7] = @ExchangeRate_7,
	[StartDate_8] = @StartDate_8,
	[ExchangeRate_8] = @ExchangeRate_8,
	[StartDate_9] = @StartDate_9,
	[ExchangeRate_9] = @ExchangeRate_9,
	[StartDate_10] = @StartDate_10,
	[ExchangeRate_10] = @ExchangeRate_10,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[CurrencyCode] = @CurrencyCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]
	@ErrorCode char(25),
	@PGNumber char(2),
	@TableID char(5),
	@ProcessClassification char(1),
	@MakerClassification char(1),
	@NumberOfKeyItems char(2),
	@CurrencyCode char(3),
	@CurrencyName nvarchar(250),
	@GenerationManagementIndication char(1),
	@DateOfMaintenanceUpdated datetime,
	@EndDate datetime,
	@StartDate_1 datetime,
	@ExchangeRate_1 decimal(9, 2),
	@StartDate_2 datetime,
	@ExchangeRate_2 decimal(9, 2),
	@StartDate_3 datetime,
	@ExchangeRate_3 decimal(9, 2),
	@StartDate_4 datetime,
	@ExchangeRate_4 decimal(9, 2),
	@StartDate_5 datetime,
	@ExchangeRate_5 decimal(9, 2),
	@StartDate_6 datetime,
	@ExchangeRate_6 decimal(9, 2),
	@StartDate_7 datetime,
	@ExchangeRate_7 decimal(9, 2),
	@StartDate_8 datetime,
	@ExchangeRate_8 decimal(9, 2),
	@StartDate_9 datetime,
	@ExchangeRate_9 decimal(9, 2),
	@StartDate_10 datetime,
	@ExchangeRate_10 decimal(9, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [CurrencyCode] FROM [dbo].[t_VNACC_Category_CurrencyExchange] WHERE [CurrencyCode] = @CurrencyCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CurrencyExchange] 
		SET
			[ErrorCode] = @ErrorCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[MakerClassification] = @MakerClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[CurrencyName] = @CurrencyName,
			[GenerationManagementIndication] = @GenerationManagementIndication,
			[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
			[EndDate] = @EndDate,
			[StartDate_1] = @StartDate_1,
			[ExchangeRate_1] = @ExchangeRate_1,
			[StartDate_2] = @StartDate_2,
			[ExchangeRate_2] = @ExchangeRate_2,
			[StartDate_3] = @StartDate_3,
			[ExchangeRate_3] = @ExchangeRate_3,
			[StartDate_4] = @StartDate_4,
			[ExchangeRate_4] = @ExchangeRate_4,
			[StartDate_5] = @StartDate_5,
			[ExchangeRate_5] = @ExchangeRate_5,
			[StartDate_6] = @StartDate_6,
			[ExchangeRate_6] = @ExchangeRate_6,
			[StartDate_7] = @StartDate_7,
			[ExchangeRate_7] = @ExchangeRate_7,
			[StartDate_8] = @StartDate_8,
			[ExchangeRate_8] = @ExchangeRate_8,
			[StartDate_9] = @StartDate_9,
			[ExchangeRate_9] = @ExchangeRate_9,
			[StartDate_10] = @StartDate_10,
			[ExchangeRate_10] = @ExchangeRate_10,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[CurrencyCode] = @CurrencyCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_CurrencyExchange]
	(
			[ErrorCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[MakerClassification],
			[NumberOfKeyItems],
			[CurrencyCode],
			[CurrencyName],
			[GenerationManagementIndication],
			[DateOfMaintenanceUpdated],
			[EndDate],
			[StartDate_1],
			[ExchangeRate_1],
			[StartDate_2],
			[ExchangeRate_2],
			[StartDate_3],
			[ExchangeRate_3],
			[StartDate_4],
			[ExchangeRate_4],
			[StartDate_5],
			[ExchangeRate_5],
			[StartDate_6],
			[ExchangeRate_6],
			[StartDate_7],
			[ExchangeRate_7],
			[StartDate_8],
			[ExchangeRate_8],
			[StartDate_9],
			[ExchangeRate_9],
			[StartDate_10],
			[ExchangeRate_10],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ErrorCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@MakerClassification,
			@NumberOfKeyItems,
			@CurrencyCode,
			@CurrencyName,
			@GenerationManagementIndication,
			@DateOfMaintenanceUpdated,
			@EndDate,
			@StartDate_1,
			@ExchangeRate_1,
			@StartDate_2,
			@ExchangeRate_2,
			@StartDate_3,
			@ExchangeRate_3,
			@StartDate_4,
			@ExchangeRate_4,
			@StartDate_5,
			@ExchangeRate_5,
			@StartDate_6,
			@ExchangeRate_6,
			@StartDate_7,
			@ExchangeRate_7,
			@StartDate_8,
			@ExchangeRate_8,
			@StartDate_9,
			@ExchangeRate_9,
			@StartDate_10,
			@ExchangeRate_10,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Delete]
	@CurrencyCode char(3)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CurrencyExchange]
WHERE
	[CurrencyCode] = @CurrencyCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CurrencyExchange] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Load]
	@CurrencyCode char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CurrencyExchange]
WHERE
	[CurrencyCode] = @CurrencyCode
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CurrencyExchange] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CurrencyExchange]	

GO

