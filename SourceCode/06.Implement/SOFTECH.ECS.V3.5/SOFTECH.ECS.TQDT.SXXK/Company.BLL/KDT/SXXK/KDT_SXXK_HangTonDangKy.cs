using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
    public partial class KDT_SXXK_HangTonDangKy 
	{
        private List<KDT_SXXK_HangTon> _ListHangTon = new List<KDT_SXXK_HangTon>();

        public List<KDT_SXXK_HangTon> hangTonList
        {
            set { this._ListHangTon = value; }
            get { return this._ListHangTon; }
        }

        public static KDT_SXXK_HangTonDangKy LoadFull(long id)
        {
            try
            {
                KDT_SXXK_HangTonDangKy hangTonDK = new KDT_SXXK_HangTonDangKy();
                hangTonDK = KDT_SXXK_HangTonDangKy.Load(id);
                List<KDT_SXXK_HangTon> list = KDT_SXXK_HangTon.SelectCollectionDynamic(" Master_ID = " + id, "");
                if (hangTonDK.ID > 0)
                {
                    hangTonDK.hangTonList = list;
                    return hangTonDK;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                return null;
            }
        
        }
     
        public bool InsertUpdatFull()
        {
            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                        this.Update(transaction);
                    foreach (KDT_SXXK_HangTon item in this.hangTonList)
                    {
                        item.Master_id = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                    throw;
                }
            }
            return ret;
        }
        public void DeleteFull(long id)
        {
            this.ID = id;
            KDT_SXXK_HangTon.DeleteDynamic("Master_ID = " + id);
            this.Delete();
        }
        
	}	
}