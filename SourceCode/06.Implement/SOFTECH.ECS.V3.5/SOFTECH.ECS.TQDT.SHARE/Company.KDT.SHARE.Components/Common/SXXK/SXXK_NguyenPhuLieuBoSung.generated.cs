using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.Common.SXXK
{
	public partial class SXXK_NguyenPhuLieuBoSung : ICloneable
	{
		#region Properties.
		
		public string MaHaiQuan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string Ma { set; get; }
		public bool NPLChinh { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<SXXK_NguyenPhuLieuBoSung> ConvertToCollection(IDataReader reader)
		{
			IList<SXXK_NguyenPhuLieuBoSung> collection = new List<SXXK_NguyenPhuLieuBoSung>();
			while (reader.Read())
			{
				SXXK_NguyenPhuLieuBoSung entity = new SXXK_NguyenPhuLieuBoSung();
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("NPLChinh"))) entity.NPLChinh = reader.GetBoolean(reader.GetOrdinal("NPLChinh"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_SXXK_NguyenPhuLieuBoSung VALUES(@MaHaiQuan, @MaDoanhNghiep, @Ma, @NPLChinh)";
            string update = "UPDATE t_SXXK_NguyenPhuLieuBoSung SET MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, Ma = @Ma, NPLChinh = @NPLChinh WHERE ID = @ID";
            string delete = "DELETE FROM t_SXXK_NguyenPhuLieuBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NPLChinh", SqlDbType.Bit, "NPLChinh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NPLChinh", SqlDbType.Bit, "NPLChinh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_SXXK_NguyenPhuLieuBoSung VALUES(@MaHaiQuan, @MaDoanhNghiep, @Ma, @NPLChinh)";
            string update = "UPDATE t_SXXK_NguyenPhuLieuBoSung SET MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, Ma = @Ma, NPLChinh = @NPLChinh WHERE ID = @ID";
            string delete = "DELETE FROM t_SXXK_NguyenPhuLieuBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NPLChinh", SqlDbType.Bit, "NPLChinh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NPLChinh", SqlDbType.Bit, "NPLChinh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static SXXK_NguyenPhuLieuBoSung Load(string maHaiQuan, string maDoanhNghiep, string ma)
		{
			const string spName = "[dbo].[p_SXXK_NguyenPhuLieuBoSung_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, ma);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<SXXK_NguyenPhuLieuBoSung> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<SXXK_NguyenPhuLieuBoSung> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<SXXK_NguyenPhuLieuBoSung> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertSXXK_NguyenPhuLieuBoSung(string maHaiQuan, string maDoanhNghiep, string ma, bool nPLChinh)
		{
			SXXK_NguyenPhuLieuBoSung entity = new SXXK_NguyenPhuLieuBoSung();	
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.Ma = ma;
			entity.NPLChinh = nPLChinh;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_SXXK_NguyenPhuLieuBoSung_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@NPLChinh", SqlDbType.Bit, NPLChinh);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<SXXK_NguyenPhuLieuBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_NguyenPhuLieuBoSung item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateSXXK_NguyenPhuLieuBoSung(string maHaiQuan, string maDoanhNghiep, string ma, bool nPLChinh)
		{
			SXXK_NguyenPhuLieuBoSung entity = new SXXK_NguyenPhuLieuBoSung();			
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.Ma = ma;
			entity.NPLChinh = nPLChinh;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_SXXK_NguyenPhuLieuBoSung_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@NPLChinh", SqlDbType.Bit, NPLChinh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<SXXK_NguyenPhuLieuBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_NguyenPhuLieuBoSung item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateSXXK_NguyenPhuLieuBoSung(string maHaiQuan, string maDoanhNghiep, string ma, bool nPLChinh)
		{
			SXXK_NguyenPhuLieuBoSung entity = new SXXK_NguyenPhuLieuBoSung();			
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.Ma = ma;
			entity.NPLChinh = nPLChinh;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SXXK_NguyenPhuLieuBoSung_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@NPLChinh", SqlDbType.Bit, NPLChinh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<SXXK_NguyenPhuLieuBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_NguyenPhuLieuBoSung item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteSXXK_NguyenPhuLieuBoSung(string maHaiQuan, string maDoanhNghiep, string ma)
		{
			SXXK_NguyenPhuLieuBoSung entity = new SXXK_NguyenPhuLieuBoSung();
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.Ma = ma;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SXXK_NguyenPhuLieuBoSung_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<SXXK_NguyenPhuLieuBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_NguyenPhuLieuBoSung item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}