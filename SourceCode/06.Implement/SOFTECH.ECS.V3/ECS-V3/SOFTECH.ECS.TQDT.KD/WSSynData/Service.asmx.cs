﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;
using System.Data;

namespace WSSynData
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        #region USER AGENT - ĐẠI LÝ

        /// <summary>
        /// Kiểm tra thông tin người dùng Đại lý.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <returns></returns>
        [WebMethod]
        public string LoginDaiLy(string userNameLogin, string passWordLogin)
        {
            DaiLy dl = _LoginDaiLy(userNameLogin, passWordLogin);
            if (dl != null)
                return Helpers.Serializer(dl);
            else
                return string.Empty;
        }

        private Company.KDT.SHARE.Components.DaiLy _LoginDaiLy(string userNameLogin, string passWordLogin)
        {
            Company.KDT.SHARE.Components.DaiLy user = Company.KDT.SHARE.Components.DaiLy.Load(userNameLogin, passWordLogin);
            return user;
        }

        /// <summary>
        /// Danh sách người dùng Đại lý
        /// </summary>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public string SelectUserDaiLy(string maDoanhNghiep, string userNameLogin, string passWordLogin)
        {
             DaiLy dl = _LoginDaiLy(userNameLogin, passWordLogin);
             if (dl != null && dl.isAdmin && dl.MaDoanhNghiep == maDoanhNghiep)
             {
                 List<Company.KDT.SHARE.Components.DaiLy> listDl = Company.KDT.SHARE.Components.DaiLy.SelectUserDaiLy(maDoanhNghiep);
                 if (listDl != null && listDl.Count > 0)
                 {
                     foreach (DaiLy item in listDl)
                     {
                         item.PASSWORD = "";
                     }
                     return Helpers.Serializer(listDl);
                 }
                 return string.Empty;
             }
             else
             {
                 return "MK";
             }  

        }

        [WebMethod]
        public string SelectUserDaiLyAll(string userNameLogin, string passWordLogin)
        {
            DaiLy dl = _LoginDaiLy(userNameLogin, passWordLogin);
            if (dl != null && dl.isAdmin)
            {
                List<Company.KDT.SHARE.Components.DaiLy> listDL = (List<Company.KDT.SHARE.Components.DaiLy>)Company.KDT.SHARE.Components.DaiLy.SelectCollectionAll();
                if (listDL != null && listDL.Count > 0)
                {
                    foreach (DaiLy item in listDL)
                    {
                        item.PASSWORD = "";
                    }
                    return Helpers.Serializer(listDL);
                }
                else
                    return string.Empty;
            }
            else
                return "MK";

        }

        /// <summary>
        /// Tạo mới người dùng Đại lý
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="tenNguoiDung"></param>
        /// <param name="matKhau"></param>
        /// <param name="hoVaTen"></param>
        /// <param name="moTa"></param>
        /// <param name="laQuanTri"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public int InsertUpdateUserDaiLy(string userNameLogin, string passWordLogin, string tenNguoiDung, string matKhau, string hoVaTen, string moTa, string maDoanhNghiep)
        {
            //Neu login thanh cong
            //Khanhhn - nếu là admin của doanh nghiệp đó mới được thêm
            DaiLy dl = _LoginDaiLy(userNameLogin, passWordLogin);
            if (dl != null && dl.isAdmin && dl.MaDoanhNghiep == maDoanhNghiep)
                return Company.KDT.SHARE.Components.DaiLy.InsertUpdateUserDaiLy_V3(tenNguoiDung, matKhau, hoVaTen, moTa,  maDoanhNghiep);
            else
                return 0;
        }

        /// <summary>
        /// Xóa người dùng Đại lý
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="tenNguoiDung"></param>
        /// <param name="matKhau"></param>
        /// <returns></returns>
        [WebMethod]
        public bool DeleteUserDaiLy(string userNameLogin, string passWordLogin, string tenNguoiDung)
        {
            //Neu login thanh cong
            DaiLy dl = _LoginDaiLy(userNameLogin, passWordLogin);
            if (dl != null && dl.isAdmin)
            {
                List<DaiLy> listDL = (List<DaiLy>)DaiLy.SelectCollectionDynamic("UserName = " + tenNguoiDung, null);
                if (listDL == null || listDL.Count == 0)
                    return false;
                DaiLy deleteDl = listDL[0];
                if (deleteDl.MaDoanhNghiep == dl.MaDoanhNghiep)
                {
                    deleteDl.Delete();
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Kiểm tra tên người dùng đã tồn tại?.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public bool CheckUserName(string userName)
        {
            Company.QuanTri.User user = new Company.QuanTri.User();
            return user.CheckUserName(userName);
        }

        /// <summary>
        /// Tạo mới cấu hình số tờ khai quy định Đại lý phải tải lên server?.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        //[WebMethod]
        //public int InsertUpdateCauHinhDaiLy(string maDoanhNghiep, string tenDoanhNghiep, string key, string value, string ghiChu)
        //{
        //    return Company.KDT.SHARE.Components.CauHinhDaiLy.InsertUpdateCauHinh(maDoanhNghiep, tenDoanhNghiep, key, value, ghiChu);
        //}

        /// <summary>
        /// Kiểm tra cấu hình số tờ khai quy định Đại lý phải tải lên server?.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        //[WebMethod]
        //public string CheckCauHinhSoToKhai(string maDoanhNghiep, string key)
        //{
        //    Company.KDT.SHARE.Components.CauHinhDaiLy cauhinh = new Company.KDT.SHARE.Components.CauHinhDaiLy();

        //    cauhinh = Company.KDT.SHARE.Components.CauHinhDaiLy.Load(maDoanhNghiep, key);

        //    if (cauhinh != null)
        //    {
        //        return string.IsNullOrEmpty(cauhinh.Value) != true ? cauhinh.Value : "";
        //    }

        //    return "";
        //}

        #endregion

        #region TỜ KHAI

        /// <summary>
        /// Gửi dữ liệu một tờ khai và hàng mậu dịch, các thông tin liên quan đến tờ khai.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="TKMD"></param>
        /// <param name="overwrite"></param>
        /// <returns></returns>
        [WebMethod]
        public string Send_SXXK(string userNameLogin, string passWordLogin, string strTKMD)
        {
            string error = "";
            //Neu login thanh cong
            Company.KDT.SHARE.Components.DaiLy userDaiLy = _LoginDaiLy(userNameLogin, passWordLogin);

            if (userDaiLy != null)
            {
                Company.BLL.KDT.ToKhaiMauDich TKMD = Helpers.Deserialize<Company.BLL.KDT.ToKhaiMauDich>(strTKMD);
                try
                {
                    //Insert to khai va hang mau dich
                    TKMD.InsertUpdateFullDaiLy();
                    error = "";
                }
                catch (Exception ex) { error = ex.Message; }

                //Them vao bang t_DongBoDuLieu_Track
                Company.KDT.SHARE.Components.Track newTrack = new Company.KDT.SHARE.Components.Track();
                newTrack.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                newTrack.MaHaiQuan = TKMD.MaHaiQuan;
                newTrack.SoToKhai = TKMD.SoToKhai;
                newTrack.NamDangKy = TKMD.NamDK;
                newTrack.MaLoaiHinh = TKMD.MaLoaiHinh;
                newTrack.TKMD_GUIDSTR = TKMD.GUIDSTR;
                //Ghi log Error neu co.
                newTrack.GhiChu = error;
                //1: Thanh cong; 0: Khong thanh cong.
                newTrack.TrangThaiDongBoTaiLen = error.Length == 0 ? 1 : 0;
                newTrack.TrangThaiDongBoTaiXuong = 0;
                //ID user dai ly gui du lieu den Server.
                newTrack.IDUserDaiLy = userDaiLy != null ? userDaiLy.ID : 0;
                newTrack.UserName = userDaiLy != null ? userDaiLy.USER_NAME : "";
                newTrack.NgayDongBo = DateTime.Now;

                newTrack.InsertUpdateBy();
            }
            else
                return "MK";

            return error;
        }


        /// <summary>
        /// Tìm kiếm thông tin tờ khai
        /// </summary>
        /// <param name="userNameLogin"> user thực hiện</param>
        /// <param name="passWordLogin"> pass </param>
        /// <param name="listDaiLy"> Tìm kiếm theo list đại lý </param>
        /// <param name="MaDoanhNghiep"> Mã doanh nghiệp đang thực hiện </param>
        /// <param name="FromNgayDongBo"> Ngày đồng bộ </param>
        /// <param name="ToNgayDongBo"> Ngày đồng bộ </param>
        /// <returns></returns>
        [WebMethod]
        public string SeachTKMD_SXXK(string userNameLogin, string passWordLogin, string listDaiLy, string MaDoanhNghiep, string FromNgayDongBo, string ToNgayDongBo)
        {
            DaiLy dl = _LoginDaiLy(userNameLogin,passWordLogin);
            if (dl != null && dl.MaDoanhNghiep == MaDoanhNghiep)
            {
                List<DaiLy> listDL = Helpers.Deserialize<List<DaiLy>>(listDaiLy);
                Company.BLL.KDT.ToKhaiMauDichCollection listTk = new Company.BLL.KDT.ToKhaiMauDichCollection();
                foreach (DaiLy item in listDL)
                {
                    listTk.AddRange(new Company.BLL.KDT.ToKhaiMauDich().seachTKMDDBDL(MaDoanhNghiep, Convert.ToDateTime(FromNgayDongBo), Convert.ToDateTime(ToNgayDongBo), item.USER_NAME));
                }
                return Helpers.Serializer(listTk);
            }
            else
                return "MK";
        }

        /// <summary>
        /// Gửi dữ liệu danh sách tờ khai và hàng mậu dịch, các thông tin liên quan đến tờ khai.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="TKMD"></param>
        /// <param name="overwrite"></param>
        /// <returns></returns>
        //[WebMethod]
        //public string Sends(string userNameLogin, string passWordLogin, ref Company.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        //{
        //    string error = "";
        //    //Neu login thanh cong
        //    Company.KDT.SHARE.Components.DaiLy userDaiLy = LoginDaiLy(userNameLogin, passWordLogin);

        //    if (userDaiLy != null)
        //    {
        //        foreach (Company.BLL.KDT.ToKhaiMauDich item in TKMDCollection)
        //        {
        //            try
        //            {
        //                //Insert to khai va hang mau dich
        //                item.InsertUpdateFull();
        //                error = "";
        //            }
        //            catch (Exception ex) { error = ex.Message; }

        //            //Them vao bang t_DongBoDuLieu_Track
        //            Company.KDT.SHARE.Components.Track newTrack = new Company.KDT.SHARE.Components.Track();
        //            newTrack.MaDoanhNghiep = item.MaDoanhNghiep;
        //            newTrack.MaHaiQuan = item.MaHaiQuan;
        //            newTrack.SoToKhai = item.SoToKhai;
        //            newTrack.NamDangKy = item.NamDK;
        //            newTrack.MaLoaiHinh = item.MaLoaiHinh;
        //            newTrack.TKMD_GUIDSTR = item.GUIDSTR;
        //            //Ghi log Error neu co.
        //            newTrack.GhiChu = error;
        //            //1: Thanh cong; 0: Khong thanh cong.
        //            newTrack.TrangThaiDongBoTaiLen = error.Length == 0 ? 1 : 0;
        //            newTrack.TrangThaiDongBoTaiXuong = 0;
        //            //ID user dai ly gui du lieu den Server.
        //            newTrack.IDUserDaiLy = userDaiLy != null ? userDaiLy.ID : 0;
        //            newTrack.UserName = userDaiLy != null ? userDaiLy.USER_NAME : "";
        //            newTrack.NgayDongBo = DateTime.Now;

        //            newTrack.Insert();

        //            //Neu co loi xay ra -> Thoat khoi vong lap
        //            if (error.Length > 0) break;
        //        }
        //    }

        //    return error;
        //}

        /// <summary>
        /// Lấy thông tin danh sách tờ khai theo mã doanh nghiệp.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        //[WebMethod]
        //public Company.BLL.KDT.ToKhaiMauDichCollection Receives(string userNameLogin, string passWordLogin, string maDoanhNghiep, DateTime tuNgay, DateTime denNgay)
        //{
        //    //Neu login thanh cong
        //    if (LoginDaiLy(userNameLogin, passWordLogin) != null)
        //    {
        //        Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
        //        foreach (Company.BLL.KDT.ToKhaiMauDich item in tkmd.SelectCollectionByDaiLy(maDoanhNghiep, tuNgay, denNgay))
        //        {

        //            tkmd.LoadHMDCollection();
        //            //Load Chung tu kem lien quan cua To khai.
        //            //tkmd.LoadChungTuHaiQuan();
        //        }
        //    }

        //    return null;
        //}

        /// <summary>
        /// Lấy thông tin danh sách tờ khai theo mã doanh nghiệp.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        //[WebMethod]
        //public Company.BLL.KDT.ToKhaiMauDichCollection ReceivesByDaiLy(string userNameLogin, string passWordLogin, string maDoanhNghiep, DateTime tuNgay, DateTime denNgay, int idUserDaiLy)
        //{
        //    //Neu login thanh cong
        //    if (LoginDaiLy(userNameLogin, passWordLogin) != null)
        //    {
        //        Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();

        //        return tkmd.SelectCollectionByDaiLy(maDoanhNghiep, tuNgay, denNgay, idUserDaiLy);
        //    }

        //    return null;
        //}

        /// <summary>
        /// Lấy thông tin tờ khai theo mã doanh nghiệp.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="soToKhai"></param>
        /// <param name="namDangKy"></param>
        /// <param name="maLoaiHinh"></param>
        /// <param name="maHaiQuan"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public string Receive_SXXK(string userNameLogin, string passWordLogin, int soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep)
        {
            //Neu login thanh cong
            DaiLy dl =_LoginDaiLy(userNameLogin, passWordLogin);
            if (dl != null && dl.MaDoanhNghiep == maDoanhNghiep)
            {
                Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
                tkmd.Load(maHaiQuan, maDoanhNghiep, soToKhai, namDangKy, maLoaiHinh);

                //Load Hang hoa mau dich
                tkmd.LoadHMDCollection();

                //Load Chung tu kem lien quan cua To khai.
                tkmd.LoadChungTuHaiQuan();

                return Helpers.Serializer(tkmd);
            }
            return "MK";
        }





        #endregion

        #region TRACK

        /// <summary>
        /// Update thông tin log sau khi nhận tờ khai về 
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="soToKhai"></param>
        /// <param name="namDangKy"></param>
        /// <param name="maLoaiHinh"></param>
        /// <param name="maHaiQuan"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        [WebMethod]
        public string UpdateTrack(string userNameLogin, string passWordLogin, long soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep, string msg)
        {
             DaiLy dl =_LoginDaiLy(userNameLogin, passWordLogin);
             if (dl != null && dl.MaDoanhNghiep == maDoanhNghiep)
             {
                 try
                 {
             
                 Track tr = Company.KDT.SHARE.Components.Track.Load(soToKhai, namDangKy, maLoaiHinh, maHaiQuan, maDoanhNghiep);
                 if (tr != null)
                 {
                     tr.GhiChu = msg;
                     tr.TrangThaiDongBoTaiXuong = string.IsNullOrEmpty(msg) ? 1 : 0;
                     tr.Update();
                     return string.Empty;
                 }
                 else
                     return "Không tìm thấy track";
                 }
                 catch (System.Exception ex)
                 {
                     return ex.Message;
                 }
             }
             return "MK";
                
        }


// 
//         [WebMethod]
//         public bool InsertUpdateTrack(string userNameLogin, string passWordLogin, Company.KDT.SHARE.Components.Track log)
//         {
//             //Neu login thanh cong
//             if (_LoginDaiLy(userNameLogin, passWordLogin) != null)
//             {
//                 log.InsertUpdateBy();
// 
//                 return true;
//             }
// 
//             return false;
//         }

        [WebMethod]
        public string SelectTrack(string userNameLogin, string passWordLogin, long soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep)
        {
            //Neu login thanh cong
            DaiLy dl = _LoginDaiLy(userNameLogin, passWordLogin);
            if (dl != null && dl.MaDoanhNghiep == maDoanhNghiep)
            {
                Track tr = Company.KDT.SHARE.Components.Track.Load(soToKhai, namDangKy, maLoaiHinh, maHaiQuan, maDoanhNghiep);
                if (tr != null)
                {
                    return Helpers.Serializer(tr);
                }
            }
            else
                return "MK";
            return string.Empty;
        }

        [WebMethod]
        public string SelectTrackDynamic(string userNameLogin, string passWordLogin, string whereCondition, string orderBy)
        {
            //Neu login thanh cong
            if (_LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                List<Company.KDT.SHARE.Components.Track> listtr = (List<Company.KDT.SHARE.Components.Track>)Company.KDT.SHARE.Components.Track.SelectCollectionDynamic(whereCondition, orderBy);
                if (listtr!= null && listtr.Count>0)
                {
                    return Helpers.Serializer(listtr);
                }
                return
                    string.Empty;
            }

            return "MK";
        }

        #endregion

        #region MỚI

        [WebMethod]
        public string CheckCauHinhDL(string MaDoanhNghiep)
        {
            string check = string.Empty;

            try
            {
                CauHinh ch = new CauHinh();
                ch = CauHinh.Load(MaDoanhNghiep);
                if (ch != null)
                {
                    if (ch.IsUse == 1)
                        check = ch.Value.ToString();
                    else
                        check = string.Empty;
                }
                else
                {
                    check = "NULL";
                }
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return check;
        }

        /// <summary>
        /// Tạo mới cấu hình số tờ khai quy định Đại lý phải tải lên server?.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public bool InsertUpdateCauHinhDaiLy(string userNameLogin, string passWordLogin, string MaDaiLy, string maDoanhNghiep, string tenDoanhNghiep, int value, string ghiChu, int isUse)
        {
            try
            {
                //Neu login thanh cong
                if (_LoginDaiLy(userNameLogin, passWordLogin) != null)
                {
                    CauHinh.InsertUpdateCauHinh(MaDaiLy, maDoanhNghiep, tenDoanhNghiep, value, ghiChu, isUse);

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex) { }

            return false;
        }

        /// <summary>
        /// Gửi dữ liệu từ máy đại lý
        /// </summary>
        /// <param name="message"></param>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <returns></returns>
        [WebMethod]
        public string SendDaiLy(string message, string userNameLogin, string passWordLogin)
        {
            string error = string.Empty;
            //Neu login thanh cong
            Company.KDT.SHARE.Components.DaiLy userDaiLy = _LoginDaiLy(userNameLogin, passWordLogin);
            if (userDaiLy != null && !userDaiLy.isAdmin)
            {
                try
                {

                    SyncDaTa msgSend = Helpers.Deserialize<SyncDaTa>(message);

                    DBDL dbdl = new DBDL();
                    dbdl.TaoDuLieu(msgSend);
                    dbdl.MSG_FROM = userNameLogin;
                    dbdl.MSG_ORIGIN = message;

                    Detail chitietToKhai = new Detail();
                    chitietToKhai.TaoDuLieu(msgSend.Declaration[0]);

                    DBDL dbdl2 = new DBDL();
                    dbdl2 = DBDL.LoadByGuiStr(dbdl.GUIDSTR);
                    if (dbdl2 != null)
                    {
                        dbdl.ID = dbdl2.ID;
                        dbdl.Update();
                    }
                    else
                        dbdl.InsertUpdate();
                    Detail chitietToKhai2 = new Detail();
                    chitietToKhai2 = Detail.LoadByGuiStr(dbdl.GUIDSTR);
                    if (chitietToKhai2 != null)
                    {
                        chitietToKhai.ID = chitietToKhai2.ID;
                        chitietToKhai.Update();
                    }
                    else
                    {
                        chitietToKhai.InsertUpdate();
                    }
                }
                catch (System.Exception ex)
                {
                    return ex.Message;
                }
            }
            else
                error = "MK";
            return error;
        }
        /// <summary>
        /// Doanh nghiệp nhận tờ khai
        /// </summary>
        /// <param name="guiStr"></param>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <returns></returns>
        [WebMethod]
        public string CheckDaiLy(string maDaiLy, string maDoanhNghiep)
        {
            string empty = string.Empty;
            int checkDaiLy = Company.KDT.SHARE.Components.CauHinh.SelectDynamic("MaDaily = '"+maDaiLy+"'", null).Tables[0].Rows.Count;
            if (checkDaiLy >=1)
            {
                DataSet dataset = new DataSet();
                string where = "MaDoanhNghiep = '" + maDoanhNghiep + "' And MaDaiLy = '" + maDaiLy + "'";
                int check = Company.KDT.SHARE.Components.CauHinh.SelectDynamic(where, null).Tables[0].Rows.Count;
                if (check == 0)
                {
                    empty = "Ma doanh nghiệp " + maDoanhNghiep + " chưa được đăng ký khai báo đại lý. Xin vui lòng liên hệ nhà cung cấp để được hỗ trợ.";
                }
            }
            else
            {
                empty = "Mã đại lý "+maDaiLy+" không đúng hoặc không chưa đăng ký . Xin vui lòng liên hệ nhà cung cấp để được hỗ trợ.";
            }
            return empty;
        }

        private HeThongPhongKhai CheckDaiLy()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Doanh nghiệp nhận tờ khai
        /// </summary>
        /// <param name="guiStr"></param>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <returns></returns>
        
        
        [WebMethod]
        public string SendDoanhNghiep(string guiStr, string userNameLogin, string passWordLogin)
        {
            string xml = string.Empty;
            try
            {
                //Neu login thanh cong
                Company.KDT.SHARE.Components.DaiLy userDaiLy = _LoginDaiLy(userNameLogin, passWordLogin);
                if (userDaiLy != null && userDaiLy.isAdmin)
                {
                    DBDL DongBo = DBDL.LoadByGuiStr(guiStr);
                    xml = DongBo.MSG_ORIGIN;
                    if (!string.IsNullOrEmpty(xml))
                    {
                        Detail chitiet = Detail.LoadByGuiStr(guiStr);
                        chitiet.TrangThai = 1;
                        chitiet.Update();
                        DongBo.STATUS = 1;
                        DongBo.Update();
                    }
                }
                else
                    xml = "MK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return xml;
        }

        /// <summary>
        /// Tìm kiếm thông tin tờ khai từ đại lý
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <param name="tuNgay"></param>
        /// <param name="denNgay"></param>
        /// <returns></returns>
        [WebMethod]
        public string ReceivesViews(string userNameLogin, string passWordLogin, string messages)
        {
            string xml = string.Empty;
            try
            {
                //Neu login thanh cong
                Company.KDT.SHARE.Components.DaiLy userDaiLy = _LoginDaiLy(userNameLogin, passWordLogin);
                if (userDaiLy != null && userDaiLy.isAdmin)
                {
                    //- nhận
                    string content = Helpers.ConvertFromBase64(messages);
                    SyncDaTa syncdata = Helpers.Deserialize<SyncDaTa>(content);
                    string maDoanhNghiep = syncdata.issuer;
                    DateTime tuNgay = Convert.ToDateTime(syncdata.FromDate.Trim());
                    DateTime denNgay = Convert.ToDateTime(syncdata.ToDate.Trim());
                    string phanhe = syncdata.Type;
                    //- xử lý

                    string condition = string.Format("");
                    List<Detail> detailCo = new List<Detail>();
                    if (syncdata.Status == "0")
                        detailCo = Detail.LoadCollectionDetail(phanhe, maDoanhNghiep, tuNgay, denNgay, true);
                    else
                        detailCo = Detail.LoadCollectionDetail(phanhe, maDoanhNghiep, tuNgay, denNgay);
                    SyncDaTa syncdataSend = new SyncDaTa() { Declaration = new List<SyncDaTaDetail>() };
                    foreach (Detail item in detailCo)
                    {
                        SyncDaTaDetail chitet = new SyncDaTaDetail();
                        chitet.Issuer = item.MaDaiLy;
                        chitet.Reference = item.GUIDSTR;
                        chitet.CustomsReference = item.SoTiepNhan.ToString();
                        chitet.Acceptance = item.NgayDangKy.ToString("yyyy-MM-dd");
                        chitet.NatureOfTransaction = item.MaLoaiHinh;
                        chitet.Number = item.SoToKhai.ToString();
                        chitet.DeclarationOffice = item.MaHaiQuan;
                        chitet.StreamlineContent = item.PhanLuong;
                        chitet.DateSend = item.NgayGui.ToString("yyyy-MM-dd");
                        chitet.Status = item.TrangThai.ToString();
                        syncdataSend.Declaration.Add(chitet);
                    }
                    //- trả về
                    xml = Helpers.Serializer(syncdataSend);
                }
                else
                    xml = "MK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return xml;
        }
        /// <summary>
        /// Send Phụ kiện - Hợp đồng - định mức GC
        /// </summary>
        /// <param name="message"></param>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <returns></returns>
        [WebMethod]
        public string SendGC(string message, string userNameLogin, string passWordLogin)
        {
            string error = string.Empty;
            //Neu login thanh cong
            Company.KDT.SHARE.Components.DaiLy userDaiLy = _LoginDaiLy(userNameLogin, passWordLogin);
            if (userDaiLy != null && !userDaiLy.isAdmin)
            {
                try
                {
                    SyncDaTa msgSend = Helpers.Deserialize<SyncDaTa>(message);
                    DBDL dbdl = new DBDL();
                    dbdl.TaoDuLieu(msgSend);
                    dbdl.MSG_FROM = userNameLogin;
                    dbdl.MSG_ORIGIN = message;
                    DBDL dbdl2 = DBDL.LoadByGuiStr(msgSend.Declaration[0].Reference);
                    if (dbdl2 != null && dbdl2.ID > 0)
                    {
                        dbdl.ID = dbdl2.ID;
                        dbdl.Update();
                    }
                    else
                    {
                        dbdl.Insert();
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
            else
                return "MK";
            return error;
        }
        /// <summary>
        /// Doanh nghiệp nhận thông tin theo Type
        /// </summary>
        /// <param name="Type">Type = HDGC - DMGC - PKGC </param>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <returns></returns>
        [WebMethod]
        public string ReceivesGC(string Type, string userNameLogin, string passWordLogin)
        {
            string xml = string.Empty;
            try
            {
                Company.KDT.SHARE.Components.DaiLy userDaiLy = _LoginDaiLy(userNameLogin, passWordLogin);
                if (userDaiLy != null && userDaiLy.isAdmin)
                {

                    string condition = string.Format("MSG_TYPE = '{0}' AND STATUS = 0 ", Type);
                    List<DBDL> DongBo = DBDL.SelectCollectionDynamic(condition, null);
                    if (DongBo != null && DongBo.Count > 0)
                    {
                        xml = DongBo[0].MSG_ORIGIN;
                        DongBo[0].STATUS = 1;
                        DongBo[0].Update();
                    }
                    else
                    {
                        xml = string.Empty;
                    }
                }
                else
                    xml = "MK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return xml;
        }

        /// <summary>
        /// Kiểm tra user đại lý
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <returns></returns>
        [WebMethod]
        public string LoginDaiLy_V3(string userNameLogin, string passWordLogin)
        {
            Company.KDT.SHARE.Components.DaiLy user = Company.KDT.SHARE.Components.DaiLy.Load(userNameLogin, passWordLogin);
            if (user != null && !user.isAdmin)
                return user.USER_NAME;
            else
                return string.Empty;
        }
        #endregion

        #region CAP NHAT DANH MUC ONLINE

        [WebMethod]
        public System.Data.DataSet GetDanhMucOnline(string passWordLogin, DateTime dateLastUpdated, Company.KDT.SHARE.Components.EDanhMucHaiQuan type)
        {
            System.Data.DataSet ds = null;

            try
            {
                if (passWordLogin != "daibangtungcanh")
                    return null;

                ds = new System.Data.DataSet("DanhMucOnline");

                string where = string.Format("DateModified > '{0}'", dateLastUpdated.ToString("yyyy-MM-dd hh:mm:ss.fff tt"));

                switch (type)
                {
                    case EDanhMucHaiQuan.CuaKhau:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.CuaKhau.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.Cuc:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.DieuKienGiaoHang:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.DieuKienGiaoHang.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.DonViHaiQuan:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.DonViTinh:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViTinh.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.LoaiPhiChungTuThanhToan:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.LoaiPhiChungTuThanhToan.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.MaHS:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.MaHS.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.NguyenTe:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.Nuoc:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.Nuoc.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.PhuongThucThanhToan:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.PhuongThucThanhToan.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.PhuongThucVanTai:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.PhuongThucVanTai.SelectDynamic(where, "");
                        break;
                    case EDanhMucHaiQuan.LoaiHinhMauDich:
                        ds = Company.KDT.SHARE.Components.DanhMucHaiQuan.LoaiHinhMauDich.SelectDynamic(where, "");
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                if (ds.Tables.Count == 0)
                {
                    DataTable dtError = new System.Data.DataTable("dtError");
                    DataColumn col = new DataColumn("Error", typeof(string));
                    dtError.Columns.Add(col);
                    ds.Tables.Add(dtError);

                    DataRow dr = dtError.NewRow();
                    dr["Error"] = ex.Message;
                    dtError.Rows.Add(dr);
                }
            }

            return ds;
        }

        #endregion

        #region THU VIEN GOP Y - GIAI DAP

        /// <summary>
        /// Danh mục lĩnh vực phân nhóm câu hỏi.
        /// </summary>
        /// <param name="passWordLogin"></param>
        /// <param name="dateLastUpdated"></param>
        /// <returns></returns>
        [WebMethod]
        public System.Data.DataSet LayDanhMucLinhVuc(string passWordLogin, DateTime dateLastUpdated)
        {
            System.Data.DataSet ds = null;

            try
            {
                if (passWordLogin != "daibangtungcanh")
                    return null;

                ds = new System.Data.DataSet("DanhMucLinhVuc");

                //string where = string.Format("DateModified > '{0}'", dateLastUpdated.ToString("yyyy-MM-dd hh:mm:ss.fff tt"));
                string where = "1 = 1";

                ds = Company.KDT.SHARE.Components.Feedback.Components.LinhVuc.SelectDynamic(where, "");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                if (ds.Tables.Count == 0)
                {
                    DataTable dtError = new System.Data.DataTable("dtError");
                    DataColumn col = new DataColumn("Error", typeof(string));
                    dtError.Columns.Add(col);
                    ds.Tables.Add(dtError);

                    DataRow dr = dtError.NewRow();
                    dr["Error"] = ex.Message;
                    dtError.Rows.Add(dr);
                }
            }

            return ds;
        }

        [WebMethod]
        public bool XoaDanhMucLinhVuc(string passWordLogin, int maLinhVuc)
        {
            try
            {
                if (passWordLogin != "daibangtungcanh")
                    return false;

                Company.KDT.SHARE.Components.Feedback.Components.LinhVuc.DeleteLinhVuc(maLinhVuc);

                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return false;
        }

        [WebMethod]
        public bool LuuDanhMucLinhVuc(string passWordLogin, System.Data.DataTable data)
        {
            try
            {
                if (passWordLogin != "daibangtungcanh")
                    return false;

                DataSet ds = new DataSet();
                ds.Tables.Add(data.Copy());

                Company.KDT.SHARE.Components.Feedback.Components.LinhVuc.UpdateDataSet(ds, ds.Tables[0].TableName);

                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return false;
        }

        /// <summary>
        /// Lay danh sach thu vien cau ho và tra loi thong tin gop y, giai dap theo dieu kien ngay cap nhat moi nhat.
        /// </summary>
        /// <param name="passWordLogin"></param>
        /// <param name="dateLastUpdated"></param>
        /// <returns></returns>
        [WebMethod]
        public System.Data.DataSet LayDanhMucThuVien(string passWordLogin, DateTime dateLastUpdated)
        {
            System.Data.DataSet ds = null;

            try
            {
                if (passWordLogin != "daibangtungcanh")
                    return null;

                ds = new System.Data.DataSet("ThuVienCauHoi");

               // string where = string.Format("NgayCapNhat >= '{0}'", dateLastUpdated.ToString("yyyy-MM-dd hh:mm:ss.fff tt"));
                string where = "1 = 1";
                ds = Company.KDT.SHARE.Components.Feedback.Components.ThuVienCauHoi.SelectDynamic(where, "");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                if (ds.Tables.Count == 0)
                {
                    DataTable dtError = new System.Data.DataTable("dtError");
                    DataColumn col = new DataColumn("Error", typeof(string));
                    dtError.Columns.Add(col);
                    ds.Tables.Add(dtError);

                    DataRow dr = dtError.NewRow();
                    dr["Error"] = ex.Message;
                    dtError.Rows.Add(dr);
                }
            }

            return ds;
        }

        /// <summary>
        /// Lay danh sach cau hoi gop y, thắc mắc từ doanh nghiệp gửi đến nhà cung cấp.
        /// </summary>
        /// <param name="passWordLogin"></param>
        /// <param name="dateLastUpdated"></param>
        /// <returns></returns>
        [WebMethod]
        public System.Data.DataSet LayDanhMucCauHoiDoanhNghiep(string passWordLogin, DateTime dateLastUpdated)
        {
            System.Data.DataSet ds = null;

            try
            {
                if (passWordLogin != "daibangtungcanh")
                    return null;

                ds = new System.Data.DataSet("CauHoiDoanhNghiep");

                //string where = string.Format("NgayTraLoi > '{0}'", dateLastUpdated.ToString("yyyy-MM-dd hh:mm:ss.fff tt"));
                string where = "1 = 1";
                ds = Company.KDT.SHARE.Components.Feedback.Components.CauHoiDoanhNghiep.SelectDynamic(where, "");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                if (ds.Tables.Count == 0)
                {
                    DataTable dtError = new System.Data.DataTable("dtError");
                    DataColumn col = new DataColumn("Error", typeof(string));
                    dtError.Columns.Add(col);
                    ds.Tables.Add(dtError);

                    DataRow dr = dtError.NewRow();
                    dr["Error"] = ex.Message;
                    dtError.Rows.Add(dr);
                }
            }

            return ds;
        }

        /// <summary>
        /// Tạo mới/ Cập nhật nội dung câu hỏi và nội dung trả lời.
        /// </summary>
        /// <param name="passWordLogin"></param>
        /// <param name="cauhoi"></param>
        /// <returns></returns>
        [WebMethod]
        public string TaoMoiCauHoi(string passWordLogin, int LinhVuc_ID, string Phanloai, string TieuDe, string NoiDungCauHoi, string TraLoi, bool HienThi, string GhiChu)
        {
            string error = "";

            try
            {
                if (passWordLogin != "daibangtungcanh")
                    return error = "Mật khẩu không hợp lệ";

               
                if (LinhVuc_ID == 0)
                    return error = "Thông tin 'Lĩnh vực' không được trống";
                if (Phanloai == "")
                    return error = "Thông tin 'Phân loại' không được trống";
                if (TieuDe == "")
                    return error = "Thông tin 'Tiêu đề' không được trống";
                if (NoiDungCauHoi == "")
                    return error = "Thông tin 'Nội dung câu hỏi' không được trống";
                if (TraLoi == "")
                    return error = "Thông tin 'Nội dung trả lời' không được trống";

                Company.KDT.SHARE.Components.Feedback.Components.ThuVienCauHoi.InsertThuVienCauHoi(LinhVuc_ID, Phanloai, TieuDe, NoiDungCauHoi, TraLoi, DateTime.Now, DateTime.Now, HienThi, GhiChu);

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); error = ex.Message; }

            return error;
        }

        [WebMethod]
        public string CapNhatCauHoi(string passWordLogin, int MaCauHoi, int LinhVuc, string Phanloai, string TieuDe, string NoiDungCauHoi, string TraLoi, bool HienThi, string GhiChu)
        {
            string error = "";

            try
            {
                if (passWordLogin != "daibangtungcanh")
                    return error = "Mật khẩu không hợp lệ";

               
                if (LinhVuc == null)
                    return error = "Thông tin 'Lĩnh vực' không được trống";
                if (Phanloai == "")
                    return error = "Thông tin 'Phân loại' không được trống";
                if (TieuDe == "")
                    return error = "Thông tin 'Tiêu đề' không được trống";
                if (NoiDungCauHoi == "")
                    return error = "Thông tin 'Nội dung câu hỏi' không được trống";
                if (TraLoi == "")
                    return error = "Thông tin 'Nội dung trả lời' không được trống";

                Company.KDT.SHARE.Components.Feedback.Components.ThuVienCauHoi.UpdateThuVienCauHoi(MaCauHoi, LinhVuc, Phanloai, TieuDe, NoiDungCauHoi, TraLoi, DateTime.Now, DateTime.Now, HienThi, GhiChu);

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); error = ex.Message; }

            return error;
        }

        [WebMethod]
        public string XoaCauHoi(string passWordLogin, int maCauHoi)
        {
            string error = "";

            try
            {
                if (passWordLogin != "daibangtungcanh")
                    return error = "Mật khẩu không hợp lệ";

                if (maCauHoi == null)
                    return error = "Thông tin 'Mã câu hỏi' không được trống";

                Company.KDT.SHARE.Components.Feedback.Components.ThuVienCauHoi.DeleteThuVienCauHoi(maCauHoi);

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); error = ex.Message; }

            return error;
        }

        #endregion
    }

}
