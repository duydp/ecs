using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Entity
{
    public partial class SerialLog
    {
        #region Private members.

        protected int _ID;
        protected string _SerialCode = string.Empty;
        protected string _KeyActivate = string.Empty;
        protected bool _Activating;
        protected string _Descriptions = string.Empty;
        protected DateTime _CreateDate;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public int ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public string SerialCode
        {
            set { this._SerialCode = value; }
            get { return this._SerialCode; }
        }

        public string KeyActivate
        {
            set { this._KeyActivate = value; }
            get { return this._KeyActivate; }
        }

        public bool Activating
        {
            set { this._Activating = value; }
            get { return this._Activating; }
        }

        public string Descriptions
        {
            set { this._Descriptions = value; }
            get { return this._Descriptions; }
        }

        public DateTime CreateDate
        {
            set { this._CreateDate = value; }
            get { return this._CreateDate; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static SerialLog LoadBy_KeyActivating(string SerialCode, string KeyActivate)
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_LoadBy_KeyActivating]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
            db.AddInParameter(dbCommand, "@KeyActivate", SqlDbType.VarChar, KeyActivate);
            SerialLog entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new SerialLog();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SerialCode"))) entity.SerialCode = reader.GetString(reader.GetOrdinal("SerialCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("KeyActivate"))) entity.KeyActivate = reader.GetString(reader.GetOrdinal("KeyActivate"));
                if (!reader.IsDBNull(reader.GetOrdinal("Activating"))) entity.Activating = reader.GetBoolean(reader.GetOrdinal("Activating"));
                if (!reader.IsDBNull(reader.GetOrdinal("Descriptions"))) entity.Descriptions = reader.GetString(reader.GetOrdinal("Descriptions"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreateDate"))) entity.CreateDate = reader.GetDateTime(reader.GetOrdinal("CreateDate"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public static SerialLog Load(int iD)
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, iD);
            SerialLog entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new SerialLog();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SerialCode"))) entity.SerialCode = reader.GetString(reader.GetOrdinal("SerialCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("KeyActivate"))) entity.KeyActivate = reader.GetString(reader.GetOrdinal("KeyActivate"));
                if (!reader.IsDBNull(reader.GetOrdinal("Activating"))) entity.Activating = reader.GetBoolean(reader.GetOrdinal("Activating"));
                if (!reader.IsDBNull(reader.GetOrdinal("Descriptions"))) entity.Descriptions = reader.GetString(reader.GetOrdinal("Descriptions"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreateDate"))) entity.CreateDate = reader.GetDateTime(reader.GetOrdinal("CreateDate"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_SerialCode(string serialCode)
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_SelectBy_SerialCode]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, serialCode);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_SerialActivating(string serialCode)
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_SelectBy_SerialActivating]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, serialCode);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_SerialCode(string serialCode)
        {
            const string spName = "p_LICENSE_SerialLog_SelectBy_SerialCode";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, serialCode);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static int InsertSerialLog(string serialCode, string keyActivate, bool activating, string descriptions, DateTime createDate)
        {
            SerialLog entity = new SerialLog();
            entity.SerialCode = serialCode;
            entity.KeyActivate = keyActivate;
            entity.Activating = activating;
            entity.Descriptions = descriptions;
            entity.CreateDate = createDate;
            return entity.Insert();
        }

        public int Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
            db.AddInParameter(dbCommand, "@KeyActivate", SqlDbType.VarChar, KeyActivate);
            db.AddInParameter(dbCommand, "@Activating", SqlDbType.Bit, Activating);
            db.AddInParameter(dbCommand, "@Descriptions", SqlDbType.NVarChar, Descriptions);
            db.AddInParameter(dbCommand, "@CreateDate", SqlDbType.DateTime, CreateDate);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateSerialLog(int iD, string serialCode, string keyActivate, bool activating, string descriptions, DateTime createDate)
        {
            SerialLog entity = new SerialLog();
            entity.SerialCode = serialCode;
            entity.KeyActivate = keyActivate;
            entity.Activating = activating;
            entity.Descriptions = descriptions;
            entity.CreateDate = createDate;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_LICENSE_SerialLog_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
            db.AddInParameter(dbCommand, "@KeyActivate", SqlDbType.VarChar, KeyActivate);
            db.AddInParameter(dbCommand, "@Activating", SqlDbType.Bit, Activating);
            db.AddInParameter(dbCommand, "@Descriptions", SqlDbType.NVarChar, Descriptions);
            db.AddInParameter(dbCommand, "@CreateDate", SqlDbType.DateTime, CreateDate);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateSerialLog(int iD, string serialCode, string keyActivate, bool activating, string descriptions, DateTime createDate)
        {
            SerialLog entity = new SerialLog();
            entity.ID = iD;
            entity.SerialCode = serialCode;
            entity.KeyActivate = keyActivate;
            entity.Activating = activating;
            entity.Descriptions = descriptions;
            entity.CreateDate = createDate;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
            db.AddInParameter(dbCommand, "@KeyActivate", SqlDbType.VarChar, KeyActivate);
            db.AddInParameter(dbCommand, "@Activating", SqlDbType.Bit, Activating);
            db.AddInParameter(dbCommand, "@Descriptions", SqlDbType.NVarChar, Descriptions);
            db.AddInParameter(dbCommand, "@CreateDate", SqlDbType.DateTime, CreateDate);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteSerialLog(int iD)
        {
            SerialLog entity = new SerialLog();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_SerialLog_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_SerialCode(string serialCode)
        {
            string spName = "[dbo].[p_LICENSE_SerialLog_DeleteBy_SerialCode]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, serialCode);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        #endregion
    }
}