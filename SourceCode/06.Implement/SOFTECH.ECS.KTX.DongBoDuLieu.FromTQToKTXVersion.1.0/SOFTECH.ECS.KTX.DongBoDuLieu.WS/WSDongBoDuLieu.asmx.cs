﻿using System;
using System.Data;
using System.Web.Services;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Xml;

namespace WSDongBoDuLieu
{
    /// <summary>
    /// Summary description for WSDongBoDuLieu
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class WSDongBoDuLieu : WebService
    {
        [WebMethod]
        public bool InsertAcountDN(DoanhNghiep doanhNghiep)
        {
           return DoanhNghiep.InsertDoanhNghiep(doanhNghiep, Server.MapPath("~") + "\\App_Data");

        }
        [WebMethod]
        public DataSet GetData(string query, string maHQ, string connectionName, string secureID, string MaDoanhNghiep, string matkhau)
        {
            if (secureID != "daibangtungcanh") return null;

            //Kiem tra cau truy van hop le
            if (!query.ToUpper().StartsWith("SELECT"))
                throw new Exception("Dữ liệu truy vấn không hợp lệ.");
            if (query.ToUpper().IndexOf("DELETE") > -1)
                throw new Exception("Dữ liệu truy vấn không hợp lệ.");
            if (query.ToUpper().IndexOf("UPDATE") > -1)
                throw new Exception("Dữ liệu truy vấn không hợp lệ.");

            string path = Server.MapPath("~") + "\\App_Data";
            //kiem tra doanh nghiep dong bo
            DoanhNghiep dn = DoanhNghiep.getDoanhNghiep(MaDoanhNghiep,path);
            if (dn == null)
                throw new Exception("Doanh nghiệp này chưa đăng ký đồng bộ dữ liệu với hải quan.Hãy đăng ký với hải quan để được đồng bộ dữ liệu.");
            if (dn.MatKhau != matkhau)
            {
                throw new Exception("Sai mật khẩu truy cập đồng bộ dữ liệu.Hãy liên hệ với hải quan để được cấp mật khẩu đồng bộ dữ liệu");
            }

            string connectionString = getConnection(maHQ, connectionName, path);
            SqlDataAdapter da = new SqlDataAdapter(query, connectionString.Trim());
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        private static string getConnection(string MaHQ, string database, string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path + "\\ConnectionStringDatabase.xml");
            XmlNode nodeRoot = doc.SelectSingleNode("Root");
            XmlNodeList node = nodeRoot.SelectNodes("ChiCuc[@MaChiCuc=\"" + MaHQ.Trim() + "\"]");
            if (node == null || node.Count==0)
            {
                throw new Exception("Mã hải quan này không có.");
            }
           
            return node[0].SelectSingleNode(database.ToUpper()).InnerText;
        }


        [WebMethod]
        public void DoiMatKhauDongBo(string secureID, string MaDoanhNghiep, string MatKhauCu, string MatKhauMoi)
        {
            if (secureID != "daibangtungcanh") return;


            string path = Server.MapPath("~") + "\\App_Data"; ;
            //kiem tra doanh nghiep dong bo
            DoanhNghiep dn = DoanhNghiep.getDoanhNghiep(MaDoanhNghiep, path);
            if (dn == null)                
                throw new Exception("Doanh nghiệp này chưa đăng ký đồng bộ dữ liệu với hải quan.Hãy đăng ký với hải quan để được đồng bộ dữ liệu.");
            if (dn.MatKhau != MatKhauCu)
            {
                throw new Exception("Mật khẩu cũ không đúng.Hãy kiểm tra lại.");
            }
            DoanhNghiep.UpdateMatKhauDoanhNghiep(MaDoanhNghiep, MatKhauMoi, path);
        }
    
    }
}
