-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]
	@MaNguoiKhai varchar(13),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@LoaiHinhXNK varchar(1),
	@MaDV_CapPhep varchar(6),
	@MaDoanhNghiep varchar(13),
	@TenDoanhNghiep nvarchar(300),
	@QuyetDinhSo varchar(17),
	@SoGiayChungNhan varchar(17),
	@NoiCap nvarchar(35),
	@NgayCap datetime,
	@MaBuuChinh varchar(7),
	@DiaChi varchar(300),
	@MaQuocGia varchar(2),
	@SoDienThoai varchar(20),
	@SoFax varchar(20),
	@Email varchar(210),
	@SoHopDongMua varchar(17),
	@NgayHopDongMua datetime,
	@SoHopDongBan varchar(17),
	@NgayHopDongBan datetime,
	@PhuongTienVanChuyen varchar(2),
	@MaCuaKhauXuatNhap varchar(6),
	@TenCuaKhauXuatNhap varchar(35),
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TenGiamDoc nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(50),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(996),
	@DaiDienDonViCapPhep nvarchar(996),
	@NgayKhaiBao datetime,
	@TenNguoiKhai nvarchar(300),
	@TenDonViCapPhep nvarchar(300),
	@TenPhuongTienVanChuyen nvarchar(75),
	@TrangThaiXuLy int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SEA]
(
	[MaNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[LoaiHinhXNK],
	[MaDV_CapPhep],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[QuyetDinhSo],
	[SoGiayChungNhan],
	[NoiCap],
	[NgayCap],
	[MaBuuChinh],
	[DiaChi],
	[MaQuocGia],
	[SoDienThoai],
	[SoFax],
	[Email],
	[SoHopDongMua],
	[NgayHopDongMua],
	[SoHopDongBan],
	[NgayHopDongBan],
	[PhuongTienVanChuyen],
	[MaCuaKhauXuatNhap],
	[TenCuaKhauXuatNhap],
	[NgayBatDau],
	[NgayKetThuc],
	[HoSoLienQuan],
	[GhiChu],
	[TenGiamDoc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienDonViCapPhep],
	[NgayKhaiBao],
	[TenNguoiKhai],
	[TenDonViCapPhep],
	[TenPhuongTienVanChuyen],
	[TrangThaiXuLy]
)
VALUES 
(
	@MaNguoiKhai,
	@SoDonXinCapPhep,
	@ChucNangChungTu,
	@LoaiGiayPhep,
	@LoaiHinhXNK,
	@MaDV_CapPhep,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@QuyetDinhSo,
	@SoGiayChungNhan,
	@NoiCap,
	@NgayCap,
	@MaBuuChinh,
	@DiaChi,
	@MaQuocGia,
	@SoDienThoai,
	@SoFax,
	@Email,
	@SoHopDongMua,
	@NgayHopDongMua,
	@SoHopDongBan,
	@NgayHopDongBan,
	@PhuongTienVanChuyen,
	@MaCuaKhauXuatNhap,
	@TenCuaKhauXuatNhap,
	@NgayBatDau,
	@NgayKetThuc,
	@HoSoLienQuan,
	@GhiChu,
	@TenGiamDoc,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@HoSoKemTheo_1,
	@HoSoKemTheo_2,
	@HoSoKemTheo_3,
	@HoSoKemTheo_4,
	@HoSoKemTheo_5,
	@HoSoKemTheo_6,
	@HoSoKemTheo_7,
	@HoSoKemTheo_8,
	@HoSoKemTheo_9,
	@HoSoKemTheo_10,
	@MaKetQuaXuLy,
	@KetQuaXuLySoGiayPhep,
	@KetQuaXuLyNgayCap,
	@KetQuaXuLyHieuLucTuNgay,
	@KetQuaXuLyHieuLucDenNgay,
	@GhiChuDanhChoCoQuanCapPhep,
	@DaiDienDonViCapPhep,
	@NgayKhaiBao,
	@TenNguoiKhai,
	@TenDonViCapPhep,
	@TenPhuongTienVanChuyen,
	@TrangThaiXuLy
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@LoaiHinhXNK varchar(1),
	@MaDV_CapPhep varchar(6),
	@MaDoanhNghiep varchar(13),
	@TenDoanhNghiep nvarchar(300),
	@QuyetDinhSo varchar(17),
	@SoGiayChungNhan varchar(17),
	@NoiCap nvarchar(35),
	@NgayCap datetime,
	@MaBuuChinh varchar(7),
	@DiaChi varchar(300),
	@MaQuocGia varchar(2),
	@SoDienThoai varchar(20),
	@SoFax varchar(20),
	@Email varchar(210),
	@SoHopDongMua varchar(17),
	@NgayHopDongMua datetime,
	@SoHopDongBan varchar(17),
	@NgayHopDongBan datetime,
	@PhuongTienVanChuyen varchar(2),
	@MaCuaKhauXuatNhap varchar(6),
	@TenCuaKhauXuatNhap varchar(35),
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TenGiamDoc nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(50),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(996),
	@DaiDienDonViCapPhep nvarchar(996),
	@NgayKhaiBao datetime,
	@TenNguoiKhai nvarchar(300),
	@TenDonViCapPhep nvarchar(300),
	@TenPhuongTienVanChuyen nvarchar(75),
	@TrangThaiXuLy int
AS

UPDATE
	[dbo].[t_KDT_VNACC_GiayPhep_SEA]
SET
	[MaNguoiKhai] = @MaNguoiKhai,
	[SoDonXinCapPhep] = @SoDonXinCapPhep,
	[ChucNangChungTu] = @ChucNangChungTu,
	[LoaiGiayPhep] = @LoaiGiayPhep,
	[LoaiHinhXNK] = @LoaiHinhXNK,
	[MaDV_CapPhep] = @MaDV_CapPhep,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[QuyetDinhSo] = @QuyetDinhSo,
	[SoGiayChungNhan] = @SoGiayChungNhan,
	[NoiCap] = @NoiCap,
	[NgayCap] = @NgayCap,
	[MaBuuChinh] = @MaBuuChinh,
	[DiaChi] = @DiaChi,
	[MaQuocGia] = @MaQuocGia,
	[SoDienThoai] = @SoDienThoai,
	[SoFax] = @SoFax,
	[Email] = @Email,
	[SoHopDongMua] = @SoHopDongMua,
	[NgayHopDongMua] = @NgayHopDongMua,
	[SoHopDongBan] = @SoHopDongBan,
	[NgayHopDongBan] = @NgayHopDongBan,
	[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
	[MaCuaKhauXuatNhap] = @MaCuaKhauXuatNhap,
	[TenCuaKhauXuatNhap] = @TenCuaKhauXuatNhap,
	[NgayBatDau] = @NgayBatDau,
	[NgayKetThuc] = @NgayKetThuc,
	[HoSoLienQuan] = @HoSoLienQuan,
	[GhiChu] = @GhiChu,
	[TenGiamDoc] = @TenGiamDoc,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[HoSoKemTheo_1] = @HoSoKemTheo_1,
	[HoSoKemTheo_2] = @HoSoKemTheo_2,
	[HoSoKemTheo_3] = @HoSoKemTheo_3,
	[HoSoKemTheo_4] = @HoSoKemTheo_4,
	[HoSoKemTheo_5] = @HoSoKemTheo_5,
	[HoSoKemTheo_6] = @HoSoKemTheo_6,
	[HoSoKemTheo_7] = @HoSoKemTheo_7,
	[HoSoKemTheo_8] = @HoSoKemTheo_8,
	[HoSoKemTheo_9] = @HoSoKemTheo_9,
	[HoSoKemTheo_10] = @HoSoKemTheo_10,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
	[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
	[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
	[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
	[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
	[DaiDienDonViCapPhep] = @DaiDienDonViCapPhep,
	[NgayKhaiBao] = @NgayKhaiBao,
	[TenNguoiKhai] = @TenNguoiKhai,
	[TenDonViCapPhep] = @TenDonViCapPhep,
	[TenPhuongTienVanChuyen] = @TenPhuongTienVanChuyen,
	[TrangThaiXuLy] = @TrangThaiXuLy
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_InsertUpdate]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@LoaiHinhXNK varchar(1),
	@MaDV_CapPhep varchar(6),
	@MaDoanhNghiep varchar(13),
	@TenDoanhNghiep nvarchar(300),
	@QuyetDinhSo varchar(17),
	@SoGiayChungNhan varchar(17),
	@NoiCap nvarchar(35),
	@NgayCap datetime,
	@MaBuuChinh varchar(7),
	@DiaChi varchar(300),
	@MaQuocGia varchar(2),
	@SoDienThoai varchar(20),
	@SoFax varchar(20),
	@Email varchar(210),
	@SoHopDongMua varchar(17),
	@NgayHopDongMua datetime,
	@SoHopDongBan varchar(17),
	@NgayHopDongBan datetime,
	@PhuongTienVanChuyen varchar(2),
	@MaCuaKhauXuatNhap varchar(6),
	@TenCuaKhauXuatNhap varchar(35),
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TenGiamDoc nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(50),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(996),
	@DaiDienDonViCapPhep nvarchar(996),
	@NgayKhaiBao datetime,
	@TenNguoiKhai nvarchar(300),
	@TenDonViCapPhep nvarchar(300),
	@TenPhuongTienVanChuyen nvarchar(75),
	@TrangThaiXuLy int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_GiayPhep_SEA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_GiayPhep_SEA] 
		SET
			[MaNguoiKhai] = @MaNguoiKhai,
			[SoDonXinCapPhep] = @SoDonXinCapPhep,
			[ChucNangChungTu] = @ChucNangChungTu,
			[LoaiGiayPhep] = @LoaiGiayPhep,
			[LoaiHinhXNK] = @LoaiHinhXNK,
			[MaDV_CapPhep] = @MaDV_CapPhep,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[QuyetDinhSo] = @QuyetDinhSo,
			[SoGiayChungNhan] = @SoGiayChungNhan,
			[NoiCap] = @NoiCap,
			[NgayCap] = @NgayCap,
			[MaBuuChinh] = @MaBuuChinh,
			[DiaChi] = @DiaChi,
			[MaQuocGia] = @MaQuocGia,
			[SoDienThoai] = @SoDienThoai,
			[SoFax] = @SoFax,
			[Email] = @Email,
			[SoHopDongMua] = @SoHopDongMua,
			[NgayHopDongMua] = @NgayHopDongMua,
			[SoHopDongBan] = @SoHopDongBan,
			[NgayHopDongBan] = @NgayHopDongBan,
			[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
			[MaCuaKhauXuatNhap] = @MaCuaKhauXuatNhap,
			[TenCuaKhauXuatNhap] = @TenCuaKhauXuatNhap,
			[NgayBatDau] = @NgayBatDau,
			[NgayKetThuc] = @NgayKetThuc,
			[HoSoLienQuan] = @HoSoLienQuan,
			[GhiChu] = @GhiChu,
			[TenGiamDoc] = @TenGiamDoc,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[HoSoKemTheo_1] = @HoSoKemTheo_1,
			[HoSoKemTheo_2] = @HoSoKemTheo_2,
			[HoSoKemTheo_3] = @HoSoKemTheo_3,
			[HoSoKemTheo_4] = @HoSoKemTheo_4,
			[HoSoKemTheo_5] = @HoSoKemTheo_5,
			[HoSoKemTheo_6] = @HoSoKemTheo_6,
			[HoSoKemTheo_7] = @HoSoKemTheo_7,
			[HoSoKemTheo_8] = @HoSoKemTheo_8,
			[HoSoKemTheo_9] = @HoSoKemTheo_9,
			[HoSoKemTheo_10] = @HoSoKemTheo_10,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
			[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
			[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
			[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
			[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
			[DaiDienDonViCapPhep] = @DaiDienDonViCapPhep,
			[NgayKhaiBao] = @NgayKhaiBao,
			[TenNguoiKhai] = @TenNguoiKhai,
			[TenDonViCapPhep] = @TenDonViCapPhep,
			[TenPhuongTienVanChuyen] = @TenPhuongTienVanChuyen,
			[TrangThaiXuLy] = @TrangThaiXuLy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SEA]
		(
			[MaNguoiKhai],
			[SoDonXinCapPhep],
			[ChucNangChungTu],
			[LoaiGiayPhep],
			[LoaiHinhXNK],
			[MaDV_CapPhep],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[QuyetDinhSo],
			[SoGiayChungNhan],
			[NoiCap],
			[NgayCap],
			[MaBuuChinh],
			[DiaChi],
			[MaQuocGia],
			[SoDienThoai],
			[SoFax],
			[Email],
			[SoHopDongMua],
			[NgayHopDongMua],
			[SoHopDongBan],
			[NgayHopDongBan],
			[PhuongTienVanChuyen],
			[MaCuaKhauXuatNhap],
			[TenCuaKhauXuatNhap],
			[NgayBatDau],
			[NgayKetThuc],
			[HoSoLienQuan],
			[GhiChu],
			[TenGiamDoc],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[HoSoKemTheo_1],
			[HoSoKemTheo_2],
			[HoSoKemTheo_3],
			[HoSoKemTheo_4],
			[HoSoKemTheo_5],
			[HoSoKemTheo_6],
			[HoSoKemTheo_7],
			[HoSoKemTheo_8],
			[HoSoKemTheo_9],
			[HoSoKemTheo_10],
			[MaKetQuaXuLy],
			[KetQuaXuLySoGiayPhep],
			[KetQuaXuLyNgayCap],
			[KetQuaXuLyHieuLucTuNgay],
			[KetQuaXuLyHieuLucDenNgay],
			[GhiChuDanhChoCoQuanCapPhep],
			[DaiDienDonViCapPhep],
			[NgayKhaiBao],
			[TenNguoiKhai],
			[TenDonViCapPhep],
			[TenPhuongTienVanChuyen],
			[TrangThaiXuLy]
		)
		VALUES 
		(
			@MaNguoiKhai,
			@SoDonXinCapPhep,
			@ChucNangChungTu,
			@LoaiGiayPhep,
			@LoaiHinhXNK,
			@MaDV_CapPhep,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@QuyetDinhSo,
			@SoGiayChungNhan,
			@NoiCap,
			@NgayCap,
			@MaBuuChinh,
			@DiaChi,
			@MaQuocGia,
			@SoDienThoai,
			@SoFax,
			@Email,
			@SoHopDongMua,
			@NgayHopDongMua,
			@SoHopDongBan,
			@NgayHopDongBan,
			@PhuongTienVanChuyen,
			@MaCuaKhauXuatNhap,
			@TenCuaKhauXuatNhap,
			@NgayBatDau,
			@NgayKetThuc,
			@HoSoLienQuan,
			@GhiChu,
			@TenGiamDoc,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@HoSoKemTheo_1,
			@HoSoKemTheo_2,
			@HoSoKemTheo_3,
			@HoSoKemTheo_4,
			@HoSoKemTheo_5,
			@HoSoKemTheo_6,
			@HoSoKemTheo_7,
			@HoSoKemTheo_8,
			@HoSoKemTheo_9,
			@HoSoKemTheo_10,
			@MaKetQuaXuLy,
			@KetQuaXuLySoGiayPhep,
			@KetQuaXuLyNgayCap,
			@KetQuaXuLyHieuLucTuNgay,
			@KetQuaXuLyHieuLucDenNgay,
			@GhiChuDanhChoCoQuanCapPhep,
			@DaiDienDonViCapPhep,
			@NgayKhaiBao,
			@TenNguoiKhai,
			@TenDonViCapPhep,
			@TenPhuongTienVanChuyen,
			@TrangThaiXuLy
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_GiayPhep_SEA]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SEA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[LoaiHinhXNK],
	[MaDV_CapPhep],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[QuyetDinhSo],
	[SoGiayChungNhan],
	[NoiCap],
	[NgayCap],
	[MaBuuChinh],
	[DiaChi],
	[MaQuocGia],
	[SoDienThoai],
	[SoFax],
	[Email],
	[SoHopDongMua],
	[NgayHopDongMua],
	[SoHopDongBan],
	[NgayHopDongBan],
	[PhuongTienVanChuyen],
	[MaCuaKhauXuatNhap],
	[TenCuaKhauXuatNhap],
	[NgayBatDau],
	[NgayKetThuc],
	[HoSoLienQuan],
	[GhiChu],
	[TenGiamDoc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienDonViCapPhep],
	[NgayKhaiBao],
	[TenNguoiKhai],
	[TenDonViCapPhep],
	[TenPhuongTienVanChuyen],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SEA]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[LoaiHinhXNK],
	[MaDV_CapPhep],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[QuyetDinhSo],
	[SoGiayChungNhan],
	[NoiCap],
	[NgayCap],
	[MaBuuChinh],
	[DiaChi],
	[MaQuocGia],
	[SoDienThoai],
	[SoFax],
	[Email],
	[SoHopDongMua],
	[NgayHopDongMua],
	[SoHopDongBan],
	[NgayHopDongBan],
	[PhuongTienVanChuyen],
	[MaCuaKhauXuatNhap],
	[TenCuaKhauXuatNhap],
	[NgayBatDau],
	[NgayKetThuc],
	[HoSoLienQuan],
	[GhiChu],
	[TenGiamDoc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienDonViCapPhep],
	[NgayKhaiBao],
	[TenNguoiKhai],
	[TenDonViCapPhep],
	[TenPhuongTienVanChuyen],
	[TrangThaiXuLy]
FROM [dbo].[t_KDT_VNACC_GiayPhep_SEA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[LoaiHinhXNK],
	[MaDV_CapPhep],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[QuyetDinhSo],
	[SoGiayChungNhan],
	[NoiCap],
	[NgayCap],
	[MaBuuChinh],
	[DiaChi],
	[MaQuocGia],
	[SoDienThoai],
	[SoFax],
	[Email],
	[SoHopDongMua],
	[NgayHopDongMua],
	[SoHopDongBan],
	[NgayHopDongBan],
	[PhuongTienVanChuyen],
	[MaCuaKhauXuatNhap],
	[TenCuaKhauXuatNhap],
	[NgayBatDau],
	[NgayKetThuc],
	[HoSoLienQuan],
	[GhiChu],
	[TenGiamDoc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienDonViCapPhep],
	[NgayKhaiBao],
	[TenNguoiKhai],
	[TenDonViCapPhep],
	[TenPhuongTienVanChuyen],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SEA]	

GO

