﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.KD.BLL.Utils;
using System.Threading;
using Company.KD.BLL.KDT.SXXK;
using System.Collections;
using System.Text;
namespace Company.KD.BLL.KDT
{
    public partial class ToKhaiTriGia
    {
        HangTriGiaCollection _HTGCollection = new HangTriGiaCollection();
        public HangTriGiaCollection HTGCollection
        {
            set { this._HTGCollection = value; }
            get { return this._HTGCollection; }
        }
       
              

        public void LoadHTGCollection()
        {
            HangTriGia hmd = new HangTriGia();
            hmd.TKTG_ID = this.ID;
            this._HTGCollection = hmd.SelectCollectionBy_TKTG_ID();
        }
      
        //-----------------------------------------------------------------------------------------
        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    foreach (HangTriGia hangTG in this.HTGCollection)
                    {                        
                        if (hangTG.ID == 0)
                        {
                            hangTG.TKTG_ID = this.ID;
                            hangTG.ID = hangTG.InsertTransaction(transaction);
                        }
                        else
                        {
                            hangTG.UpdateTransaction(transaction);
                        }
                    }                   
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public void InsertUpdateFullTransaction(SqlTransaction transaction)
        {


            if (this.ID == 0)
                this.ID = this.InsertTransaction(transaction);
            else
                this.UpdateTransaction(transaction);
            foreach (HangTriGia hangTG in this.HTGCollection)
            {
                if (hangTG.ID == 0)
                {
                    hangTG.TKTG_ID = this.ID;
                    hangTG.ID = hangTG.InsertTransaction(transaction);
                }
                else
                {
                    hangTG.UpdateTransaction(transaction);
                }
            }


        }
        #region Webservice của hải quan

        public XmlNode ConvertToXML()
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            XmlDocument doc = new XmlDocument();

            XmlElement nodeRoot = doc.CreateElement("TKTG_PP1");            

            XmlAttribute attToSo = doc.CreateAttribute("TO_SO");
            attToSo.Value = this.ToSo.ToString();
            nodeRoot.Attributes.Append(attToSo);

            XmlAttribute attLOAI_TKTG = doc.CreateAttribute("LOAI_TKTG");
            attLOAI_TKTG.Value = this.LoaiToKhaiTriGia.ToString();
            nodeRoot.Attributes.Append(attLOAI_TKTG);

            XmlAttribute attNGAY_XK = doc.CreateAttribute("NGAY_XK");
            attNGAY_XK.Value = this.NgayXuatKhau.ToString("MM/dd/yyyy");
            nodeRoot.Attributes.Append(attNGAY_XK);

            XmlAttribute attCAP_DO_TM = doc.CreateAttribute("CAP_DO_TM");
            attCAP_DO_TM.Value = this.CapDoThuongMai;
            nodeRoot.Attributes.Append(attCAP_DO_TM);

            XmlAttribute attQUYEN_SD = doc.CreateAttribute("QUYEN_SD");
            if (this.QuyenSuDung)
                attQUYEN_SD.Value = "1";
            else
                attQUYEN_SD.Value = "0";
            nodeRoot.Attributes.Append(attQUYEN_SD);

            XmlAttribute attKHONG_XD = doc.CreateAttribute("KHONG_XD");
            if (this.KhongXacDinh)
                attKHONG_XD.Value = "1";
            else
                attKHONG_XD.Value = "0";
            nodeRoot.Attributes.Append(attKHONG_XD);

            XmlAttribute attTRA_THEM = doc.CreateAttribute("TRA_THEM");
            if (this.TraThem)
                attTRA_THEM.Value = "1";
            else
                attTRA_THEM.Value = "0";
            nodeRoot.Attributes.Append(attTRA_THEM);

            XmlAttribute attTIEN_TRA_16 = doc.CreateAttribute("TIEN_TRA_16");
            if (this.TienTra)
                attTIEN_TRA_16.Value = "1";
            else
                attTIEN_TRA_16.Value = "0";
            nodeRoot.Attributes.Append(attTIEN_TRA_16);

            XmlAttribute attCO_QHDB = doc.CreateAttribute("CO_QHDB");
            if (this.CoQuanHeDacBiet)
                attCO_QHDB.Value = "1";
            else
                attCO_QHDB.Value = "0";
            nodeRoot.Attributes.Append(attCO_QHDB);

            XmlAttribute attANH_HUONG_QH = doc.CreateAttribute("ANH_HUONG_QH");
            if (this.AnhHuongQuanHe)
                attANH_HUONG_QH.Value = "1";
            else
                attANH_HUONG_QH.Value = "0";
            nodeRoot.Attributes.Append(attANH_HUONG_QH);

            XmlAttribute attGHI_CHEP_KT = doc.CreateAttribute("GHI_CHEP_KT");
            attGHI_CHEP_KT.Value = this.GhiChep;
            nodeRoot.Attributes.Append(attGHI_CHEP_KT);

            XmlAttribute attNGAY_KB = doc.CreateAttribute("NGAY_KB");
            attNGAY_KB.Value = DateTime.Today.ToString("MM/dd/yyyy");
            nodeRoot.Attributes.Append(attNGAY_KB);

            XmlAttribute attNGUOI_KB = doc.CreateAttribute("NGUOI_KB");
            attNGUOI_KB.Value = this.NguoiKhaiBao;
            nodeRoot.Attributes.Append(attNGUOI_KB);

            XmlAttribute attCHUC_DANH_KB = doc.CreateAttribute("CHUC_DANH_KB");
            attCHUC_DANH_KB.Value = this.ChucDanhNguoiKhaiBao;
            nodeRoot.Attributes.Append(attCHUC_DANH_KB);

            XmlAttribute attNGAY_TRUYEN1 = doc.CreateAttribute("NGAY_TRUYEN");
            attNGAY_TRUYEN1.Value = DateTime.Today.ToString("MM/dd/yyyy");
            nodeRoot.Attributes.Append(attNGAY_TRUYEN1);

            XmlAttribute attKIEU_QHDB = doc.CreateAttribute("KIEU_QHDB");
            attKIEU_QHDB.Value = this.KieuQuanHe;
            nodeRoot.Attributes.Append(attKIEU_QHDB);

            XmlElement CT_TKTG_PP1 = doc.CreateElement("CT_TKTG_PP1");
            nodeRoot.AppendChild(CT_TKTG_PP1);
            if (this.HTGCollection.Count == 0)
                this.LoadHTGCollection();
            foreach (HangTriGia hang in this.HTGCollection)
            {
                XmlElement ITEM = doc.CreateElement("CT_TKTG_PP1.ITEM");
                CT_TKTG_PP1.AppendChild(ITEM);

                XmlAttribute attSTTHang = doc.CreateAttribute("STTHang");
                attSTTHang.Value = hang.STTHang.ToString();
                ITEM.Attributes.Append(attSTTHang);

                XmlAttribute attGIA_HOA_DON = doc.CreateAttribute("GIA_HOA_DON");
                attGIA_HOA_DON.Value = hang.GiaTrenHoaDon.ToString(f);
                ITEM.Attributes.Append(attGIA_HOA_DON);

                XmlAttribute attTT_GIAN_TIEP = doc.CreateAttribute("TT_GIAN_TIEP");
                attTT_GIAN_TIEP.Value = hang.KhoanThanhToanGianTiep.ToString(f);
                ITEM.Attributes.Append(attTT_GIAN_TIEP);

                XmlAttribute attTRA_TRUOC = doc.CreateAttribute("TRA_TRUOC");
                attTRA_TRUOC.Value = hang.TraTruoc.ToString(f);
                ITEM.Attributes.Append(attTRA_TRUOC);

                XmlAttribute attTC_1 = doc.CreateAttribute("TC_1");
                attTC_1.Value = hang.TongCong1.ToString(f);
                ITEM.Attributes.Append(attTC_1);

                XmlAttribute attHOA_HONG = doc.CreateAttribute("HOA_HONG");
                attHOA_HONG.Value = hang.HoaHong.ToString(f);
                ITEM.Attributes.Append(attHOA_HONG);

                XmlAttribute attCP_BAO_BI = doc.CreateAttribute("CP_BAO_BI");
                attCP_BAO_BI.Value = hang.ChiPhiBaoBi.ToString(f);
                ITEM.Attributes.Append(attCP_BAO_BI);

                XmlAttribute attCP_DONG_GOI = doc.CreateAttribute("CP_DONG_GOI");
                attCP_DONG_GOI.Value = hang.ChiPhiDongGoi.ToString(f);
                ITEM.Attributes.Append(attCP_DONG_GOI);

                XmlAttribute attTRO_GIUP = doc.CreateAttribute("TRO_GIUP");
                attTRO_GIUP.Value = hang.TroGiup.ToString(f);
                ITEM.Attributes.Append(attTRO_GIUP);

                XmlAttribute attVAT_LIEU_MR = doc.CreateAttribute("VAT_LIEU_MR");
                attVAT_LIEU_MR.Value = hang.VatLieu.ToString(f);
                ITEM.Attributes.Append(attVAT_LIEU_MR);

                XmlAttribute attCONG_CU_MR = doc.CreateAttribute("CONG_CU_MR");
                attCONG_CU_MR.Value = hang.CongCu.ToString(f);
                ITEM.Attributes.Append(attCONG_CU_MR);

                XmlAttribute attNGUYEN_LIEU_MR = doc.CreateAttribute("NGUYEN_LIEU_MR");
                attNGUYEN_LIEU_MR.Value = hang.NguyenLieu.ToString(f);
                ITEM.Attributes.Append(attNGUYEN_LIEU_MR);

                XmlAttribute attTHIET_KE_MR = doc.CreateAttribute("THIET_KE_MR");
                attTHIET_KE_MR.Value = hang.ThietKe.ToString(f);
                ITEM.Attributes.Append(attTHIET_KE_MR);

                XmlAttribute attBAN_QUYEN = doc.CreateAttribute("BAN_QUYEN");
                attBAN_QUYEN.Value = hang.BanQuyen.ToString(f);
                ITEM.Attributes.Append(attBAN_QUYEN);

                XmlAttribute attTIEN_TRA_SD = doc.CreateAttribute("TIEN_TRA_SD");
                attTIEN_TRA_SD.Value = hang.TienTraSuDung.ToString(f);
                ITEM.Attributes.Append(attTIEN_TRA_SD);

                XmlAttribute attCP_VC = doc.CreateAttribute("CP_VC");
                attCP_VC.Value = "";
                ITEM.Attributes.Append(attCP_VC);

               XmlAttribute attCP_XEP_HANG = doc.CreateAttribute("CP_XEP_HANG");//ko co trong to khai
                attCP_XEP_HANG.Value = hang.ChiPhiVanChuyen.ToString(f);
                ITEM.Attributes.Append(attCP_XEP_HANG);

                XmlAttribute attCP_BH = doc.CreateAttribute("CP_BH");
                attCP_BH.Value = hang.PhiBaoHiem.ToString(f);
                ITEM.Attributes.Append(attCP_BH);

                XmlAttribute attTC_2 = doc.CreateAttribute("TC_2");
                attTC_2.Value = hang.TongCong2.ToString(f);
                ITEM.Attributes.Append(attTC_2);

                XmlAttribute attPHI_BH_MR = doc.CreateAttribute("PHI_BH_MR");
                attPHI_BH_MR.Value = hang.ChiPhiNoiDia.ToString(f);
                ITEM.Attributes.Append(attPHI_BH_MR);

                XmlAttribute attCP_PHAT_SINH = doc.CreateAttribute("CP_PHAT_SINH");
                attCP_PHAT_SINH.Value = hang.ChiPhiPhatSinh.ToString(f);
                ITEM.Attributes.Append(attCP_PHAT_SINH);

                XmlAttribute attTIEN_LAI = doc.CreateAttribute("TIEN_LAI");
                attTIEN_LAI.Value = hang.TienLai.ToString(f);
                ITEM.Attributes.Append(attTIEN_LAI);

                XmlAttribute attTIEN_THUE = doc.CreateAttribute("TIEN_THUE");
                attTIEN_THUE.Value = hang.TienThue.ToString(f);
                ITEM.Attributes.Append(attTIEN_THUE);

                XmlAttribute attGIAM_GIA = doc.CreateAttribute("GIAM_GIA");
                attGIAM_GIA.Value = hang.GiamGia.ToString(f);
                ITEM.Attributes.Append(attGIAM_GIA);

                XmlAttribute attCHIET_KHAU = doc.CreateAttribute("CHIET_KHAU");
                attCHIET_KHAU.Value = hang.ChietKhau.ToString(f);
                ITEM.Attributes.Append(attCHIET_KHAU);

                XmlAttribute attCP_KHONG_TANG = doc.CreateAttribute("CP_KHONG_TANG");
                attCP_KHONG_TANG.Value = hang.ChiPhiKhongTang.ToString(f);
                ITEM.Attributes.Append(attCP_KHONG_TANG);

                XmlAttribute attTC_3 = doc.CreateAttribute("TC_3");
                attTC_3.Value = hang.TongCong3.ToString(f);
                ITEM.Attributes.Append(attTC_3);

                XmlAttribute attTG_NGTE = doc.CreateAttribute("TG_NGTE");
                attTG_NGTE.Value = hang.TriGiaNguyenTe.ToString(f);
                ITEM.Attributes.Append(attTG_NGTE);

                XmlAttribute attTG_VND = doc.CreateAttribute("TG_VND");
                attTG_VND.Value = hang.TriGiaVND.ToString(f);
                ITEM.Attributes.Append(attTG_VND);

                XmlAttribute attNGAY_TRUYEN = doc.CreateAttribute("NGAY_TRUYEN");
                attNGAY_TRUYEN.Value = "";
                ITEM.Attributes.Append(attNGAY_TRUYEN);
            }
            return nodeRoot;
        }

   
        #endregion Webservice của hải quan
     
    }
}