/*
Run this script on:

.\ECSexpress.ECS_KTX_GC    -  This database will be modified

to synchronize it with:

192.168.72.100.ECS_KTX_GC

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.0.2 from Red Gate Software Ltd at 01/08/2013 4:53:09 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)
BEGIN TRANSACTION

DELETE [dbo].[t_HaiQuan_LoaiHinhMauDich]

-- Add 181 rows to [dbo].[t_HaiQuan_LoaiHinhMauDich]
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('CNC01', N'CT Nhập chế xuất sản xuất', 'CT-NCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('CNC02', N'CT Nhập chế xuất đầu tư', 'CT-NCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('CNC03', N'CT Nhập chế xuất tiêu dùng', 'CT-NCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('CNC04', N'CT Nhập chế xuất cho mục đích khác', 'CT-NCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('CXC01', N'CT Xuất chế xuất sản xuất', 'CT-XCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('CXC02', N'CT Xuất chế xuất đầu tư', 'CT-XCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('CXC04', N'CT Xuất chế xuất cho mục đích khác', 'CT-XCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('N0003', N'Nhap co quan', 'NCQ')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('N0006', N'Nhap qua canh', 'NQC-PMD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NCX01', N'Nhập chế xuất sản xuất', 'NCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NCX02', N'Nhập chế xuất đầu tư', 'NCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NCX03', N'Nhập chế xuất tiêu dùng', 'NCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NCX04', N'Nhập chế xuất cho mục đích khác', 'NCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT01', N'Nhập đầu tư', 'NDT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT02', N'Nhập Đầu Tư gắn máy', 'NDTG')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT03', N'Nhập Đầu Tư ô tô', 'NDTO')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT04', N'Nhập chuyển khẩu', 'NCK')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT05', N'Nhập Đầu Tư sửa chữa tái chế', 'NDT-TNST')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT06', N'Nhập Đầu Tư Kho Bảo Thuế', 'NDT-KBT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT07', N'Nhập Đầu Tư nhập viện trợ', 'NDT-NVT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT08', N'Nhập Đầu Tư nộp thuế', 'NDT-NPT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT09', N'Nhập Đầu Tư từ Việt Nam', 'NDTV')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT10', N'Nhap Kinh Doanh Đầu Tư (Trong nước)', 'NKD-DT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT11', N'Nhập đầu tư khu chế xuất', 'NDT-KCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT12', N'Nhập đầu tư tái xuất', 'NDT-TX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT13', N'Nhập đầu tư tạm xuất', 'NDT-TMX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT14', N'Nhập đầu tư khu công nghiệp', 'NDT-KCN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT15', N'Nhập đầu tư gia công khu công nghiệp', 'NDT-GCKCN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT16', N'Nhập Đầu tư tại chổ', 'NDT-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT17', N'Nhập Đầu tư liên doanh', 'NDT-LD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NDT18', N'Nhập Đầu tư nộp thuế tại chỗ', 'NDTNT-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC01', N'Nhập Gia Công', 'NGC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC02', N'Nhập Đầu Tư Gia công', 'NDT-GC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC03', N'Nhập Gia Công Kinh doanh', 'NGC-KD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC04', N'Nhập Gia Công Tạm nhập', 'NGC-TN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC05', N'Nhập Kinh Doanh Kho Bảo Thuế', 'NKD-KBT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC06', N'Hàng hóa tái nhập vào KCX', 'NTKCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC07', N'Hàng hóa tạm nhập vào KCX', 'NTXKCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC08', N'Nội địa mua hàng của Khu chế xuất', 'NKD-BND')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC09', N'Nhập khu chế xuất', 'NCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC10', N'Nhập chuyển tiếp', 'NCT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC11', N'Nhập gia công để SXXK', 'NGC-SXXK')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC12', N'Tạm nhập gia công tái chế', 'TNGCTC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC13', N'Nhập Gia công tại chổ', 'NGC-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC14', N'Nhập chế xuất tại chỗ', 'NCX-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC15', N'Nhập kho bảo thuế tại chỗ', 'NBT-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC16', N'Nhập Gia công từ KTM về nội địa', 'NGC/KTM-N§')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC18', N'Nhận nguyên liệu dư từ HĐGC khác', 'NPLV')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC19', N'Nhận sản phẩm GCCT từ HĐGC khác', 'SPV')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC20', N'Nhận máy móc thiết bị từ HĐGC khác', 'TBV')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC21', N'Nhập gia công tự cung ứng', 'NGC-CU')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NGC99', N'Nhập Gia công Tạm nhập Tái chế', 'NGC-TNTC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD01', N'Nhập Kinh Doanh', 'NKD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD02', N'Nhập Dầu khí', 'NDK')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD03', N'Nhập Kinh Doanh Đá Quí', 'NKD-DQ')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD04', N'Nhập Kinh Doanh Gắn máy', 'NKDG')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD05', N'Nhập Đầu Tư Kinh doanh', 'NDT-KD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD06', N'Nhập Kinh Doanh Ô tô', 'NKDO')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD07', N'Nhập hợp đồng đại lý', 'NHD-DL')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD08', N'Nhập Kinh doanh hàng đổi hàng', 'NKD-HDH')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD09', N'Nhập Kinh doanh Nội địa hóa', 'NKD-NDH')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD10', N'Nhập Kinh doanh Nội địa hóa - Gắn máy', 'NKD-NDHG')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD11', N'Nhập Kinh doanh tại chổ', 'NKD-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD12', N'Nhập Biên giới', 'NBG')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NKD13', N'Nhập Kinh doanh từ KTM về nội địa', 'NKD/KTM-N§')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NSX01', N'Nhập Để Sản Xuất Hàng Xuất Khẩu', 'NSXX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NSX02', N'Nhập Đầu Tư Sản xuất xuất khẩu', 'NDT-SXX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NSX03', N'Nhập san xuat xuat khau vao KCX', 'NSX-CX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NSX04', N'KCX mua hang noi dia de SXXK', 'NKD-MND-CX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NSX05', N'Tam nhap hang SXXK', 'TNSXXK')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NSX06', N'Nhập SXXK tại chổ', 'NSX-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NSX07', N'Nhập NPL vào kho bảo thuế để SXXK', 'NSX-KBT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA01', N'Tạm Nhập Tái Xuất (Nhập Phải Tái Xuất)', 'NTX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA02', N'Tái Nhập', 'NT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA03', N'Tạm Nhập Tàu Biển', 'NTTB')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA04', N'Nhập Đầu Tư Tái nhập', 'NDT-TAN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA05', N'Tái Nhập Hàng Xuất Triển Lãm', 'NTTL')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA06', N'Nhập kho ngoại quan', 'NKNQ')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA07', N'Nhập Uỷ Thác', 'NUT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA08', N'Nhập Viện Trợ', 'NVT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA09', N'Tái Nhập Thành Phẩm GC vào KCX', 'NGCT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA10', N'Tạm Nhập NPL vào KCX để Gia công', 'NGCN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA11', N'Nhập Đầu Tư Tạm nhập thi công', 'NDT-TNTC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA12', N'Mua Hàng Của Nội địa (Xí nghiệp KCX)', 'NKD-MND')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA13', N'Nhập Quá Cảnh', 'NQC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA14', N'Nhập Triển Lãm,Hàng mẫu,Quảng cáo ...', 'NTL')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA15', N'Nhập Đầu Tư Tạm nhập', 'NDT-TN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA16', N'Nhập Đầu Tư Kinh Doanh Cửa hàng M/Thuế', 'NDT-TNKD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA17', N'Nhập Địa Phương Ôtô', 'NDPO')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA18', N'Nhập Địa Phương Xe Máy', 'NDPG')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA19', N'Nhập hàng bán tại cửa hàng miễn thuế', 'NBMT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA20', N'Nhập Trung Ương Ôtô', 'NTWO')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA21', N'Nhập Trung Ương Xe Máy', 'NTWG')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA22', N'Nhập Viện Trợ Ôtô', 'NVTO')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA23', N'Nhập Viện Trợ Xe Máy', 'NVTG')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA24', N'Tạm nhập xăng dầu', 'TNXD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA25', N'Tạm nhập Tái chế', 'TNTC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA26', N'Tạm nhập Tái xuất tại chỗ', 'TNTX-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NTA27', N'Tái nhập tại chỗ', 'TN-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('PHPLN', N'Nhận nguyên liệu dư từ HĐGC khác', 'NPLV')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('PHPLX', N'Giao nguyên liệu dư cho HĐGC khác', 'NPLD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('PHSPN', N'Nhận sản phẩm GCCT từ HĐGC khác', 'SPV')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('PHSPX', N'Giao sản phẩm GCCT cho HĐGC khác', 'SPD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('PHTBN', N'Nhận máy móc thiết bị từ HĐGC khác', 'TBV')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('PHTBX', N'Giao máy móc thiết bị cho HĐGC khác', 'TBD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('X0003', N'Tai xuat', 'XTX-PMD ')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('X0005', N'Xu?t cung ?ng tàu bi?n', 'XCUTB')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XCX01', N'Xuất chế xuất sản xuất', 'XCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XCX02', N'Xuất chế xuất đầu tư', 'XCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XCX04', N'Xuất chế xuất cho mục đích khác', 'XCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT01', N'Xuất Đầu Tư', 'XDT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT02', N'Xuất Đầu Tư Gắn máy', 'XDTG')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT03', N'Xuất Đầu Tư Ô tô', 'XDTO')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT04', N'Xuất Chuyển khẩu', 'XCK')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT05', N'Xuất Đầu Tư đã sửa chữa tái chế', 'XDT-TXST')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT06', N'Xuất Đầu Tư Kho Bảo Thuế', 'XDT-KBT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT07', N'Xuất đầu tư khu công nghiệp', 'XDT-KCN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT08', N'Xuất đầu tư gia công khu công nghiệp', 'XDT-GCKCN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT09', N'Xuất đầu tư kinh doanh khu công nghiệp', 'XDT-KDKCN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT10', N'Xuất Đầu tư tại chổ', 'XDT-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XDT16', N'Xuất đầu tư tại chỗ', 'X§T-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC01', N'Xuất Gia Công', 'XGC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC02', N'Xuất Đầu Tư Gia công', 'XDT-GC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC03', N'Xuất Gia Công Kinh doanh', 'XGC-KD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC04', N'Xuất Gia Công Tái xuất', 'XGC-TX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC05', N'Xuất Kinh Doanh Kho Bảo Thuế', 'XKD-KBT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC06', N'Hàng hóa tái xuất ra nước ngoài từ KCX', 'XTKCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC07', N'Hàng hóa tạm xuất ra nước ngoài từ KCX', 'XTNKCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC08', N'Hàng trong nội địa bán cho KCX', 'XKD-MND')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC09', N'Sản Phẩm KCX xuất ra nước ngoài', 'XDTKCX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC10', N'Tái xuất gia công tái chế', 'TXGCTC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC12', N'Xuat NL tu KCX vao noi dia de GC', 'XGC-CX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC13', N'Xuất Gia công tại chổ', 'XGC-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC14', N'Xuất chế xuất tại chỗ', 'XCX-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC15', N'Xuất kho bảo thuế tại chỗ', 'XBT-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC16', N'Xuất Gia công từ nội địa vào KTM', 'XGC/N§-KTM')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC18', N'Giao nguyên phụ liệu dư cho HDGC khác', 'NPLD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC19', N'Giao sản phẩm GCCT cho HĐGC khác', 'SPD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XGC20', N'Giao máy móc thiết bị cho HĐGC khác', 'TBD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD01', N'Xuất Kinh Doanh', 'XKD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD02', N'Xuất Dầu khí', 'XDK')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD03', N'Xuất Kinh Doanh Đá Quí', 'XKD-DQ')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD04', N'Xuất Kinh Doanh Gắn máy', 'XKDG')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD05', N'Xuất Đầu Tư Kinh doanh', 'XDT-KD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD06', N'Xuất Kinh Doanh Ô tô', 'XKDO')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD07', N'Xuất kinh doanh hàng đổi hàng', 'XKD-HDH')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD08', N'Xuất Kinh doanh tại chổ', 'XKD-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD09', N'Xuất Kinh doanh từ nội địa vào KTM', 'XKD/ND-KTM')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD10', N'Xuất Biên giới', 'XKD-BG')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XKD11', N'Xuất Kinh doanh phục vụ Đầu tư', 'XKD-DT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XSX01', N'Xuất khẩu hàng SX từ hàng NK', 'XSXN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XSX02', N'Xuất Đầu Tư Sản xuất xuất khẩu', 'XDT-SXX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XSX03', N'Xuat san xuat xuat khau tu KCX', 'XSX-CX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XSX04', N'KCX ban hang noi dia de SXXK', 'XKD-BND-CX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XSX05', N'Tái xuất SXXK', 'TXSXXK')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XSX06', N'Xuất SXXK tại chổ', 'XSX-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XSX07', N'Xuất SXXK vào kho bảo thuế', 'XSX-KBT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA01', N'Tạm Xuất Tái Nhập (Xuất Phải Tái Nhập)', 'XTN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA02', N'Tái Xuất', 'XT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA03', N'Tái Xuất Tàu Biển', 'XTTB')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA04', N'Xuất Đầu Tư Tái xuất', 'XDT-TAX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA05', N'Tạm Xuất  Triển Lãm', 'XTTL')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA06', N'Xuất kho ngoại quan', 'XKNQ')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA07', N'Xuất Uỷ Thác', 'XUT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA08', N'Xuất Viện Trợ', 'XVT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA09', N'Tái Xuất Thành Phẩm GC vào Nội địa', 'XGCT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA10', N'Tạm Xuất NPL vào Nội địa để Gia công', 'XGCN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA11', N'Xuất Đầu Tư Tái xuất thi công', 'XDT-TXTC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA12', N'Bán Hàng cho Nội địa (Xí nghiệp KCX)', 'XKD-BND')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA13', N'Xuất Quá Cảnh', 'XQC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA14', N'Xuất Triển Lãm,Hàng mẫu,Quảng cáo ...', 'XTL')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA15', N'Xuất Đầu Tư Tạm xuất', 'XDT-TX')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA16', N'Tái Xuất Xăng Dầu', 'XTXD')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA17', N'Xuất hàng bán tại cửa hàng miễn thuế', 'XBMT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA18', N'Tái xuất hàng bán miễn thuế', 'XT-BMT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA19', N'Tái xuất Tái chế', 'XT-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA20', N'Tái xuất ( Hàng tạm nhập tái xuất)', 'XTX-HTN')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA21', N'Xuất Tại chỗ Tái xuất', 'XTA-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA22', N'Tạm xuất Tái nhập tại chỗ', 'TXTN-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA23', N'Tái xuất tại chỗ', 'TX-TC')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XTA24', N'TáI xuất Đầu tư Kho Bảo Thuế', 'TXDT-KBT')

INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('NCX05', N'Nhập trả hàng xuất chế xuất', 'NCX-NT')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT]) VALUES ('XCX05', N'Xuất trả hàng nhập chế xuất', 'XCX-XT')

--USE  [ECS_TQDT_GC_V4]
GO

DELETE FROM dbo.t_HaiQuan_LoaiPhieuChuyenTiep
GO

INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('PHPLN', N'Nhận nguyên liệu dư từ HĐGC khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('PHPLX', N'Giao nguyên liệu dư cho HĐGC khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('PHSPN', N'Nhận sản phẩm GCCT từ HĐGC khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('PHSPX', N'Giao sản phẩm GCCT cho HĐGC khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('PHTBN', N'Nhận máy móc thiết bị từ HĐGC khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('PHTBX', N'Giao máy móc thiết bị cho HĐGC khác')

INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('NGC18', N'Nhận nguyên liệu dư từ HĐGC khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('NGC19', N'Nhận sản phẩm GCCT từ HĐGC khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('NGC20', N'Nhận máy móc thiết bị từ HĐGC khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('XGC18', N'Giao nguyên phụ liệu dư cho HDGC khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('XGC19', N'Giao sản phẩm GCCT cho HĐGC khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhieuChuyenTiep] ([ID], [Ten]) VALUES ('XGC20', N'Giao máy móc thiết bị cho HĐGC khác')

GO
--USE  [ECS_TQDT_GC_V4]

GO

/****** Object:  StoredProcedure [dbo].[p_GC_Convert_MaHS8So]    Script Date: 02/05/2013 10:21:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_Convert_MaHS8So]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_Convert_MaHS8So]
GO

/****** Object:  StoredProcedure [dbo].[p_GC_Convert_MaHS8SoAuto]    Script Date: 02/05/2013 10:21:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_Convert_MaHS8SoAuto]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_Convert_MaHS8SoAuto]
GO

--USE  [ECS_TQDT_GC_V4]
GO

/****** Object:  StoredProcedure [dbo].[p_GC_Convert_MaHS8So]    Script Date: 02/05/2013 10:21:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create PROC [dbo].[p_GC_Convert_MaHS8So]
(
	@MaHaiQuan	VARCHAR(10),
	@MaDoanhNghiep	VARCHAR(20),
	@Ma	NVARCHAR(250),
	@DVT_ID	NVARCHAR(50),
	@MaHSCu	VARCHAR(10),
	@MaHSMoi	VARCHAR(10)
)
AS
begin

/*Updated by HungTQ, 13/09/2012*/

--NPL
Update dbo.t_KDT_GC_NguyenPhuLieu SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_NguyenPhuLieu SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--SP
Update dbo.t_KDT_GC_SanPham SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_SanPham SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--TB
Update dbo.t_KDT_GC_ThietBi SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_ThietBi SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--Hang mau
Update dbo.t_KDT_GC_HangMau SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_HangMau SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--NPL Cung ung
Update dbo.t_KDT_GC_NPLCungUng SET MaHS = @MaHSMoi WHERE MaHang = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  TKMDID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)

Update dbo.t_KDT_HangMauDich SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_HangMauDich_BanLuu SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_HangMauDichCheXuat SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_GC_HangChuyenTiep SET MaHS = @MaHSMoi WHERE MaHang = @Ma AND ID_DVT = @DVT_ID AND MaHS = @MaHSCu AND Master_ID IN (SELECT ID FROM dbo.t_KDT_GC_ToKhaiChuyenTiep where MaHaiQuanTiepNhan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep) 
Update dbo.t_KDT_GC_HangPhuKien SET MaHS = @MaHSMoi WHERE MaHang = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu 
Update dbo.t_KDT_GC_HangGSTieuHuy SET MaHS = @MaHSMoi WHERE MaHang = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu 
Update dbo.t_KDT_GC_HangThanhKhoan SET MaHS = @MaHSMoi WHERE MaHang = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu 

--Update HS 8 so hang trong Chung tu kem
Update dbo.t_KDT_HangGiayPhepDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  GiayPhep_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_GiayPhep where MaDoanhNghiep = @MaDoanhNghiep)   
Update dbo.t_KDT_HangVanDonDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu 
Update dbo.t_KDT_HangCoDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu 
Update dbo.t_KDT_HoaDonThuongMaiDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HoaDonTM_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_HoaDon where MaDoanhNghiep = @MaDoanhNghiep)   
Update dbo.t_KDT_HopDongThuongMaiDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDongTM_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_HopDong where MaDoanhNghiep = @MaDoanhNghiep)  

end

GO

/****** Object:  StoredProcedure [dbo].[p_GC_Convert_MaHS8SoAuto]    Script Date: 02/05/2013 10:21:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[p_GC_Convert_MaHS8SoAuto]  
(  
 @MaHaiQuan VARCHAR(10),  
 @MaDoanhNghiep VARCHAR(20)
)  
AS  
begin  
  
/*Updated by HungTQ, 13/09/2012*/  
  
--NPL
Update dbo.t_KDT_GC_NguyenPhuLieu SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_NguyenPhuLieu SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--SP
Update dbo.t_KDT_GC_SanPham SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_SanPham SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--TB
Update dbo.t_KDT_GC_ThietBi SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_ThietBi SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--Hang mau
Update dbo.t_KDT_GC_HangMau SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_HangMau SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--NPL Cung ung
Update dbo.t_KDT_GC_NPLCungUng SET MaHS = LEFT(MaHS, 8) WHERE TKMDID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)

Update dbo.t_KDT_HangMauDich SET MaHS = LEFT(MaHS, 8) WHERE TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_HangMauDich_BanLuu SET MaHS = LEFT(MaHS, 8) WHERE TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_HangMauDichCheXuat SET MaHS = LEFT(MaHS, 8) WHERE TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_GC_HangChuyenTiep SET MaHS = LEFT(MaHS, 8)WHERE Master_ID IN (SELECT ID FROM dbo.t_KDT_GC_ToKhaiChuyenTiep where MaHaiQuanTiepNhan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep) 
Update dbo.t_KDT_GC_HangPhuKien SET MaHS = LEFT(MaHS, 8)
Update dbo.t_KDT_GC_HangGSTieuHuy SET MaHS = LEFT(MaHS, 8)
Update dbo.t_KDT_GC_HangThanhKhoan SET MaHS = LEFT(MaHS, 8)

--Update HS 8 so hang trong Chung tu kem
Update dbo.t_KDT_HangGiayPhepDetail SET MaHS = LEFT(MaHS, 8) WHERE GiayPhep_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_GiayPhep where MaDoanhNghiep = @MaDoanhNghiep)   
Update dbo.t_KDT_HangVanDonDetail SET MaHS = LEFT(MaHS, 8)
Update dbo.t_KDT_HangCoDetail SET MaHS = LEFT(MaHS, 8)
Update dbo.t_KDT_HoaDonThuongMaiDetail SET MaHS = LEFT(MaHS, 8) WHERE HoaDonTM_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_HoaDon where MaDoanhNghiep = @MaDoanhNghiep)   
Update dbo.t_KDT_HopDongThuongMaiDetail SET MaHS = LEFT(MaHS, 8) WHERE HopDongTM_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_HopDong where MaDoanhNghiep = @MaDoanhNghiep)  
  
end

GO

UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '6.7'
      ,[Date] = getdate()
      ,[Notes] = ''
      
COMMIT TRANSACTION
GO
