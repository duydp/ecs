﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.QuanLyChungTu.CX;

namespace Company.Interface.KDT.GC
{
    public partial class UpdateStatusForm : BaseForm
    {
        public DinhMucDangKy DMDK = new DinhMucDangKy();
        public SanPhamDangKy spDangKy = new SanPhamDangKy();
        public NguyenPhuLieuDangKy nplDangKy = new NguyenPhuLieuDangKy();
        public HangDuaVaoDangKy hangDuavao = new HangDuaVaoDangKy();
        public HangDuaRaDangKy hangDuaRa = new HangDuaRaDangKy();
        public T_KDT_VNACCS_WarehouseImport WarehouseImport;
        public T_KDT_VNACCS_WarehouseExport WarehouseExport;
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New BCQT;
        public KDT_VNACCS_TotalInventoryReport BCCT;
        public DinhMucDangKyCollection DMDKCollection = new DinhMucDangKyCollection();
        public SanPhamDangKyCollection spDangKyCoecltion = new SanPhamDangKyCollection();
        public NguyenPhuLieuDangKyCollection nplDangKyCollection = new NguyenPhuLieuDangKyCollection();
        public KDT_VNACCS_StorageAreasProduction CSSX= new KDT_VNACCS_StorageAreasProduction();
        public string formType = "";
        public UpdateStatusForm()
        {
            InitializeComponent();
        }

        private void UpdateStatusForm_Load(object sender, EventArgs e)
        {
            txtSoTN.Enabled = false;
            txtSoTN.ForeColor = Color.Red;
            clcNgayTN.Enabled = false;
            clcNgayTN.ForeColor = Color.Red;
            txtChiCucHQ.Enabled = false;
            txtChiCucHQ.ForeColor = Color.Red;
            cbbTrangThai.SelectedValue = 1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            if (formType=="NPL")
            {
                txtSoTN.Text = nplDangKy.SoTiepNhan.ToString();
                txtSoTN.ForeColor = Color.Red;
                clcNgayTN.Value = nplDangKy.NgayTiepNhan;
                clcNgayTN.ForeColor = Color.Red;
                txtChiCucHQ.Text = nplDangKy.MaHaiQuan;
                txtChiCucHQ.ForeColor = Color.Red;
                switch (nplDangKy.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    case "-3":
                        txtTrangThai.Text = "Đã khai báo";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = nplDangKy.TrangThaiXuLy;
            }
            else if (formType=="HDV")
            {
                txtSoTN.Text = hangDuavao.SoTiepNhan.ToString();
                txtSoTN.ForeColor = Color.Red;
                clcNgayTN.Value = hangDuavao.NgayTiepNhan;
                clcNgayTN.ForeColor = Color.Red;
                txtChiCucHQ.Text = hangDuavao.MaHaiQuan;
                txtChiCucHQ.ForeColor = Color.Red;
                switch (hangDuavao.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    case "-3":
                        txtTrangThai.Text = "Đã khai báo";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = hangDuavao.TrangThaiXuLy;
            }
            else if (formType=="DM")
            {
                txtSoTN.Text = DMDK.SoTiepNhan.ToString();
                clcNgayTN.Value = DMDK.NgayTiepNhan;
                txtChiCucHQ.Text = DMDK.MaHaiQuan;
                switch (DMDK.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    case "-3":
                        txtTrangThai.Text = "Đã khai báo";
                        break;
                    default:
                        break;
                }                
                cbbTrangThai.SelectedValue = DMDK.TrangThaiXuLy;
            }
            else if (formType=="SP")
            {
                txtSoTN.Text = spDangKy.SoTiepNhan.ToString();
                clcNgayTN.Value = spDangKy.NgayTiepNhan;
                txtChiCucHQ.Text = spDangKy.MaHaiQuan;
                switch (spDangKy.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    case "-3":
                        txtTrangThai.Text = "Đã khai báo";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = spDangKy.TrangThaiXuLy;                
            }
            else if (formType == "PNK")
            {
                txtSoTN.Text = WarehouseImport.SoTN.ToString();
                clcNgayTN.Value = WarehouseImport.NgayTN;
                txtChiCucHQ.Text = WarehouseImport.MaHQ;
                switch (WarehouseImport.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = WarehouseImport.TrangThaiXuLy;
            }
            else if (formType == "PXK")
            {
                txtSoTN.Text = WarehouseExport.SoTN.ToString();
                clcNgayTN.Value = WarehouseExport.NgayTN;
                txtChiCucHQ.Text = WarehouseExport.MaHQ;
                switch (WarehouseExport.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = WarehouseExport.TrangThaiXuLy;
            }
            else if (formType == "BCQT")
            {
                txtSoTN.Text = BCQT.SoTN.ToString();
                clcNgayTN.Value = BCQT.NgayTN;
                txtChiCucHQ.Text = BCQT.MaHQ;
                switch (BCQT.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = BCQT.TrangThaiXuLy;
            }
            else if (formType == "BCCT")
            {
                txtSoTN.Text = BCCT.SoTiepNhan.ToString();
                clcNgayTN.Value = BCCT.NgayTiepNhan;
                txtChiCucHQ.Text = BCCT.MaHaiQuan;
                switch (BCCT.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = BCCT.TrangThaiXuLy;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                int lenghtSoTN = txtSoTN.Text.ToString().Length;
                if (formType == "NPL")
                {
                    if (lenghtSoTN <= 6 && cbbTrangThai.SelectedValue == "1" || cbbTrangThai.SelectedValue == "0" && lenghtSoTN <= 6 || cbbTrangThai.SelectedValue == "5" && lenghtSoTN <= 6)
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "NPL NÀY CHƯA ĐƯỢC KHAI BÁO ĐẾN HQ \nDOANH NGHIỆP THỰC HIỆN KHAI BÁO HỢP ĐỒNG NÀY ĐẾN HQ", false);
                        return;
                    }
                    else if (lenghtSoTN > 7 || cbbTrangThai.SelectedValue == "1" )
                    {
                        nplDangKy.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        nplDangKy.Update();
                        nplDangKy.TransgferDataToSXXK();
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }
                    else
                    {
                        nplDangKy.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        nplDangKy.Update();
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }
                }
                else if (formType == "HDV")
                {
                    if (lenghtSoTN <= 6 && cbbTrangThai.SelectedValue == "1" || cbbTrangThai.SelectedValue == "0" && lenghtSoTN <= 6 || cbbTrangThai.SelectedValue == "5" && lenghtSoTN <= 6)
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "NPL NÀY CHƯA ĐƯỢC KHAI BÁO ĐẾN HQ \nDOANH NGHIỆP THỰC HIỆN KHAI BÁO HỢP ĐỒNG NÀY ĐẾN HQ", false);
                        return;
                    }
                    else if (lenghtSoTN > 7 || cbbTrangThai.SelectedValue == "1" )
                    {
                        hangDuavao.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        hangDuavao.Update();
                        Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy.TransgferHDVToSXXK(hangDuavao);
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }
                    else
                    {
                        hangDuavao.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        hangDuavao.Update();
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }                    
                }
                else if (formType == "DM")
                {
                    if (lenghtSoTN <= 6 && cbbTrangThai.SelectedValue == "1" || cbbTrangThai.SelectedValue == "0" && lenghtSoTN <= 6 || cbbTrangThai.SelectedValue == "5" && lenghtSoTN <= 6)
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐỊNH MỨC NÀY CHƯA ĐƯỢC KHAI BÁO ĐẾN HQ \nDOANH NGHIỆP THỰC HIỆN KHAI BÁO ĐỊNH MỨC NÀY ĐẾN HQ", false);
                        return;
                    }
                    else if (lenghtSoTN > 7 || cbbTrangThai.SelectedValue == "1" )
                    {
                        DMDK.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        DMDK.Update();
                        DMDK.TransferDataToSXXK();
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();                        
                    }
                    else
                    {
                        DMDK.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        DMDK.Update();
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }
                }
                else if (formType == "SP")
                {
                    if (lenghtSoTN <= 6 && cbbTrangThai.SelectedValue == "1" || cbbTrangThai.SelectedValue == "0" && lenghtSoTN <= 6 || cbbTrangThai.SelectedValue == "5" && lenghtSoTN <= 6)
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ","PHỤ KIỆN NÀY CHƯA ĐƯỢC KHAI BÁO ĐẾN HQ \nDOANH NGHIỆP THỰC HIỆN KHAI BÁO PHỤ KIỆN NÀY ĐẾN HQ", false);
                        return;
                    }
                    else if (lenghtSoTN > 7 || cbbTrangThai.SelectedValue == "1")
                    {
                        spDangKy.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        spDangKy.Update();
                        spDangKy.TransgferDataToSXXK();
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();                        
                    }
                    {
                        spDangKy.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        spDangKy.Update();
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }                    
                }
                else if (formType == "PNK")
                {
                    WarehouseImport.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                    WarehouseImport.Update();
                    ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                    this.Close();
                }
                else if (formType == "PXK")
                {
                    WarehouseExport.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                    WarehouseExport.Update();
                    ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                    this.Close();
                }
                else if (formType == "BCQT")
                {
                    BCQT.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                    BCQT.Update();
                    ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                    this.Close();
                }
                else if (formType == "BCCT")
                {
                    BCCT.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                    BCCT.Update();
                    ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
