﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class AdditionalInformation
    {
        /// <summary>
        /// Nội dung phản hồi khác
        /// </summary>
        [XmlElement("statement")]
        public string Statement { get; set; }

        /// <summary>
        /// Mã nội dung phản hồi
        /// </summary>
        [XmlElement("content")]
        public Content Content { get; set; }

        // <summary>
        // Mã nội dung phản hồi
        // </summary>
        //[XmlElement("content")]
        //public String GhiChu { get; set; }

        /// <summary>
        /// Địa điểm
        /// </summary>
        [XmlElement("examinationPlace")]
        public string ExaminationPlace { get; set; }

        [XmlElement("time")]
        public string Time { get; set; }
        /// <summary>
        /// tuyến đường
        /// </summary>
        [XmlElement("route")]
        public string Route { get; set; }
        /// <summary>
        ///  Mô tả thông tin
        /// </summary>
        [XmlElement("statementDescription")]
        public string StatementDescription { get; set; }

        [XmlElement("Content")]
        public Content ContentPK { get; set; }

 

        /// <summary>
        /// Số ngày gia hạn thanh khoản
        /// </summary>
        [XmlElement("extensionNumberDate")]
        public string ExtensionNumberDate { get; set; }

        [XmlElement("customsValue")]
        public string CustomsValue { get; set; }

        [XmlElement("documentType")]
        public string DocumentType { get; set; }


        [XmlElement("documentReference")]
        public string DocumentReference { get; set; }

        [XmlElement("description")]
        public string Description { get; set; }

        /// <summary>
        /// Ghi chú thông tin trả về từ HQ cảng
        /// </summary>

        /// <summary>
        /// Cho phép lấy hàng hay không?
        /// 0: Chưa đủ điều kiện
        /// 1: Đã đủ điều kiện lấy hàng
        /// </summary>
        [XmlElement("inactive")]
        public string Inactive { get; set; }

        /// <summary>
        /// Diễn giải chi tiết
        /// </summary>
        [XmlElement("journalMemo")]
        public string JournalMemo { get; set; }

    }
   
}
