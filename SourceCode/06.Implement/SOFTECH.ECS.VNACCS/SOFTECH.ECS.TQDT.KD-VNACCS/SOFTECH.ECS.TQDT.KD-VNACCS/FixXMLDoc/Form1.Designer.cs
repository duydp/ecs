﻿namespace FixXMLDoc
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtContents = new System.Windows.Forms.TextBox();
            this.txtPart = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtFix = new System.Windows.Forms.TextBox();
            this.btnBrowseAll = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // txtContents
            // 
            this.txtContents.Location = new System.Drawing.Point(12, 77);
            this.txtContents.Multiline = true;
            this.txtContents.Name = "txtContents";
            this.txtContents.Size = new System.Drawing.Size(382, 375);
            this.txtContents.TabIndex = 0;
            // 
            // txtPart
            // 
            this.txtPart.Location = new System.Drawing.Point(12, 34);
            this.txtPart.Name = "txtPart";
            this.txtPart.Size = new System.Drawing.Size(305, 20);
            this.txtPart.TabIndex = 1;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(323, 32);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtFix
            // 
            this.txtFix.Location = new System.Drawing.Point(400, 77);
            this.txtFix.Multiline = true;
            this.txtFix.Name = "txtFix";
            this.txtFix.Size = new System.Drawing.Size(483, 375);
            this.txtFix.TabIndex = 3;
            // 
            // btnBrowseAll
            // 
            this.btnBrowseAll.Location = new System.Drawing.Point(544, 34);
            this.btnBrowseAll.Name = "btnBrowseAll";
            this.btnBrowseAll.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseAll.TabIndex = 2;
            this.btnBrowseAll.Text = "Browse... all";
            this.btnBrowseAll.UseVisualStyleBackColor = true;
            this.btnBrowseAll.Click += new System.EventHandler(this.btnBrowseAll_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 525);
            this.Controls.Add(this.txtFix);
            this.Controls.Add(this.btnBrowseAll);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtPart);
            this.Controls.Add(this.txtContents);
            this.Name = "Form1";
            this.Text = "Fix XML";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtContents;
        private System.Windows.Forms.TextBox txtPart;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtFix;
        private System.Windows.Forms.Button btnBrowseAll;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}

