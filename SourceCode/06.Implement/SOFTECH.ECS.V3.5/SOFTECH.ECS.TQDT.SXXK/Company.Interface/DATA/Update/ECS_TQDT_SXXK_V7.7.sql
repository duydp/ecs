/****** Object:  View [dbo].[v_KDT_SXXK_NPLXuatTon]    Script Date: 03/29/2013 14:26:41 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_KDT_SXXK_NPLXuatTon]'))
DROP VIEW [dbo].[v_KDT_SXXK_NPLXuatTon]
GO

/****** Object:  View [dbo].[v_KDT_SXXK_NPLXuatTon]    Script Date: 03/29/2013 14:26:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_KDT_SXXK_NPLXuatTon]
AS
SELECT     TOP (100) PERCENT a.SoToKhai, a.MaLoaiHinh, a.NamDangKy, a.MaHaiQuan, c.MaSanPham AS MaSP, b.TenHang AS TenSP, e.Ten AS TenDVT_SP, 
                      b.SoLuong AS LuongSP, c.MaNguyenPhuLieu AS MaNPL, c.DinhMucChung AS DinhMuc, b.SoLuong * c.DinhMucChung AS LuongNPL, 
                      b.SoLuong * c.DinhMucChung AS TonNPL, a.BangKeHoSoThanhLy_ID, CASE WHEN year(d .NGAY_THN_THX) 
                      > 1900 THEN d .Ngay_THN_THX ELSE d .NgayDangKy END AS NgayThucXuat, CASE WHEN year(d .NgayHoanThanh) 
                      > 1900 THEN d .NgayHoanThanh ELSE d .NgayDangKy END AS NgayHoanThanhXuat, d.NgayHoanThanh, a.NgayDangKy
FROM         dbo.t_KDT_SXXK_BKToKhaiXuat AS a INNER JOIN
                      dbo.t_SXXK_ToKhaiMauDich AS d ON a.SoToKhai = d.SoToKhai AND a.MaLoaiHinh = d.MaLoaiHinh AND a.NamDangKy = d.NamDangKy AND 
                      a.MaHaiQuan = d.MaHaiQuan INNER JOIN
                      dbo.t_SXXK_HangMauDich AS b ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh AND a.NamDangKy = b.NamDangKy AND 
                      a.MaHaiQuan = b.MaHaiQuan INNER JOIN
                      dbo.t_SXXK_DinhMuc AS c ON b.MaPhu = c.MaSanPham INNER JOIN
                      dbo.t_HaiQuan_DonViTinh AS e ON b.DVT_ID = e.ID
WHERE     (a.MaLoaiHinh LIKE 'XSX%' OR a.MaLoaiHinh LIKE 'XCX%') AND (d.Xuat_NPL_SP = 'S')
ORDER BY d.NgayHoanThanh, a.SoToKhai

------------------------------------------------------------------------

GO



/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]    Script Date: 03/29/2013 14:28:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]
GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]    Script Date: 03/29/2013 14:28:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]
    @MaDoanhNghiep VARCHAR(14) ,
    @MaHaiQuan CHAR(6) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
    SELECT  A.MaHaiQuan ,
            A.SoToKhai ,
            A.MaLoaiHinh ,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan        
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'XSX%'
                  OR A.MaLoaiHinh LIKE 'XGC%'
                  OR A.MaLoaiHinh LIKE 'XCX%'
                ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL         
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   MaHaiQuan = @MaHaiQuan
                    AND MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy != 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh        
        
/* Comment Script        
        
SELECT           
 A.MaHaiQuan,          
 A.SoToKhai,          
 A.MaLoaiHinh,          
 A.NamDangKy,          
 A.NgayDangKy,          
 A.NGAY_THN_THX          
FROM         
(        
 --HungTQ Updated 22/06/2011: Bo sung loc du lieu theo TrangThaiXuLy = 1        
 SELECT   DISTINCT        
  A.MaHaiQuan,         
  A.MaDoanhNghiep,         
  A.SoToKhai,          
  A.MaLoaiHinh,          
  A.NamDangKy,          
  A.NgayDangKy,          
  A.NGAY_THN_THX, tkmd.TrangThaiXuLy         
 FROM t_SXXK_ToKhaiMauDich A  LEFT JOIN  dbo.t_KDT_ToKhaiMauDich tkmd        
 ON a.SoToKhai = tkmd.SoToKhai AND a.MaLoaiHinh = tkmd.MaLoaiHinh AND a.NamDangKy = YEAR(tkmd.NgayDangKy) AND A.MaHaiQuan = tkmd.MaHaiQuan AND A.MaDoanhNghiep = tkmd.MaDoanhNghiep        
) A          
 LEFT JOIN (          
   SELECT DISTINCT          
    SoToKhai,          
    MaLoaiHinh,          
    NamDangKy,          
    MaHaiQuan          
   FROM t_KDT_SXXK_BKToKhaiXuat          
   WHERE          
    MaHaiQuan = @MaHaiQuan) D ON          
  A.SoToKhai=D.SoToKhai AND          
  A.MaLoaiHinh=D.MaLoaiHinh AND          
  A.NamDangKy=D.NamDangKy AND          
  A.MaHaiQuan=D.MaHaiQuan          
WHERE          
 A.MaDoanhNghiep = @MaDoanhNghiep AND          
 A.MaHaiQuan = @MaHaiQuan AND          
 (A.MaLoaiHinh LIKE 'XSX%' OR A.MaLoaiHinh LIKE 'XGC%')          
 AND A.NgayDangKy between @TuNgay and @DenNgay          
 AND (A.NGAY_THN_THX IS NOT NULL OR year(A.NGAY_THN_THX)!=1900 )          
 AND D.SoToKhai IS NULL           
 AND A.TrangThaiXuLy = 1        
           
ORDER BY A.NgayDangKy , A.SoToKhai, A.MaLoaiHinh          
*/          
          
          
          
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '7.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('7.7',GETDATE(), N'')
END