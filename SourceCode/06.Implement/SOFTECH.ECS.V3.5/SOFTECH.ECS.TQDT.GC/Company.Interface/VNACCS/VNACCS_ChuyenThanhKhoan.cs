﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Logger;
using Company.GC.BLL.KDT;

namespace Company.Interface.VNACCS
{
    public partial class VNACCS_ChuyenThanhKhoan : BaseForm
    {
        public string NamDangKy = "";
        public VNACCS_ChuyenThanhKhoan()
        {
            InitializeComponent();
        }

        private void VNACCS_ChuyenThanhKhoan_Load(object sender, EventArgs e)
        {
            SetError(string.Empty);
            loadData();

        }
        private void loadData()
        {
            bool isAMAUpdate = false;
            String condition = "";
            String TK = "";
            try
            {
                if (checkAMAIsUpdate.Checked ? isAMAUpdate = true : isAMAUpdate = false) ;
#if SXXK_V4
                List<KDT_VNACC_HangMauDich> lisHMDVNACCS = KDT_VNACC_HangMauDich.SelectCollectionAllDynamic("CASE WHEN TKMD_ID<>0 THEN (SELECT YEAR(NgayDangKy) FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE ID=TKMD_ID) ELSE 1900 END=" + NamDangKy, null);
#elif GC_V4
                List<KDT_VNACC_HangMauDich> lisHMDVNACCS = KDT_VNACC_HangMauDich.SelectCollectionDynamic("CASE WHEN TKMD_ID<>0 THEN (SELECT YEAR(NgayDangKy) FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE ID=TKMD_ID) ELSE 1900 END=" + NamDangKy, null);
#endif
                List<KDT_VNACC_ToKhaiMauDich> listTK = KDT_VNACC_ToKhaiMauDich.SelectCollectionGCChuaThanhKhoan(isAMAUpdate);
                if (checkVNACCSValid.Checked)
                {
                    List<KDT_VNACC_HangMauDich> lisHMD = new List<KDT_VNACC_HangMauDich>();
                    List<KDT_VNACC_ToKhaiMauDich> listTKVNACCS = new List<KDT_VNACC_ToKhaiMauDich>();
                    List<KDT_VNACC_ToKhaiMauDich> listTKMDVNACCS = new List<KDT_VNACC_ToKhaiMauDich>();
                    IList<HangMauDich> hmdCollection = new List<HangMauDich>();
                    IList<HangMauDich> hmdCollectionValid = new List<HangMauDich>();
                    TK = "(";
                    foreach (KDT_VNACC_HangMauDich hmdVNACCS in lisHMDVNACCS)
                    {
                        long TKMD_ID = hmdVNACCS.TKMD_ID;
#if SXXK_V4
                        Int32 SoToKhai = hmdVNACCS.SoToKhai;
#elif GC_V4
                        decimal SoToKhai = hmdVNACCS.SoToKhai;

#endif
                        String SoThuTuHang = hmdVNACCS.SoDong;
                        String MaHS = hmdVNACCS.MaSoHang;
                        String MaPhu = hmdVNACCS.MaHangHoa;
                        String TenHang = hmdVNACCS.TenHang;
                        Decimal SoLuong = hmdVNACCS.SoLuong1;
                        Decimal DonGiaKB = hmdVNACCS.DonGiaHoaDon;
                        Decimal TriGiaKB = hmdVNACCS.TriGiaHoaDon;
                        String where = String.Format("SoThuTuHang={0} AND MaHS='{1}' AND MaPhu=N'{2}' AND TenHang LIKE N'{3}%' AND SoLuong={4} AND DonGiaKB={5} AND  TriGiaKB ={6}", SoThuTuHang, MaHS, MaPhu, TenHang, SoLuong, DonGiaKB, TriGiaKB);

                        
#if SXXK_V4
                        where = where + " AND CASE WHEN TKMD_ID<>0 THEN (SELECT SoToKhai FROM dbo.t_KDT_ToKhaiMauDich WHERE ID=TKMD_ID )ELSE NULL END=" + SoToKhai;
                        where = where.Replace(",", ".");

                        if (HangMauDich.SelectCollectionAllDynamic(where, null).Count > 0)
#elif GC_V4
                        where = where + " AND CASE WHEN TKMD_ID<>0 THEN (SELECT SoToKhai FROM dbo.t_KDT_ToKhaiMauDich WHERE ID=TKMD_ID )ELSE NULL END=" +SoToKhai;
                        where = where.Replace(",", ".");
                        if (HangMauDich.SelectCollectionDynamic(where, null).Count > 0)
#endif
                        {
                            //
                        }
                        else
                        {
                            where = String.Format("CASE WHEN TKMD_ID<>0 THEN (SELECT SoToKhai FROM dbo.t_KDT_ToKhaiMauDich WHERE ID=TKMD_ID )ELSE NULL END={0} AND SoThuTuHang='{1}'", TKMD_ID, SoThuTuHang);
#if SXXK_V4
                            if (HangMauDich.SelectCollectionAllDynamic(where, null).Count > 0)
#elif GC_V4
                            if (HangMauDich.SelectCollectionDynamic(where, null).Count > 0)
#endif
                            {
                                hmdCollection = HangMauDich.SelectCollectionDynamic(where, null);
                                foreach (HangMauDich hmd in hmdCollection)
                                {
                                    hmdCollectionValid.Add(hmd);
                                }
                            }

                            TK += hmdVNACCS.TKMD_ID + ",";
                        }

                    }

                    TK = TK.Substring(0, TK.Length - 1);
                    TK += ")";
                    if (String.IsNullOrEmpty(TK))
                    {
                    }
                    else
                    {
                        condition = String.Format("ID IN {0}", TK);
                        listTKVNACCS = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic(condition, null);
                        foreach (KDT_VNACC_ToKhaiMauDich TKMDVNACCS in listTKVNACCS)
                        {
                            listTKMDVNACCS.Add(TKMDVNACCS);
                        }
                    }
                    grList.DataSource = listTKMDVNACCS;
                }
                else
                {
                    grList.DataSource = listTK;
                }
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThucHien_Click(object sender, EventArgs e)
        {
            btnThucHien.Enabled = false;
            btnDong.Enabled = false;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private void DoWork(object obj)
        {
            try
            {

                SetError(string.Empty);
                Janus.Windows.GridEX.GridEXRow[] listChecked = grList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length;i++ )
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    KDT_VNACC_ToKhaiMauDich tkmdVNACCS = (KDT_VNACC_ToKhaiMauDich)item.DataRow;
                    try
                    {
                        //duydp 03/12/2015 Cập nhật trạng thái TK VNACCS SET  Templ_1 = 1 là tờ khai đã cập nhật thanh khoản .
                        tkmdVNACCS.Templ_1 = "1";
                        tkmdVNACCS.Update();
                        Company.GC.BLL.VNACC.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(tkmdVNACCS, GlobalSettings.MA_DON_VI != "4000395355");
                    }
                    catch (System.Exception ex)
                    {
                        LocalLogger.Instance().WriteMessage(ex);
                        LocalLogger.Instance().WriteMessage(new Exception("Loi tai to khai " + tkmdVNACCS.SoToKhai));
                    }
                    SetProcessBar((i * 100 / listChecked.Length));
                }
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate 
                        { 
                            loadData(); 
                            btnThucHien.Enabled = true; btnDong.Enabled = true;
                            
                        }));
                }
                else
                {
                    loadData();
                    btnThucHien.Enabled = true; btnDong.Enabled = true;
                }
                SetProcessBar(0);
                SetError("Hoàn thành");
                
            }

            catch (System.Exception ex)
            {
                SetError(string.Empty);
                LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
            {
                switch (e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString())
                {
                    case "1":
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng xanh";
                        break;
                    case "2":
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng vàng";
                        break;
                    case "3":
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng đỏ";
                        break;
                }
            }
        }

        private void checkAMAIsUpdate_CheckedChanged(object sender, EventArgs e)
        {
            loadData();
        }

        private void checkVNACCS_CheckedChanged(object sender, EventArgs e)
        {
            List<KDT_VNACC_ToKhaiMauDich> listTK = KDT_VNACC_ToKhaiMauDich.SelectCollectionGCDaVaChuaThanhKhoan(true,NamDangKy);
            grList.DataSource = listTK;
        }

        private void checkVNACCSValid_CheckedChanged(object sender, EventArgs e)
        {
            loadData();
        }

    }
}
