using System;
using System.Data;
using System.Windows.Forms;
using Company.KD.BLL.DuLieuChuan;

namespace Company.Interface.Controls
{
    public partial class CuaKhauHControl : UserControl
    {
        public CuaKhauHControl()
        {
            this.InitializeComponent();
        }

        public string Ma
        {
            set 
            {
                this.txtID.Text = value;
                this.cbList.Value = this.txtID.Text.PadRight(4);
            }
            get { return this.txtID.Text; }
        }
        public bool ReadOnly
        {
            set
            {
                this.txtID.ReadOnly = value;
                this.cbList.ReadOnly = value;
            }
            get { return this.cbList.ReadOnly; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.txtID.VisualStyleManager = value;
                this.cbList.VisualStyleManager = value;
            }
            get
            {
                return this.txtID.VisualStyleManager;
            }
        }
        public string ErrorMessage
        {
            get { return this.rfvTen.ErrorMessage; }
            set { this.rfvTen.ErrorMessage = value; }
        }

        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);

        private void loadData()
        {
            if (this.txtID.Text.Trim().Length == 0) this.txtID.Text = GlobalSettings.CUA_KHAU;
            DataTable dt = CuaKhau.SelectAll().Tables[0];
            dtCuaKhau.Rows.Clear();
            foreach (DataRow row in dt.Rows)
            {
                this.dtCuaKhau.ImportRow(row);
            }
            this.cbList.Value = this.txtID.Text.PadRight(4);
        }

        private void txtMaLoaiHinh_Leave(object sender, EventArgs e)
        {
            this.txtID.Text = this.txtID.Text.Trim();
            this.cbList.Value = this.txtID.Text.PadRight(4);
        }

        private void CuaKhauControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {                
                this.loadData();
            }
        }

        private void cbList_ValueChanged(object sender, EventArgs e)
        {
            this.txtID.Text = this.cbList.Value.ToString().Trim();
            if (this.ValueChanged != null) this.ValueChanged(sender, e);
        }
    }
}