﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class MaHSForm : Company.Interface.BaseForm
    {
        DataSet ds = new DataSet();
        private static string nhom;
        private static string pn1;
        private static string pn2;
        private static string pn3;
        public MaHSForm()
        {
            InitializeComponent();
        }

        private void MaHSForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            ds = MaHS.GetAllMaHS();
            dgList.DataSource = ds;
            dgList.DataMember = ds.Tables[0].TableName;
        }

        private bool checkMaHS(string maHS)
        {
            if (ds.Tables[0].Select(" HS10SO = '" + maHS.Trim() + "'").Length == 0) return false;
            else
                return true;
        }

        private void dgList_UpdatingCell(object sender, Janus.Windows.GridEX.UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "HS10So")
            {
                string s = e.Value.ToString();

                if (s.Length < 8)
                {
                    MLMessages("Mã HS này độ dài không hợp lệ. Độ dài ít nhất phải >= 8.", "MSG_PUB10", "", false);
                    e.Cancel = true;
                    return;
                }

                nhom = s.Substring(0, 4);
                pn1 = s.Substring(4, 2);
                pn2 = s.Substring(6, 2);
                pn3 = s.Length == 10 ? s.Substring(8, 2) : "00";
                if (checkMaHS(s))
                {
                    // ShowMessage("Mã nước này đã có.", false);
                    MLMessages("Mã HS này đã có.", "MSG_PUB10", "", false);
                    e.Cancel = true;
                }
                if (s.Trim().Length == 0)
                {
                    // ShowMessage("Mã nước không được rỗng.", false);
                    MLMessages("Mã HS không được rỗng.", "MSG_CAL01", "", false);
                    e.Cancel = true;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region GetChanges

                //Updated by Hungtq, 10/08/2012.
                if (ds.Tables[0].GetChanges() != null)
                {
                    DataRow[] rows;

                    foreach (DataRow dr in ds.Tables[0].GetChanges().Rows)
                    {
                        if (dr.RowState == DataRowState.Added)
                        {
                            rows = ds.Tables[0].Select(" HS10SO = '" + dr["HS10SO"].ToString().Trim() + "'");

                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i]["DateCreated"] = System.DateTime.Now;
                                rows[i]["DateModified"] = System.DateTime.Now;
                            }
                        }
                        else if (dr.RowState == DataRowState.Modified)
                        {
                            rows = ds.Tables[0].Select(" HS10SO= '" + dr["HS10SO"].ToString().Trim() + "'");

                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i]["DateModified"] = System.DateTime.Now;
                            }
                        }
                    }
                }

                #endregion

                MaHS.Update(ds, nhom, pn1, pn2, pn3);
                MLMessages("Lưu thành công", "MSG_SAV02", "", false);
            }
            catch (Exception ex)
            {
                MLMessages("Lưu không thành công", "MSG_SAV01", "", false);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (ShowMessage(setText("Bạn có muốn xóa mã Biểu thuế (HS) này không?", "Do you want to delete ?"), true) != "Yes") e.Cancel = true;
        }
    }
}

