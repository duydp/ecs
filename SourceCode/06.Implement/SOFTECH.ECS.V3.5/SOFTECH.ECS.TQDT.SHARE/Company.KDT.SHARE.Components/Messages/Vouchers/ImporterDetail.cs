﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.CSSX;


namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class ImporterDetail
    {
        /// <summary>
        /// Địa chỉ
        /// </summary>
        [XmlElement("address")]
        public string Address { get; set; }

        /// <summary>
        /// Địa chỉ trụ sở chính (Loại)
        /// </summary>
        [XmlElement("addressType")]
        public string AddressType { get; set; }

        /// <summary>
        /// Nước đầu tư
        /// </summary>
        [XmlElement("investmentCountry")]
        public string InvestmentCountry { get; set; }

        /// <summary>
        /// Ngành nghề sản xuất
        /// </summary>
        [XmlElement("industryProduction")]
        public string IndustryProduction { get; set; }

        /// <summary>
        /// Ngày kết thúc năm tài chính
        /// </summary>
        [XmlElement("dateFinanceYear")]
        public string DateFinanceYear { get; set; }

        /// <summary>
        /// Loại hình doanh nghiệp
        /// </summary>
        [XmlElement("typeOfBusiness")]
        public string TypeOfBusiness { get; set; }

        /// <summary>
        /// Thông tin Doanh nghiệp XNK trước khi thay đổi
        /// </summary>
        [XmlElement("OldImporter")]
        public OldImporter OldImporter { get; set; }

        /// <summary>
        /// Chủ tịch Hội đồng quản trị (hoặc Chủ tịch Hội đồng thành viên)
        /// </summary>
        [XmlElement("chairmanImporter")]
        public ChairmanImporter ChairmanImporter { get; set; }

        /// <summary>
        /// Tổng giám đốc (hoặc Giám đốc):
        /// </summary>
        [XmlElement("generalDirector")]
        public GeneralDirector GeneralDirector { get; set; }

        /// <summary>
        /// Lịch sử kiểm tra cơ sở sản xuất, năng lực sản xuất
        /// </summary>
        [XmlElement("productionInspectionHis")]
        public ProductionInspectionHis ProductionInspectionHis { get; set; }

        /// <summary>
        /// Thông tin các lần kiểm tra
        /// </summary>
        [XmlElement("StorageOfGoods")]
        public StorageOfGoods StorageOfGoods { get; set; }

        /// <summary>
        /// Thông tin các lần kiểm tra
        /// </summary>
        [XmlElement("ContentInspections")]
        public ContentInspections ContentInspections { get; set; }
    }
}
