﻿

using System;
using System.Windows.Forms;
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
#if KD_V3 || KD_V4
using Company.Interface.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif GC_V3 || GC_V4
using Company.Interface.GC;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
#endif
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;
using System.Threading;
using KetQuaXuLy = Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3.KetQuaXuLy;
using Company.KDT.SHARE.QuanLyChungTu;




namespace Company.Interface.DongBoDuLieu
{
    public partial class DongBoDuLieuForm : BaseForm
    {
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        private ToKhaiMauDichCollection tkmdCollectionSelected = new ToKhaiMauDichCollection();
        private ToKhaiMauDich tkMD = new ToKhaiMauDich();
        
#if KD_V3 || KD_V4
        private string PhanHe = "KD";
#elif SXXK_V3 || SXXK_V4
        private string PhanHe = "SXXK";
#elif GC_V3 || GC_V4
        private string PhanHe = "GC";
#endif
        private ISyncData myService = WebService.SyncService();
        public string NhomLoaiHinh = "";
        private bool IsSend;
        private bool IsView;
        delegate void SetProgessBarCallback(int percent);
        private List<Detail> chitietCO;
        private List<Detail> ChiTietSelected = new List<Detail>();
        private bool Isget;
        public DongBoDuLieuForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------


        private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
        {
            //txtNamTiepNhan.Text = DateTime.Now.Year.ToString();
            txtThangDK.Text = DateTime.Now.Month.ToString();
            cbbLoaiToKhai.SelectedIndex = 0;
            ccFromDate.Text = DateTime.Today.ToString();
            ccToDate.Text = DateTime.Today.ToString();
            try
            {
                //txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            if (MainForm.versionHD == 1)
            {
                btnNhan.Enabled = false;
                btnSend.Enabled = true;
            }
            else if (MainForm.versionHD == 3)
            {
                btnNhan.Enabled = true;
                btnSend.Enabled = false;
                txtThangDK.Enabled = false;
                dgList.RootTable.Columns["TrangThaiGui"].Visible = false;
            }
            //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
            //{
            //    uiButton2.Visible = false;
            //}    
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {
            //short namdk = 0;
            //if (txtNamTiepNhan.Text.Trim().Length > 0)
            //    namdk = Convert.ToInt16(txtNamTiepNhan.Text.Trim());
            //try
            //{


            //    WSForm wsForm = new WSForm();
            //    wsForm.ShowDialog(this);
            //    if (!wsForm.IsReady) return;
            //    this.Cursor = Cursors.WaitCursor;
            //    string strSTN = "";
            //    long count=ToKhaiMauDich.DongBoDuLieuHaiQuanAll(donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI, namdk);
            //    if (count > 0)
            //    {
            //        this.Cursor = Cursors.Default;
            //        ShowMessage("Nhận dữ liệu thành công " + count + " tờ khai", false);
            //        this.search();
            //    }
            //    else
            //    {
            //        this.Cursor = Cursors.Default;
            //        ShowMessage("Không có tờ khai nào cả.", false);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    string st=ShowMessage(ex.Message+"\n Có lỗi khi thực hiện. Bạn có muốn đưa vào hàng đợi không", true);
            //    if (st == "Yes")
            //    {
            //        Company.KD.BLL.KDT.HangDoi hd = new Company.KD.BLL.KDT.HangDoi();                   
            //        hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
            //        hd.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
            //        hd.ChucNang = ChucNang.DONG_BO_DU_LIEU;
            //        hd.SoTK = 0;
            //        hd.NamDangKy = namdk;
            //        hd.MaHaiQuan = donViHaiQuanControl1.Ma;                 
            //        MainForm.AddToQueueForm(hd);
            //        MainForm.ShowQueueForm();
            //    }
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
        }
        private ToKhaiMauDich getTKMDByPrimaryKey(int sotk, string mahaiquan, string maloaihinh, int namdk)
        {
            foreach (ToKhaiMauDich tk in this.tkmdCollection)
            {
                if (tk.SoToKhai == sotk && tk.MaHaiQuan.Trim() == mahaiquan && tk.NamDK == namdk && tk.MaLoaiHinh == maloaihinh)
                    return tk;
            }
            return null;
        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {



        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void searchDaiLy()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "1 = 1";
            where += string.Format(" AND MaHaiQuan = '{0}' and MaDoanhNghiep='{1}'", donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI);
            where += " AND MaLoaiHinh LIKE '" + cbbLoaiToKhai.SelectedValue.ToString() + "%'";
            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoToKhai = " + txtSoTiepNhan.Value;
            }

            //if (txtNamTiepNhan.TextLength > 0)
            //{
            //    where += " AND NamDK = " + txtNamTiepNhan.Value;
            //}
            if (ccFromDate.Value < ccToDate.Value)
            {
                where += "AND (NgayDangKy BETWEEN '" + ccFromDate.Value.ToString("yyyy-MM-dd") + "' AND '" + ccToDate.Value.ToString("yyyy-MM-dd") + "')";
            }
            if (Convert.ToInt16(txtThangDK.Value) > 0)
            {
                where += " AND (month(NgayDangKy)=" + Convert.ToInt16(txtThangDK.Value) + ")";
            }
            if (GlobalSettings.MA_DON_VI != "0400101556")
            {
                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                        where += " AND TenChuHang LIKE '%" + txtTenChuHang.Text + "%'";
                }
                else
                {
                    where += " AND TenChuHang LIKE ''";
                }
            }
            else
            {
                label5.Text = "Tên khách hàng";
                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    where += " AND TenDonViDoiTac LIKE '%" + txtTenChuHang.Text + "%'";
                }
            }
            if (this.NhomLoaiHinh != "") where += " AND MaLoaiHinh LIKE '%" + this.NhomLoaiHinh + "%'";
            where += "AND TrangThaiXuLy = 1 AND HUONGDAN<>''";
            // Thực hiện tìm kiếm.            
            this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "");
            dgList.DataSource = this.tkmdCollection;
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (MainForm.versionHD == 1)
            {
                this.searchDaiLy();
            }
            else if (MainForm.versionHD == 3)
            {
                this.searchDoanhNghiep();
            }

        }

        private void searchDoanhNghiep()
        {
            try
            {
                setProgessBar(0);
                uiStatusBar.Panels["barTime"].Text = "00 : 00 : 00";
                if (btnSearch.Text == "&Dừng")
                {
                    bw.CancelAsync();
                    timer1.Stop();
                }
                else if (btnSearch.Text == "&Tìm kiếm")
                {
                    //if (!Login())
                    //    return;

                    if (bw.IsBusy != true)
                    {
                        IsView = true;
                        IsSend = false;
                        Isget = false;
                        timer1.Enabled = true;
                        btnSearch.Text = "&Dừng";
                        btnSearch.Enabled = true;
                        btnClose.Enabled = false;
                        bw.RunWorkerAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return;
            }

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {

                DateTime time = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + donViHaiQuanControl1.Ma + "/" + time.Year.ToString();
                if (e.Row.Cells["ActionStatus"].Text == "500")
                {
                    e.Row.Cells["TrangThaiGui"].Text = "Đã gửi";
                }
                else
                    e.Row.Cells["TrangThaiGui"].Text = "Chưa gửi";
            }
        }

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {

        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dgList_DropDown(object sender, ColumnActionEventArgs e)
        {

        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            bool overwrite = false;
            //Lay danh sach To khai duoc check tren luoi
            tkmdCollectionSelected = new ToKhaiMauDichCollection();
            for (int i = 0; i < dgList.RowCount; i++)
            {
                if (dgList.GetRow(i).RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    if (Convert.ToBoolean(dgList.GetRow(i).Cells["check"].Value))
                    {
                        ToKhaiMauDich tokhaimaudich = (ToKhaiMauDich)dgList.GetRow(i).DataRow;
                        if (tokhaimaudich.ActionStatus == 500)
                            if (!overwrite)
                            {
                                string thongbao = tokhaimaudich.SoToKhai.ToString() + "/" + tokhaimaudich.MaHaiQuan;
                                overwrite = ShowMessageTQDT("Tờ khai số " + thongbao + " đã được gửi lên. Bạn có muốn ghi đè thông tin các tờ khai đã gửi không ?", true) == "Yes";
                                if (!overwrite) return;
                            }
                        tkmdCollectionSelected.Add(tokhaimaudich);
                    }
                }
            }
            //Hien thi thong bao neu khong co to khai nao duoc chon
            if (tkmdCollectionSelected.Count == 0)
            {
                ShowMessage("Bạn chưa chọn tờ khai.", false);
                return;
            }
            try
            {
                setProgessBar(0);
                uiStatusBar.Panels["barTime"].Text = "00 : 00 : 00";

                if (btnSend.Text == "&Dừng")
                {
                    bw.CancelAsync();
                    timer1.Stop();
                }
                else if (btnSend.Text == "&Gửi dữ liệu")
                {
                    //if (!Login())
                    //    return;

                    if (bw.IsBusy != true)
                    {
                        timer1.Enabled = true;
                        IsSend = true;
                        btnSend.Text = "&Dừng";
                        btnSend.Enabled = true;
                        btnClose.Enabled = false;
                        bw.RunWorkerAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return;
            }
        }

        private void dgList_EditingCell(object sender, EditingCellEventArgs e)
        {
            //GridEXRow row = dgList.GetRow();
            //if (row.Cells["TrangThaiThanhKhoan"].Value.ToString() == "S")
            //{
            //    e.Cancel = true;
            //}
        }

        private void dgList_CellUpdated(object sender, ColumnActionEventArgs e)
        {
            //GridEXRow row = dgList.GetRow();
            //if (Convert.ToDateTime(row.Cells["NGAY_THN_THX"].Value).Year > 1900)
            //{
            //    dgList.GetRow().Cells["TrangThaiThanhKhoan"].Value = "K";
            //}
            btnSend.Enabled = true;
        }
        private string GenMessagesSend(ToKhaiMauDich tkmd, string userID)
        {
            string content = string.Empty;
            string msgSend;
            tkmd.Load();
            tkmd.LoadCO();
            tkmd.LoadHMDCollection();
            tkmd.LoadListGiayNopTiens();
            tkmd.LoadTKTGCollection();
            tkmd.LoadToKhaiTriGiaPP23();
            tkmd.LoadChungTuHaiQuan();
            tkmd.LoadChungTuKem();
            try
            {
                SyncDaTa xml = new SyncDaTa() 
                {
                    Type = "TKMD",
                    Declaration = new List<SyncDaTaDetail>()
                };
                xml.Declaration.Add(new SyncDaTaDetail 
                {
                    Issuer = userID,
                    Business = tkmd.MaDoanhNghiep,
                    Reference = tkmd.GUIDSTR,
                    CustomsReference = tkmd.SoTiepNhan.ToString(),
                    Acceptance = tkmd.NgayDangKy.ToString(),
                    Number = tkmd.SoToKhai.ToString(),
                    NatureOfTransaction = tkmd.MaLoaiHinh,
                    DeclarationOffice = tkmd.MaHaiQuan,
                    StreamlineContent = tkmd.HUONGDAN,
                    Type = PhanHe,

                });
                content = Helpers.Serializer(tkmd);
                content = Helpers.ConvertToBase64(content);
                xml.Content = new Content { Text = content };
                msgSend = Helpers.Serializer(xml);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw (ex);
            }
            return msgSend;
        }
        private void bw_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string user = "";
            string pw = "";
            #region gửi tờ khai
            if (IsSend)
            {
                string error = string.Empty;
                if (string.IsNullOrEmpty(GlobalSettings.USERNAME_DONGBO) || string.IsNullOrEmpty(GlobalSettings.PASSWOR_DONGBO))
                {
                    WSDBDLForm f = new WSDBDLForm();
                    f.ShowDialog();
                    if (!f.IsReady) return;
                    user = f.txtMaDoanhNghiep.Text;
                    pw = f.txtMatKhau.Text;
                }
                else
                {
                    user = GlobalSettings.USERNAME_DONGBO;
                    pw = GlobalSettings.PASSWOR_DONGBO;
                }
                pw = Helpers.GetMD5Value(pw);
                for (int i = 0; i < tkmdCollectionSelected.Count; i++)
                {
                    if (!e.Cancel)
                    {
                        #region Phần giành riêng cho GC_V3
#if GC_V3
                        #region Hợp đồng
                        Setlb("Đang đồng bộ Hợp Đồng");
                        HopDong HD = HopDong.Load(tkmdCollectionSelected[i].IDHopDong);
                        if (HD == null)
                        {
                            ShowThongBao("Không tìm thấy hợp đồng của tờ khai ", false);
                            return;
                        }
                        Status sthd = Status.Load(HD.GUIDSTR);
                        if (sthd == null || sthd.MSG_STATUS == 0)
                        {
                            try
                            {
                                string msgHD = Helpers.Serializer(HD);
                                msgHD = Helpers.ConvertFromBase64(msgHD);
                                SyncDaTa syn = new SyncDaTa();
                                syn.Content = new Content { Text = msgHD };
                                syn.Declaration = new List<SyncDaTaDetail>();
                                syn.Declaration.Add(new SyncDaTaDetail 
                                {
                                    Business = GlobalSettings.MA_DON_VI,
                                    Reference = HD.GUIDSTR,
                                    Type = PhanHe,
                                });
                                syn.Type = "HDGC";
                                string msgSendHD = Helpers.Serializer(syn);

                            }
                            catch (System.Exception ex)
                            {
                               
                            }
                            string HDfeedback = string.Empty; //myService.SendHopDongGC();
                            if (HDfeedback != null)
                            {
                                if (HDfeedback == "MK")
                                {
                                    ShowThongBao("Sai user hoặc mật khẩu", false);
                                    GlobalSettings.USERNAME_DONGBO = string.Empty;
                                    GlobalSettings.PASSWOR_DONGBO = string.Empty;
                                    return;
                                }
                                else
                                {
                                    SaveLogError(tkmdCollectionSelected[i], "Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng: " + HDfeedback);
                                    if (ShowThongBao("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng: " + HDfeedback, true) != "Yes")
                                        return;
                                }
                            }
                            else
                            {
                                sthd.GUIDSTR = HD.GUIDSTR;
                                sthd.MSG_STATUS = 1;
                                sthd.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.HOP_DONG_GIA_CONG);
                                sthd.CREATE_TIME = DateTime.Now;
                                sthd.InsertUpdate();
                            }
                        }
                        #endregion
                        #region Phụ kiện đăng ký
                        Setlb("Đang đồng bộ phụ kiện hợp đồng");
                        PhuKienDangKy _pkdk = new PhuKienDangKy();
                        _pkdk.HopDong_ID = HD.ID;
                        PhuKienDangKyCollection pkDKCO = _pkdk.SelectCollectionBy_HopDong_ID(null);
                        for (int j = 0; j < pkDKCO.Count; j++)
                        {
                            if (pkDKCO[i].TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                Status stpk = Status.Load(pkDKCO[i].GUIDSTR);
                                if (sthd == null || sthd.MSG_STATUS == 0)
                                {
                                    try
                                    {
                                        string msgPK = Helpers.Serializer(pkDKCO[i]);
                                        msgPK = Helpers.ConvertFromBase64(msgPK);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        SaveLogError(tkmdCollectionSelected[i], "Lỗi xảy ra khi đồng bộ dữ liệu phụ kiện số " + pkDKCO[i].SoPhuKien + ":" + ex.Message);
                                        if (ShowThongBao("Lỗi xảy ra khi đồng bộ dữ liệu phụ kiện số " + pkDKCO[i].SoPhuKien + ":" + ex.Message + ". Bạn có muốn tiếp tục", true) != "Yes")
                                            return;
                                    }
                                    string PKfeedback = string.Empty;// myService.SendPhuKienGC();
                                    if (PKfeedback != null)
                                    {

                                        SaveLogError(tkmdCollectionSelected[i], "Lỗi xảy ra khi đồng bộ dữ liệu phụ kiện số " + pkDKCO[i].SoPhuKien + ":" + PKfeedback);
                                        if (ShowThongBao("Lỗi xảy ra khi đồng bộ dữ liệu phụ kiện số " + pkDKCO[i].SoPhuKien + ":" + PKfeedback + ". Bạn có muốn tiếp tục", true) != "Yes")
                                            return;

                                    }
                                    else
                                    {
                                        sthd.GUIDSTR = pkDKCO[i].GUIDSTR;
                                        sthd.MSG_STATUS = 1;
                                        sthd.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.GC_PHU_KIEN_HOP_DONG);
                                        sthd.CREATE_TIME = DateTime.Now;
                                        sthd.InsertUpdate();
                                    }
                                }
                                bw.ReportProgress((int)(((double)(j + 1) / (double)pkDKCO.Count) * 100));
                            }
                            
                        }
                        #endregion
                        #region Định mức của sản phẩm trong tờ khai
                        if (tkmdCollectionSelected[i].LoaiHangHoa == "S")
                        {
                            //Setlb("Đang đồng bộ định mức");
                            //foreach (Company.KD.BLL.KDT.HangMauDich hmd in tkmdCollectionSelected[i].HMDCollection)
                            //{
                            //}                    
                        }
                        #endregion
#endif
                        
                        #endregion
                        string msgFeedback = string.Empty;
                        string TenToKhai = tkmdCollectionSelected[i].SoToKhai + "/" + tkmdCollectionSelected[i].MaLoaiHinh + "/" + tkmdCollectionSelected[i].MaHaiQuan;
                        Setlb("Đang gửi " + TenToKhai);
                        string xml = GenMessagesSend(tkmdCollectionSelected[i],user);
                        if (!string.IsNullOrEmpty(xml))
                        {
                            msgFeedback = myService.SendDaiLy(xml, user, pw);

                            if (!string.IsNullOrEmpty(msgFeedback))
                            {
                                if (msgFeedback == "MK")
                                {

                                    ShowThongBao("Sai user hoặc mật khẩu", false);
                                    GlobalSettings.USERNAME_DONGBO = string.Empty;
                                    GlobalSettings.PASSWOR_DONGBO = string.Empty;
                                    return;
                                }
                                error = "Tờ khai số " + TenToKhai + "gặp lỗi: " + msgFeedback + ".Bạn có muốn tiếp tục? ";
                                SaveLogError(tkmdCollectionSelected[i], error);
                                if (ShowThongBao(error, true) != "Yes")
                                    return;
                            }
                            else
                            {
                                tkmdCollectionSelected[i].ActionStatus = 500;
                                tkmdCollectionSelected[i].Update();
                            }
                        }
                        else
                        {
                            if (ShowThongBao("Không tạo được thông tin tờ khai " + TenToKhai + ". Bạn có muốn tiếp tục", true) != "Yes")
                                return;
                        }
                    }
                    bw.ReportProgress((int)(((double)(i + 1) / (double)tkmdCollection.Count) * 100));
                }
            }
            #endregion
            #region tìm kiếm tờ khai
            else if (IsView)
            {
                if (string.IsNullOrEmpty(GlobalSettings.USERNAME_DONGBO) || string.IsNullOrEmpty(GlobalSettings.PASSWOR_DONGBO))
                {
                    WSDBDLForm f = new WSDBDLForm();
                    f.ShowDialog();
                    if (!f.IsReady) return;
                    user = f.txtMaDoanhNghiep.Text;
                    pw = f.txtMatKhau.Text;
                }
                else
                {
                    user = GlobalSettings.USERNAME_DONGBO;
                    pw = GlobalSettings.PASSWOR_DONGBO;
                }
                pw = Helpers.GetMD5Value(pw);
                Setlb("Đang tìm kiếm...");
                bw.ReportProgress(30);
                SyncDaTa syncdata = new SyncDaTa();
                syncdata.issuer = GlobalSettings.MA_DON_VI;
                syncdata.FromDate = ccFromDate.Value.ToString("yyyy-MM-dd");
                syncdata.ToDate = ccToDate.Value.ToString("yyyy-MM-dd");
                syncdata.Type = PhanHe;
                string msg = Helpers.ConvertToBase64(Helpers.Serializer(syncdata));
                bw.ReportProgress(60);
                string msgFeedback = string.Empty;
                msgFeedback = myService.ReceivesViews(user, pw, msg);
                bw.ReportProgress(90);
                if (msgFeedback == "MK")
                {
                    ShowThongBao("Sai tên đăng nhập hoặc mật khẩu", false);
                    GlobalSettings.USERNAME_DONGBO = string.Empty;
                    GlobalSettings.PASSWOR_DONGBO = string.Empty;
                    return;
                }
                else
                {
                    if (string.IsNullOrEmpty(msgFeedback))
                    {
                        ShowThongBao("Không tìm thấy tờ khai", false);
                        return;
                    }
                    else
                    {
                        SyncDaTa feedback = new SyncDaTa();
                        try
                        {
                            feedback = Helpers.Deserialize<SyncDaTa>(msgFeedback);
                        }
                        catch (System.Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            SaveLogError(msgFeedback);
                            ShowThongBao("Có lỗi trả về từ server: " + msgFeedback, false);

                            return;
                        }
                        bw.ReportProgress(99);
                        chitietCO = MapperDBDL.MapperView(feedback.Declaration);
                        if (InvokeRequired)
                            this.Invoke(new MethodInvoker(delegate
                            {
                                dgList.DataSource = chitietCO;
                                dgList.Refetch();
                            }));

                    }
                }
            }
            #endregion
            #region Nhận dữ liệu
            else if (Isget)
            {
                if (string.IsNullOrEmpty(GlobalSettings.USERNAME_DONGBO) || string.IsNullOrEmpty(GlobalSettings.PASSWOR_DONGBO))
                {
                    WSDBDLForm f = new WSDBDLForm();
                    f.ShowDialog();
                    if (!f.IsReady) return;
                    user = f.txtMaDoanhNghiep.Text;
                    pw = f.txtMatKhau.Text;
                }
                else
                {
                    user = GlobalSettings.USERNAME_DONGBO;
                    pw = GlobalSettings.PASSWOR_DONGBO;
                }
                pw = Helpers.GetMD5Value(pw);
                string msgFeedback = string.Empty;
                string TenToKhai = string.Empty;
                for (int i = 0; i < ChiTietSelected.Count; i++)
                {
                    try
                    {
                        TenToKhai = ChiTietSelected[i].SoToKhai + "/" + ChiTietSelected[i].MaLoaiHinh + "/" + ChiTietSelected[i].MaHaiQuan;
                        Setlb("Đang đồng bộ tờ khai: " + TenToKhai);
                        msgFeedback = myService.SendDoanhNghiep(ChiTietSelected[i].GUIDSTR, user, pw);
                        if (msgFeedback == "MK")
                        {
                            ShowThongBao("Sai tên đăng nhập hoặc mật khẩu ", false);
                            GlobalSettings.USERNAME_DONGBO = string.Empty;
                            GlobalSettings.PASSWOR_DONGBO = string.Empty;
                        }
                        else
                        {
                            bool isContinue = false;
                            SyncDaTa xml = new SyncDaTa();
                            try
                            {
                                xml = Helpers.Deserialize<SyncDaTa>(msgFeedback);
                            }
                            catch (System.Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                                if (ShowThongBao("lỗi khi nhận dữ liệu tờ khai số " + TenToKhai + ":" + msgFeedback + ".Bạn có muốn tiếp tục? ", true) != "Yes")
                                    return;
                                else
                                    isContinue = true;
                            }
                            if (!isContinue)
                            {
                                string content = Helpers.ConvertFromBase64(xml.Content.Text);
                                ToKhaiMauDich tk = Helpers.Deserialize<ToKhaiMauDich>(content);
                                string error = tk.InsertFullFromISyncDaTa();
                                if (!string.IsNullOrEmpty(error))
                                {
                                    SaveLogError(ChiTietSelected[i], error);
                                    if (ShowThongBao("lỗi khi cập nhật dữ liệu tờ khai số " + TenToKhai + ":" + error + ".Bạn có muốn tiếp tục? ", true) != "Yes")
                                        return;
                                }
                                Thread.Sleep(1000);
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        SaveLogError(ChiTietSelected[i],ex.Message);
                        if (ShowThongBao("lỗi khi nhận dữ liệu tờ khai số " + TenToKhai + ":" + ex.Message + ".Bạn có muốn tiếp tục? ", true) != "Yes")
                            return;
                    }
                    bw.ReportProgress((int)(((double)(i + 1) / (double)ChiTietSelected.Count) * 100));
                }

            }
            #endregion

        }

        private void bw_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            try
            {
                //this.uiStatusBar.Panels["barProgress"].ProgressBarValue = e.ProgressPercentage;

                //this.uiStatusBar.Panels["barProgress"].Text = e.ProgressPercentage.ToString() + " %";

                setProgessBar(e.ProgressPercentage);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void setProgessBar(int percent)
        {
            if (uiStatusBar.InvokeRequired)
            {
                SetProgessBarCallback d = new SetProgessBarCallback(setProgessBar);
                this.Invoke(d, new object[] { percent });
            }
            else
            {
                uiStatusBar.Panels["barProgress"].ProgressBarValue = percent;

                uiStatusBar.Panels["barProgress"].Text = percent + " %";
            }
        }

        private void bw_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {

            try
            {
                btnClose.Enabled = true;
                if (IsSend)
                {
                    timer1.Stop();

                    btnSend.Text = "&Gửi dữ liệu";
                    //Neu khong xay ra loi nao
                    if (e.Result == null)
                    {
                        ShowMessageTQDT("Đã hoàn thành!", false);
                    }
                    IsSend = false;
                    //if (GlobalSettings.SOTOKHAI_DONGBO > 0 && GlobalSettings.MA_DON_VI != null)
                    //{
                    //    GlobalSettings.ISKHAIBAO = (GlobalSettings.SOTOKHAI_DONGBO >= ToKhaiMauDich.SelectCountSoTKThongQuan(GlobalSettings.MA_DON_VI));
                    //}
                    btnSearch_Click(null, EventArgs.Empty);
                }
                else if (IsView)
                {
                    btnSearch.Text = "&Tìm kiếm";
                    IsView = false;
                }
                else if (Isget)
                {
                    btnNhan.Text = "&Nhận dữ liệu";
                    Isget = false;
                    if (e.Result == null)
                    {
                        ShowMessageTQDT("Đã hoàn thành!", false);
                    }
                        searchDoanhNghiep();
                }
                uiStatusBar.Panels["barProgress"].ProgressBarValue = 0;
                Setlb("******");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new ArgumentNullException(ex.Message);
            }
        }
        delegate void Setlable(string msg);
        private void Setlb(string msg)
        {
            timer1.Enabled = true;

            if (lbTkDoing.InvokeRequired)
            {
                Setlable d = new Setlable(Setlb);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                lbTkDoing.Text = msg;
            }
        }
        private string ShowThongBao(string thongbao, bool ShowYesNoButton)
        {
            string show = string.Empty;
            if (InvokeRequired)
                this.Invoke(new MethodInvoker(delegate
                {
                    show = ShowMessage(thongbao, ShowYesNoButton);
                }));
            return show;
        }

        private void btnNhan_Click(object sender, EventArgs e)
        {
            // Đưa các tờ khai được chọn vào danh sách
            ChiTietSelected = new List<Detail>();
            for (int i = 0; i < dgList.RowCount; i++)
            {
                if (Convert.ToBoolean(dgList.GetRow(i).Cells["check"].Value))
                {
                    Detail chitiet = (Detail)dgList.GetRow(i).DataRow;
                    ChiTietSelected.Add(chitiet);
                }
            }
            //Hien thi thong bao neu khong co to khai nao duoc chon
            if (ChiTietSelected.Count == 0)
            {
                ShowMessage("Bạn chưa chọn tờ khai.", false);
                return;
            }
            try
            {
                setProgessBar(0);
                uiStatusBar.Panels["barTime"].Text = "00 : 00 : 00";

                if (btnNhan.Text == "&Dừng")
                {
                    bw.CancelAsync();
                    timer1.Stop();
                }
                else if (btnNhan.Text == "&Nhận dữ liệu")
                {
                    //if (!Login())
                    //    return;

                    if (bw.IsBusy != true)
                    {
                        timer1.Enabled = true;
                        Isget = true;
                        IsSend = false;
                        IsView = false;
                        btnNhan.Text = "&Dừng";
                        btnNhan.Enabled = true;
                        btnClose.Enabled = false;
                        bw.RunWorkerAsync();

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new ArgumentNullException(ex.Message);
            }

        }
        
        #region Save log
        private void SaveLogError(ToKhaiMauDich tkmd, string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = tkmd.SoToKhai;
            logError.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.GUIDSTR = tkmd.GUIDSTR;
            logError.TrangThai = 1;
            logError.InsertUpdate();
        }
        private void SaveLogError(string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.GhiChu = error;
            logError.NgayDongBo = DateTime.Now;
            logError.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            logError.SoToKhai = 0;
            logError.TrangThai = 0;
            logError.InsertUpdate();
        }
        private void SaveLogError(Detail chitiet, string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = chitiet.SoToKhai;
            logError.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.GUIDSTR = chitiet.GUIDSTR;
            logError.TrangThai = 2;
            logError.InsertUpdate();
        }
        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            KetQuaXuLyForm f = new KetQuaXuLyForm();
            f.ShowDialog();
        }
    }
}