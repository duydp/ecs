if NOT EXISTS (select * from t_VNACC_Category_Common where referenceDB='A521' and Code ='VK200')
insert into t_VNACC_Category_Common VALUES ('A521','VK200',N'Phân bón','','','','','')
if NOT EXISTS (select * from t_VNACC_Category_Common where referenceDB='A521' and Code ='VK210')
insert into t_VNACC_Category_Common VALUES ('A521','VK210',N'Máy móc, thiết bị chuyên dùng phục vụ cho sản xuất nông nghiệp','','','','','')
if NOT EXISTS (select * from t_VNACC_Category_Common where referenceDB='A521' and Code ='VK220')
insert into t_VNACC_Category_Common VALUES ('A521','VK220',N'Thức ăn gia súc, gia cầm và thức ăn cho vật nuôi khác','','','','','')
if NOT EXISTS (select * from t_VNACC_Category_Common where referenceDB='A521' and Code ='VK230')
insert into t_VNACC_Category_Common VALUES ('A521','VK230',N'Tàu đánh bắt xa bờ','','','','','')
if NOT EXISTS (select * from t_VNACC_Category_Common where referenceDB='A521' and Code ='VK900')
insert into t_VNACC_Category_Common VALUES ('A521','VK900',N'Hàng hóa khác','','','','','')

Go
update  t_VNACC_Category_Common set Name_VN=N'Quặng để sản xuất phân bón' where referenceDB='A522' and Code ='VB025'
Go
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.7',GETDATE(), N' Cập nhật danh muc ma mien giam và ma ap dung thue GTGT')
END	
