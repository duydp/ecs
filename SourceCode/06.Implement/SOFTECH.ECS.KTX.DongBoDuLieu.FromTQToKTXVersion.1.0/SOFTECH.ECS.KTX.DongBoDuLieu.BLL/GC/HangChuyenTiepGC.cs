﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DongBoDuLieu.BLL.GC
{
    public class HangChuyenTiepGC
    {
        public long ID { set; get; }
        public long Master_ID { set; get; }
        public int SoThuTuHang { set; get; }
        public string MaHang { set; get; }
        public string TenHang { set; get; }
        public string MaHS { set; get; }
        public decimal SoLuong { set; get; }
        public string ID_NuocXX { set; get; }
        public string ID_DVT { set; get; }
        public decimal DonGia { set; get; }
        public decimal TriGia { set; get; }
    }
    public class HangChuyenTiepList : List<HangChuyenTiepGC>
    {

    }
}
