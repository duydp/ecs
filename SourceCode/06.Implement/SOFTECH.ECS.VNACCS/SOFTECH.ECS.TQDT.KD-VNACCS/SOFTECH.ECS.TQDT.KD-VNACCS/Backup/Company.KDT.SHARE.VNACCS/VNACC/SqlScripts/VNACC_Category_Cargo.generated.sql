-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BondedAreaCode varchar(10),
	@UserCode varchar(10),
	@BondedAreaName nvarchar(250),
	@BondedAreaDemarcation char(1),
	@NecessityIndicationOfBondedAreaName int,
	@CustomsOfficeCodeForImportDeclaration varchar(5),
	@CustomsOfficeCodeForExportDeclaration varchar(5),
	@CustomsOfficeCodeForDeclarationOnTransportation varchar(5),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_Cargo]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@BondedAreaCode,
	@UserCode,
	@BondedAreaName,
	@BondedAreaDemarcation,
	@NecessityIndicationOfBondedAreaName,
	@CustomsOfficeCodeForImportDeclaration,
	@CustomsOfficeCodeForExportDeclaration,
	@CustomsOfficeCodeForDeclarationOnTransportation,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BondedAreaCode varchar(10),
	@UserCode varchar(10),
	@BondedAreaName nvarchar(250),
	@BondedAreaDemarcation char(1),
	@NecessityIndicationOfBondedAreaName int,
	@CustomsOfficeCodeForImportDeclaration varchar(5),
	@CustomsOfficeCodeForExportDeclaration varchar(5),
	@CustomsOfficeCodeForDeclarationOnTransportation varchar(5),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Cargo]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[UserCode] = @UserCode,
	[BondedAreaName] = @BondedAreaName,
	[BondedAreaDemarcation] = @BondedAreaDemarcation,
	[NecessityIndicationOfBondedAreaName] = @NecessityIndicationOfBondedAreaName,
	[CustomsOfficeCodeForImportDeclaration] = @CustomsOfficeCodeForImportDeclaration,
	[CustomsOfficeCodeForExportDeclaration] = @CustomsOfficeCodeForExportDeclaration,
	[CustomsOfficeCodeForDeclarationOnTransportation] = @CustomsOfficeCodeForDeclarationOnTransportation,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[BondedAreaCode] = @BondedAreaCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BondedAreaCode varchar(10),
	@UserCode varchar(10),
	@BondedAreaName nvarchar(250),
	@BondedAreaDemarcation char(1),
	@NecessityIndicationOfBondedAreaName int,
	@CustomsOfficeCodeForImportDeclaration varchar(5),
	@CustomsOfficeCodeForExportDeclaration varchar(5),
	@CustomsOfficeCodeForDeclarationOnTransportation varchar(5),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [BondedAreaCode] FROM [dbo].[t_VNACC_Category_Cargo] WHERE [BondedAreaCode] = @BondedAreaCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Cargo] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[UserCode] = @UserCode,
			[BondedAreaName] = @BondedAreaName,
			[BondedAreaDemarcation] = @BondedAreaDemarcation,
			[NecessityIndicationOfBondedAreaName] = @NecessityIndicationOfBondedAreaName,
			[CustomsOfficeCodeForImportDeclaration] = @CustomsOfficeCodeForImportDeclaration,
			[CustomsOfficeCodeForExportDeclaration] = @CustomsOfficeCodeForExportDeclaration,
			[CustomsOfficeCodeForDeclarationOnTransportation] = @CustomsOfficeCodeForDeclarationOnTransportation,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[BondedAreaCode] = @BondedAreaCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_Cargo]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[BondedAreaCode],
			[UserCode],
			[BondedAreaName],
			[BondedAreaDemarcation],
			[NecessityIndicationOfBondedAreaName],
			[CustomsOfficeCodeForImportDeclaration],
			[CustomsOfficeCodeForExportDeclaration],
			[CustomsOfficeCodeForDeclarationOnTransportation],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@BondedAreaCode,
			@UserCode,
			@BondedAreaName,
			@BondedAreaDemarcation,
			@NecessityIndicationOfBondedAreaName,
			@CustomsOfficeCodeForImportDeclaration,
			@CustomsOfficeCodeForExportDeclaration,
			@CustomsOfficeCodeForDeclarationOnTransportation,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Delete]
	@BondedAreaCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Cargo]
WHERE
	[BondedAreaCode] = @BondedAreaCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Cargo] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Load]
	@BondedAreaCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Cargo]
WHERE
	[BondedAreaCode] = @BondedAreaCode
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Cargo] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Cargo]	

GO

