﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class DNCX_HangRa : DeclarationBase
    {
        /// <summary>
        /// Thông tin hàng hóa ra
        /// </summary>
        [XmlElement("OutgoingGoodsItem")]
        public List<Product> Product { get; set; }
    }
}
