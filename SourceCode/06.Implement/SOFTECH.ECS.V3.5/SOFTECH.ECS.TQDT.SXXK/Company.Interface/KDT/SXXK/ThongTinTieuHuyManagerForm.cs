﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class ThongTinTieuHuyManagerForm : BaseForm
    {
        public ThongTinTieuHuyManagerForm()
        {
            InitializeComponent();
        }

        private void ThongTinTieuHuyManagerForm_Load(object sender, EventArgs e)
        {
            try
            {
                ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                ctrCoQuanHaiQuan.BackColor = Color.Transparent;
                cbStatus.SelectedValue = "-1";
                btnTimKiem_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private string GetSearchWhere()
        {
            try
            {
                string where = " 1=1";
                if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                    where += " AND SoTiepNhan = " + txtSoTiepNhan.Text;
                if (!String.IsNullOrEmpty(txtNamTiepNhan.Text))
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Text;
                if (!String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                //if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code))
                //    where += " AND MaHaiQuan = '" + ctrCoQuanHaiQuan.Code + "'";
                return where;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = KDT_VNACCS_ScrapInformation.SelectCollectionDynamic(GetSearchWhere(), "");
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH _" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel(dgList);
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                string TrangThai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (TrangThai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                KDT_VNACCS_ScrapInformation scrapInformation = new KDT_VNACCS_ScrapInformation();
                scrapInformation = (KDT_VNACCS_ScrapInformation)dgList.GetRow().DataRow;
                ThongTinTieuHuySendForm f = new ThongTinTieuHuySendForm();
                f.scrapInformation = scrapInformation;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null,null);
        }
    }
}
