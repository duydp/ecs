﻿
/****** Object:  StoredProcedure [dbo].[p_GC_BC01HSTK_GC_TT117_New]    Script Date: 07/14/2014 14:45:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_BC01HSTK_GC_TT117_New]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_BC01HSTK_GC_TT117_New]
GO


/****** Object:  StoredProcedure [dbo].[p_GC_BC01HSTK_GC_TT117_New]    Script Date: 07/14/2014 14:45:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[p_GC_BC01HSTK_GC_TT117_New]         
 @IDHopDong BIGINT,        
 @MaHaiQuan CHAR(6),        
 @MaDoanhNghiep VARCHAR(14)         
         
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
-- SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS 'STT',    
-- ROW_NUMBER() OVER (ORDER BY  ToKhai.MaNguyenPhuLieu) AS 'STT'  
  
/****** Object:  Table [dbo].[#BC01]    Script Date: 05/03/2013 08:28:14 ******/  
if OBJECT_ID('tempdb..#BC01') is not null drop table #BC01  
  
CREATE TABLE #BC01  
 (  
 STT BIGINT  
 ,ID BIGINT  
 ,MaDoanhNghiep VARCHAR(14)  
 ,MaHaiQuan VARCHAR(8)  
 ,SoToKhai VARCHAR(50)  
 ,NgayDangKy DATETIME  
 ,TenNguyenPhuLieu NVARCHAR(255)  
 ,MaNguyenPhuLieu VARCHAR(35)  
 ,SoLuong DECIMAL(18,6)  
 ,DVT VARCHAR(8)  
 ,TongLuong DECIMAL (18,6)  
 )  
--DECLARE  
-- @IDHopDong BIGINT,        
-- @MaHaiQuan CHAR(6),        
-- @MaDoanhNghiep VARCHAR(14)   
-- SET  
-- @IDHopDong = 721  
-- SET @MaHaiQuan = 'N60C'  
-- SET @MaDoanhNghiep = '4000395355'       
INSERT INTO #BC01  
(  
 STT,  
 ID,  
 MaDoanhNghiep,  
 MaHaiQuan,  
 SoToKhai,  
 NgayDangKy,  
 TenNguyenPhuLieu,  
 MaNguyenPhuLieu,  
 SoLuong,  
 DVT,  
 TongLuong  
)  
 SELECT    ROW_NUMBER() OVER (ORDER BY  ToKhai.MaNguyenPhuLieu) AS 'STT',  ToKhai.IDHopDong AS ID, ToKhai.MaDoanhNghiep, ToKhai.MaHaiQuan, ToKhai.SoToKhai, ToKhai.NgayDangKy, ToKhai.TenHang AS TenNguyenPhuLieu,   
                      ToKhai.MaNguyenPhuLieu, ToKhai.SoLuong, t_HaiQuan_DonViTinh.Ten AS DVT, '0' AS TongLuong  
FROM         (SELECT     CONVERT(varchar(20),  
 (case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_ToKhaiMauDich.SoToKhai) else CONVERT(VARCHAR(10),t_KDT_ToKhaiMauDich.SoToKhai) end )  
) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh,   
                                              t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, t_KDT_ToKhaiMauDich.MaHaiQuan,   
                                              t_KDT_HangMauDich.MaPhu AS MaNguyenPhuLieu, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.DVT_ID,   
                                              t_KDT_ToKhaiMauDich.MaDoanhNghiep  
                       FROM          t_KDT_ToKhaiMauDich INNER JOIN  
                                              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN  
                                              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID  
                       WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong) AND   
                                              (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'N%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N')  
                       UNION  
                       SELECT     CONVERT(varchar(20),   
                       (case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) else  CONVERT(VARCHAR(10),t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) end )  
                       ) + '/' + t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh AS SoToKhai,   
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong,   
                                             t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, t_KDT_GC_HangChuyenTiep.MaHang AS MaNguyenPhuLieu, t_KDT_GC_HangChuyenTiep.SoLuong,   
                                             t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.ID_DVT, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep  
                       FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN  
                                             t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID  
                       WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND   
                                             (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh IN ('PHPLN', 'PHSPN', 'NGC18', 'NGC19', 'PHTBN','NVE23')) AND (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N'))   
                      AS ToKhai INNER JOIN  
                      t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID  
WHERE     (ToKhai.IDHopDong = @IDHOPDONG) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan)  
--ORDER BY ToKhai.MaNguyenPhuLieu, ToKhai.NgayDangKy  
-- Tính lũy kế từng hàng   
UPDATE  temp1 SET temp1.TongLuong = temp2.TongLuong  
FROM #BC01 temp1 INNER JOIN  
(SELECT  
 a.STT,  
 a.MaNguyenPhuLieu,  
 Sum(b.SoLuong) AS TongLuong  
FROM  
 #BC01 a CROSS JOIN #BC01 b  
WHERE b.STT <= a.STT AND b.MaNguyenPhuLieu = a.MaNguyenPhuLieu  
 GROUP BY a.STT,a.MaNguyenPhuLieu) temp2  
ON temp1.STT = temp2.STT --AND Temp1.MaNguyenPhuLieu = Temp2.MaNguyenPhuLieu  
  
  
  
SELECT * FROM #BC01   
DROP TABLE #BC01  

END
GO


/****** Object:  StoredProcedure [dbo].[p_GC_BC02HSTK_GC_TT117_New]    Script Date: 07/14/2014 14:45:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_BC02HSTK_GC_TT117_New]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_BC02HSTK_GC_TT117_New]
GO

/****** Object:  StoredProcedure [dbo].[p_GC_BC02HSTK_GC_TT117_New]    Script Date: 07/14/2014 14:45:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================      
---- Author: Khanhhn  
---- Create date:   02/05/2013    
---- Description:   Báo cáo 02/HSTK-GC. Tính lũy kế theo từng mã hàng    
---- =============================================      
      
CREATE PROCEDURE [dbo].[p_GC_BC02HSTK_GC_TT117_New]       
   
 @IDHopDong BIGINT,      
 @MaHaiQuan CHAR(6),      
 @MaDoanhNghiep VARCHAR(14)       
       
    
AS      
BEGIN      
-- SET NOCOUNT ON added to prevent extra result sets from      
-- interfering with SELECT statements.      
SET NOCOUNT ON;      
 -- p_GC_BC02HSTK_GC_TT117 710, 'C34C','0400101556'      
-- SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',      
    --ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',   
if OBJECT_ID('tempdb..#BC02') is not null drop table #BC02  
CREATE TABLE #BC02  
(STT BIGINT  
 ,SoToKhai VARCHAR(30)  
 ,NgayDangKy DATETIME  
 ,MaHang VARCHAR(35)  
 ,TenHang NVARCHAR(255)  
 ,DVT VARCHAR(5)  
 ,SoLuong FLOAT  
 ,TongCong FLOAT  
 )  
      
INSERT INTO #BC02      
  (STT,SoToKhai,NgayDangKy,MaHang,TenHang,DVT,SoLuong,TongCong)  
SELECT DISTINCT   
                   ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu,ToKhai.NgayDangKy) AS 'STT',  CONVERT(varchar(50), (case when ToKhai.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = ToKhai.SoToKhai) else ToKhai.SoToKhai end ))  
                    + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai, ToKhai.NgayDangKy, v_KDT_SP_Xuat_HD.MaPhu AS MaHang,   
                      ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, 0 AS TongCong  
FROM         v_KDT_SP_Xuat_HD INNER JOIN  
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong,   
                                                   t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong,   
                                                   t_KDT_ToKhaiMauDich.MaDoanhNghiep  
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN  
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID  
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND   
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S')  
                            UNION  
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy,   
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, t_KDT_GC_HangChuyenTiep.MaHang,   
                                                  t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep  
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN  
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID  
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND   
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX' OR  
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19'OR  
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XVE54')) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong AND   
                      v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang INNER JOIN  
                      t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID INNER JOIN  
                      t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID  
WHERE     (v_KDT_SP_Xuat_HD.IDHopDong = @IDHopDong) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan)  
ORDER BY MaHang   
      
UPDATE t1  
SET  
 TongCong = t2.RunningTotal  
FROM #BC02 t1 INNER JOIN  
(SELECT temp1.STT AS STT,temp1.MaHang AS MaHang,SUM(temp2.SoLuong) AS RunningTotal  
   FROM #BC02 temp1 CROSS JOIN #BC02 temp2  
 WHERE temp2.STT <= temp1.STT AND Temp2.MaHang = temp1.MaHang   
 GROUP BY temp1.STT, temp1.MaHang) t2  
 ON t1.STT = t2.STT  
      
SELECT * FROM #BC02   
  
DROP TABLE #BC02      
      
END   
GO

/****** Object:  StoredProcedure [dbo].[p_GC_BC03HSTK_GC_TT117_New]    Script Date: 07/14/2014 14:46:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_BC03HSTK_GC_TT117_New]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117_New]
GO

/****** Object:  StoredProcedure [dbo].[p_GC_BC03HSTK_GC_TT117_New]    Script Date: 07/14/2014 14:46:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:  LanNT          
-- Create date:           
-- Description:   Modify 18/06/2012    
-- =============================================          
          
CREATE PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117_New]           
 -- Add the parameters for the stored procedure here          
 @IDHopDong BIGINT,          
 @MaHaiQuan CHAR(6),          
 @MaDoanhNghiep VARCHAR(14)           
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
 -- p_GC_BC03HSTK_GC_TT117 219, 'C34C','0400101556'          
-- SET @IDHopDong=306 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',          
-- declare @IDHopDong BIGINT, @MaHaiQuan CHAR(6), @MaDoanhNghiep VARCHAR(14);  set @IDHopDong=  306;set @MaHaiQuan='C34C'; Set @MaDoanhNghiep='0400101556';          
          
    --      
     /*ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT'*/    
     -- CONVERT(varchar(50), ToKhai.SoToKhai) + '/' + v_HaiQuan_MaLoaiHinh.Ten_VT AS SoToKhai  

--DECLARE    
-- @IDHopDong BIGINT,          
-- @MaHaiQuan CHAR(6),          
-- @MaDoanhNghiep VARCHAR(14)       

--SET
--@IDHopDong = 721
--SET
--@MaHaiQuan = 'N60C'
--SET
--@MaDoanhNghiep = '4000395355' 
   
if OBJECT_ID('tempdb..#BC02') is not null drop table #BC02
CREATE TABLE #BC03
(STT BIGINT
 ,SoToKhai VARCHAR(30)
 ,MaHang VARCHAR(35)
 ,TenHang NVARCHAR(255)
 ,DVT VARCHAR(5)
 ,SoLuong DECIMAL(18,6)
 ,TongCong DECIMAL(18,6)
 ,IDHopDong BIGINT
)


 INSERT INTO #BC03
 (
 	STT,
 	SoToKhai,
 	MaHang,
 	TenHang,
 	DVT,
 	SoLuong,
 	TongCong,
 	IDHopDong
 )
SELECT    ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang, ToKhai.NgayDangKy) AS 'STT', ToKhai.SoToKhai, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, 0 AS TongCong, ToKhai.IDHopDong  
FROM         (  
          
      SELECT     CONVERT(varchar(50), t_KDT_ToKhaiMauDich.SoToKhai) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai,   
              LTRIM(RTRIM(t_KDT_ToKhaiMauDich.MaLoaiHinh)) AS MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong,   
              t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.DVT_ID  
       FROM         t_KDT_ToKhaiMauDich INNER JOIN  
              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN  
              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID  
       WHERE     (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND   
        (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND  
        (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND   
        (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N') AND  
        (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND   
        (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep) AND  
        (NOT (t_KDT_ToKhaiMauDich.MaLoaiHinh IN ('XGC18,XGC19,XGC20')))  
                         
                                                                                                                        
                       UNION  
                         
       SELECT     CONVERT(varchar(50), t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) + '/' + t_HaiQuan_LoaiPhieuChuyenTiep.ID AS SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,   
                      t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_HangChuyenTiep.MaHang, t_KDT_GC_HangChuyenTiep.TenHang,   
                      t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_HangChuyenTiep.ID_DVT  
FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN  
                      t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID INNER JOIN  
                      t_HaiQuan_LoaiPhieuChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = t_HaiQuan_LoaiPhieuChuyenTiep.ID  
WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND   
                      (LTRIM(RTRIM(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh)) IN ('PHPLX', 'XGC18', 'PHSPX','PHTBX')) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N')           
       )   
                      AS ToKhai INNER JOIN  
                      t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID  
WHERE     (ToKhai.IDHopDong = @IDHopDong)  
--ORDER BY ToKhai.MaHang  

	

    
UPDATE t1
SET
	TongCong = t2.RunningTotal
FROM #BC03 t1 INNER JOIN
(SELECT temp1.STT AS STT,temp1.MaHang AS MaHang,SUM(temp2.SoLuong) AS RunningTotal
   FROM #BC03 temp1 CROSS JOIN #BC03 temp2
 WHERE temp2.STT <= temp1.STT AND Temp2.MaHang = temp1.MaHang 
 GROUP BY temp1.STT, temp1.MaHang) t2
 ON t1.STT = t2.STT
 
 
 
SELECT * FROM #BC03 
DROP TABLE #BC03  
  
END
     

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.4',GETDATE(), N' Cập nhậ store mau bao cao')
END



