﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_ChiThiHaiQuanForm : BaseForm
    {
        // public ELoaiThongTin LoaiThongTin = ELoaiThongTin.TK;
        public long Master_ID;
        public ELoaiThongTin LoaiThongTin;
        private List<KDT_VNACC_ChiThiHaiQuan> ListChiThiHQ = new List<KDT_VNACC_ChiThiHaiQuan>();
        private KDT_VNACC_ChiThiHaiQuan CTHQ = null;

        public VNACC_ChiThiHaiQuanForm()
        {
            InitializeComponent();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            /*
            try
            {
                Cursor = Cursors.WaitCursor;

                bool isAddNew = false;
                if (CTHQ == null)
                {
                    CTHQ = new KDT_VNACC_ChiThiHaiQuan();
                    isAddNew = true;
                }
                GetChiThi();
                if (isAddNew)
                    TKMD.ChiThiHQCollection.Add(CTHQ);
                grList.DataSource = TKMD.ChiThiHQCollection;
                grList.Refetch();
                CTHQ = new KDT_VNACC_ChiThiHaiQuan();
                SetChiThi();
             CTHQ=null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
            */
        }

        private void GetChiThi()
        {
            //CTHQ.PhanLoai = ctrPhanLoai.Code;
            //CTHQ.Ngay = Convert.ToDateTime(cclNgayChiThi.Value);
            //CTHQ.Ten = txtTenChiThi.Text;
            //CTHQ.NoiDung = txtNoiDung.Text;
        }

        private void SetChiThi()
        {
            if (CTHQ != null)
            {
                ctrPhanLoai.Code = CTHQ.PhanLoai;
                cclNgayChiThi.Value = CTHQ.Ngay;
                txtTenChiThi.Text = CTHQ.Ten;
                txtNoiDung.Text = CTHQ.NoiDung;
            }
        }

        private void VNACC_ChiThiHaiQuanForm_Load(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;

                LoadData(Master_ID, LoaiThongTin);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }

        private void LoadData(long master_id, ELoaiThongTin loaithongtin)
        {
            string where = string.Format("Master_id = {0} and LoaiThongTin = '{1}'", master_id, loaithongtin.ToString());
            ListChiThiHQ = KDT_VNACC_ChiThiHaiQuan.SelectCollectionDynamic(where, "");
            grList.DataSource = ListChiThiHQ;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            /*
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grList.SelectedItems;
                //List<KDT_VNACC_HangMauDich> ItemColl = new List<KDT_VNACC_HangMauDich>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa chỉ thị này này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ListChiThiHQ.Add((KDT_VNACC_ChiThiHaiQuan)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_ChiThiHaiQuan item in ListChiThiHQ)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKMD.ChiThiHQCollection.Remove(item);
                    }

                    grList.DataSource = TKMD.ChiThiHQCollection;
                    grList.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            */
        }

        private void grList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                CTHQ = grList.CurrentRow.DataRow as KDT_VNACC_ChiThiHaiQuan;

                SetChiThi();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
    }
}
