﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Logger;
using System.Threading;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface
{
    public partial class ProcessUpdateCategoryOnline : BaseForm
    {
        public int ProccesValue;
        private int limit = 99;
        private bool onRunning = false;
        private bool isError = false;
        private string msgError = "";
        public bool temp = false;
        private bool temp2 = false;
        public ProcessUpdateCategoryOnline()
        {
            InitializeComponent();
            Shown += new EventHandler(ProcessUpdateCategoryOnline_Shown);
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;
            // This event will be raised on the worker thread when the worker starts
            //backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            //// This event will be raised when we call ReportProgress
            //backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (onRunning) e.Cancel = false;
            else
                base.OnClosing(e);
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            onRunning = true;
            try
            {
                  UpdateCategoryVNACCSOnline();
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage("Lỗi cập nhật danh mục Online: ", ex);
                isError = true;
                msgError = ex.Message;
            }
            onRunning = false;
            Application.Restart();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbProgress.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pbProgress.Value = 100;
        }

        private void ProcessUpdateCategoryOnline_Load(object sender, EventArgs e)
        {
        }
        public string UpdateCategoryVNACCSOnline()
        {
            //ISyncData syndata = WebService.SyncService();
            DataSet dsCategory = new DataSet();
            Company.KDT.SHARE.Components.WSSyncData.Service syndata = WebService.SyncDataService();
            DateTime dateLastUpdated = new DateTime(1900, 1, 1);
            string msg = "";

            try
            {
                ///Md5 mkkb1998 =220c888ca1a69018c4a78bbff19e6273
                #region Get Category from Internet
                //Nhóm xử lý hồ sơ
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A014);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Đang cập nhật :  Nhóm xử lý hồ sơ ...";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr); 
                    List<VNACC_Category_CustomsSubSection> Collection = VNACC_Category_CustomsSubSection.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress( i*100/Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Đang cập nhật :  Nhóm xử lý hồ sơ ..." + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : "+Collection.Count +" Bản ghi";
                    //if (VNACC_Category_CustomsSubSection.InsertUpdateCollection(VNACC_Category_CustomsSubSection.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Nhóm xử lý hồ sơ :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                // Mã nước(Country, coded), Mã nước xuất xứ
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A015);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Đang cập nhật :  Mã nước(Country, coded), Mã nước xuất xứ ...";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_Nation> Collection = VNACC_Category_Nation.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Đang cập nhật :  Mã nước(Country, coded), Mã nước xuất xứ ..." + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";

                    //if (VNACC_Category_Nation.InsertUpdateCollection(VNACC_Category_Nation.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã nước(Country, coded), Mã nước xuất xứ :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A016);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Đang cập nhật :  Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_CityUNLOCODE> Collection = VNACC_Category_CityUNLOCODE.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Đang cập nhật :  Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_CityUNLOCODE.InsertUpdateCollection(VNACC_Category_CityUNLOCODE.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    //dsCategory = null;
                }
                // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
                //dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A016T);
                //if (dsCategory != null && dsCategory.Tables.Count > 0)
                //{
                //    if (VNACC_Category_CityUNLOCODE.InsertUpdateCollection(VNACC_Category_CityUNLOCODE.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                //        msg += "\r\n-Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                //    dsCategory = null;
                //}
                //Cơ quan Hải quan
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A038);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Đang cập nhật :  Cơ quan Hải quan";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_CustomsOffice> Collection = VNACC_Category_CustomsOffice.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Đang cập nhật :  Cơ quan Hải quan" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_CustomsOffice.InsertUpdateCollection(VNACC_Category_CustomsOffice.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Cơ quan Hải quan :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp), Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A202);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Đang cập nhật :  Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_Cargo> Collection = VNACC_Category_Cargo.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        try
                        {
                            Collection[i].InsertUpdate();
                            backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Đang cập nhật :  Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_Cargo.InsertUpdateCollection(VNACC_Category_Cargo.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp), Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp) :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                // Mã địa điểm kho hàng CFS khai báo đến Chi cục HQ Bình Dương và Đồng Nai
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A204);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Mã địa điểm kho hàng CFS khai báo đến Chi cục HQ Bình Dương và Đồng Nai";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_Cargo> Collection = VNACC_Category_Cargo.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        try
                        {
                            Collection[i].InsertUpdate();
                            backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Mã địa điểm kho hàng CFS khai báo đến Chi cục HQ Bình Dương và Đồng Nai" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_Cargo.InsertUpdateCollection(VNACC_Category_Cargo.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã địa điểm kho hàng CFS khai báo đến Chi cục HQ Bình Dương và Đồng Nai :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Container size
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A301);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Container size";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_ContainerSize> Collection = VNACC_Category_ContainerSize.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Container size" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_ContainerSize.InsertUpdateCollection(VNACC_Category_ContainerSize.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Container size :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Mã đơn vị tính (Packages unit code)
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A316);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Mã đơn vị tính (Packages unit code)";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_PackagesUnit> Collection = VNACC_Category_PackagesUnit.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Mã đơn vị tính (Packages unit code)" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_PackagesUnit.InsertUpdateCollection(VNACC_Category_PackagesUnit.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã đơn vị tính (Packages unit code) :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Mã biểu thuế nhập khẩu
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A404);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Mã biểu thuế nhập khẩu";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_TaxClassificationCode> Collection = VNACC_Category_TaxClassificationCode.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Mã biểu thuế nhập khẩu" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_TaxClassificationCode.InsertUpdateCollection(VNACC_Category_TaxClassificationCode.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã biểu thuế nhập khẩu :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                // Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A501);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_QuantityUnit> Collection = VNACC_Category_QuantityUnit.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_QuantityUnit.InsertUpdateCollection(VNACC_Category_QuantityUnit.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Mã số hàng hóa (HS)
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A506);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Mã số hàng hóa (HS)";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_HSCode> Collection = VNACC_Category_HSCode.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Mã số hàng hóa (HS)" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_HSCode.InsertUpdateCollection(VNACC_Category_HSCode.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã số hàng hóa (HS) :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Mã đồng tiền
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A527);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Mã đồng tiền";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_CurrencyExchange> Collection = VNACC_Category_CurrencyExchange.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Mã đồng tiền" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_CurrencyExchange.InsertUpdateCollection(VNACC_Category_CurrencyExchange.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã đồng tiền :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Loại chứng từ đính kèm
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A546);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Loại chứng từ đính kèm";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_ApplicationProcedureType> Collection = VNACC_Category_ApplicationProcedureType.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        Collection[i].InsertUpdate();
                        backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Loại chứng từ đính kèm" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_ApplicationProcedureType.InsertUpdateCollection(VNACC_Category_ApplicationProcedureType.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Loại chứng từ đính kèm :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Mã địa điểm dỡ hàng (Stations)
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A601);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Mã địa điểm dỡ hàng (Stations)";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_Station> Collection = VNACC_Category_Station.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        try
                        {
                            Collection[i].InsertUpdate();
                            backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Mã địa điểm dỡ hàng (Stations)" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_Station.InsertUpdateCollection(VNACC_Category_Station.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã địa điểm dỡ hàng (Stations) :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Mã địa điểm dỡ hàng (Border gate)
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A620);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Mã địa điểm dỡ hàng (Border gate)";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_BorderGate> Collection = VNACC_Category_BorderGate.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        try
                        {
                            Collection[i].InsertUpdate();
                            backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Mã địa điểm dỡ hàng (Border gate)" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_BorderGate.InsertUpdateCollection(VNACC_Category_BorderGate.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Mã địa điểm dỡ hàng (Border gate) :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Transport means (Vehicle) 
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A621);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Transport means (Vehicle)";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_TransportMean> Collection = VNACC_Category_TransportMean.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        try
                        {
                            Collection[i].InsertUpdate();
                            backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Transport means (Vehicle)" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_TransportMean.InsertUpdateCollection(VNACC_Category_TransportMean.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Transport means (Vehicle)  :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //OGA User
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A700);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "OGA User";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_OGAUser> Collection = VNACC_Category_OGAUser.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        try
                        {
                            Collection[i].InsertUpdate();
                            backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "OGA User" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_OGAUser.InsertUpdateCollection(VNACC_Category_OGAUser.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-OGA User :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                //Tổng hợp danh mục
                dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.E001);
                if (dsCategory != null && dsCategory.Tables.Count > 0)
                {
                    DataRow dr = dtProcess.NewRow();
                    dr["TenProcess"] = "Tổng hợp danh mục";
                    dr["TrangThai"] = "Đang thực hiện";
                    dtProcess.Rows.Add(dr);
                    List<VNACC_Category_Common> Collection = VNACC_Category_Common.ConvertDataSetToCollection(dsCategory);
                    for (int i = 0; i < Collection.Count; i++)
                    {
                        try
                        {
                            Collection[i].InsertUpdate();
                            backgroundWorker1.ReportProgress(i * 100 / Collection.Count);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        // Simulate long task
                        //System.Threading.Thread.Sleep(100);
                    }
                    dtProcess.Select("TenProcess='" + "Tổng hợp danh mục" + "'")[0]["TrangThai"] = "Hoàn thành";
                    dr["TotalRecord"] = "Tổng số : " + Collection.Count + " Bản ghi";
                    //if (VNACC_Category_Common.InsertUpdateCollection(VNACC_Category_Common.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
                    //    msg += "\r\n-Tổng hợp danh mục :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
                    dsCategory = null;
                }
                #endregion
                DataRow drSucess = dtProcess.NewRow();
                drSucess["TenProcess"] = "Đã cập nhật thông tin các Danh mục";
                drSucess["TrangThai"] = "Hoàn thành";
                dtProcess.Rows.Add(drSucess);
                return msg.Length != 0 ? "Đã cập nhật thông tin các Danh mục:" + msg : "";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                msg = "E|" + ex.Message;
            }

            return msg;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
        }

        private void ProcessUpdateCategoryOnline_Shown(object sender, EventArgs e)
        {
            try
            {
                if (backgroundWorker1.IsBusy)
                {
                    backgroundWorker1.CancelAsync();
                    backgroundWorker1.Dispose();
                }
                backgroundWorker1.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
