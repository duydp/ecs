﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.Windows.Forms;
using Customs.Component.Utility;
using Janus.Windows.GridEX;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Text;

namespace HaiQuan.HS
{
    public class ExportItemsForm : CommonForm
	{
		private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
		private Janus.Windows.GridEX.EditControls.EditBox txtDescription02;
		private Janus.Windows.GridEX.GridEX gridEX1;
		private IContainer components = null;

		public ExportItemsForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Janus.Windows.GridEX.GridEXLayout gridEXLayout1 = new Janus.Windows.GridEX.GridEXLayout();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ExportItemsForm));
			this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
			this.gridEX1 = new Janus.Windows.GridEX.GridEX();
			this.txtDescription02 = new Janus.Windows.GridEX.EditControls.EditBox();
			((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
			this.uiGroupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridEX1)).BeginInit();
			this.SuspendLayout();
			// 
			// uiGroupBox1
			// 
			this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
			this.uiGroupBox1.Controls.Add(this.gridEX1);
			this.uiGroupBox1.Controls.Add(this.txtDescription02);
			this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
			this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
			this.uiGroupBox1.Name = "uiGroupBox1";
			this.uiGroupBox1.Size = new System.Drawing.Size(536, 318);
			this.uiGroupBox1.TabIndex = 4;
			this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
			// 
			// gridEX1
			// 
			this.gridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
			this.gridEX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.gridEX1.ColumnAutoResize = true;
			gridEXLayout1.LayoutString = "<GridEXLayoutData><RootTable><Columns Collection=\"true\"><Column0 ID=\"ID\"><Caption" +
				">ID</Caption><DataMember>ID</DataMember><Key>ID</Key><Position>0</Position><Visi" +
				"ble>False</Visible><Width>51</Width></Column0><Column1 ID=\"STT\"><Caption>STT</Ca" +
				"ption><DataMember>STT</DataMember><Key>STT</Key><Position>1</Position><Width>51<" +
				"/Width></Column1><Column2 ID=\"Description\"><Caption>Mô tả hàng hóa</Caption><Dat" +
				"aMember>Description</DataMember><Key>Description</Key><Position>2</Position><Wid" +
				"th>208</Width></Column2><Column3 ID=\"Group\"><Caption>Thuộc nhóm, phân nhóm</Capt" +
				"ion><DataMember>Group</DataMember><Key>Group</Key><Position>3</Position><Width>1" +
				"53</Width></Column3><Column4 ID=\"Percent\"><Caption>TS (%)</Caption><DataMember>P" +
				"ercent</DataMember><Key>Percent</Key><Position>4</Position><TextAlignment>Center" +
				"</TextAlignment><Width>88</Width></Column4></Columns><FormatConditions Collectio" +
				"n=\"true\"><Condition0 ID=\"FormatCondition5\"><ColIndex>2</ColIndex><FormatStyleInd" +
				"ex>0</FormatStyleIndex><PreviewRowFormatStyleIndex>0</PreviewRowFormatStyleIndex" +
				"><TargetColIndex>2</TargetColIndex><Key>FormatCondition5</Key><FilterCondition><" +
				"ColumnIndex>-1</ColumnIndex><ConditionOperator>NotIsEmpty</ConditionOperator><Co" +
				"nditions Collection=\"true\"><FilterCondition0><ColumnIndex>-1</ColumnIndex><Condi" +
				"tionOperator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOperator></" +
				"FilterCondition0><FilterCondition1><ColumnIndex>-1</ColumnIndex><ConditionOperat" +
				"or>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOperator></FilterCond" +
				"ition1><FilterCondition2><ColumnIndex>-1</ColumnIndex><ConditionOperator>IsEmpty" +
				"</ConditionOperator><LogicalOperator>And</LogicalOperator></FilterCondition2></C" +
				"onditions></FilterCondition></Condition0><Condition1 ID=\"FormatCondition6\"><ColI" +
				"ndex>4</ColIndex><FormatStyleIndex>0</FormatStyleIndex><PreviewRowFormatStyleInd" +
				"ex>0</PreviewRowFormatStyleIndex><TargetColIndex>4</TargetColIndex><Key>FormatCo" +
				"ndition6</Key><FilterCondition><ColumnIndex>-1</ColumnIndex><ConditionOperator>N" +
				"otIsEmpty</ConditionOperator><Conditions Collection=\"true\"><FilterCondition0><Co" +
				"lumnIndex>-1</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><Logical" +
				"Operator>And</LogicalOperator></FilterCondition0><FilterCondition1><ColumnIndex>" +
				"-1</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator>A" +
				"nd</LogicalOperator></FilterCondition1><FilterCondition2><ColumnIndex>-1</Column" +
				"Index><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And</Logica" +
				"lOperator></FilterCondition2></Conditions></FilterCondition></Condition1></Forma" +
				"tConditions><GroupCondition ID=\"\" /></RootTable><FormatStyles Collection=\"true\">" +
				"<Style0 ID=\"FormatStyle1\"><BackColor>255, 255, 192</BackColor><FontBold>True</Fo" +
				"ntBold><Key>FormatStyle1</Key></Style0></FormatStyles></GridEXLayoutData>";
			this.gridEX1.DesignTimeLayout = gridEXLayout1;
			this.gridEX1.GroupByBoxVisible = false;
			this.gridEX1.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
			this.gridEX1.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
			this.gridEX1.Location = new System.Drawing.Point(8, 8);
			this.gridEX1.Name = "gridEX1";
			this.gridEX1.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
			this.gridEX1.Size = new System.Drawing.Size(520, 240);
			this.gridEX1.TabIndex = 12;
			this.gridEX1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
			this.gridEX1.SelectionChanged += new System.EventHandler(this.gridEX1_SelectionChanged);
			// 
			// txtDescription02
			// 
			this.txtDescription02.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtDescription02.AutoScrollMargin = new System.Drawing.Size(0, 0);
			this.txtDescription02.AutoScrollMinSize = new System.Drawing.Size(0, 0);
			this.txtDescription02.Location = new System.Drawing.Point(8, 256);
			this.txtDescription02.Multiline = true;
			this.txtDescription02.Name = "txtDescription02";
			this.txtDescription02.ReadOnly = true;
			this.txtDescription02.Size = new System.Drawing.Size(520, 56);
			this.txtDescription02.TabIndex = 11;
			this.txtDescription02.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
			this.txtDescription02.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
			// 
			// ExportItemsForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(536, 318);
			this.Controls.Add(this.uiGroupBox1);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ExportItemsForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Tag = "SearchForm";
			this.Text = "Biểu thuế xuất khẩu";
			this.Load += new System.EventHandler(this.ExportItemsForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
			this.uiGroupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridEX1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private void ExportItemsForm_Load(object sender, EventArgs e)
		{
			Item item = new Item();
			
			gridEX1.DataSource = item.SelectExportItems().Tables[0];
		}

		private void gridEX1_SelectionChanged(object sender, System.EventArgs e)
		{
			try
			{
				txtDescription02.Text = gridEX1.GetRow().Cells["Description"].Text;
			}
			catch
			{
			}
		}
	}
}