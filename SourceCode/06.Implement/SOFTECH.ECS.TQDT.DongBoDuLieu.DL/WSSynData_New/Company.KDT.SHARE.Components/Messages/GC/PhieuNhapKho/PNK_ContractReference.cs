﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho
{
   public class PNK_ContractReference
    {
        /// <summary>
        /// Số hợp đồng
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày hợp đồng
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Mã hải quan tiếp nhận
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }

        /// <summary>
        /// Ngày hết hạn
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }
    }
}
