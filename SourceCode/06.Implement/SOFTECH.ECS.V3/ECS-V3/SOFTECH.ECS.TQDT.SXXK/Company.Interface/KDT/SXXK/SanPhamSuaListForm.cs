﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamSuaListForm : BaseForm
    {
        public SanPhamDangKy spdk = new SanPhamDangKy();
        public SanPhamSuaListForm()
        {
            InitializeComponent();
        }

        private void SanPhamSuaListForm_Load(object sender, EventArgs e)
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            BindData();
        }
        public void BindData()
        {
            dgList.DataSource = SanPhamDangKySUA.SelectCollectionBy_IDSPDK(this.spdk.ID);
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            SanPhamSuaForm f = new SanPhamSuaForm();
            f.SPDangKy = spdk;
            f.masterId = spdk.ID;
            f.spdkSUA = new SanPhamDangKySUA() { TrangThaiXuLy = -1};
            f.ShowDialog(this);
            BindData();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            SanPhamSuaForm suaForm = new SanPhamSuaForm();
            suaForm.spdkSUA = (SanPhamDangKySUA)dgList.GetRow().DataRow;
            suaForm.masterIdSUA = suaForm.spdkSUA.ID;
            
            suaForm.masterId = this.spdk.ID;
            suaForm.SPDangKy = this.spdk;

            int tt = suaForm.spdkSUA.TrangThaiXuLy;

            suaForm.ShowDialog(this);
            if (tt != suaForm.spdkSUA.TrangThaiXuLy)
                BindData();
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
            {
                case -1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    break;
                case 0:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    break;
                case 1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                    break;
                case 2:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    break;
                case -3:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo nhưng chưa có phản hồi";
                    break;
            }
            if (Convert.ToInt32(e.Row.Cells["SoTiepNhan"].Value) == 0)
                e.Row.Cells["SoTiepNhan"].Text = string.Empty;
            if (e.Row.Cells["NgayTiepNhan"].Text == "01/01/1900")
                e.Row.Cells["NgayTiepNhan"].Text = string.Empty;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            deleteSP();
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            deleteSP();
        }

        private void deleteSP() {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count > 0)
            {
                if (MLMessages("Bạn có muốn xóa các lần khai báo này không?", "MSG_DEL01", "", true) == "Yes")
                {

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            //if (i.GetRow().Cells["TrangThaiXuLy"].Value.ToString() == "-1")//Chưa khai báo
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = Company.KDT.SHARE.Components.LoaiKhaiBao.SanPham;
                            sendXML.master_id = Convert.ToInt64(i.GetRow().Cells["ID"].Value);
                            if (!sendXML.Load() && (i.GetRow().Cells["TrangThaiXuLy"].Value.ToString() == "-1" || i.GetRow().Cells["TrangThaiXuLy"].Value.ToString() == "2"))
                            {
                                SanPhamDangKySUA spdkSUA = (SanPhamDangKySUA)i.GetRow().DataRow;
                                List<SanPhamSUA> spList = (List<SanPhamSUA>)SanPhamSUA.SelectCollectionBy_Master_IDSUA(spdkSUA.ID);
                                if (spList.Count > 0)
                                {
                                    foreach (SanPhamSUA sp in spList)
                                        sp.Delete();
                                }
                                spdkSUA.Delete();
                            }
                            else
                            {
                                int pos = i.Position + 1;
                                MLMessages("Danh sách thứ " + pos + "  đã gửi thông tin tới hải quan.", "MSG_NPL03", "" + pos + "", false);
                            }
                        }
                    }
                    this.BindData();
                }
            }
        }
    }
}
