How to enable Java WebStart applications
========================================

...including native libraries!

0. Generate jar file for your app. 

Tool: jar (JDK)

Syntax:
jar -cfv Sample.jar Sample.class


1. Generate a Keystore (*.keystore) file.

Tool: keystore (JDK)

Syntax:
keytool -genkey -keystore SecureDongleX.keystore -alias SDX

2. Pack native libraries (*.dll, ...) into Java libraries (*.jar).

Tool: jar (JDK)

Syntax:
jar -cfv dllLib.jar JSDX.dll

3. Sign ALL your application's libraries (*.jar) with your keystore by using the "jarsigner" tool.

Tool: jarsigner (JDK)

Syntax:
jarsigner -keystore mykeystore.keystore myapp.jar myalias
jarsigner -keystore mykeystore.keystore 3rdparty.jar myalias
jarsigner -keystore mykeystore.keystore nativelib.dll.jar myalias

4. Prepare JNLP File
Prepare Java WebStart JNLP File.

Syntax:
<?xml version="1.0" encoding="utf-8"?>
<!-- JNLP File for Notepad -->

<jnlp spec="1.0+"
      codebase="http://127.0.0.1/JWS/" 
      href="SDX.jnlp">
   <information>
      <title>SecureDongle X Demo</title>
      <vendor>SecureMetric</vendor>
      <description>SecureDongle X JWS Demo</description>      
      <description kind="short">How to Deploy SDX Java App using JWS</description>
      <offline-allowed/>
   </information>
    <security>
      <all-permissions/>
   </security>
   <resources>     
   	<jar href="JSDX.jar"/>   
	<j2se version="1.6+"/>	
	<nativelib href="dllLib.jar"/>
   </resources>  
   <application-desc main-class="Sample"/>
</jnlp>


5. JnlpDownloadServlet
Copy the "jnlp.sample.servlet.JnlpDownloadServlet" from Java JDK into your web applications' web library directory "WEB-INF/lib".

6. Prepare Deployment Descriptor
Prepare your web application's deployment descriptor (web.xml) to enable "JnlpDownloadServlet".

Syntax:

<servlet>
<servlet-name>JnlpDownloadServlet</servlet-name>
<servlet-class>jnlp.sample.servlet.JnlpDownloadServlet</servlet-class>
</servlet>
<servlet-mapping>
<servlet-name>JnlpDownloadServlet</servlet-name>
<url-pattern>*.jnlp</url-pattern>
</servlet-mapping>

7. Run the Application



support@securemetric.com
www.Securemetric.com
Formula for Strong Digital Security