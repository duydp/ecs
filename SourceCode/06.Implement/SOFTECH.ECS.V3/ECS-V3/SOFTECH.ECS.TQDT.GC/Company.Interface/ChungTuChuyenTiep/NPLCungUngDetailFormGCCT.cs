﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using Company.GC.BLL.KDT;
using Company.Interface.GC;
using NPLCungUng = Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng;
using NPLCungUngDetail = Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail;
using Company.GC.BLL.KDT.GC;
namespace Company.Interface
{
    public partial class NPLCungUngDetailFormGCCT : BaseForm
    {
        public ToKhaiChuyenTiep TKCT;
        public NPLCungUng nplCungUng = new NPLCungUng();
        private NPLCungUngDetail nplCungUngDetail = new NPLCungUngDetail();

        bool isEdit = false;
        public NPLCungUngDetailFormGCCT()
        {
            InitializeComponent();
        }
        private void KhoiTaoDuLieuChuan()
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            txtMaNPL.Text = nplCungUng.MaHang;
            txtTenNPL.Text = nplCungUng.TenHang;
            ccNgayChungTu.Value = DateTime.Now;

        }
        private void Set()
        {
            ctrMaLoaiHinh.Ma = nplCungUngDetail.MaLoaiHinh;
            ctrMaHaiQuan.Ma = nplCungUngDetail.MaHaiQuan;
            cbHinhThuc.SelectedValue = nplCungUngDetail.HinhThuc;
            txtSoHDon.Text = nplCungUngDetail.SoChungTu;
            ccNgayChungTu.Value = nplCungUngDetail.NgayChungTu;
            txtNguoiCap.Text = nplCungUngDetail.NoiPhatHanh;
            txtSoLuong.Text = nplCungUngDetail.SoLuong.ToString();
            cbDonViTinh.SelectedValue = nplCungUngDetail.DVT_ID;
            txtTyLeQuyDoi.Text = nplCungUngDetail.TyLeQuyDoi.ToString();
            txtDongTrenChungTu.Text = nplCungUngDetail.TyLeQuyDoi.ToString();
            txtGhiChu.Text = nplCungUngDetail.GhiChu;

        }
        private void Get()
        {
            if (!isEdit)
                nplCungUngDetail = new NPLCungUngDetail();
            nplCungUngDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            nplCungUngDetail.MaLoaiHinh = ctrMaLoaiHinh.Ma;
            nplCungUngDetail.MaHaiQuan = ctrMaHaiQuan.Ma;
            nplCungUngDetail.HinhThuc = Convert.ToInt32(cbHinhThuc.SelectedValue.ToString().Trim());
            nplCungUngDetail.SoChungTu = txtSoHDon.Text.Trim();
            nplCungUngDetail.NgayChungTu = ccNgayChungTu.Value;
            nplCungUngDetail.NoiPhatHanh = txtNguoiCap.Text.Trim();
            nplCungUngDetail.SoLuong = Convert.ToDecimal(Helpers.FormatNumeric(txtSoLuong.Text.Trim(), 3));
            nplCungUngDetail.TyLeQuyDoi = Convert.ToDecimal(Helpers.FormatNumeric(txtTyLeQuyDoi.Text.Trim(), 3));
            nplCungUngDetail.DongHangTrenCT = Convert.ToInt32(txtDongTrenChungTu.Text.Trim());
            nplCungUngDetail.GhiChu = txtGhiChu.Text.Trim();

            /*
             nplCungUng.DVT_ID = cbDonViTinh.SelectedValue.ToString();
             nplCungUng.MaHang = txtMaHang.Text.Trim();
             nplCungUng.TenHang = txtTenNPL.Text.Trim();
             nplCungUng.MaHS = txtMaHS.Text.Trim();
             nplCungUng.TongLuong = Convert.ToDouble(txtSoLuong.Value);
             * */


        }
        private void NPLCungUngForm_Load(object sender, EventArgs e)
        {
            KhoiTaoDuLieuChuan();
            dgList.DataSource = nplCungUng.NPLCungUngDetails;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["HinhThuc"].Value != null)
                e.Row.Cells["HinhThuc"].Text = Convert.ToInt32(e.Row.Cells["HinhThuc"].Value) == 1 ? "Nội địa" :
                    "Nhập khẩu";
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            error.Clear();
            bool isValid = Globals.ValidateNull(cbHinhThuc, error, "Hình thức cung ứng");
            isValid &= Globals.ValidateNull(txtSoHDon, error, "Số chứng từ");
            isValid &= Globals.ValidateNull(ccNgayChungTu, error, "Ngày chứng từ");
            if (Convert.ToDouble(txtSoLuong.Value) <= 0)
            {
                error.SetError(txtSoLuong, "Tổng số lượng không được để trống");
                isValid = false;
            }
            if (!isValid) return;
            Get();
            if (!isEdit)
            {
                nplCungUng.NPLCungUngDetails.Add(nplCungUngDetail);
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            isEdit = false;
            btnAddNew_Click(null, null);
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            nplCungUngDetail = new NPLCungUngDetail();
            Set();
            isEdit = false;
        }


        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                if (dgList.GetRow() == null) return;
                else nplCungUngDetail = (NPLCungUngDetail)dgList.GetRow().DataRow;
                if (nplCungUngDetail.ID > 0)
                {
                    nplCungUngDetail.Delete();
                }
                nplCungUng.NPLCungUngDetails.Remove(nplCungUngDetail);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.DataRow == null) return;
            nplCungUngDetail = (NPLCungUngDetail)e.Row.DataRow;
            isEdit = true;
            Set();
        }
    }
}
