using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAE8000 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ECN { get; set; }
public PropertiesAttribute JKN { get; set; }
public PropertiesAttribute FIC { get; set; }
public PropertiesAttribute BNO { get; set; }
public PropertiesAttribute DNO { get; set; }
public PropertiesAttribute TNO { get; set; }
public PropertiesAttribute K07 { get; set; }
public PropertiesAttribute K03 { get; set; }
public PropertiesAttribute K05 { get; set; }
public PropertiesAttribute ADF { get; set; }
public PropertiesAttribute K01 { get; set; }
public PropertiesAttribute K08 { get; set; }
public PropertiesAttribute K09 { get; set; }
public PropertiesAttribute K10 { get; set; }
public PropertiesAttribute AD1 { get; set; }
public PropertiesAttribute AD2 { get; set; }
public PropertiesAttribute AD3 { get; set; }
public PropertiesAttribute RID { get; set; }
public PropertiesAttribute AAA { get; set; }
public PropertiesAttribute CCM { get; set; }
public GroupAttribute CT { get; set; }

public VAE8000()
        {
ECN = new PropertiesAttribute(12, typeof(int));
JKN = new PropertiesAttribute(1, typeof(string));
FIC = new PropertiesAttribute(12, typeof(string));
BNO = new PropertiesAttribute(2, typeof(int));
DNO = new PropertiesAttribute(2, typeof(int));
TNO = new PropertiesAttribute(12, typeof(int));
K07 = new PropertiesAttribute(3, typeof(string));
K03 = new PropertiesAttribute(3, typeof(string));
K05 = new PropertiesAttribute(1, typeof(string));
ADF = new PropertiesAttribute(1, typeof(string));
K01 = new PropertiesAttribute(4, typeof(string));
K08 = new PropertiesAttribute(10, typeof(string));
K09 = new PropertiesAttribute(2, typeof(string));
K10 = new PropertiesAttribute(8, typeof(DateTime));
AD1 = new PropertiesAttribute(6, typeof(int));
AD2 = new PropertiesAttribute(8, typeof(DateTime));
AD3 = new PropertiesAttribute(6, typeof(int));
RID = new PropertiesAttribute(8, typeof(DateTime));
AAA = new PropertiesAttribute(1, typeof(string));
CCM = new PropertiesAttribute(1, typeof(string));
#region CT
List<PropertiesAttribute> listCT = new List<PropertiesAttribute>();
listCT.Add(new PropertiesAttribute(8, 10, EnumGroupID.VAE8000_D01, typeof(DateTime)));
listCT.Add(new PropertiesAttribute(120, 10, EnumGroupID.VAE8000_T01, typeof(string)));
listCT.Add(new PropertiesAttribute(420, 10, EnumGroupID.VAE8000_I01, typeof(string)));
CT = new GroupAttribute("CT", 10, listCT);
#endregion CT
TongSoByte = 5683;
}

}public partial class EnumGroupID
    {
public static readonly string VAE8000_D01 = "VAE8000_D01";
public static readonly string VAE8000_T01 = "VAE8000_T01";
public static readonly string VAE8000_I01 = "VAE8000_I01";
}

}
