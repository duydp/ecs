﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using System.Threading;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Common;
namespace Company.GC.BLL.KDT.GC
{
    public partial class KDT_GC_CungUngDangKy
    {
        private List<KDT_GC_CungUng> _ListNPLCungUng = new List<KDT_GC_CungUng>();

        public List<KDT_GC_CungUng> NPLCungUng_List
        {
            set { this._ListNPLCungUng = value; }
            get { return this._ListNPLCungUng; }
        }
        public void DeleteFull(long id)
        {
            this.ID = id;
            List<KDT_GC_CungUng> list = KDT_GC_CungUng.SelectCollectionDynamic("Master_ID = " + id, "");
            foreach (KDT_GC_CungUng item in list)
            {
                item.DeleteFull(item.ID);
            }
            this.Delete();
        }
        public static KDT_GC_CungUngDangKy LoadFull(long id)
        {
            KDT_GC_CungUngDangKy CUDK = new KDT_GC_CungUngDangKy();
            CUDK = KDT_GC_CungUngDangKy.Load(id);
            if (CUDK.ID == id)
            {
                List<KDT_GC_CungUng> lisCungUng = new List<KDT_GC_CungUng>();
                lisCungUng = KDT_GC_CungUng.SelectCollectionDynamic("Master_ID = " + CUDK.ID, "");
                foreach (KDT_GC_CungUng cungUng in lisCungUng)
                {
                    List<KDT_GC_CungUng_Detail> listCUDeTail = new List<KDT_GC_CungUng_Detail>();
                    listCUDeTail = KDT_GC_CungUng_Detail.SelectCollectionDynamic("Master_ID = " + cungUng.ID, "");
                    foreach (KDT_GC_CungUng_Detail cungUng_Detail in listCUDeTail)
                    {
                        List<KDT_GC_CungUng_ChungTu> listCUCT = new List<KDT_GC_CungUng_ChungTu>();
                        listCUCT = KDT_GC_CungUng_ChungTu.SelectCollectionDynamic("Master_ID = " + cungUng_Detail.ID, "");
                        foreach (KDT_GC_CungUng_ChungTu item in listCUCT)
                        {
                            cungUng_Detail.CungUngCT_List.Add(item);
                        }
                        cungUng.CungUngDetail_List.Add(cungUng_Detail);
                    }
                    CUDK.NPLCungUng_List.Add(cungUng);
                }
                return CUDK;
            }
            else
                return null;
        }

    }
  
}
