﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class RepresentativePerson
    {
        ///<summary>
        ///Chức vụ
        ///</summary>
        [XmlElement("contactFunction")]
        public string ContactFunction { set; get; }
        
        ///<summary>
        ///Tên người gửi
        ///</summary>
        [XmlElement("name")]
        public string Name { set; get; }

    }
}
