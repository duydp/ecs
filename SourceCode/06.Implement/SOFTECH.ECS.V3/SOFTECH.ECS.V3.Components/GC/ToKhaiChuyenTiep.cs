﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.Nodes.GC;
using Company.KDT.SHARE.Components;
using SOFTECH.ECS.V3.Components.SHARE;
using SOFTECH.ECS.V3.Components.GC.SHARE;
using Company.KDT.SHARE.Components.Utils;



namespace SOFTECH.ECS.V3.Components.GC
{//AiNPV
    public partial class ToKhaiChuyenTiep
    {
        private HangChuyenTiepCollection _HCTCollection = new HangChuyenTiepCollection();

        public HangChuyenTiepCollection HCTCollection
        {
            set { this._HCTCollection = value; }
            get { return this._HCTCollection; }
        }
        /// <summary>
        /// Config Parent To Khai Chuyen Tiep 
        /// </summary>
        /// <param name="tknhap">int</param>
        /// <param name="issuer">int</param>
        /// <param name="function">int</param>
        /// <param name="status">int</param>
        /// <param name="declarationOffice">int</param>
        /// <returns>string</returns>
        public static XElement ConfigParent(XElement Parent, Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct, int issuer, int function, int status, string declarationOffice)
        {
            if (Parent != null && tkct != null)
            {
                NguyenTe nt = NguyenTe.Load(tkct.NguyenTe_ID);
                bool checkAdditionalDocument1 = true;
                bool checkAdditionalDocument2 = true;
                CuaKhau ckTN = CuaKhau.Load(tkct.MaHaiQuanTiepNhan.Trim());

                /*Thong tin chung tu*/
                XElement issuerElement = Parent.Element(NodeTKNhapChuyenTiep.issuer);// Loại chứng từ N = 968; X= 986
                issuerElement.SetValue(issuer);
                XElement referenceElement = Parent.Element(NodeTKNhapChuyenTiep.reference);// Số tham chiếu chứng từ
                referenceElement.SetValue(tkct.GUIDSTR.ToUpper());
                XElement issueElement = Parent.Element(NodeTKNhapChuyenTiep.issue);// Ngày khai chứng từ
                issueElement.SetValue(tkct.NgayTiepNhan.Year > 1900 ? tkct.NgayTiepNhan.ToString(NgayThang.yyyyMMdd) : DateTime.Now.ToString(NgayThang.yyyyMMdd));//tkct.NgayTiepNhan
                XElement functionElement = Parent.Element(NodeTKNhapChuyenTiep.function);// Chức năng của chứng từ
                functionElement.SetValue(function);
                XElement issueLocationElement = Parent.Element(NodeTKNhapChuyenTiep.issueLocation);// Nơi khai báo
                issueLocationElement.SetValue("");//ckTN.Ten
                XElement statusElement = Parent.Element(NodeTKNhapChuyenTiep.status);// Trạng thái chứng từ
                statusElement.SetValue(status);
                XElement customsReferenceElement = Parent.Element(NodeTKNhapChuyenTiep.customsReference);// Số đăng ký tờ khai

                customsReferenceElement.SetValue(tkct.SoToKhai != 0 ? tkct.SoToKhai.ToString() : "");
                XElement acceptanceElement = Parent.Element(NodeTKNhapChuyenTiep.acceptance);// Ngày đăng ký tờ khai
                acceptanceElement.SetValue(tkct.NgayDangKy.Year > 1900 ? tkct.NgayDangKy.ToString(NgayThang.yyyyMMdd) : "");
                XElement declarationOfficeElement = Parent.Element(NodeTKNhapChuyenTiep.declarationOffice);// Mã hải quan
                declarationOfficeElement.SetValue(declarationOffice);
                XElement goodsItemElement = Parent.Element(NodeTKNhapChuyenTiep.goodsItem);// Số lượng hàng
                IList<HangChuyenTiep> lHCTiep = HangChuyenTiep.SelectCollectionBy_Master_ID(tkct.ID);
                goodsItemElement.SetValue(lHCTiep.Count);
                XElement loadingListElement = Parent.Element(NodeTKNhapChuyenTiep.loadingList);// Số lượng chứng từ, phụ lục kèm theo
                loadingListElement.SetValue(0);
                XElement totalGrossMassElement = Parent.Element(NodeTKNhapChuyenTiep.totalGrossMass);// Trọng lượng
                totalGrossMassElement.SetValue(tkct.TrongLuong);
                XElement totalNetGrossMassElement = Parent.Element(NodeTKNhapChuyenTiep.totalNetGrossMass);// Trọng lượng tịnh
                totalNetGrossMassElement.SetValue(tkct.TrongLuongTinh);
                XElement natureOfTransactionElement = Parent.Element(NodeTKNhapChuyenTiep.natureOfTransaction);// Mã loại hình
                natureOfTransactionElement.SetValue(tkct.MaLoaiHinh.Trim());
                //Phuong thuc thanh toan
                XElement PaymentElement = Parent.Element(NodeTKNhapChuyenTiep.Payment);
                XElement PaymentMethodElement = PaymentElement.Element(NodeTKNhapChuyenTiep.method);
                PaymentMethodElement.SetValue(tkct.PTTT_ID);
                //Dai ly
                XElement AgentElement = Parent.Element(NodeTKNhapChuyenTiep.Agent);
                XElement AgentNameElement = AgentElement.Element(NodeTKNhapChuyenTiep.name);
                XElement AgentIdElement = AgentElement.Element(NodeTKNhapChuyenTiep.identity);
                XElement AgentStatusElement = AgentElement.Element(NodeTKNhapChuyenTiep.status);
                AgentNameElement.SetValue(tkct.MaDaiLy);
                AgentIdElement.SetValue(tkct.MaDaiLy);
                AgentStatusElement.SetValue(tkct.MaDaiLy != "" ? "1" : "");
                int statusInt = 1;
                XElement AgentNameElement2 = null;
                XElement AgentIdElement2 = null;
                XElement AgentStatusElement2 = null;

                XElement AgentNameElement3 = null;
                XElement AgentIdElement3 = null;
                XElement AgentStatusElement3 = null;

                XElement AgentNameElement4 = null;
                XElement AgentIdElement4 = null;
                XElement AgentStatusElement4 = null;
                foreach (XElement xElement in AgentElement.ElementsAfterSelf(NodeTKNhapChuyenTiep.Agent))
                {
                    statusInt++;
                    if (statusInt == 2)// Người ủy thác
                    {
                        AgentNameElement2 = xElement.Element(NodeTKNhapChuyenTiep.name);
                        AgentIdElement2 = xElement.Element(NodeTKNhapChuyenTiep.identity);
                        AgentStatusElement2 = xElement.Element(NodeTKNhapChuyenTiep.status);
                        AgentNameElement2.SetValue("");
                        AgentIdElement2.SetValue("");
                        AgentStatusElement2.SetValue("");
                    }
                    else if (statusInt == 3)
                    {// Người khai HQ
                        AgentNameElement3 = xElement.Element(NodeTKNhapChuyenTiep.name);
                        AgentIdElement3 = xElement.Element(NodeTKNhapChuyenTiep.identity);
                        AgentStatusElement3 = xElement.Element(NodeTKNhapChuyenTiep.status);
                        AgentNameElement3.SetValue(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
                        AgentIdElement3.SetValue(tkct.MaDoanhNghiep);
                        AgentStatusElement3.SetValue(statusInt);
                    }
                    else if (statusInt == 4)
                    {// Người chịu trách nhiệm nộp thuế
                        AgentNameElement4 = xElement.Element(NodeTKNhapChuyenTiep.name);
                        AgentIdElement4 = xElement.Element(NodeTKNhapChuyenTiep.identity);
                        AgentStatusElement4 = xElement.Element(NodeTKNhapChuyenTiep.status);
                        AgentNameElement4.SetValue(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
                        AgentIdElement4.SetValue(tkct.MaDoanhNghiep);
                        AgentStatusElement4.SetValue(statusInt);
                    }
                }
                if (tkct.MaDaiLy == "")
                    AgentElement.Remove();
                if (AgentStatusElement2.Value == "")
                    AgentNameElement2.Parent.Remove();
                //Nguyen Te
                XElement CurrencyExchangeElement = Parent.Element(NodeTKNhapChuyenTiep.CurrencyExchange);
                XElement currencyTypeElement = CurrencyExchangeElement.Element(NodeTKNhapChuyenTiep.currencyType);
                XElement rateElement = CurrencyExchangeElement.Element(NodeTKNhapChuyenTiep.rate);
                if (nt != null)
                {
                    currencyTypeElement.SetValue(nt.ID);// Mã nguyên tệ
                    rateElement.SetValue(Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(tkct.TyGiaVND, 9));
                }
                //So kien
                XElement DeclarationPackagingElement = Parent.Element(NodeTKNhapChuyenTiep.DeclarationPackaging);
                XElement quantityElement = DeclarationPackagingElement.Element(NodeTKNhapChuyenTiep.quantity);
                quantityElement.SetValue(tkct.SoKien.ToString("00000000"));
                //Hợp đồng nhận
                if (checkAdditionalDocument1 == true)
                {
                    XElement AdditionalDocumentElement = Parent.Element(NodeTKNhapChuyenTiep.AdditionalDocument);
                    XElement issueAdditionalDocumentElement = AdditionalDocumentElement.Element(NodeTKNhapChuyenTiep.issue);
                    XElement referenceAdditionalDocumentElement = AdditionalDocumentElement.Element(NodeTKNhapChuyenTiep.reference);
                    XElement typeAdditionalDocumentElement = AdditionalDocumentElement.Element(NodeTKNhapChuyenTiep.type);
                    XElement nameAdditionalDocumentElement = AdditionalDocumentElement.Element(NodeTKNhapChuyenTiep.name);
                    XElement expireAdditionalDocumentElement = AdditionalDocumentElement.Element(NodeTKNhapChuyenTiep.expire);
                    issueAdditionalDocumentElement.SetValue(tkct.NgayHDDV.ToString(NgayThang.yyyyMMdd));// Ngày hợp đồng
                    referenceAdditionalDocumentElement.SetValue(tkct.SoHopDongDV);// Số hợp đồng
                    typeAdditionalDocumentElement.SetValue(tkct.MaLoaiHinh.Substring(0, 1).Equals("N") ? Message_Types.Hop_Dong_Nhan : Message_Types.Hop_Dong_Giao);// Loại chứng từ
                    nameAdditionalDocumentElement.SetValue(tkct.SoHopDongDV);// Hợp đồng
                    expireAdditionalDocumentElement.SetValue(tkct.NgayHetHanHDDV.ToString(NgayThang.yyyyMMdd));// Ngày hết hạn
                    checkAdditionalDocument1 = false;
                }
                //Hợp đồng giao
                if (checkAdditionalDocument2 == true)
                {
                    XElement AdditionalDocumentElement = Parent.Element(NodeTKNhapChuyenTiep.AdditionalDocument);
                    XNode AdditionalDocumentNode = AdditionalDocumentElement.NextNode;

                    foreach (XElement xElement in AdditionalDocumentElement.ElementsAfterSelf(NodeTKNhapChuyenTiep.AdditionalDocument))
                    {
                        XElement issueAdditionalDocumentElement2 = xElement.Element(NodeTKNhapChuyenTiep.issue);
                        XElement referenceAdditionalDocumentElement2 = xElement.Element(NodeTKNhapChuyenTiep.reference);
                        XElement typeAdditionalDocumentElement2 = xElement.Element(NodeTKNhapChuyenTiep.type);
                        XElement nameAdditionalDocumentElement2 = xElement.Element(NodeTKNhapChuyenTiep.name);
                        XElement expireAdditionalDocumentElement2 = xElement.Element(NodeTKNhapChuyenTiep.expire);
                        issueAdditionalDocumentElement2.SetValue(tkct.NgayHDKH.ToString(NgayThang.yyyyMMdd));// Ngày hợp đồng
                        referenceAdditionalDocumentElement2.SetValue(tkct.SoHDKH);// Số hợp đồng
                        typeAdditionalDocumentElement2.SetValue(tkct.MaLoaiHinh.Substring(0, 1).Equals("N") ? Message_Types.Hop_Dong_Giao : Message_Types.Hop_Dong_Nhan);// Loại chứng từ
                        nameAdditionalDocumentElement2.SetValue(tkct.SoHDKH);// Hợp đồng
                        expireAdditionalDocumentElement2.SetValue(tkct.NgayHetHanHDKH.ToString(NgayThang.yyyyMMdd));// Ngày hết hạn
                    }
                }
                if (tkct.MaLoaiHinh.Contains("N"))
                {
                    //Người nhập khẩu
                    XElement ImporterElement = Parent.Element(NodeTKNhapChuyenTiep.Importer);
                    XElement nameImporterElement = ImporterElement.Element(NodeTKNhapChuyenTiep.name);
                    XElement identityImporterElement = ImporterElement.Element(NodeTKNhapChuyenTiep.identity);
                    nameImporterElement.SetValue(FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI")));
                    identityImporterElement.SetValue(tkct.MaDoanhNghiep);
                }
                else
                {
                    XElement ExporterElement = Parent.Element(NodeTKNhapChuyenTiep.Exporter);
                    XElement nameExporterElement = ExporterElement.Element(NodeTKNhapChuyenTiep.name);
                    XElement identityExporterElement = ExporterElement.Element(NodeTKNhapChuyenTiep.identity);
                    nameExporterElement.SetValue(FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI")));
                    identityExporterElement.SetValue(tkct.MaDoanhNghiep);
                }
                //Người dại diện doanh nghiệp
                XElement RepresentativePersonElement = Parent.Element(NodeTKNhapChuyenTiep.RepresentativePerson);
                XElement contactFunctionRepresentativePersonElement = RepresentativePersonElement.Element(NodeTKNhapChuyenTiep.contactFunction);// Chức vụ
                XElement nameRepresentativePersonElement = RepresentativePersonElement.Element(NodeTKNhapChuyenTiep.name);// Tên
                contactFunctionRepresentativePersonElement.SetValue(".");
                nameRepresentativePersonElement.SetValue(".");
            }
            return Parent;
        }
        public static XElement GetXElement(XNode node)
        {
            XDocument xDoc = new XDocument();
            using (XmlWriter xmlWriter = xDoc.CreateWriter())
                node.WriteTo(xmlWriter);
            return xDoc.Root;
        }

        public static string LayPhanHoi(string pass, string xml)
        {

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            doc.GetElementsByTagName("function")[0].InnerText = Message_Functions.Hoi_trang_thai.ToString();
            //doc.GetElementsByTagName("function")[1].InnerText = Message_Functions.Hoi_trang_thai.ToString();
            doc.GetElementsByTagName("messageId")[0].InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            string kq = "";
            string msgError = string.Empty;
            int i = 0;
            XmlNode Result = null;
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);

                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/statement");
                    if (Result.InnerText != "27")
                    {
                        if (Result.InnerText == "12")
                            continue;
                        else
                            break;
                    }
                    else
                    {
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").InnerText != "")
                            return docNPL.InnerXml.ToString();
                        else
                        {

                            foreach (XmlNode node in docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").ChildNodes)
                            {
                                msgError += node.InnerText + "; \n";
                            }

                            throw new Exception(msgError + "|" + "");
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (msgError != string.Empty) throw ex;
                    if (!string.IsNullOrEmpty(kq)) Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));

                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            return docNPL.InnerXml;

        }
    }
}