﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho
{
   public  class PNK_GoodsMeasure
    {
        /// <summary>
        /// Số lượng dự kiến nhập
        /// </summary>
        [XmlElement("docQuantity")]
        public string DocQuantity { get; set; }

        /// <summary>
        /// Số lượng thực nhập
        /// </summary>
        [XmlElement("actualQuantity")]
        public string ActualQuantity { get; set; }

        /// <summary>
        /// Mã đơn vị tính an..3
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set; }
    }
}
