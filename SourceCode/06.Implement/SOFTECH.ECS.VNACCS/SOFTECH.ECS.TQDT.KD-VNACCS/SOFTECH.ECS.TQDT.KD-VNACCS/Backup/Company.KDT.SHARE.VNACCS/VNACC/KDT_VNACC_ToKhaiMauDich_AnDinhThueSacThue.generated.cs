using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long AnDinhThue_ID { set; get; }
		public string TenSacThue { set; get; }
		public decimal TieuMuc { set; get; }
		public decimal SoThueKhaiBao { set; get; }
		public decimal SoThueAnDinh { set; get; }
		public decimal SoThueChenhLech { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> collection = new List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue>();
			while (reader.Read())
			{
				KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue entity = new KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("AnDinhThue_ID"))) entity.AnDinhThue_ID = reader.GetInt64(reader.GetOrdinal("AnDinhThue_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSacThue"))) entity.TenSacThue = reader.GetString(reader.GetOrdinal("TenSacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuMuc"))) entity.TieuMuc = reader.GetDecimal(reader.GetOrdinal("TieuMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThueKhaiBao"))) entity.SoThueKhaiBao = reader.GetDecimal(reader.GetOrdinal("SoThueKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThueAnDinh"))) entity.SoThueAnDinh = reader.GetDecimal(reader.GetOrdinal("SoThueAnDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThueChenhLech"))) entity.SoThueChenhLech = reader.GetDecimal(reader.GetOrdinal("SoThueChenhLech"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> collection, long id)
        {
            foreach (KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue VALUES(@AnDinhThue_ID, @TenSacThue, @TieuMuc, @SoThueKhaiBao, @SoThueAnDinh, @SoThueChenhLech)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue SET AnDinhThue_ID = @AnDinhThue_ID, TenSacThue = @TenSacThue, TieuMuc = @TieuMuc, SoThueKhaiBao = @SoThueKhaiBao, SoThueAnDinh = @SoThueAnDinh, SoThueChenhLech = @SoThueChenhLech WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@AnDinhThue_ID", SqlDbType.BigInt, "AnDinhThue_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuMuc", SqlDbType.Decimal, "TieuMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThueKhaiBao", SqlDbType.Decimal, "SoThueKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThueAnDinh", SqlDbType.Decimal, "SoThueAnDinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThueChenhLech", SqlDbType.Decimal, "SoThueChenhLech", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@AnDinhThue_ID", SqlDbType.BigInt, "AnDinhThue_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuMuc", SqlDbType.Decimal, "TieuMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThueKhaiBao", SqlDbType.Decimal, "SoThueKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThueAnDinh", SqlDbType.Decimal, "SoThueAnDinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThueChenhLech", SqlDbType.Decimal, "SoThueChenhLech", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue VALUES(@AnDinhThue_ID, @TenSacThue, @TieuMuc, @SoThueKhaiBao, @SoThueAnDinh, @SoThueChenhLech)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue SET AnDinhThue_ID = @AnDinhThue_ID, TenSacThue = @TenSacThue, TieuMuc = @TieuMuc, SoThueKhaiBao = @SoThueKhaiBao, SoThueAnDinh = @SoThueAnDinh, SoThueChenhLech = @SoThueChenhLech WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@AnDinhThue_ID", SqlDbType.BigInt, "AnDinhThue_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuMuc", SqlDbType.Decimal, "TieuMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThueKhaiBao", SqlDbType.Decimal, "SoThueKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThueAnDinh", SqlDbType.Decimal, "SoThueAnDinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThueChenhLech", SqlDbType.Decimal, "SoThueChenhLech", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@AnDinhThue_ID", SqlDbType.BigInt, "AnDinhThue_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuMuc", SqlDbType.Decimal, "TieuMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThueKhaiBao", SqlDbType.Decimal, "SoThueKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThueAnDinh", SqlDbType.Decimal, "SoThueAnDinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThueChenhLech", SqlDbType.Decimal, "SoThueChenhLech", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> SelectCollectionBy_AnDinhThue_ID(long anDinhThue_ID)
		{
            IDataReader reader = SelectReaderBy_AnDinhThue_ID(anDinhThue_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_AnDinhThue_ID(long anDinhThue_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@AnDinhThue_ID", SqlDbType.BigInt, anDinhThue_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_AnDinhThue_ID(long anDinhThue_ID)
		{
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@AnDinhThue_ID", SqlDbType.BigInt, anDinhThue_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue(long anDinhThue_ID, string tenSacThue, decimal tieuMuc, decimal soThueKhaiBao, decimal soThueAnDinh, decimal soThueChenhLech)
		{
			KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue entity = new KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue();	
			entity.AnDinhThue_ID = anDinhThue_ID;
			entity.TenSacThue = tenSacThue;
			entity.TieuMuc = tieuMuc;
			entity.SoThueKhaiBao = soThueKhaiBao;
			entity.SoThueAnDinh = soThueAnDinh;
			entity.SoThueChenhLech = soThueChenhLech;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@AnDinhThue_ID", SqlDbType.BigInt, AnDinhThue_ID);
			db.AddInParameter(dbCommand, "@TenSacThue", SqlDbType.NVarChar, TenSacThue);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.Decimal, TieuMuc);
			db.AddInParameter(dbCommand, "@SoThueKhaiBao", SqlDbType.Decimal, SoThueKhaiBao);
			db.AddInParameter(dbCommand, "@SoThueAnDinh", SqlDbType.Decimal, SoThueAnDinh);
			db.AddInParameter(dbCommand, "@SoThueChenhLech", SqlDbType.Decimal, SoThueChenhLech);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue(long id, long anDinhThue_ID, string tenSacThue, decimal tieuMuc, decimal soThueKhaiBao, decimal soThueAnDinh, decimal soThueChenhLech)
		{
			KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue entity = new KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue();			
			entity.ID = id;
			entity.AnDinhThue_ID = anDinhThue_ID;
			entity.TenSacThue = tenSacThue;
			entity.TieuMuc = tieuMuc;
			entity.SoThueKhaiBao = soThueKhaiBao;
			entity.SoThueAnDinh = soThueAnDinh;
			entity.SoThueChenhLech = soThueChenhLech;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@AnDinhThue_ID", SqlDbType.BigInt, AnDinhThue_ID);
			db.AddInParameter(dbCommand, "@TenSacThue", SqlDbType.NVarChar, TenSacThue);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.Decimal, TieuMuc);
			db.AddInParameter(dbCommand, "@SoThueKhaiBao", SqlDbType.Decimal, SoThueKhaiBao);
			db.AddInParameter(dbCommand, "@SoThueAnDinh", SqlDbType.Decimal, SoThueAnDinh);
			db.AddInParameter(dbCommand, "@SoThueChenhLech", SqlDbType.Decimal, SoThueChenhLech);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue(long id, long anDinhThue_ID, string tenSacThue, decimal tieuMuc, decimal soThueKhaiBao, decimal soThueAnDinh, decimal soThueChenhLech)
		{
			KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue entity = new KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue();			
			entity.ID = id;
			entity.AnDinhThue_ID = anDinhThue_ID;
			entity.TenSacThue = tenSacThue;
			entity.TieuMuc = tieuMuc;
			entity.SoThueKhaiBao = soThueKhaiBao;
			entity.SoThueAnDinh = soThueAnDinh;
			entity.SoThueChenhLech = soThueChenhLech;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@AnDinhThue_ID", SqlDbType.BigInt, AnDinhThue_ID);
			db.AddInParameter(dbCommand, "@TenSacThue", SqlDbType.NVarChar, TenSacThue);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.Decimal, TieuMuc);
			db.AddInParameter(dbCommand, "@SoThueKhaiBao", SqlDbType.Decimal, SoThueKhaiBao);
			db.AddInParameter(dbCommand, "@SoThueAnDinh", SqlDbType.Decimal, SoThueAnDinh);
			db.AddInParameter(dbCommand, "@SoThueChenhLech", SqlDbType.Decimal, SoThueChenhLech);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue(long id)
		{
			KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue entity = new KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_AnDinhThue_ID(long anDinhThue_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@AnDinhThue_ID", SqlDbType.BigInt, anDinhThue_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}