-- Drop Existing Procedures
GO
IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_AdditionalDocument]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GhiChuKhac,
	@FileName,
	@Content,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_AdditionalDocument]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GhiChuKhac] = @GhiChuKhac,
	[FileName] = @FileName,
	[Content] = @Content,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_AdditionalDocument] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_AdditionalDocument] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GhiChuKhac] = @GhiChuKhac,
			[FileName] = @FileName,
			[Content] = @Content,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_AdditionalDocument]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GhiChuKhac],
			[FileName],
			[Content],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GhiChuKhac,
			@FileName,
			@Content,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_AdditionalDocument]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_AdditionalDocument] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_AdditionalDocument]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_AdditionalDocument] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectAll]

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_AdditionalDocument]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectBy_AdditionalDocument_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectBy_AdditionalDocument_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_DeleteBy_AdditionalDocument_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_DeleteBy_AdditionalDocument_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Insert]
	@AdditionalDocument_ID bigint,
	@SoChungTu nvarchar(35),
	@TenChungTu nvarchar(255),
	@NgayPhatHanh datetime,
	@NoiPhatHanh nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_AdditionalDocument_Details]
(
	[AdditionalDocument_ID],
	[SoChungTu],
	[TenChungTu],
	[NgayPhatHanh],
	[NoiPhatHanh]
)
VALUES 
(
	@AdditionalDocument_ID,
	@SoChungTu,
	@TenChungTu,
	@NgayPhatHanh,
	@NoiPhatHanh
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Update]
	@ID bigint,
	@AdditionalDocument_ID bigint,
	@SoChungTu nvarchar(35),
	@TenChungTu nvarchar(255),
	@NgayPhatHanh datetime,
	@NoiPhatHanh nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_AdditionalDocument_Details]
SET
	[AdditionalDocument_ID] = @AdditionalDocument_ID,
	[SoChungTu] = @SoChungTu,
	[TenChungTu] = @TenChungTu,
	[NgayPhatHanh] = @NgayPhatHanh,
	[NoiPhatHanh] = @NoiPhatHanh
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_InsertUpdate]
	@ID bigint,
	@AdditionalDocument_ID bigint,
	@SoChungTu nvarchar(35),
	@TenChungTu nvarchar(255),
	@NgayPhatHanh datetime,
	@NoiPhatHanh nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_AdditionalDocument_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_AdditionalDocument_Details] 
		SET
			[AdditionalDocument_ID] = @AdditionalDocument_ID,
			[SoChungTu] = @SoChungTu,
			[TenChungTu] = @TenChungTu,
			[NgayPhatHanh] = @NgayPhatHanh,
			[NoiPhatHanh] = @NoiPhatHanh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_AdditionalDocument_Details]
		(
			[AdditionalDocument_ID],
			[SoChungTu],
			[TenChungTu],
			[NgayPhatHanh],
			[NoiPhatHanh]
		)
		VALUES 
		(
			@AdditionalDocument_ID,
			@SoChungTu,
			@TenChungTu,
			@NgayPhatHanh,
			@NoiPhatHanh
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_AdditionalDocument_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_DeleteBy_AdditionalDocument_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_DeleteBy_AdditionalDocument_ID]
	@AdditionalDocument_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_AdditionalDocument_Details]
WHERE
	[AdditionalDocument_ID] = @AdditionalDocument_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_AdditionalDocument_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[AdditionalDocument_ID],
	[SoChungTu],
	[TenChungTu],
	[NgayPhatHanh],
	[NoiPhatHanh]
FROM
	[dbo].[t_KDT_VNACCS_AdditionalDocument_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectBy_AdditionalDocument_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectBy_AdditionalDocument_ID]
	@AdditionalDocument_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[AdditionalDocument_ID],
	[SoChungTu],
	[TenChungTu],
	[NgayPhatHanh],
	[NoiPhatHanh]
FROM
	[dbo].[t_KDT_VNACCS_AdditionalDocument_Details]
WHERE
	[AdditionalDocument_ID] = @AdditionalDocument_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[AdditionalDocument_ID],
	[SoChungTu],
	[TenChungTu],
	[NgayPhatHanh],
	[NoiPhatHanh]
FROM [dbo].[t_KDT_VNACCS_AdditionalDocument_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[AdditionalDocument_ID],
	[SoChungTu],
	[TenChungTu],
	[NgayPhatHanh],
	[NoiPhatHanh]
FROM
	[dbo].[t_KDT_VNACCS_AdditionalDocument_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BillOfLading]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GhiChuKhac,
	@FileName,
	@Content,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BillOfLading]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GhiChuKhac] = @GhiChuKhac,
	[FileName] = @FileName,
	[Content] = @Content,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BillOfLading] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BillOfLading] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GhiChuKhac] = @GhiChuKhac,
			[FileName] = @FileName,
			[Content] = @Content,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BillOfLading]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GhiChuKhac],
			[FileName],
			[Content],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GhiChuKhac,
			@FileName,
			@Content,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BillOfLading]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BillOfLading] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BillOfLading]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_BillOfLading] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BillOfLading]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectBy_BillOfLading_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectBy_BillOfLading_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_Detail_DeleteBy_BillOfLading_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_DeleteBy_BillOfLading_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Insert]
	@BillOfLading_ID bigint,
	@SoVanDon nvarchar(35),
	@NgayVanDon datetime,
	@NuocPhatHanh varchar(2),
	@DiaDiemCTQC nvarchar(255),
	@LoaiVanDon numeric(1, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BillOfLading_Details]
(
	[BillOfLading_ID],
	[SoVanDon],
	[NgayVanDon],
	[NuocPhatHanh],
	[DiaDiemCTQC],
	[LoaiVanDon]
)
VALUES 
(
	@BillOfLading_ID,
	@SoVanDon,
	@NgayVanDon,
	@NuocPhatHanh,
	@DiaDiemCTQC,
	@LoaiVanDon
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Update]
	@ID bigint,
	@BillOfLading_ID bigint,
	@SoVanDon nvarchar(35),
	@NgayVanDon datetime,
	@NuocPhatHanh varchar(2),
	@DiaDiemCTQC nvarchar(255),
	@LoaiVanDon numeric(1, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BillOfLading_Details]
SET
	[BillOfLading_ID] = @BillOfLading_ID,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[NuocPhatHanh] = @NuocPhatHanh,
	[DiaDiemCTQC] = @DiaDiemCTQC,
	[LoaiVanDon] = @LoaiVanDon
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Detail_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_InsertUpdate]
	@ID bigint,
	@BillOfLading_ID bigint,
	@SoVanDon nvarchar(35),
	@NgayVanDon datetime,
	@NuocPhatHanh varchar(2),
	@DiaDiemCTQC nvarchar(255),
	@LoaiVanDon numeric(1, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BillOfLading_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BillOfLading_Details] 
		SET
			[BillOfLading_ID] = @BillOfLading_ID,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[NuocPhatHanh] = @NuocPhatHanh,
			[DiaDiemCTQC] = @DiaDiemCTQC,
			[LoaiVanDon] = @LoaiVanDon
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BillOfLading_Details]
		(
			[BillOfLading_ID],
			[SoVanDon],
			[NgayVanDon],
			[NuocPhatHanh],
			[DiaDiemCTQC],
			[LoaiVanDon]
		)
		VALUES 
		(
			@BillOfLading_ID,
			@SoVanDon,
			@NgayVanDon,
			@NuocPhatHanh,
			@DiaDiemCTQC,
			@LoaiVanDon
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BillOfLading_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Detail_DeleteBy_BillOfLading_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_DeleteBy_BillOfLading_ID]
	@BillOfLading_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BillOfLading_Details]
WHERE
	[BillOfLading_ID] = @BillOfLading_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Detail_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BillOfLading_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BillOfLading_ID],
	[SoVanDon],
	[NgayVanDon],
	[NuocPhatHanh],
	[DiaDiemCTQC],
	[LoaiVanDon]
FROM
	[dbo].[t_KDT_VNACCS_BillOfLading_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectBy_BillOfLading_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectBy_BillOfLading_ID]
	@BillOfLading_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BillOfLading_ID],
	[SoVanDon],
	[NgayVanDon],
	[NuocPhatHanh],
	[DiaDiemCTQC],
	[LoaiVanDon]
FROM
	[dbo].[t_KDT_VNACCS_BillOfLading_Details]
WHERE
	[BillOfLading_ID] = @BillOfLading_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[BillOfLading_ID],
	[SoVanDon],
	[NgayVanDon],
	[NuocPhatHanh],
	[DiaDiemCTQC],
	[LoaiVanDon]
FROM [dbo].[t_KDT_VNACCS_BillOfLading_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BillOfLading_ID],
	[SoVanDon],
	[NgayVanDon],
	[NuocPhatHanh],
	[DiaDiemCTQC],
	[LoaiVanDon]
FROM
	[dbo].[t_KDT_VNACCS_BillOfLading_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_CertificateOfOrigin]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GhiChuKhac,
	@FileName,
	@Content,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_CertificateOfOrigin]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GhiChuKhac] = @GhiChuKhac,
	[FileName] = @FileName,
	[Content] = @Content,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_CertificateOfOrigin] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_CertificateOfOrigin] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GhiChuKhac] = @GhiChuKhac,
			[FileName] = @FileName,
			[Content] = @Content,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_CertificateOfOrigin]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GhiChuKhac],
			[FileName],
			[Content],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GhiChuKhac,
			@FileName,
			@Content,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_CertificateOfOrigin]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_CertificateOfOrigin] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_CertificateOfOrigin]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_CertificateOfOrigin] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_CertificateOfOrigin]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectBy_CertificateOfOrigin_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectBy_CertificateOfOrigin_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_DeleteBy_CertificateOfOrigin_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_DeleteBy_CertificateOfOrigin_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Insert]
	@CertificateOfOrigin_ID bigint,
	@SoCO nvarchar(35),
	@LoaiCO nvarchar(35),
	@ToChucCapCO nvarchar(255),
	@NgayCapCO datetime,
	@NuocCapCO varchar(2),
	@NguoiCapCO nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details]
(
	[CertificateOfOrigin_ID],
	[SoCO],
	[LoaiCO],
	[ToChucCapCO],
	[NgayCapCO],
	[NuocCapCO],
	[NguoiCapCO]
)
VALUES 
(
	@CertificateOfOrigin_ID,
	@SoCO,
	@LoaiCO,
	@ToChucCapCO,
	@NgayCapCO,
	@NuocCapCO,
	@NguoiCapCO
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Update]
	@ID bigint,
	@CertificateOfOrigin_ID bigint,
	@SoCO nvarchar(35),
	@LoaiCO nvarchar(35),
	@ToChucCapCO nvarchar(255),
	@NgayCapCO datetime,
	@NuocCapCO varchar(2),
	@NguoiCapCO nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details]
SET
	[CertificateOfOrigin_ID] = @CertificateOfOrigin_ID,
	[SoCO] = @SoCO,
	[LoaiCO] = @LoaiCO,
	[ToChucCapCO] = @ToChucCapCO,
	[NgayCapCO] = @NgayCapCO,
	[NuocCapCO] = @NuocCapCO,
	[NguoiCapCO] = @NguoiCapCO
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_InsertUpdate]
	@ID bigint,
	@CertificateOfOrigin_ID bigint,
	@SoCO nvarchar(35),
	@LoaiCO nvarchar(35),
	@ToChucCapCO nvarchar(255),
	@NgayCapCO datetime,
	@NuocCapCO varchar(2),
	@NguoiCapCO nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details] 
		SET
			[CertificateOfOrigin_ID] = @CertificateOfOrigin_ID,
			[SoCO] = @SoCO,
			[LoaiCO] = @LoaiCO,
			[ToChucCapCO] = @ToChucCapCO,
			[NgayCapCO] = @NgayCapCO,
			[NuocCapCO] = @NuocCapCO,
			[NguoiCapCO] = @NguoiCapCO
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details]
		(
			[CertificateOfOrigin_ID],
			[SoCO],
			[LoaiCO],
			[ToChucCapCO],
			[NgayCapCO],
			[NuocCapCO],
			[NguoiCapCO]
		)
		VALUES 
		(
			@CertificateOfOrigin_ID,
			@SoCO,
			@LoaiCO,
			@ToChucCapCO,
			@NgayCapCO,
			@NuocCapCO,
			@NguoiCapCO
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_DeleteBy_CertificateOfOrigin_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_DeleteBy_CertificateOfOrigin_ID]
	@CertificateOfOrigin_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details]
WHERE
	[CertificateOfOrigin_ID] = @CertificateOfOrigin_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CertificateOfOrigin_ID],
	[SoCO],
	[LoaiCO],
	[ToChucCapCO],
	[NgayCapCO],
	[NuocCapCO],
	[NguoiCapCO]
FROM
	[dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectBy_CertificateOfOrigin_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectBy_CertificateOfOrigin_ID]
	@CertificateOfOrigin_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CertificateOfOrigin_ID],
	[SoCO],
	[LoaiCO],
	[ToChucCapCO],
	[NgayCapCO],
	[NuocCapCO],
	[NguoiCapCO]
FROM
	[dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details]
WHERE
	[CertificateOfOrigin_ID] = @CertificateOfOrigin_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[CertificateOfOrigin_ID],
	[SoCO],
	[LoaiCO],
	[ToChucCapCO],
	[NgayCapCO],
	[NuocCapCO],
	[NguoiCapCO]
FROM [dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CertificateOfOrigin_ID],
	[SoCO],
	[LoaiCO],
	[ToChucCapCO],
	[NgayCapCO],
	[NuocCapCO],
	[NguoiCapCO]
FROM
	[dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_CommercialInvoice]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GhiChuKhac,
	@FileName,
	@Content,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_CommercialInvoice]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GhiChuKhac] = @GhiChuKhac,
	[FileName] = @FileName,
	[Content] = @Content,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_CommercialInvoice] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_CommercialInvoice] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GhiChuKhac] = @GhiChuKhac,
			[FileName] = @FileName,
			[Content] = @Content,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_CommercialInvoice]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GhiChuKhac],
			[FileName],
			[Content],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GhiChuKhac,
			@FileName,
			@Content,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_CommercialInvoice]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_CommercialInvoice] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_CommercialInvoice]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_CommercialInvoice] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_CommercialInvoice]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectBy_CommercialInvoice_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectBy_CommercialInvoice_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_DeleteBy_CommercialInvoice_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_DeleteBy_CommercialInvoice_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Insert]
	@CommercialInvoice_ID bigint,
	@SoHoaDonTM nvarchar(50),
	@NgayPhatHanhHDTM datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_CommercialInvoice_Details]
(
	[CommercialInvoice_ID],
	[SoHoaDonTM],
	[NgayPhatHanhHDTM]
)
VALUES 
(
	@CommercialInvoice_ID,
	@SoHoaDonTM,
	@NgayPhatHanhHDTM
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Update]
	@ID bigint,
	@CommercialInvoice_ID bigint,
	@SoHoaDonTM nvarchar(50),
	@NgayPhatHanhHDTM datetime
AS

UPDATE
	[dbo].[t_KDT_VNACCS_CommercialInvoice_Details]
SET
	[CommercialInvoice_ID] = @CommercialInvoice_ID,
	[SoHoaDonTM] = @SoHoaDonTM,
	[NgayPhatHanhHDTM] = @NgayPhatHanhHDTM
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_InsertUpdate]
	@ID bigint,
	@CommercialInvoice_ID bigint,
	@SoHoaDonTM nvarchar(50),
	@NgayPhatHanhHDTM datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_CommercialInvoice_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_CommercialInvoice_Details] 
		SET
			[CommercialInvoice_ID] = @CommercialInvoice_ID,
			[SoHoaDonTM] = @SoHoaDonTM,
			[NgayPhatHanhHDTM] = @NgayPhatHanhHDTM
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_CommercialInvoice_Details]
		(
			[CommercialInvoice_ID],
			[SoHoaDonTM],
			[NgayPhatHanhHDTM]
		)
		VALUES 
		(
			@CommercialInvoice_ID,
			@SoHoaDonTM,
			@NgayPhatHanhHDTM
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_CommercialInvoice_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_DeleteBy_CommercialInvoice_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_DeleteBy_CommercialInvoice_ID]
	@CommercialInvoice_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_CommercialInvoice_Details]
WHERE
	[CommercialInvoice_ID] = @CommercialInvoice_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_CommercialInvoice_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CommercialInvoice_ID],
	[SoHoaDonTM],
	[NgayPhatHanhHDTM]
FROM
	[dbo].[t_KDT_VNACCS_CommercialInvoice_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectBy_CommercialInvoice_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectBy_CommercialInvoice_ID]
	@CommercialInvoice_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CommercialInvoice_ID],
	[SoHoaDonTM],
	[NgayPhatHanhHDTM]
FROM
	[dbo].[t_KDT_VNACCS_CommercialInvoice_Details]
WHERE
	[CommercialInvoice_ID] = @CommercialInvoice_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[CommercialInvoice_ID],
	[SoHoaDonTM],
	[NgayPhatHanhHDTM]
FROM [dbo].[t_KDT_VNACCS_CommercialInvoice_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_Detail_SelectAll]




AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CommercialInvoice_ID],
	[SoHoaDonTM],
	[NgayPhatHanhHDTM]
FROM
	[dbo].[t_KDT_VNACCS_CommercialInvoice_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Detail_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_Container_Details]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GhiChuKhac,
	@FileName,
	@Content,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Detail_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_Container_Details]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GhiChuKhac] = @GhiChuKhac,
	[FileName] = @FileName,
	[Content] = @Content,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Detail_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_Container_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_Container_Details] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GhiChuKhac] = @GhiChuKhac,
			[FileName] = @FileName,
			[Content] = @Content,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_Container_Details]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GhiChuKhac],
			[FileName],
			[Content],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GhiChuKhac,
			@FileName,
			@Content,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Detail_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_Container_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Detail_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_Container_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Detail_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_Container_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Detail_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_Container_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Detail_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Detail_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_Container_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_ContractDocument]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GhiChuKhac,
	@FileName,
	@Content,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_ContractDocument]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GhiChuKhac] = @GhiChuKhac,
	[FileName] = @FileName,
	[Content] = @Content,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_ContractDocument] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_ContractDocument] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GhiChuKhac] = @GhiChuKhac,
			[FileName] = @FileName,
			[Content] = @Content,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_ContractDocument]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GhiChuKhac],
			[FileName],
			[Content],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GhiChuKhac,
			@FileName,
			@Content,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_ContractDocument]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_ContractDocument] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_ContractDocument]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_ContractDocument] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_ContractDocument]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectBy_ContractDocument_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectBy_ContractDocument_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_Detail_DeleteBy_ContractDocument_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_DeleteBy_ContractDocument_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Insert]
	@ContractDocument_ID bigint,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@ThoiHanThanhToan datetime,
	@TongTriGia numeric(21, 6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_ContractDocument_Details]
(
	[ContractDocument_ID],
	[SoHopDong],
	[NgayHopDong],
	[ThoiHanThanhToan],
	[TongTriGia]
)
VALUES 
(
	@ContractDocument_ID,
	@SoHopDong,
	@NgayHopDong,
	@ThoiHanThanhToan,
	@TongTriGia
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Update]
	@ID bigint,
	@ContractDocument_ID bigint,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@ThoiHanThanhToan datetime,
	@TongTriGia numeric(21, 6)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_ContractDocument_Details]
SET
	[ContractDocument_ID] = @ContractDocument_ID,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[ThoiHanThanhToan] = @ThoiHanThanhToan,
	[TongTriGia] = @TongTriGia
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Detail_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_InsertUpdate]
	@ID bigint,
	@ContractDocument_ID bigint,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@ThoiHanThanhToan datetime,
	@TongTriGia numeric(21, 6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_ContractDocument_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_ContractDocument_Details] 
		SET
			[ContractDocument_ID] = @ContractDocument_ID,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[ThoiHanThanhToan] = @ThoiHanThanhToan,
			[TongTriGia] = @TongTriGia
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_ContractDocument_Details]
		(
			[ContractDocument_ID],
			[SoHopDong],
			[NgayHopDong],
			[ThoiHanThanhToan],
			[TongTriGia]
		)
		VALUES 
		(
			@ContractDocument_ID,
			@SoHopDong,
			@NgayHopDong,
			@ThoiHanThanhToan,
			@TongTriGia
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_ContractDocument_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Detail_DeleteBy_ContractDocument_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_DeleteBy_ContractDocument_ID]
	@ContractDocument_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_ContractDocument_Details]
WHERE
	[ContractDocument_ID] = @ContractDocument_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Detail_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_ContractDocument_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ContractDocument_ID],
	[SoHopDong],
	[NgayHopDong],
	[ThoiHanThanhToan],
	[TongTriGia]
FROM
	[dbo].[t_KDT_VNACCS_ContractDocument_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectBy_ContractDocument_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectBy_ContractDocument_ID]
	@ContractDocument_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ContractDocument_ID],
	[SoHopDong],
	[NgayHopDong],
	[ThoiHanThanhToan],
	[TongTriGia]
FROM
	[dbo].[t_KDT_VNACCS_ContractDocument_Details]
WHERE
	[ContractDocument_ID] = @ContractDocument_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ContractDocument_ID],
	[SoHopDong],
	[NgayHopDong],
	[ThoiHanThanhToan],
	[TongTriGia]
FROM [dbo].[t_KDT_VNACCS_ContractDocument_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_Detail_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ContractDocument_ID],
	[SoHopDong],
	[NgayHopDong],
	[ThoiHanThanhToan],
	[TongTriGia]
FROM
	[dbo].[t_KDT_VNACCS_ContractDocument_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_License]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GhiChuKhac,
	@FileName,
	@Content,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_License]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GhiChuKhac] = @GhiChuKhac,
	[FileName] = @FileName,
	[Content] = @Content,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_License] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_License] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GhiChuKhac] = @GhiChuKhac,
			[FileName] = @FileName,
			[Content] = @Content,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_License]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GhiChuKhac],
			[FileName],
			[Content],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GhiChuKhac,
			@FileName,
			@Content,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_License]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_License] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_License]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_License] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_License]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Detail_SelectBy_License_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_SelectBy_License_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_Detail_DeleteBy_License_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_DeleteBy_License_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Detail_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_Insert]
	@License_ID bigint,
	@NguoiCapGP nvarchar(255),
	@SoGP nvarchar(35),
	@NgayCapGP datetime,
	@NoiCapGP nvarchar(255),
	@LoaiGP nvarchar(4),
	@NgayHetHanGP datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_License_Details]
(
	[License_ID],
	[NguoiCapGP],
	[SoGP],
	[NgayCapGP],
	[NoiCapGP],
	[LoaiGP],
	[NgayHetHanGP]
)
VALUES 
(
	@License_ID,
	@NguoiCapGP,
	@SoGP,
	@NgayCapGP,
	@NoiCapGP,
	@LoaiGP,
	@NgayHetHanGP
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Detail_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_Update]
	@ID bigint,
	@License_ID bigint,
	@NguoiCapGP nvarchar(255),
	@SoGP nvarchar(35),
	@NgayCapGP datetime,
	@NoiCapGP nvarchar(255),
	@LoaiGP nvarchar(4),
	@NgayHetHanGP datetime
AS

UPDATE
	[dbo].[t_KDT_VNACCS_License_Details]
SET
	[License_ID] = @License_ID,
	[NguoiCapGP] = @NguoiCapGP,
	[SoGP] = @SoGP,
	[NgayCapGP] = @NgayCapGP,
	[NoiCapGP] = @NoiCapGP,
	[LoaiGP] = @LoaiGP,
	[NgayHetHanGP] = @NgayHetHanGP
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Detail_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_InsertUpdate]
	@ID bigint,
	@License_ID bigint,
	@NguoiCapGP nvarchar(255),
	@SoGP nvarchar(35),
	@NgayCapGP datetime,
	@NoiCapGP nvarchar(255),
	@LoaiGP nvarchar(4),
	@NgayHetHanGP datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_License_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_License_Details] 
		SET
			[License_ID] = @License_ID,
			[NguoiCapGP] = @NguoiCapGP,
			[SoGP] = @SoGP,
			[NgayCapGP] = @NgayCapGP,
			[NoiCapGP] = @NoiCapGP,
			[LoaiGP] = @LoaiGP,
			[NgayHetHanGP] = @NgayHetHanGP
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_License_Details]
		(
			[License_ID],
			[NguoiCapGP],
			[SoGP],
			[NgayCapGP],
			[NoiCapGP],
			[LoaiGP],
			[NgayHetHanGP]
		)
		VALUES 
		(
			@License_ID,
			@NguoiCapGP,
			@SoGP,
			@NgayCapGP,
			@NoiCapGP,
			@LoaiGP,
			@NgayHetHanGP
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Detail_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_License_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Detail_DeleteBy_License_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_DeleteBy_License_ID]
	@License_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_License_Details]
WHERE
	[License_ID] = @License_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Detail_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_License_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Detail_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[License_ID],
	[NguoiCapGP],
	[SoGP],
	[NgayCapGP],
	[NoiCapGP],
	[LoaiGP],
	[NgayHetHanGP]
FROM
	[dbo].[t_KDT_VNACCS_License_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Detail_SelectBy_License_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_SelectBy_License_ID]
	@License_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[License_ID],
	[NguoiCapGP],
	[SoGP],
	[NgayCapGP],
	[NoiCapGP],
	[LoaiGP],
	[NgayHetHanGP]
FROM
	[dbo].[t_KDT_VNACCS_License_Details]
WHERE
	[License_ID] = @License_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Detail_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[License_ID],
	[NguoiCapGP],
	[SoGP],
	[NgayCapGP],
	[NoiCapGP],
	[LoaiGP],
	[NgayHetHanGP]
FROM [dbo].[t_KDT_VNACCS_License_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_Detail_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 1, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_Detail_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[License_ID],
	[NguoiCapGP],
	[SoGP],
	[NgayCapGP],
	[NoiCapGP],
	[LoaiGP],
	[NgayHetHanGP]
FROM
	[dbo].[t_KDT_VNACCS_License_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OverTime_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OverTime_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OverTime_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OverTime_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OverTime_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OverTime_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OverTime_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OverTime_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OverTime_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@NgayDangKy datetime,
	@GioLamThuTuc nvarchar(6),
	@NoiDung nvarchar(200),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_OverTime]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NgayDangKy],
	[GioLamThuTuc],
	[NoiDung],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@NgayDangKy,
	@GioLamThuTuc,
	@NoiDung,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OverTime_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@NgayDangKy datetime,
	@GioLamThuTuc nvarchar(6),
	@NoiDung nvarchar(200),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_OverTime]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[NgayDangKy] = @NgayDangKy,
	[GioLamThuTuc] = @GioLamThuTuc,
	[NoiDung] = @NoiDung,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OverTime_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@NgayDangKy datetime,
	@GioLamThuTuc nvarchar(6),
	@NoiDung nvarchar(200),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_OverTime] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_OverTime] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[NgayDangKy] = @NgayDangKy,
			[GioLamThuTuc] = @GioLamThuTuc,
			[NoiDung] = @NoiDung,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_OverTime]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[NgayDangKy],
			[GioLamThuTuc],
			[NoiDung],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@NgayDangKy,
			@GioLamThuTuc,
			@NoiDung,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OverTime_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_OverTime]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OverTime_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_OverTime] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OverTime_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NgayDangKy],
	[GioLamThuTuc],
	[NoiDung],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_OverTime]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OverTime_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NgayDangKy],
	[GioLamThuTuc],
	[NoiDung],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_OverTime] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OverTime_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OverTime_SelectAll]










AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NgayDangKy],
	[GioLamThuTuc],
	[NoiDung],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_OverTime]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '23.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('23.8',GETDATE(), N'CẬP NHẬT KHAI BÁO CHỨNG TỪ VNACCS')
END