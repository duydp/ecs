using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangMauDich_PhanHoi : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string SoDong { set; get; }
		public string MaPhanLoaiTaiXacNhanGia { set; get; }
		public string TenNoiXuatXu { set; get; }
		public decimal SoLuongTinhThue { set; get; }
		public string MaDVTDanhThue { set; get; }
		public decimal DonGiaTinhThue { set; get; }
		public string DV_SL_TrongDonGiaTinhThue { set; get; }
		public string MaTTDonGiaTinhThue { set; get; }
		public decimal TriGiaTinhThueS { set; get; }
		public string MaTTTriGiaTinhThueS { set; get; }
		public string MaTTSoTienMienGiam { set; get; }
		public string MaPhanLoaiThueSuatThue { set; get; }
		public string ThueSuatThue { set; get; }
		public string PhanLoaiThueSuatThue { set; get; }
		public decimal SoTienThue { set; get; }
		public string MaTTSoTienThueXuatKhau { set; get; }
		public string TienLePhi_DonGia { set; get; }
		public string TienBaoHiem_DonGia { set; get; }
		public decimal TienLePhi_SoLuong { set; get; }
		public string TienLePhi_MaDVSoLuong { set; get; }
		public decimal TienBaoHiem_SoLuong { set; get; }
		public string TienBaoHiem_MaDVSoLuong { set; get; }
		public decimal TienLePhi_KhoanTien { set; get; }
		public decimal TienBaoHiem_KhoanTien { set; get; }
		public string DieuKhoanMienGiam { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HangMauDich_PhanHoi> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HangMauDich_PhanHoi> collection = new List<KDT_VNACC_HangMauDich_PhanHoi>();
			while (reader.Read())
			{
				KDT_VNACC_HangMauDich_PhanHoi entity = new KDT_VNACC_HangMauDich_PhanHoi();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDong"))) entity.SoDong = reader.GetString(reader.GetOrdinal("SoDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiTaiXacNhanGia"))) entity.MaPhanLoaiTaiXacNhanGia = reader.GetString(reader.GetOrdinal("MaPhanLoaiTaiXacNhanGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNoiXuatXu"))) entity.TenNoiXuatXu = reader.GetString(reader.GetOrdinal("TenNoiXuatXu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTinhThue"))) entity.SoLuongTinhThue = reader.GetDecimal(reader.GetOrdinal("SoLuongTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVTDanhThue"))) entity.MaDVTDanhThue = reader.GetString(reader.GetOrdinal("MaDVTDanhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTinhThue"))) entity.DonGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("DonGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("DV_SL_TrongDonGiaTinhThue"))) entity.DV_SL_TrongDonGiaTinhThue = reader.GetString(reader.GetOrdinal("DV_SL_TrongDonGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTDonGiaTinhThue"))) entity.MaTTDonGiaTinhThue = reader.GetString(reader.GetOrdinal("MaTTDonGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThueS"))) entity.TriGiaTinhThueS = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThueS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTTriGiaTinhThueS"))) entity.MaTTTriGiaTinhThueS = reader.GetString(reader.GetOrdinal("MaTTTriGiaTinhThueS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTSoTienMienGiam"))) entity.MaTTSoTienMienGiam = reader.GetString(reader.GetOrdinal("MaTTSoTienMienGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiThueSuatThue"))) entity.MaPhanLoaiThueSuatThue = reader.GetString(reader.GetOrdinal("MaPhanLoaiThueSuatThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatThue"))) entity.ThueSuatThue = reader.GetString(reader.GetOrdinal("ThueSuatThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiThueSuatThue"))) entity.PhanLoaiThueSuatThue = reader.GetString(reader.GetOrdinal("PhanLoaiThueSuatThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienThue"))) entity.SoTienThue = reader.GetDecimal(reader.GetOrdinal("SoTienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTSoTienThueXuatKhau"))) entity.MaTTSoTienThueXuatKhau = reader.GetString(reader.GetOrdinal("MaTTSoTienThueXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_DonGia"))) entity.TienLePhi_DonGia = reader.GetString(reader.GetOrdinal("TienLePhi_DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_DonGia"))) entity.TienBaoHiem_DonGia = reader.GetString(reader.GetOrdinal("TienBaoHiem_DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_SoLuong"))) entity.TienLePhi_SoLuong = reader.GetDecimal(reader.GetOrdinal("TienLePhi_SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_MaDVSoLuong"))) entity.TienLePhi_MaDVSoLuong = reader.GetString(reader.GetOrdinal("TienLePhi_MaDVSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_SoLuong"))) entity.TienBaoHiem_SoLuong = reader.GetDecimal(reader.GetOrdinal("TienBaoHiem_SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_MaDVSoLuong"))) entity.TienBaoHiem_MaDVSoLuong = reader.GetString(reader.GetOrdinal("TienBaoHiem_MaDVSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_KhoanTien"))) entity.TienLePhi_KhoanTien = reader.GetDecimal(reader.GetOrdinal("TienLePhi_KhoanTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_KhoanTien"))) entity.TienBaoHiem_KhoanTien = reader.GetDecimal(reader.GetOrdinal("TienBaoHiem_KhoanTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DieuKhoanMienGiam"))) entity.DieuKhoanMienGiam = reader.GetString(reader.GetOrdinal("DieuKhoanMienGiam"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HangMauDich_PhanHoi> collection, long id)
        {
            foreach (KDT_VNACC_HangMauDich_PhanHoi item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangMauDich_PhanHoi VALUES(@Master_ID, @SoDong, @MaPhanLoaiTaiXacNhanGia, @TenNoiXuatXu, @SoLuongTinhThue, @MaDVTDanhThue, @DonGiaTinhThue, @DV_SL_TrongDonGiaTinhThue, @MaTTDonGiaTinhThue, @TriGiaTinhThueS, @MaTTTriGiaTinhThueS, @MaTTSoTienMienGiam, @MaPhanLoaiThueSuatThue, @ThueSuatThue, @PhanLoaiThueSuatThue, @SoTienThue, @MaTTSoTienThueXuatKhau, @TienLePhi_DonGia, @TienBaoHiem_DonGia, @TienLePhi_SoLuong, @TienLePhi_MaDVSoLuong, @TienBaoHiem_SoLuong, @TienBaoHiem_MaDVSoLuong, @TienLePhi_KhoanTien, @TienBaoHiem_KhoanTien, @DieuKhoanMienGiam)";
            string update = "UPDATE t_KDT_VNACC_HangMauDich_PhanHoi SET Master_ID = @Master_ID, SoDong = @SoDong, MaPhanLoaiTaiXacNhanGia = @MaPhanLoaiTaiXacNhanGia, TenNoiXuatXu = @TenNoiXuatXu, SoLuongTinhThue = @SoLuongTinhThue, MaDVTDanhThue = @MaDVTDanhThue, DonGiaTinhThue = @DonGiaTinhThue, DV_SL_TrongDonGiaTinhThue = @DV_SL_TrongDonGiaTinhThue, MaTTDonGiaTinhThue = @MaTTDonGiaTinhThue, TriGiaTinhThueS = @TriGiaTinhThueS, MaTTTriGiaTinhThueS = @MaTTTriGiaTinhThueS, MaTTSoTienMienGiam = @MaTTSoTienMienGiam, MaPhanLoaiThueSuatThue = @MaPhanLoaiThueSuatThue, ThueSuatThue = @ThueSuatThue, PhanLoaiThueSuatThue = @PhanLoaiThueSuatThue, SoTienThue = @SoTienThue, MaTTSoTienThueXuatKhau = @MaTTSoTienThueXuatKhau, TienLePhi_DonGia = @TienLePhi_DonGia, TienBaoHiem_DonGia = @TienBaoHiem_DonGia, TienLePhi_SoLuong = @TienLePhi_SoLuong, TienLePhi_MaDVSoLuong = @TienLePhi_MaDVSoLuong, TienBaoHiem_SoLuong = @TienBaoHiem_SoLuong, TienBaoHiem_MaDVSoLuong = @TienBaoHiem_MaDVSoLuong, TienLePhi_KhoanTien = @TienLePhi_KhoanTien, TienBaoHiem_KhoanTien = @TienBaoHiem_KhoanTien, DieuKhoanMienGiam = @DieuKhoanMienGiam WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangMauDich_PhanHoi WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, "MaPhanLoaiTaiXacNhanGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNoiXuatXu", SqlDbType.VarChar, "TenNoiXuatXu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThue", SqlDbType.Decimal, "SoLuongTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTDanhThue", SqlDbType.VarChar, "MaDVTDanhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTinhThue", SqlDbType.Decimal, "DonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, "DV_SL_TrongDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, "MaTTDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, "TriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, "MaTTTriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, "MaTTSoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, "MaPhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatThue", SqlDbType.VarChar, "ThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, "PhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThue", SqlDbType.Decimal, "SoTienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, "MaTTSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, "TienLePhi_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, "TienBaoHiem_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, "TienLePhi_SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, "TienLePhi_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, "TienBaoHiem_SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, "TienBaoHiem_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, "TienLePhi_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, "TienBaoHiem_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, "DieuKhoanMienGiam", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, "MaPhanLoaiTaiXacNhanGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNoiXuatXu", SqlDbType.VarChar, "TenNoiXuatXu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThue", SqlDbType.Decimal, "SoLuongTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTDanhThue", SqlDbType.VarChar, "MaDVTDanhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTinhThue", SqlDbType.Decimal, "DonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, "DV_SL_TrongDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, "MaTTDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, "TriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, "MaTTTriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, "MaTTSoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, "MaPhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatThue", SqlDbType.VarChar, "ThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, "PhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThue", SqlDbType.Decimal, "SoTienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, "MaTTSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, "TienLePhi_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, "TienBaoHiem_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, "TienLePhi_SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, "TienLePhi_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, "TienBaoHiem_SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, "TienBaoHiem_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, "TienLePhi_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, "TienBaoHiem_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, "DieuKhoanMienGiam", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangMauDich_PhanHoi VALUES(@Master_ID, @SoDong, @MaPhanLoaiTaiXacNhanGia, @TenNoiXuatXu, @SoLuongTinhThue, @MaDVTDanhThue, @DonGiaTinhThue, @DV_SL_TrongDonGiaTinhThue, @MaTTDonGiaTinhThue, @TriGiaTinhThueS, @MaTTTriGiaTinhThueS, @MaTTSoTienMienGiam, @MaPhanLoaiThueSuatThue, @ThueSuatThue, @PhanLoaiThueSuatThue, @SoTienThue, @MaTTSoTienThueXuatKhau, @TienLePhi_DonGia, @TienBaoHiem_DonGia, @TienLePhi_SoLuong, @TienLePhi_MaDVSoLuong, @TienBaoHiem_SoLuong, @TienBaoHiem_MaDVSoLuong, @TienLePhi_KhoanTien, @TienBaoHiem_KhoanTien, @DieuKhoanMienGiam)";
            string update = "UPDATE t_KDT_VNACC_HangMauDich_PhanHoi SET Master_ID = @Master_ID, SoDong = @SoDong, MaPhanLoaiTaiXacNhanGia = @MaPhanLoaiTaiXacNhanGia, TenNoiXuatXu = @TenNoiXuatXu, SoLuongTinhThue = @SoLuongTinhThue, MaDVTDanhThue = @MaDVTDanhThue, DonGiaTinhThue = @DonGiaTinhThue, DV_SL_TrongDonGiaTinhThue = @DV_SL_TrongDonGiaTinhThue, MaTTDonGiaTinhThue = @MaTTDonGiaTinhThue, TriGiaTinhThueS = @TriGiaTinhThueS, MaTTTriGiaTinhThueS = @MaTTTriGiaTinhThueS, MaTTSoTienMienGiam = @MaTTSoTienMienGiam, MaPhanLoaiThueSuatThue = @MaPhanLoaiThueSuatThue, ThueSuatThue = @ThueSuatThue, PhanLoaiThueSuatThue = @PhanLoaiThueSuatThue, SoTienThue = @SoTienThue, MaTTSoTienThueXuatKhau = @MaTTSoTienThueXuatKhau, TienLePhi_DonGia = @TienLePhi_DonGia, TienBaoHiem_DonGia = @TienBaoHiem_DonGia, TienLePhi_SoLuong = @TienLePhi_SoLuong, TienLePhi_MaDVSoLuong = @TienLePhi_MaDVSoLuong, TienBaoHiem_SoLuong = @TienBaoHiem_SoLuong, TienBaoHiem_MaDVSoLuong = @TienBaoHiem_MaDVSoLuong, TienLePhi_KhoanTien = @TienLePhi_KhoanTien, TienBaoHiem_KhoanTien = @TienBaoHiem_KhoanTien, DieuKhoanMienGiam = @DieuKhoanMienGiam WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangMauDich_PhanHoi WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, "MaPhanLoaiTaiXacNhanGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNoiXuatXu", SqlDbType.VarChar, "TenNoiXuatXu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThue", SqlDbType.Decimal, "SoLuongTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTDanhThue", SqlDbType.VarChar, "MaDVTDanhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTinhThue", SqlDbType.Decimal, "DonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, "DV_SL_TrongDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, "MaTTDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, "TriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, "MaTTTriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, "MaTTSoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, "MaPhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatThue", SqlDbType.VarChar, "ThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, "PhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThue", SqlDbType.Decimal, "SoTienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, "MaTTSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, "TienLePhi_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, "TienBaoHiem_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, "TienLePhi_SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, "TienLePhi_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, "TienBaoHiem_SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, "TienBaoHiem_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, "TienLePhi_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, "TienBaoHiem_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, "DieuKhoanMienGiam", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, "MaPhanLoaiTaiXacNhanGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNoiXuatXu", SqlDbType.VarChar, "TenNoiXuatXu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThue", SqlDbType.Decimal, "SoLuongTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTDanhThue", SqlDbType.VarChar, "MaDVTDanhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTinhThue", SqlDbType.Decimal, "DonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, "DV_SL_TrongDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, "MaTTDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, "TriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, "MaTTTriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, "MaTTSoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, "MaPhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatThue", SqlDbType.VarChar, "ThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, "PhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThue", SqlDbType.Decimal, "SoTienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, "MaTTSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, "TienLePhi_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, "TienBaoHiem_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, "TienLePhi_SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, "TienLePhi_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, "TienBaoHiem_SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, "TienBaoHiem_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, "TienLePhi_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, "TienBaoHiem_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, "DieuKhoanMienGiam", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HangMauDich_PhanHoi Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HangMauDich_PhanHoi> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_HangMauDich_PhanHoi> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_HangMauDich_PhanHoi> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_HangMauDich_PhanHoi(long master_ID, string soDong, string maPhanLoaiTaiXacNhanGia, string tenNoiXuatXu, decimal soLuongTinhThue, string maDVTDanhThue, decimal donGiaTinhThue, string dV_SL_TrongDonGiaTinhThue, string maTTDonGiaTinhThue, decimal triGiaTinhThueS, string maTTTriGiaTinhThueS, string maTTSoTienMienGiam, string maPhanLoaiThueSuatThue, string thueSuatThue, string phanLoaiThueSuatThue, decimal soTienThue, string maTTSoTienThueXuatKhau, string tienLePhi_DonGia, string tienBaoHiem_DonGia, decimal tienLePhi_SoLuong, string tienLePhi_MaDVSoLuong, decimal tienBaoHiem_SoLuong, string tienBaoHiem_MaDVSoLuong, decimal tienLePhi_KhoanTien, decimal tienBaoHiem_KhoanTien, string dieuKhoanMienGiam)
		{
			KDT_VNACC_HangMauDich_PhanHoi entity = new KDT_VNACC_HangMauDich_PhanHoi();	
			entity.Master_ID = master_ID;
			entity.SoDong = soDong;
			entity.MaPhanLoaiTaiXacNhanGia = maPhanLoaiTaiXacNhanGia;
			entity.TenNoiXuatXu = tenNoiXuatXu;
			entity.SoLuongTinhThue = soLuongTinhThue;
			entity.MaDVTDanhThue = maDVTDanhThue;
			entity.DonGiaTinhThue = donGiaTinhThue;
			entity.DV_SL_TrongDonGiaTinhThue = dV_SL_TrongDonGiaTinhThue;
			entity.MaTTDonGiaTinhThue = maTTDonGiaTinhThue;
			entity.TriGiaTinhThueS = triGiaTinhThueS;
			entity.MaTTTriGiaTinhThueS = maTTTriGiaTinhThueS;
			entity.MaTTSoTienMienGiam = maTTSoTienMienGiam;
			entity.MaPhanLoaiThueSuatThue = maPhanLoaiThueSuatThue;
			entity.ThueSuatThue = thueSuatThue;
			entity.PhanLoaiThueSuatThue = phanLoaiThueSuatThue;
			entity.SoTienThue = soTienThue;
			entity.MaTTSoTienThueXuatKhau = maTTSoTienThueXuatKhau;
			entity.TienLePhi_DonGia = tienLePhi_DonGia;
			entity.TienBaoHiem_DonGia = tienBaoHiem_DonGia;
			entity.TienLePhi_SoLuong = tienLePhi_SoLuong;
			entity.TienLePhi_MaDVSoLuong = tienLePhi_MaDVSoLuong;
			entity.TienBaoHiem_SoLuong = tienBaoHiem_SoLuong;
			entity.TienBaoHiem_MaDVSoLuong = tienBaoHiem_MaDVSoLuong;
			entity.TienLePhi_KhoanTien = tienLePhi_KhoanTien;
			entity.TienBaoHiem_KhoanTien = tienBaoHiem_KhoanTien;
			entity.DieuKhoanMienGiam = dieuKhoanMienGiam;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, MaPhanLoaiTaiXacNhanGia);
			db.AddInParameter(dbCommand, "@TenNoiXuatXu", SqlDbType.VarChar, TenNoiXuatXu);
			db.AddInParameter(dbCommand, "@SoLuongTinhThue", SqlDbType.Decimal, SoLuongTinhThue);
			db.AddInParameter(dbCommand, "@MaDVTDanhThue", SqlDbType.VarChar, MaDVTDanhThue);
			db.AddInParameter(dbCommand, "@DonGiaTinhThue", SqlDbType.Decimal, DonGiaTinhThue);
			db.AddInParameter(dbCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, DV_SL_TrongDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, MaTTDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, TriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, MaTTTriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, MaTTSoTienMienGiam);
			db.AddInParameter(dbCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, MaPhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@ThueSuatThue", SqlDbType.VarChar, ThueSuatThue);
			db.AddInParameter(dbCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, PhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@SoTienThue", SqlDbType.Decimal, SoTienThue);
			db.AddInParameter(dbCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, MaTTSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, TienLePhi_DonGia);
			db.AddInParameter(dbCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, TienBaoHiem_DonGia);
			db.AddInParameter(dbCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, TienLePhi_SoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, TienLePhi_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, TienBaoHiem_SoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, TienBaoHiem_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, TienLePhi_KhoanTien);
			db.AddInParameter(dbCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, TienBaoHiem_KhoanTien);
			db.AddInParameter(dbCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, DieuKhoanMienGiam);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_HangMauDich_PhanHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_PhanHoi item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HangMauDich_PhanHoi(long id, long master_ID, string soDong, string maPhanLoaiTaiXacNhanGia, string tenNoiXuatXu, decimal soLuongTinhThue, string maDVTDanhThue, decimal donGiaTinhThue, string dV_SL_TrongDonGiaTinhThue, string maTTDonGiaTinhThue, decimal triGiaTinhThueS, string maTTTriGiaTinhThueS, string maTTSoTienMienGiam, string maPhanLoaiThueSuatThue, string thueSuatThue, string phanLoaiThueSuatThue, decimal soTienThue, string maTTSoTienThueXuatKhau, string tienLePhi_DonGia, string tienBaoHiem_DonGia, decimal tienLePhi_SoLuong, string tienLePhi_MaDVSoLuong, decimal tienBaoHiem_SoLuong, string tienBaoHiem_MaDVSoLuong, decimal tienLePhi_KhoanTien, decimal tienBaoHiem_KhoanTien, string dieuKhoanMienGiam)
		{
			KDT_VNACC_HangMauDich_PhanHoi entity = new KDT_VNACC_HangMauDich_PhanHoi();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SoDong = soDong;
			entity.MaPhanLoaiTaiXacNhanGia = maPhanLoaiTaiXacNhanGia;
			entity.TenNoiXuatXu = tenNoiXuatXu;
			entity.SoLuongTinhThue = soLuongTinhThue;
			entity.MaDVTDanhThue = maDVTDanhThue;
			entity.DonGiaTinhThue = donGiaTinhThue;
			entity.DV_SL_TrongDonGiaTinhThue = dV_SL_TrongDonGiaTinhThue;
			entity.MaTTDonGiaTinhThue = maTTDonGiaTinhThue;
			entity.TriGiaTinhThueS = triGiaTinhThueS;
			entity.MaTTTriGiaTinhThueS = maTTTriGiaTinhThueS;
			entity.MaTTSoTienMienGiam = maTTSoTienMienGiam;
			entity.MaPhanLoaiThueSuatThue = maPhanLoaiThueSuatThue;
			entity.ThueSuatThue = thueSuatThue;
			entity.PhanLoaiThueSuatThue = phanLoaiThueSuatThue;
			entity.SoTienThue = soTienThue;
			entity.MaTTSoTienThueXuatKhau = maTTSoTienThueXuatKhau;
			entity.TienLePhi_DonGia = tienLePhi_DonGia;
			entity.TienBaoHiem_DonGia = tienBaoHiem_DonGia;
			entity.TienLePhi_SoLuong = tienLePhi_SoLuong;
			entity.TienLePhi_MaDVSoLuong = tienLePhi_MaDVSoLuong;
			entity.TienBaoHiem_SoLuong = tienBaoHiem_SoLuong;
			entity.TienBaoHiem_MaDVSoLuong = tienBaoHiem_MaDVSoLuong;
			entity.TienLePhi_KhoanTien = tienLePhi_KhoanTien;
			entity.TienBaoHiem_KhoanTien = tienBaoHiem_KhoanTien;
			entity.DieuKhoanMienGiam = dieuKhoanMienGiam;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, MaPhanLoaiTaiXacNhanGia);
			db.AddInParameter(dbCommand, "@TenNoiXuatXu", SqlDbType.VarChar, TenNoiXuatXu);
			db.AddInParameter(dbCommand, "@SoLuongTinhThue", SqlDbType.Decimal, SoLuongTinhThue);
			db.AddInParameter(dbCommand, "@MaDVTDanhThue", SqlDbType.VarChar, MaDVTDanhThue);
			db.AddInParameter(dbCommand, "@DonGiaTinhThue", SqlDbType.Decimal, DonGiaTinhThue);
			db.AddInParameter(dbCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, DV_SL_TrongDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, MaTTDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, TriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, MaTTTriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, MaTTSoTienMienGiam);
			db.AddInParameter(dbCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, MaPhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@ThueSuatThue", SqlDbType.VarChar, ThueSuatThue);
			db.AddInParameter(dbCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, PhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@SoTienThue", SqlDbType.Decimal, SoTienThue);
			db.AddInParameter(dbCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, MaTTSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, TienLePhi_DonGia);
			db.AddInParameter(dbCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, TienBaoHiem_DonGia);
			db.AddInParameter(dbCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, TienLePhi_SoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, TienLePhi_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, TienBaoHiem_SoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, TienBaoHiem_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, TienLePhi_KhoanTien);
			db.AddInParameter(dbCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, TienBaoHiem_KhoanTien);
			db.AddInParameter(dbCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, DieuKhoanMienGiam);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_HangMauDich_PhanHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_PhanHoi item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HangMauDich_PhanHoi(long id, long master_ID, string soDong, string maPhanLoaiTaiXacNhanGia, string tenNoiXuatXu, decimal soLuongTinhThue, string maDVTDanhThue, decimal donGiaTinhThue, string dV_SL_TrongDonGiaTinhThue, string maTTDonGiaTinhThue, decimal triGiaTinhThueS, string maTTTriGiaTinhThueS, string maTTSoTienMienGiam, string maPhanLoaiThueSuatThue, string thueSuatThue, string phanLoaiThueSuatThue, decimal soTienThue, string maTTSoTienThueXuatKhau, string tienLePhi_DonGia, string tienBaoHiem_DonGia, decimal tienLePhi_SoLuong, string tienLePhi_MaDVSoLuong, decimal tienBaoHiem_SoLuong, string tienBaoHiem_MaDVSoLuong, decimal tienLePhi_KhoanTien, decimal tienBaoHiem_KhoanTien, string dieuKhoanMienGiam)
		{
			KDT_VNACC_HangMauDich_PhanHoi entity = new KDT_VNACC_HangMauDich_PhanHoi();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SoDong = soDong;
			entity.MaPhanLoaiTaiXacNhanGia = maPhanLoaiTaiXacNhanGia;
			entity.TenNoiXuatXu = tenNoiXuatXu;
			entity.SoLuongTinhThue = soLuongTinhThue;
			entity.MaDVTDanhThue = maDVTDanhThue;
			entity.DonGiaTinhThue = donGiaTinhThue;
			entity.DV_SL_TrongDonGiaTinhThue = dV_SL_TrongDonGiaTinhThue;
			entity.MaTTDonGiaTinhThue = maTTDonGiaTinhThue;
			entity.TriGiaTinhThueS = triGiaTinhThueS;
			entity.MaTTTriGiaTinhThueS = maTTTriGiaTinhThueS;
			entity.MaTTSoTienMienGiam = maTTSoTienMienGiam;
			entity.MaPhanLoaiThueSuatThue = maPhanLoaiThueSuatThue;
			entity.ThueSuatThue = thueSuatThue;
			entity.PhanLoaiThueSuatThue = phanLoaiThueSuatThue;
			entity.SoTienThue = soTienThue;
			entity.MaTTSoTienThueXuatKhau = maTTSoTienThueXuatKhau;
			entity.TienLePhi_DonGia = tienLePhi_DonGia;
			entity.TienBaoHiem_DonGia = tienBaoHiem_DonGia;
			entity.TienLePhi_SoLuong = tienLePhi_SoLuong;
			entity.TienLePhi_MaDVSoLuong = tienLePhi_MaDVSoLuong;
			entity.TienBaoHiem_SoLuong = tienBaoHiem_SoLuong;
			entity.TienBaoHiem_MaDVSoLuong = tienBaoHiem_MaDVSoLuong;
			entity.TienLePhi_KhoanTien = tienLePhi_KhoanTien;
			entity.TienBaoHiem_KhoanTien = tienBaoHiem_KhoanTien;
			entity.DieuKhoanMienGiam = dieuKhoanMienGiam;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, MaPhanLoaiTaiXacNhanGia);
			db.AddInParameter(dbCommand, "@TenNoiXuatXu", SqlDbType.VarChar, TenNoiXuatXu);
			db.AddInParameter(dbCommand, "@SoLuongTinhThue", SqlDbType.Decimal, SoLuongTinhThue);
			db.AddInParameter(dbCommand, "@MaDVTDanhThue", SqlDbType.VarChar, MaDVTDanhThue);
			db.AddInParameter(dbCommand, "@DonGiaTinhThue", SqlDbType.Decimal, DonGiaTinhThue);
			db.AddInParameter(dbCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, DV_SL_TrongDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, MaTTDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, TriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, MaTTTriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, MaTTSoTienMienGiam);
			db.AddInParameter(dbCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, MaPhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@ThueSuatThue", SqlDbType.VarChar, ThueSuatThue);
			db.AddInParameter(dbCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, PhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@SoTienThue", SqlDbType.Decimal, SoTienThue);
			db.AddInParameter(dbCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, MaTTSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, TienLePhi_DonGia);
			db.AddInParameter(dbCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, TienBaoHiem_DonGia);
			db.AddInParameter(dbCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, TienLePhi_SoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, TienLePhi_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, TienBaoHiem_SoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, TienBaoHiem_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, TienLePhi_KhoanTien);
			db.AddInParameter(dbCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, TienBaoHiem_KhoanTien);
			db.AddInParameter(dbCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, DieuKhoanMienGiam);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_HangMauDich_PhanHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_PhanHoi item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HangMauDich_PhanHoi(long id)
		{
			KDT_VNACC_HangMauDich_PhanHoi entity = new KDT_VNACC_HangMauDich_PhanHoi();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_HangMauDich_PhanHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_PhanHoi item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}