﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class VNACC_BranchDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgListVDN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_BranchDetailsForm));
            Janus.Windows.GridEX.GridEXLayout dgListContainer_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageVDN = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListVDN = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteVDN = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDVTLuong1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDVTTrongLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtDiaChiNguoiGuiHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuongHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongTrongLuongHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongContainer = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSTT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTenNguoiNhanHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiNguoiNhanHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiGuiHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDonNhanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnAddVDN = new Janus.Windows.EditControls.UIButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.uiTabPageContainer = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListContainer = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteContainer = new Janus.Windows.EditControls.UIButton();
            this.grbContainer = new Janus.Windows.EditControls.UIGroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtSoSeal = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnAddExcel = new Janus.Windows.EditControls.UIButton();
            this.btnAddContainer = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPageVDN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListVDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            this.uiTabPageContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListContainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbContainer)).BeginInit();
            this.grbContainer.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 675), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 675);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 651);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 651);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Size = new System.Drawing.Size(1037, 675);
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1037, 675);
            this.uiTab1.TabIndex = 2;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageVDN,
            this.uiTabPageContainer});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPageVDN
            // 
            this.uiTabPageVDN.Controls.Add(this.uiGroupBox7);
            this.uiTabPageVDN.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageVDN.Name = "uiTabPageVDN";
            this.uiTabPageVDN.Size = new System.Drawing.Size(1035, 653);
            this.uiTabPageVDN.TabStop = true;
            this.uiTabPageVDN.Text = "Thông tin chi tiết vận đơn nhánh";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.uiGroupBox11);
            this.uiGroupBox7.Controls.Add(this.uiGroupBox1);
            this.uiGroupBox7.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1035, 653);
            this.uiGroupBox7.TabIndex = 2;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.AutoScroll = true;
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.dgListVDN);
            this.uiGroupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox11.Location = new System.Drawing.Point(3, 368);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(1029, 240);
            this.uiGroupBox11.TabIndex = 3;
            this.uiGroupBox11.Text = "Danh sách vận đơn nhánh";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListVDN
            // 
            this.dgListVDN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListVDN.AlternatingColors = true;
            this.dgListVDN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListVDN.ColumnAutoResize = true;
            dgListVDN_DesignTimeLayout.LayoutString = resources.GetString("dgListVDN_DesignTimeLayout.LayoutString");
            this.dgListVDN.DesignTimeLayout = dgListVDN_DesignTimeLayout;
            this.dgListVDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListVDN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListVDN.FrozenColumns = 3;
            this.dgListVDN.GroupByBoxVisible = false;
            this.dgListVDN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListVDN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListVDN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListVDN.Location = new System.Drawing.Point(3, 17);
            this.dgListVDN.Margin = new System.Windows.Forms.Padding(0);
            this.dgListVDN.Name = "dgListVDN";
            this.dgListVDN.RecordNavigator = true;
            this.dgListVDN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListVDN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListVDN.Size = new System.Drawing.Size(1023, 220);
            this.dgListVDN.TabIndex = 15;
            this.dgListVDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListVDN.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListVDN_RowDoubleClick);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnDeleteVDN);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 608);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1029, 42);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(941, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 26;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDeleteVDN
            // 
            this.btnDeleteVDN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteVDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteVDN.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteVDN.Image")));
            this.btnDeleteVDN.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteVDN.Location = new System.Drawing.Point(855, 13);
            this.btnDeleteVDN.Name = "btnDeleteVDN";
            this.btnDeleteVDN.Size = new System.Drawing.Size(80, 23);
            this.btnDeleteVDN.TabIndex = 26;
            this.btnDeleteVDN.Text = "Xóa";
            this.btnDeleteVDN.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteVDN.Click += new System.EventHandler(this.btnDeleteVDN_Click);
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.ctrDVTLuong1);
            this.uiGroupBox9.Controls.Add(this.ctrMaDVTTrongLuong);
            this.uiGroupBox9.Controls.Add(this.txtDiaChiNguoiGuiHang);
            this.uiGroupBox9.Controls.Add(this.txtSoLuongHang);
            this.uiGroupBox9.Controls.Add(this.txtTongTrongLuongHang);
            this.uiGroupBox9.Controls.Add(this.txtSoLuongContainer);
            this.uiGroupBox9.Controls.Add(this.txtSTT);
            this.uiGroupBox9.Controls.Add(this.txtTenNguoiNhanHang);
            this.uiGroupBox9.Controls.Add(this.txtDiaChiNguoiNhanHang);
            this.uiGroupBox9.Controls.Add(this.txtTenNguoiGuiHang);
            this.uiGroupBox9.Controls.Add(this.txtSoVanDonNhanh);
            this.uiGroupBox9.Controls.Add(this.btnAddVDN);
            this.uiGroupBox9.Controls.Add(this.label6);
            this.uiGroupBox9.Controls.Add(this.label25);
            this.uiGroupBox9.Controls.Add(this.label1);
            this.uiGroupBox9.Controls.Add(this.label7);
            this.uiGroupBox9.Controls.Add(this.label22);
            this.uiGroupBox9.Controls.Add(this.label21);
            this.uiGroupBox9.Controls.Add(this.label19);
            this.uiGroupBox9.Controls.Add(this.label20);
            this.uiGroupBox9.Controls.Add(this.label18);
            this.uiGroupBox9.Controls.Add(this.label27);
            this.uiGroupBox9.Controls.Add(this.label13);
            this.uiGroupBox9.Controls.Add(this.label12);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox9.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(1029, 360);
            this.uiGroupBox9.TabIndex = 0;
            this.uiGroupBox9.Text = "Thông tin chi tiết vận đơn nhánh";
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ctrDVTLuong1
            // 
            this.ctrDVTLuong1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDVTLuong1.Appearance.Options.UseBackColor = true;
            this.ctrDVTLuong1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ctrDVTLuong1.Code = "";
            this.ctrDVTLuong1.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTLuong1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong1.IsOnlyWarning = false;
            this.ctrDVTLuong1.IsValidate = true;
            this.ctrDVTLuong1.Location = new System.Drawing.Point(836, 24);
            this.ctrDVTLuong1.Name = "ctrDVTLuong1";
            this.ctrDVTLuong1.Name_VN = "";
            this.ctrDVTLuong1.SetOnlyWarning = false;
            this.ctrDVTLuong1.SetValidate = false;
            this.ctrDVTLuong1.ShowColumnCode = true;
            this.ctrDVTLuong1.ShowColumnName = false;
            this.ctrDVTLuong1.Size = new System.Drawing.Size(105, 21);
            this.ctrDVTLuong1.TabIndex = 16;
            this.ctrDVTLuong1.TagName = "";
            this.ctrDVTLuong1.Where = null;
            this.ctrDVTLuong1.WhereCondition = "";
            this.ctrDVTLuong1.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            this.ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(this.txt_TextChanged);
            // 
            // ctrMaDVTTrongLuong
            // 
            this.ctrMaDVTTrongLuong.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDVTTrongLuong.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTTrongLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E006;
            this.ctrMaDVTTrongLuong.Code = "";
            this.ctrMaDVTTrongLuong.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTTrongLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTTrongLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTTrongLuong.IsOnlyWarning = false;
            this.ctrMaDVTTrongLuong.IsValidate = true;
            this.ctrMaDVTTrongLuong.Location = new System.Drawing.Point(836, 58);
            this.ctrMaDVTTrongLuong.Name = "ctrMaDVTTrongLuong";
            this.ctrMaDVTTrongLuong.Name_VN = "";
            this.ctrMaDVTTrongLuong.SetOnlyWarning = false;
            this.ctrMaDVTTrongLuong.SetValidate = false;
            this.ctrMaDVTTrongLuong.ShowColumnCode = true;
            this.ctrMaDVTTrongLuong.ShowColumnName = false;
            this.ctrMaDVTTrongLuong.Size = new System.Drawing.Size(105, 21);
            this.ctrMaDVTTrongLuong.TabIndex = 19;
            this.ctrMaDVTTrongLuong.TagName = "";
            this.ctrMaDVTTrongLuong.Where = null;
            this.ctrMaDVTTrongLuong.WhereCondition = "";
            this.ctrMaDVTTrongLuong.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            this.ctrMaDVTTrongLuong.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(this.txt_TextChanged);
            // 
            // txtDiaChiNguoiGuiHang
            // 
            this.txtDiaChiNguoiGuiHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiNguoiGuiHang.Location = new System.Drawing.Point(269, 156);
            this.txtDiaChiNguoiGuiHang.Name = "txtDiaChiNguoiGuiHang";
            this.txtDiaChiNguoiGuiHang.Size = new System.Drawing.Size(677, 21);
            this.txtDiaChiNguoiGuiHang.TabIndex = 21;
            this.txtDiaChiNguoiGuiHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChiNguoiGuiHang.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtSoLuongHang
            // 
            this.txtSoLuongHang.DecimalDigits = 20;
            this.txtSoLuongHang.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongHang.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongHang.Location = new System.Drawing.Point(690, 24);
            this.txtSoLuongHang.MaxLength = 15;
            this.txtSoLuongHang.Name = "txtSoLuongHang";
            this.txtSoLuongHang.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongHang.Size = new System.Drawing.Size(127, 21);
            this.txtSoLuongHang.TabIndex = 15;
            this.txtSoLuongHang.Text = "0";
            this.txtSoLuongHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongHang.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongHang.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtTongTrongLuongHang
            // 
            this.txtTongTrongLuongHang.DecimalDigits = 20;
            this.txtTongTrongLuongHang.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongTrongLuongHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTrongLuongHang.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongTrongLuongHang.Location = new System.Drawing.Point(690, 58);
            this.txtTongTrongLuongHang.MaxLength = 15;
            this.txtTongTrongLuongHang.Name = "txtTongTrongLuongHang";
            this.txtTongTrongLuongHang.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTrongLuongHang.Size = new System.Drawing.Size(127, 21);
            this.txtTongTrongLuongHang.TabIndex = 18;
            this.txtTongTrongLuongHang.Text = "0";
            this.txtTongTrongLuongHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTrongLuongHang.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTrongLuongHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongTrongLuongHang.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtSoLuongContainer
            // 
            this.txtSoLuongContainer.DecimalDigits = 20;
            this.txtSoLuongContainer.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongContainer.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongContainer.Location = new System.Drawing.Point(269, 256);
            this.txtSoLuongContainer.MaxLength = 15;
            this.txtSoLuongContainer.Name = "txtSoLuongContainer";
            this.txtSoLuongContainer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongContainer.Size = new System.Drawing.Size(82, 21);
            this.txtSoLuongContainer.TabIndex = 24;
            this.txtSoLuongContainer.Text = "0";
            this.txtSoLuongContainer.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongContainer.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongContainer.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtSTT
            // 
            this.txtSTT.DecimalDigits = 20;
            this.txtSTT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSTT.Location = new System.Drawing.Point(269, 24);
            this.txtSTT.MaxLength = 15;
            this.txtSTT.Name = "txtSTT";
            this.txtSTT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSTT.Size = new System.Drawing.Size(127, 21);
            this.txtSTT.TabIndex = 14;
            this.txtSTT.Text = "0";
            this.txtSTT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSTT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSTT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSTT.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtTenNguoiNhanHang
            // 
            this.txtTenNguoiNhanHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiNhanHang.Location = new System.Drawing.Point(269, 191);
            this.txtTenNguoiNhanHang.Name = "txtTenNguoiNhanHang";
            this.txtTenNguoiNhanHang.Size = new System.Drawing.Size(677, 21);
            this.txtTenNguoiNhanHang.TabIndex = 22;
            this.txtTenNguoiNhanHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenNguoiNhanHang.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtDiaChiNguoiNhanHang
            // 
            this.txtDiaChiNguoiNhanHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiNguoiNhanHang.Location = new System.Drawing.Point(269, 221);
            this.txtDiaChiNguoiNhanHang.Name = "txtDiaChiNguoiNhanHang";
            this.txtDiaChiNguoiNhanHang.Size = new System.Drawing.Size(677, 21);
            this.txtDiaChiNguoiNhanHang.TabIndex = 23;
            this.txtDiaChiNguoiNhanHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChiNguoiNhanHang.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtTenNguoiGuiHang
            // 
            this.txtTenNguoiGuiHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiGuiHang.Location = new System.Drawing.Point(269, 117);
            this.txtTenNguoiGuiHang.Name = "txtTenNguoiGuiHang";
            this.txtTenNguoiGuiHang.Size = new System.Drawing.Size(677, 21);
            this.txtTenNguoiGuiHang.TabIndex = 20;
            this.txtTenNguoiGuiHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenNguoiGuiHang.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtSoVanDonNhanh
            // 
            this.txtSoVanDonNhanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDonNhanh.Location = new System.Drawing.Point(269, 58);
            this.txtSoVanDonNhanh.Name = "txtSoVanDonNhanh";
            this.txtSoVanDonNhanh.Size = new System.Drawing.Size(229, 21);
            this.txtSoVanDonNhanh.TabIndex = 17;
            this.txtSoVanDonNhanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanDonNhanh.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // btnAddVDN
            // 
            this.btnAddVDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddVDN.Image = ((System.Drawing.Image)(resources.GetObject("btnAddVDN.Image")));
            this.btnAddVDN.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddVDN.Location = new System.Drawing.Point(269, 307);
            this.btnAddVDN.Name = "btnAddVDN";
            this.btnAddVDN.Size = new System.Drawing.Size(82, 23);
            this.btnAddVDN.TabIndex = 25;
            this.btnAddVDN.Text = "Ghi";
            this.btnAddVDN.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddVDN.Click += new System.EventHandler(this.btnAddVDN_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(561, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số lượng hàng :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(561, 62);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(124, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Tổng trọng lượng hàng :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(397, 259);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(370, 78);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(268, 284);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Nhập = 0 khi là hàng rời";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(24, 261);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(225, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Tổng số lượng container của vận đơn nhánh :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(24, 229);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(225, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Địa chỉ người nhận hàng của vận đơn nhánh :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(24, 156);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(216, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Địa chỉ người gửi hàng của vận đơn nhánh :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(24, 191);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(211, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Tên người nhận hàng của vận đơn nhánh :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(24, 117);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(202, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Tên người gửi hàng của vận đơn nhánh :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(25, 91);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(955, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số vận đơn nhánh = “Số vận đơn gốc” + “-” + “Số nhánh”  Trong đó: - “Số vận đơn g" +
                "ốc”: số vận đơn gốc thể hiện trên vận đơn - “-”: dấu gạch ngang - “Số nhánh”: từ" +
                " 01-99.\t\r\n";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(24, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Số vận đơn nhánh :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(135, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Số thứ tự vận đơn nhánh :";
            // 
            // uiTabPageContainer
            // 
            this.uiTabPageContainer.Controls.Add(this.uiGroupBox2);
            this.uiTabPageContainer.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageContainer.Name = "uiTabPageContainer";
            this.uiTabPageContainer.Size = new System.Drawing.Size(1035, 653);
            this.uiTabPageContainer.TabStop = true;
            this.uiTabPageContainer.Text = "Thông tin Container vận đơn nhánh";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiGroupBox13);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox2.Controls.Add(this.grbContainer);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1035, 653);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.AutoScroll = true;
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.dgListContainer);
            this.uiGroupBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox13.Location = new System.Drawing.Point(3, 141);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(1029, 467);
            this.uiGroupBox13.TabIndex = 6;
            this.uiGroupBox13.Text = "Danh sách container của vận đơn nhánh";
            this.uiGroupBox13.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListContainer
            // 
            this.dgListContainer.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListContainer.AlternatingColors = true;
            this.dgListContainer.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListContainer.ColumnAutoResize = true;
            dgListContainer_DesignTimeLayout.LayoutString = resources.GetString("dgListContainer_DesignTimeLayout.LayoutString");
            this.dgListContainer.DesignTimeLayout = dgListContainer_DesignTimeLayout;
            this.dgListContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListContainer.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListContainer.FrozenColumns = 3;
            this.dgListContainer.GroupByBoxVisible = false;
            this.dgListContainer.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListContainer.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListContainer.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListContainer.Location = new System.Drawing.Point(3, 17);
            this.dgListContainer.Margin = new System.Windows.Forms.Padding(0);
            this.dgListContainer.Name = "dgListContainer";
            this.dgListContainer.RecordNavigator = true;
            this.dgListContainer.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListContainer.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListContainer.Size = new System.Drawing.Size(1023, 447);
            this.dgListContainer.TabIndex = 15;
            this.dgListContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListContainer.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListContainer_RowDoubleClick);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.btnDeleteContainer);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 608);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1029, 42);
            this.uiGroupBox3.TabIndex = 5;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnDeleteContainer
            // 
            this.btnDeleteContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteContainer.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteContainer.Image")));
            this.btnDeleteContainer.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteContainer.Location = new System.Drawing.Point(923, 13);
            this.btnDeleteContainer.Name = "btnDeleteContainer";
            this.btnDeleteContainer.Size = new System.Drawing.Size(94, 23);
            this.btnDeleteContainer.TabIndex = 30;
            this.btnDeleteContainer.Text = "Xóa";
            this.btnDeleteContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteContainer.Click += new System.EventHandler(this.btnDeleteContainer_Click);
            // 
            // grbContainer
            // 
            this.grbContainer.AutoScroll = true;
            this.grbContainer.BackColor = System.Drawing.Color.Transparent;
            this.grbContainer.Controls.Add(this.label24);
            this.grbContainer.Controls.Add(this.label23);
            this.grbContainer.Controls.Add(this.txtSoSeal);
            this.grbContainer.Controls.Add(this.txtSoContainer);
            this.grbContainer.Controls.Add(this.btnAddExcel);
            this.grbContainer.Controls.Add(this.btnAddContainer);
            this.grbContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.grbContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbContainer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grbContainer.Location = new System.Drawing.Point(3, 8);
            this.grbContainer.Name = "grbContainer";
            this.grbContainer.Size = new System.Drawing.Size(1029, 133);
            this.grbContainer.TabIndex = 3;
            this.grbContainer.Text = "Thông tin về container";
            this.grbContainer.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(28, 67);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Số seal :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(28, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(74, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Số container :";
            // 
            // txtSoSeal
            // 
            this.txtSoSeal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoSeal.Location = new System.Drawing.Point(115, 63);
            this.txtSoSeal.Name = "txtSoSeal";
            this.txtSoSeal.Size = new System.Drawing.Size(292, 21);
            this.txtSoSeal.TabIndex = 28;
            this.txtSoSeal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoSeal.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtSoContainer
            // 
            this.txtSoContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoContainer.Location = new System.Drawing.Point(115, 23);
            this.txtSoContainer.Name = "txtSoContainer";
            this.txtSoContainer.Size = new System.Drawing.Size(292, 21);
            this.txtSoContainer.TabIndex = 27;
            this.txtSoContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoContainer.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // btnAddExcel
            // 
            this.btnAddExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnAddExcel.Image")));
            this.btnAddExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddExcel.Location = new System.Drawing.Point(150, 95);
            this.btnAddExcel.Name = "btnAddExcel";
            this.btnAddExcel.Size = new System.Drawing.Size(94, 23);
            this.btnAddExcel.TabIndex = 29;
            this.btnAddExcel.Text = "Nhập Excel";
            this.btnAddExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddExcel.Click += new System.EventHandler(this.btnAddExcel_Click);
            // 
            // btnAddContainer
            // 
            this.btnAddContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddContainer.Image = ((System.Drawing.Image)(resources.GetObject("btnAddContainer.Image")));
            this.btnAddContainer.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddContainer.Location = new System.Drawing.Point(250, 95);
            this.btnAddContainer.Name = "btnAddContainer";
            this.btnAddContainer.Size = new System.Drawing.Size(94, 23);
            this.btnAddContainer.TabIndex = 29;
            this.btnAddContainer.Text = "Ghi";
            this.btnAddContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddContainer.Click += new System.EventHandler(this.btnAddContainer_Click);
            // 
            // VNACC_BranchDetailsForm
            // 
            this.ClientSize = new System.Drawing.Size(1243, 681);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_BranchDetailsForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Danh sách vận đơn nhánh";
            this.Load += new System.EventHandler(this.VNACC_BranchDetailsForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPageVDN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListVDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            this.uiTabPageContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListContainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbContainer)).EndInit();
            this.grbContainer.ResumeLayout(false);
            this.grbContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageVDN;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTTrongLuong;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiGuiHang;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongHang;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTrongLuongHang;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongContainer;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSTT;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiNhanHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiNhanHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiGuiHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDonNhanh;
        private Janus.Windows.EditControls.UIButton btnDeleteVDN;
        private Janus.Windows.EditControls.UIButton btnAddVDN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageContainer;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox grbContainer;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSeal;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoContainer;
        private Janus.Windows.EditControls.UIButton btnDeleteContainer;
        private Janus.Windows.EditControls.UIButton btnAddExcel;
        private Janus.Windows.EditControls.UIButton btnAddContainer;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.GridEX.GridEX dgListVDN;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.GridEX.GridEX dgListContainer;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label1;
    }
}
