﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
namespace Company.Interface
{
    public partial class VNACC_TK_DinhKiemDTForm : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        private static List<VNACC_Category_Common> datas = null;
        DataTable dt = new DataTable();
        public VNACC_TK_DinhKiemDTForm()
        {
            InitializeComponent();
        }
        private void VNACC_TK_DinhKiemDTForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            //Load dropdown
            datas = VNACC_Category_Common.SelectCollectionDynamic("ReferenceDB = 'E020'", "Code asc");
            grList.DropDowns["drdPhanLoai"].DataSource = datas;
            //Load grid
            dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.DinhKemCollection);
            grList.DataSource = dt;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int sott = 1;
                TKMD.DinhKemCollection.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow item in dt.Rows)
                {
                    KDT_VNACC_TK_DinhKemDienTu dk = new KDT_VNACC_TK_DinhKemDienTu();
                    if (item["ID"].ToString().Length != 0)
                    {
                        dk.ID = Convert.ToInt32(item["ID"].ToString());
                        dk.TKMD_ID = Convert.ToInt32(item["TKMD_ID"].ToString());
                    }
                    dk.SoTT = sott;
                    dk.PhanLoai = item["PhanLoai"].ToString();
                    dk.SoDinhKemKhaiBaoDT = Convert.ToDecimal(item["SoDinhKemKhaiBaoDT"].ToString());
                    sott++;
                    TKMD.DinhKemCollection.Add(dk);
                }
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa đính kèm điện tử này không?", true) == "Yes")
                    {
                        KDT_VNACC_TK_DinhKemDienTu.DeleteDynamic("ID=" + ID);
                        grList.CurrentRow.Delete();
                    }
                }
                else
                    grList.CurrentRow.Delete();
                grList.Refetch();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
    }
}
