﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Company.Interface;
using Company.GC.BLL.KDT;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace SOFTECH.ECS.TQDT.GC.GC.PhanBo
{
    public class PhanBo
    {
        public event EventHandler<PhanBoEventArgs> PBEventArgs;
        private void OnProcess(PhanBoEventArgs e)
        {
            if (PBEventArgs != null)
            {
                PBEventArgs(this, e);
            }
        }
        public DataSet TaoBangPhanBo(int tableCuoi, int soTable, long HDID, int soLuongSP,int TongBang5, int TongBang3, Company.GC.BLL.KDT.ToKhaiMauDich TKMD)
        {
            DataSet dsTemp = new DataSet();
            DataTable dttempByFive = new DataTable();
            DataTable dttempByThree = new DataTable();

            #region TaoBang5

            for (int BangSo = 0; BangSo < TongBang5; BangSo++)
            {
                //Tạo cấu trúc bảng 5
                dttempByFive = new DataTable("dtBangPhanBoSeven" + BangSo.ToString());
                DataColumn[] dcCol = new DataColumn[19];

                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int t = 4;
                for (int k = BangSo * 5; k < (BangSo + 1) * 5; k++)
                {
                    HangMauDich hmd = TKMD.HMDCollection[k];

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "DinhMuc" + k % 5;
                    dcCol[t].DataType = Type.GetType("System.Decimal");
                    dcCol[t].Caption = hmd.MaPhu;
                    t++;

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "TLHH" + k % 5;
                    dcCol[t].DataType = Type.GetType("System.Decimal");
                    dcCol[t].Caption = "";
                    t++;

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "LuongSD" + k % 5;
                    dcCol[t].DataType = Type.GetType("System.Decimal");
                    dcCol[t].Caption = hmd.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
                    t++;
                }
                dttempByFive.Columns.AddRange(dcCol);

                int stt = 0;
                //Lấy danh sách npl phân bổ cửa tờ khai xuất TKMD đưa vào dataset ds
                DataSet ds = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoToKhai(TKMD.ID, HDID);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    try
                    {
                        //Add từng dòng dữ liệu theo cấu trúc như trên
                        DataRow drData = dttempByFive.NewRow();
                        drData[0] = ++stt;
                        drData[1] = dr["MaNPL"].ToString();
                        drData[2] = dr["TenNPL"].ToString();
                        drData[3] = DonViTinh.GetName(dr["DVT_ID"].ToString());
                        for (int n = 0; n < 5; n++)
                        {
                            if (dttempByFive.Columns["DinhMuc" + n].Caption != " ")
                            {
                                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                                dm.MaSanPham = dttempByFive.Columns["DinhMuc" + n].Caption;
                                dm.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                                dm.HopDong_ID = HDID;
                                dm.Load();
                                decimal dinhMucSD = Math.Round(dm.DinhMucSuDung, GlobalSettings.SoThapPhan.DinhMuc, MidpointRounding.AwayFromZero);
                                drData["DinhMuc" + n] = dinhMucSD;
                                drData["TLHH" + n] = dm.TyLeHaoHut;
                                drData["LuongSD" + n] = Math.Round(Convert.ToDecimal(dttempByFive.Columns["LuongSD" + n].Caption) * (dinhMucSD * (1 + dm.TyLeHaoHut / 100)), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
                            }

                        }
                        dttempByFive.Rows.Add(drData);
                        OnProcess(new PhanBoEventArgs("Đang khởi tạo dữ liệu", 0));
                    }
                    catch { }
                }

                dsTemp.Tables.Add(dttempByFive);
            }

            #endregion

            #region TaoBang3
            //Tương tự như bảng 5
            Company.GC.BLL.GC.DinhMuc DMtemp = new Company.GC.BLL.GC.DinhMuc();
            decimal TLHH = DMtemp.GetTLHHOfHopDong(HDID);

            for (int BangSo = 0; BangSo < TongBang3; BangSo++)
            {
                dttempByThree = new DataTable("dtBangPhanBoFour" + BangSo.ToString());
                DataColumn[] dcCol = new DataColumn[15];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int t = 4;
                for (int k = (BangSo * 3) + (5 * TongBang5); k < (BangSo + 1) * 3 + (5 * TongBang5); k++)
                {

                    int phanchia = k - (5 * TongBang5);
                    if (k < soLuongSP)
                    {
                        HangMauDich hmd = TKMD.HMDCollection[k];

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "DinhMuc" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = hmd.MaPhu;
                        t++;

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "TLHH" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = "";
                        t++;

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "LuongSD" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = hmd.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
                        t++;
                    }
                    else
                    {
                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "DinhMuc" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = " ";
                        t++;

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "TLHH" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = " ";
                        t++;

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "LuongSD" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = " ";
                        t++;

                    }
                }


                dcCol[t] = new DataColumn();
                dcCol[t].ColumnName = "TongNPLTLHH";
                dcCol[t].DataType = Type.GetType("System.Decimal");
                dcCol[t].Caption = "TongNPLTLHH";
                t++;
                dcCol[t] = new DataColumn();
                dcCol[t].ColumnName = "PhanBo";
                dcCol[t].DataType = Type.GetType("System.String");
                dcCol[t].Caption = "PhanBo";

                dttempByThree.Columns.AddRange(dcCol);

                int stt = 0;
                DataSet ds = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoToKhai(TKMD.ID, HDID);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    try
                    {
                        DataRow drData = dttempByThree.NewRow();
                        drData[0] = ++stt;
                        drData[1] = dr["MaNPL"].ToString();
                        drData[2] = dr["TenNPL"].ToString();
                        drData[3] = DonViTinh.GetName(dr["DVT_ID"].ToString());
                        for (int n = 0; n < 3; n++)
                        {
                            if (dttempByThree.Columns["DinhMuc" + n].Caption != " ")
                            {
                                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                                dm.MaSanPham = dttempByThree.Columns["DinhMuc" + n].Caption;
                                dm.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                                dm.HopDong_ID = HDID;
                                dm.Load();

                                decimal dinhMucSD = Math.Round(dm.DinhMucSuDung, GlobalSettings.SoThapPhan.DinhMuc, MidpointRounding.AwayFromZero);
                                drData["DinhMuc" + n] = dinhMucSD;
                                drData["TLHH" + n] = dm.TyLeHaoHut;
                                drData["LuongSD" + n] = Math.Round(Convert.ToDecimal(dttempByThree.Columns["LuongSD" + n].Caption) * (dinhMucSD * (1 + dm.TyLeHaoHut / 100)), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
                            }
                        }

                        dttempByThree.Rows.Add(drData);
                    }
                    catch { }
                }

                dsTemp.Tables.Add(dttempByThree);
            }

           // decimal tongNPLTLHH = 0;

            //Add thêm dữ liệu 2 cột TongNPLTLHH va PhanBo vào table cuối cùng
            if (soTable > 0)
            {
                for (int t = 0; t < dsTemp.Tables[0].Rows.Count; t++)
                {
                    decimal tongTungNPL = 0;

                    if (tableCuoi == 0 || tableCuoi > 3)
                    {
                        for (int i = 0; i < soTable - 2; i++)
                        {
                            try
                            {
                                DataRow dr = dsTemp.Tables[i].Rows[t];
                                for (int n = 0; n < 5; n++)
                                {
                                    if (dr["LuongSD" + n] != DBNull.Value)
                                        tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                }
                            }
                            catch { }
                        }

                        for (int i = soTable - 2; i < soTable; i++)
                        {
                            try
                            {
                                DataRow dr = dsTemp.Tables[i].Rows[t];
                                for (int n = 0; n < 3; n++)
                                {
                                    if (dr["LuongSD" + n] != DBNull.Value)
                                        tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                }
                            }
                            catch { }
                        }

                    }
                    else
                    {
                        for (int i = 0; i < soTable - 1; i++)
                        {
                            try
                            {
                                DataRow dr = dsTemp.Tables[i].Rows[t];
                                for (int n = 0; n < 5; n++)
                                {
                                    if (dr["LuongSD" + n] != DBNull.Value)
                                        tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                }
                            }
                            catch { }
                        }

                        for (int i = soTable - 1; i < soTable; i++)
                        {
                            try
                            {
                                DataRow dr = dsTemp.Tables[i].Rows[t];
                                for (int n = 0; n < 3; n++)
                                {
                                    if (dr["LuongSD" + n] != DBNull.Value)
                                        tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                }
                            }
                            catch { }
                        }

                    }

                    try
                    {

                        dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["TongNPLTLHH"] = tongTungNPL;
                        dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["PhanBo"] = GetPhanBoToKhaiXuat(dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["MaNPL"].ToString(), tongTungNPL, TKMD);
                        OnProcess(new PhanBoEventArgs("Đang xử lý mã NPL : " + dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["MaNPL"].ToString(), (t *100)/dsTemp.Tables[0].Rows.Count ));
                    }
                    catch { }
                }
            }

            #endregion

            return dsTemp;
        }

        private string GetPhanBoToKhaiXuat(string maNPL, decimal tongNhuCau, Company.GC.BLL.KDT.ToKhaiMauDich TKMD)
        {
            string temp = "";
            //DataSet ds = PhanBoToKhaiXuat.SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL(this.TKMD.ID, maNPL);
            //DataSet ds = PhanBoToKhaiXuat.SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL(TKMD.ID, maNPL, TKMD.MaLoaiHinh);
            DataSet ds = new DataSet();
            decimal tongPhanBo = 0;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["SoToKhaiNhap"].ToString() != "0")
                {
                    temp += "TK " + dr["SoToKhaiNhap"] + "/" + dr["MaLoaiHinhNhap"].ToString() + "/" + dr["NamDangKyNhap"].ToString() + ": " + Convert.ToDecimal(dr["LuongPhanBo"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL) + "; ";
                    tongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                }
            }
            decimal muaVN = Math.Round(tongNhuCau - tongPhanBo, GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
            if (muaVN > 0)
                temp += "Mua VN: " + muaVN.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            return temp;
        }

    }
}
