﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class HopDongThuongMai
    {
        #region Properties.

        public long ID { set; get; }
        public string SoHopDongTM { set; get; }
        public DateTime NgayHopDongTM { set; get; }
        public DateTime ThoiHanThanhToan { set; get; }
        public string NguyenTe_ID { set; get; }
        public string PTTT_ID { set; get; }
        public string DKGH_ID { set; get; }
        public string DiaDiemGiaoHang { set; get; }
        public string MaDonViMua { set; get; }
        public string TenDonViMua { set; get; }
        public string MaDonViBan { set; get; }
        public string TenDonViBan { set; get; }
        public decimal TongTriGia { set; get; }
        public string ThongTinKhac { set; get; }
        public long TKMD_ID { set; get; }
        public string GuidStr { set; get; }
        public int LoaiKB { set; get; }
        public long SoTiepNhan { set; get; }
        public DateTime NgayTiepNhan { set; get; }
        public int TrangThai { set; get; }
        public int NamTiepNhan { set; get; }
        public string MaDoanhNghiep { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<HopDongThuongMai> ConvertToCollection(IDataReader reader)
        {
            IList<HopDongThuongMai> collection = new List<HopDongThuongMai>();
            while (reader.Read())
            {
                HopDongThuongMai entity = new HopDongThuongMai();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongTM"))) entity.SoHopDongTM = reader.GetString(reader.GetOrdinal("SoHopDongTM"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDongTM"))) entity.NgayHopDongTM = reader.GetDateTime(reader.GetOrdinal("NgayHopDongTM"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanThanhToan"))) entity.ThoiHanThanhToan = reader.GetDateTime(reader.GetOrdinal("ThoiHanThanhToan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViMua"))) entity.MaDonViMua = reader.GetString(reader.GetOrdinal("MaDonViMua"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViMua"))) entity.TenDonViMua = reader.GetString(reader.GetOrdinal("TenDonViMua"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViBan"))) entity.MaDonViBan = reader.GetString(reader.GetOrdinal("MaDonViBan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViBan"))) entity.TenDonViBan = reader.GetString(reader.GetOrdinal("TenDonViBan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGia"))) entity.TongTriGia = reader.GetDecimal(reader.GetOrdinal("TongTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static HopDongThuongMai Load(long id)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<HopDongThuongMai> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<HopDongThuongMai> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<HopDongThuongMai> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<HopDongThuongMai> SelectCollectionBy_TKMD_ID(long tKMD_ID)
        {
            IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "p_KDT_HopDongThuongMai_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertHopDongThuongMai(string soHopDongTM, DateTime ngayHopDongTM, DateTime thoiHanThanhToan, string nguyenTe_ID, string pTTT_ID, string dKGH_ID, string diaDiemGiaoHang, string maDonViMua, string tenDonViMua, string maDonViBan, string tenDonViBan, decimal tongTriGia, string thongTinKhac, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan, string maDoanhNghiep)
        {
            HopDongThuongMai entity = new HopDongThuongMai();
            entity.SoHopDongTM = soHopDongTM;
            entity.NgayHopDongTM = ngayHopDongTM;
            entity.ThoiHanThanhToan = thoiHanThanhToan;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.PTTT_ID = pTTT_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.DiaDiemGiaoHang = diaDiemGiaoHang;
            entity.MaDonViMua = maDonViMua;
            entity.TenDonViMua = tenDonViMua;
            entity.MaDonViBan = maDonViBan;
            entity.TenDonViBan = tenDonViBan;
            entity.TongTriGia = tongTriGia;
            entity.ThongTinKhac = thongTinKhac;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            entity.MaDoanhNghiep = maDoanhNghiep;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoHopDongTM", SqlDbType.VarChar, SoHopDongTM);
            db.AddInParameter(dbCommand, "@NgayHopDongTM", SqlDbType.DateTime, NgayHopDongTM.Year <= 1753 ? DBNull.Value : (object)NgayHopDongTM);
            db.AddInParameter(dbCommand, "@ThoiHanThanhToan", SqlDbType.DateTime, ThoiHanThanhToan.Year <= 1753 ? DBNull.Value : (object)ThoiHanThanhToan);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.VarChar, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@MaDonViMua", SqlDbType.VarChar, MaDonViMua);
            db.AddInParameter(dbCommand, "@TenDonViMua", SqlDbType.NVarChar, TenDonViMua);
            db.AddInParameter(dbCommand, "@MaDonViBan", SqlDbType.VarChar, MaDonViBan);
            db.AddInParameter(dbCommand, "@TenDonViBan", SqlDbType.NVarChar, TenDonViBan);
            db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Money, TongTriGia);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<HopDongThuongMai> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HopDongThuongMai item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateHopDongThuongMai(long id, string soHopDongTM, DateTime ngayHopDongTM, DateTime thoiHanThanhToan, string nguyenTe_ID, string pTTT_ID, string dKGH_ID, string diaDiemGiaoHang, string maDonViMua, string tenDonViMua, string maDonViBan, string tenDonViBan, decimal tongTriGia, string thongTinKhac, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan, string maDoanhNghiep)
        {
            HopDongThuongMai entity = new HopDongThuongMai();
            entity.ID = id;
            entity.SoHopDongTM = soHopDongTM;
            entity.NgayHopDongTM = ngayHopDongTM;
            entity.ThoiHanThanhToan = thoiHanThanhToan;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.PTTT_ID = pTTT_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.DiaDiemGiaoHang = diaDiemGiaoHang;
            entity.MaDonViMua = maDonViMua;
            entity.TenDonViMua = tenDonViMua;
            entity.MaDonViBan = maDonViBan;
            entity.TenDonViBan = tenDonViBan;
            entity.TongTriGia = tongTriGia;
            entity.ThongTinKhac = thongTinKhac;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            entity.MaDoanhNghiep = maDoanhNghiep;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_HopDongThuongMai_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoHopDongTM", SqlDbType.VarChar, SoHopDongTM);
            db.AddInParameter(dbCommand, "@NgayHopDongTM", SqlDbType.DateTime, NgayHopDongTM.Year <= 1753 ? DBNull.Value : (object)NgayHopDongTM);
            db.AddInParameter(dbCommand, "@ThoiHanThanhToan", SqlDbType.DateTime, ThoiHanThanhToan.Year <= 1753 ? DBNull.Value : (object)ThoiHanThanhToan);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.VarChar, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@MaDonViMua", SqlDbType.VarChar, MaDonViMua);
            db.AddInParameter(dbCommand, "@TenDonViMua", SqlDbType.NVarChar, TenDonViMua);
            db.AddInParameter(dbCommand, "@MaDonViBan", SqlDbType.VarChar, MaDonViBan);
            db.AddInParameter(dbCommand, "@TenDonViBan", SqlDbType.NVarChar, TenDonViBan);
            db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Money, TongTriGia);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<HopDongThuongMai> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HopDongThuongMai item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateHopDongThuongMai(long id, string soHopDongTM, DateTime ngayHopDongTM, DateTime thoiHanThanhToan, string nguyenTe_ID, string pTTT_ID, string dKGH_ID, string diaDiemGiaoHang, string maDonViMua, string tenDonViMua, string maDonViBan, string tenDonViBan, decimal tongTriGia, string thongTinKhac, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan, string maDoanhNghiep)
        {
            HopDongThuongMai entity = new HopDongThuongMai();
            entity.ID = id;
            entity.SoHopDongTM = soHopDongTM;
            entity.NgayHopDongTM = ngayHopDongTM;
            entity.ThoiHanThanhToan = thoiHanThanhToan;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.PTTT_ID = pTTT_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.DiaDiemGiaoHang = diaDiemGiaoHang;
            entity.MaDonViMua = maDonViMua;
            entity.TenDonViMua = tenDonViMua;
            entity.MaDonViBan = maDonViBan;
            entity.TenDonViBan = tenDonViBan;
            entity.TongTriGia = tongTriGia;
            entity.ThongTinKhac = thongTinKhac;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            entity.MaDoanhNghiep = maDoanhNghiep;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoHopDongTM", SqlDbType.VarChar, SoHopDongTM);
            db.AddInParameter(dbCommand, "@NgayHopDongTM", SqlDbType.DateTime, NgayHopDongTM.Year <= 1753 ? DBNull.Value : (object)NgayHopDongTM);
            db.AddInParameter(dbCommand, "@ThoiHanThanhToan", SqlDbType.DateTime, ThoiHanThanhToan.Year <= 1753 ? DBNull.Value : (object)ThoiHanThanhToan);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.VarChar, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@MaDonViMua", SqlDbType.VarChar, MaDonViMua);
            db.AddInParameter(dbCommand, "@TenDonViMua", SqlDbType.NVarChar, TenDonViMua);
            db.AddInParameter(dbCommand, "@MaDonViBan", SqlDbType.VarChar, MaDonViBan);
            db.AddInParameter(dbCommand, "@TenDonViBan", SqlDbType.NVarChar, TenDonViBan);
            db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Money, TongTriGia);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<HopDongThuongMai> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HopDongThuongMai item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteHopDongThuongMai(long id)
        {
            HopDongThuongMai entity = new HopDongThuongMai();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<HopDongThuongMai> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HopDongThuongMai item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        public static HopDongThuongMai LoadBy_SoHopDong(string SoHopDongTM)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_LoadBy_SoHopDong]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoHopDongTM", SqlDbType.Char, SoHopDongTM);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<HopDongThuongMai> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }
    }
}