namespace Company.Interface.Report
{
    partial class TQDTToKhaiXK_TT15
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TQDTToKhaiXK_TT15));
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.DetailThue = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcThueXuat_SoTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThueXuat_TriGiaTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThueXuat_TiLe = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThueXuat_TienThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThuKhac_TriGia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThuKhac_TyLe = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThuKhac_SoTien = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThueXuat_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThuKhac_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcTongTienBangSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcTongTienBangChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailHangHoa = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrtHangHoa = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcSoTTHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcMoTaHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcMaSoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcXuatXuHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuongHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcDVT_Hang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcDonGia_Hang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcTriGiaNguyenTe_Hang = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader2 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter2 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblThueSuatVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcCongHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcSoTN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcSoTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcChiCucHQDK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcNgayDKToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcNgayCapSoTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcChiCucHQCuaKhauXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcNgayDK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcSoLuongPhuLuc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcNguoiXuatKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcSoGP = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbSoGP = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcSoHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcMSNguoiXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xlbMSNguoiXuatKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xtcNgayGP = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbNgayGP = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcNgayHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcNgayHHGP = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbNgayHHGP = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcNgayHHHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcNguoiNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcHoaDonTM = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcCuaKhauXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcNguoiUT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcMSNguoiUT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xlbMSNguoiUT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcNuocNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcDaiLyHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcDieuKienGiaoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcPhuongThucThanhToan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcMSDaiLyHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.xlbMSDaiLyHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcDongTienTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcTiGiaTinhThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xlbCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcChungTuDiKem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xlbChungTuDinhKem = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcKetQuaPL = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbKetQuaPL = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailLuongHang = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcLuongHang_SoTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuongHang_SoHieuContainer = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuongHang_SoLuongKien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuongHang_TrongLuongHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuongHang_DDDongHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuongHang_CongTL = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtHangHoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 0;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 2;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailThue
            // 
            this.DetailThue.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader1,
            this.ReportFooter1});
            this.DetailThue.Level = 1;
            this.DetailThue.Name = "DetailThue";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail1.Height = 20;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable5
            // 
            this.xrTable5.Location = new System.Drawing.Point(17, 0);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable5.Size = new System.Drawing.Size(780, 20);
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcThueXuat_SoTT,
            this.xtcThueXuat_TriGiaTT,
            this.xtcThueXuat_TiLe,
            this.xtcThueXuat_TienThue,
            this.xtcThuKhac_TriGia,
            this.xtcThuKhac_TyLe,
            this.xtcThuKhac_SoTien});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 1.32;
            // 
            // xtcThueXuat_SoTT
            // 
            this.xtcThueXuat_SoTT.Name = "xtcThueXuat_SoTT";
            this.xtcThueXuat_SoTT.StylePriority.UseTextAlignment = false;
            this.xtcThueXuat_SoTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xtcThueXuat_SoTT.Weight = 0.17;
            // 
            // xtcThueXuat_TriGiaTT
            // 
            this.xtcThueXuat_TriGiaTT.Name = "xtcThueXuat_TriGiaTT";
            this.xtcThueXuat_TriGiaTT.StylePriority.UseTextAlignment = false;
            this.xtcThueXuat_TriGiaTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xtcThueXuat_TriGiaTT.Weight = 0.59121372031662267;
            // 
            // xtcThueXuat_TiLe
            // 
            this.xtcThueXuat_TiLe.Name = "xtcThueXuat_TiLe";
            this.xtcThueXuat_TiLe.StylePriority.UseTextAlignment = false;
            this.xtcThueXuat_TiLe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThueXuat_TiLe.Weight = 0.38361071646032069;
            // 
            // xtcThueXuat_TienThue
            // 
            this.xtcThueXuat_TienThue.Name = "xtcThueXuat_TienThue";
            this.xtcThueXuat_TienThue.StylePriority.UseTextAlignment = false;
            this.xtcThueXuat_TienThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThueXuat_TienThue.Weight = 0.5095291252283336;
            // 
            // xtcThuKhac_TriGia
            // 
            this.xtcThuKhac_TriGia.Name = "xtcThuKhac_TriGia";
            this.xtcThuKhac_TriGia.StylePriority.UseTextAlignment = false;
            this.xtcThuKhac_TriGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThuKhac_TriGia.Weight = 0.49604221635883905;
            // 
            // xtcThuKhac_TyLe
            // 
            this.xtcThuKhac_TyLe.Name = "xtcThuKhac_TyLe";
            this.xtcThuKhac_TyLe.StylePriority.UseTextAlignment = false;
            this.xtcThuKhac_TyLe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThuKhac_TyLe.Weight = 0.34709762532981531;
            // 
            // xtcThuKhac_SoTien
            // 
            this.xtcThuKhac_SoTien.Name = "xtcThuKhac_SoTien";
            this.xtcThuKhac_SoTien.StylePriority.UseTextAlignment = false;
            this.xtcThuKhac_SoTien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThuKhac_SoTien.Weight = 0.50250659630606864;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.ReportHeader1.Height = 50;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrTable4
            // 
            this.xrTable4.Location = new System.Drawing.Point(17, 0);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16,
            this.xrTableRow17});
            this.xrTable4.Size = new System.Drawing.Size(780, 50);
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell47,
            this.xrTableCell49});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.StylePriority.UseTextAlignment = false;
            this.xrTableRow16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow16.Weight = 1;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.Text = "Số";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell43.Weight = 0.17;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Text = "22.Thuế xuất khẩu";
            this.xrTableCell47.Weight = 1.4842419322102702;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Text = "23.Thu khác";
            this.xrTableCell49.Weight = 1.3457580677897298;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell69,
            this.xrTableCell70});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.StylePriority.UseBorders = false;
            this.xrTableRow17.Weight = 1;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.StylePriority.UseTextAlignment = false;
            this.xrTableCell50.Text = "TT";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell50.Weight = 0.17;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = "a. Trị giá tính thuế";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell51.Weight = 0.59121372031662267;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseTextAlignment = false;
            this.xrTableCell52.Text = "b. Thuế xuất (%)";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell52.Weight = 0.38361071646032069;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = "c. Tiền thuế";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell61.Weight = 0.5095291252283336;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.Text = "a. Trị giá tính thu khác";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell62.Weight = 0.49604221635883905;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseTextAlignment = false;
            this.xrTableCell69.Text = "b. Tỷ lệ (%)";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell69.Weight = 0.34709762532981531;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseTextAlignment = false;
            this.xrTableCell70.Text = "c. Số tiền";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell70.Weight = 0.50250659630606864;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.ReportFooter1.Height = 60;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrTable2
            // 
            this.xrTable2.Location = new System.Drawing.Point(17, 0);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14,
            this.xrTableRow8,
            this.xrTableRow9});
            this.xrTable2.Size = new System.Drawing.Size(780, 60);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xtcThueXuat_Cong,
            this.xrTableCell39,
            this.xtcThuKhac_Cong});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow14.StylePriority.UseBorders = false;
            this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow14.Weight = 1;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseBorders = false;
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "Cộng:";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell35.Weight = 0.3819734794668832;
            // 
            // xtcThueXuat_Cong
            // 
            this.xtcThueXuat_Cong.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcThueXuat_Cong.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xtcThueXuat_Cong.Name = "xtcThueXuat_Cong";
            this.xtcThueXuat_Cong.StylePriority.UseBorders = false;
            this.xtcThueXuat_Cong.StylePriority.UseFont = false;
            this.xtcThueXuat_Cong.StylePriority.UseTextAlignment = false;
            this.xtcThueXuat_Cong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThueXuat_Cong.Weight = 0.16903457140924161;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.Text = "Cộng:";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell39.Weight = 0.28057303294770314;
            // 
            // xtcThuKhac_Cong
            // 
            this.xtcThuKhac_Cong.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcThuKhac_Cong.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xtcThuKhac_Cong.Name = "xtcThuKhac_Cong";
            this.xtcThuKhac_Cong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcThuKhac_Cong.StylePriority.UseBorders = false;
            xrSummary1.FormatString = "{0:N2}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xtcThuKhac_Cong.Summary = xrSummary1;
            this.xtcThuKhac_Cong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThuKhac_Cong.Weight = 0.16841891617617211;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xtcTongTienBangSo});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Text = "Tổng số tiền thuế và thu khác (ô 22 + 23) bằng số:   ";
            this.xrTableCell1.Weight = 0.3819734794668832;
            // 
            // xtcTongTienBangSo
            // 
            this.xtcTongTienBangSo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcTongTienBangSo.Name = "xtcTongTienBangSo";
            this.xtcTongTienBangSo.StylePriority.UseBorders = false;
            this.xtcTongTienBangSo.StylePriority.UseTextAlignment = false;
            this.xtcTongTienBangSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xtcTongTienBangSo.Weight = 0.6180265205331168;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xtcTongTienBangChu});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Text = "Bằng chữ: ";
            this.xrTableCell13.Weight = 0.096076043569447322;
            // 
            // xtcTongTienBangChu
            // 
            this.xtcTongTienBangChu.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcTongTienBangChu.Name = "xtcTongTienBangChu";
            this.xtcTongTienBangChu.StylePriority.UseBorders = false;
            this.xtcTongTienBangChu.Weight = 0.90392395643055268;
            // 
            // DetailHangHoa
            // 
            this.DetailHangHoa.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportHeader2,
            this.ReportFooter2});
            this.DetailHangHoa.Level = 0;
            this.DetailHangHoa.Name = "DetailHangHoa";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrtHangHoa});
            this.Detail2.Height = 20;
            this.Detail2.Name = "Detail2";
            // 
            // xrtHangHoa
            // 
            this.xrtHangHoa.Location = new System.Drawing.Point(17, 0);
            this.xrtHangHoa.Name = "xrtHangHoa";
            this.xrtHangHoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrtHangHoa.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrtHangHoa.Size = new System.Drawing.Size(780, 20);
            this.xrtHangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcSoTTHang,
            this.xtcMoTaHang,
            this.xtcMaSoHang,
            this.xtcXuatXuHang,
            this.xtcLuongHang,
            this.xtcDVT_Hang,
            this.xtcDonGia_Hang,
            this.xtcTriGiaNguyenTe_Hang});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.StylePriority.UseBorders = false;
            this.xrTableRow7.StylePriority.UseTextAlignment = false;
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow7.Weight = 0.51489361702127678;
            // 
            // xtcSoTTHang
            // 
            this.xtcSoTTHang.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcSoTTHang.Name = "xtcSoTTHang";
            this.xtcSoTTHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcSoTTHang.StylePriority.UseBorders = false;
            this.xtcSoTTHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcSoTTHang.Weight = 0.032981530343007916;
            // 
            // xtcMoTaHang
            // 
            this.xtcMoTaHang.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xtcMoTaHang.Multiline = true;
            this.xtcMoTaHang.Name = "xtcMoTaHang";
            this.xtcMoTaHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcMoTaHang.StylePriority.UseBorders = false;
            this.xtcMoTaHang.StylePriority.UseTextAlignment = false;
            this.xtcMoTaHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xtcMoTaHang.Weight = 0.25197889182058059;
            // 
            // xtcMaSoHang
            // 
            this.xtcMaSoHang.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xtcMaSoHang.Multiline = true;
            this.xtcMaSoHang.Name = "xtcMaSoHang";
            this.xtcMaSoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcMaSoHang.StylePriority.UseBorders = false;
            this.xtcMaSoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcMaSoHang.Weight = 0.12137203166226911;
            // 
            // xtcXuatXuHang
            // 
            this.xtcXuatXuHang.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xtcXuatXuHang.Name = "xtcXuatXuHang";
            this.xtcXuatXuHang.StylePriority.UseBorders = false;
            this.xtcXuatXuHang.StylePriority.UseTextAlignment = false;
            this.xtcXuatXuHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcXuatXuHang.Weight = 0.097740351268547215;
            // 
            // xtcLuongHang
            // 
            this.xtcLuongHang.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xtcLuongHang.Multiline = true;
            this.xtcLuongHang.Name = "xtcLuongHang";
            this.xtcLuongHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcLuongHang.StylePriority.UseBorders = false;
            this.xtcLuongHang.StylePriority.UseTextAlignment = false;
            this.xtcLuongHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcLuongHang.Weight = 0.097491792767983684;
            // 
            // xtcDVT_Hang
            // 
            this.xtcDVT_Hang.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xtcDVT_Hang.Name = "xtcDVT_Hang";
            this.xtcDVT_Hang.StylePriority.UseBorders = false;
            this.xtcDVT_Hang.StylePriority.UseTextAlignment = false;
            this.xtcDVT_Hang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcDVT_Hang.Weight = 0.074556774169273865;
            this.xtcDVT_Hang.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcDVT_Hang_BeforePrint);
            // 
            // xtcDonGia_Hang
            // 
            this.xtcDonGia_Hang.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xtcDonGia_Hang.Name = "xtcDonGia_Hang";
            this.xtcDonGia_Hang.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xtcDonGia_Hang.StylePriority.UseBorders = false;
            this.xtcDonGia_Hang.StylePriority.UsePadding = false;
            this.xtcDonGia_Hang.StylePriority.UseTextAlignment = false;
            this.xtcDonGia_Hang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcDonGia_Hang.Weight = 0.11642480211081795;
            // 
            // xtcTriGiaNguyenTe_Hang
            // 
            this.xtcTriGiaNguyenTe_Hang.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xtcTriGiaNguyenTe_Hang.Name = "xtcTriGiaNguyenTe_Hang";
            this.xtcTriGiaNguyenTe_Hang.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xtcTriGiaNguyenTe_Hang.StylePriority.UseBorders = false;
            this.xtcTriGiaNguyenTe_Hang.StylePriority.UsePadding = false;
            this.xtcTriGiaNguyenTe_Hang.StylePriority.UseTextAlignment = false;
            this.xtcTriGiaNguyenTe_Hang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcTriGiaNguyenTe_Hang.Weight = 0.11906332453825859;
            // 
            // ReportHeader2
            // 
            this.ReportHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.ReportHeader2.Height = 35;
            this.ReportHeader2.Name = "ReportHeader2";
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Location = new System.Drawing.Point(17, 0);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.Size = new System.Drawing.Size(780, 35);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell66,
            this.xrTableCell24,
            this.xrTableCell67,
            this.xrTableCell68});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow6.StylePriority.UseTextAlignment = false;
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow6.Weight = 0.45053191489361721;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "Số TT";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell22.Weight = 0.032981530343007916;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Multiline = true;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "15.Mô tả hàng hóa";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell23.Weight = 0.25197889182058059;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell63.Multiline = true;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "16.Mã số hàng hóa";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.12137203166226911;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.Text = "17.Xuất xứ";
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell64.Weight = 0.097740351268547215;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Multiline = true;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            this.xrTableCell66.Text = "18.Lượng hàng";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell66.Weight = 0.097491792767983684;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "19.Đơn vị tính";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell24.Weight = 0.074556774169273865;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.StylePriority.UseTextAlignment = false;
            this.xrTableCell67.Text = "20.Đơn giá ";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell67.Weight = 0.11675461741424802;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.Text = "21.Trị giá nguyên tệ";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell68.Weight = 0.11873350923482852;
            // 
            // ReportFooter2
            // 
            this.ReportFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
            this.ReportFooter2.Height = 20;
            this.ReportFooter2.Name = "ReportFooter2";
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.Location = new System.Drawing.Point(17, 0);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable10.Size = new System.Drawing.Size(780, 20);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblThueSuatVATHang1,
            this.xtcCongHang});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 0.34894479585968952;
            // 
            // lblThueSuatVATHang1
            // 
            this.lblThueSuatVATHang1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblThueSuatVATHang1.Name = "lblThueSuatVATHang1";
            this.lblThueSuatVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatVATHang1.StylePriority.UseFont = false;
            this.lblThueSuatVATHang1.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang1.Text = "Cộng";
            this.lblThueSuatVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatVATHang1.Weight = 0.87066124912940746;
            // 
            // xtcCongHang
            // 
            this.xtcCongHang.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xtcCongHang.Name = "xtcCongHang";
            this.xtcCongHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xtcCongHang.StylePriority.UseFont = false;
            this.xtcCongHang.StylePriority.UsePadding = false;
            this.xtcCongHang.StylePriority.UseTextAlignment = false;
            this.xtcCongHang.Text = "0";
            this.xtcCongHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcCongHang.Weight = 0.12933875087059266;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrTable3,
            this.xrLabel8,
            this.xrLabel9,
            this.xrLabel10,
            this.xlbCucHaiQuan,
            this.winControlContainer1});
            this.GroupHeader1.Height = 355;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.StylePriority.UseBorders = false;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Location = new System.Drawing.Point(667, 58);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.Size = new System.Drawing.Size(100, 17);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.Text = "HQ/2012-XK";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Location = new System.Drawing.Point(17, 75);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow1,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow37});
            this.xrTable3.Size = new System.Drawing.Size(780, 280);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell15,
            this.xtcSoTN,
            this.xrTableCell16,
            this.xtcSoTK,
            this.xrTableCell92});
            this.xrTableRow25.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.StylePriority.UseFont = false;
            this.xrTableRow25.Weight = 0.79768786127167635;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.Text = " Chi cục Hải Quan đăng ký tờ khai:";
            this.xrTableCell89.Weight = 1.0554089709762533;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Text = " Số tham chiếu:";
            this.xrTableCell15.Weight = 0.41272579663080977;
            // 
            // xtcSoTN
            // 
            this.xtcSoTN.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcSoTN.Name = "xtcSoTN";
            this.xtcSoTN.StylePriority.UseBorders = false;
            this.xtcSoTN.Text = " ";
            this.xtcSoTN.Weight = 0.18226101075705298;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Text = " Số tờ khai:";
            this.xrTableCell16.Weight = 0.30264123042785862;
            // 
            // xtcSoTK
            // 
            this.xtcSoTK.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcSoTK.Name = "xtcSoTK";
            this.xtcSoTK.StylePriority.UseBorders = false;
            this.xtcSoTK.Text = " ";
            this.xtcSoTK.Weight = 0.46396658053948847;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell92.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorderColor = false;
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.StylePriority.UseForeColor = false;
            this.xrTableCell92.StylePriority.UseTextAlignment = false;
            this.xrTableCell92.Text = " Công chức đăng ký tờ khai";
            this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell92.Weight = 0.579038627027376;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcChiCucHQDK,
            this.xrTableCell94,
            this.xtcNgayDKToKhai,
            this.xtcNgayCapSoTK,
            this.xrTableCell95});
            this.xrTableRow26.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.StylePriority.UseFont = false;
            this.xrTableRow26.Weight = 0.79768786127167646;
            // 
            // xtcChiCucHQDK
            // 
            this.xtcChiCucHQDK.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcChiCucHQDK.Name = "xtcChiCucHQDK";
            this.xtcChiCucHQDK.StylePriority.UseBorders = false;
            this.xtcChiCucHQDK.Weight = 1.0554089709762533;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.Text = " Ngày, giờ gửi: ";
            this.xrTableCell94.Weight = 0.5911457276233002;
            // 
            // xtcNgayDKToKhai
            // 
            this.xtcNgayDKToKhai.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xtcNgayDKToKhai.Name = "xtcNgayDKToKhai";
            this.xtcNgayDKToKhai.StylePriority.UseBorders = false;
            this.xtcNgayDKToKhai.Text = " Ngày, giờ đăng ký: ";
            this.xtcNgayDKToKhai.Weight = 0.46320276030038565;
            // 
            // xtcNgayCapSoTK
            // 
            this.xtcNgayCapSoTK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xtcNgayCapSoTK.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtcNgayCapSoTK.Name = "xtcNgayCapSoTK";
            this.xtcNgayCapSoTK.StylePriority.UseBorders = false;
            this.xtcNgayCapSoTK.StylePriority.UseFont = false;
            this.xtcNgayCapSoTK.Weight = 0.30486350720519573;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell95.Multiline = true;
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.Weight = 0.581421250253704;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xtcChiCucHQCuaKhauXuat,
            this.xtcNgayDK,
            this.xrTableCell20,
            this.xtcSoLuongPhuLuc,
            this.xrTableCell99});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.79768786127167624;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.Text = " Chi cục Hải quan cửa khẩu xuất:";
            this.xrTableCell14.Weight = 0.76969251065557132;
            // 
            // xtcChiCucHQCuaKhauXuat
            // 
            this.xtcChiCucHQCuaKhauXuat.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcChiCucHQCuaKhauXuat.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtcChiCucHQCuaKhauXuat.Name = "xtcChiCucHQCuaKhauXuat";
            this.xtcChiCucHQCuaKhauXuat.StylePriority.UseBorders = false;
            this.xtcChiCucHQCuaKhauXuat.StylePriority.UseFont = false;
            this.xtcChiCucHQCuaKhauXuat.StylePriority.UseTextAlignment = false;
            this.xtcChiCucHQCuaKhauXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xtcChiCucHQCuaKhauXuat.Weight = 0.28955754008524459;
            // 
            // xtcNgayDK
            // 
            this.xtcNgayDK.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xtcNgayDK.Name = "xtcNgayDK";
            this.xtcNgayDK.StylePriority.UseFont = false;
            this.xtcNgayDK.Weight = 0.5911457276233002;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.Text = " Số lượng phụ lục tờ khai:";
            this.xrTableCell20.Weight = 0.59074233813679722;
            // 
            // xtcSoLuongPhuLuc
            // 
            this.xtcSoLuongPhuLuc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcSoLuongPhuLuc.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xtcSoLuongPhuLuc.Multiline = true;
            this.xtcSoLuongPhuLuc.Name = "xtcSoLuongPhuLuc";
            this.xtcSoLuongPhuLuc.StylePriority.UseBorders = false;
            this.xtcSoLuongPhuLuc.StylePriority.UseFont = false;
            this.xtcSoLuongPhuLuc.StylePriority.UseTextAlignment = false;
            this.xtcSoLuongPhuLuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xtcSoLuongPhuLuc.Weight = 0.17590572356403486;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell99.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.StylePriority.UseFont = false;
            this.xrTableCell99.StylePriority.UseTextAlignment = false;
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell99.Weight = 0.57899837629389084;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.xrTableCell25,
            this.xtcLoaiHinh});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.79768786127167624;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.Text = " 1.Người xuất khẩu:";
            this.xrTableCell101.Weight = 1.0554089709762533;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Text = " 5.Loại hình:";
            this.xrTableCell25.Weight = 0.38263141871321293;
            // 
            // xtcLoaiHinh
            // 
            this.xtcLoaiHinh.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcLoaiHinh.Name = "xtcLoaiHinh";
            this.xtcLoaiHinh.StylePriority.UseBorders = false;
            this.xtcLoaiHinh.Text = "  ";
            this.xtcLoaiHinh.Weight = 1.5580018266693729;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcNguoiXuatKhau,
            this.xtcSoGP,
            this.lbSoGP,
            this.xrTableCell2,
            this.xtcSoHopDong});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.79768786127167646;
            // 
            // xtcNguoiXuatKhau
            // 
            this.xtcNguoiXuatKhau.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xtcNguoiXuatKhau.Name = "xtcNguoiXuatKhau";
            this.xtcNguoiXuatKhau.StylePriority.UseBorders = false;
            this.xtcNguoiXuatKhau.Weight = 1.0514511873350925;
            // 
            // xtcSoGP
            // 
            this.xtcSoGP.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xtcSoGP.Name = "xtcSoGP";
            this.xtcSoGP.StylePriority.UseBorders = false;
            this.xtcSoGP.Text = " 6. Giấy phép số: ";
            this.xtcSoGP.Weight = 0.3858483864420541;
            // 
            // lbSoGP
            // 
            this.lbSoGP.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lbSoGP.Name = "lbSoGP";
            this.lbSoGP.StylePriority.UseBorders = false;
            this.lbSoGP.Weight = 0.54314491577024537;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Text = " 7.Hợp đồng:  ";
            this.xrTableCell2.Weight = 0.32726811447128057;
            // 
            // xtcSoHopDong
            // 
            this.xtcSoHopDong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcSoHopDong.Name = "xtcSoHopDong";
            this.xtcSoHopDong.StylePriority.UseBorders = false;
            this.xtcSoHopDong.Weight = 0.68832961234016632;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.xtcMSNguoiXK,
            this.xtcNgayGP,
            this.lbNgayGP,
            this.xrTableCell5,
            this.xtcNgayHopDong});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.StylePriority.UseBorders = false;
            this.xrTableRow30.Weight = 0.79768786127167612;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseTextAlignment = false;
            this.xrTableCell106.Text = "MST  ";
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell106.Weight = 0.5;
            // 
            // xtcMSNguoiXK
            // 
            this.xtcMSNguoiXK.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcMSNguoiXK.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xlbMSNguoiXuatKhau});
            this.xtcMSNguoiXK.Name = "xtcMSNguoiXK";
            this.xtcMSNguoiXK.StylePriority.UseBorders = false;
            this.xtcMSNguoiXK.StylePriority.UseBorderWidth = false;
            this.xtcMSNguoiXK.StylePriority.UseTextAlignment = false;
            this.xtcMSNguoiXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcMSNguoiXK.Weight = 0.55552567485285165;
            // 
            // xlbMSNguoiXuatKhau
            // 
            this.xlbMSNguoiXuatKhau.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xlbMSNguoiXuatKhau.BorderWidth = 2;
            this.xlbMSNguoiXuatKhau.Location = new System.Drawing.Point(0, 0);
            this.xlbMSNguoiXuatKhau.Name = "xlbMSNguoiXuatKhau";
            this.xlbMSNguoiXuatKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xlbMSNguoiXuatKhau.Size = new System.Drawing.Size(145, 25);
            this.xlbMSNguoiXuatKhau.StylePriority.UseBorders = false;
            this.xlbMSNguoiXuatKhau.StylePriority.UseBorderWidth = false;
            this.xlbMSNguoiXuatKhau.Text = "xlbMSDaiLyHQ";
            // 
            // xtcNgayGP
            // 
            this.xtcNgayGP.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xtcNgayGP.Name = "xtcNgayGP";
            this.xtcNgayGP.StylePriority.UseBorders = false;
            this.xtcNgayGP.Text = " Ngày: ";
            this.xtcNgayGP.Weight = 0.38189060280089304;
            // 
            // lbNgayGP
            // 
            this.lbNgayGP.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lbNgayGP.Name = "lbNgayGP";
            this.lbNgayGP.StylePriority.UseBorders = false;
            this.lbNgayGP.Weight = 0.54308656383194631;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.Text = " Ngày: ";
            this.xrTableCell5.Weight = 0.32723893850213104;
            // 
            // xtcNgayHopDong
            // 
            this.xtcNgayHopDong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcNgayHopDong.Name = "xtcNgayHopDong";
            this.xtcNgayHopDong.StylePriority.UseBorders = false;
            this.xtcNgayHopDong.Weight = 0.68830043637101679;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell110,
            this.xtcNgayHHGP,
            this.lbNgayHHGP,
            this.xrTableCell12,
            this.xtcNgayHHHopDong});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 0.79768786127167635;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.Text = " 2. Người nhập khẩu:";
            this.xrTableCell110.Weight = 1.0554089709762533;
            // 
            // xtcNgayHHGP
            // 
            this.xtcNgayHHGP.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcNgayHHGP.Name = "xtcNgayHHGP";
            this.xtcNgayHHGP.StylePriority.UseBorders = false;
            this.xtcNgayHHGP.Text = " Ngày hết hạn: ";
            this.xtcNgayHHGP.Weight = 0.38189060280089315;
            // 
            // lbNgayHHGP
            // 
            this.lbNgayHHGP.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbNgayHHGP.Name = "lbNgayHHGP";
            this.lbNgayHHGP.StylePriority.UseBorders = false;
            this.lbNgayHHGP.Weight = 0.54314491577024548;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.Text = " Ngày hết hạn: ";
            this.xrTableCell12.Weight = 0.32726811447128057;
            // 
            // xtcNgayHHHopDong
            // 
            this.xtcNgayHHHopDong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcNgayHHHopDong.Name = "xtcNgayHHHopDong";
            this.xtcNgayHHHopDong.StylePriority.UseBorders = false;
            this.xtcNgayHHHopDong.Weight = 0.68832961234016632;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcNguoiNhapKhau,
            this.xrTableCell114,
            this.xrTableCell115});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 0.79768786127167624;
            // 
            // xtcNguoiNhapKhau
            // 
            this.xtcNguoiNhapKhau.Name = "xtcNguoiNhapKhau";
            this.xtcNguoiNhapKhau.Weight = 1.0554089709762533;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell114.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorderColor = false;
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.StylePriority.UseForeColor = false;
            this.xrTableCell114.Text = " 8.Hóa đơn thương mại: ";
            this.xrTableCell114.Weight = 0.92348284960422178;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell115.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorderColor = false;
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.StylePriority.UseForeColor = false;
            this.xrTableCell115.Text = " 9.Cửa khẩu xuất hàng:";
            this.xrTableCell115.Weight = 1.0171503957783639;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell116,
            this.xtcHoaDonTM,
            this.xtcCuaKhauXuat});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.79768786127167646;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Text = " 3.Người ủy thác/ người được ủy quyền:";
            this.xrTableCell116.Weight = 1.0554089709762533;
            // 
            // xtcHoaDonTM
            // 
            this.xtcHoaDonTM.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcHoaDonTM.Name = "xtcHoaDonTM";
            this.xtcHoaDonTM.StylePriority.UseBorders = false;
            this.xtcHoaDonTM.StylePriority.UseTextAlignment = false;
            this.xtcHoaDonTM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcHoaDonTM.Weight = 0.92348284960422178;
            // 
            // xtcCuaKhauXuat
            // 
            this.xtcCuaKhauXuat.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcCuaKhauXuat.Name = "xtcCuaKhauXuat";
            this.xtcCuaKhauXuat.StylePriority.UseBorders = false;
            this.xtcCuaKhauXuat.StylePriority.UseTextAlignment = false;
            this.xtcCuaKhauXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcCuaKhauXuat.Weight = 1.0171503957783639;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcNguoiUT,
            this.xrTableCell3,
            this.xrTableCell4});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.79768786127167624;
            // 
            // xtcNguoiUT
            // 
            this.xtcNguoiUT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcNguoiUT.Name = "xtcNguoiUT";
            this.xtcNguoiUT.StylePriority.UseBorders = false;
            this.xtcNguoiUT.Weight = 1.0580474934036941;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 0.92084432717678111;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 1.0171503957783636;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119,
            this.xtcMSNguoiUT,
            this.xrTableCell121});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.79768786127167624;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorderWidth = false;
            this.xrTableCell119.StylePriority.UseTextAlignment = false;
            this.xrTableCell119.Text = "MST ";
            this.xrTableCell119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell119.Weight = 0.5;
            // 
            // xtcMSNguoiUT
            // 
            this.xtcMSNguoiUT.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xlbMSNguoiUT});
            this.xtcMSNguoiUT.Name = "xtcMSNguoiUT";
            this.xtcMSNguoiUT.StylePriority.UseBorderWidth = false;
            this.xtcMSNguoiUT.StylePriority.UseTextAlignment = false;
            this.xtcMSNguoiUT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xtcMSNguoiUT.Weight = 0.55552567485285165;
            this.xtcMSNguoiUT.WordWrap = false;
            // 
            // xlbMSNguoiUT
            // 
            this.xlbMSNguoiUT.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xlbMSNguoiUT.BorderWidth = 2;
            this.xlbMSNguoiUT.Location = new System.Drawing.Point(0, 0);
            this.xlbMSNguoiUT.Name = "xlbMSNguoiUT";
            this.xlbMSNguoiUT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xlbMSNguoiUT.Size = new System.Drawing.Size(145, 25);
            this.xlbMSNguoiUT.StylePriority.UseBorders = false;
            this.xlbMSNguoiUT.StylePriority.UseBorderWidth = false;
            this.xlbMSNguoiUT.StylePriority.UseTextAlignment = false;
            this.xlbMSNguoiUT.Text = "xlbMSDaiLyHQ";
            this.xlbMSNguoiUT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Text = " 10.Nước nhập khẩu: ";
            this.xrTableCell121.Weight = 1.9405165415059873;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.xtcNuocNhapKhau});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 0.79768786127167646;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.Text = " 4.Đại lý hải quan:";
            this.xrTableCell123.Weight = 1.0554089709762533;
            // 
            // xtcNuocNhapKhau
            // 
            this.xtcNuocNhapKhau.Name = "xtcNuocNhapKhau";
            this.xtcNuocNhapKhau.Weight = 1.9406332453825856;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcDaiLyHQ,
            this.xrTableCell26,
            this.xtcDieuKienGiaoHang,
            this.xrTableCell28,
            this.xtcPhuongThucThanhToan});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.797687861271676;
            // 
            // xtcDaiLyHQ
            // 
            this.xtcDaiLyHQ.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcDaiLyHQ.Name = "xtcDaiLyHQ";
            this.xtcDaiLyHQ.StylePriority.UseBorders = false;
            this.xtcDaiLyHQ.Weight = 1.0554089709762533;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Text = " 11.Điều kiện giao hàng:";
            this.xrTableCell26.Weight = 0.54624517962248831;
            // 
            // xtcDieuKienGiaoHang
            // 
            this.xtcDieuKienGiaoHang.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcDieuKienGiaoHang.Name = "xtcDieuKienGiaoHang";
            this.xtcDieuKienGiaoHang.StylePriority.UseBorders = false;
            this.xtcDieuKienGiaoHang.Text = " ";
            this.xtcDieuKienGiaoHang.Weight = 0.37723766998173336;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.Text = " 12.Phương thức thanh toán:";
            this.xrTableCell28.Weight = 0.61612543129693509;
            // 
            // xtcPhuongThucThanhToan
            // 
            this.xtcPhuongThucThanhToan.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcPhuongThucThanhToan.Name = "xtcPhuongThucThanhToan";
            this.xtcPhuongThucThanhToan.StylePriority.UseBorders = false;
            this.xtcPhuongThucThanhToan.Weight = 0.40102496448142877;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.xtcMSDaiLyHQ,
            this.xrTableCell27,
            this.xtcDongTienTT,
            this.xrTableCell29,
            this.xtcTiGiaTinhThue});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 0.959537572254336;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.StylePriority.UseTextAlignment = false;
            this.xrTableCell129.Text = "MST ";
            this.xrTableCell129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell129.Weight = 0.5;
            // 
            // xtcMSDaiLyHQ
            // 
            this.xtcMSDaiLyHQ.BackColor = System.Drawing.Color.Transparent;
            this.xtcMSDaiLyHQ.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xlbMSDaiLyHQ});
            this.xtcMSDaiLyHQ.Name = "xtcMSDaiLyHQ";
            this.xtcMSDaiLyHQ.StylePriority.UseBackColor = false;
            this.xtcMSDaiLyHQ.StylePriority.UseBorderColor = false;
            this.xtcMSDaiLyHQ.StylePriority.UseBorders = false;
            this.xtcMSDaiLyHQ.StylePriority.UseBorderWidth = false;
            this.xtcMSDaiLyHQ.Weight = 0.55552567485285165;
            // 
            // xlbMSDaiLyHQ
            // 
            this.xlbMSDaiLyHQ.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xlbMSDaiLyHQ.BorderWidth = 2;
            this.xlbMSDaiLyHQ.Location = new System.Drawing.Point(0, 0);
            this.xlbMSDaiLyHQ.Name = "xlbMSDaiLyHQ";
            this.xlbMSDaiLyHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xlbMSDaiLyHQ.Size = new System.Drawing.Size(145, 25);
            this.xlbMSDaiLyHQ.StylePriority.UseBorders = false;
            this.xlbMSDaiLyHQ.StylePriority.UseBorderWidth = false;
            this.xlbMSDaiLyHQ.StylePriority.UseTextAlignment = false;
            this.xlbMSDaiLyHQ.Text = "xlbMSDaiLyHQ";
            this.xlbMSDaiLyHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.Text = " 13.Đồng tiền thanh toán: ";
            this.xrTableCell27.Weight = 0.54624517962248831;
            // 
            // xtcDongTienTT
            // 
            this.xtcDongTienTT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcDongTienTT.Name = "xtcDongTienTT";
            this.xtcDongTienTT.StylePriority.UseBorders = false;
            this.xtcDongTienTT.Weight = 0.37723766998173336;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.Text = " 14.Tỉ giá tính thuế:";
            this.xrTableCell29.Weight = 0.616067079358636;
            // 
            // xtcTiGiaTinhThue
            // 
            this.xtcTiGiaTinhThue.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcTiGiaTinhThue.Name = "xtcTiGiaTinhThue";
            this.xtcTiGiaTinhThue.StylePriority.UseBorders = false;
            this.xtcTiGiaTinhThue.Weight = 0.4009666125431296;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.Location = new System.Drawing.Point(192, 0);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.Size = new System.Drawing.Size(417, 25);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "             TỜ KHAI HÀNG HÓA XUẤT KHẨU";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.Location = new System.Drawing.Point(17, 0);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.Size = new System.Drawing.Size(208, 23);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "HẢI QUAN VIỆT NAM";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.Location = new System.Drawing.Point(17, 33);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.Size = new System.Drawing.Size(100, 23);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Cục Hải Quan:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xlbCucHaiQuan
            // 
            this.xlbCucHaiQuan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xlbCucHaiQuan.Font = new System.Drawing.Font("Times New Roman", 10.5F, System.Drawing.FontStyle.Bold);
            this.xlbCucHaiQuan.Location = new System.Drawing.Point(133, 33);
            this.xlbCucHaiQuan.Name = "xlbCucHaiQuan";
            this.xlbCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xlbCucHaiQuan.Size = new System.Drawing.Size(433, 25);
            this.xlbCucHaiQuan.StylePriority.UseBorders = false;
            this.xlbCucHaiQuan.StylePriority.UseFont = false;
            this.xlbCucHaiQuan.StylePriority.UseTextAlignment = false;
            this.xlbCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.Location = new System.Drawing.Point(625, 8);
            this.winControlContainer1.LockedInUserDesigner = true;
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.Size = new System.Drawing.Size(175, 29);
            this.winControlContainer1.WinControl = this.label1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "                                                     \r\n\r\n";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.GroupFooter1.Height = 280;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTable7
            // 
            this.xrTable7.Location = new System.Drawing.Point(17, 0);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24});
            this.xrTable7.Size = new System.Drawing.Size(780, 272);
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcChungTuDiKem,
            this.xrTableCell82});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 5.6;
            // 
            // xtcChungTuDiKem
            // 
            this.xtcChungTuDiKem.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcChungTuDiKem.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xlbChungTuDinhKem,
            this.xrLabel1});
            this.xtcChungTuDiKem.Name = "xtcChungTuDiKem";
            this.xtcChungTuDiKem.StylePriority.UseBorders = false;
            this.xtcChungTuDiKem.Weight = 1.2176781002638522;
            // 
            // xlbChungTuDinhKem
            // 
            this.xlbChungTuDinhKem.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xlbChungTuDinhKem.Location = new System.Drawing.Point(0, 25);
            this.xlbChungTuDinhKem.Multiline = true;
            this.xlbChungTuDinhKem.Name = "xlbChungTuDinhKem";
            this.xlbChungTuDinhKem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xlbChungTuDinhKem.Size = new System.Drawing.Size(317, 117);
            this.xlbChungTuDinhKem.StylePriority.UseBorders = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Location = new System.Drawing.Point(0, 0);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(317, 25);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.Text = "26. Chứng từ đính kèm:";
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell82.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5,
            this.xrLabel6,
            this.xrLabel7});
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseBorders = false;
            this.xrTableCell82.Weight = 1.7823218997361474;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Location = new System.Drawing.Point(150, 0);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.Size = new System.Drawing.Size(250, 33);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "27.Tôi xin cam đoan, chịu trách nhiệm trước         pháp luật về nội dung khai tr" +
                "ên tờ khai";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Location = new System.Drawing.Point(218, 33);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.Size = new System.Drawing.Size(175, 17);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.Text = "Ngày     tháng     năm";
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Location = new System.Drawing.Point(168, 58);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.Size = new System.Drawing.Size(233, 23);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.Text = "(Người khai ký, ghi rõ họ tên, đóng dấu)";
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcKetQuaPL,
            this.xrTableCell84,
            this.xrTableCell85});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 2.0799999999999996;
            // 
            // xtcKetQuaPL
            // 
            this.xtcKetQuaPL.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcKetQuaPL.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.lbKetQuaPL});
            this.xtcKetQuaPL.Multiline = true;
            this.xtcKetQuaPL.Name = "xtcKetQuaPL";
            this.xtcKetQuaPL.StylePriority.UseBorders = false;
            this.xtcKetQuaPL.Text = "  ";
            this.xtcKetQuaPL.Weight = 1.2176781002638522;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Location = new System.Drawing.Point(0, 0);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.Size = new System.Drawing.Size(317, 19);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.Text = "28. Kết quả phân luồng và hướng dẫn thủ tục hải quan:";
            // 
            // lbKetQuaPL
            // 
            this.lbKetQuaPL.Location = new System.Drawing.Point(0, 19);
            this.lbKetQuaPL.Multiline = true;
            this.lbKetQuaPL.Name = "lbKetQuaPL";
            this.lbKetQuaPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lbKetQuaPL.Size = new System.Drawing.Size(317, 32);
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.StylePriority.UseTextAlignment = false;
            this.xrTableCell84.Text = " 30. Xác nhận thông quan";
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell84.Weight = 0.66526927219304066;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.StylePriority.UseTextAlignment = false;
            this.xrTableCell85.Text = " 31. Xác nhận của hải quan giám sát";
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell85.Weight = 1.1170526275431068;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 3.2;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseBorders = false;
            this.xrTableCell86.Text = " 29. Ghi chép khác:";
            this.xrTableCell86.Weight = 1.2176781002638522;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBorders = false;
            this.xrTableCell87.Weight = 0.66526927219304066;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.Weight = 1.1170526275431068;
            // 
            // DetailLuongHang
            // 
            this.DetailLuongHang.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.ReportHeader,
            this.ReportFooter});
            this.DetailLuongHang.Level = 2;
            this.DetailLuongHang.Name = "DetailLuongHang";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.Detail3.Height = 20;
            this.Detail3.Name = "Detail3";
            // 
            // xrTable8
            // 
            this.xrTable8.Location = new System.Drawing.Point(17, 0);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable8.Size = new System.Drawing.Size(780, 20);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcLuongHang_SoTT,
            this.xtcLuongHang_SoHieuContainer,
            this.xtcLuongHang_SoLuongKien,
            this.xtcLuongHang_TrongLuongHang,
            this.xtcLuongHang_DDDongHang});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.StylePriority.UseTextAlignment = false;
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow4.Weight = 1.32;
            // 
            // xtcLuongHang_SoTT
            // 
            this.xtcLuongHang_SoTT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcLuongHang_SoTT.Multiline = true;
            this.xtcLuongHang_SoTT.Name = "xtcLuongHang_SoTT";
            this.xtcLuongHang_SoTT.StylePriority.UseBorders = false;
            this.xtcLuongHang_SoTT.StylePriority.UseTextAlignment = false;
            xrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Count;
            xrSummary2.IgnoreNullValues = true;
            this.xtcLuongHang_SoTT.Summary = xrSummary2;
            this.xtcLuongHang_SoTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcLuongHang_SoTT.Weight = 0.16754617414248019;
            this.xtcLuongHang_SoTT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcLuongHang_SoTT_BeforePrint);
            // 
            // xtcLuongHang_SoHieuContainer
            // 
            this.xtcLuongHang_SoHieuContainer.Name = "xtcLuongHang_SoHieuContainer";
            this.xtcLuongHang_SoHieuContainer.Weight = 0.662269129287599;
            // 
            // xtcLuongHang_SoLuongKien
            // 
            this.xtcLuongHang_SoLuongKien.Name = "xtcLuongHang_SoLuongKien";
            this.xtcLuongHang_SoLuongKien.Weight = 0.85356200527704451;
            // 
            // xtcLuongHang_TrongLuongHang
            // 
            this.xtcLuongHang_TrongLuongHang.Name = "xtcLuongHang_TrongLuongHang";
            this.xtcLuongHang_TrongLuongHang.Weight = 0.816622691292876;
            // 
            // xtcLuongHang_DDDongHang
            // 
            this.xtcLuongHang_DDDongHang.Name = "xtcLuongHang_DDDongHang";
            this.xtcLuongHang_DDDongHang.Weight = 0.5;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.ReportHeader.Height = 50;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(17, 0);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3});
            this.xrTable1.Size = new System.Drawing.Size(780, 50);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "25.Lượng hàng, số hiệu container";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 3;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Số\r\n TT";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 0.16754617414248019;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "a.Số hiệu container";
            this.xrTableCell8.Weight = 0.662269129287599;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "b.Số lượng kiện trong container";
            this.xrTableCell9.Weight = 0.85356200527704451;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "c.Trọng lượng hàng trong container";
            this.xrTableCell10.Weight = 0.816622691292876;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "d.Địa điểm đóng hàng";
            this.xrTableCell11.Weight = 0.5;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
            this.ReportFooter.Height = 20;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrTable11
            // 
            this.xrTable11.Location = new System.Drawing.Point(17, 0);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable11.Size = new System.Drawing.Size(780, 20);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell30,
            this.xtcLuongHang_CongTL,
            this.xrTableCell21});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow5.Weight = 1.32;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Multiline = true;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell17.Weight = 0.16754617414248019;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 0.662269129287599;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.85356200527704451;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "Cộng:";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell30.Weight = 0.36600365333874574;
            // 
            // xtcLuongHang_CongTL
            // 
            this.xtcLuongHang_CongTL.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcLuongHang_CongTL.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtcLuongHang_CongTL.Name = "xtcLuongHang_CongTL";
            this.xtcLuongHang_CongTL.StylePriority.UseBorders = false;
            this.xtcLuongHang_CongTL.StylePriority.UseFont = false;
            this.xtcLuongHang_CongTL.StylePriority.UseTextAlignment = false;
            this.xtcLuongHang_CongTL.Text = "                    ";
            this.xtcLuongHang_CongTL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xtcLuongHang_CongTL.Weight = 0.45061903795413039;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Weight = 0.5;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // TQDTToKhaiXK_TT15
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.DetailThue,
            this.DetailHangHoa,
            this.GroupHeader1,
            this.GroupFooter1,
            this.DetailLuongHang});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(5, 15, 4, 18);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtHangHoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.DetailReportBand DetailThue;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailHangHoa;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader2;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xtcThueXuat_SoTT;
        private DevExpress.XtraReports.UI.XRTableCell xtcThueXuat_TriGiaTT;
        private DevExpress.XtraReports.UI.XRTableCell xtcThueXuat_TiLe;
        private DevExpress.XtraReports.UI.XRTableCell xtcThueXuat_TienThue;
        private DevExpress.XtraReports.UI.XRTableCell xtcThuKhac_TriGia;
        private DevExpress.XtraReports.UI.XRTableCell xtcThuKhac_TyLe;
        private DevExpress.XtraReports.UI.XRTableCell xtcThuKhac_SoTien;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xtcThuKhac_Cong;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter2;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xtcChungTuDiKem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xtcKetQuaPL;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xtcSoTN;
        private DevExpress.XtraReports.UI.XRTableCell xtcSoTK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xtcNgayDK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xtcLoaiHinh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xtcNgayGP;
        private DevExpress.XtraReports.UI.XRTableCell xtcNgayHopDong;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xtcNgayHHGP;
        private DevExpress.XtraReports.UI.XRTableCell xtcNgayHHHopDong;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xtcHoaDonTM;
        private DevExpress.XtraReports.UI.XRTableCell xtcCuaKhauXuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xtcMSNguoiUT;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xtcNuocNhapKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xtcDieuKienGiaoHang;
        private DevExpress.XtraReports.UI.XRTableCell xtcPhuongThucThanhToan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xtcDongTienTT;
        private DevExpress.XtraReports.UI.XRTableCell xtcTiGiaTinhThue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xtcChiCucHQDK;
        private DevExpress.XtraReports.UI.XRTableCell xtcChiCucHQCuaKhauXuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xtcNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xtcSoGP;
        private DevExpress.XtraReports.UI.XRTableCell xtcSoHopDong;
        private DevExpress.XtraReports.UI.XRTableCell xtcMSNguoiXK;
        private DevExpress.XtraReports.UI.XRTableCell xtcNguoiUT;
        private DevExpress.XtraReports.UI.XRTableCell xtcDaiLyHQ;
        private DevExpress.XtraReports.UI.XRTableCell xtcMSDaiLyHQ;
        private DevExpress.XtraReports.UI.XRTableCell xtcNgayDKToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xtcSoLuongPhuLuc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.DetailReportBand DetailLuongHang;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_SoTT;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_SoHieuContainer;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_SoLuongKien;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_TrongLuongHang;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_DDDongHang;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_CongTL;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTable xrtHangHoa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xtcSoTTHang;
        private DevExpress.XtraReports.UI.XRTableCell xtcMoTaHang;
        private DevExpress.XtraReports.UI.XRTableCell xtcMaSoHang;
        private DevExpress.XtraReports.UI.XRTableCell xtcXuatXuHang;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang;
        private DevExpress.XtraReports.UI.XRTableCell xtcDVT_Hang;
        private DevExpress.XtraReports.UI.XRTableCell xtcDonGia_Hang;
        private DevExpress.XtraReports.UI.XRTableCell xtcTriGiaNguyenTe_Hang;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang1;
        private DevExpress.XtraReports.UI.XRTableCell xtcCongHang;
        private DevExpress.XtraReports.UI.XRLabel xlbCucHaiQuan;
        private DevExpress.XtraReports.UI.XRTableCell xtcThueXuat_Cong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xlbChungTuDinhKem;
        private DevExpress.XtraReports.UI.XRLabel xlbMSDaiLyHQ;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRLabel xlbMSNguoiXuatKhau;
        private DevExpress.XtraReports.UI.XRLabel xlbMSNguoiUT;
        private DevExpress.XtraReports.UI.XRTableCell xtcNguoiXuatKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xtcTongTienBangSo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xtcTongTienBangChu;
        private DevExpress.XtraReports.UI.XRTableCell xtcNgayCapSoTK;
        private DevExpress.XtraReports.UI.XRTableCell lbSoGP;
        private DevExpress.XtraReports.UI.XRTableCell lbNgayGP;
        private DevExpress.XtraReports.UI.XRTableCell lbNgayHHGP;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lbKetQuaPL;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
    }
}
