namespace Company.Interface
{
    partial class BaocaoThongkeTK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.Common.JanusColorScheme janusColorScheme2 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.GridEX.GridEXLayout grdList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaocaoThongkeTK));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.CbTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.CbDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.grdList = new Janus.Windows.GridEX.GridEX();
            this.compareValidator1 = new Company.Controls.CustomValidation.CompareValidator();
            this.CbLoaiHinh = new Janus.Windows.EditControls.UIComboBox();
            this.btnView = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txtNguoiLB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNguoiDB = new System.Windows.Forms.TextBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ckbLuuMacdinh = new Janus.Windows.EditControls.UICheckBox();
            this.txtDVBC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compareValidator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.grdList);
            this.grbMain.Size = new System.Drawing.Size(773, 420);
            // 
            // visualStyleManager1
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "Office2003";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme2.Name = "Office2007";
            janusColorScheme2.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme1);
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme2);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Loại hình tở khai";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Từ Ngày";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Đến Ngày";
            // 
            // CbTuNgay
            // 
            // 
            // 
            // 
            this.CbTuNgay.DropDownCalendar.Name = "";
            this.CbTuNgay.Location = new System.Drawing.Point(117, 43);
            this.CbTuNgay.Name = "CbTuNgay";
            this.CbTuNgay.Size = new System.Drawing.Size(160, 21);
            this.CbTuNgay.TabIndex = 3;
            // 
            // CbDenNgay
            // 
            // 
            // 
            // 
            this.CbDenNgay.DropDownCalendar.Name = "";
            this.CbDenNgay.Location = new System.Drawing.Point(117, 70);
            this.CbDenNgay.Name = "CbDenNgay";
            this.CbDenNgay.Size = new System.Drawing.Size(160, 21);
            this.CbDenNgay.TabIndex = 5;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnSearch.Location = new System.Drawing.Point(362, 18);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(88, 23);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Thống kê";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            this.btnSearch.WordWrap = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // grdList
            // 
            this.grdList.AlternatingColors = true;
            this.grdList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            grdList_DesignTimeLayout.LayoutString = resources.GetString("grdList_DesignTimeLayout.LayoutString");
            this.grdList.DesignTimeLayout = grdList_DesignTimeLayout;
            this.grdList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grdList.GroupByBoxVisible = false;
            this.grdList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grdList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList.HeaderFormatStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(74)))), ((int)(((byte)(147)))));
            this.grdList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdList.Location = new System.Drawing.Point(13, 130);
            this.grdList.Name = "grdList";
            this.grdList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdList.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.False;
            this.grdList.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.grdList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdList.Size = new System.Drawing.Size(748, 278);
            this.grdList.TabIndex = 2;
            this.grdList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grdList.VisualStyleManager = this.visualStyleManager1;
            // 
            // compareValidator1
            // 
            this.compareValidator1.ControlToCompare = this.CbTuNgay;
            this.compareValidator1.ControlToValidate = this.CbDenNgay;
            this.compareValidator1.ErrorMessage = "Đến ngày phải lớn hơn từ ngày";
            this.compareValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("compareValidator1.Icon")));
            this.compareValidator1.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.compareValidator1.Tag = "compareValidator1";
            // 
            // CbLoaiHinh
            // 
            this.CbLoaiHinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Xuất khẩu";
            uiComboBoxItem1.Value = "X";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Nhập khẩu";
            uiComboBoxItem2.Value = "N";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Tất cả";
            uiComboBoxItem3.Value = "";
            this.CbLoaiHinh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3});
            this.CbLoaiHinh.Location = new System.Drawing.Point(117, 16);
            this.CbLoaiHinh.Name = "CbLoaiHinh";
            this.CbLoaiHinh.Size = new System.Drawing.Size(160, 21);
            this.CbLoaiHinh.TabIndex = 1;
            this.CbLoaiHinh.VisualStyleManager = this.visualStyleManager1;
            // 
            // btnView
            // 
            this.btnView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnView.Enabled = false;
            this.btnView.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnView.Location = new System.Drawing.Point(362, 45);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(88, 23);
            this.btnView.TabIndex = 8;
            this.btnView.Text = "Xem báo cáo";
            this.btnView.VisualStyleManager = this.vsmMain;
            this.btnView.WordWrap = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.CbLoaiHinh);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.CbTuNgay);
            this.uiGroupBox1.Controls.Add(this.CbDenNgay);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(13, 6);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(283, 118);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Kỳ thực hiện thống kê";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Người lập biểu";
            // 
            // txtNguoiLB
            // 
            this.txtNguoiLB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNguoiLB.Location = new System.Drawing.Point(111, 40);
            this.txtNguoiLB.Name = "txtNguoiLB";
            this.txtNguoiLB.Size = new System.Drawing.Size(245, 21);
            this.txtNguoiLB.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Người duyệt biểu";
            // 
            // txtNguoiDB
            // 
            this.txtNguoiDB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNguoiDB.Location = new System.Drawing.Point(111, 65);
            this.txtNguoiDB.Name = "txtNguoiDB";
            this.txtNguoiDB.Size = new System.Drawing.Size(245, 21);
            this.txtNguoiDB.TabIndex = 5;
            this.txtNguoiDB.TextChanged += new System.EventHandler(this.txtNguoiDB_TextChanged);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ckbLuuMacdinh);
            this.uiGroupBox2.Controls.Add(this.txtDVBC);
            this.uiGroupBox2.Controls.Add(this.btnView);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtNguoiDB);
            this.uiGroupBox2.Controls.Add(this.btnSearch);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.txtNguoiLB);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(302, 6);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(459, 118);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // ckbLuuMacdinh
            // 
            this.ckbLuuMacdinh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbLuuMacdinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbLuuMacdinh.Location = new System.Drawing.Point(9, 89);
            this.ckbLuuMacdinh.Name = "ckbLuuMacdinh";
            this.ckbLuuMacdinh.Size = new System.Drawing.Size(347, 25);
            this.ckbLuuMacdinh.TabIndex = 6;
            this.ckbLuuMacdinh.Text = "Lưu mặc định";
            this.ckbLuuMacdinh.CheckedChanged += new System.EventHandler(this.ckbLuuMacdinh_CheckedChanged);
            // 
            // txtDVBC
            // 
            this.txtDVBC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDVBC.Location = new System.Drawing.Point(111, 18);
            this.txtDVBC.Name = "txtDVBC";
            this.txtDVBC.Size = new System.Drawing.Size(245, 21);
            this.txtDVBC.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Đơn vị báo cáo";
            // 
            // BaocaoThongkeTK
            // 
            this.ClientSize = new System.Drawing.Size(773, 420);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BaocaoThongkeTK";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Thống Kê Tờ Khai ";
            this.Load += new System.EventHandler(this.BaocaoThongkeTK_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compareValidator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.CalendarCombo.CalendarCombo CbDenNgay;
        private Janus.Windows.CalendarCombo.CalendarCombo CbTuNgay;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.GridEX.GridEX grdList;
        private Company.Controls.CustomValidation.CompareValidator compareValidator1;
        private Janus.Windows.EditControls.UIComboBox CbLoaiHinh;
        private Janus.Windows.EditControls.UIButton btnView;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.TextBox txtDVBC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNguoiDB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNguoiLB;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UICheckBox ckbLuuMacdinh;
    }
}
