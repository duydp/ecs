﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Windows.Forms;
using System.IO;
using KeyServer;

namespace LicenseCode
{
    [XmlRoot("Management")]
    public class Management
    {
        public List<Products> Products { get; set; }
       

        public string Note { get; set; }


        public static Management loadXML()
        {
            try
            {
                string path = Application.StartupPath + "\\Management.xml";
                FileStream mystream = new FileStream(path,FileMode.Open);
                System.Xml.Serialization.XmlSerializer xmls = new XmlSerializer(typeof(Management));
                Management management = (Management)xmls.Deserialize(mystream);
                mystream.Close();
                return management;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Không tìm thấy file mã sản phẩm: " + ex.Message);
                return null;
            }
            
        }
        public bool CreateProduct(string MaSp)
        {
            try
            {
                string path = Application.StartupPath + "\\Management.xml";
                Products.Add(new Products { Id = MaSp });
                //FileInfo file = new FileInfo(path);
                //if (!file.Exists)
                //{
                //}
                FileStream mystream = new FileStream(path, FileMode.Create);
                System.Xml.Serialization.XmlSerializer xmls = new XmlSerializer(typeof(Management));
                xmls.Serialize(mystream, this);
                mystream.Close();
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }
        public bool CreatePrefix(int indexProducts, string prefix)
        {
            try
            {
                string path = Application.StartupPath + "\\Management.xml";
                if (Products[indexProducts].Prefix == null) Products[indexProducts].Prefix = new List<string>();
                Products[indexProducts].Prefix.Add(prefix);
                FileStream mystream = new FileStream(path, FileMode.Create);
                System.Xml.Serialization.XmlSerializer xmls = new XmlSerializer(typeof(Management));
                xmls.Serialize(mystream, this);
                mystream.Close();
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }
        public bool IsExitProducts(string productID)
        {
            try
            {
                foreach (Products item  in this.Products )
                {
                    if (productID.ToUpper() == item.Id.ToUpper() || productID.Equals(item.Id))
                        return true;       
                }
                return false;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Không kiểm tra được mã sản phẩm. " + ex.Message);
                return false;
            }
        }
        public bool IsExitPrefix(int indexProducts,string prefix)
        {
            try
            {
                if (Products[indexProducts].Prefix == null || Products[indexProducts].Prefix.Count == 0)
                    return false;
                foreach (string item in Products[indexProducts].Prefix)
                {
                    if (prefix.ToUpper() == item.ToUpper() || prefix.Equals(item))
                        return true;
                }
                return false;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Không kiểm tra được mã sản phẩm. " + ex.Message);
                return false;
            }
        }
        public bool IsExitDays(int indexProducts,bool trial, int days)
        {
            
            if (trial)
            {
                if (Products[indexProducts].Trial.Days == null || Products[indexProducts].Trial.Days.Count == 0)
                    return false;
                foreach (int item in Products[indexProducts].Trial.Days)
                {
                    if (days == item) return true;
                }
            }
            else
            {
                if (Products[indexProducts].License.Days == null || Products[indexProducts].License.Days.Count == 0)
                    return false;
                foreach (int item in Products[indexProducts].License.Days)
                {
                    if (days == item) return true;
                }
            }
            return false;
        }
        public Key GetKey(string productID, string RequestBy, int soNgay)
        {
            Key key = new Key();
            try
            {
                string querry = string.Format(@"ProductId = '{0}' AND RequestedBy is NULL AND Days = {1} AND year(ActivedDate) = 1900 AND Status = '0'", productID, soNgay.ToString());
                List<Key> listkey = Key.SelectCollectionDynamic(querry, "CreatedDate desc");
                if (listkey == null || listkey.Count == 0)
                    return null;
                key = listkey[0];
                key.RequestedBy = RequestBy;
                key.RequestedDate = DateTime.Now;
                key.Update();
                return key;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Lỗi xảy ra khi tìm key: " + ex.Message);
                return null;
            }
        }
        public Key checkKey(string activecode)
        {
            Key key = new Key();
            try
            {
                string querry = string.Format(@"ActivedKey = '{0}'", activecode);
                List<Key> listkey = Key.SelectCollectionDynamic(querry,null);
                if (listkey == null || listkey.Count == 0)
                    return null;
                key = listkey[0];
                return key;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi xảy ra khi tìm thông tin key: "+ex.Message);
                return null;
            }
        }
    }
}
