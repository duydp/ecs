﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
using DevExpress.XtraRichEdit.API.Word;
using System.Collections.Generic;

namespace Company.Interface.Report.VNACCS
{
    public partial class ThongBaoAnDinhThueHangHoaXNK : DevExpress.XtraReports.UI.XtraReport
    {
        public ThongBaoAnDinhThueHangHoaXNK()
        {
            InitializeComponent();
        }
        public void BindReport(VAF8020 vaf802)
        {
            lblTenCoQuanHQ.Text = vaf802.CSM.GetValue().ToString();
            lblTenChiCucHQ.Text = vaf802.CBN.GetValue().ToString();
            lblSoChungTu.Text = vaf802.SPN.GetValue().ToString();
            lblSoToKhai.Text = vaf802.ICN.GetValue().ToString();
            if (Convert.ToDateTime(vaf802.DDC.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayDKToKhai.Text = Convert.ToDateTime(vaf802.DDC.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayDKToKhai.Text = "";
            lblMaLoaiHinh.Text = vaf802.DKC.GetValue().ToString();
            lblTenDonViXNK.Text = vaf802.IEN.GetValue().ToString();
            lblMaDonViXNK.Text = vaf802.IEC.GetValue().ToString();
            lblMaBuuChinh.Text = vaf802.PCD.GetValue().ToString();
            lblDiaChiNguoiXNK.Text = vaf802.ADB.GetValue().ToString();
            lblSoDienThoai.Text = vaf802.TEL.GetValue().ToString();
            lblTenNHBaoLanh.Text = vaf802.SBN.GetValue().ToString();
            lblMaNHBaoLanh.Text = vaf802.SBK.GetValue().ToString();
            lblKyHieuChungTuBaoLanh.Text = vaf802.SBS.GetValue().ToString();
            lblSoChungTuBaoLanh.Text = vaf802.SRN.GetValue().ToString();
            lblLoaiBaoLanh.Text = vaf802.TSN.GetValue().ToString();
            lblTenNHTraThay.Text = vaf802.BNM.GetValue().ToString();
            lblMaNHTraThay.Text = vaf802.BCD.GetValue().ToString();
            lblKyHieuChungTuHanMuc.Text = vaf802.BPS.GetValue().ToString();
            lblSoHieuHanMuc.Text = vaf802.BPN.GetValue().ToString();
            BindReportTax(vaf802);
            lblTongSoThueKhaiBao.Text = vaf802.TDA.GetValue().ToString();
            lblTongSoThueAnDinh.Text = vaf802.TA4.GetValue().ToString();
            lblTongSoThueChenhLech.Text = vaf802.TDQ.GetValue().ToString();
            lblMaTienTe.Text = vaf802.CCC.GetValue().ToString();
            lblTyGia.Text = vaf802.EXR.GetValue().ToString();
            lblSoNgayAnHan.Text = vaf802.LOP.GetValue().ToString();
            if (Convert.ToDateTime(vaf802.EFD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayHetHieuLuc.Text = Convert.ToDateTime(vaf802.EFD.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayHetHieuLuc.Text = "";
            lblSoTKKhoBac.Text = vaf802.TAN.GetValue().ToString();
            lblTenKhoBac.Text = vaf802.TNM.GetValue().ToString();
            lblLaiSuatPhatChamNop.Text = vaf802.ARR.GetValue().ToString();
            if (Convert.ToDateTime(vaf802.DOP.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayPhatHanhChungTu.Text = Convert.ToDateTime(vaf802.DOP.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayPhatHanhChungTu.Text = "";
        }


        private void BindReportTax(VAF8020 vaf802)
        {

            try
            {
                string[] chuong = {"Ghi","theo","chương","của bộ","chủ","quản"};

                DataTable dt = new DataTable();
                dt.Columns.Add("SacThue", typeof(string));
                dt.Columns.Add("Chuong", typeof(string));
                dt.Columns.Add("TieuMuc", typeof(string));
                dt.Columns.Add("SoThueKhaiBao", typeof(string));
                dt.Columns.Add("SoThueAnDinh", typeof(string));
                dt.Columns.Add("SoThueChenhLech", typeof(string));
                for (int i = 0; i < vaf802.TN1.listAttribute[0].ListValue.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["SacThue"] = vaf802.TN1.listAttribute[0].GetValueCollection(i).ToString();
                    dr["Chuong"] = chuong[i];
                    dr["TieuMuc"] = vaf802.TN1.listAttribute[1].GetValueCollection(i).ToString();
                    dr["SoThueKhaiBao"] = vaf802.TN1.listAttribute[2].GetValueCollection(i).ToString();
                    dr["SoThueAnDinh"] = vaf802.TN1.listAttribute[3].GetValueCollection(i).ToString();
                    dr["SoThueChenhLech"] = vaf802.TN1.listAttribute[4].GetValueCollection(i).ToString();
                    dt.Rows.Add(dr);
                }

                while (dt.Rows.Count < 6)
                {


                    DataRow dr = dt.NewRow();
                    dr["SacThue"] = "";
                    dr["Chuong"] = chuong[dt.Rows.Count];
                    dr["TieuMuc"] = "";
                    dr["SoThueKhaiBao"] = "";
                    dr["SoThueAnDinh"] = "";
                    dr["SoThueChenhLech"] = "";

                    dt.Rows.Add(dr);
                }
                DetailReport.DataSource = dt;
                lblTenSacThue.DataBindings.Add("Text", DetailReport.DataSource, "SacThue");
                lblChuong.DataBindings.Add("Text", DetailReport.DataSource, "Chuong");
                lblTieuMuc.DataBindings.Add("Text", DetailReport.DataSource, "TieuMuc");
                lblSoThueKhaiBao.DataBindings.Add("Text", DetailReport.DataSource, "SoThueKhaiBao");
                lblSoThueAnDinh.DataBindings.Add("Text", DetailReport.DataSource, "SoThueAnDinh");
                lblSoThueChenhLech.DataBindings.Add("Text", DetailReport.DataSource, "SoThueChenhLech");



            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }

        }
    }
}
