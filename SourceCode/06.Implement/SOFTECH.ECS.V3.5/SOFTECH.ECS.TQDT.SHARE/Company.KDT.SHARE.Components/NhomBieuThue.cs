﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public class NhomBieuThue
    {
        private string _loaiThue;

        public string LoaiThue
        {
            get { return _loaiThue; }
            set { _loaiThue = value; }
        }
        private string _thueSuat;

        public string ThueSuat
        {
            get { return _thueSuat; }
            set { _thueSuat = value; }
        }
        private string _ngayHieuLuc;

        public string NgayHieuLuc
        {
            get { return _ngayHieuLuc; }
            set { _ngayHieuLuc = value; }
        }
        private string _canCuPhapLy;

        public string CanCuPhapLy
        {
            get { return _canCuPhapLy; }
            set { _canCuPhapLy = value; }
        }
    }
}
