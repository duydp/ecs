﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.GC
{
    public class NodeDinhMuc
    {

        /// <summary>
        /// Thông tin về chứng từ
        /// </summary>
        public const string Declaration = "Declaration";
        /// <summary>
        /// Mo ta issue
        /// </summary>
        public const string issuer = "issuer";
        /// <summary>
        /// Thông tin sản phẩm || Nguyên liệu
        /// </summary>
        public const string Commodity = "Commodity";
        /// <summary>
        /// Tên sản phẩm || Mã nguyên liệu
        /// </summary>
        public const string description = "description";
        /// <summary>
        /// Mã sản phẩm || Tên nguyên liệu
        /// </summary>
        public const string identification = "identification";
        /// <summary>
        /// Mã HS sản phẩm || Mã HS nguyên liệu
        /// </summary>
        public const string tariffClassification = "tariffClassification";
        /// <summary>
        /// Sản phẩm 
        /// </summary>
        public const string GoodsMeasure = "GoodsMeasure";
        /// <summary>
        /// Đơn vị tính sản phẩm || Đơn vị tính đăng ký 
        /// </summary>
        public const string measureUnit = "measureUnit";
        /// <summary>
        /// Định mức 1 sản phẩm 
        /// </summary>
        public const string ProductionNorm = "ProductionNorm";
        /// <summary>
        /// Sản phẩm được khai báo định mức
        /// </summary>
        public const string Product = "Product";
        /// <summary>
        /// Chi tiết định mức
        /// </summary>
        public const string MeterialsNorm = "MeterialsNorm";
        /// <summary>
        /// Nguyên liệu cấu thành sản phẩm
        /// </summary>
        public const string Material = "Material";	
        /// <summary>
        /// Định mức gia công
        /// </summary>
        public const string norm = "norm";	 
        /// <summary>
        /// Tỷ lệ hao hụt
        /// </summary>
        public const string loss = "loss";

    }
}
