﻿/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * Modify: LanNT 14/09/2011
 * 
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ECS.AutoUpdate
{
    public class ConstFile
    {
        public const string TEMPFOLDERNAME = "TempFolder";
        public const string CONFIGFILEKEY = "config_";
        public const string FILENAME = "AutoUpdater.config";
        public const string ROOLBACKFILE = "AppAutoUpdate.exe";
        public const string MESSAGETITLE = "Cập nhật phần mềm";
        public const string CANCELORNOT = "Tiến trình đang thực hiện. Bạn có thật sự muốn hủy không?";
        public const string APPLYTHEUPDATE = "Phần mềm cần phải khởi động lại. Vui lòng kích nút ok để khởi động lại!";
        public const string NOTNETWORK = "AppAutoUpdate.exe cập nhật không thành công. AppAutoUpdate.exe sẽ khởi động lại. Vui lòng thử cập nhật lại.";
        public const string FILEDOWNLOADOLD = "fileDownload.old";
        public const string FILEDOWNLOADOLDERROR = "fileDownloadError.old";
    }
}
