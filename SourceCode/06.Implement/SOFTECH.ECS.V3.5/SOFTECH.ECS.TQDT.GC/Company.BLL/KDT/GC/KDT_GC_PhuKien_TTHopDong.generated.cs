﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_PhuKien_TTHopDong : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long LoaiPhuKien_ID { set; get; }
		public string PTTT_ID { set; get; }
		public string NguyenTe_ID { set; get; }
		public string NuocThue_ID { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string TenDoanhNghiep { set; get; }
		public string DiaChiDoanhNghiep { set; get; }
		public string DonViDoiTac { set; get; }
		public string TenDonViDoiTac { set; get; }
		public string DiaChiDoiTac { set; get; }
		public double TongTriGiaSP { set; get; }
		public double TongTriGiaTienCong { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_PhuKien_TTHopDong> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_PhuKien_TTHopDong> collection = new List<KDT_GC_PhuKien_TTHopDong>();
			while (reader.Read())
			{
				KDT_GC_PhuKien_TTHopDong entity = new KDT_GC_PhuKien_TTHopDong();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiPhuKien_ID"))) entity.LoaiPhuKien_ID = reader.GetInt64(reader.GetOrdinal("LoaiPhuKien_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocThue_ID"))) entity.NuocThue_ID = reader.GetString(reader.GetOrdinal("NuocThue_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoanhNghiep"))) entity.DiaChiDoanhNghiep = reader.GetString(reader.GetOrdinal("DiaChiDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViDoiTac"))) entity.DonViDoiTac = reader.GetString(reader.GetOrdinal("DonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac"))) entity.DiaChiDoiTac = reader.GetString(reader.GetOrdinal("DiaChiDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaSP"))) entity.TongTriGiaSP = reader.GetDouble(reader.GetOrdinal("TongTriGiaSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTienCong"))) entity.TongTriGiaTienCong = reader.GetDouble(reader.GetOrdinal("TongTriGiaTienCong"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_PhuKien_TTHopDong> collection, long id)
        {
            foreach (KDT_GC_PhuKien_TTHopDong item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_PhuKien_TTHopDong VALUES(@LoaiPhuKien_ID, @PTTT_ID, @NguyenTe_ID, @NuocThue_ID, @MaDoanhNghiep, @TenDoanhNghiep, @DiaChiDoanhNghiep, @DonViDoiTac, @TenDonViDoiTac, @DiaChiDoiTac, @TongTriGiaSP, @TongTriGiaTienCong, @GhiChu)";
            string update = "UPDATE t_KDT_GC_PhuKien_TTHopDong SET LoaiPhuKien_ID = @LoaiPhuKien_ID, PTTT_ID = @PTTT_ID, NguyenTe_ID = @NguyenTe_ID, NuocThue_ID = @NuocThue_ID, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, DiaChiDoanhNghiep = @DiaChiDoanhNghiep, DonViDoiTac = @DonViDoiTac, TenDonViDoiTac = @TenDonViDoiTac, DiaChiDoiTac = @DiaChiDoiTac, TongTriGiaSP = @TongTriGiaSP, TongTriGiaTienCong = @TongTriGiaTienCong, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhuKien_TTHopDong WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, "LoaiPhuKien_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PTTT_ID", SqlDbType.NVarChar, "PTTT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocThue_ID", SqlDbType.Char, "NuocThue_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, "DiaChiDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViDoiTac", SqlDbType.NVarChar, "DonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, "TenDonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaSP", SqlDbType.Float, "TongTriGiaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaTienCong", SqlDbType.Float, "TongTriGiaTienCong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, "LoaiPhuKien_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PTTT_ID", SqlDbType.NVarChar, "PTTT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocThue_ID", SqlDbType.Char, "NuocThue_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, "DiaChiDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViDoiTac", SqlDbType.NVarChar, "DonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, "TenDonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaSP", SqlDbType.Float, "TongTriGiaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaTienCong", SqlDbType.Float, "TongTriGiaTienCong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_PhuKien_TTHopDong VALUES(@LoaiPhuKien_ID, @PTTT_ID, @NguyenTe_ID, @NuocThue_ID, @MaDoanhNghiep, @TenDoanhNghiep, @DiaChiDoanhNghiep, @DonViDoiTac, @TenDonViDoiTac, @DiaChiDoiTac, @TongTriGiaSP, @TongTriGiaTienCong, @GhiChu)";
            string update = "UPDATE t_KDT_GC_PhuKien_TTHopDong SET LoaiPhuKien_ID = @LoaiPhuKien_ID, PTTT_ID = @PTTT_ID, NguyenTe_ID = @NguyenTe_ID, NuocThue_ID = @NuocThue_ID, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, DiaChiDoanhNghiep = @DiaChiDoanhNghiep, DonViDoiTac = @DonViDoiTac, TenDonViDoiTac = @TenDonViDoiTac, DiaChiDoiTac = @DiaChiDoiTac, TongTriGiaSP = @TongTriGiaSP, TongTriGiaTienCong = @TongTriGiaTienCong, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhuKien_TTHopDong WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, "LoaiPhuKien_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PTTT_ID", SqlDbType.NVarChar, "PTTT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocThue_ID", SqlDbType.Char, "NuocThue_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, "DiaChiDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViDoiTac", SqlDbType.NVarChar, "DonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, "TenDonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaSP", SqlDbType.Float, "TongTriGiaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaTienCong", SqlDbType.Float, "TongTriGiaTienCong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, "LoaiPhuKien_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PTTT_ID", SqlDbType.NVarChar, "PTTT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocThue_ID", SqlDbType.Char, "NuocThue_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, "DiaChiDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViDoiTac", SqlDbType.NVarChar, "DonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, "TenDonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaSP", SqlDbType.Float, "TongTriGiaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaTienCong", SqlDbType.Float, "TongTriGiaTienCong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_PhuKien_TTHopDong Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_PhuKien_TTHopDong> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}
        public static KDT_GC_PhuKien_TTHopDong LoadBy(long LoaiPhuKien_ID)
        {
            const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectBy_LoaiPhuKien_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, LoaiPhuKien_ID);
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<KDT_GC_PhuKien_TTHopDong> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_PhuKien_TTHopDong> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_PhuKien_TTHopDong> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_GC_PhuKien_TTHopDong> SelectCollectionBy_LoaiPhuKien_ID(long loaiPhuKien_ID)
		{
            IDataReader reader = SelectReaderBy_LoaiPhuKien_ID(loaiPhuKien_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_LoaiPhuKien_ID(long loaiPhuKien_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectBy_LoaiPhuKien_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, loaiPhuKien_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_LoaiPhuKien_ID(long loaiPhuKien_ID)
		{
			const string spName = "p_KDT_GC_PhuKien_TTHopDong_SelectBy_LoaiPhuKien_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, loaiPhuKien_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_PhuKien_TTHopDong(long loaiPhuKien_ID, string pTTT_ID, string nguyenTe_ID, string nuocThue_ID, string maDoanhNghiep, string tenDoanhNghiep, string diaChiDoanhNghiep, string donViDoiTac, string tenDonViDoiTac, string diaChiDoiTac, double tongTriGiaSP, double tongTriGiaTienCong, string ghiChu)
		{
			KDT_GC_PhuKien_TTHopDong entity = new KDT_GC_PhuKien_TTHopDong();	
			entity.LoaiPhuKien_ID = loaiPhuKien_ID;
			entity.PTTT_ID = pTTT_ID;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.NuocThue_ID = nuocThue_ID;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.DiaChiDoanhNghiep = diaChiDoanhNghiep;
			entity.DonViDoiTac = donViDoiTac;
			entity.TenDonViDoiTac = tenDonViDoiTac;
			entity.DiaChiDoiTac = diaChiDoiTac;
			entity.TongTriGiaSP = tongTriGiaSP;
			entity.TongTriGiaTienCong = tongTriGiaTienCong;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, LoaiPhuKien_ID);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.NVarChar, PTTT_ID);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, DiaChiDoanhNghiep);
			db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
			db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
			db.AddInParameter(dbCommand, "@TongTriGiaSP", SqlDbType.Float, TongTriGiaSP);
			db.AddInParameter(dbCommand, "@TongTriGiaTienCong", SqlDbType.Float, TongTriGiaTienCong);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_PhuKien_TTHopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhuKien_TTHopDong item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_PhuKien_TTHopDong(long id, long loaiPhuKien_ID, string pTTT_ID, string nguyenTe_ID, string nuocThue_ID, string maDoanhNghiep, string tenDoanhNghiep, string diaChiDoanhNghiep, string donViDoiTac, string tenDonViDoiTac, string diaChiDoiTac, double tongTriGiaSP, double tongTriGiaTienCong, string ghiChu)
		{
			KDT_GC_PhuKien_TTHopDong entity = new KDT_GC_PhuKien_TTHopDong();			
			entity.ID = id;
			entity.LoaiPhuKien_ID = loaiPhuKien_ID;
			entity.PTTT_ID = pTTT_ID;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.NuocThue_ID = nuocThue_ID;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.DiaChiDoanhNghiep = diaChiDoanhNghiep;
			entity.DonViDoiTac = donViDoiTac;
			entity.TenDonViDoiTac = tenDonViDoiTac;
			entity.DiaChiDoiTac = diaChiDoiTac;
			entity.TongTriGiaSP = tongTriGiaSP;
			entity.TongTriGiaTienCong = tongTriGiaTienCong;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_PhuKien_TTHopDong_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, LoaiPhuKien_ID);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.NVarChar, PTTT_ID);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, DiaChiDoanhNghiep);
			db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
			db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
			db.AddInParameter(dbCommand, "@TongTriGiaSP", SqlDbType.Float, TongTriGiaSP);
			db.AddInParameter(dbCommand, "@TongTriGiaTienCong", SqlDbType.Float, TongTriGiaTienCong);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_PhuKien_TTHopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhuKien_TTHopDong item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_PhuKien_TTHopDong(long id, long loaiPhuKien_ID, string pTTT_ID, string nguyenTe_ID, string nuocThue_ID, string maDoanhNghiep, string tenDoanhNghiep, string diaChiDoanhNghiep, string donViDoiTac, string tenDonViDoiTac, string diaChiDoiTac, double tongTriGiaSP, double tongTriGiaTienCong, string ghiChu)
		{
			KDT_GC_PhuKien_TTHopDong entity = new KDT_GC_PhuKien_TTHopDong();			
			entity.ID = id;
			entity.LoaiPhuKien_ID = loaiPhuKien_ID;
			entity.PTTT_ID = pTTT_ID;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.NuocThue_ID = nuocThue_ID;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.DiaChiDoanhNghiep = diaChiDoanhNghiep;
			entity.DonViDoiTac = donViDoiTac;
			entity.TenDonViDoiTac = tenDonViDoiTac;
			entity.DiaChiDoiTac = diaChiDoiTac;
			entity.TongTriGiaSP = tongTriGiaSP;
			entity.TongTriGiaTienCong = tongTriGiaTienCong;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, LoaiPhuKien_ID);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.NVarChar, PTTT_ID);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, DiaChiDoanhNghiep);
			db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
			db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
			db.AddInParameter(dbCommand, "@TongTriGiaSP", SqlDbType.Float, TongTriGiaSP);
			db.AddInParameter(dbCommand, "@TongTriGiaTienCong", SqlDbType.Float, TongTriGiaTienCong);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_PhuKien_TTHopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhuKien_TTHopDong item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_PhuKien_TTHopDong(long id)
		{
			KDT_GC_PhuKien_TTHopDong entity = new KDT_GC_PhuKien_TTHopDong();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_LoaiPhuKien_ID(long loaiPhuKien_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_DeleteBy_LoaiPhuKien_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LoaiPhuKien_ID", SqlDbType.BigInt, loaiPhuKien_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKien_TTHopDong_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_PhuKien_TTHopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhuKien_TTHopDong item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}