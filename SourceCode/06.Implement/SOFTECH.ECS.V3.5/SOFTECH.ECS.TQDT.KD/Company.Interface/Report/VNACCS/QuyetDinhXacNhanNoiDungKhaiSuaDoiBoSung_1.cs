using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1 : DevExpress.XtraReports.UI.XtraReport
    {
        DataTable data = new DataTable();
        public QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1()
        {
            InitializeComponent();
        }
        public void BindReport(VAL8000 VAL,string maNV)
        {
            xrLabel1.Text = EnumThongBao.GetTenNV(maNV);
            lblSoPL.Text = "1/" + Convert.ToString(VAL.HangMD.Count + 1);
            lblSoThongBao.Text =VAL.ADD.Value.ToString();
            if (Convert.ToDateTime(VAL.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayHoanThanhKT.Text = Convert.ToDateTime(VAL.AD2.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayHoanThanhKT.Text = "";
            if (VAL.AD3.GetValue().ToString().ToUpper() != "" && VAL.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioHoanThanhKT.Text = VAL.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + VAL.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + VAL.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioHoanThanhKT.Text = "";
            }
            lblSoToKhaiBoSung.Text = VAL.A03.Value.ToString();
            //lblNgayDKKhaiBoSung.Text = VAL.A02.Value.ToString();
            //lblGioDKKhaiBoSung.Text = VAL.AD1.Value.ToString();
            if (Convert.ToDateTime(VAL.A02.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayDKKhaiBoSung.Text = Convert.ToDateTime(VAL.A02.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayDKKhaiBoSung.Text = "";

            string time = VAL.AD1.Value.ToString();
            lblGioDKKhaiBoSung.Text = time.Substring(0, 2) + ":" + time.Substring(2, 2) + ":" + time.Substring(4, 2);
            lblCoQuanNhan.Text = VAL.A00.Value.ToString();
            lblNhomXuLyHoSo.Text = VAL.A01.Value.ToString();
            lblPhanLoaiXNK.Text = VAL.ADG.Value.ToString();
            lblSoToKhaiXNK.Text = VAL.A27.Value.ToString();
            lblMaLoaiHinh.Text = VAL.ADQ.Value.ToString();
            //lblNgayToKhaiXNK.Text = VAL.A29.Value.ToString();
            if (Convert.ToDateTime(VAL.A29.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayToKhaiXNK.Text = Convert.ToDateTime(VAL.A29.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayToKhaiXNK.Text = "";
            lblDauHieuBaoQua.Text = VAL.ADE.Value.ToString();
            //lblMaHetThoiHan.Text =VAL.CTO.Value.ToString();
            //lblNgayCapPhep.Text = VAL.A30.Value.ToString();
            //lblThoiHanTaiNhapXuat.Text = VAL.ADR.Value.ToString();
            if (Convert.ToDateTime(VAL.A30.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayCapPhep.Text = Convert.ToDateTime(VAL.A30.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayCapPhep.Text = "";
            if (Convert.ToDateTime(VAL.ADR.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblThoiHanTaiNhapXuat.Text = Convert.ToDateTime(VAL.ADR.GetValue()).ToString("dd/MM/yyyy");
            else
                lblThoiHanTaiNhapXuat.Text = "";
            lblMaNguoiKhai.Text = VAL.A04.Value.ToString();
            lblTenNguoiKhai.Text = VAL.A05.Value.ToString();
            lblMaBuuChinh.Text = VAL.A06.Value.ToString();
            lblDiaChi.Text = VAL.A07.Value.ToString();
            lblSoDT.Text = VAL.A11.Value.ToString();
            lblMaDaiLyHQ.Text = VAL.A12.Value.ToString();
            lblTenDaiLyHQ.Text = VAL.A13.Value.ToString();
            lblMaNhanVienHQ.Text = VAL.A14.Value.ToString();
            lblMaLyDoKhaiBS.Text = VAL.A15.Value.ToString();
            lblPhanLoaiNopThue.Text = VAL.A16.Value.ToString();
            lblMaXacDinhThoiHanNop.Text = VAL.ADL.Value.ToString();
            //lblNgayHetHieuLuc.Text = VAL.ADS.Value.ToString();
            if (Convert.ToDateTime(VAL.ADS.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayHetHieuLuc.Text = Convert.ToDateTime(VAL.ADS.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayHetHieuLuc.Text = "";
            lblSoNgayAnHan.Text = VAL.ADT.Value.ToString();
            lblSoNgayAnHanVAT.Text = VAL.ADU.Value.ToString();
            lblSoQuanLyNoiBo.Text = VAL.A20.Value.ToString();
            lblMaTienTeTruocKhaiBao.Text = VAL.ADH.Value.ToString();
            if (!String.IsNullOrEmpty(VAL.ADI.Value.ToString().Trim()))
            {
               lblTyGiaHoiDoaiTruocKhaiBao.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(VAL.ADI.Value.ToString().Replace(".", ",")));
            }
            else
            {
                lblTyGiaHoiDoaiTruocKhaiBao.Text = "0,000";
            }
            lblGhiChuTruocKhaiBao.Text = VAL.ADJ.Value.ToString();
            if (!String.IsNullOrEmpty(VAL.ADK.Value.ToString().Trim()))
            {
                lblMaTienTeSauKhaiBao.Text = VAL.ADK.Value.ToString();
            }
            else
            {
                lblMaTienTeSauKhaiBao.Text = "";
            }
            if (!String.IsNullOrEmpty(VAL.A21.Value.ToString().Trim()))
            {
                lblTyGiaHoiDoaiSauKhaiBao.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(VAL.A21.Value.ToString().Replace(".", ",")));
            }
            else
            {
                lblTyGiaHoiDoaiSauKhaiBao.Text = "0,000";
            }
            lblGhiChuSauKhaiBao.Text = VAL.A22.Value.ToString();
            lblBieuThiSoTienTangGiamThueXNK.Text = VAL.ADB.Value.ToString();
            lblTongSoTienTangGiamThueXNK.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(VAL.A23.Value.ToString().Replace(".", ",")));
            lblMaTienTeThueXNK.Text = VAL.ADN.Value.ToString();

            //lblMaKhoanMucTiepNhan.Text = VAL.KA1.Value.ToString();

            //lblTenKhoanMucTiepNhan.Text =VAL.KB1.Value.ToString();
            //lblBieuThiSoTienTangGiamThueVaThuKhac.Text =VAL.KU1.Value.ToString();
            //lblTongSoTienTangGiamThueVaThuKhac.Text = VAL.KC1.Value.ToString();
            //lblMaTienTeThueVaThuKhac.Text =VAL.KW1.Value.ToString();
            lblTongSoTrang.Text = VAL.A24.Value.ToString();
            lblTongSoDongHang.Text = VAL.A25.Value.ToString();
            lblLyDo.Text = VAL.ADO.Value.ToString();
            lblTenNguoiPhuTrach.Text = VAL.ADF.Value.ToString();
            lblTenTruongDv.Text = VAL.ADP.Value.ToString();
            data = ReportViewVNACCS.ConvertListToTable(VAL.KA1, 5);
            BindReportSatThue(data);

        }
        private void BindReportSatThue(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblMaKhoanMucTiepNhan.DataBindings.Add("Text", DetailReport.DataSource, "VAL8000_KA1");
            lblTenKhoanMucTiepNhan.DataBindings.Add("Text", DetailReport.DataSource, "VAL8000_KB1");
            lblBieuThiSoTienTangGiamThueVaThuKhac.DataBindings.Add("Text", DetailReport.DataSource, "VAL8000_KU1");
            lblTongSoTienTangGiamThueVaThuKhac.DataBindings.Add("Text", DetailReport.DataSource, "VAL8000_KC1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaTienTeThueVaThuKhac.DataBindings.Add("Text", DetailReport.DataSource, "VAL8000_KW1");
        }
    }
}
