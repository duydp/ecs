﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
    /// <summary>
    /// Doanh nghiệp được đảm bảo nghĩa vụ nộp thuế
    /// </summary>
    public class TaxGuarantee
    {

        ///<summary>
        ///Có được đảm bảo nghĩa vụ nộp thuế không (0...1)
        ///</summary>
        [XmlElement("isGuarantee")]
        public string IsGuarantee { set; get; }
        ///<summary>
        ///Hình thức đảm bảo 
        ///</summary>
        [XmlElement("type")]
        public string Type { set; get; }
        ///<summary>
        ///Trị giá đảm bảo (18,4)
        ///</summary>
        [XmlElement("value")]
        public string Value { set; get; }
        ///<summary>
        ///Ngày bắt đầu đảm bảo (YYYY-MM-DD)
        ///</summary>
        [XmlElement("issue")]
        public string Issue { set; get; }
        ///<summary>
        ///Ngày kết thúc đảm bảo (YYYY-MM-DD)
        ///</summary>
        [XmlElement("expire")]
        public string Expire { set; get; }

    }
}
