﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using System.Globalization;
using System.Threading;
using Company.KD.BLL.KDT;
using GlobalsShare = Company.KDT.SHARE.Components.Globals;
using Company.KDT.SHARE.QuanLyChungTu;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using DuLieuChuan = Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.DuLieuChuan;
namespace Company.GC.BLL.DataTransferObjectMapper
{
    /// <summary>
    /// Pattern transfer 2 object 
    /// http://msdn.microsoft.com/en-us/library/ff649585.aspx
    /// </summary>
    public class Mapper
    {
        private static string sfmtDate = "yyyy-MM-dd";
        private static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        /// <summary>
        /// HopDong to GC_HopDong
        /// </summary>
        /// <param name="hopdong"></param>
        /// <param name="diaChiDoanhNghiep"></param>
        /// <returns></returns>
        public static GC_HopDong ToDataTransferObject_GC_HopDong(KDT.GC.HopDong hopdong, string diaChiDoanhNghiep,bool isCancel)
        {

            bool isEdit = hopdong.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            //bool isSua = hopdong.ActionStatus == (short)ActionStatus.HopDongSua;
            //if (isEdit)
            //    hopdong.SoTiepNhan = KhaiBaoSua.SoTNSua(hopdong.SoTiepNhan, hopdong.NamTN, string.Empty,
            //                hopdong.MaHaiQuan, hopdong.MaDoanhNghiep, LoaiKhaiBao.HopDong);
            GC_HopDong hd = new GC_HopDong()
            {
                Issuer = DeclarationIssuer.HOP_DONG_GIA_CONG,
                Reference = hopdong.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                //Function = isEdit || isCancel ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = isEdit || isCancel ? hopdong.SoTiepNhan.ToString() : string.Empty,
                Acceptance = isEdit || isCancel ? hopdong.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                DeclarationOffice =Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hopdong.MaHaiQuan).Trim() : hopdong.MaHaiQuan.Trim(),
                Agents = new List<Agent>(),
                AdditionalInformationNew=new AdditionalDocument{
                    Content=hopdong.GhiChu,
                },
                Importer = new NameBase { 
                    Name = hopdong.TenDoanhNghiep, 
                    Identity = hopdong.MaDoanhNghiep 
                },
                ContractDocument = new ContractDocument
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    Expire = hopdong.NgayHetHan.ToString(sfmtDate),
                    IsInverseProcedure = hopdong.IsGiaCongNguoc.ToString(),
                    Payment = new Payment 
                    { 
                        Method = hopdong.PTTT_ID 
                    },
                    CurrencyExchange = new CurrencyExchange 
                    { 
                        CurrencyType = hopdong.NguyenTe_ID 
                    },
                    Importer = new NameBase 
                    { 
                        Identity = hopdong.MaDoanhNghiep,
                        Name = hopdong.TenDoanhNghiep,
                        Address = diaChiDoanhNghiep 
                    },
                    Exporter = new NameBase 
                    { 
                        Identity = hopdong.DonViDoiTac,
                        Name = hopdong.TenDonViDoiTac,
                        Address = hopdong.DiaChiDoiTac 
                    },
                    CustomsValue = new CustomsValue 
                    { 
                        TotalPaymentValue = Helpers.FormatNumeric(hopdong.TongTriGiaTienCong, GlobalsShare.TriGiaNT),
                        TotalProductValue = Helpers.FormatNumeric(hopdong.TongTriGiaSP, GlobalsShare.TriGiaNT) 
                    },
                    //Old
                    //ImportationCountry = "VN",
                    //ExportationCountry = hopdong.NuocThue_ID.Trim(),

                   //AdditionalInformation = new List<AdditionalInformation>(),
                  // AdditionalInformations = new List<AdditionalInformation>()
                },
            };
            //hd.AdditionalInformations.Add
            //    (new AdditionalInformation
            //    {
            //        GhiChu = hopdong.GhiChu,
            //    });

            //New

            hd.ImportationCountry = "VN";
            hd.ExportationCountry = hopdong.NuocThue_ID.Trim();
            if (isCancel)
            {
                hd.Function = DeclarationFunction.HUY;
                hd.CustomsReference = hopdong.SoTiepNhan.ToString();
                hd.Acceptance = hopdong.NgayTiepNhan.ToString(sfmtDateTime);
            }
            else if (isEdit)
            {
                hd.Function = DeclarationFunction.SUA;
                hd.CustomsReference = hopdong.SoTiepNhan.ToString();
                hd.Acceptance = hopdong.NgayTiepNhan.ToString(sfmtDateTime);
            }
            else
            {
                hd.Function = DeclarationFunction.KHAI_BAO;
                hd.CustomsReference = String.Empty;
                hd.Acceptance = String.Empty;
            }
            //hd.ContractDocument.AdditionalInformations.Add(new AdditionalInformation
            //{
            //    Content = new Content
            //    {
            //        Text = hopdong.GhiChu
            //    }
            //});
            #region add Agent
            hd.Agents.Add(new Agent
                         {
                             Identity = hopdong.MaDoanhNghiep,
                             Name = hopdong.TenDoanhNghiep,
                             Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                         });
            //hd.Agents.Add(new Agent
            //            {
            //                Identity = hopdong.MaDoanhNghiep,
            //                Name = hopdong.TenDoanhNghiep,
            //                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            //            });
            #endregion

            #region Add ContractItem - Thông tin nhóm sản phẩm GC
            if (hopdong.NhomSPCollection != null && hopdong.NhomSPCollection.Count > 0)
            {
                hd.ContractDocument.Items = new List<Item>();
                foreach (KDT.GC.NhomSanPham item in hopdong.NhomSPCollection)
                    hd.ContractDocument.Items.Add(new Item
                    {
                        Identity = item.MaSanPham,
                        Name = item.TenSanPham,
                        Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                        ProductValue = Helpers.FormatNumeric(item.TriGia, 4),
                        PaymentValue = Helpers.FormatNumeric(item.GiaGiaCong, 4),
                    });
            }
            #endregion

            #region Add Product - Thông tin sản phẩm
            if (hopdong.SPCollection != null && hopdong.SPCollection.Count > 0)
            {
                hd.ContractDocument.Products = new List<Product>();
                foreach (KDT.GC.SanPham item in hopdong.SPCollection)
                    hd.ContractDocument.Products.Add(new Product
                    {
                        Commodity = new Commodity
                        {
                            Description = item.Ten,
                            Identification = item.Ma,
                            TariffClassification = item.MaHS,
                            ProductGroup = item.NhomSanPham_ID
                        },
                        GoodsMeasure = new GoodsMeasure 
                        { 
                            MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID 
                        }
                    });
            }
            #endregion

            #region Add Materials - Thông tin nguyên phụ liệu
            if (hopdong.NPLCollection != null && hopdong.NPLCollection.Count > 0)
            {
                hd.ContractDocument.Materials = new List<Product>();
                foreach (KDT.GC.NguyenPhuLieu item in hopdong.NPLCollection)
                    hd.ContractDocument.Materials.Add(new Product
                    {
                        Commodity = new Commodity
                        {
                            Description = item.Ten,
                            Identification = item.Ma, 
                            TariffClassification = item.MaHS,
                            Origin = item.TuCungUng ? "2" : "1"

                        },
                        GoodsMeasure = new GoodsMeasure 
                        { 
                            MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID 
                        }
                    });
            }
            #endregion

            #region Add Equipments - Thông tin thiết bị
            if (hopdong.TBCollection != null && hopdong.TBCollection.Count > 0)
            {
                hd.ContractDocument.Equipments = new List<Equipment>();
                foreach (KDT.GC.ThietBi item in hopdong.TBCollection)
                    hd.ContractDocument.Equipments.Add(new Equipment
                    {
                        Commodity = new Commodity
                        {
                            Description = item.Ten,
                            Identification = item.Ma,
                            TariffClassification = item.MaHS
                        },
                        GoodsMeasure = new GoodsMeasure 
                        { 
                            Quantity = Helpers.FormatNumeric(item.SoLuongDangKy, 0) ,
                            MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID                            
                        },
                        Origin = new Origin 
                        { 
                            OriginCountry = item.NuocXX_ID.Trim() 
                        },
                        CurrencyExchange = new CurrencyExchange 
                        { 
                            CurrencyType = item.NguyenTe_ID 
                        },                        
                        CustomsValue = new CustomsValue 
                        { 
                            unitPrice = Helpers.FormatNumeric(item.DonGia, GlobalsShare.DonGiaNT) 
                        },
                        Status = item.TinhTrang
                    });
            }
            #endregion Add Hàng Mẫu

            #region  Add Sample product - Thông tin hàng mẫu
            if (hopdong.SPCollection != null && hopdong.HangMauCollection.Count > 0)
            {
                hd.ContractDocument.SampleProducts = new List<Product>();
                foreach (KDT.GC.HangMau item in hopdong.HangMauCollection)
                    hd.ContractDocument.SampleProducts.Add(new Product
                    {
                        Commodity = new Commodity
                        {
                            Description = item.Ten,
                            Identification = item.Ma,
                            TariffClassification = item.MaHS,
                        },
                        GoodsMeasure = new GoodsMeasure 
                        { 
                            Quantity = Helpers.FormatNumeric(item.SoLuongDangKy, GlobalsShare.TriGiaNT) ,
                            MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID
                        }
                    });
            }
            #endregion
            return hd;
        }
        public static DeclarationBase HuyKhaiBao(string loaiToKhai, string reference, long soTiepNhan, string MaHaiQuan, DateTime ngayTiepNhan)
        {
            DeclarationBase dec = new DeclarationBase()
            {
                Issuer = loaiToKhai,
                Reference = reference,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(soTiepNhan),
                Acceptance = ngayTiepNhan.ToString(sfmtDateTime),
                DeclarationOffice = MaHaiQuan.Trim(),
                AdditionalInformations = new List<AdditionalInformation>()
            };
            dec.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Content = new Content() { Text = "." }
                });
            return dec;
        }

        public static GC_DinhMuc ToDataTransferDinhMuc(DinhMucDangKy dmdk, HopDong hopdong, string tenDN, bool Cancel)
        {

            bool isEdit = dmdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            //if (isEdit)
            //    dmdk.SoTiepNhan = KhaiBaoSua.SoTNSua(dmdk.SoTiepNhan, Convert.ToInt32(dmdk.NamTN), string.Empty, dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, LoaiKhaiBao.DinhMuc);
            
            GC_DinhMuc dinhmuc = new GC_DinhMuc()
            {
                Issuer = DeclarationIssuer.GC_DINH_MUC,
                Reference = dmdk.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = dmdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? DeclarationFunction.KHAI_BAO : DeclarationFunction.SUA,                               
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = isEdit  ? Helpers.FormatNumeric(dmdk.SoTiepNhan) : string.Empty,
                Acceptance = isEdit   ? dmdk.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdk.MaHaiQuan).Trim() : dmdk.MaHaiQuan.Trim(),
                Agents = new List<Agent>(),
                ContractReference = new ContractDocument()
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hopdong.MaHaiQuan).Trim() : hopdong.MaHaiQuan.Trim(),
                    Expire = hopdong.NgayHetHan.ToString(sfmtDate) ,
                    ProductionNormType = dmdk.LoaiDinhMuc.ToString(),
                }

            };
            dinhmuc.Agents.Add(new Agent()
            {
                Name = hopdong.TenDoanhNghiep,
                Identity = dmdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            dinhmuc.Importer = new NameBase()
            {
                Identity = dmdk.MaDoanhNghiep,
                Name = hopdong.TenDoanhNghiep
            };
            if (Cancel)
            {
                dinhmuc.Function = DeclarationFunction.HUY;
                dinhmuc.Acceptance = dmdk.NgayTiepNhan.ToString(sfmtDateTime);
                dinhmuc.CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan);
            }
            else if (isEdit)
            {
                dinhmuc.Function = DeclarationFunction.SUA;
                dinhmuc.Acceptance = dmdk.NgayTiepNhan.ToString(sfmtDateTime);
                dinhmuc.CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan);
            }
            else
            {
                dinhmuc.Function = DeclarationFunction.KHAI_BAO;
                dinhmuc.Acceptance = dmdk.NgayTiepNhan.ToString(sfmtDateTime);
                dinhmuc.CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan);
            }
            if (hopdong.TenDoanhNghiep == null || hopdong.TenDoanhNghiep.Trim() == "")
            {
                foreach (Agent item in dinhmuc.Agents)
                {
                    item.Name = tenDN;
                }
                dinhmuc.Importer.Name = tenDN;
            }
            dinhmuc.ProductionNorms = new List<ProductionNorm>();
            List<ProductionNorm> products = dinhmuc.ProductionNorms;

            int index = 0;
            foreach (DinhMuc dmSP in dmdk.DMCollection.ToArray().Distinct(new DistinctMaSP()))
            {              
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.Ma = dmSP.MaSanPham;
                sp.HopDong_ID = hopdong.ID;
                sp.Load();
                ProductionNorm p = new ProductionNorm()
                {
                    Product = new Product()
                    {
                        Commodity = new Commodity()
                        {
                            Description = sp.Ten,
                            Identification = sp.Ma,
                            TariffClassification = sp.MaHS ,
                            ProductCtrlNo = dmSP.MaDDSX,
                        },
                        GoodsMeasure = new GoodsMeasure 
                        { 
                            MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(sp.DVT_ID.Trim()) : sp.DVT_ID.Trim() 
                        }
                    }
                };
                p.MaterialsNorms = NguyenPhuLieu.GetNPLFromDinhMuc(dmdk.ID, dmSP.MaSanPham, hopdong.ID);
                if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS)
                {
                    foreach (MaterialsNorm item in p.MaterialsNorms)
                    {
                        item.Material.GoodsMeasure.MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.Material.GoodsMeasure.MeasureUnit.Trim());

                    }
                }

                products.Add(p);
                index++;
            }

            return dinhmuc;
        }

        public static GC_DinhMuc ToDataTransferDinhMuc(Company.GC.BLL.GC.KDT_GC_DinhMucThucTeDangKy dmdk, HopDong hopdong, string tenDN, bool Cancel)
        {

            bool isEdit = dmdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            GC_DinhMuc dinhmuc = new GC_DinhMuc()
            {
                Issuer = DeclarationIssuer.GC_DINH_MUC,
                Reference = dmdk.GuidString,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = dmdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? DeclarationFunction.KHAI_BAO : DeclarationFunction.SUA,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = isEdit ? Helpers.FormatNumeric(dmdk.SoTiepNhan) : string.Empty,
                Acceptance = isEdit ? dmdk.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdk.MaHaiQuan).Trim() : dmdk.MaHaiQuan.Trim(),
                Agents = new List<Agent>(),
                ContractReference = new ContractDocument()
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hopdong.MaHaiQuan).Trim() : hopdong.MaHaiQuan.Trim(),
                    Expire = hopdong.NgayHetHan.ToString(sfmtDate),
                    ProductionNormType = "1"
                }

            };
            dinhmuc.Agents.Add(new Agent()
            {
                Name = hopdong.TenDoanhNghiep,
                Identity = dmdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            dinhmuc.Importer = new NameBase()
            {
                Identity = dmdk.MaDoanhNghiep,
                Name = hopdong.TenDoanhNghiep
            };
            if (Cancel)
            {
                dinhmuc.Function = DeclarationFunction.HUY;
                dinhmuc.Acceptance = dmdk.NgayTiepNhan.ToString(sfmtDateTime);
                dinhmuc.CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan);
            }
            else if (isEdit)
            {
                dinhmuc.Function = DeclarationFunction.SUA;
                dinhmuc.Acceptance = dmdk.NgayTiepNhan.ToString(sfmtDateTime);
                dinhmuc.CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan);
            }
            else
            {
                dinhmuc.Function = DeclarationFunction.KHAI_BAO;
                dinhmuc.Acceptance = dmdk.NgayTiepNhan.ToString(sfmtDateTime);
                dinhmuc.CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan);
            }
            if (hopdong.TenDoanhNghiep == null || hopdong.TenDoanhNghiep.Trim() == "")
            {
                foreach (Agent item in dinhmuc.Agents)
                {
                    item.Name = tenDN;
                }
                dinhmuc.Importer.Name = tenDN;
            }
            dinhmuc.ProductionNorms = new List<ProductionNorm>();
            List<ProductionNorm> products = dinhmuc.ProductionNorms;
            Company.GC.BLL.GC.KDT_LenhSanXuat LenhSanXuat = new Company.GC.BLL.GC.KDT_LenhSanXuat();
            LenhSanXuat = Company.GC.BLL.GC.KDT_LenhSanXuat.Load(dmdk.LenhSanXuat_ID);
            foreach (Company.GC.BLL.GC.KDT_GC_DinhMucThucTe_SP item in dmdk.SPCollection)
            {
                ProductionNorm p = new ProductionNorm()
                {
                    Product = new Product()
                    {
                        Commodity = new Commodity()
                        {
                            Description = item.TenSanPham,
                            Identification = item.MaSanPham,
                            TariffClassification = item.MaHS,                            
                            ProductCtrlNo = LenhSanXuat.SoLenhSanXuat,
                        },
                        GoodsMeasure = new GoodsMeasure
                        {
                            MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim()
                        }
                    }
                };
                List<MaterialsNorm> MaterialsNorms = new List<MaterialsNorm>();
                foreach (Company.GC.BLL.GC.KDT_GC_DinhMucThucTe_DinhMuc items in item.DMCollection)
                {
                    MaterialsNorm entity = new MaterialsNorm() { Material = new Product() { Commodity = new Commodity(), GoodsMeasure = new GoodsMeasure() } };
                    entity.Material.Commodity.TariffClassification = items.MaHS;
                    entity.Material.GoodsMeasure.MeasureUnit = items.DVT_NPL;
                    entity.Material.Commodity.Identification = items.MaNPL;
                    entity.Material.Commodity.Description = items.TenNPL;
                    entity.Description = items.GhiChu;
                    entity.Norm = Helpers.FormatNumeric(items.DinhMucSuDung, 8);
                    entity.Loss = Helpers.FormatNumeric(items.TyLeHaoHut, 4);
                    entity.RateExchange = "1";
                    MaterialsNorms.Add(entity);
                }
                if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS)
                {
                    foreach (MaterialsNorm m in MaterialsNorms)
                    {
                        m.Material.GoodsMeasure.MeasureUnit = VNACCS_Mapper.GetCodeVNACC(m.Material.GoodsMeasure.MeasureUnit.Trim());
                    }
                }
                products.Add(p);
            }
            return dinhmuc;
        }

        public static GC_PhuKien ToDataTransferPhuKien(PhuKienDangKy pkdk, HopDong hopdong, string tenDN)
        {
            bool isKhaiSua = pkdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            GC_PhuKien phukien = new GC_PhuKien()
            {
                Issuer = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                Reference = pkdk.GUIDSTR,
                Issue = pkdk.NgayPhuKien.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                CustomsReference = isKhaiSua ? Helpers.FormatNumeric(pkdk.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiSua ? pkdk.NgayTiepNhan.ToString(
                ) : string.Empty,
                DeclarationOffice =GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(pkdk.MaHaiQuan.Trim()).Trim() : pkdk.MaHaiQuan.Trim(),
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Agents = new List<Agent>(),
                ContractReference = new ContractDocument()
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    DeclarationOffice = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hopdong.MaHaiQuan.Trim()).Trim() : hopdong.MaHaiQuan.Trim(),
                    Expire = hopdong.NgayGiaHan.Year == 1900 ? hopdong.NgayHetHan.ToString(sfmtDate) : hopdong.NgayGiaHan.ToString(sfmtDate),
                }

            };
            // Người Khai Hải Quan
            phukien.Agents.Add(new Agent
            {
                Name = hopdong.TenDoanhNghiep,
                Identity = hopdong.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            phukien.Agents.Add(new Agent
            {
                Name = hopdong.TenDoanhNghiep,
                Identity = hopdong.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            phukien.Importer = new NameBase()
            {
                Identity = hopdong.MaDoanhNghiep,
                Name = hopdong.TenDoanhNghiep
            };
            if (hopdong.TenDoanhNghiep == null || hopdong.TenDoanhNghiep.Trim() == "")
            {
                foreach (Agent item in phukien.Agents)
                {
                    item.Name = tenDN;
                }
                phukien.Importer.Name = tenDN;
            }
            phukien.SubContract = new Subcontract()
            {
                Reference = pkdk.SoPhuKien,
                Decription = pkdk.GhiChu,
                Issue = pkdk.NgayPhuKien.ToString(sfmtDate)
            };
            phukien.AdditionalInformations = new List<AdditionalInformation>();
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien loaiPK in pkdk.PKCollection)
            {
                if (loaiPK.HPKCollection == null || loaiPK.HPKCollection.Count == 0)
                    loaiPK.LoadCollection();
                AdditionalInformation additionalInformation = new AdditionalInformation()
                {
                    Statement = loaiPK.MaPhuKien.Trim()
                };
                switch (loaiPK.MaPhuKien.Trim())
                {
                    #region Hủy Hợp đồng
                    case "101":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    AdditionalInformation = new AdditionalInformation_GhiChu()
                                    {
                                        Content = pkdk.GhiChu
                                    }
                                }
                            };
                            break;
                        }
                    #endregion

                    #region Hủy đăng ký Sản phẩm
                    case "102":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Products = new List<Product>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Product p = new Product()
                                {
                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.Products.Add(p);
                            }
                            break;
                        }
                    #endregion

                    #region Hủy đăng ký Nguyên liệu
                    case "103":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Materials = new List<Product>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Product p = new Product()
                                {
                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.Materials.Add(p);
                            };
                            break;
                        }
                    #endregion

                    #region Hủy đăng ký Thiết bị
                    case "104":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Equipments = new List<Equipment>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Equipment eq = new Equipment()
                                {
                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.Equipments.Add(eq);
                            }
                            break;
                        }
                    #endregion

                    #region Hủy đăng ký Hàng mẫu
                    case "105":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    SampleProducts = new List<Equipment>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Equipment eq = new Equipment()
                                {
                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.SampleProducts.Add(eq);
                            }
                            break;
                        }
                    #endregion

                    #region Gia hạn Hợp đồng
                    case "201":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    OldExpire = DateTime.ParseExact(loaiPK.ThongTinCu, "dd/MM/yyyy", null).ToString(sfmtDate),
                                    NewExpire = DateTime.ParseExact(loaiPK.ThongTinMoi, "dd/MM/yyyy", null).ToString(sfmtDate)
                                }
                            };
                            break;
                        }
                    #endregion

                    #region Sửa thông tin chung Hợp đồng
                    case "501":
                        {
                            KDT_GC_PhuKien_TTHopDong TTHD = KDT_GC_PhuKien_TTHopDong.LoadBy(loaiPK.ID);
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Payment = new Payment { Method = TTHD.PTTT_ID.Trim() },
                                    CurrencyExchange = new CurrencyExchange
                                    {
                                        CurrencyType = TTHD.NguyenTe_ID
                                    },
                                    Importer = new NameBase
                                    {
                                        Name = TTHD.TenDoanhNghiep,
                                        Identity = TTHD.MaDoanhNghiep,
                                        Address = TTHD.DiaChiDoanhNghiep
                                    },
                                    Exporter = new NameBase
                                    {
                                        Name = TTHD.TenDonViDoiTac,
                                        Identity = TTHD.DonViDoiTac,
                                        Address = TTHD.DiaChiDoiTac
                                    },
                                    CustomsValue = new CustomsValue
                                    {
                                        TotalPaymentValue = Helpers.FormatNumeric(TTHD.TongTriGiaTienCong, GlobalsShare.TriGiaNT),
                                        TotalProductValue = Helpers.FormatNumeric(TTHD.TongTriGiaSP, GlobalsShare.TriGiaNT)
                                    },
                                    ImportationCountry = "VN",
                                    ExportationCountry = TTHD.NuocThue_ID.Trim(),
                                    AdditionalInformation = new AdditionalInformation_GhiChu
                                    {
                                        Content = TTHD.GhiChu
                                    }
                                }
                            };

                            break;
                        }
                    #endregion

                    #region Sửa Sản phẩm
                    case "502":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Products = new List<Product>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Product p = new Product()
                                {
                                    PreIdentification = hangPK.ThongTinCu,

                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS,
                                        ProductGroup = hangPK.NhomSP
                                    },
                                    GoodsMeasure = new GoodsMeasure()
                                    {
                                        MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangPK.DVT_ID.Trim()) : hangPK.DVT_ID.Substring(0, 3)
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.Products.Add(p);
                            }
                            break;
                        }
                    #endregion

                    #region Sửa Nguyên liệu
                    case "503":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Materials = new List<Product>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                                additionalInformation.ContentPK.Declaration.Materials.Add(new Product
                                {
                                    PreIdentification = hangPK.ThongTinCu,
                                    Commodity = new Commodity
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS,
                                        Origin = "1"
                                    },
                                    GoodsMeasure = new GoodsMeasure
                                    {
                                        //Quantity = Helpers.FormatNumeric(hangPK.SoLuong, 0),
                                        MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangPK.DVT_ID.Trim()) : hangPK.DVT_ID.Substring(0, 3)
                                    }
                                });
                            break;
                        }
                    #endregion

                    #region Sửa Thiết bị
                    case "504":
                        {

                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Equipments = new List<Equipment>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                                additionalInformation.ContentPK.Declaration.Equipments.Add(new Equipment
                                {
                                    PreIdentification = hangPK.ThongTinCu,
                                    Commodity = new Commodity
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS
                                    },
                                    GoodsMeasure = new GoodsMeasure
                                    {
                                        Quantity = Helpers.FormatNumeric(hangPK.SoLuong, 0),
                                        MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangPK.DVT_ID.Trim()) : hangPK.DVT_ID.Substring(0, 3)
                                    },
                                    CurrencyExchange = new CurrencyExchange
                                    {
                                        CurrencyType = hangPK.NguyenTe_ID.Trim()
                                    },
                                    Origin = new Origin
                                    {
                                        OriginCountry = hangPK.NuocXX_ID.Trim()
                                    },
                                    CustomsValue = new CustomsValue
                                    {
                                        unitPrice = Helpers.FormatNumeric(hangPK.DonGia, GlobalsShare.DonGiaNT)
                                    },
                                    Status = hangPK.TinhTrang,
                                });
                            break;
                        }
                    #endregion

                    #region Sửa Hàng mẫu
                    case "505":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    SampleProducts = new List<Equipment>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                                additionalInformation.ContentPK.Declaration.SampleProducts.Add(new Equipment
                            {
                                PreIdentification = hangPK.ThongTinCu,
                                Commodity = new Commodity
                                {
                                    Identification = hangPK.MaHang,
                                    Description = hangPK.TenHang,
                                    TariffClassification = hangPK.MaHS
                                },
                                GoodsMeasure = new GoodsMeasure
                                {
                                    Quantity = Helpers.FormatNumeric(hangPK.SoLuong, GlobalsShare.TriGiaNT),
                                    MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangPK.DVT_ID.Trim()) : hangPK.DVT_ID
                                }

                            });
                            break;
                        }
                    #endregion

                    #region Bổ sung Sản phẩm
                    case "802":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Products = new List<Product>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Product p = new Product()
                                {
                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS,
                                        ProductGroup = hangPK.NhomSP
                                    }
                                    ,
                                    GoodsMeasure = new GoodsMeasure()
                                    {
                                        MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangPK.DVT_ID.Trim()) : hangPK.DVT_ID
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.Products.Add(p);
                            }
                            break;
                        }
                    #endregion

                    #region Bổ sung Nguyên liệu
                    case "803":
                        {
                            additionalInformation.ContentPK = new Content
                            {
                                Declaration = new DeclarationPhuKien
                                {
                                    Materials = new List<Product>(),
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                                additionalInformation.ContentPK.Declaration.Materials.Add(new Product
                                {
                                    Commodity = new Commodity
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS,
                                        Origin = hangPK.TinhTrang,
                                    },
                                    GoodsMeasure = new GoodsMeasure
                                    {
                                        MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangPK.DVT_ID.Trim()) : hangPK.DVT_ID.Substring(0, 3)
                                    }
                                });


                            break;
                        }
                    #endregion

                    #region Bổ sung Thiết bị
                    case "804":
                        {
                            additionalInformation.ContentPK = new Content
                           {
                               Declaration = new DeclarationPhuKien
                               {
                                   Equipments = new List<Equipment>()
                               }
                           };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                                additionalInformation.ContentPK.Declaration.Equipments.Add(new Equipment
                                {
                                    Commodity = new Commodity
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS
                                    },
                                    GoodsMeasure = new GoodsMeasure
                                    {
                                        MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangPK.DVT_ID.Trim()) : hangPK.DVT_ID.Substring(0, 3),
                                        Quantity = Helpers.FormatNumeric(hangPK.SoLuong, GlobalsShare.TriGiaNT)
                                    },
                                    CurrencyExchange = new CurrencyExchange
                                    {
                                        CurrencyType = hangPK.NguyenTe_ID.Trim()
                                    },
                                    Origin = new Origin
                                    {
                                        OriginCountry = hangPK.NuocXX_ID.Trim()
                                    },
                                    CustomsValue = new CustomsValue
                                    {
                                        unitPrice = Helpers.FormatNumeric(hangPK.DonGia, GlobalsShare.DonGiaNT)
                                    },
                                    Status = hangPK.TinhTrang,
                                });


                            break;
                        }
                    #endregion

                    #region Bổ sung Hàng mẫu
                    case "805":
                        {
                            additionalInformation.ContentPK = new Content
                           {
                               Declaration = new DeclarationPhuKien
                               {
                                   SampleProducts = new List<Equipment>()
                               }
                           };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                additionalInformation.ContentPK.Declaration.SampleProducts.Add(new Equipment
                                {
                                    Commodity = new Commodity
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS
                                    },
                                    GoodsMeasure = new GoodsMeasure
                                    {
                                        Quantity = Helpers.FormatNumeric(hangPK.SoLuong, 0),
                                        MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangPK.DVT_ID.Trim()) : hangPK.DVT_ID.Substring(0, 3)
                                    }
                                });
                            }
                            break;
                        }
                    #endregion

                    default:
                        break;
                }
                phukien.AdditionalInformations.Add(additionalInformation);
            }
            return phukien;
        }

        public static GC_DNGiamSatTieuHuy ToDataTransferGSTieuHuy(GiamSatTieuHuy gsTieuHuy, HopDong hopdong)
        {
            bool isKhaiBaoSua = gsTieuHuy.ActionStatus == (int)ActionStatus.DN_GiamSatSua;
            GC_DNGiamSatTieuHuy dnGsTieuHuy = new GC_DNGiamSatTieuHuy()
            {
                Issuer = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                Function = isKhaiBaoSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                Reference = gsTieuHuy.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                DeclarationOffice = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(gsTieuHuy.MaHaiQuan.Trim()) : gsTieuHuy.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? Helpers.FormatNumeric(gsTieuHuy.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiBaoSua ? gsTieuHuy.NgayTiepNhan.ToString(sfmtDate) : string.Empty,
                Agents = new List<Agent>(),
                Importer = new NameBase() { Identity = gsTieuHuy.MaDoanhNghiep, Name = hopdong.TenDoanhNghiep },
                SubContructReference = new List<ContractDocument>(),
                //{
                //    Reference = hopdong.SoHopDong,
                //    Issue = hopdong.NgayKy.ToString(sfmtDate),
                //    CustomsReference = hopdong.SoHopDong,
                //},
                ContractReference = new ContractDocument()
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    DeclarationOffice = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hopdong.MaHaiQuan.Trim()) : hopdong.MaHaiQuan.Trim(),
                    Expire = hopdong.NgayHetHan.ToString(sfmtDate),
                    CustomsReference = hopdong.SoHopDong
                },
                License = new License()
                {
                    NumberLicense = gsTieuHuy.SoGiayPhep,
                    DateLicense = gsTieuHuy.NgayGiayPhep.ToString(sfmtDate),
                    ExpireDate = gsTieuHuy.NgayHetHan.ToString(sfmtDate),
                    AdminitrativeOrgan = gsTieuHuy.ToChucCap,
                },
                UserAttends = gsTieuHuy.CacBenThamGia,
                Time = gsTieuHuy.ThoiGianTieuHuy.ToString(sfmtDate),
                Location = gsTieuHuy.DiaDiemTieuHuy,
                //AdditionalInformations = new List<AdditionalInformation>(),

            };
            PhuKienDangKyCollection pkHD = new PhuKienDangKyCollection();
            pkHD = hopdong.GetPK();
            foreach (PhuKienDangKy item in pkHD)
            {
                dnGsTieuHuy.SubContructReference.Add(new ContractDocument
                {
                    Reference = item.SoPhuKien,
                    Issue = item.NgayPhuKien.ToString(sfmtDate),
                    CustomsReference = item.SoTiepNhan.ToString(),
                });

            }

            dnGsTieuHuy.AdditionalInformations.Add(new AdditionalInformation() { Content = new Content() { Text = gsTieuHuy.GhiChuKhac } });
            dnGsTieuHuy.Agents.Add(new Agent
             {
                 Name = hopdong.TenDoanhNghiep,
                 Identity = hopdong.MaDoanhNghiep,
                 Status = AgentsStatus.NGUOIKHAI_HAIQUAN
             });
            dnGsTieuHuy.Agents.Add(new Agent
                                 {
                                     Name = hopdong.TenDoanhNghiep,
                                     Identity = hopdong.MaDoanhNghiep,
                                     Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
                                 });
            if (gsTieuHuy.HangGSTieuHuys != null && gsTieuHuy.HangGSTieuHuys.Count > 0)
            {
                dnGsTieuHuy.Scraps = new List<Scrap>();
                foreach (HangGSTieuHuy item in gsTieuHuy.HangGSTieuHuys)
                {
                    Scrap scrap = new Scrap()
                    {
                        sequence = item.STTHang.ToString(),
                        Commodity = new Commodity
                        {
                            Identification = item.MaHang,
                            Type = Helpers.FormatNumeric(item.LoaiHang),
                            TariffClassification = item.MaHS,
                            Description = item.TenHang,
                        },
                        GoodsMeasure = new SXXK_GoodsMeasure
                        {
                            Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                            //ConversionRate = "1",
                            //MeasureUnits = new List<string>(),
                            RegisteredMeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim()
                        }
                    };
                    //scrap.GoodsMeasure.MeasureUnits.Add(GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim());
                    //scrap.GoodsMeasure.MeasureUnits.Add(item.DVT_ID.Trim());
                    dnGsTieuHuy.Scraps.Add(scrap);
                }

            }
            return dnGsTieuHuy;
        }
        public static GC_ThanhKhoan ToDataTransferThanhKhoan(ThanhKhoan ThanhKhoan, HopDong hopdong)
        {
            bool isKhaiBaoSua = ThanhKhoan.ActionStatus == (int)ActionStatus.ThanhKhoanSua;
            GC_ThanhKhoan ThanhKhoanHD = new GC_ThanhKhoan()
            {
                Issuer = DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD,
                Function = isKhaiBaoSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                Reference = ThanhKhoan.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                DeclarationOffice = ThanhKhoan.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? Helpers.FormatNumeric(ThanhKhoan.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiBaoSua ? ThanhKhoan.NgayTiepNhan.ToString(sfmtDate) : string.Empty,
                Agents = new List<Agent>(),
                Importer = new NameBase() { Identity = ThanhKhoan.MaDoanhNghiep, Name = hopdong.TenDoanhNghiep },
                ContractReference = new ContractDocument()
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    DeclarationOffice = hopdong.MaHaiQuan.Trim(),
                    Expire = hopdong.NgayHetHan.ToString(sfmtDate),
                    CustomsReference = hopdong.SoHopDong
                },
                AdditionalInformations = new List<AdditionalInformation>(),
                // GoodsItems = new List<Product>(),
            };
            //ThanhKhoanHD.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = ThanhKhoan.GhiChu } });
            //ThanhKhoanHD.Agents.Add(new Agent
            //{
            //    Name = hopdong.TenDoanhNghiep,
            //    Identity = hopdong.MaDoanhNghiep,
            //    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            //});
            //ThanhKhoanHD.Agents.Add(new Agent
            //{
            //    Name = hopdong.TenDoanhNghiep,
            //    Identity = hopdong.MaDoanhNghiep,
            //    Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            //});
            //foreach (HangThanhKhoan item in ThanhKhoan.HangCollection)
            //    ThanhKhoanHD.GoodsItems.Add(new Product
            //    {
            //        Commodity = new Commodity
            //        {
            //            Description = item.TenHang,
            //            Identification = item.MaHang,
            //            TariffClassification = item.MaHS,
            //            Type = item.LoaiHang.ToString(),
            //        },
            //        GoodsMeasure = new GoodsMeasure
            //        {
            //            Quantity = Helpers.FormatNumeric(item.SoLuong, 3),
            //            MeasureUnit = item.DVT_ID,
            //        }
            //    });

            return ThanhKhoanHD;
        }

        #region TransferOb tờ khai
        public static ToKhai ToDataTransferObject(ToKhaiMauDich tkmd)
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");
            bool isToKhaiSua = tkmd.SoToKhai != 0 && tkmd.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (isToKhaiSua)
                tkmd.SoTiepNhan = KhaiBaoSua.SoTNSua(tkmd.SoTiepNhan, tkmd.SoToKhai, tkmd.NamDK, tkmd.MaLoaiHinh,
                        tkmd.MaHaiQuan.Trim(), tkmd.MaDoanhNghiep, LoaiKhaiBao.ToKhai);
            #region Header
            ToKhai tokhai = new ToKhai()
            {
                Issuer = isToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_NHAP : DeclarationIssuer.GC_TOKHAI_XUAT,
                Reference = tkmd.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                // Số tiếp nhận
                CustomsReference = isToKhaiSua ? Helpers.FormatNumeric(tkmd.SoTiepNhan) : string.Empty,
                //Ngày dang ký chứng thư
                Acceptance = isToKhaiSua ? tkmd.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                // Ðon vị hải quan khai báo
                DeclarationOffice = tkmd.MaHaiQuan.Trim(),
                // Số hàng
                GoodsItem = Helpers.FormatNumeric(tkmd.SoHang),
                LoadingList = Helpers.FormatNumeric(tkmd.SoLuongPLTK),

                // Khối luợng và khối luợng tịnh
                TotalGrossMass = Helpers.FormatNumeric(tkmd.TrongLuong, 3),
                TotalNetGrossMass = Helpers.FormatNumeric(tkmd.TrongLuongNet, 3),
                // Mã Loại Hình
                NatureOfTransaction = tkmd.MaLoaiHinh,
                // Phuong thức thanh toán
                PaymentMethod = tkmd.PTTT_ID,



                #region Install element
                Agents = new List<Agent>(),

                //Nguyên tệ
                CurrencyExchange = new CurrencyExchange { CurrencyType = tkmd.NguyenTe_ID, Rate = Helpers.FormatNumeric(tkmd.TyGiaTinhThue, 3) },

                //Số kiện
                DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tkmd.SoKien) },
                //Neu có day du 1 trong 3 chứng từ :  Giấy phép, Hợp đồng, Vận đơn
                /* AdditionalDocuments = new List<AdditionalDocument>(),*/

                // Hóa Ðon thuong mại
                Invoice = new Invoice
                {
                    Issue = tkmd.NgayHoaDonThuongMai.Year <= 1900 ? string.Empty : tkmd.NgayHoaDonThuongMai.ToString(sfmtDate),
                    Reference = tkmd.SoHoaDonThuongMai,
                    Type = AdditionalDocumentType.HOA_DON_THUONG_MAI
                },


                // Doanh Nghiệp Xuất khẩu
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = "." } :
                new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() },

                //Doanh nghiệp nhập khẩu
                Importer = !isToKhaiNhap ? new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() } :
                new NameBase { Name = tkmd.TenDonViDoiTac, Identity = tkmd.MaDoanhNghiep.Trim() },


                // Người đại diện doanh nghiệp | Tên chủ hàng
                RepresentativePerson = new RepresentativePerson { Name = tkmd.TenChuHang, ContactFunction = tkmd.ChucVu },

                // Đề xuất khác | Mã loại thông tin - Ghi chú khác
                AdditionalInformations = new List<AdditionalInformation>(),

                // GoodsShipmet Thông tin hàng hóa
                GoodsShipment = new GoodsShipment(),

                /*  License = new List<License>(),
                  ContractDocument = new List<ContractDocument>(),
                  CommercialInvoices = new List<CommercialInvoice>(),
                  CertificateOfOrigins = new List<CertificateOfOrigin>(),
                  CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
                  AttachDocumentItem = new List<AttachDocumentItem>(),
                  AdditionalDocumentEx = new List<AdditionalDocument>(),
                 */
                #endregion Nrr

            };
            tokhai.AdditionalInformations.Add(new AdditionalInformation
            {
                Statement = "001",
                Content = new Content() { Text = tkmd.DeXuatKhac }
            });
            if (isToKhaiSua)
                tokhai.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Statement = "005",
                    Content = new Content() { Text = tkmd.LyDoSua }
                });
            #endregion Header

            #region Agents Đại lý khai
            tokhai.Agents = AgentsFrom(tkmd);
            #endregion

            #region AdditionalDocument Hợp đồng - Giấy phép - Vận đơn ngoài tờ khai
            bool isDocument = !string.IsNullOrEmpty(tkmd.SoHopDong) ||
                !string.IsNullOrEmpty(tkmd.SoVanDon) || !string.IsNullOrEmpty(tkmd.SoGiayPhep);
            if (isDocument) tokhai.AdditionalDocuments = new List<AdditionalDocument>();
            // Add AdditionalDocument (thêm hợp đồng)
            if (!string.IsNullOrEmpty(tkmd.SoHopDong))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayHopDong.Year > 1900 ? tkmd.NgayHopDong.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoHopDong,
                    Type = AdditionalDocumentType.HOP_DONG,
                    Name = "Hop dong",
                    Expire = tkmd.NgayHetHanHopDong.Year > 1900 ? tkmd.NgayHetHanHopDong.ToString(sfmtDate) : string.Empty
                });
            // Thêm vận đơn
            if (isToKhaiNhap && !string.IsNullOrEmpty(tkmd.SoVanDon))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayVanDon.Year > 1900 ? tkmd.NgayVanDon.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoVanDon,
                    Type = AdditionalDocumentType.BILL_OF_LADING_ORIGIN,
                    Name = "Bill of lading"
                });
            // Thêm giấy phép
            if (!string.IsNullOrEmpty(tkmd.SoGiayPhep))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayGiayPhep.Year > 1900 ? tkmd.NgayGiayPhep.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoGiayPhep,
                    Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                    Name = "Giay phep",
                    Expire = tkmd.NgayHetHanGiayPhep.Year > 1900 ? tkmd.NgayHetHanGiayPhep.ToString(sfmtDate) : string.Empty
                });

            #endregion

            #region GoodsShipment thông tin về hàng hóa
            tokhai.GoodsShipment = new GoodsShipment()
            {
                ImportationCountry = isToKhaiNhap ? null : tkmd.NuocNK_ID.Substring(0, 2),
                ExportationCountry = isToKhaiNhap ? tkmd.NuocXK_ID.Substring(0, 2) : null,
                Consignor = new NameBase { Identity = string.Empty, Name = string.Empty },
                Consignee = new NameBase { Identity = string.Empty, Name = string.Empty },
                NotifyParty = new NameBase { Identity = string.Empty, Name = string.Empty },
                DeliveryDestination = new DeliveryDestination { Line = string.Empty },
                EntryCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? tkmd.CuaKhau_ID : string.Empty, Name = isToKhaiNhap ? DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) : string.Empty },
                ExitCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? string.Empty : tkmd.CuaKhau_ID, Name = isToKhaiNhap ? string.Empty : DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) },
                Importer = isToKhaiNhap ? null : new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty },
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty } : null,
                TradeTerm = new TradeTerm { Condition = tkmd.DKGH_ID },
                //CustomsGoodsItems = new List<CustomsGoodsItem>()
            };
            #endregion GoodsShipment

            #region CustomGoodsItem Danh sách hàng khai báo

            //Add CustomGoodsItem
            int soluonghang = 0;
            if (tkmd.HMDCollection != null)
            {
                soluonghang = tkmd.HMDCollection.Count;
                tokhai.GoodsShipment.CustomsGoodsItems = new List<CustomsGoodsItem>();
            }
            for (int i = 0; i < soluonghang; i++)
            {
                HangMauDich hmd = tkmd.HMDCollection[i];
                #region CustomsGoodsItem
                CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem
                {

                    CustomsValue = Helpers.FormatNumeric(hmd.TriGiaKB, GlobalsShare.TriGiaNT),
                    Sequence = Helpers.FormatNumeric(hmd.SoThuTuHang),
                    StatisticalValue = Helpers.FormatNumeric(hmd.TriGiaTT, GlobalsShare.TriGiaNT),
                    UnitPrice = Helpers.FormatNumeric(hmd.DonGiaKB, GlobalsShare.DonGiaNT),
                    StatisticalUnitPrice = Helpers.FormatNumeric(hmd.DonGiaTT, GlobalsShare.DonGiaNT),
                    Manufacturer = new NameBase { Name = hmd.TenHangSX/*Tên hãng sx*/, Identity = hmd.MaHangSX/*Mã hãng sx*/ },
                    Origin = new Origin { OriginCountry = hmd.NuocXX_ID.Substring(0, 2) },
                    GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hmd.SoLuong, GlobalsShare.TriGiaNT), MeasureUnit = hmd.DVT_ID }

                };

                //Trị giá trên mỗi dòng hàng (Thường thì chia đều cho tất cả dòng hàng) nếu có 1 dòng hàng thì tính tổng
                //Nếu có nhiều hơn 1 dòng hàng thì chia đều ra cho mỗi dòng hàng
                // ExitToEntryCharge Tổng chi phí bằng (tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem)/số kiện hàng
                //FreightCharge chi phí vận tải cũng chia đôi ra
                //Metho = số thứ tự kiện hàng.  
                decimal dTongPhi = tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem;
                decimal dPhiVanChuyen = tkmd.PhiVanChuyen;
                customsGoodsItem.CustomsValuation = new CustomsValuation
                {
                    ExitToEntryCharge = Helpers.FormatNumeric(dTongPhi / soluonghang, 4),
                    FreightCharge = Helpers.FormatNumeric(dPhiVanChuyen / soluonghang, 4),
                    Method = string.Empty,
                    OtherChargeDeduction = "0"
                };

                #endregion

                #region Commodity Hàng trong tờ khai
                Commodity commodity = new Commodity
                {
                    Description = hmd.TenHang,
                    Identification = hmd.MaPhu,
                    TariffClassification = hmd.MaHS,
                    TariffClassificationExtension = hmd.MaHSMoRong,
                    Brand = hmd.NhanHieu,
                    Grade = hmd.QuyCachPhamChat,
                    Ingredients = hmd.ThanhPhan,
                    ModelNumber = hmd.Model,
                    DutyTaxFee = new List<DutyTaxFee>(),
                    //Type = isToKhaiNhap ? "1" : "2",
                    //Hệ thống hải quan chưa sử dụng
                    InvoiceLine = new InvoiceLine { ItemCharge = string.Empty, Line = string.Empty }
                };
                if (tkmd.LoaiHangHoa == "N")
                    commodity.Type = "1";
                else if (tkmd.LoaiHangHoa == "S")
                    commodity.Type = "2";
                else if (tkmd.LoaiHangHoa == "T")
                    commodity.Type = "3";
                else if (tkmd.LoaiHangHoa == "H")
                    commodity.Type = "4";

                #region DutyTaxFee tiền thuế của hàng
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 2),
                    DutyRegime = string.Empty,
                    SpecificTaxBase = string.Empty,
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatXNK, 1),
                    Type = DutyTaxFeeType.THUE_XNK
                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueGTGT, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatGTGT, 1),
                    Type = DutyTaxFeeType.THUE_VAT,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueTTDB, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatTTDB, 1),
                    Type = DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatXNK, 1),
                    Type = DutyTaxFeeType.THUE_KHAC,


                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].PhuThu, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].TyLeThuKhac, 1),
                    Type = DutyTaxFeeType.THUE_CHENH_LECH_GIA,

                });
                #endregion DutyTaxFee thuế

                customsGoodsItem.Commodity = commodity;
                #endregion Hàng chính

                #region AdditionalDocument if Search(Giấy phép có hàng trong tờ khai)
                //Lấy thông tin giấy phép đầu tiên
                if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = tkmd.GiayPhepCollection[0];
                    customsGoodsItem.AdditionalDocument = new AdditionalDocument()
                    {
                        Issue = giayphep.NgayGiayPhep.ToString(sfmtDate), /*Ngày cấp giấy phép yyyy-MM-dd*/
                        Issuer = giayphep.NguoiCap/*Người cấp*/,
                        IssueLocation = giayphep.NoiCap/*Nơi cấp*/,
                        Reference = giayphep.SoGiayPhep/*Số giấy phép*/,
                        Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                        Name = "GP"/* Tên giấy phép*/,
                        Expire = giayphep.NgayHetHan.ToString(sfmtDate)
                    };
                }
                #endregion

                #region CertificateOfOrigin if Search(Co có hàng trong tờ khai)

                if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.CO co = tkmd.COCollection[tkmd.COCollection.Count - 1];
                    customsGoodsItem.CertificateOfOrigin = new CertificateOfOrigin()
                    {
                        Issue = co.NgayCO.ToString(sfmtDate),
                        Issuer = co.ToChucCap,
                        IssueLocation = co.NuocCapCO,
                        Reference = co.SoCO,
                        Type = co.LoaiCO,
                        Name = "CO",
                        Expire = co.NgayHetHan.ToString(sfmtDate),
                        Exporter = co.TenDiaChiNguoiXK,
                        Importer = co.TenDiaChiNguoiNK,
                        ExportationCountry = co.MaNuocXKTrenCO,
                        ImportationCountry = co.MaNuocNKTrenCO,
                        Content = co.ThongTinMoTaChiTiet,
                        IsDebt = Helpers.FormatNumeric(co.NoCo, 0),
                        Submit = co.ThoiHanNop.ToString(sfmtDate)
                    };
                }
                #endregion
                #region AdditionalInformations - ValuationAdjustments Thêm hàng từ tờ khai trị giá 1,2,3
                if (isToKhaiNhap)
                {
                    //Thông tin tờ khai trị giá.
                    #region Tờ khai trị giá PP1
                    if (tkmd.TKTGCollection != null)
                        if (tkmd.TKTGCollection.Count > 0)
                        {
                            ToKhaiTriGia tktg = tkmd.TKTGCollection[0];
                            foreach (HangTriGia hangtrigia in tktg.HTGCollection)
                                if (hangtrigia.TenHang.Trim() == hmd.TenHang.Trim())
                                {
                                    customsGoodsItem.AdditionalInformations = new List<AdditionalInformation>();
                                    customsGoodsItem.ValuationAdjustments = new List<ValuationAdjustment>();
                                    /* Fill dữ liệu hàng  trong tờ khai trị giá PP1*/
                                    //Nếu có tờ khai trị giá thì method = 1 
                                    customsGoodsItem.CustomsValuation.Method = "1";
                                    #region AdditionalInformations Nội dung tờ khai trị giá
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TO_SO",
                                        Statement = AdditionalInformationStatement.TO_SO,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.ToSo) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "NGAY_XK",
                                        Statement = AdditionalInformationStatement.NGAY_XK,
                                        Content = new Content() { Text = tktg.NgayXuatKhau.ToString(sfmtDate) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "QUYEN_SD",
                                        Statement = AdditionalInformationStatement.QUYEN_SD,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.QuyenSuDung) }
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "KHONG_XD",
                                        Statement = AdditionalInformationStatement.KHONG_XD,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.KhongXacDinh) }
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TRA_THEM",
                                        Statement = AdditionalInformationStatement.TRA_THEM,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.TraThem) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TIEN_TRA_16",
                                        Statement = AdditionalInformationStatement.TIEN_TRA_16,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.TienTra) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "CO_QHDB",
                                        Statement = AdditionalInformationStatement.CO_QHDB,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.CoQuanHeDacBiet) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "KIEU_QHDB",
                                        Statement = AdditionalInformationStatement.KIEU_QHDB,
                                        Content = new Content() { Text = tktg.KieuQuanHe }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "ANH_HUONG_QH",
                                        Statement = AdditionalInformationStatement.ANH_HUONG_QH,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.AnhHuongQuanHe) }
                                    });
                                    #endregion Nội dung tờ khai trị giá

                                    #region ValuationAdjustments Chi tiết hàng tờ khai trị giá pp1
                                    if (tktg.HTGCollection != null && tktg.HTGCollection.Count > 0)
                                    {
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Gia_hoa_don,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.GiaTrenHoaDon, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Thanh_toan_gian_tiep,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.KhoanThanhToanGianTiep, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_tra_truoc,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TraTruoc, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_hoa_hong,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.HoaHong, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_bao_bi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiBaoBi, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_dong_goi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiDongGoi, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Khoan_tro_giup,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TroGiup, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_NVL,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.NguyenLieu, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_NL,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.VatLieu, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_cong_cu,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.CongCu, GlobalsShare.TriGiaNT)
                                        });
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_thiet_ke,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ThietKe, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_ban_quyen,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.BanQuyen, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_phai_tra,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienTraSuDung, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_van_tai,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiVanChuyen, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_bao_hiem,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.PhiBaoHiem, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_VT_BH_noi_dia,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiNoiDia, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_phat_sinh,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiPhatSinh, GlobalsShare.TriGiaNT)
                                        });


                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_lai,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienLai, GlobalsShare.TriGiaNT)
                                        });


                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Thue_phi_le_phi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienThue, GlobalsShare.TriGiaNT)
                                        });
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Khoan_Giam_Gia,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.GiamGia, GlobalsShare.TriGiaNT)
                                        });

                                    }
                                }


                                    #endregion Chi tiết hàng tờ khai trị giá

                        }
                    #endregion Tờ khai trị giá PP1

                    if (tkmd.TKTGPP23Collection != null)
                        if (tkmd.TKTGPP23Collection.Count > 0)
                        {
                            foreach (ToKhaiTriGiaPP23 tkpgP23 in tkmd.TKTGPP23Collection)
                            {
                                if (tkpgP23.TenHang.ToUpper().Trim() == hmd.TenHang.ToUpper().Trim())
                                {
                                    customsGoodsItem.AdditionalInformations = new List<AdditionalInformation>();
                                    customsGoodsItem.ValuationAdjustments = new List<ValuationAdjustment>();
                                    string maTktg = tkpgP23.MaToKhaiTriGia.ToString();
                                    customsGoodsItem.CustomsValuation.Method = maTktg;
                                    #region AdditionalInformations Nội dung tờ khai trị giá PP23

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.LyDo },
                                        Statement = maTktg + AdditionalInformationStatement.LYDO_KAD_PP1,
                                        StatementDescription = "LYDO_KAD_PP1"

                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayXuat.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_XK23,
                                        StatementDescription = "NGAY_XK"

                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = Helpers.FormatNumeric(tkpgP23.STTHangTT) },
                                        Statement = maTktg + AdditionalInformationStatement.STTHANG_TT,
                                        StatementDescription = "STTHANG_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = Helpers.FormatNumeric(tkpgP23.SoTKHangTT) },
                                        Statement = maTktg + AdditionalInformationStatement.SOTK_TT,
                                        StatementDescription = "SOTK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayDangKyHangTT.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_NK_TT,
                                        StatementDescription = "NGAY_NK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.MaHaiQuanHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.MA_HQ_TT,
                                        StatementDescription = "MA_HQ_TT"
                                    });


                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayXuatTT.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_XK_TT,
                                        StatementDescription = "NGAY_XK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.GiaiTrinh },
                                        Statement = maTktg + AdditionalInformationStatement.GIAI_TRINH,
                                        StatementDescription = "GIAI_TRINH"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.MaLoaiHinhHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.Ma_LH,
                                        StatementDescription = "Ma_LH"
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.TenHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.HANG_TUONG_TU,
                                        StatementDescription = "Ma_LH"
                                    });
                                    #endregion Nội dung tờ khai trị giá PP23

                                    #region ValuationAdjustments Chi tiết nội dung trong tờ khai tri giá PP23

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.Tri_gia_hang_TT,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.TriGiaNguyenTeHangTT, GlobalsShare.TriGiaNT)
                                    });

                                    // Cộng ghi số dương trừ ghi số âm(+/-)
                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_cap_do_TM,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongThuongMai != 0 ? tkpgP23.DieuChinhCongThuongMai : -tkpgP23.DieuChinhTruCapDoThuongMai, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_so_luong,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongSoLuong != 0 ? tkpgP23.DieuChinhCongSoLuong : -tkpgP23.DieuChinhTruSoLuong, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_khoan_khac,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongKhoanGiamGiaKhac != 0 ? tkpgP23.DieuChinhCongKhoanGiamGiaKhac : -tkpgP23.DieuChinhTruKhoanGiamGiaKhac, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_phi_van_tai,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongChiPhiVanTai != 0 ? tkpgP23.DieuChinhCongChiPhiVanTai : -tkpgP23.DieuChinhTruChiPhiVanTai, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_phi_bao_hiem,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongChiPhiBaoHiem != 0 ? tkpgP23.DieuChinhCongChiPhiBaoHiem : -tkpgP23.DieuChinhTruChiPhiBaoHiem, GlobalsShare.TriGiaNT)
                                    });

                                    #endregion Chi tiết nội dung trong tờ khai tri giá PP23

                                    /* Fill dữ liệu hàng trong tờ khai trị giá PP23*/

                                }
                            }
                        }


                }
                else
                {
                    /*
                    customsGoodsItem.SpecializedManagement = new SpecializedManagement()
                    {
                        GrossMass = "",
                        Identification = "",
                        MeasureUnit = "",
                        Quantity = "",
                        Type = "",
                        UnitPrice = ""
                    };*/
                }
                #endregion thêm hàng từ tờ khai trị giá 1,2,3
                tokhai.GoodsShipment.CustomsGoodsItems.Add(customsGoodsItem);
            }
            #endregion CustomGoodsItem Danh sách hàng khai báo

            #region Supply Nguyên phụ liệu cung ứng cho tờ khai GC XK
            if (!isToKhaiNhap && tkmd.NPLCungUngs != null && tkmd.NPLCungUngs.Count > 0)
            {

                tokhai.GoodsShipment.Supplys = new List<Supply>();
                foreach (NPLCungUng nplCungUng in tkmd.NPLCungUngs)
                {
                    Supply supply = new Supply()
                    {
                        Material = new Product()
                        {
                            Commodity = new Commodity()
                            {
                                Identification = nplCungUng.MaHang,
                                Description = nplCungUng.TenHang,
                                TariffClassification = nplCungUng.MaHS
                            }
                        }
                    };
                    if (nplCungUng.NPLCungUngDetails != null && nplCungUng.NPLCungUngDetails.Count > 0)
                    {
                        supply.SupplyItems = new List<SupplyItem>();
                        foreach (NPLCungUngDetail nplCungUngDetail in nplCungUng.NPLCungUngDetails)
                        {
                            SupplyItem supplyItem = new SupplyItem()
                            {
                                Invoice = new Invoice()
                                {
                                    Type = Helpers.FormatNumeric(nplCungUngDetail.HinhThuc),
                                    Reference = nplCungUngDetail.SoChungTu,
                                    Issue = nplCungUngDetail.NgayChungTu.ToString(sfmtDate),
                                    Issuer = nplCungUngDetail.NoiPhatHanh,
                                    NatureOfTransaction = nplCungUngDetail.MaLoaiHinh.Trim(),
                                    DeclarationOffice = nplCungUngDetail.MaHaiQuan.Trim()
                                },
                                InvoiceLine = new InvoiceLine()
                                {
                                    Line = Helpers.FormatNumeric(nplCungUngDetail.DongHangTrenCT)
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    Quantity = Helpers.FormatNumeric(nplCungUngDetail.SoLuong, 3),
                                    MeasureUnit = nplCungUngDetail.DVT_ID.Trim(),
                                    ConversionRate = Helpers.FormatNumeric(nplCungUngDetail.TyLeQuyDoi, 3)
                                }
                            };
                            supply.SupplyItems.Add(supplyItem);
                        }
                    }
                    tokhai.GoodsShipment.Supplys.Add(supply);
                }


            }
            #endregion

            #region Danh sách các chứng từ đính kèm

            if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
            #region License Giấy phép
            {
                tokhai.License = new List<License>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep in tkmd.GiayPhepCollection)
                {
                    License lic = LicenseFrom(giayPhep);
                    tokhai.License.Add(lic);
                }
            }
            #endregion Giấy phép

            if (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0)
            #region ContractDocument  Hợp đồng thương mại
            {
                tokhai.ContractDocument = new List<ContractDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai in tkmd.HopDongThuongMaiCollection)
                {
                    ContractDocument contractDocument = ContractFrom(hdThuongMai);
                    tokhai.ContractDocument.Add(contractDocument);
                }
            }
            #endregion Hợp đồng thương mại

            if (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0)
            #region CommercialInvoice Hóa đơn thương mại
            {
                tokhai.CommercialInvoices = new List<CommercialInvoice>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai in tkmd.HoaDonThuongMaiCollection)
                {
                    CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoaDonThuongMai);
                    tokhai.CommercialInvoices.Add(commercialInvoice);
                }
            }
            #endregion Hóa đơn thương mại

            if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
            #region CertificateOfOrigin Thêm CO
            {
                tokhai.CertificateOfOrigins = new List<CertificateOfOrigin>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.CO co in tkmd.COCollection)
                {
                    tokhai.CertificateOfOrigins.Add(CertificateOfOriginFrom(co, tkmd, isToKhaiNhap));
                }
            }
            #endregion CO


            if (tkmd.VanTaiDon != null)
            #region BillOfLadings Vận đơn
            {
                tokhai.BillOfLadings = new List<BillOfLading>();
                Company.KDT.SHARE.QuanLyChungTu.VanDon vandon = tkmd.VanTaiDon;
                BillOfLading billOfLading = new BillOfLading()
                {
                    Reference = vandon.SoVanDon,
                    Issue = vandon.NgayVanDon.Year > 1900 ? vandon.NgayVanDon.ToString(sfmtDate) : string.Empty,
                    IssueLocation = vandon.QuocTichPTVT
                };
                billOfLading.BorderTransportMeans = new BorderTransportMeans()
                {
                    Identity = vandon.SoHieuPTVT,
                    Identification = vandon.TenPTVT,
                    Journey = vandon.SoHieuChuyenDi,
                    ModeAndType = tkmd.PTVT_ID,
                    Departure = vandon.NgayKhoiHanh.Year > 1900 ? vandon.NgayKhoiHanh.ToString(sfmtDate) : string.Empty,
                    RegistrationNationality = string.IsNullOrEmpty(vandon.QuocTichPTVT) ? string.Empty : vandon.QuocTichPTVT.Substring(0, 2)
                };
                billOfLading.Carrier = new NameBase()
                {
                    Name = vandon.TenHangVT,
                    Identity = vandon.MaHangVT
                };

                billOfLading.Consignment = new Consignment()
                {
                    Consignor = new NameBase()
                    {
                        Name = vandon.TenNguoiGiaoHang,
                        Identity = vandon.MaNguoiGiaoHang
                    },
                    Consignee = new NameBase()
                    {
                        Name = vandon.TenNguoiNhanHang,
                        Identity = vandon.MaNguoiNhanHang
                    },
                    NotifyParty = new NameBase()
                    {
                        Name = vandon.TenNguoiNhanHangTrungGian,
                        Identity = vandon.MaNguoiNhanHangTrungGian
                    },
                    LoadingLocation = new LoadingLocation()
                    {
                        Name = vandon.TenCangXepHang,
                        Code = vandon.MaCangXepHang,
                        Loading = vandon.NgayKhoiHanh.Year > 1900 ? vandon.NgayKhoiHanh.ToString(sfmtDate) : string.Empty
                    },
                    UnloadingLocation = new UnloadingLocation()
                    {
                        Name = vandon.TenCangDoHang,
                        Code = vandon.MaCangDoHang,
                        Arrival = vandon.NgayDenPTVT.Year > 1900 ? vandon.NgayDenPTVT.ToString(sfmtDate) : string.Empty
                    },
                    DeliveryDestination = new DeliveryDestination() { Line = vandon.DiaDiemGiaoHang },


                    ConsignmentItemPackaging = new Packaging()
                    {
                        Quantity = Helpers.FormatNumeric(vandon.TongSoKien),
                        Type = vandon.LoaiKien,
                        MarkNumber = string.Empty
                    }
                };
                #region Đổ dữ liệu bên ngoài GoodsShipment
                tokhai.GoodsShipment.Consignor = billOfLading.Consignment.Consignor;
                tokhai.GoodsShipment.Consignee = billOfLading.Consignment.Consignee;
                tokhai.GoodsShipment.NotifyParty = billOfLading.Consignment.NotifyParty;
                tokhai.GoodsShipment.DeliveryDestination = billOfLading.Consignment.DeliveryDestination;
                tokhai.GoodsShipment.ExitCustomsOffice.Name = vandon.CuaKhauXuat;
                #endregion
                if (vandon.ContainerCollection != null && vandon.ContainerCollection.Count > 0)
                    billOfLading.Consignment.TransportEquipments = new List<TransportEquipment>();

                foreach (Company.KDT.SHARE.QuanLyChungTu.Container container in vandon.ContainerCollection)
                {

                    TransportEquipment transportEquipment = new TransportEquipment()
                    {
                        Characteristic = container.LoaiContainer,
                        EquipmentIdentifications = new EquipmentIdentification()
                        {
                            identification = container.SoHieu
                        },
                        Fullness = Helpers.FormatNumeric(container.Trang_thai, 0),
                        Seal = container.Seal_No
                    };

                    billOfLading.Consignment.TransportEquipments.Add(transportEquipment);
                }

                #region Hàng và Container trong vận đơn
                if (vandon.ListHangOfVanDon != null && vandon.ListHangOfVanDon.Count > 0)
                {
                    List<ConsignmentItem> hangsVanDon = new List<ConsignmentItem>();

                    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail hangVanDon in vandon.ListHangOfVanDon)
                    {
                        HangMauDich hangToKhai = tkmd.HMDCollection.Find(hang => hang.ID == hangVanDon.HMD_ID);
                        if (hangToKhai != null)
                            hangsVanDon.Add(new ConsignmentItem()
                            {
                                Sequence = Helpers.FormatNumeric(hangVanDon.SoThuTuHang),
                                ConsignmentItemPackaging = new Packaging()
                                {
                                    Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong),
                                    Type = hangVanDon.LoaiKien,
                                    MarkNumber = hangVanDon.SoHieuKien,
                                },
                                Commodity = new Commodity()
                                {
                                    Description = hangVanDon.TenHang,
                                    Identification = string.Empty,//Mã hàng
                                    TariffClassificationExtension = hangVanDon.MaHS.Trim()
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    GrossMass = Helpers.FormatNumeric(hangVanDon.TrongLuong, 3),
                                    MeasureUnit = hangVanDon.DVT_ID.Substring(0, 3)
                                },
                                EquipmentIdentification = new EquipmentIdentification()
                                {
                                    identification = hangVanDon.SoHieuContainer
                                }

                            });

                    }
                    billOfLading.Consignment.ConsignmentItems = hangsVanDon;
                }
                #endregion Hàng và Container trong vận đơn

                tokhai.BillOfLadings.Add(billOfLading);

            }
            #region Tự động thêm vận đơn rỗng cho tờ khai xuất
            else if (tkmd.VanTaiDon != null && tkmd.VanTaiDon.SoVanDon.Trim() == "." && tkmd.MaLoaiHinh.Substring(0, 1).Trim().ToUpper() == "X")
            {
                tokhai.BillOfLadings = new List<BillOfLading>();
                Company.KDT.SHARE.QuanLyChungTu.VanDon vandon = tkmd.VanTaiDon;
                BillOfLading billOfLading = new BillOfLading()
                {
                    Reference = " ",
                    Issue = " ",
                    IssueLocation = " "
                };
                billOfLading.BorderTransportMeans = new BorderTransportMeans()
                {
                    Identity = " ",
                    Identification = " ",
                    Journey = " ",
                    ModeAndType = tkmd.PTVT_ID,
                    Departure = " ",
                    RegistrationNationality = " "
                };
                billOfLading.Carrier = new NameBase()
                {
                    Name = " ",
                    Identity = " "
                };

                billOfLading.Consignment = new Consignment()
                {
                    Consignor = new NameBase()
                    {
                        Name = " ",
                        Identity = " "
                    },
                    Consignee = new NameBase()
                    {
                        Name = " ",
                        Identity = " "
                    },
                    NotifyParty = new NameBase()
                    {
                        Name = " ",
                        Identity = " "
                    },
                    LoadingLocation = new LoadingLocation()
                    {
                        Name = " ",
                        Code = "",
                        Loading = ""
                    },
                    UnloadingLocation = new UnloadingLocation()
                    {
                        Name = " ",
                        Code = " ",
                        Arrival = " "
                    },
                    DeliveryDestination = new DeliveryDestination() { Line = " " },


                    ConsignmentItemPackaging = new Packaging()
                    {
                        Quantity = " ",
                        Type = " ",
                        MarkNumber = string.Empty
                    }
                };
                #region Hàng và Container trong vận đơn
                if (vandon.ContainerCollection != null && vandon.ContainerCollection.Count > 0)
                    billOfLading.Consignment.TransportEquipments = new List<TransportEquipment>();

                foreach (Company.KDT.SHARE.QuanLyChungTu.Container container in vandon.ContainerCollection)
                {

                    TransportEquipment transportEquipment = new TransportEquipment()
                    {
                        Characteristic = container.LoaiContainer,
                        EquipmentIdentifications = new EquipmentIdentification()
                        {
                            identification = container.SoHieu
                        },
                        Fullness = Helpers.FormatNumeric(container.Trang_thai, 0),
                        Seal = container.Seal_No
                    };

                    billOfLading.Consignment.TransportEquipments.Add(transportEquipment);
                }
                if (vandon.ListHangOfVanDon != null && vandon.ListHangOfVanDon.Count > 0)
                {
                    List<ConsignmentItem> hangsVanDon = new List<ConsignmentItem>();

                    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail hangVanDon in vandon.ListHangOfVanDon)
                    {
                        HangMauDich hangToKhai = tkmd.HMDCollection.Find(hang => hang.ID == hangVanDon.HMD_ID);
                        if (hangToKhai != null)
                            hangsVanDon.Add(new ConsignmentItem()
                            {
                                Sequence = Helpers.FormatNumeric(hangVanDon.SoThuTuHang),
                                ConsignmentItemPackaging = new Packaging()
                                {
                                    Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong),
                                    Type = hangVanDon.LoaiKien,
                                    MarkNumber = hangVanDon.SoHieuKien,
                                },
                                Commodity = new Commodity()
                                {
                                    Description = hangVanDon.TenHang,
                                    Identification = string.Empty,//Mã hàng
                                    TariffClassificationExtension = hangVanDon.MaHS.Trim()
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    GrossMass = Helpers.FormatNumeric(hangVanDon.TrongLuong, 3),
                                    MeasureUnit = hangVanDon.DVT_ID.Substring(0, 3)
                                },
                                EquipmentIdentification = new EquipmentIdentification()
                                {
                                    identification = hangVanDon.SoHieuContainer
                                }

                            });

                    }
                    billOfLading.Consignment.ConsignmentItems = hangsVanDon;
                }
                #endregion Hàng và Container trong vận đơn
                tokhai.BillOfLadings.Add(billOfLading);
            }
            #endregion
            #endregion BillOfLadings  Vận đơn

            if (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0)
            #region CustomsOfficeChangedRequest Đề nghị chuyển cửa khẩu
            {
                tokhai.CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau in tkmd.listChuyenCuaKhau)
                {
                    CustomsOfficeChangedRequest customsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCuaKhau);
                    tokhai.CustomsOfficeChangedRequest.Add(customsOfficeChangedRequest);
                };
            }
            #endregion Đề nghị chuyển cửa khẩu

            if (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0)
            #region AttachDocumentItem Chứng từ đính kèm
            {
                tokhai.AttachDocumentItem = new List<AttachDocumentItem>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem in tkmd.ChungTuKemCollection)
                {
                    AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, fileinChungtuDinhKem);
                    tokhai.AttachDocumentItem.Add(attachDocumentItem);
                };
            }
            #endregion Chứng từ đính kèm

            #endregion Danh sách giấy phép XNK đi kèm

            #region AdditionalDocumentNos Thêm chứng từ nợ

            if (tkmd.ChungTuNoCollection != null && tkmd.ChungTuNoCollection.Count > 0)
            {
                tokhai.AdditionalDocumentNos = new List<AdditionalDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuNo ctn in tkmd.ChungTuNoCollection)
                {
                    tokhai.AdditionalDocumentNos.Add(new AdditionalDocument()
                    {
                        Type = ctn.MA_LOAI_CT,
                        Reference = ctn.SO_CT,
                        Issue = ctn.NGAY_CT.ToString(sfmtDate),
                        IssueLocation = ctn.NOI_CAP,
                        Issuer = ctn.TO_CHUC_CAP,
                        Expire = ctn.NgayHetHan.ToString(sfmtDate),
                        IsDebt = Helpers.FormatNumeric(ctn.IsNoChungTu),
                        Submit = ctn.ThoiHanNop.ToString(sfmtDate),
                        AdditionalInformation = new AdditionalInformation()
                        {
                            Content = new Content() { Text = ctn.DIENGIAI }
                        }

                    });
                }

            }
            #endregion
            return tokhai;
        }
        #endregion

        #region Data Mapper
        private static List<Agent> AgentsFrom(ToKhaiMauDich tkmd)
        {
            List<Agent> Agents = new List<Agent>();
            // Edit by Khanhhn - 12/06/2012
            #region Agents Đại lý khai
            // Người khai hải quan
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Đại lý khai Hải Quan
            if (!string.IsNullOrEmpty(tkmd.TenDaiLyTTHQ))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDaiLyTTHQ,
                    Identity = tkmd.MaDaiLyTTHQ,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN
                });
            // ủy Thác
            if (!string.IsNullOrEmpty(tkmd.MaDonViUT))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDonViUT,
                    Identity = tkmd.MaDonViUT,
                    Status = AgentsStatus.UYTHAC,
                });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            #endregion
            return Agents;

        }
        private static CertificateOfOrigin CertificateOfOriginFrom(Company.KDT.SHARE.QuanLyChungTu.CO co, ToKhaiMauDich tkmd, bool isToKhaiNhap)
        {
            #region Điền thông tin CO
            CertificateOfOrigin certificateOfOrigin = new CertificateOfOrigin
            {


                Reference = co.SoCO,
                Type = co.LoaiCO,
                Issuer = co.ToChucCap,
                Issue = co.NgayCO.ToString(sfmtDate),
                IssueLocation = co.NuocCapCO,
                Representative = co.NguoiKy,

                ExporterEx = isToKhaiNhap ? new NameBase() { Name = co.TenDiaChiNguoiXK, Identity = string.Empty } : new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep },
                ExportationCountryEx = new LocationNameBase { Code = co.MaNuocXKTrenCO, Name = DuLieuChuan.Nuoc.GetName(co.MaNuocXKTrenCO) },

                ImporterEx = isToKhaiNhap ? new NameBase() { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep } :
                            new NameBase { Name = co.TenDiaChiNguoiNK, Identity = string.Empty },
                ImportationCountryEx = new LocationNameBase { Code = co.MaNuocNKTrenCO, Name = DuLieuChuan.Nuoc.GetName(co.MaNuocNKTrenCO) },

                LoadingLocation = new LoadingLocation { Name = DuLieuChuan.CuaKhau.GetName(co.CangXepHang.Trim()), Code = co.CangXepHang.Trim(), Loading = co.NgayKhoiHanh.ToString(sfmtDate) },

                UnloadingLocation = new UnloadingLocation { Name = DuLieuChuan.CuaKhau.GetName(co.CangDoHang.Trim()), Code = co.CangDoHang.Trim() },

                IsDebt = Helpers.FormatNumeric(co.NoCo),

                Submit = co.ThoiHanNop.ToString(sfmtDate),

                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = co.ThongTinMoTaChiTiet } },
                //Hang in Co
                GoodsItems = new List<GoodsItem>(),
            };
            #endregion CO
            #region Điền hàng trong Co

            foreach (Company.KDT.SHARE.QuanLyChungTu.HangCoDetail hangCo in co.ListHMDofCo)
            {
                GoodsItem goodsItem = new GoodsItem();
                goodsItem.Sequence = Helpers.FormatNumeric(hangCo.SoThuTuHang);
                goodsItem.StatisticalValue = Helpers.FormatNumeric(hangCo.TriGiaKB, GlobalsShare.TriGiaNT);
                goodsItem.CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = hangCo.MaNguyenTe
                };
                goodsItem.ConsignmentItemPackaging = new Packaging()
                {
                    Quantity = Helpers.FormatNumeric(hangCo.SoLuong),
                    Type = hangCo.LoaiKien,
                    MarkNumber = hangCo.SoHieuKien
                };
                goodsItem.Commodity = new Commodity()
                {
                    Description = hangCo.TenHang,
                    Identification = string.Empty,
                    TariffClassification = hangCo.MaHS
                };
                goodsItem.GoodsMeasure = new GoodsMeasure()
                {
                    GrossMass = Helpers.FormatNumeric(hangCo.TrongLuong, 3),
                    MeasureUnit = hangCo.DVT_ID
                };
                goodsItem.Origin = new Origin()
                {
                    OriginCountry = hangCo.NuocXX_ID.Trim()
                };
                goodsItem.Invoice = new Invoice()
                {
                    Reference = hangCo.SoHoaDon,
                    Issue = hangCo.NgayHoaDon.ToString(sfmtDate)
                };
                certificateOfOrigin.GoodsItems.Add(goodsItem);
            }
            #endregion Thêm hàng
            return certificateOfOrigin;
        }
        private static AttachDocumentItem AttachDocumentItemFrom(ToKhaiMauDich tkmd, Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem)
        {
            AttachDocumentItem attachDocumentItem = new AttachDocumentItem()
            {
                Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH,
                Sequence = Helpers.FormatNumeric(tkmd.ChungTuKemCollection.IndexOf(chungtukem) + 1, 0),
                Issue = chungtukem.NGAY_CT.ToString(sfmtDate),
                Reference = chungtukem.SO_CT,
                Description = chungtukem.DIENGIAI,
                AttachedFiles = new List<AttachedFile>(),
            };

            if (chungtukem.listCTChiTiet != null && chungtukem.listCTChiTiet.Count > 0)
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet fileDetail in chungtukem.listCTChiTiet)
                {

                    attachDocumentItem.AttachedFiles.Add(new AttachedFile
                    {
                        FileName = fileDetail.FileName,
                        //Content = new Content { Text = System.Convert.ToBase64String(fileDetail.NoiDung, Base64FormattingOptions.None), Base64 = "bin.base64" },
                    });
                };
            return attachDocumentItem;
        }
        private static License LicenseFrom(Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep)
        {
            License lic = new License
            {
                Issuer = giayPhep.NguoiCap,
                Reference = giayPhep.SoGiayPhep,
                Issue = giayPhep.NgayGiayPhep.ToString(sfmtDate),
                IssueLocation = giayPhep.NoiCap,
                Type = "",
                Expire = giayPhep.NgayHetHan.ToString(sfmtDate),
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = giayPhep.ThongTinKhac } },
                GoodItems = new List<GoodsItem>()
            };

            if (giayPhep.ListHMDofGiayPhep != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail hangInGiayPhep in giayPhep.ListHMDofGiayPhep)
                {
                    lic.GoodItems.Add(new GoodsItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInGiayPhep.SoThuTuHang, 0),
                        StatisticalValue = Helpers.FormatNumeric(hangInGiayPhep.TriGiaKB, GlobalsShare.TriGiaNT),
                        CurrencyExchange = new CurrencyExchange { CurrencyType = hangInGiayPhep.MaNguyenTe },
                        Commodity = new Commodity
                        {
                            Description = hangInGiayPhep.TenHang,
                            Identification = string.Empty,
                            TariffClassification = hangInGiayPhep.MaHS
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInGiayPhep.SoLuong, 3), MeasureUnit = hangInGiayPhep.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInGiayPhep.GhiChu } }

                    });
                }
            return lic;
        }
        private static ContractDocument ContractFrom(Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai)
        {
            ContractDocument contractDocument = new ContractDocument
            {
                Reference = hdThuongMai.SoHopDongTM,
                Issue = hdThuongMai.NgayHopDongTM.ToString(sfmtDate),
                Expire = hdThuongMai.ThoiHanThanhToan.ToString(sfmtDate),
                Payment = new Payment { Method = hdThuongMai.PTTT_ID },
                TradeTerm = new TradeTerm { Condition = hdThuongMai.DKGH_ID },
                DeliveryDestination = new DeliveryDestination { Line = hdThuongMai.DiaDiemGiaoHang },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hdThuongMai.NguyenTe_ID },
                TotalValue = Helpers.FormatNumeric(hdThuongMai.TongTriGia, 2),
                Buyer = new NameBase { Name = hdThuongMai.TenDonViMua, Identity = hdThuongMai.MaDonViMua },
                Seller = new NameBase { Name = hdThuongMai.TenDonViBan, Identity = hdThuongMai.MaDonViBan },
                AdditionalInformations = new List<AdditionalInformation>(),
                ContractItems = new List<ContractItem>()
            };
            contractDocument.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = hdThuongMai.ThongTinKhac } });
            if (hdThuongMai.ListHangMDOfHopDong != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail hangInHdThuongMai in hdThuongMai.ListHangMDOfHopDong)
                    contractDocument.ContractItems.Add(new ContractItem
                    {
                        unitPrice = Helpers.FormatNumeric(hangInHdThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        statisticalValue = Helpers.FormatNumeric(hangInHdThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Commodity = new Commodity
                        {
                            Description = hangInHdThuongMai.TenHang,
                            Identification = hangInHdThuongMai.MaPhu,
                            TariffClassification = hangInHdThuongMai.MaHS,
                        },
                        Origin = new Origin { OriginCountry = hangInHdThuongMai.NuocXX_ID.Substring(0, 2) },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHdThuongMai.SoLuong, 3), MeasureUnit = hangInHdThuongMai.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHdThuongMai.GhiChu } }
                    });
            return contractDocument;
        }
        /// <summary>
        /// Thông tin tham chiếu đến Tờ khai
        /// </summary>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>DeclarationBase</returns>
        private static DeclarationBase DeclarationDocument(ToKhaiMauDich tkmd)
        {
            return new DeclarationBase()
            {
                Reference = Helpers.FormatNumeric(tkmd.SoToKhai),
                Issue = tkmd.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = tkmd.MaLoaiHinh,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };

        }
        /// <summary>
        /// Đề nghị chuyển cửa khẩu
        /// </summary>
        /// <param name="dnChuyenCuaKhau"></param>
        /// <returns></returns>
        private static CustomsOfficeChangedRequest CustomsOfficeChangedRequestFrom(Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau)
        {
            CustomsOfficeChangedRequest customsOfficeChangedRequest = new CustomsOfficeChangedRequest
            {
                AdditionalDocument = new AdditionalDocument { Reference = dnChuyenCuaKhau.SoVanDon, Issue = dnChuyenCuaKhau.NgayVanDon.ToString(sfmtDate) },
                AdditionalInformation = new AdditionalInformation
                {
                    Content = new Content { Text = dnChuyenCuaKhau.ThongTinKhac },
                    ExaminationPlace = dnChuyenCuaKhau.DiaDiemKiemTra,
                    Time = dnChuyenCuaKhau.ThoiGianDen.ToString(sfmtDate),
                    Route = dnChuyenCuaKhau.TuyenDuong
                }
            };
            return customsOfficeChangedRequest;
        }
        private static CommercialInvoice CommercialInvoiceFrom(Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai)
        {
            CommercialInvoice commercialInvoice = new CommercialInvoice
            {
                Reference = hoaDonThuongMai.SoHoaDon,
                Issue = hoaDonThuongMai.NgayHoaDon.ToString(sfmtDate),
                Seller = new NameBase { Name = hoaDonThuongMai.TenDonViBan, Identity = hoaDonThuongMai.MaDonViBan },
                Buyer = new NameBase { Name = hoaDonThuongMai.TenDonViMua, Identity = hoaDonThuongMai.MaDonViMua },
                AdditionalDocument = new AdditionalDocument { Reference = "", Issue = "" },
                Payment = new Payment { Method = hoaDonThuongMai.PTTT_ID },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hoaDonThuongMai.NguyenTe_ID },
                TradeTerm = new TradeTerm { Condition = hoaDonThuongMai.DKGH_ID.Trim() },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>()
            };

            if (hoaDonThuongMai.ListHangMDOfHoaDon != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail hangInHoaDonThuongMai in hoaDonThuongMai.ListHangMDOfHoaDon)
                    commercialInvoice.CommercialInvoiceItems.Add(new CommercialInvoiceItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoThuTuHang, 0),
                        UnitPrice = Helpers.FormatNumeric(hangInHoaDonThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        StatisticalValue = Helpers.FormatNumeric(hangInHoaDonThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Origin = new Origin { OriginCountry = hangInHoaDonThuongMai.NuocXX_ID.Substring(0, 2) },
                        Commodity = new Commodity
                        {
                            Description = hangInHoaDonThuongMai.TenHang,
                            Identification = hangInHoaDonThuongMai.MaPhu,
                            TariffClassification = hangInHoaDonThuongMai.MaHS
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoLuong, 3), MeasureUnit = hangInHoaDonThuongMai.DVT_ID },
                        ValuationAdjustment = new ValuationAdjustment
                        {
                            Addition = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaTriDieuChinhTang, GlobalsShare.TriGiaNT),
                            Deduction = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaiTriDieuChinhGiam, GlobalsShare.TriGiaNT),
                        },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHoaDonThuongMai.GhiChu } },
                    });
            return commercialInvoice;
        }
        #endregion

        #region Bổ sung chứng từ
        public static BoSungChungTu ToDataTransferBoSung(ToKhaiMauDich tkmd, object NoiDungBoSung
            /*Company.KDT.SHARE.QuanLyChungTu.CO co,
            Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem,
            Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep,
            Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong,
            Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon,
            Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK*/
            )
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");

            BoSungChungTu boSungChungTuDto = new BoSungChungTu()
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                Function = DeclarationFunction.KHAI_BAO,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };
            boSungChungTuDto.Agents = AgentsFrom(tkmd);
            boSungChungTuDto.Importer = new NameBase()
            {
                Identity = tkmd.MaDoanhNghiep,
                Name = tkmd.TenDoanhNghiep
            };
            boSungChungTuDto.DeclarationDocument = DeclarationDocument(tkmd);

            if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.CO))
            #region CO
            {
                Company.KDT.SHARE.QuanLyChungTu.CO co = (Company.KDT.SHARE.QuanLyChungTu.CO)NoiDungBoSung;
                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = co.MaDoanhNghiep,
                    Name = co.TenDiaChiNguoiNK
                };
                boSungChungTuDto.Issuer = AdditionalDocumentType.EXPORT_CO_FORM_D;
                boSungChungTuDto.Reference = co.GuidStr;

                boSungChungTuDto.CertificateOfOrigins = new List<CertificateOfOrigin>();
                CertificateOfOrigin certificateOfOrigin = CertificateOfOriginFrom(co, tkmd, isToKhaiNhap);
                boSungChungTuDto.CertificateOfOrigins.Add(certificateOfOrigin);
            }
            #endregion

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GiayPhep))
            #region Giấy phép
            {

                Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = (Company.KDT.SHARE.QuanLyChungTu.GiayPhep)NoiDungBoSung;
                boSungChungTuDto.Issuer = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE;
                boSungChungTuDto.Reference = giayphep.GuidStr;

                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = giayphep.MaDonViDuocCap,
                    Name = giayphep.TenDonViDuocCap
                };
                boSungChungTuDto.Licenses = new List<License>();
                License license = LicenseFrom(giayphep);
                boSungChungTuDto.Licenses.Add(license);
            }
            #endregion Giấy phép
            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai))
            #region Hợp đồng thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong = (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOP_DONG;
                boSungChungTuDto.Reference = hopdong.GuidStr;

                boSungChungTuDto.ContractDocuments = new List<ContractDocument>();
                ContractDocument contract = ContractFrom(hopdong);
                boSungChungTuDto.ContractDocuments.Add(contract);
            }
            #endregion Hợp đồng thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai))
            #region Hóa đơn thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon = (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOA_DON_THUONG_MAI;
                boSungChungTuDto.Reference = hoadon.GuidStr;
                boSungChungTuDto.CommercialInvoices = new List<CommercialInvoice>();
                CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoadon);
                boSungChungTuDto.CommercialInvoices.Add(commercialInvoice);
            }
            #endregion Hóa đơn thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau))
            #region Đề nghị chuyển cửa khẩu
            {
                Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK = (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.DE_NGHI_CHUYEN_CUA_KHAU;
                boSungChungTuDto.Reference = dnChuyenCK.GuidStr;
                boSungChungTuDto.NatureOfTransaction = tkmd.MaLoaiHinh.Trim();
                boSungChungTuDto.CustomsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCK);
                boSungChungTuDto.CustomsReference = tkmd.SoToKhai.ToString();
                boSungChungTuDto.Acceptance = tkmd.NgayTiepNhan.ToString(sfmtDate);
                boSungChungTuDto.DeclarationDocument = null;
                boSungChungTuDto.Agents.RemoveAt(1);
            }
            #endregion Đề nghị chuyển cửa khẩu

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.ChungTuKem))
            #region Chứng từ kèm
            {
                Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem = (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH;
                boSungChungTuDto.Reference = chungtukem.GUIDSTR;

                boSungChungTuDto.AttachDocuments = new List<AttachDocumentItem>();
                AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, chungtukem);
                boSungChungTuDto.AttachDocuments.Add(attachDocumentItem);
            }
            #endregion Chứng từ kèm
            else if (NoiDungBoSung.GetType() == typeof(GiayNopTien))
            #region Giấy nộp tiền
            {
                Company.KDT.SHARE.QuanLyChungTu.GiayNopTien giayNopTien = (GiayNopTien)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.GIAY_NOP_TIEN;
                boSungChungTuDto.Reference = giayNopTien.GuidStr;
                Receipt receipt = ReceiptFrom(giayNopTien);
                boSungChungTuDto.Receipt = receipt;
            }
            #endregion
            return boSungChungTuDto;
        }


        #endregion
        private static Receipt ReceiptFrom(GiayNopTien giayNopTien)
        {
            Receipt receipt = new Receipt()
            {
                Reference = giayNopTien.SoLenh,
                Issue = giayNopTien.NgayPhatLenh.ToString(sfmtDate),
                Payer = new Payer
                {
                    Name = giayNopTien.TenNguoiNop,
                    Identity = giayNopTien.SoCMNDNguoiNop,
                    AddressGNT = new DeliveryDestination { Line = giayNopTien.DiaChi }
                },
                TaxPayer = new Payer
                {
                    Name = giayNopTien.TenDonViNop,
                    Identity = giayNopTien.MaDonViNop,
                    Account = new Account
                    {
                        Number = giayNopTien.SoTKNop,
                        Bank = new NameBase { Name = giayNopTien.TenNganHangNop, Identity = giayNopTien.MaNganHangNop }
                    }
                },
                Payee = new Payer
                {
                    Name = giayNopTien.TenDonViNhan,
                    Identity = giayNopTien.MaDonViNhan,
                    Account = new Account
                    {
                        Number = giayNopTien.SoTKNhan,
                        Bank = new NameBase { Name = giayNopTien.TenNganHangNhan, Identity = giayNopTien.MaNganHangNhan }
                    }
                },
                DutyTaxFee = new List<DutyTaxFee>(),
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = giayNopTien.GhiChu } },
                AdditionalDocument = new List<AdditionalDocument>(),
            };
            foreach (GiayNopTienChiTiet chitiet in giayNopTien.ChiTietCollection)
            {
                receipt.DutyTaxFee.Add(DutyTaxFeeForGiayNopTien(chitiet));
            }
            foreach (GiayNopTienChungTu chungtu in giayNopTien.ChungTuCollection)
            {
                AdditionalDocument add = new AdditionalDocument
                {
                    Type = chungtu.LoaiChungTu,
                    Reference = chungtu.SoChungTu,
                    Name = chungtu.TenChungTu,
                    Issue = chungtu.NgayPhatHanh.ToString(sfmtDate),
                };
            }
            return receipt;
        }
        private static DutyTaxFee DutyTaxFeeForGiayNopTien(GiayNopTienChiTiet chiTietGiayNopTien)
        {
            DutyTaxFee tax = new DutyTaxFee
            {
                AdValoremTaxBase = Helpers.FormatNumeric(chiTietGiayNopTien.SoTien, 4),
                Deduct = Helpers.FormatNumeric(chiTietGiayNopTien.DieuChinhGiam, 4),
                Type = Helpers.FormatNumeric(chiTietGiayNopTien.SacThue),
                AdditionalInformations = new List<AdditionalInformation>(),
            };
            for (int i = 1; i < 6; i++)
            {
                AdditionalInformation add = new AdditionalInformation
                {
                    Content = new Content()
                };
                switch (i)
                {
                    case 1:
                        add.Statement = "211";
                        add.StatementDescription = "Chuong";
                        add.Content.Text = chiTietGiayNopTien.MaChuong;
                        break;
                    case 2:
                        add.Statement = "212";
                        add.StatementDescription = "Loai";
                        add.Content.Text = chiTietGiayNopTien.Loai;
                        break;
                    case 3:
                        add.Statement = "213";
                        add.StatementDescription = "Khoan";
                        add.Content.Text = chiTietGiayNopTien.Khoan;
                        break;
                    case 4:
                        add.Statement = "214";
                        add.StatementDescription = "Muc";
                        add.Content.Text = chiTietGiayNopTien.Muc;
                        break;
                    case 5:
                        add.Statement = "215";
                        add.StatementDescription = "Tieu Muc";
                        add.Content.Text = chiTietGiayNopTien.TieuMuc;
                        break;
                }
                tax.AdditionalInformations.Add(add);
            }

            return tax;
        }

        public static BoSungChungTu ToDataTransferBoSungChuyenTiep(ToKhaiChuyenTiep tkct, object NoiDungBoSung, string tenDoanhNghiep)
        /*
       Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem,
       Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep,
       Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong,
       Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon,
       Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK*/
        {
            bool isTokhaiCTiepNhap = tkct.MaLoaiHinh.Substring(0, 1).Equals("N");
            BoSungChungTu BoSungChungTuCT = new BoSungChungTu()
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                DeclarationOffice = tkct.MaHaiQuanTiepNhan.Trim(),



            };

            BoSungChungTuCT.Agents = AgentsFromCT(tkct, tenDoanhNghiep);
            BoSungChungTuCT.Importer = new NameBase() { Name = tenDoanhNghiep, Identity = tkct.MaDoanhNghiep };
            BoSungChungTuCT.DeclarationDocument = DeclarationDocumentCT(tkct);
            if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep))
            {
                #region Giấy phép tờ khai chuyển tiếp
                Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep giayphep = (Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep)NoiDungBoSung;
                BoSungChungTuCT.Issuer = isTokhaiCTiepNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE;
                BoSungChungTuCT.Reference = giayphep.GuidStr;

                BoSungChungTuCT.Importer = new NameBase()
                {
                    Identity = giayphep.MaDonViDuocCap,
                    Name = giayphep.TenDonViDuocCap
                };
                BoSungChungTuCT.Licenses = new List<License>();
                License license = LicenseFromCT(giayphep);
                BoSungChungTuCT.Licenses.Add(license);
                #endregion
            }

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong))
            {
                #region Hop Dong Thương mại tờ khai chuyển tiếp
                Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hopdong = (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong)NoiDungBoSung;
                BoSungChungTuCT.Issuer = AdditionalDocumentType.HOP_DONG;
                BoSungChungTuCT.Reference = hopdong.GuidStr;

                BoSungChungTuCT.ContractDocuments = new List<ContractDocument>();
                ContractDocument contract = ContractFromCT(hopdong);
                BoSungChungTuCT.ContractDocuments.Add(contract);
                #endregion
            }
            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon))
            {
                #region Hóa đơn thương mại Tờ khai Chuyển tiếp
                Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon hoadon = (Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon)NoiDungBoSung;
                BoSungChungTuCT.Issuer = AdditionalDocumentType.HOA_DON_THUONG_MAI;
                BoSungChungTuCT.Reference = hoadon.GuidStr;
                BoSungChungTuCT.CommercialInvoices = new List<CommercialInvoice>();
                CommercialInvoice commercialInvoice = CommercialInvoiceFromCT(hoadon);
                BoSungChungTuCT.CommercialInvoices.Add(commercialInvoice);
                #endregion
            }
            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau))
            {
                #region Đê nghị chuyển cửa khẩu Tờ khai Chuyển tiếp
                Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau dnChuyenCK = (Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau)NoiDungBoSung;
                BoSungChungTuCT.Issuer = AdditionalDocumentType.DE_NGHI_CHUYEN_CUA_KHAU;
                BoSungChungTuCT.Reference = dnChuyenCK.GuidStr;
                BoSungChungTuCT.NatureOfTransaction = tkct.MaLoaiHinh.Trim();
                BoSungChungTuCT.CustomsOfficeChangedRequest = CustomsOfficeChangedRequestFromCT(dnChuyenCK);
                BoSungChungTuCT.CustomsReference = tkct.SoToKhai.ToString();
                BoSungChungTuCT.Acceptance = tkct.NgayTiepNhan.ToString(sfmtDate);
                BoSungChungTuCT.DeclarationDocument = null;
                BoSungChungTuCT.Agents.RemoveAt(1);
                #endregion
            }
            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh))
            {
                #region Chứng từ kèm tờ khai chuyển tiếp
                Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh chungtukem = (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh)NoiDungBoSung;
                BoSungChungTuCT.Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH;
                BoSungChungTuCT.Reference = chungtukem.GUIDSTR;

                BoSungChungTuCT.AttachDocuments = new List<AttachDocumentItem>();
                AttachDocumentItem attachDocumentItem = AttachDocumentItemFromCT(tkct, chungtukem);
                BoSungChungTuCT.AttachDocuments.Add(attachDocumentItem);
                #endregion
            }
            return BoSungChungTuCT;
        }

        private class DistinctMaSP : IEqualityComparer<DinhMuc>
        {
            public bool Equals(DinhMuc x, DinhMuc y)
            {
                return x.MaSanPham == y.MaSanPham;
            }

            public int GetHashCode(DinhMuc obj)
            {
                return obj.MaSanPham.GetHashCode();
            }
        }
        /// <summary>
        /// AgentsFromCT
        /// </summary>
        /// <param name="tkct"></param>
        /// <returns></returns>
        private static List<Agent> AgentsFromCT(ToKhaiChuyenTiep tkct, string tenDoanhNghiep)
        {
            List<Agent> Agents = new List<Agent>();
            // Edit by Khanhhn - 12/06/2012
            #region Agents Đại lý khai
            // Người khai hải quan
            Agents.Add(new Agent
            {
                Name = tenDoanhNghiep,
                Identity = tkct.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Đại lý khai Hải Quan
            if (!string.IsNullOrEmpty(tkct.TenDaiLyTTHQ))
                Agents.Add(new Agent
                {
                    Name = tkct.TenDaiLyTTHQ,
                    Identity = tkct.MaDaiLy,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN
                });
            // ủy Thác
            if (!string.IsNullOrEmpty(tkct.MaDonViUT))
                Agents.Add(new Agent
                {
                    Name = tkct.TenDonViUT,
                    Identity = tkct.MaDonViUT,
                    Status = AgentsStatus.UYTHAC,
                });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            Agents.Add(new Agent
            {
                Name = tenDoanhNghiep,
                Identity = tkct.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            #endregion
            return Agents;
        }
        /// <summary>
        /// DeclarationDocumentCT
        /// </summary>
        /// <param name="tkct"></param>
        /// <returns></returns>
        private static DeclarationBase DeclarationDocumentCT(ToKhaiChuyenTiep tkct)
        {
            DeclarationBase DeclarationDoc = new DeclarationBase()
            {
                Reference = Helpers.FormatNumeric(tkct.SoToKhai),
                Issue = tkct.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = tkct.MaLoaiHinh.Trim(),
                DeclarationOffice = tkct.MaHaiQuanTiepNhan.Trim()
            };
            return DeclarationDoc;
        }
        /// <summary>
        /// LicenseCT
        /// </summary>
        /// <param name="giayPhep"></param>
        /// <returns></returns>
        private static License LicenseFromCT(Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep giayPhep)
        {
            License lic = new License
            {
                Issuer = giayPhep.NguoiCap,
                Reference = giayPhep.SoGiayPhep,
                Issue = giayPhep.NgayGiayPhep.ToString(sfmtDate),
                IssueLocation = giayPhep.NoiCap,
                Type = "",
                Expire = giayPhep.NgayHetHan.ToString(sfmtDate),
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = giayPhep.ThongTinKhac } },
                GoodItems = new List<GoodsItem>()
            };

            if (giayPhep.ListHMDofGiayPhep != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhepChiTiet hangInGiayPhep in giayPhep.ListHMDofGiayPhep)
                {
                    lic.GoodItems.Add(new GoodsItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInGiayPhep.SoThuTuHang, 0),
                        StatisticalValue = Helpers.FormatNumeric(hangInGiayPhep.TriGiaKB, GlobalsShare.TriGiaNT),
                        CurrencyExchange = new CurrencyExchange { CurrencyType = hangInGiayPhep.MaNguyenTe },
                        Commodity = new Commodity
                        {
                            Description = hangInGiayPhep.TenHang,
                            Identification = string.Empty,
                            TariffClassification = hangInGiayPhep.MaHS
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInGiayPhep.SoLuong, 3), MeasureUnit = hangInGiayPhep.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInGiayPhep.GhiChu } }

                    });
                }
            return lic;
        }
        /// <summary>
        /// CertificateOfOriginCT
        /// </summary>
        /// <param name="co"></param>
        /// <param name="tkmd"></param>
        /// <param name="isTokhaiCTiepNhap"></param>
        /// <returns></returns>
        private static AttachDocumentItem AttachDocumentItemFromCT(ToKhaiChuyenTiep tkct, Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh chungtukem)
        {
            AttachDocumentItem attachDocumentItem = new AttachDocumentItem()
            {
                Issuer = "200",
                Sequence = Helpers.FormatNumeric(tkct.AnhCollection.IndexOf(chungtukem) + 1, 0),
                Issue = chungtukem.NGAY_CT.ToString(sfmtDate),
                Reference = chungtukem.SO_CT,
                Description = chungtukem.DIENGIAI,
                AttachedFiles = new List<AttachedFile>(),
            };

            if (chungtukem.ListChungTuKemAnhChiTiet != null && chungtukem.ListChungTuKemAnhChiTiet.Count > 0)
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnhChiTiet fileDetail in chungtukem.ListChungTuKemAnhChiTiet)
                {

                    attachDocumentItem.AttachedFiles.Add(new AttachedFile
                    {
                        FileName = fileDetail.FileName,
                        //Content = new Content { Text = System.Convert.ToBase64String(fileDetail.NoiDung, Base64FormattingOptions.None), Base64 = "bin.base64" },
                    });
                };
            return attachDocumentItem;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hdThuongMai"></param>
        /// <returns></returns>
        private static ContractDocument ContractFromCT(Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hdThuongMai)
        {
            ContractDocument contractDocument = new ContractDocument
            {
                Reference = hdThuongMai.SoHopDongTM,
                Issue = hdThuongMai.NgayHopDongTM.ToString(sfmtDate),
                Expire = hdThuongMai.ThoiHanThanhToan.ToString(sfmtDate),
                Payment = new Payment { Method = hdThuongMai.PTTT_ID },
                TradeTerm = new TradeTerm { Condition = hdThuongMai.DKGH_ID },
                DeliveryDestination = new DeliveryDestination { Line = hdThuongMai.DiaDiemGiaoHang },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hdThuongMai.NguyenTe_ID },
                TotalValue = Helpers.FormatNumeric(hdThuongMai.TongTriGia, 2),
                Buyer = new NameBase { Name = hdThuongMai.TenDonViMua, Identity = hdThuongMai.MaDonViMua },
                Seller = new NameBase { Name = hdThuongMai.TenDonViBan, Identity = hdThuongMai.MaDonViBan },
                AdditionalInformations = new List<AdditionalInformation>(),
                ContractItems = new List<ContractItem>()
            };
            contractDocument.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = hdThuongMai.ThongTinKhac } });
            if (hdThuongMai.ListHangMDOfHopDong != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDongChiTiet hangInHdThuongMai in hdThuongMai.ListHangMDOfHopDong)
                    contractDocument.ContractItems.Add(new ContractItem
                    {
                        unitPrice = Helpers.FormatNumeric(hangInHdThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        statisticalValue = Helpers.FormatNumeric(hangInHdThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Commodity = new Commodity
                        {
                            Description = hangInHdThuongMai.TenHang,
                            Identification = hangInHdThuongMai.MaPhu,
                            TariffClassification = hangInHdThuongMai.MaHS,
                        },
                        Origin = new Origin { OriginCountry = hangInHdThuongMai.NuocXX_ID.Substring(0, 2) },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHdThuongMai.SoLuong, 3), MeasureUnit = hangInHdThuongMai.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHdThuongMai.GhiChu } }
                    });
            return contractDocument;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hoaDonThuongMai"></param>
        /// <returns></returns>
        private static CommercialInvoice CommercialInvoiceFromCT(Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon hoaDonThuongMai)
        {
            CommercialInvoice commercialInvoice = new CommercialInvoice
            {
                Reference = hoaDonThuongMai.SoHoaDon,
                Issue = hoaDonThuongMai.NgayHoaDon.ToString(sfmtDate),
                Seller = new NameBase { Name = hoaDonThuongMai.TenDonViBan, Identity = hoaDonThuongMai.MaDonViBan },
                Buyer = new NameBase { Name = hoaDonThuongMai.TenDonViMua, Identity = hoaDonThuongMai.MaDonViMua },
                AdditionalDocument = new AdditionalDocument { Reference = "", Issue = "" },
                Payment = new Payment { Method = hoaDonThuongMai.PTTT_ID },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hoaDonThuongMai.NguyenTe_ID },
                TradeTerm = new TradeTerm { Condition = hoaDonThuongMai.DKGH_ID },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>()
            };

            if (hoaDonThuongMai.ListHangMDOfHoaDon != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDonChiTiet hangInHoaDonThuongMai in hoaDonThuongMai.ListHangMDOfHoaDon)
                    commercialInvoice.CommercialInvoiceItems.Add(new CommercialInvoiceItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoThuTuHang, 0),
                        UnitPrice = Helpers.FormatNumeric(hangInHoaDonThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        StatisticalValue = Helpers.FormatNumeric(hangInHoaDonThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Origin = new Origin { OriginCountry = hangInHoaDonThuongMai.NuocXX_ID.Substring(0, 2) },
                        Commodity = new Commodity
                        {
                            Description = hangInHoaDonThuongMai.TenHang,
                            Identification = hangInHoaDonThuongMai.MaPhu,
                            TariffClassification = hangInHoaDonThuongMai.MaHS
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoLuong, 3), MeasureUnit = hangInHoaDonThuongMai.DVT_ID },
                        ValuationAdjustment = new ValuationAdjustment
                        {
                            Addition = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaTriDieuChinhTang, 4),
                            Deduction = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaiTriDieuChinhGiam, 8),
                        },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHoaDonThuongMai.GhiChu } },
                    });
            return commercialInvoice;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dnChuyenCuaKhau"></param>
        /// <returns></returns>
        private static CustomsOfficeChangedRequest CustomsOfficeChangedRequestFromCT(Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau dnChuyenCuaKhau)
        {
            CustomsOfficeChangedRequest customsOfficeChangedRequest = new CustomsOfficeChangedRequest
            {
                AdditionalDocument = new AdditionalDocument { Reference = dnChuyenCuaKhau.SoVanDon, Issue = dnChuyenCuaKhau.NgayVanDon.ToString(sfmtDate) },
                AdditionalInformation = new AdditionalInformation
                {
                    Content = new Content { Text = dnChuyenCuaKhau.ThongTinKhac },
                    ExaminationPlace = dnChuyenCuaKhau.DiaDiemKiemTra,
                    Time = dnChuyenCuaKhau.ThoiGianDen.ToString(sfmtDate),
                    Route = dnChuyenCuaKhau.TuyenDuong
                }
            };
            return customsOfficeChangedRequest;
        }
        public static ToKhai ToDataTransferToKhaiCT(ToKhaiChuyenTiep tkct, string tenDoanhNghiep)
        {
            bool isToKhaiNhap = tkct.MaLoaiHinh.Substring(0, 1).Equals("N");
            bool isToKhaiSua = tkct.SoToKhai != 0 && tkct.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (isToKhaiSua)
                tkct.SoTiepNhan = KhaiBaoSua.SoTNSua(tkct.SoTiepNhan, tkct.SoToKhai, tkct.NamDK, tkct.MaLoaiHinh,
                        tkct.MaHaiQuanTiepNhan, tkct.MaDoanhNghiep, LoaiKhaiBao.ToKhaiCT);

            #region Header
            ToKhai tokhai = new ToKhai()
            {
                Issuer = isToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT,
                Reference = tkct.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                // Số tiếp nhận
                CustomsReference = isToKhaiSua ? Helpers.FormatNumeric(tkct.SoTiepNhan) : string.Empty,
                //Ngày dang ký chứng thư
                Acceptance = isToKhaiSua ? tkct.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                // Ðon vị hải quan khai báo
                DeclarationOffice = tkct.MaHaiQuanTiepNhan.Trim(),
                // Số hàng
                GoodsItem = Helpers.FormatNumeric(tkct.HCTCollection.Count),
                LoadingList = Helpers.FormatNumeric(Helpers.GetLoading(tkct.HCTCollection.Count)),

                // Khối luợng và khối luợng tịnh
                TotalGrossMass = Helpers.FormatNumeric(tkct.TrongLuong, 3),
                TotalNetGrossMass = Helpers.FormatNumeric(tkct.TrongLuongNet, 3),
                // Mã Loại Hình
                NatureOfTransaction = tkct.MaLoaiHinh,
                // Phuong thức thanh toán
                PaymentMethod = tkct.PTTT_ID,



                #region Install element
                Agents = new List<Agent>(),

                //Nguyên tệ
                CurrencyExchange = new CurrencyExchange { CurrencyType = tkct.NguyenTe_ID, Rate = Helpers.FormatNumeric(tkct.TyGiaVND, 3) },

                //Số kiện
                DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tkct.SoKien) },

                // Doanh Nghiệp Xuất khẩu
                // Exporter = isToKhaiNhap ? new NameBase { Name = tkct.TenDonViDoiTac, Identity = tkct.MaKhachHang } :
                //new NameBase { Name = tenDoanhNghiep, Identity = tkct.MaDoanhNghiep.Trim() },
                Exporter = isToKhaiNhap ? null : new NameBase { Name = tenDoanhNghiep, Identity = tkct.MaDoanhNghiep },

                //Doanh nghiệp nhập khẩu
                //Importer = isToKhaiNhap ? new NameBase { Name = tenDoanhNghiep, Identity = tkct.MaDoanhNghiep } :
                //new NameBase { Name = tkct.TenDonViDoiTac, Identity = ""},
                Importer = isToKhaiNhap ? new NameBase { Name = tenDoanhNghiep, Identity = tkct.MaDoanhNghiep } : null,

                // Người đại diện doanh nghiệp | Tên chủ hàng
                RepresentativePerson = new RepresentativePerson { Name = tkct.DaiDienDoanhNghiep, ContactFunction = tkct.CanBoDangKy },

                // Đề xuất khác | Mã loại thông tin - Ghi chú khác
                AdditionalInformations = new List<AdditionalInformation>(),

                // GoodsShipmet Thông tin hàng hóa
                GoodsShipment = new GoodsShipment(),

                /*  License = new List<License>(),
                  ContractDocument = new List<ContractDocument>(),
                  CommercialInvoices = new List<CommercialInvoice>(),
                  CertificateOfOrigins = new List<CertificateOfOrigin>(),
                  CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
                  AttachDocumentItem = new List<AttachDocumentItem>(),
                  AdditionalDocumentEx = new List<AdditionalDocument>(),
                 */
                #endregion Nrr

            };
            tokhai.AdditionalInformations.Add(new AdditionalInformation
            {
                Statement = "001",
                Content = new Content() { Text = tkct.DeXuatKhac }
            });
            if (isToKhaiSua)
                tokhai.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Statement = "005",
                    Content = new Content() { Text = tkct.LyDoSua }
                });
            #endregion Header
            #region Agents Đại lý khai
            tokhai.Agents = AgentsFromCT(tkct, tenDoanhNghiep);
            #endregion
            #region AdditionalDocument Hợp đồng
            tokhai.AdditionalDocuments = new List<AdditionalDocument>();

            // Add AdditionalDocument (thêm hợp đồng) nhận          
            tokhai.AdditionalDocuments.Add(new AdditionalDocument
            {
                Issue = isToKhaiNhap ? tkct.NgayHDDV.ToString(sfmtDate) : tkct.NgayHDKH.ToString(sfmtDate),
                Reference = isToKhaiNhap ? tkct.SoHopDongDV : tkct.SoHDKH,
                Type = AdditionalDocumentType.HOP_DONG_NHAN,
                Name = "Hop dong",
                Expire = isToKhaiNhap ? tkct.NgayHetHanHDDV.ToString(sfmtDate) : tkct.NgayHetHanHDKH.ToString(sfmtDate)
            });
            // Add AdditionalDocument (thêm hợp đồng) giao          
            tokhai.AdditionalDocuments.Add(new AdditionalDocument
            {
                Issue = isToKhaiNhap ? tkct.NgayHDKH.ToString(sfmtDate) : tkct.NgayHDDV.ToString(sfmtDate),
                Reference = isToKhaiNhap ? tkct.SoHDKH : tkct.SoHopDongDV,
                Type = AdditionalDocumentType.HOP_DONG_GIAO,
                Name = "Hop dong",
                Expire = isToKhaiNhap ? tkct.NgayHetHanHDKH.ToString(sfmtDate) : tkct.NgayHetHanHDDV.ToString(sfmtDate)
            });
            #endregion
            #region GoodsShipment thông tin về hàng hóa
            tokhai.GoodsShipment = new GoodsShipment()
            {
                ImportationCountry = isToKhaiNhap ? "VN" : tkct.NuocNK_ID.Substring(0, 2),
                ExportationCountry = isToKhaiNhap ? tkct.NuocXK_ID.Substring(0, 2) : "VN",
                Consignor = isToKhaiNhap ? new NameBase { Identity = tkct.MaKhachHang, Name = tkct.NguoiChiDinhKH } : new NameBase { Identity = tkct.MaDoanhNghiep, Name = tenDoanhNghiep },
                Consignee = isToKhaiNhap ? new NameBase { Identity = tkct.MaDoanhNghiep, Name = tenDoanhNghiep } : new NameBase { Identity = tkct.MaKhachHang, Name = tkct.NguoiChiDinhKH },
                NotifyParty = new NameBase { Identity = tkct.NguoiChiDinhDV, Name = tkct.NguoiChiDinhKH },
                DeliveryDestination = new DeliveryDestination { Line = tkct.DiaDiemXepHang, Time = tkct.ThoiGianGiaoHang.ToString(sfmtDate) },
                EntryCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? tkct.CuaKhau_ID : string.Empty, Name = isToKhaiNhap ? DuLieuChuan.CuaKhau.GetName(tkct.CuaKhau_ID.Trim()) : string.Empty },
                ExitCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? string.Empty : tkct.CuaKhau_ID, Name = isToKhaiNhap ? string.Empty : DuLieuChuan.CuaKhau.GetName(tkct.CuaKhau_ID.Trim()) },
                Importer = isToKhaiNhap ? null : new NameBase { Name = tkct.TenDonViDoiTac, Identity = string.Empty },
                Exporter = isToKhaiNhap ? new NameBase { Name = tkct.TenDonViDoiTac, Identity = string.Empty } : null,
                //Exporter = new NameBase { Name = tkct.TenDonViDoiTac, Identity = tkct.MaKhachHang },
                //Importer = new NameBase { Name = tkct.TenDonViDoiTac, Identity = tkct.MaKhachHang },
                TradeTerm = new TradeTerm { Condition = tkct.DKGH_ID },
                //CustomsGoodsItems = new List<CustomsGoodsItem>()
            };
            #endregion GoodsShipment
            #region CustomGoodsItem Danh sách hàng khai báo

            //Add CustomGoodsItem
            int soluonghang = 0;
            if (tkct.HCTCollection != null)
            {
                soluonghang = tkct.HCTCollection.Count;
                tokhai.GoodsShipment.CustomsGoodsItems = new List<CustomsGoodsItem>();
            }
            for (int i = 0; i < soluonghang; i++)
            {
                HangChuyenTiep hct = tkct.HCTCollection[i];
                #region CustomsGoodsItem
                CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem
                {

                    CustomsValue = Helpers.FormatNumeric(hct.TriGia, GlobalsShare.TriGiaNT),
                    Sequence = Helpers.FormatNumeric(hct.SoThuTuHang),
                    StatisticalValue = Helpers.FormatNumeric(hct.TriGiaTT, GlobalsShare.TriGiaNT),
                    UnitPrice = Helpers.FormatNumeric(hct.DonGia, GlobalsShare.DonGiaNT),
                    StatisticalUnitPrice = Helpers.FormatNumeric(hct.DonGiaTT, GlobalsShare.DonGiaNT),
                    Manufacturer = new NameBase { Name = hct.TenHangSX/*Tên hãng sx*/, Identity = hct.MaHangSX/*Mã hãng sx*/ },
                    Origin = new Origin { OriginCountry = hct.ID_NuocXX.Substring(0, 2) },
                    GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hct.SoLuong, GlobalsShare.TriGiaNT), MeasureUnit = hct.ID_DVT }

                };


                customsGoodsItem.CustomsValuation = new CustomsValuation
                {
                    ExitToEntryCharge = "0",
                    FreightCharge = "0",
                    Method = string.Empty,
                    OtherChargeDeduction = "0"
                };

                #endregion

                #region Commodity Hàng trong tờ khai
                Commodity commodity = new Commodity
                {
                    Description = hct.TenHang,
                    Identification = hct.MaHang,
                    TariffClassification = hct.MaHS,
                    TariffClassificationExtension = hct.MaHSMoRong,
                    Brand = hct.NhanHieu,
                    Grade = hct.QuyCachPhamChat,
                    Ingredients = hct.ThanhPhan,
                    ModelNumber = hct.Model,
                    DutyTaxFee = new List<DutyTaxFee>(),
                    //Type = isToKhaiNhap ? "1" : "2",
                    //Hệ thống hải quan chưa sử dụng
                    InvoiceLine = new InvoiceLine { ItemCharge = string.Empty, Line = string.Empty }
                };
                if (tkct.LoaiHangHoa == "N")
                    commodity.Type = "1";
                else if (tkct.LoaiHangHoa == "S")
                    commodity.Type = "2";
                else if (tkct.LoaiHangHoa == "T")
                    commodity.Type = "3";
                else if (tkct.LoaiHangHoa == "H")
                    commodity.Type = "4";

                #region DutyTaxFee tiền thuế của hàng
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueXNK, 2),
                    DutyRegime = string.Empty,
                    SpecificTaxBase = string.Empty,
                    Tax = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueSuatXNK, 1),
                    Type = DutyTaxFeeType.THUE_XNK
                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueGTGT, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueSuatGTGT, 1),
                    Type = DutyTaxFeeType.THUE_VAT,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueTTDB, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueSuatTTDB, 1),
                    Type = DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueXNK, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueSuatXNK, 1),
                    Type = DutyTaxFeeType.THUE_KHAC,


                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkct.HCTCollection[i].PhuThu, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkct.HCTCollection[i].TyLeThuKhac, 1),
                    Type = DutyTaxFeeType.THUE_CHENH_LECH_GIA,

                });
                #endregion DutyTaxFee thuế

                customsGoodsItem.Commodity = commodity;
                #endregion Hàng chính

                #region AdditionalDocument if Search(Giấy phép có hàng trong tờ khai)
                //Lấy thông tin giấy phép đầu tiên
                if (tkct.GiayPhepCollection != null && tkct.GiayPhepCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep giayphep = tkct.GiayPhepCollection[0];
                    customsGoodsItem.AdditionalDocument = new AdditionalDocument()
                    {
                        Issue = giayphep.NgayGiayPhep.ToString(sfmtDate), /*Ngày cấp giấy phép yyyy-MM-dd*/
                        Issuer = giayphep.NguoiCap/*Người cấp*/,
                        IssueLocation = giayphep.NoiCap/*Nơi cấp*/,
                        Reference = giayphep.SoGiayPhep/*Số giấy phép*/,
                        Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                        Name = "GP"/* Tên giấy phép*/,
                        Expire = giayphep.NgayHetHan.ToString(sfmtDate)
                    };
                }
                #endregion

                tokhai.GoodsShipment.CustomsGoodsItems.Add(customsGoodsItem);
            }
            #endregion CustomGoodsItem Danh sách hàng khai báo
            #region Licenses Danh sách giấy phép XNK đi kèm

            if (tkct.GiayPhepCollection != null && tkct.GiayPhepCollection.Count > 0)
            #region License Giấy phép
            {
                tokhai.License = new List<License>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep giayPhep in tkct.GiayPhepCollection)
                {
                    License lic = LicenseFromCT(giayPhep);
                    tokhai.License.Add(lic);
                }
            }
            #endregion Giấy phép

            if (tkct.HopDongThuongMaiCollection != null && tkct.HopDongThuongMaiCollection.Count > 0)
            #region ContractDocument  Hợp đồng thương mại
            {
                tokhai.ContractDocument = new List<ContractDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hdThuongMai in tkct.HopDongThuongMaiCollection)
                {
                    ContractDocument contractDocument = ContractFromCT(hdThuongMai);
                    tokhai.ContractDocument.Add(contractDocument);
                }
            }
            #endregion Hợp đồng thương mại

            if (tkct.HoaDonThuongMaiCollection != null && tkct.HoaDonThuongMaiCollection.Count > 0)
            #region CommercialInvoice Hóa đơn thương mại
            {
                tokhai.CommercialInvoices = new List<CommercialInvoice>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon hoaDonThuongMai in tkct.HoaDonThuongMaiCollection)
                {
                    CommercialInvoice commercialInvoice = CommercialInvoiceFromCT(hoaDonThuongMai);
                    tokhai.CommercialInvoices.Add(commercialInvoice);
                }
            }
            #endregion Hóa đơn thương mại

            if (tkct.DeNghiChuyenCuaKhau != null && tkct.DeNghiChuyenCuaKhau.Count > 0)
            #region CustomsOfficeChangedRequest Đề nghị chuyển cửa khẩu
            {
                tokhai.CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau dnChuyenCuaKhau in tkct.DeNghiChuyenCuaKhau)
                {
                    CustomsOfficeChangedRequest customsOfficeChangedRequest = CustomsOfficeChangedRequestFromCT(dnChuyenCuaKhau);
                    tokhai.CustomsOfficeChangedRequest.Add(customsOfficeChangedRequest);
                };
            }
            #endregion Đề nghị chuyển cửa khẩu

            if (tkct.AnhCollection != null && tkct.AnhCollection.Count > 0)
            #region AttachDocumentItem Chứng từ đính kèm
            {
                tokhai.AttachDocumentItem = new List<AttachDocumentItem>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh fileinChungtuDinhKem in tkct.AnhCollection)
                {
                    AttachDocumentItem attachDocumentItem = AttachDocumentItemFromCT(tkct, fileinChungtuDinhKem);
                    tokhai.AttachDocumentItem.Add(attachDocumentItem);
                };
            }
            #endregion Chứng từ đính kèm

            #endregion Danh sách giấy phép XNK đi kèm
            #region Supply Nguyên phụ liệu cung ứng cho tờ khai GC chuyển tiếp XK
            if (!isToKhaiNhap && tkct.NPLCungUngs != null && tkct.NPLCungUngs.Count > 0)
            {

                tokhai.GoodsShipment.Supplys = new List<Supply>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng nplCungUng in tkct.NPLCungUngs)
                {
                    Supply supply = new Supply()
                    {
                        Material = new Product()
                        {
                            Commodity = new Commodity()
                            {
                                Identification = nplCungUng.MaHang,
                                Description = nplCungUng.TenHang,
                                TariffClassification = nplCungUng.MaHS
                            }
                        }
                    };
                    if (nplCungUng.NPLCungUngDetails != null && nplCungUng.NPLCungUngDetails.Count > 0)
                    {
                        supply.SupplyItems = new List<SupplyItem>();
                        foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail nplCungUngDetail in nplCungUng.NPLCungUngDetails)
                        {
                            SupplyItem supplyItem = new SupplyItem()
                            {
                                Invoice = new Invoice()
                                {
                                    Type = Helpers.FormatNumeric(nplCungUngDetail),
                                    Reference = nplCungUngDetail.SoChungTu,
                                    Issue = nplCungUngDetail.NgayChungTu.ToString(sfmtDate),
                                    Issuer = nplCungUngDetail.NoiPhatHanh,
                                    NatureOfTransaction = nplCungUngDetail.MaLoaiHinh.Trim(),
                                    DeclarationOffice = nplCungUngDetail.MaHaiQuan.Trim()
                                },
                                InvoiceLine = new InvoiceLine()
                                {
                                    Line = Helpers.FormatNumeric(nplCungUngDetail.DongHangTrenCT)
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    Quantity = Helpers.FormatNumeric(nplCungUngDetail.SoLuong, 3),
                                    MeasureUnit = nplCungUngDetail.DVT_ID.Trim(),
                                    ConversionRate = Helpers.FormatNumeric(nplCungUngDetail.TyLeQuyDoi, 3)
                                }
                            };
                            supply.SupplyItems.Add(supplyItem);
                        }
                    }
                    tokhai.GoodsShipment.Supplys.Add(supply);
                }


            }
            #endregion
            #region AdditionalDocumentNos Thêm chứng từ nợ

            if (tkct.ChungTuNoCollection != null && tkct.ChungTuNoCollection.Count > 0)
            {
                tokhai.AdditionalDocumentNos = new List<AdditionalDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuNo ctn in tkct.ChungTuNoCollection)
                {
                    tokhai.AdditionalDocumentNos.Add(new AdditionalDocument()
                    {
                        Type = ctn.MA_LOAI_CT,
                        Reference = ctn.SO_CT,
                        Issue = ctn.NGAY_CT.ToString(sfmtDate),
                        IssueLocation = ctn.NOI_CAP,
                        Issuer = ctn.TO_CHUC_CAP,
                        Expire = ctn.NgayHetHan.ToString(sfmtDate),
                        IsDebt = Helpers.FormatNumeric(ctn.IsNoChungTu),
                        Submit = ctn.ThoiHanNop.ToString(sfmtDate),
                        AdditionalInformation = new AdditionalInformation()
                        {
                            Content = new Content() { Text = ctn.DIENGIAI }
                        }

                    });
                }

            }
            #endregion
            return tokhai;
        }

        public static GC_GiaHanThanhKhoan ToDataTransferGiaHanThanhKhoan(GiaHanThanhKhoan giaHanThanhKhoan, HopDong HD)
        {
            bool isKhaiBaoSua = giaHanThanhKhoan.ActionStatus == (int)ActionStatus.GiaHanSua;
            GC_GiaHanThanhKhoan gc_GiaHanHD = new GC_GiaHanThanhKhoan()
            {
                Issuer = DeclarationIssuer.GC_GIA_HAN_THANH_KHOAN,
                Function = isKhaiBaoSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                Reference = giaHanThanhKhoan.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                DeclarationOffice = giaHanThanhKhoan.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? Helpers.FormatNumeric(giaHanThanhKhoan.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiBaoSua ? giaHanThanhKhoan.NgayTiepNhan.ToString(sfmtDate) : string.Empty,
                Agents = new List<Agent>(),
                Importer = new NameBase() { Identity = giaHanThanhKhoan.MaDoanhNghiep, Name = HD.TenDoanhNghiep },
                ContractReference = new ContractDocument()
                {
                    Reference = HD.SoHopDong,
                    Issue = HD.NgayTiepNhan.ToString(sfmtDate),
                    DeclarationOffice = HD.MaHaiQuan.Trim(),
                    Expire = HD.NgayHetHan.ToString(sfmtDate),
                    CustomsReference = HD.SoHopDong
                },

                AdditionalInformations = new List<AdditionalInformation>(),

            };
            gc_GiaHanHD.Agents.Add(new Agent
            {
                Name = HD.TenDoanhNghiep,
                Identity = HD.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            gc_GiaHanHD.Agents.Add(new Agent
            {
                Name = HD.TenDoanhNghiep,
                Identity = HD.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            gc_GiaHanHD.AdditionalInformations.Add(new AdditionalInformation()
            {
                ExtensionNumberDate = Helpers.FormatNumeric(giaHanThanhKhoan.SoNgayGiaHan),
                Content = new Content() { Text = giaHanThanhKhoan.LyDo }
            });
            return gc_GiaHanHD;
        }
    }
}
