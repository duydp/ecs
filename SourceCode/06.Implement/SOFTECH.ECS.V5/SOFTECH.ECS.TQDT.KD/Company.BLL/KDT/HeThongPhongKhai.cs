using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace Company.KD.BLL.KDT
{
    public partial class HeThongPhongKhai
    {
        //---------------------------------THOILV EDIT------------------------------------------------------------
        //Selected Login
        public DataSet SelectLogin()
        {
            string spQuery = "SELECT MaDoanhNghiep,PassWord,Role,Value_Config,Key_Config FROM t_HeThongPhongKhai ";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spQuery);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //Seclected Settings
        public string SelectedSettings(string maDoanhNghiep, string KeyConfig)
        {
            string strDS = "";
            try
            {
                string spQuery = "SELECT Value_Config FROM t_HeThongPhongKhai WHERE MaDoanhNghiep='" + maDoanhNghiep + "' AND Key_Config ='" + KeyConfig + "'";
                SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spQuery);
                DataSet ds = this.db.ExecuteDataSet(dbCommand);
                strDS = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            catch (Exception ex)
            {
                string strS = ex.Message;
            }
            return strDS;
        }

        public string SelectedSettingsName(string maDoanhNghiep, string KeyConfig)
        {
            string strDS = "";
            try
            {
                string spQuery = "SELECT TenDoanhNghiep FROM t_HeThongPhongKhai WHERE MaDoanhNghiep='" + maDoanhNghiep + "' AND Key_Config ='" + KeyConfig + "'";
                SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spQuery);
                DataSet ds = this.db.ExecuteDataSet(dbCommand);
                strDS = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            catch (Exception ex)
            {
                string strS = ex.Message;
            }
            return strDS;
        }

        //---PASS_ROOT---
        public string SelectedPassWord(string maDoanhNghiep, string KeyConfig)
        {
            string strDS = "";
            try
            {
                string spQuery = "SELECT PassWord FROM t_HeThongPhongKhai WHERE MaDoanhNghiep='" + maDoanhNghiep + "' AND Key_Config ='" + KeyConfig + "'";
                SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spQuery);
                DataSet ds = this.db.ExecuteDataSet(dbCommand);
                strDS = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            catch (Exception ex)
            {
                string strS = ex.Message;
            }
            return strDS;
        }
        //---------------
        public IDataReader SelectReaderByRole()
        {
            //string spName = " SELECT * FROM t_HaiQuanPhongKhai_CauHinh WHERE Role =1" ;
            string spName = "SELECT MaDoanhNghiep,[PassWord],TenDoanhNghiep,[Role]," +
                            " case [Role] when 1 then 'admin' end  as vaitro,Value_Config,Key_Config " +
                            " FROM t_HeThongPhongKhai WHERE [Role] = 1 ";

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
        public HeThongPhongKhaiCollection SelectCollectionByRole()
        {
            HeThongPhongKhaiCollection collection = new HeThongPhongKhaiCollection();

            IDataReader reader = this.SelectReaderByRole();
            while (reader.Read())
            {
                HeThongPhongKhai entity = new HeThongPhongKhai();

                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("PassWord"))) entity.PassWord = reader.GetString(reader.GetOrdinal("PassWord"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Key_Config"))) entity.Key_Config = reader.GetString(reader.GetOrdinal("Key_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Value_Config"))) entity.Value_Config = reader.GetString(reader.GetOrdinal("Value_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Role"))) entity.Role = reader.GetInt16(reader.GetOrdinal("Role"));
                if (!reader.IsDBNull(reader.GetOrdinal("vaitro"))) entity.vaitro = reader.GetString(reader.GetOrdinal("vaitro"));
                collection.Add(entity);
            }
            return collection;
        }
        //-------------------------------------
        public IDataReader SelectReaderByRoleDoanhNghiep()
        {
            //string spName = " SELECT * FROM t_HaiQuanPhongKhai_CauHinh WHERE Role =1" ;
            string spName = "SELECT MaDoanhNghiep,[PassWord],TenDoanhNghiep,[Role]," +
                            " case [Role] when 0 then 'User' end  as vaitro,Value_Config,Key_Config " +
                            " FROM t_HeThongPhongKhai WHERE [Role] = 0  And Key_Config=\'CauHinh\'";

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
        public HeThongPhongKhaiCollection SelectCollectionByRoleDoanhNghiep()
        {
            HeThongPhongKhaiCollection collection = new HeThongPhongKhaiCollection();

            IDataReader reader = this.SelectReaderByRoleDoanhNghiep();
            while (reader.Read())
            {
                HeThongPhongKhai entity = new HeThongPhongKhai();

                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("PassWord"))) entity.PassWord = reader.GetString(reader.GetOrdinal("PassWord"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Key_Config"))) entity.Key_Config = reader.GetString(reader.GetOrdinal("Key_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Value_Config"))) entity.Value_Config = reader.GetString(reader.GetOrdinal("Value_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Role"))) entity.Role = reader.GetInt16(reader.GetOrdinal("Role"));
                if (!reader.IsDBNull(reader.GetOrdinal("vaitro"))) entity.vaitro = reader.GetString(reader.GetOrdinal("vaitro"));
                collection.Add(entity);
            }
            return collection;
        }
        //
        //Dellete Method
        public int DeleteALL()
        {
            return this.DeleteTransactionALL(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransactionALL(SqlTransaction transaction)
        {
            string spName = "p_HeThongPhongKhai_DeleteALL";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
        public DataSet SelectDanhSachDoanhNghiep(int top)
        {
            string spQuery = "SELECT DISTINCT top "+top+" MaDoanhNghiep, TenDoanhNghiep FROM t_HeThongPhongKhai where role=0";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spQuery);
            return this.db.ExecuteDataSet(dbCommand);
        }
        public DataSet SelectCauHinhByMaDoanhNghiep(string maDoanhNghiep)
        {
            string spQuery = "SELECT * FROM t_HeThongPhongKhai WHERE MaDoanhNghiep = '"+ maDoanhNghiep +"'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spQuery);
            return this.db.ExecuteDataSet(dbCommand);
        }
        public int UpdateTenDoanhNghiep(string maDoanhNghiep, string tenDoanhNghiep)
        {
            string spQuery = "UPDATE t_HeThongPhongKhai SET TenDoanhNghiep = '" + tenDoanhNghiep + "' WHERE MaDoanhNghiep = '" + maDoanhNghiep + "'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spQuery);
            return this.db.ExecuteNonQuery(dbCommand);
        }
        public static int SelectedCountDN()
        {
            string strDS = "";
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                string spQuery = "SELECT distinct MaDoanhNghiep FROM t_HeThongPhongKhai where role=0";
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spQuery);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds.Tables[0].Rows.Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 0;
        }
        //------------------------------------------------------
    }
}