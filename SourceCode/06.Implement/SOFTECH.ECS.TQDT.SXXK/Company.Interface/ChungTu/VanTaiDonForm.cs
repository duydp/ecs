﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.BLL.DuLieuChuan;
using Company.BLL;

namespace Company.Interface
{
    public partial class VanTaiDonForm : Company.Interface.BaseForm
    {
        public ToKhaiMauDich TKMD;
        //public bool isKhaiBoSung = false;
        DataTable dtCuaKhau;
        public VanTaiDonForm()
        {
            InitializeComponent();

            SetEvent_TextBox_DoiTac();
        }

        private void VanTaiDonForm_Load(object sender, EventArgs e)
        {
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            //Thiet lap ngay mac dinh
            ccNgayVanDon.Text = ccNgayKhoiHanh.Text = DateTime.Today.ToShortDateString();

            if (TKMD.VanTaiDon != null)
            {
                cuaKhauControl1.Ma = TKMD.VanTaiDon.CuaKhauNhap_ID;
                txtCuaKhauXuat.Text = TKMD.VanTaiDon.CuaKhauXuat;
                cbDKGH.SelectedValue = TKMD.VanTaiDon.DKGH_ID;
                chkHangRoi.Checked = TKMD.VanTaiDon.HangRoi;
                txtMaDiaDiemDoHang.Text = TKMD.VanTaiDon.MaCangDoHang;
                txtMaDiaDiemXepHang.Text = TKMD.VanTaiDon.MaCangXepHang;
                txtMaHangVT.Text = TKMD.VanTaiDon.MaHangVT;
                txtMaNguoiGiaoHang.Text = TKMD.VanTaiDon.MaNguoiGiaoHang;
                txtMaNguoiNhanHang.Text = TKMD.VanTaiDon.MaNguoiNhanHang;
                txtMaNguoiNhanHangTG.Text = TKMD.VanTaiDon.MaNguoiNhanHangTrungGian;
                ccNgayDen.Value = TKMD.VanTaiDon.NgayDenPTVT;
                ccNgayVanDon.Value = TKMD.VanTaiDon.NgayVanDon;
                ctrNuocXuat.Ma = TKMD.VanTaiDon.NuocXuat_ID;
                ctrQuocTichPTVT.Ma = TKMD.VanTaiDon.QuocTichPTVT;
                txtSoHieuPTVT.Text = TKMD.VanTaiDon.SoHieuPTVT;
                txtSoVanDon.Text = TKMD.VanTaiDon.SoVanDon;
                txtTenDiaDiemDoHang.Text = TKMD.VanTaiDon.TenCangDoHang;
                txtTenDiaDiemXepHang.Text = TKMD.VanTaiDon.TenCangXepHang;
                txtTenHangVT.Text = TKMD.VanTaiDon.TenHangVT;
                txtTenNguoiGiaoHang.Text = TKMD.VanTaiDon.TenNguoiGiaoHang;
                txtTenNguoiNhanHang.Text = TKMD.VanTaiDon.TenNguoiNhanHang;
                txtTenNguoiNhanHangTG.Text = TKMD.VanTaiDon.TenNguoiNhanHangTrungGian;
                txtTenPTVT.Text = TKMD.VanTaiDon.TenPTVT;
                txtNoiDi.Text = TKMD.VanTaiDon.NoiDi;
                txtSoHieuChuyenDi.Text = TKMD.VanTaiDon.SoHieuChuyenDi;
                ccNgayKhoiHanh.Value = TKMD.VanTaiDon.NgayKhoiHanh;
                txtDiaDiemGiaoHang.Text = TKMD.VanTaiDon.DiaDiemGiaoHang;
                if (TKMD.VanTaiDon.ContainerCollection == null)
                    TKMD.VanTaiDon.ContainerCollection = new List<Container>();
                dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
            else
            {
                txtSoHieuPTVT.Text = TKMD.SoHieuPTVT;
                ccNgayDen.Text = TKMD.NgayDenPTVT.ToShortDateString();

                txtSoVanDon.Text = TKMD.SoVanDon;
                ccNgayVanDon.Text = TKMD.NgayVanDon.ToShortDateString();

                txtMaNguoiNhanHang.Text = TKMD.MaDoanhNghiep;
                txtTenNguoiNhanHang.Text = TKMD.TenDoanhNghiep;
                txtTenNguoiGiaoHang.Text = TKMD.TenDonViDoiTac;

                cuaKhauControl1.Ma = TKMD.CuaKhau_ID;
                txtMaDiaDiemDoHang.Text = cuaKhauControl1.Ma;
                txtTenDiaDiemDoHang.Text = cuaKhauControl1.Ten;

                txtTenDiaDiemXepHang.Text = TKMD.DiaDiemXepHang;

                cbDKGH.SelectedValue = TKMD.DKGH_ID;
                ctrNuocXuat.Ma = TKMD.NuocXK_ID;
                ctrQuocTichPTVT.Ma = TKMD.NuocXK_ID;
            }

            //if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    Ghi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    ThemContainerExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    TaoContainer.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    Xoa.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //}

            Ghi.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            ThemContainerExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            TaoContainer.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            Xoa.Enabled = Janus.Windows.UI.InheritableBoolean.True;

            //Thiết lập trạng thái các nút trên form.
            SetButtonStateVANDON(TKMD);

            //Load Cua khau
            dtCuaKhau = CuaKhau.SelectAll().Tables[0];
        }

        private void Save()
        {
            cvError.Validate();
            if (!cvError.IsValid)
            {
                return;
            }

            if (!ValidateVanDon())
                return;

            if (TKMD.VanTaiDon == null)
                TKMD.VanTaiDon = new VanDon();
            TKMD.CuaKhau_ID = TKMD.VanTaiDon.CuaKhauNhap_ID = cuaKhauControl1.Ma;
            TKMD.VanTaiDon.CuaKhauXuat = txtCuaKhauXuat.Text.Trim();
            TKMD.DKGH_ID = TKMD.VanTaiDon.DKGH_ID = cbDKGH.SelectedValue.ToString();
            TKMD.VanTaiDon.HangRoi = chkHangRoi.Checked;
            TKMD.VanTaiDon.MaCangDoHang = txtMaDiaDiemDoHang.Text.Trim();
            TKMD.VanTaiDon.MaCangXepHang = txtMaDiaDiemXepHang.Text.Trim();
            TKMD.VanTaiDon.MaHangVT = txtMaHangVT.Text.Trim();
            TKMD.VanTaiDon.MaNguoiGiaoHang = txtMaNguoiGiaoHang.Text.Trim();
            TKMD.VanTaiDon.MaNguoiNhanHang = txtMaNguoiNhanHang.Text.Trim();
            TKMD.VanTaiDon.MaNguoiNhanHangTrungGian = txtMaNguoiNhanHangTG.Text.Trim();
            TKMD.NgayDenPTVT = TKMD.VanTaiDon.NgayDenPTVT = ccNgayDen.Value;
            TKMD.NgayVanDon = TKMD.VanTaiDon.NgayVanDon = ccNgayVanDon.Value;
            TKMD.NuocXK_ID = TKMD.VanTaiDon.NuocXuat_ID = ctrNuocXuat.Ma;

            TKMD.VanTaiDon.SoHieuPTVT = txtSoHieuPTVT.Text.Trim();
            TKMD.SoVanDon = TKMD.VanTaiDon.SoVanDon = txtSoVanDon.Text.Trim();
            TKMD.VanTaiDon.TenCangDoHang = txtTenDiaDiemDoHang.Text.Trim();
            TKMD.DiaDiemXepHang = TKMD.VanTaiDon.TenCangXepHang = txtTenDiaDiemXepHang.Text.Trim();
            TKMD.VanTaiDon.TenHangVT = txtTenHangVT.Text.Trim();
            TKMD.VanTaiDon.TenNguoiGiaoHang = txtTenNguoiGiaoHang.Text.Trim();
            TKMD.VanTaiDon.TenNguoiNhanHang = txtTenNguoiNhanHang.Text.Trim();
            TKMD.VanTaiDon.TenNguoiNhanHangTrungGian = txtTenNguoiNhanHangTG.Text;
            TKMD.SoHieuPTVT = txtSoHieuPTVT.Text.Trim();
            TKMD.VanTaiDon.TenPTVT = txtTenPTVT.Text.Trim();
            TKMD.VanTaiDon.TKMD_ID = TKMD.ID;
            TKMD.VanTaiDon.DiaDiemGiaoHang = txtDiaDiemGiaoHang.Text.Trim();
            TKMD.VanTaiDon.NoiDi = txtNoiDi.Text.Trim();
            TKMD.VanTaiDon.SoHieuChuyenDi = txtSoHieuChuyenDi.Text.Trim();
            TKMD.VanTaiDon.NgayKhoiHanh = ccNgayKhoiHanh.Value;
            if (TKMD.VanTaiDon.ContainerCollection != null && TKMD.VanTaiDon.ContainerCollection.Count > 0)
            {
                TKMD.SoContainer20 = 0;
                TKMD.SoContainer40 = 0;
                foreach (Container c in TKMD.VanTaiDon.ContainerCollection)
                {
                    if (c.LoaiContainer == "2")
                        TKMD.SoContainer20++;
                    else if (c.LoaiContainer == "4")
                        TKMD.SoContainer40++;
                }
            }
            if (chkQuocTich.Checked)
                TKMD.VanTaiDon.QuocTichPTVT = "JP";
            else
                TKMD.VanTaiDon.QuocTichPTVT = ctrQuocTichPTVT.Ma;
            this.Close();
        }

        private void XoaContainer()
        {
            List<Container> ContainerCollection = new List<Container>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa Container này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        Container container = (Container)i.GetRow().DataRow;
                        ContainerCollection.Add(container);
                    }
                }
                foreach (Container c in ContainerCollection)
                {
                    try
                    {
                        if (c.ID > 0)
                        {
                            c.Delete();
                        }
                        TKMD.VanTaiDon.ContainerCollection.Remove(c);
                    }
                    catch { }
                }
            }
            dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void AddContainer()
        {
            AddContainerForm f = new AddContainerForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
            if (TKMD.VanTaiDon != null)
            {
                dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
        }
        private void AddContainerExcel()
        {

        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "Ghi": Save();
                    break;
                case "Xoa": XoaContainer();
                    break;
                case "TaoContainer": AddContainer();
                    break;
                case "ThemContainerExcel": AddContainerExcel();
                    break;
                case "XoaVD": XoaVanDon();
                    break;

            }
        }

        private void XoaVanDon()
        {
            try
            {
                if (TKMD.VanTaiDon == null) return;
                else if (TKMD.VanTaiDon.ID == 0)
                {
                    TKMD.VanTaiDon = null;
                }
                else
                if (ShowMessage("Bạn có muốn xóa vận đơn này không?", true) == "Yes")
                {
                    //Xoa container
                    foreach (Container c in TKMD.VanTaiDon.ContainerCollection)
                    {
                        try
                        {
                            if (c.ID > 0)
                            {
                                c.Delete();
                            }
                            TKMD.VanTaiDon.ContainerCollection.Remove(c);
                        }
                        catch (Exception ex) { throw ex; }
                    }

                    //Xoa van don
                    TKMD.VanTaiDon.Delete();
                    TKMD.VanTaiDon = null;

                    ShowMessage("Xóa vận đơn thành công.", false);

                    
                }
                this.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); ShowMessage(ex.Message, false, true, ex.StackTrace); }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            List<Container> ContainerCollection = new List<Container>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa Container này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        Container ContainerTMP = (Container)i.GetRow().DataRow;
                        ContainerCollection.Add(ContainerTMP);
                    }
                }
                foreach (Container item in ContainerCollection)
                {
                    try
                    {
                        if (item.ID > 0)
                        {
                            item.Delete();
                        }
                        TKMD.VanTaiDon.ContainerCollection.Remove(item);
                    }
                    catch { }
                }
                dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
            else
                e.Cancel = true;

        }

        private void chkQuocTich_CheckedChanged(object sender, EventArgs e)
        {
            if (chkQuocTich.Checked)
                ctrQuocTichPTVT.Enabled = false;
            else
                ctrQuocTichPTVT.Enabled = true;
        }

        private void txtSoHieuPTVT_Enter(object sender, EventArgs e)
        {
            txtSoHieuChuyenDi.Text = txtSoHieuPTVT.Text;
        }

        #region Begin Doi tac TextBox

        /// <summary>
        /// Tạo sự kiện ButtonClick, Leave cho các TextBox Mã đơn vị mua, bán.
        /// </summary>
        /// Hungtq, Update 30052010
        private void SetEvent_TextBox_DoiTac()
        {
            txtMaNguoiNhanHang.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            txtMaNguoiNhanHangTG.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            txtMaNguoiGiaoHang.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;

            txtMaNguoiNhanHang.ButtonClick += new EventHandler(txtMaNguoiNhanHang_ButtonClick);

            txtMaNguoiNhanHangTG.ButtonClick += new EventHandler(txtMaNguoiNhanHangTG_ButtonClick);

            txtMaNguoiGiaoHang.ButtonClick += new EventHandler(txtMaNguoiGiaoHang_ButtonClick);

            txtMaNguoiNhanHang.Leave += new EventHandler(txtMaNguoiNhanHang_Leave);

            txtMaNguoiNhanHangTG.Leave += new EventHandler(txtMaNguoiNhanHangTG_Leave);

            txtMaNguoiGiaoHang.Leave += new EventHandler(txtMaNguoiGiaoHang_Leave);

            txtSoHieuPTVT.TextChanged += new EventHandler(txtSoHieuPTVT_TextChanged);
        }

        void txtSoHieuPTVT_TextChanged(object sender, EventArgs e)
        {
            //if (txtSoHieuPTVT.Text.Trim().Length != 0)
            //    txtTenPTVT.Text = txtMaHangVT.Text = txtTenHangVT.Text = txtNoiDi.Text = txtSoHieuChuyenDi.Text = txtSoHieuPTVT.Text;
        }

        void txtMaNguoiGiaoHang_Leave(object sender, EventArgs e)
        {
            if (txtMaNguoiGiaoHang.Text.Trim().Length != 0)
                txtTenNguoiGiaoHang.Text = DoiTac.GetName(txtMaNguoiGiaoHang.Text.Trim());
        }

        void txtMaNguoiNhanHangTG_Leave(object sender, EventArgs e)
        {
            if (txtMaNguoiNhanHangTG.Text.Trim().Length != 0)
                txtTenNguoiNhanHangTG.Text = DoiTac.GetName(txtMaNguoiNhanHangTG.Text.Trim());
        }

        void txtMaNguoiNhanHang_Leave(object sender, EventArgs e)
        {
            if (txtMaNguoiNhanHang.Text.Trim().Length != 0)
                txtTenNguoiNhanHang.Text = DoiTac.GetName(txtMaNguoiNhanHang.Text.Trim());
        }

        void txtMaNguoiGiaoHang_ButtonClick(object sender, EventArgs e)
        {
            Company.BLL.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaNguoiGiaoHang.Text = objDoiTac.MaCongTy;
                txtTenNguoiGiaoHang.Text = objDoiTac.TenCongTy;
            }
        }

        void txtMaNguoiNhanHangTG_ButtonClick(object sender, EventArgs e)
        {
            Company.BLL.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaNguoiNhanHangTG.Text = objDoiTac.MaCongTy;
                txtTenNguoiNhanHangTG.Text = objDoiTac.TenCongTy;
            }
        }

        void txtMaNguoiNhanHang_ButtonClick(object sender, EventArgs e)
        {
            Company.BLL.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaNguoiNhanHang.Text = objDoiTac.MaCongTy;
                txtTenNguoiNhanHang.Text = objDoiTac.TenCongTy;
            }
        }

        #endregion End Doi tac TextBox

        #region Begin VALIDATE VAN DON

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateVanDon()
        {
            bool isValid = true;

            try
            {
                //So_Chung_Tu(35)
                isValid = Globals.ValidateLength(txtSoVanDon, 35, err, "Số vận đơn");

                //Ma_PTVT	char(3)

                //So_Hieu_PTVT	varchar(25)
                isValid &= Globals.ValidateLength(txtSoHieuPTVT, 25, err, "Số hiệu phương tiện vận tải");

                //Ten_PTVT varchar(255)
                isValid &= Globals.ValidateLength(txtTenPTVT, 255, err, "Tên phương tiện vận tải");

                //Ma_Hang_Van_Tai	varchar(17)
                isValid &= Globals.ValidateLength(txtMaHangVT, 17, err, "Mã hãng vận tải");

                //Ten_Hang_Van_Tai	varchar(35)
                isValid &= Globals.ValidateLength(txtTenHangVT, 35, err, "Tên hãng vận tải");

                //Ma_Nguoi_Nhan_Hang	varchar(17)
                isValid &= Globals.ValidateLength(txtMaNguoiNhanHang, 17, err, "Mã người nhận hàng");

                //Ma_Nguoi_Giao_Hang	varchar(17)
                isValid &= Globals.ValidateLength(txtMaNguoiGiaoHang, 17, err, "Mã người giao hàng");

                //Ma_Nguoi_Nhan_Hang_TG	varchar(17)
                isValid &= Globals.ValidateLength(txtMaNguoiNhanHangTG, 17, err, "Mã người nhận hàng trung gian");

                //Ma_DKGH	varchar(7)
                isValid &= Globals.ValidateLength(cbDKGH, 7, err, "Điều kiện giao hàng");

                //Ma_Cang_Do_Hang	nvarchar(50)
                isValid &= Globals.ValidateLength(txtMaDiaDiemDoHang, 50, err, "Mã địa điểm dỡ hàng");

                //Ma_Cang_Xep_Hang	varchar(11)
                isValid &= Globals.ValidateLength(txtMaDiaDiemXepHang, 11, err, "Mã địa điểm xếp hàng");

                //Ten_Cang_Xep_Hang	varchar(40)
                isValid &= Globals.ValidateLength(txtTenDiaDiemXepHang, 40, err, "Tên địa điểm xếp hàng");

                //Dia Diem Giao hang	varchar(60)
                isValid &= Globals.ValidateLength(txtDiaDiemGiaoHang, 60, err, "Địa điểm giao hàng");
            }
            catch (Exception ex) { }

            return isValid;
        }

        #endregion End VALIDATE VAN DON

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form Vận đơn.
        /// </summary>
        /// <param name="tkmd"></param>
        private void SetButtonStateVANDON(BLL.KDT.ToKhaiMauDich tkmd)
        {
            //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
            bool status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO
                || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET);

            Janus.Windows.UI.InheritableBoolean janusStatus = status == true ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;

            uiCommandBar1.Enabled = status;

            Ghi.Enabled = janusStatus;
            ThemContainerExcel.Enabled = janusStatus;
            TaoContainer.Enabled = janusStatus;
            Xoa.Enabled = janusStatus;
        }

        #endregion

        private void label27_Click(object sender, EventArgs e)
        {
            //test
        }
        private void cuaKhauControl1_Leave(object sender, System.EventArgs e)
        {
            //Mã đđ dỡ hàng đã nhập có trong danh sách cửa khẩu thì thay đổi theo cửa khẩu nhập, nếu ko có thì ko thay đổi
            if (this.dtCuaKhau.Select("ID='" + txtMaDiaDiemDoHang.Text.Trim() + "'").Length > 0)
            {
                txtMaDiaDiemDoHang.Text = cuaKhauControl1.Ma;
                if (this.dtCuaKhau.Select("ID='" + cuaKhauControl1.Ma.Trim() + "'").Length > 0)
                {
                    txtTenDiaDiemDoHang.Text = dtCuaKhau.Select("ID='" + cuaKhauControl1.Ma.Trim() + "'")[0][1].ToString();

                }
            }
        }

    }
}

