﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_QuyetToan_SanPham : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HopDong_ID { set; get; }
		public string MaSP { set; get; }
		public string TenSP { set; get; }
		public string MaHS { set; get; }
		public string DVT_ID { set; get; }
		public decimal LuongXuatDK { set; get; }
		public decimal TriGiaXuatDK { set; get; }
		public decimal LuongXuatTK { set; get; }
		public decimal TriGiaXuatTK { set; get; }
		public decimal LuongXuatNK { set; get; }
		public decimal TriGiaXuatNK { set; get; }
		public decimal LuongTonCK { set; get; }
		public decimal TriGiaTonCK { set; get; }
		public string NamQT { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_QuyetToan_SanPham> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_QuyetToan_SanPham> collection = new List<KDT_GC_QuyetToan_SanPham>();
			while (reader.Read())
			{
				KDT_GC_QuyetToan_SanPham entity = new KDT_GC_QuyetToan_SanPham();
				//if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSP"))) entity.TenSP = reader.GetString(reader.GetOrdinal("TenSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongXuatDK"))) entity.LuongXuatDK = reader.GetDecimal(reader.GetOrdinal("LuongXuatDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaXuatDK"))) entity.TriGiaXuatDK = reader.GetDecimal(reader.GetOrdinal("TriGiaXuatDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongXuatTK"))) entity.LuongXuatTK = reader.GetDecimal(reader.GetOrdinal("LuongXuatTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaXuatTK"))) entity.TriGiaXuatTK = reader.GetDecimal(reader.GetOrdinal("TriGiaXuatTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongXuatNK"))) entity.LuongXuatNK = reader.GetDecimal(reader.GetOrdinal("LuongXuatNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaXuatNK"))) entity.TriGiaXuatNK = reader.GetDecimal(reader.GetOrdinal("TriGiaXuatNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCK"))) entity.LuongTonCK = reader.GetDecimal(reader.GetOrdinal("LuongTonCK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTonCK"))) entity.TriGiaTonCK = reader.GetDecimal(reader.GetOrdinal("TriGiaTonCK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamQT"))) entity.NamQT = reader.GetString(reader.GetOrdinal("NamQT"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_QuyetToan_SanPham> collection, long id)
        {
            foreach (KDT_GC_QuyetToan_SanPham item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_QuyetToan_SanPham VALUES(@HopDong_ID, @MaSP, @TenSP, @MaHS, @DVT_ID, @LuongXuatDK, @TriGiaXuatDK, @LuongXuatTK, @TriGiaXuatTK, @LuongXuatNK, @TriGiaXuatNK, @LuongTonCK, @TriGiaTonCK, @NamQT)";
            string update = "UPDATE t_KDT_GC_QuyetToan_SanPham SET HopDong_ID = @HopDong_ID, MaSP = @MaSP, TenSP = @TenSP, MaHS = @MaHS, DVT_ID = @DVT_ID, LuongXuatDK = @LuongXuatDK, TriGiaXuatDK = @TriGiaXuatDK, LuongXuatTK = @LuongXuatTK, TriGiaXuatTK = @TriGiaXuatTK, LuongXuatNK = @LuongXuatNK, TriGiaXuatNK = @TriGiaXuatNK, LuongTonCK = @LuongTonCK, TriGiaTonCK = @TriGiaTonCK, NamQT = @NamQT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_QuyetToan_SanPham WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSP", SqlDbType.NVarChar, "TenSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuatDK", SqlDbType.Decimal, "LuongXuatDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaXuatDK", SqlDbType.Decimal, "TriGiaXuatDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuatTK", SqlDbType.Decimal, "LuongXuatTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaXuatTK", SqlDbType.Decimal, "TriGiaXuatTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuatNK", SqlDbType.Decimal, "LuongXuatNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaXuatNK", SqlDbType.Decimal, "TriGiaXuatNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSP", SqlDbType.NVarChar, "TenSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuatDK", SqlDbType.Decimal, "LuongXuatDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaXuatDK", SqlDbType.Decimal, "TriGiaXuatDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuatTK", SqlDbType.Decimal, "LuongXuatTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaXuatTK", SqlDbType.Decimal, "TriGiaXuatTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuatNK", SqlDbType.Decimal, "LuongXuatNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaXuatNK", SqlDbType.Decimal, "TriGiaXuatNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_QuyetToan_SanPham VALUES(@HopDong_ID, @MaSP, @TenSP, @MaHS, @DVT_ID, @LuongXuatDK, @TriGiaXuatDK, @LuongXuatTK, @TriGiaXuatTK, @LuongXuatNK, @TriGiaXuatNK, @LuongTonCK, @TriGiaTonCK, @NamQT)";
            string update = "UPDATE t_KDT_GC_QuyetToan_SanPham SET HopDong_ID = @HopDong_ID, MaSP = @MaSP, TenSP = @TenSP, MaHS = @MaHS, DVT_ID = @DVT_ID, LuongXuatDK = @LuongXuatDK, TriGiaXuatDK = @TriGiaXuatDK, LuongXuatTK = @LuongXuatTK, TriGiaXuatTK = @TriGiaXuatTK, LuongXuatNK = @LuongXuatNK, TriGiaXuatNK = @TriGiaXuatNK, LuongTonCK = @LuongTonCK, TriGiaTonCK = @TriGiaTonCK, NamQT = @NamQT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_QuyetToan_SanPham WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSP", SqlDbType.NVarChar, "TenSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuatDK", SqlDbType.Decimal, "LuongXuatDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaXuatDK", SqlDbType.Decimal, "TriGiaXuatDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuatTK", SqlDbType.Decimal, "LuongXuatTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaXuatTK", SqlDbType.Decimal, "TriGiaXuatTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuatNK", SqlDbType.Decimal, "LuongXuatNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaXuatNK", SqlDbType.Decimal, "TriGiaXuatNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSP", SqlDbType.NVarChar, "TenSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuatDK", SqlDbType.Decimal, "LuongXuatDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaXuatDK", SqlDbType.Decimal, "TriGiaXuatDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuatTK", SqlDbType.Decimal, "LuongXuatTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaXuatTK", SqlDbType.Decimal, "TriGiaXuatTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuatNK", SqlDbType.Decimal, "LuongXuatNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaXuatNK", SqlDbType.Decimal, "TriGiaXuatNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_QuyetToan_SanPham Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_SanPham_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_QuyetToan_SanPham> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_QuyetToan_SanPham> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_QuyetToan_SanPham> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_SanPham_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_SanPham_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_SanPham_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_SanPham_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
        public List<KDT_GC_QuyetToan_SanPham> SelectCollectionDynamicByHD_ID(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamicByHD_ID(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }
        public static IDataReader SelectReaderDynamicByHD_ID(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_SanPham_SelectDynamicByHD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_QuyetToan_SanPham(long hopDong_ID, string maSP, string tenSP, string maHS, string dVT_ID, decimal luongXuatDK, decimal triGiaXuatDK, decimal luongXuatTK, decimal triGiaXuatTK, decimal luongXuatNK, decimal triGiaXuatNK, decimal luongTonCK, decimal triGiaTonCK, string namQT)
		{
			KDT_GC_QuyetToan_SanPham entity = new KDT_GC_QuyetToan_SanPham();	
			entity.HopDong_ID = hopDong_ID;
			entity.MaSP = maSP;
			entity.TenSP = tenSP;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongXuatDK = luongXuatDK;
			entity.TriGiaXuatDK = triGiaXuatDK;
			entity.LuongXuatTK = luongXuatTK;
			entity.TriGiaXuatTK = triGiaXuatTK;
			entity.LuongXuatNK = luongXuatNK;
			entity.TriGiaXuatNK = triGiaXuatNK;
			entity.LuongTonCK = luongTonCK;
			entity.TriGiaTonCK = triGiaTonCK;
			entity.NamQT = namQT;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_SanPham_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, TenSP);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongXuatDK", SqlDbType.Decimal, LuongXuatDK);
			db.AddInParameter(dbCommand, "@TriGiaXuatDK", SqlDbType.Decimal, TriGiaXuatDK);
			db.AddInParameter(dbCommand, "@LuongXuatTK", SqlDbType.Decimal, LuongXuatTK);
			db.AddInParameter(dbCommand, "@TriGiaXuatTK", SqlDbType.Decimal, TriGiaXuatTK);
			db.AddInParameter(dbCommand, "@LuongXuatNK", SqlDbType.Decimal, LuongXuatNK);
			db.AddInParameter(dbCommand, "@TriGiaXuatNK", SqlDbType.Decimal, TriGiaXuatNK);
			db.AddInParameter(dbCommand, "@LuongTonCK", SqlDbType.Decimal, LuongTonCK);
			db.AddInParameter(dbCommand, "@TriGiaTonCK", SqlDbType.Decimal, TriGiaTonCK);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.NVarChar, NamQT);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_QuyetToan_SanPham> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_SanPham item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_QuyetToan_SanPham(long id, long hopDong_ID, string maSP, string tenSP, string maHS, string dVT_ID, decimal luongXuatDK, decimal triGiaXuatDK, decimal luongXuatTK, decimal triGiaXuatTK, decimal luongXuatNK, decimal triGiaXuatNK, decimal luongTonCK, decimal triGiaTonCK, string namQT)
		{
			KDT_GC_QuyetToan_SanPham entity = new KDT_GC_QuyetToan_SanPham();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.MaSP = maSP;
			entity.TenSP = tenSP;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongXuatDK = luongXuatDK;
			entity.TriGiaXuatDK = triGiaXuatDK;
			entity.LuongXuatTK = luongXuatTK;
			entity.TriGiaXuatTK = triGiaXuatTK;
			entity.LuongXuatNK = luongXuatNK;
			entity.TriGiaXuatNK = triGiaXuatNK;
			entity.LuongTonCK = luongTonCK;
			entity.TriGiaTonCK = triGiaTonCK;
			entity.NamQT = namQT;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_QuyetToan_SanPham_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, TenSP);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongXuatDK", SqlDbType.Decimal, LuongXuatDK);
			db.AddInParameter(dbCommand, "@TriGiaXuatDK", SqlDbType.Decimal, TriGiaXuatDK);
			db.AddInParameter(dbCommand, "@LuongXuatTK", SqlDbType.Decimal, LuongXuatTK);
			db.AddInParameter(dbCommand, "@TriGiaXuatTK", SqlDbType.Decimal, TriGiaXuatTK);
			db.AddInParameter(dbCommand, "@LuongXuatNK", SqlDbType.Decimal, LuongXuatNK);
			db.AddInParameter(dbCommand, "@TriGiaXuatNK", SqlDbType.Decimal, TriGiaXuatNK);
			db.AddInParameter(dbCommand, "@LuongTonCK", SqlDbType.Decimal, LuongTonCK);
			db.AddInParameter(dbCommand, "@TriGiaTonCK", SqlDbType.Decimal, TriGiaTonCK);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.NVarChar, NamQT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_QuyetToan_SanPham> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_SanPham item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_QuyetToan_SanPham(long id, long hopDong_ID, string maSP, string tenSP, string maHS, string dVT_ID, decimal luongXuatDK, decimal triGiaXuatDK, decimal luongXuatTK, decimal triGiaXuatTK, decimal luongXuatNK, decimal triGiaXuatNK, decimal luongTonCK, decimal triGiaTonCK, string namQT)
		{
			KDT_GC_QuyetToan_SanPham entity = new KDT_GC_QuyetToan_SanPham();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.MaSP = maSP;
			entity.TenSP = tenSP;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongXuatDK = luongXuatDK;
			entity.TriGiaXuatDK = triGiaXuatDK;
			entity.LuongXuatTK = luongXuatTK;
			entity.TriGiaXuatTK = triGiaXuatTK;
			entity.LuongXuatNK = luongXuatNK;
			entity.TriGiaXuatNK = triGiaXuatNK;
			entity.LuongTonCK = luongTonCK;
			entity.TriGiaTonCK = triGiaTonCK;
			entity.NamQT = namQT;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_SanPham_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, TenSP);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongXuatDK", SqlDbType.Decimal, LuongXuatDK);
			db.AddInParameter(dbCommand, "@TriGiaXuatDK", SqlDbType.Decimal, TriGiaXuatDK);
			db.AddInParameter(dbCommand, "@LuongXuatTK", SqlDbType.Decimal, LuongXuatTK);
			db.AddInParameter(dbCommand, "@TriGiaXuatTK", SqlDbType.Decimal, TriGiaXuatTK);
			db.AddInParameter(dbCommand, "@LuongXuatNK", SqlDbType.Decimal, LuongXuatNK);
			db.AddInParameter(dbCommand, "@TriGiaXuatNK", SqlDbType.Decimal, TriGiaXuatNK);
			db.AddInParameter(dbCommand, "@LuongTonCK", SqlDbType.Decimal, LuongTonCK);
			db.AddInParameter(dbCommand, "@TriGiaTonCK", SqlDbType.Decimal, TriGiaTonCK);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.NVarChar, NamQT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_QuyetToan_SanPham> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_SanPham item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_QuyetToan_SanPham(long id)
		{
			KDT_GC_QuyetToan_SanPham entity = new KDT_GC_QuyetToan_SanPham();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_SanPham_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public  int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_SanPham_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_QuyetToan_SanPham> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_SanPham item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}