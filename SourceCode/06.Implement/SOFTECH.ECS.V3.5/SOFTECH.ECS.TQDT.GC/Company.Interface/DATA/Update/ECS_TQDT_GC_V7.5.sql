﻿
delete from [dbo].[t_HaiQuan_LoaiCO]

-- Add 8 rows to [dbo].[t_HaiQuan_LoaiCO]
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('861', N'C/O Mẫu Khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('AI', N'C/O Mẫu AI')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('AJ', N'C/O Mẫu AJ')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('AK', N'C/O Mẫu AK')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('D', N'C/O Mẫu D')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('E', N'C/O Mẫu E')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('JV', N'C/O Mẫu JV')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('S', N'C/O Mẫu S')

GO

UPDATE [dbo].[t_HaiQuan_Version]
SET [Version] = '7.5'
  ,[Date] = getdate()
  ,[Notes] = ''