﻿SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO

UPDATE dbo.t_HaiQuan_CuaKhau SET Ten = N'ICD Phước Long 1' WHERE id = 'I001'

go
UPDATE dbo.t_HaiQuan_CuaKhau SET Ten = N'ICD Phước Long 3' WHERE id = 'I002'

go
UPDATE dbo.t_HaiQuan_CuaKhau SET Ten = N'ICD Phúc Long' WHERE id = 'I011'

go
UPDATE dbo.t_HaiQuan_CuaKhau SET Ten = N'ICD Tanamexco' WHERE id = 'I007'

go
DELETE FROM dbo.t_HaiQuan_CuaKhau WHERE id = 'I017'

go
INSERT INTO dbo.t_HaiQuan_CuaKhau(ID, Ten, MaCuc) VALUES ('I017', N'ICD Sotran', '02')

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='2.2', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('2.2', getdate(), null)
	end 