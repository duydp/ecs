
/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]    Script Date: 08/08/2014 11:27:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]    Script Date: 08/08/2014 11:27:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
----------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]  
 @MaDoanhNghiep NVarchar(14),  
 @LanThanhLy int  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
--SELECT  
-- SoToKhaiNhap,  
-- NgayDangKyNhap,  
-- MaNPL,  
-- TenNPL,   
-- LuongTonDau,  
-- CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end as LuongNhap,   
-- Sum(LuongNPLSuDung) as LuongNPLSuDung,   
-- Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,   
-- 0 as LuongNopThue,  
-- 0 as LuongXuatTheoHD,  
-- --case   
-- -- when (LuongTonDau - Sum(LuongNPLSuDung)) >= 0   
-- -- then  LuongTonDau - Sum(LuongNPLSuDung)  
-- -- else 0  
-- -- end   
-- -- as LuongTonCuoi,  
-- Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,  
-- (select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL,  
-- DonGiaTT  
    
--FROM  
-- [dbo].[t_KDT_SXXK_BCXuatNhapTon]  
--WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy  
--GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTonDau,LuongNhap  
--order by MaNPL,SoToKhaiNhap,NgayDangKyNhap  
  
Select   
 MaNPL,  
 MAX(TenNPL) as TenNPL,   
 SUM(LuongTonDau) as LuongTonDau,  
 Sum(LuongNhap) as LuongNhap,  
 Sum(LuongNPLSuDung) as LuongNPLSuDung,  
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,  
 Sum(LuongNopThue) as LuongNopThue,  
 Sum(LuongXuatTheoHD) as LuongXuatTheoHD,  
 Sum(LuongTonCuoi) as LuongTonCuoi,  
 B.DVT_ID --MAX(ID_DVT_NPL) as ID_DVT_NPL,  

 From   
(   
SELECT  
 SoToKhaiNhap,  
 NgayDangKyNhap,  
 MaNPL,  
 TenNPL,   
 LuongTonDau,  
 CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end as LuongNhap,   
 Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung,   
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,   
 0 as LuongNopThue,  
 0 as LuongXuatTheoHD,  
 --case   
 -- when (LuongTonDau - Sum(LuongNPLSuDung)) >= 0   
 -- then  LuongTonDau - Sum(LuongNPLSuDung)  
 -- else 0  
 -- end   
 -- as LuongTonCuoi,  
 Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,  
 (select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL  

    
FROM  
 [dbo].[t_KDT_SXXK_BCXuatNhapTon]  
WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy  
GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTonDau,LuongNhap  
) as A inner join t_SXXK_NguyenPhuLieu B  
on A.MaNPL = B.Ma  AND B.MaDoanhNghiep = @MaDoanhNghiep
  
Group by MaNPL,B.DVT_ID  
order by MaNPL  
  

GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.9',GETDATE(), N' Cập nhật store BC07 cua DNCX')
END	


