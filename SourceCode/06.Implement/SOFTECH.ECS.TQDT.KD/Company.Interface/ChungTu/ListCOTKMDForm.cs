﻿using System;
using System.Drawing;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL;
using Company.KD.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ListCOTKMDForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;             
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListCOTKMDForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["LoaiCO"].Text = this.LoaiCO_GetName(e.Row.Cells["LoaiCO"].Value.ToString());
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKMD.COCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            //if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    btnTaoMoi.Enabled = false;
            //    btnXoa.Enabled = false;
            //}
        }

  
      

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<CO> listCO = new List<CO>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        CO co = (CO)i.GetRow().DataRow;                       
                        listCO.Add(co);
                    }
                }
            }

            foreach (CO co in listCO)
            {
                co.Delete();
                TKMD.COCollection.Remove(co);
            }


            dgList.DataSource = TKMD.COCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

      
        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            CoForm f = new CoForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKMD.COCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            CoForm f = new CoForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.Co = (CO)e.Row.DataRow;
          
            f.ShowDialog();
            dgList.DataSource = TKMD.COCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

     


    }
}
