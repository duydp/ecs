namespace Company.Interface.Report.VNACCS
{
    partial class BanXacNhanNoiDungKhaiSuaDoiBoSung_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblGhiChuSauKhaiBao = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGhiChuTruocKhaiBao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTienTeSauKhaiBao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaHoiDoaiSauKhaiBao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoNgayAnHanVAT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoNgayAnHan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoQuanLyNoiBo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiNopThue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaXacDinhThoiHanNop = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNhanVienHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDaiLyHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDaiLyHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChi = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBuuChinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNguoiKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNguoiKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMaLyDoKhaiBS = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHetHieuLuc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMaTienTeTruocKhaiBao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaHoiDoaiTruocKhaiBao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.lblPhanLoaiXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lblNhomXuLyHoSo = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDauHieuBaoQua = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhaiXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioDKKhaiBoSung = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThoiHanTaiNhapXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayToKhaiXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDKKhaiBoSung = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayCapPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCoQuanNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhaiBoSung = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoTienTangGiamThueXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaKhoanMucTiepNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuThiSoTienTangGiamThueXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTienTeThueXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenKhoanMucTiepNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.lblMaTienTeThueVaThuKhac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuThiSoTienTangGiamThueVaThuKhac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoTienTangGiamThueVaThuKhac = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.lblTongSoDongHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblGhiChuSauKhaiBao,
            this.lblGhiChuTruocKhaiBao,
            this.xrLabel61,
            this.lblMaTienTeSauKhaiBao,
            this.xrLabel59,
            this.lblTyGiaHoiDoaiSauKhaiBao,
            this.xrLabel54,
            this.xrLabel53,
            this.xrLabel52,
            this.lblSoNgayAnHanVAT,
            this.xrLabel50,
            this.lblSoNgayAnHan,
            this.xrLabel48,
            this.lblSoQuanLyNoiBo,
            this.xrLabel45,
            this.lblPhanLoaiNopThue,
            this.xrLabel43,
            this.lblMaXacDinhThoiHanNop,
            this.xrLabel40,
            this.xrLabel39,
            this.xrLabel38,
            this.lblMaNhanVienHQ,
            this.xrLabel36,
            this.lblTenDaiLyHQ,
            this.lblMaDaiLyHQ,
            this.xrLabel33,
            this.lblSoDT,
            this.lblDiaChi,
            this.lblMaBuuChinh,
            this.lblTenNguoiKhai,
            this.xrLabel27,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel24,
            this.xrLabel23,
            this.xrLabel22,
            this.lblMaNguoiKhai,
            this.xrLine2,
            this.xrLine3,
            this.lblMaLyDoKhaiBS,
            this.lblNgayHetHieuLuc,
            this.xrLine4,
            this.lblMaTienTeTruocKhaiBao,
            this.xrLabel56,
            this.lblTyGiaHoiDoaiTruocKhaiBao,
            this.xrLine5});
            this.Detail.HeightF = 217.7083F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGhiChuSauKhaiBao
            // 
            this.lblGhiChuSauKhaiBao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblGhiChuSauKhaiBao.LocationFloat = new DevExpress.Utils.PointFloat(256.2565F, 186.8335F);
            this.lblGhiChuSauKhaiBao.Multiline = true;
            this.lblGhiChuSauKhaiBao.Name = "lblGhiChuSauKhaiBao";
            this.lblGhiChuSauKhaiBao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGhiChuSauKhaiBao.SizeF = new System.Drawing.SizeF(423.9567F, 26.99998F);
            this.lblGhiChuSauKhaiBao.StylePriority.UseFont = false;
            this.lblGhiChuSauKhaiBao.StylePriority.UseTextAlignment = false;
            this.lblGhiChuSauKhaiBao.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblGhiChuSauKhaiBao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGhiChuTruocKhaiBao
            // 
            this.lblGhiChuTruocKhaiBao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblGhiChuTruocKhaiBao.LocationFloat = new DevExpress.Utils.PointFloat(256.2532F, 160.8334F);
            this.lblGhiChuTruocKhaiBao.Multiline = true;
            this.lblGhiChuTruocKhaiBao.Name = "lblGhiChuTruocKhaiBao";
            this.lblGhiChuTruocKhaiBao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGhiChuTruocKhaiBao.SizeF = new System.Drawing.SizeF(423.96F, 26F);
            this.lblGhiChuTruocKhaiBao.StylePriority.UseFont = false;
            this.lblGhiChuTruocKhaiBao.StylePriority.UseTextAlignment = false;
            this.lblGhiChuTruocKhaiBao.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblGhiChuTruocKhaiBao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel61
            // 
            this.xrLabel61.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(256.2532F, 150.8334F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(423.9567F, 10.00003F);
            this.xrLabel61.StylePriority.UseFont = false;
            this.xrLabel61.StylePriority.UseTextAlignment = false;
            this.xrLabel61.Text = "Phần ghi chú";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaTienTeSauKhaiBao
            // 
            this.lblMaTienTeSauKhaiBao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaTienTeSauKhaiBao.LocationFloat = new DevExpress.Utils.PointFloat(120.8333F, 187.375F);
            this.lblMaTienTeSauKhaiBao.Name = "lblMaTienTeSauKhaiBao";
            this.lblMaTienTeSauKhaiBao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTienTeSauKhaiBao.SizeF = new System.Drawing.SizeF(34.375F, 10F);
            this.lblMaTienTeSauKhaiBao.StylePriority.UseFont = false;
            this.lblMaTienTeSauKhaiBao.StylePriority.UseTextAlignment = false;
            this.lblMaTienTeSauKhaiBao.Text = "XXE";
            this.lblMaTienTeSauKhaiBao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(155.2083F, 187.375F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(15.625F, 10F);
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = "-";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTyGiaHoiDoaiSauKhaiBao
            // 
            this.lblTyGiaHoiDoaiSauKhaiBao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTyGiaHoiDoaiSauKhaiBao.LocationFloat = new DevExpress.Utils.PointFloat(170.8333F, 187.375F);
            this.lblTyGiaHoiDoaiSauKhaiBao.Name = "lblTyGiaHoiDoaiSauKhaiBao";
            this.lblTyGiaHoiDoaiSauKhaiBao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaHoiDoaiSauKhaiBao.SizeF = new System.Drawing.SizeF(85.41832F, 10.00002F);
            this.lblTyGiaHoiDoaiSauKhaiBao.StylePriority.UseFont = false;
            this.lblTyGiaHoiDoaiSauKhaiBao.StylePriority.UseTextAlignment = false;
            this.lblTyGiaHoiDoaiSauKhaiBao.Text = "XXXXXXXXE";
            this.lblTyGiaHoiDoaiSauKhaiBao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(120.8333F, 150.8334F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(120.8333F, 10.00002F);
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "Tỷ giá tính thuế";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(0F, 187.375F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(120.8333F, 10.00002F);
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.Text = "Sau khi khai báo";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(3.973643E-05F, 160.8334F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(120.8333F, 10.00002F);
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "Trước khi khai báo";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoNgayAnHanVAT
            // 
            this.lblSoNgayAnHanVAT.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoNgayAnHanVAT.LocationFloat = new DevExpress.Utils.PointFloat(627.085F, 128.0001F);
            this.lblSoNgayAnHanVAT.Name = "lblSoNgayAnHanVAT";
            this.lblSoNgayAnHanVAT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoNgayAnHanVAT.SizeF = new System.Drawing.SizeF(53.125F, 10F);
            this.lblSoNgayAnHanVAT.StylePriority.UseFont = false;
            this.lblSoNgayAnHanVAT.StylePriority.UseTextAlignment = false;
            this.lblSoNgayAnHanVAT.Text = "NNE";
            this.lblSoNgayAnHanVAT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(560.42F, 128.0001F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(66.66486F, 10.00002F);
            this.xrLabel50.StylePriority.UseFont = false;
            this.xrLabel50.StylePriority.UseTextAlignment = false;
            this.xrLabel50.Text = "(GTGT)";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoNgayAnHan
            // 
            this.lblSoNgayAnHan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoNgayAnHan.LocationFloat = new DevExpress.Utils.PointFloat(627.085F, 118.0001F);
            this.lblSoNgayAnHan.Name = "lblSoNgayAnHan";
            this.lblSoNgayAnHan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoNgayAnHan.SizeF = new System.Drawing.SizeF(53.125F, 10F);
            this.lblSoNgayAnHan.StylePriority.UseFont = false;
            this.lblSoNgayAnHan.StylePriority.UseTextAlignment = false;
            this.lblSoNgayAnHan.Text = "NNE";
            this.lblSoNgayAnHan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(502.0868F, 118.0001F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(124.9982F, 10F);
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.Text = "Thời hạn nộp thuế";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoQuanLyNoiBo
            // 
            this.lblSoQuanLyNoiBo.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoQuanLyNoiBo.LocationFloat = new DevExpress.Utils.PointFloat(221.96F, 138.8334F);
            this.lblSoQuanLyNoiBo.Name = "lblSoQuanLyNoiBo";
            this.lblSoQuanLyNoiBo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoQuanLyNoiBo.SizeF = new System.Drawing.SizeF(173.625F, 10.00002F);
            this.lblSoQuanLyNoiBo.StylePriority.UseFont = false;
            this.lblSoQuanLyNoiBo.StylePriority.UseTextAlignment = false;
            this.lblSoQuanLyNoiBo.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoQuanLyNoiBo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(221.96F, 118.0001F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(173.625F, 10F);
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "Ngày hiệu lực của chứng từ";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiNopThue
            // 
            this.lblPhanLoaiNopThue.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiNopThue.LocationFloat = new DevExpress.Utils.PointFloat(395.585F, 108.0001F);
            this.lblPhanLoaiNopThue.Name = "lblPhanLoaiNopThue";
            this.lblPhanLoaiNopThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiNopThue.SizeF = new System.Drawing.SizeF(21.87499F, 10F);
            this.lblPhanLoaiNopThue.StylePriority.UseFont = false;
            this.lblPhanLoaiNopThue.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiNopThue.Text = "X";
            this.lblPhanLoaiNopThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(221.96F, 108.0001F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(173.625F, 10F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "Phân loại nộp thuế";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaXacDinhThoiHanNop
            // 
            this.lblMaXacDinhThoiHanNop.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaXacDinhThoiHanNop.LocationFloat = new DevExpress.Utils.PointFloat(173.625F, 118.0001F);
            this.lblMaXacDinhThoiHanNop.Name = "lblMaXacDinhThoiHanNop";
            this.lblMaXacDinhThoiHanNop.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaXacDinhThoiHanNop.SizeF = new System.Drawing.SizeF(21.87499F, 10F);
            this.lblMaXacDinhThoiHanNop.StylePriority.UseFont = false;
            this.lblMaXacDinhThoiHanNop.StylePriority.UseTextAlignment = false;
            this.lblMaXacDinhThoiHanNop.Text = "X";
            this.lblMaXacDinhThoiHanNop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(0F, 138.8334F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(195.5F, 10F);
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "Số quản lý trong nội bộ doanh nghiệp";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 118.0001F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(173.625F, 10F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "Mã xác định thời hạn nộp thuế";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 108.0001F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(97.58333F, 10.00001F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "Mã lý do bổ sung";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaNhanVienHQ
            // 
            this.lblMaNhanVienHQ.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaNhanVienHQ.LocationFloat = new DevExpress.Utils.PointFloat(677F, 96.00004F);
            this.lblMaNhanVienHQ.Name = "lblMaNhanVienHQ";
            this.lblMaNhanVienHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNhanVienHQ.SizeF = new System.Drawing.SizeF(50F, 10F);
            this.lblMaNhanVienHQ.StylePriority.UseFont = false;
            this.lblMaNhanVienHQ.StylePriority.UseTextAlignment = false;
            this.lblMaNhanVienHQ.Text = "XXXXE";
            this.lblMaNhanVienHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(547.5833F, 96.00004F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(129.4167F, 10.00001F);
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "Mã nhân viên Hải quan";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenDaiLyHQ
            // 
            this.lblTenDaiLyHQ.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenDaiLyHQ.LocationFloat = new DevExpress.Utils.PointFloat(147.5834F, 96.00004F);
            this.lblTenDaiLyHQ.Name = "lblTenDaiLyHQ";
            this.lblTenDaiLyHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDaiLyHQ.SizeF = new System.Drawing.SizeF(399.9999F, 10.00001F);
            this.lblTenDaiLyHQ.StylePriority.UseFont = false;
            this.lblTenDaiLyHQ.StylePriority.UseTextAlignment = false;
            this.lblTenDaiLyHQ.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblTenDaiLyHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDaiLyHQ
            // 
            this.lblMaDaiLyHQ.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDaiLyHQ.LocationFloat = new DevExpress.Utils.PointFloat(97.58334F, 96.00004F);
            this.lblMaDaiLyHQ.Name = "lblMaDaiLyHQ";
            this.lblMaDaiLyHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDaiLyHQ.SizeF = new System.Drawing.SizeF(50F, 10F);
            this.lblMaDaiLyHQ.StylePriority.UseFont = false;
            this.lblMaDaiLyHQ.StylePriority.UseTextAlignment = false;
            this.lblMaDaiLyHQ.Text = "XXXXE";
            this.lblMaDaiLyHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 96.00004F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(97.58333F, 10.00001F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "Đại lý Hải quan";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoDT
            // 
            this.lblSoDT.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoDT.LocationFloat = new DevExpress.Utils.PointFloat(120.8333F, 84.00002F);
            this.lblSoDT.Name = "lblSoDT";
            this.lblSoDT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDT.SizeF = new System.Drawing.SizeF(268.7517F, 10.00001F);
            this.lblSoDT.StylePriority.UseFont = false;
            this.lblSoDT.StylePriority.UseTextAlignment = false;
            this.lblSoDT.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoDT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChi
            // 
            this.lblDiaChi.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaChi.LocationFloat = new DevExpress.Utils.PointFloat(120.8349F, 57.00003F);
            this.lblDiaChi.Multiline = true;
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChi.SizeF = new System.Drawing.SizeF(573.96F, 27F);
            this.lblDiaChi.StylePriority.UseFont = false;
            this.lblDiaChi.StylePriority.UseTextAlignment = false;
            this.lblDiaChi.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblDiaChi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaBuuChinh
            // 
            this.lblMaBuuChinh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaBuuChinh.LocationFloat = new DevExpress.Utils.PointFloat(120.8333F, 47.00003F);
            this.lblMaBuuChinh.Name = "lblMaBuuChinh";
            this.lblMaBuuChinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBuuChinh.SizeF = new System.Drawing.SizeF(155.21F, 10.00001F);
            this.lblMaBuuChinh.StylePriority.UseFont = false;
            this.lblMaBuuChinh.StylePriority.UseTextAlignment = false;
            this.lblMaBuuChinh.Text = "XXXXXXXXX1 - XXE";
            this.lblMaBuuChinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenNguoiKhai
            // 
            this.lblTenNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenNguoiKhai.LocationFloat = new DevExpress.Utils.PointFloat(120.8333F, 20.00001F);
            this.lblTenNguoiKhai.Multiline = true;
            this.lblTenNguoiKhai.Name = "lblTenNguoiKhai";
            this.lblTenNguoiKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNguoiKhai.SizeF = new System.Drawing.SizeF(573.96F, 27F);
            this.lblTenNguoiKhai.StylePriority.UseFont = false;
            this.lblTenNguoiKhai.StylePriority.UseTextAlignment = false;
            this.lblTenNguoiKhai.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblTenNguoiKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(23.24999F, 84.00002F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(97.58333F, 10.00001F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Số điện thoại";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(23.24999F, 57.00003F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(97.58333F, 10.00001F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Địa chỉ";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(23.25001F, 47.00003F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(97.58333F, 10.00001F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Mã bưu chính";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(23.25001F, 20.00001F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(97.58333F, 10.00001F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Tên";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(23.25001F, 10.00001F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(97.58331F, 10.00001F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Mã";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(120.8333F, 10F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Người khai";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaNguoiKhai
            // 
            this.lblMaNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaNguoiKhai.LocationFloat = new DevExpress.Utils.PointFloat(120.8333F, 10.00001F);
            this.lblMaNguoiKhai.Name = "lblMaNguoiKhai";
            this.lblMaNguoiKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNguoiKhai.SizeF = new System.Drawing.SizeF(155.21F, 10.00001F);
            this.lblMaNguoiKhai.StylePriority.UseFont = false;
            this.lblMaNguoiKhai.StylePriority.UseTextAlignment = false;
            this.lblMaNguoiKhai.Text = "XXXXXXXXX1 - XXE";
            this.lblMaNguoiKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 94.00002F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(727F, 2F);
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 106F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(727F, 2F);
            // 
            // lblMaLyDoKhaiBS
            // 
            this.lblMaLyDoKhaiBS.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaLyDoKhaiBS.LocationFloat = new DevExpress.Utils.PointFloat(173.625F, 108.0001F);
            this.lblMaLyDoKhaiBS.Name = "lblMaLyDoKhaiBS";
            this.lblMaLyDoKhaiBS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaLyDoKhaiBS.SizeF = new System.Drawing.SizeF(21.87499F, 10F);
            this.lblMaLyDoKhaiBS.StylePriority.UseFont = false;
            this.lblMaLyDoKhaiBS.StylePriority.UseTextAlignment = false;
            this.lblMaLyDoKhaiBS.Text = "X";
            this.lblMaLyDoKhaiBS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayHetHieuLuc
            // 
            this.lblNgayHetHieuLuc.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblNgayHetHieuLuc.LocationFloat = new DevExpress.Utils.PointFloat(395.585F, 118.0001F);
            this.lblNgayHetHieuLuc.Name = "lblNgayHetHieuLuc";
            this.lblNgayHetHieuLuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHetHieuLuc.SizeF = new System.Drawing.SizeF(80.20999F, 10F);
            this.lblNgayHetHieuLuc.StylePriority.UseFont = false;
            this.lblNgayHetHieuLuc.StylePriority.UseTextAlignment = false;
            this.lblNgayHetHieuLuc.Text = "dd/MM/yyyy";
            this.lblNgayHetHieuLuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine4
            // 
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 148.8334F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(727F, 2F);
            // 
            // lblMaTienTeTruocKhaiBao
            // 
            this.lblMaTienTeTruocKhaiBao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaTienTeTruocKhaiBao.LocationFloat = new DevExpress.Utils.PointFloat(120.8349F, 160.8334F);
            this.lblMaTienTeTruocKhaiBao.Name = "lblMaTienTeTruocKhaiBao";
            this.lblMaTienTeTruocKhaiBao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTienTeTruocKhaiBao.SizeF = new System.Drawing.SizeF(34.375F, 10F);
            this.lblMaTienTeTruocKhaiBao.StylePriority.UseFont = false;
            this.lblMaTienTeTruocKhaiBao.StylePriority.UseTextAlignment = false;
            this.lblMaTienTeTruocKhaiBao.Text = "XXE";
            this.lblMaTienTeTruocKhaiBao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(155.2099F, 160.8334F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(15.625F, 10F);
            this.xrLabel56.StylePriority.UseFont = false;
            this.xrLabel56.StylePriority.UseTextAlignment = false;
            this.xrLabel56.Text = "-";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTyGiaHoiDoaiTruocKhaiBao
            // 
            this.lblTyGiaHoiDoaiTruocKhaiBao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTyGiaHoiDoaiTruocKhaiBao.LocationFloat = new DevExpress.Utils.PointFloat(170.8349F, 160.8334F);
            this.lblTyGiaHoiDoaiTruocKhaiBao.Name = "lblTyGiaHoiDoaiTruocKhaiBao";
            this.lblTyGiaHoiDoaiTruocKhaiBao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaHoiDoaiTruocKhaiBao.SizeF = new System.Drawing.SizeF(85.41832F, 10.00002F);
            this.lblTyGiaHoiDoaiTruocKhaiBao.StylePriority.UseFont = false;
            this.lblTyGiaHoiDoaiTruocKhaiBao.StylePriority.UseTextAlignment = false;
            this.lblTyGiaHoiDoaiTruocKhaiBao.Text = "XXXXXXXXE";
            this.lblTyGiaHoiDoaiTruocKhaiBao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine5
            // 
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 213.8334F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(727F, 2F);
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.lblPhanLoaiXNK,
            this.xrLine1,
            this.lblNhomXuLyHoSo,
            this.lblMaLoaiHinh,
            this.xrLabel19,
            this.xrLabel18,
            this.lblDauHieuBaoQua,
            this.lblSoToKhaiXNK,
            this.lblGioDKKhaiBoSung,
            this.lblThoiHanTaiNhapXuat,
            this.lblNgayToKhaiXNK,
            this.lblNgayDKKhaiBoSung,
            this.lblNgayCapPhep,
            this.lblCoQuanNhan,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel2,
            this.xrLabel3,
            this.lblSoToKhaiBoSung,
            this.xrLabel1});
            this.TopMargin.HeightF = 143.2084F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiXNK
            // 
            this.lblPhanLoaiXNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiXNK.LocationFloat = new DevExpress.Utils.PointFloat(120.8333F, 92.91668F);
            this.lblPhanLoaiXNK.Name = "lblPhanLoaiXNK";
            this.lblPhanLoaiXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiXNK.SizeF = new System.Drawing.SizeF(21.87499F, 10F);
            this.lblPhanLoaiXNK.StylePriority.UseFont = false;
            this.lblPhanLoaiXNK.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiXNK.Text = "X";
            this.lblPhanLoaiXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 112.9167F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(727F, 2F);
            // 
            // lblNhomXuLyHoSo
            // 
            this.lblNhomXuLyHoSo.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNhomXuLyHoSo.LocationFloat = new DevExpress.Utils.PointFloat(523.9583F, 82.91667F);
            this.lblNhomXuLyHoSo.Name = "lblNhomXuLyHoSo";
            this.lblNhomXuLyHoSo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNhomXuLyHoSo.SizeF = new System.Drawing.SizeF(34.375F, 10F);
            this.lblNhomXuLyHoSo.StylePriority.UseFont = false;
            this.lblNhomXuLyHoSo.StylePriority.UseTextAlignment = false;
            this.lblNhomXuLyHoSo.Text = "XE";
            this.lblNhomXuLyHoSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaLoaiHinh.LocationFloat = new DevExpress.Utils.PointFloat(291.6683F, 92.91668F);
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaLoaiHinh.SizeF = new System.Drawing.SizeF(34.375F, 10F);
            this.lblMaLoaiHinh.StylePriority.UseFont = false;
            this.lblMaLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(276.0433F, 92.91668F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(15.625F, 10F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "-";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(142.7083F, 92.91668F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(15.625F, 10F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "-";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDauHieuBaoQua
            // 
            this.lblDauHieuBaoQua.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDauHieuBaoQua.LocationFloat = new DevExpress.Utils.PointFloat(625.0016F, 92.91668F);
            this.lblDauHieuBaoQua.Name = "lblDauHieuBaoQua";
            this.lblDauHieuBaoQua.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDauHieuBaoQua.SizeF = new System.Drawing.SizeF(21.87499F, 10F);
            this.lblDauHieuBaoQua.StylePriority.UseFont = false;
            this.lblDauHieuBaoQua.StylePriority.UseTextAlignment = false;
            this.lblDauHieuBaoQua.Text = "X";
            this.lblDauHieuBaoQua.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhaiXNK
            // 
            this.lblSoToKhaiXNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhaiXNK.LocationFloat = new DevExpress.Utils.PointFloat(158.3333F, 92.91668F);
            this.lblSoToKhaiXNK.Name = "lblSoToKhaiXNK";
            this.lblSoToKhaiXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiXNK.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblSoToKhaiXNK.StylePriority.UseFont = false;
            this.lblSoToKhaiXNK.StylePriority.UseTextAlignment = false;
            this.lblSoToKhaiXNK.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGioDKKhaiBoSung
            // 
            this.lblGioDKKhaiBoSung.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblGioDKKhaiBoSung.LocationFloat = new DevExpress.Utils.PointFloat(609.3766F, 72.91666F);
            this.lblGioDKKhaiBoSung.Name = "lblGioDKKhaiBoSung";
            this.lblGioDKKhaiBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioDKKhaiBoSung.SizeF = new System.Drawing.SizeF(85.4183F, 10F);
            this.lblGioDKKhaiBoSung.StylePriority.UseFont = false;
            this.lblGioDKKhaiBoSung.StylePriority.UseTextAlignment = false;
            this.lblGioDKKhaiBoSung.Text = "hh:mm:ss";
            this.lblGioDKKhaiBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThoiHanTaiNhapXuat
            // 
            this.lblThoiHanTaiNhapXuat.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblThoiHanTaiNhapXuat.LocationFloat = new DevExpress.Utils.PointFloat(523.9583F, 102.9167F);
            this.lblThoiHanTaiNhapXuat.Name = "lblThoiHanTaiNhapXuat";
            this.lblThoiHanTaiNhapXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThoiHanTaiNhapXuat.SizeF = new System.Drawing.SizeF(85.41827F, 10.00001F);
            this.lblThoiHanTaiNhapXuat.StylePriority.UseFont = false;
            this.lblThoiHanTaiNhapXuat.StylePriority.UseTextAlignment = false;
            this.lblThoiHanTaiNhapXuat.Text = "dd/MM/yyyy";
            this.lblThoiHanTaiNhapXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayToKhaiXNK
            // 
            this.lblNgayToKhaiXNK.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblNgayToKhaiXNK.LocationFloat = new DevExpress.Utils.PointFloat(523.9583F, 92.91668F);
            this.lblNgayToKhaiXNK.Name = "lblNgayToKhaiXNK";
            this.lblNgayToKhaiXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayToKhaiXNK.SizeF = new System.Drawing.SizeF(85.41827F, 10F);
            this.lblNgayToKhaiXNK.StylePriority.UseFont = false;
            this.lblNgayToKhaiXNK.StylePriority.UseTextAlignment = false;
            this.lblNgayToKhaiXNK.Text = "dd/MM/yyyy";
            this.lblNgayToKhaiXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayDKKhaiBoSung
            // 
            this.lblNgayDKKhaiBoSung.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblNgayDKKhaiBoSung.LocationFloat = new DevExpress.Utils.PointFloat(523.9583F, 72.91666F);
            this.lblNgayDKKhaiBoSung.Name = "lblNgayDKKhaiBoSung";
            this.lblNgayDKKhaiBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDKKhaiBoSung.SizeF = new System.Drawing.SizeF(85.4183F, 10F);
            this.lblNgayDKKhaiBoSung.StylePriority.UseFont = false;
            this.lblNgayDKKhaiBoSung.StylePriority.UseTextAlignment = false;
            this.lblNgayDKKhaiBoSung.Text = "dd/MM/yyyy";
            this.lblNgayDKKhaiBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayCapPhep
            // 
            this.lblNgayCapPhep.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblNgayCapPhep.LocationFloat = new DevExpress.Utils.PointFloat(120.8333F, 102.9167F);
            this.lblNgayCapPhep.Name = "lblNgayCapPhep";
            this.lblNgayCapPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayCapPhep.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblNgayCapPhep.StylePriority.UseFont = false;
            this.lblNgayCapPhep.StylePriority.UseTextAlignment = false;
            this.lblNgayCapPhep.Text = "dd/MM/yyyy";
            this.lblNgayCapPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblCoQuanNhan
            // 
            this.lblCoQuanNhan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblCoQuanNhan.LocationFloat = new DevExpress.Utils.PointFloat(120.8333F, 82.91667F);
            this.lblCoQuanNhan.Name = "lblCoQuanNhan";
            this.lblCoQuanNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCoQuanNhan.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblCoQuanNhan.StylePriority.UseFont = false;
            this.lblCoQuanNhan.StylePriority.UseTextAlignment = false;
            this.lblCoQuanNhan.Text = "XXXXXXXXXE";
            this.lblCoQuanNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(360.4167F, 102.9167F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(163.5417F, 10.00001F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(360.4167F, 92.91668F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(163.5417F, 10F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Ngày tờ khai xuất nhập khẩu";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(360.4167F, 82.91667F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(120.8333F, 10F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Nhóm xử lý hồ sơ";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(360.4167F, 72.91666F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(120.8333F, 10F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Ngày đăng ký";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 102.9167F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(120.8333F, 10F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Ngày cấp phép";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 92.91668F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(120.8333F, 10F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Số tờ khai";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 82.91667F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(120.8333F, 10F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Cơ quan nhận";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 72.91666F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(120.8333F, 10F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Số tờ khai bổ sung";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhaiBoSung
            // 
            this.lblSoToKhaiBoSung.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhaiBoSung.LocationFloat = new DevExpress.Utils.PointFloat(120.8333F, 72.91666F);
            this.lblSoToKhaiBoSung.Name = "lblSoToKhaiBoSung";
            this.lblSoToKhaiBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiBoSung.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblSoToKhaiBoSung.StylePriority.UseFont = false;
            this.lblSoToKhaiBoSung.StylePriority.UseTextAlignment = false;
            this.lblSoToKhaiBoSung.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(142.7083F, 28.75001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(432.2023F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Bản xác nhận nội dung khai sửa đổi, bổ sung";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(52.79171F, 0F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(120.8333F, 10.00002F);
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "Mã sắc thuế";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel65
            // 
            this.xrLabel65.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(240.2917F, 0F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(177.1682F, 10.00002F);
            this.xrLabel65.StylePriority.UseFont = false;
            this.xrLabel65.StylePriority.UseTextAlignment = false;
            this.xrLabel65.Text = "Tổng số tiền tăng/giảm thuế";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoTienTangGiamThueXNK
            // 
            this.lblTongSoTienTangGiamThueXNK.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTongSoTienTangGiamThueXNK.LocationFloat = new DevExpress.Utils.PointFloat(262.1667F, 9.999974F);
            this.lblTongSoTienTangGiamThueXNK.Name = "lblTongSoTienTangGiamThueXNK";
            this.lblTongSoTienTangGiamThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoTienTangGiamThueXNK.SizeF = new System.Drawing.SizeF(119.5417F, 10.00003F);
            this.lblTongSoTienTangGiamThueXNK.StylePriority.UseFont = false;
            this.lblTongSoTienTangGiamThueXNK.StylePriority.UseTextAlignment = false;
            this.lblTongSoTienTangGiamThueXNK.Text = "12,345,678,901";
            this.lblTongSoTienTangGiamThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaKhoanMucTiepNhan
            // 
            this.lblMaKhoanMucTiepNhan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaKhoanMucTiepNhan.LocationFloat = new DevExpress.Utils.PointFloat(30.91672F, 0F);
            this.lblMaKhoanMucTiepNhan.Name = "lblMaKhoanMucTiepNhan";
            this.lblMaKhoanMucTiepNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaKhoanMucTiepNhan.SizeF = new System.Drawing.SizeF(21.87499F, 10F);
            this.lblMaKhoanMucTiepNhan.StylePriority.UseFont = false;
            this.lblMaKhoanMucTiepNhan.StylePriority.UseTextAlignment = false;
            this.lblMaKhoanMucTiepNhan.Text = "X";
            this.lblMaKhoanMucTiepNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel68
            // 
            this.xrLabel68.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(52.79171F, 10.00004F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(120.8333F, 10.00002F);
            this.xrLabel68.StylePriority.UseFont = false;
            this.xrLabel68.StylePriority.UseTextAlignment = false;
            this.xrLabel68.Text = "Thuế xuất nhập khẩu";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblBieuThiSoTienTangGiamThueXNK
            // 
            this.lblBieuThiSoTienTangGiamThueXNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblBieuThiSoTienTangGiamThueXNK.LocationFloat = new DevExpress.Utils.PointFloat(240.2917F, 10.00004F);
            this.lblBieuThiSoTienTangGiamThueXNK.Name = "lblBieuThiSoTienTangGiamThueXNK";
            this.lblBieuThiSoTienTangGiamThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuThiSoTienTangGiamThueXNK.SizeF = new System.Drawing.SizeF(21.87499F, 10F);
            this.lblBieuThiSoTienTangGiamThueXNK.StylePriority.UseFont = false;
            this.lblBieuThiSoTienTangGiamThueXNK.StylePriority.UseTextAlignment = false;
            this.lblBieuThiSoTienTangGiamThueXNK.Text = "X";
            this.lblBieuThiSoTienTangGiamThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaTienTeThueXNK
            // 
            this.lblMaTienTeThueXNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaTienTeThueXNK.LocationFloat = new DevExpress.Utils.PointFloat(381.7084F, 10.00004F);
            this.lblMaTienTeThueXNK.Name = "lblMaTienTeThueXNK";
            this.lblMaTienTeThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTienTeThueXNK.SizeF = new System.Drawing.SizeF(34.375F, 10F);
            this.lblMaTienTeThueXNK.StylePriority.UseFont = false;
            this.lblMaTienTeThueXNK.StylePriority.UseTextAlignment = false;
            this.lblMaTienTeThueXNK.Text = "XXE";
            this.lblMaTienTeThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenKhoanMucTiepNhan
            // 
            this.lblTenKhoanMucTiepNhan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenKhoanMucTiepNhan.LocationFloat = new DevExpress.Utils.PointFloat(52.79171F, 0F);
            this.lblTenKhoanMucTiepNhan.Name = "lblTenKhoanMucTiepNhan";
            this.lblTenKhoanMucTiepNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenKhoanMucTiepNhan.SizeF = new System.Drawing.SizeF(120.8333F, 10.00001F);
            this.lblTenKhoanMucTiepNhan.StylePriority.UseFont = false;
            this.lblTenKhoanMucTiepNhan.StylePriority.UseTextAlignment = false;
            this.lblTenKhoanMucTiepNhan.Text = "XXXXXXXXE";
            this.lblTenKhoanMucTiepNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader,
            this.DetailReport1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTenKhoanMucTiepNhan,
            this.lblMaKhoanMucTiepNhan,
            this.lblMaTienTeThueVaThuKhac,
            this.lblBieuThiSoTienTangGiamThueVaThuKhac,
            this.lblTongSoTienTangGiamThueVaThuKhac});
            this.Detail1.HeightF = 70.83334F;
            this.Detail1.Name = "Detail1";
            // 
            // lblMaTienTeThueVaThuKhac
            // 
            this.lblMaTienTeThueVaThuKhac.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaTienTeThueVaThuKhac.LocationFloat = new DevExpress.Utils.PointFloat(381.7084F, 0F);
            this.lblMaTienTeThueVaThuKhac.Name = "lblMaTienTeThueVaThuKhac";
            this.lblMaTienTeThueVaThuKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTienTeThueVaThuKhac.SizeF = new System.Drawing.SizeF(34.37503F, 10.00001F);
            this.lblMaTienTeThueVaThuKhac.StylePriority.UseFont = false;
            this.lblMaTienTeThueVaThuKhac.StylePriority.UseTextAlignment = false;
            this.lblMaTienTeThueVaThuKhac.Text = "XXE";
            this.lblMaTienTeThueVaThuKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblBieuThiSoTienTangGiamThueVaThuKhac
            // 
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.LocationFloat = new DevExpress.Utils.PointFloat(240.2917F, 0F);
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.Name = "lblBieuThiSoTienTangGiamThueVaThuKhac";
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.SizeF = new System.Drawing.SizeF(21.87494F, 10.00001F);
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.StylePriority.UseFont = false;
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.StylePriority.UseTextAlignment = false;
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.Text = "X";
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoTienTangGiamThueVaThuKhac
            // 
            this.lblTongSoTienTangGiamThueVaThuKhac.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTongSoTienTangGiamThueVaThuKhac.LocationFloat = new DevExpress.Utils.PointFloat(262.1667F, 0F);
            this.lblTongSoTienTangGiamThueVaThuKhac.Name = "lblTongSoTienTangGiamThueVaThuKhac";
            this.lblTongSoTienTangGiamThueVaThuKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoTienTangGiamThueVaThuKhac.SizeF = new System.Drawing.SizeF(119.5417F, 10.00004F);
            this.lblTongSoTienTangGiamThueVaThuKhac.StylePriority.UseFont = false;
            this.lblTongSoTienTangGiamThueVaThuKhac.StylePriority.UseTextAlignment = false;
            this.lblTongSoTienTangGiamThueVaThuKhac.Text = "12,345,678,901";
            this.lblTongSoTienTangGiamThueVaThuKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel64,
            this.xrLabel65,
            this.lblTongSoTienTangGiamThueXNK,
            this.xrLabel68,
            this.lblBieuThiSoTienTangGiamThueXNK,
            this.lblMaTienTeThueXNK});
            this.ReportHeader.HeightF = 20.83333F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2});
            this.DetailReport1.Level = 0;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTongSoDongHang,
            this.xrLabel76,
            this.xrLabel75,
            this.lblTongSoTrang});
            this.Detail2.HeightF = 73.95834F;
            this.Detail2.Name = "Detail2";
            // 
            // lblTongSoDongHang
            // 
            this.lblTongSoDongHang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongSoDongHang.LocationFloat = new DevExpress.Utils.PointFloat(667.3766F, 0F);
            this.lblTongSoDongHang.Name = "lblTongSoDongHang";
            this.lblTongSoDongHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoDongHang.SizeF = new System.Drawing.SizeF(39.99384F, 9.999974F);
            this.lblTongSoDongHang.StylePriority.UseFont = false;
            this.lblTongSoDongHang.StylePriority.UseTextAlignment = false;
            this.lblTongSoDongHang.Text = "NE";
            this.lblTongSoDongHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(493.5788F, 0F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(173.7978F, 10.00004F);
            this.xrLabel76.StylePriority.UseFont = false;
            this.xrLabel76.StylePriority.UseTextAlignment = false;
            this.xrLabel76.Text = "Tổng số dòng hàng của tờ khai";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(314.2532F, 0F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(139.3318F, 10.00004F);
            this.xrLabel75.StylePriority.UseFont = false;
            this.xrLabel75.StylePriority.UseTextAlignment = false;
            this.xrLabel75.Text = "Tổng số trang của tờ khai";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoTrang
            // 
            this.lblTongSoTrang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(453.585F, 0F);
            this.lblTongSoTrang.Name = "lblTongSoTrang";
            this.lblTongSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoTrang.SizeF = new System.Drawing.SizeF(39.99384F, 9.999974F);
            this.lblTongSoTrang.StylePriority.UseFont = false;
            this.lblTongSoTrang.StylePriority.UseTextAlignment = false;
            this.lblTongSoTrang.Text = "NE";
            this.lblTongSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(609.3766F, 92.91668F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(15.625F, 10F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "-";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BanXacNhanNoiDungKhaiSuaDoiBoSung_1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(68, 55, 143, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhaiBoSung;
        private DevExpress.XtraReports.UI.XRLabel lblGioDKKhaiBoSung;
        private DevExpress.XtraReports.UI.XRLabel lblThoiHanTaiNhapXuat;
        private DevExpress.XtraReports.UI.XRLabel lblNgayToKhaiXNK;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDKKhaiBoSung;
        private DevExpress.XtraReports.UI.XRLabel lblNgayCapPhep;
        private DevExpress.XtraReports.UI.XRLabel lblCoQuanNhan;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lblNhomXuLyHoSo;
        private DevExpress.XtraReports.UI.XRLabel lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel lblDauHieuBaoQua;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhaiXNK;
        private DevExpress.XtraReports.UI.XRLabel lblTenNguoiKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiKhai;
        private DevExpress.XtraReports.UI.XRLabel lblTenDaiLyHQ;
        private DevExpress.XtraReports.UI.XRLabel lblMaDaiLyHQ;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel lblSoDT;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChi;
        private DevExpress.XtraReports.UI.XRLabel lblMaBuuChinh;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel lblSoQuanLyNoiBo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiNopThue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel lblMaXacDinhThoiHanNop;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel lblMaNhanVienHQ;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lblMaLyDoKhaiBS;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHetHieuLuc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel lblSoNgayAnHanVAT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel lblSoNgayAnHan;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel lblGhiChuSauKhaiBao;
        private DevExpress.XtraReports.UI.XRLabel lblGhiChuTruocKhaiBao;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel lblMaTienTeSauKhaiBao;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaHoiDoaiSauKhaiBao;
        private DevExpress.XtraReports.UI.XRLabel lblMaTienTeTruocKhaiBao;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaHoiDoaiTruocKhaiBao;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoTienTangGiamThueXNK;
        private DevExpress.XtraReports.UI.XRLabel lblMaKhoanMucTiepNhan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel lblBieuThiSoTienTangGiamThueXNK;
        private DevExpress.XtraReports.UI.XRLabel lblMaTienTeThueXNK;
        private DevExpress.XtraReports.UI.XRLabel lblTenKhoanMucTiepNhan;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel lblMaTienTeThueVaThuKhac;
        private DevExpress.XtraReports.UI.XRLabel lblBieuThiSoTienTangGiamThueVaThuKhac;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoTienTangGiamThueVaThuKhac;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoDongHang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoTrang;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiXNK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
    }
}
