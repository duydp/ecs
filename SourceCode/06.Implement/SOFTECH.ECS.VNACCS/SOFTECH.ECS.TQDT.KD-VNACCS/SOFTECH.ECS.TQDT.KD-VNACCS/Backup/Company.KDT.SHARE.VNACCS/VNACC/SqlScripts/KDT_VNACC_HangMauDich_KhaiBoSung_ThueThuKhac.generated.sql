-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]
	@HMDBoSung_ID bigint,
	@SoDong varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac numeric(16, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatTruocKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueTruocKhiKhaiBoSungThuKhac varchar(16),
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac numeric(17, 0),
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatSauKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueSauKhiKhaiBoSungThuKhac varchar(16),
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueVaThuKhac varchar(1),
	@SoTienTangGiamThuKhac numeric(16, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
(
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
)
VALUES 
(
	@HMDBoSung_ID,
	@SoDong,
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
	@ThueSuatTruocKhiKhaiBoSungThuKhac,
	@SoTienThueTruocKhiKhaiBoSungThuKhac,
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
	@ThueSuatSauKhiKhaiBoSungThuKhac,
	@SoTienThueSauKhiKhaiBoSungThuKhac,
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
	@HienThiSoTienTangGiamThueVaThuKhac,
	@SoTienTangGiamThuKhac
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]
	@ID bigint,
	@HMDBoSung_ID bigint,
	@SoDong varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac numeric(16, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatTruocKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueTruocKhiKhaiBoSungThuKhac varchar(16),
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac numeric(17, 0),
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatSauKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueSauKhiKhaiBoSungThuKhac varchar(16),
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueVaThuKhac varchar(1),
	@SoTienTangGiamThuKhac numeric(16, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
SET
	[HMDBoSung_ID] = @HMDBoSung_ID,
	[SoDong] = @SoDong,
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac] = @TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac] = @MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
	[ThueSuatTruocKhiKhaiBoSungThuKhac] = @ThueSuatTruocKhiKhaiBoSungThuKhac,
	[SoTienThueTruocKhiKhaiBoSungThuKhac] = @SoTienThueTruocKhiKhaiBoSungThuKhac,
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac] = @TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac] = @MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
	[ThueSuatSauKhiKhaiBoSungThuKhac] = @ThueSuatSauKhiKhaiBoSungThuKhac,
	[SoTienThueSauKhiKhaiBoSungThuKhac] = @SoTienThueSauKhiKhaiBoSungThuKhac,
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung] = @HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung] = @HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
	[HienThiSoTienTangGiamThueVaThuKhac] = @HienThiSoTienTangGiamThueVaThuKhac,
	[SoTienTangGiamThuKhac] = @SoTienTangGiamThuKhac
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]
	@ID bigint,
	@HMDBoSung_ID bigint,
	@SoDong varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac numeric(16, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatTruocKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueTruocKhiKhaiBoSungThuKhac varchar(16),
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac numeric(17, 0),
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatSauKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueSauKhiKhaiBoSungThuKhac varchar(16),
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueVaThuKhac varchar(1),
	@SoTienTangGiamThuKhac numeric(16, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] 
		SET
			[HMDBoSung_ID] = @HMDBoSung_ID,
			[SoDong] = @SoDong,
			[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac] = @TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
			[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac] = @MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
			[ThueSuatTruocKhiKhaiBoSungThuKhac] = @ThueSuatTruocKhiKhaiBoSungThuKhac,
			[SoTienThueTruocKhiKhaiBoSungThuKhac] = @SoTienThueTruocKhiKhaiBoSungThuKhac,
			[TriGiaTinhThueSauKhiKhaiBoSungThuKhac] = @TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
			[SoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			[MaApDungThueSuatSauKhiKhaiBoSungThuKhac] = @MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
			[ThueSuatSauKhiKhaiBoSungThuKhac] = @ThueSuatSauKhiKhaiBoSungThuKhac,
			[SoTienThueSauKhiKhaiBoSungThuKhac] = @SoTienThueSauKhiKhaiBoSungThuKhac,
			[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung] = @HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
			[HienThiMienThueVaThuKhacSauKhiKhaiBoSung] = @HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
			[HienThiSoTienTangGiamThueVaThuKhac] = @HienThiSoTienTangGiamThueVaThuKhac,
			[SoTienTangGiamThuKhac] = @SoTienTangGiamThuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
		(
			[HMDBoSung_ID],
			[SoDong],
			[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
			[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
			[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
			[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
			[ThueSuatTruocKhiKhaiBoSungThuKhac],
			[SoTienThueTruocKhiKhaiBoSungThuKhac],
			[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
			[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
			[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
			[ThueSuatSauKhiKhaiBoSungThuKhac],
			[SoTienThueSauKhiKhaiBoSungThuKhac],
			[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
			[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
			[HienThiSoTienTangGiamThueVaThuKhac],
			[SoTienTangGiamThuKhac]
		)
		VALUES 
		(
			@HMDBoSung_ID,
			@SoDong,
			@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
			@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
			@ThueSuatTruocKhiKhaiBoSungThuKhac,
			@SoTienThueTruocKhiKhaiBoSungThuKhac,
			@TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
			@SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			@MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
			@ThueSuatSauKhiKhaiBoSungThuKhac,
			@SoTienThueSauKhiKhaiBoSungThuKhac,
			@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
			@HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
			@HienThiSoTienTangGiamThueVaThuKhac,
			@SoTienTangGiamThuKhac
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]
	@HMDBoSung_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[HMDBoSung_ID] = @HMDBoSung_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]
	@HMDBoSung_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[HMDBoSung_ID] = @HMDBoSung_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]	

GO

