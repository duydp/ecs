﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KD.BLL;
using Company.KD.BLL.Utils;
using Infragistics.Excel;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.BLL;
using Company.BLL.Utils;
using Infragistics.Excel;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using Infragistics.Excel;
#endif

namespace Company.Interface.HS08SO
{
    public partial class ChuyenMaHS08So : BaseForm
    {
        private DataTable dt;
        private DataTable dtHS = MaHS.SelectAll();
        private List<HangMauDich> HangCollection = new List<HangMauDich>();
        private List<HangMauDich> HangCollectionExcel = new List<HangMauDich>();

        public ChuyenMaHS08So()
        {
            InitializeComponent();
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            if (cboLoaiHangHoa.SelectedValue == null)
                return;

            SelectHangForm f = new SelectHangForm();
            f.LoaiHangHoa = cboLoaiHangHoa.SelectedValue.ToString();
            f.ShowDialog(this);

            try
            {
                dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo<HangMauDich>(f.HangCollectionSelected);

                dgList.DataSource = dt;

                dt.RowChanging += new DataRowChangeEventHandler(ds_RowChanging);

            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void ChuyenMaHS08So_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            //Tab theo ma
            cboLoaiHangHoa.SelectedIndex = 0;
            CreateColumnCombo(dgList);

            //Tab Excel
            cboLoaiHangHoa2.SelectedIndex = 0;
            CreateColumnCombo(dgList2);

            Cursor = Cursors.Default;
        }

        void ds_RowChanging(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Add || e.Action == DataRowAction.Change)
            {
                if (e.Row["MaHSMoi"].ToString() == "")
                {
                    e.Row.SetColumnError("MaHSMoi", "Mã HS không được rỗng");
                }
                else
                {
                    e.Row.SetColumnError("MaHSMoi", "");
                }
            }
        }

        public void CreateColumnCombo(Janus.Windows.GridEX.GridEX grid)
        {
            //System.Data.DataTable dt = MaHS.SelectAll();

            //Create multiplevalues column

            //When using a many-to-manyn relation, the DataMember of the MultipleValues
            //column must be the name of the relation between the parent table and the 
            //relation table.

            //Janus.Windows.GridEX.GridEXColumn col = new Janus.Windows.GridEX.GridEXColumn();
            Janus.Windows.GridEX.GridEXColumn col = grid.RootTable.Columns["MaHSMoi"];
            col.EditType = Janus.Windows.GridEX.EditType.Combo;
            col.Caption = "Mã HS 8 số";
            col.Width = 100;
            //Since the column will be bound to a list containing DataRowView objects, we must specify which
            //field in the DataRowView will be used as value 
            col.MultipleValueDataMember = "HS10SO";

            //Fill the ValueList with the categories table
            col.HasValueList = true;
            col.ValueList.PopulateValueList(dtHS.DefaultView, "HS10SO", "HS10SO");
            col.DefaultGroupInterval = Janus.Windows.GridEX.GroupInterval.Text;
            col.CompareTarget = Janus.Windows.GridEX.ColumnCompareTarget.Text;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (dt.HasErrors) return;

            Save(dgList.GetRows());
        }

        private void Save(GridEXRow[] items)
        {
            try
            {
                if (items.Length <= 0) return;

                HangCollection.Clear();
                foreach (GridEXRow i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DataRowView drv = (DataRowView)i.DataRow;

                        if (drv["MaHSMoi"].ToString() == "")
                        {
                            ShowMessage("Có dòng hàng trong danh sách chưa nhập Mã HS mới.", false);
                            return;
                        }
                        else
                        {
                            HangMauDich HMD = new HangMauDich();
                            HMD.Ma = drv["Ma"].ToString();
                            HMD.Ten = drv["Ten"].ToString();
                            HMD.MaHS = drv["MaHS"].ToString();
                            HMD.DVT_ID = drv["DVT_ID"].ToString();
                            HMD.MaHSMoi = drv["MaHSMoi"].ToString();
                            HangCollection.Add(HMD);
                        }
                    }
                }

                bool success = true;
                foreach (HangMauDich item in HangCollection)
                {
#if KD_V3 || KD_V4
                    success &= Company.KDT.SHARE.Components.Globals.ConvertHS8Manual_ByName(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, item.Ten, item.DVT_ID, item.MaHS, item.MaHSMoi, "KD");
#elif SXXK_V3 || SXXK_V4
                    success &= Company.KDT.SHARE.Components.Globals.ConvertHS8Manual_ByCode(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, item.Ma, item.DVT_ID, item.MaHS, item.MaHSMoi, "SXXK");
#elif GC_V3
                    success &= Company.KDT.SHARE.Components.Globals.ConvertHS8Manual_ByCode(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, item.Ma, item.DVT_ID, item.MaHS, item.MaHSMoi, "GC");
#endif

                    if (!success)
                    {
                        break;
                    }
                }

                if (success && HangCollection.Count > 0)
                    ShowMessage("Đã cập nhật mã HS mới thành công.", false);
                else
                    ShowMessage("Cập nhật mã HS mới không thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Xảy ra lỗi trong quá trình cập nhật mã HS: " + ex.Message, false);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave2_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
                return;

            Save(dgList2.GetRows());
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                txtFilePath.Text = openFileDialog1.FileName;
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
                return;

            ReadFileExcel(txtFilePath.Text);
        }

        private void ReadFileExcel(string p)
        {
            try
            {
                #region Read

                Workbook wb = new Workbook();

                Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text, true);
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                    return;
                }
                try
                {
                    ws = wb.Worksheets[txtSheet.Text];
                }
                catch
                {
                    MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"MSG_EXC01", "", txtSheet.Text, false);
                    return;
                }
                //editing :
                WorksheetRowCollection wsrc = ws.Rows;
                char maHangColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
                int maHangCol = ConvertCharToInt(maHangColumn);

                char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
                int tenHangCol = ConvertCharToInt(tenHangColumn);

                char maHSColumn = Convert.ToChar(txtMaHSColumn.Text.Trim());
                int maHSCol = ConvertCharToInt(maHSColumn);

                char dVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
                int dVTCol = ConvertCharToInt(dVTColumn);

                char maHSMoiColumn = Convert.ToChar(txtMaHSMoiColumn.Text.Trim());
                int maHSMoiCol = ConvertCharToInt(maHSMoiColumn);

                int beginRow = Convert.ToInt32(txtRow.Value) - 1;
                HangCollectionExcel.Clear();

                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            HangMauDich hmd = new HangMauDich();

                            //Ma hang
                            hmd.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
#if KD_V3 || KD_V4
                            if (hmd.Ma.Trim().Length == 0)
                                continue;
#elif SXXK_V3 || SXXK_V4 || GC_V3
                            if (hmd.MaPhu.Trim().Length == 0)
                            {
                                MLMessages("Mã hàng ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }
#endif

                            //Ten hang
                            hmd.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                            if (hmd.Ten.Trim().Length == 0)
                            {
                                MLMessages("Tên hàng ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }

                            //DVT
                            try
                            {
                                hmd.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetID(Convert.ToString(wsr.Cells[dVTCol].Value).Trim());
                                if (hmd.DVT_ID.Equals(""))
                                {
                                    MLMessages("Đơn vị tính ở dòng " + (wsr.Index + 1) + " không hợp lệ.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                    return;
                                }
                            }
                            catch
                            {
                                MLMessages("Đơn vị tính ở dòng " + (wsr.Index + 1) + " không hợp lệ.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }

                            //MaHS cu
                            hmd.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                            if (hmd.MaHS.Trim().Length == 0)
                            {
                                MLMessages("Mã HS cũ ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }

                            //MaHS moi
                            hmd.MaHSMoi = Convert.ToString(wsr.Cells[maHSMoiCol].Value).Trim();
                            if (hmd.MaHSMoi.Trim().Length == 0)
                            {
                                MLMessages("Mã HS mới ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }

                            HangCollectionExcel.Add(hmd);
                        }
                        catch
                        {
                            if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", "MSG_EXC06", Convert.ToString(wsr.Index + 1), true) != "Yes")
                            {
                                return;
                            }
                        }
                    }
                }

                #endregion

                dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo<HangMauDich>(HangCollectionExcel);

                dgList2.DataSource = dt;

                dt.RowChanging += new DataRowChangeEventHandler(ds_RowChanging);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi đọc tệp tin Excel: " + ex.Message, false);
            }
        }

        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

        private bool ValidateForm()
        {
            bool valid = true;

            valid &= ValidateControl.ValidateNull(txtSheet, errorProvider1, "Tên Sheet");

            valid &= ValidateControl.ValidateNull(txtRow, errorProvider1, "Dòng bắt đầu");

            valid &= ValidateControl.ValidateNull(txtMaHangColumn, errorProvider1, "Mã hàng");

            valid &= ValidateControl.ValidateNull(txtTenHangColumn, errorProvider1, "Tên hàng");

            valid &= ValidateControl.ValidateNull(txtDVTColumn, errorProvider1, "Đơn vị tính");

            valid &= ValidateControl.ValidateNull(txtMaHSColumn, errorProvider1, "Mã HS cũ");

            valid &= ValidateControl.ValidateNull(txtMaHSMoiColumn, errorProvider1, "Mã HS mới");

            valid &= ValidateControl.ValidateNull(txtFilePath, errorProvider1, "Đường dẫn tệp tin");

            return valid;
        }

        private void lblLinkExcelTemplate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_ChuyenMaHS8So();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void dgList2_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }
    }
}
