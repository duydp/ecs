import java.io.*;
import java.util.Random;

public class Sample
{
   public static void main(String[] args)
   {
      int iRetcode, iSelect = 0, iBlock_index;
      int iTmp;
      int iHandle, startByte;
      int[] iUid = new int[1], iHid = new int[1], iLen = new int[1];
      byte[] cBuffer = new byte[1000], cTmp = new byte[64];
      byte[] key512 = new byte[512];
      byte[] plainText = new byte[1000];
      String strTmp;

      JSDX sdx = new JSDX();
      do
      {
         System.out.println();
         System.out.println("1. Write Data");
         System.out.println("2. Read Data");
         System.out.println("3. Transform Data");
         System.out.println("4. RSA Encryption");
         System.out.println("5. RSA Decryption");
         System.out.println("6. Write with Map");
         System.out.println("7. Read with Map");
         System.out.println("0. Exit");
         System.out.print("Please enter selection: ");
         try
         {
            iSelect = System.in.read();
            do
            {
               iTmp = System.in.read();
            } while (iTmp != 0xA);
            iSelect -= 0x30;
         }
         catch (Exception e)
         {
            System.out.println("Input Error!");
            return;
         }

         // Write to SecureDongle X with specified UID
         else if (iSelect == 1)
         {
            iRetcode = sdx.SDX_Find();
            if (iRetcode < 0)
            {
               System.out.println("Error Finding SecureDongle X: " + Integer.toHexString(iRetcode));
               return;
            }
            if (iRetcode == 0)
            {
               System.out.println("No SecureDongle X plugged");
               return;
            }
            System.out.println("Found SecureDongle X: " + Integer.toString(iRetcode));

            System.out.print("Please input UID (i.e. 715400947): ");
            try
            {
               iTmp = System.in.read(cTmp);

               // Delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               // Translate the content in buffer into string, and then into int
               strTmp = new String(cTmp, 0, iTmp - 2);
               iUid[0] = new Integer(strTmp).intValue();
            }
            catch (Exception e)
            {
               System.out.println("UID input error!");
               return;
            }

            iRetcode = sdx.SDX_Open(1, iUid[0], iHid);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + iRetcode);
               return;
            }
            System.out.println("Succeeded Opening SecureDongle X, UID: " + Integer.toString(iUid[0]));

            iHandle = iRetcode;
            System.out.print("Please input write block index (0-4): ");
            try
            {
               iBlock_index = System.in.read();
               // Delete carriages
               do
               {
                  iTmp = System.in.read();
               } while (iTmp != 0xA);
               iBlock_index -= 0x30;
            }
            catch (Exception e)
            {
               System.out.println("Read block index error!");
               return;
            }
            System.out.print("Please input data to be written (i.e. SDX - Sample Data): ");
            try
            {
               iTmp = System.in.read(cBuffer);
               // Delete carriages
               cBuffer[iTmp - 1] = 0;
               cBuffer[iTmp - 2] = 0;
            }
            catch (Exception e)
            {
               System.out.println("Read buffer error!");
               return;
            }

            iRetcode = sdx.SDX_Write(iHandle, iBlock_index, cBuffer);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + Integer.toHexString(iRetcode));
               return;
            }

            System.out.println("Write OK");
            sdx.SDX_Close(iHandle);
         }

         // Read SecureDongle X with specified UID
         else if (iSelect == 2)
         {
            iRetcode = sdx.SDX_Find();
            if (iRetcode < 0)
            {
               System.out.println("Error Finding SecureDongle X: " + Integer.toHexString(iRetcode));
               return;
            }
            if (iRetcode == 0)
            {
               System.out.println("No SecureDongle X plugged");
               return;
            }
            System.out.println("Found SecureDongle X: " + Integer.toString(iRetcode));

            System.out.print("Please input UID (i.e. 715400947): ");
            try
            {
               iTmp = System.in.read(cTmp);

               // Delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               // Translate the content in buffer into string, and then into int
               strTmp = new String(cTmp, 0, iTmp - 2);
               iUid[0] = new Integer(strTmp).intValue();
            }
            catch (Exception e)
            {
               System.out.println("UID input error!");
               return;
            }

            iRetcode = sdx.SDX_Open(1, iUid[0], iHid);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + iRetcode);
               return;
            }
            System.out.println("Succeeded Opening SecureDongle X, UID: " + Integer.toString(iUid[0]));

            iHandle = iRetcode;
            System.out.print("Please input Read block index (0-4): ");
            try
            {
               iBlock_index = System.in.read();
               // Delete carriages
               do
               {
                  iTmp = System.in.read();
               } while (iTmp != 0xA);
               iBlock_index -= 0x30;
            }
            catch (Exception e)
            {
               System.out.println("Read block index error!");
               return;
            }
            iRetcode = sdx.SDX_Read(iHandle, iBlock_index, cBuffer);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + Integer.toHexString(iRetcode));
               return;
            }
            System.out.println("Read: " + (new String(cBuffer, 0, BufLen(cBuffer, 512))));
            sdx.SDX_Close(iHandle);
         }

         // Transform data using specified SecureDongle X
         else if (iSelect == 3)
         {
            iRetcode = sdx.SDX_Find();
            if (iRetcode < 0)
            {
               System.out.println("Error Finding SecureDongle X: " + Integer.toHexString(iRetcode));
               return;
            }
            if (iRetcode == 0)
            {
               System.out.println("No SecureDongle X plugged");
               return;
            }
            System.out.println("Found SecureDongle X: " + Integer.toString(iRetcode));

            System.out.print("Please input UID (i.e. 715400947): ");
            try
            {
               iTmp = System.in.read(cTmp);

               // Delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               // Translate the content in buffer into string, and then into int
               strTmp = new String(cTmp, 0, iTmp - 2);
               iUid[0] = new Integer(strTmp).intValue();
            }
            catch (Exception e)
            {
               System.out.println("UID input error!");
               return;
            }

            iRetcode = sdx.SDX_Open(1, iUid[0], iHid);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + iRetcode);
               return;
            }
            System.out.println("Succeeded Opening SecureDongle X, UID: " + Integer.toString(iUid[0]));

            iHandle = iRetcode;
            System.out.print("Please input data (i.e. SDXTransform Test): ");
            try
            {
               iTmp = System.in.read(cBuffer);
               // delete carriages
               cBuffer[iTmp - 1] = 0;
               cBuffer[iTmp - 2] = 0;
            }
            catch (Exception e)
            {
               System.out.println("Read buffer error!");
               return;
            }
            iBlock_index = BufLen(cBuffer, 512);
            iRetcode = sdx.SDX_Transform(iHandle, iBlock_index, cBuffer);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + Integer.toHexString(iRetcode));
               return;
            }
            System.out.println("Transform result: " + (new String(cBuffer, 0, BufLen(cBuffer, 512))));
            sdx.SDX_Close(iHandle);
         }

         // Encrypt using RSA and store to SecureDongle X with specified UID
         else if (iSelect == 4)
         {
            iRetcode = sdx.SDX_Find();
            if (iRetcode < 0)
            {
               System.out.println("Error Finding SecureDongle X: " + Integer.toHexString(iRetcode));
               return;
            }
            if (iRetcode == 0)
            {
               System.out.println("No SecureDongle X plugged");
               return;
            }
            System.out.println("Found SecureDongle X: " + Integer.toString(iRetcode));

            System.out.print("Please input UID (i.e. 715400947): ");
            try
            {
               iTmp = System.in.read(cTmp);

               // Delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               // Translate the content in buffer into string, and then into int
               strTmp = new String(cTmp, 0, iTmp - 2);
               iUid[0] = new Integer(strTmp).intValue();
            }
            catch (Exception e)
            {
               System.out.println("UID input error!");
               return;
            }

            iRetcode = sdx.SDX_Open(1, iUid[0], iHid);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + iRetcode);
               return;
            }
            System.out.println("Succeeded Opening SecureDongle X, UID: " + Integer.toString(iUid[0]));

            iHandle = iRetcode;

            // Input start byte to write
            System.out.print("Please enter start index (0-2559): ");
            try
            {
               iTmp = System.in.read(cTmp);
               // delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               iTmp -= 2;

               startByte = Integer.parseInt(new String(cTmp, 0, iTmp));
            }
            catch (Exception e)
            {
               System.out.println("Read startIndex error!");
               return;
            }

            System.out.print("Please enter data to encrypt (i.e. SDX Test RSA Encrypt): ");
            try
            {
               iTmp = System.in.read(cBuffer);
               // delete carriages
               cBuffer[iTmp - 1] = 0;
               cBuffer[iTmp - 2] = 0;

               iLen[0] = iTmp - 2;
            }
            catch (Exception e)
            {
               System.out.println("Read buffer error!");
               return;
            }
            iRetcode = sdx.SDX_RSAEncrypt(iHandle, startByte, cBuffer, iLen, key512);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + Integer.toHexString(iRetcode));
               return;
            }
            System.out.println("Write success, size: " + iLen[0] + ". Key to decrypt is stored inside key512 variable.");
            System.out.println("You can try reading the contents of the SDX");

            sdx.SDX_Close(iHandle);
         }

         // Read from SecureDongle X with specified UID and decrypt using RSA
         else if (iSelect == 5)
         {
            iRetcode = sdx.SDX_Find();
            if (iRetcode < 0)
            {
               System.out.println("Error Finding SecureDongle X: " + Integer.toHexString(iRetcode));
               return;
            }
            if (iRetcode == 0)
            {
               System.out.println("No SecureDongle X plugged");
               return;
            }
            System.out.println("Found SecureDongle X: " + Integer.toString(iRetcode));

            System.out.print("Please input UID (i.e. 715400947): ");
            try
            {
               iTmp = System.in.read(cTmp);

               // Delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               // Translate the content in buffer into string, and then into int
               strTmp = new String(cTmp, 0, iTmp - 2);
               iUid[0] = new Integer(strTmp).intValue();
            }
            catch (Exception e)
            {
               System.out.println("UID input error!");
               return;
            }

            iRetcode = sdx.SDX_Open(1, iUid[0], iHid);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + iRetcode);
               return;
            }
            System.out.println("Succeeded Opening SecureDongle X, UID: " + Integer.toString(iUid[0]));

            iHandle = iRetcode;

            // input start byte to read
            System.out.print("Please enter start index (0-2559): ");
            try
            {
               iTmp = System.in.read(cTmp);
               // delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               iTmp -= 2;

               startByte = Integer.parseInt(new String(cTmp, 0, iTmp));
            }
            catch (Exception e)
            {
               System.out.println("Read startIndex error!");
               return;
            }

            System.out.print("Please enter length of data to decrypt: ");
            try
            {
               iTmp = System.in.read(cTmp);
               // delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               iTmp -= 2;

               iLen[0] = Integer.parseInt(new String(cTmp, 0, iTmp));
            }
            catch (Exception e)
            {
               System.out.println("Read length of data error!");
               return;
            }

            iRetcode = sdx.SDX_RSADecrypt(iHandle, startByte, plainText, iLen, key512);

            if (iRetcode < 0)
            {
               System.out.println("Error: " + Integer.toHexString(iRetcode));
               return;
            }
            System.out.println("Successfully read data with size: " + iLen[0] + " bytes");
            System.out.println("Decrypted Data: " + new String(plainText, 0, iLen[0]));

            sdx.SDX_Close(iHandle);
         }

         // Write to SecureDongle X with specified UID
         // This is a sample of simple Data Mapping to put plain text data not in order
         // For example, in 5-byte data, byte 1 will be put in SDX memory location 10, byte 2 will be put in SDX memory location 1
         else if (iSelect == 6)
         {
            iRetcode = sdx.SDX_Find();
            if (iRetcode < 0)
            {
               System.out.println("Error Finding SecureDongle X: " + Integer.toHexString(iRetcode));
               return;
            }
            if (iRetcode == 0)
            {
               System.out.println("No SecureDongle X plugged");
               return;
            }
            System.out.println("Found SecureDongle X: " + Integer.toString(iRetcode));

            System.out.print("Please input UID (i.e. 715400947): ");
            try
            {
               iTmp = System.in.read(cTmp);

               // Delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               // Translate the content in buffer into string, and then into int
               strTmp = new String(cTmp, 0, iTmp - 2);
               iUid[0] = new Integer(strTmp).intValue();
            }
            catch (Exception e)
            {
               System.out.println("UID input error!");
               return;
            }

            iRetcode = sdx.SDX_Open(1, iUid[0], iHid);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + iRetcode);
               return;
            }
            System.out.println("Succeeded Opening SecureDongle X, UID: " + Integer.toString(iUid[0]));

            iHandle = iRetcode;
            System.out.print("Please input write block index (0-4): ");
            try
            {
               iBlock_index = System.in.read();
               // Delete carriages
               do
               {
                  iTmp = System.in.read();
               } while (iTmp != 0xA);
               iBlock_index -= 0x30;
            }
            catch (Exception e)
            {
               System.out.println("Read block index error!");
               return;
            }
            System.out.print("Please input data to be written (i.e. SDX - Sample Map Encryption): ");
            try
            {
               iTmp = System.in.read(cBuffer);
               // Delete carriages
               cBuffer[iTmp - 1] = 0;
               cBuffer[iTmp - 2] = 0;
            }
            catch (Exception e)
            {
               System.out.println("Read buffer error!");
               return;
            }
            String sBuffer = new String(cBuffer).trim();
            System.out.println("Data Length = " + Integer.toString(sBuffer.length()));

            //****************** ENCRYPTION SAMPLE *******************
            int i, sze, indx;
            byte[] temp = new byte[512];
            Random rnd = new Random();
            if (sBuffer.length() > 512)
            {
               System.out.println("Error: Data size max is 512");
               return;
            }

            for (i = 0; i < 512; i++)
            {
               if (rnd.nextInt(2) == 1)
               {
                  temp[i] = (byte)(rnd.nextInt(26) + 65);
               }
               else
               {
                  temp[i] = (byte)(rnd.nextInt(26) + 97);
               }
            }
            for (i = 0; i <= sBuffer.length() - 1; i++)
            {
               sze = i * (sBuffer.length() - 1);
               indx = sze % 512;
               temp[indx] = cBuffer[i];
            }
            //********************************************************

            iRetcode = sdx.SDX_Write(iHandle, iBlock_index, temp);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + Integer.toHexString(iRetcode));
               return;
            }

            System.out.println("Data Written: " + (new String(temp, 0, BufLen(temp, 512))));
            sdx.SDX_Close(iHandle);
         }

         // Read from SecureDongle X with specified UID
         // This is the sample to read the Mapped data
         else if (iSelect == 7)
         {
            iRetcode = sdx.SDX_Find();
            if (iRetcode < 0)
            {
               System.out.println("Error Finding SecureDongle X: " + Integer.toHexString(iRetcode));
               return;
            }
            if (iRetcode == 0)
            {
               System.out.println("No SecureDongle X plugged");
               return;
            }
            System.out.println("Found SecureDongle X: " + Integer.toString(iRetcode));

            System.out.print("Please input UID (i.e. 715400947): ");
            try
            {
               iTmp = System.in.read(cTmp);

               // Delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               // Translate the content in buffer into string, and then into int
               strTmp = new String(cTmp, 0, iTmp - 2);
               iUid[0] = new Integer(strTmp).intValue();
            }
            catch (Exception e)
            {
               System.out.println("UID input error!");
               return;
            }

            iRetcode = sdx.SDX_Open(1, iUid[0], iHid);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + iRetcode);
               return;
            }
            System.out.println("Succeeded Opening SecureDongle X, UID: " + Integer.toString(iUid[0]));

            iHandle = iRetcode;
            System.out.print("Please input Read block index (0-4): ");
            try
            {
               iBlock_index = System.in.read();
               // Delete carriages
               do
               {
                  iTmp = System.in.read();
               } while (iTmp != 0xA);
               iBlock_index -= 0x30;
            }
            catch (Exception e)
            {
               System.out.println("Read block index error!");
               return;
            }

            int iDataSize;
            System.out.print("Please enter size of the data to be read (1-512): ");
            try
            {
               iTmp = System.in.read(cTmp);

               // Delete carriages
               cTmp[iTmp - 1] = 0;
               cTmp[iTmp - 2] = 0;

               // Translate the content in buffer into string, and then into int
               strTmp = new String(cTmp, 0, iTmp - 2);
               iDataSize = new Integer(strTmp).intValue();
            }
            catch (Exception e)
            {
               System.out.println("Read data size error!");
               return;
            }
				System.out.println(iDataSize);
            iRetcode = sdx.SDX_Read(iHandle, iBlock_index, cBuffer);
            if (iRetcode < 0)
            {
               System.out.println("Error: " + Integer.toHexString(iRetcode));
               return;
            }


            //******************* DECRYPTION **********************
            String buff = "";
            int i, sze, indx;

            for (i = 0; i < iDataSize; i++)
            {
               sze = i * (iDataSize - 1);
               indx = sze % 512;
               buff = buff + (char)cBuffer[indx];
            }
            //*****************************************************
  
            System.out.println("Read: " + buff);
            sdx.SDX_Close(iHandle);
         }
      } while (iSelect != 0);
   }

   // find blank character in buf, and return the length.
   public static int BufLen(byte[] buffer, int len)
   {
      int i = 0;
      for (i = 0; i < len && buffer[i] != 0; i++) ;
      return i;
   }
}


