﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class T_KDT_THUPHI_THONGBAO : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TK_ID { set; get; }
		public int TrangThai { set; get; }
		public string SoThongBao { set; get; }
		public DateTime NgayThongBao { set; get; }
		public string DonViThongBao { set; get; }
		public string MaDonVi { set; get; }
		public string TenDonVi { set; get; }
		public string DiaChi { set; get; }
		public string TenDonViXNK { set; get; }
		public string MaDonViXNK { set; get; }
		public string DiaChiXNK { set; get; }
		public string GuidString { set; get; }
        public List<T_KDT_THUPHI_THONGBAO_FILE> FileCollection = new List<T_KDT_THUPHI_THONGBAO_FILE>();
        public List<T_KDT_THUPHI_THONGBAO_PHI> PhiCollection = new List<T_KDT_THUPHI_THONGBAO_PHI>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KDT_THUPHI_THONGBAO> ConvertToCollection(IDataReader reader)
		{
			List<T_KDT_THUPHI_THONGBAO> collection = new List<T_KDT_THUPHI_THONGBAO>();
			while (reader.Read())
			{
				T_KDT_THUPHI_THONGBAO entity = new T_KDT_THUPHI_THONGBAO();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TK_ID"))) entity.TK_ID = reader.GetInt64(reader.GetOrdinal("TK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThongBao"))) entity.SoThongBao = reader.GetString(reader.GetOrdinal("SoThongBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayThongBao"))) entity.NgayThongBao = reader.GetDateTime(reader.GetOrdinal("NgayThongBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViThongBao"))) entity.DonViThongBao = reader.GetString(reader.GetOrdinal("DonViThongBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonVi"))) entity.MaDonVi = reader.GetString(reader.GetOrdinal("MaDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonVi"))) entity.TenDonVi = reader.GetString(reader.GetOrdinal("TenDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViXNK"))) entity.TenDonViXNK = reader.GetString(reader.GetOrdinal("TenDonViXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViXNK"))) entity.MaDonViXNK = reader.GetString(reader.GetOrdinal("MaDonViXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiXNK"))) entity.DiaChiXNK = reader.GetString(reader.GetOrdinal("DiaChiXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidString"))) entity.GuidString = reader.GetString(reader.GetOrdinal("GuidString"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KDT_THUPHI_THONGBAO> collection, long id)
        {
            foreach (T_KDT_THUPHI_THONGBAO item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KDT_THUPHI_THONGBAO VALUES(@TK_ID, @TrangThai, @SoThongBao, @NgayThongBao, @DonViThongBao, @MaDonVi, @TenDonVi, @DiaChi, @TenDonViXNK, @MaDonViXNK, @DiaChiXNK, @GuidString)";
            string update = "UPDATE T_KDT_THUPHI_THONGBAO SET TK_ID = @TK_ID, TrangThai = @TrangThai, SoThongBao = @SoThongBao, NgayThongBao = @NgayThongBao, DonViThongBao = @DonViThongBao, MaDonVi = @MaDonVi, TenDonVi = @TenDonVi, DiaChi = @DiaChi, TenDonViXNK = @TenDonViXNK, MaDonViXNK = @MaDonViXNK, DiaChiXNK = @DiaChiXNK, GuidString = @GuidString WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_THONGBAO WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TK_ID", SqlDbType.BigInt, "TK_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThongBao", SqlDbType.NVarChar, "SoThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayThongBao", SqlDbType.DateTime, "NgayThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViThongBao", SqlDbType.NVarChar, "DonViThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonVi", SqlDbType.NVarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViXNK", SqlDbType.NVarChar, "TenDonViXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViXNK", SqlDbType.NVarChar, "MaDonViXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiXNK", SqlDbType.NVarChar, "DiaChiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidString", SqlDbType.VarChar, "GuidString", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TK_ID", SqlDbType.BigInt, "TK_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThongBao", SqlDbType.NVarChar, "SoThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayThongBao", SqlDbType.DateTime, "NgayThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViThongBao", SqlDbType.NVarChar, "DonViThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonVi", SqlDbType.NVarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViXNK", SqlDbType.NVarChar, "TenDonViXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViXNK", SqlDbType.NVarChar, "MaDonViXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiXNK", SqlDbType.NVarChar, "DiaChiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidString", SqlDbType.VarChar, "GuidString", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KDT_THUPHI_THONGBAO VALUES(@TK_ID, @TrangThai, @SoThongBao, @NgayThongBao, @DonViThongBao, @MaDonVi, @TenDonVi, @DiaChi, @TenDonViXNK, @MaDonViXNK, @DiaChiXNK, @GuidString)";
            string update = "UPDATE T_KDT_THUPHI_THONGBAO SET TK_ID = @TK_ID, TrangThai = @TrangThai, SoThongBao = @SoThongBao, NgayThongBao = @NgayThongBao, DonViThongBao = @DonViThongBao, MaDonVi = @MaDonVi, TenDonVi = @TenDonVi, DiaChi = @DiaChi, TenDonViXNK = @TenDonViXNK, MaDonViXNK = @MaDonViXNK, DiaChiXNK = @DiaChiXNK, GuidString = @GuidString WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_THONGBAO WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TK_ID", SqlDbType.BigInt, "TK_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThongBao", SqlDbType.NVarChar, "SoThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayThongBao", SqlDbType.DateTime, "NgayThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViThongBao", SqlDbType.NVarChar, "DonViThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonVi", SqlDbType.NVarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViXNK", SqlDbType.NVarChar, "TenDonViXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViXNK", SqlDbType.NVarChar, "MaDonViXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiXNK", SqlDbType.NVarChar, "DiaChiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidString", SqlDbType.VarChar, "GuidString", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TK_ID", SqlDbType.BigInt, "TK_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThongBao", SqlDbType.NVarChar, "SoThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayThongBao", SqlDbType.DateTime, "NgayThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViThongBao", SqlDbType.NVarChar, "DonViThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonVi", SqlDbType.NVarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViXNK", SqlDbType.NVarChar, "TenDonViXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViXNK", SqlDbType.NVarChar, "MaDonViXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiXNK", SqlDbType.NVarChar, "DiaChiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidString", SqlDbType.VarChar, "GuidString", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KDT_THUPHI_THONGBAO Load(long id)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KDT_THUPHI_THONGBAO> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KDT_THUPHI_THONGBAO> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KDT_THUPHI_THONGBAO> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<T_KDT_THUPHI_THONGBAO> SelectCollectionBy_TK_ID(long tK_ID)
		{
            IDataReader reader = SelectReaderBy_TK_ID(tK_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TK_ID(long tK_ID)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_SelectBy_TK_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TK_ID", SqlDbType.BigInt, tK_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TK_ID(long tK_ID)
		{
			const string spName = "p_T_KDT_THUPHI_THONGBAO_SelectBy_TK_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TK_ID", SqlDbType.BigInt, tK_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KDT_THUPHI_THONGBAO(long tK_ID, int trangThai, string soThongBao, DateTime ngayThongBao, string donViThongBao, string maDonVi, string tenDonVi, string diaChi, string tenDonViXNK, string maDonViXNK, string diaChiXNK, string guidString)
		{
			T_KDT_THUPHI_THONGBAO entity = new T_KDT_THUPHI_THONGBAO();	
			entity.TK_ID = tK_ID;
			entity.TrangThai = trangThai;
			entity.SoThongBao = soThongBao;
			entity.NgayThongBao = ngayThongBao;
			entity.DonViThongBao = donViThongBao;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.DiaChi = diaChi;
			entity.TenDonViXNK = tenDonViXNK;
			entity.MaDonViXNK = maDonViXNK;
			entity.DiaChiXNK = diaChiXNK;
			entity.GuidString = guidString;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TK_ID", SqlDbType.BigInt, TK_ID);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@SoThongBao", SqlDbType.NVarChar, SoThongBao);
			db.AddInParameter(dbCommand, "@NgayThongBao", SqlDbType.DateTime, NgayThongBao.Year <= 1753 ? DBNull.Value : (object) NgayThongBao);
			db.AddInParameter(dbCommand, "@DonViThongBao", SqlDbType.NVarChar, DonViThongBao);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.NVarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@TenDonViXNK", SqlDbType.NVarChar, TenDonViXNK);
			db.AddInParameter(dbCommand, "@MaDonViXNK", SqlDbType.NVarChar, MaDonViXNK);
			db.AddInParameter(dbCommand, "@DiaChiXNK", SqlDbType.NVarChar, DiaChiXNK);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.VarChar, GuidString);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KDT_THUPHI_THONGBAO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_THONGBAO item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KDT_THUPHI_THONGBAO(long id, long tK_ID, int trangThai, string soThongBao, DateTime ngayThongBao, string donViThongBao, string maDonVi, string tenDonVi, string diaChi, string tenDonViXNK, string maDonViXNK, string diaChiXNK, string guidString)
		{
			T_KDT_THUPHI_THONGBAO entity = new T_KDT_THUPHI_THONGBAO();			
			entity.ID = id;
			entity.TK_ID = tK_ID;
			entity.TrangThai = trangThai;
			entity.SoThongBao = soThongBao;
			entity.NgayThongBao = ngayThongBao;
			entity.DonViThongBao = donViThongBao;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.DiaChi = diaChi;
			entity.TenDonViXNK = tenDonViXNK;
			entity.MaDonViXNK = maDonViXNK;
			entity.DiaChiXNK = diaChiXNK;
			entity.GuidString = guidString;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KDT_THUPHI_THONGBAO_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TK_ID", SqlDbType.BigInt, TK_ID);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@SoThongBao", SqlDbType.NVarChar, SoThongBao);
			db.AddInParameter(dbCommand, "@NgayThongBao", SqlDbType.DateTime, NgayThongBao.Year <= 1753 ? DBNull.Value : (object) NgayThongBao);
			db.AddInParameter(dbCommand, "@DonViThongBao", SqlDbType.NVarChar, DonViThongBao);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.NVarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@TenDonViXNK", SqlDbType.NVarChar, TenDonViXNK);
			db.AddInParameter(dbCommand, "@MaDonViXNK", SqlDbType.NVarChar, MaDonViXNK);
			db.AddInParameter(dbCommand, "@DiaChiXNK", SqlDbType.NVarChar, DiaChiXNK);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.VarChar, GuidString);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KDT_THUPHI_THONGBAO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_THONGBAO item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KDT_THUPHI_THONGBAO(long id, long tK_ID, int trangThai, string soThongBao, DateTime ngayThongBao, string donViThongBao, string maDonVi, string tenDonVi, string diaChi, string tenDonViXNK, string maDonViXNK, string diaChiXNK, string guidString)
		{
			T_KDT_THUPHI_THONGBAO entity = new T_KDT_THUPHI_THONGBAO();			
			entity.ID = id;
			entity.TK_ID = tK_ID;
			entity.TrangThai = trangThai;
			entity.SoThongBao = soThongBao;
			entity.NgayThongBao = ngayThongBao;
			entity.DonViThongBao = donViThongBao;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.DiaChi = diaChi;
			entity.TenDonViXNK = tenDonViXNK;
			entity.MaDonViXNK = maDonViXNK;
			entity.DiaChiXNK = diaChiXNK;
			entity.GuidString = guidString;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TK_ID", SqlDbType.BigInt, TK_ID);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@SoThongBao", SqlDbType.NVarChar, SoThongBao);
			db.AddInParameter(dbCommand, "@NgayThongBao", SqlDbType.DateTime, NgayThongBao.Year <= 1753 ? DBNull.Value : (object) NgayThongBao);
			db.AddInParameter(dbCommand, "@DonViThongBao", SqlDbType.NVarChar, DonViThongBao);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.NVarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@TenDonViXNK", SqlDbType.NVarChar, TenDonViXNK);
			db.AddInParameter(dbCommand, "@MaDonViXNK", SqlDbType.NVarChar, MaDonViXNK);
			db.AddInParameter(dbCommand, "@DiaChiXNK", SqlDbType.NVarChar, DiaChiXNK);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.VarChar, GuidString);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KDT_THUPHI_THONGBAO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_THONGBAO item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KDT_THUPHI_THONGBAO(long id)
		{
			T_KDT_THUPHI_THONGBAO entity = new T_KDT_THUPHI_THONGBAO();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TK_ID(long tK_ID)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_DeleteBy_TK_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TK_ID", SqlDbType.BigInt, tK_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KDT_THUPHI_THONGBAO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_THONGBAO item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }

                    foreach (T_KDT_THUPHI_THONGBAO_FILE item in FileCollection)
                    {
                        item.TB_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    foreach (T_KDT_THUPHI_THONGBAO_PHI item in PhiCollection)
                    {
                        item.TB_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
	}	
}