namespace Company.GC.BLL
{
    public partial class DoanhNghiep : EntityBase
    {
        protected string _MaDoanhNghiep = string.Empty;
        protected string _MatKhau = string.Empty;
        protected string _TenDoanhNghiep = string.Empty;
        protected string _DiaChi = string.Empty;
        protected string _DienThoai = string.Empty;
        protected string _Fax = string.Empty;

        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }

        public string MatKhau
        {
            set { this._MatKhau = value; }
            get { return this._MatKhau; }
        }

        public string TenDoanhNghiep
        {
            set { this._TenDoanhNghiep = value; }
            get { return this._TenDoanhNghiep; }
        }

        public string DiaChi
        {
            set { this._DiaChi = value; }
            get { return this._DiaChi; }
        }

        public string DienThoai
        {
            set { this._DienThoai = value; }
            get { return this._DienThoai; }
        }

        public string Fax
        {
            set { this._Fax = value; }
            get { return this._Fax; }
        }
    }
}