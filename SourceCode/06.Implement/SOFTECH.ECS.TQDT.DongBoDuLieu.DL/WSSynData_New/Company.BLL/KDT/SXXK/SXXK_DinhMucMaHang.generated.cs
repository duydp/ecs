﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class SXXK_DinhMucMaHang : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long LanThanhLy { set; get; }
		public long Master_ID { set; get; }
		public string TenKH { set; get; }
		public string PO { set; get; }
		public string Style { set; get; }
		public string MaSP { set; get; }
		public string MatHang { set; get; }
		public long SoLuongPO { set; get; }
		public decimal SLXuat { set; get; }
		public decimal ChenhLech { set; get; }
		public string MaNPL { set; get; }
		public string TenNPL { set; get; }
		public string Art { set; get; }
		public string DVT_NPL { set; get; }
		public string NCC { set; get; }
		public decimal SPTieuThu { set; get; }
		public decimal DinhMuc { set; get; }
		public decimal TyLeHaoHut { set; get; }
		public decimal DinhMuc_HaoHut { set; get; }
		public decimal NhuCauXuat { set; get; }
		public decimal XuatKho { set; get; }
		public decimal ThuHoi { set; get; }
		public decimal ThucXuatKho { set; get; }
		public decimal DinhMucThucTe { set; get; }
		public decimal SoTKN_VNACCS { set; get; }
		public string Invoid_HD { set; get; }
		public DateTime NgayChungTu { set; get; }
		public decimal SoLuongNhapKho { set; get; }
		public decimal SoLuongSuDung { set; get; }
		public decimal SoLuongThanhKhoan { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<SXXK_DinhMucMaHang> ConvertToCollection(IDataReader reader)
		{
			List<SXXK_DinhMucMaHang> collection = new List<SXXK_DinhMucMaHang>();
			while (reader.Read())
			{
				SXXK_DinhMucMaHang entity = new SXXK_DinhMucMaHang();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt64(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("PO"))) entity.PO = reader.GetString(reader.GetOrdinal("PO"));
				if (!reader.IsDBNull(reader.GetOrdinal("Style"))) entity.Style = reader.GetString(reader.GetOrdinal("Style"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("MatHang"))) entity.MatHang = reader.GetString(reader.GetOrdinal("MatHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPO"))) entity.SoLuongPO = reader.GetInt64(reader.GetOrdinal("SoLuongPO"));
				if (!reader.IsDBNull(reader.GetOrdinal("SLXuat"))) entity.SLXuat = reader.GetDecimal(reader.GetOrdinal("SLXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChenhLech"))) entity.ChenhLech = reader.GetDecimal(reader.GetOrdinal("ChenhLech"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("Art"))) entity.Art = reader.GetString(reader.GetOrdinal("Art"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_NPL"))) entity.DVT_NPL = reader.GetString(reader.GetOrdinal("DVT_NPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NCC"))) entity.NCC = reader.GetString(reader.GetOrdinal("NCC"));
				if (!reader.IsDBNull(reader.GetOrdinal("SPTieuThu"))) entity.SPTieuThu = reader.GetDecimal(reader.GetOrdinal("SPTieuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetDecimal(reader.GetOrdinal("DinhMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc_HaoHut"))) entity.DinhMuc_HaoHut = reader.GetDecimal(reader.GetOrdinal("DinhMuc_HaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhuCauXuat"))) entity.NhuCauXuat = reader.GetDecimal(reader.GetOrdinal("NhuCauXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("XuatKho"))) entity.XuatKho = reader.GetDecimal(reader.GetOrdinal("XuatKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThuHoi"))) entity.ThuHoi = reader.GetDecimal(reader.GetOrdinal("ThuHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThucXuatKho"))) entity.ThucXuatKho = reader.GetDecimal(reader.GetOrdinal("ThucXuatKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucThucTe"))) entity.DinhMucThucTe = reader.GetDecimal(reader.GetOrdinal("DinhMucThucTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKN_VNACCS"))) entity.SoTKN_VNACCS = reader.GetDecimal(reader.GetOrdinal("SoTKN_VNACCS"));
				if (!reader.IsDBNull(reader.GetOrdinal("Invoid_HD"))) entity.Invoid_HD = reader.GetString(reader.GetOrdinal("Invoid_HD"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungTu"))) entity.NgayChungTu = reader.GetDateTime(reader.GetOrdinal("NgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongNhapKho"))) entity.SoLuongNhapKho = reader.GetDecimal(reader.GetOrdinal("SoLuongNhapKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongSuDung"))) entity.SoLuongSuDung = reader.GetDecimal(reader.GetOrdinal("SoLuongSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongThanhKhoan"))) entity.SoLuongThanhKhoan = reader.GetDecimal(reader.GetOrdinal("SoLuongThanhKhoan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<SXXK_DinhMucMaHang> collection, long id)
        {
            foreach (SXXK_DinhMucMaHang item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_SXXK_DinhMucMaHang VALUES(@LanThanhLy, @Master_ID, @TenKH, @PO, @Style, @MaSP, @MatHang, @SoLuongPO, @SLXuat, @ChenhLech, @MaNPL, @TenNPL, @Art, @DVT_NPL, @NCC, @SPTieuThu, @DinhMuc, @TyLeHaoHut, @DinhMuc_HaoHut, @NhuCauXuat, @XuatKho, @ThuHoi, @ThucXuatKho, @DinhMucThucTe, @SoTKN_VNACCS, @Invoid_HD, @NgayChungTu, @SoLuongNhapKho, @SoLuongSuDung, @SoLuongThanhKhoan)";
            string update = "UPDATE t_SXXK_DinhMucMaHang SET LanThanhLy = @LanThanhLy, Master_ID = @Master_ID, TenKH = @TenKH, PO = @PO, Style = @Style, MaSP = @MaSP, MatHang = @MatHang, SoLuongPO = @SoLuongPO, SLXuat = @SLXuat, ChenhLech = @ChenhLech, MaNPL = @MaNPL, TenNPL = @TenNPL, Art = @Art, DVT_NPL = @DVT_NPL, NCC = @NCC, SPTieuThu = @SPTieuThu, DinhMuc = @DinhMuc, TyLeHaoHut = @TyLeHaoHut, DinhMuc_HaoHut = @DinhMuc_HaoHut, NhuCauXuat = @NhuCauXuat, XuatKho = @XuatKho, ThuHoi = @ThuHoi, ThucXuatKho = @ThucXuatKho, DinhMucThucTe = @DinhMucThucTe, SoTKN_VNACCS = @SoTKN_VNACCS, Invoid_HD = @Invoid_HD, NgayChungTu = @NgayChungTu, SoLuongNhapKho = @SoLuongNhapKho, SoLuongSuDung = @SoLuongSuDung, SoLuongThanhKhoan = @SoLuongThanhKhoan WHERE ID = @ID";
            string delete = "DELETE FROM t_SXXK_DinhMucMaHang WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LanThanhLy", SqlDbType.BigInt, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKH", SqlDbType.NVarChar, "TenKH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PO", SqlDbType.VarChar, "PO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Style", SqlDbType.VarChar, "Style", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MatHang", SqlDbType.NVarChar, "MatHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongPO", SqlDbType.BigInt, "SoLuongPO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SLXuat", SqlDbType.Decimal, "SLXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChenhLech", SqlDbType.Decimal, "ChenhLech", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Art", SqlDbType.VarChar, "Art", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_NPL", SqlDbType.VarChar, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NCC", SqlDbType.NVarChar, "NCC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SPTieuThu", SqlDbType.Decimal, "SPTieuThu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMuc", SqlDbType.Decimal, "DinhMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMuc_HaoHut", SqlDbType.Decimal, "DinhMuc_HaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhuCauXuat", SqlDbType.Decimal, "NhuCauXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XuatKho", SqlDbType.Decimal, "XuatKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThuHoi", SqlDbType.Decimal, "ThuHoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThucXuatKho", SqlDbType.Decimal, "ThucXuatKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucThucTe", SqlDbType.Decimal, "DinhMucThucTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, "SoTKN_VNACCS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Invoid_HD", SqlDbType.VarChar, "Invoid_HD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongNhapKho", SqlDbType.Decimal, "SoLuongNhapKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongSuDung", SqlDbType.Decimal, "SoLuongSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, "SoLuongThanhKhoan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LanThanhLy", SqlDbType.BigInt, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKH", SqlDbType.NVarChar, "TenKH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PO", SqlDbType.VarChar, "PO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Style", SqlDbType.VarChar, "Style", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MatHang", SqlDbType.NVarChar, "MatHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongPO", SqlDbType.BigInt, "SoLuongPO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SLXuat", SqlDbType.Decimal, "SLXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChenhLech", SqlDbType.Decimal, "ChenhLech", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Art", SqlDbType.VarChar, "Art", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_NPL", SqlDbType.VarChar, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NCC", SqlDbType.NVarChar, "NCC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SPTieuThu", SqlDbType.Decimal, "SPTieuThu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMuc", SqlDbType.Decimal, "DinhMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMuc_HaoHut", SqlDbType.Decimal, "DinhMuc_HaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhuCauXuat", SqlDbType.Decimal, "NhuCauXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XuatKho", SqlDbType.Decimal, "XuatKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThuHoi", SqlDbType.Decimal, "ThuHoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThucXuatKho", SqlDbType.Decimal, "ThucXuatKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucThucTe", SqlDbType.Decimal, "DinhMucThucTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, "SoTKN_VNACCS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Invoid_HD", SqlDbType.VarChar, "Invoid_HD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongNhapKho", SqlDbType.Decimal, "SoLuongNhapKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongSuDung", SqlDbType.Decimal, "SoLuongSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, "SoLuongThanhKhoan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_SXXK_DinhMucMaHang VALUES(@LanThanhLy, @Master_ID, @TenKH, @PO, @Style, @MaSP, @MatHang, @SoLuongPO, @SLXuat, @ChenhLech, @MaNPL, @TenNPL, @Art, @DVT_NPL, @NCC, @SPTieuThu, @DinhMuc, @TyLeHaoHut, @DinhMuc_HaoHut, @NhuCauXuat, @XuatKho, @ThuHoi, @ThucXuatKho, @DinhMucThucTe, @SoTKN_VNACCS, @Invoid_HD, @NgayChungTu, @SoLuongNhapKho, @SoLuongSuDung, @SoLuongThanhKhoan)";
            string update = "UPDATE t_SXXK_DinhMucMaHang SET LanThanhLy = @LanThanhLy, Master_ID = @Master_ID, TenKH = @TenKH, PO = @PO, Style = @Style, MaSP = @MaSP, MatHang = @MatHang, SoLuongPO = @SoLuongPO, SLXuat = @SLXuat, ChenhLech = @ChenhLech, MaNPL = @MaNPL, TenNPL = @TenNPL, Art = @Art, DVT_NPL = @DVT_NPL, NCC = @NCC, SPTieuThu = @SPTieuThu, DinhMuc = @DinhMuc, TyLeHaoHut = @TyLeHaoHut, DinhMuc_HaoHut = @DinhMuc_HaoHut, NhuCauXuat = @NhuCauXuat, XuatKho = @XuatKho, ThuHoi = @ThuHoi, ThucXuatKho = @ThucXuatKho, DinhMucThucTe = @DinhMucThucTe, SoTKN_VNACCS = @SoTKN_VNACCS, Invoid_HD = @Invoid_HD, NgayChungTu = @NgayChungTu, SoLuongNhapKho = @SoLuongNhapKho, SoLuongSuDung = @SoLuongSuDung, SoLuongThanhKhoan = @SoLuongThanhKhoan WHERE ID = @ID";
            string delete = "DELETE FROM t_SXXK_DinhMucMaHang WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LanThanhLy", SqlDbType.BigInt, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKH", SqlDbType.NVarChar, "TenKH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PO", SqlDbType.VarChar, "PO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Style", SqlDbType.VarChar, "Style", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MatHang", SqlDbType.NVarChar, "MatHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongPO", SqlDbType.BigInt, "SoLuongPO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SLXuat", SqlDbType.Decimal, "SLXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChenhLech", SqlDbType.Decimal, "ChenhLech", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Art", SqlDbType.VarChar, "Art", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_NPL", SqlDbType.VarChar, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NCC", SqlDbType.NVarChar, "NCC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SPTieuThu", SqlDbType.Decimal, "SPTieuThu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMuc", SqlDbType.Decimal, "DinhMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMuc_HaoHut", SqlDbType.Decimal, "DinhMuc_HaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhuCauXuat", SqlDbType.Decimal, "NhuCauXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XuatKho", SqlDbType.Decimal, "XuatKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThuHoi", SqlDbType.Decimal, "ThuHoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThucXuatKho", SqlDbType.Decimal, "ThucXuatKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucThucTe", SqlDbType.Decimal, "DinhMucThucTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, "SoTKN_VNACCS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Invoid_HD", SqlDbType.VarChar, "Invoid_HD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongNhapKho", SqlDbType.Decimal, "SoLuongNhapKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongSuDung", SqlDbType.Decimal, "SoLuongSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, "SoLuongThanhKhoan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LanThanhLy", SqlDbType.BigInt, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKH", SqlDbType.NVarChar, "TenKH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PO", SqlDbType.VarChar, "PO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Style", SqlDbType.VarChar, "Style", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MatHang", SqlDbType.NVarChar, "MatHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongPO", SqlDbType.BigInt, "SoLuongPO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SLXuat", SqlDbType.Decimal, "SLXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChenhLech", SqlDbType.Decimal, "ChenhLech", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Art", SqlDbType.VarChar, "Art", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_NPL", SqlDbType.VarChar, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NCC", SqlDbType.NVarChar, "NCC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SPTieuThu", SqlDbType.Decimal, "SPTieuThu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMuc", SqlDbType.Decimal, "DinhMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMuc_HaoHut", SqlDbType.Decimal, "DinhMuc_HaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhuCauXuat", SqlDbType.Decimal, "NhuCauXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XuatKho", SqlDbType.Decimal, "XuatKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThuHoi", SqlDbType.Decimal, "ThuHoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThucXuatKho", SqlDbType.Decimal, "ThucXuatKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucThucTe", SqlDbType.Decimal, "DinhMucThucTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, "SoTKN_VNACCS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Invoid_HD", SqlDbType.VarChar, "Invoid_HD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongNhapKho", SqlDbType.Decimal, "SoLuongNhapKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongSuDung", SqlDbType.Decimal, "SoLuongSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, "SoLuongThanhKhoan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static SXXK_DinhMucMaHang Load(long id)
		{
			const string spName = "[dbo].[p_SXXK_DinhMucMaHang_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<SXXK_DinhMucMaHang> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<SXXK_DinhMucMaHang> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<SXXK_DinhMucMaHang> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_SXXK_DinhMucMaHang_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SXXK_DinhMucMaHang_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
        public static DataSet SelectDynamicDMRegisted(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_SXXK_DinhMucMaHang_SelectDynamicDMRegisted]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_SXXK_DinhMucMaHang_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SXXK_DinhMucMaHang_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertSXXK_DinhMucMaHang(long lanThanhLy, long master_ID, string tenKH, string pO, string style, string maSP, string matHang, long soLuongPO, decimal sLXuat, decimal chenhLech, string maNPL, string tenNPL, string art, string dVT_NPL, string nCC, decimal sPTieuThu, decimal dinhMuc, decimal tyLeHaoHut, decimal dinhMuc_HaoHut, decimal nhuCauXuat, decimal xuatKho, decimal thuHoi, decimal thucXuatKho, decimal dinhMucThucTe, decimal soTKN_VNACCS, string invoid_HD, DateTime ngayChungTu, decimal soLuongNhapKho, decimal soLuongSuDung, decimal soLuongThanhKhoan)
		{
			SXXK_DinhMucMaHang entity = new SXXK_DinhMucMaHang();	
			entity.LanThanhLy = lanThanhLy;
			entity.Master_ID = master_ID;
			entity.TenKH = tenKH;
			entity.PO = pO;
			entity.Style = style;
			entity.MaSP = maSP;
			entity.MatHang = matHang;
			entity.SoLuongPO = soLuongPO;
			entity.SLXuat = sLXuat;
			entity.ChenhLech = chenhLech;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.Art = art;
			entity.DVT_NPL = dVT_NPL;
			entity.NCC = nCC;
			entity.SPTieuThu = sPTieuThu;
			entity.DinhMuc = dinhMuc;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.DinhMuc_HaoHut = dinhMuc_HaoHut;
			entity.NhuCauXuat = nhuCauXuat;
			entity.XuatKho = xuatKho;
			entity.ThuHoi = thuHoi;
			entity.ThucXuatKho = thucXuatKho;
			entity.DinhMucThucTe = dinhMucThucTe;
			entity.SoTKN_VNACCS = soTKN_VNACCS;
			entity.Invoid_HD = invoid_HD;
			entity.NgayChungTu = ngayChungTu;
			entity.SoLuongNhapKho = soLuongNhapKho;
			entity.SoLuongSuDung = soLuongSuDung;
			entity.SoLuongThanhKhoan = soLuongThanhKhoan;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_SXXK_DinhMucMaHang_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.BigInt, LanThanhLy);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
			db.AddInParameter(dbCommand, "@PO", SqlDbType.VarChar, PO);
			db.AddInParameter(dbCommand, "@Style", SqlDbType.VarChar, Style);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@MatHang", SqlDbType.NVarChar, MatHang);
			db.AddInParameter(dbCommand, "@SoLuongPO", SqlDbType.BigInt, SoLuongPO);
			db.AddInParameter(dbCommand, "@SLXuat", SqlDbType.Decimal, SLXuat);
			db.AddInParameter(dbCommand, "@ChenhLech", SqlDbType.Decimal, ChenhLech);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@Art", SqlDbType.VarChar, Art);
			db.AddInParameter(dbCommand, "@DVT_NPL", SqlDbType.VarChar, DVT_NPL);
			db.AddInParameter(dbCommand, "@NCC", SqlDbType.NVarChar, NCC);
			db.AddInParameter(dbCommand, "@SPTieuThu", SqlDbType.Decimal, SPTieuThu);
			db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, DinhMuc);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@DinhMuc_HaoHut", SqlDbType.Decimal, DinhMuc_HaoHut);
			db.AddInParameter(dbCommand, "@NhuCauXuat", SqlDbType.Decimal, NhuCauXuat);
			db.AddInParameter(dbCommand, "@XuatKho", SqlDbType.Decimal, XuatKho);
			db.AddInParameter(dbCommand, "@ThuHoi", SqlDbType.Decimal, ThuHoi);
			db.AddInParameter(dbCommand, "@ThucXuatKho", SqlDbType.Decimal, ThucXuatKho);
			db.AddInParameter(dbCommand, "@DinhMucThucTe", SqlDbType.Decimal, DinhMucThucTe);
			db.AddInParameter(dbCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, SoTKN_VNACCS);
			db.AddInParameter(dbCommand, "@Invoid_HD", SqlDbType.VarChar, Invoid_HD);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@SoLuongNhapKho", SqlDbType.Decimal, SoLuongNhapKho);
			db.AddInParameter(dbCommand, "@SoLuongSuDung", SqlDbType.Decimal, SoLuongSuDung);
			db.AddInParameter(dbCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, SoLuongThanhKhoan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<SXXK_DinhMucMaHang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_DinhMucMaHang item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateSXXK_DinhMucMaHang(long id, long lanThanhLy, long master_ID, string tenKH, string pO, string style, string maSP, string matHang, long soLuongPO, decimal sLXuat, decimal chenhLech, string maNPL, string tenNPL, string art, string dVT_NPL, string nCC, decimal sPTieuThu, decimal dinhMuc, decimal tyLeHaoHut, decimal dinhMuc_HaoHut, decimal nhuCauXuat, decimal xuatKho, decimal thuHoi, decimal thucXuatKho, decimal dinhMucThucTe, decimal soTKN_VNACCS, string invoid_HD, DateTime ngayChungTu, decimal soLuongNhapKho, decimal soLuongSuDung, decimal soLuongThanhKhoan)
		{
			SXXK_DinhMucMaHang entity = new SXXK_DinhMucMaHang();			
			entity.ID = id;
			entity.LanThanhLy = lanThanhLy;
			entity.Master_ID = master_ID;
			entity.TenKH = tenKH;
			entity.PO = pO;
			entity.Style = style;
			entity.MaSP = maSP;
			entity.MatHang = matHang;
			entity.SoLuongPO = soLuongPO;
			entity.SLXuat = sLXuat;
			entity.ChenhLech = chenhLech;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.Art = art;
			entity.DVT_NPL = dVT_NPL;
			entity.NCC = nCC;
			entity.SPTieuThu = sPTieuThu;
			entity.DinhMuc = dinhMuc;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.DinhMuc_HaoHut = dinhMuc_HaoHut;
			entity.NhuCauXuat = nhuCauXuat;
			entity.XuatKho = xuatKho;
			entity.ThuHoi = thuHoi;
			entity.ThucXuatKho = thucXuatKho;
			entity.DinhMucThucTe = dinhMucThucTe;
			entity.SoTKN_VNACCS = soTKN_VNACCS;
			entity.Invoid_HD = invoid_HD;
			entity.NgayChungTu = ngayChungTu;
			entity.SoLuongNhapKho = soLuongNhapKho;
			entity.SoLuongSuDung = soLuongSuDung;
			entity.SoLuongThanhKhoan = soLuongThanhKhoan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_SXXK_DinhMucMaHang_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.BigInt, LanThanhLy);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
			db.AddInParameter(dbCommand, "@PO", SqlDbType.VarChar, PO);
			db.AddInParameter(dbCommand, "@Style", SqlDbType.VarChar, Style);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@MatHang", SqlDbType.NVarChar, MatHang);
			db.AddInParameter(dbCommand, "@SoLuongPO", SqlDbType.BigInt, SoLuongPO);
			db.AddInParameter(dbCommand, "@SLXuat", SqlDbType.Decimal, SLXuat);
			db.AddInParameter(dbCommand, "@ChenhLech", SqlDbType.Decimal, ChenhLech);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@Art", SqlDbType.VarChar, Art);
			db.AddInParameter(dbCommand, "@DVT_NPL", SqlDbType.VarChar, DVT_NPL);
			db.AddInParameter(dbCommand, "@NCC", SqlDbType.NVarChar, NCC);
			db.AddInParameter(dbCommand, "@SPTieuThu", SqlDbType.Decimal, SPTieuThu);
			db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, DinhMuc);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@DinhMuc_HaoHut", SqlDbType.Decimal, DinhMuc_HaoHut);
			db.AddInParameter(dbCommand, "@NhuCauXuat", SqlDbType.Decimal, NhuCauXuat);
			db.AddInParameter(dbCommand, "@XuatKho", SqlDbType.Decimal, XuatKho);
			db.AddInParameter(dbCommand, "@ThuHoi", SqlDbType.Decimal, ThuHoi);
			db.AddInParameter(dbCommand, "@ThucXuatKho", SqlDbType.Decimal, ThucXuatKho);
			db.AddInParameter(dbCommand, "@DinhMucThucTe", SqlDbType.Decimal, DinhMucThucTe);
			db.AddInParameter(dbCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, SoTKN_VNACCS);
			db.AddInParameter(dbCommand, "@Invoid_HD", SqlDbType.VarChar, Invoid_HD);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@SoLuongNhapKho", SqlDbType.Decimal, SoLuongNhapKho);
			db.AddInParameter(dbCommand, "@SoLuongSuDung", SqlDbType.Decimal, SoLuongSuDung);
			db.AddInParameter(dbCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, SoLuongThanhKhoan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<SXXK_DinhMucMaHang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_DinhMucMaHang item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateSXXK_DinhMucMaHang(long id, long lanThanhLy, long master_ID, string tenKH, string pO, string style, string maSP, string matHang, long soLuongPO, decimal sLXuat, decimal chenhLech, string maNPL, string tenNPL, string art, string dVT_NPL, string nCC, decimal sPTieuThu, decimal dinhMuc, decimal tyLeHaoHut, decimal dinhMuc_HaoHut, decimal nhuCauXuat, decimal xuatKho, decimal thuHoi, decimal thucXuatKho, decimal dinhMucThucTe, decimal soTKN_VNACCS, string invoid_HD, DateTime ngayChungTu, decimal soLuongNhapKho, decimal soLuongSuDung, decimal soLuongThanhKhoan)
		{
			SXXK_DinhMucMaHang entity = new SXXK_DinhMucMaHang();			
			entity.ID = id;
			entity.LanThanhLy = lanThanhLy;
			entity.Master_ID = master_ID;
			entity.TenKH = tenKH;
			entity.PO = pO;
			entity.Style = style;
			entity.MaSP = maSP;
			entity.MatHang = matHang;
			entity.SoLuongPO = soLuongPO;
			entity.SLXuat = sLXuat;
			entity.ChenhLech = chenhLech;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.Art = art;
			entity.DVT_NPL = dVT_NPL;
			entity.NCC = nCC;
			entity.SPTieuThu = sPTieuThu;
			entity.DinhMuc = dinhMuc;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.DinhMuc_HaoHut = dinhMuc_HaoHut;
			entity.NhuCauXuat = nhuCauXuat;
			entity.XuatKho = xuatKho;
			entity.ThuHoi = thuHoi;
			entity.ThucXuatKho = thucXuatKho;
			entity.DinhMucThucTe = dinhMucThucTe;
			entity.SoTKN_VNACCS = soTKN_VNACCS;
			entity.Invoid_HD = invoid_HD;
			entity.NgayChungTu = ngayChungTu;
			entity.SoLuongNhapKho = soLuongNhapKho;
			entity.SoLuongSuDung = soLuongSuDung;
			entity.SoLuongThanhKhoan = soLuongThanhKhoan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
        //
        public static void UpdateTransaction(SqlTransaction transaction, List<SXXK_DinhMucMaHang> collection)
        {
            try
            {
                foreach (SXXK_DinhMucMaHang item in collection)
                {
                    item.Update(transaction);
                    
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }

        }
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SXXK_DinhMucMaHang_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.BigInt, LanThanhLy);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
			db.AddInParameter(dbCommand, "@PO", SqlDbType.VarChar, PO);
			db.AddInParameter(dbCommand, "@Style", SqlDbType.VarChar, Style);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@MatHang", SqlDbType.NVarChar, MatHang);
			db.AddInParameter(dbCommand, "@SoLuongPO", SqlDbType.BigInt, SoLuongPO);
			db.AddInParameter(dbCommand, "@SLXuat", SqlDbType.Decimal, SLXuat);
			db.AddInParameter(dbCommand, "@ChenhLech", SqlDbType.Decimal, ChenhLech);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@Art", SqlDbType.VarChar, Art);
			db.AddInParameter(dbCommand, "@DVT_NPL", SqlDbType.VarChar, DVT_NPL);
			db.AddInParameter(dbCommand, "@NCC", SqlDbType.NVarChar, NCC);
			db.AddInParameter(dbCommand, "@SPTieuThu", SqlDbType.Decimal, SPTieuThu);
			db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, DinhMuc);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@DinhMuc_HaoHut", SqlDbType.Decimal, DinhMuc_HaoHut);
			db.AddInParameter(dbCommand, "@NhuCauXuat", SqlDbType.Decimal, NhuCauXuat);
			db.AddInParameter(dbCommand, "@XuatKho", SqlDbType.Decimal, XuatKho);
			db.AddInParameter(dbCommand, "@ThuHoi", SqlDbType.Decimal, ThuHoi);
			db.AddInParameter(dbCommand, "@ThucXuatKho", SqlDbType.Decimal, ThucXuatKho);
			db.AddInParameter(dbCommand, "@DinhMucThucTe", SqlDbType.Decimal, DinhMucThucTe);
			db.AddInParameter(dbCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, SoTKN_VNACCS);
			db.AddInParameter(dbCommand, "@Invoid_HD", SqlDbType.VarChar, Invoid_HD);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@SoLuongNhapKho", SqlDbType.Decimal, SoLuongNhapKho);
			db.AddInParameter(dbCommand, "@SoLuongSuDung", SqlDbType.Decimal, SoLuongSuDung);
			db.AddInParameter(dbCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, SoLuongThanhKhoan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<SXXK_DinhMucMaHang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_DinhMucMaHang item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteSXXK_DinhMucMaHang(long id)
		{
			SXXK_DinhMucMaHang entity = new SXXK_DinhMucMaHang();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SXXK_DinhMucMaHang_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_SXXK_DinhMucMaHang_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<SXXK_DinhMucMaHang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_DinhMucMaHang item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}