﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class DonViTinhQuyDoi
    {
        public bool _IsUse = true;
        /// <summary>
        /// biến tạm - không lưu vào database (using like a Flag)
        /// </summary>
        public bool IsUse { get { return _IsUse; } set { _IsUse = value; } }

    }
}
