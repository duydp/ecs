IF OBJECT_ID('t_KDT_VNACC_ToKhaiMauDich_Log') IS NOT NULL
DROP TABLE t_KDT_VNACC_ToKhaiMauDich_Log
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_Log]
(
[ID] [bigint]  NULL ,
[SoToKhai] [numeric] (12, 0) NULL,
[PhanLoaiBaoCaoSuaDoi] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoToKhaiDauTien] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoNhanhToKhai] [numeric] (2, 0) NULL,
[TongSoTKChiaNho] [numeric] (2, 0) NULL,
[SoToKhaiTNTX] [numeric] (12, 0) NULL,
[MaLoaiHinh] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhanLoaiHH] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhuongThucVT] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLoaiToChuc] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CoQuanHaiQuan] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NhomXuLyHS] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThoiHanTaiNhapTaiXuat] [datetime] NULL,
[NgayDangKy] [datetime] NULL,
[MaDonVi] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDonVi] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaBuuChinhDonVi] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaChiDonVi] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoDienThoaiDonVi] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaUyThac] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenUyThac] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDoiTac] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDoiTac] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaBuuChinhDoiTac] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaChiDoiTac1] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaChiDoiTac2] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaChiDoiTac3] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaChiDoiTac4] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNuocDoiTac] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NguoiUyThacXK] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDaiLyHQ] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong] [numeric] (8, 0) NULL,
[MaDVTSoLuong] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrongLuong] [numeric] (14, 4) NULL,
[MaDVTTrongLuong] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDDLuuKho] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoHieuKyHieu] [varchar] (140) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPTVC] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenPTVC] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHangDen] [datetime] NULL,
[MaDiaDiemDoHang] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDiaDiemDohang] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDiaDiemXepHang] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDiaDiemXepHang] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuongCont] [numeric] (3, 0) NULL,
[MaKetQuaKiemTra] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaVanbanPhapQuy1] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaVanbanPhapQuy2] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaVanbanPhapQuy3] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaVanbanPhapQuy4] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaVanbanPhapQuy5] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLoaiHD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTiepNhanHD] [numeric] (12, 0) NULL,
[SoHoaDon] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayPhatHanhHD] [datetime] NULL,
[PhuongThucTT] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLoaiGiaHD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDieuKienGiaHD] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTHoaDon] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongTriGiaHD] [numeric] (24, 4) NULL,
[MaPhanLoaiTriGia] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTiepNhanTKTriGia] [numeric] (9, 0) NULL,
[MaTTHieuChinhTriGia] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GiaHieuChinhTriGia] [numeric] (24, 4) NULL,
[MaPhanLoaiPhiVC] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTPhiVC] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhiVanChuyen] [numeric] (22, 4) NULL,
[MaPhanLoaiPhiBH] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTPhiBH] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhiBaoHiem] [numeric] (22, 4) NULL,
[SoDangKyBH] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChiTietKhaiTriGia] [nvarchar] (280) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGiaTinhThue] [numeric] (24, 4) NULL,
[PhanLoaiKhongQDVND] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTTriGiaTinhThue] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongHeSoPhanBoTG] [numeric] (24, 4) NULL,
[MaLyDoDeNghiBP] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NguoiNopThue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNHTraThueThay] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamPhatHanhHM] [numeric] (4, 0) NULL,
[KyHieuCTHanMuc] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoCTHanMuc] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaXDThoiHanNopThue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNHBaoLanh] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamPhatHanhBL] [numeric] (4, 0) NULL,
[KyHieuCTBaoLanh] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoCTBaoLanh] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayNhapKhoDau] [datetime] NULL,
[NgayKhoiHanhVC] [datetime] NULL,
[DiaDiemDichVC] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayDen] [datetime] NULL,
[GhiChu] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoQuanLyNoiBoDN] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhanLoaiKiemTra] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaSoThueDaiDien] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenCoQuanHaiQuan] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayThayDoiDangKy] [datetime] NULL,
[BieuThiTruongHopHetHan] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDaiLyHaiQuan] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNhanVienHaiQuan] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDDLuuKho] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhanLoaiTongGiaCoBan] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLoaiCongThucChuan] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhanLoaiDieuChinhTriGia] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhuongPhapDieuChinhTriGia] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongTienThuePhaiNop] [numeric] (15, 4) NULL,
[SoTienBaoLanh] [numeric] (15, 4) NULL,
[TenTruongDonViHaiQuan] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayCapPhep] [datetime] NULL,
[PhanLoaiThamTraSauThongQuan] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayPheDuyetBP] [datetime] NULL,
[NgayHoanThanhKiemTraBP] [datetime] NULL,
[SoNgayDoiCapPhepNhapKhau] [numeric] (2, 0) NULL,
[TieuDe] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaSacThueAnHan_VAT] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenSacThueAnHan_VAT] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HanNopThueSauKhiAnHan_VAT] [datetime] NULL,
[PhanLoaiNopThue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongSoTienThueXuatKhau] [numeric] (15, 4) NULL,
[MaTTTongTienThueXuatKhau] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongSoTienLePhi] [numeric] (15, 4) NULL,
[MaTTCuaSoTienBaoLanh] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoQuanLyNguoiSuDung] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHoanThanhKiemTra] [datetime] NULL,
[TongSoTrangCuaToKhai] [numeric] (2, 0) NULL,
[TongSoDongHangCuaToKhai] [numeric] (2, 0) NULL,
[NgayKhaiBaoNopThue] [datetime] NULL DEFAULT GETDATE(),
[TrangThaiXuLy] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InputMessageID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTag] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexTag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HopDong_ID] [bigint] NULL ,
[HopDong_So] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiHang] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Templ_1] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS ,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_ToKhaiMauDich_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_ToKhaiMauDich_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_ToKhaiMauDich_Log 
ON dbo.t_KDT_VNACC_ToKhaiMauDich
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_ToKhaiMauDich_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_ToKhaiMauDich_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_ToKhaiMauDich_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACC_HangMauDich_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_HangMauDich_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACC_HangMauDich_Log]
(
[ID] [bigint]  NULL ,
[TKMD_ID] [bigint] NOT NULL,
[MaSoHang] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaQuanLy] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHang] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ThueSuat] [numeric] (10, 3) NULL,
[ThueSuatTuyetDoi] [numeric] (14, 4) NULL,
[MaDVTTuyetDoi] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTTuyetDoi] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NuocXuatXu] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong1] [numeric] (16, 4) NULL,
[DVTLuong1] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuong2] [numeric] (16, 4) NULL,
[DVTLuong2] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGiaHoaDon] [numeric] (24, 4) NULL,
[DonGiaHoaDon] [numeric] (24, 6) NULL,
[MaTTDonGia] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVTDonGia] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaBieuThueNK] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaHanNgach] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaThueNKTheoLuong] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaMienGiamThue] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienGiamThue] [numeric] (20, 4) NULL,
[TriGiaTinhThue] [numeric] (24, 4) NULL,
[MaTTTriGiaTinhThue] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoMucKhaiKhoanDC] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTTDongHangTKTNTX] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoDMMienThue] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoDongDMMienThue] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaMienGiam] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienMienGiam] [numeric] (20, 4) NULL,
[MaTTSoTienMienGiam] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaVanBanPhapQuyKhac1] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaVanBanPhapQuyKhac2] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaVanBanPhapQuyKhac3] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaVanBanPhapQuyKhac4] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaVanBanPhapQuyKhac5] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoDong] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhanLoaiTaiXacNhanGia] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNoiXuatXu] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuongTinhThue] [numeric] (16, 4) NULL,
[MaDVTDanhThue] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DonGiaTinhThue] [numeric] (22, 4) NULL,
[DV_SL_TrongDonGiaTinhThue] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTDonGiaTinhThue] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGiaTinhThueS] [numeric] (21, 4) NULL,
[MaTTTriGiaTinhThueS] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTSoTienMienGiam1] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhanLoaiThueSuatThue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThueSuatThue] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLoaiThueSuatThue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienThue] [numeric] (20, 4) NULL,
[MaTTSoTienThueXuatKhau] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TienLePhi_DonGia] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TienBaoHiem_DonGia] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TienLePhi_SoLuong] [numeric] (16, 4) NULL,
[TienLePhi_MaDVSoLuong] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TienBaoHiem_SoLuong] [numeric] (16, 4) NULL,
[TienBaoHiem_MaDVSoLuong] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TienLePhi_KhoanTien] [numeric] (20, 4) NULL,
[TienBaoHiem_KhoanTien] [numeric] (20, 4) NULL,
[DieuKhoanMienGiam] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaHangHoa] [nvarchar] (48) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Templ_1] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACC_HangMauDich_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_HangMauDich_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_HangMauDich_Log 
ON dbo.t_KDT_VNACC_HangMauDich
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_VNACC_TK_Container_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_TK_Container_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACC_TK_Container_Log]
(
[ID] [bigint]  NULL,
[TKMD_ID] [bigint] NOT NULL,
[MaDiaDiem1] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDiaDiem2] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDiaDiem3] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDiaDiem4] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDiaDiem5] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDiaDiem] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaChiDiaDiem] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InputMessageID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTag] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexTag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_TK_Container_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_TK_Container_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_TK_Container_Log 
ON dbo.t_KDT_VNACC_TK_Container
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_Container_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_Container_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_Container_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACC_TK_DinhKemDienTu_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_TK_DinhKemDienTu_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACC_TK_DinhKemDienTu_Log]
(
[ID] [bigint]  NULL ,
[TKMD_ID] [bigint] NOT NULL,
[SoTT] [int] NULL,
[PhanLoai] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoDinhKemKhaiBaoDT] [numeric] (12, 0) NULL,
[InputMessageID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTag] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexTag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_TK_DinhKemDienTu_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_TK_DinhKemDienTu_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_TK_DinhKemDienTu_Log 
ON dbo.t_KDT_VNACC_TK_DinhKemDienTu
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_DinhKemDienTu_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_DinhKemDienTu_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_DinhKemDienTu_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO


IF OBJECT_ID('t_KDT_VNACC_TK_GiayPhep_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_TK_GiayPhep_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACC_TK_GiayPhep_Log]
(
[ID] [bigint]  NULL ,
[TKMD_ID] [bigint] NOT NULL,
[SoTT] [int] NULL,
[PhanLoai] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoGiayPhep] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InputMessageID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTag] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexTag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_TK_GiayPhep_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_TK_GiayPhep_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_TK_GiayPhep_Log 
ON dbo.t_KDT_VNACC_TK_GiayPhep
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_GiayPhep_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_GiayPhep_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_GiayPhep_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACC_TK_KhoanDieuChinh_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_TK_KhoanDieuChinh_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh_Log]
(
[ID] [bigint]  NULL ,
[TKMD_ID] [bigint] NOT NULL,
[SoTT] [int] NULL,
[MaTenDieuChinh] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhanLoaiDieuChinh] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTDieuChinhTriGia] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGiaKhoanDieuChinh] [numeric] (24, 4) NULL,
[TongHeSoPhanBo] [numeric] (24, 4) NULL,
[InputMessageID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTag] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexTag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_TK_KhoanDieuChinh_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_TK_KhoanDieuChinh_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_TK_KhoanDieuChinh_Log 
ON dbo.t_KDT_VNACC_TK_KhoanDieuChinh
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_KhoanDieuChinh_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_KhoanDieuChinh_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_KhoanDieuChinh_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACC_TK_PhanHoi_AnHan_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_TK_PhanHoi_AnHan_Log
GO

CREATE TABLE [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan_Log]
(
[ID] [bigint]  NULL ,
[Master_ID] [bigint] NOT NULL,
[MaSacThueAnHan] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenSacThueAnHan] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HanNopThueSauKhiAnHan] [datetime] NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_TK_PhanHoi_AnHan_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_TK_PhanHoi_AnHan_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_TK_PhanHoi_AnHan_Log 
ON dbo.t_KDT_VNACC_TK_PhanHoi_AnHan
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_PhanHoi_AnHan_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_PhanHoi_AnHan_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_PhanHoi_AnHan_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO


IF OBJECT_ID('t_KDT_VNACC_TK_PhanHoi_SacThue_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_TK_PhanHoi_SacThue_Log
GO

CREATE TABLE [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue_Log]
(
[ID] [bigint]  NULL ,
[Master_ID] [bigint] NOT NULL,
[MaSacThue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenSacThue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongTienThue] [numeric] (15, 4) NULL,
[SoDongTongTienThue] [numeric] (2, 0) NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_TK_PhanHoi_SacThue_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_TK_PhanHoi_SacThue_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_TK_PhanHoi_SacThue_Log 
ON dbo.t_KDT_VNACC_TK_PhanHoi_SacThue
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_PhanHoi_SacThue_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_PhanHoi_SacThue_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_PhanHoi_SacThue_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACC_TK_PhanHoi_TyGia_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_TK_PhanHoi_TyGia_Log
GO

CREATE TABLE [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia_Log]
(
[ID] [bigint]  NULL ,
[Master_ID] [bigint] NOT NULL,
[MaTTTyGiaTinhThue] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TyGiaTinhThue] [numeric] (13, 4) NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_TK_PhanHoi_TyGia_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_TK_PhanHoi_TyGia_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_TK_PhanHoi_TyGia_Log 
ON dbo.t_KDT_VNACC_TK_PhanHoi_TyGia
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_PhanHoi_TyGia_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_PhanHoi_TyGia_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_PhanHoi_TyGia_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACC_TK_SoContainer_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_TK_SoContainer_Log
GO

CREATE TABLE [dbo].[t_KDT_VNACC_TK_SoContainer_Log]
(
[ID] [bigint]  NULL ,
[Master_id] [bigint] NOT NULL,
[SoTT] [int] NULL,
[SoContainer] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InputMessageID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTag] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexTag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_TK_SoContainer_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_TK_SoContainer_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_TK_SoContainer_Log 
ON dbo.t_KDT_VNACC_TK_SoContainer
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_SoContainer_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_SoContainer_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_SoContainer_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACC_TK_SoVanDon_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_TK_SoVanDon_Log
GO

CREATE TABLE [dbo].[t_KDT_VNACC_TK_SoVanDon_Log]
(
[ID] [bigint]  NULL ,
[TKMD_ID] [bigint] NULL,
[SoTT] [int] NULL,
[SoVanDon] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InputMessageID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTag] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexTag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoVanDonChu] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamVanDonChu] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiDinhDanh] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayVanDon] [datetime] NULL,
[SoDinhDanh] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_TK_SoVanDon_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_TK_SoVanDon_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_TK_SoVanDon_Log 
ON dbo.t_KDT_VNACC_TK_SoVanDon
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_SoVanDon_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_SoVanDon_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_SoVanDon_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACC_TK_TrungChuyen_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_TK_TrungChuyen_Log
GO

CREATE TABLE [dbo].[t_KDT_VNACC_TK_TrungChuyen_Log]
(
[ID] [bigint]  NULL ,
[TKMD_ID] [bigint] NOT NULL,
[SoTT] [int] NULL,
[MaDiaDiem] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayDen] [datetime] NULL,
[NgayKhoiHanh] [datetime] NULL,
[InputMessageID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTag] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexTag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACC_TK_TrungChuyen_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_TK_TrungChuyen_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_TK_TrungChuyen_Log 
ON dbo.t_KDT_VNACC_TK_TrungChuyen
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_TrungChuyen_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_TrungChuyen_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_TK_TrungChuyen_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_VNACC_HangMauDich_PhanHoi_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_HangMauDich_PhanHoi_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi_Log]
(
[ID] [bigint]  NULL ,
[Master_ID] [bigint] NOT NULL,
[SoDong] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhanLoaiTaiXacNhanGia] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNoiXuatXu] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuongTinhThue] [numeric] (12, 0) NULL,
[MaDVTDanhThue] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DonGiaTinhThue] [numeric] (18, 4) NULL,
[DV_SL_TrongDonGiaTinhThue] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTDonGiaTinhThue] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGiaTinhThueS] [numeric] (17, 4) NULL,
[MaTTTriGiaTinhThueS] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTSoTienMienGiam] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhanLoaiThueSuatThue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThueSuatThue] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLoaiThueSuatThue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienThue] [numeric] (16, 0) NULL,
[MaTTSoTienThueXuatKhau] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TienLePhi_DonGia] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TienBaoHiem_DonGia] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TienLePhi_SoLuong] [numeric] (12, 0) NULL,
[TienLePhi_MaDVSoLuong] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TienBaoHiem_SoLuong] [numeric] (12, 0) NULL,
[TienBaoHiem_MaDVSoLuong] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TienLePhi_KhoanTien] [numeric] (16, 4) NULL,
[TienBaoHiem_KhoanTien] [numeric] (16, 4) NULL,
[DieuKhoanMienGiam] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACC_HangMauDich_PhanHoi_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_HangMauDich_PhanHoi_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_HangMauDich_PhanHoi_Log
ON dbo.t_KDT_VNACC_HangMauDich_PhanHoi 
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_PhanHoi_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_PhanHoi_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_PhanHoi_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

GO
IF OBJECT_ID('t_KDT_VNACC_HangMauDich_ThueThuKhac_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_HangMauDich_ThueThuKhac_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac_Log]
(
[ID] [bigint]  NULL ,
[Master_id] [bigint] NOT NULL,
[MaTSThueThuKhac] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaMGThueThuKhac] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienGiamThueThuKhac] [numeric] (16, 4) NULL,
[TenKhoanMucThueVaThuKhac] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGiaTinhThueVaThuKhac] [numeric] (17, 4) NULL,
[SoLuongTinhThueVaThuKhac] [numeric] (14, 0) NULL,
[MaDVTDanhThueVaThuKhac] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThueSuatThueVaThuKhac] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienThueVaThuKhac] [numeric] (16, 4) NULL,
[DieuKhoanMienGiamThueVaThuKhac] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACC_HangMauDich_ThueThuKhac_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_HangMauDich_ThueThuKhac_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_HangMauDich_ThueThuKhac_Log
ON dbo.t_KDT_VNACC_HangMauDich_ThueThuKhac 
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_ThueThuKhac_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_ThueThuKhac_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_ThueThuKhac_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Log]
(
[ID] [bigint]  NULL ,
[TKMD_ID] [bigint] NOT NULL,
[SoToKhaiBoSung] [numeric] (12, 0) NULL,
[CoQuanHaiQuan] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NhomXuLyHoSo] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NhomXuLyHoSoID] [int] NULL,
[PhanLoaiXuatNhapKhau] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoToKhai] [numeric] (12, 0) NULL,
[MaLoaiHinh] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayKhaiBao] [datetime] NULL,
[NgayCapPhep] [datetime] NULL,
[ThoiHanTaiNhapTaiXuat] [datetime] NULL,
[MaNguoiKhai] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNguoiKhai] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaBuuChinh] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaChiNguoiKhai] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoDienThoaiNguoiKhai] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaLyDoKhaiBoSung] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTienTeTienThue] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNganHangTraThueThay] [varchar] (110) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamPhatHanhHanMuc] [numeric] (4, 0) NULL,
[KiHieuChungTuPhatHanhHanMuc] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoChungTuPhatHanhHanMuc] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaXacDinhThoiHanNopThue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNganHangBaoLanh] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamPhatHanhBaoLanh] [numeric] (4, 0) NULL,
[KyHieuPhatHanhChungTuBaoLanh] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoHieuPhatHanhChungTuBaoLanh] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTienTeTruocKhiKhaiBoSung] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TyGiaHoiDoaiTruocKhiKhaiBoSung] [numeric] (9, 0) NULL,
[MaTienTeSauKhiKhaiBoSung] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TyGiaHoiDoaiSauKhiKhaiBoSung] [numeric] (9, 0) NULL,
[SoQuanLyTrongNoiBoDoanhNghiep] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GhiChuNoiDungLienQuanSauKhiKhaiBoSung] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTiepNhan] [numeric] (12, 0) NULL,
[NgayTiepNhan] [datetime] NULL,
[PhanLuong] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HuongDan] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InputMessageID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTag] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexTag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoThongBao] [numeric] (12, 0) NULL,
[NgayHoanThanhKiemTra] [datetime] NULL,
[GioHoanThanhKiemTra] [datetime] NULL,
[NgayDangKyKhaiBoSung] [datetime] NULL,
[GioDangKyKhaiBoSung] [datetime] NULL,
[DauHieuBaoQua60Ngay] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaHetThoiHan] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDaiLyHaiQuan] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDaiLyHaiQuan] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNhanVienHaiQuan] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLoaiNopThue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHieuLucChungTu] [datetime] NULL,
[SoNgayNopThue] [numeric] (3, 0) NULL,
[SoNgayNopThueDanhChoVATHangHoaDacBiet] [numeric] (3, 0) NULL,
[HienThiTongSoTienTangGiamThueXuatNhapKhau] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongSoTienTangGiamThueXuatNhapKhau] [numeric] (11, 0) NULL,
[MaTienTeTongSoTienTangGiamThueXuatNhapKhau] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongSoTrangToKhaiBoSung] [numeric] (2, 0) NULL,
[TongSoDongHangToKhaiBoSung] [numeric] (2, 0) NULL,
[LyDo] [nvarchar] (600) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNguoiPhuTrach] [nvarchar] (108) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenTruongDonViHaiQuan] [nvarchar] (108) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayDangKyDuLieu] [datetime] NULL,
[GioDangKyDuLieu] [datetime] NULL,
[TrangThaiXuLy] [int] NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Log
ON dbo.t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung 
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Log]
(
[ID] [bigint]  NULL ,
[TKMDBoSung_ID] [bigint] NULL,
[SoDong] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HienThiTongSoTienTangGiamThuKhac] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongSoTienTangGiamThuKhac] [numeric] (11, 0) NULL,
[MaTienTeTongSoTienTangGiamThuKhac] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Log
ON dbo.t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac 
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Log
GO

CREATE TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Log]
(
[ID] [bigint]  NULL,
[TKMDBoSung_ID] [bigint] NULL,
[SoDong] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoThuTuDongHangTrenToKhaiGoc] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MoTaHangHoaTruocKhiKhaiBoSung] [nvarchar] (600) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MoTaHangHoaSauKhiKhaiBoSung] [nvarchar] (600) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaSoHangHoaTruocKhiKhaiBoSung] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaSoHangHoaSauKhiKhaiBoSung] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNuocXuatXuTruocKhiKhaiBoSung] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNuocXuatXuSauKhiKhaiBoSung] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGiaTinhThueTruocKhiKhaiBoSung] [numeric] (21, 6) NULL,
[TriGiaTinhThueSauKhiKhaiBoSung] [numeric] (21, 6) NULL,
[SoLuongTinhThueTruocKhiKhaiBoSung] [numeric] (21, 6) NULL,
[SoLuongTinhThueSauKhiKhaiBoSung] [numeric] (21, 6) NULL,
[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThueSuatTruocKhiKhaiBoSung] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThueSuatSauKhiKhaiBoSung] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienThueTruocKhiKhaiBoSung] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienThueSauKhiKhaiBoSung] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HienThiMienThueTruocKhiKhaiBoSung] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HienThiMienThueSauKhiKhaiBoSung] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HienThiSoTienTangGiamThueXuatNhapKhau] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienTangGiamThue] [numeric] (16, 6) NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Log
ON dbo.t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue 
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Log]
(
[ID] [bigint]  NULL ,
[HMDBoSung_ID] [bigint] NULL,
[SoDong] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac] [numeric] (21, 6) NULL,
[TriGiaTinhThueSauKhiKhaiBoSungThuKhac] [numeric] (17, 0) NULL,
[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac] [numeric] (16, 6) NULL,
[SoLuongTinhThueSauKhiKhaiBoSungThuKhac] [numeric] (12, 0) NULL,
[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaApDungThueSuatSauKhiKhaiBoSungThuKhac] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThueSuatTruocKhiKhaiBoSungThuKhac] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThueSuatSauKhiKhaiBoSungThuKhac] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienThueTruocKhiKhaiBoSungThuKhac] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienThueSauKhiKhaiBoSungThuKhac] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HienThiMienThueVaThuKhacSauKhiKhaiBoSung] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HienThiSoTienTangGiamThueVaThuKhac] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTienTangGiamThuKhac] [numeric] (16, 0) NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Log
GO

CREATE TRIGGER trg_t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Log
ON dbo.t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '38.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('38.0',GETDATE(), N'CẬP NHẬT GHI LOG TỜ KHAI - HÀNG HÓA -THUẾ VNACCS VÀ AMA')
END