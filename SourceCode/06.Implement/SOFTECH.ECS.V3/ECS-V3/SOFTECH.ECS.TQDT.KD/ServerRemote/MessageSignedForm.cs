﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SignRemote;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.Interface.VNACCS;
using System.Xml;
using Company.Interface;

namespace ServerRemote
{
    public partial class MessageSignedForm : Company.Interface.BaseForm
    {
        public MessageSignedForm()
        {
            InitializeComponent();
        }
        private string GetSearchWhere()
        {
            string where = " 1 = 1";
            if (!String.IsNullOrEmpty(txtNguoiGui.Text.ToString().Trim()))
                where += " AND MSG_FROM LIKE '%" + txtNguoiGui.Text.ToString().Trim() + "%'";
            DateTime dateFrom = clcTuNgay.Value;
            DateTime dateTo = clcDenNgay.Value;

            where += " AND CREATE_TIME BETWEEN '" + dateFrom.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + dateTo.ToString("yyyy-MM-dd 23:59:59") + "'";
            return where;
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = MSG_OUTBOX.SelectCollectionDynamic(GetSearchWhere(), "ID");
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<MSG_OUTBOX> ItemColl = new List<MSG_OUTBOX>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ItemColl.Add((MSG_OUTBOX)i.GetRow().DataRow);
                    }

                }
                foreach (MSG_OUTBOX item in ItemColl)
                {
                    if (item.ID > 0)
                        item.Delete();
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MessageSignedForm_Load(object sender, EventArgs e)
        {
            clcTuNgay.Value = DateTime.Now;
            clcDenNgay.Value = DateTime.Now;
            BindData();
        }

        private void grList_ColumnButtonClick(object sender, ColumnActionEventArgs ev)
        {
            try
            {
                GridEXSelectedItem e = grList.SelectedItems[0];
                if (e.GetRow().RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    MSG_OUTBOX msg = new MSG_OUTBOX();
                    msg = (MSG_OUTBOX)e.GetRow().DataRow;
                    string MsgXML = "";
                    string MsgEDI = "";
                    if (msg != null)
                    {
                        if (msg.STATUS == 1)
                        {
                            if (msg.MSG_ORIGIN.Length % 4 == 0)
                            {
                                if (!msg.MSG_ORIGIN.TrimStart().StartsWith("<"))
                                {
                                    MsgEDI = Helpers.ConvertFromBase64VNACCS(msg.MSG_ORIGIN);
                                }
                                else
                                {
                                    MsgEDI = msg.MSG_ORIGIN;
                                }
                            }
                            else
                            {
                                MsgEDI = msg.MSG_ORIGIN;
                            }
                            ShowMessagesEDI f = new ShowMessagesEDI(MsgEDI);
                            f.ShowDialog(this);
                        }
                        else
                        {
                            if (msg.MSG_ORIGIN.Length % 4 == 0)
                            {
                                if (!msg.MSG_ORIGIN.TrimStart().StartsWith("<"))
                                {
                                    MsgXML = Helpers.ConvertFromBase64VNACCS(msg.MSG_ORIGIN);
                                }
                                else
                                {
                                    MsgXML = msg.MSG_ORIGIN;
                                }
                            }
                            else
                            {
                                MsgXML = msg.MSG_ORIGIN;
                            }
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(MsgXML);
                            ViewMessagesForm f = new ViewMessagesForm();
                            f.doc = doc;
                            f.ShowDialog(this);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void grList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    MSG_OUTBOX msg = new MSG_OUTBOX();
                    msg = (MSG_OUTBOX)e.Row.DataRow;
                    string MsgXML = "";
                    string MsgEDI = "";
                    if (msg != null)
                    {
                        if (msg.STATUS == 1)
                        {
                            if (msg.MSG_ORIGIN.Length % 4 == 0)
                            {
                                if (!msg.MSG_ORIGIN.TrimStart().StartsWith("<"))
                                {
                                    MsgEDI = Helpers.ConvertFromBase64VNACCS(msg.MSG_ORIGIN);
                                }
                                else
                                {
                                    MsgEDI = msg.MSG_ORIGIN;
                                }
                            }
                            else
                            {
                                MsgEDI = msg.MSG_ORIGIN;
                            }
                            ShowMessagesEDI f = new ShowMessagesEDI(MsgEDI);
                            f.ShowDialog(this);
                        }
                        else
                        {
                            if (msg.MSG_ORIGIN.Length % 4 == 0)
                            {
                                if (!msg.MSG_ORIGIN.TrimStart().StartsWith("<"))
                                {
                                    MsgXML = Helpers.ConvertFromBase64VNACCS(msg.MSG_ORIGIN);
                                }
                                else
                                {
                                    MsgXML = msg.MSG_ORIGIN;
                                }
                            }
                            else
                            {
                                MsgXML = msg.MSG_ORIGIN;
                            }
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(MsgXML);
                            ViewMessagesForm f = new ViewMessagesForm();
                            f.doc = doc;
                            f.ShowDialog(this);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void grList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                string TrangThai = e.Row.Cells["STATUS"].Value.ToString();
                switch (TrangThai)
                {
                    case "1":
                        e.Row.Cells["STATUS"].Text = "VNACCS";
                        break;
                    case "0":
                        e.Row.Cells["STATUS"].Text = "V5";
                        break;
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
