using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class KDT_VNACC_TK_Container : ICloneable
    {
        List<KDT_VNACC_TK_SoContainer> _ListSoContainer = new List<KDT_VNACC_TK_SoContainer>();
        public List<KDT_VNACC_TK_SoContainer> SoContainerCollection
        {
            set { this._ListSoContainer = value; }
            get { return this._ListSoContainer; }
        }
        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert();
                    else
                        this.Update();

                    //S� container
                    foreach (KDT_VNACC_TK_SoContainer item in this.SoContainerCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.Master_id = this.ID;
                            item.ID = item.Insert();
                        }
                        else
                        {
                            item.InsertUpdate();
                        }
                    }
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }
}