using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ToKhaiMauDich_PhanHoi : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string PhanLoaiBaoCaoSuaDoi { set; get; }
		public string MaPhanLoaiKiemTra { set; get; }
		public string MaSoThueDaiDien { set; get; }
		public string TenCoQuanHaiQuan { set; get; }
		public DateTime NgayDangKy { set; get; }
		public DateTime NgayThayDoiDangKy { set; get; }
		public string BieuThiTruongHopHetHan { set; get; }
		public string TenDaiLyHaiQuan { set; get; }
		public string MaNhanVienHaiQuan { set; get; }
		public string TenDDLuuKho { set; get; }
		public string MaPhanLoaiTongGiaCoBan { set; get; }
		public string PhanLoaiCongThucChuan { set; get; }
		public string MaPhanLoaiDieuChinhTriGia { set; get; }
		public string PhuongPhapDieuChinhTriGia { set; get; }
		public decimal TongTienThuePhaiNop { set; get; }
		public decimal SoTienBaoLanh { set; get; }
		public string TenTruongDonViHaiQuan { set; get; }
		public DateTime NgayCapPhep { set; get; }
		public string PhanLoaiThamTraSauThongQuan { set; get; }
		public DateTime NgayPheDuyetBP { set; get; }
		public DateTime NgayHoanThanhKiemTraBP { set; get; }
		public decimal SoNgayDoiCapPhepNhapKhau { set; get; }
		public string TieuDe { set; get; }
		public string MaSacThueAnHan_VAT { set; get; }
		public string TenSacThueAnHan_VAT { set; get; }
		public DateTime HanNopThueSauKhiAnHan_VAT { set; get; }
		public string PhanLoaiNopThue { set; get; }
		public decimal TongSoTienThueXuatKhau { set; get; }
		public string MaTTTongTienThueXuatKhau { set; get; }
		public decimal TongSoTienLePhi { set; get; }
		public string MaTTCuaSoTienBaoLanh { set; get; }
		public string SoQuanLyNguoiSuDung { set; get; }
		public DateTime NgayHoanThanhKiemTra { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ToKhaiMauDich_PhanHoi> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ToKhaiMauDich_PhanHoi> collection = new List<KDT_VNACC_ToKhaiMauDich_PhanHoi>();
			while (reader.Read())
			{
				KDT_VNACC_ToKhaiMauDich_PhanHoi entity = new KDT_VNACC_ToKhaiMauDich_PhanHoi();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiBaoCaoSuaDoi"))) entity.PhanLoaiBaoCaoSuaDoi = reader.GetString(reader.GetOrdinal("PhanLoaiBaoCaoSuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiKiemTra"))) entity.MaPhanLoaiKiemTra = reader.GetString(reader.GetOrdinal("MaPhanLoaiKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoThueDaiDien"))) entity.MaSoThueDaiDien = reader.GetString(reader.GetOrdinal("MaSoThueDaiDien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCoQuanHaiQuan"))) entity.TenCoQuanHaiQuan = reader.GetString(reader.GetOrdinal("TenCoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayThayDoiDangKy"))) entity.NgayThayDoiDangKy = reader.GetDateTime(reader.GetOrdinal("NgayThayDoiDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("BieuThiTruongHopHetHan"))) entity.BieuThiTruongHopHetHan = reader.GetString(reader.GetOrdinal("BieuThiTruongHopHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyHaiQuan"))) entity.TenDaiLyHaiQuan = reader.GetString(reader.GetOrdinal("TenDaiLyHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNhanVienHaiQuan"))) entity.MaNhanVienHaiQuan = reader.GetString(reader.GetOrdinal("MaNhanVienHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDDLuuKho"))) entity.TenDDLuuKho = reader.GetString(reader.GetOrdinal("TenDDLuuKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiTongGiaCoBan"))) entity.MaPhanLoaiTongGiaCoBan = reader.GetString(reader.GetOrdinal("MaPhanLoaiTongGiaCoBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiCongThucChuan"))) entity.PhanLoaiCongThucChuan = reader.GetString(reader.GetOrdinal("PhanLoaiCongThucChuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiDieuChinhTriGia"))) entity.MaPhanLoaiDieuChinhTriGia = reader.GetString(reader.GetOrdinal("MaPhanLoaiDieuChinhTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongPhapDieuChinhTriGia"))) entity.PhuongPhapDieuChinhTriGia = reader.GetString(reader.GetOrdinal("PhuongPhapDieuChinhTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTienThuePhaiNop"))) entity.TongTienThuePhaiNop = reader.GetDecimal(reader.GetOrdinal("TongTienThuePhaiNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienBaoLanh"))) entity.SoTienBaoLanh = reader.GetDecimal(reader.GetOrdinal("SoTienBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenTruongDonViHaiQuan"))) entity.TenTruongDonViHaiQuan = reader.GetString(reader.GetOrdinal("TenTruongDonViHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapPhep"))) entity.NgayCapPhep = reader.GetDateTime(reader.GetOrdinal("NgayCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiThamTraSauThongQuan"))) entity.PhanLoaiThamTraSauThongQuan = reader.GetString(reader.GetOrdinal("PhanLoaiThamTraSauThongQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPheDuyetBP"))) entity.NgayPheDuyetBP = reader.GetDateTime(reader.GetOrdinal("NgayPheDuyetBP"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhKiemTraBP"))) entity.NgayHoanThanhKiemTraBP = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhKiemTraBP"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayDoiCapPhepNhapKhau"))) entity.SoNgayDoiCapPhepNhapKhau = reader.GetDecimal(reader.GetOrdinal("SoNgayDoiCapPhepNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDe"))) entity.TieuDe = reader.GetString(reader.GetOrdinal("TieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSacThueAnHan_VAT"))) entity.MaSacThueAnHan_VAT = reader.GetString(reader.GetOrdinal("MaSacThueAnHan_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSacThueAnHan_VAT"))) entity.TenSacThueAnHan_VAT = reader.GetString(reader.GetOrdinal("TenSacThueAnHan_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("HanNopThueSauKhiAnHan_VAT"))) entity.HanNopThueSauKhiAnHan_VAT = reader.GetDateTime(reader.GetOrdinal("HanNopThueSauKhiAnHan_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiNopThue"))) entity.PhanLoaiNopThue = reader.GetString(reader.GetOrdinal("PhanLoaiNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoTienThueXuatKhau"))) entity.TongSoTienThueXuatKhau = reader.GetDecimal(reader.GetOrdinal("TongSoTienThueXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTTongTienThueXuatKhau"))) entity.MaTTTongTienThueXuatKhau = reader.GetString(reader.GetOrdinal("MaTTTongTienThueXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoTienLePhi"))) entity.TongSoTienLePhi = reader.GetDecimal(reader.GetOrdinal("TongSoTienLePhi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTCuaSoTienBaoLanh"))) entity.MaTTCuaSoTienBaoLanh = reader.GetString(reader.GetOrdinal("MaTTCuaSoTienBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuanLyNguoiSuDung"))) entity.SoQuanLyNguoiSuDung = reader.GetString(reader.GetOrdinal("SoQuanLyNguoiSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhKiemTra"))) entity.NgayHoanThanhKiemTra = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhKiemTra"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ToKhaiMauDich_PhanHoi> collection, long id)
        {
            foreach (KDT_VNACC_ToKhaiMauDich_PhanHoi item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_PhanHoi VALUES(@Master_ID, @PhanLoaiBaoCaoSuaDoi, @MaPhanLoaiKiemTra, @MaSoThueDaiDien, @TenCoQuanHaiQuan, @NgayDangKy, @NgayThayDoiDangKy, @BieuThiTruongHopHetHan, @TenDaiLyHaiQuan, @MaNhanVienHaiQuan, @TenDDLuuKho, @MaPhanLoaiTongGiaCoBan, @PhanLoaiCongThucChuan, @MaPhanLoaiDieuChinhTriGia, @PhuongPhapDieuChinhTriGia, @TongTienThuePhaiNop, @SoTienBaoLanh, @TenTruongDonViHaiQuan, @NgayCapPhep, @PhanLoaiThamTraSauThongQuan, @NgayPheDuyetBP, @NgayHoanThanhKiemTraBP, @SoNgayDoiCapPhepNhapKhau, @TieuDe, @MaSacThueAnHan_VAT, @TenSacThueAnHan_VAT, @HanNopThueSauKhiAnHan_VAT, @PhanLoaiNopThue, @TongSoTienThueXuatKhau, @MaTTTongTienThueXuatKhau, @TongSoTienLePhi, @MaTTCuaSoTienBaoLanh, @SoQuanLyNguoiSuDung, @NgayHoanThanhKiemTra)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_PhanHoi SET Master_ID = @Master_ID, PhanLoaiBaoCaoSuaDoi = @PhanLoaiBaoCaoSuaDoi, MaPhanLoaiKiemTra = @MaPhanLoaiKiemTra, MaSoThueDaiDien = @MaSoThueDaiDien, TenCoQuanHaiQuan = @TenCoQuanHaiQuan, NgayDangKy = @NgayDangKy, NgayThayDoiDangKy = @NgayThayDoiDangKy, BieuThiTruongHopHetHan = @BieuThiTruongHopHetHan, TenDaiLyHaiQuan = @TenDaiLyHaiQuan, MaNhanVienHaiQuan = @MaNhanVienHaiQuan, TenDDLuuKho = @TenDDLuuKho, MaPhanLoaiTongGiaCoBan = @MaPhanLoaiTongGiaCoBan, PhanLoaiCongThucChuan = @PhanLoaiCongThucChuan, MaPhanLoaiDieuChinhTriGia = @MaPhanLoaiDieuChinhTriGia, PhuongPhapDieuChinhTriGia = @PhuongPhapDieuChinhTriGia, TongTienThuePhaiNop = @TongTienThuePhaiNop, SoTienBaoLanh = @SoTienBaoLanh, TenTruongDonViHaiQuan = @TenTruongDonViHaiQuan, NgayCapPhep = @NgayCapPhep, PhanLoaiThamTraSauThongQuan = @PhanLoaiThamTraSauThongQuan, NgayPheDuyetBP = @NgayPheDuyetBP, NgayHoanThanhKiemTraBP = @NgayHoanThanhKiemTraBP, SoNgayDoiCapPhepNhapKhau = @SoNgayDoiCapPhepNhapKhau, TieuDe = @TieuDe, MaSacThueAnHan_VAT = @MaSacThueAnHan_VAT, TenSacThueAnHan_VAT = @TenSacThueAnHan_VAT, HanNopThueSauKhiAnHan_VAT = @HanNopThueSauKhiAnHan_VAT, PhanLoaiNopThue = @PhanLoaiNopThue, TongSoTienThueXuatKhau = @TongSoTienThueXuatKhau, MaTTTongTienThueXuatKhau = @MaTTTongTienThueXuatKhau, TongSoTienLePhi = @TongSoTienLePhi, MaTTCuaSoTienBaoLanh = @MaTTCuaSoTienBaoLanh, SoQuanLyNguoiSuDung = @SoQuanLyNguoiSuDung, NgayHoanThanhKiemTra = @NgayHoanThanhKiemTra WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_PhanHoi WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, "PhanLoaiBaoCaoSuaDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, "MaPhanLoaiKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, "MaSoThueDaiDien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, "NgayThayDoiDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, "BieuThiTruongHopHetHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDDLuuKho", SqlDbType.VarChar, "TenDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, "MaPhanLoaiTongGiaCoBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, "PhanLoaiCongThucChuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, "MaPhanLoaiDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, "PhuongPhapDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, "TongTienThuePhaiNop", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, "PhanLoaiThamTraSauThongQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, "NgayPheDuyetBP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, "NgayHoanThanhKiemTraBP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, "SoNgayDoiCapPhepNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.VarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, "MaSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, "TenSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, "HanNopThueSauKhiAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, "TongSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, "MaTTTongTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienLePhi", SqlDbType.Decimal, "TongSoTienLePhi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, "MaTTCuaSoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, "SoQuanLyNguoiSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, "PhanLoaiBaoCaoSuaDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, "MaPhanLoaiKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, "MaSoThueDaiDien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, "NgayThayDoiDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, "BieuThiTruongHopHetHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDDLuuKho", SqlDbType.VarChar, "TenDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, "MaPhanLoaiTongGiaCoBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, "PhanLoaiCongThucChuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, "MaPhanLoaiDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, "PhuongPhapDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, "TongTienThuePhaiNop", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, "PhanLoaiThamTraSauThongQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, "NgayPheDuyetBP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, "NgayHoanThanhKiemTraBP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, "SoNgayDoiCapPhepNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.VarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, "MaSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, "TenSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, "HanNopThueSauKhiAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, "TongSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, "MaTTTongTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienLePhi", SqlDbType.Decimal, "TongSoTienLePhi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, "MaTTCuaSoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, "SoQuanLyNguoiSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_PhanHoi VALUES(@Master_ID, @PhanLoaiBaoCaoSuaDoi, @MaPhanLoaiKiemTra, @MaSoThueDaiDien, @TenCoQuanHaiQuan, @NgayDangKy, @NgayThayDoiDangKy, @BieuThiTruongHopHetHan, @TenDaiLyHaiQuan, @MaNhanVienHaiQuan, @TenDDLuuKho, @MaPhanLoaiTongGiaCoBan, @PhanLoaiCongThucChuan, @MaPhanLoaiDieuChinhTriGia, @PhuongPhapDieuChinhTriGia, @TongTienThuePhaiNop, @SoTienBaoLanh, @TenTruongDonViHaiQuan, @NgayCapPhep, @PhanLoaiThamTraSauThongQuan, @NgayPheDuyetBP, @NgayHoanThanhKiemTraBP, @SoNgayDoiCapPhepNhapKhau, @TieuDe, @MaSacThueAnHan_VAT, @TenSacThueAnHan_VAT, @HanNopThueSauKhiAnHan_VAT, @PhanLoaiNopThue, @TongSoTienThueXuatKhau, @MaTTTongTienThueXuatKhau, @TongSoTienLePhi, @MaTTCuaSoTienBaoLanh, @SoQuanLyNguoiSuDung, @NgayHoanThanhKiemTra)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_PhanHoi SET Master_ID = @Master_ID, PhanLoaiBaoCaoSuaDoi = @PhanLoaiBaoCaoSuaDoi, MaPhanLoaiKiemTra = @MaPhanLoaiKiemTra, MaSoThueDaiDien = @MaSoThueDaiDien, TenCoQuanHaiQuan = @TenCoQuanHaiQuan, NgayDangKy = @NgayDangKy, NgayThayDoiDangKy = @NgayThayDoiDangKy, BieuThiTruongHopHetHan = @BieuThiTruongHopHetHan, TenDaiLyHaiQuan = @TenDaiLyHaiQuan, MaNhanVienHaiQuan = @MaNhanVienHaiQuan, TenDDLuuKho = @TenDDLuuKho, MaPhanLoaiTongGiaCoBan = @MaPhanLoaiTongGiaCoBan, PhanLoaiCongThucChuan = @PhanLoaiCongThucChuan, MaPhanLoaiDieuChinhTriGia = @MaPhanLoaiDieuChinhTriGia, PhuongPhapDieuChinhTriGia = @PhuongPhapDieuChinhTriGia, TongTienThuePhaiNop = @TongTienThuePhaiNop, SoTienBaoLanh = @SoTienBaoLanh, TenTruongDonViHaiQuan = @TenTruongDonViHaiQuan, NgayCapPhep = @NgayCapPhep, PhanLoaiThamTraSauThongQuan = @PhanLoaiThamTraSauThongQuan, NgayPheDuyetBP = @NgayPheDuyetBP, NgayHoanThanhKiemTraBP = @NgayHoanThanhKiemTraBP, SoNgayDoiCapPhepNhapKhau = @SoNgayDoiCapPhepNhapKhau, TieuDe = @TieuDe, MaSacThueAnHan_VAT = @MaSacThueAnHan_VAT, TenSacThueAnHan_VAT = @TenSacThueAnHan_VAT, HanNopThueSauKhiAnHan_VAT = @HanNopThueSauKhiAnHan_VAT, PhanLoaiNopThue = @PhanLoaiNopThue, TongSoTienThueXuatKhau = @TongSoTienThueXuatKhau, MaTTTongTienThueXuatKhau = @MaTTTongTienThueXuatKhau, TongSoTienLePhi = @TongSoTienLePhi, MaTTCuaSoTienBaoLanh = @MaTTCuaSoTienBaoLanh, SoQuanLyNguoiSuDung = @SoQuanLyNguoiSuDung, NgayHoanThanhKiemTra = @NgayHoanThanhKiemTra WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_PhanHoi WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, "PhanLoaiBaoCaoSuaDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, "MaPhanLoaiKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, "MaSoThueDaiDien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, "NgayThayDoiDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, "BieuThiTruongHopHetHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDDLuuKho", SqlDbType.VarChar, "TenDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, "MaPhanLoaiTongGiaCoBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, "PhanLoaiCongThucChuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, "MaPhanLoaiDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, "PhuongPhapDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, "TongTienThuePhaiNop", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, "PhanLoaiThamTraSauThongQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, "NgayPheDuyetBP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, "NgayHoanThanhKiemTraBP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, "SoNgayDoiCapPhepNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.VarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, "MaSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, "TenSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, "HanNopThueSauKhiAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, "TongSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, "MaTTTongTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienLePhi", SqlDbType.Decimal, "TongSoTienLePhi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, "MaTTCuaSoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, "SoQuanLyNguoiSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, "PhanLoaiBaoCaoSuaDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, "MaPhanLoaiKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, "MaSoThueDaiDien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, "NgayThayDoiDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, "BieuThiTruongHopHetHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDDLuuKho", SqlDbType.VarChar, "TenDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, "MaPhanLoaiTongGiaCoBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, "PhanLoaiCongThucChuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, "MaPhanLoaiDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, "PhuongPhapDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, "TongTienThuePhaiNop", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, "PhanLoaiThamTraSauThongQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, "NgayPheDuyetBP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, "NgayHoanThanhKiemTraBP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, "SoNgayDoiCapPhepNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.VarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, "MaSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, "TenSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, "HanNopThueSauKhiAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, "TongSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, "MaTTTongTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienLePhi", SqlDbType.Decimal, "TongSoTienLePhi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, "MaTTCuaSoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, "SoQuanLyNguoiSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ToKhaiMauDich_PhanHoi Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ToKhaiMauDich_PhanHoi> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ToKhaiMauDich_PhanHoi> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ToKhaiMauDich_PhanHoi> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ToKhaiMauDich_PhanHoi(long master_ID, string phanLoaiBaoCaoSuaDoi, string maPhanLoaiKiemTra, string maSoThueDaiDien, string tenCoQuanHaiQuan, DateTime ngayDangKy, DateTime ngayThayDoiDangKy, string bieuThiTruongHopHetHan, string tenDaiLyHaiQuan, string maNhanVienHaiQuan, string tenDDLuuKho, string maPhanLoaiTongGiaCoBan, string phanLoaiCongThucChuan, string maPhanLoaiDieuChinhTriGia, string phuongPhapDieuChinhTriGia, decimal tongTienThuePhaiNop, decimal soTienBaoLanh, string tenTruongDonViHaiQuan, DateTime ngayCapPhep, string phanLoaiThamTraSauThongQuan, DateTime ngayPheDuyetBP, DateTime ngayHoanThanhKiemTraBP, decimal soNgayDoiCapPhepNhapKhau, string tieuDe, string maSacThueAnHan_VAT, string tenSacThueAnHan_VAT, DateTime hanNopThueSauKhiAnHan_VAT, string phanLoaiNopThue, decimal tongSoTienThueXuatKhau, string maTTTongTienThueXuatKhau, decimal tongSoTienLePhi, string maTTCuaSoTienBaoLanh, string soQuanLyNguoiSuDung, DateTime ngayHoanThanhKiemTra)
		{
			KDT_VNACC_ToKhaiMauDich_PhanHoi entity = new KDT_VNACC_ToKhaiMauDich_PhanHoi();	
			entity.Master_ID = master_ID;
			entity.PhanLoaiBaoCaoSuaDoi = phanLoaiBaoCaoSuaDoi;
			entity.MaPhanLoaiKiemTra = maPhanLoaiKiemTra;
			entity.MaSoThueDaiDien = maSoThueDaiDien;
			entity.TenCoQuanHaiQuan = tenCoQuanHaiQuan;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayThayDoiDangKy = ngayThayDoiDangKy;
			entity.BieuThiTruongHopHetHan = bieuThiTruongHopHetHan;
			entity.TenDaiLyHaiQuan = tenDaiLyHaiQuan;
			entity.MaNhanVienHaiQuan = maNhanVienHaiQuan;
			entity.TenDDLuuKho = tenDDLuuKho;
			entity.MaPhanLoaiTongGiaCoBan = maPhanLoaiTongGiaCoBan;
			entity.PhanLoaiCongThucChuan = phanLoaiCongThucChuan;
			entity.MaPhanLoaiDieuChinhTriGia = maPhanLoaiDieuChinhTriGia;
			entity.PhuongPhapDieuChinhTriGia = phuongPhapDieuChinhTriGia;
			entity.TongTienThuePhaiNop = tongTienThuePhaiNop;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.TenTruongDonViHaiQuan = tenTruongDonViHaiQuan;
			entity.NgayCapPhep = ngayCapPhep;
			entity.PhanLoaiThamTraSauThongQuan = phanLoaiThamTraSauThongQuan;
			entity.NgayPheDuyetBP = ngayPheDuyetBP;
			entity.NgayHoanThanhKiemTraBP = ngayHoanThanhKiemTraBP;
			entity.SoNgayDoiCapPhepNhapKhau = soNgayDoiCapPhepNhapKhau;
			entity.TieuDe = tieuDe;
			entity.MaSacThueAnHan_VAT = maSacThueAnHan_VAT;
			entity.TenSacThueAnHan_VAT = tenSacThueAnHan_VAT;
			entity.HanNopThueSauKhiAnHan_VAT = hanNopThueSauKhiAnHan_VAT;
			entity.PhanLoaiNopThue = phanLoaiNopThue;
			entity.TongSoTienThueXuatKhau = tongSoTienThueXuatKhau;
			entity.MaTTTongTienThueXuatKhau = maTTTongTienThueXuatKhau;
			entity.TongSoTienLePhi = tongSoTienLePhi;
			entity.MaTTCuaSoTienBaoLanh = maTTCuaSoTienBaoLanh;
			entity.SoQuanLyNguoiSuDung = soQuanLyNguoiSuDung;
			entity.NgayHoanThanhKiemTra = ngayHoanThanhKiemTra;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, PhanLoaiBaoCaoSuaDoi);
			db.AddInParameter(dbCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, MaPhanLoaiKiemTra);
			db.AddInParameter(dbCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, MaSoThueDaiDien);
			db.AddInParameter(dbCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, TenCoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, NgayThayDoiDangKy.Year <= 1753 ? DBNull.Value : (object) NgayThayDoiDangKy);
			db.AddInParameter(dbCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, BieuThiTruongHopHetHan);
			db.AddInParameter(dbCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, TenDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, MaNhanVienHaiQuan);
			db.AddInParameter(dbCommand, "@TenDDLuuKho", SqlDbType.VarChar, TenDDLuuKho);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, MaPhanLoaiTongGiaCoBan);
			db.AddInParameter(dbCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, PhanLoaiCongThucChuan);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, MaPhanLoaiDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, PhuongPhapDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, TongTienThuePhaiNop);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, TenTruongDonViHaiQuan);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, PhanLoaiThamTraSauThongQuan);
			db.AddInParameter(dbCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, NgayPheDuyetBP.Year <= 1753 ? DBNull.Value : (object) NgayPheDuyetBP);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, NgayHoanThanhKiemTraBP.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTraBP);
			db.AddInParameter(dbCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, SoNgayDoiCapPhepNhapKhau);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.VarChar, TieuDe);
			db.AddInParameter(dbCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, MaSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, TenSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, HanNopThueSauKhiAnHan_VAT.Year <= 1753 ? DBNull.Value : (object) HanNopThueSauKhiAnHan_VAT);
			db.AddInParameter(dbCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, PhanLoaiNopThue);
			db.AddInParameter(dbCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, TongSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, MaTTTongTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TongSoTienLePhi", SqlDbType.Decimal, TongSoTienLePhi);
			db.AddInParameter(dbCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, MaTTCuaSoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, SoQuanLyNguoiSuDung);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, NgayHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTra);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ToKhaiMauDich_PhanHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_PhanHoi item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ToKhaiMauDich_PhanHoi(long id, long master_ID, string phanLoaiBaoCaoSuaDoi, string maPhanLoaiKiemTra, string maSoThueDaiDien, string tenCoQuanHaiQuan, DateTime ngayDangKy, DateTime ngayThayDoiDangKy, string bieuThiTruongHopHetHan, string tenDaiLyHaiQuan, string maNhanVienHaiQuan, string tenDDLuuKho, string maPhanLoaiTongGiaCoBan, string phanLoaiCongThucChuan, string maPhanLoaiDieuChinhTriGia, string phuongPhapDieuChinhTriGia, decimal tongTienThuePhaiNop, decimal soTienBaoLanh, string tenTruongDonViHaiQuan, DateTime ngayCapPhep, string phanLoaiThamTraSauThongQuan, DateTime ngayPheDuyetBP, DateTime ngayHoanThanhKiemTraBP, decimal soNgayDoiCapPhepNhapKhau, string tieuDe, string maSacThueAnHan_VAT, string tenSacThueAnHan_VAT, DateTime hanNopThueSauKhiAnHan_VAT, string phanLoaiNopThue, decimal tongSoTienThueXuatKhau, string maTTTongTienThueXuatKhau, decimal tongSoTienLePhi, string maTTCuaSoTienBaoLanh, string soQuanLyNguoiSuDung, DateTime ngayHoanThanhKiemTra)
		{
			KDT_VNACC_ToKhaiMauDich_PhanHoi entity = new KDT_VNACC_ToKhaiMauDich_PhanHoi();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.PhanLoaiBaoCaoSuaDoi = phanLoaiBaoCaoSuaDoi;
			entity.MaPhanLoaiKiemTra = maPhanLoaiKiemTra;
			entity.MaSoThueDaiDien = maSoThueDaiDien;
			entity.TenCoQuanHaiQuan = tenCoQuanHaiQuan;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayThayDoiDangKy = ngayThayDoiDangKy;
			entity.BieuThiTruongHopHetHan = bieuThiTruongHopHetHan;
			entity.TenDaiLyHaiQuan = tenDaiLyHaiQuan;
			entity.MaNhanVienHaiQuan = maNhanVienHaiQuan;
			entity.TenDDLuuKho = tenDDLuuKho;
			entity.MaPhanLoaiTongGiaCoBan = maPhanLoaiTongGiaCoBan;
			entity.PhanLoaiCongThucChuan = phanLoaiCongThucChuan;
			entity.MaPhanLoaiDieuChinhTriGia = maPhanLoaiDieuChinhTriGia;
			entity.PhuongPhapDieuChinhTriGia = phuongPhapDieuChinhTriGia;
			entity.TongTienThuePhaiNop = tongTienThuePhaiNop;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.TenTruongDonViHaiQuan = tenTruongDonViHaiQuan;
			entity.NgayCapPhep = ngayCapPhep;
			entity.PhanLoaiThamTraSauThongQuan = phanLoaiThamTraSauThongQuan;
			entity.NgayPheDuyetBP = ngayPheDuyetBP;
			entity.NgayHoanThanhKiemTraBP = ngayHoanThanhKiemTraBP;
			entity.SoNgayDoiCapPhepNhapKhau = soNgayDoiCapPhepNhapKhau;
			entity.TieuDe = tieuDe;
			entity.MaSacThueAnHan_VAT = maSacThueAnHan_VAT;
			entity.TenSacThueAnHan_VAT = tenSacThueAnHan_VAT;
			entity.HanNopThueSauKhiAnHan_VAT = hanNopThueSauKhiAnHan_VAT;
			entity.PhanLoaiNopThue = phanLoaiNopThue;
			entity.TongSoTienThueXuatKhau = tongSoTienThueXuatKhau;
			entity.MaTTTongTienThueXuatKhau = maTTTongTienThueXuatKhau;
			entity.TongSoTienLePhi = tongSoTienLePhi;
			entity.MaTTCuaSoTienBaoLanh = maTTCuaSoTienBaoLanh;
			entity.SoQuanLyNguoiSuDung = soQuanLyNguoiSuDung;
			entity.NgayHoanThanhKiemTra = ngayHoanThanhKiemTra;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, PhanLoaiBaoCaoSuaDoi);
			db.AddInParameter(dbCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, MaPhanLoaiKiemTra);
			db.AddInParameter(dbCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, MaSoThueDaiDien);
			db.AddInParameter(dbCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, TenCoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, NgayThayDoiDangKy.Year <= 1753 ? DBNull.Value : (object) NgayThayDoiDangKy);
			db.AddInParameter(dbCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, BieuThiTruongHopHetHan);
			db.AddInParameter(dbCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, TenDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, MaNhanVienHaiQuan);
			db.AddInParameter(dbCommand, "@TenDDLuuKho", SqlDbType.VarChar, TenDDLuuKho);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, MaPhanLoaiTongGiaCoBan);
			db.AddInParameter(dbCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, PhanLoaiCongThucChuan);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, MaPhanLoaiDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, PhuongPhapDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, TongTienThuePhaiNop);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, TenTruongDonViHaiQuan);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, PhanLoaiThamTraSauThongQuan);
			db.AddInParameter(dbCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, NgayPheDuyetBP.Year <= 1753 ? DBNull.Value : (object) NgayPheDuyetBP);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, NgayHoanThanhKiemTraBP.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTraBP);
			db.AddInParameter(dbCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, SoNgayDoiCapPhepNhapKhau);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.VarChar, TieuDe);
			db.AddInParameter(dbCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, MaSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, TenSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, HanNopThueSauKhiAnHan_VAT.Year <= 1753 ? DBNull.Value : (object) HanNopThueSauKhiAnHan_VAT);
			db.AddInParameter(dbCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, PhanLoaiNopThue);
			db.AddInParameter(dbCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, TongSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, MaTTTongTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TongSoTienLePhi", SqlDbType.Decimal, TongSoTienLePhi);
			db.AddInParameter(dbCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, MaTTCuaSoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, SoQuanLyNguoiSuDung);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, NgayHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTra);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ToKhaiMauDich_PhanHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_PhanHoi item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ToKhaiMauDich_PhanHoi(long id, long master_ID, string phanLoaiBaoCaoSuaDoi, string maPhanLoaiKiemTra, string maSoThueDaiDien, string tenCoQuanHaiQuan, DateTime ngayDangKy, DateTime ngayThayDoiDangKy, string bieuThiTruongHopHetHan, string tenDaiLyHaiQuan, string maNhanVienHaiQuan, string tenDDLuuKho, string maPhanLoaiTongGiaCoBan, string phanLoaiCongThucChuan, string maPhanLoaiDieuChinhTriGia, string phuongPhapDieuChinhTriGia, decimal tongTienThuePhaiNop, decimal soTienBaoLanh, string tenTruongDonViHaiQuan, DateTime ngayCapPhep, string phanLoaiThamTraSauThongQuan, DateTime ngayPheDuyetBP, DateTime ngayHoanThanhKiemTraBP, decimal soNgayDoiCapPhepNhapKhau, string tieuDe, string maSacThueAnHan_VAT, string tenSacThueAnHan_VAT, DateTime hanNopThueSauKhiAnHan_VAT, string phanLoaiNopThue, decimal tongSoTienThueXuatKhau, string maTTTongTienThueXuatKhau, decimal tongSoTienLePhi, string maTTCuaSoTienBaoLanh, string soQuanLyNguoiSuDung, DateTime ngayHoanThanhKiemTra)
		{
			KDT_VNACC_ToKhaiMauDich_PhanHoi entity = new KDT_VNACC_ToKhaiMauDich_PhanHoi();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.PhanLoaiBaoCaoSuaDoi = phanLoaiBaoCaoSuaDoi;
			entity.MaPhanLoaiKiemTra = maPhanLoaiKiemTra;
			entity.MaSoThueDaiDien = maSoThueDaiDien;
			entity.TenCoQuanHaiQuan = tenCoQuanHaiQuan;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayThayDoiDangKy = ngayThayDoiDangKy;
			entity.BieuThiTruongHopHetHan = bieuThiTruongHopHetHan;
			entity.TenDaiLyHaiQuan = tenDaiLyHaiQuan;
			entity.MaNhanVienHaiQuan = maNhanVienHaiQuan;
			entity.TenDDLuuKho = tenDDLuuKho;
			entity.MaPhanLoaiTongGiaCoBan = maPhanLoaiTongGiaCoBan;
			entity.PhanLoaiCongThucChuan = phanLoaiCongThucChuan;
			entity.MaPhanLoaiDieuChinhTriGia = maPhanLoaiDieuChinhTriGia;
			entity.PhuongPhapDieuChinhTriGia = phuongPhapDieuChinhTriGia;
			entity.TongTienThuePhaiNop = tongTienThuePhaiNop;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.TenTruongDonViHaiQuan = tenTruongDonViHaiQuan;
			entity.NgayCapPhep = ngayCapPhep;
			entity.PhanLoaiThamTraSauThongQuan = phanLoaiThamTraSauThongQuan;
			entity.NgayPheDuyetBP = ngayPheDuyetBP;
			entity.NgayHoanThanhKiemTraBP = ngayHoanThanhKiemTraBP;
			entity.SoNgayDoiCapPhepNhapKhau = soNgayDoiCapPhepNhapKhau;
			entity.TieuDe = tieuDe;
			entity.MaSacThueAnHan_VAT = maSacThueAnHan_VAT;
			entity.TenSacThueAnHan_VAT = tenSacThueAnHan_VAT;
			entity.HanNopThueSauKhiAnHan_VAT = hanNopThueSauKhiAnHan_VAT;
			entity.PhanLoaiNopThue = phanLoaiNopThue;
			entity.TongSoTienThueXuatKhau = tongSoTienThueXuatKhau;
			entity.MaTTTongTienThueXuatKhau = maTTTongTienThueXuatKhau;
			entity.TongSoTienLePhi = tongSoTienLePhi;
			entity.MaTTCuaSoTienBaoLanh = maTTCuaSoTienBaoLanh;
			entity.SoQuanLyNguoiSuDung = soQuanLyNguoiSuDung;
			entity.NgayHoanThanhKiemTra = ngayHoanThanhKiemTra;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, PhanLoaiBaoCaoSuaDoi);
			db.AddInParameter(dbCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, MaPhanLoaiKiemTra);
			db.AddInParameter(dbCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, MaSoThueDaiDien);
			db.AddInParameter(dbCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, TenCoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, NgayThayDoiDangKy.Year <= 1753 ? DBNull.Value : (object) NgayThayDoiDangKy);
			db.AddInParameter(dbCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, BieuThiTruongHopHetHan);
			db.AddInParameter(dbCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, TenDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, MaNhanVienHaiQuan);
			db.AddInParameter(dbCommand, "@TenDDLuuKho", SqlDbType.VarChar, TenDDLuuKho);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, MaPhanLoaiTongGiaCoBan);
			db.AddInParameter(dbCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, PhanLoaiCongThucChuan);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, MaPhanLoaiDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, PhuongPhapDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, TongTienThuePhaiNop);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, TenTruongDonViHaiQuan);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, PhanLoaiThamTraSauThongQuan);
			db.AddInParameter(dbCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, NgayPheDuyetBP.Year <= 1753 ? DBNull.Value : (object) NgayPheDuyetBP);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, NgayHoanThanhKiemTraBP.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTraBP);
			db.AddInParameter(dbCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, SoNgayDoiCapPhepNhapKhau);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.VarChar, TieuDe);
			db.AddInParameter(dbCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, MaSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, TenSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, HanNopThueSauKhiAnHan_VAT.Year <= 1753 ? DBNull.Value : (object) HanNopThueSauKhiAnHan_VAT);
			db.AddInParameter(dbCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, PhanLoaiNopThue);
			db.AddInParameter(dbCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, TongSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, MaTTTongTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TongSoTienLePhi", SqlDbType.Decimal, TongSoTienLePhi);
			db.AddInParameter(dbCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, MaTTCuaSoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, SoQuanLyNguoiSuDung);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, NgayHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTra);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ToKhaiMauDich_PhanHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_PhanHoi item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ToKhaiMauDich_PhanHoi(long id)
		{
			KDT_VNACC_ToKhaiMauDich_PhanHoi entity = new KDT_VNACC_ToKhaiMauDich_PhanHoi();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ToKhaiMauDich_PhanHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_PhanHoi item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}