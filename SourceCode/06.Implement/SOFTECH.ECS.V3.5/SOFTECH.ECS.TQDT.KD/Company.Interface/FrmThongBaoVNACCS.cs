﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using System.IO;
using System.Xml;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class FrmThongBaoVNACCS : BaseForm
    {
        public FrmThongBaoVNACCS()
        {
            InitializeComponent();
        }
        public void EditHTML(string tieude, string link)
        {
            try
            {
                webBrowser1.Navigate(new Uri(link));
            }
            catch
            {
                MessageBox.Show("Invalid Url.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void ReadXML()
        {
            XmlDocument loadDoc = new XmlDocument();
            loadDoc.Load("http://ecs.softech.cloud/AutoUpdate/Help/ThongBao.xml");
            foreach (XmlNode favNodeParent in loadDoc.SelectNodes("/Root/ParentNode"))
            {
                foreach (XmlNode favNodeMain in favNodeParent)
                {
                    TreeListNode empNode = tlHelp.AppendNode(null, null);
                    if (favNodeMain.Name == "ChildNodeMain")
                    {
                        empNode.SetValue("name", favNodeMain.InnerText);
                        empNode.ImageIndex = 0;
                        empNode.SelectImageIndex = 1;
                    }
                    foreach (XmlNode favNode in favNodeParent)
                    {
                        if (favNode.Name != "ChildNodeMain")
                        {
                            TreeListNode empNodeChild = tlHelp.AppendNode(null, empNode);
                            empNodeChild.SetValue("name", favNode.Attributes["name"].InnerText);
                            empNodeChild.SetValue("value", favNode.Attributes["url"].InnerText);
                            empNodeChild.ImageIndex = 2;
                            empNodeChild.SelectImageIndex = 2;
                        }
                    }
                    break;
                }

            }
            tlHelp.ExpandAll();
        }

        private void tlHelp_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string TieuDe = tlHelp.FocusedNode.GetValue(0).ToString();
                string LinkPath = tlHelp.FocusedNode.GetValue(1).ToString();
                EditHTML(TieuDe, LinkPath);
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void ckbKhongHienThi_CheckedChanged(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ThongBaoVNACCS", ckbKhongHienThi.Checked.ToString());
        }

        private void FrmThongBaoVNACCS_Load(object sender, EventArgs e)
        {
            try
            {
                ckbKhongHienThi.Checked = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ThongBaoVNACCS", "false"));
                ReadXML();
                EditHTML("Thông báo", "http://ecs.softech.cloud/AutoUpdate/help/HuongDanKyTuDongChoFileDinhKem.mht");
                //tlHelp.TreeLineStyle = LineStyle.Large;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
    }
}