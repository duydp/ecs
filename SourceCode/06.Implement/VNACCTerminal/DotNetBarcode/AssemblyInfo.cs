﻿// Assembly DotNetBarcode, Version 2.4.2480.34660

[assembly: System.Reflection.AssemblyVersion("2.4.2480.34660")]
[assembly: System.Reflection.AssemblyCompany("DotNetBarcode")]
[assembly: System.Reflection.AssemblyTitle("DotNetBarcode2.4.0 クラス・ライブラリ")]
[assembly: System.Reflection.AssemblyProduct("DotNetBarcode")]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: System.Reflection.AssemblyCopyright("Copyright (C) 2004-2006 DotNetBarcode. All Rights Reserved")]
[assembly: System.Reflection.AssemblyDescription("QRCode対応完全無料ライブラリDotNetBarcodeV2.4.0")]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Runtime.InteropServices.Guid("FB95E399-3BD9-4F41-A404-E213A56940DB")]
[assembly: System.CLSCompliant(true)]
[assembly: System.Reflection.AssemblyTrademark("Copyright (C) 2004-2006 DotNetBarcode. All Rights Reserved")]

