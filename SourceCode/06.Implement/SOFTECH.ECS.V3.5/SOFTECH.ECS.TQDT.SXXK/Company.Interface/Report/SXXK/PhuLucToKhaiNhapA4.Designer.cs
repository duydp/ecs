﻿namespace Company.Interface.Report.SXXK
{
    partial class PhuLucToKhaiNhapA4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblMaHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblbanLuuHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine37 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine36 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine35 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine34 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine33 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine32 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine31 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine30 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine29 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine28 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine27 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine26 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine25 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine24 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine23 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine22 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine21 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine20 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine19 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine18 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine17 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine16 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine15 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThongBaoMienThue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThueXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.BackColor = System.Drawing.Color.Transparent;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMaHQ,
            this.lblbanLuuHQ,
            this.xrLine37,
            this.xrLine36,
            this.xrLine35,
            this.xrLine34,
            this.xrLine33,
            this.xrLine32,
            this.xrLine31,
            this.xrLine30,
            this.xrLine29,
            this.xrLine28,
            this.xrLine27,
            this.xrLine26,
            this.xrLine25,
            this.xrLine24,
            this.xrLine23,
            this.xrLine22,
            this.xrLine21,
            this.xrLine20,
            this.xrLine19,
            this.xrLine18,
            this.xrLine17,
            this.xrLine16,
            this.xrLine15,
            this.xrLine14,
            this.xrLine13,
            this.xrLine12,
            this.xrLine11,
            this.xrLine10,
            this.xrLine9,
            this.xrLine8,
            this.xrLine7,
            this.xrLine6,
            this.xrLine5,
            this.xrLine4,
            this.xrLine3,
            this.xrLine2,
            this.xrLine1,
            this.xrTable3,
            this.lblThongBaoMienThue,
            this.xrLabel5,
            this.xrLabel6,
            this.xrLabel7,
            this.xrLabel8,
            this.xrLabel4,
            this.lblNgayDangKy,
            this.xrLabel3,
            this.lblSoToKhai,
            this.xrLabel1,
            this.xrTable2,
            this.xrTable1,
            this.xrLabel9,
            this.xrLabel10});
            this.Detail.Height = 2301;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaHQ
            // 
            this.lblMaHQ.Location = new System.Drawing.Point(424, 75);
            this.lblMaHQ.Name = "lblMaHQ";
            this.lblMaHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHQ.Size = new System.Drawing.Size(95, 17);
            this.lblMaHQ.Tag = "Mã loại hình";
            this.lblMaHQ.Text = "SXXK/C60B";
            this.lblMaHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblbanLuuHQ
            // 
            this.lblbanLuuHQ.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.lblbanLuuHQ.Location = new System.Drawing.Point(217, 32);
            this.lblbanLuuHQ.Name = "lblbanLuuHQ";
            this.lblbanLuuHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblbanLuuHQ.Size = new System.Drawing.Size(392, 26);
            this.lblbanLuuHQ.StylePriority.UseFont = false;
            this.lblbanLuuHQ.StylePriority.UseTextAlignment = false;
            this.lblbanLuuHQ.Text = "Bản lưu nguoi khai Hai quan";
            this.lblbanLuuHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine37
            // 
            this.xrLine37.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine37.Location = new System.Drawing.Point(17, 2187);
            this.xrLine37.Name = "xrLine37";
            this.xrLine37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine37.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine36
            // 
            this.xrLine36.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine36.Location = new System.Drawing.Point(17, 2152);
            this.xrLine36.Name = "xrLine36";
            this.xrLine36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine36.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine35
            // 
            this.xrLine35.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine35.Location = new System.Drawing.Point(17, 2117);
            this.xrLine35.Name = "xrLine35";
            this.xrLine35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine35.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine34
            // 
            this.xrLine34.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine34.Location = new System.Drawing.Point(17, 2082);
            this.xrLine34.Name = "xrLine34";
            this.xrLine34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine34.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine33
            // 
            this.xrLine33.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine33.Location = new System.Drawing.Point(17, 2047);
            this.xrLine33.Name = "xrLine33";
            this.xrLine33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine33.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine32
            // 
            this.xrLine32.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine32.Location = new System.Drawing.Point(17, 2012);
            this.xrLine32.Name = "xrLine32";
            this.xrLine32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine32.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine31
            // 
            this.xrLine31.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine31.Location = new System.Drawing.Point(17, 1977);
            this.xrLine31.Name = "xrLine31";
            this.xrLine31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine31.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine30
            // 
            this.xrLine30.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine30.Location = new System.Drawing.Point(17, 1942);
            this.xrLine30.Name = "xrLine30";
            this.xrLine30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine30.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine29
            // 
            this.xrLine29.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine29.Location = new System.Drawing.Point(17, 1792);
            this.xrLine29.Name = "xrLine29";
            this.xrLine29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine29.Size = new System.Drawing.Size(658, 25);
            // 
            // xrLine28
            // 
            this.xrLine28.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine28.Location = new System.Drawing.Point(17, 1757);
            this.xrLine28.Name = "xrLine28";
            this.xrLine28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine28.Size = new System.Drawing.Size(658, 25);
            // 
            // xrLine27
            // 
            this.xrLine27.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine27.Location = new System.Drawing.Point(17, 1722);
            this.xrLine27.Name = "xrLine27";
            this.xrLine27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine27.Size = new System.Drawing.Size(658, 25);
            // 
            // xrLine26
            // 
            this.xrLine26.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine26.Location = new System.Drawing.Point(17, 1687);
            this.xrLine26.Name = "xrLine26";
            this.xrLine26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine26.Size = new System.Drawing.Size(658, 25);
            // 
            // xrLine25
            // 
            this.xrLine25.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine25.Location = new System.Drawing.Point(17, 1652);
            this.xrLine25.Name = "xrLine25";
            this.xrLine25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine25.Size = new System.Drawing.Size(658, 25);
            // 
            // xrLine24
            // 
            this.xrLine24.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine24.Location = new System.Drawing.Point(17, 1617);
            this.xrLine24.Name = "xrLine24";
            this.xrLine24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine24.Size = new System.Drawing.Size(658, 25);
            // 
            // xrLine23
            // 
            this.xrLine23.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine23.Location = new System.Drawing.Point(17, 1582);
            this.xrLine23.Name = "xrLine23";
            this.xrLine23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine23.Size = new System.Drawing.Size(658, 25);
            // 
            // xrLine22
            // 
            this.xrLine22.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine22.Location = new System.Drawing.Point(17, 1547);
            this.xrLine22.Name = "xrLine22";
            this.xrLine22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine22.Size = new System.Drawing.Size(658, 25);
            // 
            // xrLine21
            // 
            this.xrLine21.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine21.Location = new System.Drawing.Point(17, 1400);
            this.xrLine21.Name = "xrLine21";
            this.xrLine21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine21.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine20
            // 
            this.xrLine20.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine20.Location = new System.Drawing.Point(17, 1365);
            this.xrLine20.Name = "xrLine20";
            this.xrLine20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine20.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine19
            // 
            this.xrLine19.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine19.Location = new System.Drawing.Point(17, 1330);
            this.xrLine19.Name = "xrLine19";
            this.xrLine19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine19.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine18
            // 
            this.xrLine18.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine18.Location = new System.Drawing.Point(17, 1295);
            this.xrLine18.Name = "xrLine18";
            this.xrLine18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine18.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine17
            // 
            this.xrLine17.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine17.Location = new System.Drawing.Point(17, 1260);
            this.xrLine17.Name = "xrLine17";
            this.xrLine17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine17.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine16
            // 
            this.xrLine16.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine16.Location = new System.Drawing.Point(17, 945);
            this.xrLine16.Name = "xrLine16";
            this.xrLine16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine16.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine15
            // 
            this.xrLine15.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine15.Location = new System.Drawing.Point(17, 906);
            this.xrLine15.Name = "xrLine15";
            this.xrLine15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine15.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine14
            // 
            this.xrLine14.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine14.Location = new System.Drawing.Point(17, 867);
            this.xrLine14.Name = "xrLine14";
            this.xrLine14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine14.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine13
            // 
            this.xrLine13.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine13.Location = new System.Drawing.Point(17, 828);
            this.xrLine13.Name = "xrLine13";
            this.xrLine13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine13.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine12
            // 
            this.xrLine12.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine12.Location = new System.Drawing.Point(17, 789);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine12.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine11
            // 
            this.xrLine11.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine11.Location = new System.Drawing.Point(17, 750);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine11.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine10
            // 
            this.xrLine10.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine10.Location = new System.Drawing.Point(17, 710);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine10.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine9
            // 
            this.xrLine9.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine9.Location = new System.Drawing.Point(17, 673);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine9.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine8
            // 
            this.xrLine8.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine8.Location = new System.Drawing.Point(17, 486);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine8.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine7
            // 
            this.xrLine7.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine7.Location = new System.Drawing.Point(17, 450);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine7.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine6
            // 
            this.xrLine6.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine6.Location = new System.Drawing.Point(17, 415);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine6.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine5
            // 
            this.xrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine5.Location = new System.Drawing.Point(17, 375);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine5.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine4
            // 
            this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine4.Location = new System.Drawing.Point(17, 340);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine4.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine3
            // 
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine3.Location = new System.Drawing.Point(17, 267);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine3.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine2
            // 
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine2.Location = new System.Drawing.Point(17, 302);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine2.Size = new System.Drawing.Size(775, 25);
            // 
            // xrLine1
            // 
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine1.Location = new System.Drawing.Point(17, 228);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.Size = new System.Drawing.Size(775, 25);
            // 
            // xrTable3
            // 
            this.xrTable3.Location = new System.Drawing.Point(17, 1170);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29,
            this.xrTableRow28,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40,
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow52,
            this.xrTableRow27,
            this.xrTableRow41});
            this.xrTable3.Size = new System.Drawing.Size(775, 1106);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow29.Weight = 0.03074141048824593;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell119.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell119.Text = "B- PHẦN DÀNH CHO KIỂM TRA CỦA HẢI QUAN";
            this.xrTableCell119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell119.Weight = 1;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell110});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow28.Weight = 0.03074141048824593;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell110.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell110.Text = "I- PHẦN KIỂM TRA HÀNG HÓA";
            this.xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell110.Weight = 1;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow30.Weight = 0.189873417721519;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell136.Weight = 1;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell145});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow31.Weight = 0.031645569620253167;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell145.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell145.Text = "II- PHẦN KIỂM TRA THUẾ";
            this.xrTableCell145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell145.Weight = 1;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell146,
            this.xrTableCell147,
            this.xrTableCell149,
            this.xrTableCell150,
            this.xrTableCell151,
            this.xrTableCell153});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow32.Weight = 0.036166365280289332;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell146.Multiline = true;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell146.Text = "SỐ\r\n TT";
            this.xrTableCell146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell146.Weight = 0.052903225806451612;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell147.Text = " Mã số hàng hóa";
            this.xrTableCell147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell147.Weight = 0.21548387096774194;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell149.Text = "Lượng";
            this.xrTableCell149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell149.Weight = 0.18322580645161291;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell150.Text = "Xuất xứ";
            this.xrTableCell150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell150.Weight = 0.18193548387096775;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell151.Text = "Đơn giá tính thuế";
            this.xrTableCell151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell151.Weight = 0.21548387096774194;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.xrTableCell153.Text = "36. Cán bộ kiểm tra  thuế (Ký ghi rõ họ";
            this.xrTableCell153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell153.Weight = 0.15096774193548387;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155,
            this.xrTableCell156,
            this.xrTableCell158,
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell162});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow33.Weight = 0.031645569620253167;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell155.Text = "1";
            this.xrTableCell155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell155.Weight = 0.052903225806451612;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell156.Weight = 0.21548387096774194;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell158.Weight = 0.18322580645161291;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell159.Weight = 0.18193548387096775;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell160.Weight = 0.21548387096774194;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.xrTableCell162.Text = "tên, ngày, tháng , năm)";
            this.xrTableCell162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell162.Weight = 0.15096774193548387;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164,
            this.xrTableCell165,
            this.xrTableCell167,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell171});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow34.Weight = 0.031645569620253167;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell164.Text = "2";
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell164.Weight = 0.052903225806451612;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell165.Weight = 0.21548387096774194;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell167.Weight = 0.18322580645161291;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell168.Weight = 0.18193548387096775;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell169.Weight = 0.21548387096774194;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell171.Weight = 0.15096774193548387;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell176,
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell180});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow35.Weight = 0.031645569620253167;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell173.Text = "3";
            this.xrTableCell173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell173.Weight = 0.052903225806451612;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell174.Weight = 0.21548387096774194;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell176.Weight = 0.18322580645161291;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell177.Weight = 0.18193548387096775;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell178.Weight = 0.21548387096774194;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell180.Weight = 0.15096774193548387;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell182,
            this.xrTableCell183,
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell187,
            this.xrTableCell189});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow36.Weight = 0.031645569620253167;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell182.Text = "4";
            this.xrTableCell182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell182.Weight = 0.052903225806451612;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell183.Weight = 0.21548387096774194;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell185.Weight = 0.18322580645161291;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell186.Weight = 0.18193548387096775;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell187.Weight = 0.21548387096774194;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell189.Weight = 0.15096774193548387;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell194,
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell198});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow37.Weight = 0.031645569620253167;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell191.Text = "5";
            this.xrTableCell191.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell191.Weight = 0.052903225806451612;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell192.Weight = 0.21548387096774194;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell194.Weight = 0.18322580645161291;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell195.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell195.Weight = 0.18193548387096775;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell196.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell196.Weight = 0.21548387096774194;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell198.Weight = 0.15096774193548387;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell200,
            this.xrTableCell201,
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell205,
            this.xrTableCell207});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow38.Weight = 0.031645569620253167;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell200.Text = "6";
            this.xrTableCell200.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell200.Weight = 0.052903225806451612;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell201.Weight = 0.21548387096774194;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell203.Weight = 0.18322580645161291;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell204.Weight = 0.18193548387096775;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell205.Weight = 0.21548387096774194;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell207.Weight = 0.15096774193548387;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell209,
            this.xrTableCell210,
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell214,
            this.xrTableCell216});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow39.Weight = 0.031645569620253167;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell209.Text = "7";
            this.xrTableCell209.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell209.Weight = 0.052903225806451612;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell210.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell210.Weight = 0.21548387096774194;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell212.Weight = 0.18322580645161291;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell213.Weight = 0.18193548387096775;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell214.Weight = 0.21548387096774194;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell216.Weight = 0.15096774193548387;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell218,
            this.xrTableCell219,
            this.xrTableCell221,
            this.xrTableCell222,
            this.xrTableCell223,
            this.xrTableCell225});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow40.Weight = 0.031645569620253167;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell218.Text = "8";
            this.xrTableCell218.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell218.Weight = 0.052903225806451612;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell219.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell219.Weight = 0.21548387096774194;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell221.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell221.Weight = 0.18322580645161291;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell222.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell222.Weight = 0.18193548387096775;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell223.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell223.Weight = 0.21548387096774194;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell225.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell225.Weight = 0.15096774193548387;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell236,
            this.xrTableCell237,
            this.xrTableCell239,
            this.xrTableCell240,
            this.xrTableCell241,
            this.xrTableCell243});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow42.Weight = 0.031645569620253167;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell236.Text = "9";
            this.xrTableCell236.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell236.Weight = 0.052903225806451612;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell237.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell237.Weight = 0.21548387096774194;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell239.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell239.Weight = 0.18322580645161291;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell240.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell240.Weight = 0.18193548387096775;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell241.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell241.Weight = 0.21548387096774194;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell243.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell243.Weight = 0.15096774193548387;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell245,
            this.xrTableCell246,
            this.xrTableCell249,
            this.xrTableCell252});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow43.Weight = 0.031645569620253167;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell245.Text = "SỐ";
            this.xrTableCell245.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell245.Weight = 0.052903225806451612;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell246.Text = "Tiền thuế nhập khẩu";
            this.xrTableCell246.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell246.Weight = 0.39870967741935481;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell249.Text = "Tiền thuế GTGT (hoặc TTĐB)";
            this.xrTableCell249.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell249.Weight = 0.39741935483870966;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell252.Text = "Thu khác";
            this.xrTableCell252.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell252.Weight = 0.15096774193548387;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell254,
            this.xrTableCell255,
            this.xrTableCell256,
            this.xrTableCell257,
            this.xrTableCell258,
            this.xrTableCell259,
            this.xrTableCell260,
            this.xrTableCell261,
            this.xrTableCell262});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow44.Weight = 0.0433996383363472;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell254.Text = "TT";
            this.xrTableCell254.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell254.Weight = 0.052903225806451612;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell255.Multiline = true;
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell255.Text = "Trị giá tính thuế\r\n(VNĐ)";
            this.xrTableCell255.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell255.Weight = 0.16129032258064516;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell256.Text = "Thuế suất (%)";
            this.xrTableCell256.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell256.Weight = 0.054193548387096772;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell257.Text = "Tiền thuế";
            this.xrTableCell257.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell257.Weight = 0.18322580645161291;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell258.Text = "Trị giá tính thuế";
            this.xrTableCell258.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell258.Weight = 0.18193548387096775;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell259.Text = "Thuế suất (%)";
            this.xrTableCell259.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell259.Weight = 0.054193548387096772;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell260.Text = "Tiền thuế";
            this.xrTableCell260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell260.Weight = 0.16129032258064516;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell261.Multiline = true;
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell261.Text = "Tỷ lệ\r\n(%)";
            this.xrTableCell261.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell261.Weight = 0.054193548387096772;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell262.Text = "Số tiền";
            this.xrTableCell262.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell262.Weight = 0.0967741935483871;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell265,
            this.xrTableCell266,
            this.xrTableCell267,
            this.xrTableCell268,
            this.xrTableCell269,
            this.xrTableCell270,
            this.xrTableCell271});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow45.Weight = 0.031645569620253167;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell263.Text = "1";
            this.xrTableCell263.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell263.Weight = 0.052903225806451612;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell264.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell264.Weight = 0.16129032258064516;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell265.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell265.Weight = 0.054193548387096772;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell266.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell266.Weight = 0.18322580645161291;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell267.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell267.Weight = 0.18193548387096775;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell268.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell268.Weight = 0.054193548387096772;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell269.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell269.Weight = 0.16129032258064516;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell270.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell270.Weight = 0.054193548387096772;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell271.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell271.Weight = 0.0967741935483871;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell272,
            this.xrTableCell273,
            this.xrTableCell274,
            this.xrTableCell275,
            this.xrTableCell276,
            this.xrTableCell277,
            this.xrTableCell278,
            this.xrTableCell279,
            this.xrTableCell280});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow46.Weight = 0.031645569620253167;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell272.Text = "2";
            this.xrTableCell272.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell272.Weight = 0.052903225806451612;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell273.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell273.Weight = 0.16129032258064516;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell274.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell274.Weight = 0.054193548387096772;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell275.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell275.Weight = 0.18322580645161291;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell276.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell276.Weight = 0.18193548387096775;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell277.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell277.Weight = 0.054193548387096772;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell278.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell278.Weight = 0.16129032258064516;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell279.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell279.Weight = 0.054193548387096772;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell280.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell280.Weight = 0.0967741935483871;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell281,
            this.xrTableCell282,
            this.xrTableCell283,
            this.xrTableCell284,
            this.xrTableCell285,
            this.xrTableCell286,
            this.xrTableCell287,
            this.xrTableCell288,
            this.xrTableCell289});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow47.Weight = 0.031645569620253167;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell281.Text = "3";
            this.xrTableCell281.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell281.Weight = 0.052903225806451612;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell282.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell282.Weight = 0.16129032258064516;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell283.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell283.Weight = 0.054193548387096772;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell284.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell284.Weight = 0.18322580645161291;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell285.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell285.Weight = 0.18193548387096775;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell286.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell286.Weight = 0.054193548387096772;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell287.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell287.Weight = 0.16129032258064516;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell288.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell288.Weight = 0.054193548387096772;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell289.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell289.Weight = 0.0967741935483871;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell290,
            this.xrTableCell291,
            this.xrTableCell292,
            this.xrTableCell293,
            this.xrTableCell294,
            this.xrTableCell295,
            this.xrTableCell296,
            this.xrTableCell297,
            this.xrTableCell298});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow48.Weight = 0.031645569620253167;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell290.Text = "4";
            this.xrTableCell290.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell290.Weight = 0.052903225806451612;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell291.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell291.Weight = 0.16129032258064516;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell292.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell292.Weight = 0.054193548387096772;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell293.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell293.Weight = 0.18322580645161291;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell294.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell294.Weight = 0.18193548387096775;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell295.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell295.Weight = 0.054193548387096772;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell296.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell296.Weight = 0.16129032258064516;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell297.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell297.Weight = 0.054193548387096772;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell298.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell298.Weight = 0.0967741935483871;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell299,
            this.xrTableCell300,
            this.xrTableCell301,
            this.xrTableCell302,
            this.xrTableCell303,
            this.xrTableCell304,
            this.xrTableCell305,
            this.xrTableCell306,
            this.xrTableCell307});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow49.Weight = 0.031645569620253167;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell299.Text = "5";
            this.xrTableCell299.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell299.Weight = 0.052903225806451612;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell300.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell300.Weight = 0.16129032258064516;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell301.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell301.Weight = 0.054193548387096772;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell302.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell302.Weight = 0.18322580645161291;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell303.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell303.Weight = 0.18193548387096775;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell304.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell304.Weight = 0.054193548387096772;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell305.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell305.Weight = 0.16129032258064516;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell306.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell306.Weight = 0.054193548387096772;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell307.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell307.Weight = 0.0967741935483871;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell308,
            this.xrTableCell309,
            this.xrTableCell310,
            this.xrTableCell311,
            this.xrTableCell312,
            this.xrTableCell313,
            this.xrTableCell314,
            this.xrTableCell315,
            this.xrTableCell316});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow50.Weight = 0.031645569620253167;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell308.Text = "6";
            this.xrTableCell308.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell308.Weight = 0.052903225806451612;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell309.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell309.Weight = 0.16129032258064516;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell310.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell310.Weight = 0.054193548387096772;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell311.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell311.Weight = 0.18322580645161291;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell312.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell312.Weight = 0.18193548387096775;
            // 
            // xrTableCell313
            // 
            this.xrTableCell313.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell313.Name = "xrTableCell313";
            this.xrTableCell313.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell313.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell313.Weight = 0.054193548387096772;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell314.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell314.Weight = 0.16129032258064516;
            // 
            // xrTableCell315
            // 
            this.xrTableCell315.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell315.Name = "xrTableCell315";
            this.xrTableCell315.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell315.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell315.Weight = 0.054193548387096772;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell316.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell316.Weight = 0.0967741935483871;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell317,
            this.xrTableCell318,
            this.xrTableCell319,
            this.xrTableCell320,
            this.xrTableCell321,
            this.xrTableCell322,
            this.xrTableCell323,
            this.xrTableCell324,
            this.xrTableCell325});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow51.Weight = 0.031645569620253167;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell317.Text = "7";
            this.xrTableCell317.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell317.Weight = 0.052903225806451612;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell318.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell318.Weight = 0.16129032258064516;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell319.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell319.Weight = 0.054193548387096772;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell320.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell320.Weight = 0.18322580645161291;
            // 
            // xrTableCell321
            // 
            this.xrTableCell321.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell321.Name = "xrTableCell321";
            this.xrTableCell321.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell321.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell321.Weight = 0.18193548387096775;
            // 
            // xrTableCell322
            // 
            this.xrTableCell322.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell322.Name = "xrTableCell322";
            this.xrTableCell322.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell322.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell322.Weight = 0.054193548387096772;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell323.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell323.Weight = 0.16129032258064516;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell324.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell324.Weight = 0.054193548387096772;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell325.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell325.Weight = 0.0967741935483871;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell326,
            this.xrTableCell327,
            this.xrTableCell328,
            this.xrTableCell329,
            this.xrTableCell330,
            this.xrTableCell331,
            this.xrTableCell332,
            this.xrTableCell333,
            this.xrTableCell334});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow52.Weight = 0.031645569620253167;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell326.Text = "8";
            this.xrTableCell326.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell326.Weight = 0.052903225806451612;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell327.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell327.Weight = 0.16129032258064516;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell328.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell328.Weight = 0.054193548387096772;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell329.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell329.Weight = 0.18322580645161291;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell330.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell330.Weight = 0.18193548387096775;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell331.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell331.Weight = 0.054193548387096772;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell332.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell332.Weight = 0.16129032258064516;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell333.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell333.Weight = 0.054193548387096772;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell334.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell334.Weight = 0.0967741935483871;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.xrTableCell86,
            this.xrTableCell102,
            this.xrTableCell103,
            this.xrTableCell104,
            this.xrTableCell105,
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell109});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow27.Weight = 0.031645569620253167;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell83.Text = "9";
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell83.Weight = 0.052903225806451612;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell86.Weight = 0.16129032258064516;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell102.Weight = 0.054193548387096772;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell103.Weight = 0.18322580645161291;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell104.Weight = 0.18193548387096775;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell105.Weight = 0.054193548387096772;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell107.Weight = 0.16129032258064516;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell108.Weight = 0.054193548387096772;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell109.Weight = 0.0967741935483871;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell114,
            this.xrTableCell115,
            this.xrTableCell117,
            this.xrTableCell118});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow41.Weight = 0.036166365280289332;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell111.Weight = 0.052903225806451612;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100F);
            this.xrTableCell112.Text = "Cộng:";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell112.Weight = 0.21548387096774194;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell114.Weight = 0.18322580645161291;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell115.Weight = 0.2361290322580645;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell117.Weight = 0.16129032258064516;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell118.Weight = 0.15096774193548387;
            // 
            // lblThongBaoMienThue
            // 
            this.lblThongBaoMienThue.BackColor = System.Drawing.Color.Transparent;
            this.lblThongBaoMienThue.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.lblThongBaoMienThue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblThongBaoMienThue.Location = new System.Drawing.Point(350, 667);
            this.lblThongBaoMienThue.Name = "lblThongBaoMienThue";
            this.lblThongBaoMienThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThongBaoMienThue.Size = new System.Drawing.Size(283, 125);
            this.lblThongBaoMienThue.Text = "HÀNG KHÔNG CHỊU THUẾ GTGT THEO ĐIỂM 1.22 MỤC II PHẦN A THÔNG TƯ 32/2007/BTC NGÀY " +
                "09/04/2007";
            this.lblThongBaoMienThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Location = new System.Drawing.Point(692, 25);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.Size = new System.Drawing.Size(100, 17);
            this.xrLabel5.Text = "PLTK/2002-NK";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Location = new System.Drawing.Point(233, 92);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.Size = new System.Drawing.Size(100, 17);
            this.xrLabel6.Text = "Ngày đăng ký :";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Location = new System.Drawing.Point(233, 75);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.Size = new System.Drawing.Size(100, 17);
            this.xrLabel7.Text = "Kèm tờ khai số: ";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Location = new System.Drawing.Point(233, 58);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.Size = new System.Drawing.Size(100, 17);
            this.xrLabel8.Text = "Phụ lục số: ";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.Location = new System.Drawing.Point(217, 8);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.Size = new System.Drawing.Size(391, 25);
            this.xrLabel4.Text = "PHỤ LỤC TỜ KHAI HÀNG HÓA NHẬP KHẨU";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Location = new System.Drawing.Point(325, 92);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.Size = new System.Drawing.Size(83, 17);
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Location = new System.Drawing.Point(392, 75);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.Size = new System.Drawing.Size(33, 17);
            this.xrLabel3.Tag = "Mã loại hình";
            this.xrLabel3.Text = "/NK/";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.CanGrow = false;
            this.lblSoToKhai.Location = new System.Drawing.Point(325, 75);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.Size = new System.Drawing.Size(67, 17);
            this.lblSoToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoToKhai.Text = ".......................";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Location = new System.Drawing.Point(325, 58);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(100, 17);
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Location = new System.Drawing.Point(17, 574);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26});
            this.xrTable2.Size = new System.Drawing.Size(775, 548);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell84,
            this.xrTableCell87});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow22.Weight = 0.06569343065693431;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell80.Text = "SỐ";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell80.Weight = 0.052903225806451612;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell81.Text = "24. TIỀN THUẾ NHẬP KHẨU";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell81.Weight = 0.36129032258064514;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell84.Text = "25. TIỀN THUẾ GTGT (HOẶC TTĐB)";
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell84.Weight = 0.37032258064516127;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell87.Text = "26. THU KHÁC";
            this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell87.Weight = 0.21548387096774194;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow23.Weight = 0.063868613138686137;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell89.Text = "TT";
            this.xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell89.Weight = 0.052903225806451612;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell90.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell90.Multiline = true;
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell90.Text = "Trị giá tính thuế \r\n(VNĐ)";
            this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell90.Weight = 0.15096774193548387;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell91.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell91.Multiline = true;
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell91.Text = "Thuế\r\nsuất (%)";
            this.xrTableCell91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell91.Weight = 0.064516129032258063;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell92.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell92.Text = "Tiền thuế";
            this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell92.Weight = 0.14580645161290323;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell93.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell93.Text = "Trị giá tính thuế";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell93.Weight = 0.15483870967741936;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell94.Multiline = true;
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell94.Text = "Thuế\r\nsuất (%)";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell94.Weight = 0.064516129032258063;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell95.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell95.Text = "Tiền thuế";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell95.Weight = 0.15096774193548387;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell96.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell96.Multiline = true;
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell96.Text = "Tỷ lệ\r\n(%)";
            this.xrTableCell96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell96.Weight = 0.064516129032258063;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell97.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell97.Text = "Số tiền";
            this.xrTableCell97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell97.Weight = 0.15096774193548387;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.TriGiaTT1,
            this.ThueSuatXNK1,
            this.TienThueXNK1,
            this.xrTableCell26,
            this.xrTableCell71,
            this.xrTableCell62,
            this.xrTableCell44,
            this.xrTableCell53});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 0.071167883211678828;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell35.Text = "1";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell35.Weight = 0.052903225806451612;
            // 
            // TriGiaTT1
            // 
            this.TriGiaTT1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.TriGiaTT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT1.Name = "TriGiaTT1";
            this.TriGiaTT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT1.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK1
            // 
            this.ThueSuatXNK1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK1.Name = "ThueSuatXNK1";
            this.ThueSuatXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK1.Weight = 0.064516129032258063;
            // 
            // TienThueXNK1
            // 
            this.TienThueXNK1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK1.Name = "TienThueXNK1";
            this.TienThueXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK1.Weight = 0.14580645161290323;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell26.KeepTogether = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell26.Weight = 0.15483870967741936;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell71.KeepTogether = true;
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell71.Weight = 0.064516129032258063;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell62.KeepTogether = true;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell62.Weight = 0.15096774193548387;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell44.Weight = 0.064516129032258063;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell53.Weight = 0.15096774193548387;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.TriGiaTT2,
            this.ThueSuatXNK2,
            this.TienThueXNK2,
            this.xrTableCell27,
            this.xrTableCell72,
            this.xrTableCell63,
            this.xrTableCell45,
            this.xrTableCell54});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow11.Weight = 0.071167883211678828;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell36.Text = "2";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell36.Weight = 0.052903225806451612;
            // 
            // TriGiaTT2
            // 
            this.TriGiaTT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT2.Name = "TriGiaTT2";
            this.TriGiaTT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT2.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK2
            // 
            this.ThueSuatXNK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK2.Name = "ThueSuatXNK2";
            this.ThueSuatXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK2.Weight = 0.064516129032258063;
            // 
            // TienThueXNK2
            // 
            this.TienThueXNK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK2.Name = "TienThueXNK2";
            this.TienThueXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK2.Weight = 0.14580645161290323;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell27.KeepTogether = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell27.Weight = 0.15483870967741936;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell72.KeepTogether = true;
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell72.Weight = 0.064516129032258063;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell63.KeepTogether = true;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell63.Weight = 0.15096774193548387;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell45.Weight = 0.064516129032258063;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell54.Weight = 0.15096774193548387;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.TriGiaTT3,
            this.ThueSuatXNK3,
            this.TienThueXNK3,
            this.xrTableCell28,
            this.xrTableCell73,
            this.xrTableCell64,
            this.xrTableCell46,
            this.xrTableCell55});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow12.Weight = 0.071167883211678828;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell37.Text = "3";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell37.Weight = 0.052903225806451612;
            // 
            // TriGiaTT3
            // 
            this.TriGiaTT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT3.Name = "TriGiaTT3";
            this.TriGiaTT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT3.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK3
            // 
            this.ThueSuatXNK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK3.Name = "ThueSuatXNK3";
            this.ThueSuatXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK3.Weight = 0.064516129032258063;
            // 
            // TienThueXNK3
            // 
            this.TienThueXNK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK3.Name = "TienThueXNK3";
            this.TienThueXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK3.Weight = 0.14580645161290323;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell28.KeepTogether = true;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell28.Weight = 0.15483870967741936;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell73.KeepTogether = true;
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell73.Weight = 0.064516129032258063;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell64.KeepTogether = true;
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell64.Weight = 0.15096774193548387;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell46.Weight = 0.064516129032258063;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell55.Weight = 0.15096774193548387;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.TriGiaTT4,
            this.ThueSuatXNK4,
            this.TienThueXNK4,
            this.xrTableCell29,
            this.xrTableCell74,
            this.xrTableCell65,
            this.xrTableCell47,
            this.xrTableCell56});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow13.Weight = 0.071167883211678828;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell38.KeepTogether = true;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell38.Text = "4";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell38.Weight = 0.052903225806451612;
            // 
            // TriGiaTT4
            // 
            this.TriGiaTT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT4.KeepTogether = true;
            this.TriGiaTT4.Name = "TriGiaTT4";
            this.TriGiaTT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT4.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK4
            // 
            this.ThueSuatXNK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK4.KeepTogether = true;
            this.ThueSuatXNK4.Name = "ThueSuatXNK4";
            this.ThueSuatXNK4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK4.Weight = 0.064516129032258063;
            // 
            // TienThueXNK4
            // 
            this.TienThueXNK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK4.KeepTogether = true;
            this.TienThueXNK4.Name = "TienThueXNK4";
            this.TienThueXNK4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK4.Weight = 0.14580645161290323;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell29.Weight = 0.15483870967741936;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell74.Weight = 0.064516129032258063;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell65.Weight = 0.15096774193548387;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell47.Weight = 0.064516129032258063;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell56.Weight = 0.15096774193548387;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.TriGiaTT5,
            this.ThueSuatXNK5,
            this.TienThueXNK5,
            this.xrTableCell30,
            this.xrTableCell75,
            this.xrTableCell66,
            this.xrTableCell48,
            this.xrTableCell57});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow14.Weight = 0.071167883211678828;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell39.KeepTogether = true;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell39.Text = "5";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell39.Weight = 0.052903225806451612;
            // 
            // TriGiaTT5
            // 
            this.TriGiaTT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT5.KeepTogether = true;
            this.TriGiaTT5.Name = "TriGiaTT5";
            this.TriGiaTT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT5.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK5
            // 
            this.ThueSuatXNK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK5.KeepTogether = true;
            this.ThueSuatXNK5.Name = "ThueSuatXNK5";
            this.ThueSuatXNK5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK5.Weight = 0.064516129032258063;
            // 
            // TienThueXNK5
            // 
            this.TienThueXNK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK5.KeepTogether = true;
            this.TienThueXNK5.Name = "TienThueXNK5";
            this.TienThueXNK5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK5.Weight = 0.14580645161290323;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell30.Weight = 0.15483870967741936;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell75.Weight = 0.064516129032258063;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell66.KeepTogether = true;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell66.Weight = 0.15096774193548387;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell48.Weight = 0.064516129032258063;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell57.Weight = 0.15096774193548387;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.TriGiaTT6,
            this.ThueSuatXNK6,
            this.TienThueXNK6,
            this.xrTableCell31,
            this.xrTableCell76,
            this.xrTableCell67,
            this.xrTableCell49,
            this.xrTableCell58});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow15.Weight = 0.071167883211678828;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell40.Text = "6";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell40.Weight = 0.052903225806451612;
            // 
            // TriGiaTT6
            // 
            this.TriGiaTT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT6.Name = "TriGiaTT6";
            this.TriGiaTT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT6.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK6
            // 
            this.ThueSuatXNK6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK6.Name = "ThueSuatXNK6";
            this.ThueSuatXNK6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK6.Weight = 0.064516129032258063;
            // 
            // TienThueXNK6
            // 
            this.TienThueXNK6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK6.Name = "TienThueXNK6";
            this.TienThueXNK6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK6.Weight = 0.14580645161290323;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell31.Weight = 0.15483870967741936;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell76.Weight = 0.064516129032258063;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell67.Weight = 0.15096774193548387;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell49.Weight = 0.064516129032258063;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell58.Weight = 0.15096774193548387;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.TriGiaTT7,
            this.ThueSuatXNK7,
            this.TienThueXNK7,
            this.xrTableCell32,
            this.xrTableCell77,
            this.xrTableCell68,
            this.xrTableCell50,
            this.xrTableCell59});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow16.Weight = 0.071167883211678828;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell41.Text = "7";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell41.Weight = 0.052903225806451612;
            // 
            // TriGiaTT7
            // 
            this.TriGiaTT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT7.Name = "TriGiaTT7";
            this.TriGiaTT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT7.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK7
            // 
            this.ThueSuatXNK7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK7.Name = "ThueSuatXNK7";
            this.ThueSuatXNK7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK7.Weight = 0.064516129032258063;
            // 
            // TienThueXNK7
            // 
            this.TienThueXNK7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK7.Name = "TienThueXNK7";
            this.TienThueXNK7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK7.Weight = 0.14580645161290323;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell32.Weight = 0.15483870967741936;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell77.Weight = 0.064516129032258063;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell68.Weight = 0.15096774193548387;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell50.Weight = 0.064516129032258063;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell59.Weight = 0.15096774193548387;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.TriGiaTT8,
            this.ThueSuatXNK8,
            this.TienThueXNK8,
            this.xrTableCell33,
            this.xrTableCell78,
            this.xrTableCell69,
            this.xrTableCell51,
            this.xrTableCell60});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow17.Weight = 0.071167883211678828;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell42.Text = "8";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell42.Weight = 0.052903225806451612;
            // 
            // TriGiaTT8
            // 
            this.TriGiaTT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT8.Name = "TriGiaTT8";
            this.TriGiaTT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT8.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK8
            // 
            this.ThueSuatXNK8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK8.Name = "ThueSuatXNK8";
            this.ThueSuatXNK8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK8.Weight = 0.064516129032258063;
            // 
            // TienThueXNK8
            // 
            this.TienThueXNK8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK8.Name = "TienThueXNK8";
            this.TienThueXNK8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK8.Weight = 0.14580645161290323;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell33.Weight = 0.15483870967741936;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell78.Weight = 0.064516129032258063;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell69.Weight = 0.15096774193548387;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell51.Weight = 0.064516129032258063;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell60.Weight = 0.15096774193548387;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.TriGiaTT9,
            this.ThueSuatXNK9,
            this.TienThueXNK9,
            this.xrTableCell34,
            this.xrTableCell79,
            this.xrTableCell70,
            this.xrTableCell52,
            this.xrTableCell61});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow18.Weight = 0.071167883211678828;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell43.Text = "9";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell43.Weight = 0.052903225806451612;
            // 
            // TriGiaTT9
            // 
            this.TriGiaTT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaTT9.Name = "TriGiaTT9";
            this.TriGiaTT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT9.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK9
            // 
            this.ThueSuatXNK9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ThueSuatXNK9.Name = "ThueSuatXNK9";
            this.ThueSuatXNK9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK9.Weight = 0.064516129032258063;
            // 
            // TienThueXNK9
            // 
            this.TienThueXNK9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TienThueXNK9.Name = "TienThueXNK9";
            this.TienThueXNK9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK9.Weight = 0.14580645161290323;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell34.Weight = 0.15483870967741936;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell79.Weight = 0.064516129032258063;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell70.Weight = 0.15096774193548387;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell52.Weight = 0.064516129032258063;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell61.Weight = 0.15096774193548387;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.lblTongTienThueXNK,
            this.xrTableCell88,
            this.xrTableCell98,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow24.Weight = 0.06569343065693431;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell85.Text = "Cộng:\t";
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell85.Weight = 0.26838709677419353;
            // 
            // lblTongTienThueXNK
            // 
            this.lblTongTienThueXNK.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTienThueXNK.Name = "lblTongTienThueXNK";
            this.lblTongTienThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTienThueXNK.Weight = 0.14580645161290323;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell88.Weight = 0.15483870967741936;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell98.Weight = 0.064516129032258063;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell99.Weight = 0.15096774193548387;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell100.Weight = 0.064516129032258063;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell101.Weight = 0.15096774193548387;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell82});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow25.Weight = 0.072992700729927;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell82.Text = "29. Tôi xin cam đoan, chịu trách nhiệm trước pháp luật về những nội dung khai báo" +
                " trên tờ khai này.";
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell82.Weight = 1;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow26.Weight = 0.091240875912408759;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell106.Text = "Người khai ký, đóng dấu, ghi rõ họ tên, chức danh";
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell106.Weight = 1;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Location = new System.Drawing.Point(17, 117);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20,
            this.xrTableRow19,
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow21});
            this.xrTable1.Size = new System.Drawing.Size(775, 457);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow20.Weight = 0.080962800875273522;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell25.Text = "A- PHẦN DÀNH CHO NGƯỜI KHAI HẢI QUAN KÊ KHAI VÀ TÍNH THUẾ";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell25.Weight = 1;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow19.Weight = 0.10940919037199125;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.Text = "Số \r\nTT";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell10.Weight = 0.052903225806451612;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.Text = "17. TÊN HÀNG \r\nQUY CÁCH PHẨM CHẤT";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.36129032258064514;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Multiline = true;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell12.Text = "18. MÃ SỐ\r\nHÀNG HÓA";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.10709677419354839;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell13.Text = "19. XUẤT XỨ";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.07483870967741936;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell14.Text = "20. LƯỢNG";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 0.11096774193548387;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.Text = "21. ĐƠN VỊ TÍNH";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.076129032258064513;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell16.Text = "22. ĐƠN GIÁ NGUYÊN TỆ";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.0967741935483871;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell17.Text = "23. TRỊ GIÁ NGUYÊN TỆ";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell17.Weight = 0.12;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.TenHang1,
            this.MaHS1,
            this.XuatXu1,
            this.Luong1,
            this.DVT1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 0.080962800875273522;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.Text = "1";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.052903225806451612;
            // 
            // TenHang1
            // 
            this.TenHang1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang1.Tag = "Tên hàng 1";
            this.TenHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang1.Weight = 0.36129032258064514;
            this.TenHang1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS1
            // 
            this.MaHS1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS1.Weight = 0.10709677419354839;
            // 
            // XuatXu1
            // 
            this.XuatXu1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu1.Name = "XuatXu1";
            this.XuatXu1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu1.Weight = 0.07483870967741936;
            // 
            // Luong1
            // 
            this.Luong1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong1.Weight = 0.11096774193548387;
            // 
            // DVT1
            // 
            this.DVT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT1.Weight = 0.076129032258064513;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT1.Weight = 0.0967741935483871;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.Tag = "Trị giá NT 1";
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT1.Weight = 0.12;
            this.TriGiaNT1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.TenHang2,
            this.MaHS2,
            this.XuatXu2,
            this.Luong2,
            this.DVT2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 0.080962800875273522;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.Text = "2";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.052903225806451612;
            // 
            // TenHang2
            // 
            this.TenHang2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang2.Tag = "Tên hàng 2";
            this.TenHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang2.Weight = 0.36129032258064514;
            this.TenHang2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS2
            // 
            this.MaHS2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS2.Weight = 0.10709677419354839;
            // 
            // XuatXu2
            // 
            this.XuatXu2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu2.Name = "XuatXu2";
            this.XuatXu2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu2.Weight = 0.07483870967741936;
            // 
            // Luong2
            // 
            this.Luong2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong2.Weight = 0.11096774193548387;
            // 
            // DVT2
            // 
            this.DVT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT2.Weight = 0.076129032258064513;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT2.Weight = 0.0967741935483871;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.Tag = "Trị giá NT 2";
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT2.Weight = 0.12;
            this.TriGiaNT2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.TenHang3,
            this.MaHS3,
            this.XuatXu3,
            this.Luong3,
            this.DVT3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 0.080962800875273522;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.Text = "3";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.052903225806451612;
            // 
            // TenHang3
            // 
            this.TenHang3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang3.Tag = "Tên hàng 3";
            this.TenHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang3.Weight = 0.36129032258064514;
            this.TenHang3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS3
            // 
            this.MaHS3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS3.Weight = 0.10709677419354839;
            // 
            // XuatXu3
            // 
            this.XuatXu3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu3.Name = "XuatXu3";
            this.XuatXu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu3.Weight = 0.07483870967741936;
            // 
            // Luong3
            // 
            this.Luong3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong3.Weight = 0.11096774193548387;
            // 
            // DVT3
            // 
            this.DVT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT3.Weight = 0.076129032258064513;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT3.Weight = 0.0967741935483871;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.Tag = "Trị giá NT 3";
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT3.Weight = 0.12;
            this.TriGiaNT3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.TenHang4,
            this.MaHS4,
            this.XuatXu4,
            this.Luong4,
            this.DVT4,
            this.DonGiaNT4,
            this.TriGiaNT4});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 0.080962800875273522;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.Text = "4";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 0.052903225806451612;
            // 
            // TenHang4
            // 
            this.TenHang4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang4.Name = "TenHang4";
            this.TenHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang4.Tag = "Tên hàng 4";
            this.TenHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang4.Weight = 0.36129032258064514;
            this.TenHang4.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS4
            // 
            this.MaHS4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS4.Name = "MaHS4";
            this.MaHS4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS4.Weight = 0.10709677419354839;
            // 
            // XuatXu4
            // 
            this.XuatXu4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu4.Name = "XuatXu4";
            this.XuatXu4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu4.Weight = 0.07483870967741936;
            // 
            // Luong4
            // 
            this.Luong4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong4.Name = "Luong4";
            this.Luong4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong4.Weight = 0.11096774193548387;
            // 
            // DVT4
            // 
            this.DVT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT4.Name = "DVT4";
            this.DVT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT4.Weight = 0.076129032258064513;
            // 
            // DonGiaNT4
            // 
            this.DonGiaNT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT4.Name = "DonGiaNT4";
            this.DonGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT4.Weight = 0.0967741935483871;
            // 
            // TriGiaNT4
            // 
            this.TriGiaNT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT4.Name = "TriGiaNT4";
            this.TriGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT4.Tag = "Trị giá NT 4";
            this.TriGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT4.Weight = 0.12;
            this.TriGiaNT4.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.TenHang5,
            this.MaHS5,
            this.XuatXu5,
            this.Luong5,
            this.DVT5,
            this.DonGiaNT5,
            this.TriGiaNT5});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 0.080962800875273522;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.Text = "5";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.052903225806451612;
            // 
            // TenHang5
            // 
            this.TenHang5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang5.Name = "TenHang5";
            this.TenHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang5.Tag = "Tên hàng 5";
            this.TenHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang5.Weight = 0.36129032258064514;
            this.TenHang5.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS5
            // 
            this.MaHS5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS5.Name = "MaHS5";
            this.MaHS5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS5.Weight = 0.10709677419354839;
            // 
            // XuatXu5
            // 
            this.XuatXu5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu5.Name = "XuatXu5";
            this.XuatXu5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu5.Weight = 0.07483870967741936;
            // 
            // Luong5
            // 
            this.Luong5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong5.Name = "Luong5";
            this.Luong5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong5.Weight = 0.11096774193548387;
            // 
            // DVT5
            // 
            this.DVT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT5.Name = "DVT5";
            this.DVT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT5.Weight = 0.076129032258064513;
            // 
            // DonGiaNT5
            // 
            this.DonGiaNT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT5.Name = "DonGiaNT5";
            this.DonGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT5.Weight = 0.0967741935483871;
            // 
            // TriGiaNT5
            // 
            this.TriGiaNT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT5.Name = "TriGiaNT5";
            this.TriGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT5.Tag = "Trị giá NT 5";
            this.TriGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT5.Weight = 0.12;
            this.TriGiaNT5.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.TenHang6,
            this.MaHS6,
            this.XuatXu6,
            this.Luong6,
            this.DVT6,
            this.DonGiaNT6,
            this.TriGiaNT6});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 0.080962800875273522;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.Text = "6";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 0.052903225806451612;
            // 
            // TenHang6
            // 
            this.TenHang6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang6.Name = "TenHang6";
            this.TenHang6.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang6.Tag = "Tên hàng 6";
            this.TenHang6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang6.Weight = 0.36129032258064514;
            this.TenHang6.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS6
            // 
            this.MaHS6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS6.Name = "MaHS6";
            this.MaHS6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS6.Weight = 0.10709677419354839;
            // 
            // XuatXu6
            // 
            this.XuatXu6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu6.Name = "XuatXu6";
            this.XuatXu6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu6.Weight = 0.07483870967741936;
            // 
            // Luong6
            // 
            this.Luong6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong6.Name = "Luong6";
            this.Luong6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong6.Weight = 0.11096774193548387;
            // 
            // DVT6
            // 
            this.DVT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT6.Name = "DVT6";
            this.DVT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT6.Weight = 0.076129032258064513;
            // 
            // DonGiaNT6
            // 
            this.DonGiaNT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT6.Name = "DonGiaNT6";
            this.DonGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT6.Weight = 0.0967741935483871;
            // 
            // TriGiaNT6
            // 
            this.TriGiaNT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT6.Name = "TriGiaNT6";
            this.TriGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT6.Tag = "Trị giá NT 6";
            this.TriGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT6.Weight = 0.12;
            this.TriGiaNT6.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.TenHang7,
            this.MaHS7,
            this.XuatXu7,
            this.Luong7,
            this.DVT7,
            this.DonGiaNT7,
            this.TriGiaNT7});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.080962800875273522;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.Text = "7";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 0.052903225806451612;
            // 
            // TenHang7
            // 
            this.TenHang7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang7.Name = "TenHang7";
            this.TenHang7.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang7.Tag = "Tên hàng 7";
            this.TenHang7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang7.Weight = 0.36129032258064514;
            this.TenHang7.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS7
            // 
            this.MaHS7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS7.Name = "MaHS7";
            this.MaHS7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS7.Weight = 0.10709677419354839;
            // 
            // XuatXu7
            // 
            this.XuatXu7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu7.Name = "XuatXu7";
            this.XuatXu7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu7.Weight = 0.07483870967741936;
            // 
            // Luong7
            // 
            this.Luong7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong7.Name = "Luong7";
            this.Luong7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong7.Weight = 0.11096774193548387;
            // 
            // DVT7
            // 
            this.DVT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT7.Name = "DVT7";
            this.DVT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT7.Weight = 0.076129032258064513;
            // 
            // DonGiaNT7
            // 
            this.DonGiaNT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT7.Name = "DonGiaNT7";
            this.DonGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT7.Weight = 0.0967741935483871;
            // 
            // TriGiaNT7
            // 
            this.TriGiaNT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT7.Name = "TriGiaNT7";
            this.TriGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT7.Tag = "Trị giá NT 7";
            this.TriGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT7.Weight = 0.12;
            this.TriGiaNT7.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.TenHang8,
            this.MaHS8,
            this.XuatXu8,
            this.Luong8,
            this.DVT8,
            this.DonGiaNT8,
            this.TriGiaNT8});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 0.080962800875273522;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.Text = "8";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.052903225806451612;
            // 
            // TenHang8
            // 
            this.TenHang8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang8.Name = "TenHang8";
            this.TenHang8.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang8.Tag = "Tên hàng 8";
            this.TenHang8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang8.Weight = 0.36129032258064514;
            this.TenHang8.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS8
            // 
            this.MaHS8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS8.Name = "MaHS8";
            this.MaHS8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS8.Weight = 0.10709677419354839;
            // 
            // XuatXu8
            // 
            this.XuatXu8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu8.Name = "XuatXu8";
            this.XuatXu8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu8.Weight = 0.07483870967741936;
            // 
            // Luong8
            // 
            this.Luong8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong8.Name = "Luong8";
            this.Luong8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong8.Weight = 0.11096774193548387;
            // 
            // DVT8
            // 
            this.DVT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT8.Name = "DVT8";
            this.DVT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT8.Weight = 0.076129032258064513;
            // 
            // DonGiaNT8
            // 
            this.DonGiaNT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT8.Name = "DonGiaNT8";
            this.DonGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT8.Weight = 0.0967741935483871;
            // 
            // TriGiaNT8
            // 
            this.TriGiaNT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT8.Name = "TriGiaNT8";
            this.TriGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT8.Tag = "Trị giá NT 8";
            this.TriGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT8.Weight = 0.12;
            this.TriGiaNT8.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.TenHang9,
            this.MaHS9,
            this.XuatXu9,
            this.Luong9,
            this.DVT9,
            this.DonGiaNT9,
            this.TriGiaNT9});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 0.080962800875273522;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.Text = "9";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell9.Weight = 0.052903225806451612;
            // 
            // TenHang9
            // 
            this.TenHang9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang9.Name = "TenHang9";
            this.TenHang9.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang9.Tag = "Tên hàng 9";
            this.TenHang9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang9.Weight = 0.36129032258064514;
            this.TenHang9.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS9
            // 
            this.MaHS9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS9.Name = "MaHS9";
            this.MaHS9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS9.Weight = 0.10709677419354839;
            // 
            // XuatXu9
            // 
            this.XuatXu9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.XuatXu9.Name = "XuatXu9";
            this.XuatXu9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu9.Weight = 0.07483870967741936;
            // 
            // Luong9
            // 
            this.Luong9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong9.Name = "Luong9";
            this.Luong9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong9.Weight = 0.11096774193548387;
            // 
            // DVT9
            // 
            this.DVT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT9.Name = "DVT9";
            this.DVT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT9.Weight = 0.076129032258064513;
            // 
            // DonGiaNT9
            // 
            this.DonGiaNT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT9.Name = "DonGiaNT9";
            this.DonGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT9.Weight = 0.0967741935483871;
            // 
            // TriGiaNT9
            // 
            this.TriGiaNT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT9.Name = "TriGiaNT9";
            this.TriGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT9.Tag = "Trị giá NT 9";
            this.TriGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT9.Weight = 0.12;
            this.TriGiaNT9.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.lblTongTriGiaNT});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow21.Weight = 0.080962800875273522;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell18.Weight = 0.052903225806451612;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell19.Text = "Cộng:";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 0.36129032258064514;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell20.Weight = 0.10709677419354839;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell21.Weight = 0.07483870967741936;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 0.11096774193548387;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell23.Weight = 0.076129032258064513;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell24.Weight = 0.0967741935483871;
            // 
            // lblTongTriGiaNT
            // 
            this.lblTongTriGiaNT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTriGiaNT.Name = "lblTongTriGiaNT";
            this.lblTongTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaNT.Tag = "Trị giá NT";
            this.lblTongTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTriGiaNT.Weight = 0.12;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.BorderWidth = 2;
            this.xrLabel9.Location = new System.Drawing.Point(14, 114);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.Size = new System.Drawing.Size(781, 1011);
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.BorderWidth = 2;
            this.xrLabel10.Location = new System.Drawing.Point(14, 1167);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.Size = new System.Drawing.Size(781, 1114);
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PhuLucToKhaiNhapA4
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(8, 8, 6, 19);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell TenHang4;
        private DevExpress.XtraReports.UI.XRTableCell MaHS4;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu4;
        private DevExpress.XtraReports.UI.XRTableCell Luong4;
        private DevExpress.XtraReports.UI.XRTableCell DVT4;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell TenHang5;
        private DevExpress.XtraReports.UI.XRTableCell MaHS5;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu5;
        private DevExpress.XtraReports.UI.XRTableCell Luong5;
        private DevExpress.XtraReports.UI.XRTableCell DVT5;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell TenHang6;
        private DevExpress.XtraReports.UI.XRTableCell MaHS6;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu6;
        private DevExpress.XtraReports.UI.XRTableCell Luong6;
        private DevExpress.XtraReports.UI.XRTableCell DVT6;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell TenHang7;
        private DevExpress.XtraReports.UI.XRTableCell MaHS7;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu7;
        private DevExpress.XtraReports.UI.XRTableCell Luong7;
        private DevExpress.XtraReports.UI.XRTableCell DVT7;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell TenHang8;
        private DevExpress.XtraReports.UI.XRTableCell MaHS8;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu8;
        private DevExpress.XtraReports.UI.XRTableCell Luong8;
        private DevExpress.XtraReports.UI.XRTableCell DVT8;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell TenHang9;
        private DevExpress.XtraReports.UI.XRTableCell MaHS9;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu9;
        private DevExpress.XtraReports.UI.XRTableCell Luong9;
        private DevExpress.XtraReports.UI.XRTableCell DVT9;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT9;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT4;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK4;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT5;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK5;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT6;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK6;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT7;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK7;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT8;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK8;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT9;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK9;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblThongBaoMienThue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaNT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThueXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLine xrLine16;
        private DevExpress.XtraReports.UI.XRLine xrLine15;
        private DevExpress.XtraReports.UI.XRLine xrLine14;
        private DevExpress.XtraReports.UI.XRLine xrLine13;
        private DevExpress.XtraReports.UI.XRLine xrLine12;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRLine xrLine21;
        private DevExpress.XtraReports.UI.XRLine xrLine20;
        private DevExpress.XtraReports.UI.XRLine xrLine19;
        private DevExpress.XtraReports.UI.XRLine xrLine18;
        private DevExpress.XtraReports.UI.XRLine xrLine17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine22;
        private DevExpress.XtraReports.UI.XRLine xrLine36;
        private DevExpress.XtraReports.UI.XRLine xrLine35;
        private DevExpress.XtraReports.UI.XRLine xrLine34;
        private DevExpress.XtraReports.UI.XRLine xrLine33;
        private DevExpress.XtraReports.UI.XRLine xrLine32;
        private DevExpress.XtraReports.UI.XRLine xrLine31;
        private DevExpress.XtraReports.UI.XRLine xrLine30;
        private DevExpress.XtraReports.UI.XRLine xrLine29;
        private DevExpress.XtraReports.UI.XRLine xrLine28;
        private DevExpress.XtraReports.UI.XRLine xrLine27;
        private DevExpress.XtraReports.UI.XRLine xrLine26;
        private DevExpress.XtraReports.UI.XRLine xrLine25;
        private DevExpress.XtraReports.UI.XRLine xrLine24;
        private DevExpress.XtraReports.UI.XRLine xrLine23;
        private DevExpress.XtraReports.UI.XRLine xrLine37;
        private DevExpress.XtraReports.UI.XRLabel lblbanLuuHQ;
        private DevExpress.XtraReports.UI.XRLabel lblMaHQ;
    }
}
