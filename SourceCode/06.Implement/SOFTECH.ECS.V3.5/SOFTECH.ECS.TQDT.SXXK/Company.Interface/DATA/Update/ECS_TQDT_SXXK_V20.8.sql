      
        
----------------------------------------------------------------------------------------------------        
        
alter PROCEDURE [dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]        
  
 @MaDoanhNghiep NVarchar(14),      
 @MaHaiQuan varchar (6),        
 @LanThanhLy int ,       
@BangKeHoSoThanhLy_ID  int
as
    
SET NOCOUNT ON        
SET TRANSACTION ISOLATION LEVEL READ COMMITTED        
      
Create table #BangKeNPLTon       
      ( MaNPL nvarchar(80),      
    TenNPL nvarchar(255),      
    LuongTonDau numeric(24,8),      
    LuongNhap numeric(24,8),      
   LuongNPLSuDung numeric(24,8),      
   LuongNPLTaiXuat numeric(24,8),      
   LuongNopThue numeric(24,8),      
   LuongXuatTheoHD numeric(24,8),      
   LuongTonCuoi numeric(24,8),      
   DVT_ID varchar(10)      
   )      
      
insert into #BangKeNPLTon (MaNPL ,      
    TenNPL ,      
    LuongTonDau ,      
    LuongNhap ,      
   LuongNPLSuDung ,      
   LuongNPLTaiXuat ,      
   LuongNopThue,      
   LuongXuatTheoHD ,      
   LuongTonCuoi,      
   DVT_ID )      
      
Select         
 A.MaNPL,        
 MAX(B.Ten) as TenNPL,         
 SUM(A.LuongTonDau)  as LuongTonDau,        
 Sum(LuongNhap) as LuongNhap,        
 Sum(LuongNPLSuDung) as LuongNPLSuDung,        
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,        
 Sum(LuongNopThue) as LuongNopThue,        
 Sum(LuongXuatTheoHD) as LuongXuatTheoHD,        
 Sum(LuongTonCuoi) as LuongTonCuoi,        
 B.DVT_ID --MAX(ID_DVT_NPL) as ID_DVT_NPL,        
      
 From         
(      
	SELECT        
			 SoToKhaiNhap,        
			 NgayDangKyNhap,        
			 MaNPL,        
			 TenNPL, 
			 --Tinh luong ton dau gom (to khai tk do dang và to khai da dưa vao bang ke o những lan thanh ly truoc)        
			 CASE when 
				(select COUNT(*) from t_KDT_SXXK_BKToKhaiNhap where BangKeHoSoThanhLy_ID <@BangKeHoSoThanhLy_ID and SoToKhai=SoToKhaiNhap and MaLoaiHinh= MaLoaiHinhNhap  and YEAR(NgayDangKy)= YEAR(NgayDangKyNhap))>0 
			 then LuongTonDau 
			 else 0 end as LuongTonDau,        
			 -- tinh luong nhap(neu to khai da có trong bang ke truoc do thi tinh luong nhap = 0)
			 CASE when  
				(select COUNT(*) from t_KDT_SXXK_BKToKhaiNhap where BangKeHoSoThanhLy_ID <@BangKeHoSoThanhLy_ID and SoToKhai=SoToKhaiNhap and MaLoaiHinh= MaLoaiHinhNhap and YEAR(NgayDangKy)= YEAR(NgayDangKyNhap) )>0 
				then 0 
				else (CASE when 
							LuongNhap = LuongTonDau  
						then (Case when  MaLoaiHinhNhap not like '%V%' then 0 else LuongNhap end) 
						else 0 
						end) 
				end as LuongNhap,         
			 --Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung,         
			 Sum( Case when LuongTonCuoi<0 then (case when LuongNPLSuDung + LuongTonCuoi >LuongTonDau then LuongTonDau else  LuongNPLSuDung + LuongTonCuoi end) else (Case when  
			   ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau   
			   then LuongTonDau- LuongTonCuoi  
			   else LuongNPLSuDung end) end  
			 )  as LuongNPLSuDung,   
			 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,         
			 0 as LuongNopThue,        
			 0 as LuongXuatTheoHD,        
			 Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,        
			 (select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL        
			      
			          
			FROM        
			 [dbo].[t_KDT_SXXK_BCXuatNhapTon]        
			WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy   
			GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTondau,LuongNhap        
			) as A inner join t_SXXK_NguyenPhuLieu B        
on A.MaNPL = B.Ma  AND B.MaDoanhNghiep = @MaDoanhNghiep And B.MaHaiQuan= @MaHaiQuan  
--inner join    t_KDT_SXXK_BKToKhaiNhap as D on a.SoToKhaiNhap= D.SoToKhai and  D.BangKeHoSoThanhLy_ID =302

Group by A.MaNPL,B.DVT_ID        
order by A.MaNPL        
      
 if(exists( select * from t_KDT_SXXK_BangKeHoSoThanhLy where MaterID = (select top 1 ID from  t_KDT_SXXK_HoSoThanhLyDangKy where LanThanhLy = @LanThanhLy AND MaBangKe = 'DTLNPLCHUATL')))      
 begin       
  update #BangKeNPLTon set LuongTonCuoi = LuongTonCuoi + b.Luong --, LuongNPLSuDung = LuongNPLSuDung- b.Luong      
  from #BangKeNPLTon a inner join (select sum(Luong)as Luong,MaNPL,DVT_ID  from t_KDT_SXXK_BKNPLChuaThanhLy where BangKeHoSoThanhLy_ID = (select top 1 ID from t_KDT_SXXK_BangKeHoSoThanhLy where MaterID = (select top 1 ID from  t_KDT_SXXK_HoSoThanhLyDangKy
  
    
 where LanThanhLy = @LanThanhLY AND MaBangKe = 'DTLNPLCHUATL')) group by MaNPL,DVT_ID) b      
  on a.MaNPL = b.MaNPL and a.DVT_ID = b.DVT_ID      
end      
      
select * from #BangKeNPLTon      
drop table #BangKeNPLTon      
  -----------------------------------------------------------------------------------------------
  
  GO

Create  PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiNhap_CX]        
  
 @MaDoanhNghiep NVarchar(14),      
 @NamDangky int,        
 @LanThanhLy int ,       
 @BangKeHoSoThanhLy_ID  int
as
    
SET NOCOUNT ON        
SET TRANSACTION ISOLATION LEVEL READ COMMITTED       


SELECT 
	row_number() OVER (ORDER BY b.NgayDangKyNhap,b.SoToKhaiNhap) as STT,
	Cast((case when b.MaLoaiHinhNhap like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = b.SoToKhaiNhap) else b.SoToKhaiNhap end ) as varchar(100)) +'/' + c.Ten_VT as SoToKhai, 
	b.SoToKhaiNhap, 
	a.NgayDangKy as NgayDangKy, 
	a.NgayHoanThanh as NgayHoanThanh, 
	a.SoHopDong, a.NgayHopDong ,  
	a.MaLoaiHinh, b.ThueNK , 
	a.MaHaiQuan 
FROM (SELECT 
			SoToKhaiNhap,
			NgayDangKyNhap,
			case when Year(Max(NgayThucNhap)) <= 1900 
					then (Select top 1 temp.NgayHoanThanh 
							from t_SXXK_ToKhaiMauDich temp 
							where  temp.SoToKhai = c.SoToKhaiNhap 
									and temp.NgayDangKy = c.NgayDangKyNhap 
									and temp.MaLoaiHinh = c.MaLoaiHinhNhap)
					 else MAX(NgayThucNhap) end As NgayThucNhap,
			MaLoaiHinhNhap,
			sum(TienThueHoan) as ThueNK  
		FROM (SELECT 
				SoToKhaiNhap, 
				NgayDangKyNhap, 
				Max(NgayThucNhap) As NgayThucNhap ,
				MaLoaiHinhNhap, 
				CASE WHEN sum(LuongNPLSuDung) = LuongNhap 
						THEN ThueNKNop 
						ELSE  Round(sum(LuongNPLSuDung)*ThueXNK/Luong,0) 
						END as TienThueHoan 
			FROM t_KDT_SXXK_BCThueXNK 
			WHERE LanThanhLy = @LanThanhLy 
					AND MaDoanhNghiep = @MaDoanhNghiep
					--and 
			GROUP BY  SoToKhaiNhap,NgayDangKyNhap,MaNPL,MaLoaiHinhNhap,Luong,LuongNhap,ThueXNK,ThueNKNop ) c 
GROUP BY SoToKhaiNhap, NgayDangKyNhap, MaLoaiHinhNhap) b  
INNER JOIN t_SXXK_ToKhaiMauDich a ON b.SoToKhaiNhap = a.SoToKhai  
									AND year(b.NgayDangKyNhap) = year(a.NgayDangKy)  
									AND b.MaLoaiHinhNhap = a.MaLoaiHinh  
INNER JOIN t_HaiQuan_LoaiHinhMauDich c ON a.MaLoaiHinh = c.ID 
INNER JOIN ( Select * from t_KDT_SXXK_BKToKhaiNhap 
						where BangKeHoSoThanhLy_ID=@BangKeHoSoThanhLy_ID 
							and SoToKhai not in(Select sotokhai  from t_KDT_SXXK_BKToKhaiNhap 
						where BangKeHoSoThanhLy_ID < @BangKeHoSoThanhLy_ID and NamDangKy =@NamDangky ) 
) D on D.SoToKhai=a.SoToKhai 
		and D.MaHaiQuan= a.MaHaiQuan and D.MaLoaiHinh= a.MaLoaiHinh
 
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '20.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('20.8',GETDATE(), N' Cap nhat store [p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy], P-KDT_SXXK_BKToKhaiNhap_CX  cho che xuat')
END