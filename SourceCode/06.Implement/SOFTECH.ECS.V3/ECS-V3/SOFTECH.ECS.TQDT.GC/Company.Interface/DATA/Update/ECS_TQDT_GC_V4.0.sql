/*
Run this script on:

        192.168.72.100.ECS_TQDT_GC_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_GC_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 08/16/2012 1:41:12 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhauGetCuaKhau]'
GO
DROP PROCEDURE p_HaiQuan_NhomCuaKhauGetCuaKhau
GO

CREATE PROCEDURE p_HaiQuan_NhomCuaKhauGetCuaKhau          
 -- Add the parameters for the stored procedure here          
 @Cuc_ID nvarchar(10)          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
  SELECT     t_HaiQuan_CuaKhau.ID, t_HaiQuan_CuaKhau.Ten, t_HaiQuan_NhomCuaKhau.Cuc_ID    
  FROM         t_HaiQuan_NhomCuaKhau INNER JOIN          
         t_HaiQuan_CuaKhau ON t_HaiQuan_NhomCuaKhau.CuaKhau_ID = t_HaiQuan_CuaKhau.ID          
 WHERE     (t_HaiQuan_NhomCuaKhau.Cuc_ID like '%' +@Cuc_ID +'%')         
          
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='4.0', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('4.0', getdate(), null)
	end 
