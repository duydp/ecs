﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;

namespace Company.BLL.DataTransferObjectMapper
{
    /// <summary>
    /// Ánh xạ từ BOs(Bussines object)  sang DTOs (Data Transfer Objects)    
    /// </summary>
    public class Mapper
    {
        /// <summary>
        /// Chuyển dữ liệu từ SanPhamDangKy BO sang SXXK_SP DTO/ Huy Du Lieu Da Duoc Duyet
        /// </summary>
        /// <param name="sanphamdangky">SanPhamDangKy</param>
        /// <returns>SXXK_SP</returns>
        public static SXXK_SanPham ToDataTransferObject_SXXK_SP(SanPhamDangKy sanphamdangky,bool Huy)
        {
            SXXK_SanPham sanpham = new SXXK_SanPham
            {
                //Issuer = DeclarationIssuer.SXXK_DANHMUC_SP,

                Reference = sanphamdangky.GUIDSTR,
                Issue = DateTime.Now.ToString(),
                IssueLocation = "",
                DeclarationOffice = sanphamdangky.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = sanphamdangky.SoTiepNhan.ToString(),
                Acceptance = sanphamdangky.NgayTiepNhan.ToString("dd-MM-yyyy HH:mm:ss"),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "??", Identity = sanphamdangky.MaDoanhNghiep },
                Product = new List<Product>()
            };
            if (Huy)
                sanpham.Function = DeclarationFunction.HUY;
            else
            {
                sanpham.Function = DeclarationFunction.KHAI_BAO;
                sanpham.Agents.Add(new Agent
                {
                    Name = "??",
                    Identity = sanphamdangky.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                });
                if (sanphamdangky.SPCollection != null)
                    foreach (SanPham item in sanphamdangky.SPCollection)
                    {
                        sanpham.Product.Add(new Product
                        {
                            Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS },
                            GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID }
                        });
                    }
            }
            return sanpham;
        }
        public static SXXK_SanPham ToDaTaTransferObject_SXXK_SP_SUA(SanPhamDangKySUA sanphamdangkySUA, List<SanPhamSUA> listSP )
        {
            SXXK_SanPham sanpham = new SXXK_SanPham
            {
                //Issuer = DeclarationIssuer.SXXK_DANHMUC_SP,
                Function = DeclarationFunction.KHAI_BAO,
                Reference = sanphamdangkySUA.GUIDSTR,
                Issue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                IssueLocation = "",
                DeclarationOffice = sanphamdangkySUA.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = sanphamdangkySUA.SoTiepNhan.ToString(),
                Acceptance = sanphamdangkySUA.NgayTiepNhan.ToString("dd-MM-yyyy HH:mm:ss"),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "??", Identity = sanphamdangkySUA.MaDoanhNghiep },
                Product = new List<Product>()
            };
            sanpham.Agents.Add(new Agent
            {
                Name = "??",
                Identity = sanphamdangkySUA.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (listSP != null)
                foreach (SanPhamSUA item in listSP)
                {
                    sanpham.Product.Add(new Product
                    {
                        Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID }
                    });
                }
            return sanpham;
        }
        /// <summary>
        /// Chuyển dữ liệu từ NguyenPhuLieuDangKy BO sang SXXK_NPL DTO
        /// </summary>
        /// <param name="nguyenphulieudangky">NguyenPhuLieuDangKy</param>
        /// <returns>SXXK_NPL</returns>
        public static SXXK_NguyenPhuLieu ToDataTransferObject_SXXK_NPL(NguyenPhuLieuDangKy npldangky,bool Huy)
        {
            SXXK_NguyenPhuLieu nguyenphulieu = new SXXK_NguyenPhuLieu
            {
                Issue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                //Issuer = DeclarationIssuer.SXXK_DANHMUC_NPL,
                Reference = npldangky.GUIDSTR,
                IssueLocation = "",
                DeclarationOffice = npldangky.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = npldangky.SoTiepNhan.ToString(),
                Acceptance = npldangky.NgayTiepNhan.ToString("yyyy-MM-dd HH:mm:ss"),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "??", Identity = npldangky.MaDoanhNghiep },
                Product = new List<Product>(),
            };
            if (Huy) nguyenphulieu.Function = DeclarationFunction.HUY;
            else
                nguyenphulieu.Function = DeclarationFunction.KHAI_BAO;
            nguyenphulieu.Agents.Add(new Agent
            {
                Name = "??",
                Identity = npldangky.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (npldangky.NPLCollection != null)
                foreach (NguyenPhuLieu item in npldangky.NPLCollection)
                {
                    nguyenphulieu.Product.Add(new Product
                    {
                        Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID }
                    });
                }
            return nguyenphulieu;
        }
        public static SXXK_NguyenPhuLieu ToDaTaTransferObject_SXXK_NPL_SUA(NguyenPhuLieuDangKySUA npldangkysua,List<NguyenPhuLieuSUA> listNPLSua)
        {
            SXXK_NguyenPhuLieu nguyenphulieu = new SXXK_NguyenPhuLieu
            {
                Issue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                //Issuer = DeclarationIssuer.SXXK_DANHMUC_NPL,
                Function = DeclarationFunction.SUA,
                Reference = npldangkysua.GUIDSTR,
                IssueLocation = "",
                DeclarationOffice = npldangkysua.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = npldangkysua.SoTiepNhan.ToString(),
                Acceptance = npldangkysua.NgayTiepNhan.ToString("yyyy-MM-dd HH:mm:ss"),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "", Identity = npldangkysua.MaDoanhNghiep },
                Product = new List<Product>(),
            };
            nguyenphulieu.Agents.Add(new Agent
            {
                Name = "",
                Identity = npldangkysua.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (listNPLSua != null)
                foreach (NguyenPhuLieuSUA item in listNPLSua)
                {
                    nguyenphulieu.Product.Add(new Product
                    {
                        Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID }
                    });
                }
            return nguyenphulieu;
        }
        /// <summary>
        /// Chuyển dữ liệu từ DinhMucDangKy BO sang SXXK_DinhMucSP DTO
        /// </summary>
        /// <param name="dinhmucdangky">DinhMucDangKy</param>
        /// <returns>SXXK_DinhMucSP</returns>
        public static SXXK_DinhMucSP ToDataTransferObject_SXXK_DinhMuc(DinhMucDangKy dmdk,bool huy)
        {
            SXXK_DinhMucSP dm = new SXXK_DinhMucSP
            {
                //Issuer = DeclarationIssuer.SXXK_DINHMUC_SP,
                Reference = dmdk.GUIDSTR,
                Issue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                IssueLocation = "",
                DeclarationOffice = dmdk.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan, 0),
                Acceptance = dmdk.NgayTiepNhan.ToString("yyyy-MM-dd HH:mm:ss"),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "??", Identity = dmdk.MaDoanhNghiep },
                ProductionNorm = new List<ProductionNorm>(),
            };
            if (huy) dm.Function = DeclarationFunction.HUY;
            else dm.Function = DeclarationFunction.KHAI_BAO;
            dm.Agents.Add(new Agent
            {
                Name = "??",
                Identity = dmdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,

            });
            for (int i = 0; i < dmdk.DMCollection.Count; i++)
            {
                string masanpham = dmdk.DMCollection[i].MaSanPham;
                ProductionNorm ProNor;
                if (masanpham != "")
                {
                    Company.BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                    spSXXK = Company.BLL.SXXK.SanPham.getSanPham(dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, masanpham);
                    ProNor = new ProductionNorm
                   {
                       Product = new Product
                       {
                           Commodity = new Commodity { Identification = dmdk.DMCollection[i].MaSanPham, Description = spSXXK.Ten, TariffClassification = spSXXK.MaHS },
                           GoodsMeasure = new GoodsMeasure { MeasureUnit = spSXXK.DVT_ID },
                       },
                       MaterialsNorms = new List<MaterialsNorm>()
                   };
                    for (int j = i; j < dmdk.DMCollection.Count; j++) // Duyệt tìm những nguyên phụ liệu cho sản phẩm [i]    
                        if (masanpham == dmdk.DMCollection[j].MaSanPham)
                        {
                            Company.BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                            nplSXXK = Company.BLL.SXXK.NguyenPhuLieu.getNguyenPhuLieu(dmdk.MaHaiQuan,dmdk.MaDoanhNghiep,dmdk.DMCollection[j].MaNguyenPhuLieu);
                            ProNor.MaterialsNorms.Add(new MaterialsNorm
                            {
                                Material = new Product
                                {
                                    Commodity = new Commodity { Identification = dmdk.DMCollection[j].MaNguyenPhuLieu, Description = dmdk.DMCollection[j].TenNPL, TariffClassification = nplSXXK.MaHS },
                                    GoodsMeasure = new GoodsMeasure { MeasureUnit = dmdk.DMCollection[j].DVT_ID },
                                },
                                Norm = Helpers.FormatNumeric(dmdk.DMCollection[j].DinhMucSuDung, 6),
                                Loss = Helpers.FormatNumeric(dmdk.DMCollection[j].TyLeHaoHut, 4)

                            });
                            dmdk.DMCollection[j].MaSanPham = ""; // Loại những sản phẩm trùng
                        }
                    dm.ProductionNorm.Add(ProNor);
                }
            };
            return dm;
        }
        public static SXXK_DinhMucSP ToDataTransferObject_SXXK_DinhMuc_SUA(DinhMucDangKySUA dmdk, IList<DinhMucSUA> listDMSua)
        {
            SXXK_DinhMucSP dm = new SXXK_DinhMucSP
            {
                //Issuer = DeclarationIssuer.SXXK_DINHMUC_SP,
                Function = DeclarationFunction.SUA,
                Reference = dmdk.GUIDSTR,
                Issue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                IssueLocation = "",
                DeclarationOffice = dmdk.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan, 0),
                Acceptance = dmdk.NgayTiepNhan.ToString("yyyy-MM-dd HH:mm:ss"),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "??", Identity = dmdk.MaDoanhNghiep },
                ProductionNorm = new List<ProductionNorm>(),
            };
            dm.Agents.Add(new Agent
            {
                Name = "??",
                Identity = dmdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,

            });
            for (int i = 0; i < listDMSua.Count; i++)
            {
                string masanpham = listDMSua[i].MaSanPham;
                ProductionNorm ProNor;
                if (masanpham != "")
                {
                    Company.BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                    spSXXK.Ma = masanpham;
                    spSXXK.Load();
                    ProNor = new ProductionNorm
                    {
                        Product = new Product
                        {
                            Commodity = new Commodity { Identification = listDMSua[i].MaSanPham, Description = spSXXK.Ten, TariffClassification = spSXXK.MaHS },
                            GoodsMeasure = new GoodsMeasure { MeasureUnit = spSXXK.DVT_ID },
                        },
                        MaterialsNorms = new List<MaterialsNorm>()
                    };
                    for (int j = i; j < listDMSua.Count; j++) // Duyệt tìm những nguyên phụ liệu cho sản phẩm [i]    
                        if (masanpham == listDMSua[j].MaSanPham)
                        {
                            Company.BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                            nplSXXK.Ma = listDMSua[j].MaNguyenPhuLieu;
                            nplSXXK.Load();
                            ProNor.MaterialsNorms.Add(new MaterialsNorm
                            {
                                Material = new Product
                                {
                                    Commodity = new Commodity { Identification = listDMSua[j].MaNguyenPhuLieu, Description = nplSXXK.Ten, TariffClassification = nplSXXK.MaHS },
                                    GoodsMeasure = new GoodsMeasure { MeasureUnit = listDMSua[j].DVT_ID },
                                },
                                Norm = Helpers.FormatNumeric(listDMSua[j].DinhMucSuDung, 6),
                                Loss = Helpers.FormatNumeric(listDMSua[j].TyLeHaoHut, 4)

                            });
                            listDMSua[j].MaSanPham = ""; // Loại những sản phẩm trùng
                        }
                    dm.ProductionNorm.Add(ProNor);
                }
            };
            return dm;
        }
        /// <summary>
        /// Chuyển dữ liệu từ ToKhaiMauDich BO sang SXXK_ToKhaiXuat DTO
        /// </summary>
        /// <param name="tokhaimaudich">ToKhaiMauDich</param>
        /// <returns>SXXK_ToKhaiXuat</returns>
        //public static SXXK_ToKhaiXuat ToDataTransferObject_SXXK_ToKhaiXuat(ToKhaiMauDich tokhaimaudich)
        //{
        //    {
        //        string formatDateDetail = "yyyy-MM-dd HH:mm:ss";
        //        string formatDate = "yyyy-MM-dd";
        //        SXXK_ToKhaiXuat tkx = new SXXK_ToKhaiXuat()
        //        {
        //            Issuer = DeclarationIssuer.SXXK_TOKHAI_XUAT ,
        //            Reference = tokhaimaudich.GUIDSTR,
        //            Issue = tokhaimaudich.NgayDangKy.ToString(formatDateDetail),
        //            Function = DeclarationFunction.KHAI_BAO,
        //            IssueLocation = "",
        //            Status = tokhaimaudich.TrangThaiXuLy.ToString(),
        //            // Số tiếp nhận
        //            CustomsReference = tokhaimaudich.SoTiepNhan.ToString(),
        //            //Ngày dang ký chứng thư
        //            Acceptance = tokhaimaudich.NgayTiepNhan.ToString(formatDateDetail),
        //            // Ðon vị hải quan khai báo
        //            DeclarationOffice = tokhaimaudich.MaHaiQuan,
        //            // Số hàng
        //            GoodsItem = tokhaimaudich.SoHang.ToString(),
        //            LoadingList = tokhaimaudich.SoLuongPLTK.ToString(),

        //            // Khối luợng và khối luợng tịnh
        //            TotalGrossMass = Helpers.FormatNumeric(tokhaimaudich.TrongLuong,3),
        //            TotalNetGrossMass = Helpers.FormatNumeric(tokhaimaudich.TrongLuongNet,3),
        //            // Mã Loại Hình
        //            NatureOfTransaction = tokhaimaudich.MaLoaiHinh,
        //            // Phuong thức thanh toán
        //            PaymentMethod = tokhaimaudich.PTTT_ID,
        //            Agents = new List<Agent>(),
        //            //Nguyên tệ
        //            CurrencyExchange = new CurrencyExchange { CurrencyType = tokhaimaudich.NguyenTe_ID, Rate = tokhaimaudich.TyGiaTinhThue.ToString() },
        //            DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tokhaimaudich.SoKien,2)},
        //            // Hợp Ðồng
        //            AdditionalDocument = new List<AdditionalDocument>(),
        //            // Hóa Ðon thuong mại
        //            Invoice = new Invoice { Issue = tokhaimaudich.NgayHoaDonThuongMai.ToString(formatDate), Reference = tokhaimaudich.SoHoaDonThuongMai, Type = "??" },
        //            // Doanh Nghiệp Xuất khẩu
        //            Exporter = new NameBase { Name = tokhaimaudich.TenDoanhNghiep, Identity = tokhaimaudich.MaDoanhNghiep.ToString() },
        //            // Nguười gởi ???
        //            RepresentativePerson = new RepresentativePerson { },
        //            // ???
        //            AdditionalInformation = new AdditionalInformation { Content = new Content { Text = tokhaimaudich.DeXuatKhac }, Statement ="??" },
        //            // GoodsShipmet
        //            GoodsShipment = new GoodsShipment
        //            {
        //                ImportationCountry = tokhaimaudich.NuocNK_ID,
        //                Consignee = new NameBase { Identity = "" , Name = "" },
        //                Consignor = new NameBase { Identity = "", Name = "" },
        //                NotifyParty = new NameBase { Identity = "", Name = "" },
        //                DeliveryDestination = new DeliveryDestination { Line = "" },
        //                ExitCustomsOffice = new LocationNameBase { Code = tokhaimaudich.CuaKhau_ID},
        //                Importer = new NameBase { Name = tokhaimaudich.TenDonViDoiTac, Identity = "" },
        //                TradeTerm = new TradeTerm { Condition = tokhaimaudich.DKGH_ID },
        //                // Vận đơn
        //                Consignment = new Consignment(),
        //                CustomsGoodsItems = new List<CustomsGoodsItem>()
                        
        //            },
        //            License = new List<License>(),
        //            ContractDocument = new List<ContractDocument>(),
        //            CommercialInvoices = new List<CommercialInvoice>(),
        //            CertificateOfOrigin = new List<CertificateOfOrigin>(),
        //            CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
        //            BillOfLading = new List<BillOfLading>(),
        //            AttachDocumentItem = new List<AttachDocumentItem>(),
        //            AdditionalDocumentEx = new List<AdditionalDocument>(),
        //        };
                
        //        // Người Khai Hải Quan
        //        tkx.Agents.Add(new Agent
        //        {
        //            Name = tokhaimaudich.TenDaiLyTTHQ,
        //            Identity = tokhaimaudich.MaDaiLyTTHQ,
        //            Status = AgentsStatus.NGUOIKHAI_HAIQUAN
        //        });
        //        // ủy Thác
        //        tkx.Agents.Add(new Agent
        //        {
        //            Name = tokhaimaudich.TenDonViUT,
        //            Identity = tokhaimaudich.MaDonViUT,
        //            Status = AgentsStatus.UYTHAC,
        //        });
        //        // Doanh Nghiệp chịu trách nhiệm nộp thuế
        //        tkx.Agents.Add(new Agent
        //        {
        //            Name = tokhaimaudich.TenDoanhNghiep,
        //            Identity = tokhaimaudich.MaDoanhNghiep,
        //            Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
        //        });
        //        //Mã MID
        //        tkx.Agents.Add(new Agent
        //        {
        //            Name = "??",
        //            Identity = "??",
        //            Status = AgentsStatus.MA_MID
        //        });
        //        // Add AdditionalDocument (thêm hợp đồng)
        //        tkx.AdditionalDocument.Add(new AdditionalDocument
        //        {
        //            Issue = tokhaimaudich.NgayHopDong.ToString(formatDate),
        //            Reference = tokhaimaudich.SoHopDong,
        //            Expire = tokhaimaudich.NgayHetHanHopDong.ToString(formatDate),
        //            Name = "??",
        //            Type = AdditionalDocumentType.HOP_DONG
        //        });
        //        // Thêm giấy phép
        //        tkx.AdditionalDocument.Add(new AdditionalDocument
        //        {
        //            Issue = tokhaimaudich.NgayGiayPhep.ToString(formatDate),
        //            Reference = tokhaimaudich.SoGiayPhep,
        //            Expire = tokhaimaudich.NgayHetHanGiayPhep.ToString(formatDate),
        //            Name = "??",
        //            Type = AdditionalDocumentType.EXPORT_LICENCE
        //        });

        //        //Add CustomGoodsItem
        //        int SoHang = tokhaimaudich.HMDCollection.Count;
        //        for (int i = 0; i < SoHang; i++)
        //        {

        //            tkx.GoodsShipment.CustomsGoodsItems.Add(new CustomsGoodsItem
        //             {

        //                 CustomsValue = Helpers.FormatNumeric(tokhaimaudich.HMDCollection[i].TriGiaKB,2),
        //                 StatisticalValue = tokhaimaudich.HMDCollection[i].TriGiaTT.ToString(),
        //                 UnitPrice = Helpers.FormatNumeric(tokhaimaudich.HMDCollection[i].DonGiaKB,2),
        //                 StatisticalUnitPrice = Helpers.FormatNumeric(tokhaimaudich.HMDCollection[i].DonGiaTT,2),
        //                 Sequence = tokhaimaudich.HMDCollection[i].SoThuTuHang.ToString(),
        //                 Manufacturer = new NameBase { Name = "", Identity = "" },
        //                 Origin = new Origin { OriginCountry = tokhaimaudich.HMDCollection[i].NuocXX_ID },
        //                 Commodity = new Commodity
        //                 {
        //                     Type       = "??",
        //                     Description = tokhaimaudich.HMDCollection[i].TenHang,
        //                     Identification = tokhaimaudich.HMDCollection[i].MaPhu,
        //                     TariffClassification = tokhaimaudich.HMDCollection[i].MaHS,
        //                     TariffClassificationExtension = "",
        //                     Brand = "??",
        //                     Grade = "??",
        //                     Ingredients = "??",
        //                     ModelNumber = "??",
        //                     DutyTaxFee = new List<DutyTaxFee>(),
        //                     InvoiceLine = new InvoiceLine { ItemCharge = "", Line = "" }
        //                 },
        //                 GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(tokhaimaudich.HMDCollection[i].SoLuong,2), MeasureUnit = tokhaimaudich.HMDCollection[i].DVT_ID },
        //                 CustomsValuation = new CustomsValuation
        //                 {
        //                     ExitToEntryCharge = Helpers.FormatNumeric(tokhaimaudich.PhiKhac,2),
        //                     FreightCharge = Helpers.FormatNumeric(tokhaimaudich.PhiVanChuyen,2),
        //                     Method = "",
        //                     OtherChargeDeduction = "0",
        //                 },
        //                 SpecializedManagement = new SpecializedManagement
        //                 {
        //                   Identification = "??",
        //                   Type = tokhaimaudich.HMDCollection[i].DVT_HTS,
        //                   Quantity = tokhaimaudich.HMDCollection[i].SoLuong_HTS.ToString(),
        //                   UnitPrice = "??",
        //                   MeasureUnit = tokhaimaudich.HMDCollection[i].Ma_HTS,
        //                 }
        //             });
        //            //add DutyTaxFee
        //            tkx.GoodsShipment.CustomsGoodsItems[i].Commodity.DutyTaxFee.Add(new DutyTaxFee
        //             {
        //                 DutyRegime = "",
        //                 SpecificTaxBase = "",
        //                 Type = "1",
        //                 Tax = tokhaimaudich.HMDCollection[i].ThueSuatXNK.ToString(),
        //                 AdValoremTaxBase = tokhaimaudich.HMDCollection[i].ThueXNK.ToString(),
        //             });
        //            tkx.GoodsShipment.CustomsGoodsItems[i].Commodity.DutyTaxFee.Add(new DutyTaxFee
        //             {
        //                 DutyRegime = "",
        //                 SpecificTaxBase = "",
        //                 Type = "2",
        //                 Tax = tokhaimaudich.HMDCollection[i].ThueSuatGTGT.ToString(),
        //                 AdValoremTaxBase = tokhaimaudich.HMDCollection[i].ThueGTGT.ToString(),
        //             });
        //            tkx.GoodsShipment.CustomsGoodsItems[i].Commodity.DutyTaxFee.Add(new DutyTaxFee
        //             {
        //                 DutyRegime = "",
        //                 SpecificTaxBase = "",
        //                 Type = "3",
        //                 Tax = tokhaimaudich.HMDCollection[i].ThueSuatTTDB.ToString(),
        //                 AdValoremTaxBase = tokhaimaudich.HMDCollection[i].ThueTTDB.ToString(),
        //             });
        //            tkx.GoodsShipment.CustomsGoodsItems[i].Commodity.DutyTaxFee.Add(new DutyTaxFee
        //              {
        //                  DutyRegime = "",
        //                  SpecificTaxBase = "",
        //                  Type = "4",
        //                  Tax = tokhaimaudich.HMDCollection[i].ThueSuatXNK.ToString(),
        //                  AdValoremTaxBase = tokhaimaudich.HMDCollection[i].ThueXNK.ToString(),
        //              });
        //            tkx.GoodsShipment.CustomsGoodsItems[i].Commodity.DutyTaxFee.Add(new DutyTaxFee
        //             {
        //                 DutyRegime = "",
        //                 SpecificTaxBase = "",
        //                 Type = "5",
        //                 Tax = tokhaimaudich.HMDCollection[i].TyLeThuKhac.ToString(),
        //                 AdValoremTaxBase = tokhaimaudich.HMDCollection[i].TriGiaThuKhac.ToString(),
        //             });
        //        }
        //        //add License
        //        if (tokhaimaudich.GiayPhepCollection != null)
        //            foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep items in tokhaimaudich.GiayPhepCollection)
        //            {
        //                tkx.License.Add(new License
        //                {
        //                    Issuer = items.NguoiCap,
        //                    Reference = items.SoGiayPhep,
        //                    Issue = items.NgayGiayPhep.ToString(formatDate),
        //                    IssueLocation = items.NoiCap,
        //                    Type = "",
        //                    Expire = items.NgayHetHan.ToString(formatDate),
        //                    AdditionalInformation = new AdditionalInformation { Content = new Content { Text = items.ThongTinKhac } },
        //                    GoodItems = new List<GoodsItem>()
        //                });
        //                int index = tokhaimaudich.GiayPhepCollection.IndexOf(items);
        //                if (items.ListHMDofGiayPhep != null)
        //                    foreach (Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail items2 in items.ListHMDofGiayPhep)

        //                        tkx.License[index].GoodItems.Add(new GoodsItem
        //                        {
        //                            Sequence = items2.SoThuTuHang.ToString(),
        //                            StatisticalValue = Helpers.FormatNumeric(items2.TriGiaKB,2),
        //                            CurrencyExchange = new CurrencyExchange { CurrencyType = items2.MaNguyenTe },
        //                            Commodity = new Commodity
        //                            {
        //                                Description = items2.TenHang,
        //                                Identification = "",
        //                                TariffClassification = items2.MaHS
        //                            },
        //                            GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(items2.SoLuong,2), MeasureUnit = items2.DVT_ID },
        //                            AdditionalInformation = new AdditionalInformation { Content = new Content { Text = items2.GhiChu } }

        //                        });
        //            }
        //        if (tokhaimaudich.HopDongThuongMaiCollection != null)
        //            foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai items in tokhaimaudich.HopDongThuongMaiCollection)
        //            {
        //                tkx.ContractDocument.Add(new ContractDocument
        //                {
        //                    Reference = items.SoHopDongTM,
        //                    Issue = items.NgayHopDongTM.ToString(formatDate),
        //                    Expire = items.ThoiHanThanhToan.ToString(formatDate),
        //                    Payment = new Payment { Method = items.PTTT_ID },
        //                    TradeTerm = new TradeTerm { Condition = items.DKGH_ID },
        //                    DeliveryDestination = new DeliveryDestination { Line = items.DiaDiemGiaoHang },
        //                    CurrencyExchange = new CurrencyExchange { CurrencyType = items.NguyenTe_ID },
        //                    TotalValue = items.TongTriGia.ToString(),
        //                    Buyer = new NameBase { Name = items.TenDonViBan, Identity = items.MaDonViBan },
        //                    Seller = new NameBase { Name = items.TenDonViMua, Identity = items.MaDonViMua },
        //                    AdditionalInformation = new AdditionalInformation { Content = new Content { Text = items.ThongTinKhac } },
        //                    ContractItems = new List<ContractItem>()
        //                });
        //                int index = tokhaimaudich.HopDongThuongMaiCollection.IndexOf(items);
        //                if (items.ListHangMDOfHopDong != null)
        //                    foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail items2 in items.ListHangMDOfHopDong)
        //                        tkx.ContractDocument[index].ContractItems.Add(new ContractItem
        //                        {
        //                            unitPrice = items2.DonGiaKB.ToString(),
        //                            statisticalValue = items2.TriGiaKB.ToString(),
        //                            Commodity = new Commodity
        //                            {
        //                                Description = items2.TenHang,
        //                                Identification = items2.MaPhu,
        //                                TariffClassification = items2.MaHS,
        //                            },
        //                            Origin = new Origin { OriginCountry = items2.NuocXX_ID },
        //                            GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(items2.SoLuong,2), MeasureUnit = items2.DVT_ID },
        //                            AdditionalInformation = new AdditionalInformation { Content = new Content { Text = items2.GhiChu } }
        //                        });
        //            }
        //        if (tokhaimaudich.HoaDonThuongMaiCollection != null)
        //            foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai items in tokhaimaudich.HoaDonThuongMaiCollection)
        //            {
        //                tkx.CommercialInvoices.Add(new CommercialInvoice
        //                {
        //                    Reference = items.SoHoaDon,
        //                    Issue = items.NgayHoaDon.ToString(formatDate),
        //                    Seller = new NameBase { Name = items.TenDonViBan, Identity = items.MaDonViBan },
        //                    Buyer = new NameBase { Name = items.TenDonViMua, Identity = items.MaDonViMua },
        //                    AdditionalDocument = new AdditionalDocument { Reference = "", Issue = "" },
        //                    Payment = new Payment { Method = items.PTTT_ID },
        //                    CurrencyExchange = new CurrencyExchange { CurrencyType = items.NguyenTe_ID },
        //                    TradeTerm = new TradeTerm { Condition = items.DKGH_ID },
        //                    CommercialInvoiceItems = new List<CommercialInvoiceItem>()
        //                });
        //                int index = tokhaimaudich.HoaDonThuongMaiCollection.IndexOf(items);
        //                if (items.ListHangMDOfHoaDon != null)
        //                    foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail items2 in items.ListHangMDOfHoaDon)
        //                        tkx.CommercialInvoices[index].CommercialInvoiceItems.Add(new CommercialInvoiceItem
        //                        {
        //                            Sequence = items2.SoThuTuHang.ToString(),
        //                            UnitPrice = Helpers.FormatNumeric(items2.DonGiaKB,2),
        //                            StatisticalValue = Helpers.FormatNumeric(items2.TriGiaKB,2),
        //                            Origin = new Origin { OriginCountry = items2.NuocXX_ID },
        //                            Commodity = new Commodity
        //                            {
        //                                Description = items2.TenHang,
        //                                Identification = items2.MaPhu,
        //                                TariffClassification = items2.MaHS,
        //                            },
        //                            GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(items2.SoLuong,2), MeasureUnit = items2.DVT_ID },
        //                            ValuationAdjustment = new ValuationAdjustment
        //                            {
        //                                Addition = items2.GiaTriDieuChinhTang.ToString(),
        //                                Deduction = items2.GiaiTriDieuChinhGiam.ToString(),
        //                            },
        //                            AdditionalInformation = new AdditionalInformation { Content = new Content { Text = items2.GhiChu } },
        //                        });
        //            }
        //        if (tokhaimaudich.COCollection != null)
        //            foreach (Company.KDT.SHARE.QuanLyChungTu.CO items in tokhaimaudich.COCollection)
        //            {
        //                tkx.CertificateOfOrigin.Add(new CertificateOfOrigin
        //                {
        //                    Reference = items.SoCO,
        //                    Type = items.LoaiCO,
        //                    Issuer = items.ToChucCap,
        //                    Issue = items.NgayCO.ToString(formatDate),
        //                    IssueLocation = items.NuocCapCO,
        //                    Representative = items.NguoiKy,
        //                    ExporterEx = new NameBase { Name = items.TenDiaChiNguoiXK, Identity = "?" },
        //                    ImporterEx = new NameBase { Name = items.TenDiaChiNguoiNK, Identity = "?" },
        //                    ExportationCountryEx = new LocationNameBase { Code = items.MaNuocXKTrenCO, Name = "?" },
        //                    ImportationCountryEx = new LocationNameBase { Code = items.MaNuocNKTrenCO, Name = "?" },
        //                    LoadingLocation = new LoadingLocation { Name = "??", Code = "??", Loading = "??" },
        //                    UnloadingLocation = new UnloadingLocation { Name = "??", Code = "??" },
        //                    IsDebt = items.NoCo.ToString(),
        //                    Submit = items.ThoiHanNop.ToString(formatDate),
        //                    AdditionalInformation = new AdditionalInformation { Content = new Content { Text = items.ThongTinMoTaChiTiet } },
        //                    GoodsItems = new List<GoodsItem>(),
        //                });
        //            }
        //        if (tokhaimaudich.listChuyenCuaKhau != null)
        //            foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau items in tokhaimaudich.listChuyenCuaKhau)
        //            {
        //                tkx.CustomsOfficeChangedRequest.Add(new CustomsOfficeChangedRequest
        //                {
        //                    AdditionalDocument = new AdditionalDocument { Reference = items.ThongTinKhac, Issue = items.NgayVanDon.ToString(formatDate) },
        //                    AdditionalInformation = new AdditionalInformation
        //                    {
        //                        Content = new Content { Text = items.ThongTinKhac },
        //                        ExaminationPlace = items.DiaDiemKiemTra,
        //                        Time = items.ThoiGianDen.ToString(formatDate),
        //                        Route = items.TuyenDuong
        //                    },
        //                });
        //            };

        //        if (tokhaimaudich.ChungTuKemCollection != null)
        //            foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem items in tokhaimaudich.ChungTuKemCollection)
        //            {
        //                tkx.AttachDocumentItem.Add(new AttachDocumentItem
        //                {
        //                    Issuer = items.MA_LOAI_CT,
        //                    Sequence = tokhaimaudich.ChungTuKemCollection.IndexOf(items).ToString(),
        //                    Issue = items.NGAY_CT.ToString(formatDate),
        //                    Reference = items.SO_CT,
        //                    Description = items.DIENGIAI,
        //                    AttachedFiles = new List<AttachedFile>(),
        //                });
        //                if (items.listCTChiTiet.Count > 0)
        //                    foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet items2 in items.listCTChiTiet)
        //                        tkx.AttachDocumentItem[tokhaimaudich.ChungTuKemCollection.IndexOf(items)].AttachedFiles.Add(new AttachedFile
        //                        {
        //                            FileName = items2.FileName,
        //                            Content = new Content { Text = System.Text.Encoding.ASCII.GetString(items2.NoiDung) }
        //                        });
        //            };
        //        return tkx;


        //    }

        //}
        /// <summary>
        /// Huy Khai Bao To Khai Chua Duoc Duyet
        /// </summary>
        /// <param name="LoaiToKhai"></param>
        /// <returns></returns>
        public static DeclarationBase HuyKhaiBao(string loaiToKhai,string reference,long soTiepNhan,string maHaiQuan,DateTime ngayTiepNhan)
        {
            DeclarationBase dec = new DeclarationBase() 
            {
                Issuer = loaiToKhai,
                Reference = reference,
                Issue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(soTiepNhan, 0),
                Acceptance = ngayTiepNhan.ToString("yyyy-MM-dd HH:mm:ss"),
                DeclarationOffice = maHaiQuan,
                //AdditionalInformation = new AdditionalInformation()
                //{
                //    Content = new Content() { Text = "." }
                //}
             };
            return dec;
        }

        //public static KD_ToKhaiMauDich ToDataTransferObject(ToKhaiMauDich tkmd)
        //{
        //    string formatDateDetail = "yyyy-MM-dd HH:mm:ss";
        //    string formatDate = "yyyy-MM-dd";
        //    bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");
        //    bool isToKhaiSua = tkmd.SoToKhai != 0 && tkmd.TrangThaiXuLy ==Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
        //    #region Header
        //    KD_ToKhaiMauDich tokhai = new KD_ToKhaiMauDich()
        //    {
        //        Issuer = isToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT,
        //        Reference = tkmd.GUIDSTR,
        //        Issue = DateTime.Now.ToString(formatDateDetail),
        //        Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
        //        IssueLocation = string.Empty,
        //        Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
        //        // Số tiếp nhận
        //        CustomsReference = isToKhaiSua ? Helpers.FormatNumeric(tkmd.SoTiepNhan) : string.Empty,
        //        //Ngày dang ký chứng thư
        //        Acceptance = isToKhaiSua ? tkmd.NgayTiepNhan.ToString(formatDateDetail) : string.Empty,
        //        // Ðon vị hải quan khai báo
        //        DeclarationOffice = tkmd.MaHaiQuan.Trim(),
        //        // Số hàng
        //        GoodsItem = Helpers.FormatNumeric(tkmd.SoHang),
        //        LoadingList = Helpers.FormatNumeric(tkmd.SoLuongPLTK),

        //        // Khối luợng và khối luợng tịnh
        //        TotalGrossMass = Helpers.FormatNumeric(tkmd.TrongLuong, 3),
        //        TotalNetGrossMass = Helpers.FormatNumeric(tkmd.TrongLuongNet, 3),
        //        // Mã Loại Hình
        //        NatureOfTransaction = tkmd.MaLoaiHinh,
        //        // Phuong thức thanh toán
        //        PaymentMethod = tkmd.PTTT_ID,



        //        #region Install element
        //        Agents = new List<Agent>(),

        //        //Nguyên tệ
        //        CurrencyExchange = new CurrencyExchange { CurrencyType = tkmd.NguyenTe_ID, Rate = Helpers.FormatNumeric(tkmd.TyGiaTinhThue, 3) },

        //        //Số kiện
        //        DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tkmd.SoKien) },

        //        // Hợp Ðồng
        //        AdditionalDocument = new List<AdditionalDocument>(),

        //        // Hóa Ðon thuong mại
        //        Invoice = new Invoice { Issue = tkmd.NgayHoaDonThuongMai.ToString(formatDate), Reference = tkmd.SoHoaDonThuongMai, Type = AdditionalDocumentType.HOA_DON_THUONG_MAI },

        //        // Doanh Nghiệp Xuất khẩu
        //        Exporter = isToKhaiNhap ? null : new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() },

        //        //Doanh nghiệp nhập khẩu
        //        Importer = isToKhaiNhap ? new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() } : null,

        //        // Nguười gởi ???
        //        RepresentativePerson = new RepresentativePerson { },

        //        // Đề xuất khác | Mã loại thông tin - Ghi chú khác
        //        AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = tkmd.DeXuatKhac } },

        //        // GoodsShipmet Thông tin hàng hóa
        //        GoodsShipment = new GoodsShipment(),

        //        License = new List<License>(),
        //        ContractDocument = new List<ContractDocument>(),
        //        CommercialInvoices = new List<CommercialInvoice>(),
        //        CertificateOfOrigin = new List<CertificateOfOrigin>(),
        //        CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
        //        AttachDocumentItem = new List<AttachDocumentItem>(),
        //        AdditionalDocumentEx = new List<AdditionalDocument>(),
        //        #endregion Nrr

        //    };
        //    #endregion Header

        //    #region Agents Đại lý khai
        //    // Người Khai Hải Quan
        //    tokhai.Agents.Add(new Agent
        //    {
        //        Name = tkmd.TenDaiLyTTHQ,
        //        Identity = tkmd.MaDaiLyTTHQ,
        //        Status = AgentsStatus.NGUOIKHAI_HAIQUAN
        //    });
        //    // ủy Thác
        //    tokhai.Agents.Add(new Agent
        //    {
        //        Name = tkmd.TenDonViUT,
        //        Identity = tkmd.MaDonViUT,
        //        Status = AgentsStatus.UYTHAC,
        //    });
        //    // Doanh Nghiệp chịu trách nhiệm nộp thuế
        //    tokhai.Agents.Add(new Agent
        //    {
        //        Name = tkmd.TenDoanhNghiep,
        //        Identity = tkmd.MaDoanhNghiep,
        //        Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
        //    });
        //    #endregion

        //    #region AdditionalDocument Hợp đồng - Giấy phép
        //    // Add AdditionalDocument (thêm hợp đồng)
        //    tokhai.AdditionalDocument.Add(new AdditionalDocument
        //    {
        //        Issue = tkmd.NgayHopDong.ToString(formatDate),
        //        Reference = tkmd.SoHopDong,
        //        Type = AdditionalDocumentType.HOP_DONG,
        //        Name = "???",
        //        Expire = tkmd.NgayHetHanHopDong.ToString(formatDate)
        //    });

        //    // Thêm giấy phép
        //    tokhai.AdditionalDocument.Add(new AdditionalDocument
        //    {
        //        Issue = tkmd.NgayGiayPhep.ToString(formatDate),
        //        Reference = tkmd.SoGiayPhep,
        //        Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
        //        Name = "???",
        //        Expire = tkmd.NgayHetHanGiayPhep.ToString(formatDate)
        //    });

        //    #endregion

        //    #region GoodsShipment thông tin về hàng hóa
        //    tokhai.GoodsShipment = new GoodsShipment()
        //    {
        //        ImportationCountry = isToKhaiNhap ? null : tkmd.NuocNK_ID.Substring(0, 2),
        //        ExportationCountry = isToKhaiNhap ? tkmd.NuocXK_ID.Substring(0, 2) : null,
        //        Consignor = new NameBase { Identity = string.Empty, Name = string.Empty },
        //        Consignee = new NameBase { Identity = string.Empty, Name = string.Empty },
        //        NotifyParty = new NameBase { Identity = string.Empty, Name = string.Empty },
        //        DeliveryDestination = new DeliveryDestination { Line = string.Empty },
        //        EntryCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? tkmd.CuaKhau_ID : string.Empty, Name = isToKhaiNhap ? Company.BLL.DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) : string.Empty },
        //        ExitCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? string.Empty : tkmd.CuaKhau_ID, Name = isToKhaiNhap ? string.Empty : Company.BLL.DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) },
        //        Importer = isToKhaiNhap ? null : new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty },
        //        Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty } : null,
        //        TradeTerm = new TradeTerm { Condition = tkmd.DKGH_ID },
        //        //CustomsGoodsItems = new List<CustomsGoodsItem>()
        //    };
        //    #endregion GoodsShipment

        //    #region CustomGoodsItem Danh sách hàng khai báo

        //    //Add CustomGoodsItem
        //    int soluonghang = 0;
        //    if (tkmd.HMDCollection != null)
        //    {
        //        soluonghang = tkmd.HMDCollection.Count;
        //        tokhai.GoodsShipment.CustomsGoodsItems = new List<CustomsGoodsItem>();
        //    }
        //    for (int i = 0; i < soluonghang; i++)
        //    {
        //        HangMauDich hmd = tkmd.HMDCollection[i];
        //        #region CustomsGoodsItem
        //        CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem
        //        {

        //            CustomsValue = Helpers.FormatNumeric(hmd.TriGiaKB, 2),
        //            Sequence = Helpers.FormatNumeric(hmd.SoThuTuHang),
        //            StatisticalValue = Helpers.FormatNumeric(hmd.TriGiaTT),
        //            UnitPrice = Helpers.FormatNumeric(hmd.DonGiaKB, 2),
        //            StatisticalUnitPrice = Helpers.FormatNumeric(hmd.DonGiaTT, 2),
        //            Manufacturer = new NameBase { Name = "??"/*Tên hãng sx*/, Identity = "??"/*Mã hãng sx*/ },
        //            Origin = new Origin { OriginCountry = hmd.NuocXX_ID.Substring(0, 2) },
        //            GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hmd.SoLuong, 3), MeasureUnit = hmd.DVT_ID },
        //            CustomsValuation = new CustomsValuation
        //            {
        //                //Trị giá trên mỗi dòng hàng (Thường thì chia đều cho tất cả dòng hàng)
        //                ExitToEntryCharge = Helpers.FormatNumeric(i == 0 ? (tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem) : 0, 2),
        //                FreightCharge = Helpers.FormatNumeric(i == 0 ? (tkmd.PhiVanChuyen) : 0, 2),
        //                Method = string.Empty,
        //                OtherChargeDeduction = "0"
        //            }
        //        };
        //        #endregion

        //        #region Commodity Hàng trong tờ khai
        //        Commodity commodity = new Commodity
        //        {
        //            Description = hmd.TenHang,
        //            Identification = hmd.MaPhu,
        //            TariffClassification = hmd.MaHS,
        //            TariffClassificationExtension = "",
        //            Brand = "???",
        //            Grade = "???",
        //            Ingredients = "???",
        //            ModelNumber = "???",
        //            DutyTaxFee = new List<DutyTaxFee>(),
        //            InvoiceLine = new InvoiceLine { ItemCharge = "", Line = "" }
        //        };
        //        #region DutyTaxFee tiền thuế của hàng
        //        commodity.DutyTaxFee.Add(new DutyTaxFee
        //        {
        //            AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 2),
        //            DutyRegime = string.Empty,
        //            SpecificTaxBase = string.Empty,
        //            Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatXNK, 1),
        //            Type = DutyTaxFeeType.THUE_XNK
        //        });
        //        commodity.DutyTaxFee.Add(new DutyTaxFee
        //        {
        //            AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueGTGT, 10),
        //            DutyRegime = "",
        //            SpecificTaxBase = "",
        //            Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatGTGT, 1),
        //            Type = DutyTaxFeeType.THUE_VAT,

        //        });
        //        commodity.DutyTaxFee.Add(new DutyTaxFee
        //        {
        //            AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueTTDB, 10),
        //            DutyRegime = "",
        //            SpecificTaxBase = "",
        //            Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatTTDB, 1),
        //            Type = DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET,

        //        });
        //        commodity.DutyTaxFee.Add(new DutyTaxFee
        //        {
        //            AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 10),
        //            DutyRegime = "",
        //            SpecificTaxBase = "",
        //            Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatXNK, 1),
        //            Type = DutyTaxFeeType.THUE_KHAC,


        //        });
        //        commodity.DutyTaxFee.Add(new DutyTaxFee
        //        {
        //            AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].PhuThu, 10),
        //            DutyRegime = "",
        //            SpecificTaxBase = "",
        //            Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].TyLeThuKhac, 1),
        //            Type = DutyTaxFeeType.THUE_CHENH_LECH_GIA,

        //        });
        //        #endregion DutyTaxFee thuế

        //        customsGoodsItem.Commodity = commodity;
        //        #endregion Hàng chính

        //        #region AdditionalDocument if Search(Giấy phép có hàng trong tờ khai)
        //        //Lấy thông tin giấy phép đầu tiên
        //        if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
        //        {
        //            Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = tkmd.GiayPhepCollection[0];
        //            customsGoodsItem.AdditionalDocument = new AdditionalDocument()
        //            {
        //                Issue = giayphep.NgayGiayPhep.ToString(formatDate), /*Ngày cấp giấy phép yyyy-MM-dd*/
        //                Issuer = giayphep.NguoiCap/*Người cấp*/,
        //                IssueLocation = giayphep.NoiCap/*Nơi cấp*/,
        //                Reference = giayphep.SoGiayPhep/*Số giấy phép*/,
        //                Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
        //                Name = "GP"/* Tên giấy phép*/,
        //                Expire = giayphep.NgayHetHan.ToString(formatDate)
        //            };
        //        }
        //        #endregion

        //        #region CertificateOfOrigin if Search(Co có hàng trong tờ khai)
        //        /*
        //        if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
        //        {
        //            Company.KDT.SHARE.QuanLyChungTu.CO co = tkmd.COCollection[tkmd.COCollection.Count - 1];
        //            customsGoodsItem.CertificateOfOrigin = new CertificateOfOrigin()
        //            {
        //                Issue = co.NgayCO.ToString(formatDate),
        //                Issuer = co.ToChucCap,
        //                IssueLocation = co.NuocCapCO,
        //                Reference = co.SoCO,
        //                Type = co.LoaiCO,
        //                Name = "CO",
        //                Expire = "",
        //                Exporter = isToKhaiNhap ? co.TenDiaChiNguoiXK : null,
        //                Importer = isToKhaiNhap ? null : co.TenDiaChiNguoiNK,
        //                ExportationCountry = isToKhaiNhap ? co.MaNuocXKTrenCO : null,
        //                ImportationCountry = isToKhaiNhap ? null : co.MaNuocNKTrenCO,
        //                Content = co.ThongTinMoTaChiTiet,
        //                IsDebt = Helpers.FormatNumeric(co.NoCo, 0),
        //                Submit = co.ThoiHanNop.ToString(formatDate)
        //            };
        //        }*/
        //        #endregion

        //        #region AdditionalInformations - ValuationAdjustments Thêm hàng từ tờ khai trị giá 1,2,3
               
        //        //if (isToKhaiNhap)
        //        //{
        //        //    //Thông tin tờ khai trị giá.
        //        //    #region Tờ khai trị giá PP1
        //        //    if (tkmd.TKTGCollection != null && tkmd.TKTGCollection.Count > 0)
        //        //    {
        //        //        customsGoodsItem.AdditionalInformations = new List<AdditionalInformation>();
        //        //        customsGoodsItem.ValuationAdjustments = new List<ValuationAdjustment>();
        //        //        /* Fill dữ liệu hàng  trong tờ khai trị giá PP1*/
        //        //        ToKhaiTriGia tktg = tkmd.TKTGCollection[0];

        //        //        #region AdditionalInformations Nội dung tờ khai trị giá
        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            StatementDescription = "TO_SO",
        //        //            Statement = AdditionalInformationStatement.TO_SO,
        //        //            Content = new Content() { Text = Helpers.FormatNumeric(tktg.ToSo) }
        //        //        });
        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            StatementDescription = "NGAY_XK",
        //        //            Statement = AdditionalInformationStatement.NGAY_XK,
        //        //            Content = new Content() { Text = tktg.NgayXuatKhau.ToString(formatDate) }
        //        //        });
        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            StatementDescription = "QUYEN_SD",
        //        //            Statement = AdditionalInformationStatement.QUYEN_SD,
        //        //            Content = new Content() { Text = Helpers.FormatNumeric(tktg.QuyenSuDung) }
        //        //        });

        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            StatementDescription = "KHONG_XD",
        //        //            Statement = AdditionalInformationStatement.KHONG_XD,
        //        //            Content = new Content() { Text = Helpers.FormatNumeric(tktg.KhongXacDinh) }
        //        //        });

        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            StatementDescription = "TRA_THEM",
        //        //            Statement = AdditionalInformationStatement.TRA_THEM,
        //        //            Content = new Content() { Text = Helpers.FormatNumeric(tktg.TraThem) }
        //        //        });
        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            StatementDescription = "TIEN_TRA_16",
        //        //            Statement = AdditionalInformationStatement.TIEN_TRA_16,
        //        //            Content = new Content() { Text = Helpers.FormatNumeric(tktg.TienTra) }
        //        //        });
        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            StatementDescription = "CO_QHDB",
        //        //            Statement = AdditionalInformationStatement.CO_QHDB,
        //        //            Content = new Content() { Text = Helpers.FormatNumeric(tktg.CoQuanHeDacBiet) }
        //        //        });
        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            StatementDescription = "KIEU_QHDB",
        //        //            Statement = AdditionalInformationStatement.KIEU_QHDB,
        //        //            Content = new Content() { Text = tktg.KieuQuanHe }
        //        //        });
        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            StatementDescription = "ANH_HUONG_QH",
        //        //            Statement = AdditionalInformationStatement.ANH_HUONG_QH,
        //        //            Content = new Content() { Text = Helpers.FormatNumeric(tktg.AnhHuongQuanHe) }
        //        //        });
        //        //        #endregion Nội dung tờ khai trị giá

        //        //        #region ValuationAdjustments Chi tiết hàng tờ khai trị giá pp1
        //        //        if (tktg.HTGCollection != null && tktg.HTGCollection.Count > 0)
        //        //        {
        //        //            HangTriGia hangtrigia = tktg.HTGCollection[0];
        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Gia_hoa_don,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.GiaTrenHoaDon, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Thanh_toan_gian_tiep,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.KhoanThanhToanGianTiep, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Tien_tra_truoc,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.TraTruoc, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Phi_hoa_hong,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.HoaHong, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Phi_bao_bi,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiBaoBi, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Phi_dong_goi,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiDongGoi, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Khoan_tro_giup,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.TroGiup, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Tro_giup_NVL,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.NguyenLieu, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Tro_giup_NL,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.VatLieu, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Tro_giup_cong_cu,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.CongCu, 2)
        //        //            });
        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Tro_giup_thiet_ke,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.ThietKe, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Tien_ban_quyen,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.BanQuyen, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Tien_phai_tra,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.TienTraSuDung, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Phi_van_tai,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiVanChuyen, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Phi_bao_hiem,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.PhiBaoHiem, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Phi_VT_BH_noi_dia,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiNoiDia, 2)
        //        //            });

        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Phi_phat_sinh,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiPhatSinh, 2)
        //        //            });


        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Tien_lai,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.TienLai, 2)
        //        //            });


        //        //            customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //            {
        //        //                Addition = ValuationAdjustmentAddition.Thue_phi_le_phi,
        //        //                Percentage = string.Empty,
        //        //                Amount = Helpers.FormatNumeric(hangtrigia.TienThue, 2)
        //        //            });

        //        //        }


        //        //        #endregion Chi tiết hàng tờ khai trị giá

        //        //    }
        //        //    #endregion Tờ khai trị giá PP1

        //        //    if (tkmd.TKTGPP23Collection != null && tkmd.TKTGPP23Collection.Count > 0)
        //        //    {
        //        //        customsGoodsItem.AdditionalInformations = new List<AdditionalInformation>();
        //        //        customsGoodsItem.ValuationAdjustments = new List<ValuationAdjustment>();

        //        //        ToKhaiTriGiaPP23 tkpgP23 = tkmd.TKTGPP23Collection[tkmd.TKTGPP23Collection.Count - 1];
        //        //        string maTktg = tkpgP23.MaToKhaiTriGia.ToString();
        //        //        #region AdditionalInformations Nội dung tờ khai trị giá PP23

        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            Content = new Content() { Text = "Lydo PP" + maTktg },
        //        //            Statement = maTktg + AdditionalInformationStatement.LYDO_KAD_PP1,
        //        //            StatementDescription = "LYDO_KAD_PP1"

        //        //        });
        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            Content = new Content() { Text = tkpgP23.NgayXuat.ToString(formatDate) },
        //        //            Statement = maTktg + AdditionalInformationStatement.NGAY_XK,
        //        //            StatementDescription = "NGAY_XK"

        //        //        });

        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            Content = new Content() { Text = Helpers.FormatNumeric(tkpgP23.STTHangTT) },
        //        //            Statement = maTktg + AdditionalInformationStatement.STTHANG_TT,
        //        //            StatementDescription = "STTHANG_TT"
        //        //        });

        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            Content = new Content() { Text = Helpers.FormatNumeric(tkpgP23.SoTKHangTT) },
        //        //            Statement = maTktg + AdditionalInformationStatement.SOTK_TT,
        //        //            StatementDescription = "SOTK_TT"
        //        //        });

        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            Content = new Content() { Text = tkpgP23.NgayDangKyHangTT.ToString(formatDate) },
        //        //            Statement = maTktg + AdditionalInformationStatement.NGAY_NK_TT,
        //        //            StatementDescription = "NGAY_NK_TT"
        //        //        });

        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            Content = new Content() { Text = tkpgP23.MaHaiQuanHangTT },
        //        //            Statement = maTktg + AdditionalInformationStatement.MA_HQ_TT,
        //        //            StatementDescription = "MA_HQ_TT"
        //        //        });


        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            Content = new Content() { Text = tkpgP23.NgayXuatTT.ToString(formatDate) },
        //        //            Statement = maTktg + AdditionalInformationStatement.NGAY_XK_TT,
        //        //            StatementDescription = "NGAY_XK_TT"
        //        //        });

        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            Content = new Content() { Text = tkpgP23.GiaiTrinh },
        //        //            Statement = maTktg + AdditionalInformationStatement.GIAI_TRINH,
        //        //            StatementDescription = "GIAI_TRINH"
        //        //        });

        //        //        customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
        //        //        {
        //        //            Content = new Content() { Text = tkpgP23.MaLoaiHinhHangTT },
        //        //            Statement = maTktg + AdditionalInformationStatement.Ma_LH,
        //        //            StatementDescription = "Ma_LH"
        //        //        });
        //        //        #endregion Nội dung tờ khai trị giá PP23

        //        //        #region ValuationAdjustments Chi tiết nội dung trong tờ khai tri giá PP23

        //        //        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //        {
        //        //            Addition = maTktg + ValuationAdjustmentAddition.Tri_gia_hang_TT,
        //        //            Percentage = string.Empty,
        //        //            Amount = Helpers.FormatNumeric(tkpgP23.TriGiaNguyenTeHangTT, 2)
        //        //        });

        //        //        // Cộng ghi số dương trừ ghi số âm(+/-)
        //        //        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //        {
        //        //            Addition = maTktg + ValuationAdjustmentAddition.DCC_cap_do_TM,
        //        //            Percentage = string.Empty,
        //        //            Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongThuongMai != 0 ? tkpgP23.DieuChinhCongThuongMai : -tkpgP23.DieuChinhTruCapDoThuongMai, 2)
        //        //        });

        //        //        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //        {
        //        //            Addition = maTktg + ValuationAdjustmentAddition.DCC_so_luong,
        //        //            Percentage = string.Empty,
        //        //            Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongSoLuong != 0 ? tkpgP23.DieuChinhCongSoLuong : -tkpgP23.DieuChinhTruSoLuong, 2)
        //        //        });

        //        //        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //        {
        //        //            Addition = maTktg + ValuationAdjustmentAddition.DCC_khoan_khac,
        //        //            Percentage = string.Empty,
        //        //            Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongKhoanGiamGiaKhac != 0 ? tkpgP23.DieuChinhCongKhoanGiamGiaKhac : -tkpgP23.DieuChinhTruKhoanGiamGiaKhac, 2)
        //        //        });

        //        //        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //        {
        //        //            Addition = maTktg + ValuationAdjustmentAddition.DCC_phi_van_tai,
        //        //            Percentage = string.Empty,
        //        //            Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongChiPhiVanTai != 0 ? tkpgP23.DieuChinhCongChiPhiVanTai : -tkpgP23.DieuChinhTruChiPhiVanTai, 2)
        //        //        });

        //        //        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
        //        //        {
        //        //            Addition = maTktg + ValuationAdjustmentAddition.DCC_phi_bao_hiem,
        //        //            Percentage = string.Empty,
        //        //            Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongChiPhiBaoHiem != 0 ? tkpgP23.DieuChinhCongChiPhiBaoHiem : -tkpgP23.DieuChinhTruChiPhiBaoHiem, 2)
        //        //        });

        //        //        #endregion Chi tiết nội dung trong tờ khai tri giá PP23

        //        //        /* Fill dữ liệu hàng trong tờ khai trị giá PP23*/

        //        //    }


        //        //}
        //        //else
        //        //{
        //        //    /*
        //        //    customsGoodsItem.SpecializedManagement = new SpecializedManagement()
        //        //    {
        //        //        GrossMass = "",
        //        //        Identification = "",
        //        //        MeasureUnit = "",
        //        //        Quantity = "",
        //        //        Type = "",
        //        //        UnitPrice = ""
        //        //    };*/
        //        //}
        //        #endregion thêm hàng từ tờ khai trị giá 1,2,3

        //        tokhai.GoodsShipment.CustomsGoodsItems.Add(customsGoodsItem);
        //    }

        //    #endregion CustomGoodsItem Danh sách hàng khai báo
        //    #region Licenses Danh sách giấy phép XNK đi kèm


        //    if (tkmd.GiayPhepCollection != null)
        //        #region License Giấy phép
        //        foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep in tkmd.GiayPhepCollection)
        //        {
        //            License lic = new License
        //            {
        //                Issuer = giayPhep.NguoiCap,
        //                Reference = giayPhep.SoGiayPhep,
        //                Issue = giayPhep.NgayGiayPhep.ToString(formatDate),
        //                IssueLocation = giayPhep.NoiCap,
        //                Type = "",
        //                Expire = giayPhep.NgayHetHan.ToString(formatDate),
        //                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = giayPhep.ThongTinKhac } },
        //                GoodItems = new List<GoodsItem>()
        //            };

        //            if (giayPhep.ListHMDofGiayPhep != null)
        //                foreach (Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail hangInGiayPhep in giayPhep.ListHMDofGiayPhep)
        //                {
        //                    lic.GoodItems.Add(new GoodsItem
        //                    {
        //                        Sequence = Helpers.FormatNumeric(hangInGiayPhep.SoThuTuHang, 0),
        //                        StatisticalValue = Helpers.FormatNumeric(hangInGiayPhep.TriGiaKB, 3),
        //                        CurrencyExchange = new CurrencyExchange { CurrencyType = hangInGiayPhep.MaNguyenTe },
        //                        Commodity = new Commodity
        //                        {
        //                            Description = hangInGiayPhep.TenHang,
        //                            Identification = "",
        //                            TariffClassification = hangInGiayPhep.MaHS
        //                        },
        //                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInGiayPhep.SoLuong, 3), MeasureUnit = hangInGiayPhep.DVT_ID },
        //                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInGiayPhep.GhiChu } }

        //                    });
        //                }
        //            tokhai.License.Add(lic);
        //        }
        //        #endregion Giấy phép

        //    if (tkmd.HopDongThuongMaiCollection != null)
        //        #region ContractDocument  Hợp đồng thương mại
        //        foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai in tkmd.HopDongThuongMaiCollection)
        //        {
        //            ContractDocument contractDocument = new ContractDocument
        //            {
        //                Reference = hdThuongMai.SoHopDongTM,
        //                Issue = hdThuongMai.NgayHopDongTM.ToString(formatDate),
        //                Expire = hdThuongMai.ThoiHanThanhToan.ToString(formatDate),
        //                Payment = new Payment { Method = hdThuongMai.PTTT_ID },
        //                TradeTerm = new TradeTerm { Condition = hdThuongMai.DKGH_ID },
        //                DeliveryDestination = new DeliveryDestination { Line = hdThuongMai.DiaDiemGiaoHang },
        //                CurrencyExchange = new CurrencyExchange { CurrencyType = hdThuongMai.NguyenTe_ID },
        //                TotalValue = Helpers.FormatNumeric(hdThuongMai.TongTriGia, 2),
        //                Buyer = new NameBase { Name = hdThuongMai.TenDonViMua, Identity = hdThuongMai.MaDonViMua },
        //                Seller = new NameBase { Name = hdThuongMai.TenDonViBan, Identity = hdThuongMai.MaDonViBan },
        //                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hdThuongMai.ThongTinKhac } },
        //                ContractItems = new List<ContractItem>()
        //            };


        //            if (hdThuongMai.ListHangMDOfHopDong != null)
        //                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail hangInHdThuongMai in hdThuongMai.ListHangMDOfHopDong)
        //                    contractDocument.ContractItems.Add(new ContractItem
        //                    {
        //                        unitPrice = Helpers.FormatNumeric(hangInHdThuongMai.DonGiaKB, 2),
        //                        statisticalValue = Helpers.FormatNumeric(hangInHdThuongMai.TriGiaKB, 2),
        //                        Commodity = new Commodity
        //                        {
        //                            Description = hangInHdThuongMai.TenHang,
        //                            Identification = hangInHdThuongMai.MaPhu,
        //                            TariffClassification = hangInHdThuongMai.MaHS,
        //                        },
        //                        Origin = new Origin { OriginCountry = hangInHdThuongMai.NuocXX_ID.Substring(0, 2) },
        //                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHdThuongMai.SoLuong, 3), MeasureUnit = hangInHdThuongMai.DVT_ID },
        //                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHdThuongMai.GhiChu } }
        //                    });
        //            tokhai.ContractDocument.Add(contractDocument);
        //        }
        //        #endregion Hợp đồng thương mại

        //    if (tkmd.HoaDonThuongMaiCollection != null)
        //        #region CommercialInvoice Hóa đơn thương mại
        //        foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai in tkmd.HoaDonThuongMaiCollection)
        //        {
        //            CommercialInvoice commercialInvoice = new CommercialInvoice
        //            {
        //                Reference = hoaDonThuongMai.SoHoaDon,
        //                Issue = hoaDonThuongMai.NgayHoaDon.ToString(formatDate),
        //                Seller = new NameBase { Name = hoaDonThuongMai.TenDonViBan, Identity = hoaDonThuongMai.MaDonViBan },
        //                Buyer = new NameBase { Name = hoaDonThuongMai.TenDonViMua, Identity = hoaDonThuongMai.MaDonViMua },
        //                AdditionalDocument = new AdditionalDocument { Reference = "", Issue = "" },
        //                Payment = new Payment { Method = hoaDonThuongMai.PTTT_ID },
        //                CurrencyExchange = new CurrencyExchange { CurrencyType = hoaDonThuongMai.NguyenTe_ID },
        //                TradeTerm = new TradeTerm { Condition = hoaDonThuongMai.DKGH_ID },
        //                CommercialInvoiceItems = new List<CommercialInvoiceItem>()
        //            };

        //            if (hoaDonThuongMai.ListHangMDOfHoaDon != null)
        //                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail hangInHoaDonThuongMai in hoaDonThuongMai.ListHangMDOfHoaDon)
        //                    commercialInvoice.CommercialInvoiceItems.Add(new CommercialInvoiceItem
        //                    {
        //                        Sequence = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoThuTuHang, 0),
        //                        UnitPrice = Helpers.FormatNumeric(hangInHoaDonThuongMai.DonGiaKB, 2),
        //                        StatisticalValue = Helpers.FormatNumeric(hangInHoaDonThuongMai.TriGiaKB),
        //                        Origin = new Origin { OriginCountry = hangInHoaDonThuongMai.NuocXX_ID.Substring(0, 2) },
        //                        Commodity = new Commodity
        //                        {
        //                            Description = hangInHoaDonThuongMai.TenHang,
        //                            Identification = hangInHoaDonThuongMai.MaPhu,
        //                            TariffClassification = hangInHoaDonThuongMai.MaHS
        //                        },
        //                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoLuong, 3), MeasureUnit = hangInHoaDonThuongMai.DVT_ID },
        //                        ValuationAdjustment = new ValuationAdjustment
        //                        {
        //                            Addition = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaTriDieuChinhTang, 4),
        //                            Deduction = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaiTriDieuChinhGiam, 8),
        //                        },
        //                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHoaDonThuongMai.GhiChu } },
        //                    });
        //            tokhai.CommercialInvoices.Add(commercialInvoice);
        //        }
        //        #endregion Hóa đơn thương mại

        //    if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
        //    {

        //        #region CertificateOfOrigin CO

        //        //foreach (Company.KDT.SHARE.QuanLyChungTu.CO co in tkmd.COCollection)
        //        //{
        //        //    CertificateOfOrigin certificateOfOrigin = new CertificateOfOrigin
        //        //    {


        //        //        Reference = co.SoCO,
        //        //        Type = co.LoaiCO,
        //        //        Issuer = co.ToChucCap,
        //        //        Issue = co.NgayCO.ToString(formatDate),
        //        //        IssueLocation = co.NuocCapCO,
        //        //        Representative = co.NguoiKy,

        //        //        ExporterEx = isToKhaiNhap ? new NameBase() { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep } : new NameBase { Name = co.TenDiaChiNguoiXK, Identity = tkmd.MaDoanhNghiep },
        //        //        ExportationCountryEx = new LocationNameBase { Code = co.MaNuocXKTrenCO, Name = Company.KD.BLL.DuLieuChuan.Nuoc.GetName(co.MaNuocXKTrenCO) },

        //        //        ImporterEx = isToKhaiNhap ? new NameBase() { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep } :
        //        //                    new NameBase { Name = co.TenDiaChiNguoiNK, Identity = tkmd.MaDoanhNghiep },
        //        //        ImportationCountryEx = new LocationNameBase { Code = co.MaNuocNKTrenCO, Name = Company.KD.BLL.DuLieuChuan.Nuoc.GetName(co.MaNuocNKTrenCO) },

        //        //        LoadingLocation = new LoadingLocation { Name = "??", Code = "??", Loading = "??" },

        //        //        UnloadingLocation = new UnloadingLocation { Name = "??", Code = "??" },

        //        //        IsDebt = Helpers.FormatNumeric(co.NoCo),

        //        //        Submit = co.ThoiHanNop.ToString(formatDate),

        //        //        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = co.ThongTinMoTaChiTiet } },
        //        //        //Hang in Co
        //        //        GoodsItems = new List<GoodsItem>(),
        //        //    };
        //        //    //Điền hàng trong Co
        //        //    tokhai.CertificateOfOrigin.Add(certificateOfOrigin);
        //        //}
        //        #endregion CO
        //    }

        //    if (isToKhaiNhap && tkmd.VanTaiDon != null)
        //    #region BillOfLadings Vận đơn
        //    {
        //        tokhai.BillOfLadings = new List<BillOfLading>();
        //        Company.KDT.SHARE.QuanLyChungTu.VanDon vandon = tkmd.VanTaiDon;
        //        BillOfLading billOfLading = new BillOfLading()
        //        {
        //            Reference = vandon.SoVanDon,
        //            Issue = vandon.NgayVanDon.ToString(formatDate),
        //            IssueLocation = vandon.QuocTichPTVT
        //        };
        //        billOfLading.BorderTransportMeans = new BorderTransportMeans()
        //        {
        //            Identity = vandon.SoHieuPTVT,
        //            Identification = vandon.TenPTVT,
        //            Journey = vandon.SoHieuChuyenDi,
        //            ModeAndType = "001",
        //            Departure = vandon.NgayKhoiHanh.ToString(formatDate),
        //            RegistrationNationality = vandon.QuocTichPTVT
        //        };
        //        billOfLading.Carrier = new NameBase()
        //        {
        //            Name = vandon.TenHangVT,
        //            Identity = vandon.MaHangVT
        //        };

        //        billOfLading.Consignment = new Consignment()
        //        {
        //            Consignor = new NameBase()
        //            {
        //                Name = vandon.TenNguoiGiaoHang,
        //                Identity = vandon.MaNguoiGiaoHang
        //            },
        //            Consignee = new NameBase()
        //            {
        //                Name = vandon.TenNguoiNhanHang,
        //                Identity = vandon.MaNguoiNhanHang
        //            },
        //            NotifyParty = new NameBase()
        //            {
        //                Name = vandon.TenNguoiNhanHangTrungGian,
        //                Identity = vandon.MaNguoiNhanHangTrungGian
        //            },
        //            LoadingLocation = new LoadingLocation()
        //            {
        //                Name = vandon.TenCangXepHang,
        //                Code = vandon.MaCangXepHang,
        //                Loading = vandon.NgayKhoiHanh.ToString(formatDate)
        //            },
        //            UnloadingLocation = new UnloadingLocation()
        //            {
        //                Name = vandon.TenCangDoHang,
        //                Code = vandon.MaCangDoHang,
        //                Arrival = vandon.NgayDenPTVT.ToString(formatDate)
        //            },
        //            DeliveryDestination = new DeliveryDestination() { Line = "???" },

        //            //Bắt buộc nhập tổng số kiện vào loại kiện( Chưa có)
        //            ConsignmentItemPackaging = new Packaging()
        //            {
        //                Quantity = "0",
        //                Type = "2",
        //                MarkNumber = ""
        //            }
        //        };
        //        if (vandon.ContainerCollection != null && vandon.ContainerCollection.Count > 0)
        //            billOfLading.Consignment.TransportEquipments = new List<TransportEquipment>();

        //        foreach (Company.KDT.SHARE.QuanLyChungTu.Container container in vandon.ContainerCollection)
        //        {

        //            TransportEquipment transportEquipment = new TransportEquipment()
        //            {
        //                Characteristic = container.LoaiContainer,
        //                Fullness = Helpers.FormatNumeric(container.Trang_thai, 0),
        //                Seal = container.Seal_No
        //            };

        //            billOfLading.Consignment.TransportEquipments.Add(transportEquipment);
        //        }

        //        //billOfLading.Consignment.ConsignmentItem 

        //        tokhai.BillOfLadings.Add(billOfLading);

        //    }
        //    #endregion BillOfLadings  Vận đơn

        //    if (tkmd.listChuyenCuaKhau != null)
        //        #region Đề nghị chuyển cửa khẩu
        //        foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau in tkmd.listChuyenCuaKhau)
        //        {
        //            tokhai.CustomsOfficeChangedRequest.Add(new CustomsOfficeChangedRequest
        //            {
        //                AdditionalDocument = new AdditionalDocument { Reference = dnChuyenCuaKhau.SoVanDon, Issue = dnChuyenCuaKhau.NgayVanDon.ToString(formatDate) },
        //                AdditionalInformation = new AdditionalInformation
        //                {
        //                    Content = new Content { Text = dnChuyenCuaKhau.ThongTinKhac },
        //                    ExaminationPlace = dnChuyenCuaKhau.DiaDiemKiemTra,
        //                    Time = dnChuyenCuaKhau.ThoiGianDen.ToString(formatDate),
        //                    Route = dnChuyenCuaKhau.TuyenDuong
        //                },
        //            });
        //        };
        //        #endregion Đề nghị chuyển cửa khẩu

        //    if (tkmd.ChungTuKemCollection != null)
        //        #region Chứng từ đính kèm

        //        foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem in tkmd.ChungTuKemCollection)
        //        {
        //            tokhai.AttachDocumentItem.Add(new AttachDocumentItem
        //            {
        //                Issuer = "200",
        //                Sequence = Helpers.FormatNumeric(tkmd.ChungTuKemCollection.IndexOf(fileinChungtuDinhKem) + 1, 0),
        //                Issue = fileinChungtuDinhKem.NGAY_CT.ToString(formatDate),
        //                Reference = fileinChungtuDinhKem.SO_CT,
        //                Description = fileinChungtuDinhKem.DIENGIAI,
        //                AttachedFiles = new List<AttachedFile>(),
        //            });
        //            if (fileinChungtuDinhKem.listCTChiTiet != null && fileinChungtuDinhKem.listCTChiTiet.Count > 0)
        //                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet fileDetail in fileinChungtuDinhKem.listCTChiTiet)
        //                    tokhai.AttachDocumentItem[tkmd.ChungTuKemCollection.IndexOf(fileinChungtuDinhKem)].AttachedFiles.Add(new AttachedFile
        //                    {
        //                        FileName = fileDetail.FileName,
        //                        Content = new Content { Text = System.Convert.ToBase64String(fileDetail.NoiDung, Base64FormattingOptions.None), Base64 = "bin.base64" },
        //                    });
        //        };
        //        #endregion Chứng từ đính kèm

        //    #endregion Danh sách giấy phép XNK đi kèm
        //    return tokhai;
        //}
    }
}