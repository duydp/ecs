﻿IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB010')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Thuốc lá điếu, xì gà và các chế phẩm khác từ cây thuốc lá.' ,Notes=N'70' WHERE ReferenceDB='A528' AND Code='TB010'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB010',N'Thuốc lá điếu, xì gà và các chế phẩm khác từ cây thuốc lá.','70')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB020')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Rượu từ 20 độ trở lên' ,Notes=N'55' WHERE ReferenceDB='A528' AND Code='TB020'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB020',N'Rượu từ 20 độ trở lên','55')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB030')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Rượu dưới 20 độ.' ,Notes=N'30' WHERE ReferenceDB='A528' AND Code='TB030'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB030',N'Rượu dưới 20 độ.','30')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB040')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Bia' ,Notes=N'55' WHERE ReferenceDB='A528' AND Code='TB040'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB040',N'Bia','55')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB050')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chở người từ 09 chỗ trở xuống có dung tích xi lanh từ 2.000 cm3 trở xuống ; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.' ,Notes=N'45' WHERE ReferenceDB='A528' AND Code='TB050'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB050',N'Xe ô tô chở người từ 09 chỗ trở xuống có dung tích xi lanh từ 2.000 cm3 trở xuống ; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.','45')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB060')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 3.000 cm3 ; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.' ,Notes=N'50' WHERE ReferenceDB='A528' AND Code='TB060'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB060',N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 3.000 cm3 ; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.','50')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB070')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3 ; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.' ,Notes=N'60' WHERE ReferenceDB='A528' AND Code='TB070'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB070',N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3 ; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.','60')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB080')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chở người từ 10 đến dưới 16 chỗ, trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.' ,Notes=N'30' WHERE ReferenceDB='A528' AND Code='TB080'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB080',N'Xe ô tô chở người từ 10 đến dưới 16 chỗ, trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.','30')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB090')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chở người từ 16 đến dưới 24 chỗ, trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.' ,Notes=N'15' WHERE ReferenceDB='A528' AND Code='TB090'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB090',N'Xe ô tô chở người từ 16 đến dưới 24 chỗ, trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.','15')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB100')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô vừa chở người, vừa chở hàng; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.' ,Notes=N'15' WHERE ReferenceDB='A528' AND Code='TB100'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB100',N'Xe ô tô vừa chở người, vừa chở hàng; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12.','15')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB110')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 09 chỗ trở xuống có dung tích xi lanh từ 2.000 cm3 trở xuống.' ,Notes=N'31,5' WHERE ReferenceDB='A528' AND Code='TB110'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB110',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 09 chỗ trở xuống có dung tích xi lanh từ 2.000 cm3 trở xuống.','31,5')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB120')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 3.000 cm3.' ,Notes=N'35' WHERE ReferenceDB='A528' AND Code='TB120'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB120',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 3.000 cm3.','35')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB130')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3.' ,Notes=N'42' WHERE ReferenceDB='A528' AND Code='TB130'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB130',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3.','42')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB140')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 10 đến dưới 16 chỗ.' ,Notes=N'21' WHERE ReferenceDB='A528' AND Code='TB140'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB140',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 10 đến dưới 16 chỗ.','21')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB150')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 16 đến dưới 24 chỗ.' ,Notes=N'10,5' WHERE ReferenceDB='A528' AND Code='TB150'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB150',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 16 đến dưới 24 chỗ.','10,5')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB160')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô vừa chở người, vừa chở hàng.' ,Notes=N'10,5' WHERE ReferenceDB='A528' AND Code='TB160'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB160',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô vừa chở người, vừa chở hàng.','10,5')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB170')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 2.000 cm3 trở xuống.' ,Notes=N'22,5' WHERE ReferenceDB='A528' AND Code='TB170'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB170',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 2.000 cm3 trở xuống.','22,5')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB180')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 3.000 cm3.' ,Notes=N'25' WHERE ReferenceDB='A528' AND Code='TB180'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB180',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 3.000 cm3.','25')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB190')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3.' ,Notes=N'30' WHERE ReferenceDB='A528' AND Code='TB190'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB190',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3.','30')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB200')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 10 đến dưới 16 chỗ.' ,Notes=N'15' WHERE ReferenceDB='A528' AND Code='TB200'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB200',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 10 đến dưới 16 chỗ.','15')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB210')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 16 đến dưới 24 chỗ.' ,Notes=N'7,5' WHERE ReferenceDB='A528' AND Code='TB210'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB210',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 16 đến dưới 24 chỗ.','7,5')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB220')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô vừa chở người, vừa chở hàng.' ,Notes=N'7,5' WHERE ReferenceDB='A528' AND Code='TB220'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB220',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô vừa chở người, vừa chở hàng.','7,5')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB230')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng điện, loại chở người từ 9 chỗ trở xuống.' ,Notes=N'25' WHERE ReferenceDB='A528' AND Code='TB230'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB230',N'Xe ô tô chạy bằng điện, loại chở người từ 9 chỗ trở xuống.','25')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB240')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng điện, loại chở người từ 10 đến dưới 16 chỗ.' ,Notes=N'15' WHERE ReferenceDB='A528' AND Code='TB240'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB240',N'Xe ô tô chạy bằng điện, loại chở người từ 10 đến dưới 16 chỗ.','15')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB250')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng điện, loại chở người từ 16 đến dưới 24 chỗ.' ,Notes=N'10' WHERE ReferenceDB='A528' AND Code='TB250'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB250',N'Xe ô tô chạy bằng điện, loại chở người từ 16 đến dưới 24 chỗ.','10')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB260')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe ô tô chạy bằng điện, loại thiết kế vừa chở người, vừa chở hàng.' ,Notes=N'10' WHERE ReferenceDB='A528' AND Code='TB260'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB260',N'Xe ô tô chạy bằng điện, loại thiết kế vừa chở người, vừa chở hàng.','10')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB270')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xe mô tô hai bánh, xe mô tô ba bánh có dung tích xi lanh trên 125cm3.' ,Notes=N'20' WHERE ReferenceDB='A528' AND Code='TB270'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB270',N'Xe mô tô hai bánh, xe mô tô ba bánh có dung tích xi lanh trên 125cm3.','20')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB280')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tàu bay.' ,Notes=N'30' WHERE ReferenceDB='A528' AND Code='TB280'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB280',N'Tàu bay.','30')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB290')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Du thuyền.' ,Notes=N'30' WHERE ReferenceDB='A528' AND Code='TB290'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB290',N'Du thuyền.','30')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB300')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xăng các loại, nap-ta, chế phẩm tái hợp và các chế phẩm khác để pha chế xăng.' ,Notes=N'10' WHERE ReferenceDB='A528' AND Code='TB300'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB300',N'Xăng các loại, nap-ta, chế phẩm tái hợp và các chế phẩm khác để pha chế xăng.','10')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB310')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Điều hoà nhiệt độ công suất từ 90.000 BTU trở xuống.' ,Notes=N'10' WHERE ReferenceDB='A528' AND Code='TB310'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB310',N'Điều hoà nhiệt độ công suất từ 90.000 BTU trở xuống.','10')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB320')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Bài lá.' ,Notes=N'40' WHERE ReferenceDB='A528' AND Code='TB320'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB320',N'Bài lá.','40')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB330')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Vàng mã, hàng mã.' ,Notes=N'70' WHERE ReferenceDB='A528' AND Code='TB330'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB330',N'Vàng mã, hàng mã.','70')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB301')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xăng E5' ,Notes=N'8' WHERE ReferenceDB='A528' AND Code='TB301'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB301',N'Xăng E5','8')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB302')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xăng E10' ,Notes=N'7' WHERE ReferenceDB='A528' AND Code='TB302'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A522','TB302',N'Xăng E10','7')
END
GO 
UPDATE t_VNACC_Category_Common SET Name_VN=N'Miễn thuế BVMT' WHERE Code='M' AND ReferenceDB='A522'

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '22.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('22.8',GETDATE(), N'Cập nhật thuế và thu khác ')
END	