﻿//using Infragistics.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Infragistics.Excel;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    public partial class KiemTraDuLieuDMForm : Company.Interface.BaseForm
    {
        public KiemTraDuLieuDMForm()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dgList.DataSource = DinhMuc.GetDanhSachDinhMucSaiDuLieu(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, txtMaSP.Text.Trim()).Tables[0];
        }

        private void KiemTraDuLieuDMForm_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TenNPL"].Text == "" || e.Row.Cells["TenNPL"].Text == null)
                {
                    e.Row.Cells["Loi"].Text = setText("Nguyên phụ liệu này không có trong danh sách đã đăng ký","This Material does not exist in registered Material list");
                }
                if (e.Row.Cells["TenSP"].Text == "" || e.Row.Cells["TenSP"].Text == null)
                {
                    e.Row.Cells["Loi"].Text += setText("Sản phẩm này không có trong danh sách đã đăng ký", "This Product does not exist in registered Product list");
                }
            }
            
        }

       
    }
}

