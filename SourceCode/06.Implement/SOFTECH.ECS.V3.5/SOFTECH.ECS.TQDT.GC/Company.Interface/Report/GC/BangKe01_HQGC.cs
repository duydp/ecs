﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe01_HQGC : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;
        public BangKe01_HQGC()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt)
        {
            //dt.TableName = "BK01HQGC";
            ReportHeader.Visible = this.First;
            ReportFooter1.Visible = this.Last;
            ReportFooter.Visible = this.Last;
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = dt;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                }
            }
            lblMathang.Text = loaiSP;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;
            //string SL1, SL2, SL3, SL4;

            lblTK1.Text = dt.Columns[4].Caption;
            lblTK2.Text = dt.Columns[5].Caption;
            lblTK3.Text = dt.Columns[6].Caption;
            lblTK4.Text = dt.Columns[7].Caption;

            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL", "{0:N0}");
            lblLuong1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[4].ColumnName, "{0:N"+ GlobalSettings.SoThapPhan.LuongNPL +"}");
            lblLuong2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[5].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong3.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[6].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong4.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[7].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTong.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Tong", "{0:N"+ GlobalSettings.SoThapPhan.LuongNPL + "}");
            xrLabel19.Text = GlobalSettings.TieudeNgay;
        }

        private void lblLuong2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTK2.Text.Trim().Length == 0) lblLuong2.Text = "";
        }

        private void lblLuong3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTK3.Text.Trim().Length == 0) lblLuong3.Text = "";
        }

        private void lblLuong4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTK4.Text.Trim().Length == 0) lblLuong4.Text = "";
        }
    }
}
