﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
  public class TransportEquipment
    {
        [XmlElement("characteristic")]
        public string Characteristic { get; set; }
        [XmlElement("fullness")]
        public string Fullness { get; set; }
        [XmlElement("seal")]
        public string Seal { get; set; }
      /// <summary>
      /// XmlElement : EquipmentIdentification"
      /// </summary>
        [XmlElement("EquipmentIdentification")]
        public EquipmentIdentification EquipmentIdentifications { get; set; }
    
    }
}
