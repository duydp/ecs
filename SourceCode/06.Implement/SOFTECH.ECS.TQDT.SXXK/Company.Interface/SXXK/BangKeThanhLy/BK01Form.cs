using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK01Form : BaseForm
    {
        private BKToKhaiNhapCollection bkTKNLucDauCollection = new BKToKhaiNhapCollection();
        public BKToKhaiNhapCollection bkTKNCollection = new BKToKhaiNhapCollection();
        public ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        string UserName = "";
        public BK01Form()
        {
            InitializeComponent();
        }

        private void BK01Form_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgTKN.RootTable.Columns["NGAY_THN_THX"].DefaultValue = DateTime.Today;
                dgTKNTL.RootTable.Columns["NgayThucNhap"].DefaultValue = DateTime.Today;

                this.UserName = MainForm.EcsQuanTri.Identity.Name;
                if (this.HSTL.TrangThaiThanhKhoan == 401 || this.HSTL.SoTiepNhan == 1)
                {
                    uiButton2.Enabled = btnDelete.Enabled = false;
                    cmdSave.Enabled = false;
                    dgTKNTL.AllowDelete = InheritableBoolean.False;
                }
                int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
                DateTime fromDate = ccFromDate.Value;
                DateTime toDate = ccToDate.Value;
                if (GlobalSettings.TKhoanKCX == 0)// Nếu ko fải KCX thì không lấy TKN sau ngày TKX
                {
                    if (toDate > this.getMaxDateBKTKX().AddDays(0 - chenhLech)) toDate = this.getMaxDateBKTKX().AddDays(0 - chenhLech);
                }

                fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
                toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

                //Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
                this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKNChuaThanhLyByDate_PhanLuong(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate, UserName, txtTenChuHang.Text);

                int i = this.HSTL.getBKToKhaiNhap();
                if (i >= 0)
                {
                    this.bkTKNCollection = this.HSTL.BKCollection[i].bkTKNCollection;
                    foreach (BKToKhaiNhap bk in this.bkTKNCollection)
                    {
                        this.RemoveTKMD(bk.SoToKhai, bk.MaLoaiHinh, bk.NamDangKy, bk.MaHaiQuan);
                    }
                }
                this.dgTKNTL.DataSource = this.bkTKNCollection;
                this.dgTKN.DataSource = this.tkmdCollection;
                this.bkTKNLucDauCollection = (BKToKhaiNhapCollection)this.bkTKNCollection.Clone();
            }
            catch (Exception ex) { throw ex; }
            finally { this.Cursor = Cursors.Default; }
        }

        private DateTime getMaxDateBKTKX()
        {
            int i = this.HSTL.getBKToKhaiXuat();
            this.HSTL.BKCollection[i].LoadChiTietBangKe();
            BKToKhaiXuatCollection bkTKXCollection = this.HSTL.BKCollection[i].bkTKXColletion;
            DateTime dt = bkTKXCollection[0].NgayDangKy;
            if (bkTKXCollection[0].NgayThucXuat != null && bkTKXCollection[0].NgayThucXuat.Year > 1900)
                dt = bkTKXCollection[0].NgayThucXuat;

            for (int j = 1; j < bkTKXCollection.Count; j++)
            {
                DateTime dt2 = bkTKXCollection[j].NgayDangKy;
                if (bkTKXCollection[j].NgayThucXuat != null && bkTKXCollection[j].NgayThucXuat.Year > 1900)
                    dt2 = bkTKXCollection[j].NgayThucXuat;
                if (dt < dt2) dt = dt2;
            }
            return dt;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            ToKhaiMauDichCollection tkmds = new ToKhaiMauDichCollection();
            foreach (GridEXRow row in dgTKN.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    ToKhaiMauDich tkmd = (ToKhaiMauDich)row.DataRow;
                    BKToKhaiNhap bk = new BKToKhaiNhap();
                    bk.MaHaiQuan = tkmd.MaHaiQuan;
                    bk.MaLoaiHinh = tkmd.MaLoaiHinh;
                    bk.NamDangKy = tkmd.NamDangKy;
                    bk.NgayDangKy = tkmd.NgayDangKy;
                    bk.NgayThucNhap = tkmd.NGAY_THN_THX;
                    bk.NgayHoanThanh = tkmd.NgayHoanThanh;
                    bk.SoToKhai = tkmd.SoToKhai;
                    bk.UserName = MainForm.EcsQuanTri.Identity.Name;
                    this.bkTKNCollection.Add(bk);
                    tkmds.Add(tkmd);
                }
            }
            foreach (ToKhaiMauDich tkmd in tkmds)
                this.RemoveTKMD(tkmd);
            try
            {
                dgTKN.Refetch();
            }
            catch
            {
                dgTKN.Refresh();
            }
            try
            {
                dgTKNTL.Refetch();
            }
            catch
            {
                dgTKNTL.Refresh();
            }
        }


        private void RemoveTKMD(ToKhaiMauDich tkmd)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == tkmd.SoToKhai && this.tkmdCollection[i].MaLoaiHinh == tkmd.MaLoaiHinh && this.tkmdCollection[i].NamDangKy == tkmd.NamDangKy && this.tkmdCollection[i].MaHaiQuan == tkmd.MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }
        private void RemoveTKMD(int SoToKhai, string MaLoaiHinh, short NamDangKy, string MaHaiQuan)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == SoToKhai && this.tkmdCollection[i].MaLoaiHinh == MaLoaiHinh && this.tkmdCollection[i].NamDangKy == NamDangKy && this.tkmdCollection[i].MaHaiQuan == MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }
        private void RemoveBKTKN(BKToKhaiNhap bk)
        {
            for (int i = 0; i < this.bkTKNCollection.Count; i++)
            {
                if (this.bkTKNCollection[i].SoToKhai == bk.SoToKhai && this.bkTKNCollection[i].MaLoaiHinh == bk.MaLoaiHinh && this.bkTKNCollection[i].NamDangKy == bk.NamDangKy && this.bkTKNCollection[i].MaHaiQuan == bk.MaHaiQuan)
                {
                    this.bkTKNCollection.RemoveAt(i);
                    break;
                }
            }
        }

        private void dgTKN_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }

        private void dgTKNTL_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }
        public int KiemTraTruocKhiLuu()
        {
            BKToKhaiNhapCollection bktknCollection1 = new BKToKhaiNhap().SelectCollectionDynamic("UserName != '" + MainForm.EcsQuanTri.Identity.Name + "'", "");
            BKToKhaiNhapCollection bktknCollection2 = this.bkTKNCollection;
            foreach (BKToKhaiNhap bk2 in bktknCollection2)
            {
                foreach (BKToKhaiNhap bk1 in bktknCollection1)
                {
                    if (bk2.SoToKhai == bk1.SoToKhai && bk2.MaLoaiHinh == bk1.MaLoaiHinh && bk2.NamDangKy == bk1.NamDangKy && bk2.MaHaiQuan == bk1.MaHaiQuan) return bk2.SoToKhai;

                }

            }
            return 0;
        }
        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.bkTKNCollection.Count == 0)
                {
                    // ShowMessage("Danh sách tờ khai nhập cần thanh lý chưa có!", false);
                    MLMessages("Danh sách tờ khai nhập cần thanh lý chưa có!", "MSG_THK78", "", false);
                    return;
                }
                //int k = KiemTraTruocKhiLuu();
                //if (k > 0)
                //{
                //    if(ShowMessage("Tờ khai nhập số " + k + " đã thuộc về bộ hồ sơ của người dùng khác.", true)!= "Yes")
                //        return;

                //}
                this.Cursor = Cursors.WaitCursor;

                this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].bkTKNCollection = this.bkTKNCollection;
                foreach (BKToKhaiNhap bk in this.bkTKNLucDauCollection)
                {
                    if (!this.bkTKNCollection.Contains(bk)) bk.DeleteFull();
                }
                this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].InsertUpdate_BKTKN(this.bkTKNCollection);
                this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgTKNTL_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {

            if (e.Row.RowType == RowType.Record)
            {
                // if (ShowMessage("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
                if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
                {
                    BKToKhaiNhap bk = (BKToKhaiNhap)e.Row.DataRow;
                    //bk.DeleteFull();
                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.MaHaiQuan = bk.MaHaiQuan;
                    tkmd.SoToKhai = bk.SoToKhai;
                    tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                    tkmd.NgayDangKy = bk.NgayDangKy;
                    tkmd.NamDangKy = bk.NamDangKy;
                    tkmd.NGAY_THN_THX = bk.NgayThucNhap;
                    this.tkmdCollection.Add(tkmd);
                    dgTKN.Refetch();
                }
                else return;
            }

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
            DateTime fromDate = ccFromDate.Value;
            DateTime toDate = ccToDate.Value;
            if (fromDate > toDate)
            {
                MLMessages("Từ ngày phải nhỏ hơn hoặc bằng đến ngày.", "MSG_SEA01", "", false);
                return;
            }

            if (toDate > this.getMaxDateBKTKX().AddDays(0 - chenhLech)) toDate = this.getMaxDateBKTKX().AddDays(0 - chenhLech);

            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

            //Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
            this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKNChuaThanhLyByDate_PhanLuong(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate, UserName, txtTenChuHang.Text);

            foreach (BKToKhaiNhap bk in this.bkTKNCollection)
            {
                this.RemoveTKMD(bk.SoToKhai, bk.MaLoaiHinh, bk.NamDangKy, bk.MaHaiQuan);
            }
            dgTKN.DataSource = this.tkmdCollection;
            try
            {
                dgTKN.Refetch();
            }
            catch
            {
                dgTKN.Refresh();
            }
        }

        private void dgTKNTL_DeletingRecords(object sender, CancelEventArgs e)
        {
            // if (ShowMessage("Khi bạn xóa các tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
            if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
            {
                this.Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = dgTKNTL.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {

                    if (i.RowType == RowType.Record)
                    {
                        BKToKhaiNhap bk = (BKToKhaiNhap)i.GetRow().DataRow;
                        //if(bk.ID>0)
                        //    bk.DeleteFull();
                        ToKhaiMauDich tkmd = new ToKhaiMauDich();
                        tkmd.MaHaiQuan = bk.MaHaiQuan;
                        tkmd.SoToKhai = bk.SoToKhai;
                        tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                        tkmd.NgayDangKy = bk.NgayDangKy;
                        tkmd.NamDangKy = bk.NamDangKy;
                        tkmd.NGAY_THN_THX = bk.NgayThucNhap;
                        this.tkmdCollection.Add(tkmd);
                    }
                }
                try
                {
                    dgTKN.Refetch();
                }
                catch
                {
                    dgTKN.Refresh();
                }
                this.Cursor = Cursors.Default;
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // if (ShowMessage("Khi bạn xóa các tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
            if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
            {
                this.Cursor = Cursors.WaitCursor;
                BKToKhaiNhapCollection bks = new BKToKhaiNhapCollection();
                foreach (GridEXRow row in dgTKNTL.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {
                        BKToKhaiNhap bk = (BKToKhaiNhap)row.DataRow;
                        //if (bk.ID > 0) bk.DeleteFull();
                        ToKhaiMauDich tkmd = new ToKhaiMauDich();
                        tkmd.MaHaiQuan = bk.MaHaiQuan;
                        tkmd.SoToKhai = bk.SoToKhai;
                        tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                        tkmd.NgayDangKy = bk.NgayDangKy;
                        tkmd.NamDangKy = bk.NamDangKy;
                        tkmd.NGAY_THN_THX = bk.NgayThucNhap;
                        tkmd.NgayHoanThanh = Convert.ToDateTime(row.Cells["NgayHoanThanh"].Value);
                        this.tkmdCollection.Add(tkmd);
                        bks.Add(bk);
                    }
                }
                foreach (BKToKhaiNhap bk in bks)
                    this.RemoveBKTKN(bk);
                try
                {
                    dgTKNTL.Refetch();
                }
                catch
                {
                    dgTKNTL.Refresh();
                }
                try
                {
                    dgTKN.Refetch();
                }
                catch
                {
                    dgTKN.Refresh();
                }

                this.Cursor = Cursors.Default;
            }
        }

        private void BK01Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.bkTKNCollection = this.bkTKNLucDauCollection;
            this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].bkTKNCollection = this.bkTKNCollection;

        }

        private void dgTKN_LoadingRow(object sender, RowLoadEventArgs e)
        {

            if (e.Row.RowType == RowType.Record)
            {
                if (Convert.ToDateTime(e.Row.Cells["Ngay_THN_THX"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["Ngay_THN_THX"].Text = "";
                if (Convert.ToDateTime(e.Row.Cells["NgayHoanThanh"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayHoanThanh"].Text = "";

                if (e.Row.Cells["PhanLuong"].Value != null)
                {
                    if (e.Row.Cells["PhanLuong"].Value.ToString() == "1")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Green;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.White;
                    }
                    else if (e.Row.Cells["PhanLuong"].Value.ToString() == "2")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Yellow;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.Red;
                    }
                    else if (e.Row.Cells["PhanLuong"].Value.ToString() == "3")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Red;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.White;
                    }

                    e.Row.Cells["PhanLuong"].Text = Company.KDT.SHARE.Components.Globals.GetPhanLuong(e.Row.Cells["PhanLuong"].Text);
                    e.Row.Cells["SoToKhai"].ToolTipText = Company.KDT.SHARE.Components.Globals.GetPhanLuong(e.Row.Cells["PhanLuong"].Text);
                }
            }
        }

        private void dgTKNTL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (Convert.ToDateTime(e.Row.Cells["NgayThucNhap"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayThucNhap"].Text = "";
                if (Convert.ToDateTime(e.Row.Cells["NgayHoanThanh"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayHoanThanh"].Text = "";

                if (e.Row.Cells["PhanLuong"].Value != null)
                {
                    if (e.Row.Cells["PhanLuong"].Value.ToString() == "1")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Green;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.White;
                    }
                    else if (e.Row.Cells["PhanLuong"].Value.ToString() == "2")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Yellow;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.Red;
                    }
                    else if (e.Row.Cells["PhanLuong"].Value.ToString() == "3")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Red;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.White;
                    }

                    e.Row.Cells["PhanLuong"].Text = Company.KDT.SHARE.Components.Globals.GetPhanLuong(e.Row.Cells["PhanLuong"].Text);
                    e.Row.Cells["SoToKhai"].ToolTipText = Company.KDT.SHARE.Components.Globals.GetPhanLuong(e.Row.Cells["PhanLuong"].Text);
                }
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {

        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }


        private void dgTKN_RecordUpdated(object sender, EventArgs e)
        {
            GridEXRow row = dgTKN.GetRow();
            ToKhaiMauDich TKMD = (ToKhaiMauDich)row.DataRow;
            DateTime temp = TKMD.NGAY_THN_THX;
            TKMD.Load();
            TKMD.NGAY_THN_THX = temp;
            TKMD.Update();
        }

        private void dgTKNTL_RecordUpdated(object sender, EventArgs e)
        {
            GridEXRow row = dgTKNTL.GetRow();
            BKToKhaiNhap bk = (BKToKhaiNhap)row.DataRow;
            bk.Update();
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.SoToKhai = bk.SoToKhai;
            TKMD.MaLoaiHinh = bk.MaLoaiHinh;
            TKMD.MaHaiQuan = bk.MaHaiQuan;
            TKMD.NamDangKy = bk.NamDangKy;
            TKMD.Load();
            TKMD.NGAY_THN_THX = bk.NgayThucNhap;
            TKMD.Update();
        }

        private void btnGoTo_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTKN.FindAll(dgTKN.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, Convert.ToInt64(txtSoToKhai.Value)) > 0)
                {

                    foreach (GridEXSelectedItem item in dgTKN.SelectedItems)
                        item.GetRow().IsChecked = true;
                }
                else
                {
                    MLMessages("Không có tờ khai này trong hệ thống", "MSG_THK79", "", false);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        
        private void btnGoToTL_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTKNTL.FindAll(dgTKNTL.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, Convert.ToInt64(txtSoToKhaiTL.Value)) > 0)
                {

                    foreach (GridEXSelectedItem item in dgTKNTL.SelectedItems)
                        item.GetRow().IsChecked = true;
                }
                else
                {
                    MLMessages("Không có tờ khai này trong danh sách", "MSG_THK79", "", false);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "BangKeToKhaiNhapDuaVaoThanhKhoan_" + HSTL.LanThanhLy + ".xls";
                sfNPL.Filter = "Excel files| *.xls";

                if (sfNPL.ShowDialog() == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgTKNTL;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    ImportHSTKTKNForm tkn = new ImportHSTKTKNForm();
                    tkn.maLoaiHinh = "N";
                    tkn.ShowDialog();

                    Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection BKTKNCollectionTemp = tkn.TKMDReadCollection;
                    string msgTKNo = "";
                    int sumTKN = 0;

                    foreach (Company.BLL.SXXK.ToKhai.ToKhaiMauDich tk in BKTKNCollectionTemp)
                    {
                        if (dgTKN.FindAll(dgTKN.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, tk.SoToKhai) > 0)
                        {
                            foreach (GridEXSelectedItem item in dgTKN.SelectedItems)
                                item.GetRow().IsChecked = true;
                        }
                        else if (dgTKNTL.FindAll(dgTKNTL.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, tk.SoToKhai) > 0)
                        {

                        }
                        else
                        {
                            sumTKN += 1;
                            msgTKNo += "Tờ khai số: " + tk.SoToKhai + ", Mã loại hình: " + tk.MaLoaiHinh + ", Ngày đăng ký: " + tk.NgayDangKy + "\n";
                        }
                    }

                    if (msgTKNo != "")
                        MLMessages("Có " + sumTKN + " tờ khai không có trong hệ thống\r\n" + msgTKNo, "MSG_THK79", "", false);
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); MessageBox.Show(ex.Message); }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}
