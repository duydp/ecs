﻿namespace SOFTECH.ECS.TQDT.SXXK.KDT.SXXK
{
    partial class NhomCuaKhauForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout gridFrom_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridTo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NhomCuaKhauForm));
            this.btnOK = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.btnRemove = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.gridFrom = new Janus.Windows.GridEX.GridEX();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.gridTo = new Janus.Windows.GridEX.GridEX();
            this.chAll = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTo)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnAdd);
            this.grbMain.Controls.Add(this.btnRemove);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.btnOK);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Size = new System.Drawing.Size(690, 457);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnOK.Icon")));
            this.btnOK.Location = new System.Drawing.Point(514, 427);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(70, 23);
            this.btnOK.TabIndex = 14;
            this.btnOK.Text = "Đồng ý";
            this.btnOK.VisualStyleManager = this.vsmMain;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(590, 427);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Chi cục:";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAdd.Icon")));
            this.btnAdd.Location = new System.Drawing.Point(337, 205);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(26, 25);
            this.btnAdd.TabIndex = 24;
            this.btnAdd.VisualStyleManager = this.vsmMain;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Icon = ((System.Drawing.Icon)(resources.GetObject("btnRemove.Icon")));
            this.btnRemove.Location = new System.Drawing.Point(337, 234);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(26, 25);
            this.btnRemove.TabIndex = 25;
            this.btnRemove.VisualStyleManager = this.vsmMain;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.chAll);
            this.uiGroupBox4.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(12, 3);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(675, 54);
            this.uiGroupBox4.TabIndex = 26;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(72, 20);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(293, 22);
            this.donViHaiQuanControl1.TabIndex = 19;
            this.donViHaiQuanControl1.VisualStyleManager = this.vsmMain;
            this.donViHaiQuanControl1.ValueChanged += new Company.Interface.Controls.DonViHaiQuanControl.ValueChangedEventHandler(this.donViHaiQuanControl1_ValueChanged);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.gridFrom);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 92);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(315, 323);
            this.uiGroupBox1.TabIndex = 27;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // gridFrom
            // 
            this.gridFrom.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridFrom.AlternatingColors = true;
            this.gridFrom.AutomaticSort = false;
            this.gridFrom.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.gridFrom.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.gridFrom.ColumnAutoResize = true;
            gridFrom_DesignTimeLayout.LayoutString = resources.GetString("gridFrom_DesignTimeLayout.LayoutString");
            this.gridFrom.DesignTimeLayout = gridFrom_DesignTimeLayout;
            this.gridFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridFrom.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic;
            this.gridFrom.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridFrom.GroupByBoxVisible = false;
            this.gridFrom.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridFrom.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridFrom.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridFrom.Location = new System.Drawing.Point(3, 8);
            this.gridFrom.Name = "gridFrom";
            this.gridFrom.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridFrom.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridFrom.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridFrom.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.gridFrom.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridFrom.Size = new System.Drawing.Size(309, 312);
            this.gridFrom.TabIndex = 26;
            this.gridFrom.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.gridFrom.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(11, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 14);
            this.label1.TabIndex = 28;
            this.label1.Text = "Danh sách cửa khẩu chưa xếp nhóm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(373, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(214, 14);
            this.label2.TabIndex = 29;
            this.label2.Text = "Danh sách cửa khẩu đã xếp nhóm";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.gridTo);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(373, 89);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(291, 329);
            this.uiGroupBox2.TabIndex = 28;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // gridTo
            // 
            this.gridTo.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridTo.AlternatingColors = true;
            this.gridTo.AutomaticSort = false;
            this.gridTo.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.gridTo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.gridTo.ColumnAutoResize = true;
            gridTo_DesignTimeLayout.LayoutString = resources.GetString("gridTo_DesignTimeLayout.LayoutString");
            this.gridTo.DesignTimeLayout = gridTo_DesignTimeLayout;
            this.gridTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTo.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic;
            this.gridTo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridTo.GroupByBoxVisible = false;
            this.gridTo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridTo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridTo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridTo.Location = new System.Drawing.Point(3, 8);
            this.gridTo.Name = "gridTo";
            this.gridTo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridTo.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridTo.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridTo.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.gridTo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridTo.Size = new System.Drawing.Size(285, 318);
            this.gridTo.TabIndex = 27;
            this.gridTo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.gridTo.VisualStyleManager = this.vsmMain;
            // 
            // chAll
            // 
            this.chAll.AutoSize = true;
            this.chAll.Location = new System.Drawing.Point(371, 23);
            this.chAll.Name = "chAll";
            this.chAll.Size = new System.Drawing.Size(282, 17);
            this.chAll.TabIndex = 20;
            this.chAll.Text = "Hiển thị hết danh sách cửa khẩu đã xếp nhóm";
            this.chAll.UseVisualStyleBackColor = true;
            this.chAll.CheckedChanged += new System.EventHandler(this.chAll_CheckedChanged);
            // 
            // NhomCuaKhauForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 457);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "NhomCuaKhauForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Nhóm cửa khẩu";
            this.Load += new System.EventHandler(this.NhomCuaKhauForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnOK;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.EditControls.UIButton btnRemove;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX gridFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX gridTo;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private System.Windows.Forms.CheckBox chAll;
    }
}