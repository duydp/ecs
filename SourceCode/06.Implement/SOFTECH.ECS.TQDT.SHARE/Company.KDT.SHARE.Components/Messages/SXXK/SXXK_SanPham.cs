﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class SXXK_SanPham:DeclarationBase
    {
        /// <summary>
        /// Mặt hàng
        /// </summary>
        [XmlElement("Product")]
        public List<Product> Product { get; set; }
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }

    }
}
