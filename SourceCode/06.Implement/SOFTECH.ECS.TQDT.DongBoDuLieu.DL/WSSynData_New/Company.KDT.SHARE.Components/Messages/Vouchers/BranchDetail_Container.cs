﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
  public  class BranchDetail_Container
    {
        /// <summary>
        /// Số container
        /// </summary>
        [XmlElement("container")]
        public string Container { get; set; }

        /// <summary>
        /// Số seal
        /// </summary>
        [XmlElement("seal")]
        public string Seal { get; set; }
    }
}
