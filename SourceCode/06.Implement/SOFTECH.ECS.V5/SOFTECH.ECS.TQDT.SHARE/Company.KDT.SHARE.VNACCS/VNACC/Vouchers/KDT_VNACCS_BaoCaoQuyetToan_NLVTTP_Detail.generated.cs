﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long GoodItems_ID { set; get; }
		public decimal STT { set; get; }
		public decimal LoaiHangHoa { set; get; }
		public decimal TaiKhoan { set; get; }
		public string TenHangHoa { set; get; }
		public string MaHangHoa { set; get; }
		public string DVT { set; get; }
		public decimal LuongNLVTXuatKhau { set; get; }
		public decimal LuongNLVTTon { set; get; }
		public decimal LuongSPGCNhapKhau { set; get; }
		public decimal LuongSPGCBan { set; get; }
		public decimal LuongTieuHuy { set; get; }
		public decimal LuongBan { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> collection = new List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail>();
			while (reader.Read())
			{
				KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GoodItems_ID"))) entity.GoodItems_ID = reader.GetInt64(reader.GetOrdinal("GoodItems_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetDecimal(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetDecimal(reader.GetOrdinal("LoaiHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("TaiKhoan"))) entity.TaiKhoan = reader.GetDecimal(reader.GetOrdinal("TaiKhoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangHoa"))) entity.TenHangHoa = reader.GetString(reader.GetOrdinal("TenHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangHoa"))) entity.MaHangHoa = reader.GetString(reader.GetOrdinal("MaHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNLVTXuatKhau"))) entity.LuongNLVTXuatKhau = reader.GetDecimal(reader.GetOrdinal("LuongNLVTXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNLVTTon"))) entity.LuongNLVTTon = reader.GetDecimal(reader.GetOrdinal("LuongNLVTTon"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongSPGCNhapKhau"))) entity.LuongSPGCNhapKhau = reader.GetDecimal(reader.GetOrdinal("LuongSPGCNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongSPGCBan"))) entity.LuongSPGCBan = reader.GetDecimal(reader.GetOrdinal("LuongSPGCBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTieuHuy"))) entity.LuongTieuHuy = reader.GetDecimal(reader.GetOrdinal("LuongTieuHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongBan"))) entity.LuongBan = reader.GetDecimal(reader.GetOrdinal("LuongBan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> collection, long id)
        {
            foreach (KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details VALUES(@GoodItems_ID, @STT, @LoaiHangHoa, @TaiKhoan, @TenHangHoa, @MaHangHoa, @DVT, @LuongNLVTXuatKhau, @LuongNLVTTon, @LuongSPGCNhapKhau, @LuongSPGCBan, @LuongTieuHuy, @LuongBan)";
            string update = "UPDATE t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details SET GoodItems_ID = @GoodItems_ID, STT = @STT, LoaiHangHoa = @LoaiHangHoa, TaiKhoan = @TaiKhoan, TenHangHoa = @TenHangHoa, MaHangHoa = @MaHangHoa, DVT = @DVT, LuongNLVTXuatKhau = @LuongNLVTXuatKhau, LuongNLVTTon = @LuongNLVTTon, LuongSPGCNhapKhau = @LuongSPGCNhapKhau, LuongSPGCBan = @LuongSPGCBan, LuongTieuHuy = @LuongTieuHuy, LuongBan = @LuongBan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GoodItems_ID", SqlDbType.BigInt, "GoodItems_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangHoa", SqlDbType.Decimal, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TaiKhoan", SqlDbType.Decimal, "TaiKhoan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNLVTXuatKhau", SqlDbType.Decimal, "LuongNLVTXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNLVTTon", SqlDbType.Decimal, "LuongNLVTTon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSPGCNhapKhau", SqlDbType.Decimal, "LuongSPGCNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSPGCBan", SqlDbType.Decimal, "LuongSPGCBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTieuHuy", SqlDbType.Decimal, "LuongTieuHuy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongBan", SqlDbType.Decimal, "LuongBan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GoodItems_ID", SqlDbType.BigInt, "GoodItems_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangHoa", SqlDbType.Decimal, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TaiKhoan", SqlDbType.Decimal, "TaiKhoan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNLVTXuatKhau", SqlDbType.Decimal, "LuongNLVTXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNLVTTon", SqlDbType.Decimal, "LuongNLVTTon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSPGCNhapKhau", SqlDbType.Decimal, "LuongSPGCNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSPGCBan", SqlDbType.Decimal, "LuongSPGCBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTieuHuy", SqlDbType.Decimal, "LuongTieuHuy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongBan", SqlDbType.Decimal, "LuongBan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details VALUES(@GoodItems_ID, @STT, @LoaiHangHoa, @TaiKhoan, @TenHangHoa, @MaHangHoa, @DVT, @LuongNLVTXuatKhau, @LuongNLVTTon, @LuongSPGCNhapKhau, @LuongSPGCBan, @LuongTieuHuy, @LuongBan)";
            string update = "UPDATE t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details SET GoodItems_ID = @GoodItems_ID, STT = @STT, LoaiHangHoa = @LoaiHangHoa, TaiKhoan = @TaiKhoan, TenHangHoa = @TenHangHoa, MaHangHoa = @MaHangHoa, DVT = @DVT, LuongNLVTXuatKhau = @LuongNLVTXuatKhau, LuongNLVTTon = @LuongNLVTTon, LuongSPGCNhapKhau = @LuongSPGCNhapKhau, LuongSPGCBan = @LuongSPGCBan, LuongTieuHuy = @LuongTieuHuy, LuongBan = @LuongBan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GoodItems_ID", SqlDbType.BigInt, "GoodItems_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangHoa", SqlDbType.Decimal, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TaiKhoan", SqlDbType.Decimal, "TaiKhoan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNLVTXuatKhau", SqlDbType.Decimal, "LuongNLVTXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNLVTTon", SqlDbType.Decimal, "LuongNLVTTon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSPGCNhapKhau", SqlDbType.Decimal, "LuongSPGCNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSPGCBan", SqlDbType.Decimal, "LuongSPGCBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTieuHuy", SqlDbType.Decimal, "LuongTieuHuy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongBan", SqlDbType.Decimal, "LuongBan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GoodItems_ID", SqlDbType.BigInt, "GoodItems_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangHoa", SqlDbType.Decimal, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TaiKhoan", SqlDbType.Decimal, "TaiKhoan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNLVTXuatKhau", SqlDbType.Decimal, "LuongNLVTXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNLVTTon", SqlDbType.Decimal, "LuongNLVTTon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSPGCNhapKhau", SqlDbType.Decimal, "LuongSPGCNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSPGCBan", SqlDbType.Decimal, "LuongSPGCBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTieuHuy", SqlDbType.Decimal, "LuongTieuHuy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongBan", SqlDbType.Decimal, "LuongBan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> SelectCollectionBy_GoodItems_ID(long goodItems_ID)
		{
            IDataReader reader = SelectReaderBy_GoodItems_ID(goodItems_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_GoodItems_ID(long goodItems_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectBy_GoodItems_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GoodItems_ID", SqlDbType.BigInt, goodItems_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_GoodItems_ID(long goodItems_ID)
		{
			const string spName = "p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectBy_GoodItems_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GoodItems_ID", SqlDbType.BigInt, goodItems_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail(long goodItems_ID, decimal sTT, decimal loaiHangHoa, decimal taiKhoan, string tenHangHoa, string maHangHoa, string dVT, decimal luongNLVTXuatKhau, decimal luongNLVTTon, decimal luongSPGCNhapKhau, decimal luongSPGCBan, decimal luongTieuHuy, decimal luongBan)
		{
			KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail();	
			entity.GoodItems_ID = goodItems_ID;
			entity.STT = sTT;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.TaiKhoan = taiKhoan;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.DVT = dVT;
			entity.LuongNLVTXuatKhau = luongNLVTXuatKhau;
			entity.LuongNLVTTon = luongNLVTTon;
			entity.LuongSPGCNhapKhau = luongSPGCNhapKhau;
			entity.LuongSPGCBan = luongSPGCBan;
			entity.LuongTieuHuy = luongTieuHuy;
			entity.LuongBan = luongBan;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GoodItems_ID", SqlDbType.BigInt, GoodItems_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Decimal, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@TaiKhoan", SqlDbType.Decimal, TaiKhoan);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LuongNLVTXuatKhau", SqlDbType.Decimal, LuongNLVTXuatKhau);
			db.AddInParameter(dbCommand, "@LuongNLVTTon", SqlDbType.Decimal, LuongNLVTTon);
			db.AddInParameter(dbCommand, "@LuongSPGCNhapKhau", SqlDbType.Decimal, LuongSPGCNhapKhau);
			db.AddInParameter(dbCommand, "@LuongSPGCBan", SqlDbType.Decimal, LuongSPGCBan);
			db.AddInParameter(dbCommand, "@LuongTieuHuy", SqlDbType.Decimal, LuongTieuHuy);
			db.AddInParameter(dbCommand, "@LuongBan", SqlDbType.Decimal, LuongBan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail(long id, long goodItems_ID, decimal sTT, decimal loaiHangHoa, decimal taiKhoan, string tenHangHoa, string maHangHoa, string dVT, decimal luongNLVTXuatKhau, decimal luongNLVTTon, decimal luongSPGCNhapKhau, decimal luongSPGCBan, decimal luongTieuHuy, decimal luongBan)
		{
			KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail();			
			entity.ID = id;
			entity.GoodItems_ID = goodItems_ID;
			entity.STT = sTT;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.TaiKhoan = taiKhoan;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.DVT = dVT;
			entity.LuongNLVTXuatKhau = luongNLVTXuatKhau;
			entity.LuongNLVTTon = luongNLVTTon;
			entity.LuongSPGCNhapKhau = luongSPGCNhapKhau;
			entity.LuongSPGCBan = luongSPGCBan;
			entity.LuongTieuHuy = luongTieuHuy;
			entity.LuongBan = luongBan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GoodItems_ID", SqlDbType.BigInt, GoodItems_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Decimal, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@TaiKhoan", SqlDbType.Decimal, TaiKhoan);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LuongNLVTXuatKhau", SqlDbType.Decimal, LuongNLVTXuatKhau);
			db.AddInParameter(dbCommand, "@LuongNLVTTon", SqlDbType.Decimal, LuongNLVTTon);
			db.AddInParameter(dbCommand, "@LuongSPGCNhapKhau", SqlDbType.Decimal, LuongSPGCNhapKhau);
			db.AddInParameter(dbCommand, "@LuongSPGCBan", SqlDbType.Decimal, LuongSPGCBan);
			db.AddInParameter(dbCommand, "@LuongTieuHuy", SqlDbType.Decimal, LuongTieuHuy);
			db.AddInParameter(dbCommand, "@LuongBan", SqlDbType.Decimal, LuongBan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail(long id, long goodItems_ID, decimal sTT, decimal loaiHangHoa, decimal taiKhoan, string tenHangHoa, string maHangHoa, string dVT, decimal luongNLVTXuatKhau, decimal luongNLVTTon, decimal luongSPGCNhapKhau, decimal luongSPGCBan, decimal luongTieuHuy, decimal luongBan)
		{
			KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail();			
			entity.ID = id;
			entity.GoodItems_ID = goodItems_ID;
			entity.STT = sTT;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.TaiKhoan = taiKhoan;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.DVT = dVT;
			entity.LuongNLVTXuatKhau = luongNLVTXuatKhau;
			entity.LuongNLVTTon = luongNLVTTon;
			entity.LuongSPGCNhapKhau = luongSPGCNhapKhau;
			entity.LuongSPGCBan = luongSPGCBan;
			entity.LuongTieuHuy = luongTieuHuy;
			entity.LuongBan = luongBan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GoodItems_ID", SqlDbType.BigInt, GoodItems_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Decimal, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@TaiKhoan", SqlDbType.Decimal, TaiKhoan);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LuongNLVTXuatKhau", SqlDbType.Decimal, LuongNLVTXuatKhau);
			db.AddInParameter(dbCommand, "@LuongNLVTTon", SqlDbType.Decimal, LuongNLVTTon);
			db.AddInParameter(dbCommand, "@LuongSPGCNhapKhau", SqlDbType.Decimal, LuongSPGCNhapKhau);
			db.AddInParameter(dbCommand, "@LuongSPGCBan", SqlDbType.Decimal, LuongSPGCBan);
			db.AddInParameter(dbCommand, "@LuongTieuHuy", SqlDbType.Decimal, LuongTieuHuy);
			db.AddInParameter(dbCommand, "@LuongBan", SqlDbType.Decimal, LuongBan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail(long id)
		{
			KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_GoodItems_ID(long goodItems_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_DeleteBy_GoodItems_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GoodItems_ID", SqlDbType.BigInt, goodItems_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}