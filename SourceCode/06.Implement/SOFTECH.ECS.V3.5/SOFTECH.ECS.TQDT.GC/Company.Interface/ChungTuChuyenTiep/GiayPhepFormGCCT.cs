﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using Company.KDT.SHARE.Components.Messages.Send;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;
using NguyenTe = Company.KD.BLL.DuLieuChuan.NguyenTe;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
using NguyenTe = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangChuyenTiep = Company.GC.BLL.KDT.GC.HangChuyenTiep;
using NguyenTe = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe;
using Company.GC.BLL.KDT.GC;
using GiayPhep = Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep;
#endif


namespace Company.Interface
{
    public partial class GiayPhepFormGCCT : Company.Interface.BaseForm
    {
        public ToKhaiChuyenTiep TKCT;
        public GiayPhep giayPhep = new GiayPhep();
        public bool isKhaiBoSung = false;
        string versionSend = Company.KDT.SHARE.Components.Globals.VersionSend;

        public GiayPhepFormGCCT()
        {
            InitializeComponent();
            giayPhep.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
        }

        private void BindData()
        {

            dgList.DataSource = giayPhep.ConvertListToDataSet(HangChuyenTiep.SelectBy_Master_ID(TKCT.ID).Tables[0]);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void GiayPhepForm_Load(object sender, EventArgs e)
        {
            if (this._NguyenTe == null)
                _NguyenTe = NguyenTe.SelectAll().Tables[0];
            foreach (DataRow rowNuoc in this._NguyenTe.Rows)
                dgList.RootTable.Columns["MaNguyenTe"].ValueList.Add(rowNuoc["ID"].ToString(), rowNuoc["Ten"].ToString());

            if (giayPhep.ID <= 0)
            {
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                txtMaDVdcCap.Text = TKCT.MaDoanhNghiep;
                txtTenDVdcCap.Text = GlobalSettings.TEN_DON_VI;
            }
            else if (giayPhep.ID > 0)
            {
                txtMaCQCap.Text = giayPhep.MaCoQuanCap;
                txtMaDVdcCap.Text = giayPhep.MaDonViDuocCap;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                txtNguoiCap.Text = giayPhep.NguoiCap;
                txtNoiCap.Text = giayPhep.NoiCap;
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                txtTenDVdcCap.Text = giayPhep.TenDonViDuocCap;
                txtTenCQCap.Text = giayPhep.TenQuanCap;
                txtThongTinKhac.Text = giayPhep.ThongTinKhac;
                BindData();
            }
            if (TKCT.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            else if (giayPhep.SoTiepNhan > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                ccNgayTiepNhan.Text = giayPhep.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = giayPhep.SoTiepNhan + "";
            }
            else if (TKCT.SoToKhai > 0 && int.Parse(TKCT.PhanLuong != "" ? TKCT.PhanLuong : "0") == 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKCT.PhanLuong != "")
            {
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            SetButtonStateGIAYPHEP(TKCT, giayPhep);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<GiayPhepChiTiet> HangGiayPhepDetailCollection = new List<GiayPhepChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        GiayPhepChiTiet hgpDetail = new GiayPhepChiTiet();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hgpDetail.HCT_ID = Convert.ToInt64(row["HCT_ID"]);
                        hgpDetail.ID = Convert.ToInt64(row["ID"]);
                        HangGiayPhepDetailCollection.Add(hgpDetail);
                    }
                }
                foreach (GiayPhepChiTiet hgpDetailTMP in HangGiayPhepDetailCollection)
                {
                    try
                    {
                        if (hgpDetailTMP.ID > 0)
                        {
                            hgpDetailTMP.Delete();
                        }
                        foreach (GiayPhepChiTiet hgpDetail in giayPhep.ListHMDofGiayPhep)
                        {
                            if (hgpDetail.HCT_ID == hgpDetailTMP.HCT_ID)
                            {
                                giayPhep.ListHMDofGiayPhep.Remove(hgpDetail);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private bool checkSoGiayPhep(string soGiayPhep)
        {
            foreach (GiayPhep gpTMP in TKCT.GiayPhepCollection)
            {
                if (gpTMP.SoGiayPhep.Trim().ToUpper() == soGiayPhep.Trim().ToUpper())
                    return true;
            }
            return false;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;

            TKCT.GiayPhepCollection.Remove(giayPhep);

            if (checkSoGiayPhep(txtSoGiayPhep.Text))
            {
                if (giayPhep.ID > 0)
                    TKCT.GiayPhepCollection.Add(giayPhep);
                ShowMessage("Số giấy phép này đã tồn tại.", false);
                return;
            }

            giayPhep.MaCoQuanCap = txtMaCQCap.Text.Trim();
            giayPhep.MaDonViDuocCap = txtMaDVdcCap.Text.Trim();
            giayPhep.NgayGiayPhep = ccNgayGiayPhep.Value;
            giayPhep.NgayHetHan = ccNgayHHGiayPhep.Value;
            giayPhep.NguoiCap = txtNguoiCap.Text.Trim();
            giayPhep.NoiCap = txtNoiCap.Text.Trim();
            giayPhep.SoGiayPhep = txtSoGiayPhep.Text.Trim();
            giayPhep.TenDonViDuocCap = txtTenDVdcCap.Text.Trim();
            giayPhep.TenQuanCap = txtTenCQCap.Text.Trim();
            giayPhep.ThongTinKhac = txtThongTinKhac.Text;
            giayPhep.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            giayPhep.TKCT_ID = TKCT.ID;
            if (giayPhep.NgayTiepNhan.Year < 1900) giayPhep.NgayTiepNhan = new DateTime(1900, 1, 1);
            if (string.IsNullOrEmpty(giayPhep.GuidStr)) giayPhep.GuidStr = Guid.NewGuid().ToString();
            if (isKhaiBoSung)
                giayPhep.LoaiKB = 1;
            else
                giayPhep.LoaiKB = 0;
            GridEXRow[] rowCollection = dgList.GetRows();
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (GiayPhepChiTiet item in giayPhep.ListHMDofGiayPhep)
                {
                    if (rowview["HCT_ID"].ToString().Trim() == item.HCT_ID.ToString().Trim())
                    {
                        item.GhiChu = row.Cells["GhiChu"].Text;
                        item.MaChuyenNganh = row.Cells["MaChuyenNganh"].Value.ToString();
                        if (row.Cells["MaNguyenTe"].Text != null)
                            try
                            {
                                item.MaNguyenTe = row.Cells["MaNguyenTe"].Value.ToString();
                            }
                            catch { item.MaNguyenTe = "VND"; }
                        else
                        {
                            ShowMessage("Chưa chọn nguyên tệ cho hàng : " + (row.RowIndex + 1), false);
                            return;
                        }
                        break;
                    }
                }
            }

            try
            {
                giayPhep.InsertUpdateFull();
                TKCT.GiayPhepCollection.Add(giayPhep);
                BindData();
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
                return;
            }

        }

        public List<HangMauDich> ConvertListToHangMauDichCollection(List<GiayPhepChiTiet> listHangGiayPhep, List<HangMauDich> hangCollection)
        {

            List<HangMauDich> tmp = new List<HangMauDich>();

            foreach (GiayPhepChiTiet item in listHangGiayPhep)
            {
                foreach (HangMauDich HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HCT_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichFormGCCT f = new SelectHangMauDichFormGCCT();
            f.TKCT = TKCT;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất hàng khi kích nút Chọn hàng
            //if (giayPhep.ListHMDofGiayPhep.Count > 0)
            //{
            //    f.TKMD.HMDCollection = ConvertListToHangMauDichCollection(giayPhep.ListHMDofGiayPhep,TKMD.HMDCollection);
            //}
            f.ShowDialog();
            if (f.HCTCollection.Count > 0)
            {
                foreach (HangChuyenTiep HCT in f.HCTCollection)
                {
                    bool ok = false;
                    foreach (GiayPhepChiTiet hangGiayPhepDetail in giayPhep.ListHMDofGiayPhep)
                    {
                        if (hangGiayPhepDetail.HCT_ID == HCT.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        GiayPhepChiTiet hgpDetail = new GiayPhepChiTiet();
                        hgpDetail.HCT_ID = HCT.ID;
                        hgpDetail.GiayPhep_ID = giayPhep.ID;
                        hgpDetail.MaPhu = HCT.MaHang;
                        hgpDetail.MaHS = HCT.MaHS;
                        hgpDetail.TenHang = HCT.TenHang;
                        hgpDetail.DVT_ID = HCT.ID_DVT;
                        hgpDetail.SoThuTuHang = HCT.SoThuTuHang;
                        hgpDetail.SoLuong = HCT.SoLuong;
                        hgpDetail.NuocXX_ID = HCT.ID_NuocXX;
                        hgpDetail.MaNguyenTe = TKCT.NguyenTe_ID;
                        hgpDetail.DonGiaKB = Convert.ToDouble(HCT.DonGia);
                        hgpDetail.TriGiaKB = Convert.ToDouble(HCT.TriGia);
                        giayPhep.ListHMDofGiayPhep.Add(hgpDetail);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void btnChonGP_Click(object sender, EventArgs e)
        {
            ManageGiayPhepFormGCCT f = new ManageGiayPhepFormGCCT();
            f.isBrower = true;
            f.TKMD_ID = TKCT.ID;
            f.ShowDialog();
            if (f.giayPhep != null)
            {
                giayPhep = f.giayPhep;
                txtMaCQCap.Text = giayPhep.MaCoQuanCap;
                txtMaDVdcCap.Text = giayPhep.MaDonViDuocCap;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                txtNguoiCap.Text = giayPhep.NguoiCap;
                txtNoiCap.Text = giayPhep.NoiCap;
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                txtTenDVdcCap.Text = giayPhep.TenDonViDuocCap;
                txtTenCQCap.Text = giayPhep.TenQuanCap;
                txtThongTinKhac.Text = giayPhep.ThongTinKhac;
                giayPhep.ID = 0;
                giayPhep.GuidStr = "";
                giayPhep.SoTiepNhan = 0;
                giayPhep.NamTiepNhan = 0;
                giayPhep.NgayTiepNhan = new DateTime(1900, 1, 1);
                BindData();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["ID_DVT"].Text = this.DonViTinh_GetName(e.Row.Cells["ID_DVT"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (giayPhep.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }

            try
            {
                if (giayPhep.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO || giayPhep.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET)
                    giayPhep.GuidStr = Guid.NewGuid().ToString();
                else if (giayPhep.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    ShowMessage("Bạn đã gửi yêu cầu khai báo bổ sung đến hải quan\r\nVui lòng nhận phản hồi thông tin", false);
                    return;
                }
                #region V3

                ObjectSend msgSend = SingleMessage.BoSungChungTuGCCT(TKCT, giayPhep, GlobalSettings.TEN_DON_VI);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);
                if (isSend && (giayPhep.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                    giayPhep.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET))
                {
                    sendMessageForm.Message.XmlSaveMessage(giayPhep.ID, MessageTitle.KhaiBaoBoSungGiayPhep);
                    giayPhep.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    SetButtonStateGIAYPHEP(TKCT, giayPhep);
                    giayPhep.Update();
                    btnLayPhanHoi_Click(null, null);
                }


                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }


        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {

            ObjectSend msgSend = SingleMessage.FeedBackGCCT(TKCT, giayPhep.GuidStr);
            SendMessageForm sendMessageForm = new SendMessageForm();
            sendMessageForm.Send += SendMessage;
            sendMessageForm.DoSend(msgSend);
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (giayPhep.GuidStr != null && giayPhep.GuidStr != "")
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = giayPhep.ID;
                form.DeclarationIssuer = TKCT.MaLoaiHinh.Substring(0,1).ToUpper() == "N" ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE;
                form.ShowDialog(this);
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form GIAY PHEP.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private void SetButtonStateGIAYPHEP(ToKhaiChuyenTiep tkct, GiayPhep giayphep)
        {
            if (giayphep == null)
                return;

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                     || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                      || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnChonGP.Enabled = status;
                btnChonHang.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            else if (tkct.SoToKhai > 0 && TKCT.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {
                btnKetQuaXuLy.Enabled = true;
                //Re set button declaration LanNT


                bool khaiBaoEnable = (giayphep.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     giayphep.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnChonHang.Enabled = btnXoa.Enabled = btnChonGP.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = giayphep.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   giayphep.TrangThai == TrangThaiXuLy.DA_DUYET ||
                                   giayphep.TrangThai == TrangThaiXuLy.CHO_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
                
                if (giayphep.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || giayphep.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    txtSoTiepNhan.Text = string.Empty;
                    ccNgayTiepNhan.Text = string.Empty;
                }
                else
                {
                    txtSoTiepNhan.Text = giayphep.SoTiepNhan.ToString();
                    ccNgayTiepNhan.Value = giayphep.NgayTiepNhan;
                }

                switch (giayphep.TrangThai)
                {
                    case -1:
                        lbTrangThai.Text = "Chưa Khai Báo";
                        break;
                    case -3:
                        lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                        break;
                    case 0:
                        lbTrangThai.Text = "Chờ Duyệt";
                        break;
                    case 1:
                        lbTrangThai.Text = "Đã Duyệt";
                        break;
                    case 2:
                        lbTrangThai.Text = "Hải quan không phê duyệt";
                        break;
                }

            }

        }

        #endregion
        
        #region  V3

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    FeedBackContent feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function)
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(giayPhep.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                giayPhep.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                this.ShowMessageTQDT("Hải quan từ chối tiếp nhận", noidung, false);
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            noidung = noidung.Replace(string.Format("Message [{0}]", giayPhep.GuidStr), string.Empty);
                            this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            btnKhaiBao.Enabled = false;
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                giayPhep.SoTiepNhan = long.Parse(vals[0].Trim());
                                giayPhep.NamTiepNhan = int.Parse(vals[1].Trim());
                                giayPhep.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                e.FeedBackMessage.XmlSaveMessage(giayPhep.ID, MessageTitle.KhaiBaoBoSungGiayPhepDuocCapSoTN, noidung);
                                giayPhep.TrangThai = TrangThaiXuLy.CHO_DUYET;
                                this.ShowMessageTQDT("Được cấp số tiếp nhận.\n" + giayPhep.SoTiepNhan.ToString() + "\n" + giayPhep.NgayTiepNhan.ToString(), false);
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(giayPhep.ID, MessageTitle.KhaiBaoBoSungGiayPhepDuocChapNhan, noidung);
                                giayPhep.TrangThai = TrangThaiXuLy.DA_DUYET;
                                ShowMessageTQDT("Chứng từ bổ sung đã được duyệt", false);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(giayPhep.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }
                    if (isUpdate)
                    {
                        giayPhep.Update();
                        SetButtonStateGIAYPHEP(TKCT, giayPhep);
                    }

                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion
    }
}

