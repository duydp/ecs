using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_HSCode
    {
        protected static List<VNACC_Category_HSCode> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_HSCode> collection = new List<VNACC_Category_HSCode>();
            while (reader.Read())
            {
                VNACC_Category_HSCode entity = new VNACC_Category_HSCode();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("HSCode"))) entity.HSCode = reader.GetString(reader.GetOrdinal("HSCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_HSCode> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [HSCode] ,[Notes] FROM [dbo].[t_VNACC_Category_HSCode]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_HSCode> SelectCollectionBy(List<VNACC_Category_HSCode> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_HSCode o)
            {
                return o.TableID.Contains(tableID);
            });
        }
    }
}