﻿using System;

using System.Data;
using System.Threading;
using System.Globalization;
using System.Resources;
using Company.Controls.CustomValidation;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.GC.BLL.KDT.GC;
using System.Collections.Generic;

namespace Company.Interface.KDT.GC
{
    public partial class PhuLucHopDongTKCT : BaseForm
    {
        public ToKhaiChuyenTiep TKCT;
        public PhuLucHopDong PhuLuc;
        public PhuLucHopDongTKCT()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void PhuLucHopDongTKCT_Load(object sender, EventArgs e)
        {
            if (TKCT.ID > 0)
            {
                TKCT.LoadPhuLucHD();
                if (TKCT.PhuLucHDCollection != null && TKCT.PhuLucHDCollection.Count > 0)
                    PhuLuc = TKCT.PhuLucHDCollection[0];
            }
            if (PhuLuc != null)
            {
                txtSoPhuLuc.Text = PhuLuc.SoPhuLuc;
                ccNgayHetHan.Value = PhuLuc.NgayHetHan;
                ccNgayHetHan.Text = ccNgayHetHan.Value.ToString("dd/MM/yyyy");
                ccNgayPhuLuc.Value = PhuLuc.NgayPhuLuc;
                ccNgayPhuLuc.Text = ccNgayPhuLuc.Value.ToString("dd/MM/yyyy");
            }

            bool isEnable = TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO
                            || TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                            || TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                            || TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;

            btnSave.Enabled = isEnable;
        }
       
        private bool save()
        {
            cvError.Validate();
            if (!cvError.IsValid)
            {
                return false;
            }
            if (ccNgayPhuLuc.Value != null && ccNgayHetHan.Value != null && ccNgayHetHan.Value < ccNgayPhuLuc.Value)
            {
                ShowMessage("Ngày hết hạn không được nhỏ hơn ngày phụ lục hợp đồng", false);
                return false;
            }
            if (TKCT.PhuLucHDCollection == null)
            {
                TKCT.PhuLucHDCollection = new List<PhuLucHopDong>();
            }

         //   GiayKiemTra.ChungTuCollection.Remove(PhuLuc);

           
            if (PhuLuc == null)
                PhuLuc = new PhuLucHopDong();
            PhuLuc.SoPhuLuc = txtSoPhuLuc.Text.Trim();
            PhuLuc.NgayPhuLuc = ccNgayPhuLuc.Value;
            PhuLuc.NgayHetHan = ccNgayHetHan.Value;
            PhuLuc.TKCT_ID = TKCT.ID;
            if (TKCT.PhuLucHDCollection.Count > 0)
            {
                TKCT.PhuLucHDCollection[0] = PhuLuc;
            }
            else
                TKCT.PhuLucHDCollection.Add(PhuLuc);
            

//             if (cbLoaiChungTu.SelectedValue.ToString() == "2")
//                 TKMD.SoContainer20++;
//             else
//                 TKMD.SoContainer40++;
            return true;

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (save())
            {
                ShowMessage("Đã lưu phụ lục hợp đồng. Chú ý: Lưu thông tin tờ khai để cập nhật phụ lục", false);
                this.Close();
            }
        }






    }
}