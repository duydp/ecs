﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using System.Data;
using Company.BLL.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class ReportNPL : DevExpress.XtraReports.UI.XtraReport
    {
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
        private int STT = 0;
        public ReportNPL()
        {
            InitializeComponent();
        }
        public void BindReportDinhMucDaDangKy()
        {
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;

            this.DataSource = this.NPLCollection;
            lblMaSP.DataBindings.Add("Text", this.DataSource, "Ma");
            lblTenSP.DataBindings.Add("Text", this.DataSource, "Ten");
            lblMaHS.DataBindings.Add("Text", this.DataSource, "MaHS");
            xrLabel5.Text = xrLabel76.Text = GlobalSettings.TieudeNgay;
        }
        public void BindHD(string HD)
        {
            lblHopDong.Text = HD;
        }
        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDVT.Text = DonViTinh.GetName(GetCurrentColumnValue("DVT_ID"));
        }

        private void ReportNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;

        }

        private void xrTableCell7_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if ((bool)GetCurrentColumnValue("NPLChinh")) xrTableCell7.Text = "X";
            else xrTableCell7.Text = "";
        }

    }
}
