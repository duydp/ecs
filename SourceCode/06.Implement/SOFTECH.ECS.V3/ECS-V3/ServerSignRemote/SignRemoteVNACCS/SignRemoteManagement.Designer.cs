﻿namespace SignRemoteVNACCS
{
    partial class SignRemoteManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout gridList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignRemoteManagement));
            Janus.Windows.GridEX.GridEXLayout gridInbox_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridOutbox_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridLogin_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.gridList = new Janus.Windows.GridEX.GridEX();
            this.txtTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtUserName = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtFrom = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gridInbox = new Janus.Windows.GridEX.GridEX();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNguoiNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiGui = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gridOutbox = new Janus.Windows.GridEX.GridEX();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMaDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.gridLogin = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.dateTo = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtpDateFrom = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.ui = new Janus.Windows.EditControls.UIGroupBox();
            this.dateDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dateTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteUser = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteLogin = new Janus.Windows.EditControls.UIButton();
            this.btnCreateLogin = new Janus.Windows.EditControls.UIButton();
            this.btnSearchLogin = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteInbox = new Janus.Windows.EditControls.UIButton();
            this.btnSearchInbox = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteOutbox = new Janus.Windows.EditControls.UIButton();
            this.btnSearchOutbox = new Janus.Windows.EditControls.UIButton();
            this.dateWarning = new Janus.Windows.CalendarCombo.CalendarCombo();
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridInbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridOutbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ui)).BeginInit();
            this.ui.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridList
            // 
            this.gridList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridList.ColumnAutoResize = true;
            gridList_DesignTimeLayout.LayoutString = resources.GetString("gridList_DesignTimeLayout.LayoutString");
            this.gridList.DesignTimeLayout = gridList_DesignTimeLayout;
            this.gridList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridList.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridList.GridLineColor = System.Drawing.SystemColors.Control;
            this.gridList.GroupByBoxVisible = false;
            this.gridList.HeaderFormatStyle.BackColorGradient = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(238)))), ((int)(((byte)(134)))));
            this.gridList.Location = new System.Drawing.Point(3, 8);
            this.gridList.Name = "gridList";
            this.gridList.RecordNavigator = true;
            this.gridList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridList.ScrollBars = Janus.Windows.GridEX.ScrollBars.Vertical;
            this.gridList.Size = new System.Drawing.Size(1190, 554);
            this.gridList.TabIndex = 6;
            this.gridList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridList_RowDoubleClick);
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(176, 57);
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(495, 23);
            this.txtTenDoanhNghiep.TabIndex = 3;
            this.txtTenDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDoanhNghiep.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(486, 20);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(179, 23);
            this.txtUserName.TabIndex = 2;
            this.txtUserName.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtUserName.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(176, 20);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(195, 23);
            this.txtMaDoanhNghiep.TabIndex = 1;
            this.txtMaDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDoanhNghiep.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(23, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 16);
            this.label7.TabIndex = 20;
            this.label7.Text = "Mã doanh nghiệp :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(21, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 16);
            this.label8.TabIndex = 19;
            this.label8.Text = "Tên doanh nghiệp :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(377, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 16);
            this.label9.TabIndex = 18;
            this.label9.Text = "Tên đăng nhập :";
            // 
            // txtTo
            // 
            this.txtTo.Location = new System.Drawing.Point(143, 60);
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new System.Drawing.Size(463, 23);
            this.txtTo.TabIndex = 28;
            this.txtTo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTo.TextChanged += new System.EventHandler(this.btnSearchInbox_Click);
            // 
            // txtFrom
            // 
            this.txtFrom.Location = new System.Drawing.Point(145, 21);
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new System.Drawing.Size(145, 23);
            this.txtFrom.TabIndex = 26;
            this.txtFrom.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtFrom.TextChanged += new System.EventHandler(this.btnSearchInbox_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(21, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 32;
            this.label1.Text = "Người gửi :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(19, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 16);
            this.label2.TabIndex = 31;
            this.label2.Text = "Người nhận :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(532, 24);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 30;
            this.label10.Text = "Đến ngày :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(305, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 16);
            this.label3.TabIndex = 30;
            this.label3.Text = "Từ ngày :";
            // 
            // gridInbox
            // 
            this.gridInbox.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridInbox.ColumnAutoResize = true;
            gridInbox_DesignTimeLayout.LayoutString = resources.GetString("gridInbox_DesignTimeLayout.LayoutString");
            this.gridInbox.DesignTimeLayout = gridInbox_DesignTimeLayout;
            this.gridInbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridInbox.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridInbox.GridLineColor = System.Drawing.SystemColors.Control;
            this.gridInbox.GroupByBoxVisible = false;
            this.gridInbox.HeaderFormatStyle.BackColorGradient = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(238)))), ((int)(((byte)(134)))));
            this.gridInbox.Location = new System.Drawing.Point(3, 8);
            this.gridInbox.Name = "gridInbox";
            this.gridInbox.RecordNavigator = true;
            this.gridInbox.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridInbox.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridInbox.ScrollBars = Janus.Windows.GridEX.ScrollBars.Vertical;
            this.gridInbox.Size = new System.Drawing.Size(1190, 548);
            this.gridInbox.TabIndex = 25;
            this.gridInbox.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(532, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 16);
            this.label6.TabIndex = 40;
            this.label6.Text = "Đến ngày :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(305, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 16);
            this.label11.TabIndex = 41;
            this.label11.Text = "Từ ngày :";
            // 
            // txtNguoiNhan
            // 
            this.txtNguoiNhan.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiNhan.Location = new System.Drawing.Point(143, 60);
            this.txtNguoiNhan.Name = "txtNguoiNhan";
            this.txtNguoiNhan.Size = new System.Drawing.Size(463, 23);
            this.txtNguoiNhan.TabIndex = 35;
            this.txtNguoiNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNguoiNhan.TextChanged += new System.EventHandler(this.btnSearchOutbox_Click);
            // 
            // txtNguoiGui
            // 
            this.txtNguoiGui.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiGui.Location = new System.Drawing.Point(145, 21);
            this.txtNguoiGui.Name = "txtNguoiGui";
            this.txtNguoiGui.Size = new System.Drawing.Size(145, 23);
            this.txtNguoiGui.TabIndex = 34;
            this.txtNguoiGui.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNguoiGui.TextChanged += new System.EventHandler(this.btnSearchOutbox_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(21, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 39;
            this.label4.Text = "Người gửi :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(19, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 16);
            this.label5.TabIndex = 38;
            this.label5.Text = "Người nhận :";
            // 
            // gridOutbox
            // 
            this.gridOutbox.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridOutbox.ColumnAutoResize = true;
            gridOutbox_DesignTimeLayout.LayoutString = resources.GetString("gridOutbox_DesignTimeLayout.LayoutString");
            this.gridOutbox.DesignTimeLayout = gridOutbox_DesignTimeLayout;
            this.gridOutbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridOutbox.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridOutbox.GridLineColor = System.Drawing.SystemColors.Control;
            this.gridOutbox.GroupByBoxVisible = false;
            this.gridOutbox.HeaderFormatStyle.BackColorGradient = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(238)))), ((int)(((byte)(134)))));
            this.gridOutbox.Location = new System.Drawing.Point(3, 8);
            this.gridOutbox.Name = "gridOutbox";
            this.gridOutbox.RecordNavigator = true;
            this.gridOutbox.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridOutbox.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridOutbox.ScrollBars = Janus.Windows.GridEX.ScrollBars.Vertical;
            this.gridOutbox.Size = new System.Drawing.Size(1190, 548);
            this.gridOutbox.TabIndex = 26;
            this.gridOutbox.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(377, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 16);
            this.label12.TabIndex = 47;
            this.label12.Text = "Cảnh báo :";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(176, 20);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(195, 23);
            this.txtMaDN.TabIndex = 44;
            this.txtMaDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDN.TextChanged += new System.EventHandler(this.btnSearchLogin_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(23, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(115, 16);
            this.label13.TabIndex = 46;
            this.label13.Text = "Mã doanh nghiệp :";
            // 
            // gridLogin
            // 
            this.gridLogin.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridLogin.ColumnAutoResize = true;
            gridLogin_DesignTimeLayout.LayoutString = resources.GetString("gridLogin_DesignTimeLayout.LayoutString");
            this.gridLogin.DesignTimeLayout = gridLogin_DesignTimeLayout;
            this.gridLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridLogin.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridLogin.GridLineColor = System.Drawing.SystemColors.Control;
            this.gridLogin.GroupByBoxVisible = false;
            this.gridLogin.HeaderFormatStyle.BackColorGradient = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(238)))), ((int)(((byte)(134)))));
            this.gridLogin.Location = new System.Drawing.Point(3, 8);
            this.gridLogin.Name = "gridLogin";
            this.gridLogin.RecordNavigator = true;
            this.gridLogin.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridLogin.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridLogin.ScrollBars = Janus.Windows.GridEX.ScrollBars.Vertical;
            this.gridLogin.Size = new System.Drawing.Size(1190, 587);
            this.gridLogin.TabIndex = 27;
            this.gridLogin.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridLogin.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridLogin_RowDoubleClick);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiTab1);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1198, 684);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiTab1
            // 
            this.uiTab1.BackColor = System.Drawing.Color.Transparent;
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1198, 684);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage4,
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.uiGroupBox8);
            this.uiTabPage4.Controls.Add(this.uiGroupBox9);
            this.uiTabPage4.Location = new System.Drawing.Point(1, 24);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1196, 659);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Quản lý Đăng nhập Tài khoản ký";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.gridList);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Location = new System.Drawing.Point(0, 94);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(1196, 565);
            this.uiGroupBox8.TabIndex = 3;
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.btnDeleteUser);
            this.uiGroupBox9.Controls.Add(this.btnAdd);
            this.uiGroupBox9.Controls.Add(this.btnSearch);
            this.uiGroupBox9.Controls.Add(this.label9);
            this.uiGroupBox9.Controls.Add(this.label8);
            this.uiGroupBox9.Controls.Add(this.label7);
            this.uiGroupBox9.Controls.Add(this.txtTenDoanhNghiep);
            this.uiGroupBox9.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox9.Controls.Add(this.txtUserName);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox9.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(1196, 94);
            this.uiGroupBox9.TabIndex = 2;
            this.uiGroupBox9.Text = "Tìm kiếm";
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.uiGroupBox3);
            this.uiTabPage1.Controls.Add(this.uiGroupBox2);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 24);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1196, 659);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Quản lý Tài khoản Chữ ký số Online";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.gridLogin);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 61);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1196, 598);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dateWarning);
            this.uiGroupBox2.Controls.Add(this.btnDeleteLogin);
            this.uiGroupBox2.Controls.Add(this.btnCreateLogin);
            this.uiGroupBox2.Controls.Add(this.btnSearchLogin);
            this.uiGroupBox2.Controls.Add(this.txtMaDN);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1196, 61);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Tìm kiếm";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.uiGroupBox4);
            this.uiTabPage2.Controls.Add(this.uiGroupBox5);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 24);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1196, 659);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Quản lý Message đã gửi lý Online";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.gridInbox);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 100);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1196, 559);
            this.uiGroupBox4.TabIndex = 3;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.btnDeleteInbox);
            this.uiGroupBox5.Controls.Add(this.btnSearchInbox);
            this.uiGroupBox5.Controls.Add(this.dateTo);
            this.uiGroupBox5.Controls.Add(this.dtpDateFrom);
            this.uiGroupBox5.Controls.Add(this.label3);
            this.uiGroupBox5.Controls.Add(this.label10);
            this.uiGroupBox5.Controls.Add(this.label2);
            this.uiGroupBox5.Controls.Add(this.txtTo);
            this.uiGroupBox5.Controls.Add(this.label1);
            this.uiGroupBox5.Controls.Add(this.txtFrom);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1196, 100);
            this.uiGroupBox5.TabIndex = 2;
            this.uiGroupBox5.Text = "Tìm kiếm";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // dateTo
            // 
            this.dateTo.CustomFormat = "dd/MM/yyyy";
            this.dateTo.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dateTo.DropDownCalendar.Name = "";
            this.dateTo.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dateTo.Location = new System.Drawing.Point(609, 19);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(146, 23);
            this.dateTo.TabIndex = 55;
            this.dateTo.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dateTo.TextChanged += new System.EventHandler(this.btnSearchInbox_Click);
            // 
            // dtpDateFrom
            // 
            this.dtpDateFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpDateFrom.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtpDateFrom.DropDownCalendar.Name = "";
            this.dtpDateFrom.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpDateFrom.Location = new System.Drawing.Point(380, 19);
            this.dtpDateFrom.Name = "dtpDateFrom";
            this.dtpDateFrom.Size = new System.Drawing.Size(146, 23);
            this.dtpDateFrom.TabIndex = 54;
            this.dtpDateFrom.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpDateFrom.TextChanged += new System.EventHandler(this.btnSearchInbox_Click);
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.uiGroupBox6);
            this.uiTabPage3.Controls.Add(this.ui);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 24);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1196, 659);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Quản lý Message đã lý Online";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.gridOutbox);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 100);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1196, 559);
            this.uiGroupBox6.TabIndex = 3;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ui
            // 
            this.ui.AutoScroll = true;
            this.ui.BackColor = System.Drawing.Color.Transparent;
            this.ui.Controls.Add(this.btnDeleteOutbox);
            this.ui.Controls.Add(this.btnSearchOutbox);
            this.ui.Controls.Add(this.dateDenNgay);
            this.ui.Controls.Add(this.dateTuNgay);
            this.ui.Controls.Add(this.label5);
            this.ui.Controls.Add(this.label4);
            this.ui.Controls.Add(this.label6);
            this.ui.Controls.Add(this.txtNguoiGui);
            this.ui.Controls.Add(this.label11);
            this.ui.Controls.Add(this.txtNguoiNhan);
            this.ui.Dock = System.Windows.Forms.DockStyle.Top;
            this.ui.Location = new System.Drawing.Point(0, 0);
            this.ui.Name = "ui";
            this.ui.Size = new System.Drawing.Size(1196, 100);
            this.ui.TabIndex = 2;
            this.ui.Text = "Tìm kiếm";
            this.ui.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // dateDenNgay
            // 
            this.dateDenNgay.CustomFormat = "dd/MM/yyyy";
            this.dateDenNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dateDenNgay.DropDownCalendar.Name = "";
            this.dateDenNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dateDenNgay.Location = new System.Drawing.Point(609, 19);
            this.dateDenNgay.Name = "dateDenNgay";
            this.dateDenNgay.Size = new System.Drawing.Size(146, 23);
            this.dateDenNgay.TabIndex = 53;
            this.dateDenNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dateDenNgay.TextChanged += new System.EventHandler(this.btnSearchOutbox_Click);
            // 
            // dateTuNgay
            // 
            this.dateTuNgay.CustomFormat = "dd/MM/yyyy";
            this.dateTuNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dateTuNgay.DropDownCalendar.Name = "";
            this.dateTuNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dateTuNgay.Location = new System.Drawing.Point(380, 19);
            this.dateTuNgay.Name = "dateTuNgay";
            this.dateTuNgay.Size = new System.Drawing.Size(146, 23);
            this.dateTuNgay.TabIndex = 53;
            this.dateTuNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dateTuNgay.TextChanged += new System.EventHandler(this.btnSearchOutbox_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::SignRemoteVNACCS.Properties.Resources.Search_48px;
            this.btnSearch.Location = new System.Drawing.Point(685, 17);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(106, 25);
            this.btnSearch.TabIndex = 51;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = global::SignRemoteVNACCS.Properties.Resources.Plus_48px;
            this.btnAdd.Location = new System.Drawing.Point(685, 54);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(106, 25);
            this.btnAdd.TabIndex = 51;
            this.btnAdd.Text = "Thêm mới";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDeleteUser
            // 
            this.btnDeleteUser.Image = global::SignRemoteVNACCS.Properties.Resources.Trash_32px;
            this.btnDeleteUser.Location = new System.Drawing.Point(797, 55);
            this.btnDeleteUser.Name = "btnDeleteUser";
            this.btnDeleteUser.Size = new System.Drawing.Size(106, 25);
            this.btnDeleteUser.TabIndex = 51;
            this.btnDeleteUser.Text = "Xóa";
            this.btnDeleteUser.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteUser.Click += new System.EventHandler(this.btnDeleteUser_Click);
            // 
            // btnDeleteLogin
            // 
            this.btnDeleteLogin.Image = global::SignRemoteVNACCS.Properties.Resources.Trash_32px;
            this.btnDeleteLogin.Location = new System.Drawing.Point(858, 19);
            this.btnDeleteLogin.Name = "btnDeleteLogin";
            this.btnDeleteLogin.Size = new System.Drawing.Size(106, 25);
            this.btnDeleteLogin.TabIndex = 54;
            this.btnDeleteLogin.Text = "Xóa";
            this.btnDeleteLogin.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteLogin.Click += new System.EventHandler(this.btnDeleteLogin_Click);
            // 
            // btnCreateLogin
            // 
            this.btnCreateLogin.Image = global::SignRemoteVNACCS.Properties.Resources.Plus_48px;
            this.btnCreateLogin.Location = new System.Drawing.Point(712, 18);
            this.btnCreateLogin.Name = "btnCreateLogin";
            this.btnCreateLogin.Size = new System.Drawing.Size(140, 25);
            this.btnCreateLogin.TabIndex = 53;
            this.btnCreateLogin.Text = "Tạo mới - Cập nhật";
            this.btnCreateLogin.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCreateLogin.Click += new System.EventHandler(this.btnCreateLogin_Click);
            // 
            // btnSearchLogin
            // 
            this.btnSearchLogin.Image = global::SignRemoteVNACCS.Properties.Resources.Search_48px;
            this.btnSearchLogin.Location = new System.Drawing.Point(600, 18);
            this.btnSearchLogin.Name = "btnSearchLogin";
            this.btnSearchLogin.Size = new System.Drawing.Size(106, 25);
            this.btnSearchLogin.TabIndex = 52;
            this.btnSearchLogin.Text = "Tìm kiếm";
            this.btnSearchLogin.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchLogin.Click += new System.EventHandler(this.btnSearchLogin_Click);
            // 
            // btnDeleteInbox
            // 
            this.btnDeleteInbox.Image = global::SignRemoteVNACCS.Properties.Resources.Trash_32px;
            this.btnDeleteInbox.Location = new System.Drawing.Point(727, 59);
            this.btnDeleteInbox.Name = "btnDeleteInbox";
            this.btnDeleteInbox.Size = new System.Drawing.Size(106, 25);
            this.btnDeleteInbox.TabIndex = 57;
            this.btnDeleteInbox.Text = "Xóa";
            this.btnDeleteInbox.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteInbox.Click += new System.EventHandler(this.btnDeleteInbox_Click);
            // 
            // btnSearchInbox
            // 
            this.btnSearchInbox.Image = global::SignRemoteVNACCS.Properties.Resources.Search_48px;
            this.btnSearchInbox.Location = new System.Drawing.Point(615, 59);
            this.btnSearchInbox.Name = "btnSearchInbox";
            this.btnSearchInbox.Size = new System.Drawing.Size(106, 25);
            this.btnSearchInbox.TabIndex = 56;
            this.btnSearchInbox.Text = "Tìm kiếm";
            this.btnSearchInbox.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchInbox.Click += new System.EventHandler(this.btnSearchInbox_Click);
            // 
            // btnDeleteOutbox
            // 
            this.btnDeleteOutbox.Image = global::SignRemoteVNACCS.Properties.Resources.Trash_32px;
            this.btnDeleteOutbox.Location = new System.Drawing.Point(727, 59);
            this.btnDeleteOutbox.Name = "btnDeleteOutbox";
            this.btnDeleteOutbox.Size = new System.Drawing.Size(106, 25);
            this.btnDeleteOutbox.TabIndex = 59;
            this.btnDeleteOutbox.Text = "Xóa";
            this.btnDeleteOutbox.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteOutbox.Click += new System.EventHandler(this.btnDeleteOutbox_Click);
            // 
            // btnSearchOutbox
            // 
            this.btnSearchOutbox.Image = global::SignRemoteVNACCS.Properties.Resources.Search_48px;
            this.btnSearchOutbox.Location = new System.Drawing.Point(615, 59);
            this.btnSearchOutbox.Name = "btnSearchOutbox";
            this.btnSearchOutbox.Size = new System.Drawing.Size(106, 25);
            this.btnSearchOutbox.TabIndex = 58;
            this.btnSearchOutbox.Text = "Tìm kiếm";
            this.btnSearchOutbox.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchOutbox.Click += new System.EventHandler(this.btnSearchOutbox_Click);
            // 
            // dateWarning
            // 
            this.dateWarning.CustomFormat = "dd/MM/yyyy";
            this.dateWarning.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dateWarning.DropDownCalendar.Name = "";
            this.dateWarning.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dateWarning.Location = new System.Drawing.Point(448, 18);
            this.dateWarning.Name = "dateWarning";
            this.dateWarning.Size = new System.Drawing.Size(146, 23);
            this.dateWarning.TabIndex = 55;
            this.dateWarning.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // SignRemoteManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1198, 684);
            this.Controls.Add(this.uiGroupBox1);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SignRemoteManagement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý Tài khoản chữ ký số Online";
            this.Load += new System.EventHandler(this.SignRemoteManagement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridInbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridOutbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ui)).EndInit();
            this.ui.ResumeLayout(false);
            this.ui.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX gridList;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox txtUserName;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.GridEX gridInbox;
        private Janus.Windows.GridEX.GridEX gridOutbox;
        private Janus.Windows.GridEX.EditControls.EditBox txtTo;
        private Janus.Windows.GridEX.EditControls.EditBox txtFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiGui;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.GridEX gridLogin;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDN;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox ui;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.CalendarCombo.CalendarCombo dateTuNgay;
        private Janus.Windows.CalendarCombo.CalendarCombo dateDenNgay;
        private Janus.Windows.CalendarCombo.CalendarCombo dateTo;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpDateFrom;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.EditControls.UIButton btnDeleteUser;
        private Janus.Windows.EditControls.UIButton btnDeleteLogin;
        private Janus.Windows.EditControls.UIButton btnCreateLogin;
        private Janus.Windows.EditControls.UIButton btnSearchLogin;
        private Janus.Windows.EditControls.UIButton btnDeleteInbox;
        private Janus.Windows.EditControls.UIButton btnSearchInbox;
        private Janus.Windows.CalendarCombo.CalendarCombo dateWarning;
        private Janus.Windows.EditControls.UIButton btnDeleteOutbox;
        private Janus.Windows.EditControls.UIButton btnSearchOutbox;



    }
}