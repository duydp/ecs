go
ALTER TABLE [dbo].[t_GC_ThanhKhoanHDGC] ADD Ma [nvarchar] (50) NULL
 
GO
/****** Object:  StoredProcedure [dbo].[p_GC_ThanhKhoanHDGC_Insert]    Script Date: 29/12/2015 3:14:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThanhKhoanHDGC_Insert]
-- Database: ECS_GIACONG
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_ThanhKhoanHDGC_Insert]
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@DVT nvarchar(15),
	@TongLuongNK numeric(18, 5),
	@TongLuongCU numeric(18, 5),
	@TongLuongXK numeric(18, 5),
	@ChenhLech numeric(18, 5),
	@KetLuanXLCL nvarchar(max),
	@OldHD_ID bigint,
	@STT int,
	@HopDong_ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_GC_ThanhKhoanHDGC]
(
	[Ma],
	[Ten],
	[DVT],
	[TongLuongNK],
	[TongLuongCU],
	[TongLuongXK],
	[ChenhLech],
	[KetLuanXLCL],
	[OldHD_ID],
	[STT]
)
VALUES 
(
	@Ma,
	@Ten,
	@DVT,
	@TongLuongNK,
	@TongLuongCU,
	@TongLuongXK,
	@ChenhLech,
	@KetLuanXLCL,
	@OldHD_ID,
	@STT
)

SET @HopDong_ID = SCOPE_IDENTITY()

go
 GO
/****** Object:  StoredProcedure [dbo].[p_GC_ThanhKhoanHDGC_SelectDynamic]    Script Date: 29/12/2015 3:19:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThanhKhoanHDGC_SelectDynamic]
-- Database: ECS_GIACONG
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_ThanhKhoanHDGC_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[HopDong_ID],[Ma],
	[Ten],
	[DVT],
	[TongLuongNK],
	[TongLuongCU],
	[TongLuongXK],
	[ChenhLech],
	[KetLuanXLCL],
	[OldHD_ID],
	[STT]
 FROM [dbo].[t_GC_ThanhKhoanHDGC] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


go
 GO
/****** Object:  StoredProcedure [dbo].[p_GC_ThanhKhoanHDGC_InsertUpdate]    Script Date: 29/12/2015 3:16:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThanhKhoanHDGC_InsertUpdate]
-- Database: ECS_GIACONG
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_ThanhKhoanHDGC_InsertUpdate]
	@HopDong_ID bigint,
	@Ma	 nvarchar(50),
	@Ten nvarchar(255),
	@DVT nvarchar(15),
	@TongLuongNK numeric(18, 5),
	@TongLuongCU numeric(18, 5),
	@TongLuongXK numeric(18, 5),
	@ChenhLech numeric(18, 5),
	@KetLuanXLCL nvarchar(max),
	@OldHD_ID bigint,
	@STT int
AS
IF EXISTS(SELECT [HopDong_ID] FROM [dbo].[t_GC_ThanhKhoanHDGC] WHERE [HopDong_ID] = @HopDong_ID)
	BEGIN
		UPDATE
			[dbo].[t_GC_ThanhKhoanHDGC] 
		SET
			[Ma] = @Ma,
			[Ten] = @Ten,
			[DVT] = @DVT,
			[TongLuongNK] = @TongLuongNK,
			[TongLuongCU] = @TongLuongCU,
			[TongLuongXK] = @TongLuongXK,
			[ChenhLech] = @ChenhLech,
			[KetLuanXLCL] = @KetLuanXLCL,
			[OldHD_ID] = @OldHD_ID,
			[STT] = @STT
		WHERE
			[HopDong_ID] = @HopDong_ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_GC_ThanhKhoanHDGC]
		(
			[Ma],
			[Ten],
			[DVT],
			[TongLuongNK],
			[TongLuongCU],
			[TongLuongXK],
			[ChenhLech],
			[KetLuanXLCL],
			[OldHD_ID],
			[STT]
		)
		VALUES 
		(
			@Ma,
			@Ten,
			@DVT,
			@TongLuongNK,
			@TongLuongCU,
			@TongLuongXK,
			@ChenhLech,
			@KetLuanXLCL,
			@OldHD_ID,
			@STT
		)		
	END
go

 GO
/****** Object:  StoredProcedure [dbo].[p_GC_ThanhKhoanHDGC_Load]    Script Date: 29/12/2015 3:18:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThanhKhoanHDGC_Load]
-- Database: ECS_GIACONG
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_ThanhKhoanHDGC_Load]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[DVT],
	[TongLuongNK],
	[TongLuongCU],
	[TongLuongXK],
	[ChenhLech],
	[KetLuanXLCL],
	[OldHD_ID],
	[STT]
FROM
	[dbo].[t_GC_ThanhKhoanHDGC]
WHERE
	[HopDong_ID] = @HopDong_ID

go
GO
/****** Object:  StoredProcedure [dbo].[p_GC_ThanhKhoanHDGC_SelectAll]    Script Date: 29/12/2015 3:19:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThanhKhoanHDGC_SelectAll]
-- Database: ECS_GIACONG
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_ThanhKhoanHDGC_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[DVT],
	[TongLuongNK],
	[TongLuongCU],
	[TongLuongXK],
	[ChenhLech],
	[KetLuanXLCL],
	[OldHD_ID],
	[STT]
FROM
	[dbo].[t_GC_ThanhKhoanHDGC]

 go
 GO
/****** Object:  StoredProcedure [dbo].[p_GC_ThanhKhoanHDGC_Update]    Script Date: 29/12/2015 3:20:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThanhKhoanHDGC_Update]
-- Database: ECS_GIACONG
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_ThanhKhoanHDGC_Update]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@DVT nvarchar(15),
	@TongLuongNK numeric(18, 5),
	@TongLuongCU numeric(18, 5),
	@TongLuongXK numeric(18, 5),
	@ChenhLech numeric(18, 5),
	@KetLuanXLCL nvarchar(max),
	@OldHD_ID bigint,
	@STT int
AS
UPDATE
	[dbo].[t_GC_ThanhKhoanHDGC]
SET
	[Ma] = @Ma,
	[Ten] = @Ten,
	[DVT] = @DVT,
	[TongLuongNK] = @TongLuongNK,
	[TongLuongCU] = @TongLuongCU,
	[TongLuongXK] = @TongLuongXK,
	[ChenhLech] = @ChenhLech,
	[KetLuanXLCL] = @KetLuanXLCL,
	[OldHD_ID] = @OldHD_ID,
	[STT] = @STT
WHERE
	[HopDong_ID] = @HopDong_ID


GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '21.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('21.8',GETDATE(),'Cập nhật thanh khoản mẫu 06')
END	