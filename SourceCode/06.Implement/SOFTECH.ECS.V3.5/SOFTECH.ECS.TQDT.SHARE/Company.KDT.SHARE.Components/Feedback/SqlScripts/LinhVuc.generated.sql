-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_Insert]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_Update]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_Delete]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_Load]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GopY_LinhVuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_LinhVuc_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_Insert]
-- Database: ecs
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 02, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_Insert]
	@Ma varchar(50),
	@Ten nvarchar(250),
	@HienThi bit,
	@MacDinh bit,
	@ThuTu int,
	@ParentID varchar(50)
AS
INSERT INTO [dbo].[t_GopY_LinhVuc]
(
	[Ma],
	[Ten],
	[HienThi],
	[MacDinh],
	[ThuTu],
	[ParentID]
)
VALUES
(
	@Ma,
	@Ten,
	@HienThi,
	@MacDinh,
	@ThuTu,
	@ParentID
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_Update]
-- Database: ecs
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 02, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_Update]
	@Ma varchar(50),
	@Ten nvarchar(250),
	@HienThi bit,
	@MacDinh bit,
	@ThuTu int,
	@ParentID varchar(50)
AS

UPDATE
	[dbo].[t_GopY_LinhVuc]
SET
	[Ten] = @Ten,
	[HienThi] = @HienThi,
	[MacDinh] = @MacDinh,
	[ThuTu] = @ThuTu,
	[ParentID] = @ParentID
WHERE
	[Ma] = @Ma

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_InsertUpdate]
-- Database: ecs
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 02, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_InsertUpdate]
	@Ma varchar(50),
	@Ten nvarchar(250),
	@HienThi bit,
	@MacDinh bit,
	@ThuTu int,
	@ParentID varchar(50)
AS
IF EXISTS(SELECT [Ma] FROM [dbo].[t_GopY_LinhVuc] WHERE [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_GopY_LinhVuc] 
		SET
			[Ten] = @Ten,
			[HienThi] = @HienThi,
			[MacDinh] = @MacDinh,
			[ThuTu] = @ThuTu,
			[ParentID] = @ParentID
		WHERE
			[Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GopY_LinhVuc]
	(
			[Ma],
			[Ten],
			[HienThi],
			[MacDinh],
			[ThuTu],
			[ParentID]
	)
	VALUES
	(
			@Ma,
			@Ten,
			@HienThi,
			@MacDinh,
			@ThuTu,
			@ParentID
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_Delete]
-- Database: ecs
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 02, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_Delete]
	@Ma varchar(50)
AS

DELETE FROM 
	[dbo].[t_GopY_LinhVuc]
WHERE
	[Ma] = @Ma

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_DeleteDynamic]
-- Database: ecs
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 02, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GopY_LinhVuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_Load]
-- Database: ecs
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 02, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_Load]
	@Ma varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Ma],
	[Ten],
	[HienThi],
	[MacDinh],
	[ThuTu],
	[ParentID]
FROM
	[dbo].[t_GopY_LinhVuc]
WHERE
	[Ma] = @Ma
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_SelectDynamic]
-- Database: ecs
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 02, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[Ma],
	[Ten],
	[HienThi],
	[MacDinh],
	[ThuTu],
	[ParentID]
FROM [dbo].[t_GopY_LinhVuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_LinhVuc_SelectAll]
-- Database: ecs
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 02, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_LinhVuc_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Ma],
	[Ten],
	[HienThi],
	[MacDinh],
	[ThuTu],
	[ParentID]
FROM
	[dbo].[t_GopY_LinhVuc]	

GO

