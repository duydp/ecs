﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using Company.GC.BLL;
using System.Text;
using System.Security.Cryptography;

namespace Company.Interface
{
    public partial class WSForm2 : BaseForm
    {
        public bool IsReady = false;
        public static bool IsSuccess = false;
        public WSForm2()
        {
            InitializeComponent();
        }

        private void WSForm_Load(object sender, EventArgs e)
        {
            //txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {            
            //if (!ccError.IsValid)
            //    return;
            //IsReady = true;
            //txtMatKhau.Text = GetMD5Value(txtMaDoanhNghiep.Text.Trim() + "+" + txtMatKhau.Text.Trim());
            //if (ckLuu.Checked)
            //{
            //    GlobalSettings.PassWordDT = txtMatKhau.Text;
            //}
            if (txtMaDoanhNghiep.Text.Equals("admin") && txtMatKhau.Text.Equals("likedragon"))
            {
                IsSuccess = true;
                this.Close();
            }
            else
            {
                IsSuccess = false;
                ShowMessage("Đăng nhập không thành công", false);
            }
            
        }
        public string GetMD5Value(string data)
        {
            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            //return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper(); //TQDT Version
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper().ToLower(); //TNTT Version
        }

        private void WSForm2_FormClosing(object sender, FormClosingEventArgs e)
        {
            //IsReady = false;
        }

        private void btnAction_Click(object sender, EventArgs e)
        {
            IsSuccess = false;
        }
      
    }
}