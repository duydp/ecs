﻿namespace Company.Interface.Report.GC
{
    partial class TQDTPhuLucToKhaiXuat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TQDTPhuLucToKhaiXuat));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine17 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine18 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHaiQuanCuaKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThueXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThueGTGT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaThuKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongbangso = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongThueXNKChu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine16 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine15 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThongBaoMienThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThongBaoMienThueGTGT = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.BackColor = System.Drawing.Color.Transparent;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrTable2,
            this.xrLine16,
            this.xrLine15,
            this.xrLine14,
            this.xrLine13,
            this.xrLine12,
            this.xrLine11,
            this.xrLine10,
            this.xrLabel9,
            this.xrLine9,
            this.xrLine1,
            this.xrLine2,
            this.xrLine3,
            this.xrLine4,
            this.xrLine5,
            this.xrLine6,
            this.xrLine7,
            this.xrLine8,
            this.xrLabel7,
            this.winControlContainer1,
            this.xrLabel6,
            this.xrLabel8,
            this.lblThongBaoMienThue,
            this.lblThongBaoMienThueGTGT});
            this.Detail.Height = 2301;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Location = new System.Drawing.Point(20, 60);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27,
            this.xrTableRow20,
            this.xrTableRow19,
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow21});
            this.xrTable1.Size = new System.Drawing.Size(772, 467);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.13376006636881588;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5,
            this.xrLabel10,
            this.xrLine17,
            this.xrLabel11,
            this.lblChiCucHaiQuan,
            this.xrLine18,
            this.xrLabel13,
            this.xrLabel16,
            this.xrLabel19,
            this.lblNgayDangKy,
            this.lblSoToKhai,
            this.xrLabel1,
            this.xrLabel3,
            this.lblChiCucHaiQuanCuaKhau});
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Text = "xrTableCell26";
            this.xrTableCell26.Weight = 1;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.Location = new System.Drawing.Point(600, 6);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.Size = new System.Drawing.Size(83, 17);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "Số tờ khai:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Location = new System.Drawing.Point(9, 3);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.Size = new System.Drawing.Size(100, 17);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.Text = "Chi cục hải quan:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine17
            // 
            this.xrLine17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine17.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine17.Location = new System.Drawing.Point(358, 0);
            this.xrLine17.Name = "xrLine17";
            this.xrLine17.Size = new System.Drawing.Size(17, 75);
            this.xrLine17.StylePriority.UseBorders = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.Location = new System.Drawing.Point(8, 23);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.Size = new System.Drawing.Size(175, 17);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.Text = "Chi cục hải quan cửa khẩu";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblChiCucHaiQuan
            // 
            this.lblChiCucHaiQuan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblChiCucHaiQuan.Location = new System.Drawing.Point(117, 6);
            this.lblChiCucHaiQuan.Name = "lblChiCucHaiQuan";
            this.lblChiCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHaiQuan.Size = new System.Drawing.Size(233, 17);
            this.lblChiCucHaiQuan.StylePriority.UseBorders = false;
            this.lblChiCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine18
            // 
            this.xrLine18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine18.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine18.Location = new System.Drawing.Point(583, 0);
            this.xrLine18.Name = "xrLine18";
            this.xrLine18.Size = new System.Drawing.Size(17, 75);
            this.xrLine18.StylePriority.UseBorders = false;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.Location = new System.Drawing.Point(600, 36);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.Size = new System.Drawing.Size(75, 17);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.Text = "Loại hình :";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel16.Location = new System.Drawing.Point(375, 8);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.Size = new System.Drawing.Size(75, 17);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Phụ lục số:";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.Location = new System.Drawing.Point(375, 35);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.Size = new System.Drawing.Size(125, 17);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.Text = "Ngày,giờ đăng ký:";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayDangKy.Location = new System.Drawing.Point(483, 34);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.Size = new System.Drawing.Size(100, 17);
            this.lblNgayDangKy.StylePriority.UseBorders = false;
            this.lblNgayDangKy.Text = "10/10/2010 00:00:00";
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSoToKhai.CanGrow = false;
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblSoToKhai.Location = new System.Drawing.Point(675, 6);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.Size = new System.Drawing.Size(83, 17);
            this.lblSoToKhai.StylePriority.UseBorders = false;
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.Text = "361";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.Location = new System.Drawing.Point(450, 7);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(83, 17);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.Location = new System.Drawing.Point(675, 36);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.Size = new System.Drawing.Size(67, 17);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.Tag = "Mã loại hình";
            this.xrLabel3.Text = "NKD01";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblChiCucHaiQuanCuaKhau
            // 
            this.lblChiCucHaiQuanCuaKhau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblChiCucHaiQuanCuaKhau.Location = new System.Drawing.Point(33, 40);
            this.lblChiCucHaiQuanCuaKhau.Name = "lblChiCucHaiQuanCuaKhau";
            this.lblChiCucHaiQuanCuaKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHaiQuanCuaKhau.Size = new System.Drawing.Size(312, 17);
            this.lblChiCucHaiQuanCuaKhau.StylePriority.UseBorders = false;
            this.lblChiCucHaiQuanCuaKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow20.Weight = 0.061975459103121626;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell25.Text = "PHẦN THÔNG TIN HÀNG HÓA";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell25.Weight = 1;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow19.Weight = 0.10940919037199125;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.Text = "Số \r\nTT";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell10.Weight = 0.052903225806451612;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.Text = "17. TÊN HÀNG \r\nQUY CÁCH PHẨM CHẤT";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.36129032258064514;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Multiline = true;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell12.Text = "18. MÃ SỐ\r\nHÀNG HÓA";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.10709677419354839;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell13.Text = "19. XUẤT XỨ";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.07483870967741936;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell14.Text = "20. SỐ LƯỢNG";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 0.11096774193548387;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.Text = "21. ĐƠN VỊ TÍNH";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.076129032258064513;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell16.Text = "22. ĐƠN GIÁ NGUYÊN TỆ";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.10325087748621092;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell17.Text = "23. TRỊ GIÁ NGUYÊN TỆ";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell17.Weight = 0.11352331606217617;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.TenHang1,
            this.MaHS1,
            this.XuatXu1,
            this.Luong1,
            this.DVT1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 0.0682949290048569;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.Text = "1";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.052903225806451612;
            // 
            // TenHang1
            // 
            this.TenHang1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang1.Tag = "Tên hàng 1";
            this.TenHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang1.Weight = 0.36129032258064514;
            this.TenHang1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick_1);
            // 
            // MaHS1
            // 
            this.MaHS1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS1.Weight = 0.10709677419354839;
            // 
            // XuatXu1
            // 
            this.XuatXu1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu1.Name = "XuatXu1";
            this.XuatXu1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu1.Weight = 0.07483870967741936;
            // 
            // Luong1
            // 
            this.Luong1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong1.Weight = 0.11096774193548387;
            // 
            // DVT1
            // 
            this.DVT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT1.Weight = 0.076129032258064513;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT1.Weight = 0.10325087748621092;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.Tag = "Trị giá NT 1";
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT1.Weight = 0.11352331606217617;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.TenHang2,
            this.MaHS2,
            this.XuatXu2,
            this.Luong2,
            this.DVT2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 0.068294929004856883;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.Text = "2";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.052903225806451612;
            // 
            // TenHang2
            // 
            this.TenHang2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang2.Tag = "Tên hàng 2";
            this.TenHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang2.Weight = 0.36129032258064514;
            this.TenHang2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang2_PreviewClick);
            // 
            // MaHS2
            // 
            this.MaHS2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS2.Weight = 0.10709677419354839;
            // 
            // XuatXu2
            // 
            this.XuatXu2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu2.Name = "XuatXu2";
            this.XuatXu2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu2.Weight = 0.07483870967741936;
            // 
            // Luong2
            // 
            this.Luong2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong2.Weight = 0.11096774193548387;
            // 
            // DVT2
            // 
            this.DVT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT2.Weight = 0.076129032258064513;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT2.Weight = 0.10325087748621092;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.Tag = "Trị giá NT 2";
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT2.Weight = 0.11352331606217617;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.TenHang3,
            this.MaHS3,
            this.XuatXu3,
            this.Luong3,
            this.DVT3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 0.068294929004856883;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.Text = "3";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.052903225806451612;
            // 
            // TenHang3
            // 
            this.TenHang3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang3.Tag = "Tên hàng 3";
            this.TenHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang3.Weight = 0.36129032258064514;
            this.TenHang3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang3_PreviewClick);
            // 
            // MaHS3
            // 
            this.MaHS3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS3.Weight = 0.10709677419354839;
            // 
            // XuatXu3
            // 
            this.XuatXu3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu3.Name = "XuatXu3";
            this.XuatXu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu3.Weight = 0.07483870967741936;
            // 
            // Luong3
            // 
            this.Luong3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong3.Weight = 0.11096774193548387;
            // 
            // DVT3
            // 
            this.DVT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT3.Weight = 0.076129032258064513;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT3.Weight = 0.10325087748621092;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.Tag = "Trị giá NT 3";
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT3.Weight = 0.11352331606217617;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.TenHang4,
            this.MaHS4,
            this.XuatXu4,
            this.Luong4,
            this.DVT4,
            this.DonGiaNT4,
            this.TriGiaNT4});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 0.0682949290048569;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.Text = "4";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 0.052903225806451612;
            // 
            // TenHang4
            // 
            this.TenHang4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang4.Name = "TenHang4";
            this.TenHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang4.Tag = "Tên hàng 4";
            this.TenHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang4.Weight = 0.36129032258064514;
            this.TenHang4.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang4_PreviewClick);
            // 
            // MaHS4
            // 
            this.MaHS4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS4.Name = "MaHS4";
            this.MaHS4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS4.Weight = 0.10709677419354839;
            // 
            // XuatXu4
            // 
            this.XuatXu4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu4.Name = "XuatXu4";
            this.XuatXu4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu4.Weight = 0.07483870967741936;
            // 
            // Luong4
            // 
            this.Luong4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong4.Name = "Luong4";
            this.Luong4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong4.Weight = 0.11096774193548387;
            // 
            // DVT4
            // 
            this.DVT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT4.Name = "DVT4";
            this.DVT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT4.Weight = 0.076129032258064513;
            // 
            // DonGiaNT4
            // 
            this.DonGiaNT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT4.Name = "DonGiaNT4";
            this.DonGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT4.Weight = 0.10325087748621092;
            // 
            // TriGiaNT4
            // 
            this.TriGiaNT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT4.Name = "TriGiaNT4";
            this.TriGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT4.Tag = "Trị giá NT 4";
            this.TriGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT4.Weight = 0.11352331606217617;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.TenHang5,
            this.MaHS5,
            this.XuatXu5,
            this.Luong5,
            this.DVT5,
            this.DonGiaNT5,
            this.TriGiaNT5});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 0.068294929004856883;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.Text = "5";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.052903225806451612;
            // 
            // TenHang5
            // 
            this.TenHang5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang5.Name = "TenHang5";
            this.TenHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang5.Tag = "Tên hàng 5";
            this.TenHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang5.Weight = 0.36129032258064514;
            this.TenHang5.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang5_PreviewClick);
            // 
            // MaHS5
            // 
            this.MaHS5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS5.Name = "MaHS5";
            this.MaHS5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS5.Weight = 0.10709677419354839;
            // 
            // XuatXu5
            // 
            this.XuatXu5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu5.Name = "XuatXu5";
            this.XuatXu5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu5.Weight = 0.07483870967741936;
            // 
            // Luong5
            // 
            this.Luong5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong5.Name = "Luong5";
            this.Luong5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong5.Weight = 0.11096774193548387;
            // 
            // DVT5
            // 
            this.DVT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT5.Name = "DVT5";
            this.DVT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT5.Weight = 0.076129032258064513;
            // 
            // DonGiaNT5
            // 
            this.DonGiaNT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT5.Name = "DonGiaNT5";
            this.DonGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT5.Weight = 0.10325087748621092;
            // 
            // TriGiaNT5
            // 
            this.TriGiaNT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT5.Name = "TriGiaNT5";
            this.TriGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT5.Tag = "Trị giá NT 5";
            this.TriGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT5.Weight = 0.11352331606217617;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.TenHang6,
            this.MaHS6,
            this.XuatXu6,
            this.Luong6,
            this.DVT6,
            this.DonGiaNT6,
            this.TriGiaNT6});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 0.068294929004856883;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.Text = "6";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 0.052903225806451612;
            // 
            // TenHang6
            // 
            this.TenHang6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang6.Name = "TenHang6";
            this.TenHang6.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang6.Tag = "Tên hàng 6";
            this.TenHang6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang6.Weight = 0.36129032258064514;
            this.TenHang6.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang6_PreviewClick);
            // 
            // MaHS6
            // 
            this.MaHS6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS6.Name = "MaHS6";
            this.MaHS6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS6.Weight = 0.10709677419354839;
            // 
            // XuatXu6
            // 
            this.XuatXu6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu6.Name = "XuatXu6";
            this.XuatXu6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu6.Weight = 0.07483870967741936;
            // 
            // Luong6
            // 
            this.Luong6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong6.Name = "Luong6";
            this.Luong6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong6.Weight = 0.11096774193548387;
            // 
            // DVT6
            // 
            this.DVT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT6.Name = "DVT6";
            this.DVT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT6.Weight = 0.076129032258064513;
            // 
            // DonGiaNT6
            // 
            this.DonGiaNT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT6.Name = "DonGiaNT6";
            this.DonGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT6.Weight = 0.10325087748621092;
            // 
            // TriGiaNT6
            // 
            this.TriGiaNT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT6.Name = "TriGiaNT6";
            this.TriGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT6.Tag = "Trị giá NT 6";
            this.TriGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT6.Weight = 0.11352331606217617;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.TenHang7,
            this.MaHS7,
            this.XuatXu7,
            this.Luong7,
            this.DVT7,
            this.DonGiaNT7,
            this.TriGiaNT7});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.068294929004856883;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.Text = "7";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 0.052903225806451612;
            // 
            // TenHang7
            // 
            this.TenHang7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang7.Name = "TenHang7";
            this.TenHang7.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang7.Tag = "Tên hàng 7";
            this.TenHang7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang7.Weight = 0.36129032258064514;
            this.TenHang7.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang7_PreviewClick);
            // 
            // MaHS7
            // 
            this.MaHS7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS7.Name = "MaHS7";
            this.MaHS7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS7.Weight = 0.10709677419354839;
            // 
            // XuatXu7
            // 
            this.XuatXu7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu7.Name = "XuatXu7";
            this.XuatXu7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu7.Weight = 0.07483870967741936;
            // 
            // Luong7
            // 
            this.Luong7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong7.Name = "Luong7";
            this.Luong7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong7.Weight = 0.11096774193548387;
            // 
            // DVT7
            // 
            this.DVT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT7.Name = "DVT7";
            this.DVT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT7.Weight = 0.076129032258064513;
            // 
            // DonGiaNT7
            // 
            this.DonGiaNT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT7.Name = "DonGiaNT7";
            this.DonGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT7.Weight = 0.10325087748621092;
            // 
            // TriGiaNT7
            // 
            this.TriGiaNT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT7.Name = "TriGiaNT7";
            this.TriGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT7.Tag = "Trị giá NT 7";
            this.TriGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT7.Weight = 0.11352331606217617;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.TenHang8,
            this.MaHS8,
            this.XuatXu8,
            this.Luong8,
            this.DVT8,
            this.DonGiaNT8,
            this.TriGiaNT8});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 0.068294929004856855;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.Text = "8";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.052903225806451612;
            // 
            // TenHang8
            // 
            this.TenHang8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang8.Name = "TenHang8";
            this.TenHang8.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang8.Tag = "Tên hàng 8";
            this.TenHang8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang8.Weight = 0.36129032258064514;
            this.TenHang8.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang8_PreviewClick);
            // 
            // MaHS8
            // 
            this.MaHS8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS8.Name = "MaHS8";
            this.MaHS8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS8.Weight = 0.10709677419354839;
            // 
            // XuatXu8
            // 
            this.XuatXu8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu8.Name = "XuatXu8";
            this.XuatXu8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu8.Weight = 0.07483870967741936;
            // 
            // Luong8
            // 
            this.Luong8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong8.Name = "Luong8";
            this.Luong8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong8.Weight = 0.11096774193548387;
            // 
            // DVT8
            // 
            this.DVT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT8.Name = "DVT8";
            this.DVT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT8.Weight = 0.076129032258064513;
            // 
            // DonGiaNT8
            // 
            this.DonGiaNT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT8.Name = "DonGiaNT8";
            this.DonGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT8.Weight = 0.10325087748621092;
            // 
            // TriGiaNT8
            // 
            this.TriGiaNT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT8.Name = "TriGiaNT8";
            this.TriGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT8.Tag = "Trị giá NT 8";
            this.TriGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT8.Weight = 0.11352331606217617;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.TenHang9,
            this.MaHS9,
            this.XuatXu9,
            this.Luong9,
            this.DVT9,
            this.DonGiaNT9,
            this.TriGiaNT9});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 0.0682949290048569;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.Text = "9";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell9.Weight = 0.052903225806451612;
            // 
            // TenHang9
            // 
            this.TenHang9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang9.Name = "TenHang9";
            this.TenHang9.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.TenHang9.Tag = "Tên hàng 9";
            this.TenHang9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang9.Weight = 0.36129032258064514;
            this.TenHang9.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang9_PreviewClick);
            // 
            // MaHS9
            // 
            this.MaHS9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS9.Name = "MaHS9";
            this.MaHS9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS9.Weight = 0.10709677419354839;
            // 
            // XuatXu9
            // 
            this.XuatXu9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.XuatXu9.Name = "XuatXu9";
            this.XuatXu9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu9.Weight = 0.07483870967741936;
            // 
            // Luong9
            // 
            this.Luong9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong9.Name = "Luong9";
            this.Luong9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong9.Weight = 0.11096774193548387;
            // 
            // DVT9
            // 
            this.DVT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT9.Name = "DVT9";
            this.DVT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT9.Weight = 0.076129032258064513;
            // 
            // DonGiaNT9
            // 
            this.DonGiaNT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT9.Name = "DonGiaNT9";
            this.DonGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT9.Weight = 0.10325087748621092;
            // 
            // TriGiaNT9
            // 
            this.TriGiaNT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT9.Name = "TriGiaNT9";
            this.TriGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT9.Tag = "Trị giá NT 9";
            this.TriGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT9.Weight = 0.11352331606217617;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.lblTongTriGiaNT});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow21.Weight = 0.066183617026454117;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell18.Weight = 0.052903225806451612;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell19.Text = "Cộng:";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 0.36129032258064514;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell20.Weight = 0.10709677419354839;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell21.Weight = 0.07483870967741936;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 0.11096774193548387;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell23.Weight = 0.076129032258064513;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell24.Weight = 0.10325087748621092;
            // 
            // lblTongTriGiaNT
            // 
            this.lblTongTriGiaNT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTriGiaNT.Name = "lblTongTriGiaNT";
            this.lblTongTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaNT.Tag = "Trị giá NT";
            this.lblTongTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTriGiaNT.Weight = 0.11352331606217617;
            // 
            // xrTable2
            // 
            this.xrTable2.Location = new System.Drawing.Point(20, 527);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow24,
            this.xrTableRow28,
            this.xrTableRow25,
            this.xrTableRow26});
            this.xrTable2.Size = new System.Drawing.Size(772, 590);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell84,
            this.xrTableCell87});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow22.Weight = 0.06569343065693431;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell80.Text = "SỐ";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell80.Weight = 0.052903225806451612;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell81.Text = "24. TIỀN THUẾ XUẤT KHẨU";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell81.Weight = 0.36258565936820991;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell84.Text = "25. TIỀN THUẾ GTGT (HOẶC TTĐB)";
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell84.Weight = 0.37161791743272604;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell87.Text = "26. THU KHÁC";
            this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell87.Weight = 0.21289319739261239;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow23.Weight = 0.063868613138686137;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell89.Text = "TT";
            this.xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell89.Weight = 0.052903225806451612;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell90.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell90.Multiline = true;
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell90.Text = "Trị giá tính thuế \r\n(VNĐ)";
            this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell90.Weight = 0.15096774193548387;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell91.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell91.Multiline = true;
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell91.Text = "Thuế\r\nsuất (%)";
            this.xrTableCell91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell91.Weight = 0.064516129032258063;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell92.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell92.Text = "Tiền thuế";
            this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell92.Weight = 0.14580645161290323;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell93.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell93.Text = "Trị giá tính thuế";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell93.Weight = 0.15483870967741936;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell94.Multiline = true;
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell94.Text = "Thuế\r\nsuất (%)";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell94.Weight = 0.064516129032258063;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell95.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell95.Text = "Tiền thuế";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell95.Weight = 0.15096774193548387;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell96.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell96.Multiline = true;
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell96.Text = "Tỷ lệ\r\n(%)";
            this.xrTableCell96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell96.Weight = 0.064516129032258063;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell97.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell97.Text = "Số tiền";
            this.xrTableCell97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell97.Weight = 0.15096774193548387;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.TriGiaTT1,
            this.ThueSuatXNK1,
            this.TienThueXNK1,
            this.TriGiaTTGTGT1,
            this.ThueSuatGTGT1,
            this.TienThueGTGT1,
            this.TyLeThuKhac1,
            this.TriGiaThuKhac1});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 0.0583941605839416;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell35.Text = "1";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell35.Weight = 0.052903225806451612;
            // 
            // TriGiaTT1
            // 
            this.TriGiaTT1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.TriGiaTT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT1.Name = "TriGiaTT1";
            this.TriGiaTT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT1.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK1
            // 
            this.ThueSuatXNK1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK1.Name = "ThueSuatXNK1";
            this.ThueSuatXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK1.Weight = 0.064516129032258063;
            // 
            // TienThueXNK1
            // 
            this.TienThueXNK1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK1.Name = "TienThueXNK1";
            this.TienThueXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK1.Weight = 0.14580645161290323;
            // 
            // TriGiaTTGTGT1
            // 
            this.TriGiaTTGTGT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTTGTGT1.KeepTogether = true;
            this.TriGiaTTGTGT1.Name = "TriGiaTTGTGT1";
            this.TriGiaTTGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT1.StylePriority.UseTextAlignment = false;
            this.TriGiaTTGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT1.Weight = 0.15483870967741936;
            // 
            // ThueSuatGTGT1
            // 
            this.ThueSuatGTGT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatGTGT1.KeepTogether = true;
            this.ThueSuatGTGT1.Name = "ThueSuatGTGT1";
            this.ThueSuatGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT1.StylePriority.UseTextAlignment = false;
            this.ThueSuatGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT1.Weight = 0.064516129032258063;
            // 
            // TienThueGTGT1
            // 
            this.TienThueGTGT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueGTGT1.KeepTogether = true;
            this.TienThueGTGT1.Name = "TienThueGTGT1";
            this.TienThueGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT1.StylePriority.UseTextAlignment = false;
            this.TienThueGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT1.Weight = 0.15096774193548387;
            // 
            // TyLeThuKhac1
            // 
            this.TyLeThuKhac1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TyLeThuKhac1.Name = "TyLeThuKhac1";
            this.TyLeThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac1.StylePriority.UseTextAlignment = false;
            this.TyLeThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac1.Weight = 0.064516129032258063;
            // 
            // TriGiaThuKhac1
            // 
            this.TriGiaThuKhac1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaThuKhac1.Name = "TriGiaThuKhac1";
            this.TriGiaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac1.StylePriority.UseTextAlignment = false;
            this.TriGiaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac1.Weight = 0.15096774193548387;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.TriGiaTT2,
            this.ThueSuatXNK2,
            this.TienThueXNK2,
            this.TriGiaTTGTGT2,
            this.ThueSuatGTGT2,
            this.TienThueGTGT2,
            this.TyLeThuKhac2,
            this.TriGiaThuKhac2});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow11.Weight = 0.058394160583941604;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell36.Text = "2";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell36.Weight = 0.052903225806451612;
            // 
            // TriGiaTT2
            // 
            this.TriGiaTT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT2.Name = "TriGiaTT2";
            this.TriGiaTT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT2.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK2
            // 
            this.ThueSuatXNK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK2.Name = "ThueSuatXNK2";
            this.ThueSuatXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK2.Weight = 0.064516129032258063;
            // 
            // TienThueXNK2
            // 
            this.TienThueXNK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK2.Name = "TienThueXNK2";
            this.TienThueXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK2.Weight = 0.14580645161290323;
            // 
            // TriGiaTTGTGT2
            // 
            this.TriGiaTTGTGT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTTGTGT2.KeepTogether = true;
            this.TriGiaTTGTGT2.Name = "TriGiaTTGTGT2";
            this.TriGiaTTGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT2.StylePriority.UseTextAlignment = false;
            this.TriGiaTTGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT2.Weight = 0.15483870967741936;
            // 
            // ThueSuatGTGT2
            // 
            this.ThueSuatGTGT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatGTGT2.KeepTogether = true;
            this.ThueSuatGTGT2.Name = "ThueSuatGTGT2";
            this.ThueSuatGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT2.StylePriority.UseTextAlignment = false;
            this.ThueSuatGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT2.Weight = 0.064516129032258063;
            // 
            // TienThueGTGT2
            // 
            this.TienThueGTGT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueGTGT2.KeepTogether = true;
            this.TienThueGTGT2.Name = "TienThueGTGT2";
            this.TienThueGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT2.StylePriority.UseTextAlignment = false;
            this.TienThueGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT2.Weight = 0.15096774193548387;
            // 
            // TyLeThuKhac2
            // 
            this.TyLeThuKhac2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TyLeThuKhac2.Name = "TyLeThuKhac2";
            this.TyLeThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac2.StylePriority.UseTextAlignment = false;
            this.TyLeThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac2.Weight = 0.064516129032258063;
            // 
            // TriGiaThuKhac2
            // 
            this.TriGiaThuKhac2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaThuKhac2.Name = "TriGiaThuKhac2";
            this.TriGiaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac2.StylePriority.UseTextAlignment = false;
            this.TriGiaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac2.Weight = 0.15096774193548387;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.TriGiaTT3,
            this.ThueSuatXNK3,
            this.TienThueXNK3,
            this.TriGiaTTGTGT3,
            this.ThueSuatGTGT3,
            this.TienThueGTGT3,
            this.TyLeThuKhac3,
            this.TriGiaThuKhac3});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow12.Weight = 0.0583941605839416;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell37.Text = "3";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell37.Weight = 0.052903225806451612;
            // 
            // TriGiaTT3
            // 
            this.TriGiaTT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT3.Name = "TriGiaTT3";
            this.TriGiaTT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT3.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK3
            // 
            this.ThueSuatXNK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK3.Name = "ThueSuatXNK3";
            this.ThueSuatXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK3.Weight = 0.064516129032258063;
            // 
            // TienThueXNK3
            // 
            this.TienThueXNK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK3.Name = "TienThueXNK3";
            this.TienThueXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK3.Weight = 0.14580645161290323;
            // 
            // TriGiaTTGTGT3
            // 
            this.TriGiaTTGTGT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTTGTGT3.KeepTogether = true;
            this.TriGiaTTGTGT3.Name = "TriGiaTTGTGT3";
            this.TriGiaTTGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT3.StylePriority.UseTextAlignment = false;
            this.TriGiaTTGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT3.Weight = 0.15483870967741936;
            // 
            // ThueSuatGTGT3
            // 
            this.ThueSuatGTGT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatGTGT3.KeepTogether = true;
            this.ThueSuatGTGT3.Name = "ThueSuatGTGT3";
            this.ThueSuatGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT3.StylePriority.UseTextAlignment = false;
            this.ThueSuatGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT3.Weight = 0.064516129032258063;
            // 
            // TienThueGTGT3
            // 
            this.TienThueGTGT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueGTGT3.KeepTogether = true;
            this.TienThueGTGT3.Name = "TienThueGTGT3";
            this.TienThueGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT3.StylePriority.UseTextAlignment = false;
            this.TienThueGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT3.Weight = 0.15096774193548387;
            // 
            // TyLeThuKhac3
            // 
            this.TyLeThuKhac3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TyLeThuKhac3.Name = "TyLeThuKhac3";
            this.TyLeThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac3.StylePriority.UseTextAlignment = false;
            this.TyLeThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac3.Weight = 0.064516129032258063;
            // 
            // TriGiaThuKhac3
            // 
            this.TriGiaThuKhac3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaThuKhac3.Name = "TriGiaThuKhac3";
            this.TriGiaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac3.StylePriority.UseTextAlignment = false;
            this.TriGiaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac3.Weight = 0.15096774193548387;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.TriGiaTT4,
            this.ThueSuatXNK4,
            this.TienThueXNK4,
            this.TriGiaTTGTGT4,
            this.ThueSuatGTGT4,
            this.TienThueGTGT4,
            this.TyLeThuKhac4,
            this.TriGiaThuKhac4});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow13.Weight = 0.0583941605839416;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell38.KeepTogether = true;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell38.Text = "4";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell38.Weight = 0.052903225806451612;
            // 
            // TriGiaTT4
            // 
            this.TriGiaTT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT4.KeepTogether = true;
            this.TriGiaTT4.Name = "TriGiaTT4";
            this.TriGiaTT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT4.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK4
            // 
            this.ThueSuatXNK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK4.KeepTogether = true;
            this.ThueSuatXNK4.Name = "ThueSuatXNK4";
            this.ThueSuatXNK4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK4.Weight = 0.064516129032258063;
            // 
            // TienThueXNK4
            // 
            this.TienThueXNK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK4.KeepTogether = true;
            this.TienThueXNK4.Name = "TienThueXNK4";
            this.TienThueXNK4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK4.Weight = 0.14580645161290323;
            // 
            // TriGiaTTGTGT4
            // 
            this.TriGiaTTGTGT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTTGTGT4.Name = "TriGiaTTGTGT4";
            this.TriGiaTTGTGT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT4.StylePriority.UseTextAlignment = false;
            this.TriGiaTTGTGT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT4.Weight = 0.15483870967741936;
            // 
            // ThueSuatGTGT4
            // 
            this.ThueSuatGTGT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatGTGT4.Name = "ThueSuatGTGT4";
            this.ThueSuatGTGT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT4.StylePriority.UseTextAlignment = false;
            this.ThueSuatGTGT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT4.Weight = 0.064516129032258063;
            // 
            // TienThueGTGT4
            // 
            this.TienThueGTGT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueGTGT4.Name = "TienThueGTGT4";
            this.TienThueGTGT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT4.StylePriority.UseTextAlignment = false;
            this.TienThueGTGT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT4.Weight = 0.15096774193548387;
            // 
            // TyLeThuKhac4
            // 
            this.TyLeThuKhac4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TyLeThuKhac4.Name = "TyLeThuKhac4";
            this.TyLeThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac4.StylePriority.UseTextAlignment = false;
            this.TyLeThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac4.Weight = 0.064516129032258063;
            // 
            // TriGiaThuKhac4
            // 
            this.TriGiaThuKhac4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaThuKhac4.Name = "TriGiaThuKhac4";
            this.TriGiaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac4.StylePriority.UseTextAlignment = false;
            this.TriGiaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac4.Weight = 0.15096774193548387;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.TriGiaTT5,
            this.ThueSuatXNK5,
            this.TienThueXNK5,
            this.TriGiaTTGTGT5,
            this.ThueSuatGTGT5,
            this.TienThueGTGT5,
            this.TyLeThuKhac5,
            this.TriGiaThuKhac5});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow14.Weight = 0.05839416058394159;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell39.KeepTogether = true;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell39.Text = "5";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell39.Weight = 0.052903225806451612;
            // 
            // TriGiaTT5
            // 
            this.TriGiaTT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT5.KeepTogether = true;
            this.TriGiaTT5.Name = "TriGiaTT5";
            this.TriGiaTT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT5.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK5
            // 
            this.ThueSuatXNK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK5.KeepTogether = true;
            this.ThueSuatXNK5.Name = "ThueSuatXNK5";
            this.ThueSuatXNK5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK5.Weight = 0.064516129032258063;
            // 
            // TienThueXNK5
            // 
            this.TienThueXNK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK5.KeepTogether = true;
            this.TienThueXNK5.Name = "TienThueXNK5";
            this.TienThueXNK5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK5.Weight = 0.14580645161290323;
            // 
            // TriGiaTTGTGT5
            // 
            this.TriGiaTTGTGT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTTGTGT5.Name = "TriGiaTTGTGT5";
            this.TriGiaTTGTGT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT5.StylePriority.UseTextAlignment = false;
            this.TriGiaTTGTGT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT5.Weight = 0.15483870967741936;
            // 
            // ThueSuatGTGT5
            // 
            this.ThueSuatGTGT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatGTGT5.Name = "ThueSuatGTGT5";
            this.ThueSuatGTGT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT5.StylePriority.UseTextAlignment = false;
            this.ThueSuatGTGT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT5.Weight = 0.064516129032258063;
            // 
            // TienThueGTGT5
            // 
            this.TienThueGTGT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueGTGT5.KeepTogether = true;
            this.TienThueGTGT5.Name = "TienThueGTGT5";
            this.TienThueGTGT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT5.StylePriority.UseTextAlignment = false;
            this.TienThueGTGT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT5.Weight = 0.15096774193548387;
            // 
            // TyLeThuKhac5
            // 
            this.TyLeThuKhac5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TyLeThuKhac5.Name = "TyLeThuKhac5";
            this.TyLeThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac5.StylePriority.UseTextAlignment = false;
            this.TyLeThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac5.Weight = 0.064516129032258063;
            // 
            // TriGiaThuKhac5
            // 
            this.TriGiaThuKhac5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaThuKhac5.Name = "TriGiaThuKhac5";
            this.TriGiaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac5.StylePriority.UseTextAlignment = false;
            this.TriGiaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac5.Weight = 0.15096774193548387;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.TriGiaTT6,
            this.ThueSuatXNK6,
            this.TienThueXNK6,
            this.TriGiaTTGTGT6,
            this.ThueSuatGTGT6,
            this.TienThueGTGT6,
            this.TyLeThuKhac6,
            this.TriGiaThuKhac6});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow15.Weight = 0.058394160583941618;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell40.Text = "6";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell40.Weight = 0.052903225806451612;
            // 
            // TriGiaTT6
            // 
            this.TriGiaTT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT6.Name = "TriGiaTT6";
            this.TriGiaTT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT6.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK6
            // 
            this.ThueSuatXNK6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK6.Name = "ThueSuatXNK6";
            this.ThueSuatXNK6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK6.Weight = 0.064516129032258063;
            // 
            // TienThueXNK6
            // 
            this.TienThueXNK6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK6.Name = "TienThueXNK6";
            this.TienThueXNK6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK6.Weight = 0.14580645161290323;
            // 
            // TriGiaTTGTGT6
            // 
            this.TriGiaTTGTGT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTTGTGT6.Name = "TriGiaTTGTGT6";
            this.TriGiaTTGTGT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT6.StylePriority.UseTextAlignment = false;
            this.TriGiaTTGTGT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT6.Weight = 0.15483870967741936;
            // 
            // ThueSuatGTGT6
            // 
            this.ThueSuatGTGT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatGTGT6.Name = "ThueSuatGTGT6";
            this.ThueSuatGTGT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT6.StylePriority.UseTextAlignment = false;
            this.ThueSuatGTGT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT6.Weight = 0.064516129032258063;
            // 
            // TienThueGTGT6
            // 
            this.TienThueGTGT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueGTGT6.Name = "TienThueGTGT6";
            this.TienThueGTGT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT6.StylePriority.UseTextAlignment = false;
            this.TienThueGTGT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT6.Weight = 0.15096774193548387;
            // 
            // TyLeThuKhac6
            // 
            this.TyLeThuKhac6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TyLeThuKhac6.Name = "TyLeThuKhac6";
            this.TyLeThuKhac6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac6.StylePriority.UseTextAlignment = false;
            this.TyLeThuKhac6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac6.Weight = 0.064516129032258063;
            // 
            // TriGiaThuKhac6
            // 
            this.TriGiaThuKhac6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaThuKhac6.Name = "TriGiaThuKhac6";
            this.TriGiaThuKhac6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac6.StylePriority.UseTextAlignment = false;
            this.TriGiaThuKhac6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac6.Weight = 0.15096774193548387;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.TriGiaTT7,
            this.ThueSuatXNK7,
            this.TienThueXNK7,
            this.TriGiaTTGTGT7,
            this.ThueSuatGTGT7,
            this.TienThueGTGT7,
            this.TyLeThuKhac7,
            this.TriGiaThuKhac7});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow16.Weight = 0.058394160583941604;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell41.Text = "7";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell41.Weight = 0.052903225806451612;
            // 
            // TriGiaTT7
            // 
            this.TriGiaTT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT7.Name = "TriGiaTT7";
            this.TriGiaTT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT7.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK7
            // 
            this.ThueSuatXNK7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK7.Name = "ThueSuatXNK7";
            this.ThueSuatXNK7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK7.Weight = 0.064516129032258063;
            // 
            // TienThueXNK7
            // 
            this.TienThueXNK7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK7.Name = "TienThueXNK7";
            this.TienThueXNK7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK7.Weight = 0.14580645161290323;
            // 
            // TriGiaTTGTGT7
            // 
            this.TriGiaTTGTGT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTTGTGT7.Name = "TriGiaTTGTGT7";
            this.TriGiaTTGTGT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT7.StylePriority.UseTextAlignment = false;
            this.TriGiaTTGTGT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT7.Weight = 0.15483870967741936;
            // 
            // ThueSuatGTGT7
            // 
            this.ThueSuatGTGT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatGTGT7.Name = "ThueSuatGTGT7";
            this.ThueSuatGTGT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT7.StylePriority.UseTextAlignment = false;
            this.ThueSuatGTGT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT7.Weight = 0.064516129032258063;
            // 
            // TienThueGTGT7
            // 
            this.TienThueGTGT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueGTGT7.Name = "TienThueGTGT7";
            this.TienThueGTGT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT7.StylePriority.UseTextAlignment = false;
            this.TienThueGTGT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT7.Weight = 0.15096774193548387;
            // 
            // TyLeThuKhac7
            // 
            this.TyLeThuKhac7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TyLeThuKhac7.Name = "TyLeThuKhac7";
            this.TyLeThuKhac7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac7.StylePriority.UseTextAlignment = false;
            this.TyLeThuKhac7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac7.Weight = 0.064516129032258063;
            // 
            // TriGiaThuKhac7
            // 
            this.TriGiaThuKhac7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaThuKhac7.Name = "TriGiaThuKhac7";
            this.TriGiaThuKhac7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac7.StylePriority.UseTextAlignment = false;
            this.TriGiaThuKhac7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac7.Weight = 0.15096774193548387;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.TriGiaTT8,
            this.ThueSuatXNK8,
            this.TienThueXNK8,
            this.TriGiaTTGTGT8,
            this.ThueSuatGTGT8,
            this.TienThueGTGT8,
            this.TyLeThuKhac8,
            this.TriGiaThuKhac8});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow17.Weight = 0.058394160583941604;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell42.Text = "8";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell42.Weight = 0.052903225806451612;
            // 
            // TriGiaTT8
            // 
            this.TriGiaTT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTT8.Name = "TriGiaTT8";
            this.TriGiaTT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT8.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK8
            // 
            this.ThueSuatXNK8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatXNK8.Name = "ThueSuatXNK8";
            this.ThueSuatXNK8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK8.Weight = 0.064516129032258063;
            // 
            // TienThueXNK8
            // 
            this.TienThueXNK8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueXNK8.Name = "TienThueXNK8";
            this.TienThueXNK8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK8.Weight = 0.14580645161290323;
            // 
            // TriGiaTTGTGT8
            // 
            this.TriGiaTTGTGT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaTTGTGT8.Name = "TriGiaTTGTGT8";
            this.TriGiaTTGTGT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT8.StylePriority.UseTextAlignment = false;
            this.TriGiaTTGTGT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT8.Weight = 0.15483870967741936;
            // 
            // ThueSuatGTGT8
            // 
            this.ThueSuatGTGT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ThueSuatGTGT8.Name = "ThueSuatGTGT8";
            this.ThueSuatGTGT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT8.StylePriority.UseTextAlignment = false;
            this.ThueSuatGTGT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT8.Weight = 0.064516129032258063;
            // 
            // TienThueGTGT8
            // 
            this.TienThueGTGT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TienThueGTGT8.Name = "TienThueGTGT8";
            this.TienThueGTGT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT8.StylePriority.UseTextAlignment = false;
            this.TienThueGTGT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT8.Weight = 0.15096774193548387;
            // 
            // TyLeThuKhac8
            // 
            this.TyLeThuKhac8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TyLeThuKhac8.Name = "TyLeThuKhac8";
            this.TyLeThuKhac8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac8.StylePriority.UseTextAlignment = false;
            this.TyLeThuKhac8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac8.Weight = 0.064516129032258063;
            // 
            // TriGiaThuKhac8
            // 
            this.TriGiaThuKhac8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaThuKhac8.Name = "TriGiaThuKhac8";
            this.TriGiaThuKhac8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac8.StylePriority.UseTextAlignment = false;
            this.TriGiaThuKhac8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac8.Weight = 0.15096774193548387;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.TriGiaTT9,
            this.ThueSuatXNK9,
            this.TienThueXNK9,
            this.TriGiaTTGTGT9,
            this.ThueSuatGTGT9,
            this.TienThueGTGT9,
            this.TyLeThuKhac9,
            this.TriGiaThuKhac9});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow18.Weight = 0.058394160583941618;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell43.Text = "9";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell43.Weight = 0.052903225806451612;
            // 
            // TriGiaTT9
            // 
            this.TriGiaTT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaTT9.Name = "TriGiaTT9";
            this.TriGiaTT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT9.Weight = 0.15096774193548387;
            // 
            // ThueSuatXNK9
            // 
            this.ThueSuatXNK9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ThueSuatXNK9.Name = "ThueSuatXNK9";
            this.ThueSuatXNK9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK9.Weight = 0.064516129032258063;
            // 
            // TienThueXNK9
            // 
            this.TienThueXNK9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TienThueXNK9.Name = "TienThueXNK9";
            this.TienThueXNK9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK9.Weight = 0.14580645161290323;
            // 
            // TriGiaTTGTGT9
            // 
            this.TriGiaTTGTGT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaTTGTGT9.Name = "TriGiaTTGTGT9";
            this.TriGiaTTGTGT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT9.StylePriority.UseTextAlignment = false;
            this.TriGiaTTGTGT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT9.Weight = 0.15483870967741936;
            // 
            // ThueSuatGTGT9
            // 
            this.ThueSuatGTGT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ThueSuatGTGT9.Name = "ThueSuatGTGT9";
            this.ThueSuatGTGT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT9.StylePriority.UseTextAlignment = false;
            this.ThueSuatGTGT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT9.Weight = 0.064516129032258063;
            // 
            // TienThueGTGT9
            // 
            this.TienThueGTGT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TienThueGTGT9.Name = "TienThueGTGT9";
            this.TienThueGTGT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT9.StylePriority.UseTextAlignment = false;
            this.TienThueGTGT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT9.Weight = 0.15096774193548387;
            // 
            // TyLeThuKhac9
            // 
            this.TyLeThuKhac9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TyLeThuKhac9.Name = "TyLeThuKhac9";
            this.TyLeThuKhac9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac9.StylePriority.UseTextAlignment = false;
            this.TyLeThuKhac9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac9.Weight = 0.064516129032258063;
            // 
            // TriGiaThuKhac9
            // 
            this.TriGiaThuKhac9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaThuKhac9.Name = "TriGiaThuKhac9";
            this.TriGiaThuKhac9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac9.StylePriority.UseTextAlignment = false;
            this.TriGiaThuKhac9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac9.Weight = 0.15096774193548387;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.lblTongTienThueXNK,
            this.xrTableCell88,
            this.xrTableCell98,
            this.lblTongTienThueGTGT,
            this.xrTableCell100,
            this.lblTongTriGiaThuKhac});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow24.Weight = 0.063868613138686137;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell85.Text = "Cộng:\t";
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell85.Weight = 0.26968243356175831;
            // 
            // lblTongTienThueXNK
            // 
            this.lblTongTienThueXNK.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTienThueXNK.Name = "lblTongTienThueXNK";
            this.lblTongTienThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTienThueXNK.Weight = 0.14580645161290323;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell88.StylePriority.UseTextAlignment = false;
            this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell88.Weight = 0.15483870967741936;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell98.StylePriority.UseTextAlignment = false;
            this.xrTableCell98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell98.Weight = 0.064516129032258063;
            // 
            // lblTongTienThueGTGT
            // 
            this.lblTongTienThueGTGT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTienThueGTGT.Name = "lblTongTienThueGTGT";
            this.lblTongTienThueGTGT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThueGTGT.StylePriority.UseTextAlignment = false;
            this.lblTongTienThueGTGT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTienThueGTGT.Weight = 0.15096774193548387;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell100.StylePriority.UseTextAlignment = false;
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell100.Weight = 0.064516129032258063;
            // 
            // lblTongTriGiaThuKhac
            // 
            this.lblTongTriGiaThuKhac.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTriGiaThuKhac.Name = "lblTongTriGiaThuKhac";
            this.lblTongTriGiaThuKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaThuKhac.StylePriority.UseTextAlignment = false;
            this.lblTongTriGiaThuKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTriGiaThuKhac.Weight = 0.1496724051479191;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.11861313868613141;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.lblTongbangso,
            this.xrLabel14,
            this.lblTongThueXNKChu});
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.Text = " ";
            this.xrTableCell27.Weight = 1;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Location = new System.Drawing.Point(17, 12);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.Size = new System.Drawing.Size(317, 17);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.Text = "30.Tổng số tiền thuế và thu khác(ô 24+25+26): Bằng số :";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongbangso
            // 
            this.lblTongbangso.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTongbangso.Location = new System.Drawing.Point(335, 12);
            this.lblTongbangso.Name = "lblTongbangso";
            this.lblTongbangso.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongbangso.Size = new System.Drawing.Size(317, 17);
            this.lblTongbangso.StylePriority.UseBorders = false;
            this.lblTongbangso.Text = " ";
            this.lblTongbangso.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Location = new System.Drawing.Point(17, 39);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.Size = new System.Drawing.Size(75, 17);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.Text = "Bằng chữ :";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongThueXNKChu
            // 
            this.lblTongThueXNKChu.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTongThueXNKChu.Location = new System.Drawing.Point(100, 39);
            this.lblTongThueXNKChu.Name = "lblTongThueXNKChu";
            this.lblTongThueXNKChu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueXNKChu.Size = new System.Drawing.Size(642, 17);
            this.lblTongThueXNKChu.StylePriority.UseBorders = false;
            this.lblTongThueXNKChu.Text = " ";
            this.lblTongThueXNKChu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell82});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow25.Weight = 0.14416058394160583;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell82.Text = "29. Tôi xin cam đoan, chịu trách nhiệm trước pháp luật về những nội dung khai báo" +
                " trên tờ khai này.";
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell82.Weight = 1;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow26.Weight = 0.094890510948905119;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.Text = "(Người khai ký, đóng dấu, ghi rõ họ tên, chức danh,ký tên và đóng dấu)";
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell106.Weight = 1;
            // 
            // xrLine16
            // 
            this.xrLine16.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine16.Location = new System.Drawing.Point(22, 842);
            this.xrLine16.Name = "xrLine16";
            this.xrLine16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine16.Size = new System.Drawing.Size(770, 25);
            // 
            // xrLine15
            // 
            this.xrLine15.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine15.Location = new System.Drawing.Point(22, 810);
            this.xrLine15.Name = "xrLine15";
            this.xrLine15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine15.Size = new System.Drawing.Size(770, 25);
            // 
            // xrLine14
            // 
            this.xrLine14.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine14.Location = new System.Drawing.Point(22, 783);
            this.xrLine14.Name = "xrLine14";
            this.xrLine14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine14.Size = new System.Drawing.Size(770, 17);
            // 
            // xrLine13
            // 
            this.xrLine13.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine13.Location = new System.Drawing.Point(22, 750);
            this.xrLine13.Name = "xrLine13";
            this.xrLine13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine13.Size = new System.Drawing.Size(770, 17);
            // 
            // xrLine12
            // 
            this.xrLine12.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine12.Location = new System.Drawing.Point(22, 719);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine12.Size = new System.Drawing.Size(770, 17);
            // 
            // xrLine11
            // 
            this.xrLine11.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine11.Location = new System.Drawing.Point(22, 685);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine11.Size = new System.Drawing.Size(770, 17);
            // 
            // xrLine10
            // 
            this.xrLine10.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine10.Location = new System.Drawing.Point(22, 652);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine10.Size = new System.Drawing.Size(770, 17);
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.BorderWidth = 2;
            this.xrLabel9.Location = new System.Drawing.Point(20, 58);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.Size = new System.Drawing.Size(772, 1075);
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine9
            // 
            this.xrLine9.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine9.Location = new System.Drawing.Point(22, 619);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine9.Size = new System.Drawing.Size(770, 25);
            // 
            // xrLine1
            // 
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine1.Location = new System.Drawing.Point(22, 458);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.Size = new System.Drawing.Size(770, 17);
            // 
            // xrLine2
            // 
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine2.Location = new System.Drawing.Point(22, 425);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine2.Size = new System.Drawing.Size(770, 25);
            // 
            // xrLine3
            // 
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine3.Location = new System.Drawing.Point(22, 400);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine3.Size = new System.Drawing.Size(770, 17);
            // 
            // xrLine4
            // 
            this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine4.Location = new System.Drawing.Point(22, 367);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine4.Size = new System.Drawing.Size(770, 16);
            // 
            // xrLine5
            // 
            this.xrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine5.Location = new System.Drawing.Point(22, 333);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine5.Size = new System.Drawing.Size(770, 19);
            // 
            // xrLine6
            // 
            this.xrLine6.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine6.Location = new System.Drawing.Point(22, 300);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine6.Size = new System.Drawing.Size(770, 19);
            // 
            // xrLine7
            // 
            this.xrLine7.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine7.Location = new System.Drawing.Point(22, 267);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine7.Size = new System.Drawing.Size(770, 19);
            // 
            // xrLine8
            // 
            this.xrLine8.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine8.Location = new System.Drawing.Point(22, 233);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine8.Size = new System.Drawing.Size(770, 19);
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Location = new System.Drawing.Point(475, 32);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.Size = new System.Drawing.Size(317, 17);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.Text = "HQ/2009-PLĐKTXK";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.Location = new System.Drawing.Point(475, 8);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.Size = new System.Drawing.Size(317, 25);
            this.winControlContainer1.WinControl = this.label1;
            // 
            // label1
            // 
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(304, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = " ";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.Location = new System.Drawing.Point(17, 33);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.Size = new System.Drawing.Size(450, 25);
            this.xrLabel6.Text = "Xuất khẩu";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.Location = new System.Drawing.Point(18, 8);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.Size = new System.Drawing.Size(449, 25);
            this.xrLabel8.Text = "PHỤ LỤC TỜ KHAI HẢI QUAN ĐIỆN TỬ";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblThongBaoMienThue
            // 
            this.lblThongBaoMienThue.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblThongBaoMienThue.CanGrow = false;
            this.lblThongBaoMienThue.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.lblThongBaoMienThue.Location = new System.Drawing.Point(25, 600);
            this.lblThongBaoMienThue.Multiline = true;
            this.lblThongBaoMienThue.Name = "lblThongBaoMienThue";
            this.lblThongBaoMienThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThongBaoMienThue.Size = new System.Drawing.Size(308, 283);
            this.lblThongBaoMienThue.StylePriority.UseBorders = false;
            this.lblThongBaoMienThue.Text = "Hàng xuất khẩu thuộc đối tượng không chịu thuế theo quy định tại điều 2.3 nghị đị" +
                "nh 149/2005/NN-CP của Chính phủ, ngày 05/12/2005";
            this.lblThongBaoMienThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblThongBaoMienThueGTGT
            // 
            this.lblThongBaoMienThueGTGT.BackColor = System.Drawing.Color.Transparent;
            this.lblThongBaoMienThueGTGT.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblThongBaoMienThueGTGT.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.lblThongBaoMienThueGTGT.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblThongBaoMienThueGTGT.Location = new System.Drawing.Point(467, 608);
            this.lblThongBaoMienThueGTGT.Name = "lblThongBaoMienThueGTGT";
            this.lblThongBaoMienThueGTGT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThongBaoMienThueGTGT.Size = new System.Drawing.Size(325, 275);
            this.lblThongBaoMienThueGTGT.StylePriority.UseBorders = false;
            this.lblThongBaoMienThueGTGT.Text = "HÀNG KHÔNG CHỊU THUẾ GTGT THEO ĐIỂM 1.22 MỤC II PHẦN A THÔNG TƯ 32/2007/BTC NGÀY " +
                "09/04/2007";
            this.lblThongBaoMienThueGTGT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TQDTPhuLucToKhaiXuat
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(8, 8, 6, 19);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell TenHang4;
        private DevExpress.XtraReports.UI.XRTableCell MaHS4;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu4;
        private DevExpress.XtraReports.UI.XRTableCell Luong4;
        private DevExpress.XtraReports.UI.XRTableCell DVT4;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell TenHang5;
        private DevExpress.XtraReports.UI.XRTableCell MaHS5;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu5;
        private DevExpress.XtraReports.UI.XRTableCell Luong5;
        private DevExpress.XtraReports.UI.XRTableCell DVT5;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell TenHang6;
        private DevExpress.XtraReports.UI.XRTableCell MaHS6;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu6;
        private DevExpress.XtraReports.UI.XRTableCell Luong6;
        private DevExpress.XtraReports.UI.XRTableCell DVT6;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell TenHang7;
        private DevExpress.XtraReports.UI.XRTableCell MaHS7;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu7;
        private DevExpress.XtraReports.UI.XRTableCell Luong7;
        private DevExpress.XtraReports.UI.XRTableCell DVT7;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell TenHang8;
        private DevExpress.XtraReports.UI.XRTableCell MaHS8;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu8;
        private DevExpress.XtraReports.UI.XRTableCell Luong8;
        private DevExpress.XtraReports.UI.XRTableCell DVT8;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell TenHang9;
        private DevExpress.XtraReports.UI.XRTableCell MaHS9;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu9;
        private DevExpress.XtraReports.UI.XRTableCell Luong9;
        private DevExpress.XtraReports.UI.XRTableCell DVT9;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT9;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT4;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK4;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT5;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK5;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT6;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK6;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT7;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK7;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT8;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK8;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT9;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK9;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK9;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaNT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT9;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT4;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT4;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT5;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT6;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT6;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac6;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT7;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT7;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac7;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT8;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT8;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac8;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT9;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT9;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThueXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThueGTGT;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaThuKhac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRLine xrLine16;
        private DevExpress.XtraReports.UI.XRLine xrLine15;
        private DevExpress.XtraReports.UI.XRLine xrLine14;
        private DevExpress.XtraReports.UI.XRLine xrLine13;
        private DevExpress.XtraReports.UI.XRLine xrLine12;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHaiQuan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLine xrLine18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblTongbangso;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lblTongThueXNKChu;
        private DevExpress.XtraReports.UI.XRLabel lblThongBaoMienThue;
        private DevExpress.XtraReports.UI.XRLabel lblThongBaoMienThueGTGT;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHaiQuanCuaKhau;
    }
}
