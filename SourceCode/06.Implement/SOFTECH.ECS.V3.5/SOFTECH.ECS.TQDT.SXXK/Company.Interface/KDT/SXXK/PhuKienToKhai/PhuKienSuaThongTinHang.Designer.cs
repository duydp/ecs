﻿namespace Company.Interface.KDT.SXXK
{
    partial class PhuKienSuaThongTinHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grvHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhuKienSuaThongTinHang));
            this.txtMaHangCu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnDong = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDVT_Old = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtSoLuongCu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoTT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDVT_New = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtSoLuongMoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.grvHang = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvHang)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grvHang);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnDong);
            this.grbMain.Size = new System.Drawing.Size(632, 420);
            // 
            // txtMaHangCu
            // 
            this.txtMaHangCu.Location = new System.Drawing.Point(345, 20);
            this.txtMaHangCu.Name = "txtMaHangCu";
            this.txtMaHangCu.Size = new System.Drawing.Size(220, 21);
            this.txtMaHangCu.TabIndex = 0;
            this.txtMaHangCu.VisualStyleManager = this.vsmMain;
            // 
            // btnDong
            // 
            this.btnDong.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnDong.Image = global::Company.Interface.Properties.Resources.exit;
            this.btnDong.Location = new System.Drawing.Point(556, 153);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(75, 23);
            this.btnDong.TabIndex = 1;
            this.btnDong.Text = "Đóng";
            this.btnDong.VisualStyleManager = this.vsmMain;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(291, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Mã hàng";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ctrDVT_Old);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongCu);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 59);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(305, 85);
            this.uiGroupBox1.TabIndex = 4;
            this.uiGroupBox1.Text = "Thông tin đã khai báo";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ctrDVT_Old
            // 
            this.ctrDVT_Old.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDVT_Old.Appearance.Options.UseBackColor = true;
            this.ctrDVT_Old.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVT_Old.Code = "";
            this.ctrDVT_Old.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVT_Old.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVT_Old.IsOnlyWarning = false;
            this.ctrDVT_Old.IsValidate = true;
            this.ctrDVT_Old.Location = new System.Drawing.Point(97, 47);
            this.ctrDVT_Old.Name = "ctrDVT_Old";
            this.ctrDVT_Old.Name_VN = "";
            this.ctrDVT_Old.SetOnlyWarning = false;
            this.ctrDVT_Old.SetValidate = false;
            this.ctrDVT_Old.ShowColumnCode = true;
            this.ctrDVT_Old.ShowColumnName = true;
            this.ctrDVT_Old.Size = new System.Drawing.Size(193, 21);
            this.ctrDVT_Old.TabIndex = 6;
            this.ctrDVT_Old.TagCode = "";
            this.ctrDVT_Old.TagName = "";
            this.ctrDVT_Old.Where = null;
            this.ctrDVT_Old.WhereCondition = "";
            // 
            // txtSoLuongCu
            // 
            this.txtSoLuongCu.DecimalDigits = 4;
            this.txtSoLuongCu.Location = new System.Drawing.Point(97, 16);
            this.txtSoLuongCu.Name = "txtSoLuongCu";
            this.txtSoLuongCu.Size = new System.Drawing.Size(104, 21);
            this.txtSoLuongCu.TabIndex = 5;
            this.txtSoLuongCu.Text = "0.0000";
            this.txtSoLuongCu.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(25, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Đơn vị tính";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(25, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Số lượng";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtSoTT);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.txtMaHangCu);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(632, 53);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.Text = "Dòng hàng muốn sửa đổi";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTT
            // 
            this.txtSoTT.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoTT.DecimalDigits = 0;
            this.txtSoTT.Location = new System.Drawing.Point(170, 20);
            this.txtSoTT.Name = "txtSoTT";
            this.txtSoTT.Size = new System.Drawing.Size(104, 21);
            this.txtSoTT.TabIndex = 5;
            this.txtSoTT.Text = "0";
            this.txtSoTT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTT.VisualStyleManager = this.vsmMain;
            this.txtSoTT.ButtonClick += new System.EventHandler(this.txtSoTT_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(6, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Số thứ tự của hàng trên tờ khai";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.ctrDVT_New);
            this.uiGroupBox3.Controls.Add(this.txtSoLuongMoi);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Location = new System.Drawing.Point(315, 59);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(317, 85);
            this.uiGroupBox3.TabIndex = 4;
            this.uiGroupBox3.Text = "Thông tin sửa đổi";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ctrDVT_New
            // 
            this.ctrDVT_New.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDVT_New.Appearance.Options.UseBackColor = true;
            this.ctrDVT_New.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVT_New.Code = "";
            this.ctrDVT_New.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVT_New.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVT_New.IsOnlyWarning = false;
            this.ctrDVT_New.IsValidate = true;
            this.ctrDVT_New.Location = new System.Drawing.Point(98, 47);
            this.ctrDVT_New.Name = "ctrDVT_New";
            this.ctrDVT_New.Name_VN = "";
            this.ctrDVT_New.SetOnlyWarning = false;
            this.ctrDVT_New.SetValidate = false;
            this.ctrDVT_New.ShowColumnCode = true;
            this.ctrDVT_New.ShowColumnName = true;
            this.ctrDVT_New.Size = new System.Drawing.Size(193, 21);
            this.ctrDVT_New.TabIndex = 6;
            this.ctrDVT_New.TagCode = "";
            this.ctrDVT_New.TagName = "";
            this.ctrDVT_New.Where = null;
            this.ctrDVT_New.WhereCondition = "";
            // 
            // txtSoLuongMoi
            // 
            this.txtSoLuongMoi.DecimalDigits = 4;
            this.txtSoLuongMoi.Location = new System.Drawing.Point(98, 16);
            this.txtSoLuongMoi.Name = "txtSoLuongMoi";
            this.txtSoLuongMoi.Size = new System.Drawing.Size(104, 21);
            this.txtSoLuongMoi.TabIndex = 5;
            this.txtSoLuongMoi.Text = "0.0000";
            this.txtSoLuongMoi.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(27, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Đơn vị tính";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(27, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Số lượng";
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnGhi.Image = global::Company.Interface.Properties.Resources.disk;
            this.btnGhi.Location = new System.Drawing.Point(475, 153);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 1;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // grvHang
            // 
            this.grvHang.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grvHang.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grvHang.ColumnAutoResize = true;
            grvHang_DesignTimeLayout.LayoutString = resources.GetString("grvHang_DesignTimeLayout.LayoutString");
            this.grvHang.DesignTimeLayout = grvHang_DesignTimeLayout;
            this.grvHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvHang.GroupByBoxVisible = false;
            this.grvHang.Location = new System.Drawing.Point(3, 182);
            this.grvHang.Name = "grvHang";
            this.grvHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grvHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grvHang.Size = new System.Drawing.Size(629, 238);
            this.grvHang.TabIndex = 5;
            this.grvHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grvHang.VisualStyleManager = this.vsmMain;
            this.grvHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grvHang_RowDoubleClick);
            this.grvHang.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.grvHang_DeletingRecords);
            // 
            // PhuKienSuaThongTinHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 420);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "PhuKienSuaThongTinHang";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Phụ kiện sửa thông tin hàng của tờ khai đưa vào thanh khoản";
            this.Load += new System.EventHandler(this.PhuKienSuaMaHang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvHang)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangCu;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTT;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.GridEX.GridEX grvHang;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongMoi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrDVT_Old;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongCu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrDVT_New;
    }
}