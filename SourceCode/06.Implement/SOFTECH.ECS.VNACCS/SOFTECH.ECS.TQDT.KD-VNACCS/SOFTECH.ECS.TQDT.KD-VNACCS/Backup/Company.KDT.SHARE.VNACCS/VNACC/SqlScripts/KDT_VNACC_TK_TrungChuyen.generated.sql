-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Insert]
	@TKMD_ID bigint,
	@SoTT int,
	@MaDiaDiem varchar(7),
	@NgayDen datetime,
	@NgayKhoiHanh datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_TrungChuyen]
(
	[TKMD_ID],
	[SoTT],
	[MaDiaDiem],
	[NgayDen],
	[NgayKhoiHanh],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@SoTT,
	@MaDiaDiem,
	@NgayDen,
	@NgayKhoiHanh,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@MaDiaDiem varchar(7),
	@NgayDen datetime,
	@NgayKhoiHanh datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_TrungChuyen]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoTT] = @SoTT,
	[MaDiaDiem] = @MaDiaDiem,
	[NgayDen] = @NgayDen,
	[NgayKhoiHanh] = @NgayKhoiHanh,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@MaDiaDiem varchar(7),
	@NgayDen datetime,
	@NgayKhoiHanh datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_TrungChuyen] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_TrungChuyen] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoTT] = @SoTT,
			[MaDiaDiem] = @MaDiaDiem,
			[NgayDen] = @NgayDen,
			[NgayKhoiHanh] = @NgayKhoiHanh,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_TrungChuyen]
		(
			[TKMD_ID],
			[SoTT],
			[MaDiaDiem],
			[NgayDen],
			[NgayKhoiHanh],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoTT,
			@MaDiaDiem,
			@NgayDen,
			@NgayKhoiHanh,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_TrungChuyen]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_TrungChuyen] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaDiaDiem],
	[NgayDen],
	[NgayKhoiHanh],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_TrungChuyen]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaDiaDiem],
	[NgayDen],
	[NgayKhoiHanh],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_TrungChuyen] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaDiaDiem],
	[NgayDen],
	[NgayKhoiHanh],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_TrungChuyen]	

GO

