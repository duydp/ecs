/*
Run this script on:

        ecsteam.ECS_TQDT_KD_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_KD

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 04/13/2012 10:19:33 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] DROP CONSTRAINT [PK_t_HaiQuan_CuaKhau]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] DROP CONSTRAINT [DF_t_HaiQuan_CuaKhau_UuTien]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_SXXK_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_SXXK_ToKhaiMauDich] ALTER COLUMN [CuaKhau_ID] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_ToKhaiMauDich_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_Update]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_Update]
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint,
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(6),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(100),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@Xuat_NPL_SP char(1),
	@ThanhLy char(1),
	@NGAY_THN_THX datetime,
	@MaDonViUT varchar(14),
	@TrangThaiThanhKhoan char(1),
	@ChungTu nvarchar(40),
	@PhanLuong varchar(2),
	@NgayHoanThanh datetime,
	@TrangThai smallint,
	@TrongLuongNet float,
	@SoTienKhoan money
AS
UPDATE
	[dbo].[t_SXXK_ToKhaiMauDich]
SET
	[NgayDangKy] = @NgayDangKy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
	[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
	[TenDonViDoiTac] = @TenDonViDoiTac,
	[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHanHopDong] = @NgayHetHanHopDong,
	[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
	[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
	[PTVT_ID] = @PTVT_ID,
	[SoHieuPTVT] = @SoHieuPTVT,
	[NgayDenPTVT] = @NgayDenPTVT,
	[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
	[LoaiVanDon] = @LoaiVanDon,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[NuocXK_ID] = @NuocXK_ID,
	[NuocNK_ID] = @NuocNK_ID,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[CuaKhau_ID] = @CuaKhau_ID,
	[DKGH_ID] = @DKGH_ID,
	[NguyenTe_ID] = @NguyenTe_ID,
	[TyGiaTinhThue] = @TyGiaTinhThue,
	[TyGiaUSD] = @TyGiaUSD,
	[PTTT_ID] = @PTTT_ID,
	[SoHang] = @SoHang,
	[SoLuongPLTK] = @SoLuongPLTK,
	[TenChuHang] = @TenChuHang,
	[SoContainer20] = @SoContainer20,
	[SoContainer40] = @SoContainer40,
	[SoKien] = @SoKien,
	[TrongLuong] = @TrongLuong,
	[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
	[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
	[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
	[LePhiHaiQuan] = @LePhiHaiQuan,
	[PhiBaoHiem] = @PhiBaoHiem,
	[PhiVanChuyen] = @PhiVanChuyen,
	[PhiXepDoHang] = @PhiXepDoHang,
	[PhiKhac] = @PhiKhac,
	[Xuat_NPL_SP] = @Xuat_NPL_SP,
	[ThanhLy] = @ThanhLy,
	[NGAY_THN_THX] = @NGAY_THN_THX,
	[MaDonViUT] = @MaDonViUT,
	[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
	[ChungTu] = @ChungTu,
	[PhanLuong] = @PhanLuong,
	[NgayHoanThanh] = @NgayHoanThanh,
	[TrangThai] = @TrangThai,
	[TrongLuongNet] = @TrongLuongNet,
	[SoTienKhoan] = @SoTienKhoan
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [SoToKhai] = @SoToKhai
	AND [MaLoaiHinh] = @MaLoaiHinh
	AND [NamDangKy] = @NamDangKy


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_ToKhaiMauDich_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_InsertUpdate]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_InsertUpdate]
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint,
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(6),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(100),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@Xuat_NPL_SP char(1),
	@ThanhLy char(1),
	@NGAY_THN_THX datetime,
	@MaDonViUT varchar(14),
	@TrangThaiThanhKhoan char(1),
	@ChungTu nvarchar(40),
	@PhanLuong varchar(2),
	@NgayHoanThanh datetime,
	@TrangThai smallint,
	@TrongLuongNet float,
	@SoTienKhoan money
AS
IF EXISTS(SELECT [MaHaiQuan], [SoToKhai], [MaLoaiHinh], [NamDangKy] FROM [dbo].[t_SXXK_ToKhaiMauDich] WHERE [MaHaiQuan] = @MaHaiQuan AND [SoToKhai] = @SoToKhai AND [MaLoaiHinh] = @MaLoaiHinh AND [NamDangKy] = @NamDangKy)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_ToKhaiMauDich] 
		SET
			[NgayDangKy] = @NgayDangKy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
			[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
			[TenDonViDoiTac] = @TenDonViDoiTac,
			[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHanHopDong] = @NgayHetHanHopDong,
			[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
			[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
			[PTVT_ID] = @PTVT_ID,
			[SoHieuPTVT] = @SoHieuPTVT,
			[NgayDenPTVT] = @NgayDenPTVT,
			[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
			[LoaiVanDon] = @LoaiVanDon,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[NuocXK_ID] = @NuocXK_ID,
			[NuocNK_ID] = @NuocNK_ID,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[CuaKhau_ID] = @CuaKhau_ID,
			[DKGH_ID] = @DKGH_ID,
			[NguyenTe_ID] = @NguyenTe_ID,
			[TyGiaTinhThue] = @TyGiaTinhThue,
			[TyGiaUSD] = @TyGiaUSD,
			[PTTT_ID] = @PTTT_ID,
			[SoHang] = @SoHang,
			[SoLuongPLTK] = @SoLuongPLTK,
			[TenChuHang] = @TenChuHang,
			[SoContainer20] = @SoContainer20,
			[SoContainer40] = @SoContainer40,
			[SoKien] = @SoKien,
			[TrongLuong] = @TrongLuong,
			[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
			[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
			[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
			[LePhiHaiQuan] = @LePhiHaiQuan,
			[PhiBaoHiem] = @PhiBaoHiem,
			[PhiVanChuyen] = @PhiVanChuyen,
			[PhiXepDoHang] = @PhiXepDoHang,
			[PhiKhac] = @PhiKhac,
			[Xuat_NPL_SP] = @Xuat_NPL_SP,
			[ThanhLy] = @ThanhLy,
			[NGAY_THN_THX] = @NGAY_THN_THX,
			[MaDonViUT] = @MaDonViUT,
			[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
			[ChungTu] = @ChungTu,
			[PhanLuong] = @PhanLuong,
			[NgayHoanThanh] = @NgayHoanThanh,
			[TrangThai] = @TrangThai,
			[TrongLuongNet] = @TrongLuongNet,
			[SoTienKhoan] = @SoTienKhoan
		WHERE
			[MaHaiQuan] = @MaHaiQuan
			AND [SoToKhai] = @SoToKhai
			AND [MaLoaiHinh] = @MaLoaiHinh
			AND [NamDangKy] = @NamDangKy
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_ToKhaiMauDich]
	(
			[MaHaiQuan],
			[SoToKhai],
			[MaLoaiHinh],
			[NamDangKy],
			[NgayDangKy],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaDaiLyTTHQ],
			[TenDaiLyTTHQ],
			[TenDonViDoiTac],
			[ChiTietDonViDoiTac],
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHanGiayPhep],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHanHopDong],
			[SoHoaDonThuongMai],
			[NgayHoaDonThuongMai],
			[PTVT_ID],
			[SoHieuPTVT],
			[NgayDenPTVT],
			[QuocTichPTVT_ID],
			[LoaiVanDon],
			[SoVanDon],
			[NgayVanDon],
			[NuocXK_ID],
			[NuocNK_ID],
			[DiaDiemXepHang],
			[CuaKhau_ID],
			[DKGH_ID],
			[NguyenTe_ID],
			[TyGiaTinhThue],
			[TyGiaUSD],
			[PTTT_ID],
			[SoHang],
			[SoLuongPLTK],
			[TenChuHang],
			[SoContainer20],
			[SoContainer40],
			[SoKien],
			[TrongLuong],
			[TongTriGiaKhaiBao],
			[TongTriGiaTinhThue],
			[LoaiToKhaiGiaCong],
			[LePhiHaiQuan],
			[PhiBaoHiem],
			[PhiVanChuyen],
			[PhiXepDoHang],
			[PhiKhac],
			[Xuat_NPL_SP],
			[ThanhLy],
			[NGAY_THN_THX],
			[MaDonViUT],
			[TrangThaiThanhKhoan],
			[ChungTu],
			[PhanLuong],
			[NgayHoanThanh],
			[TrangThai],
			[TrongLuongNet],
			[SoTienKhoan]
	)
	VALUES
	(
			@MaHaiQuan,
			@SoToKhai,
			@MaLoaiHinh,
			@NamDangKy,
			@NgayDangKy,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaDaiLyTTHQ,
			@TenDaiLyTTHQ,
			@TenDonViDoiTac,
			@ChiTietDonViDoiTac,
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHanGiayPhep,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHanHopDong,
			@SoHoaDonThuongMai,
			@NgayHoaDonThuongMai,
			@PTVT_ID,
			@SoHieuPTVT,
			@NgayDenPTVT,
			@QuocTichPTVT_ID,
			@LoaiVanDon,
			@SoVanDon,
			@NgayVanDon,
			@NuocXK_ID,
			@NuocNK_ID,
			@DiaDiemXepHang,
			@CuaKhau_ID,
			@DKGH_ID,
			@NguyenTe_ID,
			@TyGiaTinhThue,
			@TyGiaUSD,
			@PTTT_ID,
			@SoHang,
			@SoLuongPLTK,
			@TenChuHang,
			@SoContainer20,
			@SoContainer40,
			@SoKien,
			@TrongLuong,
			@TongTriGiaKhaiBao,
			@TongTriGiaTinhThue,
			@LoaiToKhaiGiaCong,
			@LePhiHaiQuan,
			@PhiBaoHiem,
			@PhiVanChuyen,
			@PhiXepDoHang,
			@PhiKhac,
			@Xuat_NPL_SP,
			@ThanhLy,
			@NGAY_THN_THX,
			@MaDonViUT,
			@TrangThaiThanhKhoan,
			@ChungTu,
			@PhanLuong,
			@NgayHoanThanh,
			@TrangThai,
			@TrongLuongNet,
			@SoTienKhoan
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_ToKhaiMauDich_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_Insert]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_Insert]
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint,
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(6),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(100),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@Xuat_NPL_SP char(1),
	@ThanhLy char(1),
	@NGAY_THN_THX datetime,
	@MaDonViUT varchar(14),
	@TrangThaiThanhKhoan char(1),
	@ChungTu nvarchar(40),
	@PhanLuong varchar(2),
	@NgayHoanThanh datetime,
	@TrangThai smallint,
	@TrongLuongNet float,
	@SoTienKhoan money
AS
INSERT INTO [dbo].[t_SXXK_ToKhaiMauDich]
(
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NamDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[Xuat_NPL_SP],
	[ThanhLy],
	[NGAY_THN_THX],
	[MaDonViUT],
	[TrangThaiThanhKhoan],
	[ChungTu],
	[PhanLuong],
	[NgayHoanThanh],
	[TrangThai],
	[TrongLuongNet],
	[SoTienKhoan]
)
VALUES
(
	@MaHaiQuan,
	@SoToKhai,
	@MaLoaiHinh,
	@NamDangKy,
	@NgayDangKy,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaDaiLyTTHQ,
	@TenDaiLyTTHQ,
	@TenDonViDoiTac,
	@ChiTietDonViDoiTac,
	@SoGiayPhep,
	@NgayGiayPhep,
	@NgayHetHanGiayPhep,
	@SoHopDong,
	@NgayHopDong,
	@NgayHetHanHopDong,
	@SoHoaDonThuongMai,
	@NgayHoaDonThuongMai,
	@PTVT_ID,
	@SoHieuPTVT,
	@NgayDenPTVT,
	@QuocTichPTVT_ID,
	@LoaiVanDon,
	@SoVanDon,
	@NgayVanDon,
	@NuocXK_ID,
	@NuocNK_ID,
	@DiaDiemXepHang,
	@CuaKhau_ID,
	@DKGH_ID,
	@NguyenTe_ID,
	@TyGiaTinhThue,
	@TyGiaUSD,
	@PTTT_ID,
	@SoHang,
	@SoLuongPLTK,
	@TenChuHang,
	@SoContainer20,
	@SoContainer40,
	@SoKien,
	@TrongLuong,
	@TongTriGiaKhaiBao,
	@TongTriGiaTinhThue,
	@LoaiToKhaiGiaCong,
	@LePhiHaiQuan,
	@PhiBaoHiem,
	@PhiVanChuyen,
	@PhiXepDoHang,
	@PhiKhac,
	@Xuat_NPL_SP,
	@ThanhLy,
	@NGAY_THN_THX,
	@MaDonViUT,
	@TrangThaiThanhKhoan,
	@ChungTu,
	@PhanLuong,
	@NgayHoanThanh,
	@TrangThai,
	@TrongLuongNet,
	@SoTienKhoan
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[view_HangXuatKhau]'
GO
EXEC sp_refreshview N'[dbo].[view_HangXuatKhau]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[view_HangDPXuatKhau]'
GO
EXEC sp_refreshview N'[dbo].[view_HangDPXuatKhau]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ADD
[MaCuc] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ALTER COLUMN [ID] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ALTER COLUMN [UuTien] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_CuaKhau] on [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ADD CONSTRAINT [PK_t_HaiQuan_CuaKhau] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_ToKhaiMauDich] ALTER COLUMN [CuaKhau_ID] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_BangKeConatainer]'
GO
EXEC sp_refreshview N'[dbo].[t_View_BangKeConatainer]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 30, 2011
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(6),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@GUIDSTR varchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference varchar(50),
	@NamDK int,
	@HUONGDAN nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDich] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
			[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
			[TenDonViDoiTac] = @TenDonViDoiTac,
			[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHanHopDong] = @NgayHetHanHopDong,
			[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
			[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
			[PTVT_ID] = @PTVT_ID,
			[SoHieuPTVT] = @SoHieuPTVT,
			[NgayDenPTVT] = @NgayDenPTVT,
			[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
			[LoaiVanDon] = @LoaiVanDon,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[NuocXK_ID] = @NuocXK_ID,
			[NuocNK_ID] = @NuocNK_ID,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[CuaKhau_ID] = @CuaKhau_ID,
			[DKGH_ID] = @DKGH_ID,
			[NguyenTe_ID] = @NguyenTe_ID,
			[TyGiaTinhThue] = @TyGiaTinhThue,
			[TyGiaUSD] = @TyGiaUSD,
			[PTTT_ID] = @PTTT_ID,
			[SoHang] = @SoHang,
			[SoLuongPLTK] = @SoLuongPLTK,
			[TenChuHang] = @TenChuHang,
			[SoContainer20] = @SoContainer20,
			[SoContainer40] = @SoContainer40,
			[SoKien] = @SoKien,
			[TrongLuong] = @TrongLuong,
			[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
			[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
			[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
			[LePhiHaiQuan] = @LePhiHaiQuan,
			[PhiBaoHiem] = @PhiBaoHiem,
			[PhiVanChuyen] = @PhiVanChuyen,
			[PhiXepDoHang] = @PhiXepDoHang,
			[PhiKhac] = @PhiKhac,
			[CanBoDangKy] = @CanBoDangKy,
			[QuanLyMay] = @QuanLyMay,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[LoaiHangHoa] = @LoaiHangHoa,
			[GiayTo] = @GiayTo,
			[PhanLuong] = @PhanLuong,
			[MaDonViUT] = @MaDonViUT,
			[TenDonViUT] = @TenDonViUT,
			[TrongLuongNet] = @TrongLuongNet,
			[SoTienKhoan] = @SoTienKhoan,
			[GUIDSTR] = @GUIDSTR,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ActionStatus] = @ActionStatus,
			[GuidReference] = @GuidReference,
			[NamDK] = @NamDK,
			[HUONGDAN] = @HUONGDAN
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDich]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaDaiLyTTHQ],
			[TenDaiLyTTHQ],
			[TenDonViDoiTac],
			[ChiTietDonViDoiTac],
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHanGiayPhep],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHanHopDong],
			[SoHoaDonThuongMai],
			[NgayHoaDonThuongMai],
			[PTVT_ID],
			[SoHieuPTVT],
			[NgayDenPTVT],
			[QuocTichPTVT_ID],
			[LoaiVanDon],
			[SoVanDon],
			[NgayVanDon],
			[NuocXK_ID],
			[NuocNK_ID],
			[DiaDiemXepHang],
			[CuaKhau_ID],
			[DKGH_ID],
			[NguyenTe_ID],
			[TyGiaTinhThue],
			[TyGiaUSD],
			[PTTT_ID],
			[SoHang],
			[SoLuongPLTK],
			[TenChuHang],
			[SoContainer20],
			[SoContainer40],
			[SoKien],
			[TrongLuong],
			[TongTriGiaKhaiBao],
			[TongTriGiaTinhThue],
			[LoaiToKhaiGiaCong],
			[LePhiHaiQuan],
			[PhiBaoHiem],
			[PhiVanChuyen],
			[PhiXepDoHang],
			[PhiKhac],
			[CanBoDangKy],
			[QuanLyMay],
			[TrangThaiXuLy],
			[LoaiHangHoa],
			[GiayTo],
			[PhanLuong],
			[MaDonViUT],
			[TenDonViUT],
			[TrongLuongNet],
			[SoTienKhoan],
			[GUIDSTR],
			[DeXuatKhac],
			[LyDoSua],
			[ActionStatus],
			[GuidReference],
			[NamDK],
			[HUONGDAN]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaDaiLyTTHQ,
			@TenDaiLyTTHQ,
			@TenDonViDoiTac,
			@ChiTietDonViDoiTac,
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHanGiayPhep,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHanHopDong,
			@SoHoaDonThuongMai,
			@NgayHoaDonThuongMai,
			@PTVT_ID,
			@SoHieuPTVT,
			@NgayDenPTVT,
			@QuocTichPTVT_ID,
			@LoaiVanDon,
			@SoVanDon,
			@NgayVanDon,
			@NuocXK_ID,
			@NuocNK_ID,
			@DiaDiemXepHang,
			@CuaKhau_ID,
			@DKGH_ID,
			@NguyenTe_ID,
			@TyGiaTinhThue,
			@TyGiaUSD,
			@PTTT_ID,
			@SoHang,
			@SoLuongPLTK,
			@TenChuHang,
			@SoContainer20,
			@SoContainer40,
			@SoKien,
			@TrongLuong,
			@TongTriGiaKhaiBao,
			@TongTriGiaTinhThue,
			@LoaiToKhaiGiaCong,
			@LePhiHaiQuan,
			@PhiBaoHiem,
			@PhiVanChuyen,
			@PhiXepDoHang,
			@PhiKhac,
			@CanBoDangKy,
			@QuanLyMay,
			@TrangThaiXuLy,
			@LoaiHangHoa,
			@GiayTo,
			@PhanLuong,
			@MaDonViUT,
			@TenDonViUT,
			@TrongLuongNet,
			@SoTienKhoan,
			@GUIDSTR,
			@DeXuatKhac,
			@LyDoSua,
			@ActionStatus,
			@GuidReference,
			@NamDK,
			@HUONGDAN
		)		
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Update]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 30, 2011
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(6),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@GUIDSTR varchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference varchar(50),
	@NamDK int,
	@HUONGDAN nvarchar(max)
AS

UPDATE
	[dbo].[t_KDT_ToKhaiMauDich]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
	[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
	[TenDonViDoiTac] = @TenDonViDoiTac,
	[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHanHopDong] = @NgayHetHanHopDong,
	[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
	[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
	[PTVT_ID] = @PTVT_ID,
	[SoHieuPTVT] = @SoHieuPTVT,
	[NgayDenPTVT] = @NgayDenPTVT,
	[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
	[LoaiVanDon] = @LoaiVanDon,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[NuocXK_ID] = @NuocXK_ID,
	[NuocNK_ID] = @NuocNK_ID,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[CuaKhau_ID] = @CuaKhau_ID,
	[DKGH_ID] = @DKGH_ID,
	[NguyenTe_ID] = @NguyenTe_ID,
	[TyGiaTinhThue] = @TyGiaTinhThue,
	[TyGiaUSD] = @TyGiaUSD,
	[PTTT_ID] = @PTTT_ID,
	[SoHang] = @SoHang,
	[SoLuongPLTK] = @SoLuongPLTK,
	[TenChuHang] = @TenChuHang,
	[SoContainer20] = @SoContainer20,
	[SoContainer40] = @SoContainer40,
	[SoKien] = @SoKien,
	[TrongLuong] = @TrongLuong,
	[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
	[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
	[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
	[LePhiHaiQuan] = @LePhiHaiQuan,
	[PhiBaoHiem] = @PhiBaoHiem,
	[PhiVanChuyen] = @PhiVanChuyen,
	[PhiXepDoHang] = @PhiXepDoHang,
	[PhiKhac] = @PhiKhac,
	[CanBoDangKy] = @CanBoDangKy,
	[QuanLyMay] = @QuanLyMay,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[LoaiHangHoa] = @LoaiHangHoa,
	[GiayTo] = @GiayTo,
	[PhanLuong] = @PhanLuong,
	[MaDonViUT] = @MaDonViUT,
	[TenDonViUT] = @TenDonViUT,
	[TrongLuongNet] = @TrongLuongNet,
	[SoTienKhoan] = @SoTienKhoan,
	[GUIDSTR] = @GUIDSTR,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ActionStatus] = @ActionStatus,
	[GuidReference] = @GuidReference,
	[NamDK] = @NamDK,
	[HUONGDAN] = @HUONGDAN
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Insert]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 30, 2011
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(6),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@GUIDSTR varchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference varchar(50),
	@NamDK int,
	@HUONGDAN nvarchar(max),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_ToKhaiMauDich]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaDaiLyTTHQ,
	@TenDaiLyTTHQ,
	@TenDonViDoiTac,
	@ChiTietDonViDoiTac,
	@SoGiayPhep,
	@NgayGiayPhep,
	@NgayHetHanGiayPhep,
	@SoHopDong,
	@NgayHopDong,
	@NgayHetHanHopDong,
	@SoHoaDonThuongMai,
	@NgayHoaDonThuongMai,
	@PTVT_ID,
	@SoHieuPTVT,
	@NgayDenPTVT,
	@QuocTichPTVT_ID,
	@LoaiVanDon,
	@SoVanDon,
	@NgayVanDon,
	@NuocXK_ID,
	@NuocNK_ID,
	@DiaDiemXepHang,
	@CuaKhau_ID,
	@DKGH_ID,
	@NguyenTe_ID,
	@TyGiaTinhThue,
	@TyGiaUSD,
	@PTTT_ID,
	@SoHang,
	@SoLuongPLTK,
	@TenChuHang,
	@SoContainer20,
	@SoContainer40,
	@SoKien,
	@TrongLuong,
	@TongTriGiaKhaiBao,
	@TongTriGiaTinhThue,
	@LoaiToKhaiGiaCong,
	@LePhiHaiQuan,
	@PhiBaoHiem,
	@PhiVanChuyen,
	@PhiXepDoHang,
	@PhiKhac,
	@CanBoDangKy,
	@QuanLyMay,
	@TrangThaiXuLy,
	@LoaiHangHoa,
	@GiayTo,
	@PhanLuong,
	@MaDonViUT,
	@TenDonViUT,
	@TrongLuongNet,
	@SoTienKhoan,
	@GUIDSTR,
	@DeXuatKhac,
	@LyDoSua,
	@ActionStatus,
	@GuidReference,
	@NamDK,
	@HUONGDAN
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_DinhMuc_InsertUpdate_KTX]'
GO
  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_InsertUpdate]  
-- Database: HaiQuanLuoi  
-- Author: Ngo Thanh Tung  
-- Time created: Monday, August 25, 2008  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_InsertUpdate_KTX]  
 @MaHaiQuan char(6),  
 @MaDoanhNghiep varchar(14),  
 @MaSanPham varchar(30),  
 @MaNguyenPhuLieu varchar(30),  
 @DinhMucSuDung numeric(18, 8),  
 @TyLeHaoHut numeric(18, 5),  
 @DinhMucChung numeric(18, 8),  
 @GhiChu varchar(250)  
AS  
IF EXISTS(SELECT [MaHaiQuan], [MaDoanhNghiep], [MaSanPham], [MaNguyenPhuLieu] FROM [dbo].[t_SXXK_DinhMuc] WHERE [MaHaiQuan] = @MaHaiQuan AND [MaDoanhNghiep] = @MaDoanhNghiep AND [MaSanPham] = @MaSanPham AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu)  
 BEGIN  
  UPDATE  
   [dbo].[t_SXXK_DinhMuc]   
  SET  
   [DinhMucSuDung] = @DinhMucSuDung,  
   [TyLeHaoHut] = @TyLeHaoHut,  
   [DinhMucChung] = @DinhMucChung,  
   [GhiChu] = @GhiChu  
  WHERE  
   [MaHaiQuan] = @MaHaiQuan  
   AND [MaDoanhNghiep] = @MaDoanhNghiep  
   AND [MaSanPham] = @MaSanPham  
   AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu  
 END  
ELSE  
 BEGIN  
 INSERT INTO [dbo].[t_SXXK_DinhMuc]  
 (  
   [MaHaiQuan],  
   [MaDoanhNghiep],  
   [MaSanPham],  
   [MaNguyenPhuLieu],  
   [DinhMucSuDung],  
   [TyLeHaoHut],  
   [DinhMucChung],  
   [GhiChu]  
 )  
 VALUES  
 (  
   @MaHaiQuan,  
   @MaDoanhNghiep,  
   @MaSanPham,  
   @MaNguyenPhuLieu,  
   @DinhMucSuDung,  
   @TyLeHaoHut,  
   @DinhMucChung,  
   @GhiChu  
 )   
 END  
 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ADD CONSTRAINT [DF_t_HaiQuan_CuaKhau_UuTien] DEFAULT ((100)) FOR [UuTien]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE dbo.t_HaiQuan_Version SET [Version] ='1.7', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.7', getdate(), null)
	end