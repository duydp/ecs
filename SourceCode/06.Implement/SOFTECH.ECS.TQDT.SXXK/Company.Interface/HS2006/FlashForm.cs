﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace HaiQuan.HS
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FlashFrom : Form
	{
		public System.Windows.Forms.Timer timer1;
		private IContainer components;
		private int second = 0;

		public FlashFrom()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FlashFrom));
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// FlashFrom
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(634, 298);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "FlashFrom";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.TopMost = true;

		}

		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		private static void Main()
		{
			Application.CurrentCulture = new CultureInfo("vi-VN");
			Application.Run(new FlashFrom());
		}


		private void timer1_Tick(object sender, EventArgs e)
		{
			if (second == 4)
			{
				timer1.Enabled = false;
				new MainForm().Show();
				this.Hide();
			}
			else
			{
				second ++;
			}
		}

		private void FlashFrom_Click(object sender, System.EventArgs e)
		{
			if (!this.timer1.Enabled) this.Hide();
		}
	}
}