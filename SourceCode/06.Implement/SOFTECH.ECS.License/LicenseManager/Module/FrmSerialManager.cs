using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmSerialManager : DevExpress.XtraEditors.XtraForm
    {
        private DataTable dtSerial;

        public FrmSerialManager()
        {
            InitializeComponent();
        }

        #region Adding Method
        private void DisplayGridHeader()
        {
            gridData.Columns["Code"].Caption = "Serial";
            gridData.Columns["LicenseID"].Caption = "Mã bản quyền";
            gridData.Columns["LicenseName"].Caption = "Tên bản quyền";
            gridData.Columns["NumberClient"].Caption = "Số máy";
            gridData.Columns["Term"].Caption = "Thời hạn";
            //gridData.Columns["ProductID"].Caption = "Mã sản phẩm";
            gridData.Columns["ProductName"].Caption = "Tên sản phẩm";
            //gridData.Columns["EmployeeID"].Caption = "Mã nhân viên";
            gridData.Columns["UserName"].Caption = "Tên nhân viên";
            gridData.Columns["CreateDate"].Caption = "Ngày tạo";
            gridData.Columns["Status"].Caption = "Trạng thái";

            gridData.Columns["LicenseID"].Visible = false;
            gridData.Columns["ProductID"].Visible = false;
            gridData.Columns["EmployeeID"].Visible = false;
        }

        private void OpenForm(string code, bool isEdit)
        {
            if (!ClassLib.IsFormShowed("Ser_" + code))
            {
                FrmSerialInfo obj = new FrmSerialInfo(code);
                obj.MdiParent = this.MdiParent;
                obj.isEdit = isEdit;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Ser_" + code].Activate();
            }
        }

        public void RefreshData()
        {
            //gridSerial.DataSource = Serial.SelectAll().Tables[0];

            dtSerial = Serial.SelectAll_Extend().Tables[0];
            
            gridSerial.DataSource = dtSerial;
            
            this.DisplayGridHeader();
        }
        #endregion

        private void FrmSerialManager_Load(object sender, EventArgs e)
        {
            this.RefreshData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!ClassLib.IsFormShowed("Ser_New"))
            {
                FrmSerialInfo obj = new FrmSerialInfo(null);
                obj.MdiParent = this.MdiParent;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Ser_New"].Activate();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string Code = gridData.GetRowCellValue(i, "Code").ToString();
                    this.OpenForm(Code, true);
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string Code = gridData.GetRowCellValue(i, "Code").ToString();
                    string Message = "Bạn có muốn xóa Serial: " + Code;
                    if (MessageBox.Show(Message, "Xóa Serial", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        SerialLog.DeleteBy_SerialCode(Code);
                        
                        Serial.DeleteSerial(Code);
                    }
                }
            }
            this.RefreshData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            gridData.ClearColumnsFilter();
            gridData.ClearGrouping();
            gridData.ClearSorting();
            this.RefreshData();
        }

        private void gridSerial_DoubleClick(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string Code = gridData.GetRowCellValue(i, "Code").ToString();
                    this.OpenForm(Code, false);
                }
            }
        }

    }
}