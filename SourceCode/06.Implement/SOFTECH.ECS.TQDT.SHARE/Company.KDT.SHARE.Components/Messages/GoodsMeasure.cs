﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class GoodsMeasure
    {
        /// <summary>
        /// Số lượng(double) n..14,3
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }
        /// <summary>
        /// Khối lượng
        /// </summary>
        [XmlElement("grossMass")]
        public string GrossMass { get; set; }
        /// <summary>
        /// Mã đơn vị tính an..3
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set;}
    }
}
