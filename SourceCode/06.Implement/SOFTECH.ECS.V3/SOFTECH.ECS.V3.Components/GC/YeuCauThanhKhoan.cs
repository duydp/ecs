﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.GC.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using SOFTECH.ECS.V3.Components.SHARE;
namespace SOFTECH.ECS.V3.Components.GC
{//AiNPV
    public partial class YeuCauThanhKhoan
    {
        /// <summary>
        /// Config Yeu Cau Thanh Khoan
        /// </summary>
        /// <param name="tknhap">int</param>
        /// <param name="issuer">int</param>
        /// <param name="function">int</param>
        /// <param name="status">int</param>
        /// <param name="declarationOffice">int</param>
        /// <returns>string</returns>
        public static string ConfigYeuCauThanhKhoan(ThanhKhoanHDGC tkHopDong, int issuer, int function, int status, int declarationOffice)
        {
            string doc = "";
            string path = "../../../SOFTECH.ECS.V3.Components/XML/GC/YeuCauThanhKhoan.xml";
            XDocument docXML = XDocument.Load(path);
            if (docXML != null)
            {
                XElement Root = docXML.Root;
                if (Root != null && tkHopDong != null)
                {
                    XElement AdditionalDocument = Root.Element(NodeYeuCauThanhKhoan.AdditionalDocument);
                    XElement GoodsItems = AdditionalDocument.Element(NodeYeuCauThanhKhoan.GoodsItems);
                    XElement tempGoodsItems = new XElement(GoodsItems);
                    HopDong hdong = HopDong.Load(tkHopDong.HopDong_ID);
                    foreach (XElement element in Root.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.issuer.ToUpper()))
                        {
                            element.SetValue(issuer);
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.reference.ToUpper()))
                        {
                            element.SetValue(hdong.ID);
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.issue.ToUpper()))
                        {
                            element.SetValue(hdong.NgayDangKy);
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.function.ToUpper()))
                        {
                            element.SetValue(function);
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.customsReference.ToUpper()))
                        {
                            element.SetValue(hdong.GUIDSTR);
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.status.ToUpper()))
                        {
                            element.SetValue(status);
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.acceptance.ToUpper()))
                        {
                            element.SetValue(hdong.NgayDangKy);
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.declarationOffice.ToUpper()))
                        {
                            element.SetValue(declarationOffice);
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.Agent.ToUpper()))
                        {
                            foreach (XElement AgentChild in element.Elements())
                            {
                                if (AgentChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.name.ToUpper()))
                                {
                                    //??Not Found
                                }
                                else if (AgentChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.identity.ToUpper()))
                                {
                                    AgentChild.SetValue(hdong.MaDaiLy);
                                }
                                else if (AgentChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.status.ToUpper()))
                                {
                                    //??Not Found
                                }
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.Importer.ToUpper()))
                        {
                            foreach (XElement ImporterChild in element.Elements())
                            {
                                if (ImporterChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.name.ToUpper()))
                                {
                                    ImporterChild.SetValue("");//??Not Found
                                }
                                else if (ImporterChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.identity.ToUpper()))
                                {
                                    ImporterChild.SetValue(hdong.MaDoanhNghiep);
                                }
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.ContractReference.ToUpper()))
                        {
                            foreach (XElement ContractReferenceChild in element.Elements())
                            {
                                if (ContractReferenceChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.reference.ToUpper()))
                                {
                                    ContractReferenceChild.SetValue(tkHopDong.HopDong_ID);
                                }
                                else if (ContractReferenceChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.issue.ToUpper()))
                                {
                                    ContractReferenceChild.SetValue(hdong.NgayDangKy);
                                }
                                else if (ContractReferenceChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.declarationOffice.ToUpper()))
                                {
                                    ContractReferenceChild.SetValue(hdong.MaHaiQuan);
                                }
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.AdditionalInfomation.ToUpper()))
                        {
                            foreach (XElement AdditionalInfomationChild in element.Elements())
                            {
                                if (AdditionalInfomationChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.content.ToUpper()))
                                {
                                    if (hdong.HUONGDAN != null)
                                    {
                                        AdditionalInfomationChild.SetValue(hdong.HUONGDAN);
                                    }
                                }
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.AdditionalDocument.ToUpper()))
                        {
                            foreach (XElement AdditionalDocumentChild in element.Elements())
                            {
                                if (AdditionalDocumentChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.documentType.ToUpper()))
                                {
                                    AdditionalDocumentChild.SetValue(Message_Types.Yeu_cau_thanh_khoan_hop_dong_gia_cong);
                                }
                                else if (AdditionalDocumentChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.documentReference.ToUpper()))
                                {
                                    AdditionalDocumentChild.SetValue(hdong.ID);
                                }
                                else if (AdditionalDocumentChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.description.ToUpper()))
                                {
                                    if (hdong.HUONGDAN != null)
                                    {
                                        AdditionalDocumentChild.SetValue(hdong.HUONGDAN);
                                    }
                                }
                                else if (AdditionalDocumentChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.GoodsItems.ToUpper()))
                                {
                                    //???? NotFound
                                }
                            }
                        }
                    }    
                }
                else
                {
                    return doc; //return "";
                }
            }
            docXML.Save(@"D:\temp.xml");
            doc = docXML.ToString();
            return doc;
        }
        public static XElement configGoodsItems(XElement GoodsItems, HopDong hopdong)
        {
            if (hopdong != null)
            {
                foreach (XElement element in GoodsItems.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.Commodity.ToUpper()))
                    {
                        foreach (XElement CommodityChild in element.Elements())
                        {
                            if (CommodityChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.description.ToUpper()))
                            {
                                CommodityChild.SetValue("");
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.identification.ToUpper()))
                            {
                                CommodityChild.SetValue("");
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.comodityType.ToUpper()))
                            {
                                CommodityChild.SetValue("");
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.GoodsMeasure.ToUpper()))
                    {
                        foreach (XElement GoodsMeasureChild in element.Elements())
                        {
                            if (GoodsMeasureChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.quantity.ToUpper()))
                            {
                                GoodsMeasureChild.SetValue("");
                            }
                            else if (GoodsMeasureChild.Name.ToString().ToUpper().Equals(NodeYeuCauThanhKhoan.measureUnit.ToUpper()))
                            {
                                GoodsMeasureChild.SetValue("");
                            }
                        }
                    }
                }
                return GoodsItems;
            }
            else
            {
                return GoodsItems;
            }
        }
    }
}
