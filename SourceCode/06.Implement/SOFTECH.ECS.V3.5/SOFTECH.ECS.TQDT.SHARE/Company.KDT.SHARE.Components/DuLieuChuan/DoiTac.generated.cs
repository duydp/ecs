using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.DuLieuChuan
{
	public partial class DoiTac : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaCongTy { set; get; }
		public string TenCongTy { set; get; }
		public string DiaChi { set; get; }
		public string DienThoai { set; get; }
		public string Email { set; get; }
		public string Fax { set; get; }
		public string GhiChu { set; get; }
		public string MaDoanhNghiep { set; get; }
		public DateTime DateCreated { set; get; }
		public DateTime DateModified { set; get; }
		public string DiaChi2 { set; get; }
		public string BuuChinh { set; get; }
		public string TinhThanh { set; get; }
		public string QuocGia { set; get; }
		public string MaNuoc { set; get; }
		public string MaDaiLyHQ { set; get; }
		public string TenDaiLyHQ { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<DoiTac> ConvertToCollection(IDataReader reader)
		{
			List<DoiTac> collection = new List<DoiTac>();
			while (reader.Read())
			{
				DoiTac entity = new DoiTac();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCongTy"))) entity.MaCongTy = reader.GetString(reader.GetOrdinal("MaCongTy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCongTy"))) entity.TenCongTy = reader.GetString(reader.GetOrdinal("TenCongTy"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
				if (!reader.IsDBNull(reader.GetOrdinal("DienThoai"))) entity.DienThoai = reader.GetString(reader.GetOrdinal("DienThoai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
				if (!reader.IsDBNull(reader.GetOrdinal("Fax"))) entity.Fax = reader.GetString(reader.GetOrdinal("Fax"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateCreated"))) entity.DateCreated = reader.GetDateTime(reader.GetOrdinal("DateCreated"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateModified"))) entity.DateModified = reader.GetDateTime(reader.GetOrdinal("DateModified"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChi2"))) entity.DiaChi2 = reader.GetString(reader.GetOrdinal("DiaChi2"));
				if (!reader.IsDBNull(reader.GetOrdinal("BuuChinh"))) entity.BuuChinh = reader.GetString(reader.GetOrdinal("BuuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhThanh"))) entity.TinhThanh = reader.GetString(reader.GetOrdinal("TinhThanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuocGia"))) entity.QuocGia = reader.GetString(reader.GetOrdinal("QuocGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNuoc"))) entity.MaNuoc = reader.GetString(reader.GetOrdinal("MaNuoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyHQ"))) entity.MaDaiLyHQ = reader.GetString(reader.GetOrdinal("MaDaiLyHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyHQ"))) entity.TenDaiLyHQ = reader.GetString(reader.GetOrdinal("TenDaiLyHQ"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<DoiTac> collection, long id)
        {
            foreach (DoiTac item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_DoiTac VALUES(@MaCongTy, @TenCongTy, @DiaChi, @DienThoai, @Email, @Fax, @GhiChu, @MaDoanhNghiep, @DateCreated, @DateModified, @DiaChi2, @BuuChinh, @TinhThanh, @QuocGia, @MaNuoc, @MaDaiLyHQ, @TenDaiLyHQ)";
            string update = "UPDATE t_HaiQuan_DoiTac SET MaCongTy = @MaCongTy, TenCongTy = @TenCongTy, DiaChi = @DiaChi, DienThoai = @DienThoai, Email = @Email, Fax = @Fax, GhiChu = @GhiChu, MaDoanhNghiep = @MaDoanhNghiep, DateCreated = @DateCreated, DateModified = @DateModified, DiaChi2 = @DiaChi2, BuuChinh = @BuuChinh, TinhThanh = @TinhThanh, QuocGia = @QuocGia, MaNuoc = @MaNuoc, MaDaiLyHQ = @MaDaiLyHQ, TenDaiLyHQ = @TenDaiLyHQ WHERE ID = @ID";
            string delete = "DELETE FROM t_HaiQuan_DoiTac WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCongTy", SqlDbType.VarChar, "MaCongTy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCongTy", SqlDbType.VarChar, "TenCongTy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DienThoai", SqlDbType.VarChar, "DienThoai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Email", SqlDbType.VarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Fax", SqlDbType.VarChar, "Fax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi2", SqlDbType.NVarChar, "DiaChi2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BuuChinh", SqlDbType.VarChar, "BuuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanh", SqlDbType.NVarChar, "TinhThanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuocGia", SqlDbType.NVarChar, "QuocGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuoc", SqlDbType.VarChar, "MaNuoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDaiLyHQ", SqlDbType.NVarChar, "TenDaiLyHQ", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCongTy", SqlDbType.VarChar, "MaCongTy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCongTy", SqlDbType.VarChar, "TenCongTy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DienThoai", SqlDbType.VarChar, "DienThoai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Email", SqlDbType.VarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Fax", SqlDbType.VarChar, "Fax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi2", SqlDbType.NVarChar, "DiaChi2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BuuChinh", SqlDbType.VarChar, "BuuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanh", SqlDbType.NVarChar, "TinhThanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuocGia", SqlDbType.NVarChar, "QuocGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuoc", SqlDbType.VarChar, "MaNuoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDaiLyHQ", SqlDbType.NVarChar, "TenDaiLyHQ", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_HaiQuan_DoiTac VALUES(@MaCongTy, @TenCongTy, @DiaChi, @DienThoai, @Email, @Fax, @GhiChu, @MaDoanhNghiep, @DateCreated, @DateModified, @DiaChi2, @BuuChinh, @TinhThanh, @QuocGia, @MaNuoc, @MaDaiLyHQ, @TenDaiLyHQ)";
            string update = "UPDATE t_HaiQuan_DoiTac SET MaCongTy = @MaCongTy, TenCongTy = @TenCongTy, DiaChi = @DiaChi, DienThoai = @DienThoai, Email = @Email, Fax = @Fax, GhiChu = @GhiChu, MaDoanhNghiep = @MaDoanhNghiep, DateCreated = @DateCreated, DateModified = @DateModified, DiaChi2 = @DiaChi2, BuuChinh = @BuuChinh, TinhThanh = @TinhThanh, QuocGia = @QuocGia, MaNuoc = @MaNuoc, MaDaiLyHQ = @MaDaiLyHQ, TenDaiLyHQ = @TenDaiLyHQ WHERE ID = @ID";
            string delete = "DELETE FROM t_HaiQuan_DoiTac WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCongTy", SqlDbType.VarChar, "MaCongTy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCongTy", SqlDbType.VarChar, "TenCongTy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DienThoai", SqlDbType.VarChar, "DienThoai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Email", SqlDbType.VarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Fax", SqlDbType.VarChar, "Fax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi2", SqlDbType.NVarChar, "DiaChi2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BuuChinh", SqlDbType.VarChar, "BuuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanh", SqlDbType.NVarChar, "TinhThanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuocGia", SqlDbType.NVarChar, "QuocGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuoc", SqlDbType.VarChar, "MaNuoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDaiLyHQ", SqlDbType.NVarChar, "TenDaiLyHQ", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCongTy", SqlDbType.VarChar, "MaCongTy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCongTy", SqlDbType.VarChar, "TenCongTy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DienThoai", SqlDbType.VarChar, "DienThoai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Email", SqlDbType.VarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Fax", SqlDbType.VarChar, "Fax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi2", SqlDbType.NVarChar, "DiaChi2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BuuChinh", SqlDbType.VarChar, "BuuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanh", SqlDbType.NVarChar, "TinhThanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuocGia", SqlDbType.NVarChar, "QuocGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuoc", SqlDbType.VarChar, "MaNuoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDaiLyHQ", SqlDbType.NVarChar, "TenDaiLyHQ", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static DoiTac Load(long id)
		{
			const string spName = "[dbo].[p_DoiTac_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<DoiTac> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<DoiTac> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<DoiTac> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_DoiTac_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_DoiTac_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_DoiTac_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_DoiTac_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertDoiTac(string maCongTy, string tenCongTy, string diaChi, string dienThoai, string email, string fax, string ghiChu, string maDoanhNghiep, DateTime dateCreated, DateTime dateModified, string diaChi2, string buuChinh, string tinhThanh, string quocGia, string maNuoc, string maDaiLyHQ, string tenDaiLyHQ)
		{
			DoiTac entity = new DoiTac();	
			entity.MaCongTy = maCongTy;
			entity.TenCongTy = tenCongTy;
			entity.DiaChi = diaChi;
			entity.DienThoai = dienThoai;
			entity.Email = email;
			entity.Fax = fax;
			entity.GhiChu = ghiChu;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			entity.DiaChi2 = diaChi2;
			entity.BuuChinh = buuChinh;
			entity.TinhThanh = tinhThanh;
			entity.QuocGia = quocGia;
			entity.MaNuoc = maNuoc;
			entity.MaDaiLyHQ = maDaiLyHQ;
			entity.TenDaiLyHQ = tenDaiLyHQ;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_DoiTac_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaCongTy", SqlDbType.VarChar, MaCongTy);
			db.AddInParameter(dbCommand, "@TenCongTy", SqlDbType.VarChar, TenCongTy);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@DienThoai", SqlDbType.VarChar, DienThoai);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, Email);
			db.AddInParameter(dbCommand, "@Fax", SqlDbType.VarChar, Fax);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			db.AddInParameter(dbCommand, "@DiaChi2", SqlDbType.NVarChar, DiaChi2);
			db.AddInParameter(dbCommand, "@BuuChinh", SqlDbType.VarChar, BuuChinh);
			db.AddInParameter(dbCommand, "@TinhThanh", SqlDbType.NVarChar, TinhThanh);
			db.AddInParameter(dbCommand, "@QuocGia", SqlDbType.NVarChar, QuocGia);
			db.AddInParameter(dbCommand, "@MaNuoc", SqlDbType.VarChar, MaNuoc);
			db.AddInParameter(dbCommand, "@MaDaiLyHQ", SqlDbType.VarChar, MaDaiLyHQ);
			db.AddInParameter(dbCommand, "@TenDaiLyHQ", SqlDbType.NVarChar, TenDaiLyHQ);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<DoiTac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DoiTac item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateDoiTac(long id, string maCongTy, string tenCongTy, string diaChi, string dienThoai, string email, string fax, string ghiChu, string maDoanhNghiep, DateTime dateCreated, DateTime dateModified, string diaChi2, string buuChinh, string tinhThanh, string quocGia, string maNuoc, string maDaiLyHQ, string tenDaiLyHQ)
		{
			DoiTac entity = new DoiTac();			
			entity.ID = id;
			entity.MaCongTy = maCongTy;
			entity.TenCongTy = tenCongTy;
			entity.DiaChi = diaChi;
			entity.DienThoai = dienThoai;
			entity.Email = email;
			entity.Fax = fax;
			entity.GhiChu = ghiChu;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			entity.DiaChi2 = diaChi2;
			entity.BuuChinh = buuChinh;
			entity.TinhThanh = tinhThanh;
			entity.QuocGia = quocGia;
			entity.MaNuoc = maNuoc;
			entity.MaDaiLyHQ = maDaiLyHQ;
			entity.TenDaiLyHQ = tenDaiLyHQ;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_DoiTac_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaCongTy", SqlDbType.VarChar, MaCongTy);
			db.AddInParameter(dbCommand, "@TenCongTy", SqlDbType.VarChar, TenCongTy);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@DienThoai", SqlDbType.VarChar, DienThoai);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, Email);
			db.AddInParameter(dbCommand, "@Fax", SqlDbType.VarChar, Fax);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			db.AddInParameter(dbCommand, "@DiaChi2", SqlDbType.NVarChar, DiaChi2);
			db.AddInParameter(dbCommand, "@BuuChinh", SqlDbType.VarChar, BuuChinh);
			db.AddInParameter(dbCommand, "@TinhThanh", SqlDbType.NVarChar, TinhThanh);
			db.AddInParameter(dbCommand, "@QuocGia", SqlDbType.NVarChar, QuocGia);
			db.AddInParameter(dbCommand, "@MaNuoc", SqlDbType.VarChar, MaNuoc);
			db.AddInParameter(dbCommand, "@MaDaiLyHQ", SqlDbType.VarChar, MaDaiLyHQ);
			db.AddInParameter(dbCommand, "@TenDaiLyHQ", SqlDbType.NVarChar, TenDaiLyHQ);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<DoiTac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DoiTac item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateDoiTac(long id, string maCongTy, string tenCongTy, string diaChi, string dienThoai, string email, string fax, string ghiChu, string maDoanhNghiep, DateTime dateCreated, DateTime dateModified, string diaChi2, string buuChinh, string tinhThanh, string quocGia, string maNuoc, string maDaiLyHQ, string tenDaiLyHQ)
		{
			DoiTac entity = new DoiTac();			
			entity.ID = id;
			entity.MaCongTy = maCongTy;
			entity.TenCongTy = tenCongTy;
			entity.DiaChi = diaChi;
			entity.DienThoai = dienThoai;
			entity.Email = email;
			entity.Fax = fax;
			entity.GhiChu = ghiChu;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			entity.DiaChi2 = diaChi2;
			entity.BuuChinh = buuChinh;
			entity.TinhThanh = tinhThanh;
			entity.QuocGia = quocGia;
			entity.MaNuoc = maNuoc;
			entity.MaDaiLyHQ = maDaiLyHQ;
			entity.TenDaiLyHQ = tenDaiLyHQ;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_DoiTac_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaCongTy", SqlDbType.VarChar, MaCongTy);
			db.AddInParameter(dbCommand, "@TenCongTy", SqlDbType.VarChar, TenCongTy);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@DienThoai", SqlDbType.VarChar, DienThoai);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, Email);
			db.AddInParameter(dbCommand, "@Fax", SqlDbType.VarChar, Fax);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			db.AddInParameter(dbCommand, "@DiaChi2", SqlDbType.NVarChar, DiaChi2);
			db.AddInParameter(dbCommand, "@BuuChinh", SqlDbType.VarChar, BuuChinh);
			db.AddInParameter(dbCommand, "@TinhThanh", SqlDbType.NVarChar, TinhThanh);
			db.AddInParameter(dbCommand, "@QuocGia", SqlDbType.NVarChar, QuocGia);
			db.AddInParameter(dbCommand, "@MaNuoc", SqlDbType.VarChar, MaNuoc);
			db.AddInParameter(dbCommand, "@MaDaiLyHQ", SqlDbType.VarChar, MaDaiLyHQ);
			db.AddInParameter(dbCommand, "@TenDaiLyHQ", SqlDbType.NVarChar, TenDaiLyHQ);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<DoiTac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DoiTac item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteDoiTac(long id)
		{
			DoiTac entity = new DoiTac();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_DoiTac_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_DoiTac_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<DoiTac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DoiTac item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}