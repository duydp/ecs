﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
using Company.Interface.SXXK;
using Company.KDT.SHARE.QuanLyChungTu.CTTT;

namespace Company.Interface
{
    public partial class FrmChungTuThanhToanChiTiet : Company.Interface.BaseForm
    {
        public CTTT_ChungTu ChungTuThanhToan = new CTTT_ChungTu();

        public bool IsOpenFromChayThanhLy;
        private DataTable dtToKhaiDuocChon = new DataTable();
        private DataTable dtDanhSachTK = new DataTable();
        public FrmChungTuThanhToanChiTiet()
        {
            InitializeComponent();
        }
        public string LoaiChungTu { get; set; }
        private void FrmChungTuThanhToanChiTiet_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ChungTuThanhToan.ID > 0)
                    LoaiChungTu = ChungTuThanhToan.LoaiChungTu;
              
                if (!string.IsNullOrEmpty(LoaiChungTu))
                {
                    if (LoaiChungTu == enumLoaiChungTuTT.Nhap)
                    {
                        lblLoaiChungTu.Text = "Chứng từ TT tờ khai nhập";
                        lblLoaiChungTu.ForeColor = Color.Black;
                        pnLoaiChungTuColor.BackColor = Color.Blue;

                        dtFromDate.Value = new DateTime(DateTime.Now.Year, 1, 1);
                        dtToDate.Value = DateTime.Today;
                    }
                    else if (LoaiChungTu == enumLoaiChungTuTT.Xuat)
                    {
                        lblLoaiChungTu.Text = "Chứng từ TT tờ khai xuất";
                        lblLoaiChungTu.ForeColor = Color.Black;
                        pnLoaiChungTuColor.BackColor = Color.MistyRose;
                        dtFromDate.Value = DateTime.Today.AddDays(-90);
                        dtToDate.Value = DateTime.Today;
                    }
                }
                else
                { lblLoaiChungTu.Text = "Chưa phân định loại chứng từ "; }
                
                

                LoadNguyenTe();

                numLanThanhLy.Enabled = !IsOpenFromChayThanhLy;

                // Phương thức thanh toán.
                cboHinhThucThanhToan.DataSource = Company.KDT.SHARE.Components.DuLieuChuan.PhuongThucThanhToan.SelectAll().Tables[0];
                cboHinhThucThanhToan.DisplayMember = cboHinhThucThanhToan.ValueMember = "ID";
                cboHinhThucThanhToan.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

                if (cboHinhThucThanhToan.Items.Count > 0)
                    cboHinhThucThanhToan.SelectedIndex = 0;

                // Loại chi phi chung tu thanh toan.
                DataTable dtChiPhiKhac = Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhiChungTuThanhToan.SelectAll().Tables[0];
                dgChiPhi.DataSource = dtChiPhiKhac;
                DataTable dtChiPhi = dtChiPhiKhac.Clone();

                DataColumn triGia = new DataColumn("TriGia");
                triGia.DataType = typeof(decimal);
                dtChiPhi.Columns.Add(triGia);
                if(!dtChiPhi.Columns.Contains("GhiChu"))
                {
                    DataColumn GhiChu = new DataColumn("GhiChu");
                    GhiChu.DataType = typeof(string);
                    dtChiPhi.Columns.Add(GhiChu);
                }
                //Load phi chung tu thanh toan cua Chung tu da tao
                foreach (CTTT_ChiPhiKhac item in ChungTuThanhToan.ChiPhiKhacCollections)
                {
                    DataRow dr = dtChiPhi.NewRow();
                    dr["ID"] = item.MaChiPhi;
                    dr["TriGia"] = item.TriGia;
                    dr["GhiChu"] = item.GhiChu;
                    dtChiPhi.Rows.Add(dr);
                    DataRow[] arrDr = dtChiPhiKhac.Select("ID='" + item.MaChiPhi + "'");
                    foreach (DataRow drItem in arrDr)
                    {
                        dr[1] = drItem[1];
                        dtChiPhiKhac.Rows.Remove(drItem);
                    }
                }

                dgChiPhiDuocChon.Tables[0].Columns["TriGia"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
                dgChiPhiDuocChon.Tables[0].Columns["TriGia"].InputMask = "Number" + GlobalSettings.SoThapPhan.DinhMuc;
                dgChiPhiDuocChon.Tables[0].Columns["TriGia"].TotalFormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;

                dgChiPhiDuocChon.DataSource = dtChiPhi;

                numLanThanhLy.ValueChanged -= new EventHandler(numLanThanhLy_ValueChanged);
                numLanThanhLy.Value = HSTL.LanThanhLy;
                numLanThanhLy.ValueChanged += new EventHandler(numLanThanhLy_ValueChanged);

                InitialDSTK();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; }
        }

        private void LoadHoSoThanhLy(int lanThanhLy)
        {
            //Load thong tin ho so thanh ly (Neu co)
            //if (lanThanhLy > 0)
            //{
            //    HSTL = new HoSoThanhLyDangKy();
            //    HSTL.LanThanhLy = ChungTuThanhToan.LanThanhLy;
            //    HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //    HSTL.GetIdByLanThanhLy(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
            //    HSTL = HoSoThanhLyDangKy.Load(HSTL.ID);
            //}

            //if (HSTL == null) HSTL = new HoSoThanhLyDangKy();
        }

        private void FrmChungTuThanhToanChiTiet_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.bkTKXCollection = this.bkTKXLucDauCollection;
            this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion = this.bkTKXCollection;
        }

        #region Chon to khai

        private BKToKhaiXuatCollection bkTKXLucDauCollection = new BKToKhaiXuatCollection();
        private BKToKhaiXuatCollection bkTKXCollection = new BKToKhaiXuatCollection();
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();

        private void InitialDSTK()
        {
            try
            {
                dgTKXDuocChon.DeletingRecord += new RowActionCancelEventHandler(dgTKXTL_DeletingRecord);
                dgTKXDuocChon.DeletingRecords += new CancelEventHandler(dgTKXTL_DeletingRecords);
                dgTKXDuocChon.LoadingRow += new RowLoadEventHandler(dgTKXTL_LoadingRow);
                dgTKXDuocChon.RowDoubleClick += new RowActionEventHandler(dgTKXTL_RowDoubleClick);

                dgTKX.LoadingRow += new RowLoadEventHandler(dgTKX_LoadingRow);
                dgTKX.RowDoubleClick += new RowActionEventHandler(dgTKX_RowDoubleClick);

                //dgTKXTL.RootTable.Columns["NgayThucXuat"].DefaultValue = DateTime.Today;

                dgTKXDuocChon.ColumnAutoResize = true;
                dgTKXDuocChon.ColumnAutoSizeMode = ColumnAutoSizeMode.Default;
                dgTKXDuocChon.Tables[0].Columns["TriGia"].FormatString = "N" + GlobalSettings.TriGiaNT;
                dgTKXDuocChon.Tables[0].Columns["TriGia"].InputMask = "Number" + GlobalSettings.TriGiaNT;
                dgTKXDuocChon.Tables[0].Columns["TriGia"].TotalFormatString = "N" + GlobalSettings.TriGiaNT;

                if (this.HSTL.TrangThaiThanhKhoan == 401 || this.HSTL.SoTiepNhan == 1)
                {
                    //btnAdd.Enabled = btnDelete.Enabled = false;
                    //btnSave.Enabled = false;
                    dgTKXDuocChon.AllowDelete = InheritableBoolean.False;
                }
                           

                if (ChungTuThanhToan.ID != 0)
                {
                    numTongtriGia.Value = ChungTuThanhToan.TongTriGia;
                    txtSoChungTu.Text = ChungTuThanhToan.SoChungTu;
                    dtNgayChungTu.Value = ChungTuThanhToan.NgayChungTu;
                    cboHinhThucThanhToan.SelectedValue = ChungTuThanhToan.HinhThucThanhToan;
                    cboDongTienThanhToan.SelectedValue = ChungTuThanhToan.DongTienThanhToan;
                    this.dtToKhaiDuocChon = this.ChungTuThanhToan.LoadChungTuChiTietTable();
                    getSum();
                }
                else
                    this.dtToKhaiDuocChon = this.ChungTuThanhToan.LoadChungTuChiTietTable();
                SearchDataNew();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
        }

        private void SearchData()
        {
            DataTable dtToKhai = new DataTable();
            int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
            DateTime fromDate = dtFromDate.Value;
            DateTime toDate = dtToDate.Value;
            int LanThanhLy = System.Convert.ToInt16(numLanThanhLy.Value);
            if (LanThanhLy > 0)
            {
                if (this.LoaiChungTu == enumLoaiChungTuTT.Xuat)
                {
                    if (fromDate < this.getMaxDateBKTKN().AddDays(chenhLech))
                    {
                        fromDate = this.getMaxDateBKTKN().AddDays(0 - chenhLech);
                        dtFromDate.Value = fromDate;
                    }
                }
                else if (this.LoaiChungTu == enumLoaiChungTuTT.Nhap)
                {

                }
            }
            else
            {
                toDate = dtToDate.Value;
                fromDate = dtFromDate.Value;
            }
            if (this.LoaiChungTu == enumLoaiChungTuTT.Xuat)
                this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKXChuaThanhLyByDate_PhanLuong_DungChoCTTT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate);
            else if (this.LoaiChungTu == enumLoaiChungTuTT.Nhap)
                dtToKhai = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTK_DungChoCTTT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate,true);

            List<BKToKhaiXuat> tokhaiXuatSource = new List<BKToKhaiXuat>();
            List<BKToKhaiXuat> tokhaiXuatDest = new List<BKToKhaiXuat>();
            DataTable dtDest = new DataTable();
            if (this.ChungTuThanhToan == null)
                dtDest = new CTTT_ChungTu { ID = 0 }.LoadChungTuChiTietTable();
            else
                dtDest = this.ChungTuThanhToan.LoadChungTuChiTietTable();

            //if (this.HSTL.LanThanhLy > 0)
            //{
            //    int i = this.HSTL.getBKToKhaiXuat();

            //    if (i >= 0)
            //    {
            //        this.bkTKXCollection = this.HSTL.BKCollection[i].bkTKXColletion;

            //        //Neu tao chung tu thanh toan trong ho so thanh ly -> load du lieu theo Bang ke to khai xuat
            //        if (IsOpenFromChayThanhLy)
            //        {
            //            foreach (BKToKhaiXuat bk in this.bkTKXCollection)
            //            {
            //                this.RemoveTKMD(bk.SoToKhai, bk.MaLoaiHinh, bk.NamDangKy, bk.MaHaiQuan);
            //            }
            //        }
            //    }

            //    if (this.bkTKXCollection.Count > 0)
            //    {
            //        foreach (BKToKhaiXuat item in this.bkTKXCollection)
            //        {
            //            CTTT_ChungTuChiTiet CTTT_ChungTuChiTiet = ChungTuThanhToan.ChungTuChiTietCollections.Find(c => c.SoToKhai == item.SoToKhai && item.NgayDangKy == c.NgayDangKy);
            //            if (CTTT_ChungTuChiTiet != null)
            //            {
            //                item.TriGia = CTTT_ChungTuChiTiet.TriGia;
            //                tokhaiXuatDest.Add(item);
            //            }
            //            else tokhaiXuatSource.Add(item);
            //        }
            //    }
            //    else
            //    {
            //        foreach (ToKhaiMauDich item in tkmdCollection)
            //        {
            //            BKToKhaiXuat bkTKX = new BKToKhaiXuat();
            //            bkTKX.SoToKhai = item.SoToKhai;
            //            bkTKX.MaLoaiHinh = item.MaLoaiHinh;
            //            bkTKX.NamDangKy = item.NamDangKy;
            //            bkTKX.MaHaiQuan = item.MaHaiQuan;
            //            bkTKX.NgayDangKy = item.NgayDangKy;
            //            bkTKX.NgayThucXuat = item.NGAY_THN_THX;
            //            bkTKX.NgayHoanThanh = item.NgayHoanThanh;
            //            tokhaiXuatSource.Add(bkTKX);
            //        }
            //    }

            //    this.bkTKXLucDauCollection = (BKToKhaiXuatCollection)this.bkTKXCollection.Clone();
            //}
            //else
            //{
            ////foreach (ToKhaiMauDich item in tkmdCollection)
            ////{
            ////    BKToKhaiXuat bkTKX = new BKToKhaiXuat();
            ////    bkTKX.SoToKhai = item.SoToKhai;
            ////    bkTKX.MaLoaiHinh = item.MaLoaiHinh;
            ////    bkTKX.NamDangKy = item.NamDangKy;
            ////    bkTKX.MaHaiQuan = item.MaHaiQuan;
            ////    bkTKX.NgayDangKy = item.NgayDangKy;
            ////    bkTKX.NgayThucXuat = item.NGAY_THN_THX;
            ////    bkTKX.NgayHoanThanh = item.NgayHoanThanh;
            ////    tokhaiXuatSource.Add(bkTKX);
            ////}

            List<BKToKhaiXuat> luuTamXoa = new List<BKToKhaiXuat>();
            //foreach (BKToKhaiXuat item in tokhaiXuatSource)
            //{
            //    CTTT_ChungTuChiTiet CTTT_ChungTuChiTiet = ChungTuThanhToan.ChungTuChiTietCollections.Find(c => c.SoToKhai == item.SoToKhai && item.NgayDangKy.Year == c.NgayDangKy.Year && item.MaLoaiHinh == c.MaLoaiHinh);
            //    if (CTTT_ChungTuChiTiet != null)
            //    {
            //        item.TriGia = CTTT_ChungTuChiTiet.TriGia;
            //        tokhaiXuatDest.Add(item);

            //        BKToKhaiXuat bkTKX = new BKToKhaiXuat();
            //        bkTKX.SoToKhai = item.SoToKhai;
            //        bkTKX.MaLoaiHinh = item.MaLoaiHinh;
            //        bkTKX.NamDangKy = item.NamDangKy;
            //        bkTKX.MaHaiQuan = item.MaHaiQuan;
            //        bkTKX.NgayDangKy = item.NgayDangKy;
            //        luuTamXoa.Add(bkTKX);
            //    }
            //}
            if (ChungTuThanhToan != null && ChungTuThanhToan.ChungTuChiTietCollections != null)
            {
                //foreach (CTTT_ChungTuChiTiet CTTT_ChungTuChiTiet in ChungTuThanhToan.ChungTuChiTietCollections)
                //{
//                    BKToKhaiXuat item = new BKToKhaiXuat();
//                    item.SoToKhai = CTTT_ChungTuChiTiet.SoToKhai;
//                    item.MaLoaiHinh = CTTT_ChungTuChiTiet.MaLoaiHinh;
//                    item.NamDangKy = (short)CTTT_ChungTuChiTiet.NgayDangKy.Year;
//                    item.MaHaiQuan = CTTT_ChungTuChiTiet.MaHaiQuan;
//                    item.NgayDangKy = CTTT_ChungTuChiTiet.NgayDangKy;
//                    item.TriGia = CTTT_ChungTuChiTiet.TriGia;
//                    tokhaiXuatDest.Add(item);
// 
//                     BKToKhaiXuat bkTKX = new BKToKhaiXuat();
//                     bkTKX.SoToKhai = item.SoToKhai;
//                     bkTKX.MaLoaiHinh = item.MaLoaiHinh;
//                     bkTKX.NamDangKy = item.NamDangKy;
//                     bkTKX.MaHaiQuan = item.MaHaiQuan;
//                     bkTKX.NgayDangKy = item.NgayDangKy;
//                     luuTamXoa.Add(bkTKX);
                    //Xoa to khai (Left panel)
                foreach (DataRow dr in dtDest.Rows)
                {
                    DataRow[] drow = dtToKhai.Select(string.Format("SoToKhai = {0} and MaLoaiHinh = '{1}' and NgayDangKy = #{2}#", dr["SoToKhai"], dr["MaLoaiHinh"], System.Convert.ToDateTime(dr["NgayDangKy"]).ToString("yyyy-MM-dd")));
                    foreach (DataRow item in drow)
                    {
                        dtToKhai.Rows.Remove(item);
                    }
                }
                //}
            }
            //Xoa to khai (Left panel)
            //foreach (BKToKhaiXuat item in luuTamXoa)
            //{
            //    BKToKhaiXuat objBK = tokhaiXuatSource.Find(delegate(BKToKhaiXuat o)
            //    {
            //        return o.SoToKhai == item.SoToKhai && o.MaLoaiHinh == item.MaLoaiHinh && o.NamDangKy == item.NamDangKy && o.MaHaiQuan == item.MaHaiQuan;
            //    });

            //    if (objBK != null)
            //        tokhaiXuatSource.Remove(objBK);
            //}
            //}

//             dgTKX.DataSource = tokhaiXuatSource;
//             dgTKXDuocChon.DataSource = tokhaiXuatDest;
            dgTKX.DataSource = dtToKhai;
            dgTKXDuocChon.DataSource = dtDest;
        }


        private void SearchDataNew()
        {
            //DataTable dtToKhai = new DataTable();
            int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
            DateTime fromDate = dtFromDate.Value;
            DateTime toDate = dtToDate.Value;
            int LanThanhLy = System.Convert.ToInt16(numLanThanhLy.Value);
            if (LanThanhLy > 0)
            {
                if (this.LoaiChungTu == enumLoaiChungTuTT.Xuat)
                {
                    if (fromDate < this.getMaxDateBKTKN().AddDays(chenhLech))
                    {
                        fromDate = this.getMaxDateBKTKN().AddDays(0 - chenhLech);
                        dtFromDate.Value = fromDate;
                    }
                }
                else if (this.LoaiChungTu == enumLoaiChungTuTT.Nhap)
                {

                }
            }
            else
            {
                toDate = dtToDate.Value;
                fromDate = dtFromDate.Value;
            }
            if (this.LoaiChungTu == enumLoaiChungTuTT.Xuat)
                //this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKXChuaThanhLyByDate_PhanLuong_DungChoCTTT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate);
                dtDanhSachTK = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTK_DungChoCTTT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate, false);
            else if (this.LoaiChungTu == enumLoaiChungTuTT.Nhap)
                dtDanhSachTK = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTK_DungChoCTTT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate,true);

            List<BKToKhaiXuat> tokhaiXuatSource = new List<BKToKhaiXuat>();
            List<BKToKhaiXuat> tokhaiXuatDest = new List<BKToKhaiXuat>();
                
            //DataTable dtDest = new DataTable();
            //if (this.ChungTuThanhToan == null)
            //    dtDest = new CTTT_ChungTu { ID = 0 }.LoadChungTuChiTietTable();
            //else
            //    dtDest = this.ChungTuThanhToan.LoadChungTuChiTietTable();

          
            List<BKToKhaiXuat> luuTamXoa = new List<BKToKhaiXuat>();
      
            if (ChungTuThanhToan != null && ChungTuThanhToan.ChungTuChiTietCollections != null)
            {
                //Xoa to khai (Left panel)
                foreach (DataRow dr in dtToKhaiDuocChon.Rows)
                {
                    DataRow[] drow = dtDanhSachTK.Select(string.Format("SoToKhai = {0} and MaLoaiHinh = '{1}' and NgayDangKy = #{2}#", dr["SoToKhai"], dr["MaLoaiHinh"], System.Convert.ToDateTime(dr["NgayDangKy"]).ToString("yyyy-MM-dd HH:mm:ss")));
                    foreach (DataRow item in drow)
                    {
                        dtDanhSachTK.Rows.Remove(item);
                    }
                }
             
            }
            dgTKX.DataSource = dtDanhSachTK;
            dgTKXDuocChon.DataSource = this.dtToKhaiDuocChon;

            try
            {
                dgTKX.Refetch();
                dgTKXDuocChon.Refetch();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private DateTime getMaxDateBKTKN()
        {
            int i = this.HSTL.getBKToKhaiNhap();

            if (i < 0)
                return new DateTime(DateTime.Today.Year, 1, 1);

            this.HSTL.BKCollection[i].LoadChiTietBangKe();

            BKToKhaiNhapCollection bkTKNCollection = this.HSTL.BKCollection[i].bkTKNCollection;

            DateTime dt = bkTKNCollection[0].NgayDangKy;

            for (int j = 1; j < bkTKNCollection.Count; j++)
            {
                if (dt > bkTKNCollection[j].NgayDangKy) dt = bkTKNCollection[j].NgayDangKy;
            }

            return dt;
        }

        private void RemoveBKTKX(BKToKhaiXuat bk)
        {
            for (int i = 0; i < this.bkTKXCollection.Count; i++)
            {
                if (this.bkTKXCollection[i].SoToKhai == bk.SoToKhai && this.bkTKXCollection[i].MaLoaiHinh == bk.MaLoaiHinh && this.bkTKXCollection[i].NamDangKy == bk.NamDangKy && this.bkTKXCollection[i].MaHaiQuan == bk.MaHaiQuan)
                {
                    this.bkTKXCollection.RemoveAt(i);
                    break;
                }
            }
        }
        private void RemoveTKMD(ToKhaiMauDich tkmd)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == tkmd.SoToKhai && this.tkmdCollection[i].MaLoaiHinh == tkmd.MaLoaiHinh && this.tkmdCollection[i].NamDangKy == tkmd.NamDangKy && this.tkmdCollection[i].MaHaiQuan == tkmd.MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }
        private void RemoveTKMD(int SoToKhai, string MaLoaiHinh, short NamDangKy, string MaHaiQuan)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == SoToKhai && this.tkmdCollection[i].MaLoaiHinh == MaLoaiHinh && this.tkmdCollection[i].NamDangKy == NamDangKy && this.tkmdCollection[i].MaHaiQuan == MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }

        private void dgTKXTL_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                //if (ShowMessage("Khi bạn xóa tờ khai xuất này thì dữ liệu liên quan đến tờ khai xuất trong các bảng kê: BK05, BK06, BK08 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
                if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
                {
                    BKToKhaiXuat bk = (BKToKhaiXuat)e.Row.DataRow;
                    bk.DeleteFull();
                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.MaHaiQuan = bk.MaHaiQuan;
                    tkmd.SoToKhai = bk.SoToKhai;
                    tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                    tkmd.NgayDangKy = bk.NgayDangKy;
                    tkmd.NamDangKy = bk.NamDangKy;
                    tkmd.NGAY_THN_THX = bk.NgayThucXuat;
                    this.tkmdCollection.Add(tkmd);
                    try
                    {
                        dgTKX.Refetch();
                    }
                    catch
                    {
                        dgTKX.Refresh();
                    }
                }
                else return;
            }
        }
        private void dgTKXTL_DeletingRecords(object sender, CancelEventArgs e)
        {
            //if (ShowMessage("Khi bạn xóa các tờ khai xuất này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê:  BK05, BK06, BK08 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
            if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
            {
                this.Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = dgTKXDuocChon.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        BKToKhaiXuat bk = (BKToKhaiXuat)i.GetRow().DataRow;
                        //if (bk.ID > 0)
                        //    bk.DeleteFull();
                        ToKhaiMauDich tkmd = new ToKhaiMauDich();
                        tkmd.MaHaiQuan = bk.MaHaiQuan;
                        tkmd.SoToKhai = bk.SoToKhai;
                        tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                        tkmd.NgayDangKy = bk.NgayDangKy;
                        tkmd.NamDangKy = bk.NamDangKy;
                        tkmd.NGAY_THN_THX = bk.NgayThucXuat;
                        this.tkmdCollection.Add(tkmd);
                    }
                }
                try
                {
                    dgTKX.Refetch();
                }
                catch
                {
                    dgTKX.Refresh();
                }
                this.Cursor = Cursors.Default;
            }
        }
        private void dgTKXTL_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();

                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog(this);
            }
        }
        private void dgTKXTL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //if (Convert.ToDateTime(e.Row.Cells["NgayThucXuat"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayThucXuat"].Text = "";
            if (Convert.ToDateTime(e.Row.Cells["NgayHoanThanh"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayHoanThanh"].Text = "";
        }

        private void dgTKX_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["NgayHoanThanh"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayHoanThanh"].Text = "";
        }
        private void dgTKX_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog(this);
            }
        }

        private void btnGoTo_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSoToKhai.Text.Length > 5)
                {
                    if (dgTKX.FindAll(dgTKX.Tables[0].Columns["SoToKhaiVNACCS"], ConditionOperator.Contains, Convert.ToDecimal(txtSoToKhai.Text)) > 0)
                    {
                        foreach (GridEXSelectedItem item in dgTKX.SelectedItems)
                            item.GetRow().IsChecked = true;
                    }
                    else
                    {
                        MLMessages("Không có tờ khai này trong hệ thống", "MSG_THK79", "", false);
                    }
                }
                else
                {
                    if (dgTKX.FindAll(dgTKX.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, Convert.ToInt64(txtSoToKhai.Value)) > 0)
                    {
                        foreach (GridEXSelectedItem item in dgTKX.SelectedItems)
                            item.GetRow().IsChecked = true;
                    }
                    else
                    {
                        MLMessages("Không có tờ khai này trong hệ thống", "MSG_THK79", "", false);
                    }
                }
                
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SearchDataNew();

                try
                {
                    dgTKX.Refetch();
                }
                catch
                {
                    dgTKX.Refresh();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //List<BKToKhaiXuat> tkxDich = (List<BKToKhaiXuat>)dgTKXDuocChon.DataSource;
                //DataTable dtDest = 
                foreach (GridEXRow row in dgTKX.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {

                        //BKToKhaiXuat tkmd = (BKToKhaiXuat)row.DataRow;

                        //BKToKhaiXuat bk = new BKToKhaiXuat();
                        //bk.MaHaiQuan = tkmd.MaHaiQuan;
                        //bk.MaLoaiHinh = tkmd.MaLoaiHinh;
                        //bk.NamDangKy = tkmd.NamDangKy;
                        //bk.NgayDangKy = tkmd.NgayDangKy;
                        //bk.NgayThucXuat = tkmd.NGAY_THN_THX;
                        //bk.SoToKhai = tkmd.SoToKhai;
                        //bk.NgayHoanThanh = tkmd.NgayHoanThanh;
                        //this.bkTKXCollection.Add(bk);
                        //tkmds.Add(tkmd);
                        DataRowView drSource = (DataRowView)row.DataRow;
                        DataRow dr = this.dtToKhaiDuocChon.NewRow();
                        dr["SoToKhai"] = drSource["SoToKhai"];
                        dr["MaLoaiHinh"] = drSource["MaLoaiHinh"];
                        dr["MaHaiQuan"] = drSource["MaHaiQuan"];
                        dr["NgayDangKy"] = drSource["NgayDangKy"];
                        dr["GiaTriConLai"] = drSource["GiaTriConLai"];
                        dr["SoToKhaiVNACCS"] = drSource["SoToKhaiVNACCS"];
                        dr["TriGia"] = 0;
                        this.dtToKhaiDuocChon.Rows.Add(dr);
                        this.dtDanhSachTK.Rows.Remove(drSource.Row);
                        //tkxDich.Add(tkmd);
                    }
                }
                //List<BKToKhaiXuat> tkxNguon = (List<BKToKhaiXuat>)dgTKX.DataSource;
                //foreach (BKToKhaiXuat item in tkxDich)
                //{
                //    tkxNguon.Remove(item);
                //}
                try
                {
                    dgTKX.Refetch();
                }
                catch
                {
                    dgTKX.Refresh();
                }
                try
                {
                    dgTKXDuocChon.Refetch();
                }
                catch
                {
                    dgTKXDuocChon.Refresh();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //if (ShowMessage("Khi bạn xóa các tờ khai xuất này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK05, BK06, BK08 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
            //if (MLMessages("Bạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
            //{
            this.Cursor = Cursors.WaitCursor;

            //List<BKToKhaiXuat> tkxDich = (List<BKToKhaiXuat>)dgTKXDuocChon.DataSource;
            //List<BKToKhaiXuat> tkxNguon = (List<BKToKhaiXuat>)dgTKX.DataSource;
            //List<BKToKhaiXuat> tkItems = new List<BKToKhaiXuat>();

            foreach (GridEXRow row in dgTKXDuocChon.GetCheckedRows())
            {

                if (row.RowType == RowType.Record)
                {

                    //BKToKhaiXuat bk = (BKToKhaiXuat)row.DataRow;
                    //CTTT_ChungTuChiTiet ctct = ChungTuThanhToan.ChungTuChiTietCollections.Find(c => c.SoToKhai == bk.SoToKhai && bk.NgayDangKy == c.NgayDangKy);
                    //if (ctct != null) ChungTuThanhToan.ChungTuChiTietCollections.Remove(ctct);
                    //tkItems.Add(bk);
                    this.dtToKhaiDuocChon.Rows.Remove(((DataRowView)row.DataRow).Row);
                    
                }
            }
            //foreach (BKToKhaiXuat item in tkItems)
            //{
            //    tkxDich.Remove(item);
            //    tkxNguon.Add(item);
            //}
            //foreach (BKToKhaiXuat bk in tkxDich)
            //    this.RemoveBKTKX(bk);

            try
            {
                dgTKXDuocChon.Refetch();
            }
            catch
            {
                dgTKXDuocChon.Refresh();
            }
            SearchDataNew();
//             try
//             {
//                 dgTKX.Refetch();
//             }
//             catch
//             {
//                 dgTKX.Refresh();
//             }
            this.Cursor = Cursors.Default;
            //}
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
  

        #endregion

        /// <summary>
        /// Lưu thông tin chứng từ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool valid = ValidateControl.ValidateNumber(numLanThanhLy, errorProvider1, "Lần thanh lý.");

                valid = ValidateControl.ValidateNull(txtSoChungTu, errorProvider1, "Số chứng từ.");

                valid = ValidateControl.ValidateNull(cboHinhThucThanhToan, errorProvider1, "Hình thức thanh toán.");

                valid = ValidateControl.ValidateNull(cboDongTienThanhToan, errorProvider1, "Đồng tiền thanh toán.");

                //valid = Company.Interface.Globals.ValidateNumber(numTongtriGia, errorProvider1, "Tổng trị giá.");
                errorProvider1.SetError(numTongtriGia, "");
                if (System.Convert.ToInt64(numTongtriGia.Value) <= 0)
                {
                    errorProvider1.SetError(numTongtriGia, "Tổng trị giá phải > 0");
                    valid = false;
                }
                

                if (!valid) return;

                //CTTT_ChungTu.DeleteChungTu(ChungTuThanhToan.ID);
                //Tao doi tuong Chung tu thanh toan
                CTTT_ChungTu cttt = ChungTuThanhToan;//new CTTT_ChungTu();
                cttt.LanThanhLy = (int)numLanThanhLy.Value;
                cttt.SoChungTu = txtSoChungTu.Text;
                cttt.NgayChungTu = dtNgayChungTu.Value;
                cttt.HinhThucThanhToan = cboHinhThucThanhToan.SelectedValue.ToString();
                cttt.DongTienThanhToan = cboDongTienThanhToan.SelectedValue.ToString();
                cttt.TongTriGia = numTongtriGia.Value;
                cttt.ConLai = numTongtriGia.Value - getSum();
                cttt.LoaiChungTu = this.LoaiChungTu;
                if (cttt.ID == 0)
                {
                    List<CTTT_ChungTu> list = CTTT_ChungTu.SelectCollectionDynamic(string.Format("SoChungTu = '{0}' and Day(NgayChungTu) = '{1}' and Month(NgayChungTu) = {2} and Year(NgayChungTu) = {3} And LoaiChungTu = '{4}'", cttt.SoChungTu.Trim(), cttt.NgayChungTu.Day, cttt.NgayChungTu.Month, cttt.NgayChungTu.Year,cttt.LoaiChungTu), null);
                    if (list != null && list.Count > 0)
                    {
                        ShowMessage("Số chứng từ  này đã tồn tại ", false);
                        return;
                    }
                }


                //Kiem tra co thong tin to khai

                if (dgTKXDuocChon.RowCount > 0)
                {
                    //Tao danh sach to khai cua chung tu thanh toan -- Tạo mới                    

                    cttt.ChungTuChiTietCollections = new List<CTTT_ChungTuChiTiet>();

                    for (int i = 0; i < dgTKXDuocChon.RowCount; i++)
                    {
                        CTTT_ChungTuChiTiet chiTiet = new CTTT_ChungTuChiTiet();
                        chiTiet.IDCT = cttt.ID;
                        chiTiet.SoToKhai = Convert.ToInt32(dgTKXDuocChon.GetRow(i).Cells["SoToKhai"].Value);
                        chiTiet.MaLoaiHinh = dgTKXDuocChon.GetRow(i).Cells["MaLoaiHinh"].Value.ToString();
                        chiTiet.NgayDangKy = Convert.ToDateTime(dgTKXDuocChon.GetRow(i).Cells["NgayDangKy"].Value);
                        chiTiet.TriGia = Convert.ToDecimal(dgTKXDuocChon.GetRow(i).Cells["TriGia"].Value);
                        chiTiet.MaHaiQuan = dgTKXDuocChon.GetRow(i).Cells["MaHaiQuan"].Value.ToString();
                        cttt.ChungTuChiTietCollections.Add(chiTiet);
                    }

                }
                if (dgChiPhiDuocChon.RowCount > 0)
                {
                    cttt.ChiPhiKhacCollections = new List<CTTT_ChiPhiKhac>();
                    for (int i = 0; i < dgChiPhiDuocChon.RowCount; i++)
                    {
                        CTTT_ChiPhiKhac CTTT_ChiPhiKhac = new CTTT_ChiPhiKhac();
                        CTTT_ChiPhiKhac.IDCT = cttt.ID;
                        CTTT_ChiPhiKhac.MaChiPhi = dgChiPhiDuocChon.GetRow(i).Cells["ID"].Value.ToString();
                        object value = dgChiPhiDuocChon.GetRow(i).Cells["TriGia"].Value;
                        if (!value.Equals(DBNull.Value))
                            CTTT_ChiPhiKhac.TriGia = (decimal)value;
                        CTTT_ChiPhiKhac.GhiChu = dgChiPhiDuocChon.GetRow(i).Cells["GhiChu"].Value.ToString();
                        cttt.ChiPhiKhacCollections.Add(CTTT_ChiPhiKhac);
                    }
                }
                List<CTTT_ChungTuChiTiet> chungtuchitiets = (List<CTTT_ChungTuChiTiet>)CTTT_ChungTuChiTiet.SelectCollectionBy_IDCT(cttt.ID);
                List<CTTT_ChiPhiKhac> chiphikhacs = Company.KDT.SHARE.QuanLyChungTu.CTTT.CTTT_ChiPhiKhac.SelectCollectionBy_IDCT(cttt.ID);

                CTTT_ChungTuChiTiet.DeleteCollection(chungtuchitiets);
                Company.KDT.SHARE.QuanLyChungTu.CTTT.CTTT_ChiPhiKhac.DeleteCollection(chiphikhacs);

                int id = 0;
                if (cttt.ID == 0) id = cttt.Insert();
                else
                {
                    cttt.Update();
                    id = cttt.ID;
                }
                foreach (CTTT_ChungTuChiTiet item in cttt.ChungTuChiTietCollections)
                {
                    item.IDCT = id;
                }
                foreach (CTTT_ChiPhiKhac item in cttt.ChiPhiKhacCollections)
                {
                    item.IDCT = id;
                }

                CTTT_ChungTuChiTiet.InsertCollection(cttt.ChungTuChiTietCollections);
                Company.KDT.SHARE.QuanLyChungTu.CTTT.CTTT_ChiPhiKhac.InsertCollection(cttt.ChiPhiKhacCollections);


                //if (cttt.ID == 0)
                //{
                //    //Them moi Chung tu thanh toan va lay ID
                //    cttt.Insert();                                      
                //}
                Globals.ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage("Quá trình lưu không thành công.\r\nChi tiết: " + ex.Message, false);
            }
        }

        private void btnToRight_Click(object sender, EventArgs e)
        {

            try
            {
                DataTable dtNguon = (DataTable)dgChiPhi.DataSource;
                DataTable dtDich = (DataTable)dgChiPhiDuocChon.DataSource;
                List<DataRow> drItems = new List<DataRow>();

                foreach (GridEXRow row in dgChiPhi.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {
                        DataRowView drView = (DataRowView)row.DataRow;
                        drItems.Add(drView.Row);
                    }
                }

                foreach (DataRow dr in drItems)
                {
                    DataRow myRow = dtDich.NewRow();
                    myRow[0] = dr[0];
                    myRow[1] = dr[1];
                    //myRow[2] = dr[2];
                    dtDich.Rows.Add(myRow);
                    dtNguon.Rows.Remove(dr);
                }

                try
                {
                    dgChiPhi.Refetch();
                }
                catch
                {
                    dgChiPhi.Refresh();
                }
                try
                {

                    dgChiPhiDuocChon.Refetch();
                }
                catch
                {
                    dgChiPhiDuocChon.Refresh();
                }
            }
            catch (Exception ex)
            { }
        }

        private void btnToLeft_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtNguon = (DataTable)dgChiPhi.DataSource;
                DataTable dtDich = (DataTable)dgChiPhiDuocChon.DataSource;
                List<DataRow> drItems = new List<DataRow>();

                foreach (GridEXRow row in dgChiPhiDuocChon.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {

                        DataRowView drView = (DataRowView)row.DataRow;
                        drItems.Add(drView.Row);
                        CTTT_ChiPhiKhac cpk = ChungTuThanhToan.ChiPhiKhacCollections.Find(cp => cp.MaChiPhi == drView.Row[0].ToString());
                        if (cpk != null) ChungTuThanhToan.ChiPhiKhacCollections.Remove(cpk);
                    }
                }

                foreach (DataRow dr in drItems)
                {
                    DataRow myRow = dtNguon.NewRow();
                    myRow[0] = dr[0];
                    myRow[1] = dr[1];
                    dtNguon.Rows.Add(myRow);
                    dtDich.Rows.Remove(dr);
                }

                try
                {
                    dgChiPhi.Refetch();
                }
                catch
                {
                    dgChiPhi.Refresh();
                }
                try
                {

                    dgChiPhiDuocChon.Refetch();
                }
                catch
                {
                    dgChiPhiDuocChon.Refresh();
                }

                getSum();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private decimal getSum()
        {
            decimal tongNhap = 0M;
            try
            {
                dgTKXDuocChon.UpdateData();
                dgChiPhiDuocChon.UpdateData();

                tongNhap = (decimal)dgTKXDuocChon.GetTotalRow().GetSubTotal(dgTKXDuocChon.Tables[0].Columns["TriGia"], AggregateFunction.Sum);

                decimal tongChiPhiKhac = (decimal)dgChiPhiDuocChon.GetTotalRow().GetSubTotal(dgChiPhiDuocChon.Tables[0].Columns["TriGia"], AggregateFunction.Sum);
                tongNhap = tongNhap - tongChiPhiKhac;

                lbTong.Text = tongNhap.ToString("N" + GlobalSettings.SoThapPhan.DinhMuc);

                numConLai.Value = numTongtriGia.Value - tongNhap;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            return tongNhap;
        }

        private void numConLai_ValueChanged(object sender, EventArgs e)
        {
            label16.Text = numConLai.Value.ToString("N" + GlobalSettings.SoThapPhan.DinhMuc);
            lblConLai.Text = Company.BLL.Utils.VNCurrency.ToString(numConLai.Value, cboDongTienThanhToan.SelectedValue.ToString());
        }

        private void numTongtriGia_ValueChanged(object sender, EventArgs e)
        {
            decimal tongnhap = getSum();
            numConLai.Value = numTongtriGia.Value - decimal.Parse(lbTong.Text);
            lblTongTriGia.Text = Company.BLL.Utils.VNCurrency.ToString(numTongtriGia.Value, cboDongTienThanhToan.SelectedValue.ToString());
        }
        private void dgTKXTL_SelectionChanged(object sender, EventArgs e)
        {
            //dgTKXTL_CellValueChanged(null, null);
        }
        private void dgTKXTL_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "TriGia")
            {
                try
                {
                    decimal tongNhap = getSum();

                    //decimal init = 0;
                    //if (e.InitialValue.ToString() != "") init = (decimal)e.InitialValue;
                    //tongNhap += (decimal)e.Value - init;
                    //lbTong.Text = tongNhap.ToString("N" + GlobalSettings.SoThapPhan.DinhMuc);
                    //numConLai.Value = numTongtriGia.Value - tongNhap;
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            }
        }

        private void dgTKXDuocChon_CellUpdated(object sender, ColumnActionEventArgs e)
        {
            if (e.Column.Key == "TriGia")
            {
                try
                {
                    decimal tongNhap = getSum();
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            }
        }

        private void dgChiPhiDuocChon_CellUpdated(object sender, ColumnActionEventArgs e)
        {
            if (e.Column.Key == "TriGia")
            {
                try
                {
                    decimal tongNhap = getSum();
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            }
        }

        private void LoadNguyenTe()
        {

            try
            {
                Cursor = Cursors.WaitCursor;

                DataTable dtNT = Company.KDT.SHARE.Components.Globals.GlobalDanhMucChuanHQ.Tables["NguyenTe"];

                cboDongTienThanhToan.Items.Clear();
                cboDongTienThanhToan.DataSource = dtNT;
                cboDongTienThanhToan.DisplayMember = "Ten";
                cboDongTienThanhToan.ValueMember = "ID";
                if (ChungTuThanhToan.ID == 0)
                {
                    cboDongTienThanhToan.SelectedValue = GlobalSettings.NGUYEN_TE_MAC_DINH;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void numLanThanhLy_ValueChanged(object sender, EventArgs e)
        {
            //if ((int)numLanThanhLy.Value > 0)
            //    LayThongTInHoSoThanhKhoan((int)numLanThanhLy.Value);
            //else
            //    LayTatCaThongTinToKhai();
        }

        private void LayTatCaThongTinToKhai()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                HSTL = new HoSoThanhLyDangKy();
                InitialDSTK();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void LayThongTInHoSoThanhKhoan(int lanThanhLy)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (lanThanhLy > 0)
                {
                    //Load thong tin ho so thanh ly (Neu co)
                    HSTL = new HoSoThanhLyDangKy();
                    HSTL.LanThanhLy = lanThanhLy;
                    HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    //HSTL.GetIdByLanThanhLy(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    HSTL.GetIdByLanThanhLy();
                    HSTL = HoSoThanhLyDangKy.Load(HSTL.ID);
                    HSTL.LoadBKCollection();

                    if (HSTL == null) HSTL = new HoSoThanhLyDangKy();

                    //Kiem tra load bang ke to khai nhap/ xuat Neu chua co
                    foreach (BangKeHoSoThanhLy bk in this.HSTL.BKCollection)
                    {
                        if ((bk.TenBangKe == "DTLTKN" && bk.bkTKNCollection.Count == 0)
                            || (bk.TenBangKe == "DTLTKX" && bk.bkTKNCollection.Count == 0))
                        {
                            bk.LoadChiTietBangKe();
                        }
                    }

                    //Kiem tra chung tu thanh toan
                    if (ChungTuThanhToan.ChungTuChiTietCollections.Count == 0)
                        ChungTuThanhToan.LoadChungTuChiTiet();

                    if (ChungTuThanhToan.ChiPhiKhacCollections.Count == 0)
                        ChungTuThanhToan.LoadChiPhiKhac();
                }

                InitialDSTK();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void cboDongTienThanhToan_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                lblTongTriGia.Text = Company.BLL.Utils.VNCurrency.ToString(numTongtriGia.Value, cboDongTienThanhToan.SelectedValue.ToString());

                lblConLai.Text = Company.BLL.Utils.VNCurrency.ToString(numConLai.Value, cboDongTienThanhToan.SelectedValue.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "BangKeToKhai_ChuaCoCTT" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                sfNPL.ShowDialog(this);

                if (sfNPL.FileName != "")
                {
                    dgTKX.Tables[0].Columns["Select"].Visible = false;
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgTKX;
                    try
                    {
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi mở file " + ex.Message, false);

                    }
                    dgTKX.Tables[0].Columns["Select"].Visible = true;

                }
                dgTKX.Refetch();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}
