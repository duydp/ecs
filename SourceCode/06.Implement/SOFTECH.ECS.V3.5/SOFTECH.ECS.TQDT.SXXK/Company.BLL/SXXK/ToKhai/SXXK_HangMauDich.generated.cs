﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.SXXK.ToKhai
{
	public partial class SXXK_HangMauDich : ICloneable
	{
		#region Properties.
		
		public int SoToKhai { set; get; }
		public string MaLoaiHinh { set; get; }
		public string MaHaiQuan { set; get; }
		public short NamDangKy { set; get; }
		public short SoThuTuHang { set; get; }
		public string MaHS { set; get; }
		public string MaPhu { set; get; }
		public string TenHang { set; get; }
		public string NuocXX_ID { set; get; }
		public string DVT_ID { set; get; }
		public decimal SoLuong { set; get; }
		public decimal DonGiaKB { set; get; }
		public decimal DonGiaTT { set; get; }
		public decimal TriGiaKB { set; get; }
		public decimal TriGiaTT { set; get; }
		public decimal TriGiaKB_VND { set; get; }
		public decimal ThueSuatXNK { set; get; }
		public decimal ThueSuatTTDB { set; get; }
		public decimal ThueSuatGTGT { set; get; }
		public decimal ThueXNK { set; get; }
		public decimal ThueTTDB { set; get; }
		public decimal ThueGTGT { set; get; }
		public decimal PhuThu { set; get; }
		public decimal TyLeThuKhac { set; get; }
		public decimal TriGiaThuKhac { set; get; }
		public byte MienThue { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<SXXK_HangMauDich> ConvertToCollection(IDataReader reader)
		{
			List<SXXK_HangMauDich> collection = new List<SXXK_HangMauDich>();
			while (reader.Read())
			{
				SXXK_HangMauDich entity = new SXXK_HangMauDich();
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt16(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_SXXK_HangMauDich VALUES(@SoToKhai, @MaLoaiHinh, @MaHaiQuan, @NamDangKy, @SoThuTuHang, @MaHS, @MaPhu, @TenHang, @NuocXX_ID, @DVT_ID, @SoLuong, @DonGiaKB, @DonGiaTT, @TriGiaKB, @TriGiaTT, @TriGiaKB_VND, @ThueSuatXNK, @ThueSuatTTDB, @ThueSuatGTGT, @ThueXNK, @ThueTTDB, @ThueGTGT, @PhuThu, @TyLeThuKhac, @TriGiaThuKhac, @MienThue)";
            string update = "UPDATE t_SXXK_HangMauDich SET SoToKhai = @SoToKhai, MaLoaiHinh = @MaLoaiHinh, MaHaiQuan = @MaHaiQuan, NamDangKy = @NamDangKy, SoThuTuHang = @SoThuTuHang, MaHS = @MaHS, MaPhu = @MaPhu, TenHang = @TenHang, NuocXX_ID = @NuocXX_ID, DVT_ID = @DVT_ID, SoLuong = @SoLuong, DonGiaKB = @DonGiaKB, DonGiaTT = @DonGiaTT, TriGiaKB = @TriGiaKB, TriGiaTT = @TriGiaTT, TriGiaKB_VND = @TriGiaKB_VND, ThueSuatXNK = @ThueSuatXNK, ThueSuatTTDB = @ThueSuatTTDB, ThueSuatGTGT = @ThueSuatGTGT, ThueXNK = @ThueXNK, ThueTTDB = @ThueTTDB, ThueGTGT = @ThueGTGT, PhuThu = @PhuThu, TyLeThuKhac = @TyLeThuKhac, TriGiaThuKhac = @TriGiaThuKhac, MienThue = @MienThue WHERE ID = @ID";
            string delete = "DELETE FROM t_SXXK_HangMauDich WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Int, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.Char, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamDangKy", SqlDbType.SmallInt, "NamDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuHang", SqlDbType.SmallInt, "SoThuTuHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaKB", SqlDbType.Decimal, "TriGiaKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaKB_VND", SqlDbType.Decimal, "TriGiaKB_VND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatXNK", SqlDbType.Decimal, "ThueSuatXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTTDB", SqlDbType.Decimal, "ThueSuatTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatGTGT", SqlDbType.Decimal, "ThueSuatGTGT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueXNK", SqlDbType.Money, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueTTDB", SqlDbType.Money, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueGTGT", SqlDbType.Money, "ThueGTGT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuThu", SqlDbType.Money, "PhuThu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeThuKhac", SqlDbType.Decimal, "TyLeThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaThuKhac", SqlDbType.Money, "TriGiaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MienThue", SqlDbType.TinyInt, "MienThue", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Int, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.Char, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamDangKy", SqlDbType.SmallInt, "NamDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuHang", SqlDbType.SmallInt, "SoThuTuHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaKB", SqlDbType.Decimal, "TriGiaKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaKB_VND", SqlDbType.Decimal, "TriGiaKB_VND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatXNK", SqlDbType.Decimal, "ThueSuatXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTTDB", SqlDbType.Decimal, "ThueSuatTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatGTGT", SqlDbType.Decimal, "ThueSuatGTGT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueXNK", SqlDbType.Money, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueTTDB", SqlDbType.Money, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueGTGT", SqlDbType.Money, "ThueGTGT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuThu", SqlDbType.Money, "PhuThu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeThuKhac", SqlDbType.Decimal, "TyLeThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaThuKhac", SqlDbType.Money, "TriGiaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MienThue", SqlDbType.TinyInt, "MienThue", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@SoToKhai", SqlDbType.Int, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaLoaiHinh", SqlDbType.Char, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@NamDangKy", SqlDbType.SmallInt, "NamDangKy", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@SoThuTuHang", SqlDbType.SmallInt, "SoThuTuHang", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_SXXK_HangMauDich VALUES(@SoToKhai, @MaLoaiHinh, @MaHaiQuan, @NamDangKy, @SoThuTuHang, @MaHS, @MaPhu, @TenHang, @NuocXX_ID, @DVT_ID, @SoLuong, @DonGiaKB, @DonGiaTT, @TriGiaKB, @TriGiaTT, @TriGiaKB_VND, @ThueSuatXNK, @ThueSuatTTDB, @ThueSuatGTGT, @ThueXNK, @ThueTTDB, @ThueGTGT, @PhuThu, @TyLeThuKhac, @TriGiaThuKhac, @MienThue)";
            string update = "UPDATE t_SXXK_HangMauDich SET SoToKhai = @SoToKhai, MaLoaiHinh = @MaLoaiHinh, MaHaiQuan = @MaHaiQuan, NamDangKy = @NamDangKy, SoThuTuHang = @SoThuTuHang, MaHS = @MaHS, MaPhu = @MaPhu, TenHang = @TenHang, NuocXX_ID = @NuocXX_ID, DVT_ID = @DVT_ID, SoLuong = @SoLuong, DonGiaKB = @DonGiaKB, DonGiaTT = @DonGiaTT, TriGiaKB = @TriGiaKB, TriGiaTT = @TriGiaTT, TriGiaKB_VND = @TriGiaKB_VND, ThueSuatXNK = @ThueSuatXNK, ThueSuatTTDB = @ThueSuatTTDB, ThueSuatGTGT = @ThueSuatGTGT, ThueXNK = @ThueXNK, ThueTTDB = @ThueTTDB, ThueGTGT = @ThueGTGT, PhuThu = @PhuThu, TyLeThuKhac = @TyLeThuKhac, TriGiaThuKhac = @TriGiaThuKhac, MienThue = @MienThue WHERE ID = @ID";
            string delete = "DELETE FROM t_SXXK_HangMauDich WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Int, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.Char, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamDangKy", SqlDbType.SmallInt, "NamDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuHang", SqlDbType.SmallInt, "SoThuTuHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaKB", SqlDbType.Decimal, "TriGiaKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaKB_VND", SqlDbType.Decimal, "TriGiaKB_VND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatXNK", SqlDbType.Decimal, "ThueSuatXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTTDB", SqlDbType.Decimal, "ThueSuatTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatGTGT", SqlDbType.Decimal, "ThueSuatGTGT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueXNK", SqlDbType.Money, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueTTDB", SqlDbType.Money, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueGTGT", SqlDbType.Money, "ThueGTGT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuThu", SqlDbType.Money, "PhuThu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeThuKhac", SqlDbType.Decimal, "TyLeThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaThuKhac", SqlDbType.Money, "TriGiaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MienThue", SqlDbType.TinyInt, "MienThue", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Int, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.Char, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamDangKy", SqlDbType.SmallInt, "NamDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuHang", SqlDbType.SmallInt, "SoThuTuHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaKB", SqlDbType.Decimal, "TriGiaKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaKB_VND", SqlDbType.Decimal, "TriGiaKB_VND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatXNK", SqlDbType.Decimal, "ThueSuatXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTTDB", SqlDbType.Decimal, "ThueSuatTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatGTGT", SqlDbType.Decimal, "ThueSuatGTGT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueXNK", SqlDbType.Money, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueTTDB", SqlDbType.Money, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueGTGT", SqlDbType.Money, "ThueGTGT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuThu", SqlDbType.Money, "PhuThu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeThuKhac", SqlDbType.Decimal, "TyLeThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaThuKhac", SqlDbType.Money, "TriGiaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MienThue", SqlDbType.TinyInt, "MienThue", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@SoToKhai", SqlDbType.Int, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaLoaiHinh", SqlDbType.Char, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@NamDangKy", SqlDbType.SmallInt, "NamDangKy", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@SoThuTuHang", SqlDbType.SmallInt, "SoThuTuHang", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static SXXK_HangMauDich Load(int soToKhai, string maLoaiHinh, string maHaiQuan, short namDangKy, short soThuTuHang, string maPhu, decimal soLuong, decimal donGiaTT)
		{
			const string spName = "[dbo].[p_SXXK_HangMauDich_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, namDangKy);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, soThuTuHang);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maPhu);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, soLuong);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, donGiaTT);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<SXXK_HangMauDich> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<SXXK_HangMauDich> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<SXXK_HangMauDich> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_SXXK_HangMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SXXK_HangMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_SXXK_HangMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SXXK_HangMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertSXXK_HangMauDich(int soToKhai, string maLoaiHinh, string maHaiQuan, short namDangKy, short soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, decimal donGiaKB, decimal donGiaTT, decimal triGiaKB, decimal triGiaTT, decimal triGiaKB_VND, decimal thueSuatXNK, decimal thueSuatTTDB, decimal thueSuatGTGT, decimal thueXNK, decimal thueTTDB, decimal thueGTGT, decimal phuThu, decimal tyLeThuKhac, decimal triGiaThuKhac, byte mienThue)
		{
			SXXK_HangMauDich entity = new SXXK_HangMauDich();	
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.NamDangKy = namDangKy;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHS = maHS;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.NuocXX_ID = nuocXX_ID;
			entity.DVT_ID = dVT_ID;
			entity.SoLuong = soLuong;
			entity.DonGiaKB = donGiaKB;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaKB = triGiaKB;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaKB_VND = triGiaKB_VND;
			entity.ThueSuatXNK = thueSuatXNK;
			entity.ThueSuatTTDB = thueSuatTTDB;
			entity.ThueSuatGTGT = thueSuatGTGT;
			entity.ThueXNK = thueXNK;
			entity.ThueTTDB = thueTTDB;
			entity.ThueGTGT = thueGTGT;
			entity.PhuThu = phuThu;
			entity.TyLeThuKhac = tyLeThuKhac;
			entity.TriGiaThuKhac = triGiaThuKhac;
			entity.MienThue = mienThue;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_SXXK_HangMauDich_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, NamDangKy);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
			db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
			db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
			db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
			db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
			db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
			db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<SXXK_HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_HangMauDich item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateSXXK_HangMauDich(int soToKhai, string maLoaiHinh, string maHaiQuan, short namDangKy, short soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, decimal donGiaKB, decimal donGiaTT, decimal triGiaKB, decimal triGiaTT, decimal triGiaKB_VND, decimal thueSuatXNK, decimal thueSuatTTDB, decimal thueSuatGTGT, decimal thueXNK, decimal thueTTDB, decimal thueGTGT, decimal phuThu, decimal tyLeThuKhac, decimal triGiaThuKhac, byte mienThue)
		{
			SXXK_HangMauDich entity = new SXXK_HangMauDich();			
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.NamDangKy = namDangKy;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHS = maHS;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.NuocXX_ID = nuocXX_ID;
			entity.DVT_ID = dVT_ID;
			entity.SoLuong = soLuong;
			entity.DonGiaKB = donGiaKB;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaKB = triGiaKB;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaKB_VND = triGiaKB_VND;
			entity.ThueSuatXNK = thueSuatXNK;
			entity.ThueSuatTTDB = thueSuatTTDB;
			entity.ThueSuatGTGT = thueSuatGTGT;
			entity.ThueXNK = thueXNK;
			entity.ThueTTDB = thueTTDB;
			entity.ThueGTGT = thueGTGT;
			entity.PhuThu = phuThu;
			entity.TyLeThuKhac = tyLeThuKhac;
			entity.TriGiaThuKhac = triGiaThuKhac;
			entity.MienThue = mienThue;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_SXXK_HangMauDich_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, NamDangKy);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
			db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
			db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
			db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
			db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
			db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
			db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<SXXK_HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_HangMauDich item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateSXXK_HangMauDich(int soToKhai, string maLoaiHinh, string maHaiQuan, short namDangKy, short soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, decimal donGiaKB, decimal donGiaTT, decimal triGiaKB, decimal triGiaTT, decimal triGiaKB_VND, decimal thueSuatXNK, decimal thueSuatTTDB, decimal thueSuatGTGT, decimal thueXNK, decimal thueTTDB, decimal thueGTGT, decimal phuThu, decimal tyLeThuKhac, decimal triGiaThuKhac, byte mienThue)
		{
			SXXK_HangMauDich entity = new SXXK_HangMauDich();			
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.NamDangKy = namDangKy;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHS = maHS;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.NuocXX_ID = nuocXX_ID;
			entity.DVT_ID = dVT_ID;
			entity.SoLuong = soLuong;
			entity.DonGiaKB = donGiaKB;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaKB = triGiaKB;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaKB_VND = triGiaKB_VND;
			entity.ThueSuatXNK = thueSuatXNK;
			entity.ThueSuatTTDB = thueSuatTTDB;
			entity.ThueSuatGTGT = thueSuatGTGT;
			entity.ThueXNK = thueXNK;
			entity.ThueTTDB = thueTTDB;
			entity.ThueGTGT = thueGTGT;
			entity.PhuThu = phuThu;
			entity.TyLeThuKhac = tyLeThuKhac;
			entity.TriGiaThuKhac = triGiaThuKhac;
			entity.MienThue = mienThue;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SXXK_HangMauDich_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, NamDangKy);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
			db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
			db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
			db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
			db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
			db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
			db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<SXXK_HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_HangMauDich item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteSXXK_HangMauDich(int soToKhai, string maLoaiHinh, string maHaiQuan, short namDangKy, short soThuTuHang, string maPhu, decimal soLuong, decimal donGiaTT)
		{
			SXXK_HangMauDich entity = new SXXK_HangMauDich();
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.NamDangKy = namDangKy;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaPhu = maPhu;
			entity.SoLuong = soLuong;
			entity.DonGiaTT = donGiaTT;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SXXK_HangMauDich_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, NamDangKy);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_SXXK_HangMauDich_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<SXXK_HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_HangMauDich item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}