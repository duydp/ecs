﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
   public class CX_ThongBaoGiaiTrinh_ChenhLech:DeclarationBase
    {
       /// <summary>
       /// 
       /// </summary>
        [XmlElement("AditionalInformation")]
       public List<AdditionalInformation> AditionalInformationL { get; set; }
       /// <summary>
       /// 
       /// </summary>
       [XmlElement("CustomsGoodsItem")]
        public List<CustomsGoodsItem> CustomsGoodsItemL { get; set; }
    }
}
