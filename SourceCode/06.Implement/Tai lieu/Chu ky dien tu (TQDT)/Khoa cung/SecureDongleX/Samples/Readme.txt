Please do not forget to copy the required library file(s) to the correct folders.
For example:
Copy SDX.dll found on \API\Dynamic to Windows\System32 folder OR to the folder where the .exe resides.
Copy the correct version of SDX.dcu for Delphi sample that uses dcu.
Copy the correct version of SDX.lib for C++ sample that uses lib.

support@securemetric.com
www.Securemetric.com
Formula for Strong Digital Security