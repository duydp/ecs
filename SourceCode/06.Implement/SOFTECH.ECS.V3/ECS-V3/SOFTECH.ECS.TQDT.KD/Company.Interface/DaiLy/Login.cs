﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
using Company.KD.BLL.KDT;
using Company.KD.BLL.Utils;
using Company.KDT.SHARE.Components;

namespace Company.Interface.DaiLy
{
    public partial class Login : Form
    {
        protected Company.Controls.MessageBoxControl _MsgBox;
        public Login()
        {
            InitializeComponent();
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new Company.Controls.MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private bool checkSoLuongDoanhNghiep()
        {
            KdtObj obj = Company.KD.BLL.Utils.KDT.getSoLuongDoanhNghiep();
            int soLuongDN = Company.KD.BLL.KDT.HeThongPhongKhai.SelectedCountDN();
            if (soLuongDN > obj.SoLuong)
                return true;
            return false;
            //return true;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            // Command -  KhanhHN - 11.06.2012
            //GlobalSettings.DATABASE_NAME = Properties.Settings.Default.DATABASE_NAME;
            //GlobalSettings.USER = Properties.Settings.Default.user;
            //GlobalSettings.SERVER_NAME = Properties.Settings.Default.ServerName;
            //GlobalSettings.PASS = Properties.Settings.Default.pass;

            //DATLMQ bo sung kiem tra gia tri null o cbMaDoanhNghiep
            if (cbMaDoanhNghiep.Value == null)
                ShowMessage("Vui lòng chọn mã doanh nghiệp.", false);
            else
            {
                #region kiểm tra cấu hình đại lý
                try
                {
                    ISyncData myService = WebService.SyncService();
                    string check = myService.CheckCauHinhDL(cbMaDoanhNghiep.Value.ToString().Trim());
                    GlobalSettings.SOTOKHAI_DONGBO = 0;
                    if (int.TryParse(check, out GlobalSettings.SOTOKHAI_DONGBO))
                    {
                        if (GlobalSettings.SOTOKHAI_DONGBO > 0)
                        {
                            GlobalSettings.ISKHAIBAO = (GlobalSettings.SOTOKHAI_DONGBO > new ToKhaiMauDich().SelectCountSoTKThongQuan(cbMaDoanhNghiep.Value.ToString().Trim()));
                        }
                        else
                        {
                            GlobalSettings.ISKHAIBAO = true;
                        }
                    }
                    else
                    {
                        //if (check == "NULL")
                        //    ShowMessage("Doanh nghiệp chưa cấu hình giới hạn số tờ khai cho đại lý", false);
                        //else
                        //    ShowMessage("Lỗi kết nối kiểm tra cấu hình: " + check, false);
                        //GlobalSettings.ISKHAIBAO = false;
                    }
                }

                catch (Exception ex)
                {
                    GlobalSettings.ISKHAIBAO = false;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                   // ShowMessage("Lỗi kiểm tra cấu hình cho đại lý: " + ex.Message, false);
                }

                #endregion
                DataTable dtCauHinh = new Company.KD.BLL.KDT.HeThongPhongKhai().SelectCauHinhByMaDoanhNghiep(cbMaDoanhNghiep.Value.ToString()).Tables[0];
                if (dtCauHinh.Rows.Count == 0)
                {
                    //MessageBox.Show("Không có mã doanh nghiệp này.");
                    ShowMessage("Không có mã doanh nghiệp này.", false);
                    return;
                }
                GlobalSettings.MA_DON_VI = cbMaDoanhNghiep.Value.ToString().Trim();
                GlobalSettings.TEN_DON_VI = dtCauHinh.Rows[0]["TenDoanhNghiep"].ToString();
                GlobalSettings.DIA_CHI = GetValue(dtCauHinh, "DIA_CHI");
                GlobalSettings.MaMID = GetValue(dtCauHinh, "MaMID");
                if (GetValue(dtCauHinh, "MA_HAI_QUAN") != "")
                    GlobalSettings.MA_HAI_QUAN = GetValue(dtCauHinh, "MA_HAI_QUAN");
                if (GetValue(dtCauHinh, "TEN_HAI_QUAN") != "")
                    GlobalSettings.TEN_HAI_QUAN = GetValue(dtCauHinh, "TEN_HAI_QUAN");
                if (GetValue(dtCauHinh, "MA_CUC_HAI_QUAN") != "")
                    GlobalSettings.MA_CUC_HAI_QUAN = GetValue(dtCauHinh, "MA_CUC_HAI_QUAN");
                if (GetValue(dtCauHinh, "TEN_CUC_HAI_QUAN") != "")
                    GlobalSettings.TEN_CUC_HAI_QUAN = GetValue(dtCauHinh, "TEN_CUC_HAI_QUAN");
                if (GetValue(dtCauHinh, "TEN_HAI_QUAN_NGAN") != "")
                    GlobalSettings.TEN_HAI_QUAN_NGAN = GetValue(dtCauHinh, "TEN_HAI_QUAN_NGAN");
                if (GetValue(dtCauHinh, "MailHaiQuan") != "")
                    GlobalSettings.MailHaiQuan = GetValue(dtCauHinh, "MailHaiQuan");
                if (GetValue(dtCauHinh, "TEN_DOI_TAC") != "")
                    GlobalSettings.TEN_DOI_TAC = GetValue(dtCauHinh, "TEN_DOI_TAC");
                if (GetValue(dtCauHinh, "MaHTS") != "")
                    GlobalSettings.MaHTS = Convert.ToInt32(GetValue(dtCauHinh, "MaHTS"));
                if (GetValue(dtCauHinh, "TuDongTinhThue") != "")
                    GlobalSettings.TuDongTinhThue = GetValue(dtCauHinh, "TuDongTinhThue");
                if (GetValue(dtCauHinh, "CUA_KHAU") != "")
                    GlobalSettings.CUA_KHAU = GetValue(dtCauHinh, "CUA_KHAU");
                if (GetValue(dtCauHinh, "DIA_DIEM_DO_HANG") != "")
                    GlobalSettings.DIA_DIEM_DO_HANG = GetValue(dtCauHinh, "DIA_DIEM_DO_HANG");
                if (GetValue(dtCauHinh, "DKGH_MAC_DINH") != "")
                    GlobalSettings.DKGH_MAC_DINH = GetValue(dtCauHinh, "DKGH_MAC_DINH");
                if (GetValue(dtCauHinh, "DVT_MAC_DINH") != "")
                    GlobalSettings.DVT_MAC_DINH = GetValue(dtCauHinh, "DVT_MAC_DINH");
                GlobalSettings.LOAI_HINH = "NSX01";
                if (GetValue(dtCauHinh, "NGUYEN_TE_MAC_DINH") != "")
                    GlobalSettings.NGUYEN_TE_MAC_DINH = GetValue(dtCauHinh, "NGUYEN_TE_MAC_DINH");
                if (GetValue(dtCauHinh, "NUOC") != "")
                    GlobalSettings.NUOC = GetValue(dtCauHinh, "NUOC");
                if (GetValue(dtCauHinh, "PTTT_MAC_DINH") != "")
                    GlobalSettings.PTTT_MAC_DINH = GetValue(dtCauHinh, "PTTT_MAC_DINH");
                if (GetValue(dtCauHinh, "PTVT_MAC_DINH") != "")
                    GlobalSettings.PTVT_MAC_DINH = GetValue(dtCauHinh, "PTVT_MAC_DINH");
                //if (GetValue(dtCauHinh, "ThongBaoHetHan") != "")
                //    GlobalSettings.thong = Convert.ToInt32(GetValue(dtCauHinh, "ThongBaoHetHan"));
                GlobalSettings.KhoiTao_GiaTriMacDinh();
                
                //MainForm.EcsQuanTri = ECSPrincipal.LayDanhSachQuyen();
                MainForm.isLoginSuccess = true;
                this.Close();
            }

        }
        public void GetValueConfig(string madoanhnghiep)
        {

            if (GlobalSettings.IsOnlyMe) return;
            DataTable dtCauHinh = new Company.KD.BLL.KDT.HeThongPhongKhai().SelectCauHinhByMaDoanhNghiep(madoanhnghiep).Tables[0];
            GlobalSettings.MA_DON_VI = madoanhnghiep;
            string tendoanhnghiep = new Company.KD.BLL.KDT.HeThongPhongKhai().SelectedSettingsName(madoanhnghiep, "DIA_CHI");
            if (!string.IsNullOrEmpty(tendoanhnghiep))
                GlobalSettings.TEN_DON_VI = tendoanhnghiep;
            GlobalSettings.DIA_CHI = GetValue(dtCauHinh, "DIA_CHI");
            GlobalSettings.MaMID = GetValue(dtCauHinh, "MaMID");
            if (GetValue(dtCauHinh, "IsSignRemote") != "")
            {
                Company.KDT.SHARE.Components.Globals.IsSignRemote = bool.Parse(GetValue(dtCauHinh, "IsSignRemote"));
                Company.KDT.SHARE.Components.Globals.IsSignature = Company.KDT.SHARE.Components.Globals.IsSignRemote;
            }
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsSignRemote", Company.KDT.SHARE.Components.Globals.IsSignRemote == true ? "true" : "false");
            if (GetValue(dtCauHinh, "MA_HAI_QUAN") != "")
                GlobalSettings.MA_HAI_QUAN = GetValue(dtCauHinh, "MA_HAI_QUAN");
            if (GetValue(dtCauHinh, "TEN_HAI_QUAN") != "")
                GlobalSettings.TEN_HAI_QUAN = GetValue(dtCauHinh, "TEN_HAI_QUAN");
            if (GetValue(dtCauHinh, "MA_CUC_HAI_QUAN") != "")
                GlobalSettings.MA_CUC_HAI_QUAN = GetValue(dtCauHinh, "MA_CUC_HAI_QUAN");
            if (GetValue(dtCauHinh, "TEN_CUC_HAI_QUAN") != "")
                GlobalSettings.TEN_CUC_HAI_QUAN = GetValue(dtCauHinh, "TEN_CUC_HAI_QUAN");
            if (GetValue(dtCauHinh, "TEN_HAI_QUAN_NGAN") != "")
                GlobalSettings.TEN_HAI_QUAN_NGAN = GetValue(dtCauHinh, "TEN_HAI_QUAN_NGAN");
            if (GetValue(dtCauHinh, "MailHaiQuan") != "")
                GlobalSettings.MailHaiQuan = GetValue(dtCauHinh, "MailHaiQuan");
            if (GetValue(dtCauHinh, "TEN_DOI_TAC") != "")
                GlobalSettings.TEN_DOI_TAC = GetValue(dtCauHinh, "TEN_DOI_TAC");
            if (GetValue(dtCauHinh, "MaHTS") != "")
                GlobalSettings.MaHTS = Convert.ToInt32(GetValue(dtCauHinh, "MaHTS"));
            if (GetValue(dtCauHinh, "TuDongTinhThue") != "")
                GlobalSettings.TuDongTinhThue = GetValue(dtCauHinh, "TuDongTinhThue");
            if (GetValue(dtCauHinh, "CUA_KHAU") != "")
                GlobalSettings.CUA_KHAU = GetValue(dtCauHinh, "CUA_KHAU");
            if (GetValue(dtCauHinh, "DIA_DIEM_DO_HANG") != "")
                GlobalSettings.DIA_DIEM_DO_HANG = GetValue(dtCauHinh, "DIA_DIEM_DO_HANG");
            if (GetValue(dtCauHinh, "DKGH_MAC_DINH") != "")
                GlobalSettings.DKGH_MAC_DINH = GetValue(dtCauHinh, "DKGH_MAC_DINH");
            if (GetValue(dtCauHinh, "DVT_MAC_DINH") != "")
                GlobalSettings.DVT_MAC_DINH = GetValue(dtCauHinh, "DVT_MAC_DINH");
            GlobalSettings.LOAI_HINH = "NSX01";
            if (GetValue(dtCauHinh, "NGUYEN_TE_MAC_DINH") != "")
                GlobalSettings.NGUYEN_TE_MAC_DINH = GetValue(dtCauHinh, "NGUYEN_TE_MAC_DINH");
            if (GetValue(dtCauHinh, "NUOC") != "")
                GlobalSettings.NUOC = GetValue(dtCauHinh, "NUOC");
            if (GetValue(dtCauHinh, "PTTT_MAC_DINH") != "")
                GlobalSettings.PTTT_MAC_DINH = GetValue(dtCauHinh, "PTTT_MAC_DINH");
            if (GetValue(dtCauHinh, "PTVT_MAC_DINH") != "")
                GlobalSettings.PTVT_MAC_DINH = GetValue(dtCauHinh, "PTVT_MAC_DINH");
            if (GetValue(dtCauHinh, "ThongBaoHetHan") != "")
                GlobalSettings.ThongBaoHetHan = Convert.ToInt32(GetValue(dtCauHinh, "ThongBaoHetHan"));
            if (GetValue(dtCauHinh, "SoTienKhoanTKN") != "")
                GlobalSettings.SoTienKhoanTKN = Convert.ToDecimal(GetValue(dtCauHinh, "SoTienKhoanTKN"));
            if (GetValue(dtCauHinh, "SoTienKhoanTKX") != "")
                GlobalSettings.SoTienKhoanTKX = Convert.ToDecimal(GetValue(dtCauHinh, "SoTienKhoanTKX"));
            if (GetValue(dtCauHinh, "DinhMuc") != "")
                GlobalSettings.SoThapPhan.DinhMuc = Convert.ToInt32(GetValue(dtCauHinh, "DinhMuc"));
            if (GetValue(dtCauHinh, "DiaChiWS") != "")
                GlobalSettings.DiaChiWS = "http://"+GetValue(dtCauHinh, "DIA_CHI_HQ") + "/" + GetValue(dtCauHinh, "KDTService/CISService.asmx");
            //else
            //    GlobalSettings.SoThapPhan.DinhMuc = 5;
            if (GetValue(dtCauHinh, "LuongNPL") != "")
                GlobalSettings.SoThapPhan.LuongNPL = Convert.ToInt32(GetValue(dtCauHinh, "LuongNPL"));
            //else
            //    GlobalSettings.SoThapPhan.LuongNPL = 5;
            if (GetValue(dtCauHinh, "LuongSP") != "")
                GlobalSettings.SoThapPhan.LuongSP = Convert.ToInt32(GetValue(dtCauHinh, "LuongSP"));
            //else
            //    GlobalSettings.SoThapPhan.LuongSP = 0;

            if (GetValue(dtCauHinh, "SapXepTheoTK") != "")
                GlobalSettings.SoThapPhan.SapXepTheoTK = Convert.ToInt32(GetValue(dtCauHinh, "SapXepTheoTK"));
            //else
            //    GlobalSettings.SoThapPhan.SapXepTheoTK = 1;
            if (GetValue(dtCauHinh, "NPLKoTK") != "")
                GlobalSettings.SoThapPhan.NPLKoTK = Convert.ToInt32(GetValue(dtCauHinh, "NPLKoTK"));

            if (!string.IsNullOrEmpty(GetValue(dtCauHinh, "DIA_CHI_HQ")) && !string.IsNullOrEmpty(GetValue(dtCauHinh, "TEN_DICH_VU")))
                GlobalSettings.DiaChiWS = GetValue(dtCauHinh, "DIA_CHI_HQ") + "/" + GetValue(dtCauHinh, "TEN_DICH_VU");
            //else
            //    GlobalSettings.SoThapPhan.NPLKoTK = 0;
            //if (GetValue(dtCauHinh, "MauBC01") != "")
            //    GlobalSettings.SoThapPhan.MauBC01 = Convert.ToInt32(GetValue(dtCauHinh, "MauBC01"));
            ////else
            ////    GlobalSettings.SoThapPhan.MauBC01 = 1;
            //if (GetValue(dtCauHinh, "TachLam2") != "")
            //    GlobalSettings.SoThapPhan.TachLam2 = Convert.ToInt32(GetValue(dtCauHinh, "TachLam2"));
            ////else
            ////    GlobalSettings.SoThapPhan.TachLam2 = 1;
            //if (GetValue(dtCauHinh, "MauBC04") != "")
            //    GlobalSettings.SoThapPhan.MauBC04 = Convert.ToInt32(GetValue(dtCauHinh, "MauBC04"));
            //else
            //    GlobalSettings.SoThapPhan.MauBC04 = 1;
            //GlobalSettings.Luu_SoThapPhan(GlobalSettings.SoThapPhan.DinhMuc, GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.LuongSP, GlobalSettings.SoThapPhan.SapXepTheoTK, GlobalSettings.SoThapPhan.NPLKoTK, GlobalSettings.SoThapPhan.MauBC01, GlobalSettings.SoThapPhan.TachLam2, GlobalSettings.SoThapPhan.MauBC04, GlobalSettings.SoThapPhan.TLHH);

        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog(this);
        }
        private string GetValue(DataTable dtCauHinh, string key)
        {
            foreach (DataRow dr in dtCauHinh.Rows)
            {
                if (dr["Key_Config"].ToString().Trim().ToLower() == key.Trim().ToLower()) return dr["Value_Config"].ToString();
            }
            return "";
        }
        private void Login_Load(object sender, EventArgs e)
        {
            try
            {
                KdtObj obj = null;
                try { obj = Company.KD.BLL.Utils.KDT.getSoLuongDoanhNghiep(); }
                catch { }
                if (obj != null)
                {
                    if (checkSoLuongDoanhNghiep())
                    {
                        ShowMessage("Bạn đã nhập số quá số lượng quy định doanh nghiệp được khai báo.", false);
                    }
                }
                else
                {
                    obj = new KdtObj();
                    obj.SoLuong = 1;
                }

                DataTable dtMaDoanhNghiep = new Company.KD.BLL.KDT.HeThongPhongKhai().SelectDanhSachDoanhNghiep((int)obj.SoLuong).Tables[0];
                foreach (DataRow row in dtMaDoanhNghiep.Rows)
                {
                    row["TenDoanhNghiep"] = row["MaDoanhNghiep"] + " : " + row["TenDoanhNghiep"];
                }

                if (dtMaDoanhNghiep.Rows.Count > 0)
                {
                    cbMaDoanhNghiep.DataSource = dtMaDoanhNghiep;
                    cbMaDoanhNghiep.DisplayMember = "TenDoanhNghiep";
                    cbMaDoanhNghiep.ValueMember = "MaDoanhNghiep";
                    cbMaDoanhNghiep.SelectedIndex = 0;
                }
            }
            catch
            {
                ShowMessage("Không kết nối được tới cơ sở dữ liệu.", false);
            }
        }
    }
}