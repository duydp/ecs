﻿using System;
using System.Windows.Forms;
using Company.KD.BLL;
using Company.KD.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL.SXXK.ToKhai;
using Company.KDT.SHARE.Components;
//using Company.Interface.Reports;

namespace Company.Interface.SXXK
{    
    public partial class ToKhaiMauDichDetailForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();       
        public string NhomLoaiHinh = string.Empty;

        public ToKhaiMauDichDetailForm()
        {
            InitializeComponent();            
        }

        private void loadTKMDData()
        {
            ctrDonViHaiQuan.Ma = this.TKMD.MaHaiQuan;
            txtSoLuongPLTK.Value = this.TKMD.SoLuongPLTK;
            txtSotk.Text = this.TKMD.SoToKhai.ToString();
            if (this.TKMD.TrangThaiThanhKhoan == "S")
               // lblTrangThai.Text = setText( "Đã được tính thuế sau kiểm hóa","");         
            if (this.TKMD.TrangThaiThanhKhoan == "K")
              //  lblTrangThai.Text = setText("Đã được kiểm hóa","");
            if (this.TKMD.TrangThaiThanhKhoan == "C")
              //  lblTrangThai.Text = "Đã được điều chỉnh thuế";
            // Doanh nghiệp.
            txtMaDonVi.Text = this.TKMD.MaDoanhNghiep;
            //txtTenDonVi.Text = this.TKMD.TenDoanhNghiep;
          
            // Đơn vị đối tác.
            txtTenDonViDoiTac.Text = this.TKMD.TenDonViDoiTac;
           
            // Đại lý TTHQ.
            txtMaDaiLy.Text = this.TKMD.MaDaiLyTTHQ;
            txtTenDaiLy.Text = this.TKMD.TenDaiLyTTHQ;

            txtMaDonViUyThac.Text = this.TKMD.MaDonViUT;
            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Ma = this.TKMD.MaLoaiHinh;
            
            // Phương tiện vận tải.
            cbPTVT.SelectedValue = this.TKMD.PTVT_ID;
            txtSoHieuPTVT.Text = this.TKMD.SoHieuPTVT;
            ccNgayDen.Value = this.TKMD.NgayDenPTVT;
          
            // Nước.
            if (this.TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                ctrNuocXuatKhau.Ma = this.TKMD.NuocXK_ID;
            else
                ctrNuocXuatKhau.Ma = this.TKMD.NuocNK_ID;
           
            // ĐKGH.
            cbDKGH.SelectedValue = this.TKMD.DKGH_ID;
           
            // PTTT.
            cbPTTT.SelectedValue = this.TKMD.PTTT_ID;
           
            // Giấy phép.
            txtSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep.Year > 1900) ccNgayGiayPhep.Value = this.TKMD.NgayGiayPhep;
            if (this.TKMD.NgayHetHanGiayPhep.Year > 1900) ccNgayHHGiayPhep.Value = this.TKMD.NgayHetHanGiayPhep;
            if (this.TKMD.SoGiayPhep.Length > 0) chkGiayPhep.Checked = true;
           
            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.Text = this.TKMD.SoHoaDonThuongMai;
            if (this.TKMD.NgayHoaDonThuongMai.Year > 1900) ccNgayHDTM.Value = this.TKMD.NgayHoaDonThuongMai;
            if (this.TKMD.SoHoaDonThuongMai.Length > 0) chkHoaDonThuongMai.Checked = true;
           
            // Địa điểm dỡ hàng.
            ctrCuaKhau.Ma = this.TKMD.CuaKhau_ID;
            
            // Nguyên tệ.
            ctrNguyenTe.Ma = this.TKMD.NguyenTe_ID;
            txtTyGiaTinhThue.Value = this.TKMD.TyGiaTinhThue;
            txtTyGiaUSD.Value = this.TKMD.TyGiaUSD;
            
            // Hợp đồng.
            txtSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong.Year > 1900) ccNgayHopDong.Value = this.TKMD.NgayHopDong;
            if (this.TKMD.NgayHetHanHopDong.Year > 1900) ccNgayHHHopDong.Value = this.TKMD.NgayHetHanHopDong;
            if (this.TKMD.SoHopDong.Length > 0) chkHopDong.Checked = true;
            
            // Vận tải đơn.
            txtSoVanTaiDon.Text = this.TKMD.SoVanDon;
            if (this.TKMD.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Value = this.TKMD.NgayVanDon;
            if (this.TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;
            
            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
            if (this.TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;
            
            // Tên chủ hàng.
            txtTenChuHang.Text = this.TKMD.TenChuHang;
            
            // Container 20.
            txtSoContainer20.Value = this.TKMD.SoContainer20;
            
            // Container 40.
            txtSoContainer40.Value = this.TKMD.SoContainer40;
            
            // Số kiện hàng.
            txtSoKien.Value = this.TKMD.SoKien;
            
            // Trọng lượng.
            txtTrongLuong.Value = this.TKMD.TrongLuong;
            
            // Lệ phí HQ.
            txtLePhiHQ.Value = this.TKMD.LePhiHaiQuan;
            
            // Phí BH.
            txtPhiBaoHiem.Value = this.TKMD.PhiBaoHiem;
            txtPhiKhac.Value = this.TKMD.PhiKhac;
            txtTrongLuongTinh.Value = this.TKMD.TrongLuongNet;
            // Phí VC.
            txtPhiVanChuyen.Value = this.TKMD.PhiVanChuyen;
            //chung tu
            txtChungTuTK.Text = this.TKMD.ChungTu;
            if (this.TKMD.Xuat_NPL_SP == "S")
            {
                radSP.Checked = true;
                radNPL.Checked = false;
                radTB.Checked = false;
            }
            else if (this.TKMD.Xuat_NPL_SP == "N")
            {
                radSP.Checked = false;
                radNPL.Checked = true;
                radTB.Checked = false;
            }
            else if (this.TKMD.Xuat_NPL_SP == "T")
            {
                radSP.Checked = false;
                radNPL.Checked = false;
                radTB.Checked = true;
            }

            this.TKMD.LoadHMDCollection();
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();            
        }
        
        private void khoitao_DuLieuChuan()
        {
            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Nhom = this.NhomLoaiHinh;
            ctrLoaiHinhMauDich.Ma = this.NhomLoaiHinh + "01";       

            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll();
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;
            
            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll();
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            
            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll();
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;            
        }

        private void khoitao_GiaoDienToKhai()
        {
            this.NhomLoaiHinh = this.NhomLoaiHinh.Substring(0, 3);
            string loaiToKhai = this.NhomLoaiHinh.Substring(0, 1);            
            if (loaiToKhai == "N")
            {
                // Tiêu đề.
                
                this.Text =setText( "Tờ khai nhập khẩu (" + this.NhomLoaiHinh + ")","Import declaration(" + this.NhomLoaiHinh + ")");
            }
            else
            {
                // Tiêu đề.
                this.Text = setText("Tờ khai xuất khẩu (" + this.NhomLoaiHinh + ")", "Export declaration (" + this.NhomLoaiHinh + ")");
                this.uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(255,228,225);
                // Người XK / Người NK.
                grbNguoiNK.Text =setText( "Người xuất khẩu","Exporter");
                grbNguoiXK.Text = setText("Người nhập khẩu", "Importer");
                
                // Phương tiện vận tải.
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = false;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = false;
                
                // Vận tải đơn.
                chkVanTaiDon.Visible = false;
                grbVanTaiDon.Visible = false;
                
                // Nước XK.
                grbNuocXK.Text = setText("Nước nhập khẩu"," National import ");
                
                // Địa điểm xếp hàng / Địa điểm dỡ hàng.
                grbDiaDiemDoHang.Text = setText("Địa điểm xếp hàng", "Handling place");
                chkDiaDiemXepHang.Visible = grbDiaDiemXepHang.Visible = false;
            }
            
            // Loại hình gia công.
            if (this.NhomLoaiHinh == "NGC" || this.NhomLoaiHinh == "XGC")
            {
                chkHopDong.Checked = true;
                chkHopDong.Visible = false;
                txtSoHopDong.ReadOnly = ccNgayHopDong.ReadOnly = ccNgayHHHopDong.ReadOnly = true;
                txtSoHopDong.BackColor = ccNgayHopDong.BackColor = ccNgayHHHopDong.BackColor = Color.FromArgb(255, 255, 192);
                txtSoHopDong.ButtonStyle = EditButtonStyle.Ellipsis;
                if (this.NhomLoaiHinh == "NGC")
                {
                    radSP.Visible = false;
                    radSP.Checked = false;
                    radNPL.Checked = true;
                    radTB.Checked = false;

                }
            }
            else if (this.NhomLoaiHinh == "NSX" || this.NhomLoaiHinh == "XSX")
            {
                if (this.NhomLoaiHinh == "NSX")
                {
                    grpLoaiHangHoa.Visible = false;
                    radNPL.Checked = true;
                    radSP.Checked = false;
                    radTB.Checked = false;

                }
                else
                {
                    radTB.Visible = false;
                }
            }
            else
                grpLoaiHangHoa.Visible = false;
            
        }
                
        //-----------------------------------------------------------------------------------------
        
        private void ToKhaiNhapKhauSXXK_Form_Load(object sender, EventArgs e)
        {
            
            ccNgayDen.Value = DateTime.Today;
            TKMD.Load();
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDienToKhai();
            // Người nhập khẩu / Người xuất khẩu.
            txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
            txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;
            {
                this.loadTKMDData();
                this.ViewTKMD();
                this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);
            }
          
            //txtTenDonViDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
            {
                uiCommandBar1.Visible = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }  
        }

        private void ViewTKMD()
        {
            ctrDonViHaiQuan.ReadOnly = true;
            txtSoLuongPLTK.ReadOnly = true;

            // Doanh nghiệp.
            txtMaDonVi.ReadOnly = true;
            txtTenDonVi.ReadOnly = true;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.ReadOnly = true;

            // Đại lý TTHQ.
            txtMaDaiLy.ReadOnly = true;
            txtTenDaiLy.ReadOnly = true;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.ReadOnly = true ;

            // Phương tiện vận tải.
            cbPTVT.ReadOnly = true;
            txtSoHieuPTVT.ReadOnly = true;
            ccNgayDen.ReadOnly = true;

            // Nước.
            ctrNuocXuatKhau.ReadOnly = true;

            // ĐKGH.
            cbDKGH.ReadOnly = true;

            // PTTT.
            cbPTTT.ReadOnly = true;

            // Giấy phép.
            txtSoGiayPhep.ReadOnly = true;
            ccNgayGiayPhep.ReadOnly = true;
            ccNgayHHGiayPhep.ReadOnly = true;


            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.ReadOnly = true;
            ccNgayHDTM.ReadOnly = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.ReadOnly = true;

            // Nguyên tệ.
            ctrNguyenTe.ReadOnly = true;
            txtTyGiaTinhThue.ReadOnly = true;
            txtTyGiaUSD.ReadOnly = true;

            // Hợp đồng.
            txtSoHopDong.ReadOnly = true;
            ccNgayHopDong.ReadOnly = true;
            ccNgayHHHopDong.ReadOnly = true;

            // Vận tải đơn.
            txtSoVanTaiDon.ReadOnly = true;
            ccNgayVanTaiDon.ReadOnly = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.ReadOnly = true;

            // Tên chủ hàng.
            txtTenChuHang.ReadOnly = true;

            // Container 20.
            txtSoContainer20.ReadOnly = true;

            // Container 40.
            txtSoContainer40.ReadOnly = true;

            // Số kiện hàng.
            txtSoKien.ReadOnly = true;

            // Trọng lượng.
            txtTrongLuong.ReadOnly = true;

            // Lệ phí HQ.
            txtLePhiHQ.ReadOnly = true;

            // Phí BH.
            txtPhiBaoHiem.ReadOnly = true;

            // Phí VC.
            txtPhiVanChuyen.ReadOnly = true;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHoaDonThuongMai_CheckedChanged(object sender, EventArgs e)
        {
            grbHoaDonThuongMai.Enabled = chkHoaDonThuongMai.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkGiayPhep_CheckedChanged(object sender, EventArgs e)
        {
            gbGiayPhep.Enabled = chkGiayPhep.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHopDong_CheckedChanged(object sender, EventArgs e)
        {
            grbHopDong.Enabled = chkHopDong.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkVanTaiDon_CheckedChanged(object sender, EventArgs e)
        {
            grbVanTaiDon.Enabled = chkVanTaiDon.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkDiaDiemXepHang_CheckedChanged(object sender, EventArgs e)
        {
            grbDiaDiemXepHang.Enabled = chkDiaDiemXepHang.Checked;
        }
        //-----------------------------------------------------------------------------------------    

        private void tinhLaiThue()
        {
            this.TKMD.TongTriGiaKhaiBao = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                // Tính lại thuế.
                hmd.TinhThue(Convert.ToDecimal(txtTyGiaTinhThue.Value));
                // Tổng trị giá khai báo.
                this.TKMD.TongTriGiaKhaiBao +=Convert.ToDecimal(hmd.TriGiaKB);
            }
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();            
        }
        //-----------------------------------------------------------------------------------------
        
        private void inToKhai()
        {
            //ReportViewerForm f = new ReportViewerForm();
            //f.TKMD = this.TKMD;
            //f.ShowDialog(this);
        }
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch(e.Command.Key)
            {              
                case "cmdTinhLaiThue":
                    this.tinhLaiThue();
                    break;               
                case "cmdPrint": 
                    this.inToKhai();
                    break;  
                case "NhanDuLieu":
                    this.download();
                    break;

            }
        }
        private void download()
        {
            //try
            //{                           
            //            WSForm wsForm = new WSForm();
            //            wsForm.ShowDialog(this);
            //            if (!wsForm.IsReady) return;
            //            this.Cursor = Cursors.WaitCursor;
            //            TKMD.DongBoDuLieuHaiQuan();
            //            MLMessages("Nhận dữ liệu thành công! ","MSG_WRN34","", false);
            //            loadTKMDData();
            //            this.Cursor = Cursors.Default;
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    ShowMessage(ex.Message, false);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
        }
     
        //-----------------------------------------------------------------------------------------
        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
           
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangMauDichRegisterForm f = new HangMauDichRegisterForm();
                    f.HMD = (HangMauDich)i.GetRow().DataRow;                   
                    if (radNPL.Checked) this.TKMD.Xuat_NPL_SP = "N";
                    if (radSP.Checked) this.TKMD.Xuat_NPL_SP = "S";
                    if (radTB.Checked) this.TKMD.Xuat_NPL_SP = "T";
                    f.LoaiHangHoa = this.TKMD.Xuat_NPL_SP;
                    f.NhomLoaiHinh = this.NhomLoaiHinh;
                    f.MaHaiQuan = this.TKMD.MaHaiQuan;
                    f.MaNguyenTe = ctrNguyenTe.Ma;
                    f.TyGiaTT = Convert.ToDouble(txtTyGiaTinhThue.Value);
                    f.HMDCollection = this.TKMD.HMDCollection;
                    f.TKMD = TKMD;
                    f.create = false;
                    f.ShowDialog(this);
                    this.TKMD.HMDCollection=f.HMDCollection;
                    try
                    {
                        dgList.DataSource = TKMD.HMDCollection;
                        dgList.Refetch();
                    }
                    catch
                    {
                        dgList.Refresh();
                    }
                }
                break;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text.ToString());            
        }

      


       
        private void cmMain_CommandClick_1(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            if (e.Command.Key == "ThemHang")
            {
                HangMauDichRegisterForm f = new HangMauDichRegisterForm();
                f.HMD = new HangMauDich();               
                if (radNPL.Checked) this.TKMD.Xuat_NPL_SP = "N";
                if (radSP.Checked) this.TKMD.Xuat_NPL_SP = "S";
                if (radTB.Checked) this.TKMD.Xuat_NPL_SP = "T";
                f.LoaiHangHoa = this.TKMD.Xuat_NPL_SP;
                f.NhomLoaiHinh = this.NhomLoaiHinh;
                f.MaHaiQuan = this.TKMD.MaHaiQuan;
                f.MaNguyenTe = ctrNguyenTe.Ma;
                f.TyGiaTT = Convert.ToDouble(txtTyGiaTinhThue.Value);
                f.HMDCollection = this.TKMD.HMDCollection;
                f.TKMD = TKMD;
                f.create = true;
                f.ShowDialog(this);
                this.TKMD.HMDCollection = f.HMDCollection;
                try
                {
                    dgList.DataSource = TKMD.HMDCollection;
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
            else
            {
                int i = 0;
                try
                {
                    TKMD.InsertUpdateFullHang();
                    MLMessages("Cập nhật thành công.","MSG_SAV02","", false);
                }
                catch (Exception ex)
                {
                    ShowMessage("Có lỗi khi cập nhật.\n Lỗi : " + ex.Message, false);
                }
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MLMessages("Bạn có muốn xóa các thông tin đã chọn không?","MSG_CAN01","", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
                        try
                        {
                            hmd.Delete();
                        }
                        catch (Exception ex)
                        {
                            e.Cancel = true;
                            ShowMessage("Có lỗi khi xóa hàng :"+ex.Message, false);
                        }
                    }
                }
            }
            else
                e.Cancel = true;
        }
    }
}