using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_CityUNLOCODE
    {
        protected static List<VNACC_Category_CityUNLOCODE> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_CityUNLOCODE> collection = new List<VNACC_Category_CityUNLOCODE>();
            while (reader.Read())
            {
                VNACC_Category_CityUNLOCODE entity = new VNACC_Category_CityUNLOCODE();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CityCode"))) entity.CityCode = reader.GetString(reader.GetOrdinal("CityCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("CountryCode"))) entity.CountryCode = reader.GetString(reader.GetOrdinal("CountryCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("CityNameOrStateName"))) entity.CityNameOrStateName = reader.GetString(reader.GetOrdinal("CityNameOrStateName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        protected static List<VNACC_Category_CityUNLOCODE> ConvertToCollectionMinimize2(IDataReader reader)
        {
            List<VNACC_Category_CityUNLOCODE> collection = new List<VNACC_Category_CityUNLOCODE>();
            while (reader.Read())
            {
                VNACC_Category_CityUNLOCODE entity = new VNACC_Category_CityUNLOCODE();
                if (!reader.IsDBNull(reader.GetOrdinal("LOCODE"))) entity.LOCODE = reader.GetString(reader.GetOrdinal("LOCODE"));
                if (!reader.IsDBNull(reader.GetOrdinal("CityNameOrStateName"))) entity.CityNameOrStateName = reader.GetString(reader.GetOrdinal("CityNameOrStateName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_CityUNLOCODE> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [CityCode], [CityNameOrStateName], CountryCode, LOCODE FROM [dbo].[t_VNACC_Category_CityUNLOCODE]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_CityUNLOCODE> SelectForImporterExporter()
        {
            string spName = @"SELECT RIGHT(LOCODE,3) AS LOCODE, CityNameOrStateName
                                FROM [t_VNACC_Category_CityUNLOCODE]  
                                WHERE CountryCode = 'VN'";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize2(reader);
        }
      
        public static List<VNACC_Category_CityUNLOCODE> SelectCollectionAllMinimize2(string Where)
        {
            string spName = @"SELECT
                                          (case
                                          when  [LOCODE] = ''
                                          then CountryCode + CityCode
                                          else LOCODE
                                          end  ) as LOCODE
                                          ,[CityNameOrStateName]
                                      FROM [t_VNACC_Category_CityUNLOCODE]  ";
            if (!string.IsNullOrEmpty(Where)) spName += "Where " + Where;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize2(reader);
        }
        public static List<VNACC_Category_CityUNLOCODE> SelectCollectionBy(List<VNACC_Category_CityUNLOCODE> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_CityUNLOCODE o)
            {
                return true;// o.TableID.Contains(tableID);
            });
        }

        public static List<VNACC_Category_CityUNLOCODE> SelectCollectionByCountryCode(List<VNACC_Category_CityUNLOCODE> collections, string countryCode)
        {
            return collections.FindAll(delegate(VNACC_Category_CityUNLOCODE o)
            {
                return (o.LOCODE.StartsWith(countryCode));
            });
        }
        
    }
}