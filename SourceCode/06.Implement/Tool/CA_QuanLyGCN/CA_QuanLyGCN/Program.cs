﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CA_QuanLyGCN
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Global.LoadConnStr(Application.StartupPath);
                Global.LoadCauHinh();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                if (ex.Message.Contains("Microsoft.ACE.OLEDB.12.0"))
                {
                    if (MessageBox.Show("Thiết lập cơ sở dữ liệu", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        string command = Application.StartupPath + "\\DATA\\AccessDatabaseEngine.exe";
                        string argss = " /passive";

                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        process.StartInfo.FileName = command;
                        process.StartInfo.Arguments = argss;
                        process.Start();
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
            }
            

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Global.LoadConnStr(Application.StartupPath);
            Application.Run(new MainForm());
        }
    }
}
