using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace Company.GC.BLL.KDT
{
	public partial class HangMauDich : EntityBase
	{
		#region Private members.
		
		protected long _ID;
		protected long _TKMD_ID;
		protected int _SoThuTuHang;
		protected string _MaHS = String.Empty;
		protected string _MaPhu = String.Empty;
		protected string _TenHang = String.Empty;
		protected string _NuocXX_ID = String.Empty;
		protected string _DVT_ID = String.Empty;
		protected decimal _SoLuong;
		protected decimal _TrongLuong;
		protected decimal _DonGiaKB;
		protected decimal _DonGiaTT;
		protected decimal _TriGiaKB;
		protected decimal _TriGiaTT;
		protected decimal _TriGiaKB_VND;
		protected decimal _ThueSuatXNK;
		protected decimal _ThueSuatTTDB;
		protected decimal _ThueSuatGTGT;
		protected decimal _ThueXNK;
		protected decimal _ThueTTDB;
		protected decimal _ThueGTGT;
		protected decimal _PhuThu;
		protected decimal _TyLeThuKhac;
		protected decimal _TriGiaThuKhac;
		protected byte _MienThue;
        protected string _Ma_HTS = String.Empty;
        protected string _DVT_HTS = String.Empty;
        protected decimal _SoLuong_HTS;
        protected string _NhomHang = string.Empty;
        protected string _ThueSuatXNKGiam = string.Empty;
        protected string _ThueSuatTTDBGiam = string.Empty;
        protected string _ThueSuatVATGiam = string.Empty;
		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		public long TKMD_ID
		{
			set {this._TKMD_ID = value;}
			get {return this._TKMD_ID;}
		}
		public int SoThuTuHang
		{
			set {this._SoThuTuHang = value;}
			get {return this._SoThuTuHang;}
		}
		public string MaHS
		{
			set {this._MaHS = value;}
			get {return this._MaHS;}
		}
		public string MaPhu
		{
			set {this._MaPhu = value;}
			get {return this._MaPhu;}
		}
		public string TenHang
		{
			set {this._TenHang = value;}
			get {return this._TenHang;}
		}
		public string NuocXX_ID
		{
			set {this._NuocXX_ID = value;}
			get {return this._NuocXX_ID;}
		}
		public string DVT_ID
		{
			set {this._DVT_ID = value;}
			get {return this._DVT_ID;}
		}
		public decimal SoLuong
		{
			set {this._SoLuong = value;}
			get {return this._SoLuong;}
		}
		public decimal TrongLuong
		{
			set {this._TrongLuong = value;}
			get {return this._TrongLuong;}
		}
		public decimal DonGiaKB
		{
			set {this._DonGiaKB = value;}
			get {return this._DonGiaKB;}
		}
		public decimal DonGiaTT
		{
			set {this._DonGiaTT = value;}
			get {return this._DonGiaTT;}
		}
		public decimal TriGiaKB
		{
			set {this._TriGiaKB = value;}
			get {return this._TriGiaKB;}
		}
		public decimal TriGiaTT
		{
			set {this._TriGiaTT = value;}
			get {return this._TriGiaTT;}
		}
		public decimal TriGiaKB_VND
		{
			set {this._TriGiaKB_VND = value;}
			get {return this._TriGiaKB_VND;}
		}
		public decimal ThueSuatXNK
		{
			set {this._ThueSuatXNK = value;}
			get {return this._ThueSuatXNK;}
		}
		public decimal ThueSuatTTDB
		{
			set {this._ThueSuatTTDB = value;}
			get {return this._ThueSuatTTDB;}
		}
		public decimal ThueSuatGTGT
		{
			set {this._ThueSuatGTGT = value;}
			get {return this._ThueSuatGTGT;}
		}
		public decimal ThueXNK
		{
			set {this._ThueXNK = value;}
			get {return this._ThueXNK;}
		}
		public decimal ThueTTDB
		{
			set {this._ThueTTDB = value;}
			get {return this._ThueTTDB;}
		}
		public decimal ThueGTGT
		{
			set {this._ThueGTGT = value;}
			get {return this._ThueGTGT;}
		}
		public decimal PhuThu
		{
			set {this._PhuThu = value;}
			get {return this._PhuThu;}
		}
		public decimal TyLeThuKhac
		{
			set {this._TyLeThuKhac = value;}
			get {return this._TyLeThuKhac;}
		}
		public decimal TriGiaThuKhac
		{
			set {this._TriGiaThuKhac = value;}
			get {return this._TriGiaThuKhac;}
		}
		public byte MienThue
		{
			set {this._MienThue = value;}
			get {return this._MienThue;}
		}

        public string Ma_HTS
        {
            set { this._Ma_HTS = value; }
            get { return this._Ma_HTS; }
        }
        public string DVT_HTS
        {
            set { this._DVT_HTS = value; }
            get { return this._DVT_HTS; }
        }
        public decimal SoLuong_HTS
        {
            set { this._SoLuong_HTS = value; }
            get { return this._SoLuong_HTS; }
        }
        public string ThueSuatXNKGiam
        {
            set { this._ThueSuatXNKGiam = value; }
            get { return this._ThueSuatXNKGiam; }
        }
        public string ThueSuatTTDBGiam
        {
            set { this._ThueSuatTTDBGiam = value; }
            get { return this._ThueSuatTTDBGiam; }
        }
        public string ThueSuatVATGiam
        {
            set { this._ThueSuatVATGiam = value; }
            get { return this._ThueSuatVATGiam; }
        }
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
        public string NhomHang
        {
            set { this._NhomHang = value; }
            get { return this._NhomHang; }
        }
	    #endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_KDT_HangMauDich_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) this._TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) this._SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) this._MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) this._DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) this._ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) this._ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) this._ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this._ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this._ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) this._ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this._PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) this._TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) this._TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) this._MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) this._Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) this._DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) this._SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) this.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) this.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) this.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) this._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) this._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) this._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                }
                catch { }
                reader.Close();
				return true;
			}
            reader.Close();
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public HangMauDichCollection SelectCollectionBy_TKMD_ID()
		{
			string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
			
			HangMauDichCollection collection = new HangMauDichCollection();
            SqlDataReader reader = (SqlDataReader) this.db.ExecuteReader(dbCommand);
			while (reader.Read())
			{

				HangMauDich entity = new HangMauDich();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity._Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity._DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity._SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                }
                catch { }
                collection.Add(entity);
			}
            reader.Close();
			return collection;
		}

		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_TKMD_ID()
		{
			string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
						
            return this.db.ExecuteDataSet(dbCommand);
		}
        public DataSet SelectBy_TKMD_ID(long TKMD_ID)
        {
            string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);

            return this.db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_KDT_HangMauDich_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_HangMauDich_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_HangMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_HangMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public HangMauDichCollection SelectCollectionAll()
		{
			HangMauDichCollection collection = new HangMauDichCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				HangMauDich entity = new HangMauDich();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                }
                catch { }
                collection.Add(entity);
			}
            reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public HangMauDichCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			HangMauDichCollection collection = new HangMauDichCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				HangMauDich entity = new HangMauDich();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                }
                catch { }
                collection.Add(entity);
			}
            reader.Close();
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_HangMauDich_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
			this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, this._SoThuTuHang);
			this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
			this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
			this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
			this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
			this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
			this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
			this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
			this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Money, this._DonGiaTT);
			this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Money, this._TriGiaKB);
			this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Money, this._TriGiaTT);
			this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Money, this._TriGiaKB_VND);
			this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
			this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
			this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
			this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
			this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
			this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
			this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
			this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
			this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
			this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);
            this.db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, this._Ma_HTS);
            this.db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, this._DVT_HTS);
            this.db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, this._SoLuong_HTS);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, this._ThueSuatXNKGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, this._ThueSuatTTDBGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, this._ThueSuatVATGiam);
			if (transaction != null)
			{
				this.db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				this.db.ExecuteNonQuery(dbCommand);
				this._ID = (long) this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(HangMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (HangMauDich item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, HangMauDichCollection collection)
        {
            foreach (HangMauDich item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_HangMauDich_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
			this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, this._SoThuTuHang);
			this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
			this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
			this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
			this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
			this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
			this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
			this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
			this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Money, this._DonGiaTT);
			this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Money, this._TriGiaKB);
			this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Money, this._TriGiaTT);
			this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Money, this._TriGiaKB_VND);
			this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
			this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
			this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
			this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
			this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
			this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
			this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
			this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
			this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
			this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);
            this.db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, this._Ma_HTS);
            this.db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, this._DVT_HTS);
            this.db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, this._SoLuong_HTS);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, this._ThueSuatXNKGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, this._ThueSuatTTDBGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, this._ThueSuatVATGiam);
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(HangMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (HangMauDich item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_HangMauDich_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
			this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, this._SoThuTuHang);
			this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
			this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
			this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
			this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
			this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
			this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
			this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
			this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Money, this._DonGiaTT);
			this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Money, this._TriGiaKB);
			this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Money, this._TriGiaTT);
			this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Money, this._TriGiaKB_VND);
			this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
			this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
			this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
			this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
			this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
			this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
			this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
			this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
			this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
			this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);
            this.db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, this._Ma_HTS);
            this.db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, this._DVT_HTS);
            this.db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, this._SoLuong_HTS);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, this._ThueSuatXNKGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, this._ThueSuatTTDBGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, this._ThueSuatVATGiam);
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(HangMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (HangMauDich item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_HangMauDich_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(HangMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (HangMauDich item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(HangMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (HangMauDich item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion
	}	
}