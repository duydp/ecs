/*
Run this script on:

192.168.72.100.ECS_TQDT_GC_V4_DONGKINH    -  This database will be modified

to synchronize it with:

192.168.72.100.ECS_TQDT_GC_V4

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.1.0 from Red Gate Software Ltd at 02/26/2013 10:52:03 AM

*/
		
SET XACT_ABORT ON
GO
SET ARITHABORT ON
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

DELETE from [dbo].[t_HaiQuan_BieuThue]

-- Add rows to [dbo].[t_HaiQuan_BieuThue]
SET IDENTITY_INSERT [dbo].[t_HaiQuan_BieuThue] ON
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (11, N'', N'', N'')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (12, N'B01', N'B01 - Biểu thuế nhập khẩu ưu đãi', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (13, N'B02', N'B02 - Biểu thuế nhập khẩu ưu đãi - Chương 98', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (14, N'B03', N'B03 - Biểu thuế nhập khẩu thông thường (bằng 150% thuế suất MFN)', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (15, N'B04', N'B04 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại hàng hóa ASEAN (ATIGA)', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (16, N'B05', N'B05 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Thương mại Tự do ASEAN - Trung Quốc', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (17, N'B06', N'B06 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Thương mại Tự do ASEAN - Hàn Quốc', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (18, N'B07', N'B07 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Thương mại Tự dơ ASEAN - úc - NiuDi -lân', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (19, N'B08', N'B08 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại hàng hóa ASEAN-ấn Độ', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (20, N'B09', N'B09 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định đối tác kinh tế toàn diện ASEAN-Nhật Bản', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (21, N'B10', N'B10 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định đối tác kinh tế Việt Nam-Nhật Bản', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (22, N'B11', N'B11 - Biểu thuế nhập khẩu đối với các mặt hàng được áp dụng ưu đãi thuế suất thuế nhập khẩu Việt - Lào', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (23, N'B12', N'B12 - Biểu thuế nhập khẩu ưu đãi đặc biệt đối với hàng hóa nhập khẩu có xuất xứ từ Vương Quốc Campuchia', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (24, N'B13', N'B13 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định thương mại Tự do Việt Nam - Chi Lê', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (25, N'B14', N'B14 - Biểu thuế NK ngoài hạn ngạch', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (26, N'B15', N'B15 - Biểu thuế nhập khẩu tuyệt đối', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (27, N'B16', N'B16 - Biểu thuế nhập khẩu hỗn hợp', N'NTD')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (28, N'B21', N'B21 - Biểu thuế VAT 0%', N'VAT')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (29, N'B22', N'B22 - Biểu thuế VAT 5%', N'VAT')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (30, N'B23', N'B23 - Biểu thuế VAT 10%', N'VAT')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (31, N'B24', N'B24 - Biểu thuế VAT 15%', N'VAT')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (32, N'B31', N'B31 - Biểu thuế xuất khẩu (B31)', N'X')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (33, N'B32', N'B32 - Biểu thuế xuất khẩu (B32)', N'X')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (34, N'B33', N'B33 - Biểu thuế xuất khẩu (B33)', N'X')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (35, N'B34', N'B34 - Biểu thuế xuất khẩu (B34)', N'X')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (36, N'B41', N'B41 - Biểu thuế TTĐB', N'TTDB')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (37, N'B51', N'B51 - Biểu thuế môi trường', N'MT')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (38, N'B61', N'B61 - Biểu thuế dành cho loại hình gia công, chế xuất với thuế suất 0%', N'KHAC')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (39, N'B99', N'B99 - Biểu thuế khác, dành cho hàng miễn thuế', N'KHAC')
SET IDENTITY_INSERT [dbo].[t_HaiQuan_BieuThue] OFF

--USE [ECS_TQDT_GC_V4]
--GO

/****** Object:  StoredProcedure [dbo].[p_GC_CapNhat_LoaiHangHoa_ToKhai]    Script Date: 02/26/2013 13:42:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_CapNhat_LoaiHangHoa_ToKhai]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_CapNhat_LoaiHangHoa_ToKhai]
GO

--USE [ECS_TQDT_GC_V4]
--GO

/****** Object:  StoredProcedure [dbo].[p_GC_CapNhat_LoaiHangHoa_ToKhai]    Script Date: 02/26/2013 13:42:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[p_GC_CapNhat_LoaiHangHoa_ToKhai]
as

begin

UPDATE dbo.t_KDT_GC_ToKhaiChuyenTiep
SET LoaiHangHoa = CASE 
		WHEN SUBSTRING(RTRIM(MaLoaiHinh), 1, 4) = 'PHPL' THEN 'N'
		WHEN RTRIM(MaLoaiHinh) = 'PHSPX' THEN 'S'
		WHEN RTRIM(MaLoaiHinh) = 'XGC19' THEN 'S'
		WHEN SUBSTRING(RTRIM(MaLoaiHinh), 1, 4) = 'PHTB' THEN 'T'
		when SUBSTRING(RTRIM(MaLoaiHinh), 4, 2) = '20' THEN 'T'		
		when SUBSTRING(RTRIM(MaLoaiHinh), 4, 2) = '18' THEN 'N'		
		ELSE 'N'
	END
where loaihanghoa is null
	
end
GO

-- Operation applied to 29 rows out of 29
COMMIT TRANSACTION
GO
       
UPDATE [dbo].[t_HaiQuan_Version]
SET [Version] = '7.7'
  ,[Date] = getdate()
  ,[Notes] = ''