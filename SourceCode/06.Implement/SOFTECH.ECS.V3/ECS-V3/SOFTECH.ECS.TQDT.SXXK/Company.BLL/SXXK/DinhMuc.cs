using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.BLL.SXXK
{
    public partial class DinhMuc
    {
        public void Saved(DinhMucCollection collection)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (DinhMuc item in collection)
                    {
                        InsertUpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public DataSet getSanPhamDinhMuc(string maHaiQuan)
        {
            string sql = "SELECT Ma,SoDinhMuc ,Ten   ,MaHS,DVT_ID " +
                        "FROM t_SXXK_ThongTinDinhMuc " +
                        "inner join t_SXXK_SanPham on t_SXXK_SanPham.Ma=t_SXXK_ThongTinDinhMuc.MaSanPham ORDER BY MaSanPham";
            DbCommand dbc = db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            db.LoadDataSet(dbc, ds, "SanPham");

            sql = "SELECT a.MaSanPham , a.MaNguyenPhuLieu,b.Ten, b.maHS ,b.DVT_ID ,a.DinhMucSuDung,a.TyLeHaoHut FROM  t_SXXK_DinhMuc a " +
              "INNER JOIN t_SXXK_NguyenPhuLieu b ON a.MaNguyenPhuLieu = b.Ma and a.MaHaiQuan=b.MaHaiQuan and a.MaDoanhNghiep=b.MaDoanhNghiep " +
              " WHERE a.maHaiQuan='" + maHaiQuan + "' ORDER BY a.MaSanPham";
            DbCommand dbc1 = db.GetSqlStringCommand(sql);
            db.LoadDataSet(dbc1, ds, "tblNguyenPhuLieu");
            DataRelation rel = new DataRelation("rel",
                            ds.Tables[0].Columns[0],
                            ds.Tables[1].Columns[0], false);
            ds.Relations.Add(rel);
            return ds;
        }
        public static DinhMucCollection GetDinhMucXML(string maHaiQuan, string maDoanhNghiep)
        {
            string sql = "SELECT a.MaHaiQuan,a.MaDoanhNghiep, a.MaSanPham , a.MaNguyenPhuLieu,b.Ten, b.maHS ,b.DVT_ID ,a.DinhMucSuDung,a.TyLeHaoHut,a.DinhMucChung FROM  t_SXXK_DinhMuc a " +
              " INNER JOIN t_SXXK_NguyenPhuLieu b ON a.MaNguyenPhuLieu = b.Ma AND a.MaDoanhNghiep = b.MaDoanhNghiep AND a.MaHaiQuan = b.MaHaiQuan" +
              " WHERE a.maHaiQuan='" + maHaiQuan + "' and a.madoanhnghiep='" + maDoanhNghiep + "' ORDER BY a.MaSanPham";
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbc1 = db.GetSqlStringCommand(sql);
            DinhMucCollection collection = new DinhMucCollection();

            IDataReader reader = db.ExecuteReader(dbc1);
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPHam = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                entity.TenNPL = reader.GetString(reader.GetOrdinal("Ten"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public DinhMucCollection getSanPhamDinhMucCollection(string maHaiQuan, string maDoanhNghiep, string maSanPham)
        {
            string sql = "SELECT a.MaHaiQuan,a.MaDoanhNghiep, a.MaSanPham , a.MaNguyenPhuLieu,b.Ten, b.maHS ,b.DVT_ID ,a.DinhMucSuDung,a.TyLeHaoHut,a.DinhMucChung FROM  t_SXXK_DinhMuc a " +
              "LEFT JOIN t_SXXK_NguyenPhuLieu b ON a.MaNguyenPhuLieu = b.Ma and a.MaHaiQuan=b.MaHaiQuan and a.MaDoanhNghiep=b.MaDoanhNghiep " +
              " WHERE a.maHaiQuan='" + maHaiQuan + "' and a.madoanhnghiep='" + maDoanhNghiep + "' AND a.MaSanPham LIKE '%" + maSanPham + "%' ORDER BY a.MaSanPham";
            DbCommand dbc1 = db.GetSqlStringCommand(sql);
            DinhMucCollection collection = new DinhMucCollection();

            IDataReader reader = db.ExecuteReader(dbc1);
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPHam = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("Ten"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public DinhMucCollection GetDMFromUserName(string userName)
        {
            string query = "SELECT dm.MaHaiQuan, dm.MaDoanhNghiep, dm.MaSanPham, dm.MaNguyenPhuLieu, npl.Ten, dm.DinhMucSuDung, dm.TyLeHaoHut, dm.DinhMucChung " +
                         "FROM dbo.t_SXXK_DinhMuc dm LEFT JOIN dbo.t_SXXK_NguyenPhuLieu npl ON dm.MaNguyenPhuLieu = npl.Ma WHERE MaSanPham IN " +
                         "(SELECT MaSanPham FROM dbo.t_KDT_SXXK_DinhMuc WHERE Master_ID IN (SELECT id_dk FROM dbo.t_KDT_SXXK_LogKhaiBao " +
                         "WHERE LoaiKhaiBao = 'DM'  AND UserNameKhaiBao = '" + userName + "')) ORDER BY dm.MaSanPham";
            DbCommand dbc1 = db.GetSqlStringCommand(query);
            DinhMucCollection collection = new DinhMucCollection();

            IDataReader reader = db.ExecuteReader(dbc1);
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPHam = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("Ten"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public DinhMucCollection getSanPhamDinhMucLoiCollection(string maHaiQuan, string maDoanhNghiep, string maSanPham)
        {
            string sql = "SELECT a.MaHaiQuan,a.MaDoanhNghiep, a.MaSanPham , a.MaNguyenPhuLieu,b.Ten, b.maHS ,b.DVT_ID ,a.DinhMucSuDung,a.TyLeHaoHut,a.DinhMucChung FROM  t_SXXK_DinhMuc a " +
              "LEFT JOIN t_SXXK_NguyenPhuLieu b ON a.MaNguyenPhuLieu = b.Ma and a.MaHaiQuan=b.MaHaiQuan and a.MaDoanhNghiep=b.MaDoanhNghiep" +
              " WHERE a.maHaiQuan='" + maHaiQuan + "' and a.madoanhnghiep='" + maDoanhNghiep + "' AND a.MaSanPham LIKE '%" + maSanPham + "%' and  Ten is null ORDER BY a.MaSanPham ";
            DbCommand dbc1 = db.GetSqlStringCommand(sql);
            DinhMucCollection collection = new DinhMucCollection();

            IDataReader reader = db.ExecuteReader(dbc1);
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPHam = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("Ten"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public DataSet getSanPhamChuacoDinhMuc(string maHaiQuan, string maDoanhNghiep)
        {
            string sql = "SELECT * from t_SXXK_SanPham " +
                        " where ma not in (select masanpham from t_SXXK_ThongTinDinhMuc) and MaHaiQuan like'%" + maHaiQuan.Trim() + "%' AND MaDoanhNghiep like '%" + maDoanhNghiep.Trim() + "%'";
            DbCommand dbc = db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            db.LoadDataSet(dbc, ds, "SanPham");
            return ds;
        }

        public static DataSet getDinhMucOfSanPham(string masp, string mahaiquan, string madv)
        {
            DataSet ds = new DataSet();
            try
            {
                string pSql = "p_SXXK_DinhMucOfSanPhamDK";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(pSql);
                db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, masp);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahaiquan);

                ds = db.ExecuteDataSet(dbCommand);
                dbCommand.Connection.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return ds;
        }

        public static DataSet getDinhMucOfSanPham(string masp, string mahaiquan, string madv, decimal SoLuongSP, int SoThapPhanNPL, SqlTransaction transaction)
        {
            string sql = "SELECT MaNguyenPhuLieu,Round((DinhMucChung * @SoLuongSP),@SoThapPhanNPL) as SoLuong,npl.Ten as TenNPL,DinhMucChung " +
                        "FROM [dbo].[t_SXXK_DinhMuc] dm inner join t_SXXK_NguyenPhuLieu npl " +
                        "on dm.MaNguyenPhuLieu=npl.Ma and dm.MaHaiQuan=npl.MaHaiQuan and dm.MaDoanhNghiep=npl.MaDoanhNghiep " +
                        "inner join t_HaiQuan_DonViTinh dvt on dvt.id=npl.DVT_ID " +
                         "WHERE [MaSanPham]=@MaSanPham and npl.MaDoanhNghiep=@MaDoanhNghiep and npl.MaHaiQuan=@MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, masp);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahaiquan);
            db.AddInParameter(dbCommand, "@SoLuongSP", SqlDbType.Decimal, SoLuongSP);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand, transaction);
            return ds;
        }

        public static DataSet getDinhMucOfSanPham(string masp, string mahaiquan, string madv, decimal SoLuongSP, int SoThapPhanNPL)
        {
            string sql = "SELECT MaNguyenPhuLieu,Round((DinhMucChung * @SoLuongSP),@SoThapPhanNPL) as SoLuong,npl.Ten as TenNPL,DinhMucChung " +
                        "FROM [dbo].[t_SXXK_DinhMuc] dm inner join t_SXXK_NguyenPhuLieu npl " +
                        "on dm.MaNguyenPhuLieu=npl.Ma and dm.MaHaiQuan=npl.MaHaiQuan and dm.MaDoanhNghiep=npl.MaDoanhNghiep " +
                        "inner join t_HaiQuan_DonViTinh dvt on dvt.id=npl.DVT_ID " +
                         "WHERE [MaSanPham]=@MaSanPham and npl.MaDoanhNghiep=@MaDoanhNghiep and npl.MaHaiQuan=@MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, masp);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahaiquan);
            db.AddInParameter(dbCommand, "@SoLuongSP", SqlDbType.Float, SoLuongSP);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);

            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            dbCommand.Connection.Close();
            return ds;
        }
        //ThoiLV 
        public bool InsertUpdateGrid(DinhMucCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DinhMuc item in collection)
                    {
                        ThongTinDinhMuc ttdm = new ThongTinDinhMuc();
                        ttdm.MaDoanhNghiep = item.MaDoanhNghiep;
                        ttdm.MaHaiQuan = item.MaHaiQuan;
                        ttdm.SoDinhMuc = 0;
                        ttdm.NgayDangKy = DateTime.Today;
                        ttdm.MaSanPham = item.MaSanPHam.Trim();
                        ttdm.InsertUpdateTransaction(transaction);
                        item.DinhMucChung = item.DinhMucSuDung + item.DinhMucSuDung * item.TyLeHaoHut / 100;
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public static bool checkIsExist(string maHQ, string maDN, string maSP)
        {
            string sql = "select * from t_SXXK_DinhMuc where  MaHaiQuan=@MaHaiQuan AND MaDoanhNghiep =@MaDoanhNghiep and MaSanPham=@MaSanPham";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDN);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHQ);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            return true;
        }
        public static bool checkIsExist(string maHQ, string maDN, string maSP, SqlTransaction tran)
        {
            string sql = "select * from t_SXXK_DinhMuc where  MaHaiQuan=@MaHaiQuan AND MaDoanhNghiep =@MaDoanhNghiep and MaSanPham=@MaSanPham";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDN);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHQ);
            object o = db.ExecuteScalar(dbCommand, tran);
            if (o == null)
                return false;
            return true;
        }


        public static void NhapDMXML(DinhMucCollection DinhMucCollection, string maHQ, string maDN)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (DinhMuc dm in DinhMucCollection)
                    {
                        ThongTinDinhMuc ttdm = new ThongTinDinhMuc();
                        ttdm.IsExist = dm.IsExist;
                        ttdm.MaHaiQuan = dm.MaHaiQuan;
                        ttdm.MaDoanhNghiep = dm.MaDoanhNghiep;
                        ttdm.MaSanPham = dm.MaSanPHam;
                        ttdm.NgayDangKy = DateTime.Now;
                        ttdm.NgayApDung = DateTime.Now;
                        ttdm.NgayHetHan = DateTime.Now;
                        ttdm.SoDinhMuc = 1;
                        ttdm.ThanhLy = 0;
                        if (!checkIsExist(ttdm.MaHaiQuan.Trim().ToString(), ttdm.MaDoanhNghiep.Trim().ToString(), ttdm.MaSanPham.Trim().ToString(), transaction))
                            try
                            {
                                ttdm.InsertTransaction(transaction);
                            }
                            catch { }

                        // ttdm.NgayDangKy = dm.
                        DinhMuc npltemp = new DinhMuc();
                        npltemp.MaHaiQuan = maHQ;
                        npltemp.MaDoanhNghiep = maDN;
                        npltemp.MaNguyenPhuLieu = dm.MaNguyenPhuLieu;
                        npltemp.MaSanPHam = dm.MaSanPHam;
                        npltemp.TenNPL = dm.TenNPL;
                        npltemp.DinhMucChung = dm.DinhMucChung;
                        npltemp.DinhMucSuDung = dm.DinhMucSuDung;
                        npltemp.GhiChu = dm.GhiChu;
                        npltemp.IsExist = dm.IsExist;
                        npltemp.TyLeHaoHut = dm.TyLeHaoHut;

                        try { npltemp.InsertTransaction(transaction); }
                        catch { }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        //------------
        public bool UpdateRegistedToDatabase(string maHaiQuan, string maDoanhNghiep, DataSet ds, DataSet dsDinhMuc)
        {
            DinhMucCollection collection = new DinhMucCollection();
            ThongTinDinhMuc ttDinhMuc = new ThongTinDinhMuc();
            DataSet dsThongTinDMuc = ttDinhMuc.SelectDynamic("madoanhnghiep='" + maDoanhNghiep + "' and mahaiquan='" + maHaiQuan + "'", "");
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ThongTinDinhMuc ttdm = new ThongTinDinhMuc();
                        ttdm.MaHaiQuan = maHaiQuan;
                        ttdm.MaDoanhNghiep = maDoanhNghiep;
                        ttdm.MaSanPham = row["Ma_SP"].ToString().Trim();
                        ttdm.SoDinhMuc = Convert.ToInt32(row["SO_DM"].ToString());
                        ttdm.NgayDangKy = Convert.ToDateTime(row["NGAY_DK"].ToString());

                        try
                        {
                            ttdm.NgayApDung = Convert.ToDateTime(row["NGAY_AD"].ToString());
                        }
                        catch
                        {
                            ttdm.NgayApDung = new DateTime(1900, 1, 1);
                        }
                        try
                        {
                            ttdm.NgayHetHan = Convert.ToDateTime(row["NGAY_HH"].ToString());
                        }
                        catch
                        {
                            ttdm.NgayHetHan = new DateTime(1900, 1, 1);
                        }
                        DataRow[] rowTTDM = dsThongTinDMuc.Tables[0].Select("MaSanPham='" + ttdm.MaSanPham + "' and MaDoanhNghiep='" + ttdm.MaDoanhNghiep + "' and MaHaiQuan='" + ttdm.MaHaiQuan + "'");
                        if (rowTTDM == null || rowTTDM.Length == 0)
                            ttdm.InsertTransaction(transaction);
                    }
                    foreach (DataRow row in dsDinhMuc.Tables[0].Rows)
                    {
                        DinhMuc dm = new DinhMuc();
                        dm.MaHaiQuan = maHaiQuan;
                        dm.MaDoanhNghiep = maDoanhNghiep;
                        dm.MaSanPHam = row["Ma_SP"].ToString().Trim();
                        dm.MaNguyenPhuLieu = row["Ma_NPL"].ToString().Trim();
                        dm.DinhMucSuDung = Convert.ToDecimal(row["DM_SD"].ToString());
                        dm.DinhMucChung = Convert.ToDecimal(row["DM_Chung"].ToString());
                        dm.TyLeHaoHut = Convert.ToDecimal(row["TL_HH"].ToString());
                        dm.GhiChu = row["GHI_CHU"].ToString();
                        DataRow[] rowTTDM = dsThongTinDMuc.Tables[0].Select("MaSanPham='" + dm.MaSanPHam + "' and MaDoanhNghiep='" + dm.MaDoanhNghiep + "' and MaHaiQuan='" + dm.MaHaiQuan + "'");
                        if (rowTTDM == null || rowTTDM.Length == 0)
                            dm.InsertTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }


        //public static void DongBoDuLieuHaiQuan(string maHaiQuan, string maDoanhNghiep, string[] masp)
        //{
        //    SXXKService service = new SXXKService();
        //    DataSet ds = service.DinhMuc_GetDanhSachOfSanPham(maHaiQuan, maDoanhNghiep, masp);
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    string ma = "";
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        try
        //        {

        //            foreach (DataRow row in ds.Tables[0].Rows)
        //            {
        //                DinhMuc dm = new DinhMuc();
        //                dm.MaHaiQuan = maHaiQuan;
        //                dm.MaDoanhNghiep = maDoanhNghiep;
        //                dm.MaSanPHam = row["Ma_SP"].ToString().Trim();
        //                if (ma.Trim() != dm.MaSanPHam.Trim())
        //                {
        //                    ma = dm.MaSanPHam;
        //                    dm.DeleteByForeignKeyTransaction(transaction);
        //                }
        //                dm.MaNguyenPhuLieu = row["Ma_NPL"].ToString().Trim();
        //                dm.DinhMucSuDung = Convert.ToDecimal(row["DM_SD"].ToString());
        //                dm.DinhMucChung = Convert.ToDecimal(row["DM_Chung"].ToString());
        //                dm.GhiChu = row["GHI_CHU"].ToString();
        //                dm.InsertUpdateTransaction(transaction);
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}
        public static DataSet GetDanhSachDinhMucSaiDuLieu(string maHQ, string maDN, string masp)
        {
            string sql = "select * from t_View_KiemTraDuLieuDinhMuc where  MaHaiQuan=@MaHaiQuan AND MaDoanhNghiep =@MaDoanhNghiep ";
            if (masp != "")
            {
                sql += " and MaSanPham=@MaSanPham";
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            if (masp != "")
                db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, masp);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDN);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHQ);
            return db.ExecuteDataSet(dbCommand);
        }

        public static DinhMuc getDinhMuc(string maHaiQuan, string maDoanhNghiep, string maSP, string maNPL)
        {
            string sql = "SELECT * from t_SXXK_DinhMuc " +
                        " where  MaHaiQuan='" + maHaiQuan + "' AND MaDoanhNghiep = '" + maDoanhNghiep + "' AND MaSanPham = '" + maSP + "'" +
                        " AND MaNguyenPhuLieu ='" + maNPL + "'";

            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbc = db.GetSqlStringCommand(sql);


            IDataReader reader = db.ExecuteReader(dbc);
            DinhMuc entity = null;
            while (reader.Read())
            {
                entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPHam = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
            }
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_DinhMuc_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this.MaSanPHam);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this.MaNguyenPhuLieu);
            db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this.DinhMucSuDung);
            db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this.TyLeHaoHut);
            db.AddInParameter(dbCommand, "@DinhMucChung", SqlDbType.Decimal, this.DinhMucChung);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, this.GhiChu);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransactionKTX(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_DinhMuc_InsertUpdate_KTX";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this.MaSanPHam);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this.MaNguyenPhuLieu);
            db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this.DinhMucSuDung);
            db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this.TyLeHaoHut);
            db.AddInParameter(dbCommand, "@DinhMucChung", SqlDbType.Decimal, this.DinhMucChung);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, this.GhiChu);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public bool InsertUpdate(DinhMucCollection collection, SqlTransaction transaction)
        {
            bool ret = true;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();

                try
                {
                    //bool ret01 = true;
                    foreach (DinhMuc item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            //ret01 = false;
                            break;
                        }
                    }
                    //if (ret01)
                    //{
                    //    transaction.Commit();
                    //    ret = true;
                    //}
                    //else
                    //{
                    //    transaction.Rollback();
                    //    ret = false;
                    //}
                }
                catch(Exception ex)
                {
                    ret = false;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw;
                    //transaction.Rollback();
                }
                //finally
                //{
                //    connection.Close();

                //    Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("Đã lưu {0} định mức", collection.Count)));
                //}
            }
            return ret;
        }
    }
}
