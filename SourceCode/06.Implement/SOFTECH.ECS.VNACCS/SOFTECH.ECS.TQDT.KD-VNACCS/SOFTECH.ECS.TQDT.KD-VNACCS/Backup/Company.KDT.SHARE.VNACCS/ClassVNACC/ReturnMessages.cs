﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Company.KDT.SHARE.VNACCS.ClassVNACC
{
    public class ReturnMessages
    {
        public VNACCHeader header { get; set; }
        public StringBuilder Body { get; set; }
        public ReturnMessages(string str)
        {
            int index = 0;
            string[] StringLines = str.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            for (int i = 0; i < StringLines.Length; i++)
            {
                if (StringLines[i].Length == 398 || ASCIIEncoding.ASCII.GetBytes(StringLines[i]).Length == 400)
                {
                    this.header = new VNACCHeader(StringLines[i]);
                    index = i; // 398 byte đầu tiên
                    break;
                }
            }
            index++;
            if (this.header != null)
            {
                int BodyByte = 0;
                long LengMessages = Convert.ToInt64(this.header.MessagesLength.Value) - 400;
                if (this.Body == null) this.Body = new StringBuilder();
                while (BodyByte < Convert.ToInt64(this.header.MessagesLength.Value) && index < StringLines.Length)
                {
                    if (StringLines[index].StartsWith("------"))
                    {
                        break;
                    }
                    this.Body.Append(StringLines[index]).AppendLine();
                    BodyByte = BodyByte +  UTF8Encoding.UTF8.GetBytes(StringLines[index]).Length;
                    index++;
                }
            }
            else
            {
                string[] lines = str.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                //ReturnMessages msgReturn = new ReturnMessages(lines);
                int index1 = 0;
                int i1 = 0;
                int j = 0;
                if (lines != null && lines.Length > 0)
                {

                    for (int i = 0; i < lines.Length; i++)
                    {
                        if (lines[i].Length == 398 || ASCIIEncoding.ASCII.GetBytes(lines[i]).Length == 400)
                        {
                            this.header = new VNACCHeader(lines[i]);
                            index1 = i; // 398 byte đầu tiên
                            break;
                        }
                    }
                }
                try
                {

                    if (this.header != null && this.header.MessagesLength != null && !string.IsNullOrEmpty(this.header.MessagesLength.Value.ToString().Trim()))
                    {
                        index1++;
                        int BodyByte = 0;
                        j = 1;
                        long LengMessages = Convert.ToInt64(this.header.MessagesLength.Value) - 400;
                        j = 2;
                        if (this.Body == null) this.Body = new StringBuilder();
                        j = 3;
                        while (BodyByte < LengMessages && index1 < lines.Length)
                        {
                            j++;
                            this.Body.Append(lines[index1]).AppendLine();
                            BodyByte = BodyByte + UTF8Encoding.UTF8.GetBytes(lines[index1]).Length + 2;
                            index1++;
                        }
                    }
                    else
                    {
                        i1 = 1;
                        int BodyByte = 0;
                        //int LengMessages = Convert.ToInt16(this.header.MessagesLength.Value) - 400;
                        if (this.Body == null) this.Body = new StringBuilder();
                        while (index1 < lines.Length)
                        {

                            this.Body.Append(lines[index1]).AppendLine();
                            BodyByte = BodyByte + UTF8Encoding.UTF8.GetBytes(lines[index1]).Length + 2;
                            index1++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(j.ToString()));
                    throw ex;
                }
            }

        }
        public ReturnMessages(string[] StringLines)
        {
            int index = 0;
            int i1 = 0;
            int j = 0;
                if (StringLines != null && StringLines.Length > 0)
                {

                    for (int i = 0; i < StringLines.Length; i++)
                    {
                        if (StringLines[i].Length == 398 || ASCIIEncoding.ASCII.GetBytes(StringLines[i]).Length == 400)
                        {
                            this.header = new VNACCHeader(StringLines[i]);
                            index = i; // 398 byte đầu tiên
                            break;
                        }
                    }
                }
                try
                {

                    if (this.header != null && this.header.MessagesLength != null && !string.IsNullOrEmpty(this.header.MessagesLength.Value.ToString().Trim()))
                {
                    index++;
                    int BodyByte = 0;
                    j = 1;
                    long LengMessages = Convert.ToInt64(this.header.MessagesLength.Value) - 400;
                    j = 2;
                    if (this.Body == null) this.Body = new StringBuilder();
                    j = 3;
                    while (BodyByte < LengMessages && index < StringLines.Length)
                    {
                        j++;
                        this.Body.Append(StringLines[index]).AppendLine();
                        BodyByte = BodyByte + UTF8Encoding.UTF8.GetBytes(StringLines[index]).Length + 2;
                        index++;
                    }
                }
                else
                {
                    i1 = 1;
                    int BodyByte = 0;
                    //int LengMessages = Convert.ToInt16(this.header.MessagesLength.Value) - 400;
                    if (this.Body == null) this.Body = new StringBuilder();
                    while (index < StringLines.Length)
                    {
                        
                        this.Body.Append(StringLines[index]).AppendLine();
                        BodyByte = BodyByte + UTF8Encoding.UTF8.GetBytes(StringLines[index]).Length + 2;
                        index++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Logger.LocalLogger.Instance().WriteMessage(new Exception(j.ToString()));
                throw ex;
            }
           
        }

        public string getMaNVPhanHoi()
        {
            if (this.header != null)
            {
                return this.header.VungDuTru_1.GetValue(false).ToString().Substring(0, 7);
            }
            else
                return string.Empty;
        }

        public string GetSoTNHeader()
        {
            if (this.header != null)
            {
                return this.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 12);
            }
            return string.Empty;
        }
        public string GetRTPtag()
        {
            if (this.header != null)
            {
                return this.header.VungDuTru_2.GetValue(false).ToString().Substring(64, 30);
            }
            return string.Empty;
        }

        public string GetThongBaoPhanHoi()
        {
            string MaNVPhanHoi = this.getMaNVPhanHoi();
            if (string.IsNullOrEmpty(MaNVPhanHoi))
                return string.Empty;
            else
            {
                PropertyInfo proterty = typeof(EnumThongBao).GetProperty(MaNVPhanHoi);
                if (proterty != null)
                    return proterty.GetValue(null, null).ToString();
                else
                    return string.Empty;
            }

        }
        public static string GetThongBaoPhanHoi(string MaNVPhanHoi)
        {
            if (string.IsNullOrEmpty(MaNVPhanHoi))
                return string.Empty;
            else
            {
                if (MaNVPhanHoi.Contains("X"))
                    MaNVPhanHoi = MaNVPhanHoi.Replace("X", "").Trim();
                FieldInfo field = typeof(EnumThongBao).GetField(MaNVPhanHoi);
                if (field != null)
                    return field.GetValue(null).ToString();
                else
                    return string.Empty;
            }
        }
    }
}
