﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
   public class Staff
    {
        /// <summary>
        /// Bộ phận quản lý
        /// </summary>
        [XmlElement("manageQuantity")]
        public string ManageQuantity { get; set; }

        /// <summary>
        /// Số lượng công nhân
        /// </summary>
        [XmlElement("workerQuantity")]
        public string WorkerQuantity { get; set; }
    }
}
