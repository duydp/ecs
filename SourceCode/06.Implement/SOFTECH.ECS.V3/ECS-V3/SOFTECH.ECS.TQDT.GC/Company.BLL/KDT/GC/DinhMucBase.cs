﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.KDT.GC
{
    public partial class DinhMuc : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected string _MaSanPham = String.Empty;
        protected string _MaNguyenPhuLieu = String.Empty;
        protected string _DVT_ID = String.Empty;
        protected decimal _DinhMucSuDung;
        protected decimal _TyLeHaoHut;
        protected string _GhiChu = String.Empty;
        protected int _STTHang;
        protected long _Master_ID;
        protected decimal _NPL_TuCungUng;
        protected string _TenSanPham = String.Empty;
        protected string _TenNPL = String.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public string MaSanPham
        {
            set { this._MaSanPham = value; }
            get { return this._MaSanPham; }
        }
        public string MaNguyenPhuLieu
        {
            set { this._MaNguyenPhuLieu = value; }
            get { return this._MaNguyenPhuLieu; }
        }
        public string DVT_ID
        {
            set { this._DVT_ID = value; }
            get { return this._DVT_ID; }
        }
        public decimal DinhMucSuDung
        {
            set { this._DinhMucSuDung = value; }
            get { return this._DinhMucSuDung; }
        }
        public decimal TyLeHaoHut
        {
            set { this._TyLeHaoHut = value; }
            get { return this._TyLeHaoHut; }
        }
        public string GhiChu
        {
            set { this._GhiChu = value; }
            get { return this._GhiChu; }
        }
        public int STTHang
        {
            set { this._STTHang = value; }
            get { return this._STTHang; }
        }
        public long Master_ID
        {
            set { this._Master_ID = value; }
            get { return this._Master_ID; }
        }
        public decimal NPL_TuCungUng
        {
            set { this._NPL_TuCungUng = value; }
            get { return this._NPL_TuCungUng; }
        }
        public string TenSanPham
        {
            set { this._TenSanPham = value; }
            get { return this._TenSanPham; }
        }
        public string TenNPL
        {
            set { this._TenNPL = value; }
            get { return this._TenNPL; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_GC_DinhMuc_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) this._MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) this._MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) this._DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) this._TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) this._Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPL_TuCungUng"))) this._NPL_TuCungUng = reader.GetDecimal(reader.GetOrdinal("NPL_TuCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) this._TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) this._TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                return true;
            }
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public DinhMucCollection SelectCollectionBy_Master_ID()
        {
            string spName = "p_KDT_GC_DinhMuc_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            DinhMucCollection collection = new DinhMucCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPL_TuCungUng"))) entity.NPL_TuCungUng = reader.GetDecimal(reader.GetOrdinal("NPL_TuCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_Master_ID()
        {
            string spName = "p_KDT_GC_DinhMuc_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_KDT_GC_DinhMuc_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_GC_DinhMuc_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_GC_DinhMuc_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_GC_DinhMuc_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DinhMucCollection SelectCollectionAll()
        {
            DinhMucCollection collection = new DinhMucCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPL_TuCungUng"))) entity.NPL_TuCungUng = reader.GetDecimal(reader.GetOrdinal("NPL_TuCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DinhMucCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            DinhMucCollection collection = new DinhMucCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPL_TuCungUng"))) entity.NPL_TuCungUng = reader.GetDecimal(reader.GetOrdinal("NPL_TuCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                collection.Add(entity);
            }
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_DinhMuc_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this._DinhMucSuDung);
            this.db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this._TyLeHaoHut);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, this._NPL_TuCungUng);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(DinhMucCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DinhMuc item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, DinhMucCollection collection)
        {
            foreach (DinhMuc item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_DinhMuc_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this._DinhMucSuDung);
            this.db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this._TyLeHaoHut);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, this._NPL_TuCungUng);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(DinhMucCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DinhMuc item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_DinhMuc_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this._DinhMucSuDung);
            this.db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this._TyLeHaoHut);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, this._NPL_TuCungUng);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(DinhMucCollection collection, SqlTransaction transaction)
        {
            foreach (DinhMuc item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

      

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_DinhMuc_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(DinhMucCollection collection, SqlTransaction transaction)
        {
            foreach (DinhMuc item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(DinhMucCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DinhMuc item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion
    }
}