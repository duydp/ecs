﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseRegisterForm : BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_DANHSACHKHO kho = new T_KHOKETOAN_DANHSACHKHO();
        public WareHouseRegisterForm()
        {
            InitializeComponent();

            cbbTK.DataSource = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            cbbTK.DisplayMember = "MATK";
            cbbTK.ValueMember = "MATK";
        }

        private void WareHouseRegisterForm_Load(object sender, EventArgs e)
        {
            if (kho.ID > 0)
            {
                SetWareHouse();
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaKho, errorProvider, " Mã kho ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenKho, errorProvider, " Tên kho ");
                isValid &= ValidateControl.ValidateChoose(cbbTK, errorProvider, " Tài khoản ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetWareHouse();
                // Kiểm tra đã tồn tại kho .
                List<T_KHOKETOAN_DANHSACHKHO> KhoCollection = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
                foreach (T_KHOKETOAN_DANHSACHKHO item in KhoCollection)
                {
                    if (item.MAKHO == txtMaKho.Text.ToString().Trim() && kho.ID == 0)
                    {
                        errorProvider.SetError(txtMaKho, " Mã kho : ''" + txtMaKho.Text.ToString() + "'' này đã được khai báo .");
                        return;
                    }
                }
                kho.InsertUpdate();
                ShowMessage("Lưu thành công ", false);
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void GetWareHouse()
        {
            try
            {
                kho.MAKHO = txtMaKho.Text.ToString();
                kho.TENKHO = txtTenKho.Text.ToString();
                kho.TAIKHOANKHO = cbbTK.Value.ToString();
                kho.GHICHU = txtGhiChu.Text.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetWareHouse()
        {
            try
            {
                txtMaKho.Text = kho.MAKHO.ToString();
                txtTenKho.Text = kho.TENKHO.ToString();
                cbbTK.Value = kho.TAIKHOANKHO.ToString();
                txtGhiChu.Text = kho.GHICHU.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
