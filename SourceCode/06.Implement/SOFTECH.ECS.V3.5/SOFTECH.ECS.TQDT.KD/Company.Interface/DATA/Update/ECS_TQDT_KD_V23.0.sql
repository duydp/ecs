
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N't_KDT_VNACCS_CapSoDinhDanh'))
	DROP TABLE t_KDT_VNACCS_CapSoDinhDanh
GO

CREATE TABLE t_KDT_VNACCS_CapSoDinhDanh
(
ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
SoTiepNhan BIGINT,
NgayTiepNhan DATETIME,
MaDoanhNghiep NVARCHAR(50) NOT NULL,
MaHQ NVARCHAR(50),
LoaiDoiTuong INT,
LoaiTTHH INT,
SoDinhDanh NVARCHAR(50) ,
NgayCap DATETIME,
GuidStr VARCHAR(MAX),
TrangThaiXuLy [int] NULL,
CodeContent VARCHAR(MAX)
)

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CapSoDinhDanh_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CapSoDinhDanh_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CapSoDinhDanh_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CapSoDinhDanh_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CapSoDinhDanh_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CapSoDinhDanh_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaDoanhNghiep nvarchar(50),
	@MaHQ nvarchar(50),
	@LoaiDoiTuong int,
	@LoaiTTHH int,
	@SoDinhDanh nvarchar(50),
	@NgayCap datetime,
	@GuidStr varchar(max),
	@TrangThaiXuLy int,
	@CodeContent varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_CapSoDinhDanh]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaDoanhNghiep],
	[MaHQ],
	[LoaiDoiTuong],
	[LoaiTTHH],
	[SoDinhDanh],
	[NgayCap],
	[GuidStr],
	[TrangThaiXuLy],
	[CodeContent]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaDoanhNghiep,
	@MaHQ,
	@LoaiDoiTuong,
	@LoaiTTHH,
	@SoDinhDanh,
	@NgayCap,
	@GuidStr,
	@TrangThaiXuLy,
	@CodeContent
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaDoanhNghiep nvarchar(50),
	@MaHQ nvarchar(50),
	@LoaiDoiTuong int,
	@LoaiTTHH int,
	@SoDinhDanh nvarchar(50),
	@NgayCap datetime,
	@GuidStr varchar(max),
	@TrangThaiXuLy int,
	@CodeContent varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_CapSoDinhDanh]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[MaHQ] = @MaHQ,
	[LoaiDoiTuong] = @LoaiDoiTuong,
	[LoaiTTHH] = @LoaiTTHH,
	[SoDinhDanh] = @SoDinhDanh,
	[NgayCap] = @NgayCap,
	[GuidStr] = @GuidStr,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[CodeContent] = @CodeContent
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CapSoDinhDanh_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaDoanhNghiep nvarchar(50),
	@MaHQ nvarchar(50),
	@LoaiDoiTuong int,
	@LoaiTTHH int,
	@SoDinhDanh nvarchar(50),
	@NgayCap datetime,
	@GuidStr varchar(max),
	@TrangThaiXuLy int,
	@CodeContent varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_CapSoDinhDanh] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_CapSoDinhDanh] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[MaHQ] = @MaHQ,
			[LoaiDoiTuong] = @LoaiDoiTuong,
			[LoaiTTHH] = @LoaiTTHH,
			[SoDinhDanh] = @SoDinhDanh,
			[NgayCap] = @NgayCap,
			[GuidStr] = @GuidStr,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[CodeContent] = @CodeContent
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_CapSoDinhDanh]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaDoanhNghiep],
			[MaHQ],
			[LoaiDoiTuong],
			[LoaiTTHH],
			[SoDinhDanh],
			[NgayCap],
			[GuidStr],
			[TrangThaiXuLy],
			[CodeContent]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaDoanhNghiep,
			@MaHQ,
			@LoaiDoiTuong,
			@LoaiTTHH,
			@SoDinhDanh,
			@NgayCap,
			@GuidStr,
			@TrangThaiXuLy,
			@CodeContent
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_CapSoDinhDanh]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CapSoDinhDanh_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_CapSoDinhDanh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaDoanhNghiep],
	[MaHQ],
	[LoaiDoiTuong],
	[LoaiTTHH],
	[SoDinhDanh],
	[NgayCap],
	[GuidStr],
	[TrangThaiXuLy],
	[CodeContent]
FROM
	[dbo].[t_KDT_VNACCS_CapSoDinhDanh]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaDoanhNghiep],
	[MaHQ],
	[LoaiDoiTuong],
	[LoaiTTHH],
	[SoDinhDanh],
	[NgayCap],
	[GuidStr],
	[TrangThaiXuLy],
	[CodeContent]
FROM [dbo].[t_KDT_VNACCS_CapSoDinhDanh] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaDoanhNghiep],
	[MaHQ],
	[LoaiDoiTuong],
	[LoaiTTHH],
	[SoDinhDanh],
	[NgayCap],
	[GuidStr],
	[TrangThaiXuLy],
	[CodeContent]
FROM
	[dbo].[t_KDT_VNACCS_CapSoDinhDanh]	

GO

 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '23.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('23.0',GETDATE(), N'CẬP NHẬT KHAI BÁO SỐ ĐỊNH DANH HÀNG HÓA')
END