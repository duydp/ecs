using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.GC.BLL.KDT.GC;

namespace DongBoDuLieu.BLL.GC
{
    public class HD
    {
        public static DataSet GetDataSetHopDong(string MaHaiQuan, string MaDoanhNghiep, int namDangKy, string matKhau)
        {
            
            StringBuilder query = new StringBuilder();
            query.Append("SELECT 0		AS SoTiepNhan, ");
            query.Append("HD.So_HD			AS SoHopDong, ");
            query.Append("HD.Ma_HQHD		AS MaHaiQuan, ");
            query.Append("HD.DVGC			AS MaDoanhNghiep, ");
            query.Append("HD.Ngay_Ky		AS NgayKy, ");
            query.Append("HD.Ngay_HH		AS NgayHetHan, ");
            query.Append("HD.Ngay_GH		AS NgayGiaHan, ");
            query.Append("HD.NuocThueGC		AS NuocThue_ID, ");
            query.Append("HD.NgTe			AS NguyenTe_ID ,");
            query.Append("HD.DVDT		    AS DVDT, ");
            query.Append("HD.DiaChi_DT		AS DiaChi_DT ");
            query.Append("FROM DHDGC HD ");
            query.Append("where DVGC='" + MaDoanhNghiep + "' and Ma_HQHD='" + MaHaiQuan + "' AND YEAR(NGAY_KY) >=" + namDangKy);

            DataSet dsHopDong = WebServiceDB.getDataBySQL(query.ToString(), MaHaiQuan, "SLXNK", MaDoanhNghiep, matKhau);
            return dsHopDong;
            
            /*
            StringBuilder query = new StringBuilder();
            query.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            query.Append("SELECT 0		AS SoTiepNhan, ");
            query.Append("HD.So_HD			AS SoHopDong, ");
            query.Append("HD.Ma_HQHD		AS MaHaiQuan, ");
            query.Append("HD.DVGC			AS MaDoanhNghiep, ");
            query.Append("HD.Ngay_Ky		AS NgayKy, ");
            query.Append("HD.Ngay_HH		AS NgayHetHan, ");
            query.Append("HD.Ngay_GH		AS NgayGiaHan, ");
            query.Append("HD.NuocThueGC		AS NuocThue_ID, ");
            query.Append("HD.NgTe			AS NguyenTe_ID ,");
            query.Append("HD.DVDT		    AS DVDT, ");
            query.Append("HD.DiaChi_DT		AS DiaChi_DT ");
            query.Append("FROM DHDGC HD ");
            query.Append("where DVGC=''" + MaDoanhNghiep + "'' and Ma_HQHD=''" + MaHaiQuan + "'' AND YEAR(NGAY_KY) >=" + namDangKy);
            query.Append(" ') AS a;");

            DataSet dsHopDong = WebServiceDB.getDataBySQL(query.ToString(), MaHaiQuan, "SLXNK", MaDoanhNghiep, matKhau);
            return dsHopDong;
            */
        }

        public static DataSet GetDataSetHopDongLocal(string MaHaiQuan, string MaDoanhNghiep, int namDangKy, string matKhau, string databaseNameTarget)
        {
            StringBuilder query = new StringBuilder();
            query.Append("SELECT 0		AS SoTiepNhan, ");
            query.Append("hd.SoHopDong , ");
            query.Append("hd.MaHaiQuan , ");
            query.Append("hd.MaDoanhNghiep , ");
            query.Append("hd.NgayKy , ");
            query.Append("hd.NgayHetHan , ");
            query.Append("hd.NgayGiaHan , ");
            query.Append("HD.NuocThue_ID , ");
            query.Append("hd.NguyenTe_ID , ");
            query.Append("hd.DonViDoiTac AS DVDT , ");
            query.Append("hd.DiaChiDoiTac AS DiaChi_DT ");
            query.Append("FROM    dbo.t_KDT_GC_HopDong HD  ");
            query.Append("where MaDoanhNghiep='" + MaDoanhNghiep + "' and MaHaiQuan='" + MaHaiQuan + "' AND YEAR(NgayKy) >=" + namDangKy);

            DataSet dsHopDong = WebServiceDB.getDataBySQLDatabaseLocal(query.ToString(), MaHaiQuan);
            return dsHopDong;
        }

        public static DataSet GetHopDongAtTMP()
        {
            string sql = "select * from t_KDT_GC_HopDong where TrangThaiXuLy=1";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }

        public static DataSet GetHopDongAtDN()
        {
            string sql = "select * from t_KDT_GC_HopDong where TrangThaiXuLy=1";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }

        public static bool InsertUpdateHopDongDongBoDuLieu(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, string connectionString)
        {
            bool ret;
            /*
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.ID == 0)
                        InsertTransaction(transaction);
                    else
                        UpdateTransaction(transaction);
                    if (ID > 0)
                    {
                        int i = 1;
                        //xoa ben npl dung cho xu ly du lieu
                        BLL.GC.NguyenPhuLieu nplXuLy = new Company.GC.BLL.GC.NguyenPhuLieu();
                        nplXuLy.HopDong_ID = this.ID;
                        nplXuLy.DeleteBy_HopDong_ID(transaction);
                        NguyenPhuLieu nplHD = new NguyenPhuLieu();
                        nplHD.HopDong_ID = this.ID;
                        nplHD.DeleteBy_HopDong_ID(transaction);
                        foreach (NguyenPhuLieu item in NPLCollection)
                        {
                            try
                            {
                                item.HopDong_ID = this._ID;
                                item.STTHang = i++;
                                item.InsertTransaction(transaction);
                                //cap nhat sang du lieu xu ly
                                BLL.GC.NguyenPhuLieu entity = new Company.GC.BLL.GC.NguyenPhuLieu();
                                entity.HopDong_ID = this.ID;
                                entity.Ma = item.Ma;
                                entity.MaHS = item.MaHS;
                                entity.DVT_ID = item.DVT_ID;
                                entity.SoLuongDangKy = item.SoLuongDangKy;
                                entity.STTHang = item.STTHang;
                                entity.Ten = item.Ten;
                                try
                                {
                                    entity.InsertTransaction(transaction);
                                }
                                catch { }
                            }
                            catch { }
                        }
                        i = 1;
                        BLL.GC.NhomSanPham ItemDelete = new Company.GC.BLL.GC.NhomSanPham();
                        ItemDelete.HopDong_ID = this.ID;
                        ItemDelete.DeleteBy_HopDong_ID(transaction);
                        NhomSanPham ItemHD = new NhomSanPham();
                        ItemHD.HopDong_ID = this.ID;
                        ItemHD.DeleteBy_HopDong_ID(transaction);
                        foreach (NhomSanPham item in nhomSPCollection)
                        {
                            item.HopDong_ID = this._ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransaction(transaction);

                            BLL.GC.NhomSanPham entity = new Company.GC.BLL.GC.NhomSanPham();
                            entity.HopDong_ID = this.ID;
                            entity.MaSanPham = item.MaSanPham;
                            entity.GiaGiaCong = item.GiaGiaCong;
                            entity.SoLuong = item.SoLuong;
                            entity.STTHang = item.STTHang;
                            entity.TenSanPham = item.TenSanPham;
                            entity.InsertTransaction(transaction);
                        }
                        i = 1;
                        BLL.GC.SanPham SPDelete = new Company.GC.BLL.GC.SanPham();
                        SPDelete.HopDong_ID = this.ID;
                        SPDelete.DeleteBy_HopDong_ID(transaction);

                        SanPham SPHD = new SanPham();
                        SPHD.HopDong_ID = this.ID;
                        SPHD.DeleteBy_HopDong_ID(transaction);
                        foreach (SanPham item in SPCollection)
                        {
                            item.HopDong_ID = this._ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransaction(transaction);

                            BLL.GC.SanPham entity = new Company.GC.BLL.GC.SanPham();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.NhomSanPham_ID = item.NhomSanPham_ID;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            try
                            {
                                entity.InsertTransaction(transaction);
                            }
                            catch { }
                        }
                        i = 1;
                        BLL.GC.ThietBi TBDelete = new Company.GC.BLL.GC.ThietBi();
                        TBDelete.HopDong_ID = this.ID;
                        TBDelete.DeleteBy_HopDong_ID(transaction);

                        ThietBi TBHD = new ThietBi();
                        TBHD.HopDong_ID = this.ID;
                        TBHD.DeleteTransaction(transaction);
                        foreach (ThietBi item in TBCollection)
                        {
                            item.HopDong_ID = this._ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransaction(transaction);

                            BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.DonGia = item.DonGia;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            entity.NguyenTe_ID = item.NguyenTe_ID;
                            entity.NuocXX_ID = item.NuocXX_ID;
                            entity.TinhTrang = item.TinhTrang;
                            entity.TriGia = item.TriGia;
                            try
                            {
                                entity.InsertTransaction(transaction);
                            }
                            catch { }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            */
            return true;
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            /*
            string spName = "p_KDT_GC_HopDong_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            this.db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, this._NgayKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, this._NgayHetHan);
            this.db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, this._NgayGiaHan);
            this.db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, this._NuocThue_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, this._DonViDoiTac);
            this.db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, this._DiaChiDoiTac);
            this.db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, this._CanBoTheoDoi);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, this._CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, this._TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamTN", SqlDbType.Int, this._NamTN);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, this._HuongdanPL);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            */
            return 0;
        }

    }
}

