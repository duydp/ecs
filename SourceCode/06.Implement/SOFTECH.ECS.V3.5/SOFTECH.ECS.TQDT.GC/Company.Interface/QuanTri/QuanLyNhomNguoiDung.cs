﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
using Company.GC.BLL;
using Company.KDT.SHARE.Components;

namespace Company.Interface.QuanTri
{
    public partial class QuanLyNhomNguoiDung : BaseForm
    {
        GROUPSCollection collection = new GROUPSCollection();
        GROUPS group = new GROUPS();
        public QuanLyNhomNguoiDung()
        {
            InitializeComponent();
        }

        private void QuanLyNhomNguoiDung_Load(object sender, EventArgs e)
        {
            collection = group.SelectCollectionAll();
            dgList.DataSource = collection;
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem_GC.CreateGroup)))
            {
                TaoMoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem_GC.Permission)))
            {
                PhanQuyen.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem_GC.DeleteGroup)))
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            GROUPS groupEdit = (GROUPS)e.Row.DataRow;
            NhomNguoiDungEditForm f = new NhomNguoiDungEditForm();
            f.group = groupEdit;
            f.ShowDialog();
            collection = group.SelectCollectionAll();
            dgList.DataSource = collection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            if (e.Command.Key == "TaoMoi")
            {
                NhomNguoiDungEditForm f = new NhomNguoiDungEditForm();
                f.ShowDialog();
                collection = group.SelectCollectionAll();
                dgList.DataSource = collection;
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
            else
            {
                QuanLyPhanQuyen f = new QuanLyPhanQuyen();
                f.ShowDialog();
            }
        }

        private void dgList_DeletingRecord(object sender, Janus.Windows.GridEX.RowActionCancelEventArgs e)
        {
            if (ShowMessage("Bạn có muốn xóa nhóm này không ?", true) == "Yes")
            {
                GROUPS g = (GROUPS)e.Row.DataRow;
                g.LoadUserList();
                if (g.CheckUserInGroup())
                {
                    if (ShowMessage("Có người dùng nằm trong nhóm này. Bạn có muốn xóa không ?", true) == "Yes")
                    {
                        g.Delete();
                    }
                    else
                        e.Cancel = true;
                }
                else
                    g.Delete();
            }
            else
                e.Cancel = true;
        }
    }
}

