﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Linq;
using Company.KDT.SHARE.QuanLyChungTu;
namespace Company.Interface.KDT.GC
{
    public partial class ThietBiGCBoSungForm : BaseFormHaveGuidPanel
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
        public bool boolFlag;
        public string MaLoaiPK = string.Empty;
        public String Caption;
        public bool IsChange;

        public ThietBiGCBoSungForm()
        {
            InitializeComponent();
            ctrNguyenTe.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
            ctrNuoc.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtDonGia.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGia.TextChanged -= new EventHandler(txt_TextChanged);
        }

        private void BindData()
        {
            try
            {
                dgThietBi.Refresh();
                dgThietBi.DataSource = LoaiPK.HPKCollection;
                dgThietBi.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || pkdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void SetData()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = HangPK.MaHang.Trim();
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = HangPK.TenHang.Trim();
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = HangPK.MaHS;
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

                ctrNuoc.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
                ctrNuoc.Ma = HangPK.NuocXX_ID;
                ctrNuoc.ValueChanged += new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);

                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Text = HangPK.SoLuong.ToString();
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtDonGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtDonGia.Text = HangPK.DonGia.ToString();
                txtDonGia.TextChanged += new EventHandler(txt_TextChanged);

                txtTriGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGia.Text = HangPK.TriGia.ToString();
                txtTriGia.TextChanged += new EventHandler(txt_TextChanged);

                ctrNguyenTe.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
                ctrNguyenTe.Ma = HangPK.NguyenTe_ID;
                ctrNguyenTe.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);

                chTinhTrang.CheckedChanged -= new EventHandler(txt_TextChanged);
                chTinhTrang.Checked = HangPK.TinhTrang == "1";
                chTinhTrang.CheckedChanged += new EventHandler(txt_TextChanged);

                txtMaCu.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaCu.Text = HangPK.ThongTinCu.Trim();
                txtMaCu.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ThietBiGCEditForm_Load(object sender, EventArgs e)
        {
            bool isSuaSP = false;
            isSuaSP = MaLoaiPK == "504";
            dgThietBi.RootTable.Columns["ThongTinCu"].Visible = txtMaCu.ReadOnly =
            lblCu.Visible = txtMaCu.Visible = lblTenCu.Visible = isSuaSP;

            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";

            cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;

            Caption = this.Text;
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == MaLoaiPK)
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (this.OpenType == OpenFormType.View)
            {
                LoaiPK.LoadCollection();
                btnAdd.Enabled = false;
                btnXoa.Enabled = false;
                bntAddExcel.Enabled = false;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            else
            {
                LoaiPK.LoadCollection();
                btnAdd.Enabled = true;
                btnXoa.Enabled = true;
                bntAddExcel.Enabled = true;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            //Registered
            if (boolFlag == true)
            {
                btnAdd.Enabled = true;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            LoaiPK.MaPhuKien = MaLoaiPK;
            if (LoaiPK.HPKCollection.Count == 0)
                LoaiPK.LoadCollection();
            if (MaLoaiPK == "104")
            {
                txtMa.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            }
            BindData();
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
        }

        private void setErro()
        {
            error.Clear();
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            error.SetError(txtMaHS, null);

        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                if (MaLoaiPK == "804")
                {
                    isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã thiết bị", isOnlyWarning);
                }
                else if (MaLoaiPK == "504")
                {
                    isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã thiết bị", isOnlyWarning);
                    isValid &= ValidateControl.ValidateSpecialChar(txtMaCu, errorProvider, "Mã thiết bị", isOnlyWarning);
                }
                else
                {
                    isValid &= ValidateControl.ValidateSpecialChar(txtMaCu, errorProvider, "Mã thiết bị", isOnlyWarning);
                }
                //Cursor = Cursors.WaitCursor;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtTriGia_Leave(null, null);
            txtMa.Focus();
            cvError.Validate();
            if (!ValidateForm(false))
                return;
            if (!cvError.IsValid) return;
            {
                if (Convert.ToDecimal(txtSoLuong.Text) <= 0)
                {
                    error.SetError(txtSoLuong, setText("Số lượng phải lớn hơn 0.", "This value must be greater than 0"));
                    return;
                }
                if (Convert.ToDecimal(txtDonGia.Text) <= 0)
                {
                    error.SetError(txtDonGia, setText("Đơn giá phải lớn hơn 0.", "This value must be greater than 0"));
                    return;
                }

                if (Convert.ToDecimal(txtTriGia.Text) <= 0)
                {
                    error.SetError(txtTriGia, setText("Giá gia công phải lớn hơn 0.", "This value must be greater than 0"));
                    return;
                }
                checkExitsThietBiAndSTTHang();
            }
        }
        private void checkExitsThietBiAndSTTHang()
        {
            try
            {
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                tb.HopDong_ID = this.HD.ID;
                tb.Ma = txtMa.Text.Trim();
                if (tb.Load())
                {
                    if (tb.Ma.Trim().ToUpper() != HangPK.MaHang.Trim().ToUpper())
                    {
                        showMsg("MSG_0203002");
                        //MLMessages("Thiết bị này đã được khai báo.","MSG_STN08","", false);
                        txtMa.Focus();
                        return;
                    }
                }
                LoaiPK.HPKCollection.Remove(HangPK);
                foreach (HangPhuKien p in LoaiPK.HPKCollection)
                {
                    if (p.MaHang == txtMa.Text.Trim())
                    {
                        showMsg("MSG_0203003");
                        //MLMessages("Đã có thiết bị này trong danh sách.", "MSG_WRN06", "", false);
                        if (HangPK.MaHang.Trim() != "")
                            LoaiPK.HPKCollection.Add(HangPK);
                        txtMa.Focus();
                        return;
                    }
                }
                HangPK.Delete(LoaiPK.MaPhuKien, HD.ID);
                HangPK.ID = 0;
                HangPK.MaHang = txtMa.Text.Trim();
                HangPK.TenHang = txtTen.Text.Trim();
                HangPK.MaHS = txtMaHS.Text.Trim();
                HangPK.ThongTinCu = txtMaCu.Text.Trim();
                HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                HangPK.NuocXX_ID = ctrNuoc.Ma;
                HangPK.SoLuong = Convert.ToDecimal(txtSoLuong.Text);
                HangPK.DonGia = Convert.ToDouble(txtDonGia.Text);
                HangPK.TriGia = Convert.ToDouble(txtTriGia.Text);
                HangPK.NguyenTe_ID = ctrNguyenTe.Ma;
                HangPK.TinhTrang = chTinhTrang.Checked ? "1" : "0";
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                LoaiPK.InsertUpdateBoSungTB(pkdk.HopDong_ID);
                Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                if (lpk == null)
                    pkdk.PKCollection.Add(LoaiPK);
                reset();
                this.setErro();
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void dgThietBi_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            HangPK = (HangPhuKien)e.Row.DataRow;
            if (HangPK != null)
            {
                SetData();
            }
        }

        private void dgThietBi_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            e.Row.Cells["TenXuatXu"].Text = this.Nuoc_GetName(e.Row.Cells["XuatXu"].Value).Trim();
            string tinhTrang = e.Row.Cells["TinhTrang"].Value.ToString() == "0" ? "Còn mới" : "Cũ";
            e.Row.Cells["TinhTrang"].Text = tinhTrang;
            try
            {
                e.Row.Cells["TenNguyenTe"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe"].Value).Trim();
            }
            catch { }
        }

        private void txtTriGia_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal soluong = Convert.ToDecimal(txtSoLuong.Text);
                decimal dongia = Convert.ToDecimal(txtDonGia.Text);
                decimal trigia = soluong * dongia;
                txtTriGia.Text = trigia.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                txtDonGia.Text = "0";
                txtTriGia.Text = "0";
            }
        }

        private void dgThietBi_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void ThietBiGCBoSungForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (!btnAdd.Enabled) return;
            if (LoaiPK.HPKCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
            Company.GC.BLL.KDT.GC.HangPhuKienCollection pkColl = new HangPhuKienCollection();
            pkColl = (HangPhuKienCollection)LoaiPK.HPKCollection.Clone();
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {  //pkDelete.Delete(LoaiPK.MaPhuKien,pkdk.HopDong_ID);
                            try
                            {
                                pkDelete.DeleteTransaction(null);
                            }
                            catch { }
                        }
                        try
                        {
                            pkColl.Remove(pkDelete);
                            LoaiPK.HPKCollection = (HangPhuKienCollection)pkColl.Clone();
                        }
                        catch { 
                        }
                    }
                }
                BindData();
                this.reset();
                this.setErro();
                this.SetChange(true);
            }
        }

        private void reset()
        {
            txtMa.TextChanged -= new EventHandler(txt_TextChanged);
            txtMa.Text = "";
            txtMa.TextChanged += new EventHandler(txt_TextChanged);

            txtTen.TextChanged -= new EventHandler(txt_TextChanged);
            txtTen.Text = "";
            txtTen.TextChanged += new EventHandler(txt_TextChanged);

            txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaHS.Text = "";
            txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong.Text = "0.000";
            txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

            txtDonGia.TextChanged -= new EventHandler(txt_TextChanged);
            txtDonGia.Text = "0.000";
            txtDonGia.TextChanged += new EventHandler(txt_TextChanged);

            txtTriGia.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGia.Text = "0.000";
            txtTriGia.TextChanged += new EventHandler(txt_TextChanged);

            chTinhTrang.Checked = false;

            ctrNuoc.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
            ctrNuoc.Ma = GlobalSettings.NUOC;
            ctrNuoc.ValueChanged += new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);

            cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

            ctrNguyenTe.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
            ctrNguyenTe.Ma = GlobalSettings.NGUYEN_TE_MAC_DINH;
            ctrNguyenTe.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
            BindData();
            HangPK = new HangPhuKien();


        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            FormChonThietBi frm = new FormChonThietBi();
            frm.HD = HD;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (frm.HPKCollection.Count > 0)
                    {
                        if (LoaiPK.HPKCollection.Count == 0)
                        {
                            foreach (HangPhuKien hangPK in frm.HPKCollection)
                            {
                                hangPK.Master_ID = LoaiPK.ID;
                                LoaiPK.HPKCollection.Add(hangPK);
                            }
                        }
                        else
                        {
                            bool IsExits = false;
                            foreach (HangPhuKien hangPK in frm.HPKCollection)
                            {
                                IsExits = false;
                                foreach (HangPhuKien item in LoaiPK.HPKCollection)
                                {
                                    if (hangPK.MaHang == item.MaHang)
                                    {
                                        IsExits = true;
                                        break;
                                    }
                                }
                                if (!IsExits)
                                {
                                    hangPK.Master_ID = LoaiPK.ID;
                                    LoaiPK.HPKCollection.Add(hangPK);
                                }
                            }
                        }
                        this.SetChange(true);
                    }
                    //foreach (HangPhuKien hangPK in frm.HPKCollection)
                    //{
                    //    hangPK.Master_ID = LoaiPK.ID;
                    //    LoaiPK.HPKCollection.Add(hangPK);
                    //}
                    LoaiPK.Master_ID = pkdk.ID;
                    LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                    if (boolFlag == true)
                    {
                        LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                        boolFlag = false;
                    }
                    else
                    {
                        LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
                    }
                    Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                    if (lpk == null)
                        pkdk.PKCollection.Add(LoaiPK);
                    BindData();
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                reset();
                this.setErro();
                this.SetChange(true);
            }

        }

        private void bntAddExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Company.Interface.KDT.SXXK.ThietBiReadExcelForm f = new Company.Interface.KDT.SXXK.ThietBiReadExcelForm();
                f.FormImport = "PK";
                f.HD = HD;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                foreach (ThietBi item in f.TBCollection)
                {
                    LoaiPK.HPKCollection.Add(new HangPhuKien
                    {
                        MaHang = item.Ma.Trim(),
                        TenHang = item.Ten.Trim(),
                        MaHS = item.MaHS,
                        DVT_ID = item.DVT_ID,
                        SoLuong = item.SoLuongDangKy,
                        DonGia = item.DonGia
                    });
                }
                this.SetChange(f.ImportExcelSucces);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                if (boolFlag == true)
                {
                    LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                    boolFlag = false;
                }
                else
                {
                    LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
                }
                Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                if (lpk == null)
                    pkdk.PKCollection.Add(LoaiPK);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }

        private void ctrNuoc_Load(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void uiGroupBox2_Click(object sender, EventArgs e)
        {

        }

        private void dgThietBi_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            HangPK = (HangPhuKien)i.GetRow().DataRow;
                            if (HangPK != null)
                            {
                                SetData();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }   
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH THIẾT BỊ KHAI BÁO CỦA PHỤ KIỆN.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgThietBi;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgThietBi.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaTB.Text);
                dgThietBi.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

    }
}