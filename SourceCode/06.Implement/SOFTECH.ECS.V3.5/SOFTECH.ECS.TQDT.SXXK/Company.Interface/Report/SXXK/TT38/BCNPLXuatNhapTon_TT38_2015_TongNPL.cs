using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Linq;

namespace Company.Interface.Report.SXXK
{
    public partial class BCNPLXuatNhapTon_TT38_2015_TongNPL : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        private int STTBanLuu = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        private DataTable DtGhiChu = new DataTable();
        private DataTable dt = new DataTable();
        private decimal SoToKhaiNhap = 0;
        private decimal SoToKhaiNhapBanLuu = 0;
        private decimal SoToKhaiXuat = 0;
        private decimal SoToKhaiXuatBanLuu = 0;
        private decimal temp = 0;

        private string MaNPLTemp = "";
        private string MaNPLBanLuu = "";
        private int cnt = 1; //Gia tri phai thiet lap mac dinh ban dau = 1.
        private DataTable dtMaNPLTemp;

        private string MaSPTemp = "";
        private string MaSPBanLuu = "";

        private decimal _TongLuongNhap = 0;
        private decimal _TongLuongTonDau = 0;
        private decimal _TongLuongNPLSuDung = 0;
        private decimal _TongLuongTonCuoi = 0;
        private decimal _TongLuongNPLTaiXuat = 0;

        private int _ChuyenTiepTK = 0;

        public BCNPLXuatNhapTon_TT38_2015_TongNPL()
        {
            InitializeComponent();
        }

        public void BindReport(string where)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            string order = "";
            int sapxeptkx = Properties.Settings.Default.TKX;
            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi, LuongNhap DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                if (sapxeptkx == 2)
                    order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    //order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                    //TODO: Update by Hungtq 08/06/2012. Bỏ sắp xếp theo "ngayhoanthanhxuat" 
                    //dùng hiển thị đúng thứ tự tờ khai âm kế tiếp. Ngày của tờ khai âm kế tiếp có thế < tờ khai trước đó.

                    //Tuannq 05062015
                    //Thay đổi ORDER theo LUONGNHAP
                    //order = "MaNPL, ngaydangkynhap, LuongNhap DESC";
                    order = "MaNPL, ngaydangkynhap, LuongTonCuoi DESC";
                //ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi, 
                else
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
            }
            else
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";

            }

            
            dtMaNPLTemp = BLL.SXXK.ThanhKhoan.BCXuatNhapTon.GetSoLanNPLThamGiaThanhKhoan(GlobalSettings.MA_DON_VI, this.LanThanhLy);
        

            dt = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("(MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND LanThanhLy = " + this.LanThanhLy + ")" + where, order).Tables[0];
          

            int i = 0;
            string maNPL = "";
            DateTime ngayDangKyNhap = DateTime.Today;
            long soToKhaiNhap = 0;
            string _luongNhap = "";
            foreach (DataRow dr in dt.Rows)
            {
                if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]) && _luongNhap ==dr["LuongNhap"].ToString())
                {
                    dr["STT"] = i;
                }
                else
                {
                    maNPL = dr["MaNPL"].ToString();
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                    _luongNhap = dr["LuongNhap"].ToString();
                    i++;
                    dr["STT"] = i;
                }
            }
            
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;

            //Updated by Hungtq 18/09/2012
            //lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            //lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            //lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL"),
                new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                //Tuannq 05062015
                //Thay dổi GROUP print report

                //Thay đổi nhóm theo điều kiện xét
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                //new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
              // new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});
                new DevExpress.XtraReports.UI.GroupField("LuongNhap")});
                //new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                //new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")
            }
            else
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL"),
                new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});

            }
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
            if (!string.IsNullOrEmpty(GlobalSettings.NoiDungSoHoSoThanhKhoan))
            {
                lblSHSTK.Text = lblSHSTK.Text + GlobalSettings.NoiDungSoHoSoThanhKhoan;
            }
            lblSHSTK.Width = 300;
            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            //lblMaNPL.DataBindings.Add("Text", this.DataSource,  dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblNgayHoanThanhNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhNhap", "{0:dd/MM/yy}");
            lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //lblMaSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaSP");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            if (GlobalSettings.SoThapPhan.MauBC04 == 0)
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            else
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            lblLuongSPXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSPXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblTenDVT_SP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_SP");
            lblDinhMuc.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMuc", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoToKhaiTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiTaiXuat");
            lblNgayTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTaiXuat", "{0:dd/MM/yy}");
            lblLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");
            //lblChuyenMucDichKhac.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChuyenMucDichKhac", "{0:n3}");

            //Updated by Hungtq 18/09/2012
            //lblTongLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");

            //HQ
            lblNgayThangHQ.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            lblNgayThangDN.Text = GlobalSettings.TieudeNgay;

            //Bo sung them cot Index tren table
            DataColumn colIndex = new DataColumn("Index", typeof(int));
            dt.Columns.Add(colIndex);

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                dt.Rows[j]["Index"] = j;
            }
        }

        public void BindReportByMaNPL(string where, string MaNPL)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            string order = "";
            int sapxeptkx = Properties.Settings.Default.TKX;
            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                if (sapxeptkx == 2)
                    order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    //order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                    //TODO: Update by Hungtq 08/06/2012. Bỏ sắp xếp theo "ngayhoanthanhxuat" 
                    //dùng hiển thị đúng thứ tự tờ khai âm kế tiếp. Ngày của tờ khai âm kế tiếp có thế < tờ khai trước đó.
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
                else
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
            }
            else
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";

            }

            dtMaNPLTemp = BLL.SXXK.ThanhKhoan.BCXuatNhapTon.GetSoLanNPLThamGiaThanhKhoan(GlobalSettings.MA_DON_VI, this.LanThanhLy);

            dt = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("(MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaNPL ='" + MaNPL + "' AND LanThanhLy = " + this.LanThanhLy + ")" + where, order).Tables[0];
            int i = 1;
            string maNPL = "";
            DateTime ngayDangKyNhap = DateTime.Today;
            long soToKhaiNhap = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]))
                {
                    dr["STT"] = i;
                }
                else
                {
                    maNPL = dr["MaNPL"].ToString();
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                    i++;
                    dr["STT"] = i;
                }
            }
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;
            //lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            //lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            //lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap")});
                //new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                //new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                //new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")
            }
            else
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});

            }
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
            if (!string.IsNullOrEmpty(GlobalSettings.NoiDungSoHoSoThanhKhoan))
            {
                lblSHSTK.Text = lblSHSTK.Text + GlobalSettings.NoiDungSoHoSoThanhKhoan;
            }
            lblSHSTK.Width = 300;
            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            //lblMaNPL.DataBindings.Add("Text", this.DataSource,  dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblNgayHoanThanhNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhNhap", "{0:dd/MM/yy}");
            lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //lblMaSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaSP");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            if (GlobalSettings.SoThapPhan.MauBC04 == 0)
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            else
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            lblLuongSPXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSPXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblTenDVT_SP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_SP");
            lblDinhMuc.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMuc", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoToKhaiTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiTaiXuat");
            lblNgayTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTaiXuat", "{0:dd/MM/yy}");
            lblLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep", "{0:n3}");
            //lblChuyenMucDichKhac.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChuyenMucDichKhac", "{0:n3}");

            //Updated by Hungtq 18/09/2012
            //lblTongLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");

            //HQ
            lblNgayThangHQ.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            lblNgayThangDN.Text = GlobalSettings.TieudeNgay;

            //Bo sung them cot Index tren table
            DataColumn colIndex = new DataColumn("Index", typeof(int));
            dt.Columns.Add(colIndex);

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                dt.Rows[j]["Index"] = j;
            }
        }

        private void lblMaSP_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblMaSP.Text = GetCurrentColumnValue("TenSP").ToString();
            if (GetCurrentColumnValue("MaSP").ToString().Trim().Length > 0)
                lblMaSP.Text = lblMaSP.Text + " / " + GetCurrentColumnValue("MaSP").ToString();
        }

        private void lblSoToKhaiTaiXuat_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiTaiXuat")) == 0)
                lblSoToKhaiTaiXuat1.Text = "";
        }

        private void lblLuongNPLTaiXuat_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0)
                lblTongLuongNPLTaiXuat.Text = "";
        }

        private void lblToKhaiNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblToKhaiNhap.Text = "Tổng lượng tờ khai " + Convert.ToString(GetCurrentColumnValue("SoToKhaiNhap")) + "/" + GetCurrentColumnValue("MaLoaiHinhNhap").ToString() + "/" + Convert.ToDateTime(GetCurrentColumnValue("NgayDangKyNhap")).Year;
            lblToKhaiNhap.Text = "Tổng lượng nguyên phụ liệu " + GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblMaNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();
        }

        private void BCNPLXuatNhapTon_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
            this.DtGhiChu = new DataTable();
            DataColumn[] cols = new DataColumn[3];
            cols[0] = new DataColumn("MaNPL", typeof(string));
            cols[1] = new DataColumn("From", typeof(int));
            cols[2] = new DataColumn("To", typeof(int));
            this.DtGhiChu.Columns.AddRange(cols);
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (MaNPLBanLuu != GetCurrentColumnValue("MaNPL").ToString())
            {
                this.STT++;
            }

            temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
            if (lblSoToKhaiNhap.Text != "")
                SoToKhaiNhap = Convert.ToInt64(lblSoToKhaiNhap.Text);
        }

        private void GroupHeader1_AfterPrint(object sender, EventArgs e)
        {

            //TODO: Updated by HungTQ, 17/09/2012.
            //Visible Group Footer
            MaNPLTemp = GetCurrentColumnValue("MaNPL").ToString(); //Da su dung dong nay tai vi tri Detail_BeforePrint
            MaSPTemp = GetCurrentColumnValue("MaSP").ToString() + "/" + GetCurrentColumnValue("SoToKhaiXuat").ToString();
           
            if (MaNPLBanLuu != MaNPLTemp)
            {
                MaNPLBanLuu = MaNPLTemp;
                cnt = 1;
                _TongLuongNhap = (System.Decimal)GetCurrentColumnValue("LuongNhap");
                _TongLuongTonDau = (System.Decimal)GetCurrentColumnValue("LuongTonDau");
               
                _TongLuongNPLSuDung = 0;
                _TongLuongTonCuoi = 0;
                _TongLuongNPLTaiXuat = 0;
            }
            else
            {
                cnt += 1;
                _TongLuongNhap += (System.Decimal)GetCurrentColumnValue("LuongNhap");
                _TongLuongTonDau += (System.Decimal)GetCurrentColumnValue("LuongTonDau");
            }
                      
            GroupFooter1.Visible = (cnt == LaySoLanMaNPLThamGiaThanhKhoan(MaNPLTemp));
          

        }

        private int LaySoLanMaNPLThamGiaThanhKhoan(string maNPL)
        {
            return dtMaNPLTemp.Select("MaNPL = '" + maNPL + "'").Length;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (STTBanLuu != STT)
            {
                lblSTT.Text = this.STT + "";

                STTBanLuu = STT;
            }
            else
                lblSTT.Text = "";
        }

        private void lblSoToKhaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) > 0) ((XRTableCell)sender).Text = "";
            if (((XRTableCell)sender).Name == "lblLuongSPXuat")
            {
                if (Convert.ToDecimal(GetCurrentColumnValue("LuongSPXuat")) == 0) ((XRTableCell)sender).Text = "";
            }
        }

        private void lblSoToKhaiTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToInt64(GetCurrentColumnValue("SoToKhaiTaiXuat")) == 0) lblSoToKhaiTaiXuat.Text = "";
        }

        private void lblNgayTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDateTime(GetCurrentColumnValue("NgayTaiXuat")).Year == 1900) lblNgayTaiXuat.Text = "";
        }

        private void lblLuongNPLTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0) lblLuongNPLTaiXuat.Text = "";
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
#if DEBUG
            //SoToKhaiNhap = Convert.ToInt64(GetCurrentColumnValue("SoToKhaiNhap"));
            //SoToKhaiXuat = Convert.ToInt64(GetCurrentColumnValue("SoToKhaiXuat"));
            //if ((SoToKhaiNhap == 477 || SoToKhaiNhap == 614) && SoToKhaiXuat == 239 && GetCurrentColumnValue("MaSP").ToString() == "Ta-ko3-TB-SPS")
            //{ }
#endif
            //Cap nhat so to khai nhap ban luu
            SoToKhaiNhap = Convert.ToInt64(GetCurrentColumnValue("SoToKhaiNhap"));
            if (SoToKhaiNhapBanLuu != SoToKhaiNhap)
            {
                SoToKhaiNhapBanLuu = SoToKhaiNhap;
            }

            //Cap nhat so to khai nhap ban luu
            if (SoToKhaiXuatBanLuu != SoToKhaiXuat)
            {
                SoToKhaiXuatBanLuu = SoToKhaiXuat;
            }

            if (GetCurrentColumnValue("MaSP") == null) e.Cancel = true;
            if (GetCurrentColumnValue("MaSP").ToString() == "") e.Cancel = true;

            MaSPTemp = GetCurrentColumnValue("MaSP").ToString() + "/" + GetCurrentColumnValue("SoToKhaiXuat").ToString();

            //TODO: Updated by HungTQ, 17/09/2012.
            //Visible Group Footer            
            if (MaNPLBanLuu != MaNPLTemp)
            {
                MaNPLBanLuu = MaNPLTemp;

                _TongLuongNPLSuDung = (System.Decimal)GetCurrentColumnValue("LuongNPLSuDung");
                _TongLuongNPLTaiXuat = (System.Decimal)GetCurrentColumnValue("LuongNPLTaiXuat");
            }
            else
            {
                if (MaSPBanLuu != MaSPTemp)
                {
                    MaSPBanLuu = MaSPTemp;
                    _TongLuongNPLSuDung += (System.Decimal)GetCurrentColumnValue("LuongNPLSuDung");
                }
                else
                {
                    //Updated by HungTQ, 23/01/2013.
                    //Kiem tra truong hop dong tinh tong NPL cuoi cung cua bao cao do luon luon = 0.
                    if (cnt == 1)
                    {
                        _TongLuongNPLSuDung += (System.Decimal)GetCurrentColumnValue("LuongNPLSuDung");
                    }
                    //else
                    //{
                        //if (SoToKhaiNhapBanLuu == SoToKhaiNhap && SoToKhaiXuatBanLuu == SoToKhaiXuat)
                          //  _TongLuongNPLSuDung += (System.Decimal)GetCurrentColumnValue("LuongNPLSuDung");
                    //}
                }

                _TongLuongNPLTaiXuat += (System.Decimal)GetCurrentColumnValue("LuongNPLTaiXuat");
            }

            //lblThanhKhoanTiep.Text = SoToKhaiNhap.ToString() + ", " + _TongLuongNPLSuDung.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
        }

        private void lblThanhKhoanTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblThanhKhoanTiep.Text.Contains("Chuyển TK"))
            {
                DataRow dr = this.DtGhiChu.NewRow();
                dr["MaNPL"] = GetCurrentColumnValue("MaNPL").ToString();
                dr["From"] = Convert.ToInt64(GetCurrentColumnValue("SoToKhaiNhap"));
                dr["To"] = Convert.ToInt64(lblThanhKhoanTiep.Text.Replace("Chuyển TK ", ""));
                this.DtGhiChu.Rows.Add(dr);
            }

            //if ((temp - Convert.ToDecimal(GetCurrentColumnValue("LuongNPLSuDung"))) != Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi")))
            //{
            //    int from = GetFrom(GetCurrentColumnValue("MaNPL").ToString(), Convert.ToInt64(GetCurrentColumnValue("SoToKhaiNhap")));
            //    if (from > 0)
            //        lblThanhKhoanTiep.Text = "Chuyển từ TK " + from;
            //    else
            //        lblThanhKhoanTiep.Text = "";
            //}
            //else
            //if (Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi")) < 0)
            //{
            //    if (MaNPLBanLuu == MaNPLTemp)
            //    {
            //        int from = GetFrom(GetCurrentColumnValue("MaNPL").ToString(), Convert.ToInt64(GetCurrentColumnValue("SoToKhaiNhap")));
            //        if (from > 0)
            //            lblThanhKhoanTiep.Text = "Chuyển từ TK " + from;
            //        //else
            //        //    lblThanhKhoanTiep.Text = "";
            //    }
            //}
            //else
            //    lblThanhKhoanTiep.Text = "";
            temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
        }

        private decimal GetFrom(string maNPL, int to)
        {
            foreach (DataRow dr in this.DtGhiChu.Rows)
            {
                if (dr["MaNPL"].ToString() == maNPL && to == Convert.ToInt64(dr["To"]))
                    return Convert.ToInt64(dr["From"]);
            }
            return 0;
        }

        private void lblTongThanhKhoanTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTongThanhKhoanTiep.Text.Contains("Chuyển TK"))
            {
                DataRow dr = this.DtGhiChu.NewRow();
                dr["MaNPL"] = GetCurrentColumnValue("MaNPL").ToString();
                dr["From"] = Convert.ToInt64(GetCurrentColumnValue("SoToKhaiNhap"));
                dr["To"] = Convert.ToInt64(lblTongThanhKhoanTiep.Text.Replace("Chuyển TK ", ""));
                this.DtGhiChu.Rows.Add(dr);
            }
        }

        private void lblDinhMuc_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDinhMuc.Text = GetCurrentColumnValue("DinhMuc").ToString() + " / " + GetCurrentColumnValue("TenDVT_SP").ToString();
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                //TODO: Updated by HungTQ, 17/09/2012.
                lblTongLuongNhap.Text = _TongLuongNhap.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                lblTongTonDau.Text = _TongLuongTonDau.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                lblTongLuongNPLSuDung.Text = _TongLuongNPLSuDung.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                lblTongTonCuoi.Text = (_TongLuongTonDau - _TongLuongNPLSuDung - _TongLuongNPLTaiXuat).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}
