﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using System.Data;
using Company.Interface.SXXK;

namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamSendForm_CX : BaseFormHaveGuidPanel
    {
        private readonly SanPhamDangKy spDangKy = new SanPhamDangKy();
        public SanPham SPDetail;
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public bool isAdd = true;
        public bool isEdit = false;
        public bool isCancel = false;
        public SanPhamCollection SPCollection = new SanPhamCollection();
        SanPhamRegistedForm SPRegistedForm = new SanPhamRegistedForm();
        //-----------------------------------------------------------------------------------------
        public SanPhamSendForm_CX()
        {
            InitializeComponent();
        }

        #region Private methods.

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            //cmbMucDich.DataSource = MucDichSuDung.SelectAll().Tables[0];
            //cmbMucDich.DisplayMember = "TenLoaiMucDich";
            //cmbMucDich.ValueMember = "ID";
            SPCollection = SanPham.SelectCollectionAll();
        }

        //-----------------------------------------------------------------------------------------
        private void SanPhamSendForm_CX_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            if (spDangKy.ID == 0)
            {
                if (isEdit)
                {
                    spDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                }
                else if (isCancel)
                {
                    spDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                }
                else
                {
                    if (spDangKy.ID == 0)
                    {
                        spDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                }
            }
            else
            {
                spDangKy.LoadSPCollection();
            }
            BindData();
            SetCommandStatus();
        }
        private void SetCommandStatus()
        {
            if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = spDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || spDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                dtpNgayTN.Value = DateTime.Now;
                lblTrangThai.Text = spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = spDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || spDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO || spDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = spDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = spDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lblTrangThai.Text = setText("Chờ duyệt", "Wait approved");
                this.OpenType = OpenFormType.View;
            }


        }
        //private void SetCommandStatus()
        //{
        //    if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
        //    {
        //        cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

        //        cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

        //        lblTrangThai.Text = setText("Đã duyệt", "Approved");
        //        this.OpenType = OpenFormType.View;
        //    }
        //    else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
        //    {
        //        cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

        //        cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

        //        lblTrangThai.Text = spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Wait for approval") : setText("Chờ hủy", "Wait for cancel");
        //        this.OpenType = OpenFormType.View;
        //    }
        //    else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || spDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
        //    {
        //        cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdAddExcel.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

        //        cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        lblTrangThai.Text = spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY ? setText("Đã hủy", "Canceled") : setText("Không phê duyệt", "Not approved");
        //        this.OpenType = OpenFormType.View;
        //    }
        //    else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
        //    {
        //        cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

        //        cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

        //        lblTrangThai.Text = "Đang hủy";
        //        this.OpenType = OpenFormType.Edit;
        //    }
        //    else
        //    {
        //        cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

        //        cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

        //        lblTrangThai.Text = spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Not declared") : setText("Đang sửa", "Editing");
        //        this.OpenType = OpenFormType.Edit;
        //    }

        //}

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới nguyên phụ liệu.
        /// </summary>
        private void add()
        {
            SanPhamEditForm_CX f = new SanPhamEditForm_CX();
            f.OpenType = OpenFormType.Insert;
            f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            f.SPCollection = spDangKy.SPCollection;
            f.spDangKy = spDangKy;
            f.ShowDialog(this);

            if (f.SPDetail != null)
            {
                this.spDangKy.SPCollection.Add(f.SPDetail);
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void save()
        {
            try
            {

                if (this.spDangKy.SPCollection.Count == 0)
                {
                    // ShowMessage("Danh sách sản phẩm rỗng.\nKhông thể cập nhật dữ liệu.", false);
                    MLMessages("Danh sách sản phẩm rỗng.\nKhông thể cập nhật dữ liệu.", "MSG_SAV11", "", false);

                    return;
                }

                this.Cursor = Cursors.WaitCursor;
                // Master.
                this.spDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (this.spDangKy.ID == 0)
                {
                    this.spDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    this.spDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.spDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.spDangKy.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.spDangKy.GUIDSTR = Guid.NewGuid().ToString();
                }
                if (this.spDangKy.InsertUpdateFull())
                {
                    this.Cursor = Cursors.Default;

                    #region Lưu log thao tác
                    string where = "1 = 1";
                    where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", spDangKy.ID, LoaiKhaiBao.SanPham);
                    List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                    if (listLog.Count > 0)
                    {
                        long idLog = listLog[0].IDLog;
                        string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                        long idDK = listLog[0].ID_DK;
                        string guidstr = listLog[0].GUIDSTR_DK;
                        string userKhaiBao = listLog[0].UserNameKhaiBao;
                        DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                        string userSuaDoi = GlobalSettings.UserLog;
                        DateTime ngaySuaDoi = DateTime.Now;
                        string ghiChu = listLog[0].GhiChu;
                        bool isDelete = listLog[0].IsDelete;
                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                    userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                    }
                    else
                    {
                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                        log.LoaiKhaiBao = LoaiKhaiBao.SanPham;
                        log.ID_DK = spDangKy.ID;
                        log.GUIDSTR_DK = this.spDangKy.GUIDSTR;
                        log.UserNameKhaiBao = GlobalSettings.UserLog;
                        log.NgayKhaiBao = DateTime.Now;
                        log.UserNameSuaDoi = GlobalSettings.UserLog;
                        log.NgaySuaDoi = DateTime.Now;
                        log.GhiChu = "";
                        log.IsDelete = false;
                        log.Insert();
                    }
                    #endregion
                    MLMessages("Lưu thành công!", "MSG_SAV02", "", false);
                }
                else
                {
                    this.Cursor = Cursors.Default;
                    MLMessages("Lưu không thành công!", "MSG_SAV01", "", false);
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        //-----------------------------------------------------------------------------------------
        private void updateRowOnGrid(string maNPL)
        {
            GridEXRow[] jrows = dgList.GetRows();
            foreach (GridEXRow row in jrows)
            {
                if (row.Cells["Ma"].Value.ToString().Equals(maNPL))
                {
                    row.BeginEdit();
                    row.Cells["IsExistOnServer"].Value = 1;
                    row.EndEdit();
                    break;
                }
            }

        }

        //-----------------------------------------------------------------------------------------
        private void updateRowsOnGrid(IEnumerable<string> collection)
        {
            GridEXRow[] jrows = dgList.GetRows();
            foreach (GridEXRow row in jrows)
            {
                row.BeginEdit();
                row.Cells["IsExistOnServer"].Value = 0;
                row.EndEdit();
            }

            foreach (string s in collection)
            {
                this.updateRowOnGrid(s);
            }
        }
        #endregion


        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                string MucDichSuDung = e.Row.Cells["MucDichSuDung"].Value.ToString();
                switch (MucDichSuDung)
                {
                    case "01":
                        e.Row.Cells["MucDichSuDung"].Text = "Nguồn gốc là sản phẩm bán thành phầm, phế liệu phế phẩm";
                        break;
                    case "02":
                        e.Row.Cells["MucDichSuDung"].Text = "Nguồn gốc là máy móc tạo tài sản cố định chờ thanh lý";
                        break;
                    case "04":
                        e.Row.Cells["MucDichSuDung"].Text = "Nguồn gốc là các hàng hóa khác đưa vào phục vụ cho các hoạt động sản xuất của DN chờ đưa ra";
                        break;
                    default:
                        break;
                }
                //e.Row.Cells["MucDichSuDung"].Text = Company.KDT.SHARE.Components.DuLieuChuan.MucDichSuDung.GetName(e.Row.Cells["MucDichSuDung"].Value.ToString());
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //GridEXSelectedItemCollection items = dgList.SelectedItems;
            //foreach (GridEXSelectedItem i in items)
            //{
            //    if (i.RowType == RowType.Record)
            //    {
            //        SanPhamEditForm f = new SanPhamEditForm();
            //        f.SPDetail = this.spDangKy.SPCollection[i.Position];
            //        f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //        f.OpenType = OpenFormType.Edit;
            //        f.SPCollection = spDangKy.SPCollection;
            //        f.SPCollection.RemoveAt(i.Position);
            //        f.spDangKy = spDangKy;
            //        f.ShowDialog(this);
            //    }
            //    break;
            //}
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    SPDetail = new SanPham();
                    SPDetail = (SanPham)e.Row.DataRow;
                    txtMa.Text = this.SPDetail.Ma;
                    txtTen.Text = this.SPDetail.Ten;
                    txtMaHS.Text = this.SPDetail.MaHS;
                    cbDonViTinh.SelectedValue = this.SPDetail.DVT_ID;
                    cmbMucDich.SelectedValue = this.SPDetail.MucDich;
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdSend":
                    this.SendV5();
                    break;
                case "cmdAddExcel":
                    this.AddSanPhamFromExcel();
                    break;
                case "XacNhan":
                    this.FeedBackV5();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                case "cmdCancel":
                    if (ShowMessage("Doanh nghiệp có chắc chắn muốn Khai báo hủy Sản phẩm này đến HQ không ? ", true) == "Yes")
                    {
                        spDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        spDangKy.Update();
                        SetCommandStatus();
                        this.SendV5();
                    }
                    break;
                case "cmdEdit":
                    this.SendV5();
                    break;
                case "cmdSuaSanPham":
                    this.SuaSanPham();
                    break;
                case "cmdResult":
                    this.Result();
                    break;
            }
        }
        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = spDangKy.ID;
                form.DeclarationIssuer = DeclarationIssuer.DNCX_HANG_RA;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        // duydp
        private void SuaSanPham()
        {
            try
            {
                string msg = "";
                msg += "-------------Thông tin sản phẩm đã khai báo-------------";
                msg += "\nSố tiếp nhận : " + spDangKy.SoTiepNhan.ToString();
                msg += "\nNgày tiếp nhận : " + spDangKy.NgayTiepNhan.ToString("dd/MM/yy hh:mm:ss");
                msg += "\nHải quan tiếp nhận : " + spDangKy.MaHaiQuan.ToString();
                msg += "\n--------------------Thông tin xác nhận--------------------";
                msg += "\nBạn có muốn chuyển sang trạng thái Khai báo sửa không ?";
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", msg, true) == "Yes")
                {
                    spDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    spDangKy.InsertUpdate();
                    spDangKy.DeleteFull(spDangKy.MaDoanhNghiep, spDangKy.MaHaiQuan);
                    SetCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void inPhieuTN()
        {
            try
            {
                if (this.spDangKy.SoTiepNhan == 0) return;
                Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
                phieuTN.phieu = "SẢN PHẨM";
                phieuTN.soTN = this.spDangKy.SoTiepNhan.ToString();
                phieuTN.ngayTN = this.spDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
                phieuTN.BindReport();
                phieuTN.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_CX_HangDuaRaDangKy", "", Convert.ToInt32(spDangKy.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void AddSanPhamFromExcel()
        {
            try
            {
                SanPhamReadExcelForm f = new SanPhamReadExcelForm();
                f.SPCollection = this.spDangKy.SPCollection;
                f.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Delete();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void XoaSanPham(GridEXSelectedItemCollection items)
        {

            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            List<SanPham> sanphams = new List<SanPham>();
            List<GridEXSelectedItem> itemRemove = new List<GridEXSelectedItem>();
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    SanPham sp = (SanPham)i.GetRow().DataRow;
                    sanphams.Add(sp);
                    msgWarning += string.Format("Mã ={0} [Xóa]\r\n", sp.Ma);
                }
            }
            try
            {
                msgWarning += "Bạn có đồng ý xóa không?";
                if (Globals.ShowMessage(msgWarning, true) != "Yes") return;

                foreach (SanPham item in sanphams)
                {
                    if (item.ID > 0 && item.CloneToDB(null))
                        item.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                }
                foreach (SanPham item in sanphams)
                {
                    //spDangKy.DanhSachHangDuaRa.Remove(item);
                }

            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void Delete()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaSanPham(items);
            BindData();

        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Delete();
            //SanPhamCollection SPCollectionTMP = new SanPhamCollection();
            //if (MLMessages("Bạn có muốn xóa sản phẩm này không?", "", "MSG_DEL01", true) == "Yes")
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            SanPham spSelected = (SanPham)i.GetRow().DataRow;
            //            // Thực hiện xóa trong CSDL.
            //            if (spSelected.ID > 0)
            //            {
            //                spSelected.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
            //                SPCollectionTMP.Add(spSelected);
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    return;
            //}
            //foreach (SanPham sp in SPCollectionTMP)
            //{
            //    spDangKy.DanhSachHangDuaRa.Remove(sp);
            //}
            //try { dgList.Refetch(); }
            //catch { dgList.Refresh(); }
        }
        //--------------------------------------------------------------------------------
        #region Send V5 Create by LANNT
        private void SendV5()
        {
            //if (GlobalSettings.SendV4 && !Company.KDT.SHARE.Components.Globals.LaDNCX)
            //{
            //    ShowMessage("Chức năng này không sử dụng theo chuẩn khai báo V4", false);
            //    return;

            //}
            if (spDangKy.ID == 0)
            {
                this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                return;
            }
            if (this.spDangKy.SPCollection.Count == 0)
            {
                // ShowMessage("Danh sách sản phẩm rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                return;
            }
            bool isKhaibaosua = false;
            if(spDangKy.TrangThaiXuLy ==5)
                isKhaibaosua= true;
            this.spDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN.Trim();
            this.spDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI.Trim();
            this.spDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY.Trim();
            try
            {
                spDangKy.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.SXXK_SanPham sanpham = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferObject_DNCX_HangDuaRa(spDangKy, "", isKhaibaosua, GlobalSettings.TEN_DON_VI);
              
                ObjectSend msgSend = new ObjectSend(new NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = spDangKy.MaDoanhNghiep,
                               },
                                new NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spDangKy.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(spDangKy.MaHaiQuan).Trim() : spDangKy.MaHaiQuan
                                     // Identity = spDangKy.MaHaiQuan,
                                 },
                                  new SubjectBase()
                                {
                                    Type = DeclarationIssuer.DNCX_HANG_RA,
                                    Reference = spDangKy.GUIDSTR,
                                },
                                sanpham);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(spDangKy.ID, MessageTitle.KhaiBaoSanPham);
                    cmdSend.Enabled = cmdAdd.Enabled = cmdAddExcel.Enabled = cmdAddExcel1.Enabled  = cmdSave.Enabled = cmdSave1.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    MsgSend sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.SanPham;
                    sendXML.func = 1;
                    sendXML.master_id = spDangKy.ID;
                    //sendXML.msg = msgSend;
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            spDangKy.DeleteFull(spDangKy.MaDoanhNghiep, spDangKy.MaHaiQuan);
                        }
                        else
                        {
                            spDangKy.TransgferDataToSXXK();
                        }
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        FeedBackV5();
                        SetCommandStatus();
                    }
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.SanPham;
            sendXML.master_id = spDangKy.ID;
            if (!sendXML.Load())
            {
                ShowMessage("THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = spDangKy.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                 {
                     Issuer = DeclarationIssuer.DNCX_HANG_RA,
                     Reference = reference,
                     Function = DeclarationFunction.HOI_TRANG_THAI,
                     Type = DeclarationIssuer.DNCX_HANG_RA,

                 };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = spDangKy.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spDangKy.MaHaiQuan.Trim()),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(spDangKy.MaHaiQuan).Trim() : spDangKy.MaHaiQuan
                                                  //Identity = spDangKy.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            spDangKy.DeleteFull(spDangKy.MaDoanhNghiep, spDangKy.MaHaiQuan);
                        }
                        else
                        {
                            spDangKy.TransgferDataToSXXK();
                        }
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        spDangKy.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                    }
                }

            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            {
                feedbackContent = SingleMessage.SanPhamSendHandler(spDangKy, ref msgInfor, e);
            }
        }

        #endregion

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMa, errorProvider, "Mã Sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTen, errorProvider, "Tên Sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHS, errorProvider, "Mã số hàng hóa", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbDonViTinh, errorProvider, "Đơn vị tính", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cmbMucDich, errorProvider, "Mục đích sử dụng", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool checkSPExit(string maSP)
        {
            if (spDangKy.SPCollection != null)
            {

                foreach (SanPham sp in spDangKy.SPCollection)
                {
                    if (sp.Ma.Trim().ToUpper() == maSP.Trim().ToUpper()) return true;
                }
            }
            return false;
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (checkSPExit(txtMa.Text.Trim()))
                {
                    MLMessages("Sản phẩm này đã được thêm vào trước đây.", "MSG_PUB10", "", false);
                    return;
                }
                this.SPDetail = new SanPham();
                this.SPDetail.Ma = txtMa.Text.Trim();
                this.SPDetail.Ten = txtTen.Text.Trim();
                this.SPDetail.MaHS = txtMaHS.Text.Trim();
                this.SPDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                this.SPDetail.MucDich = cmbMucDich.SelectedValue.ToString();
                bool isExits = false;
                // KIỂM TRA NPL ĐÃ ĐĂNG KÝ TRƯỚC ĐÓ
                if (!isEdit || !isCancel)
                {
                    foreach (SanPham item in SPCollection)
                    {
                        if (item.Ma == this.SPDetail.Ma && item.Master_ID != spDangKy.ID)
                        {
                            SanPhamDangKy SPExits = new SanPhamDangKy();
                            SPExits.ID = item.Master_ID;
                            SPExits.Load();
                            string Error = "";
                            Error += "\nID : " + SPExits.ID;
                            Error += "\nSỐ TIẾP NHẬN : " + SPExits.SoTiepNhan.ToString();
                            Error += "\nNGÀY TIẾP NHẬN : " + SPExits.NgayTiepNhan.ToString("dd/MM/yyyy");
                            switch (SPExits.TrangThaiXuLy)
                            {
                                case -4:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ KHAI BÁO HỦY";
                                    break;
                                case -3:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ KHAI BÁO";
                                    break;
                                case -2:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : CHỜ DUYỆT KHAI BÁO SỬA";
                                    break;
                                case -1:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : CHƯA KHAI BÁO";
                                    break;
                                case 0:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : CHỜ DUYỆT";
                                    break;
                                case 1:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ DUYỆT";
                                    break;
                                case 2:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : KHÔNG PHÊ DUYỆT";
                                    break;
                                case 4:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ KHAI BÁO SỬA";
                                    break;
                                case 5:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : ĐANG SỬA";
                                    break;
                                case 10:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ  HỦY";
                                    break;
                                case 11:
                                    Error += "\nTRẠNG THÁI XỬ LÝ : CHỜ HỦY";
                                    break;
                                default:
                                    break;
                            }
                            ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "SẢN PHẨM NÀY ĐÃ ĐƯỢC ĐĂNG KÝ TRƯỚC ĐÓ VỚI THÔNG TIN " + Error, false);
                            //errorProvider.SetError(txtMa, "Sản phẩm này đã được đăng ký\n" + Error);
                            isExits = true;
                            return;
                        }
                    }
                    if (!isExits)
                    {
                        spDangKy.SPCollection.Add(SPDetail);
                    }
                }
                else
                {
                    // KIỂM TRA MÃ SP NÀY ĐÃ ĐĂNG KÝ HAY CHƯA . CHỈ NHỮNG SP ĐÃ ĐĂNG KÝ MỚI ĐƯỢC PHÉP KHAI BÁO SỬA HOẶC HỦY
                    isExits = false;
                    foreach (SanPham item in SPCollection)
                    {
                        if (item.Ma == this.SPDetail.Ma)
                        {
                            errorProvider.SetError(txtMa, "SẢN PHẨM NÀY CHƯA ĐƯỢC ĐĂNG KÝ CHỈ NHỮNG SP ĐÃ ĐĂNG KÝ MỚI ĐƯỢC PHÉP KHAI BÁO SỬA HOẶC HỦY .");
                            isExits = true;
                            return;
                        }
                    }
                    if (!isExits)
                    {
                        spDangKy.SPCollection.Add(SPDetail);
                    }
                }
                isAdd = true;
                BindData();
                txtMa.Text = String.Empty;
                txtTen.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = this.spDangKy.SPCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            SPDetail = new SanPham();
                            SPDetail = (SanPham)i.GetRow().DataRow;

                            txtMa.Text = this.SPDetail.Ma;
                            txtTen.Text = this.SPDetail.Ten;
                            txtMaHS.Text = this.SPDetail.MaHS;
                            cbDonViTinh.SelectedValue = this.SPDetail.DVT_ID;
                            cmbMucDich.SelectedValue = this.SPDetail.MucDich;
                            isAdd = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH SẢN PHẨM KHAI BÁO.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMa_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                bool isExits = false;
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    if (spDangKy.SPCollection.Count >= 1)
                    {
                        isExits = false;
                        foreach (SanPham item in spDangKy.SPCollection)
                        {
                            if (item.Ma == this.SPRegistedForm.SanPhamSelected.Ma)
                            {
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            txtMa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                            txtTen.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                            txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                            cmbMucDich.SelectedValue = "01";
                        }
                    }
                    else
                    {
                        txtMa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTen.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                        cmbMucDich.SelectedValue = "01";
                    }
                }
                else if (this.SPRegistedForm.SanPhamsCollectionSelected.Count >= 1)
                {
                    SanPhamCollection SPSelectCollection = new SanPhamCollection();
                    isExits = false;
                    foreach (Company.BLL.SXXK.SanPham item in SPRegistedForm.SanPhamsCollectionSelected)
                    {
                        if (spDangKy.SPCollection.Count >= 1)
                        {
                            foreach (SanPham ite in spDangKy.SPCollection)
                            {
                                isExits = false;
                                if (ite.Ma == item.Ma)
                                {
                                    isExits = true;
                                    break;
                                }
                            }
                            if (!isExits)
                            {
                                SPDetail = new SanPham();
                                SPDetail.Ma = item.Ma;
                                SPDetail.Ten = item.Ten;
                                SPDetail.MaHS = item.MaHS;
                                SPDetail.DVT_ID = item.DVT_ID;
                                SPDetail.MucDich = "01";
                                SPSelectCollection.Add(SPDetail);
                            }
                        }
                        else
                        {
                            SPDetail = new SanPham();
                            SPDetail.Ma = item.Ma;
                            SPDetail.Ten = item.Ten;
                            SPDetail.MaHS = item.MaHS;
                            SPDetail.DVT_ID = item.DVT_ID;
                            SPDetail.MucDich = "01";
                            spDangKy.SPCollection.Add(SPDetail);
                        }
                    }
                    foreach (SanPham item in SPSelectCollection)
                    {
                        spDangKy.SPCollection.Add(item);
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}