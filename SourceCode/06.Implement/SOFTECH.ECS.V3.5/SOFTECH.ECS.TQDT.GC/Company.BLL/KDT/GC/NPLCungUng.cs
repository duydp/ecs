using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;

namespace Company.GC.BLL.KDT.GC
{
    public partial class NPLCungUng
    {
        public void SetDabaseMoi(string nameDatabase)
        {
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameDatabase);
        }
        //---------------------------------------------------------------------------------------------
        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        public static DataTable GetNPLTuCungUng(string MaDN, int NamDK, long HopDongID)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            const string spName = "[dbo].[p_NPLTuCungUng]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDN.Trim());
            db.AddInParameter(dbCommand, "@HopDongID", SqlDbType.BigInt, HopDongID);
            db.AddInParameter(dbCommand, "@NamTN", SqlDbType.Int, NamDK);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            DataTable dt = new DataTable();
            if (ds.Tables.Count == 0)
                return dt;
            else return ds.Tables[0];
        }
        public List<NPLCungUngDetail> NPLCungUngDetails { get; set; }
        public static List<NPLCungUng> PhanBoNPLCungUngs(long master_id, long TKMDID, long IDHopDong)
        {
            List<NPLCungUng> items = new List<NPLCungUng>();

            const string spName = "[dbo].[p_XuLyNPLCungUng]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_id);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMDID);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NPLCungUng entity = new NPLCungUng();
                entity.TKMDID = TKMDID;
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuong"))) entity.TongLuong = reader.GetDouble(reader.GetOrdinal("TongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                items.Add(entity);
            }
            reader.Close();
            return items;
        }
        public static List<NPLCungUngDetail> CungUngChiTiets(long TKMD_ID, long master_id, long IDHopDong)
        {
            List<NPLCungUngDetail> items
                = new List<NPLCungUngDetail>();
            const string spName = "[dbo].[p_XuLyNPLCungUngChiTiet]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_id);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NPLCungUngDetail entity = new NPLCungUngDetail() { TyLeQuyDoi = 1 };
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCung"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("LuongCung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.DongHangTrenCT = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayChungTu"))) entity.NgayChungTu = reader.GetDateTime(reader.GetOrdinal("NgayChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("HinhThuCungUng"))) entity.HinhThuc = reader.GetInt32(reader.GetOrdinal("HinhThuCungUng"));
                items.Add(entity);
            }
            reader.Close();
            return items;
        }
    }
}
