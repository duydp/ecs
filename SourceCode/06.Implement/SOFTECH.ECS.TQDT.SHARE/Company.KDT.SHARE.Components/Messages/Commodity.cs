﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{

    public class Commodity : CommodityBase
    {
         /// <summary>
         /// Mã HS mở rộng
         /// </summary>
         [XmlElement("tariffClassificationExtension")]
         public string TariffClassificationExtension { set; get; }
         /// <summary>
         /// Nhãn hiệu
         /// </summary>
         [XmlElement("brand")]
         public string Brand { set; get; }
         /// <summary>
         /// Quy cách, phẩm chất
         /// </summary>
         [XmlElement("grade")]
         public string Grade { set; get; }
         /// <summary>
         /// Thành phần
         /// </summary>
         [XmlElement("ingredients")]
         public string Ingredients { set; get; }
         /// <summary>
         /// model hàng hóa
         /// </summary>
         [XmlElement("modelNumber")]
         public string ModelNumber { set; get; }
         /// <summary>
         /// Thuế
         /// </summary>
         [XmlElement("DutyTaxFee")]
         public List<DutyTaxFee> DutyTaxFee { set; get; }
         /// <summary>
         /// ...(chưa sử dụng)
         /// </summary>
         [XmlElement("InvoiceLine")]
         public InvoiceLine InvoiceLine { set; get; }
        /// <summary>
        /// Nhóm sản phẩm
        /// </summary>
         [XmlElement("ProductGroup")]
         public string ProductGroup { get; set; }
    }
}
