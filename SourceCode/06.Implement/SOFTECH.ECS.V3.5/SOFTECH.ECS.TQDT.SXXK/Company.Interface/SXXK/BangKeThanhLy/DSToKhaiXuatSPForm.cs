using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.SXXK.BangKeThanhLy
{
    public partial class DSToKhaiXuatSPForm : BaseForm
    {
        public decimal SoToKhai = 0;
        public string MaLoaiHinh ="";
        public DateTime NgayDangKy = new DateTime(1900,1,1);
        public string MaSP = "";
        public string TenSP= "";
        public decimal SoLuong = 0;
        public long BangKeHSTL_ID;
        public string DVT_ID;
        public int sothutuhang = 0;
        public DataSet ds;
        public DSToKhaiXuatSPForm()
        {
            InitializeComponent();
        }

        private void dgList1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                this.SoToKhai = Convert.ToDecimal(e.Row.Cells["SoToKhai"].Value);
                this.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.TenSP = e.Row.Cells["TenHang"].Text;
                this.SoLuong = Convert.ToDecimal(e.Row.Cells["SoLuong"].Value);
                this.MaHaiQuan = Convert.ToString(e.Row.Cells["MaHaiQuan"].Value);
                this.MaSP = e.Row.Cells["Ma"].Text;
                this.DVT_ID = Convert.ToString(e.Row.Cells["DVT_ID"].Value);
                this.sothutuhang = Convert.ToInt32(e.Row.Cells["SoThuTuHang"].Value);
                this.Close();
            }
        }

        private void dgList1_LoadingRow(object sender, RowLoadEventArgs e)
        {
           
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
            }
        }

 
        private void DSToKhaiXuatSPForm_Load_1(object sender, EventArgs e)
        {
            dgList1.DataSource = ds.Tables[0];
        }
    }
}