using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.KDT.DNCX
{
    public partial class HangTon_ManagerForm : BaseForm
    {
        List<KDT_SXXK_HangTonDangKy> ListHangTon = new List<KDT_SXXK_HangTonDangKy>();
        private KDT_SXXK_HangTonDangKy HangTonDK = new KDT_SXXK_HangTonDangKy();
       
        public HangTon_ManagerForm()
        {
            InitializeComponent();
        }

        private void HangTon_ManagerForm_Load(object sender, EventArgs e)
        {
            LoadData();
            
        }
        
        private void LoadData()
        {
            ListHangTon = KDT_SXXK_HangTonDangKy.SelectCollectionAll();
            dgList2.DataSource = ListHangTon;
            dgList2.Refetch();
        }

        

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string id = dgList2.CurrentRow.Cells["ID"].Value.ToString();
                    HangTonDK = KDT_SXXK_HangTonDangKy.LoadFull(Convert.ToInt32(id));
                    HangTonDetailForm frm = new HangTonDetailForm();
                    frm.hangTonDK = HangTonDK;
                    frm.ShowDialog(this);
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
           
            
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                
                case "cmdThemMoi":
                    ThemMoi();
                    break;
                //case"cmdKhaiBao":
                //    SendV5();
                //    break;
                //case"cmdNhanPhanHoi":
                //    FeedBackV5();
                //    break;
                //case"cmdKetQua":
                //    KetQuaXuLyTCU();
                //    break;


            }
        }

        private void ThemMoi()
        {
            HangTonDK = new KDT_SXXK_HangTonDangKy();
            HangTonDetailForm frm = new HangTonDetailForm();
            frm.hangTonDK = HangTonDK;
            frm.ShowDialog(this);
            LoadData();
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        ///// <param name="e"></param>
        //void SendHandler(object sender, SendEventArgs e)
        //{
        //    feedbackContent = SingleMessage.HangTonSendHandler(BKHangTon, ref msgInfor, e);
        //}
        //void SendMessage(object sender, SendEventArgs e)
        //{
        //    this.Invoke(
        //        new EventHandler<SendEventArgs>(SendHandler),
        //        sender, e);
        //}
        //private void SendV5()
        //{
        //    if (ShowMessage("Bạn có muốn khai báo bảng kê tự cung ứng này đến Hải quan?", true) == "Yes")
        //    {
        //        if (BKHangTon.ID == 0)
        //        {
        //            this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
        //            return;
        //        }
        //        else
        //        {
        //            List<KDT_SXXK_BKHangTonKhoDangKy> listHT = KDT_SXXK_BKHangTonKhoDangKy.SelectCollectionDynamic("LanThanhLy= " + HSTL.LanThanhLy, "");
        //            BKHangTon = listHT[0];
        //        }
        //        MsgSend sendXML = new MsgSend();
        //        sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
        //        sendXML.master_id = BKHangTon.ID;
        //        if (sendXML.Load())
        //        {
        //            ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
        //            cmdluu.Enabled = cmdluu1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //            cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

        //            return;
        //        }
        //        try
        //        {
                   

        //                string returnMessage = string.Empty;
        //                BKHangTon.GUIDSTR = Guid.NewGuid().ToString();
        //                SXXK_SanPham TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferObject_DNCX_HangTonKho(BKHangTon, "", DeclarationIssuer.SXCX_HangTon, false, GlobalSettings.TEN_DON_VI);

        //            ObjectSend msgSend = new ObjectSend(
        //                     new NameBase()
        //                     {
        //                         Name = GlobalSettings.TEN_DON_VI,
        //                         Identity = BKHangTon.MaDoanhNghiep
        //                     },
        //                      new NameBase()
        //                      {
        //                          Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BKHangTon.MaHaiQuan)),
        //                          Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BKHangTon.MaHaiQuan).Trim() : BKHangTon.MaHaiQuan
        //                          //Identity = BKHangTon.MaHaiQuan
        //                      },
        //                      new SubjectBase()
        //                      {

        //                          Type = DeclarationIssuer.SXCX_HangTon,
        //                          Function = DeclarationFunction.KHAI_BAO,
        //                          Reference = BKHangTon.GUIDSTR,
        //                      },
        //                      TuCungUng
        //                    );
        //                BKHangTon.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
        //                SendMessageForm sendForm = new SendMessageForm();
        //                sendForm.Send += SendMessage;
        //                bool isSend = sendForm.DoSend(msgSend);
        //                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
        //                {
        //                    sendForm.Message.XmlSaveMessage(BKHangTon.ID, MessageTitle.KhaiBaoPhuKien);
        //                    cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //                    cmdluu.Enabled = cmdluu1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = cmdluu.Enabled = cmdluu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //                    //btnDelete.Enabled = false;
        //                    sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
        //                    sendXML.master_id = BKHangTon.ID;
        //                    sendXML.msg = msgSend.ToString();
        //                    sendXML.func = 1;
        //                    sendXML.InsertUpdate();

        //                    BKHangTon.Update();
        //                    FeedBackV5();
        //                }
        //                else if (!string.IsNullOrEmpty(msgInfor))
        //                    ShowMessageTQDT(msgInfor, false);
                    
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //            
        //        }
        //    }
        //}
        //private void FeedBackV5()
        //{
        //    bool isFeedBack = true;
        //    int count = Company.KDT.SHARE.Components.Globals.CountSend;
        //    //MsgSend sendXML = new MsgSend();
        //    //sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
        //    //sendXML.master_id = BKHangTon.ID;
        //    //if (!sendXML.Load())
        //    //{
        //    //    ShowMessage("THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
        //    //    XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //    //    cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //    //    return;
        //    //}
        //    while (isFeedBack)
        //    {
        //        string reference = BKHangTon.GUIDSTR;

        //        SubjectBase subjectBase = new SubjectBase()
        //        {
        //            Issuer = DeclarationIssuer.SXXK_TUCUNGUNG,
        //            Reference = reference,
        //            Function = DeclarationFunction.HOI_TRANG_THAI,
        //            Type = DeclarationIssuer.SXXK_TUCUNGUNG,

        //        };

        //        ObjectSend msgSend = new ObjectSend(
        //                                    new NameBase()
        //                                    {
        //                                        Name = GlobalSettings.TEN_DON_VI,
        //                                        Identity = BKHangTon.MaDoanhNghiep
        //                                    },
        //                                      new NameBase()
        //                                      {
        //                                          Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BKHangTon.MaHaiQuan.Trim())),
        //                                          Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BKHangTon.MaHaiQuan).Trim() : BKHangTon.MaHaiQuan
        //                                          //Identity = BKHangTon.MaHaiQuan
        //                                      }, subjectBase, null);
        //        SendMessageForm sendForm = new SendMessageForm();
        //        sendForm.Send += SendMessage;
        //        isFeedBack = sendForm.DoSend(msgSend);
        //        if (isFeedBack)
        //        {
        //            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
        //        }

        //    }
        //}
        //private void KetQuaXuLyTCU()
        //{
        //    ThongDiepForm form = new ThongDiepForm();
        //    form.ItemID = this.BKHangTon.ID;
        //    form.DeclarationIssuer = DeclarationIssuer.SXCX_HangTon;
        //    form.ShowDialog(this);
        //}

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(dgList2.CurrentRow.Cells["ID"].Value.ToString());

                if (ID > 0)
                {
                    if (ShowMessage("Bạn muốn xóa bảng kê cung ứng?", true) == "Yes")
                    {
                        KDT_SXXK_HangTonDangKy npl = new KDT_SXXK_HangTonDangKy();
                        npl.DeleteFull(ID);
                    }
                }
                dgList2.CurrentRow.Delete();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList2_LoadingRow(object sender, RowLoadEventArgs e)
        {

            if (e.Row.RowType == RowType.Record)
            {
                string th = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (th)
                {
                    case"-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "5":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Sửa bảng kê";
                        break;
                    case "-5":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đang hủy";
                        break;
                    case "11":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                        break;
                    case "10":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                        break; 
                }
            }
        }
    }
}