﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ServerRemote
{
    public partial class frmFlash : Form
    {
        delegate void StringParameterDelegate(string Text);
        delegate void StringParameterWithStatusDelegate(string Text, TypeOfMessage tom);
        delegate void SplashShowCloseDelegate();
        bool CloseSplashScreenFlag = false;

        public frmFlash()
        {
            InitializeComponent();
        }
        public void ShowSplashScreen()
        {
            try
            {
                if (InvokeRequired)
                {
                    BeginInvoke(new SplashShowCloseDelegate(ShowSplashScreen));
                    return;
                }
                this.Show();
                Application.Run(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("ShowSplashScreen", ex);
            }

        }
        public void CloseSplashScreen()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new SplashShowCloseDelegate(CloseSplashScreen));
                return;
            }
            CloseSplashScreenFlag = true;
            this.Close();
        }
        public void UpdateStatusText(string Text)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new StringParameterDelegate(UpdateStatusText), new object[] { Text });
                return;
            }
            label1.ForeColor = Color.Green;
            label1.Text = Text;
        }
        public void UpdateStatusTextWithStatus(string Text, TypeOfMessage tom)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new StringParameterWithStatusDelegate(UpdateStatusTextWithStatus), new object[] { Text, tom });
                return;
            }
            switch (tom)
            {
                case TypeOfMessage.Error:
                    label1.ForeColor = Color.Red;
                    break;
                case TypeOfMessage.Warning:
                    label1.ForeColor = Color.Yellow;
                    break;
                case TypeOfMessage.Success:
                    label1.ForeColor = Color.White;
                    break;
            }
            label1.Text = Text;

        }
        private void frmFlash_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CloseSplashScreenFlag == false)
                e.Cancel = true;
        }
    }
}
