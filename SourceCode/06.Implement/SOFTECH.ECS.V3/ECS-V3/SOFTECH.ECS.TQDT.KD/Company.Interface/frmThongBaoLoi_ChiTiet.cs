﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.Controls;

namespace Company.Interface
{
    public partial class frmThongBaoLoi_ChiTiet : Form
    {
        public int trangthai = 0;
        public int id ;
        public frmThongBaoLoi_ChiTiet()
        {
            InitializeComponent();
        }
        protected MessageBoxControl _MsgBox;
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmThongBaoLoi_ChiTiet_Load(object sender, EventArgs e)
        {
            if (trangthai == 1)
            {
                Company.KDT.SHARE.Components.ThongBaoLoi Error = Company.KDT.SHARE.Components.ThongBaoLoi.Load(id);
                txtTieuDe.Text = Error.TieuDeThongBao;
                txtLoi.Text = Error.Loi;
                txtThongBao.Text = Error.ThongBao;
                txtGhiChu.Text = Error.GhiChu;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            bool isValidate = false;
            isValidate = Globals.ValidateNull(txtTieuDe, errorProvider, "Tiêu đề thông báo");
            isValidate &= Globals.ValidateNull(txtLoi, errorProvider, "Nội dung lỗi");
            isValidate &= Globals.ValidateNull(txtThongBao, errorProvider, "Hướng dẫn xữ lý");
            if (!isValidate)
                return;

            try
            {
                Company.KDT.SHARE.Components.ThongBaoLoi tbl = new Company.KDT.SHARE.Components.ThongBaoLoi();
                tbl.TieuDeThongBao = txtTieuDe.Text;
                tbl.Loi = txtLoi.Text;
                tbl.ThongBao = txtThongBao.Text;
                tbl.GhiChu = txtGhiChu.Text;
                tbl.DateCreated = DateTime.Today;
                tbl.DateModified = DateTime.Now;
                if (trangthai == 1)
                {
                    tbl.ID = id;
                    tbl.Update();
                }
                else
                {
                    tbl.Insert();
                }
                ShowMessage("Lưu thông báo thành công ",false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lưu thông báo không thành công \n\n"+ex, false);
            }
        }
    }
}
