﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;
using System.Linq;
using GlobalsShare = Company.KDT.SHARE.Components.Globals;
using Company.KDT.SHARE.Components.Messages;
using Company.KDT.SHARE.QuanLyChungTu;
using HangMauDich = Company.BLL.KDT.HangMauDich;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.SXXK;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.PhieuKho;
using Company.KDT.SHARE.VNACCS.VNACC.PhieuKho;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.Components.Messages.GoodsItem;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho;

namespace Company.BLL.DataTransferObjectMapper
{
    /// <summary>
    /// Ánh xạ từ BOs(Bussines object)  sang DTOs (Data Transfer Objects)    
    /// </summary>
    public class Mapper_V4
    {
        private static string sfmtDate = "yyyy-MM-dd";
        private static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        private static string sfmtYear = "yyyy";
        /// <summary>
        /// Chuyển dữ liệu từ SanPhamDangKy BO sang SXXK_SP DTO/ Huy Du Lieu Da Duoc Duyet
        /// </summary>
        /// <param name="sanphamdangky">SanPhamDangKy</param>
        /// <returns>SXXK_SP</returns>
        public static Company.KDT.SHARE.Components.SXXK_SanPham ToDataTransferObject_SXXK_SP(SanPhamDangKy sanphamdangky, bool isCancel, string TenDonVi)
        {
            bool isEdit = sanphamdangky.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            Company.KDT.SHARE.Components.SXXK_SanPham sanpham = new Company.KDT.SHARE.Components.SXXK_SanPham
            {
                Issuer = DeclarationIssuer.SXXK_SP,

                Reference = sanphamdangky.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = isEdit || isCancel ? sanphamdangky.SoTiepNhan.ToString() : String.Empty,
                Acceptance = isEdit || isCancel ? sanphamdangky.NgayTiepNhan.ToString(sfmtDateTime) : String.Empty,
                DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(sanphamdangky.MaHaiQuan).Trim() : sanphamdangky.MaHaiQuan,
                Agents = new List<Agent>(),
                Importer = new NameBase 
                { 
                    Name = TenDonVi,
                    Identity = sanphamdangky.MaDoanhNghiep 
                },
                Product = new List<Product>()
            };
            if (isCancel)
            {
                sanpham.Function = DeclarationFunction.HUY;
            }else if (isEdit)
	        {
		        sanpham.Function = DeclarationFunction.SUA;
	        }
            else
            {
               sanpham.Function = DeclarationFunction.KHAI_BAO;
            }
                sanpham.Agents.Add(new Agent
                {
                    Name = TenDonVi,
                    Identity = sanphamdangky.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                });
                if (sanphamdangky.SPCollection != null)
                    foreach (SanPham item in sanphamdangky.SPCollection)
                    {
                        sanpham.Product.Add(new Product
                        {
                            Commodity = new Commodity
                            {
                                Description = item.Ten,
                                Identification = item.Ma,
                                TariffClassification = item.MaHS.Trim()
                            },
                            GoodsMeasure = new GoodsMeasure
                            {
                                MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) : item.DVT_ID
                            }
                        });
                    }
            return sanpham;
        }

        public static Company.KDT.SHARE.Components.SXXK_SanPham ToDaTaTransferObject_SXXK_SP_SUA(SanPhamDangKySUA sanphamdangkySUA, List<SanPhamSUA> listSP)
        {
            Company.KDT.SHARE.Components.SXXK_SanPham sanpham = new Company.KDT.SHARE.Components.SXXK_SanPham
            {
                Issuer = DeclarationIssuer.SXXK_SP,
                Function = DeclarationFunction.KHAI_BAO,
                Reference = sanphamdangkySUA.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(sanphamdangkySUA.MaHaiQuan) : sanphamdangkySUA.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = sanphamdangkySUA.SoTiepNhan.ToString(),
                Acceptance = sanphamdangkySUA.NgayTiepNhan.ToString(sfmtDateTime),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "??", Identity = sanphamdangkySUA.MaDoanhNghiep },
                Product = new List<Product>()
            };
            sanpham.Agents.Add(new Agent
            {
                Name = "??",
                Identity = sanphamdangkySUA.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (listSP != null)
                foreach (SanPhamSUA item in listSP)
                {
                    sanpham.Product.Add(new Product
                    {
                        Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS.Trim() },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item.DVT_ID) : item.DVT_ID }
                    });
                }
            return sanpham;
        }
        /// <summary>
        /// Chuyển dữ liệu từ NguyenPhuLieuDangKy BO sang SXXK_NPL DTO
        /// </summary>
        /// <param name="nguyenphulieudangky">NguyenPhuLieuDangKy</param>
        /// <returns>SXXK_NPL</returns>
        public static Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu ToDataTransferObject_SXXK_NPL(NguyenPhuLieuDangKy npldangky, bool Huy)
        {
            Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu nguyenphulieu = new Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Issuer = DeclarationIssuer.SXXK_NPL,
                Reference = npldangky.GUIDSTR,
                IssueLocation = "",
                DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(npldangky.MaHaiQuan) : npldangky.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(npldangky.SoTiepNhan),
                Acceptance = npldangky.NgayTiepNhan.ToString(sfmtDateTime),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "??", Identity = npldangky.MaDoanhNghiep },
                Product = new List<Product>(),
            };
            if (Huy) nguyenphulieu.Function = DeclarationFunction.HUY;
            else
                nguyenphulieu.Function = DeclarationFunction.KHAI_BAO;
            nguyenphulieu.Agents.Add(new Agent
            {
                Name = "??",
                Identity = npldangky.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (npldangky.NPLCollection != null)
                foreach (NguyenPhuLieu item in npldangky.NPLCollection)
                {
                    nguyenphulieu.Product.Add(new Product
                    {
                        Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS.Trim() },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item.DVT_ID) : item.DVT_ID }
                    });
                }
            return nguyenphulieu;
        }
        public static Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu ToDaTaTransferObject_SXXK_NPL_SUA(NguyenPhuLieuDangKySUA npldangkysua, List<NguyenPhuLieuSUA> listNPLSua)
        {
            Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu nguyenphulieu = new Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Issuer = DeclarationIssuer.SXXK_NPL,
                Function = DeclarationFunction.SUA,
                Reference = npldangkysua.GUIDSTR,
                IssueLocation = "",
                DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(npldangkysua.MaHaiQuan) : npldangkysua.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,

                CustomsReference = Helpers.FormatNumeric(npldangkysua.SoTiepNhan),
                Acceptance = npldangkysua.NgayTiepNhan.ToString(sfmtDateTime),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "", Identity = npldangkysua.MaDoanhNghiep },
                Product = new List<Product>(),
            };
            nguyenphulieu.Agents.Add(new Agent
            {
                Name = "",
                Identity = npldangkysua.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (listNPLSua != null)
                foreach (NguyenPhuLieuSUA item in listNPLSua)
                {
                    nguyenphulieu.Product.Add(new Product
                    {
                        Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS.Trim() },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID }
                    });
                }
            return nguyenphulieu;
        }

        /// <summary>
        /// Chuyển dữ liệu từ DinhMucDangKy BO sang SXXK_DinhMucSP DTO
        /// </summary>
        /// <param name="dinhmucdangky">DinhMucDangKy</param>
        /// <returns>SXXK_DinhMucSP</returns>
        public static SXXK_DinhMucSP ToDataTransferObject_SXXK_DinhMuc(DinhMucDangKy dmdk, bool huy, bool isKhaiBaoSua)
        {
            SXXK_DinhMucSP dm = new SXXK_DinhMucSP
            {

                Reference = dmdk.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdk.MaHaiQuan) : dmdk.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan, 0),
                Acceptance = dmdk.NgayTiepNhan.ToString(sfmtDateTime),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "??", Identity = dmdk.MaDoanhNghiep },
                ProductionNorm = new List<ProductionNorm>(),
            };
            if (Globals.LaDNCX)
                dm.Issuer = DeclarationIssuer.DNCX_DINHMUC_SANPHAM;
            else
                dm.Issuer = DeclarationIssuer.SXXK_DINH_MUC;
            if (huy) dm.Function = DeclarationFunction.HUY;
            else if (isKhaiBaoSua) dm.Function = DeclarationFunction.SUA;
            else dm.Function = DeclarationFunction.KHAI_BAO;

            dm.Agents.Add(new Agent
            {
                Name = "??",
                Identity = dmdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,

            });
            IEnumerable<DinhMuc> DMCollection = dmdk.DMCollection.ToArray().Distinct(new
            DistinctMaSP());
            try
            {
                foreach (DinhMuc dmSP in DMCollection)
                {
                    Company.BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                    spSXXK = Company.BLL.SXXK.SanPham.getSanPham(dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, dmSP.MaSanPham);
                    ProductionNorm proNorm = new ProductionNorm
                    {
                        Product = new Product
                        {
                            Commodity = new Commodity { Identification = dmSP.MaSanPham, Description = spSXXK.Ten, TariffClassification = spSXXK.MaHS.Trim() },
                            GoodsMeasure = new GoodsMeasure { MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(spSXXK.DVT_ID.Trim()) : spSXXK.DVT_ID }//spSXXK.DVT_ID }
                        },
                        MaterialsNorms = new List<MaterialsNorm>(),
                    };
                    proNorm.MaterialsNorms = NguyenPhuLieu.GetNPLFromDinhMuc(dmdk.ID, dmSP.MaSanPham, dmdk.MaDoanhNghiep, dmdk.MaHaiQuan);

                    if (Globals.IsKhaiVNACCS)
                    {
                        foreach (MaterialsNorm item in proNorm.MaterialsNorms)
                        {
                            item.Material.GoodsMeasure.MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.Material.GoodsMeasure.MeasureUnit.Trim());


                        }
                    }
                    //IEnumerable<DinhMuc> dmItems = from d in dmdk.DMCollection.ToArray()
                    //                               where d.MaSanPham == dmSP.MaSanPham
                    //                               select d;
                    //if (dmItems != null)
                    //{
                    //    foreach (DinhMuc dmNPL in dmItems)
                    //    {
                    //        Company.BLL.SXXK.NguyenPhuLieu nplSXXK = Company.BLL.SXXK.NguyenPhuLieu.getNguyenPhuLieu(dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, dmNPL.MaNguyenPhuLieu);
                    //        proNorm.MaterialsNorms.Add(new MaterialsNorm
                    //        {
                    //            Material = new Product
                    //                {
                    //                    Commodity = new Commodity { Identification = dmNPL.MaNguyenPhuLieu, Description = dmNPL.TenNPL, TariffClassification = nplSXXK.MaHS.Trim() },
                    //                    GoodsMeasure = new GoodsMeasure { MeasureUnit = dmNPL.DVT_ID },
                    //                },
                    //            Norm = Helpers.FormatNumeric(dmNPL.DinhMucSuDung, GlobalsShare.TriGiaNT),
                    //            Loss = Helpers.FormatNumeric(dmNPL.TyLeHaoHut, GlobalsShare.TriGiaNT)

                    //        });
                    //    }
                    dm.ProductionNorm.Add(proNorm);
                    //}
                }
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            

            return dm;
        }
        public static SXXK_DinhMucSP ToDataTransferObject_SXXK_DinhMuc_SUA(DinhMucDangKySUA dmdk, IList<DinhMucSUA> listDMSua, string tenDN)
        {
            long soTiepNhanDMDK;
            DateTime ngayTiepNhanDMDK;
            DinhMucDangKy objDm = DinhMucDangKy.Load(dmdk.IDDMDK);
            soTiepNhanDMDK = objDm == null ? 0 : objDm.SoTiepNhan;
            ngayTiepNhanDMDK = objDm == null ? new DateTime(1900, 01, 01) : objDm.NgayTiepNhan;
            SXXK_DinhMucSP dm = new SXXK_DinhMucSP
            {

                Function = DeclarationFunction.SUA,
                Reference = dmdk.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdk.MaHaiQuan) : dmdk.MaHaiQuan,

                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(soTiepNhanDMDK, 0),
                Acceptance = ngayTiepNhanDMDK.ToString(sfmtDateTime),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "tenDN", Identity = dmdk.MaDoanhNghiep },
                ProductionNorm = new List<ProductionNorm>(),
            };
            if (Globals.LaDNCX)
                dm.Issuer = DeclarationIssuer.DNCX_DINHMUC_SANPHAM;
            else
                dm.Issuer = DeclarationIssuer.SXXK_DINH_MUC;
            dm.Agents.Add(new Agent
            {
                Name = "tenDN",
                Identity = dmdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,

            });
            IEnumerable<DinhMucSUA> DMCollection = listDMSua.ToArray().Distinct(new
             DistinctMaSPSua());
            foreach (DinhMucSUA dmSP in DMCollection)
            {
                Company.BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                spSXXK = Company.BLL.SXXK.SanPham.getSanPham(dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, dmSP.MaSanPham);
                ProductionNorm proNorm = new ProductionNorm
                {
                    Product = new Product
                    {
                        Commodity = new Commodity { Identification = dmSP.MaSanPham, Description = spSXXK.Ten, TariffClassification = spSXXK.MaHS.Trim() },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(spSXXK.DVT_ID) : spSXXK.DVT_ID }//spSXXK.DVT_ID }
                    },
                    MaterialsNorms = new List<MaterialsNorm>(),
                };
                proNorm.MaterialsNorms = NguyenPhuLieu.GetNPLFromDinhMucSUA(dmdk.ID, dmSP.MaSanPham, dmdk.MaDoanhNghiep, dmdk.MaHaiQuan);
                //IEnumerable<DinhMucSUA> dmItems = from d in listDMSua.ToArray()
                //                                  where d.MaSanPham == dmSP.MaSanPham
                //                                  select d;
                //if (dmItems != null)
                //{
                //foreach (DinhMucSUA dmNPL in dmItems)
                //{
                //    Company.BLL.SXXK.NguyenPhuLieu nplSXXK = Company.BLL.SXXK.NguyenPhuLieu.getNguyenPhuLieu(dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, dmNPL.MaNguyenPhuLieu);
                //    proNorm.MaterialsNorms.Add(new MaterialsNorm
                //    {
                //        Material = new Product
                //        {
                //            Commodity = new Commodity { Identification = dmNPL.MaNguyenPhuLieu, Description = nplSXXK.Ten, TariffClassification = nplSXXK.MaHS.Trim() },
                //            GoodsMeasure = new GoodsMeasure { MeasureUnit = dmNPL.DVT_ID },
                //        },
                //        Norm = Helpers.FormatNumeric(dmNPL.DinhMucSuDung, 6),
                //        Loss = Helpers.FormatNumeric(dmNPL.TyLeHaoHut, 4)

                //    });
                //}
                dm.ProductionNorm.Add(proNorm);
                //}
            };
            return dm;
        }
        /// <summary>
        /// Huy Khai Bao To Khai Chua Duoc Duyet
        /// </summary>
        /// <param name="LoaiToKhai"></param>
        /// <returns></returns>
        public static DeclarationBase HuyKhaiBao(string loaiToKhai, string reference, long soTiepNhan, string maHaiQuan, DateTime ngayTiepNhan)
        {
            DeclarationBase dec = new DeclarationBase()
            {
                Issuer = loaiToKhai,
                Reference = reference,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(soTiepNhan, 0),
                Acceptance = ngayTiepNhan.ToString(sfmtDateTime),
                DeclarationOffice = maHaiQuan,
                AdditionalInformations = new List<AdditionalInformation>()
            };
            dec.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Content = new Content() { Text = "." }
                });
            return dec;
        }

        #region TransferOb tờ khai SXXK
        public static ToKhai ToDataTransferObject(ToKhaiMauDich tkmd)
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");
            bool isToKhaiSua = tkmd.SoToKhai != 0 && tkmd.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (isToKhaiSua)
                tkmd.SoTiepNhan = Company.KDT.SHARE.QuanLyChungTu.KhaiBaoSua.SoTNSua(tkmd.SoTiepNhan, tkmd.SoToKhai, tkmd.NgayDangKy.Year, tkmd.MaLoaiHinh,
                        tkmd.MaHaiQuan, tkmd.MaDoanhNghiep, LoaiKhaiBao.ToKhai);

            #region Header
            ToKhai tokhai = new ToKhai()
            {
                Issuer = isToKhaiNhap ? DeclarationIssuer.SXXK_TOKHAI_NHAP : DeclarationIssuer.SXXK_TOKHAI_XUAT,
                Reference = tkmd.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                // Số tiếp nhận
                CustomsReference = isToKhaiSua ? Helpers.FormatNumeric(tkmd.SoTiepNhan) : string.Empty,
                //Ngày dang ký chứng thư
                Acceptance = isToKhaiSua ? tkmd.NgayDangKy.ToString(sfmtDateTime) : string.Empty,
                // Ðon vị hải quan khai báo
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkmd.MaHaiQuan.Trim()).Trim() : tkmd.MaHaiQuan.Trim(),
                //tkmd.MaHaiQuan.Trim(),
                // Số hàng
                GoodsItem = Helpers.FormatNumeric(tkmd.SoHang),
                LoadingList = Helpers.FormatNumeric(tkmd.SoLuongPLTK),

                // Khối luợng và khối luợng tịnh
                TotalGrossMass = Helpers.FormatNumeric(tkmd.TrongLuong, 3),
                TotalNetGrossMass = Helpers.FormatNumeric(tkmd.TrongLuongNet, 3),
                // Mã Loại Hình
                NatureOfTransaction = tkmd.MaLoaiHinh.Contains("V") ? tkmd.MaLoaiHinh.Substring(2, 3) : tkmd.MaLoaiHinh,
                // Phuong thức thanh toán
                PaymentMethod = tkmd.PTTT_ID,



                #region Install element
                Agents = new List<Agent>(),

                //Nguyên tệ
                CurrencyExchange = new CurrencyExchange { CurrencyType = tkmd.NguyenTe_ID, Rate = Helpers.FormatNumeric(tkmd.TyGiaTinhThue, 3) },

                //Số kiện
                DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tkmd.SoKien) },
                //Neu có day du 1 trong 3 chứng từ :  Giấy phép, Hợp đồng, Vận đơn
                /* AdditionalDocuments = new List<AdditionalDocument>(),*/

                // Hóa Ðon thuong mại
                Invoice = new Invoice { Issue = tkmd.NgayHoaDonThuongMai.Year <= 1900 ? string.Empty : tkmd.NgayHoaDonThuongMai.ToString(sfmtDate), Reference = tkmd.SoHoaDonThuongMai, Type = AdditionalDocumentType.HOA_DON_THUONG_MAI },


                //Minhnd16/04/2015
                //// Doanh Nghiệp Xuất khẩu
                //Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() } :
                //new NameBase { Name = tkmd.TenDonViDoiTac, Identity = "." },

                ////Doanh nghiệp nhập khẩu
                //Importer = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = "." } :
                //new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() },

                //Minhnd16/04/2015



                // Doanh Nghiệp Xuất khẩu
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty } :
                new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() },


                //Doanh nghiệp nhập khẩu
                Importer = isToKhaiNhap ? new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() } :
                new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty },

                // Người đại diện doanh nghiệp | Tên chủ hàng
                RepresentativePerson = new RepresentativePerson { Name = tkmd.TenChuHang, ContactFunction = tkmd.ChucVu },

                // Đề xuất khác | Mã loại thông tin - Ghi chú khác
                AdditionalInformations = new List<AdditionalInformation>(),

                // GoodsShipmet Thông tin hàng hóa
                GoodsShipment = new GoodsShipment(),

                /* License = new List<License>(),
                 ContractDocument = new List<ContractDocument>(),
                 CommercialInvoices = new List<CommercialInvoice>(),
                 CertificateOfOrigins = new List<CertificateOfOrigin>(),
                 CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
                 AttachDocumentItem = new List<AttachDocumentItem>(),
                 AdditionalDocumentEx = new List<AdditionalDocument>(),*/

                #region Ân hạng thuế , Đảm bảo nghĩa vụ nộp thuế va` bao lanh thue
                TaxGrace = tkmd.AnHanThue.IsAnHan ? new Company.KDT.SHARE.Components.Messages.TaxGrace()
                {
                    IsGrace = "1",
                    Reason = tkmd.AnHanThue.LyDoAnHan,
                    Value = Helpers.FormatNumeric(tkmd.AnHanThue.SoNgay, 0)
                } : null,

                TaxGuarantee = tkmd.DamBaoNghiaVuNopThue.IsDamBao ? new Company.KDT.SHARE.Components.Messages.TaxGuarantee()
                {
                    IsGuarantee = "1",
                    Type = tkmd.DamBaoNghiaVuNopThue.HinhThuc,
                    Value = Helpers.FormatNumeric(tkmd.DamBaoNghiaVuNopThue.TriGiaDB, Globals.TriGiaNT),
                    Issue = tkmd.DamBaoNghiaVuNopThue.NgayBatDau.ToString(sfmtDate),
                    Expire = tkmd.DamBaoNghiaVuNopThue.NgayKetThuc.ToString(sfmtDate)
                } : null,

                TaxGuaranteeAgent = tkmd.BaoLanhThue != null ? new TaxGuaranteeAgent()
                {
                    IdentityGuarantor = tkmd.BaoLanhThue.DonViBaoLanh,
                    Reference = tkmd.BaoLanhThue.SoGiayBaoLanh,
                    Year = tkmd.BaoLanhThue.NamChungTuBaoLanh.ToString("N0"),
                    Issue = tkmd.BaoLanhThue.NgayHieuLuc.ToString(sfmtDate),
                    Type = tkmd.BaoLanhThue.LoaiBaoLanh,
                    Value = Helpers.FormatNumeric(tkmd.BaoLanhThue.SoTienBaoLanh, 4),
                    RemainValue = Helpers.FormatNumeric(tkmd.BaoLanhThue.SoDuTienBaoLanh, 4),
                    DayValue = tkmd.BaoLanhThue.SoNgayDuocBaoLanh.ToString("N0"),
                    Expire = tkmd.BaoLanhThue.NgayHetHieuLuc.ToString(sfmtDate),
                    SignDate = tkmd.BaoLanhThue.NgayKyBaoLanh.ToString(sfmtDate)
                } : null,
                #endregion

                #region  Số container của tờ khai
                TransportEquipment = new QuantityContainer
                {
                    Quantity20 = "0",
                    Quantity40 = "0",
                    Quantity45 = "0",
                    QuantityOthers = "0"
                },


                #endregion

                //TemporaryImportExpire = new TemporaryImportExpire {Statement = "" , Content = "" },

                #endregion Nrr

            };
            #region Neu khai to khai Thu cong
            if (Company.KDT.SHARE.Components.Globals.IsKTX)
            {
                Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkdk = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                tkdk.LoadBy(tkmd.MaHaiQuan, tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.NamDK);
                tokhai.CustomsReferenceManual = tkmd.ChiTietDonViDoiTac;
                tokhai.Clearance = tkdk.NGAY_THN_THX.ToString(sfmtDate);
                tokhai.Channel = tkmd.PhanLuong;
                tokhai.Acceptance = tkmd.NgayDangKy.ToString(sfmtDateTime);
            }
            else if (Company.KDT.SHARE.Components.Globals.IsKhaiTK)
            {
                Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkdk = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                tkdk.LoadBy(tkmd.MaHaiQuan, tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.NamDK);
                tokhai.Clearance = tkdk.NGAY_THN_THX.ToString(sfmtDate);
                tokhai.Channel = tkmd.PhanLuong;
                tokhai.Acceptance = tkmd.NgayDangKy.ToString(sfmtDateTime);
            }
            if (tkmd.TenDonViDoiTac.Contains("/"))
            {
                if (isToKhaiNhap)
                {
                    //tokhai.Importer.Identity = tkmd.TenDonViDoiTac.Split('/')[0].ToString();
                    //tokhai.Importer.Name = tkmd.TenDonViDoiTac.Split('/')[1].ToString();
                    //tokhai.Exporter.Identity = tkmd.MaDoanhNghiep.ToString();
                    //tokhai.Exporter.Name = tkmd.TenDoanhNghiep.ToString();
                    //minhnd
                    //fix sai mã tên nhập xuât
                    tokhai.Exporter.Identity = tkmd.TenDonViDoiTac.Split('/')[0].ToString();
                    tokhai.Exporter.Name = tkmd.TenDonViDoiTac.Split('/')[1].ToString();
                    tokhai.Importer.Identity = tkmd.MaDoanhNghiep.ToString();
                    tokhai.Importer.Name = tkmd.TenDoanhNghiep.ToString();
                }
                else
                {
                    tokhai.Exporter.Identity = tkmd.TenDonViDoiTac.Split('/')[0].ToString();
                    tokhai.Exporter.Name = tkmd.TenDonViDoiTac.Split('/')[1].ToString();
                    tokhai.Importer.Identity = tkmd.MaDoanhNghiep.ToString();
                    tokhai.Importer.Name = tkmd.TenDoanhNghiep.ToString();
                }
            }

            #endregion

            #region fill  Số container của tờ khai (nếu có)
            //if (tkmd.VanTaiDon != null && tkmd.VanTaiDon.ContainerCollection != null && tkmd.VanTaiDon.ContainerCollection.Count > 0)
            //{
            //    int cont20 = 0;
            //    int cont40 = 0;
            //    int cont45 = 0;
            //    int contOther = 0;
            //    foreach (Container cont in tkmd.VanTaiDon.ContainerCollection)
            //    {
            //        switch (cont.LoaiContainer)
            //        {
            //            case "2":
            //                cont20++;
            //                break;
            //            case "4":
            //                cont40++;
            //                break;
            //            case "45":
            //                cont45++;
            //                break;
            //            case "0":
            //                contOther++;
            //                break;
            //            default:
            //                break;
            //        }
            //    }
            if (tkmd.SoLuongContainer != null)
            {
                tokhai.TransportEquipment.Quantity20 = Helpers.FormatNumeric(tkmd.SoLuongContainer.Cont20, 0);
                tokhai.TransportEquipment.Quantity40 = Helpers.FormatNumeric(tkmd.SoLuongContainer.Cont40, 0);
                tokhai.TransportEquipment.Quantity45 = Helpers.FormatNumeric(tkmd.SoLuongContainer.Cont45, 0);
                tokhai.TransportEquipment.QuantityOthers = Helpers.FormatNumeric(tkmd.SoLuongContainer.ContKhac, 0);
            };
            //}



            #endregion

            tokhai.AdditionalInformations.Add(
                new AdditionalInformation
                {
                    Statement = "001",
                    Content = new Content() { Text = tkmd.DeXuatKhac }
                }
                );
            if (isToKhaiSua)
                tokhai.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Statement = "005",
                    Content = new Content() { Text = tkmd.LyDoSua }
                });


            #endregion Header
            #region Neu khai to khai Thu cong va dua vao TK
            if (Company.KDT.SHARE.Components.Globals.IsKTX)
            {
                if (Company.KDT.SHARE.Components.Globals.IsKhaiTK)
                {
                    BLL.SXXK.ToKhai.ToKhaiMauDich tkdk = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkdk.SoToKhai = tkmd.SoToKhai;
                    tkdk.MaHaiQuan = tkmd.MaHaiQuan;
                    tkdk.NamDangKy = Convert.ToInt16(tkmd.NgayDangKy.Year);
                    tkdk.MaLoaiHinh = tkmd.MaLoaiHinh;
                    tkdk.Load();
                    if (tkmd.MaLoaiHinh.Contains("V"))
                        tokhai.NatureOfTransaction = tkmd.MaLoaiHinh.Substring(2, 3);
                    tokhai.CustomsReference = tkmd.ChiTietDonViDoiTac;
                    tokhai.DeclarationOfficeLiquidity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkmd.MaHaiQuan.Trim());
                    tokhai.Clearance = tkdk.NgayHoanThanh.ToString(sfmtDate);//tkmd.NgayGiayPhep.ToString(sfmtDate);
                    tokhai.Channel = tkmd.PhanLuong;
                    tokhai.Acceptance = tkmd.NgayDangKy.ToString(sfmtDateTime);

                }
                else
                {
                    tokhai.CustomsReferenceManual = tkmd.ChiTietDonViDoiTac;
                    tokhai.Clearance = tkmd.NgayGiayPhep.ToString(sfmtDate);
                    tokhai.Channel = tkmd.PhanLuong;
                    tokhai.Acceptance = tkmd.NgayDangKy.ToString(sfmtDateTime);
                }
            }
            #endregion
            #region Agents Đại lý khai
            tokhai.Agents = AgentsFrom(tkmd);
            #endregion

            #region AdditionalDocument Hợp đồng - Giấy phép - Vận đơn ngoài tờ khai
            bool isDocument = !string.IsNullOrEmpty(tkmd.SoHopDong) ||
                !string.IsNullOrEmpty(tkmd.SoVanDon) || !string.IsNullOrEmpty(tkmd.SoGiayPhep);
            if (isDocument) tokhai.AdditionalDocuments = new List<AdditionalDocument>();
            // Add AdditionalDocument (thêm hợp đồng)
            if (!string.IsNullOrEmpty(tkmd.SoHopDong))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayHopDong.Year > 1900 ? tkmd.NgayHopDong.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoHopDong,
                    Type = AdditionalDocumentType.HOP_DONG,
                    Name = "Hop dong",
                    Expire = tkmd.NgayHetHanHopDong.Year > 1900 ? tkmd.NgayHetHanHopDong.ToString(sfmtDate) : string.Empty
                });
            // Thêm vận đơn
            if (isToKhaiNhap && !string.IsNullOrEmpty(tkmd.SoVanDon))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayVanDon.Year > 1900 ? tkmd.NgayVanDon.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoVanDon,
                    Type = AdditionalDocumentType.BILL_OF_LADING_ORIGIN,
                    Name = "Bill of lading"
                });
            // Thêm giấy phép
            if (!string.IsNullOrEmpty(tkmd.SoGiayPhep))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayGiayPhep.Year > 1900 ? tkmd.NgayGiayPhep.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoGiayPhep,
                    Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                    Name = "Giay phep",
                    Expire = tkmd.NgayHetHanGiayPhep.Year > 1900 ? tkmd.NgayHetHanGiayPhep.ToString(sfmtDate) : string.Empty
                });

            #endregion


            #region GoodsShipment thông tin về hàng hóa
            tokhai.GoodsShipment = new GoodsShipment()
            {
                ImportationCountry = isToKhaiNhap ? null : tkmd.NuocNK_ID.Substring(0, 2),
                ExportationCountry = isToKhaiNhap ? tkmd.NuocXK_ID.Substring(0, 2) : null,
                Consignor = new NameBase { Identity = string.Empty, Name = string.Empty },
                Consignee = new NameBase { Identity = string.Empty, Name = string.Empty },
                NotifyParty = new NameBase { Identity = string.Empty, Name = string.Empty },
                DeliveryDestination = new DeliveryDestination { Line = string.Empty },
                EntryCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? tkmd.CuaKhau_ID : string.Empty, Name = isToKhaiNhap ? Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) : string.Empty },
                ExitCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? string.Empty : tkmd.CuaKhau_ID, Name = isToKhaiNhap ? string.Empty : Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) },
                Importer = isToKhaiNhap ? null : new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty },
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty } : null,
                TradeTerm = new TradeTerm { Condition = tkmd.DKGH_ID },
                //CustomsGoodsItems = new List<CustomsGoodsItem>()
            };
            #endregion GoodsShipment

            #region CustomGoodsItem Danh sách hàng khai báo

            //Add CustomGoodsItem
            int soluonghang = 0;
            decimal TongTriGiaHang = 0;
            decimal TongSoLuongHang = 0;
            if (tkmd.HMDCollection != null)
            {
                soluonghang = tkmd.HMDCollection.Count;
                foreach (HangMauDich hmd in tkmd.HMDCollection)
                {
                    TongTriGiaHang += hmd.TriGiaKB;
                    TongSoLuongHang += hmd.SoLuong;
                }
                tokhai.GoodsShipment.CustomsGoodsItems = new List<CustomsGoodsItem>();
            }
            for (int i = 0; i < soluonghang; i++)
            {
                HangMauDich hmd = tkmd.HMDCollection[i];

                #region CustomsGoodsItem
                CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem
                {

                    CustomsValue = Helpers.FormatNumeric(hmd.TriGiaKB, GlobalsShare.TriGiaNT),
                    Sequence = Helpers.FormatNumeric(hmd.SoThuTuHang),
                    StatisticalValue = Helpers.FormatNumeric(hmd.TriGiaTT),
                    UnitPrice = Helpers.FormatNumeric(hmd.DonGiaKB, GlobalsShare.DonGiaNT),
                    StatisticalUnitPrice = Helpers.FormatNumeric(hmd.DonGiaTT, GlobalsShare.DonGiaNT),
                    Manufacturer = new NameBase { Name = hmd.TenHangSX/*Tên hãng sx*/, Identity = hmd.MaHangSX/*Mã hãng sx*/ },
                    Origin = new Origin { OriginCountry = hmd.NuocXX_ID.Substring(0, 2) },
                    GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hmd.SoLuong, 4), MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hmd.DVT_ID) : hmd.DVT_ID /*hmd.DVT_ID */}
                };

                //Trị giá trên mỗi dòng hàng (Thường thì chia đều cho tất cả dòng hàng) nếu có 1 dòng hàng thì tính tổng
                //Nếu có nhiều hơn 1 dòng hàng thì chia đều ra cho mỗi dòng hàng
                // ExitToEntryCharge Tổng chi phí bằng (tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem)/số kiện hàng
                //FreightCharge chi phí vận tải cũng chia đôi ra
                //Metho = số thứ tự kiện hàng.  
                decimal dTongPhi = tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem;
                decimal Phi = (dTongPhi / TongTriGiaHang) * hmd.TriGiaKB;
                decimal dPhiVanChuyen = (tkmd.PhiVanChuyen / TongTriGiaHang) * hmd.TriGiaKB; ;
                customsGoodsItem.CustomsValuation = new CustomsValuation
                {
                    ExitToEntryCharge = Helpers.FormatNumeric(Phi, 4),
                    FreightCharge = Helpers.FormatNumeric(dPhiVanChuyen, 4),
                    Method = string.Empty,
                    OtherChargeDeduction = "0"
                };

                #endregion

                #region Commodity Hàng trong tờ khai
                Commodity commodity = new Commodity
                {
                    Description = hmd.TenHang,
                    Identification = hmd.MaPhu,
                    TariffClassification = hmd.MaHS.Trim(),
                    TariffClassificationExtension = hmd.MaHSMoRong,
                    Brand = hmd.NhanHieu,
                    Grade = hmd.QuyCachPhamChat,
                    Ingredients = hmd.ThanhPhan,
                    ModelNumber = hmd.Model,
                    DutyTaxFee = new List<DutyTaxFee>(),
                    IsNew = hmd.isHangCu ? "0" : "1",
                    isIntegrate = hmd.IsHangDongBo ? "1" : "0",
                    DutyPreference = hmd.CheDoUuDai,
                    RegisterCustoms = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkmd.MaHaiQuan.Trim()).Trim() : tkmd.MaHaiQuan.Trim(),
                    //Hệ thống hải quan chưa sử dụng

                    InvoiceLine = new InvoiceLine { ItemCharge = string.Empty, Line = string.Empty }
                };
                if (tkmd.LoaiHangHoa == "N")
                    commodity.Type = "1";
                else if (tkmd.LoaiHangHoa == "S")
                    commodity.Type = "2";
                else if (tkmd.LoaiHangHoa == "T")
                    commodity.Type = "3";
                #region DutyTaxFee tiền thuế của hàng
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 2),
                    DutyRegime = hmd.BieuThueXNK,
                    SpecificTaxBase = string.Empty,
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatXNK, 2),
                    AbsoluteTax = tkmd.HMDCollection[i].ThueTuyetDoi ? Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 4) : string.Empty,
                    Type = DutyTaxFeeType.THUE_XNK
                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueGTGT, 2),
                    DutyRegime = hmd.BieuThueGTGT,
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatGTGT, 2),
                    Type = DutyTaxFeeType.THUE_VAT,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueTTDB, 2),
                    DutyRegime = hmd.BieuThueTTDB,
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatTTDB, 2),
                    Type = DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].TriGiaThuKhac, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].TyLeThuKhac, 2),
                    Type = DutyTaxFeeType.THUE_KHAC,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(0, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(0, 2),
                    Type = DutyTaxFeeType.THUE_CHENH_LECH_GIA,

                });

                #region Thuế bảo vệ môi trường và thuế chống bán phá giá
                if (tkmd.HMDCollection[i].ThueBVMT > 0)
                {

                    commodity.DutyTaxFee.Add(new DutyTaxFee
                    {
                        AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueBVMT, 2),
                        DutyRegime = hmd.BieuThueBVMT,
                        SpecificTaxBase = "",
                        Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatBVMT, 4),
                        Type = DutyTaxFeeType.THUE_BAO_VE_MOI_TRUONG,

                    });
                }
                if (tkmd.HMDCollection[i].ThueChongPhaGia > 0)
                {
                    commodity.DutyTaxFee.Add(new DutyTaxFee
                    {
                        AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueChongPhaGia, 2),
                        DutyRegime = hmd.BieuThueCBPG,
                        SpecificTaxBase = "",
                        Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatChongPhaGia, 4),
                        Type = DutyTaxFeeType.THUE_CHONG_BAN_PHA_GIA,

                    });
                }
                #endregion

                #region Phí hải quan(nếu có)
                tkmd.LoadListLePhiHQ();
                foreach (LePhiHQ lephi in tkmd.LePhiHQCollection)
                {
                    commodity.DutyTaxFee.Add(new DutyTaxFee
                    {
                        AdValoremTaxBase = Helpers.FormatNumeric(lephi.SoTienLePhi, 2),
                        DutyRegime = "",
                        SpecificTaxBase = "",
                        Tax = Helpers.FormatNumeric(0, 4),
                        Type = lephi.MaLePhi,

                    });
                }
                #endregion

                #endregion DutyTaxFee thuế

                customsGoodsItem.Commodity = commodity;
                #endregion Hàng chính

                #region AdditionalDocument if Search(Giấy phép có hàng trong tờ khai)
                //Lấy thông tin giấy phép đầu tiên
                if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = tkmd.GiayPhepCollection[0];
                    customsGoodsItem.AdditionalDocument = new AdditionalDocument()
                    {
                        Issue = giayphep.NgayGiayPhep.ToString(sfmtDate), /*Ngày cấp giấy phép yyyy-MM-dd*/
                        Issuer = giayphep.NguoiCap/*Người cấp*/,
                        IssueLocation = giayphep.NoiCap/*Nơi cấp*/,
                        Reference = giayphep.SoGiayPhep/*Số giấy phép*/,
                        Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                        Name = "GP"/* Tên giấy phép*/,
                        Expire = giayphep.NgayHetHan.ToString(sfmtDate)
                    };
                }
                #endregion

                #region CertificateOfOrigin if Search(Co có hàng trong tờ khai)

                if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.CO co = tkmd.COCollection[tkmd.COCollection.Count - 1];
                    customsGoodsItem.CertificateOfOrigin = new Company.KDT.SHARE.Components.CertificateOfOrigin()
                    {
                        Issue = co.NgayCO.ToString(sfmtDate),
                        Issuer = co.ToChucCap,
                        IssueLocation = co.NuocCapCO,
                        Reference = co.SoCO,
                        Type = co.LoaiCO,
                        Name = "CO",
                        Expire = co.NgayHetHan.ToString(sfmtDate),
                        Exporter = co.TenDiaChiNguoiXK,
                        Importer = co.TenDiaChiNguoiNK,
                        ExportationCountry = co.MaNuocXKTrenCO,
                        ImportationCountry = co.MaNuocNKTrenCO,
                        Content = co.ThongTinMoTaChiTiet,
                        IsDebt = Helpers.FormatNumeric(co.NoCo, 0),
                        Submit = co.ThoiHanNop.ToString(sfmtDate)
                    };
                }
                #endregion

                #region Văn bản miễn thuế
                if (hmd.MienGiamThueCollection != null && hmd.MienGiamThueCollection.Count > 0)
                {
                    ReduceTax miengiam = new ReduceTax
                    {
                        Reference = hmd.MienGiamThueCollection[0].SoVanBanMienGiam,
                        tax = Helpers.FormatNumeric(hmd.MienGiamThueCollection[0].ThueSuatTruocGiam, 4),
                        redureValue = Helpers.FormatNumeric(hmd.MienGiamThueCollection[0].TyLeMienGiam, 4)
                    };
                    customsGoodsItem.ReduceTax = new List<ReduceTax>();
                    customsGoodsItem.ReduceTax.Add(miengiam);
                }
                #endregion

                tokhai.GoodsShipment.CustomsGoodsItems.Add(customsGoodsItem);
            }

            #endregion CustomGoodsItem Danh sách hàng khai báo

            #region Danh sách chứng từ  XNK đi kèm

            if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
            #region License Giấy phép
            {
                tokhai.License = new List<Company.KDT.SHARE.Components.License>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep in tkmd.GiayPhepCollection)
                {
                    Company.KDT.SHARE.Components.License lic = LicenseFrom(giayPhep);
                    tokhai.License.Add(lic);
                }
            }
            #endregion Giấy phép

            if (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0)
            #region ContractDocument  Hợp đồng thương mại
            {
                tokhai.ContractDocument = new List<Company.KDT.SHARE.Components.ContractDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai in tkmd.HopDongThuongMaiCollection)
                {
                    Company.KDT.SHARE.Components.ContractDocument contractDocument = ContractFrom(hdThuongMai);
                    tokhai.ContractDocument.Add(contractDocument);
                }
            }
            #endregion Hợp đồng thương mại

            if (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0)
            #region CommercialInvoice Hóa đơn thương mại
            {
                tokhai.CommercialInvoices = new List<Company.KDT.SHARE.Components.CommercialInvoice>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai in tkmd.HoaDonThuongMaiCollection)
                {
                    Company.KDT.SHARE.Components.CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoaDonThuongMai);
                    tokhai.CommercialInvoices.Add(commercialInvoice);
                }
            }
            #endregion Hóa đơn thương mại

            if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
            #region CertificateOfOrigin Thêm CO
            {
                tokhai.CertificateOfOrigins = new List<Company.KDT.SHARE.Components.CertificateOfOrigin>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.CO co in tkmd.COCollection)
                {
                    tokhai.CertificateOfOrigins.Add(CertificateOfOriginFrom(co, tkmd, isToKhaiNhap));
                }
            }
            #endregion CO


            if (tkmd.VanTaiDon != null)
            #region BillOfLadings Vận đơn
            {

                tokhai.BillOfLadings = new List<Company.KDT.SHARE.Components.BillOfLading>();
                Company.KDT.SHARE.QuanLyChungTu.VanDon vandon = tkmd.VanTaiDon;
                Company.KDT.SHARE.Components.BillOfLading billOfLading;
                if (isToKhaiNhap)
                {
                    billOfLading = new Company.KDT.SHARE.Components.BillOfLading()
                    {
                        Reference = vandon.SoVanDon,
                        Issue = vandon.NgayVanDon.ToString(sfmtDate),
                        IssueLocation = vandon.ID_NuocPhatHanh,
                        IsContainer = vandon.HangRoi ? "0" : "1",
                        TransitLocation = vandon.DiaDiemChuyenTai,
                        Category = vandon.LoaiVanDon,
                    };
                    billOfLading.BorderTransportMeans = new BorderTransportMeans()
                    {
                        Identity = vandon.SoHieuPTVT,
                        Identification = vandon.TenPTVT,
                        Journey = (vandon.LoaiVanDon == LoaiVanDon.DUONG_KHONG || vandon.LoaiVanDon == LoaiVanDon.DUONG_BIEN) ? vandon.SoHieuChuyenDi : null,
                        ModeAndType = tkmd.PTVT_ID,
                        Departure = vandon.NgayKhoiHanh.ToString(sfmtDate),
                        RegistrationNationality = (vandon.LoaiVanDon == LoaiVanDon.DUONG_KHONG && string.IsNullOrEmpty(vandon.QuocTichPTVT)) ? vandon.QuocTichPTVT.Substring(0, 2) : string.Empty
                    };
                    billOfLading.Carrier = (vandon.LoaiVanDon == LoaiVanDon.DUONG_KHONG || vandon.LoaiVanDon == LoaiVanDon.DUONG_BIEN) ? new NameBase()
                    {
                        Name = vandon.TenHangVT,
                        Identity = vandon.MaHangVT
                    } : null;
                    billOfLading.Consignment = new Consignment()
                    {
                        Consignor = new NameBase()
                        {
                            Name = vandon.TenNguoiGiaoHang,
                            Identity = vandon.MaNguoiGiaoHang
                        },
                        Consignee = new NameBase()
                        {
                            Name = vandon.TenNguoiNhanHang,
                            Identity = vandon.MaNguoiNhanHang
                        },
                        NotifyParty = (vandon.LoaiVanDon == LoaiVanDon.DUONG_BO) ? null : new NameBase()
                        {
                            Name = vandon.TenNguoiNhanHangTrungGian,
                            Identity = vandon.MaNguoiNhanHangTrungGian
                        },
                        LoadingLocation = new Company.KDT.SHARE.Components.LoadingLocation()
                        {
                            Name = vandon.TenCangXepHang,
                            Code = vandon.MaCangXepHang,
                            Loading = (vandon.LoaiVanDon == LoaiVanDon.DUONG_BO) ? string.Empty : vandon.NgayKhoiHanh.ToString(sfmtDate)
                        },
                        UnloadingLocation = new UnloadingLocation()
                        {
                            Name = vandon.TenCangDoHang,
                            Code = vandon.MaCangDoHang,
                            Arrival = vandon.NgayDenPTVT.ToString(sfmtDate)
                        },
                        DeliveryDestination = new DeliveryDestination() { Line = vandon.DiaDiemGiaoHang },
                        ConsignmentItemPackaging = new Packaging()
                        {
                            Quantity = Helpers.FormatNumeric(vandon.TongSoKien),
                            Type = vandon.LoaiKien,
                            MarkNumber = string.Empty
                        }
                    };
                }
                else
                {
                    billOfLading = new Company.KDT.SHARE.Components.BillOfLading()
                    {
                        Reference = string.Empty,
                        Issue = string.Empty,
                        IssueLocation = string.Empty,
                        IsContainer = vandon.HangRoi ? "0" : "1",
                        TransitLocation = string.Empty,
                        Category = vandon.LoaiVanDon,
                    };
                    billOfLading.BorderTransportMeans = new BorderTransportMeans()
                    {
                        Identity = string.Empty,
                        Identification = string.Empty,
                        Journey = string.Empty,
                        ModeAndType = string.Empty,
                        Departure = string.Empty,
                        RegistrationNationality = string.Empty
                    };
                    billOfLading.Carrier = new NameBase()
                    {
                        Name = string.Empty,
                        Identity = string.Empty
                    };
                    billOfLading.Consignment = new Consignment()
                    {
                        Consignor = new NameBase()
                        {
                            Name = string.Empty,
                            Identity = string.Empty
                        },
                        Consignee = new NameBase()
                        {
                            Name = string.Empty,
                            Identity = string.Empty
                        },
                        NotifyParty = (vandon.LoaiVanDon != LoaiVanDon.DUONG_KHONG) ? null : new NameBase()
                        {
                            Name = string.Empty,
                            Identity = string.Empty
                        },
                        LoadingLocation = new Company.KDT.SHARE.Components.LoadingLocation()
                        {
                            Name = string.Empty,
                            Code = string.Empty,
                            Loading = string.Empty
                        },
                        UnloadingLocation = new UnloadingLocation()
                        {
                            Name = string.Empty,
                            Code = string.Empty,
                            Arrival = string.Empty
                        },
                        DeliveryDestination = new DeliveryDestination() { Line = vandon.DiaDiemGiaoHang },
                        ConsignmentItemPackaging = new Packaging()
                        {
                            Quantity = Helpers.FormatNumeric(vandon.TongSoKien),
                            Type = vandon.LoaiKien,
                            MarkNumber = string.Empty
                        }
                    };
                }
                #region Đổ dữ liệu bên ngoài GoodsShipment
                tokhai.GoodsShipment.Consignor = billOfLading.Consignment.Consignor;
                tokhai.GoodsShipment.Consignee = billOfLading.Consignment.Consignee;
                tokhai.GoodsShipment.NotifyParty = billOfLading.Consignment.NotifyParty;
                tokhai.GoodsShipment.DeliveryDestination = billOfLading.Consignment.DeliveryDestination;
                tokhai.GoodsShipment.ExitCustomsOffice.Name = vandon.CuaKhauXuat;
                #endregion
                if (vandon.ContainerCollection != null && vandon.ContainerCollection.Count > 0)
                    billOfLading.Consignment.TransportEquipments = new List<TransportEquipment>();

                foreach (Company.KDT.SHARE.QuanLyChungTu.Container container in vandon.ContainerCollection)
                {

                    TransportEquipment transportEquipment = new TransportEquipment()
                    {
                        Characteristic = container.LoaiContainer,
                        EquipmentIdentifications = new EquipmentIdentification()
                        {
                            identification = container.SoHieu
                        },
                        Fullness = Helpers.FormatNumeric(container.Trang_thai, 0),
                        Seal = container.Seal_No,
                        GrossMass = Helpers.FormatNumeric(container.TrongLuong, 4),
                        NetMass = Helpers.FormatNumeric(container.TrongLuongNet, 4),
                        Quantity = Helpers.FormatNumeric(container.SoKien, 2),
                        PackagingLocation = container.DiaDiemDongHang
                    };

                    billOfLading.Consignment.TransportEquipments.Add(transportEquipment);
                }

                #region Hàng  trong vận đơn
                if (vandon.ListHangOfVanDon != null && vandon.ListHangOfVanDon.Count > 0)
                {
                    List<ConsignmentItem> hangsVanDon = new List<ConsignmentItem>();

                    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail hangVanDon in vandon.ListHangOfVanDon)
                    {
                        HangMauDich hangToKhai = tkmd.HMDCollection.Find(hang => hang.ID == hangVanDon.HMD_ID);
                        if (hangToKhai != null)
                            hangsVanDon.Add(new ConsignmentItem()
                            {
                                Sequence = Helpers.FormatNumeric(hangVanDon.SoThuTuHang),
                                ConsignmentItemPackaging = new Packaging()
                                {
                                    Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong),
                                    Type = hangVanDon.LoaiKien,
                                    //MarkNumber = hangVanDon.SoHieuKien,
                                },
                                Commodity = new Commodity()
                                {
                                    Description = hangVanDon.TenHang,
                                    Identification = hangToKhai.Ma,//Mã hàng
                                    TariffClassification = hangVanDon.MaHS.Trim()
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    GrossMass = Helpers.FormatNumeric(hangVanDon.TrongLuong, 4),
                                    Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong, 4),
                                    MeasureUnit = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangVanDon.DVT_ID.Substring(0, 3)) : hangVanDon.DVT_ID.Substring(0, 3), //hangVanDon.DVT_ID.Substring(0, 3),
                                    NetMass = Helpers.FormatNumeric(hangVanDon.TrongLuongTinh, 4),
                                    CustomsValue = Helpers.FormatNumeric(hangVanDon.TriGiaKB, 4),

                                },
                                //                                 EquipmentIdentification = new EquipmentIdentification()
                                //                                 {
                                //                                     identification = hangVanDon.SoHieuContainer
                                //                                 }

                            });

                    }
                    billOfLading.Consignment.ConsignmentItems = hangsVanDon;
                }
                #endregion Hàng và Container trong vận đơn

                tokhai.BillOfLadings.Add(billOfLading);

            }

            #endregion BillOfLadings  Vận đơn

            if (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0)
            #region CustomsOfficeChangedRequest Đề nghị chuyển cửa khẩu
            {
                tokhai.CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau in tkmd.listChuyenCuaKhau)
                {
                    CustomsOfficeChangedRequest customsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCuaKhau);
                    tokhai.CustomsOfficeChangedRequest.Add(customsOfficeChangedRequest);
                };
            }
            #endregion Đề nghị chuyển cửa khẩu

            if (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0)
            #region AttachDocumentItem Chứng từ đính kèm
            {
                tokhai.AttachDocumentItem = new List<AttachDocumentItem>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem in tkmd.ChungTuKemCollection)
                {
                    if (fileinChungtuDinhKem.LoaiKB == "0")
                    {
                        AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, fileinChungtuDinhKem);
                        tokhai.AttachDocumentItem.Add(attachDocumentItem);
                    }
                };
            }
            #endregion Chứng từ đính kèm

            if (tkmd.GiayKiemTraCollection != null && tkmd.GiayKiemTraCollection.Count > 0)
            {
                #region Giấy đăng ký kiểm tra / giấy kết quả kiểm tra

                foreach (GiayKiemTra gkt in tkmd.GiayKiemTraCollection)
                {
                    if (gkt.LoaiGiay == LoaiGiayKiemTra.DANG_KY_KIEM_TRA)
                    {
                        gkt.LoadFull();
                        tokhai.ExaminationRegistration = ExamRegistration(gkt);
                    }
                    else if (gkt.LoaiGiay == LoaiGiayKiemTra.KET_QUA_KIEM_TRA)
                    {
                        gkt.LoadFull();
                        tokhai.ExaminationResult = ExaminationResult(gkt);
                    }


                }
                #endregion
            }

            if (tkmd.ChungThuGD != null && tkmd.ChungThuGD.LoaiKB == 0 && tkmd.ChungThuGD.ListHang.Count > 0)
            {
                #region Chứng thư giám định

                CertificateOfInspection chungthu = CertificateOfInspectionFrom(tkmd.ChungThuGD);
                tokhai.CertificateOfInspection = chungthu;

                #endregion
            }


            #endregion Danh sách giấy phép XNK đi kèm

            #region AdditionalDocumentNos Thêm chứng từ nợ

            if (tkmd.ChungTuNoCollection != null && tkmd.ChungTuNoCollection.Count > 0)
            {
                tokhai.AdditionalDocumentNos = new List<AdditionalDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuNo ctn in tkmd.ChungTuNoCollection)
                {
                    tokhai.AdditionalDocumentNos.Add(new AdditionalDocument()
                    {
                        Type = ctn.MA_LOAI_CT,
                        Reference = ctn.SO_CT,
                        Issue = ctn.NGAY_CT.ToString(sfmtDate),
                        IssueLocation = ctn.NOI_CAP,
                        Issuer = ctn.TO_CHUC_CAP,
                        Expire = ctn.NgayHetHan.ToString(sfmtDate),
                        IsDebt = Helpers.FormatNumeric(ctn.IsNoChungTu),
                        Submit = ctn.ThoiHanNop.ToString(sfmtDate),
                        AdditionalInformation = new AdditionalInformation()
                        {
                            Content = new Content() { Text = ctn.DIENGIAI }
                        }

                    });
                }

            }
            #endregion
            return tokhai;
        }
        #region Data Mapper
        private static List<Agent> AgentsFrom(ToKhaiMauDich tkmd)
        {
            List<Agent> Agents = new List<Agent>();
            // Edit by Khanhhn - 12/06/2012
            #region Agents Đại lý khai
            // Người khai hải quan
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Đại lý khai Hải Quan
            if (!string.IsNullOrEmpty(tkmd.TenDaiLyTTHQ))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDaiLyTTHQ,
                    Identity = tkmd.MaDaiLyTTHQ,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN
                });
            // ủy Thác
            if (!string.IsNullOrEmpty(tkmd.MaDonViUT))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDonViUT,
                    Identity = tkmd.MaDonViUT,
                    Status = AgentsStatus.UYTHAC,
                });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            #endregion
            return Agents;
        }
        private static Company.KDT.SHARE.Components.CertificateOfOrigin CertificateOfOriginFrom(Company.KDT.SHARE.QuanLyChungTu.CO co, ToKhaiMauDich tkmd, bool isToKhaiNhap)
        {
            #region Điền thông tin CO
            Company.KDT.SHARE.Components.CertificateOfOrigin certificateOfOrigin = new Company.KDT.SHARE.Components.CertificateOfOrigin
            {


                Reference = co.SoCO,
                Type = co.LoaiCO,
                Issuer = co.ToChucCap,
                Issue = co.NgayCO.ToString(sfmtDate),
                IssueLocation = co.NuocCapCO,
                Representative = co.NguoiKy,

                ExporterEx = isToKhaiNhap ? new NameBase() { Name = co.TenDiaChiNguoiXK, Identity = string.Empty } : new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep },
                ExportationCountryEx = new LocationNameBase { Code = co.MaNuocXKTrenCO, Name = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(co.MaNuocXKTrenCO) },

                ImporterEx = isToKhaiNhap ? new NameBase() { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep } :
                            new NameBase { Name = co.TenDiaChiNguoiNK, Identity = string.Empty },
                ImportationCountryEx = new LocationNameBase { Code = co.MaNuocNKTrenCO, Name = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(co.MaNuocNKTrenCO) },

                //LoadingLocation = new LoadingLocation { Name = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangXepHang.Trim()), Code = co.CangXepHang.Trim(), Loading = co.NgayKhoiHanh.ToString(sfmtDate) },
                //LoadingLocation = new LoadingLocation
                //{
                //    //Neu la TK Nhap, chi can lay ten Cang xep hang, khong lay thong tin tu ma
                //    Name = tkmd.MaLoaiHinh.Substring(0, 1) == "N" ? co.CangXepHang.Trim() : Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangXepHang.Trim()),
                //    Code = co.CangXepHang.Trim(),
                //    Loading = co.NgayKhoiHanh.ToString(sfmtDate)
                //},

                //UnloadingLocation = new UnloadingLocation { Name = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangDoHang.Trim()), Code = co.CangDoHang.Trim() },
                //UnloadingLocation = new UnloadingLocation
                //{
                //    //Neu la TK Xuat, chi can lay ten Cang do hang, khong lay thong tin tu ma
                //    Name = tkmd.MaLoaiHinh.Substring(0, 1) == "X" ? co.TenCangDoHang.Trim() : Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangDoHang.Trim()),
                //    Code = co.CangDoHang.Trim()
                //},

                IsDebt = Helpers.FormatNumeric(co.NoCo),

                Submit = co.ThoiHanNop.ToString(sfmtDate),

                Description = co.MoTaHangHoa,
                PercentOrigin = Helpers.FormatNumeric(co.HamLuongXuatXu, 4),

                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = co.ThongTinMoTaChiTiet } },
                //Hang in Co
                GoodsItems = new List<GoodsItem>(),
            };
            #endregion CO
            #region Điền hàng trong Co

            //             foreach (Company.KDT.SHARE.QuanLyChungTu.HangCoDetail hangCo in co.ListHMDofCo)
            //             {
            //                 GoodsItem goodsItem = new GoodsItem();
            //                 goodsItem.Sequence = Helpers.FormatNumeric(hangCo.SoThuTuHang);
            //                 goodsItem.StatisticalValue = Helpers.FormatNumeric(hangCo.TriGiaKB, GlobalsShare.TriGiaNT);
            //                 goodsItem.CurrencyExchange = new CurrencyExchange()
            //                 {
            //                     CurrencyType = hangCo.MaNguyenTe
            //                 };
            //                 goodsItem.ConsignmentItemPackaging = new Packaging()
            //                 {
            //                     Quantity = Helpers.FormatNumeric(hangCo.SoLuong),
            //                     Type = hangCo.LoaiKien,
            //                     MarkNumber = hangCo.SoHieuKien
            //                 };
            //                 goodsItem.Commodity = new Commodity()
            //                 {
            //                     Description = hangCo.TenHang,
            //                     Identification = hangCo.MaPhu,
            //                     TariffClassification = hangCo.MaHS.Trim()
            //                 };
            //                 goodsItem.GoodsMeasure = new GoodsMeasure()
            //                 {
            //                     GrossMass = Helpers.FormatNumeric(hangCo.TrongLuong, 3),
            //                     MeasureUnit = hangCo.DVT_ID
            //                 };
            //                 goodsItem.Origin = new Origin()
            //                 {
            //                     OriginCountry = hangCo.NuocXX_ID.Trim()
            //                 };
            //                 goodsItem.Invoice = new Invoice()
            //                 {
            //                     Reference = hangCo.SoHoaDon,
            //                     Issue = hangCo.NgayHoaDon.ToString(sfmtDate)
            //                 };
            //                 certificateOfOrigin.GoodsItems.Add(goodsItem);
            //             }
            #endregion Thêm hàng
            return certificateOfOrigin;
        }
        private static AttachDocumentItem AttachDocumentItemFrom(ToKhaiMauDich tkmd, Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem)
        {
            AttachDocumentItem attachDocumentItem = new AttachDocumentItem()
            {
                Issuer = "200",
                Sequence = Helpers.FormatNumeric(tkmd.ChungTuKemCollection.IndexOf(chungtukem) + 1, 0),
                Issue = chungtukem.NGAY_CT.ToString(sfmtDate),
                Reference = chungtukem.SO_CT,
                Description = chungtukem.DIENGIAI,
                AttachedFiles = new List<Company.KDT.SHARE.Components.AttachedFile>(),
            };

            if (chungtukem.listCTChiTiet != null && chungtukem.listCTChiTiet.Count > 0)
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet fileDetail in chungtukem.listCTChiTiet)
                {

                    attachDocumentItem.AttachedFiles.Add(new Company.KDT.SHARE.Components.AttachedFile
                    {
                        FileName = fileDetail.FileName,
                        //Content = new Content { Text = System.Convert.ToBase64String(fileDetail.NoiDung, Base64FormattingOptions.None), Base64 = "bin.base64" },
                    });
                };
            return attachDocumentItem;
        }
        private static Company.KDT.SHARE.Components.License LicenseFrom(Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep)
        {
            Company.KDT.SHARE.Components.License lic = new Company.KDT.SHARE.Components.License
            {
                Issuer = giayPhep.NguoiCap,
                Reference = giayPhep.SoGiayPhep,
                Issue = giayPhep.NgayGiayPhep.ToString(sfmtDate),
                IssueLocation = giayPhep.NoiCap,
                Type = "",
                Expire = giayPhep.NgayHetHan.ToString(sfmtDate),
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = giayPhep.ThongTinKhac } },
                GoodItems = new List<GoodsItem>()
            };

            if (giayPhep.ListHMDofGiayPhep != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail hangInGiayPhep in giayPhep.ListHMDofGiayPhep)
                {
                    lic.GoodItems.Add(new GoodsItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInGiayPhep.SoThuTuHang, 0),
                        StatisticalValue = Helpers.FormatNumeric(hangInGiayPhep.TriGiaKB, GlobalsShare.DonGiaNT),
                        CurrencyExchange = new CurrencyExchange { CurrencyType = hangInGiayPhep.MaNguyenTe },
                        Commodity = new Commodity
                        {
                            Description = hangInGiayPhep.TenHang,
                            Identification = string.Empty,
                            TariffClassification = hangInGiayPhep.MaHS.Trim()
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInGiayPhep.SoLuong, GlobalsShare.DonGiaNT), MeasureUnit = hangInGiayPhep.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInGiayPhep.GhiChu } }

                    });
                }
            return lic;
        }
        private static Company.KDT.SHARE.Components.ContractDocument ContractFrom(Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai)
        {
            Company.KDT.SHARE.Components.ContractDocument contractDocument = new Company.KDT.SHARE.Components.ContractDocument
            {
                Reference = hdThuongMai.SoHopDongTM,
                Issue = hdThuongMai.NgayHopDongTM.ToString(sfmtDate),
                Expire = hdThuongMai.ThoiHanThanhToan.ToString(sfmtDate),
                Payment = new Payment { Method = hdThuongMai.PTTT_ID },
                TradeTerm = new TradeTerm { Condition = hdThuongMai.DKGH_ID },
                DeliveryDestination = new DeliveryDestination { Line = hdThuongMai.DiaDiemGiaoHang },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hdThuongMai.NguyenTe_ID },
                TotalValue = Helpers.FormatNumeric(hdThuongMai.TongTriGia, GlobalsShare.TriGiaNT),
                Buyer = new NameBase { Name = hdThuongMai.TenDonViMua, Identity = hdThuongMai.MaDonViMua },
                Seller = new NameBase { Name = hdThuongMai.TenDonViBan, Identity = hdThuongMai.MaDonViBan },
                AdditionalInformations = new List<AdditionalInformation>(),
                ContractItems = new List<ContractItem>()
            };
            contractDocument.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = hdThuongMai.ThongTinKhac } });
            if (hdThuongMai.ListHangMDOfHopDong != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail hangInHdThuongMai in hdThuongMai.ListHangMDOfHopDong)
                    contractDocument.ContractItems.Add(new ContractItem
                    {
                        unitPrice = Helpers.FormatNumeric(hangInHdThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        statisticalValue = Helpers.FormatNumeric(hangInHdThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Commodity = new Commodity
                        {
                            Description = hangInHdThuongMai.TenHang,
                            Identification = hangInHdThuongMai.MaPhu,
                            TariffClassification = hangInHdThuongMai.MaHS.Trim(),
                        },
                        Origin = new Origin { OriginCountry = hangInHdThuongMai.NuocXX_ID.Substring(0, 2) },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHdThuongMai.SoLuong, GlobalsShare.DonGiaNT), MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangInHdThuongMai.DVT_ID.Trim()) : hangInHdThuongMai.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHdThuongMai.GhiChu } }
                    });
            return contractDocument;
        }
        /// <summary>
        /// Thông tin tham chiếu đến Tờ khai
        /// </summary>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>DeclarationBase</returns>
        private static DeclarationBase DeclarationDocument(ToKhaiMauDich tkmd)
        {
            return new DeclarationBase()
            {
                Reference = Helpers.FormatNumeric(tkmd.SoToKhai),
                Issue = tkmd.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = tkmd.MaLoaiHinh,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };

        }
        /// <summary>
        /// Đề nghị chuyển cửa khẩu
        /// </summary>
        /// <param name="dnChuyenCuaKhau"></param>
        /// <returns></returns>
        private static CustomsOfficeChangedRequest CustomsOfficeChangedRequestFrom(Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau)
        {
            CustomsOfficeChangedRequest customsOfficeChangedRequest = new CustomsOfficeChangedRequest
            {
                AdditionalDocument = new AdditionalDocument { Reference = dnChuyenCuaKhau.SoVanDon, Issue = dnChuyenCuaKhau.NgayVanDon.ToString(sfmtDate) },
                AdditionalInformation = new AdditionalInformation
                {
                    Content = new Content { Text = dnChuyenCuaKhau.ThongTinKhac },
                    ExaminationPlace = dnChuyenCuaKhau.DiaDiemKiemTra.Trim().Length > 0 ? dnChuyenCuaKhau.DiaDiemKiemTra : dnChuyenCuaKhau.DiaDiemKiemTraCucHQThanhPho,
                    Time = dnChuyenCuaKhau.ThoiGianDen.ToString(sfmtDate),
                    Route = dnChuyenCuaKhau.TuyenDuong
                },
                BorderTransportMeans = new BorderTransportMeans { ModeAndType = dnChuyenCuaKhau.PTVT_ID }
            };
            return customsOfficeChangedRequest;
        }
        private static Company.KDT.SHARE.Components.CommercialInvoice CommercialInvoiceFrom(Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai)
        {
            Company.KDT.SHARE.Components.CommercialInvoice commercialInvoice = new Company.KDT.SHARE.Components.CommercialInvoice
            {
                Reference = hoaDonThuongMai.SoHoaDon,
                Issue = hoaDonThuongMai.NgayHoaDon.ToString(sfmtDate),
                Seller = new NameBase { Name = hoaDonThuongMai.TenDonViBan, Identity = hoaDonThuongMai.MaDonViBan },
                Buyer = new NameBase { Name = hoaDonThuongMai.TenDonViMua, Identity = hoaDonThuongMai.MaDonViMua },
                AdditionalDocument = new AdditionalDocument { Reference = "", Issue = "" },
                Payment = new Payment { Method = hoaDonThuongMai.PTTT_ID.Trim() },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hoaDonThuongMai.NguyenTe_ID },
                TradeTerm = new TradeTerm { Condition = hoaDonThuongMai.DKGH_ID.Trim() },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>()
            };

            if (hoaDonThuongMai.ListHangMDOfHoaDon != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail hangInHoaDonThuongMai in hoaDonThuongMai.ListHangMDOfHoaDon)
                    commercialInvoice.CommercialInvoiceItems.Add(new CommercialInvoiceItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoThuTuHang, 0),
                        UnitPrice = Helpers.FormatNumeric(hangInHoaDonThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        StatisticalValue = Helpers.FormatNumeric(hangInHoaDonThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Origin = new Origin { OriginCountry = hangInHoaDonThuongMai.NuocXX_ID.Trim() },
                        Commodity = new Commodity
                        {
                            Description = hangInHoaDonThuongMai.TenHang,
                            Identification = hangInHoaDonThuongMai.MaPhu,
                            TariffClassification = hangInHoaDonThuongMai.MaHS.Trim()
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoLuong, GlobalsShare.DonGiaNT), MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangInHoaDonThuongMai.DVT_ID.Trim()) : hangInHoaDonThuongMai.DVT_ID },
                        ValuationAdjustment = new ValuationAdjustment
                        {
                            Addition = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaTriDieuChinhTang, GlobalsShare.TriGiaNT),
                            Deduction = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaiTriDieuChinhGiam, GlobalsShare.TriGiaNT),
                        },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHoaDonThuongMai.GhiChu } },
                    });
            return commercialInvoice;
        }

        private static ExaminationResult ExaminationResult(GiayKiemTra giaykt)
        {
            ExaminationResult Result = new ExaminationResult()
            {
                Reference = giaykt.SoGiayKiemTra,
                Issue = giaykt.NgayDangKy.ToString(sfmtDate),
                Register = new NameBase() { Identity = giaykt.MaNguoiDuocCap, Name = giaykt.TenNguoiDuocCap },
                //GoodsItem = new List<GoodsItem>()
            };
            foreach (HangGiayKiemTra item in giaykt.HangCollection)
            {

                GoodsItem itemGiayKT = new GoodsItem()
                {
                    Commodity = new Commodity()
                    {
                        Identification = item.MaHang,
                        Description = item.TenHang,
                        TariffClassification = item.MaHS.Trim()
                    },
                    Origin = new Origin { OriginCountry = item.NuocXX_ID },
                    GoodsMeasure = new GoodsMeasure()
                    {
                        Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                        MeasureUnit = item.DVT_ID
                    },
                    AdditionalInformation = new AdditionalInformation()
                    {
                        Content = new Content { Text = item.GhiChu }
                    },
                    AdditionalDocument = (!string.IsNullOrEmpty(item.LoaiChungTu) && !string.IsNullOrEmpty(item.TenChungTu)) ? new AdditionalDocument()
                    {
                        Type = item.LoaiChungTu,
                        Reference = item.SoChungTu,
                        Name = item.TenChungTu,
                        Issue = item.NgayPhatHanh.ToString(sfmtDate)
                    } : null,
                    Examination = new Examination()
                    {
                        Place = item.DiaDiemKiemTra.Trim(),
                        Examiner = new NameBase()
                                        {
                                            Name = item.TenCoQuanKT.Trim(),
                                            Identity = item.MaCoQuanKT.Trim(),
                                            Result = item.KetQuaKT.Trim()
                                        }

                    },


                };
                //Result.GoodsItem.Add(itemGiayKT);
            }
            return Result;
        }

        private static ExaminationRegistration ExamRegistration(GiayKiemTra giaykt)
        {
            giaykt.LoadFull();
            ExaminationRegistration registration = new ExaminationRegistration()
            {
                Reference = giaykt.SoGiayKiemTra,
                Issue = giaykt.NgayDangKy.ToString(sfmtDate),
                Register = new NameBase() { Identity = giaykt.MaNguoiDuocCap, Name = giaykt.TenNguoiDuocCap },
            };
            foreach (HangGiayKiemTra item in giaykt.HangCollection)
            {

                GoodsItem itemGiayKT = new GoodsItem()
                {
                    Commodity = new Commodity()
                    {
                        Identification = item.MaHang,
                        Description = item.TenHang,
                        TariffClassification = item.MaHS.Trim()
                    },
                    Origin = new Origin { OriginCountry = item.NuocXX_ID },
                    GoodsMeasure = new GoodsMeasure()
                    {
                        Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                        MeasureUnit = item.DVT_ID
                    },
                    AdditionalInformation = new AdditionalInformation()
                    {
                        Content = new Content { Text = item.GhiChu }
                    },
                    AdditionalDocument = (!string.IsNullOrEmpty(item.LoaiChungTu) && !string.IsNullOrEmpty(item.TenChungTu)) ? new AdditionalDocument()
                    {
                        Type = item.LoaiChungTu,
                        Reference = item.SoChungTu,
                        Name = item.TenChungTu,
                        Issue = item.NgayPhatHanh.ToString(sfmtDate)
                    } : null,
                    Examination = new Examination()
                    {
                        Place = item.DiaDiemKiemTra.Trim(),

                    },
                    Examiner = new NameBase()
                    {

                        Name = item.TenCoQuanKT.Trim(),
                        Identity = item.MaCoQuanKT.Trim(),
                        //Result = item.KetQuaKT
                    }


                };
                //registration.GoodsItem.Add(itemGiayKT);
            }
            return registration;
        }

        private static CertificateOfInspection CertificateOfInspectionFrom(ChungThuGiamDinh GiamDinh)
        {
            GiamDinh.LoadHang();
            CertificateOfInspection CTGiamDinh = new CertificateOfInspection()
            {
                Examination = new Examination()
                {
                    Place = GiamDinh.DiaDiem,
                    Examiner = new NameBase { Name = GiamDinh.TenCoQuanGD, Identity = GiamDinh.MaCoQuanGD },
                    ExaminePerson = new NameBase { Name = GiamDinh.CanBoGD },
                    Result = new Result { content = GiamDinh.NoiDung, resultOfExam = GiamDinh.KetQua },

                },
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = GiamDinh.ThongTinKhac } },
                //GoodsItem = new List<GoodsItem>()
            };
            foreach (HangGiamDinh item in GiamDinh.ListHang)
            {
                GoodsItem hang = new GoodsItem
                {
                    Commodity = new Commodity
                    {
                        Description = item.TenHang,
                        Identification = item.MaPhu,
                        TariffClassification = item.MaHS.Trim()
                    },
                    Origin = new Origin { OriginCountry = item.NuocXX_ID },
                    GoodsMeasure = new GoodsMeasure
                    {
                        Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                        MeasureUnit = item.DVT_ID
                    },
                    BillOfLading = new Company.KDT.SHARE.Components.BillOfLading
                    {
                        Reference = item.SoVanTaiDon,
                        Issue = item.NgayVanDon.ToString(sfmtDate)
                    },
                    EquipmentIdentification = new EquipmentIdentification
                    {
                        identification = item.SoHieuContainer,
                        description = item.TinhTrangContainer ? "1" : "0"
                    },
                    AdditionalInformation = new AdditionalInformation
                    {
                        Content = new Content { Text = item.GhiChu }
                    }

                };
                //CTGiamDinh.GoodsItem.Add(hang);
            }
            return CTGiamDinh;
        }


        #endregion

        #region Bổ sung chứng từ
        public static BoSungChungTu ToDataTransferBoSung(ToKhaiMauDich tkmd, object NoiDungBoSung
            /*Company.KDT.SHARE.QuanLyChungTu.CO co,
            Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem,
            Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep,
            Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong,
            Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon,
            Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK*/
            )
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");

            BoSungChungTu boSungChungTuDto = new BoSungChungTu()
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                Function = DeclarationFunction.KHAI_BAO,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };
            boSungChungTuDto.Agents = AgentsFrom(tkmd);
            boSungChungTuDto.Importer = new NameBase()
            {
                Identity = tkmd.MaDoanhNghiep,
                Name = tkmd.TenDoanhNghiep
            };
            boSungChungTuDto.DeclarationDocument = DeclarationDocument(tkmd);

            if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.CO))
            #region CO
            {
                Company.KDT.SHARE.QuanLyChungTu.CO co = (Company.KDT.SHARE.QuanLyChungTu.CO)NoiDungBoSung;
                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = co.MaDoanhNghiep,
                    Name = co.TenDiaChiNguoiNK
                };
                boSungChungTuDto.Issuer = AdditionalDocumentType.EXPORT_CO_FORM_D;
                boSungChungTuDto.Reference = co.GuidStr;

                boSungChungTuDto.CertificateOfOrigins = new List<Company.KDT.SHARE.Components.CertificateOfOrigin>();
                Company.KDT.SHARE.Components.CertificateOfOrigin certificateOfOrigin = CertificateOfOriginFrom(co, tkmd, isToKhaiNhap);
                boSungChungTuDto.CertificateOfOrigins.Add(certificateOfOrigin);
            }
            #endregion

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GiayPhep))
            #region Giấy phép
            {

                Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = (Company.KDT.SHARE.QuanLyChungTu.GiayPhep)NoiDungBoSung;
                boSungChungTuDto.Issuer = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE;
                boSungChungTuDto.Reference = giayphep.GuidStr;

                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = giayphep.MaDonViDuocCap,
                    Name = giayphep.TenDonViDuocCap
                };
                boSungChungTuDto.Licenses = new List<Company.KDT.SHARE.Components.License>();
                Company.KDT.SHARE.Components.License license = LicenseFrom(giayphep);
                boSungChungTuDto.Licenses.Add(license);
            }
            #endregion Giấy phép

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai))
            #region Hợp đồng thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong = (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOP_DONG;
                boSungChungTuDto.Reference = hopdong.GuidStr;

                boSungChungTuDto.ContractDocuments = new List<Company.KDT.SHARE.Components.ContractDocument>();
                Company.KDT.SHARE.Components.ContractDocument contract = ContractFrom(hopdong);
                boSungChungTuDto.ContractDocuments.Add(contract);
            }
            #endregion Hợp đồng thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai))
            #region Hóa đơn thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon = (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOA_DON_THUONG_MAI;
                boSungChungTuDto.Reference = hoadon.GuidStr;
                boSungChungTuDto.CommercialInvoices = new List<Company.KDT.SHARE.Components.CommercialInvoice>();
                Company.KDT.SHARE.Components.CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoadon);
                boSungChungTuDto.CommercialInvoices.Add(commercialInvoice);
            }
            #endregion Hóa đơn thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau))
            #region Đề nghị chuyển cửa khẩu
            {
                Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK = (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.DE_NGHI_CHUYEN_CUA_KHAU;
                boSungChungTuDto.Reference = dnChuyenCK.GuidStr;
                boSungChungTuDto.NatureOfTransaction = tkmd.MaLoaiHinh.Trim();
                boSungChungTuDto.CustomsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCK);
                boSungChungTuDto.CustomsReference = tkmd.SoToKhai.ToString();
                boSungChungTuDto.Acceptance = tkmd.NgayTiepNhan.ToString(sfmtDate);
                boSungChungTuDto.DeclarationDocument = null;
                boSungChungTuDto.Agents.RemoveAt(1);
            }
            #endregion Đề nghị chuyển cửa khẩu

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.ChungTuKem))
            #region Chứng từ kèm
            {
                Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem = (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH;
                boSungChungTuDto.Reference = chungtukem.GUIDSTR;

                boSungChungTuDto.AttachDocuments = new List<AttachDocumentItem>();
                AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, chungtukem);
                boSungChungTuDto.AttachDocuments.Add(attachDocumentItem);
            }
            #endregion Chứng từ kèm

            else if (NoiDungBoSung.GetType() == typeof(GiayKiemTra))
            {
                #region Giấy kiểm tra
                GiayKiemTra giayKT = (GiayKiemTra)NoiDungBoSung;
                boSungChungTuDto.Reference = giayKT.GuidStr;
                if (giayKT.LoaiGiay == LoaiGiayKiemTra.DANG_KY_KIEM_TRA)
                {
                    boSungChungTuDto.Issuer = AdditionalDocumentType.GIAY_DANG_KY_KT;
                    boSungChungTuDto.ExaminationRegistration = ExamRegistration(giayKT);
                }
                else if (giayKT.LoaiGiay == LoaiGiayKiemTra.KET_QUA_KIEM_TRA)
                {
                    boSungChungTuDto.Issuer = AdditionalDocumentType.GIAY_KET_QUA_KT;
                    boSungChungTuDto.ExaminationResult = ExaminationResult(giayKT);
                }
                #endregion
            }
            else if (NoiDungBoSung.GetType() == typeof(ChungThuGiamDinh))
            {
                #region Chứng thư giám định
                ChungThuGiamDinh chungthuGD = (ChungThuGiamDinh)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.CHUNG_THU_GIAM_DINH;
                boSungChungTuDto.Reference = chungthuGD.GuidStr;
                boSungChungTuDto.NatureOfTransaction = tkmd.MaLoaiHinh.Trim();
                boSungChungTuDto.CertificateOfInspection = CertificateOfInspectionFrom(chungthuGD);
                boSungChungTuDto.CustomsReference = tkmd.SoToKhai.ToString();
                boSungChungTuDto.Acceptance = tkmd.NgayTiepNhan.ToString(sfmtDate);
                #endregion
            }
            return boSungChungTuDto;
        }


        #endregion

        public static SXXK_HosoThanhKhoan ToDataTransferSXXK_HoSoThanhKhoan(HoSoThanhLyDangKy hosothanhly, string tenDoanhNghiep)
        {
            string lloi = "";
            try
            {


                SXXK_HosoThanhKhoan thanhkhoan = new SXXK_HosoThanhKhoan()
                {
                    Issuer = Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                    Function = DeclarationFunction.KHAI_BAO,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    IssueLocation = string.Empty,
                    DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hosothanhly.MaHaiQuanTiepNhan.Trim()) : hosothanhly.MaHaiQuanTiepNhan.Trim(),
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                    CustomsReference = string.Empty,
                    Acceptance = string.Empty,
                    Agents = new List<Agent>(),
                    LoadingList = Helpers.FormatNumeric(hosothanhly.ChungTuTTs.Count),
                    Importer = new NameBase()
                    {
                        Name = tenDoanhNghiep,
                        Identity = hosothanhly.MaDoanhNghiep.Trim()
                    },
                    Reference = hosothanhly.GuidStr,
                    ImportDeclarationList = new ImportDeclarationDocument()
                    {
                        Reference = hosothanhly.GuidStr,
                        CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                        Issuer = Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                        Function = DeclarationFunction.KHAI_BAO,
                        Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                        Issue = DateTime.Now.ToString(sfmtDate),
                        Acceptance = DateTime.Now.ToString(sfmtDate),
                        DeclarationDocuments = new List<DeclarationDocument>()
                    },
                    ExportDeclarationList = new ImportDeclarationDocument()
                    {
                        Reference = hosothanhly.GuidStr,
                        CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                        Issuer = Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                        Function = DeclarationFunction.KHAI_BAO,
                        Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                        Issue = DateTime.Now.ToString(sfmtDate),
                        Acceptance = DateTime.Now.ToString(sfmtDate),
                        DeclarationDocuments = new List<DeclarationDocument>()
                    },
                    PaymentDocumentList = new PaymentDocumentList()
                    {
                        Reference = hosothanhly.GuidStr,
                        CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                        Issuer = Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                        Function = DeclarationFunction.KHAI_BAO,
                        Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                        Issue = DateTime.Now.ToString(sfmtDate),
                        Acceptance = DateTime.Now.ToString(sfmtDate),
                        ContractReferences = new List<SXXK_ContractDocument>()
                    },

                    MaterialLeft = new DocumentList()
                    {
                        Reference = hosothanhly.GuidStr,
                        CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                        Issuer = Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                        Function = DeclarationFunction.KHAI_BAO,
                        Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                        Issue = DateTime.Now.ToString(sfmtDate),
                        Acceptance = DateTime.Now.ToString(sfmtDate)
                    },
                    MaterialExport = new DocumentList()
                    {
                        Reference = hosothanhly.GuidStr,
                        CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                        Issuer = Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                        Function = DeclarationFunction.KHAI_BAO,
                        Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                        Issue = DateTime.Now.ToString(sfmtDate),
                        Acceptance = DateTime.Now.ToString(sfmtDate),
                        ContractDocuments = new List<SXXK_ContractDocument>()


                    },
                    MaterialNotExport = new DocumentList()
                    {
                        Reference = hosothanhly.GuidStr,
                        CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                        Issuer = Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                        Function = DeclarationFunction.KHAI_BAO,
                        Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                        Issue = DateTime.Now.ToString(sfmtDate),
                        Acceptance = DateTime.Now.ToString(sfmtDate),
                        ContractDocuments = new List<SXXK_ContractDocument>()

                    },
                    //MaterialSupply = new DocumentList()
                    //{
                    //    Reference = hosothanhly.GuidStr,
                    //    CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                    //    Issuer = Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                    //    Function = DeclarationFunction.KHAI_BAO,
                    //    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                    //    Issue = DateTime.Now.ToString(sfmtDate),
                    //    Acceptance = DateTime.Now.ToString(sfmtDate),

                    //},
                    //MaterialList = new DocumentList()
                    //{
                    //    Reference = hosothanhly.GuidStr,
                    //    CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                    //    Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                    //    Function = DeclarationFunction.KHAI_BAO,
                    //    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                    //    Issue = DateTime.Now.ToString(sfmtDate),
                    //    Acceptance = DateTime.Now.ToString(sfmtDate),
                    //    ImportDeclarationDocuments = new List<SXXK_ContractDocument>()
                    //},

                    MaterialReExport = new DocumentList()
                    {
                        Reference = hosothanhly.GuidStr,
                        CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                        Issuer = Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                        Function = DeclarationFunction.KHAI_BAO,
                        Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                        Issue = DateTime.Now.ToString(sfmtDate),
                        Acceptance = DateTime.Now.ToString(sfmtDate),
                        ImportDeclarationDocuments = new List<SXXK_ContractDocument>(),
                    }
                };
                thanhkhoan.Agents.Add(new Agent()
                {
                    Name = tenDoanhNghiep,
                    Identity = hosothanhly.MaDoanhNghiep.Trim(),
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN

                });
                if (Globals.LaDNCX)
                {
                    //thanhkhoan.FinalYear= hosothanhly.NgayBatDau.Year.ToString();
                    //if (0 < hosothanhly.NgayBatDau.Month && hosothanhly.NgayBatDau.Month < 4)
                    //    thanhkhoan.FinalQuarter = "1";
                    //else if (3 < hosothanhly.NgayBatDau.Month && hosothanhly.NgayBatDau.Month < 7)
                    //    thanhkhoan.FinalQuarter = "2";
                    //else if (6 < hosothanhly.NgayBatDau.Month && hosothanhly.NgayBatDau.Month < 9)
                    //    thanhkhoan.FinalQuarter = "3";
                    //else
                    //    thanhkhoan.FinalQuarter = "4";
                    string[] st = hosothanhly.SoQuyetDinh.Split('/');
                    thanhkhoan.FinalQuarter = st[0].ToString();
                    thanhkhoan.FinalYear = st[1].ToString();
                }
        #endregion Header
                lloi += " 1 .Bat dau bang ke To Khai Nhap \r\n ";
                #region ImportDeclarationList Bảng kê tờ khai nhập
                int sobanke = hosothanhly.getBKToKhaiNhap();
                if (sobanke > -1)
                {
                    hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                    if (Company.KDT.SHARE.Components.Globals.KhaiBaoThanhKoanLoai2)
                    {

                        DataTable dt = new DataTable();
                        if (Globals.LaDNCX)
                        {
                            int NamDangky = hosothanhly.NgayKetThuc.Year;
                            if (hosothanhly.SoQuyetDinh.Contains("/"))
                                NamDangky = Convert.ToInt32(hosothanhly.SoQuyetDinh.Split('/')[1]);

                            dt = new Company.BLL.KDT.SXXK.BKToKhaiNhap().getBKToKhaiNhap_CX(hosothanhly.MaDoanhNghiep, DateTime.Now.Year, hosothanhly.LanThanhLy, Convert.ToInt32(hosothanhly.BKCollection[hosothanhly.getBKToKhaiNhap()].ID));
                        }
                        else
                            dt = new Company.BLL.KDT.SXXK.BKToKhaiNhap().getBKToKhaiNhapForReport(hosothanhly.LanThanhLy, 1, hosothanhly.MaDoanhNghiep, hosothanhly.MaHaiQuanTiepNhan, string.Empty).Tables[0];
                        foreach (DataRow dr in dt.Rows)
                        {
                            #region bang ke to khai cua DNCX
                            if (Globals.LaDNCX)
                            {
                                if (dr["MaLoaiHinh"].ToString().Contains("NV"))
                                {
                                    if (!dr["MaLoaiHinh"].ToString().Contains("B13"))
                                    {
                                        lloi += " Sotokhai= " + dr["SoToKhai"].ToString() + "\r\n";
                                        thanhkhoan.ImportDeclarationList.DeclarationDocuments.Add(new DeclarationDocument()
                                        {
                                            CustomsReference = dr["SoToKhai"].ToString().Split('/')[0],
                                            NatureOfTransaction = Globals.IsKhaiVNACCS ? dr["MaLoaiHinh"].ToString().Trim().Replace("NV", "") : dr["MaLoaiHinh"].ToString().Trim(),
                                            Acceptance = Convert.ToDateTime(dr["NgayDangKy"]).ToString(sfmtDate),
                                            DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dr["MaHaiQuan"].ToString().Trim()) : dr["MaHaiQuan"].ToString().Trim(),
                                            Clearance = Convert.ToDateTime(dr["NgayHoanThanh"]).ToString(sfmtDate),
                                        });
                                    }
                                }
                            }
                            #endregion
                            else
                            {
                                thanhkhoan.ImportDeclarationList.DeclarationDocuments.Add(new DeclarationDocument()
                                {
                                    CustomsReference = dr["SoToKhai"].ToString().Split('/')[0],
                                    NatureOfTransaction = Globals.IsKhaiVNACCS ? dr["MaLoaiHinh"].ToString().Trim().Replace("NV", "") : dr["MaLoaiHinh"].ToString().Trim(),
                                    Acceptance = Convert.ToDateTime(dr["NgayDangKy"]).ToString(sfmtDate),
                                    DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dr["MaHaiQuan"].ToString().Trim()) : dr["MaHaiQuan"].ToString().Trim(),
                                    Clearance = Convert.ToDateTime(dr["NgayHoanThanh"]).ToString(sfmtDate),
                                });
                            }
                        }
                    }
                    else
                    {
                        foreach (BKToKhaiNhap tkn in hosothanhly.BKCollection[sobanke].bkTKNCollection)
                        {
                            #region bang ke to khai cua DNCX
                            if (Globals.LaDNCX)
                            {
                                if (tkn.MaLoaiHinh.Trim().Contains("NV"))
                                {
                                    thanhkhoan.ImportDeclarationList.DeclarationDocuments.Add(new DeclarationDocument()
                                    {
                                        CustomsReference = Helpers.FormatNumeric(tkn.SoToKhai),
                                        NatureOfTransaction = Globals.IsKhaiVNACCS ? tkn.MaLoaiHinh.Trim().Replace("NV", "") : tkn.MaLoaiHinh.Trim(),
                                        Acceptance = tkn.NgayDangKy.ToString(sfmtDate),
                                        DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkn.MaHaiQuan.Trim()) : tkn.MaHaiQuan.Trim(),
                                        Clearance = tkn.NgayHoanThanh.ToString(sfmtDate)
                                    });
                                }
                            }
                            #endregion
                            else
                            {
                                thanhkhoan.ImportDeclarationList.DeclarationDocuments.Add(new DeclarationDocument()
                                {
                                    CustomsReference = Helpers.FormatNumeric(tkn.SoToKhai),
                                    NatureOfTransaction = Globals.IsKhaiVNACCS ? tkn.MaLoaiHinh.Trim().Replace("NV", "") : tkn.MaLoaiHinh.Trim(),
                                    Acceptance = tkn.NgayDangKy.ToString(sfmtDate),
                                    DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkn.MaHaiQuan.Trim()) : tkn.MaHaiQuan.Trim(),
                                    Clearance = tkn.NgayHoanThanh.ToString(sfmtDate)
                                });
                            }
                        }
                    }

                }
                #endregion
                lloi += " => Ket thuc bang ke nhap \r\n";
                lloi += " 2 .Bat dau bang ke To Khai Xuất \r\n ";
                sobanke = hosothanhly.getBKToKhaiXuat();
                if (sobanke > -1)
                {
                    hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                    #region ExportDeclarationList Bảng kê tờ khai Xuất
                    if (Company.KDT.SHARE.Components.Globals.KhaiBaoThanhKoanLoai2)
                    {
                        DataTable dt = new Company.BLL.KDT.SXXK.BKToKhaiXuat().getBKToKhaiXuatForReport(hosothanhly.LanThanhLy, hosothanhly.MaDoanhNghiep, hosothanhly.MaHaiQuanTiepNhan).Tables[0];
                        foreach (DataRow dr in dt.Rows)
                        {
                            #region Bang ke to khai cua DNCX
                            if (Globals.LaDNCX)
                            {
                                if (dr["MaLoaiHinh"].ToString().Contains("XV"))
                                {
                                    if (!dr["MaLoaiHinh"].ToString().Contains("XVB13"))
                                    {
                                        thanhkhoan.ExportDeclarationList.DeclarationDocuments.Add(new DeclarationDocument()
                                        {
                                            CustomsReference = dr["SoToKhai"].ToString().Split('/')[0], //Helpers.FormatNumeric(dr["SoToKhaiXuat"]),
                                            NatureOfTransaction = Globals.IsKhaiVNACCS ? dr["MaLoaiHinh"].ToString().Trim().Replace("XV", "") : dr["MaLoaiHinh"].ToString().Trim(),
                                            Acceptance = Convert.ToDateTime(dr["NgayDangKy"]).ToString(sfmtDate),
                                            DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dr["MaHaiQuan"].ToString().Trim()) : dr["MaHaiQuan"].ToString().Trim(),
                                            Clearance = Convert.ToDateTime(dr["NgayThucXuat"]).ToString(sfmtDate),
                                        });
                                    }
                                }
                            }
                            #endregion
                            else
                            {
                                thanhkhoan.ExportDeclarationList.DeclarationDocuments.Add(new DeclarationDocument()
                                {
                                    CustomsReference = dr["SoToKhai"].ToString().Split('/')[0], //Helpers.FormatNumeric(dr["SoToKhaiXuat"]),
                                    NatureOfTransaction = Globals.IsKhaiVNACCS ? dr["MaLoaiHinh"].ToString().Trim().Replace("XV", "") : dr["MaLoaiHinh"].ToString().Trim(),
                                    Acceptance = Convert.ToDateTime(dr["NgayDangKy"]).ToString(sfmtDate),
                                    DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dr["MaHaiQuan"].ToString().Trim()) : dr["MaHaiQuan"].ToString().Trim(),
                                    Clearance = Convert.ToDateTime(dr["NgayThucXuat"]).ToString(sfmtDate),
                                });
                            }
                        }
                    }
                    else
                    {
                        foreach (BKToKhaiXuat tkx in hosothanhly.BKCollection[sobanke].bkTKXColletion)
                        {
                            thanhkhoan.ExportDeclarationList.DeclarationDocuments.Add(new DeclarationDocument()
                            {
                                CustomsReference = Helpers.FormatNumeric(tkx.SoToKhai),
                                NatureOfTransaction = Globals.IsKhaiVNACCS ? tkx.MaLoaiHinh.Trim().Replace("XV", "") : tkx.MaLoaiHinh.Trim(),
                                Acceptance = tkx.NgayDangKy.ToString(sfmtDate),
                                DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkx.MaHaiQuan.Trim()) : tkx.MaHaiQuan.Trim(),
                                Clearance = tkx.NgayHoanThanh.ToString(sfmtDate)
                            });
                        }
                    }
                    #endregion
                }
                lloi += " => .Kết thúc bang ke To Khai xuat \r\n ";
                #region PaymentDocumentList Bảng chứng từ thanh toán
                hosothanhly.ChungTuTTs = ChungTuTT.SelectCollectionDynamic("Master_ID = " + hosothanhly.ID, null);
                foreach (ChungTuTT cttt in hosothanhly.ChungTuTTs)
                {
                    SXXK_ContractDocument contract = new SXXK_ContractDocument()
                    {
                        Reference = cttt.SoHopDong.Trim(),
                        Issue = cttt.NgayHopDong.ToString(sfmtDate),
                        CustomsValue = Helpers.FormatNumeric(cttt.TriGiaTT, 2),
                        PaymentDocument = new PaymentDocument()
                        {
                            Reference = cttt.SoChungTu,
                            Issue = cttt.NgayChungTu.ToString(sfmtDate),
                            IssueLocation = cttt.NoiPhatHanh,
                            PaymentMethod = cttt.PTTT_ID.Trim(),
                            CustomsValue = Helpers.FormatNumeric(cttt.TriGiaTT, 2),
                            AdditionalInformations = new List<AdditionalInformation>()
                        }
                    };
                    contract.PaymentDocument.AdditionalInformations.Add(new AdditionalInformation()
                    {
                        //CustomsValue = cttt.GhiChu,
                        Content = new Content() { Text = cttt.GhiChu }
                    });
                    cttt.ChungTuTTChitiets = ChungTuTTChitiet.SelectCollectionDynamic("Master_ID = " + cttt.ID, null);
                    if (cttt.ChungTuTTChitiets.Count > 0)
                        contract.Commoditys = new List<Commodity>();

                    foreach (ChungTuTTChitiet item in cttt.ChungTuTTChitiets)
                    {
                        contract.Commoditys.Add(new Commodity()
                        {
                            Identification = item.MaHang,
                            StatisticalValue = Helpers.FormatNumeric(item.TriGia, 3)
                        });
                    }
                    thanhkhoan.PaymentDocumentList.ContractReferences.Add(contract);

                }
                #endregion

                sobanke = hosothanhly.getBKNPLChuaThanhLY();
                #region MaterialLeft Bảng kê nguyên phụ liệu chưa thanh lý
                if (sobanke > -1)
                {
                    lloi += " 3 .Bat dau bang ke NPL chua thanh ly \r\n ";
                    hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                    thanhkhoan.MaterialLeft.ContractDocuments = new List<SXXK_ContractDocument>();

                    BKNPLChuaThanhLyCollection bkSoToKhais = new BKNPLChuaThanhLyCollection();

                    foreach (BKNPLChuaThanhLy item in hosothanhly.BKCollection[sobanke].bkNPLCTLCollection)
                    {
                        bool isEqual = false;
                        foreach (BKNPLChuaThanhLy item1 in bkSoToKhais)
                        {
                            if (item.Equals(item1))
                            {
                                isEqual = true;
                                break;
                            }
                        }

                        if (!isEqual)
                            bkSoToKhais.Add(item);
                    }
                    foreach (BKNPLChuaThanhLy bkCTL in bkSoToKhais)
                    {
                        SXXK_ContractDocument bagKeChuaTL = new SXXK_ContractDocument()
                        {

                            CustomsReference = Helpers.FormatNumeric(bkCTL.SoToKhai),
                            NatureOfTransaction = Globals.IsKhaiVNACCS ? bkCTL.MaLoaiHinh.Trim().Replace("NV", "") : bkCTL.MaLoaiHinh.Trim(),
                            Acceptance = bkCTL.NgayDangKy.ToString(sfmtDate),
                            DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(bkCTL.MaHaiQuan.Trim()) : bkCTL.MaHaiQuan.Trim(),
                            Materials = new List<SXXK_Product>()
                        };
                        #region Thêm hàng
                        foreach (BKNPLChuaThanhLy item in hosothanhly.BKCollection[sobanke].bkNPLCTLCollection)
                        {
                            if (item.Equals(bkCTL))
                            {
                                SXXK_Product material = new SXXK_Product()
                                    {
                                        Commoditys = new List<Commodity>(),
                                        GoodsMeasures = new List<SXXK_GoodsMeasure>()
                                    };


                                Commodity Commodity = new Commodity()
                                {
                                    Identification = item.MaNPL,
                                    Description = item.TenNPL
                                };
                                SXXK_GoodsMeasure GoodsMeasure = new SXXK_GoodsMeasure()
                                {

                                    Quantity = Helpers.FormatNumeric(item.Luong, 5),
                                    MeasureUnits = new List<string>(),
                                    ConversionRate = "1"
                                };
                                GoodsMeasure.MeasureUnits.Add(Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim());
                                GoodsMeasure.MeasureUnits.Add(Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim());
                                material.Commoditys.Add(Commodity);
                                material.GoodsMeasures.Add(GoodsMeasure);
                                #region Kiểu thêm hàng bt
                                //material = new SXXK_Product()
                                //{
                                //    Commoditys = new Commodity()
                                //    {
                                //        Identification = item.MaNPL,
                                //        Description = item.TenNPL
                                //    },
                                //    GoodsMeasure = new SXXK_GoodsMeasure()
                                //    {
                                //        Tariff = Helpers.FormatNumeric(item.Luong, 5),
                                //        MeasureUnits = new List<string>(),
                                //        ConversionRate = "1"
                                //    }
                                //};
                                //material.GoodsMeasure.MeasureUnits.Add(item.DVT_ID.Trim());
                                //material.GoodsMeasure.MeasureUnits.Add(item.DVT_ID.Trim());

                                //Cách theo hàng LanNT
                                //bagKeChuaTL.Materials.Add(material);
                                #endregion
                                bagKeChuaTL.Materials.Add(material);
                            }

                        }

                        #endregion
                        if (bkCTL.MaLoaiHinh.Contains("V"))
                            bagKeChuaTL.CustomsReference = CapSoToKhai.GetSoTKVNACCS(bkCTL.SoToKhai).ToString();
                        thanhkhoan.MaterialLeft.ContractDocuments.Add(bagKeChuaTL);
                    }
                }
                //else
                //{
                //    thanhkhoan.MaterialLeft = null;
                //}
                #endregion
                lloi += " => Ket thuc bang ke \r\n ";
                lloi += " 4 .Bat dau bang ke NPL Tieu Huy \r\n ";

                sobanke = hosothanhly.getBKNPLXinHuy();
                if (sobanke > -1)
                {
                    #region Bảng kê nguyên nguyên phụ liệu hủy
                    thanhkhoan.ExportNonDeclarationList = new ExportNonDeclarationDocument()
                        {
                            Consignees = new List<Consignee>()
                        };

                    Consignee consignee = new Consignee()
                    {
                        Name = tenDoanhNghiep,
                        Identity = hosothanhly.MaDoanhNghiep.Trim(),
                        Function = "2",
                        DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hosothanhly.MaHaiQuanTiepNhan.Trim()),
                        CustomsGoodsItems = new List<CustomsGoodsItem>()
                    };

                    foreach (BKNPLXinHuy bkCTL in hosothanhly.BKCollection[sobanke].bkNPLXHCollection)
                    {
                        CustomsGoodsItem item = new CustomsGoodsItem()
                            {
                                Commodity = new Commodity()
                                {
                                    Description = bkCTL.TenNPL,
                                    Identification = bkCTL.MaNPL,
                                    TariffClassification = bkCTL.MaHS,
                                    Type = "1"
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    Quantity = Helpers.FormatNumeric(bkCTL.LuongHuy, 4),
                                    MeasureUnit = VNACCS_Mapper.GetCodeVNACC(bkCTL.DVT_ID)
                                }
                            };
                        consignee.CustomsGoodsItems.Add(item);

                    }
                    thanhkhoan.ExportNonDeclarationList.Consignees.Add(consignee);
                }

                    #endregion

                lloi += " => Ket thuc bang ke \r\n ";
                sobanke = hosothanhly.getBKNPLXuatGiaCong();
                if (sobanke > -1)
                #region MaterialList Bảng kê nguyên phụ liệu xuất khẩu qua sản phẩm theo hợp đồng gia công
                {
                    hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                    BKNPLXuatGiaCongCollection bkNPL2GC = hosothanhly.BKCollection[sobanke].bkNPLXGCCollection;
                    BKNPLXuatGiaCongCollection bkToKhais = new BKNPLXuatGiaCongCollection();
                    foreach (BKNPLXuatGiaCong item in bkNPL2GC)
                    {
                        bool isExist = false;
                        foreach (BKNPLXuatGiaCong itemDt in bkToKhais)
                        {
                            isExist = itemDt.Equals(item);
                            if (isExist) break;
                        }
                        if (!isExist)
                            bkToKhais.Add(item);
                    }

                    foreach (BKNPLXuatGiaCong bkCTL in bkToKhais)
                    {
                        SXXK_ContractDocument bkNPXToGC = new SXXK_ContractDocument()
                        {
                            CustomsReference = Helpers.FormatNumeric(bkCTL.SoToKhai),
                            NatureOfTransaction = Globals.IsKhaiVNACCS ? bkCTL.MaLoaiHinh.Trim().Replace("XV", "") : bkCTL.MaLoaiHinh.Trim(),
                            Acceptance = bkCTL.NgayDangKy.ToString(sfmtDate),
                            DeclarationOffice = bkCTL.MaHaiQuan.Trim(),
                            Materials = new List<SXXK_Product>()
                        };

                        SXXK_Product Material = new SXXK_Product()
                        {
                            Commoditys = new List<Commodity>(),
                            DetailMaterialExports = new List<SXXK_GoodsMeasure>()
                        };

                        foreach (BKNPLXuatGiaCong item in hosothanhly.BKCollection[sobanke].bkNPLXGCCollection)
                        {
                            if (item.Equals(bkCTL))
                            {
                                Commodity Commodity = new Commodity()
                                {
                                    Description = item.TenNPL,
                                    Identification = item.MaNPL
                                };
                                Material.Commoditys.Add(Commodity);
                                SXXK_GoodsMeasure DetailMaterialExport = new SXXK_GoodsMeasure()
                                {
                                    CustomsReference = Helpers.FormatNumeric(item.SoToKhaiXuat),
                                    NatureOfTransaction = Globals.IsKhaiVNACCS ? item.MaLoaiHinhXuat.Trim().Replace("XV", "") : item.MaLoaiHinhXuat.Trim(),
                                    Acceptance = item.NgayDangKyXuat.ToString(sfmtDate),
                                    DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item.MaHaiQuanXuat.Trim()) : item.MaHaiQuanXuat.Trim(),
                                    GoodsMeasure = new GoodsMeasure()
                                    {
                                        Quantity = Helpers.FormatNumeric(item.LuongXuat, 5),
                                        MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim()
                                    }

                                };
                                Material.DetailMaterialExports.Add(DetailMaterialExport);
                            }
                        }
                        bkNPXToGC.Materials.Add(Material);
                        thanhkhoan.MaterialExport.ContractDocuments.Add(bkNPXToGC);
                    }
                }
                #endregion

                sobanke = hosothanhly.getBKNPLNopThue();
                if (sobanke > -1)
                #region MaterialNotExport Danh sách nguyên phụ liệu nộp thuế
                {
                    hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                    BKNPLNopThueTieuThuNoiDiaCollection bkToKhais = new BKNPLNopThueTieuThuNoiDiaCollection();

                    foreach (BKNPLNopThueTieuThuNoiDia bkCTL in hosothanhly.BKCollection[sobanke].bkNPLNTCollection)
                    {
                        bool isEqual = false;
                        foreach (BKNPLNopThueTieuThuNoiDia item in bkToKhais)
                        {
                            if (bkCTL.Equals(item))
                            {
                                isEqual = true;
                                break;
                            }
                        }
                        if (!isEqual)
                            bkToKhais.Add(bkCTL);
                    }
                    foreach (BKNPLNopThueTieuThuNoiDia bkItem in bkToKhais)
                    {
                        SXXK_ContractDocument document = new SXXK_ContractDocument()
                        {
                            CustomsReference = Helpers.FormatNumeric(bkItem.SoToKhai),
                            NatureOfTransaction = Globals.IsKhaiVNACCS ? bkItem.MaLoaiHinh.Trim().Replace("NV", "") : bkItem.MaLoaiHinh.Trim(),
                            Acceptance = bkItem.NgayDangKy.ToString(sfmtDate),
                            DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(bkItem.MaHaiQuan.Trim()) : bkItem.MaHaiQuan.Trim(),
                            // MethodOfProcess = "1",
                            Materials = new List<SXXK_Product>()
                        };
                        SXXK_Product material = new SXXK_Product()
                        {
                            Commoditys = new List<Commodity>(),
                            GoodsMeasures = new List<SXXK_GoodsMeasure>()
                        };

                        foreach (BKNPLNopThueTieuThuNoiDia item in hosothanhly.BKCollection[sobanke].bkNPLNTCollection)
                        {
                            if (bkItem.Equals(item))
                            {
                                Commodity Commodity = new Commodity()
                                {
                                    Description = item.TenNPL,
                                    Identification = item.MaNPL
                                };
                                material.Commoditys.Add(Commodity);
                                SXXK_GoodsMeasure GoodsMeasure = new SXXK_GoodsMeasure()
                                {
                                    Quantity = Helpers.FormatNumeric(item.LuongNopThue, 5),
                                    MeasureUnits = new List<string>(),
                                    ConversionRate = "1",
                                    MethodOfProcess = "1",
                                };
                                GoodsMeasure.MeasureUnits.Add(Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim());
                                GoodsMeasure.MeasureUnits.Add(Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim());
                                material.GoodsMeasures.Add(GoodsMeasure);

                            }
                        }
                        document.Materials.Add(material);
                        thanhkhoan.MaterialNotExport.ContractDocuments.Add(document);
                    }

                }
                #endregion

                sobanke = hosothanhly.getBKNPLTuCungUng();
                if (sobanke > -1)
                #region MaterialSupply Danh sách nguyên phụ liệu tự cung ứng
                {
                    SXXK_SupplyMaterial TuCungUng = new SXXK_SupplyMaterial() { Materials = new List<SXXK_MaterialSupply_Material>() };

                    hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                    foreach (KDT_SXXK_BKNPLTuCungUng_Detail item in hosothanhly.BKCollection[sobanke].bkNPLTCUCollection)
                    {
                        SXXK_MaterialSupply_Material Material = new SXXK_MaterialSupply_Material()
                        {
                            Identification = item.MaNPL,
                            Description = item.TenNPL,
                            Group = item.LoaiNPL.ToString(),
                            Content = item.GiaiTrinh,


                        };
                        Material.DetailMaterial = new SXXK_MaterialSupply_DetailMaterial()
                        {
                            CustomsReference = item.SoChungTu,
                            Acceptance = item.NgayChungTu.ToString(sfmtDate)
                        };
                        Material.GoodsMeasure = new GoodsMeasure()
                        {
                            Quantity = Helpers.FormatNumeric(item.LuongTuCungUng, 4),
                            MeasureUnit = item.DVT_ID,
                            UnitPrice = Helpers.FormatNumeric(item.DonGiaTT, 6),
                            Tax = Helpers.FormatNumeric(item.ThueSuat, 4)
                        };

                        TuCungUng.Materials.Add(Material);
                    }
                    thanhkhoan.SupplyMaterial = TuCungUng;

                }
                #endregion Danh sách nguyên phụ liệu tự cung ứng
                lloi += " 5 .Bat dau bang ke NPL Tai xuat \r\n ";
                #region Danh sách nguyên phụ liệu tái xuất

                sobanke = hosothanhly.getBKNPLTaiXuat();
                if (sobanke > -1)
                {

                    BangKeHoSoThanhLyCollection listBangKe = BangKeHoSoThanhLy.SelectCollectionBy_MaterID(hosothanhly.ID);
                    long IDBangKeNPLTaiXuat = 0;
                    foreach (BangKeHoSoThanhLy item in listBangKe)
                    {
                        if (item.MaBangKe == "DTLNPLTX")
                        {
                            IDBangKeNPLTaiXuat = item.ID;
                            break;
                        }
                    }
                    BKNPLTaiXuat nplTaiXuat = new BKNPLTaiXuat();
                    nplTaiXuat.ID = IDBangKeNPLTaiXuat;
                    nplTaiXuat.Load();
                    nplTaiXuat.BangKeHoSoThanhLy_ID = hosothanhly.BKCollection[sobanke].ID;
                    DataTable dtNplTaiXuat = nplTaiXuat.SelectBy_BangKeHoSoThanhLy_ID().Tables[0];
                    List<int> listSoToKhaiNhap = new List<int>();
                    BKNPLTaiXuatCollection bkTaiXuat = BKNPLTaiXuat.SelectCollectionBy_BangKeHoSoThanhLy_ID(IDBangKeNPLTaiXuat);
                    //bkTaiXuat.Add(nplTaiXuat);
                    foreach (BKNPLTaiXuat bkCTL in bkTaiXuat)
                    {
                        bool value = false;
                        foreach (int sotk in listSoToKhaiNhap)
                        {
                            if (sotk == bkCTL.SoToKhai)
                            {
                                value = true;
                                break;
                            }
                        }
                        if (!value)
                        {
                            SXXK_ContractDocument DSToKhaiNhap = new SXXK_ContractDocument();
                            decimal SoToKhaiVNACCS = 0;
                            if (bkCTL.MaLoaiHinh.Trim().Contains("V"))
                                SoToKhaiVNACCS = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(bkCTL.SoToKhai);
                            DSToKhaiNhap.CustomsReference = Globals.IsKhaiVNACCS && SoToKhaiVNACCS > 0 ? SoToKhaiVNACCS.ToString() : bkCTL.SoToKhai.ToString();
                            DSToKhaiNhap.NatureOfTransaction = Globals.IsKhaiVNACCS ? bkCTL.MaLoaiHinh.Trim().Replace("NV", "") : bkCTL.MaLoaiHinh.Trim();
                            DSToKhaiNhap.Acceptance = bkCTL.NgayDangKy.ToString(sfmtDateTime);
                            DSToKhaiNhap.DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(bkCTL.MaHaiQuan.Trim()) : bkCTL.MaHaiQuan.Trim();
                            DSToKhaiNhap.Commoditys = new List<Commodity>();
                            DSToKhaiNhap.GoodsMeasures = new List<GoodsMeasure>();
                            DataTable dtNPL = dtNplTaiXuat.Select("SoToKhai = " + bkCTL.SoToKhai.ToString(), "MaNPL").CopyToDataTable();
                            List<string> listNPL = new List<string>();
                            foreach (DataRow drNPL in dtNPL.Rows)
                            {
                                bool TrungNPL = false;
                                foreach (string maNPL in listNPL)
                                {
                                    if (maNPL.ToUpper() == drNPL["MaNPL"].ToString().Trim().ToUpper())
                                    {
                                        TrungNPL = true;
                                        break;
                                    }
                                }
                                if (!TrungNPL)
                                {
                                    DataRow[] dtToKhaiXuat = dtNPL.Select("SoToKhaiXuat = " + drNPL["SoToKhaiXuat"].ToString(), "SoToKhaiXuat");
                                    DSToKhaiNhap.Materials = new List<SXXK_Product>();
                                    SXXK_Product NPL = new SXXK_Product();
                                    NPL.Commoditys = new List<Commodity>();
                                    NPL.GoodsMeasures = new List<SXXK_GoodsMeasure>();

                                    Commodity comdity = new Commodity
                                        {
                                            Identification = drNPL["MaNPL"].ToString(),
                                            Description = drNPL["TenNPL"].ToString()

                                        };
                                    SXXK_GoodsMeasure measureUnit = new SXXK_GoodsMeasure
                                    {
                                        Quantity = Helpers.FormatNumeric(drNPL["LuongTaiXuat"], 4),
                                        MeasureUnits = new List<string>(),
                                        UnitPrice = Helpers.FormatNumeric(drNPL["DonGiaTT"], 6)

                                    };
                                    measureUnit.MeasureUnits.Add(Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(drNPL["DVT_ID"].ToString().Trim()) : drNPL["DVT_ID"].ToString().Trim());
                                    NPL.Commoditys.Add(comdity);
                                    NPL.GoodsMeasures.Add(measureUnit);

                                    NPL.ExportDeclarationDocument = new List<SXXK_ContractDocument>();
                                    foreach (DataRow dr in dtToKhaiXuat)
                                    {
                                        decimal SoToKhaiXuatVNACCS = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(System.Convert.ToInt32(dr["SoToKhaiXuat"]));
                                        SXXK_ContractDocument ToKhaiXuat = new SXXK_ContractDocument()
                                        {
                                            CustomsReference = SoToKhaiXuatVNACCS > 0 ? SoToKhaiXuatVNACCS.ToString() : dr["SoToKhaiXuat"].ToString(),
                                            NatureOfTransaction = Globals.IsKhaiVNACCS ? dr["MaLoaiHinhXuat"].ToString().Trim().Replace("XV", "") : dr["MaLoaiHinhXuat"].ToString().Trim(),
                                            Acceptance = Convert.ToDateTime(dr["NgayDangKyXuat"]).ToString(sfmtDateTime),
                                            DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dr["MaHaiQuanXuat"].ToString().Trim()) : dr["MaHaiQuanXuat"].ToString().Trim()
                                        };
                                        NPL.ExportDeclarationDocument.Add(ToKhaiXuat);
                                    }
                                    DSToKhaiNhap.Materials.Add(NPL);
                                }


                            }
                            thanhkhoan.MaterialReExport.ImportDeclarationDocuments.Add(DSToKhaiNhap);

                        }
                    }

                }
                #endregion

                #region Chưa dùng đến
                sobanke = hosothanhly.getBKNPLTamNopThue();
                if (sobanke > -1)
                    #region Danh sách nguyên phụ liệu tạm nộp thuế
                    foreach (BKNPLTamNopThue bkCTL in hosothanhly.BKCollection[sobanke].bkNPLTNTCollection)
                    {
                    }
                    #endregion
                sobanke = hosothanhly.getBKNPLNhapKinhDoanh();
                if (sobanke > -1)
                    #region Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh
                    foreach (BKNPLXuatSuDungNKD bkCTL in hosothanhly.BKCollection[sobanke].bkNPLNKDCollection)
                    {
                    }
                    #endregion
                #endregion Chưa dùng đến
                PreviousCustomsDocument Previoust = new PreviousCustomsDocument();
                Previoust.Code = " ";
                Previoust.Number = " ";

                thanhkhoan.ImportDeclarationList.PreviousCustomsDocument = Previoust;
                thanhkhoan.ExportDeclarationList.PreviousCustomsDocument = Previoust;
                //bang ke Chung tu thanh toan
                #region bang ke chung tu thanh toan


                thanhkhoan.PaymentDocumentList.PreviousCustomsDocument = Previoust;

                #endregion

                thanhkhoan.MaterialExport.PreviousCustomsDocument = Previoust;
                thanhkhoan.MaterialLeft.PreviousCustomsDocument = Previoust;
                thanhkhoan.MaterialNotExport.PreviousCustomsDocument = Previoust;
                thanhkhoan.MaterialReExport.PreviousCustomsDocument = Previoust;
                #region Bang ke NPL nhap xuat ton
                if (Globals.IsKhaiVNACCS)
                {
                    //Báo cáo tổng hợp nhập – xuất – tồn nguyên liệu, vật tư
                    if (Globals.LaDNCX)
                    {
                        lloi += " 3 .Bat dau bang ke NPL Nhap ton \r\n ";
                        Company.BLL.KDT.SXXK.BCXuatNhapTon bcxnt = new BCXuatNhapTon();
                        DataTable dt = bcxnt.GetBC07_KCX_XNTByLanThanhLy(hosothanhly.MaDoanhNghiep, hosothanhly.LanThanhLy, hosothanhly.MaHaiQuanTiepNhan, Convert.ToInt32(hosothanhly.BKCollection[hosothanhly.getBKToKhaiNhap()].ID));
                        Company.KDT.SHARE.Components.Messages.SXXK.SXXK_FinalMaterial FinalGoodsItems = new Company.KDT.SHARE.Components.Messages.SXXK.SXXK_FinalMaterial();
                        FinalGoodsItems.FinalGoodsItem = new List<SXXK_Material>();
                        foreach (DataRow dr in dt.Rows)
                        {
                            SXXK_Material finalGoodsItem = new SXXK_Material()
                            {
                                Commodity = new Commodity()
                                {
                                    Identification = dr["MaNPL"].ToString(),
                                    Description = dr["TenNPL"].ToString(),
                                    Type = "1"
                                },
                                GoodsMeasure = new SXXK_GoodsMeasure()
                                {
                                    Quantity1 = Helpers.FormatNumeric(dr["LuongTonDau"], 4),
                                    Quantity2 = Helpers.FormatNumeric(dr["LuongNhap"], 4),
                                    Quantity3 = Helpers.FormatNumeric(dr["LuongNPLSuDung"], 4),
                                    Quantity4 = Helpers.FormatNumeric(dr["LuongNPLTaiXuat"], 4),
                                    Quantity5 = Helpers.FormatNumeric(dr["LuongTieuHuy"], 4),//Lượng xuất khong mo to khai là luong tieu huy
                                    Quantity6 = Helpers.FormatNumeric(dr["LuongTonCuoi"], 4),
                                    MeasureUnits = new List<string>()
                                    // UnitPrice = Helpers.FormatNumeric(dr["DonGiaTT"], 4)
                                }
                            };
                            finalGoodsItem.GoodsMeasure.MeasureUnits.Add(VNACCS_Mapper.GetCodeVNACC(dr["DVT_ID"].ToString().Trim()));

                            FinalGoodsItems.FinalGoodsItem.Add(finalGoodsItem);

                        }
                        thanhkhoan.FinalGoodsItems = FinalGoodsItems;
                        lloi += " => .Kết thuc bang ke NPL Nhap Ton \r\n ";
                    }
                    else
                    {
                        //Báo cáo tổng hợp nhập – xuất – tồn nguyên liệu, vật tư

                        Company.BLL.KDT.SXXK.BCXuatNhapTon bcxnt = new BCXuatNhapTon();
                        DataTable dt = new DataTable();
                        //if (hosothanhly.MaDoanhNghiep.Trim() == "0600016097")
                        //{
                        //    dt = bcxnt.GetBCXNTByLanThanhLyAllNPL(hosothanhly.MaDoanhNghiep, hosothanhly.LanThanhLy);
                        //}
                        //else
                        dt = bcxnt.GetBCXNTByLanThanhLy(hosothanhly.MaDoanhNghiep, hosothanhly.LanThanhLy);
                        Company.KDT.SHARE.Components.Messages.SXXK.SXXK_FinalMaterial FinalMaterial = new Company.KDT.SHARE.Components.Messages.SXXK.SXXK_FinalMaterial();
                        FinalMaterial.Materials = new List<SXXK_Material>();
                        foreach (DataRow dr in dt.Rows)
                        {
                            SXXK_Material material = new SXXK_Material()
                            {
                                Commodity = new Commodity()
                                {
                                    Identification = dr["MaNPL"].ToString(),
                                    Description = dr["TenNPL"].ToString()
                                },
                                GoodsMeasure = new SXXK_GoodsMeasure()
                                {
                                    Quantity1 = Helpers.FormatNumeric(dr["LuongTonDau"], 4),
                                    Quantity2 = Helpers.FormatNumeric(dr["LuongNhap"], 4),
                                    Quantity3 = Helpers.FormatNumeric(dr["LuongNPLSuDung"], 4),
                                    Quantity4 = Helpers.FormatNumeric(dr["LuongNPLTaiXuat"], 4),
                                    Quantity5 = Helpers.FormatNumeric(dr["LuongNopThue"], 4),
                                    Quantity6 = Helpers.FormatNumeric(dr["LuongXuatTheoHD"], 4),
                                    Quantity7 = Helpers.FormatNumeric(dr["LuongTonCuoi"], 4),
                                    MeasureUnits = new List<string>(),
                                    UnitPrice = Helpers.FormatNumeric(dr["DonGiaTT"], 6),
                                    Tax = Helpers.FormatNumeric(dr["ThueSuat"], 4),
                                }
                            };
                            material.GoodsMeasure.MeasureUnits.Add(VNACCS_Mapper.GetCodeVNACC(dr["DVT_ID"].ToString().Trim()));

                            FinalMaterial.Materials.Add(material);
                            thanhkhoan.FinalMaterial = FinalMaterial;

                        }
                    }

                }
                #endregion
                return thanhkhoan;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(lloi, ex);
                throw ex;
            }
        }

        //class DistinctMaSP : IEqualityComparer<DinhMuc>
        //{
        //    public bool Equals(DinhMuc x, DinhMuc y)
        //    {
        //        return x.MaSanPham == y.MaSanPham;
        //    }

        //    public int GetHashCode(DinhMuc obj)
        //    {
        //        return obj.MaSanPham.GetHashCode();
        //    }
        //}
        //class DistinctMaSPSua : IEqualityComparer<DinhMucSUA>
        //{
        //    public bool Equals(DinhMucSUA x, DinhMucSUA y)
        //    {
        //        return x.MaSanPham == y.MaSanPham;
        //    }

        //    public int GetHashCode(DinhMucSUA obj)
        //    {
        //        return obj.MaSanPham.GetHashCode();
        //    }
        //}





        #region Chế xuất
        public static Company.KDT.SHARE.Components.SXXK_SanPham ToDataTransferObject_DNCX_HangDuaVao(HangDuaVaoDangKy sanphamdangky, string Ghichu, int LoaiChungTu, bool isEdit,bool isCancel, string tenDN)
        {

            Company.KDT.SHARE.Components.SXXK_SanPham sanpham = new Company.KDT.SHARE.Components.SXXK_SanPham
            {
                Issuer = LoaiChungTu.ToString(),

                Reference = sanphamdangky.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = isEdit || isCancel ? sanphamdangky.SoTiepNhan.ToString() : string.Empty,
                Acceptance = isEdit || isCancel ? sanphamdangky.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(sanphamdangky.MaHaiQuan.Trim()) : sanphamdangky.MaHaiQuan.Trim(),
                Agents = new List<Agent>(),
                Product = new List<Product>(),
                AdditionalInformations = new List<AdditionalInformation>(),
            };
            //if (LoaiChungTu == 502)
            //    sanpham.Exporter = new NameBase { Name = "??", Identity = sanphamdangky.MaDoanhNghiep };
            //else
            if (isEdit)
            {
                sanpham.Function = DeclarationFunction.SUA;
            }
            else if (isCancel)
            {
                sanpham.Function = DeclarationFunction.HUY;
            }
            else
	        {
                sanpham.Function = DeclarationFunction.KHAI_BAO;
	        }
            sanpham.Importer = new NameBase 
            { 
                Name = tenDN,
                Identity = sanphamdangky.MaDoanhNghiep 
            };
            sanpham.Agents.Add(new Agent
            {
                Name = tenDN,
                Identity = sanphamdangky.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (sanphamdangky.DanhSachHangDuaVao != null)
                switch (LoaiChungTu)
                {
                    case 561:
                        sanpham.IncomingGoodsItem = new List<Product>();
                        foreach (HangDuaVao item in sanphamdangky.DanhSachHangDuaVao)
                        {
                            sanpham.IncomingGoodsItem.Add(new Product
                            {
                                Commodity = new Commodity 
                                { 
                                    Identification = item.Ma,
                                    Description = item.Ten,
                                    TariffClassification = item.MaHS.Trim(),
                                    Usage = String.IsNullOrEmpty(item.MucDich) ? "01" : item.MucDich ,
                                    Type = String.IsNullOrEmpty(item.LoaiNPL) ? "1" : item.LoaiNPL
                                },
                                GoodsMeasure = new GoodsMeasure
                                {
                                    MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT_ID)
                                }
                            });
                        }
                        break;
                    case 562:
                        sanpham.OutgoingGoodsItem = new List<Product>();
                        foreach (HangDuaVao item in sanphamdangky.DanhSachHangDuaVao)
                        {
                            sanpham.OutgoingGoodsItem.Add(new Product
                            {
                                Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS.Trim(), Type = item.LoaiNPL, Usage = item.MucDich },
                                GoodsMeasure = new GoodsMeasure { MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) }
                            });
                        }
                        break;
                    //                     case 510:
                    //                         sanpham.CustomsGoodsItem = new List<Product>();
                    //                         foreach (HangDuaVao item in sanphamdangky.DanhSachHangDuaVao)
                    //                         {
                    //                             sanpham.CustomsGoodsItem.Add(new Product
                    //                             {
                    //                                 Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, Type = item.LoaiSP },
                    //                                 GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID, Tariff = item.SoLuong.ToString() }
                    //                             });
                    //                         }
                    //                         break;
                    //                     case 512:
                    //                         sanpham.CustomsGoodsItem = new List<Product>();
                    //                         sanpham.time = DateTime.Now.ToString(sfmtDateTime);
                    //                         foreach (HangDuaVao item in sanphamdangky.DanhSachHangDuaVao)
                    //                         {
                    //                             sanpham.CustomsGoodsItem.Add(new Product
                    //                             {
                    //                                 Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, Type = item.LoaiSP },
                    //                                 GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID,/* Tariff = item.SoLuong.ToString(),*/ Quantity = Helpers.FormatNumeric(item.SoLuong, 6) }
                    //                             });
                    //                         }
                    //                         break;
                    //                     case 513:
                    //                         sanpham.CustomsGoodsItem = new List<Product>();
                    //                         sanpham.time = DateTime.Now.ToString(sfmtDateTime);
                    //                         foreach (HangDuaVao item in sanphamdangky.DanhSachHangDuaVao)
                    //                         {
                    //                             sanpham.CustomsGoodsItem.Add(new Product
                    //                             {
                    //                                 Commodity = new Commodity { Identification = item.Ma, Description = item.Ten },
                    //                                 GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID, /* Tariff = item.SoLuong.ToString(),*/ Quantity = Helpers.FormatNumeric(item.SoLuong, 6) }
                    //                             });
                    //                         }
                    //                         break;


                }
            //sanpham.AdditionalInformations.Add(new AdditionalInformation
            //{
            //    Content = new Content 
            //    {
            //        Text = Ghichu 
            //    }
            //});

            return sanpham;
        }
        public static Company.KDT.SHARE.Components.SXXK_SanPham ToDataTransferObject_DNCX_HangDuaRa(SanPhamDangKy sanphamdangky, string Ghichu, bool isKhaiBaoSua, string tenDN)
        {
            isKhaiBaoSua = sanphamdangky.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || sanphamdangky.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET ? true : false;
            Company.KDT.SHARE.Components.SXXK_SanPham sanpham = new Company.KDT.SHARE.Components.SXXK_SanPham
            {
                Issuer = DeclarationIssuer.DNCX_HANG_RA,// LoaiChungTu.ToString(),
                Reference = sanphamdangky.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? sanphamdangky.SoTiepNhan.ToString() : string.Empty,
                Acceptance = isKhaiBaoSua ? sanphamdangky.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(sanphamdangky.MaHaiQuan.Trim()) : sanphamdangky.MaHaiQuan.Trim(),
                Agents = new List<Agent>(),
                Product = new List<Product>(),
                AdditionalInformations = new List<AdditionalInformation>(),
            };
            if (sanphamdangky.TrangThaiXuLy==TrangThaiXuLy.SUATKDADUYET)
            {
                sanpham.Function = DeclarationFunction.SUA;
            }
            else if (sanphamdangky.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                sanpham.Function = DeclarationFunction.HUY;
            }
            else
            {
                sanpham.Function = DeclarationFunction.KHAI_BAO;
            }
            sanpham.Exporter = new NameBase 
            { 
                Name = tenDN,
                Identity = sanphamdangky.MaDoanhNghiep 
            };
            sanpham.Agents.Add(new Agent
            {
                Name = tenDN,
                Identity = sanphamdangky.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (sanphamdangky.SPCollection != null)


                sanpham.OutgoingGoodsItem = new List<Product>();
            foreach (SanPham item in sanphamdangky.SPCollection)
            {
                sanpham.OutgoingGoodsItem.Add(new Product
                {
                    Commodity = new Commodity 
                    { 
                        Description = item.Ten,
                        Identification = item.Ma,
                        TariffClassification = item.MaHS.Trim(),
                        Usage = String.IsNullOrEmpty(item.MucDich) ? "01" : item.MucDich,
                        Type = String.IsNullOrEmpty(item.LoaiSP) ? "2" : item.LoaiSP
                    },
                    GoodsMeasure = new GoodsMeasure 
                    { 
                        MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) 
                    }
                });
            }


            sanpham.AdditionalInformations.Add(new AdditionalInformation
            {
                Content = new Content 
                { 
                    Text = Ghichu 
                }
            });

            return sanpham;
        }

        public static CX_ThanhlyTaiSanCoDinh ToDataTransferObject_TLTSCD(Company.BLL.SXXK.KDT_SXXK_TLTSCDDangKy ThanhLyTSCD, bool isSua, string TenDoanhNghiep)
        {
            if (ThanhLyTSCD.HangThanhLyCollection == null || ThanhLyTSCD.HangThanhLyCollection.Count == 0)
                ThanhLyTSCD.LoadHangCollection();
            CX_ThanhlyTaiSanCoDinh tltscd = new CX_ThanhlyTaiSanCoDinh()
            {
                Issuer = DeclarationIssuer.THANHLY_TSCD,
                Reference = ThanhLyTSCD.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = isSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                IssueLocation = "",
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = isSua ? ThanhLyTSCD.SoTiepNhan.ToString() : string.Empty,
                Acceptance = isSua ? ThanhLyTSCD.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                DeclarationOffice = ThanhLyTSCD.MaHaiQuan.Trim(),
                Agents = new List<Agent>()
                {
                    new Agent 
                    {
                        Identity = ThanhLyTSCD.MaDoanhNghiep,
                        Name = TenDoanhNghiep,
                        Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                   
                    }
                },
                Importer = new NameBase()
                {
                    Name = TenDoanhNghiep,
                    Identity = ThanhLyTSCD.MaDoanhNghiep,

                },
                Time = ThanhLyTSCD.NgayThongBaoTL.ToString(sfmtDate)

            };
            tltscd.CustomsGoodsItemL = new List<Company.KDT.SHARE.Components.Messages.DNCX.CX_CustomsGoodsItem>();
            foreach (Company.BLL.SXXK.KDT_SXXK_TLTSCD item in ThanhLyTSCD.HangThanhLyCollection)
            {
                tltscd.CustomsGoodsItemL.Add(ToDataTransferObject_HangTLTSCD(item));
            }

            return tltscd;
        }
        public static Company.KDT.SHARE.Components.Messages.DNCX.CX_CustomsGoodsItem ToDataTransferObject_HangTLTSCD(Company.BLL.SXXK.KDT_SXXK_TLTSCD TaiSanThanhLy)
        {
            return new Company.KDT.SHARE.Components.Messages.DNCX.CX_CustomsGoodsItem()
            {
                Commodity = new Commodity()
                {
                    Description = TaiSanThanhLy.TenSanPham,
                    Identification = TaiSanThanhLy.MaSanPham
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = Helpers.FormatNumeric(TaiSanThanhLy.SoLuongTL, 4),
                    MeasureUnit = TaiSanThanhLy.DVT_ID
                },
                Reference = new Company.KDT.SHARE.Components.Messages.DNCX.CX_Reference()
                {
                    CustomsReference = TaiSanThanhLy.SoToKhai,
                    NatureOfTransaction = TaiSanThanhLy.MaLoaiHinhTK,
                    Acceptance = TaiSanThanhLy.NgayDangKyTK.ToString(sfmtDate),
                    DeclarationOffice = TaiSanThanhLy.MaHaiQuanTK
                }
            };



        }
        public static Company.KDT.SHARE.Components.SXXK_SanPham ToDataTransferObject_DNCX_HangTonKho(KDT_SXXK_BKHangTonKhoDangKy hangtondangky, string Ghichu, string LoaiChungTu, bool isKhaiBaoSua, string tenDN, long BangkeHSTL_ID)
        {

            Company.KDT.SHARE.Components.SXXK_SanPham sanpham = new Company.KDT.SHARE.Components.SXXK_SanPham
            {
                Issuer = LoaiChungTu.ToString(),
                Reference = hangtondangky.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hangtondangky.MaHaiQuan.Trim()),
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? hangtondangky.SoTiepNhan.ToString() : string.Empty,
                Acceptance = isKhaiBaoSua ? hangtondangky.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                FinalYear = hangtondangky.NamQuyetToan.ToString(),
                FinalQuarter = hangtondangky.QuyQuyetToan.ToString(),
                Agents = new List<Agent>(),
                Product = new List<Product>(),
                AdditionalInformations = new List<AdditionalInformation>(),
            };
            //if (LoaiChungTu == 502)
            //    sanpham.Exporter = new NameBase { Name = "??", Identity = hangtondangky.MaDoanhNghiep };
            //else
            sanpham.Importer = new NameBase { Name = tenDN, Identity = hangtondangky.MaDoanhNghiep };

            sanpham.Function = DeclarationFunction.KHAI_BAO;
            sanpham.Agents.Add(new Agent
            {
                Name = tenDN,
                Identity = hangtondangky.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            BCXuatNhapTon bcxnt = new BCXuatNhapTon();
            DataTable dt = bcxnt.GetBC07_KCX_XNTByLanThanhLy(hangtondangky.MaDoanhNghiep, hangtondangky.LanThanhLy, hangtondangky.MaHaiQuan, Convert.ToInt32(BangkeHSTL_ID));
            sanpham.CustomsGoodsItem = new List<Product>();
            foreach (DataRow dr in dt.Rows)
            {
                sanpham.CustomsGoodsItem.Add(new Product
                {
                    Commodity = new Commodity
                    {
                        Identification = dr["MaNPL"].ToString(),
                        Description = dr["TenNPL"].ToString(),
                        Type = "1"
                    },
                    GoodsMeasure = new GoodsMeasure
                    {
                        Quantity = Helpers.FormatNumeric(Convert.ToDecimal(dr["LuongTonCuoi"].ToString()), 6),
                        Quantity1 = Helpers.FormatNumeric(Convert.ToDecimal(dr["LuongTonCuoi"].ToString()), 6),
                        Quantity2 = Helpers.FormatNumeric(Convert.ToDecimal(dr["LuongTonCuoi"].ToString()), 6),
                        MeasureUnit = VNACCS_Mapper.GetCodeVNACC(dr["DVT_ID"].ToString().Trim())
                    }
                });
            }
            //if (hangtondangky.DanhSachHangDuaVao != null)


            //            sanpham.IncomingGoodsItem = new List<Product>();
            //foreach (HangDuaVao item in hangtondangky.DanhSachHangDuaVao)
            //{
            //    sanpham.IncomingGoodsItem.Add(new Product
            //    {
            //        Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS.Trim(), Type = item.LoaiNPL, Usage = item.MucDich },
            //        GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID }
            //    });
            //}
            return sanpham;
        }

        public static SXXK_HangTon ToDataTransferHangTon(KDT_SXXK_HangTonDangKy HangTonDK, string TenDoanhNghiep, bool isKhaiBaoSua, bool khaibaohuy)
        {
            SXXK_HangTon hangton = new SXXK_HangTon()
            {
                Issuer = DeclarationIssuer.SXCX_HangTon,
                Function = isKhaiBaoSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                Reference = HangTonDK.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                DeclarationOffice = HangTonDK.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? Helpers.FormatNumeric(HangTonDK.SoTiepNhan) : "", //isKhaiSua ? Helpers.FormatNumeric(pkdk.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiBaoSua ? HangTonDK.NgayTiepNhan.ToString(sfmtDateTime) : "", //isKhaiSua ? pkdk.NgayTiepNhan.ToString(                ) : string.Empty,
                Importer = new NameBase() { Identity = HangTonDK.MaDoanhNghiep, Name = TenDoanhNghiep },
                FinalYear = HangTonDK.NamQuyetToan.ToString(),
                FinalQuarter = HangTonDK.QuyQuyetToan.ToString(),
                Agents = new List<Agent>(),
                CustomsGoodsItems = new List<CustomsGoodsItem>(),
            };
            if (khaibaohuy)
            {
                hangton.Issue = HangTonDK.NgayTiepNhan.ToString(sfmtDateTime);
                hangton.Function = "1";
                hangton.FinalYear = HangTonDK.NamQuyetToan.ToString();
                hangton.FinalQuarter = HangTonDK.QuyQuyetToan.ToString();
                hangton.CustomsReference = HangTonDK.SoTiepNhan.ToString();
                hangton.Acceptance = HangTonDK.NgayTiepNhan.ToString(sfmtDate);
            }
            hangton.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = HangTonDK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            foreach (KDT_SXXK_HangTon item in HangTonDK.hangTonList)
            {
                CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem()
                {
                    Commodity = new Commodity()
                    {
                        Description = item.TenHang,
                        Identification = item.MaHang,
                        Type = item.LoaiHang.ToString()
                    },
                    GoodsMeasure = new GoodsMeasure()
                    {
                        Quantity1 = Helpers.FormatNumeric(item.LuongTonSoSach, 4),
                        Quantity2 = Helpers.FormatNumeric(item.LuongTonThucTe, 4),
                        MeasureUnit = item.DVT_ID
                    }
                };
                hangton.CustomsGoodsItems.Add(customsGoodsItem);
            }

            return hangton;
        }
        public static SXXK_BaoCaoChotTon ToDataTransferHangTon(KDT_VNACCS_TotalInventoryReport totalInventoryReport, string TenDoanhNghiep,string DiaChi)
        {
            SXXK_BaoCaoChotTon SXXK_BaoCaoChotTon = new SXXK_BaoCaoChotTon()
            {
                Issuer = DeclarationIssuer.SXCX_HangTon,
                Function = DeclarationFunction.KHAI_BAO,
                Reference = totalInventoryReport.GuidString,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                DeclarationOffice = totalInventoryReport.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(totalInventoryReport.SoTiepNhan),
                Acceptance = totalInventoryReport.NgayTiepNhan.ToString(sfmtDateTime),
                Importer = new NameBase() 
                { 
                    Identity = totalInventoryReport.MaDoanhNghiep,
                    Name = TenDoanhNghiep ,
                    Address = DiaChi,
                },
                FinishDate = totalInventoryReport.NgayChotTon.ToString(sfmtDate),
                Type = totalInventoryReport.LoaiBaoCao.ToString(),
                Agents = new List<Agent>(),
                GoodsItems = new List<CustomsGoodsItem>(),
            };
            SXXK_BaoCaoChotTon.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = totalInventoryReport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            foreach (KDT_VNACCS_TotalInventoryReport_Detail item in totalInventoryReport.DetailCollection)
            {
                CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem()
                {                 
                };
                SXXK_BaoCaoChotTon.GoodsItems.Add(customsGoodsItem);
            }

            return SXXK_BaoCaoChotTon;
        }
        public static SXXK_HosoThanhKhoan ToDataTransferBKTaiXuat(KDT_CX_TaiXuatDangKy TieuHuyDK, string quyQuyetToan, string TenDoanhNghiep, bool isKhaiBaoSua)
        {
            SXXK_HosoThanhKhoan TaiXuat_XML = new SXXK_HosoThanhKhoan()
          {
              Issuer = DeclarationIssuer.BKTaiXuatKCX,
              Function = DeclarationFunction.KHAI_BAO,
              Issue = DateTime.Now.ToString(sfmtDate),
              IssueLocation = string.Empty,
              DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TieuHuyDK.MaHaiQuan.Trim()),
              Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
              CustomsReference = string.Empty,
              Acceptance = string.Empty,
              Agents = new List<Agent>(),

              Importer = new NameBase()
              {
                  Name = TenDoanhNghiep,
                  Identity = TieuHuyDK.MaDoanhNghiep.Trim()
              },
              Reference = TieuHuyDK.GUIDSTR,

              MaterialReExport = new DocumentList()
              {
                  ExportDeclarationDocument = new List<SXXK_ContractDocument>(),
              }
          };
            string[] str = quyQuyetToan.Split('/');
            TaiXuat_XML.FinalQuarter = str[0].ToString();
            TaiXuat_XML.FinalYear = str[1].ToString();
            TaiXuat_XML.Agents.Add(new Agent()
            {
                Name = TenDoanhNghiep,
                Identity = TieuHuyDK.MaDoanhNghiep.Trim(),
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN

            });
            foreach (KDT_CX_TaiXuat item in TieuHuyDK.TaiXuatList)
            {
                bool isTrungToKhai = false;
                foreach (SXXK_ContractDocument tk in TaiXuat_XML.MaterialReExport.ExportDeclarationDocument)
                {
                    if (tk.CustomsReference == item.SoToKhai.ToString())
                    {
                        isTrungToKhai = true;
                        break;
                    }
                }
                if (isTrungToKhai)
                    continue;
                List<KDT_CX_TaiXuat> listTX = new List<KDT_CX_TaiXuat>();
                listTX = TieuHuyDK.TaiXuatList.FindAll(x => x.SoToKhai == item.SoToKhai);
                Company.KDT.SHARE.Components.SXXK_ContractDocument tkcu = new Company.KDT.SHARE.Components.SXXK_ContractDocument()
                {
                    CustomsReference = item.SoToKhai.ToString(),
                    NatureOfTransaction = item.MaLoaiHinh.Contains("V") == true ? item.MaLoaiHinh.Trim().Substring(2) : item.MaLoaiHinh.Trim(),
                    Acceptance = item.NgayDangKy.ToString(sfmtDateTime),
                    DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item.MaHaiQuan),
                    Materials = new List<SXXK_Product>(),
                };
                foreach (KDT_CX_TaiXuat tx in listTX)
                {
                    SXXK_Product Material = new SXXK_Product()
                        {
                            Commoditys = new List<Commodity>(),
                            GoodsMeasures = new List<SXXK_GoodsMeasure>()
                        };
                    Commodity com = new Commodity()
                    {
                        Identification = tx.MaHang,
                        Description = tx.TenHang,
                        Type = tx.LoaiHang.ToString()
                    };
                    Material.Commoditys.Add(com);
                    SXXK_GoodsMeasure Good = new SXXK_GoodsMeasure()
                    {
                        Quantity = Helpers.FormatNumeric(tx.LuongTaiXuat, 4),
                        MeasureUnits = new List<string>(),
                        Content = tx.Temp
                    };
                    Good.MeasureUnits.Add(tx.DVT);
                    Material.GoodsMeasures.Add(Good);
                    tkcu.Materials.Add(Material);
                }

                TaiXuat_XML.MaterialReExport.ExportDeclarationDocument.Add(tkcu);
            }
            return TaiXuat_XML;
        }
        #endregion
        #region Bang ke NPL tieu huy
        public static SXXK_HosoThanhKhoan ToDataTransferBKTieuHuy(KDT_CX_NPLXinHuyDangKy TieuHuyDK, string quyQuyetToan, string TenDoanhNghiep, bool isKhaiBaoSua)
        {
            SXXK_HosoThanhKhoan TaiXuat_XML = new SXXK_HosoThanhKhoan()
          {
              Issuer = DeclarationIssuer.BKTieuHuyKCX,
              Function = DeclarationFunction.KHAI_BAO,
              Issue = DateTime.Now.ToString(sfmtDate),
              IssueLocation = string.Empty,
              DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TieuHuyDK.MaHaiQuan.Trim()),
              Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
              CustomsReference = string.Empty,
              Acceptance = string.Empty,
              Agents = new List<Agent>(),
              time = TieuHuyDK.NgayThongBaoHuy.ToString(sfmtDate),

              Importer = new NameBase()
              {
                  Name = TenDoanhNghiep,
                  Identity = TieuHuyDK.MaDoanhNghiep.Trim(),
                  //
              },
              Reference = TieuHuyDK.GUIDSTR,
              CustomsGoodsItems = new List<Company.KDT.SHARE.Components.Messages.DNCX.CustomsGoodsItem_BC>()

          };
            string[] str = quyQuyetToan.Split('/');
            TaiXuat_XML.FinalQuarter = str[0].ToString();
            TaiXuat_XML.FinalYear = str[1].ToString();
            TaiXuat_XML.Agents.Add(new Agent()
            {
                Name = TenDoanhNghiep,
                Identity = TieuHuyDK.MaDoanhNghiep.Trim(),
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN

            });
            foreach (KDT_CX_NPLXinHuy item in TieuHuyDK.XinHuyList)
            {
                Company.KDT.SHARE.Components.Messages.DNCX.CustomsGoodsItem_BC TieuHuy = new Company.KDT.SHARE.Components.Messages.DNCX.CustomsGoodsItem_BC();
                TieuHuy.Commodity = new Commodity()
                {
                    Description = item.TenHang,
                    Identification = item.MaHang,
                    Type = item.LoaiHang.ToString()
                };
                TieuHuy.GoodsMeasure = new Company.KDT.SHARE.Components.Messages.DNCX.GoodsMeasure_BC()
                {
                    Quantitys = new List<string>(),
                    MeasureUnit = item.DVT
                };
                TieuHuy.GoodsMeasure.Quantitys.Add(Helpers.FormatNumeric(item.LuongTieuHuy, 4));
                TieuHuy.reference = new DeclarationBase()
                {
                    CustomsReference = item.SoToKhai.ToString(),
                    NatureOfTransaction = item.MaLoaiHinh.Substring(2, 3),
                    Acceptance = item.NgayDangKy.ToString(sfmtDate),
                    DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item.MaHaiQuan)

                };
                TaiXuat_XML.CustomsGoodsItems.Add(TieuHuy);
            }
            return TaiXuat_XML;
        }
        #endregion
        public static PhuKienToKhai ToDataTransferPhuKienToKhai(KDT_SXXK_PhuKienDangKy pkdk, string TenDoanhNghiep)
        {
            PhuKienToKhai phukien = new PhuKienToKhai()
            {
                Issuer = DeclarationIssuer.PHUKIEN_TOKHAI,
                Function = DeclarationFunction.KHAI_BAO,
                Reference = pkdk.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                DeclarationOffice = pkdk.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = pkdk.SoToKhai, //isKhaiSua ? Helpers.FormatNumeric(pkdk.SoTiepNhan) : string.Empty,
                Acceptance = pkdk.NgayDangKy.ToString(sfmtDate), //isKhaiSua ? pkdk.NgayTiepNhan.ToString(                ) : string.Empty,
                NatureOfTransaction = pkdk.MaLoaiHinh,
                Importer = new NameBase() { Identity = pkdk.MaDoanhNghiep, Name = TenDoanhNghiep },
                Agents = new List<Agent>(),

            };
            // Người Khai Hải Quan
            phukien.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = pkdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            //             phukien.Agents.Add(new Agent
            //             {
            //                 Name = hopdong.TenDoanhNghiep,
            //                 Identity = hopdong.MaDoanhNghiep,
            //                 Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            //             });
            //             phukien.Importer = new NameBase()
            //             {
            //                 Identity = hopdong.MaDoanhNghiep,
            //                 Name = hopdong.TenDoanhNghiep
            //             };
            phukien.SubContract = new Subcontract()
            {
                Reference = pkdk.SoPhuKien,
                Decription = pkdk.GhiChu,
                Issue = pkdk.NgayPhuKien.ToString(sfmtDate)
            };
            phukien.AdditionalInformations = new List<PhuKienToKhai_AdditionalInformation>();
            foreach (KDT_SXXK_LoaiPhuKien loaiPK in pkdk.ListPhuKien)
            {
                //if (loaiPK. == null || loaiPK.HPKCollection.Count == 0)
                //    loaiPK.LoadCollection();
                PhuKienToKhai_AdditionalInformation additionalInformation = new PhuKienToKhai_AdditionalInformation()
                {
                    Statement = loaiPK.MaPhuKien.Trim()
                };
                switch (loaiPK.MaPhuKien.Trim())
                {
                    #region Phụ kiện sửa thông tin ngày đưa vào thanh khoản (501)
                    case "501":
                        {
                            additionalInformation.Content = new PhuKienToKhai_Content
                            {
                                Declaration = new DeclarationPhuKienToKhai()
                                {
                                    NewClearance = loaiPK.ListPhuKienDeTail[0].NgayTKSuaDoi.ToString(sfmtDate)
                                }
                            };
                            if (loaiPK.ListPhuKienDeTail[0].NgayTKHeThong.Year > 2000)
                            {
                                additionalInformation.Content.Declaration.OldClearance = loaiPK.ListPhuKienDeTail[0].NgayTKHeThong.ToString(sfmtDate);
                            }
                            break;
                        }
                    #endregion

                    #region Phụ kiện sửa thông tin hợp đồng của tờ khai(502)
                    case "502":
                        {
                            additionalInformation.Content = new PhuKienToKhai_Content
                            {
                                Declaration = new DeclarationPhuKienToKhai()
                                {
                                    NewContractDocument = new DeclarationContract()
                                    {
                                        NewIssue = loaiPK.ListPhuKienDeTail[0].NgayHopDongMoi.ToString(sfmtDate),
                                        NewReference = loaiPK.ListPhuKienDeTail[0].SoHopDongMoi
                                    }
                                }
                            };
                            if (!string.IsNullOrEmpty(loaiPK.ListPhuKienDeTail[0].SoHopDongCu))
                            {
                                additionalInformation.Content.Declaration.NewContractDocument = new DeclarationContract()
                                {
                                    OldReference = loaiPK.ListPhuKienDeTail[0].SoHopDongCu,
                                    OldIssue = loaiPK.ListPhuKienDeTail[0].NgayHopDongCu.ToString(sfmtDate)
                                };
                            }
                            break;
                        }
                    #endregion

                    #region Phụ kiện sửa thông tin mã hàng của tờ khai(503)
                    case "503":
                        {
                            additionalInformation.Content = new PhuKienToKhai_Content
                           {
                               Declaration = new DeclarationPhuKienToKhai()
                               {
                                   GoodsShipment = new PhuKienGoodsShipment() { CustomsGoodsItem = new List<PhuKienCustomsGoodsItem>() }
                               }
                           };
                            foreach (KDT_SXXK_PhuKienDetail item in loaiPK.ListPhuKienDeTail)
                            {
                                PhuKienCustomsGoodsItem pkdetail = new PhuKienCustomsGoodsItem
                                   {
                                       sequence = item.STTHang.ToString(),
                                       NewCommodity = new PhuKien_NewCommodity()
                                       {
                                           NewIdentification = item.MaHangMoi.Trim(),
                                           NewRegisterCustoms = item.MaHQDangKyMoi.Trim(),
                                           NewType = item.LoaiHangMoi.Trim()
                                       }
                                   };
                                if (!string.IsNullOrEmpty(item.MaHangCu.Trim()))
                                {
                                    pkdetail.OldCommodity = new PhuKien_OldCommodity
                                    {
                                        OldIdentification = item.MaHangCu.Trim(),
                                        //OldRegisterCustoms = item.MaHQDangKyCu.Trim(),
                                        //Lỗi sai mã hải quan
                                        OldRegisterCustoms = item.MaHQDangKyCu,
                                        OldType = item.LoaiHangCu.Trim()
                                    };
                                }

                                additionalInformation.Content.Declaration.GoodsShipment.CustomsGoodsItem.Add(pkdetail);
                            }

                            break;
                        }
                    #endregion

                    #region Phụ kiện sửa thông tin số lượng hàng của tờ khai(504)
                    case "504":
                        {
                            additionalInformation.Content = new PhuKienToKhai_Content
                            {
                                Declaration = new DeclarationPhuKienToKhai()
                                {
                                    GoodsShipment = new PhuKienGoodsShipment() { CustomsGoodsItem = new List<PhuKienCustomsGoodsItem>() }
                                }
                            };
                            foreach (KDT_SXXK_PhuKienDetail item in loaiPK.ListPhuKienDeTail)
                            {
                                PhuKienCustomsGoodsItem pkdetail = new PhuKienCustomsGoodsItem
                                {
                                    sequence = item.STTHang.ToString(),
                                    Commodity = new PhuKien_Commodity()
                                    {
                                        Identification = item.MaHangCu.Trim(),

                                    },
                                    GoodsMeasure = new PhuKien_GoodsMeasure()
                                    {
                                        OldQuantity = Helpers.FormatNumeric(item.SoLuongCu, 4),
                                        NewQuantity = Helpers.FormatNumeric(item.SoLuongMoi, 4)
                                    }
                                };
                                additionalInformation.Content.Declaration.GoodsShipment.CustomsGoodsItem.Add(pkdetail);
                            }

                            break;
                        }
                    #endregion

                    #region Phụ kiện sửa thông tin đon vị tính hàng của tờ khai(505)
                    case "505":
                        {
                            additionalInformation.Content = new PhuKienToKhai_Content
                            {
                                Declaration = new DeclarationPhuKienToKhai()
                                {
                                    GoodsShipment = new PhuKienGoodsShipment() { CustomsGoodsItem = new List<PhuKienCustomsGoodsItem>() }
                                }
                            };
                            foreach (KDT_SXXK_PhuKienDetail item in loaiPK.ListPhuKienDeTail)
                            {
                                PhuKienCustomsGoodsItem pkdetail = new PhuKienCustomsGoodsItem
                                {
                                    sequence = item.STTHang.ToString(),
                                    Commodity = new PhuKien_Commodity()
                                    {
                                        Identification = item.MaHangCu.Trim(),

                                    },
                                    GoodsMeasure = new PhuKien_GoodsMeasure()
                                    {
                                        OldMeasureUnit = item.DVTCu,
                                        NewMeasureUnit = item.DVTMoi.Trim()
                                    }
                                };
                                additionalInformation.Content.Declaration.GoodsShipment.CustomsGoodsItem.Add(pkdetail);
                            }

                            break;
                        }
                    #endregion
                    default:
                        break;
                }
                phukien.AdditionalInformations.Add(additionalInformation);
            }
            return phukien;
        }

        public static SXXK_TuCungUng ToDataTransferTuCungUng(KDT_SXXK_BKNPLCungUngDangKy CUdk, string TenDoanhNghiep, bool isKhaiBaoSua)
        {
            SXXK_TuCungUng TCU = new SXXK_TuCungUng()
            {
                Issuer = DeclarationIssuer.SXXK_TUCUNGUNG,
                Function = isKhaiBaoSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                Reference = CUdk.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(CUdk.MaHaiQuan.Trim()),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? Helpers.FormatNumeric(CUdk.SoTiepNhan) : "", //isKhaiSua ? Helpers.FormatNumeric(pkdk.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiBaoSua ? CUdk.NgayTiepNhan.ToString(sfmtDateTime) : "", //isKhaiSua ? pkdk.NgayTiepNhan.ToString(                ) : string.Empty,
                Importer = new NameBase() { Identity = CUdk.MaDoanhNghiep, Name = TenDoanhNghiep },
                Agents = new List<Agent>(),
            };
            TCU.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = CUdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            TCU.MaterialSupply = new SXXK_MaterialSupply();
            TCU.MaterialSupply.DeclarationDocument = new List<DeclarationDocument>();

            foreach (KDT_SXXK_BKNPLCungUng item in CUdk.TKCungUng_List)
            {
                bool isTrungToKhai = false;
                foreach (DeclarationDocument tk in TCU.MaterialSupply.DeclarationDocument)
                {
                    if (tk.CustomsReference == item.SoToKhaiXuat.ToString())
                    {
                        string tempMaLoaiHinh = item.MaLoaiHinh.Contains("V") == true ? item.MaLoaiHinh.Trim().Substring(2) : item.MaLoaiHinh.Trim();
                        if (tk.NatureOfTransaction == tempMaLoaiHinh)
                        {
                            isTrungToKhai = true;
                            break;
                        }
                    }
                }
                if (isTrungToKhai)
                    continue;
                List<KDT_SXXK_BKNPLCungUng> listSp = new List<KDT_SXXK_BKNPLCungUng>();
                //if(item.MaLoaiHinh.Contains("V"))
                listSp = CUdk.TKCungUng_List.FindAll(x => x.SoToKhaiXuat == item.SoToKhaiXuat && x.MaLoaiHinh.Trim() == item.MaLoaiHinh.Trim());
                //else
                //    listSp = CUdk.TKCungUng_List.FindAll(x => x.SoToKhaiXuat == item.SoToKhaiXuat && x.MaLoaiHinh.Trim() == item.MaLoaiHinh);
                Company.KDT.SHARE.Components.DeclarationDocument tkcu = new Company.KDT.SHARE.Components.DeclarationDocument()
                {
                    CustomsReference = item.SoToKhaiXuat.ToString(),
                    NatureOfTransaction = item.MaLoaiHinh.Contains("V") == true ? item.MaLoaiHinh.Trim().Substring(2) : item.MaLoaiHinh.Trim(),
                    Acceptance = item.NgayDangKy.ToString(sfmtDateTime),
                    DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item.MaHaiQuan),
                    Product = new List<SXXK_MaterialSupply_Product>(),
                };
                foreach (KDT_SXXK_BKNPLCungUng item2 in listSp)
                {
                    SXXK_MaterialSupply_Product spcu = new SXXK_MaterialSupply_Product()
                {
                    RegisterCustoms = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item2.MaHaiQuan.Trim()),
                    Identification = item2.MaSanPham.Trim(),
                    Description = item2.TenSanPham.Trim(),
                    MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item2.DVT_ID.Trim()),
                    Material = new List<SXXK_MaterialSupply_Material>()

                };
                    foreach (KDT_SXXK_BKNPLCungUng_Detail npl in item2.Npl_Detail_List)
                    {
                        SXXK_MaterialSupply_Material nplCu = new SXXK_MaterialSupply_Material()
                        {
                            Identification = npl.MaNPL.Trim(),
                            Description = npl.TenNPL.Trim(),
                            Group = npl.NhomLyDo.ToString(),
                            Content = npl.GiaiTrinh.Trim(),
                            DetailMaterial = new SXXK_MaterialSupply_DetailMaterial()
                            {
                                CustomsReference = npl.SoChungTu,
                                Acceptance = npl.NgayChungTu.ToString(sfmtDateTime)
                            },
                            GoodsMeasure = new GoodsMeasure()
                            {
                                Quantity = Helpers.FormatNumeric(npl.LuongNPL, 4),
                                MeasureUnit = VNACCS_Mapper.GetCodeVNACC(npl.DVT_ID.Trim()),
                                Tax = Helpers.FormatNumeric(npl.ThueXNK, 4),
                                UnitPrice = Helpers.FormatNumeric(npl.DonGiaTT, 6),
                            }
                        };
                        spcu.Material.Add(nplCu);
                    }
                    tkcu.Product.Add(spcu);
                }
                TCU.MaterialSupply.DeclarationDocument.Add(tkcu);
            }
            return TCU;
        }

        public static CapSoDinhDanh_VNACCS ToDataTransferCapSoDinhDanh(KDT_VNACCS_CapSoDinhDanh capSoDinhDanh, string TenDoanhNghiep)
        {
            CapSoDinhDanh_VNACCS CapSoDinhDanh_VNACCS = new CapSoDinhDanh_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = capSoDinhDanh.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = capSoDinhDanh.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
                RequestType = new IssueBase()
                {
                    Type = capSoDinhDanh.LoaiDoiTuong.ToString(),
                },
                TransportEquipmentType = new IssueBase()
                {
                    Type = capSoDinhDanh.LoaiTTHH.ToString(),
                },
                CustomsImporter = new NameBase()
                {
                    Identity = capSoDinhDanh.MaDoanhNghiep,
                }
            };
            // Người Khai Hải Quan
            CapSoDinhDanh_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = capSoDinhDanh.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            CapSoDinhDanh_VNACCS.Issuer = DeclarationIssuer.DinhDanh;
            CapSoDinhDanh_VNACCS.DeclarationOffice = capSoDinhDanh.MaHQ.Trim();
            CapSoDinhDanh_VNACCS.CustomsReference = capSoDinhDanh.SoTiepNhan.ToString();
            CapSoDinhDanh_VNACCS.Acceptance = capSoDinhDanh.NgayTiepNhan.ToString(sfmtDateTime);

            return CapSoDinhDanh_VNACCS;
        }

        public static Company.KDT.SHARE.Components.Container_VNACCS ToDataTransferContainer(KDT_ContainerDangKy ContDK, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep, bool KVGS)
        {
            Company.KDT.SHARE.Components.Container_VNACCS Cont = new Company.KDT.SHARE.Components.Container_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = ContDK.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = ContDK.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Cont.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = ContDK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            if (KVGS)
            {
                Cont.Issuer = DeclarationIssuer.Container_KVGS;
                Cont.CustomsReference = TKMD.SoToKhai.ToString();
                Cont.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
                Cont.DeclarationOffice = TKMD.CoQuanHaiQuan.Trim();
                Cont.NatureOfTransaction = TKMD.MaLoaiHinh;
            }
            else
            {
                Cont.Issuer = DeclarationIssuer.Container;
                Cont.DeclarationOffice = ContDK.MaHQ.Trim();
                Cont.CustomsReference = string.Empty;
                Cont.Acceptance = string.Empty;

                ////Phiph
                //Cont.DeclarationDocument = new DeclarationDocument()
                //{
                //    Reference = TKMD.SoToKhai.ToString(),
                //    Issue = TKMD.NgayDangKy.ToString(sfmtDate),
                //    NatureOfTransaction = TKMD.MaLoaiHinh,
                //    DeclarationOffice = TKMD.CoQuanHaiQuan
                //};
                //TransportEquipments transportEquipments = new TransportEquipments()
                //{
                //    TransportEquipment = new List<TransportEquipment>()
                //};
                //foreach (KDT_ContainerBS item in ContDK.ListCont)
                //{
                //    transportEquipments.TransportEquipment.Add(new TransportEquipment
                //    {
                //        BillOfLading = item.SoVanDon,
                //        Container = item.SoContainer,
                //        Seal = item.SoSeal,
                //        Content = item.GhiChu
                //    });
                //}
                //Cont.TransportEquipments = transportEquipments;
                ////Phiph

            }
            //minhnd 21/03/2015
            Cont.DeclarationDocument = new Company.KDT.SHARE.Components.Messages.SXXK.Container_DeclarationDocument()
            {
                Reference = TKMD.SoToKhai.ToString(),
                Issue = TKMD.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = TKMD.MaLoaiHinh,
                DeclarationOffice = TKMD.CoQuanHaiQuan
            };
            TransportEquipments transportEquipments = new TransportEquipments()
            {
                TransportEquipment = new List<TransportEquipment>()
            };
            foreach (KDT_ContainerBS item in ContDK.ListCont)
            {
                transportEquipments.TransportEquipment.Add(new TransportEquipment
                {
                    BillOfLading = item.SoVanDon,
                    Container = item.SoContainer,
                    Seal = item.SoSeal,
                    Content = item.GhiChu
                });
            }
            Cont.TransportEquipments = transportEquipments;
            //minhnd 21/03/2015
            return Cont;
        }

        public static CertificateOfOrigins_VNACCS ToDataTransferCertificateOfOrigin(KDT_VNACCS_CertificateOfOrigin certificateOfOrigin, KDT_VNACCS_CertificateOfOrigin_Detail certificateOfOriginDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            CertificateOfOrigins_VNACCS certificateOfOrigins_VNACCS = new CertificateOfOrigins_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = certificateOfOrigin.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = certificateOfOrigin.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            certificateOfOrigins_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = certificateOfOrigin.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                certificateOfOrigins_VNACCS.Function = DeclarationFunction.SUA;
                certificateOfOrigins_VNACCS.CustomsReference = certificateOfOrigin.SoTN.ToString();
                certificateOfOrigins_VNACCS.Acceptance = certificateOfOrigin.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                certificateOfOrigins_VNACCS.Function = DeclarationFunction.HUY;
                certificateOfOrigins_VNACCS.CustomsReference = certificateOfOrigin.SoTN.ToString();
                certificateOfOrigins_VNACCS.Acceptance = certificateOfOrigin.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                certificateOfOrigins_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                certificateOfOrigins_VNACCS.CustomsReference = string.Empty;
                certificateOfOrigins_VNACCS.Acceptance = string.Empty;
            }
            certificateOfOrigins_VNACCS.Issuer = DeclarationIssuer.CertificateOfOrigin;
            certificateOfOrigins_VNACCS.DeclarationOffice = certificateOfOrigin.MaHQ.Trim();
            //certificateOfOrigins_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //certificateOfOrigins_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);

            if (FormType == "TKMD")
            {
                certificateOfOrigins_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                certificateOfOrigins_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            CertificateOfOriginsNew certificateOfOrigins = new CertificateOfOriginsNew()
            {
                CertificateOfOriginNew = new List<CertificateOfOriginNew>()
            };
            foreach (KDT_VNACCS_CertificateOfOrigin_Detail item in certificateOfOrigin.CertificateOfOriginCollection)
            {
                certificateOfOrigins.CertificateOfOriginNew.Add(new CertificateOfOriginNew
                {
                    Reference = item.SoCO,
                    Type = item.LoaiCO,
                    Issuer = item.ToChucCapCO,
                    Issue = item.NgayCapCO.ToString(sfmtDate),
                    IssueLocation = item.NuocCapCO,
                    Representative = item.NguoiCapCO,
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = certificateOfOrigin.FileName,
                        Content = certificateOfOrigin.Content
                    }
                });
            }
            certificateOfOrigins_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument()
            {
                Content = certificateOfOrigin.GhiChuKhac
            };
            certificateOfOrigins_VNACCS.CertificateOfOriginsNew = certificateOfOrigins;
            return certificateOfOrigins_VNACCS;
        }
        public static Licenses_VNACCS ToDataTransferLicense(KDT_VNACCS_License license, KDT_VNACCS_License_Detail licenseDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            Licenses_VNACCS Licenses_VNACCS = new Licenses_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = license.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = license.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Licenses_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = license.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                Licenses_VNACCS.Function = DeclarationFunction.SUA;
                Licenses_VNACCS.CustomsReference = license.SoTN.ToString();
                Licenses_VNACCS.Acceptance = license.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                Licenses_VNACCS.Function = DeclarationFunction.HUY;
                Licenses_VNACCS.CustomsReference = license.SoTN.ToString();
                Licenses_VNACCS.Acceptance = license.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                Licenses_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                Licenses_VNACCS.CustomsReference = license.SoTN.ToString();
                Licenses_VNACCS.Acceptance = license.NgayTN.ToString(sfmtDateTime);
            }
            Licenses_VNACCS.Issuer = DeclarationIssuer.License;
            Licenses_VNACCS.DeclarationOffice = license.MaHQ.Trim();
            //Licenses_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //Licenses_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType == "TKMD")
            {
                Licenses_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                Licenses_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }

            Licenses licenses = new Licenses()
            {
                License = new List<Company.KDT.SHARE.Components.Messages.Vouchers.License>()
            };
            foreach (KDT_VNACCS_License_Detail item in license.LicenseCollection)
            {
                licenses.License.Add(new Company.KDT.SHARE.Components.Messages.Vouchers.License
                {
                    Issuer = item.NguoiCapGP,
                    Reference = item.SoGP,
                    Issue = item.NgayCapGP.ToString(sfmtDate),
                    IssueLocation = item.NoiCapGP,
                    Category = item.LoaiGP,
                    Expire = item.NgayHetHanGP.ToString(sfmtDate),
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = license.FileName,
                        Content = license.Content
                    }
                });
            }
            Licenses_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = license.GhiChuKhac
            };

            Licenses_VNACCS.Licenses = licenses;
            return Licenses_VNACCS;
        }
        public static Contract_VNACCS ToDataTransferContract(KDT_VNACCS_ContractDocument contractDocument, KDT_VNACCS_ContractDocument_Detail contractDocumentDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            Contract_VNACCS Contract_VNACCS = new Contract_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = contractDocument.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = contractDocument.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Contract_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = contractDocument.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                Contract_VNACCS.Function = DeclarationFunction.SUA;
                Contract_VNACCS.CustomsReference = contractDocument.SoTN.ToString();
                Contract_VNACCS.Acceptance = contractDocument.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                Contract_VNACCS.Function = DeclarationFunction.HUY;
                Contract_VNACCS.CustomsReference = contractDocument.SoTN.ToString();
                Contract_VNACCS.Acceptance = contractDocument.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                Contract_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                Contract_VNACCS.CustomsReference = string.Empty;
                Contract_VNACCS.Acceptance = string.Empty;
            }
            Contract_VNACCS.Issuer = DeclarationIssuer.ContractDocument;
            Contract_VNACCS.DeclarationOffice = contractDocument.MaHQ.Trim();
            //Contract_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //Contract_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType == "TKMD")
            {
                Contract_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                Contract_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            Contract_VNACCS.DeclarationDocument = new DeclarationDocument()
            {
                Reference = TKMD.SoToKhai.ToString(),
                Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = TKMD.MaLoaiHinh,
                DeclarationOffice = TKMD.CoQuanHaiQuan
            };
            ContractDocuments contractDocuments = new ContractDocuments()
            {
                ContractDocument = new List<Company.KDT.SHARE.Components.Messages.Vouchers.ContractDocument>()
            };

            foreach (KDT_VNACCS_ContractDocument_Detail item in contractDocument.ContractDocumentCollection)
            {
                contractDocuments.ContractDocument.Add(new Company.KDT.SHARE.Components.Messages.Vouchers.ContractDocument
                {
                    Reference = item.SoHopDong,
                    Issue = item.NgayHopDong.ToString(sfmtDate),
                    Expire = item.ThoiHanThanhToan.ToString(sfmtDate),
                    TotalValue = item.TongTriGia,
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = contractDocument.FileName,
                        Content = contractDocument.Content
                    }
                });
            }
            Contract_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = contractDocument.GhiChuKhac
            };

            Contract_VNACCS.ContractDocuments = contractDocuments;
            return Contract_VNACCS;
        }
        public static CommercialInvoice_VNACCS ToDataTransferCommercialInvoice(KDT_VNACCS_CommercialInvoice commercialInvoice, KDT_VNACCS_CommercialInvoice_Detail commercialInvoiceDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            CommercialInvoice_VNACCS CommercialInvoice_VNACCS = new CommercialInvoice_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = commercialInvoice.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = commercialInvoice.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            CommercialInvoice_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = commercialInvoice.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                CommercialInvoice_VNACCS.Function = DeclarationFunction.SUA;
                CommercialInvoice_VNACCS.CustomsReference = commercialInvoice.SoTN.ToString();
                CommercialInvoice_VNACCS.Acceptance = commercialInvoice.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                CommercialInvoice_VNACCS.Function = DeclarationFunction.HUY;
                CommercialInvoice_VNACCS.CustomsReference = commercialInvoice.SoTN.ToString();
                CommercialInvoice_VNACCS.Acceptance = commercialInvoice.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                CommercialInvoice_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                CommercialInvoice_VNACCS.CustomsReference = string.Empty;
                CommercialInvoice_VNACCS.Acceptance = string.Empty;
            }
            CommercialInvoice_VNACCS.Issuer = DeclarationIssuer.CommercialInvoice;
            CommercialInvoice_VNACCS.DeclarationOffice = commercialInvoice.MaHQ.Trim();
            //CommercialInvoice_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //CommercialInvoice_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType == "TKMD")
            {
                CommercialInvoice_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                CommercialInvoice_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            CommercialInvoices commercialInvoices = new CommercialInvoices()
            {
                CommercialInvoice = new List<Company.KDT.SHARE.Components.Messages.Vouchers.CommercialInvoice>()
            };
            foreach (KDT_VNACCS_CommercialInvoice_Detail item in commercialInvoice.CommercialInvoiceCollection)
            {
                commercialInvoices.CommercialInvoice.Add(new Company.KDT.SHARE.Components.Messages.Vouchers.CommercialInvoice
                {
                    Reference = item.SoHoaDonTM,
                    Issue = item.NgayPhatHanhHDTM.ToString(sfmtDate),
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = commercialInvoice.FileName,
                        Content = commercialInvoice.Content
                    }
                });
            }
            CommercialInvoice_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = commercialInvoice.GhiChuKhac
            };

            CommercialInvoice_VNACCS.CommercialInvoices = commercialInvoices;
            return CommercialInvoice_VNACCS;
        }
        public static BillOfLading_VNACCS ToDataTransferBillOfLading(KDT_VNACCS_BillOfLading billOfLading, KDT_VNACCS_BillOfLading_Detail billOfLadingDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            BillOfLading_VNACCS BillOfLading_VNACCS = new BillOfLading_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = billOfLading.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = billOfLading.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            BillOfLading_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = billOfLading.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                BillOfLading_VNACCS.Function = DeclarationFunction.SUA;
                BillOfLading_VNACCS.CustomsReference = billOfLading.SoTN.ToString();
                BillOfLading_VNACCS.Acceptance = billOfLading.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                BillOfLading_VNACCS.Function = DeclarationFunction.HUY;
                BillOfLading_VNACCS.CustomsReference = billOfLading.SoTN.ToString();
                BillOfLading_VNACCS.Acceptance = billOfLading.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                BillOfLading_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                BillOfLading_VNACCS.CustomsReference = string.Empty;
                BillOfLading_VNACCS.Acceptance = string.Empty;
            }
            BillOfLading_VNACCS.Issuer = DeclarationIssuer.BillOfLading;
            BillOfLading_VNACCS.DeclarationOffice = billOfLading.MaHQ.Trim();
            //BillOfLading_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //BillOfLading_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType == "TKMD")
            {
                BillOfLading_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                BillOfLading_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            BillOfLadings billOfLadings = new BillOfLadings()
            {
                BillOfLading = new List<Company.KDT.SHARE.Components.BillOfLading>()
            };
            foreach (KDT_VNACCS_BillOfLading_Detail item in billOfLading.BillOfLadingCollection)
            {
                billOfLadings.BillOfLading.Add(new Company.KDT.SHARE.Components.BillOfLading
                {
                    Reference = item.SoVanDon,
                    Issue = item.NgayVanDon.ToString(sfmtDate),
                    IssueLocation = item.NuocPhatHanh,
                    TransitLocation = item.DiaDiemCTQC,
                    Category = item.LoaiVanDon.ToString(),
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = billOfLading.FileName,
                        Content = billOfLading.Content
                    }
                });
            }
            BillOfLading_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = billOfLading.GhiChuKhac
            };
            BillOfLading_VNACCS.BillOfLadings = billOfLadings;
            return BillOfLading_VNACCS;
        }
        public static Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS ToDataTransferContainer(KDT_VNACCS_Container_Detail container, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS Container_VNACCS = new Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = container.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = container.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Container_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = container.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                Container_VNACCS.Function = DeclarationFunction.SUA;
                Container_VNACCS.CustomsReference = container.SoTN.ToString();
                Container_VNACCS.Acceptance = container.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                Container_VNACCS.Function = DeclarationFunction.HUY;
                Container_VNACCS.CustomsReference = container.SoTN.ToString();
                Container_VNACCS.Acceptance = container.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                Container_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                Container_VNACCS.CustomsReference = string.Empty;
                Container_VNACCS.Acceptance = string.Empty;
            }
            Container_VNACCS.Issuer = DeclarationIssuer.Containers;
            Container_VNACCS.DeclarationOffice = container.MaHQ.Trim();
            //Container_VNACCS.CustomsReference = string.Empty;
            //Container_VNACCS.Acceptance = DateTime.Now.ToString(sfmtDateTime);

            if (FormType == "TKMD")
            {
                Container_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                Container_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            Container_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = container.GhiChuKhac
            };
            Container_VNACCS.AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
            {
                FileName = container.FileName,
                Content = container.Content
            };
            return Container_VNACCS;
        }
        public static AdditionalDocument_VNACCS ToDataTransferAdditionalDocument(KDT_VNACCS_AdditionalDocument additionalDocument, KDT_VNACCS_AdditionalDocument_Detail additionalDocument_Detail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            AdditionalDocument_VNACCS AdditionalDocument_VNACCS = new AdditionalDocument_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = additionalDocument.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = additionalDocument.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            AdditionalDocument_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = additionalDocument.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.SUA;
                AdditionalDocument_VNACCS.CustomsReference = additionalDocument.SoTN.ToString();
                AdditionalDocument_VNACCS.Acceptance = additionalDocument.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.HUY;
                AdditionalDocument_VNACCS.CustomsReference = additionalDocument.SoTN.ToString();
                AdditionalDocument_VNACCS.Acceptance = additionalDocument.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                AdditionalDocument_VNACCS.CustomsReference = string.Empty;
                AdditionalDocument_VNACCS.Acceptance = string.Empty;
            }
            AdditionalDocument_VNACCS.Issuer = DeclarationIssuer.AdditionalDocument;
            AdditionalDocument_VNACCS.DeclarationOffice = additionalDocument.MaHQ.Trim();
            if (FormType == "TKMD")
            {
                AdditionalDocument_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                AdditionalDocument_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }

            AdditionalDocuments additionalDocuments = new AdditionalDocuments()
            {
                AdditionalDocument = new List<AdditionalDocument>()
            };
            foreach (KDT_VNACCS_AdditionalDocument_Detail item in additionalDocument.AdditionalDocumentCollection)
            {
                additionalDocuments.AdditionalDocument.Add(new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Reference = item.SoChungTu,
                    Description = item.TenChungTu,
                    Issue = item.NgayPhatHanh.ToString(sfmtDate),
                    IssueLocation = item.NoiPhatHanh,
                    Type = additionalDocument.LoaiChungTu.ToString(),
                    AdditionalDocumentSub = new AdditionalDocument()
                    {
                        Content = additionalDocument.GhiChuKhac
                    },
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = additionalDocument.FileName,
                        Content = additionalDocument.Content
                    }
                });
            }
            //AdditionalDocument_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            //{
            //    Content = additionalDocument.GhiChuKhac
            //};
            AdditionalDocument_VNACCS.AdditionalDocuments = additionalDocuments;
            return AdditionalDocument_VNACCS;
        }
        public static Overtime_VNACCS ToDataTransferOvertime(KDT_VNACCS_OverTime overTime, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep, string Status)
        {
            if (Status == "Send")
            {
                Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
                {
                    Function = DeclarationFunction.KHAI_BAO,
                    Reference = overTime.GuidStr,
                    Issue = DateTime.Now.ToString(sfmtDateTime),
                    IssueLocation = string.Empty,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                    Importer = new NameBase()
                    {
                        Identity = overTime.MaDoanhNghiep,
                        Name = TenDoanhNghiep
                    },
                    Agents = new List<Agent>(),
                };
                // Người Khai Hải Quan
                Overtime_VNACCS.Agents.Add(new Agent
                {
                    Name = TenDoanhNghiep,
                    Identity = overTime.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                });
                Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
                Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
                Overtime_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
                Overtime_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = overTime.NgayDangKy.ToString(sfmtDate),
                    Hour = overTime.GioLamThuTuc.ToString(),
                    Content = overTime.NoiDung,
                };
                return Overtime_VNACCS;
            }
            else if (Status == "Edit")
            {
                Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
                {
                    Function = DeclarationFunction.SUA,
                    Reference = overTime.GuidStr,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    IssueLocation = string.Empty,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                    Importer = new NameBase()
                    {
                        Identity = overTime.MaDoanhNghiep,
                        Name = TenDoanhNghiep
                    },
                    Agents = new List<Agent>(),
                };
                // Người Khai Hải Quan
                Overtime_VNACCS.Agents.Add(new Agent
                {
                    Name = TenDoanhNghiep,
                    Identity = overTime.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                });
                Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
                Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
                Overtime_VNACCS.CustomsReference = overTime.SoTN.ToString();
                Overtime_VNACCS.Acceptance = overTime.NgayTN.ToString(sfmtDateTime);

                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = overTime.NgayDangKy.ToString(sfmtDate),
                    Hour = overTime.GioLamThuTuc.ToString(),
                    Content = overTime.NoiDung,
                };
                return Overtime_VNACCS;
            }
            else
            {
                Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
                {
                    Function = DeclarationFunction.HUY,
                    Reference = overTime.GuidStr,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    IssueLocation = string.Empty,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                    Importer = new NameBase()
                    {
                        Identity = overTime.MaDoanhNghiep,
                        Name = TenDoanhNghiep
                    },
                    Agents = new List<Agent>(),
                };
                // Người Khai Hải Quan
                Overtime_VNACCS.Agents.Add(new Agent
                {
                    Name = TenDoanhNghiep,
                    Identity = overTime.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                });
                Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
                Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
                Overtime_VNACCS.CustomsReference = overTime.SoTN.ToString();
                Overtime_VNACCS.Acceptance = overTime.NgayTN.ToString(sfmtDateTime);
                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Content = overTime.NoiDung,
                };
                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = overTime.NgayDangKy.ToString(sfmtDate),
                    Hour = overTime.GioLamThuTuc.ToString(),
                    Content = overTime.NoiDung,
                };
                return Overtime_VNACCS;
            }
        }

        public static TransportEquipment_VNACCS ToDataTransferTransportEquipment(KDT_VNACCS_TransportEquipment transportEquipment, KDT_VNACCS_TransportEquipment_Detail transportEquipmentDetail, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep)
        {
            TransportEquipment_VNACCS TransportEquipment_VNACCS = new TransportEquipment_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = transportEquipment.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = transportEquipment.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            TransportEquipment_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = transportEquipment.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            TransportEquipment_VNACCS.Issuer = DeclarationIssuer.TransportEquipment;
            TransportEquipment_VNACCS.DeclarationOffice = transportEquipment.MaHQ.Trim();
            TransportEquipment_VNACCS.CustomsReference = transportEquipment.SoTN.ToString();
            TransportEquipment_VNACCS.Acceptance = transportEquipment.NgayTN.ToString(sfmtDateTime);
            TransportEquipment_VNACCS.DeclarationDocument = new DeclarationDocument()
            {
                Reference = TKMD.SoToKhai.ToString(),
                IsExportImport = transportEquipment.CoBaoXNK,
                Issue = TKMD.NgayDangKy.ToString(sfmtDateTime),
                Clearance = TKMD.NgayCapPhep.ToString(sfmtDateTime),
                NatureOfTransaction = transportEquipment.MaLoaiHinhVanChuyen,
                DeclarationOffice = TKMD.CoQuanHaiQuan,
                DeclarationOfficeControl = transportEquipment.MaHQGiamSat,
                Transportation = transportEquipment.MaPTVC,
                PurposesTransport = transportEquipment.MaMucDichVC,
                StartDateOfTransport = transportEquipment.NgayBatDauVC.ToString(sfmtDateTime),
                ArrivalDateOfTransport = transportEquipment.NgayKetThucVC.ToString(sfmtDateTime),
            };
            TransportEquipment_VNACCS.ContractDocument = new Company.KDT.SHARE.Components.Messages.Vouchers.ContractDocument()
            {
                Issue = transportEquipment.NgayHopDongVC.ToString(sfmtDate),
                Reference = transportEquipment.SoHopDongVC,
                Expire = transportEquipment.NgayHetHanHDVC.ToString(sfmtDate),
            };
            TransportEquipment_VNACCS.LoadingLocation = new UnloadingLocation()
            {
                CustomsCode = transportEquipment.MaDiaDiemXepHang,
                CustomsName = transportEquipment.TenDiaDiemXepHang,
                Code = transportEquipment.MaViTriXepHang,
                PortCode = transportEquipment.MaCangCuaKhauGaXepHang,
            };
            TransportEquipment_VNACCS.UnloadingLocation = new UnloadingLocation()
            {
                CustomsCode = transportEquipment.MaDiaDiemDoHang,
                CustomsName = transportEquipment.TenDiaDiemDoHang,
                Code = transportEquipment.MaViTriDoHang,
                PortCode = transportEquipment.MaCangCuaKhauGaDoHang,
            };
            TransportEquipment_VNACCS.AdditionalInformation = new AdditionalDocument()
            {
                Content = transportEquipment.GhiChuKhac,
                Route = transportEquipment.TuyenDuong,
                Tel = transportEquipment.SoDienThoaiHQ,
                Fax = transportEquipment.SoFaxHQ,
                Name = transportEquipment.TenDaiDienDN,
                Time = transportEquipment.ThoiGianVC,
                Km = transportEquipment.SoKMVC.ToString(),
            };
            TransportEquipments transportEquipments = new TransportEquipments()
            {
                TransportEquipment = new List<TransportEquipment>()
            };
            foreach (KDT_VNACCS_TransportEquipment_Detail item in transportEquipment.TransportEquipmentCollection)
            {
                transportEquipments.TransportEquipment.Add(new TransportEquipment
                {
                    Container = item.SoContainer,
                    BillOfLading = item.SoVanDon,
                    Seal = item.SoSeal,
                    CustomsSeal = item.SoSealHQ,
                    Type = item.LoaiContainer,
                });
            }
            TransportEquipment_VNACCS.TransportEquipments = transportEquipments;
            return TransportEquipment_VNACCS;
        }

        public static GoodsItems_VNACCS ToDataTransferGoodsItem(KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodsItem, KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail goodsItem_Detail, string DiaChi, string TenDoanhNghiep, string Status)
        {
            GoodsItems_VNACCS GoodsItems_VNACCS = new GoodsItems_VNACCS()
            {

                Reference = goodsItem.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                //Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                FinalYear = goodsItem.NamQuyetToan.ToString(),
                //StartDate = goodsItem.NgayTN.ToString(sfmtDate),
                //FinishDate = goodsItem.NgayTN.ToString(sfmtDate),
                Type = goodsItem.LoaiHinhBaoCao.ToString(),
                Unit = goodsItem.DVTBaoCao.ToString(),
                Importer = new NameBase()
                {
                    Identity = goodsItem.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            if (Status == "Send")
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.SUA;
            }
            // Người Khai Hải Quan
            GoodsItems_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = goodsItem.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            GoodsItems_VNACCS.Issuer = DeclarationIssuer.GoodsItem;
            GoodsItems_VNACCS.DeclarationOffice = goodsItem.MaHQ.Trim();
            GoodsItems_VNACCS.CustomsReference = goodsItem.SoTN.ToString();
            GoodsItems_VNACCS.Acceptance = goodsItem.NgayTN.ToString(sfmtDateTime);

            GoodsItems goodsItems = new GoodsItems()
            {
                GoodsItem = new List<GoodsItem>()
            };
            foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in goodsItem.GoodItemsCollection)
            {
                goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                {
                    Sequence = item.STT.ToString(),
                    //DescriptionMaterial = item.TenHangHoa,
                    //IdentificationMaterial = item.MaHangHoa,
                    //MeasureUnitMaterial = item.DVT,
                    //QuantityBeginMaterial = item.TonDauKy,
                    //QuantityImportMaterial = item.NhapTrongKy,
                    ////QuantityReExportMaterial = item.TonDauKy,
                    ////QuantityRePurposeMaterial = item.TonDauKy,
                    ////QuantityExportProduct = item.XuatTrongKy,
                    //QuantityExportOther = item.XuatTrongKy,
                    //QuantityExcessMaterial = item.TonCuoiKy,
                    Type = item.LoaiHangHoa.ToString(),
                    Account = item.TaiKhoan.ToString(),
                    Description = item.TenHangHoa,
                    Identification = item.MaHangHoa,
                    MeasureUnit = item.DVT,
                    QuantityBegin = item.TonDauKy,
                    QuantityImport = item.NhapTrongKy,
                    QuantityExport = item.XuatTrongKy,
                    QuantityExcess = item.TonCuoiKy,
                    Content = item.GhiChu,
                });
            }
            GoodsItems_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = goodsItem.GhiChuKhac
            };
            GoodsItems_VNACCS.AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
            {
                FileName = goodsItem.FileName,
                Content = goodsItem.Content
            };
            GoodsItems_VNACCS.GoodsItems = goodsItems;
            return GoodsItems_VNACCS;
        }
        public static GoodsItems_VNACCS ToDataTransferGoodsItemNew(KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New goodsItem, KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New goodsItem_Detail, string DiaChi, string TenDoanhNghiep, string Status)
        {
            GoodsItems_VNACCS GoodsItems_VNACCS = new GoodsItems_VNACCS()
            {

                Reference = goodsItem.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                //Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                //FinalYear = goodsItem.NamQuyetToan.ToString(),
                StartDate = goodsItem.NgayBatDauBC.ToString(sfmtDate),
                FinishDate = goodsItem.NgayKetThucBC.ToString(sfmtDate),
                Type = goodsItem.LoaiBaoCao.ToString(), 
                UpdateType = goodsItem.LoaiSua.ToString(),
                Importer = new NameBase()
                {
                    Identity = goodsItem.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            if (Status == "Send")
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else if ((Status == "Edit"))
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.SUA;
                GoodsItems_VNACCS.UpdateType = goodsItem.LoaiSua.ToString();
            }
            else
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.HUY;
            }
            // Người Khai Hải Quan
            GoodsItems_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = goodsItem.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            GoodsItems_VNACCS.Issuer = DeclarationIssuer.GoodsItem_SXXK;
            GoodsItems_VNACCS.DeclarationOffice = goodsItem.MaHQ.Trim();
            GoodsItems_VNACCS.CustomsReference = goodsItem.SoTN.ToString();
            GoodsItems_VNACCS.Acceptance = goodsItem.NgayTN.Year == 1 ? DateTime.Now.ToString(sfmtDateTime) : goodsItem.NgayTN.ToString(sfmtDateTime);

            GoodsItems goodsItems = new GoodsItems()
            {
                GoodsItem = new List<GoodsItem>()
            };
            foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in goodsItem.GoodItemsCollection)
            {
                goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                {
                    Sequence = item.STT.ToString(),
                    DescriptionMaterial = item.TenHangHoa,
                    IdentificationMaterial = item.MaHangHoa,
                    MeasureUnitMaterial = item.DVT,
                    QuantityBeginMaterial = item.TonDauKy,
                    QuantityImportMaterial = item.NhapTrongKy,
                    QuantityReExportMaterial = item.TaiXuat,
                    QuantityRePurposeMaterial = item.ChuyenMDSD,
                    QuantityExportProduct = item.XuatTrongKy,
                    QuantityExportOther = item.XuatKhac,
                    QuantityExcessMaterial = item.TonCuoiKy,
                    Content = item.GhiChu,
                });
            }
            GoodsItems_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = goodsItem.GhiChuKhac
            };
            GoodsItems_VNACCS.GoodsItems = goodsItems;
            return GoodsItems_VNACCS;
        }
        public static Company.KDT.SHARE.Components.Messages.CSSX.StorageAreasProduction_VNACCS ToDataTransferStorageAreasProduction(KDT_VNACCS_StorageAreasProduction storageAreasProduction, string DiaChi, string TenDoanhNghiep, string Status)
        {
            Company.KDT.SHARE.Components.Messages.CSSX.StorageAreasProduction_VNACCS StorageAreasProduction_VNACCS = new Company.KDT.SHARE.Components.Messages.CSSX.StorageAreasProduction_VNACCS()
            {

                Reference = storageAreasProduction.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,

                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Name = TenDoanhNghiep,
                    Identity = storageAreasProduction.MaDoanhNghiep,
                },
                ImporterDetail = new Company.KDT.SHARE.Components.Messages.CSSX.ImporterDetail()
                {
                    Address = DiaChi,
                    AddressType = storageAreasProduction.LoaiDiaChiTruSoChinh.ToString(),
                    InvestmentCountry = storageAreasProduction.NuocDauTu,
                    IndustryProduction = storageAreasProduction.NganhNgheSanXuat,
                    DateFinanceYear = storageAreasProduction.NgayKetThucNamTC.ToString("dd-MM"),
                    TypeOfBusiness = storageAreasProduction.LoaiHinhDN.ToString(),
                    OldImporter = new Company.KDT.SHARE.Components.Messages.CSSX.OldImporter()
                    {
                        Name = storageAreasProduction.TenDoanhNghiepTKTD,
                        Identity = storageAreasProduction.MaDoanhNghiepTKTD,
                        Reason = storageAreasProduction.LyDoChuyenDoi,
                    },
                    ChairmanImporter = new Company.KDT.SHARE.Components.Messages.CSSX.ChairmanImporter()
                    {
                        Identity = storageAreasProduction.SoCMNDCT,
                        Issue = storageAreasProduction.NgayCapGiayPhepCT.ToString(sfmtDate),
                        IssueLocation = storageAreasProduction.NoiCapGiayPhepCT,
                        PermanentResidence = storageAreasProduction.NoiDangKyHKTTCT,
                        PhoneNumbers = storageAreasProduction.SoDienThoaiCT,
                    },
                    GeneralDirector = new Company.KDT.SHARE.Components.Messages.CSSX.GeneralDirector()
                    {
                        Identity = storageAreasProduction.SoCMNDGD,
                        Issue = storageAreasProduction.NgayCapGiayPhepGD.ToString(sfmtDate),
                        IssueLocation = storageAreasProduction.NoiCapGiayPhepGD,
                        PermanentResidence = storageAreasProduction.NoiDangKyHKTTGD,
                        PhoneNumbers = storageAreasProduction.SoDienThoaiGD,
                    },
                    //StorageOfGoods = new StorageOfGoods()
                    //{
                    //    StorageOfGood = new List<StorageOfGood>(),
                    //},                    
                    ProductionInspectionHis = new Company.KDT.SHARE.Components.Messages.CSSX.ProductionInspectionHis()
                    {
                        IsInspection = storageAreasProduction.DaDuocCQHQKT.ToString(),
                        //ContentInspections = new Company.KDT.SHARE.Components.Messages.CSSX.ContentInspections() 
                        //{
                        //    ContentInspection = new List<Company.KDT.SHARE.Components.Messages.CSSX.ContentInspection>()
                        //},
                    },
                }
            };

            Company.KDT.SHARE.Components.Messages.CSSX.StorageOfGoods storageOfGoods = new Company.KDT.SHARE.Components.Messages.CSSX.StorageOfGoods()
            {
                StorageOfGood = new List<Company.KDT.SHARE.Components.Messages.CSSX.StorageOfGood>()
            };

            foreach (KDT_VNACCS_StorageOfGood item in storageAreasProduction.StorageOfGoodCollection)
            {
                storageOfGoods.StorageOfGood.Add(new Company.KDT.SHARE.Components.Messages.CSSX.StorageOfGood
                {
                    Name = item.Ten,
                    Identity = item.Ma
                });
            }
            StorageAreasProduction_VNACCS.ImporterDetail.StorageOfGoods = storageOfGoods;
            Company.KDT.SHARE.Components.Messages.CSSX.ContentInspections contentInspections = new Company.KDT.SHARE.Components.Messages.CSSX.ContentInspections()
            {
                ContentInspection = new List<Company.KDT.SHARE.Components.Messages.CSSX.ContentInspection>()
            };
            foreach (KDT_VNACCS_ContentInspection item in storageAreasProduction.ContentInspectionCollection)
            {
                contentInspections.ContentInspection.Add(new Company.KDT.SHARE.Components.Messages.CSSX.ContentInspection
                {
                    InspectionNumbers = item.SoBienBanKiemTra,
                    ConclusionNumbers = item.SoKetLuanKiemTra,
                    InspectionDate = item.NgayKiemTra.ToString(sfmtDate),
                });
            }
            StorageAreasProduction_VNACCS.ImporterDetail.ProductionInspectionHis.ContentInspections = contentInspections;

            if (Status == "Send")
            {
                StorageAreasProduction_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else
            {
                StorageAreasProduction_VNACCS.Function = DeclarationFunction.SUA;
            }
            // Người Khai Hải Quan
            StorageAreasProduction_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = storageAreasProduction.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            StorageAreasProduction_VNACCS.Issuer = DeclarationIssuer.StorageAreasProduction;
            StorageAreasProduction_VNACCS.DeclarationOffice = storageAreasProduction.MaHQ.Trim();
            StorageAreasProduction_VNACCS.CustomsReference = storageAreasProduction.SoTN.ToString();
            StorageAreasProduction_VNACCS.Acceptance = storageAreasProduction.NgayTN.ToString(sfmtDateTime);
            StorageAreasProduction_VNACCS.UpdateType = storageAreasProduction.LoaiSua.ToString();

            Company.KDT.SHARE.Components.Messages.CSSX.ManufactureFactories manufactureFactories = new Company.KDT.SHARE.Components.Messages.CSSX.ManufactureFactories()
            {
                ManufactureFactory = new List<Company.KDT.SHARE.Components.Messages.CSSX.ManufactureFactory>()
            };
            foreach (KDT_VNACCS_ManufactureFactory item in storageAreasProduction.ManufactureFactoryCollection)
            {
                Company.KDT.SHARE.Components.Messages.CSSX.Careers carrery = new Company.KDT.SHARE.Components.Messages.CSSX.Careers()
                {
                    Career = new List<Company.KDT.SHARE.Components.Messages.CSSX.Career>()
                };
                List<KDT_VNACCS_Careery> CareeryCollection = KDT_VNACCS_Careery.SelectCollectionBy_ManufactureFactory_ID(item.ID);
                foreach (KDT_VNACCS_Careery careery in CareeryCollection)
                {
                    List<KDT_VNACCS_Careeries_Product> ProductCollection = KDT_VNACCS_Careeries_Product.SelectCollectionBy_Careeries_ID(careery.ID);
                    Company.KDT.SHARE.Components.Messages.CSSX.Products products = new Company.KDT.SHARE.Components.Messages.CSSX.Products()
                    {
                        Product = new List<Company.KDT.SHARE.Components.Messages.CSSX.Product>(),
                    };
                    foreach (KDT_VNACCS_Careeries_Product product in ProductCollection)
                    {
                        products.Product.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Product
                        {
                            Identification = product.MaSP,
                            TariffClassification = product.MaHS,
                            Period = product.ChuKySXTG.ToString(),
                            MeasureUnit = product.ChuKySXDVT.ToString(),
                        });
                    }
                    List<KDT_VNACCS_ProductionCapacity_Product> ProductionCapacityCollection = KDT_VNACCS_ProductionCapacity_Product.SelectCollectionBy_Careeries_ID(careery.ID);

                    Company.KDT.SHARE.Components.Messages.CSSX.Products productionCapacities = new Company.KDT.SHARE.Components.Messages.CSSX.Products()
                    {
                        Product = new List<Company.KDT.SHARE.Components.Messages.CSSX.Product>(),
                    };
                    foreach (KDT_VNACCS_ProductionCapacity_Product product in ProductionCapacityCollection)
                    {
                        productionCapacities.Product.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Product
                        {
                            Time = product.ThoiGianSXTG.ToString(),
                            MeasureUnitTime = product.ThoiGianSXDVT.ToString(),
                            Identification = product.MaSP,
                            TariffClassification = product.MaHS,
                            MeasureUnit = product.DVT.ToString(),
                            Quantity = product.SoLuong.ToString(),
                        });
                    }
                    carrery.Career.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Career
                    {
                        Type = careery.LoaiNganhNghe.ToString(),
                        Period = products,
                        ProductionCapacity = productionCapacities,
                    });
                }
                manufactureFactories.ManufactureFactory.Add(new Company.KDT.SHARE.Components.Messages.CSSX.ManufactureFactory
                {
                    Type = item.LoaiCSSX.ToString(),
                    Address = item.DiaChiCSSX,
                    AddressType = item.LoaiDiaChiCSSX.ToString(),
                    Square = item.DienTichNhaXuong.ToString().Replace(",", "."),
                    WorkerQuantity = item.SoLuongCongNhan.ToString(),
                    Machine = new Company.KDT.SHARE.Components.Messages.CSSX.Machine()
                    {
                        OwnedQuantity = item.SoLuongSoHuu,
                        RentQuantity = item.SoLuongDiThue,
                        OtherQuantity = item.SoLuongKhac,
                        TotalQuantity = item.TongSoLuong,
                        ProductionCapacity = item.NangLucSanXuat,
                    },
                    Careers = carrery,
                });

            }
            StorageAreasProduction_VNACCS.ManufactureFactories = manufactureFactories;
            Company.KDT.SHARE.Components.Messages.CSSX.Careers career = new Company.KDT.SHARE.Components.Messages.CSSX.Careers()
            {
                Career = new List<Company.KDT.SHARE.Components.Messages.CSSX.Career>()
            };
            foreach (KDT_VNACCS_Career item in storageAreasProduction.CareerCollection)
            {
                career.Career.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Career
                {
                    Type = item.LoaiNganhNghe.ToString(),
                });
            }
            StorageAreasProduction_VNACCS.Careers = career;

            StorageAreasProduction_VNACCS.Staff = new Company.KDT.SHARE.Components.Messages.CSSX.Staff
            {
                ManageQuantity = storageAreasProduction.BoPhanQuanLy.ToString(),
                WorkerQuantity = storageAreasProduction.SoLuongCongNhan.ToString(),
            };

            StorageAreasProduction_VNACCS.Machine = new Company.KDT.SHARE.Components.Messages.CSSX.Machine
            {
                OwnedQuantity = storageAreasProduction.SoLuongSoHuu,
                RentQuantity = storageAreasProduction.SoLuongDiThue,
                OtherQuantity = storageAreasProduction.SoLuongKhac,
                TotalQuantity = storageAreasProduction.TongSoLuong,
            };
            if (storageAreasProduction.HoldingCompanyCollection.Count >=1)
            {
                Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompanies holdingCompanies = new Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompanies()
                {
                    Quantity = storageAreasProduction.SoLuongThanhVienCTM.ToString(),
                    HoldingCompany = new List<Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompany>()
                };
                foreach (KDT_VNACCS_HoldingCompany item in storageAreasProduction.HoldingCompanyCollection)
                {
                    holdingCompanies.HoldingCompany.Add(new Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompany
                    {
                        Name = item.TenDoanhNghiep,
                        Identity = item.MaDoanhNghiep,
                        Address = item.DiaChiCSSX,
                    });
                }
                StorageAreasProduction_VNACCS.HoldingCompanies = holdingCompanies;   
            }

            if (storageAreasProduction.AffiliatedMemberCompanyCollection.Count >=1)
            {
                Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompanies affiliatedMemberCompanies = new Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompanies()
                {
                    Name = storageAreasProduction.TenCongTyMe,
                    Identity = storageAreasProduction.MaCongTyMe,
                    Quantity = storageAreasProduction.SoLuongThanhVien.ToString(),
                    AffiliatedMemberCompany = new List<Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompany>()
                };
                foreach (KDT_VNACCS_AffiliatedMemberCompany item in storageAreasProduction.AffiliatedMemberCompanyCollection)
                {
                    affiliatedMemberCompanies.AffiliatedMemberCompany.Add(new Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompany
                    {
                        Name = item.TenDoanhNghiep,
                        Identity = item.MaDoanhNghiep,
                        Address = item.DiaChiCSSX,
                    });
                }
                StorageAreasProduction_VNACCS.AffiliatedMemberCompanies = affiliatedMemberCompanies;   
            }
            if ( storageAreasProduction.MemberCompanyCollection.Count >=1)
            {
                Company.KDT.SHARE.Components.Messages.CSSX.MemberCompanies memberCompanies = new Company.KDT.SHARE.Components.Messages.CSSX.MemberCompanies()
                {
                    Quantity = storageAreasProduction.SoLuongChiNhanh.ToString(),
                    MemberCompany = new List<Company.KDT.SHARE.Components.Messages.CSSX.MemberCompany>()
                };
                foreach (KDT_VNACCS_MemberCompany item in storageAreasProduction.MemberCompanyCollection)
                {
                    memberCompanies.MemberCompany.Add(new Company.KDT.SHARE.Components.Messages.CSSX.MemberCompany
                    {
                        Name = item.TenDoanhNghiep,
                        Identity = item.MaDoanhNghiep,
                        Address = item.DiaChiChiNhanh,
                    });
                }
                StorageAreasProduction_VNACCS.MemberCompanies = memberCompanies;   
            }

            StorageAreasProduction_VNACCS.ComplianceWithLaws = new Company.KDT.SHARE.Components.Messages.CSSX.ComplianceWithLaws
            {
                Smuggling = storageAreasProduction.BiXuPhatVeBuonLau.ToString(),
                TaxEvasion = storageAreasProduction.BiXuPhatVeTronThue.ToString(),
                HandlingViolations = storageAreasProduction.BiXuPhatVeKeToan.ToString(),
            };

            if (storageAreasProduction.OutsourcingManufactureFactoryCollection.Count >=1)
            {
                Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactories outsourcingManufactureFactories = new Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactories()
                {
                    OutsourcingManufactureFactory = new List<Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactory>()
                };

                foreach (KDT_VNACCS_OutsourcingManufactureFactory item in storageAreasProduction.OutsourcingManufactureFactoryCollection)
                {
                    Company.KDT.SHARE.Components.Messages.CSSX.ContractDocuments contractDocuments = new Company.KDT.SHARE.Components.Messages.CSSX.ContractDocuments()
                    {
                        ContractDocument = new List<Company.KDT.SHARE.Components.Messages.CSSX.ContractDocument>(),
                    };

                    List<KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument> ContractDocumentCollection = KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument.SelectCollectionBy_OutsourcingManufactureFactory_ID(item.ID);

                    foreach (KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument contractDocument in ContractDocumentCollection)
                    {
                        contractDocuments.ContractDocument.Add(new Company.KDT.SHARE.Components.Messages.CSSX.ContractDocument
                        {
                            Reference = contractDocument.SoHopDong.ToString(),
                            Issue = contractDocument.NgayHopDong.ToString(sfmtDate),
                            Expire = contractDocument.NgayHetHan.ToString(sfmtDate)
                        });
                    }

                    List<KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory> OurManufactureFactoryCollection = KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory.SelectCollectionBy_OutsourcingManufactureFactory_ID(item.ID);
                    List<Company.KDT.SHARE.Components.Messages.CSSX.OurManufactureFactory> ourManufactureFactories = new List<Company.KDT.SHARE.Components.Messages.CSSX.OurManufactureFactory>();
                    foreach (KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory ourManufactureFactory in OurManufactureFactoryCollection)
                    {
                        List<KDT_VNACCS_OutsourcingManufactureFactory_Product> ProductCollection = KDT_VNACCS_OutsourcingManufactureFactory_Product.SelectCollectionBy_OutsourcingManufactureFactory_ManufactureFactory_ID(ourManufactureFactory.ID);
                        Company.KDT.SHARE.Components.Messages.CSSX.Products products = new Company.KDT.SHARE.Components.Messages.CSSX.Products()
                        {
                            Product = new List<Company.KDT.SHARE.Components.Messages.CSSX.Product>(),
                        };
                        foreach (KDT_VNACCS_OutsourcingManufactureFactory_Product product in ProductCollection)
                        {
                            products.Product.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Product
                            {
                                Identification = product.MaSP,
                                TariffClassification = product.MaHS,
                                Period = product.ChuKySXTG.ToString(),
                                MeasureUnit = product.ChuKySXDVT.ToString(),
                            });
                        }

                        List<KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product> ProductionCapacityCollection = KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product.SelectCollectionBy_OutsourcingManufactureFactory_ManufactureFactory_ID(ourManufactureFactory.ID);

                        Company.KDT.SHARE.Components.Messages.CSSX.Products productionCapacities = new Company.KDT.SHARE.Components.Messages.CSSX.Products()
                        {
                            Product = new List<Company.KDT.SHARE.Components.Messages.CSSX.Product>(),
                        };
                        foreach (KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product product in ProductionCapacityCollection)
                        {
                            productionCapacities.Product.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Product
                            {
                                Time = product.ThoiGianSXTG.ToString(),
                                MeasureUnitTime = product.ThoiGianSXDVT.ToString(),
                                Identification = product.MaSP,
                                TariffClassification = product.MaHS,
                                MeasureUnit = product.DVT.ToString(),
                                Quantity = product.SoLuong.ToString(),
                            });
                        }
                        Company.KDT.SHARE.Components.Messages.CSSX.OurManufactureFactory outsourcingManufactureFactory = new Company.KDT.SHARE.Components.Messages.CSSX.OurManufactureFactory()
                        {
                            Address = ourManufactureFactory.DiaChiCSSX,
                            Square = ourManufactureFactory.DienTichNX.ToString().Replace(",", "."),
                            WorkerQuantity = ourManufactureFactory.SoLuongCongNhan.ToString(),
                            Period = products,
                            ProductionCapacity = productionCapacities,
                        };
                        ourManufactureFactories.Add(outsourcingManufactureFactory);
                        outsourcingManufactureFactory.Machine = new Company.KDT.SHARE.Components.Messages.CSSX.Machine()
                        {
                            OwnedQuantity = ourManufactureFactory.SoLuongSoHuu,
                            RentQuantity = ourManufactureFactory.SoLuongDiThue,
                            OtherQuantity = ourManufactureFactory.SoLuongKhac,
                            TotalQuantity = ourManufactureFactory.TongSoLuong,
                            ProductionCapacity = ourManufactureFactory.NangLucSX.ToString()
                        };
                    }
                    outsourcingManufactureFactories.OutsourcingManufactureFactory.Add(new Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactory
                    {
                        Name = item.TenDoiTac,
                        Identity = item.MaDoiTac,
                        Address = item.DiaChiDoiTac,
                        ContractDocuments = contractDocuments,
                        OutManufactureFactory = ourManufactureFactories
                    });

                }
                StorageAreasProduction_VNACCS.OutsourcingManufactureFactories = outsourcingManufactureFactories;
            }
            Company.KDT.SHARE.Components.Messages.CSSX.FileAttacheds attachedFiles = new Company.KDT.SHARE.Components.Messages.CSSX.FileAttacheds()
            {
                File = new List<Company.KDT.SHARE.Components.Messages.CSSX.FileAttached>(),
            };
            foreach (KDT_VNACCS_StorageAreasProduction_AttachedFile attachedFile in storageAreasProduction.AttachedFileGoodCollection)
            {
                attachedFiles.File.Add(new Company.KDT.SHARE.Components.Messages.CSSX.FileAttached
                {
                    FileName = attachedFile.FileName,
                    Content = attachedFile.Content,
                });
            }
            StorageAreasProduction_VNACCS.AttachedFiles = attachedFiles;
            StorageAreasProduction_VNACCS.Information = new Company.KDT.SHARE.Components.Messages.CSSX.Information
            {
                Content = storageAreasProduction.GhiChuKhac
            };
            return StorageAreasProduction_VNACCS;
        }
        public static PhieuNhapKho ToDaTaTransferPhieuKhoCFS(KDT_KhoCFS_DangKy khoCFS, string maDV,string tenVD)
        {

            PhieuNhapKho pnk = new PhieuNhapKho()
            {
                Issuer = DeclarationIssuer.TTHangGuiKho,
                Function = DeclarationFunction.KHAIBAO_HANGGUIKHO,
                Reference = khoCFS.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = khoCFS.MaHQ,
                Status = "1",
                CustomsReference = "",
                Acceptance = DateTime.Now.ToString(sfmtDate),
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Name = tenVD,
                    Identity = maDV
                },
                ReferencePNK = new ReferencePNK()
                {
                    Indentity = maDV,
                    DeclarationOffice = khoCFS.MaKhoCFS.Substring(0, 4),
                    Warehouse = khoCFS.MaKhoCFS,
                    //Test mã kho
                    //Warehouse = "43ND001",
                    DeclarationList = new List<DeclarationList>() 
                }
            };
            foreach (KDT_KhoCFS_Detail tk  in khoCFS.ListTK)
            {
                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(tk.TKMD_ID);
                KDT_VNACC_TK_PhanHoi_TyGia tygia = KDT_VNACC_TK_PhanHoi_TyGia.SelectCollectionAll().Find(delegate(KDT_VNACC_TK_PhanHoi_TyGia t)
                {
                    return t.Master_ID == TKMD.ID;
                });
                KDT_VNACC_HangMauDich hmd = KDT_VNACC_HangMauDich.SelectCollectionAll().Find(delegate(KDT_VNACC_HangMauDich h)
                {
                    return h.TKMD_ID == TKMD.ID;
                });
                pnk.ReferencePNK.DeclarationList.Add(new DeclarationList()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Acceptance = TKMD.NgayDangKy.ToString(sfmtDate),
                    //Acceptance = DateTime.Now.ToString(sfmtDate),
                    DeclarationOffice = TKMD.CoQuanHaiQuan,
                    DeclarationHeadOffice = "",
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    Identification = hmd.MaSoHang.Substring(0, 4),
                    Identificationname = hmd.TenHang,
                    PackagingQuanlity = float.Parse(TKMD.SoLuong.ToString()).ToString(),
                    PackagingUnit = TKMD.MaDVTSoLuong,
                    TotalGrossMass = TKMD.TrongLuong.ToString(),
                    TotalGrossMassUnit = TKMD.MaDVTTrongLuong,
                    TotalcustomsValue = TKMD.TongTriGiaHD.ToString(),
                    //TotalcustomsValue = "8.08",
                    CurrencyType = TKMD.MaTTHoaDon,
                    RateVND = tygia.TyGiaTinhThue.ToString(),
                    RateUSD = "",
                    QueueDestination = TKMD.MaDiaDiemXepHang,
                    DeliveryDestination = khoCFS.MaKhoCFS,
                    DeliveryDestinationDate = TKMD.NgayDen.ToString(sfmtDate),
                    //DeliveryDestinationDate = DateTime.Now.ToString(sfmtDate),
                    PluongInformation = TKMD.MaPhanLoaiKiemTra,
                });
            }
            
            pnk.Agents.Add(new Agent()
            {
                Name = tenVD,
                Identity = maDV,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            return pnk;
        }

        public static GC_ImportWareHouse ToDataTransferImportWareHouseNew(T_KDT_VNACCS_WarehouseImport warehouseImport, T_KDT_VNACCS_WarehouseImport_Detail warehouseImport_Detail, T_KDT_VNACCS_WarehouseImport_GoodsDetail warehouseImport_GoodsDetail, string TenDoanhNghiep,string Status)
        {
            GC_ImportWareHouse ImportWareHouse = new GC_ImportWareHouse()
            {

                Issuer = DeclarationIssuer.ImportWareHouse,
                Reference = warehouseImport.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                //Function = DeclarationFunction.SUA,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = warehouseImport.SoTN.ToString(),
                Acceptance = warehouseImport.NgayTN.ToString(sfmtDate),
                DeclarationOffice = warehouseImport.MaHQ,
                StartDate = warehouseImport.NgayBatDauBC.ToString(sfmtDate),
                FinishDate = warehouseImport.NgayKetthucBC.ToString(sfmtDate),
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Identity = warehouseImport.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Warehouse = new NameBase()
                {
                    Name = warehouseImport.TenKho,
                    Identity = warehouseImport.MaKho
                },
            };
            if (Status == "Send")
            {
                ImportWareHouse.Function = DeclarationFunction.KHAI_BAO;
            }
            else if (Status == "Edit")
            {
                ImportWareHouse.Function = DeclarationFunction.SUA;
            }
            else
            {
                ImportWareHouse.Function = DeclarationFunction.HUY;
            }
            // Người Khai Hải Quan
            ImportWareHouse.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = warehouseImport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            PNK_AdditionalDocuments additionalDocuments = new PNK_AdditionalDocuments()
            {
                AdditionalDocument = new List<PNK_AdditionalDocument>(),
            };

            foreach (T_KDT_VNACCS_WarehouseImport_Detail item in warehouseImport.WarehouseImportDetailsCollection)
            {
                int i = 1;
                additionalDocuments.AdditionalDocument.Add(new PNK_AdditionalDocument
                {
                    Sequence = i++.ToString(),
                    Identification = item.SoPhieuNhap,
                    Issue = item.NgayPhieuNhap.ToString(sfmtDate),
                    NameConsignor = item.TenNguoiGiaoHang,
                    IdentityConsignor = item.MaNguoiGiaoHang,
                    CustomsGoodsItem = T_KDT_VNACCS_WarehouseImport_GoodsDetail.SelectCollectionBy_WarehouseImport_Details_ID_Update(item.ID,warehouseImport),
                    AdditionalInformation = new PNK_AdditionalInformation()
                    {
                        Content = warehouseImport.GhiChuKhac,
                    }
                });
            }
            ImportWareHouse.AdditionalDocuments = additionalDocuments;
            //ImportWareHouse.ContractReference = new PNK_ContractReference()
            //{
            //    Reference = "SOFTECH NEW",
            //    Issue = "2018-03-16",
            //    DeclarationOffice = "01TE",
            //    Expire = "2019-03-16",

            //};
            return ImportWareHouse;
        }
        public static ImportWareHouse ToDataTransferImportWareHouse(T_KDT_VNACCS_WarehouseImport warehouseImport, T_KDT_VNACCS_WarehouseImport_Detail warehouseImport_Detail, T_KDT_VNACCS_WarehouseImport_GoodsDetail warehouseImport_GoodsDetail, string TenDoanhNghiep)
        {
            bool isEdit = warehouseImport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            ImportWareHouse ImportWareHouse = new ImportWareHouse()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = warehouseImport.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = warehouseImport.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Warehouse = new NameBase()
                {
                    Name = warehouseImport.TenKho,
                    Identity = warehouseImport.MaKho
                },
                Agents = new List<Agent>(),
                //AdditionalInformationNew = new AdditionalDocument() 
                //{
                //    Content=warehouseImport.GhiChuKhac,
                //},
                //DeclarationDocument = new DeclarationDocument()
                //{
                //    Reference = warehouseImport.SoTKChungTu.ToString(),
                //    Type = warehouseImport.Loai.ToString()
                //},
            };
            // Người Khai Hải Quan
            ImportWareHouse.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = warehouseImport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            ImportWareHouse.Issuer = "220";//DeclarationIssuer.ImportWareHouse;
            ImportWareHouse.DeclarationOffice = "01TE";//warehouseImport.MaHQ.Trim();
            ImportWareHouse.CustomsReference = isEdit ? warehouseImport.SoTN.ToString() : String.Empty;
            ImportWareHouse.Acceptance = isEdit ? warehouseImport.NgayTN.ToString(sfmtDateTime) : String.Empty;
            ImportWareHouse.StartDate = warehouseImport.NgayBatDauBC.ToString(sfmtDate);
            ImportWareHouse.FinishDate = warehouseImport.NgayKetthucBC.ToString(sfmtDate);
            AdditionalDocuments additionalDocuments = new AdditionalDocuments()
            {
                AdditionalDocument = new List<AdditionalDocument>(),
            };
            foreach (T_KDT_VNACCS_WarehouseImport_Detail item in warehouseImport.WarehouseImportDetailsCollection)
            {
                int STT = 0;
                additionalDocuments.AdditionalDocument.Add(new AdditionalDocument
                {
                    Sequence = STT++.ToString(),//item.STT.ToString(),
                    Identification = item.SoPhieuNhap,
                    Issue = item.NgayPhieuNhap.ToString(sfmtDate),
                    NameConsignor = item.TenNguoiGiaoHang,
                    IdentityConsignor = item.MaNguoiGiaoHang,
                    //CustomsGoodsItem = new List<CustomsGoodsItem>(),
                    CustomsGoodsItem = T_KDT_VNACCS_WarehouseImport_GoodsDetail.SelectCollectionBy_WarehouseImport_Details_ID_New(item.ID),
                    //DeclarationDocument = new DeclarationDocument()
                    //{
                    //    Reference = warehouseImport.SoTKChungTu.ToString(),
                    //    Type = warehouseImport.Loai.ToString()
                    //},
                    AdditionalInformation = new AdditionalInformation()
                    {
                        Content = new Content
                    {
                        Text = warehouseImport.GhiChuKhac,
                    }
                    },
                });

                //CustomsGoodsItem = T_KDT_VNACCS_WarehouseImport_GoodsDetail.SelectCollectionBy_WarehouseImport_Details_ID_New(item.ID);
                //AdditionalDocument additionalDocument = new AdditionalDocument
                //{
                //    Sequence = item.STT.ToString(),
                //    Identification = item.SoPhieuNhap,
                //    Issue = item.NgayPhieuNhap.ToString(sfmtDate),
                //    NameConsignor = item.TenNguoiGiaoHang,
                //    IdentityConsignor = item.MaNguoiGiaoHang,
                //    CustomsGoodsItem = new List<CustomsGoodsItem>()
                //};
                //additionalDocument.CustomsGoodsItem = T_KDT_VNACCS_WarehouseImport_GoodsDetail.SelectCollectionBy_WarehouseImport_Details_ID_New(item.ID);
                //ImportWareHouse.AdditionalDocuments.Add(additionalDocument);


                ImportWareHouse.AdditionalDocumentNew = additionalDocuments;
                //ImportWareHouse.DeclarationDocument = new DeclarationDocument()
                //{
                //    Reference = warehouseImport.SoTKChungTu.ToString(),
                //    Type = warehouseImport.Loai.ToString()
                //};
                //ImportWareHouse.ContractReference = new ContractReference
                //{
                //    Reference = warehouseImport.SoTN.ToString(),
                //    Issue = warehouseImport.NgayTN.ToString(sfmtDate),
                //    DeclarationOffice =warehouseImport.MaHQ,
                //    Expire = warehouseImport.NgayBatDauBC.ToString(sfmtDate),
                //};
                //ImportWareHouse.AdditionalInformations  = new List<AdditionalInformation>();
                //ImportWareHouse.AdditionalInformationNew = new AdditionalDocument()
                //{
                //    Content = warehouseImport.GhiChuKhac,
                //};
            }
            return ImportWareHouse;
        }
        public static ExportWareHouse ToDataTransferExportWareHouse(T_KDT_VNACCS_WarehouseExport warehouseExport, T_KDT_VNACCS_WarehouseExport_Detail warehouseExport_Detail, T_KDT_VNACCS_WarehouseExport_GoodsDetail warehouseExport_GoodsDetail, string TenDoanhNghiep)
        {
            bool isEdit = warehouseExport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            ExportWareHouse ExportWareHouse = new ExportWareHouse()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = warehouseExport.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = warehouseExport.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Warehouse = new NameBase()
                {
                    Name = warehouseExport.TenKho,
                    Identity = warehouseExport.MaKho
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            ExportWareHouse.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = warehouseExport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            ExportWareHouse.Issuer = DeclarationIssuer.EXportWareHouse;
            ExportWareHouse.DeclarationOffice = warehouseExport.MaHQ.Trim();
            ExportWareHouse.CustomsReference = isEdit ? warehouseExport.SoTN.ToString(): String.Empty;
            ExportWareHouse.Acceptance = warehouseExport.NgayTN.ToString(sfmtDateTime);
            ExportWareHouse.StartDate = warehouseExport.NgayBatDauBC.ToString(sfmtDate);
            ExportWareHouse.FinishDate = warehouseExport.NgayKetthucBC.ToString(sfmtDate);
            AdditionalDocuments additionalDocuments = new AdditionalDocuments()
            {
                AdditionalDocument = new List<AdditionalDocument>(),
            };

            foreach (T_KDT_VNACCS_WarehouseExport_Detail item in warehouseExport.WarehouseExportCollection)
            {
                additionalDocuments.AdditionalDocument.Add(new AdditionalDocument
                {
                    Sequence = item.STT.ToString(),
                    Identification = item.SoPhieuXuat,
                    Issue = item.NgayPhieuXuat.ToString(sfmtDate),
                    NameConsignee = item.TenNguoiNhanHang,
                    IdentityConsignee = item.MaNguoiNhanHang,
                    CustomsGoodsItem = T_KDT_VNACCS_WarehouseExport_GoodsDetail.SelectCollectionBy_WarehouseExport_Details_ID_New(item.ID),
                });
            }
            ExportWareHouse.AdditionalDocumentNew = additionalDocuments;
            ExportWareHouse.DeclarationDocument = new DeclarationDocument()
            {
                Reference = warehouseExport.SoTKChungTu.ToString(),
                Type = warehouseExport.Loai.ToString()
            };
            ExportWareHouse.AdditionalInformationNew = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = warehouseExport.GhiChuKhac,
            };
            return ExportWareHouse;
        }

        public static GC_ImportWareHouse ToDataTransferExportWareHouseNew(T_KDT_VNACCS_WarehouseExport warehouseExport, T_KDT_VNACCS_WarehouseExport_Detail warehouseExport_Detail, T_KDT_VNACCS_WarehouseExport_GoodsDetail warehouseExport_GoodsDetail, string TenDoanhNghiep ,string Status)
        {
            GC_ImportWareHouse ExportWareHouse = new GC_ImportWareHouse()
            {

                Issuer = DeclarationIssuer.EXportWareHouse,
                Reference = warehouseExport.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                //Function = DeclarationFunction.SUA,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = warehouseExport.SoTN.ToString(),
                Acceptance = warehouseExport.NgayTN.ToString(sfmtDate),
                DeclarationOffice = warehouseExport.MaHQ,
                StartDate = warehouseExport.NgayBatDauBC.ToString(sfmtDate),
                FinishDate = warehouseExport.NgayKetthucBC.ToString(sfmtDate),
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Identity = warehouseExport.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Warehouse = new NameBase()
                {
                    Name = warehouseExport.TenKho,
                    Identity = warehouseExport.MaKho
                },
            };
            if (Status=="Send")
            {
                ExportWareHouse.Function = DeclarationFunction.KHAI_BAO;
            }
            else if (Status == "Edit")
            {
                ExportWareHouse.Function = DeclarationFunction.SUA;
            }
            else
            {
                ExportWareHouse.Function = DeclarationFunction.HUY;
            }
            // Người Khai Hải Quan
            ExportWareHouse.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = warehouseExport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            PNK_AdditionalDocuments additionalDocuments = new PNK_AdditionalDocuments()
            {
                AdditionalDocument = new List<PNK_AdditionalDocument>(),
            };

            foreach (T_KDT_VNACCS_WarehouseExport_Detail item in warehouseExport.WarehouseExportCollection)
            {
                int i = 1;
                additionalDocuments.AdditionalDocument.Add(new PNK_AdditionalDocument
                {
                    Sequence = i++.ToString(),
                    Identification = item.SoPhieuXuat,
                    Issue = item.NgayPhieuXuat.ToString(sfmtDate),
                    NameConsignee = item.TenNguoiNhanHang,
                    IdentityConsignee = item.MaNguoiNhanHang,
                    CustomsGoodsItem = T_KDT_VNACCS_WarehouseExport_GoodsDetail.SelectCollectionBy_WarehouseExport_Details_ID_Update(item.ID,warehouseExport),
                    AdditionalInformation = new PNK_AdditionalInformation()
                    {
                        Content = warehouseExport.GhiChuKhac,
                    }
                });
            }
            ExportWareHouse.AdditionalDocuments = additionalDocuments;
            //ImportWareHouse.ContractReference = new PNK_ContractReference()
            //{
            //    Reference = "SOFTECH NEW",
            //    Issue = "2018-03-16",
            //    DeclarationOffice = "01TE",
            //    Expire = "2019-03-16",

            //};
            return ExportWareHouse;
        }
        public static ScrapInformation_VNACCS ToDataTransferScrapInformation(KDT_VNACCS_ScrapInformation scrapInformation, KDT_VNACCS_ScrapInformation_Detail scrapInformation_Detail, string TenDoanhNghiep)
        {
            ScrapInformation_VNACCS ScrapInformation = new ScrapInformation_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = scrapInformation.GuidString,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = scrapInformation.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                   
                },
                Time = DateTime.Now.ToString(sfmtDate),
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            ScrapInformation.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = scrapInformation.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            ScrapInformation.Issuer = DeclarationIssuer.ScrapInformation;
            ScrapInformation.DeclarationOffice = scrapInformation.MaHaiQuan.Trim();
            ScrapInformation.CustomsReference = scrapInformation.SoTiepNhan.ToString();
            ScrapInformation.Acceptance = scrapInformation.NgayTiepNhan.ToString(sfmtDate);
            ScrapInformation.Scrap = new List<Company.KDT.SHARE.Components.Messages.GoodsItem.Scrap>();
            List<Company.KDT.SHARE.Components.Messages.GoodsItem.Scrap> scrap = ScrapInformation.Scrap;
            foreach (KDT_VNACCS_ScrapInformation_Detail item in scrapInformation.ScrapInformationCollection)
            {
                Company.KDT.SHARE.Components.Messages.GoodsItem.Scrap scraps = new Company.KDT.SHARE.Components.Messages.GoodsItem.Scrap()
                {
                    Commodity = new Commodity()
                    {
                        Description = item.TenHangHoa,
                        Identification = item.MaHangHoa,
                        Type = item.LoaiHangHoa.ToString(),
                    },
                    GoodsMeasure = new GoodsMeasure() 
                    {
                        Quantity = item.SoLuong.ToString().Replace(",","."),
                        MeasureUnit = item.DVT,
                    },
                };
                scrap.Add(scraps);
            }
            //ScrapInformation.Scrap = scrapInformation;
            return ScrapInformation;
        }

        public static GoodsItems_VNACCS ToDataTransferTotalInventoryReport(KDT_VNACCS_TotalInventoryReport totalInventoryReport, KDT_VNACCS_TotalInventoryReport_Detail totalInventoryRepor_Detail, string TenDoanhNghiep,string DiaChi,string Status)
        {
            GoodsItems_VNACCS GoodsItems_VNACCS = new GoodsItems_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = totalInventoryReport.GuidString,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = totalInventoryReport.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            if (Status=="Send")
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else if (Status == "Edit")
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.SUA;
            }
            else
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.HUY;
            }
            // Người Khai Hải Quan
            GoodsItems_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = totalInventoryReport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            GoodsItems_VNACCS.FinishDate = totalInventoryReport.NgayChotTon.ToString(sfmtDate);
            GoodsItems_VNACCS.UpdateType = totalInventoryReport.LoaiSua.ToString();
            GoodsItems_VNACCS.Type = totalInventoryReport.LoaiBaoCao.ToString();
            GoodsItems_VNACCS.Issuer = DeclarationIssuer.TotalInventoryReport;
            GoodsItems_VNACCS.DeclarationOffice = totalInventoryReport.MaHaiQuan.Trim();
            GoodsItems_VNACCS.CustomsReference = totalInventoryReport.SoTiepNhan.ToString();
            GoodsItems_VNACCS.Acceptance = totalInventoryReport.NgayTiepNhan.ToString(sfmtDateTime);
            GoodsItems goodsItems = new GoodsItems()
            {
                GoodsItem = new List<GoodsItem>()
            };
            foreach (KDT_VNACCS_TotalInventoryReport_Detail item in totalInventoryReport.DetailCollection)
            {
                goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                {
                    Description = item.TenHangHoa,
                    Identification = item.MaHangHoa,
                    Quantity1 = Decimal.Round(item.SoLuongTonKhoSoSach,4, MidpointRounding.AwayFromZero),
                    Quantity2 = Decimal.Round(item.SoLuongTonKhoThucTe, 4, MidpointRounding.AwayFromZero),
                    MeasureUnit = item.DVT,
                });
            }
            GoodsItems_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = totalInventoryReport.GhiChuKhac
            };
            GoodsItems_VNACCS.GoodsItems = goodsItems;
            return GoodsItems_VNACCS;
        }

        public static BillOfLadingNew_VNACCS ToDataTransferBillOfLadingNew(KDT_VNACCS_BillOfLadingNew billOfLading)
        {

            BillOfLadingNew_VNACCS BillOfLading_VNACCS = new BillOfLadingNew_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = billOfLading.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = billOfLading.MaDoanhNghiep,
                    Name = billOfLading.TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            BillOfLading_VNACCS.Agents.Add(new Agent
            {
                Name = billOfLading.TenDoanhNghiep,
                Identity = billOfLading.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            BillOfLading_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            BillOfLading_VNACCS.CustomsReference = string.Empty;
            BillOfLading_VNACCS.Acceptance = string.Empty;
            BillOfLading_VNACCS.Issuer = DeclarationIssuer.BillOfLadingNew;
            BillOfLading_VNACCS.DeclarationOffice = billOfLading.MaHQ.Trim();

            BillOfLadingNews billOfLadings = new BillOfLadingNews()
            {
                BillOfLading = new List<BillOfLadingNew>(),
            };

            foreach (KDT_VNACCS_BillOfLadings_Detail item in billOfLading.Collection)
            {
                BranchDetails branchDetails = new BranchDetails()
                {
                    BranchDetail = new List<BranchDetail>(),
                };
                List<KDT_VNACCS_BranchDetail> collection = KDT_VNACCS_BranchDetail.SelectCollectionBy_BillOfLadings_Details_ID(item.ID);
                foreach (KDT_VNACCS_BranchDetail branchDetail in collection)
                {
                    BranchDetail_Containers transportEquipments = new BranchDetail_Containers()
                    {
                        TransportEquipment = new List<BranchDetail_Container>()
                    };
                    List<KDT_VNACCS_BranchDetail_TransportEquipment> TransportEquipmentCollction = KDT_VNACCS_BranchDetail_TransportEquipment.SelectCollectionBy_BranchDetail_ID(branchDetail.ID);
                    foreach (KDT_VNACCS_BranchDetail_TransportEquipment transportEquipment in TransportEquipmentCollction)
                    {
                        transportEquipments.TransportEquipment.Add(new BranchDetail_Container
                        {
                            Container = transportEquipment.SoContainer,
                            Seal = transportEquipment.SoSeal
                        });
                    }
                    branchDetails.BranchDetail.Add(new BranchDetail
                    {
                        Sequence = branchDetail.STT.ToString(),
                        Reference = branchDetail.SoVanDon,
                        ShipperName = branchDetail.TenNguoiGuiHang,
                        ShipperAddress = branchDetail.DiaChiNguoiGuiHang,
                        ConsigneeName = branchDetail.TenNguoiNhanHang,
                        ConsigneeAddress = branchDetail.DiaChiNguoiNhanHang,
                        NumberOfCont = branchDetail.TongSoLuongContainer.ToString(),
                        CargoPiece = branchDetail.SoLuongHang.ToString(),
                        PieceUnitCode = branchDetail.DVTSoLuong,
                        CargoWeight = branchDetail.TongTrongLuongHang.ToString().Replace(",", "."),
                        WeightUnitCode = branchDetail.DVTTrongLuong,
                        TransportEquipments = transportEquipments
                    });
                }
                billOfLadings.BillOfLading.Add(new BillOfLadingNew
                {
                    Reference = item.SoVanDonGoc,
                    Issue = item.NgayVanDonGoc.ToString(sfmtDate),
                    Issuer = item.MaNguoiPhatHanh,
                    Number = item.SoLuongVDN.ToString(),
                    Type = item.PhanLoaiTachVD.ToString(),
                    IsContainer = item.LoaiHang.ToString(),
                    BranchDetails = branchDetails
                });
            }
            BillOfLading_VNACCS.BillOfLadings = billOfLadings;
            return BillOfLading_VNACCS;
        }
        #region Khai báo Thu phí cảng HP
        public static Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information ToDataTransferRegisterInformation(T_KDT_THUPHI_DOANHNGHIEP customer, string TenDoanhNghiep)
        {
            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information Register_Information = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information()
            {
                Issuer = DeclarationIssuer.RegisterInformation,
                Reference = customer.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                Company = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Company()
                {
                    Name = customer.TenDoanhNghiep,
                    Identity = customer.MaDoanhNghiep,
                    Address = customer.DiaChi,
                    Contact = customer.NguoiLienHe,
                    Email = customer.Email,
                    Phone = customer.SoDienThoai,
                },
                SignDigital = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.SignDigital()
                {
                    Serial = customer.Serial,
                    CertString = customer.CertString,
                    Subject = customer.Subject,
                    Issuer = customer.Issuer,
                    ValidFrom = customer.ValidFrom.ToString(sfmtDate),
                    ValidTo = customer.ValidTo.ToString(sfmtDate),
                }
            };
            return Register_Information;
        }

        public static Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_HangContainer ToDataTransferRegisterTK(T_KDT_THUPHI_TOKHAI TK, string TenDoanhNghiep)
        {
            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_HangContainer Register_HangContainer = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_HangContainer()
            {
                Issuer = TK.LoaiHangHoa == 100 ? DeclarationIssuer.RegisterHangContainer : DeclarationIssuer.RegisterHangRoi,
                Reference = TK.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = String.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = TK.SoTiepNhan.ToString(),
                Acceptance = TK.NgayTiepNhan.ToString(sfmtDateTime),
                DeclarationOffice = TK.DiemThuPhi,
                Agent = new Agent()
                {
                    Name = TK.TenDoanhNghiep,
                    Identity = TK.MaDoanhNghiep,
                },
                Importer = new NameBase()
                {
                    Name = TK.TenDoanhNghiep,
                    Identity = TK.MaDoanhNghiep,
                },
                NoticeOfPayment = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.NoticeOfPayment()
                {
                    Notice = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Notice()
                    {
                        NoticeNo = TK.SoTKNP,
                        NoticeDate = TK.NgayTKNP.ToString(sfmtDate),
                        PaymentMethod = TK.HinhThucTT,
                    },
                    CustomsReference = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.CustomsReference()
                    {
                        CustomsDeclare = TK.SoTK,
                        CustomsDeclareDate = TK.NgayTK.ToString(sfmtDate),
                        CustomsDeclareType = TK.MaLoaiHinh,
                        CustomsCode = TK.MaHQ,
                        TariffTypeCode = TK.NhomLoaiHinh,
                        TransportCode = TK.MaPTVC.ToString(),
                        DestinationCode = TK.MaDiaDiemLuuKho,
                    },
                    AdditionalInformation = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_AdditionalInformation()
                    {
                        Content = String.IsNullOrEmpty(TK.GhiChu) ? "" : TK.GhiChu,
                    }
                }
            };

            Register_HangContainer.Issuer = TK.LoaiHangHoa == 100 ? DeclarationIssuer.RegisterHangContainer : DeclarationIssuer.RegisterHangRoi;

            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipments transportEquipments = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipments()
            {
                TransportEquipment = new List<Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipment>()
            };
            if (TK.LoaiHangHoa == 100)
            {
                foreach (T_KDT_THUPHI_TOKHAI_DSCONTAINER item in TK.ContainerCollection)
                {
                    transportEquipments.TransportEquipment.Add(new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipment
                    {
                        BillOfLading = item.SoVanDon,
                        Container = item.SoContainer,
                        Seal = item.SoSeal,
                        ContainerType = item.Loai.ToString(),
                        ContainerKind = item.TinhChat.ToString(),
                        Quantity = item.SoLuong.ToString(),
                        JournalMemo = item.GhiChu,
                    });
                }
            }
            else
            {
                foreach (T_KDT_THUPHI_TOKHAI_DSCONTAINER item in TK.ContainerCollection)
                {
                    transportEquipments.TransportEquipment.Add(new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipment
                    {
                        BillOfLading = item.SoVanDon,
                        GrossMass = item.TongTrongLuong,
                        MeasureUnit = item.DVT,
                        JournalMemo = item.GhiChu,
                    });
                }
            }
            Register_HangContainer.NoticeOfPayment.TransportEquipments = transportEquipments;

            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.FileAttachs fileAttachs = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.FileAttachs()
            {
                File = new List<Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.File>()
            };
            foreach (T_KDT_THUPHI_TOKHAI_FILE item in TK.FileCollection)
            {
                fileAttachs.File.Add(new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.File
                {
                    Type = item.Loai,
                    Name = item.TenFile,
                    Content = item.Content,
                });
            }
            Register_HangContainer.NoticeOfPayment.FileAttachs = fileAttachs;
            return Register_HangContainer;
        }

        public static Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_InfomationSearch ToDataTransferSearchInformation(T_KDT_THUPHI_BIENLAI BL, string TenDoanhNghiep, string LoaiHangHoa, string SoBienLai, string SoToKhai, string SoVanDon, string SoContainer)
        {
            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_InfomationSearch InfomationSearch = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_InfomationSearch()
            {
                Issuer = DeclarationIssuer.SearchInformation,
                Reference = BL.GuidString,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = "",
                Acceptance = "",
                DeclarationOffice = "03CC",
                Agent = new Agent()
                {
                    Name = "Chi cục HQ CK cảng Hải Phòng KV I",
                    Identity = "03CC",
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                },
                InfomationSearch = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.InfomationSearch()
                {
                    GoodItemType = LoaiHangHoa,
                    ReceiptNo = SoBienLai,
                    CustomsReference = SoToKhai,
                    BillOfLading = SoVanDon,
                    ContainerNo = SoContainer,
                },
            };
            return InfomationSearch;
        }
        #endregion
    }
}