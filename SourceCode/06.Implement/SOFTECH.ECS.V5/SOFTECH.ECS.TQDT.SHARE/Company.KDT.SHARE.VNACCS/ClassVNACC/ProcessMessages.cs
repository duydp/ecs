﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Company.KDT.SHARE.VNACCS.Maper;

namespace Company.KDT.SHARE.VNACCS.ClassVNACC
{
    public class ProcessMessages
    {
        public static KDT_VNACC_ToKhaiMauDich XuLyMsg(ReturnMessages returnMSG)
        {
            return XuLyMsg(returnMSG, false);
        }

        public static KDT_VNACC_ToKhaiMauDich XuLyMsg(ReturnMessages returnMSG, bool IsThemMoi)
        {
            try
            {
        
            if(returnMSG != null && returnMSG.header != null && returnMSG.header.MaNghiepVu != null)
            {
                string NghiepVu = returnMSG.getMaNVPhanHoi();
                if(!string.IsNullOrEmpty(NghiepVu))
                {
                    if(EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhai.Contains(NghiepVu))
                    {
                        KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                        //TKMD.
                        TKMD = KDT_VNACC_ToKhaiMauDich.Load(KDT_VNACC_ToKhaiMauDich.GetID(System.Convert.ToDecimal(returnMSG.GetSoTNHeader())));
                        //TKMD = new KDT_VNACC_ToKhaiMauDich();
                        if (TKMD != null && TKMD.ID > 0)
                            TKMD.LoadFull();
                        else if (!IsThemMoi)
                            return null;
                        else if (IsThemMoi)
                            TKMD = new KDT_VNACC_ToKhaiMauDich();

                        GetDataResult_TKMD(returnMSG, NghiepVu, TKMD);
                        if (TKMD != null)
                            if (!IsThemMoi)
                            {
                                if (TKMD.InsertUpdateFull())
                                    return TKMD;
                            }
                            else
                                return TKMD;
                    }
                       
                }
            }
            return null;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static string GetMaNV(ReturnMessages returnMSG, bool SSR)
        {
            string MaNV = string.Empty;
            if (!SSR)
                MaNV = returnMSG.header.MaNghiepVu.GetValue().ToString();
            if (string.IsNullOrEmpty(MaNV) || string.IsNullOrEmpty(MaNV.Trim()))
                MaNV = returnMSG.getMaNVPhanHoi();
            return MaNV;

        }
        public static string GetMaNV(ReturnMessages returnMSG)
        { return GetMaNV(returnMSG, false); }
        public static object GetDataResult_HoaDon(ReturnMessages returnMSG, string MaNghiepVu, KDT_VNACC_HoaDon HoaDon)
        {
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG);
            }
            if (!string.IsNullOrEmpty(MaNV))
            {
                #region phản hồi Hóa đơn
                if ((MaNV == EnumNghiepVu.IVA01 || MaNV == EnumNghiepVu.IVA)
                    || (MaNV == EnumNghiepVuPhanHoi.VAL0870 || MaNV == EnumNghiepVuPhanHoi.VAL0880 || MaNV == EnumNghiepVuPhanHoi.VAL0890 || MaNV == EnumNghiepVuPhanHoi.VAL0940))
                {
                    if (MaNV == EnumNghiepVuPhanHoi.VAL0880)
                    {
                        VAL0880 val0880 = new VAL0880();// in phiếu tiếp nhận - chưa xử lý
                        val0880.GetObject<VAL0880>(returnMSG.Body.ToString(), EnumNghiepVuPhanHoi.VAL0880, true, VAL0880.TongSoByte);
                        HoaDon.SoTiepNhan = System.Convert.ToDecimal(val0880.SOTIEPNHANHOADONDIENTU.GetValue());
                        HoaDon.MaNguoiXNK = val0880.MANGUOIXUATKHAU.GetValue().ToString();
                        HoaDon.NgayTiepNhan = System.Convert.ToDateTime(val0880.NGAYTHANGNAM.GetValue());
                        //HoaDon.NguoiSU  val0880.NGUOISUDUNG.GetValue().ToString();
                        HoaDon.PhanLoaiVanChuyen = val0880.PHANLOAIVANCHUYEN.GetValue().ToString();
                        HoaDon.PhanLoaiXuatNhap = val0880.PHANLOAIXUATNHAPKHAU.GetValue().ToString();
                        HoaDon.SoHoaDon = val0880.SOHOADON.GetValue().ToString();
                        HoaDon.SoPL = val0880.SOPL.GetValue().ToString();
                        HoaDon.TenNguoiXNK = val0880.TENNGUOIXUATKHAU.GetValue().ToString();
                        return HoaDon;
                    }
                    else if ((MaNV == EnumNghiepVuPhanHoi.VAL0870 || MaNV == EnumNghiepVuPhanHoi.VAL0890)
                        || (MaNV == EnumNghiepVu.IVA || MaNV == EnumNghiepVu.IVA01))
                    {
                        VAL0870 val = new VAL0870(returnMSG.Body.ToString());
                        HoaDon.TrangThaiXuLy = "1";
                        return VNACCMaperToObject.GetFromIVA(val, HoaDon);
                    }
                    else if (MaNV == EnumNghiepVuPhanHoi.VAL0940)
                    {
                        VAL0940 val = new VAL0940(returnMSG.Body.ToString());
                        return VNACCMaperToObject.GetFromIVA(val, HoaDon);
                    }
                }
                #endregion
            }

            return null;

        }


        public static object GetDataResult_ToKhaiTriGiaThap(ReturnMessages returnMSG, string MaNghiepVu, KDT_VNACCS_TKTriGiaThap TKTriGiaThap)
        {
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG);
            }
            if (!string.IsNullOrEmpty(MaNV))
            {
                #region phản hồi tờ khai trị giá thấp
                if (MaNghiepVu == EnumNghiepVu.MIC)
                {
                    MIC mic = new MIC();
                    mic.GetObject<MIC>(returnMSG.Body.ToString(), EnumNghiepVu.MIC, true, MIC.TongSoByte);
                    return VNACCMaperToObject.GetFromMIC(mic, TKTriGiaThap);
                }
                else if (MaNghiepVu == EnumNghiepVu.MEC)
                {
                    MEC mec = new MEC();
                    mec.GetObject<MEC>(returnMSG.Body.ToString(), EnumNghiepVu.MEC, true, MIC.TongSoByte);
                    return VNACCMaperToObject.GetFromMEC(mec, TKTriGiaThap);
                }

                #endregion
            }

            return null;

        }
        public static object GetDataResult_TKMD(ReturnMessages returnMSG, string MaNghiepVu, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG, returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR");
            }
            if (!string.IsNullOrEmpty(MaNV))
            {
                #region phản hồi tờ khai
                if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiNhap_BanXacNhan.Contains(MaNV))
                {
                    VAD1AC0 vad1 = new VAD1AC0();
                    vad1.LoadVAD1AC0(returnMSG.Body.ToString());
                    if (returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR")
                    {
                        if (MaNV == EnumNghiepVuPhanHoi.VAD0AB0)
                            TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoSua;
                        else
                            TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoTam;
                        TKMD = VNACCMaperToObject.GetTKMDFromVAD1AC0(vad1, TKMD);

                    }
                    else
                    {
                        if (MaNV == EnumNghiepVuPhanHoi.VAD2AY0 || MaNV == EnumNghiepVuPhanHoi.VAD3AY0 || MaNV == EnumNghiepVuPhanHoi.VAD2AC0)
                            TKMD = VNACCMaperToObject.GetTKMDFromVAD1AC0(vad1, TKMD);
                        TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoChinhThuc;                    
                    }
                  
                    //TKMD.MaPhanLoaiKiemTra = MaNV.Substring(3, 1);
                    if (vad1.A06.GetValue().ToString().Contains("1"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 1.ToString();
                    }
                    else if (vad1.A06.GetValue().ToString().Contains("2"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 2.ToString();
                    }
                    else if (vad1.A06.GetValue().ToString().Contains("3"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 3.ToString();
                    }
                    TKMD.NgayDangKy = System.Convert.ToDateTime(vad1.A09.GetValue());
                    TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, vad1.AD1.GetValue().ToString());
                    
                    return TKMD;
                }
                else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiNhap_BanChapNhanThongQuan.Contains(MaNV))
                {
                    VAD1AG0 vad1 = new VAD1AG0();
                    vad1.LoadVAD1AG0(returnMSG.Body.ToString());
                    //if (returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR")
                    //{
                    //    TKMD = VNACCMaperToObject.GetTKMDFromVAD1AG(vad1, TKMD);
                    //    TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.ThongQuan;
                    //}
                    //else
                    //{
                    //    TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.ThongQuan;
                    //}
                    //TKMD.MaPhanLoaiKiemTra = MaNV.Substring(3, 1);
                    TKMD = VNACCMaperToObject.GetTKMDFromVAD1AG(vad1, TKMD);
                    TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.ThongQuan;

                    if (vad1.A06.GetValue().ToString().Contains("1"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 1.ToString();
                    }
                    else if (vad1.A06.GetValue().ToString().Contains("2"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 2.ToString();
                    }
                    else if (vad1.A06.GetValue().ToString().Contains("3"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 3.ToString();
                    }
                    TKMD.NgayDangKy = System.Convert.ToDateTime(vad1.A09.GetValue());
                    TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, vad1.AD1.GetValue().ToString());
                    //Company.BLL.VNACCS.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(TKMD);
                    return TKMD;
                }
                else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanChapNhanThongQuan.Contains(MaNV))
                {
                    //TKMD.MaPhanLoaiKiemTra = MaNV.Substring(3, 1);
                    
                    TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.ThongQuan;
                    VAE1LF0 vae = new VAE1LF0();
                    vae.LoadVAE1LF0(returnMSG.Body.ToString());
                    //
                    TKMD = VNACCMaperToObject.GetTKMDFromVAE1LF0(vae, TKMD);
                    TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.ThongQuan;

                    TKMD.NgayCapPhep = System.Convert.ToDateTime(vae.K86.GetValue());
                    TKMD.NgayCapPhep = HelperVNACCS.AddHoursToDateTime(TKMD.NgayCapPhep, vae.AD5.GetValue().ToString());
                    TKMD.NgayDangKy = System.Convert.ToDateTime(vae.K10.GetValue());
                    TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, vae.AD1.GetValue().ToString());
                    if (vae.K07.GetValue().ToString().Contains("1"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 1.ToString();
                    }
                    else if (vae.K07.GetValue().ToString().Contains("2"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 2.ToString();
                    }
                    else if (vae.K07.GetValue().ToString().Contains("3"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 3.ToString();
                    }
                }
                else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanXacNhan.Contains(MaNV))
                {
                    VAE1LD0 vae = new VAE1LD0();
                    vae.LoadVAE1LD0(returnMSG.Body.ToString());
                    if (returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR")
                    {
                        TKMD = VNACCMaperToObject.GetTKMDFromVAE(vae, TKMD);
                        if (MaNV == EnumNghiepVuPhanHoi.VAE0LB0)
                            TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoSua;
                        else
                            TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoTam;
                        //TKMD.TongHeSoPhanBoTG = 0;
                    }
                    else
                    {
                        if (MaNV == EnumNghiepVuPhanHoi.VAE2LY0 || MaNV == EnumNghiepVuPhanHoi.VAE3LY0 || MaNV == EnumNghiepVuPhanHoi.VAE2LD0)
                            TKMD = VNACCMaperToObject.GetTKMDFromVAE(vae, TKMD);
                        TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoChinhThuc;
                    }
                    //TKMD.MaPhanLoaiKiemTra = MaNV.Substring(3, 1);
                    if (vae.K07.GetValue().ToString().Contains("1"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 1.ToString();
                    }
                    else if (vae.K07.GetValue().ToString().Contains("2"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 2.ToString();
                    }
                    else if (vae.K07.GetValue().ToString().Contains("3"))
                    {
                        TKMD.MaPhanLoaiKiemTra = 3.ToString();
                    }
                    TKMD.NgayDangKy = System.Convert.ToDateTime(vae.K10.GetValue());
                    TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, vae.AD1.GetValue().ToString());
                    return TKMD;
                }
                else if(MaNV == EnumNghiepVuPhanHoi.VAD4190 || MaNV == EnumNghiepVuPhanHoi.VAD4240)
                {
                    VAD4190 vad = new VAD4190();
                    vad.LoadVAD4190(returnMSG.Body.ToString());
                    TKMD = VNACCMaperToObject.GetTKMDFromVAD4190(vad, TKMD);
                    TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoTam;
                }
                else if (MaNV == EnumNghiepVuPhanHoi.VAD0AP0)
                {
                    VAD0AP0 vad = new VAD0AP0();
                    vad.LoadVAD0AP0(HelperVNACCS.SubStringByBytes(returnMSG.Body.ToString(), 77));
                    TKMD = VNACCMaperToObject.GetTKMDFromVAD0AP0(vad, TKMD);
                    TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoChinhThuc;
                }
                else if (MaNV == EnumNghiepVuPhanHoi.VAE0LC0)
                {
                    VAE0LC0 vae = new VAE0LC0();
                    vae.LoadVAE0LC0(HelperVNACCS.SubStringByBytes(returnMSG.Body.ToString(), 77));
                    TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoChinhThuc;
                    TKMD = VNACCMaperToObject.GetTKMDFromVAE0LC0(vae, TKMD);
                    
                }
                else if (MaNV == EnumNghiepVuPhanHoi.VAE4000 || MaNV == EnumNghiepVuPhanHoi.VAE4110)
                {
                    VAE4000 vad = new VAE4000();
                    vad.LoadVAE4000(returnMSG.Body.ToString());
                    TKMD = VNACCMaperToObject.GetTKMDFromVAE4000(vad, TKMD);
                    TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoTam;
                }
                else if (MaNV == EnumNghiepVuPhanHoi.VAD8000)
                {
                    VAD8000 vad8000 = new VAD8000();
                    vad8000.GetObject<VAD8000>(returnMSG.Body.ToString(), "VAD8000", false, VAD8000.TongSoByte);
                    List<KDT_VNACC_ChiThiHaiQuan> lChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
                    for (int i = 0; i < vad8000.CT.listAttribute[0].ListValue.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(vad8000.CT.listAttribute[0].GetValueCollection(i).ToString().Trim()) &&  Convert.ToDateTime(vad8000.CT.listAttribute[0].GetValueCollection(i)).Year > 1900)
                        {
                            KDT_VNACC_ChiThiHaiQuan ChiThiHQ = new KDT_VNACC_ChiThiHaiQuan();
                            ChiThiHQ.Ngay = Convert.ToDateTime(vad8000.CT.listAttribute[0].GetValueCollection(i));
                            ChiThiHQ.Ten = vad8000.CT.listAttribute[1].GetValueCollection(i).ToString();
                            ChiThiHQ.NoiDung = vad8000.CT.listAttribute[2].GetValueCollection(i).ToString();
                            lChiThi.Add(ChiThiHQ);
                        }
                    }
                    TKMD.ChiThiHQCollection = lChiThi;
                }
                else if (MaNV == EnumNghiepVuPhanHoi.VAE8000)
                {
                    VAE8000 vae8000 = new VAE8000();
                    vae8000.GetObject<VAE8000>(returnMSG.Body.ToString(), "VAE8000", false, VAE8000.TongSoByte);
                    List<KDT_VNACC_ChiThiHaiQuan> lChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
                    for (int i = 0; i < vae8000.CT.listAttribute[0].ListValue.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(vae8000.CT.listAttribute[0].GetValueCollection(i).ToString().Trim()) && Convert.ToDateTime(vae8000.CT.listAttribute[0].GetValueCollection(i)).Year > 1900)
                        {
                            KDT_VNACC_ChiThiHaiQuan ChiThiHQ = new KDT_VNACC_ChiThiHaiQuan();
                            ChiThiHQ.Ngay = Convert.ToDateTime(vae8000.CT.listAttribute[0].GetValueCollection(i));
                            ChiThiHQ.Ten = vae8000.CT.listAttribute[1].GetValueCollection(i).ToString();
                            ChiThiHQ.NoiDung = vae8000.CT.listAttribute[2].GetValueCollection(i).ToString();
                            lChiThi.Add(ChiThiHQ);
                        }
                    }
                    TKMD.ChiThiHQCollection = lChiThi;
                }
                else
                {
                    return TKMD; //throw new Exception("Không thể phân tích nội dung trả về");
                }
                #endregion
            }
            return null;
        }

        //TODO: Chua lam
        public static object GetDataResult_TKBoSung(ReturnMessages returnMSG, string MaNghiepVu, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBS)
        {
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG, returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR");
            }
            if (!string.IsNullOrEmpty(MaNV))
            {
                if (MaNV == "VAL8020")
                    TKBS.TrangThaiXuLy = System.Convert.ToInt16(EnumTrangThaiXuLy.KhaiBaoChinhThuc);
                else if (MaNV == "VAL8030" || MaNV == "VAL8040" || MaNV == "VAL8050")
                    TKBS.TrangThaiXuLy = System.Convert.ToInt16(EnumTrangThaiXuLy.ThongQuan);
                else if (MaNV == "VAL8060")
                    TKBS.TrangThaiXuLy = System.Convert.ToInt16(EnumTrangThaiXuLy.TuChoi);

            }
            return TKBS;
        }
        public static object GetDataResult_TEA(ReturnMessages returnMSG, string MaNghiepVu, KDT_VNACCS_TEA TKBS)
        {
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG, returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR");
            }
            if (!string.IsNullOrEmpty(MaNV))
            {
                if (MaNV == EnumNghiepVuPhanHoi.VAD8010)              
                    TKBS.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoSua;
            }
            return TKBS;
        }
        //TODO: Chua lam
        public static object GetDataResult_ChungTuDinhKem(ReturnMessages returnMSG, string MaNghiepVu, KDT_VNACC_ChungTuDinhKem ChungTuKem)
        {
            //return null;
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG, returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR");
            }

            if (!string.IsNullOrEmpty(MaNV))
            {
                string trangThaiXuLy = ChungTuKem.TrangThaiXuLy;
                if (MaNV == EnumNghiepVuPhanHoi.VAL2800)
                {
                    trangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoChinhThuc;
                    VAL2800 val = new VAL2800();
                    val.LoadVAL2800(returnMSG.Body.ToString());
                    ChungTuKem.SoTiepNhan = System.Convert.ToDecimal(val.JNO.GetValue());
                    ChungTuKem.NgayTiepNhan = System.Convert.ToDateTime(val.YMD.GetValue());
                }
                else if (MaNV == EnumNghiepVuPhanHoi.VAL0020)
                {
                    trangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoChinhThuc;
                    VAL0020 val = new VAL0020();
                    val.LoadVAL0020(returnMSG.Body.ToString());
                    ChungTuKem.SoTiepNhan = System.Convert.ToDecimal(val.JNO.GetValue());
                    ChungTuKem.NgayTiepNhan = System.Convert.ToDateTime(val.YMD.GetValue());
                }
                else if (MaNV == EnumNghiepVuPhanHoi.VAL2700)
                {
                    trangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoChinhThuc;
                    VAL2700 val = new VAL2700();
                    val.LoadVAL2700(returnMSG.Body.ToString());
                    ChungTuKem.SoTiepNhan = System.Convert.ToDecimal(val.AB.GetValue());
                    //ChungTuKem.NgayTiepNhan = System.Convert.ToDateTime(val.YMD.GetValue());
                }

                ChungTuKem.TrangThaiXuLy = trangThaiXuLy;
            }
            return ChungTuKem;
        }

        public static object GetDataResult_ChungTuThanhToan_IAS(ReturnMessages returnMSG, string MaNghiepVu, KDT_VNACC_ChungTuThanhToan CTTT)
        {
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG, returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR");
            }

            if (!string.IsNullOrEmpty(MaNV))
            {
                int trangThaiXuLy = Convert.ToInt32(EnumTrangThaiXuLy.ChuaKhaiBao);

                if (CheckInGroup(MaNV, EnumNghiepVuPhanHoi.GroupPhanHoi_ChungTuThanhToan_HuongDan))
                {
                    CTTT.TrangThaiXuLy = EnumTrangThaiXuLy.ThongQuan;
                    if (MaNghiepVu == EnumNghiepVuPhanHoi.VAF8050)
                    {
                        VAF8050 vaf8050 = new VAF8050();
                        vaf8050.GetObject<VAF8050>(returnMSG.Body.ToString(), EnumNghiepVuPhanHoi.VAF8050, true, VAF8050.TongSoByte);
                        return VNACCMaperToObject.GetCTTTFromVAF805(vaf8050, CTTT);
                    }
                    else if (MaNghiepVu == EnumNghiepVuPhanHoi.VAF8060)
                    {
                        VAF8060 vaf8060 = new VAF8060();
                        vaf8060.LoadVAF8060(returnMSG.Body.ToString());
                        return VNACCMaperToObject.GetCTTTFromVAF806(vaf8060,CTTT);
                    }
                    else if (MaNghiepVu == EnumNghiepVuPhanHoi.VAF8070)
                    {
                        VAF8070 vaf8070 = new VAF8070();
                        vaf8070.GetObject<VAF8070>(returnMSG.Body.ToString(), EnumNghiepVuPhanHoi.VAF8070, true, VAF8070.TongSoByte);
                        return VNACCMaperToObject.GetCTTTFromvaf807(vaf8070, CTTT);
                    }
                    else if (MaNghiepVu == EnumNghiepVuPhanHoi.VAF8080)
                    {
                        VAF8080 vaf8080 = new VAF8080();
                        vaf8080.GetObject<VAF8080>(returnMSG.Body.ToString(), EnumNghiepVuPhanHoi.VAF8080, true, VAF8080.TongSoByte);
                        return VNACCMaperToObject.GetCTTTFromvaf808(vaf8080, CTTT);
                    }



                    return CTTT;

                   

                }

               
            }
            return CTTT;
        }

        public static object GetDataResult_ChungTuThanhToan_IBA(ReturnMessages returnMSG, string MaNghiepVu, KDT_VNACC_ChungTuThanhToan CTTT)
        {
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG, returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR");
            }

            if (!string.IsNullOrEmpty(MaNV))
            {
                int trangThaiXuLy = Convert.ToInt32(EnumTrangThaiXuLy.ChuaKhaiBao);

                if (CheckInGroup(MaNV, EnumNghiepVuPhanHoi.GroupPhanHoi_ChungTuThanhToan_HuongDan))
                {
                    //TODO: Chua ro trang thai
                }

                CTTT.TrangThaiXuLy = trangThaiXuLy.ToString();

            }
            return CTTT;
        }

        /// <summary>
        /// Lấy thông tin phản hồi
        /// </summary>
        /// <param name="returnMSG"></param>
        /// <param name="MaNghiepVu"></param>
        /// <param name="GiayPhep"></param>
        /// <returns></returns>
        public static object GetDataResult_GiayPhep(ReturnMessages returnMSG, string MaNghiepVu, IKDT_VNACC_GiayPhep GiayPhep, ELoaiThongTin loaiGiayPhep)
        {
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG, returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR");
            }

            if (!string.IsNullOrEmpty(MaNV))
            {
                int trangThaiXuLy = Convert.ToInt32(EnumTrangThaiXuLy.ChuaKhaiBao);

                #region Phản hồi thông tin

                if (CheckInGroup(MaNV, EnumNghiepVuPhanHoi.GroupPhanHoi_GiayPhep_BanXacNhan))
                {
                    trangThaiXuLy = Convert.ToInt32(EnumTrangThaiXuLy.KhaiBaoChinhThuc);
                }
                else if (CheckInGroup(MaNV, EnumNghiepVuPhanHoi.GroupPhanHoi_GiayPhep_BanDuyet))
                {
                    trangThaiXuLy = Convert.ToInt32(EnumTrangThaiXuLy.ThongQuan);
                }

                if (loaiGiayPhep == ELoaiThongTin.GP_SEA)
                    (GiayPhep as KDT_VNACC_GiayPhep_SEA).TrangThaiXuLy = trangThaiXuLy;
                else if (loaiGiayPhep == ELoaiThongTin.GP_SFA)
                    (GiayPhep as KDT_VNACC_GiayPhep_SFA).TrangThaiXuLy = trangThaiXuLy;
                else if (loaiGiayPhep == ELoaiThongTin.GP_SAA)
                    (GiayPhep as KDT_VNACC_GiayPhep_SAA).TrangThaiXuLy = trangThaiXuLy;
                else if (loaiGiayPhep == ELoaiThongTin.GP_SMA)
                    (GiayPhep as KDT_VNACC_GiayPhep_SMA).TrangThaiXuLy = trangThaiXuLy;

                #endregion
            }

            return GiayPhep;
        }

        public static object GetDataResult_GiayPhep(ReturnMessages returnMSG, string MaNghiepVu, object GiayPhep)
        {
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG, returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR");
            }

            if (!string.IsNullOrEmpty(MaNV))
            {
                int trangThaiXuLy = Convert.ToInt32(EnumTrangThaiXuLy.ChuaKhaiBao);

                #region Phản hồi thông tin

                if (CheckInGroup(MaNV, EnumNghiepVuPhanHoi.GroupPhanHoi_GiayPhep_BanXacNhan))
                {
                    trangThaiXuLy = Convert.ToInt32(EnumTrangThaiXuLy.KhaiBaoChinhThuc);
                }
                else if (CheckInGroup(MaNV, EnumNghiepVuPhanHoi.GroupPhanHoi_GiayPhep_BanDuyet))
                {
                    trangThaiXuLy = Convert.ToInt32(EnumTrangThaiXuLy.ThongQuan);
                }
                else if (CheckInGroup(MaNV, EnumNghiepVuPhanHoi.GroupPhanHoi_GiayPhep_TuChoi))
                {
                    trangThaiXuLy = Convert.ToInt32(EnumTrangThaiXuLy.TuChoi);
                }

                if (GiayPhep.GetType() == typeof(KDT_VNACC_GiayPhep_SEA))
                    (GiayPhep as KDT_VNACC_GiayPhep_SEA).TrangThaiXuLy = trangThaiXuLy;
                else if (GiayPhep.GetType() == typeof(KDT_VNACC_GiayPhep_SFA))
                    (GiayPhep as KDT_VNACC_GiayPhep_SFA).TrangThaiXuLy = trangThaiXuLy;
                else if (GiayPhep.GetType() == typeof(KDT_VNACC_GiayPhep_SAA))
                    (GiayPhep as KDT_VNACC_GiayPhep_SAA).TrangThaiXuLy = trangThaiXuLy;
                else if (GiayPhep.GetType() == typeof(KDT_VNACC_GiayPhep_SMA))
                    (GiayPhep as KDT_VNACC_GiayPhep_SMA).TrangThaiXuLy = trangThaiXuLy;

                #endregion
            }
            return GiayPhep;
        }



        public static KDT_VNACC_ToKhaiVanChuyen GetDataResult_TKVC(ReturnMessages returnMSG, string MaNghiepVu, KDT_VNACC_ToKhaiVanChuyen TKVC)
        {
            string MaNV;
            if (!string.IsNullOrEmpty(MaNghiepVu))
                MaNV = MaNghiepVu;
            else
            {
                MaNV = GetMaNV(returnMSG, returnMSG.header.MaDieuKhienGD.GetValue().ToString() == "SSR");
            }
            if (!string.IsNullOrEmpty(MaNV))
            {
                string trangThaiXuLy = TKVC.TrangThaiXuLy;
                if (MaNV == EnumNghiepVuPhanHoi.VAS5030)
                {
                    trangThaiXuLy =EnumTrangThaiXuLy.KhaiBaoChinhThuc;
                }
                else if (MaNV == EnumNghiepVuPhanHoi.VAS501)
                {
                    trangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoTam;
                    VAS501 vas = new VAS501();
                    vas.LoadVAS501(returnMSG.Body.ToString());
                    TKVC = Maper.VNACCMaperToObject.GetTKVCFromVAS5010(vas,TKVC);
                }
                else if (MaNV == EnumNghiepVuPhanHoi.VAS5050 || MaNV == EnumNghiepVuPhanHoi.VAS5040)
                {
                    trangThaiXuLy = EnumTrangThaiXuLy.ThongQuan;
                    VAS5050 vas = new VAS5050();
                    vas.LoadVAS5050(returnMSG.Body.ToString());
                    TKVC = Maper.VNACCMaperToObject.GetTKVCFromVAS5050(vas, TKVC);
                }
                else if (MaNV == EnumNghiepVuPhanHoi.VAS5130)
                {
                    trangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoSua;
                    VAS5130 vas = new VAS5130();
                    vas.LoadVAS5050(returnMSG.Body.ToString());
                    
                    TKVC = Maper.VNACCMaperToObject.GetTKVCFromVAS5130(vas, TKVC);
                }
                TKVC.TrangThaiXuLy = trangThaiXuLy;
            }
            return TKVC;
        }
        public static bool CheckInGroup(string NghiepVu, List<string> NhomNghiepVu)
        {
            return NhomNghiepVu.Contains(NghiepVu);
        }



        
    }
}
