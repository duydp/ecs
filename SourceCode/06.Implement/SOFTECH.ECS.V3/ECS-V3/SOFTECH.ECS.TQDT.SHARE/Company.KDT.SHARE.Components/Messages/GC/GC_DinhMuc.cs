﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class GC_DinhMuc : DeclarationBase
    {
        /// <summary>
        /// Thông tin hợp đồng
        /// </summary>
        [XmlElement("ContractReference")]
        public ContractDocument ContractReference { get; set; }
        /// <summary>
        /// Định mức 1 sản phẩm 
        /// </summary>
        [XmlElement("ProductionNorm")]
        public List<ProductionNorm> ProductionNorms { get; set; }


    }
}
