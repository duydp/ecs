﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.KDT.SXXK
{
    public partial class PhuKienManagerFrm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        private List<KDT_SXXK_PhuKienDangKy> ListPhuKien = new List<KDT_SXXK_PhuKienDangKy>();
        KDT_SXXK_PhuKienDangKy PKDK = new KDT_SXXK_PhuKienDangKy();
        public PhuKienManagerFrm()
        {
            InitializeComponent();
        }
        private void LoadData()
        {
            if (TKMD!= null&& TKMD.ID > 0)
            {
                ListPhuKien = KDT_SXXK_PhuKienDangKy.SelectCollectionDynamic("TKMD_id = " + TKMD.ID, "ID");
            }
            dgList.DataSource = ListPhuKien;
        }

        private void PhuKienManagerFrm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void cmMainPKHD_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //case "cmdSave":
                //    this.save();
                //    break;
                case "cmdAdd":
                    this.add(true);
                    break;
            }
        }
        private void add(bool isadd)
        {
            PhuKienTKSendForm f = new PhuKienTKSendForm();
            f.TKMD = TKMD;
            if (!isadd)
            {
                f.pkdk = PKDK;
            }
            f.ShowDialog();
            LoadData();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                long id = Convert.ToUInt32(dgList.CurrentRow.Cells["ID"].Value);
                PKDK= KDT_SXXK_PhuKienDangKy.LoadFull(id);
                add(false);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
                sendXML.master_id = PKDK.ID;
                                
                int ID = Convert.ToInt32(dgList.CurrentRow.Cells["ID"].Value.ToString());
                int trangthai =Convert.ToInt32(dgList.CurrentRow.Cells["TrangThaiXuLy"].Value.ToString().Trim());
                if (ID > 0)
                {
                    if (trangthai == 0 || trangthai == 1 || sendXML.Load())
                    {
                        ShowMessage("Phụ kiện đã được khai báo không thể xóa.", false);
                        return;
                    }
                    else
                    {
                        if (ShowMessage("Bạn có muốn xóa số phụ kiện này không?", true) == "Yes")
                        {
                            PKDK = new KDT_SXXK_PhuKienDangKy();
                            PKDK.ID = ID;
                            PKDK.DeleteFull(ID);
                            dgList.CurrentRow.Delete();
                        }
                    }
                }

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (trangthai)
                {
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                }
            }
        }

       

      
    }
}
