using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    public partial class SelectNPLForm : BaseForm
    {
        public NguyenPhuLieuCollection NPLCollectionSelect = new NguyenPhuLieuCollection();
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
        NguyenPhuLieuCollection NPLTMPCollection = new NguyenPhuLieuCollection();
        NguyenPhuLieu NPL = new NguyenPhuLieu();
        public SelectNPLForm()
        {
            InitializeComponent();
        }
        private void RemoveNPL(NguyenPhuLieu NPL, NguyenPhuLieuCollection NPLCollection)
        {
            foreach (NguyenPhuLieu NPLDelete in NPLCollection)
            {
                if (NPLDelete.Ma == NPL.Ma)
                {
                    NPLCollection.Remove(NPLDelete);
                    break;
                }
            }
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            NPLTMPCollection.Clear();
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    NguyenPhuLieu NPL = (NguyenPhuLieu)row.DataRow;
                    NguyenPhuLieu NPLSelect = new NguyenPhuLieu();
                    NPLSelect.Ma = NPL.Ma;
                    NPLSelect.Ten = NPL.Ten;
                    NPLSelect.DVT_ID = NPL.DVT_ID;
                    NPLSelect.MaHS = NPL.MaHS;
                    NPLCollectionSelect.Add(NPLSelect);
                    NPLTMPCollection.Add(NPLSelect);
                }
            }
            foreach (NguyenPhuLieu NPL in NPLTMPCollection)
            {
                RemoveNPL(NPL, NPLCollection);
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }

            dgListSelect.Refetch();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            NPLTMPCollection.Clear();
            foreach (GridEXRow row in dgListSelect.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    NguyenPhuLieu NPL = (NguyenPhuLieu)row.DataRow;
                    NguyenPhuLieu NPLSelect = new NguyenPhuLieu();
                    NPLSelect.Ma = NPL.Ma;
                    NPLSelect.Ten = NPL.Ten;
                    NPLSelect.DVT_ID = NPL.DVT_ID;
                    NPLSelect.MaHS = NPL.MaHS;
                    NPLCollection.Add(NPLSelect);
                    NPLTMPCollection.Add(NPLSelect);
                }
            }
            foreach (NguyenPhuLieu NPL in NPLTMPCollection)
            {
                RemoveNPL(NPL, NPLCollectionSelect);
            }
            try
            {
                dgListSelect.Refetch();
            }
            catch
            {
                dgListSelect.Refresh();
            }

            dgList.Refetch();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void dgListSelect_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
             this.Close();
        }

        private void SelectNPLForm_Load(object sender, EventArgs e)
        {
            this.NPLCollection = NPL.SelectCollectionDynamic(" MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "'", "");
            dgList.DataSource = this.NPLCollection;
            dgList.Refetch();
            dgListSelect.DataSource = NPLCollectionSelect;
            dgListSelect.Refetch();
        }

     
    }
}
