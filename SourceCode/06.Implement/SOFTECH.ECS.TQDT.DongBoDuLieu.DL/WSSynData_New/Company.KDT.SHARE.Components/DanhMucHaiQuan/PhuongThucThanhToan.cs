﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
    public partial class PhuongThucThanhToan
    {
        public static List<PhuongThucThanhToan> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<PhuongThucThanhToan> collection = new List<PhuongThucThanhToan>();

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                PhuongThucThanhToan entity = new PhuongThucThanhToan();
                entity.ID = dataSet.Tables[0].Rows[i]["ID"].ToString();
                entity.GhiChu = dataSet.Tables[0].Rows[i]["GhiChu"].ToString();
                entity.DateCreated = dataSet.Tables[0].Rows[i]["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateCreated"]) : DateTime.Today;
                entity.DateModified = dataSet.Tables[0].Rows[i]["DateModified"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateModified"]) : DateTime.Today;
                collection.Add(entity);
            }

            return collection;
        }
    }
}
