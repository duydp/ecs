using System;
using System.Data;
using System.Data.SqlClient;
using Company.KD.BLL;
using System.Collections;

namespace Company.QuanTri
{
	public partial class User : EntityBase
	{
        public ArrayList RoleList = new ArrayList();
        private bool _Check = false;
        public bool Check
        {
            set { _Check = value; }
            get { return _Check; }
        }
        public bool Load(string user_name)
        {
            string spName = "select * from [User] where [USER_NAME]=@USER_NAME";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, user_name);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("USER_NAME"))) this._USER_NAME = reader.GetString(reader.GetOrdinal("USER_NAME"));
                if (!reader.IsDBNull(reader.GetOrdinal("PASSWORD"))) this._PASSWORD = reader.GetString(reader.GetOrdinal("PASSWORD"));
                if (!reader.IsDBNull(reader.GetOrdinal("HO_TEN"))) this._HO_TEN = reader.GetString(reader.GetOrdinal("HO_TEN"));
                if (!reader.IsDBNull(reader.GetOrdinal("MO_TA"))) this._MO_TA = reader.GetString(reader.GetOrdinal("MO_TA"));
                if (!reader.IsDBNull(reader.GetOrdinal("isAdmin"))) this._isAdmin = reader.GetBoolean(reader.GetOrdinal("isAdmin"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool TestPassWord(long ID,string password)
        {
            string spName = "select * from [User] where ID=@ID and [PASSWORD]=@PASSWORD";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, password);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public ArrayList GetEffectivePermissionList()
        {
            string sql = "Select  ID from ROLE ";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            
            ArrayList permissions = new ArrayList();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                permissions.Add(reader.GetInt64(0));
            }
            reader.Close();
            return permissions;
        }
        public ArrayList GetUserRoles()
        {
            string sql = "select MA_NHOM from GROUPS";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            
            ArrayList permissions = new ArrayList();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                permissions.Add(reader.GetInt64(0));
            }
            reader.Close();
            return permissions;
        }
        public ArrayList GetEffectivePermissionList(long UserID)
        {
            string sql = "Select distinct ID_ROLE from GROUP_ROLE where GROUP_ID in(select MA_NHOM from USER_GROUP where USER_ID=@USER_ID)";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, UserID);
            ArrayList permissions = new ArrayList();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                permissions.Add(reader.GetInt64(0));
            }
            reader.Close();
            return permissions;
        }
        public ArrayList GetUserRoles(long UserID)
        {
            string sql = "select distinct MA_NHOM from USER_GROUP where USER_ID=@USER_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, UserID);
            ArrayList permissions = new ArrayList();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                permissions.Add(reader.GetInt64(0));
            }
            reader.Close();
            return permissions;
        }
        public void LoadRoleList()
        {
            RoleList = GetUserRoles(this.ID);
        }
        public long ValidateLogin(string UserName, string password)
        {
            string spName = "select ID from [User] where [USER_NAME]=@USER_NAME and [PASSWORD]=@PASSWORD";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, UserName);
            this.db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, password);

            object o = this.db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);
        }

        public bool CheckUserName(string UserName)
        {
            string spName = "select ID from [User] where [USER_NAME]=@USER_NAME and ID<>@ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, UserName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this.ID);            

            object o = this.db.ExecuteScalar(dbCommand);
            return (o != null);
        }
        public void InsertUpdateFull()
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID > 0)
                        this.UpdateTransaction(transaction);
                    else
                        this.InsertTransaction(transaction);
                    DeleteRoleTransaction(transaction);
                    for (int i = 0; i < RoleList.Count; ++i)
                    {
                        USER_GROUP ug = new USER_GROUP();
                        ug.USER_ID = this.ID;
                        ug.MA_NHOM=Convert.ToInt64(RoleList[i]);
                        ug.InsertTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch(Exception ex)
                {                    
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }            
        }

        public int DeleteRoleTransaction(SqlTransaction transaction)
        {
            string spName = "delete from USER_GROUP where USER_ID=@USER_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
       
	}	
}