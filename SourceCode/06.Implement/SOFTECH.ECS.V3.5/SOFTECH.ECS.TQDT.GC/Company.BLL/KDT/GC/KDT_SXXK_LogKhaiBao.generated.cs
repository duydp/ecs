﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_SXXK_LogKhaiBao : ICloneable
	{
		#region Properties.
		
		public long IDLog { set; get; }
		public string LoaiKhaiBao { set; get; }
		public long ID_DK { set; get; }
		public string GUIDSTR_DK { set; get; }
		public string UserNameKhaiBao { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		public string UserNameSuaDoi { set; get; }
		public DateTime NgaySuaDoi { set; get; }
		public string GhiChu { set; get; }
		public bool IsDelete { set; get; }
        public List<KDT_GC_DinhMuc_Log> DMCollection = new List<KDT_GC_DinhMuc_Log>();
        public List<KDT_GC_LoaiPhuKien_Log> LoaiPKCollection = new List<KDT_GC_LoaiPhuKien_Log>();
        public List<KDT_GC_NhomSanPham_Log> NhomSanPhamCollection = new List<KDT_GC_NhomSanPham_Log>();
        public List<KDT_GC_SanPham_Log> SanPhamCollection = new List<KDT_GC_SanPham_Log>();
        public List<KDT_GC_NguyenPhuLieu_Log> NPLCollection = new List<KDT_GC_NguyenPhuLieu_Log>();
        public List<KDT_GC_ThietBi_Log> TBCollection = new List<KDT_GC_ThietBi_Log>();
        public List<KDT_GC_HangMau_Log> HMCollection = new List<KDT_GC_HangMau_Log>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_SXXK_LogKhaiBao> ConvertToCollection(IDataReader reader)
		{
			List<KDT_SXXK_LogKhaiBao> collection = new List<KDT_SXXK_LogKhaiBao>();
			while (reader.Read())
			{
				KDT_SXXK_LogKhaiBao entity = new KDT_SXXK_LogKhaiBao();
				if (!reader.IsDBNull(reader.GetOrdinal("IDLog"))) entity.IDLog = reader.GetInt64(reader.GetOrdinal("IDLog"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKhaiBao"))) entity.LoaiKhaiBao = reader.GetString(reader.GetOrdinal("LoaiKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_DK"))) entity.ID_DK = reader.GetInt64(reader.GetOrdinal("ID_DK"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR_DK"))) entity.GUIDSTR_DK = reader.GetString(reader.GetOrdinal("GUIDSTR_DK"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameKhaiBao"))) entity.UserNameKhaiBao = reader.GetString(reader.GetOrdinal("UserNameKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameSuaDoi"))) entity.UserNameSuaDoi = reader.GetString(reader.GetOrdinal("UserNameSuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgaySuaDoi"))) entity.NgaySuaDoi = reader.GetDateTime(reader.GetOrdinal("NgaySuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsDelete"))) entity.IsDelete = reader.GetBoolean(reader.GetOrdinal("IsDelete"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_SXXK_LogKhaiBao> collection, long iDLog)
        {
            foreach (KDT_SXXK_LogKhaiBao item in collection)
            {
                if (item.IDLog == iDLog)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_SXXK_LogKhaiBao VALUES(@LoaiKhaiBao, @ID_DK, @GUIDSTR_DK, @UserNameKhaiBao, @NgayKhaiBao, @UserNameSuaDoi, @NgaySuaDoi, @GhiChu, @IsDelete)";
            string update = "UPDATE t_KDT_SXXK_LogKhaiBao SET LoaiKhaiBao = @LoaiKhaiBao, ID_DK = @ID_DK, GUIDSTR_DK = @GUIDSTR_DK, UserNameKhaiBao = @UserNameKhaiBao, NgayKhaiBao = @NgayKhaiBao, UserNameSuaDoi = @UserNameSuaDoi, NgaySuaDoi = @NgaySuaDoi, GhiChu = @GhiChu, IsDelete = @IsDelete WHERE IDLog = @IDLog";
            string delete = "DELETE FROM t_KDT_SXXK_LogKhaiBao WHERE IDLog = @IDLog";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@IDLog", SqlDbType.BigInt, "IDLog", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKhaiBao", SqlDbType.VarChar, "LoaiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_DK", SqlDbType.BigInt, "ID_DK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR_DK", SqlDbType.NVarChar, "GUIDSTR_DK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserNameKhaiBao", SqlDbType.NVarChar, "UserNameKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserNameSuaDoi", SqlDbType.NVarChar, "UserNameSuaDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgaySuaDoi", SqlDbType.DateTime, "NgaySuaDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsDelete", SqlDbType.Bit, "IsDelete", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@IDLog", SqlDbType.BigInt, "IDLog", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKhaiBao", SqlDbType.VarChar, "LoaiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_DK", SqlDbType.BigInt, "ID_DK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR_DK", SqlDbType.NVarChar, "GUIDSTR_DK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserNameKhaiBao", SqlDbType.NVarChar, "UserNameKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserNameSuaDoi", SqlDbType.NVarChar, "UserNameSuaDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgaySuaDoi", SqlDbType.DateTime, "NgaySuaDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsDelete", SqlDbType.Bit, "IsDelete", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@IDLog", SqlDbType.BigInt, "IDLog", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_SXXK_LogKhaiBao VALUES(@LoaiKhaiBao, @ID_DK, @GUIDSTR_DK, @UserNameKhaiBao, @NgayKhaiBao, @UserNameSuaDoi, @NgaySuaDoi, @GhiChu, @IsDelete)";
            string update = "UPDATE t_KDT_SXXK_LogKhaiBao SET LoaiKhaiBao = @LoaiKhaiBao, ID_DK = @ID_DK, GUIDSTR_DK = @GUIDSTR_DK, UserNameKhaiBao = @UserNameKhaiBao, NgayKhaiBao = @NgayKhaiBao, UserNameSuaDoi = @UserNameSuaDoi, NgaySuaDoi = @NgaySuaDoi, GhiChu = @GhiChu, IsDelete = @IsDelete WHERE IDLog = @IDLog";
            string delete = "DELETE FROM t_KDT_SXXK_LogKhaiBao WHERE IDLog = @IDLog";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@IDLog", SqlDbType.BigInt, "IDLog", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKhaiBao", SqlDbType.VarChar, "LoaiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_DK", SqlDbType.BigInt, "ID_DK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR_DK", SqlDbType.NVarChar, "GUIDSTR_DK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserNameKhaiBao", SqlDbType.NVarChar, "UserNameKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserNameSuaDoi", SqlDbType.NVarChar, "UserNameSuaDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgaySuaDoi", SqlDbType.DateTime, "NgaySuaDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsDelete", SqlDbType.Bit, "IsDelete", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@IDLog", SqlDbType.BigInt, "IDLog", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKhaiBao", SqlDbType.VarChar, "LoaiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_DK", SqlDbType.BigInt, "ID_DK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR_DK", SqlDbType.NVarChar, "GUIDSTR_DK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserNameKhaiBao", SqlDbType.NVarChar, "UserNameKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserNameSuaDoi", SqlDbType.NVarChar, "UserNameSuaDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgaySuaDoi", SqlDbType.DateTime, "NgaySuaDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsDelete", SqlDbType.Bit, "IsDelete", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@IDLog", SqlDbType.BigInt, "IDLog", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_SXXK_LogKhaiBao Load(long iDLog)
		{
			const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDLog", SqlDbType.BigInt, iDLog);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_SXXK_LogKhaiBao> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_SXXK_LogKhaiBao> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_SXXK_LogKhaiBao> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_SXXK_LogKhaiBao(string loaiKhaiBao, long iD_DK, string gUIDSTR_DK, string userNameKhaiBao, DateTime ngayKhaiBao, string userNameSuaDoi, DateTime ngaySuaDoi, string ghiChu, bool isDelete)
		{
			KDT_SXXK_LogKhaiBao entity = new KDT_SXXK_LogKhaiBao();	
			entity.LoaiKhaiBao = loaiKhaiBao;
			entity.ID_DK = iD_DK;
			entity.GUIDSTR_DK = gUIDSTR_DK;
			entity.UserNameKhaiBao = userNameKhaiBao;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.UserNameSuaDoi = userNameSuaDoi;
			entity.NgaySuaDoi = ngaySuaDoi;
			entity.GhiChu = ghiChu;
			entity.IsDelete = isDelete;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@IDLog", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LoaiKhaiBao", SqlDbType.VarChar, LoaiKhaiBao);
			db.AddInParameter(dbCommand, "@ID_DK", SqlDbType.BigInt, ID_DK);
			db.AddInParameter(dbCommand, "@GUIDSTR_DK", SqlDbType.NVarChar, GUIDSTR_DK);
			db.AddInParameter(dbCommand, "@UserNameKhaiBao", SqlDbType.NVarChar, UserNameKhaiBao);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@UserNameSuaDoi", SqlDbType.NVarChar, UserNameSuaDoi);
			db.AddInParameter(dbCommand, "@NgaySuaDoi", SqlDbType.DateTime, NgaySuaDoi.Year <= 1753 ? DBNull.Value : (object) NgaySuaDoi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsDelete", SqlDbType.Bit, IsDelete);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				IDLog = (long) db.GetParameterValue(dbCommand, "@IDLog");
				return IDLog;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				IDLog = (long) db.GetParameterValue(dbCommand, "@IDLog");
				return IDLog;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_SXXK_LogKhaiBao> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_LogKhaiBao item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_SXXK_LogKhaiBao(long iDLog, string loaiKhaiBao, long iD_DK, string gUIDSTR_DK, string userNameKhaiBao, DateTime ngayKhaiBao, string userNameSuaDoi, DateTime ngaySuaDoi, string ghiChu, bool isDelete)
		{
			KDT_SXXK_LogKhaiBao entity = new KDT_SXXK_LogKhaiBao();			
			entity.IDLog = iDLog;
			entity.LoaiKhaiBao = loaiKhaiBao;
			entity.ID_DK = iD_DK;
			entity.GUIDSTR_DK = gUIDSTR_DK;
			entity.UserNameKhaiBao = userNameKhaiBao;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.UserNameSuaDoi = userNameSuaDoi;
			entity.NgaySuaDoi = ngaySuaDoi;
			entity.GhiChu = ghiChu;
			entity.IsDelete = isDelete;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_LogKhaiBao_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@IDLog", SqlDbType.BigInt, IDLog);
			db.AddInParameter(dbCommand, "@LoaiKhaiBao", SqlDbType.VarChar, LoaiKhaiBao);
			db.AddInParameter(dbCommand, "@ID_DK", SqlDbType.BigInt, ID_DK);
			db.AddInParameter(dbCommand, "@GUIDSTR_DK", SqlDbType.NVarChar, GUIDSTR_DK);
			db.AddInParameter(dbCommand, "@UserNameKhaiBao", SqlDbType.NVarChar, UserNameKhaiBao);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@UserNameSuaDoi", SqlDbType.NVarChar, UserNameSuaDoi);
			db.AddInParameter(dbCommand, "@NgaySuaDoi", SqlDbType.DateTime, NgaySuaDoi.Year <= 1753 ? DBNull.Value : (object) NgaySuaDoi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsDelete", SqlDbType.Bit, IsDelete);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_SXXK_LogKhaiBao> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_LogKhaiBao item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_SXXK_LogKhaiBao(long iDLog, string loaiKhaiBao, long iD_DK, string gUIDSTR_DK, string userNameKhaiBao, DateTime ngayKhaiBao, string userNameSuaDoi, DateTime ngaySuaDoi, string ghiChu, bool isDelete)
		{
			KDT_SXXK_LogKhaiBao entity = new KDT_SXXK_LogKhaiBao();			
			entity.IDLog = iDLog;
			entity.LoaiKhaiBao = loaiKhaiBao;
			entity.ID_DK = iD_DK;
			entity.GUIDSTR_DK = gUIDSTR_DK;
			entity.UserNameKhaiBao = userNameKhaiBao;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.UserNameSuaDoi = userNameSuaDoi;
			entity.NgaySuaDoi = ngaySuaDoi;
			entity.GhiChu = ghiChu;
			entity.IsDelete = isDelete;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@IDLog", SqlDbType.BigInt, IDLog);
			db.AddInParameter(dbCommand, "@LoaiKhaiBao", SqlDbType.VarChar, LoaiKhaiBao);
			db.AddInParameter(dbCommand, "@ID_DK", SqlDbType.BigInt, ID_DK);
			db.AddInParameter(dbCommand, "@GUIDSTR_DK", SqlDbType.NVarChar, GUIDSTR_DK);
			db.AddInParameter(dbCommand, "@UserNameKhaiBao", SqlDbType.NVarChar, UserNameKhaiBao);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@UserNameSuaDoi", SqlDbType.NVarChar, UserNameSuaDoi);
			db.AddInParameter(dbCommand, "@NgaySuaDoi", SqlDbType.DateTime, NgaySuaDoi.Year <= 1753 ? DBNull.Value : (object) NgaySuaDoi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsDelete", SqlDbType.Bit, IsDelete);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_SXXK_LogKhaiBao> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_LogKhaiBao item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_SXXK_LogKhaiBao(long iDLog)
		{
			KDT_SXXK_LogKhaiBao entity = new KDT_SXXK_LogKhaiBao();
			entity.IDLog = iDLog;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDLog", SqlDbType.BigInt, IDLog);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_SXXK_LogKhaiBao> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_LogKhaiBao item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.IDLog;
                try
                {
                    if (this.IDLog == 0)
                        Insert(transaction);
                    else
                        Update(transaction);
                    foreach (KDT_GC_DinhMuc_Log dm in this.DMCollection)
                    {
                        dm.Master_ID = this.IDLog;
                        if (dm.ID > 0)
                            dm.Update(transaction);
                        else
                            dm.Insert(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.IDLog = id;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateFullPhuKien()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.IDLog;
                try
                {
                    if (this.IDLog == 0)
                        Insert(transaction);
                    else
                        Update(transaction);

                    foreach (KDT_GC_LoaiPhuKien_Log pk in LoaiPKCollection)
                    {
                        int stt = 1;
                        pk.Master_ID = this.IDLog;
                        if (pk.ID > 0)
                            pk.Update(transaction);
                        else
                            pk.Insert(transaction);
                        foreach (KDT_GC_HangPhuKien_Log hang in pk.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = pk.ID;
                            if (hang.ID == 0)
                                hang.Insert(transaction);
                            else
                                hang.Update(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.IDLog = id;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateHDGC()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.IDLog;
                try
                {
                    if (this.IDLog == 0)
                        Insert(transaction);
                    else
                        Update(transaction);

                    foreach (KDT_GC_NhomSanPham_Log entity in NhomSanPhamCollection)
                    {
                        entity.Master_ID = this.IDLog;
                        if (entity.ID > 0)
                            entity.Update(transaction);
                        else
                            entity.Insert(transaction);
                    }
                    foreach (KDT_GC_NguyenPhuLieu_Log entity in NPLCollection)
                    {
                        entity.Master_ID = this.IDLog;
                        if (entity.ID > 0)
                            entity.Update(transaction);
                        else
                            entity.Insert(transaction);
                    }
                    foreach (KDT_GC_SanPham_Log entity in SanPhamCollection)
                    {
                        entity.Master_ID = this.IDLog;
                        if (entity.ID > 0)
                            entity.Update(transaction);
                        else
                            entity.Insert(transaction);
                    }
                    foreach (KDT_GC_ThietBi_Log entity in TBCollection)
                    {
                        entity.Master_ID = this.IDLog;
                        if (entity.ID > 0)
                            entity.Update(transaction);
                        else
                            entity.Insert(transaction);
                    }
                    foreach (KDT_GC_HangMau_Log entity in HMCollection)
                    {
                        entity.Master_ID = this.IDLog;
                        if (entity.ID > 0)
                            entity.Update(transaction);
                        else
                            entity.Insert(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.IDLog = id;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
	}	
}