﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAE8020 : BasicVNACC
    {

        public List<VAE8020_HANG> HangHoa { get; set; }

        //public StringBuilder BuilEdiMessagesIVA(StringBuilder StrBuild)
        //{
        //    StringBuilder str = StrBuild;
        //    str = BuildEdiMessages<VAS5050>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5050");
        //    foreach (VAS5050_HANG item in HangHoa)
        //    {
        //        item.BuildEdiMessages<VAS5050_HANG>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5050_HANG");

        //    }
        //    return str;
        //}
        public void LoadVAE8020(string strResult)
        {
            try
            {
                this.GetObject<VAE8020>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAE8020, false, VAE8020.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAE8020.TongSoByte);
                while (true)
                {
                    VAE8020_HANG ivaHang = new VAE8020_HANG();
                    ivaHang.GetObject<VAE8020_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAE8020_HANG", false, VAE8020_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAE8020_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAE8020_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAE8020_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAE8020_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
