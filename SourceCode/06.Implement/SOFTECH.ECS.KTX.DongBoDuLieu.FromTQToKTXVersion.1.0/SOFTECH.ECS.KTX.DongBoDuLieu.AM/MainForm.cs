using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ToolCreateAccount
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void mnCauHinhDatabase_Click(object sender, EventArgs e)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ConfigurationDatabase"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ConfigurationDatabase f = new ConfigurationDatabase();
            f.MdiParent = this;            
            f.Show();
        }

        private void mnCauHinhDN_Click(object sender, EventArgs e)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CreateAccount"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            CreateAccount f = new CreateAccount();
            f.MdiParent = this;
            f.Show();
        }
    }
}