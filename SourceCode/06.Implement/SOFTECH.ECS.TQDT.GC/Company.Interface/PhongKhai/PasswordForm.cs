using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Security.Cryptography;

namespace Company.Interface.PhongKhai
{
    public partial class PasswordForm : BaseForm
    {
        public bool IsPass = false;
        public PasswordForm()
        {
            InitializeComponent();
        }

        private void PasswordForm_Load(object sender, EventArgs e)
        {

        }
        private string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (GetMD5Value(txtMatKhau.Text) == ConfigurationManager.AppSettings["PIN"])
            {
                this.IsPass = true;
                this.Close();
            }
            else
            {
                this.IsPass = false;
                lblThongBao.Text = "Nhập sai mật khẩu bảo vệ.";

            }
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.IsPass = false;
            this.Close();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }
    }
}