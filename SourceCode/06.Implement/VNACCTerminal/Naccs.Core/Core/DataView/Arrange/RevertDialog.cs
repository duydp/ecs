﻿namespace Naccs.Core.DataView.Arrange
{
    using Naccs.Core.Classes;
    using Naccs.Core.Properties;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Windows.Forms;

    public class RevertDialog : Form
    {
        private ToolStripMenuItem AllCheckOff_MouseClick;
        private ToolStripMenuItem AllCheckOn_MouseClick;
        private Button btnClose;
        private Button btnExract;
        private Button btnFind;
        private Button btnRevert;
        private DataGridViewTextBoxColumn clAirSea;
        private DataGridViewTextBoxColumn clFolder;
        private DataGridViewTextBoxColumn clInputNo;
        private DataGridViewTextBoxColumn clJobCode;
        private DataGridViewTextBoxColumn clJobInfo;
        private DataGridViewTextBoxColumn clmFileName;
        private DataGridViewTextBoxColumn clOutCode;
        private DataGridViewCheckBoxColumn clSelect;
        private DataGridViewTextBoxColumn clTimeStamp;
        private ComboBox cmbFindKind;
        private ComboBox cmbMonth;
        private ComboBox cmbYear;
        private const string cnFindKindColumn = "FIND_KIND_CLM";
        private const string cnFindKindName = "FIND_KIND_NAME";
        private const string cnMonth = "MONTH";
        private const int cnSelect_ColumnIndex = 9;
        private const string cnStorageFolder = "PastDataView";
        private const string cnYear = "YEAR";
        private IContainer components;
        private ContextMenuStrip contextMenuStrip1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridView dgvSelectList;
        private string dlMonth = "";
        private string dlYear = "";
        private int err;
        private DataTable findKind_table;
        private GroupBox grpDate;
        private GroupBox grpFind;
        private Label label1;
        private Label lblFindKind;
        private Label lblFindString;
        private Label lblMonth;
        private Label lblYear;
        private ToolStripMenuItem mnAllCheckOff;
        private ToolStripMenuItem mnAllCheckOn;
        private ToolStripMenuItem mnEdit;
        private ToolStripSeparator mnEditSep1;
        private ToolStripMenuItem mnExit;
        private ToolStripMenuItem mnFile;
        private ToolStripSeparator mnFileSep1;
        private ToolStripMenuItem mnOpen;
        private MenuStrip mnRevertDialog;
        private ToolStripMenuItem mnSelectCheckOff;
        private ToolStripMenuItem mnSelectCheckOn;
        private string pastDataView_YYYYMM = "";
        private string pastDataViewPath = "";
        private string path = "";
        private Panel pnlBottom;
        private Panel pnlTop;
        private Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs> _RevertEvent;
        private ToolStripMenuItem SelectCheckOff_MouseClick;
        private ToolStripMenuItem SelectCheckOn_MouseClick;
        private TextBox txbFindString;
        private DataTable year_month_table;

        public event Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs> RevertEvent
        {
            add
            {
                Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs> handler2;
                Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs> revertEvent = this._RevertEvent;
                do
                {
                    handler2 = revertEvent;
                    Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs> handler3 = (Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs>) Delegate.Combine(handler2, value);
                    revertEvent = Interlocked.CompareExchange<Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs>>(ref this._RevertEvent, handler3, handler2);
                }
                while (revertEvent != handler2);
            }
            remove
            {
                Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs> handler2;
                Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs> revertEvent = this._RevertEvent;
                do
                {
                    handler2 = revertEvent;
                    Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs> handler3 = (Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs>) Delegate.Remove(handler2, value);
                    revertEvent = Interlocked.CompareExchange<Naccs.Core.DataView.Arrange.EventHandler<RevertEventArgs>>(ref this._RevertEvent, handler3, handler2);
                }
                while (revertEvent != handler2);
            }
        }

        public RevertDialog()
        {
            this.InitializeComponent();
            this.pastDataViewPath = ArrangeDataTable.PastDataViewPath;
            this.cmbYear.Items.Clear();
            this.cmbMonth.Items.Clear();
            this.year_month_table = new DataTable();
            this.year_month_table.Columns.Add("YEAR");
            this.year_month_table.Columns.Add("MONTH");
            this.makeYearCombo();
            this.findKind_table = new DataTable();
            this.findKind_table.Columns.Add("FIND_KIND_NAME");
            this.findKind_table.Columns.Add("FIND_KIND_CLM");
            this.makeFindKindCombo();
        }

        private void AllCheckOff_MouseClick_Click(object sender, EventArgs e)
        {
            this.mnAllCheckOff_Click(sender, e);
        }

        private void AllCheckOn_MouseClick_Click(object sender, EventArgs e)
        {
            this.mnAllCheckOn_Click(sender, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnExract_Click(object sender, EventArgs e)
        {
            this.cmbCheck();
            if (this.err != 1)
            {
                this.pastDataView_YYYYMM = this.cmbYear.Text + this.cmbMonth.Text;
                this.dlYear = this.cmbYear.Text;
                this.dlMonth = this.cmbMonth.Text;
                this.path = Path.GetDirectoryName(ArrangeDataTable.IndexFilePath(this.pastDataView_YYYYMM));
                if (Directory.Exists(this.path))
                {
                    try
                    {
                        this.btnExract.Enabled = false;
                        Cursor.Current = Cursors.WaitCursor;
                        try
                        {
                            ArrangeDataTable.Load((DataTable) this.dgvSelectList.DataSource, this.pastDataView_YYYYMM);
                            this.dgvSelectList.Sort(this.dgvSelectList.Columns[7], ListSortDirection.Descending);
                        }
                        catch (Exception exception)
                        {
                            MessageDialog dialog = new MessageDialog();
                            dialog.ShowMessage("E301", this.path, null, exception);
                            dialog.Dispose();
                        }
                        return;
                    }
                    finally
                    {
                        this.btnExract.Enabled = true;
                        Cursor.Current = Cursors.Default;
                        if (this.dgvSelectList.RowCount > 0)
                        {
                            this.dgvSelectList.CurrentCell = this.dgvSelectList[1, 0];
                            this.mnAllCheckOff_Click(sender, e);
                        }
                        else
                        {
                            base.ActiveControl = this.cmbYear;
                        }
                    }
                }
                this.RevertDialog_Load(sender, e);
                base.ActiveControl = this.cmbYear;
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.cmbFindKind.Text))
            {
                SerchFromGridView.serch(this.dgvSelectList, this.cmbFindKind.SelectedValue.ToString(), this.txbFindString.Text);
            }
        }

        private void btnRevert_Click(object sender, EventArgs e)
        {
            List<string> list = new List<string>();
            foreach (DataGridViewRow row in (IEnumerable) this.dgvSelectList.Rows)
            {
                if (!string.IsNullOrEmpty(row.Cells["clSelect"].Value.ToString()) && ((bool) row.Cells["clSelect"].Value))
                {
                    list.Add(row.Cells["clFileName"].Value.ToString());
                }
            }
            if (list.Count > 0)
            {
                this.path = Path.GetDirectoryName(ArrangeDataTable.IndexFilePath(this.pastDataView_YYYYMM));
                this.DoRevertEvent(this.path, list.ToArray());
            }
        }

        private int cmbCheck()
        {
            int length = this.cmbYear.Text.Length;
            int num2 = this.cmbMonth.Text.Length;
            if ((length != 4) || !Regex.IsMatch(this.cmbYear.Text, "^[0-9]+$"))
            {
                string message = Resources.ResourceManager.GetString("CORE21");
                using (MessageDialogSimpleForm form = new MessageDialogSimpleForm())
                {
                    form.ShowMessage(ButtonPatern.OK_ONLY, MessageKind.Error, message);
                }
                base.ActiveControl = this.cmbYear;
                this.err = 1;
                return this.err;
            }
            if ((num2 != 2) || !Regex.IsMatch(this.cmbMonth.Text, "^[0-9]+$"))
            {
                string str4 = Resources.ResourceManager.GetString("CORE22");
                using (MessageDialogSimpleForm form2 = new MessageDialogSimpleForm())
                {
                    form2.ShowMessage(ButtonPatern.OK_ONLY, MessageKind.Error, str4);
                }
                base.ActiveControl = this.cmbMonth;
                this.err = 1;
            }
            return this.err;
        }

        private void cmbYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.makeMonthCombo();
        }

        private void cmbYear_TextUpdate(object sender, EventArgs e)
        {
            this.makeMonthCombo();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void DoRevertEvent(string pathroot, string[] filenames)
        {
            if (this._RevertEvent != null)
            {
                RevertEventArgs e = new RevertEventArgs();
                e.PathRoot = pathroot;
                e.FileNames = filenames;
                if (this._RevertEvent(this, e))
                {
                    this.dgvSelectList.CurrentCell = null;
                    for (int i = this.dgvSelectList.RowCount - 1; i > -1; i--)
                    {
                        DataGridViewRow row = this.dgvSelectList.Rows[i];
                        if (Convert.ToBoolean(row.Cells["clSelect"].Value))
                        {
                            this.dgvSelectList.Rows.RemoveAt(i);
                        }
                    }
                    this.dgvSelectList.Refresh();
                }
            }
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            DataGridViewCellStyle style = new DataGridViewCellStyle();
            this.btnFind = new Button();
            this.lblFindString = new Label();
            this.txbFindString = new TextBox();
            this.lblFindKind = new Label();
            this.cmbFindKind = new ComboBox();
            this.mnRevertDialog = new MenuStrip();
            this.mnFile = new ToolStripMenuItem();
            this.mnOpen = new ToolStripMenuItem();
            this.mnFileSep1 = new ToolStripSeparator();
            this.mnExit = new ToolStripMenuItem();
            this.mnEdit = new ToolStripMenuItem();
            this.mnAllCheckOn = new ToolStripMenuItem();
            this.mnAllCheckOff = new ToolStripMenuItem();
            this.mnEditSep1 = new ToolStripSeparator();
            this.mnSelectCheckOn = new ToolStripMenuItem();
            this.mnSelectCheckOff = new ToolStripMenuItem();
            this.grpFind = new GroupBox();
            this.dgvSelectList = new DataGridView();
            this.clSelect = new DataGridViewCheckBoxColumn();
            this.clFolder = new DataGridViewTextBoxColumn();
            this.clAirSea = new DataGridViewTextBoxColumn();
            this.clJobCode = new DataGridViewTextBoxColumn();
            this.clOutCode = new DataGridViewTextBoxColumn();
            this.clInputNo = new DataGridViewTextBoxColumn();
            this.clJobInfo = new DataGridViewTextBoxColumn();
            this.clTimeStamp = new DataGridViewTextBoxColumn();
            this.clmFileName = new DataGridViewTextBoxColumn();
            this.btnRevert = new Button();
            this.btnClose = new Button();
            this.lblYear = new Label();
            this.cmbYear = new ComboBox();
            this.cmbMonth = new ComboBox();
            this.lblMonth = new Label();
            this.grpDate = new GroupBox();
            this.btnExract = new Button();
            this.pnlTop = new Panel();
            this.pnlBottom = new Panel();
            this.label1 = new Label();
            this.contextMenuStrip1 = new ContextMenuStrip(this.components);
            this.AllCheckOn_MouseClick = new ToolStripMenuItem();
            this.AllCheckOff_MouseClick = new ToolStripMenuItem();
            this.SelectCheckOn_MouseClick = new ToolStripMenuItem();
            this.SelectCheckOff_MouseClick = new ToolStripMenuItem();
            this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new DataGridViewTextBoxColumn();
            this.mnRevertDialog.SuspendLayout();
            this.grpFind.SuspendLayout();
            ((ISupportInitialize) this.dgvSelectList).BeginInit();
            this.grpDate.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            base.SuspendLayout();
            this.btnFind.Location = new Point(0x1e6, 14);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new Size(70, 0x17);
            this.btnFind.TabIndex = 4;
            this.btnFind.Text = "T\x00ecm kiếm";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new EventHandler(this.btnFind_Click);
            this.lblFindString.AutoSize = true;
            this.lblFindString.Location = new Point(6, 15);
            this.lblFindString.Name = "lblFindString";
            this.lblFindString.Size = new Size(0x52, 0x18);
            this.lblFindString.TabIndex = 0;
            this.lblFindString.Text = "T\x00ecm kiếm theo \r\nchuỗi";
            this.txbFindString.Location = new Point(0x56, 0x11);
            this.txbFindString.Name = "txbFindString";
            this.txbFindString.Size = new Size(100, 0x13);
            this.txbFindString.TabIndex = 1;
            this.lblFindKind.AutoSize = true;
            this.lblFindKind.Location = new Point(0xc0, 20);
            this.lblFindKind.Name = "lblFindKind";
            this.lblFindKind.Size = new Size(0x63, 12);
            this.lblFindKind.TabIndex = 2;
            this.lblFindKind.Text = "Ph\x00e2n loại t\x00ecm kiếm";
            this.cmbFindKind.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cmbFindKind.FormattingEnabled = true;
            this.cmbFindKind.Location = new Point(0x12a, 0x10);
            this.cmbFindKind.Name = "cmbFindKind";
            this.cmbFindKind.Size = new Size(0xb6, 20);
            this.cmbFindKind.TabIndex = 3;
            this.mnRevertDialog.Items.AddRange(new ToolStripItem[] { this.mnFile, this.mnEdit });
            this.mnRevertDialog.Location = new Point(0, 0);
            this.mnRevertDialog.Name = "mnRevertDialog";
            this.mnRevertDialog.Size = new Size(0x353, 0x1a);
            this.mnRevertDialog.TabIndex = 0;
            this.mnRevertDialog.Text = "menuStrip1";
            this.mnRevertDialog.ItemClicked += new ToolStripItemClickedEventHandler(this.mnRevertDialog_ItemClicked);
            this.mnFile.DropDownItems.AddRange(new ToolStripItem[] { this.mnOpen, this.mnFileSep1, this.mnExit });
            this.mnFile.Name = "mnFile";
            this.mnFile.Size = new Size(0x52, 0x16);
            this.mnFile.Text = "Tệp tin (&F)";
            this.mnOpen.Name = "mnOpen";
            this.mnOpen.Size = new Size(0xbd, 0x16);
            this.mnOpen.Text = "X\x00f3a tin nhắn cũ (&D)";
            this.mnOpen.Click += new EventHandler(this.mnDelete_Click);
            this.mnFileSep1.Name = "mnFileSep1";
            this.mnFileSep1.Size = new Size(0xba, 6);
            this.mnExit.Name = "mnExit";
            this.mnExit.Size = new Size(0xbd, 0x16);
            this.mnExit.Text = "Đ\x00f3ng (&X)";
            this.mnExit.Click += new EventHandler(this.mnExit_Click);
            this.mnEdit.DropDownItems.AddRange(new ToolStripItem[] { this.mnAllCheckOn, this.mnAllCheckOff, this.mnEditSep1, this.mnSelectCheckOn, this.mnSelectCheckOff });
            this.mnEdit.Name = "mnEdit";
            this.mnEdit.Size = new Size(0x3f, 0x16);
            this.mnEdit.Text = "Sửa (&E)";
            this.mnAllCheckOn.Name = "mnAllCheckOn";
            this.mnAllCheckOn.ShortcutKeys = Keys.Control | Keys.A;
            this.mnAllCheckOn.Size = new Size(0x139, 0x16);
            this.mnAllCheckOn.Text = "Chọn tất cả (&A)";
            this.mnAllCheckOn.Click += new EventHandler(this.mnAllCheckOn_Click);
            this.mnAllCheckOff.Name = "mnAllCheckOff";
            this.mnAllCheckOff.ShortcutKeys = Keys.Control | Keys.Shift | Keys.A;
            this.mnAllCheckOff.Size = new Size(0x139, 0x16);
            this.mnAllCheckOff.Text = "Bỏ chọn tất cả (&C)";
            this.mnAllCheckOff.Click += new EventHandler(this.mnAllCheckOff_Click);
            this.mnEditSep1.Name = "mnEditSep1";
            this.mnEditSep1.Size = new Size(310, 6);
            this.mnSelectCheckOn.Name = "mnSelectCheckOn";
            this.mnSelectCheckOn.ShortcutKeys = Keys.Control | Keys.R;
            this.mnSelectCheckOn.Size = new Size(0x139, 0x16);
            this.mnSelectCheckOn.Text = "Chọn v\x00f9ng lựa chọn (&R)";
            this.mnSelectCheckOn.Click += new EventHandler(this.mnSelectCheckOn_Click);
            this.mnSelectCheckOff.Name = "mnSelectCheckOff";
            this.mnSelectCheckOff.ShortcutKeys = Keys.Control | Keys.Shift | Keys.R;
            this.mnSelectCheckOff.Size = new Size(0x139, 0x16);
            this.mnSelectCheckOff.Text = "Bỏ chọn v\x00f9ng lựa chọn (&Q)";
            this.mnSelectCheckOff.Click += new EventHandler(this.mnSelectCheckOff_Click);
            this.grpFind.Controls.Add(this.lblFindString);
            this.grpFind.Controls.Add(this.btnFind);
            this.grpFind.Controls.Add(this.cmbFindKind);
            this.grpFind.Controls.Add(this.txbFindString);
            this.grpFind.Controls.Add(this.lblFindKind);
            this.grpFind.Location = new Point(270, 3);
            this.grpFind.Name = "grpFind";
            this.grpFind.Size = new Size(0x233, 0x2d);
            this.grpFind.TabIndex = 2;
            this.grpFind.TabStop = false;
            this.grpFind.Text = "T\x00ecm kiếm";
            this.dgvSelectList.AllowUserToAddRows = false;
            this.dgvSelectList.AllowUserToDeleteRows = false;
            this.dgvSelectList.AllowUserToResizeRows = false;
            this.dgvSelectList.BorderStyle = BorderStyle.Fixed3D;
            style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            style.BackColor = SystemColors.Control;
            style.ForeColor = SystemColors.WindowText;
            style.SelectionBackColor = SystemColors.Highlight;
            style.SelectionForeColor = SystemColors.HighlightText;
            style.WrapMode = DataGridViewTriState.False;
            this.dgvSelectList.ColumnHeadersDefaultCellStyle = style;
            this.dgvSelectList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSelectList.Columns.AddRange(new DataGridViewColumn[] { this.clSelect, this.clFolder, this.clAirSea, this.clJobCode, this.clOutCode, this.clInputNo, this.clJobInfo, this.clTimeStamp, this.clmFileName });
            this.dgvSelectList.Dock = DockStyle.Fill;
            this.dgvSelectList.Location = new Point(0, 0x52);
            this.dgvSelectList.Name = "dgvSelectList";
            this.dgvSelectList.RowTemplate.Height = 0x15;
            this.dgvSelectList.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvSelectList.Size = new Size(0x353, 0x116);
            this.dgvSelectList.TabIndex = 2;
            this.clSelect.DataPropertyName = "clSelect";
            this.clSelect.HeaderText = "Chọn";
            this.clSelect.Name = "clSelect";
            this.clSelect.Resizable = DataGridViewTriState.True;
            this.clSelect.Width = 0x2d;
            this.clFolder.DataPropertyName = "clFolder";
            this.clFolder.HeaderText = "Thư mục";
            this.clFolder.Name = "clFolder";
            this.clFolder.ReadOnly = true;
            this.clFolder.Width = 120;
            this.clAirSea.DataPropertyName = "clAirSea";
            this.clAirSea.HeaderText = "A/S";
            this.clAirSea.Name = "clAirSea";
            this.clAirSea.ReadOnly = true;
            this.clAirSea.Visible = false;
            this.clAirSea.Width = 40;
            this.clJobCode.DataPropertyName = "clJobCode";
            this.clJobCode.HeaderText = "M\x00e3 nghiệp vụ";
            this.clJobCode.Name = "clJobCode";
            this.clJobCode.ReadOnly = true;
            this.clOutCode.DataPropertyName = "clOutCode";
            this.clOutCode.HeaderText = "M\x00e3 th\x00f4ng điệp đầu ra";
            this.clOutCode.Name = "clOutCode";
            this.clOutCode.ReadOnly = true;
            this.clInputNo.DataPropertyName = "clInputNo";
            this.clInputNo.HeaderText = "M\x00e3 th\x00f4ng điệp đầu v\x00e0o";
            this.clInputNo.Name = "clInputNo";
            this.clInputNo.ReadOnly = true;
            this.clInputNo.Width = 160;
            this.clJobInfo.DataPropertyName = "clJobInfo";
            this.clJobInfo.HeaderText = "Ti\x00eau đề";
            this.clJobInfo.Name = "clJobInfo";
            this.clJobInfo.ReadOnly = true;
            this.clJobInfo.Width = 120;
            this.clTimeStamp.DataPropertyName = "clTimeStamp";
            this.clTimeStamp.HeaderText = "Thời gian gửi th\x00f4ng điệp";
            this.clTimeStamp.Name = "clTimeStamp";
            this.clTimeStamp.ReadOnly = true;
            this.clTimeStamp.Width = 160;
            this.clmFileName.DataPropertyName = "clmFileName";
            this.clmFileName.HeaderText = "B\x00e1o c\x00e1o t\x00ean";
            this.clmFileName.Name = "clmFileName";
            this.clmFileName.ReadOnly = true;
            this.clmFileName.Visible = false;
            this.btnRevert.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnRevert.Location = new Point(0x23b, 9);
            this.btnRevert.Name = "btnRevert";
            this.btnRevert.Size = new Size(0xcd, 0x17);
            this.btnRevert.TabIndex = 0;
            this.btnRevert.Text = "Hiển thị danh s\x00e1ch gửi nhận tin nhắn";
            this.btnRevert.UseVisualStyleBackColor = true;
            this.btnRevert.Click += new EventHandler(this.btnRevert_Click);
            this.btnClose.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnClose.Location = new Point(0x30e, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new Size(0x33, 0x17);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Đ\x00f3ng";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new EventHandler(this.btnClose_Click);
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new Point(0x97, 0x15);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new Size(0x1c, 12);
            this.lblYear.TabIndex = 1;
            this.lblYear.Text = "Năm";
            this.cmbYear.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cmbYear.FormattingEnabled = true;
            this.cmbYear.Items.AddRange(new object[] { "2009" });
            this.cmbYear.Location = new Point(0x5f, 0x11);
            this.cmbYear.MaxLength = 4;
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new Size(50, 20);
            this.cmbYear.TabIndex = 2;
            this.cmbYear.SelectedIndexChanged += new EventHandler(this.cmbYear_SelectedIndexChanged);
            this.cmbYear.TextUpdate += new EventHandler(this.cmbYear_TextUpdate);
            this.cmbMonth.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cmbMonth.FormattingEnabled = true;
            this.cmbMonth.Items.AddRange(new object[] { "12" });
            this.cmbMonth.Location = new Point(8, 0x11);
            this.cmbMonth.MaxLength = 2;
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.Size = new Size(0x27, 20);
            this.cmbMonth.TabIndex = 0;
            this.lblMonth.AutoSize = true;
            this.lblMonth.Location = new Point(0x35, 0x15);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new Size(0x24, 12);
            this.lblMonth.TabIndex = 3;
            this.lblMonth.Text = "Th\x00e1ng";
            this.grpDate.Controls.Add(this.btnExract);
            this.grpDate.Controls.Add(this.lblYear);
            this.grpDate.Controls.Add(this.cmbMonth);
            this.grpDate.Controls.Add(this.cmbYear);
            this.grpDate.Controls.Add(this.lblMonth);
            this.grpDate.Location = new Point(14, 3);
            this.grpDate.Name = "grpDate";
            this.grpDate.Size = new Size(0xf9, 0x2d);
            this.grpDate.TabIndex = 1;
            this.grpDate.TabStop = false;
            this.grpDate.Text = "Thời gian";
            this.btnExract.Location = new Point(0xb5, 14);
            this.btnExract.Name = "btnExract";
            this.btnExract.Size = new Size(0x3e, 0x17);
            this.btnExract.TabIndex = 4;
            this.btnExract.Text = "Hiển thị";
            this.btnExract.UseVisualStyleBackColor = true;
            this.btnExract.Click += new EventHandler(this.btnExract_Click);
            this.pnlTop.BackColor = SystemColors.GradientInactiveCaption;
            this.pnlTop.Controls.Add(this.grpDate);
            this.pnlTop.Controls.Add(this.grpFind);
            this.pnlTop.Dock = DockStyle.Top;
            this.pnlTop.Location = new Point(0, 0x1a);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new Size(0x353, 0x38);
            this.pnlTop.TabIndex = 1;
            this.pnlBottom.Controls.Add(this.label1);
            this.pnlBottom.Controls.Add(this.btnClose);
            this.pnlBottom.Controls.Add(this.btnRevert);
            this.pnlBottom.Dock = DockStyle.Bottom;
            this.pnlBottom.Location = new Point(0, 360);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new Size(0x353, 0x29);
            this.pnlBottom.TabIndex = 3;
            this.label1.AutoSize = true;
            this.label1.Location = new Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x48, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "Số trường hợp";
            this.label1.Visible = false;
            this.contextMenuStrip1.Items.AddRange(new ToolStripItem[] { this.AllCheckOn_MouseClick, this.AllCheckOff_MouseClick, this.SelectCheckOn_MouseClick, this.SelectCheckOff_MouseClick });
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new Size(230, 0x5c);
            this.AllCheckOn_MouseClick.Name = "AllCheckOn_MouseClick";
            this.AllCheckOn_MouseClick.Size = new Size(0xe5, 0x16);
            this.AllCheckOn_MouseClick.Text = "Chọn tất cả (&A)";
            this.AllCheckOn_MouseClick.Click += new EventHandler(this.AllCheckOn_MouseClick_Click);
            this.AllCheckOff_MouseClick.Name = "AllCheckOff_MouseClick";
            this.AllCheckOff_MouseClick.Size = new Size(0xe5, 0x16);
            this.AllCheckOff_MouseClick.Text = "Bỏ chọn tất cả (&C)";
            this.AllCheckOff_MouseClick.Click += new EventHandler(this.AllCheckOff_MouseClick_Click);
            this.SelectCheckOn_MouseClick.Name = "SelectCheckOn_MouseClick";
            this.SelectCheckOn_MouseClick.Size = new Size(0xe5, 0x16);
            this.SelectCheckOn_MouseClick.Text = "Chọn v\x00f9ng lựa chọn (&R)";
            this.SelectCheckOn_MouseClick.Click += new EventHandler(this.SelectCheckOn_MouseClick_Click);
            this.SelectCheckOff_MouseClick.Name = "SelectCheckOff_MouseClick";
            this.SelectCheckOff_MouseClick.Size = new Size(0xe5, 0x16);
            this.SelectCheckOff_MouseClick.Text = "Bỏ chọn v\x00f9ng lựa chọn (&Q)";
            this.SelectCheckOff_MouseClick.Click += new EventHandler(this.SelectCheckOff_MouseClick_Click);
            this.dataGridViewTextBoxColumn1.DataPropertyName = "clmFolder";
            this.dataGridViewTextBoxColumn1.HeaderText = "フォルダ名";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 130;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "clmSystem";
            this.dataGridViewTextBoxColumn2.HeaderText = "A/S";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 40;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "clmJobcode";
            this.dataGridViewTextBoxColumn3.HeaderText = "業務コード";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 90;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "clmOutcode";
            this.dataGridViewTextBoxColumn4.HeaderText = "出力コード";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "clmInputInfo";
            this.dataGridViewTextBoxColumn5.HeaderText = "入力No";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "clmSubject";
            this.dataGridViewTextBoxColumn6.HeaderText = "業務固有情報";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 130;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "clmTimestamp";
            this.dataGridViewTextBoxColumn7.HeaderText = "送受信時刻";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 120;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "clmFileName";
            this.dataGridViewTextBoxColumn8.HeaderText = "ファイル名";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            base.AutoScaleDimensions = new SizeF(6f, 12f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x353, 0x191);
            this.ContextMenuStrip = this.contextMenuStrip1;
            base.ControlBox = false;
            base.Controls.Add(this.dgvSelectList);
            base.Controls.Add(this.pnlBottom);
            base.Controls.Add(this.pnlTop);
            base.Controls.Add(this.mnRevertDialog);
            this.DoubleBuffered = true;
            base.MainMenuStrip = this.mnRevertDialog;
            base.Name = "RevertDialog";
            this.Text = "Quản l\x00fd danh s\x00e1ch tin nhắn cũ";
            base.Load += new EventHandler(this.RevertDialog_Load);
            this.mnRevertDialog.ResumeLayout(false);
            this.mnRevertDialog.PerformLayout();
            this.grpFind.ResumeLayout(false);
            this.grpFind.PerformLayout();
            ((ISupportInitialize) this.dgvSelectList).EndInit();
            this.grpDate.ResumeLayout(false);
            this.grpDate.PerformLayout();
            this.pnlTop.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void makeFindKindCombo()
        {
            this.findKind_table.Rows.Add(new string[] { "Thư mục", "clFolder" });
            this.findKind_table.Rows.Add(new string[] { "M\x00e3 nghiệp vụ", "clJobCode" });
            this.findKind_table.Rows.Add(new string[] { "M\x00e3 th\x00f4ng điệp đầu ra", "clOutCode" });
            this.findKind_table.Rows.Add(new string[] { "M\x00e3 th\x00f4ng điệp đầu v\x00e0o", "clInputNo" });
            this.findKind_table.Rows.Add(new string[] { "Ti\x00eau đề", "clJobInfo" });
            DataView view = new DataView(this.findKind_table);
            this.cmbFindKind.DisplayMember = "FIND_KIND_NAME";
            this.cmbFindKind.ValueMember = "FIND_KIND_CLM";
            this.cmbFindKind.DataSource = view;
            this.cmbFindKind.SelectedValue = "";
        }

        private void makeMonthCombo()
        {
            DataView view = new DataView(this.year_month_table);
            this.cmbMonth.DisplayMember = "MONTH";
            this.cmbMonth.ValueMember = "MONTH";
            view.RowFilter = string.Format("YEAR = '{0}'", this.cmbYear.Text);
            this.cmbMonth.DataSource = view;
        }

        private void makeYearCombo()
        {
            try
            {
                string[] array = Directory.GetDirectories(this.pastDataViewPath, "*", SearchOption.AllDirectories);
                Array.Reverse(array);
                List<string> list = new List<string>();
                new List<string>();
                string str = "";
                string str2 = "";
                string str3 = "";
                foreach (string str4 in array)
                {
                    string str5 = str4.Replace(this.pastDataViewPath + @"\", "");
                    str = str5.Substring(0, 4);
                    str3 = str5.Substring(4, 2);
                    if (!str2.Equals(str))
                    {
                        list.Add(str);
                        str2 = str;
                    }
                    this.year_month_table.Rows.Add(new string[] { str, str3 });
                }
                this.cmbYear.DataSource = list.ToArray();
            }
            catch (Exception)
            {
            }
        }

        private void mnAllCheckOff_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in (IEnumerable) this.dgvSelectList.Rows)
            {
                row.Cells["clSelect"].Value = false;
            }
            this.dgvSelectList.EndEdit();
        }

        private void mnAllCheckOn_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in (IEnumerable) this.dgvSelectList.Rows)
            {
                row.Cells["clSelect"].Value = true;
            }
            this.dgvSelectList.EndEdit();
        }

        private void mnDelete_Click(object sender, EventArgs e)
        {
            if (((this.dlYear != "") && (this.dlMonth != "")) && ((this.dlYear == this.cmbYear.Text) && (this.dlMonth == this.cmbMonth.Text)))
            {
                this.pastDataView_YYYYMM = this.cmbYear.Text + this.cmbMonth.Text;
                string path = Path.Combine(this.pastDataViewPath, this.pastDataView_YYYYMM);
                if (Directory.Exists(path))
                {
                    MessageDialog dialog = new MessageDialog();
                    DialogResult result = dialog.ShowMessage("C405", this.pastDataView_YYYYMM, "");
                    dialog.Close();
                    if (result == DialogResult.Yes)
                    {
                        if (ArrangeDataTable.IsLock(this.pastDataView_YYYYMM))
                        {
                            string message = Resources.ResourceManager.GetString("CORE23");
                            using (MessageDialogSimpleForm form = new MessageDialogSimpleForm())
                            {
                                form.ShowMessage(ButtonPatern.OK_ONLY, MessageKind.Error, message);
                            }
                            Environment.Exit(0);
                        }
                        Directory.Delete(path, true);
                        this.RevertDialog_Load(sender, e);
                    }
                }
            }
        }

        private void mnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void mnRevertDialog_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
        }

        private void mnSelectCheckOff_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dgvSelectList.SelectedRows)
            {
                row.Cells["clSelect"].Value = false;
            }
            this.dgvSelectList.EndEdit();
        }

        private void mnSelectCheckOn_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dgvSelectList.SelectedRows)
            {
                row.Cells["clSelect"].Value = true;
            }
            this.dgvSelectList.EndEdit();
        }

        private void RevertDialog_Load(object sender, EventArgs e)
        {
            DataTable table = ArrangeDataTable.CreateDataTable();
            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.Boolean");
            column.ColumnName = "clSelect";
            table.Columns.Add(column);
            this.dgvSelectList.DataSource = table;
            this.dgvSelectList.Columns["clFileName"].Visible = false;
            this.dgvSelectList.AlternatingRowsDefaultCellStyle.BackColor = Color.Gainsboro;
        }

        private void SelectCheckOff_MouseClick_Click(object sender, EventArgs e)
        {
            this.mnSelectCheckOff_Click(sender, e);
        }

        private void SelectCheckOn_MouseClick_Click(object sender, EventArgs e)
        {
            this.mnSelectCheckOn_Click(sender, e);
        }

        public Icon FormIcon
        {
            set
            {
                base.Icon = value;
            }
        }
    }
}

