using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V2
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;
#elif GC_V2
using Company.GC.BLL.KDT;
using Company.GC.BLL.DuLieuChuan;
#elif SXXK_V2
using Company.BLL.KDT;
using Company.BLL.DuLieuChuan;
#endif


using System.Collections.Generic;
using System.Linq; 

namespace Company.Interface.Report
{
    public partial class TQDTPhuLucToKhaiXuat_TT15 : DevExpress.XtraReports.UI.XtraReport
    {
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        public int SoToKhai;
        public DateTime NgayDangKy;
        public ToKhaiMauDich TKMD;
        public Company.Interface.Report.ReportViewTKXTQDTFormTT15 report;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public int soDongHang;
        public int sTT_Container;
        #if KD_V2
        double _TongTriGiaNguyenTe = 0;
        double _TongThueXuatKhau = 0;
        double _TongThueThuKhac = 0;
        #elif GC_V2
        decimal _TongTriGiaNguyenTe = 0;
        decimal _TongThueXuatKhau = 0;
        decimal _TongThueThuKhac = 0;
        #elif SXXK_V2
        decimal _TongTriGiaNguyenTe = 0;
        decimal _TongThueXuatKhau = 0;
        decimal _TongThueThuKhac = 0;
        //#elif 
        //double _TongTriGiaNguyenTe = 0;
        //double _TongThueXuatKhau = 0;
        //double _TongThueThuKhac = 0;
        #endif 
        public TQDTPhuLucToKhaiXuat_TT15()
        {
            InitializeComponent();
        }

        public void BindReport(string pls)
        {
            _TongTriGiaNguyenTe = 0;
            _TongThueXuatKhau = 0;
            _TongThueThuKhac = 0;
            try
            {
                TinhTong();
                sTT_Container = (Convert.ToInt16(pls)) * 4 - 3;
                #region Thông tin chính
                xtcChiCucHaiQuan.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                xtcChiCucCuaKhau.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);
                xtcSoPhuLuc.Text = pls;
                xtcNgayDangKy.Text = NgayDangKy.ToString("dd-MM-yyyy");
                xtcSoToKhai.Text = SoToKhai.ToString(); ;
                xtcLoaiHinh.Text = TKMD.MaLoaiHinh;
                #endregion
                #region Bảng mô tả hàng - Ô 15 - Ô 21
                DetailHangHoa.DataSource = HMDCollection;
                xtcHang_STT.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoThuTuHang");
                xtcHang_MoTa.DataBindings.Add("Text", DetailHangHoa.DataSource, "TenHang");
                xtcHang_MaSo.DataBindings.Add("Text", DetailHangHoa.DataSource, "MaHS");
                xtcHang_XuatXu.DataBindings.Add("Text", DetailHangHoa.DataSource, "NuocXX_ID");
                xtcHang_LuongHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoLuong");
                xtcHang_DVT.DataBindings.Add("Text", DetailHangHoa.DataSource, "DVT_ID");
                xtcHang_DonGiaNT.DataBindings.Add("Text", DetailHangHoa.DataSource, "DonGiaKB", Company.KDT.SHARE.Components.Globals.FormatNumber(2));
                xtcHang_TriGiaNT.DataBindings.Add("Text", DetailHangHoa.DataSource, "TriGiaKB", Company.KDT.SHARE.Components.Globals.FormatNumber(2));
                xtcHang_Cong.Text = _TongTriGiaNguyenTe.ToString();
                #endregion
                #region Bảng Thuế xuất - Ô 22 + 23
                DetailThue.DataSource = HMDCollection;
                xtcThueXK_TriGiaTinhThue.DataBindings.Add("Text", DetailThue.DataSource, "TriGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(2));
                xtcThueXK_ThueSuat.DataBindings.Add("Text", DetailThue.DataSource, "ThueSuatXNK");
                xtcThueXK_Thue.DataBindings.Add("Text", DetailThue.DataSource, "ThueXNK", Company.KDT.SHARE.Components.Globals.FormatNumber(2));
                xtcThueXK_STT.DataBindings.Add("Text", DetailThue.DataSource, "SoThuTuHang");
                xtcThuKhac_TriGiaTT.DataBindings.Add("Text", DetailThue.DataSource, "TriGiaThuKhac");
                xtcThuKhac_TyLe.DataBindings.Add("Text", DetailThue.DataSource, "TyLeThuKhac");
                xtcThuKhac_SoTien.DataBindings.Add("Text", DetailThue.DataSource, "PhuThu", Company.KDT.SHARE.Components.Globals.FormatNumber(2));
                xtcThueXK_Cong.Text = _TongThueXuatKhau.ToString();
                xtcThuKhac_Cong.Text = _TongThueThuKhac.ToString();
                #endregion
                #region Bảng Container - Ô 25
                DetailLuongHang.DataSource = ContainerCo;
                xtcLuonghang_SoHieu.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoHieu");
                xtcLuongHang_SoLuong.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoHieu");
                xtcLuongHang_TrongLuong.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoHieu");
                xtcLuongHang_DiaDiem.DataBindings.Add("Text", TKMD.VanTaiDon, "TenCangXepHang");
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Co 1 loi ko xac dinh duoc la do hang ko nhap gia tri FOC ma de trong, bat buoc phai nhap = 0: Khi import excel
            }
        }

        private void xtcLuongHang_STT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRTableCell)sender).Text = sTT_Container.ToString();
            sTT_Container++;
        }
        private void TinhTong()
        {
            _TongThueThuKhac = _TongThueXuatKhau = _TongTriGiaNguyenTe = 0;
            foreach (HangMauDich item in HMDCollection)
            {
                _TongTriGiaNguyenTe += item.TriGiaKB;
                _TongThueXuatKhau += item.ThueXNK;
                _TongThueThuKhac += item.PhuThu;
            }
        }
        //private decimal TinhSoLuongHangContainer(string soHieuContainer)
        //{
        //    IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangCollection = from h in TKMD.VanTaiDon.ListHangOfVanDon
        //                                                                                   where h.SoHieuContainer == soHieuContainer
        //                                                                                   select h;
        //    decimal soLuongHang = 0;
        //    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail item in HangCollection)
        //        soLuongHang += item.SoLuong;
        //    return soLuongHang;
        //}
        //private double TinhTrongLuongHangContainer(string soHieuContainer)
        //{
        //    IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangCollection = from h in TKMD.VanTaiDon.ListHangOfVanDon
        //                                                                                   where h.SoHieuContainer == soHieuContainer
        //                                                                                   select h;
        //    double trongLuong = 0;
        //    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail item in HangCollection)
        //        trongLuong += item.TrongLuong;
        //    _TinhTongTrongLuong = +trongLuong;
        //    return trongLuong;
        //}

        //private void xtcLuongHang_SoLuong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    XRTableCell slkien = (XRTableCell)sender;
        //    if (!string.IsNullOrEmpty(slkien.Text))
        //        slkien.Text = TinhSoLuongHangContainer(slkien.Text).ToString();
        //}

        //private void xtcLuongHang_TrongLuong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    XRTableCell TrongLuongHang = (XRTableCell)sender;
        //    if (!string.IsNullOrEmpty(TrongLuongHang.Text))
        //        TrongLuongHang.Text = TinhTrongLuongHangContainer(TrongLuongHang.Text).ToString(); 
        //}

        //private void xtcLuongHang_Cong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    XRTableCell tongTL = (XRTableCell)sender;
        //    if (_TinhTongTrongLuong != 0)
        //        tongTL.Text = _TinhTongTrongLuong.ToString();

        //}

        private void xtcHang_DVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dvt = (XRTableCell)sender;

            if (!string.IsNullOrEmpty(dvt.Text))
                dvt.Text = DonViTinh.GetName(dvt.Text.Trim());
        }

    }
}
