-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Hang_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Hang_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Hang_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Hang_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Hang_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Hang_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Insert]
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaNPL2 nvarchar(30),
	@TenHang nvarchar(200),
	@Art nvarchar(30),
	@NCC nvarchar(30),
	@Size nvarchar(30),
	@Color nvarchar(30),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@DM numeric(18, 2),
	@PhanTram numeric(18, 2),
	@Nhucau numeric(24, 4),
	@ThucNhan numeric(24, 4),
	@GhiChu nvarchar(200),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_PhieuXuatKho_Hang]
(
	[TKMD_ID],
	[MaSoHang],
	[MaNPL2],
	[TenHang],
	[Art],
	[NCC],
	[Size],
	[Color],
	[SoLuong1],
	[DVTLuong1],
	[DM],
	[PhanTram],
	[Nhucau],
	[ThucNhan],
	[GhiChu]
)
VALUES 
(
	@TKMD_ID,
	@MaSoHang,
	@MaNPL2,
	@TenHang,
	@Art,
	@NCC,
	@Size,
	@Color,
	@SoLuong1,
	@DVTLuong1,
	@DM,
	@PhanTram,
	@Nhucau,
	@ThucNhan,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaNPL2 nvarchar(30),
	@TenHang nvarchar(200),
	@Art nvarchar(30),
	@NCC nvarchar(30),
	@Size nvarchar(30),
	@Color nvarchar(30),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@DM numeric(18, 2),
	@PhanTram numeric(18, 2),
	@Nhucau numeric(24, 4),
	@ThucNhan numeric(24, 4),
	@GhiChu nvarchar(200)
AS

UPDATE
	[dbo].[t_KDT_GC_PhieuXuatKho_Hang]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaSoHang] = @MaSoHang,
	[MaNPL2] = @MaNPL2,
	[TenHang] = @TenHang,
	[Art] = @Art,
	[NCC] = @NCC,
	[Size] = @Size,
	[Color] = @Color,
	[SoLuong1] = @SoLuong1,
	[DVTLuong1] = @DVTLuong1,
	[DM] = @DM,
	[PhanTram] = @PhanTram,
	[Nhucau] = @Nhucau,
	[ThucNhan] = @ThucNhan,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaNPL2 nvarchar(30),
	@TenHang nvarchar(200),
	@Art nvarchar(30),
	@NCC nvarchar(30),
	@Size nvarchar(30),
	@Color nvarchar(30),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@DM numeric(18, 2),
	@PhanTram numeric(18, 2),
	@Nhucau numeric(24, 4),
	@ThucNhan numeric(24, 4),
	@GhiChu nvarchar(200)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_PhieuXuatKho_Hang] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_PhieuXuatKho_Hang] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaSoHang] = @MaSoHang,
			[MaNPL2] = @MaNPL2,
			[TenHang] = @TenHang,
			[Art] = @Art,
			[NCC] = @NCC,
			[Size] = @Size,
			[Color] = @Color,
			[SoLuong1] = @SoLuong1,
			[DVTLuong1] = @DVTLuong1,
			[DM] = @DM,
			[PhanTram] = @PhanTram,
			[Nhucau] = @Nhucau,
			[ThucNhan] = @ThucNhan,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_PhieuXuatKho_Hang]
		(
			[TKMD_ID],
			[MaSoHang],
			[MaNPL2],
			[TenHang],
			[Art],
			[NCC],
			[Size],
			[Color],
			[SoLuong1],
			[DVTLuong1],
			[DM],
			[PhanTram],
			[Nhucau],
			[ThucNhan],
			[GhiChu]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaSoHang,
			@MaNPL2,
			@TenHang,
			@Art,
			@NCC,
			@Size,
			@Color,
			@SoLuong1,
			@DVTLuong1,
			@DM,
			@PhanTram,
			@Nhucau,
			@ThucNhan,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_PhieuXuatKho_Hang]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_PhieuXuatKho_Hang] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaNPL2],
	[TenHang],
	[Art],
	[NCC],
	[Size],
	[Color],
	[SoLuong1],
	[DVTLuong1],
	[DM],
	[PhanTram],
	[Nhucau],
	[ThucNhan],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_PhieuXuatKho_Hang]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaNPL2],
	[TenHang],
	[Art],
	[NCC],
	[Size],
	[Color],
	[SoLuong1],
	[DVTLuong1],
	[DM],
	[PhanTram],
	[Nhucau],
	[ThucNhan],
	[GhiChu]
FROM [dbo].[t_KDT_GC_PhieuXuatKho_Hang] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaNPL2],
	[TenHang],
	[Art],
	[NCC],
	[Size],
	[Color],
	[SoLuong1],
	[DVTLuong1],
	[DM],
	[PhanTram],
	[Nhucau],
	[ThucNhan],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_PhieuXuatKho_Hang]	

GO

