﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.KDT.SHARE.Components;
namespace Company.KD.BLL.SXXK.ToKhai
{
	public partial class ToKhaiMauDich : EntityBase
	{
		#region Private members.
		
		protected string _MaHaiQuan = String.Empty;
		protected int _SoToKhai;
		protected string _MaLoaiHinh = String.Empty;
		protected short _NamDangKy;
		protected DateTime _NgayDangKy = new DateTime(1900, 01, 01);
		protected string _MaDoanhNghiep = String.Empty;
		protected string _TenDoanhNghiep = String.Empty;
		protected string _MaDaiLyTTHQ = String.Empty;
		protected string _TenDaiLyTTHQ = String.Empty;
		protected string _TenDonViDoiTac = String.Empty;
		protected string _ChiTietDonViDoiTac = String.Empty;
		protected string _SoGiayPhep = String.Empty;
		protected DateTime _NgayGiayPhep = new DateTime(1900, 01, 01);
		protected DateTime _NgayHetHanGiayPhep = new DateTime(1900, 01, 01);
		protected string _SoHopDong = String.Empty;
		protected DateTime _NgayHopDong = new DateTime(1900, 01, 01);
		protected DateTime _NgayHetHanHopDong = new DateTime(1900, 01, 01);
		protected string _SoHoaDonThuongMai = String.Empty;
		protected DateTime _NgayHoaDonThuongMai = new DateTime(1900, 01, 01);
		protected string _PTVT_ID = String.Empty;
		protected string _SoHieuPTVT = String.Empty;
		protected DateTime _NgayDenPTVT = new DateTime(1900, 01, 01);
		protected string _QuocTichPTVT_ID = String.Empty;
		protected string _LoaiVanDon = String.Empty;
		protected string _SoVanDon = String.Empty;
		protected DateTime _NgayVanDon = new DateTime(1900, 01, 01);
		protected string _NuocXK_ID = String.Empty;
		protected string _NuocNK_ID = String.Empty;
		protected string _DiaDiemXepHang = String.Empty;
		protected string _CuaKhau_ID = String.Empty;
		protected string _DKGH_ID = String.Empty;
		protected string _NguyenTe_ID = String.Empty;
		protected decimal _TyGiaTinhThue;
		protected decimal _TyGiaUSD;
		protected string _PTTT_ID = String.Empty;
		protected short _SoHang;
		protected short _SoLuongPLTK;
		protected string _TenChuHang = String.Empty;
		protected decimal _SoContainer20;
		protected decimal _SoContainer40;
		protected decimal _SoKien;
		protected decimal _TrongLuong;
		protected decimal _TongTriGiaKhaiBao;
		protected decimal _TongTriGiaTinhThue;
		protected string _LoaiToKhaiGiaCong = String.Empty;
		protected decimal _LePhiHaiQuan;
		protected decimal _PhiBaoHiem;
		protected decimal _PhiVanChuyen;
		protected decimal _PhiXepDoHang;
		protected decimal _PhiKhac;
		protected string _Xuat_NPL_SP = String.Empty;
		protected string _ThanhLy = String.Empty;
		protected DateTime _NGAY_THN_THX = new DateTime(1900, 01, 01);
        protected DateTime _NGayHoanThanh = new DateTime(1900, 01, 01);
		protected string _MaDonViUT = String.Empty;
        private HangMauDichCollection _HMDCollection = new HangMauDichCollection();
        protected string _TrangThaiThanhKhoan = String.Empty;
        protected string _ChungTu = String.Empty;
        protected string _PhanLuong = String.Empty;
        protected int _TrangThai;
        protected double _TrongLuongNet;
        protected decimal _SoTienKhoan;
		
		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.

        public DateTime NgayHoanThanh
        {
            set { this._NGayHoanThanh = value; }
            get { return this._NGayHoanThanh; }
        }


        public string PhanLuong
        {
            set { this._PhanLuong = value; }
            get { return this._PhanLuong; }
        }

        public string ChungTu
        {
            set { this._ChungTu = value; }
            get { return this._ChungTu; }
        }
        public HangMauDichCollection HMDCollection
        {
            set { this._HMDCollection = value; }
            get { return this._HMDCollection; }
        }

		public string MaHaiQuan
		{
			set {this._MaHaiQuan = value;}
			get {return this._MaHaiQuan;}
		}
		public int SoToKhai
		{
			set {this._SoToKhai = value;}
			get {return this._SoToKhai;}
		}
		public string MaLoaiHinh
		{
			set {this._MaLoaiHinh = value;}
			get {return this._MaLoaiHinh;}
		}
		public short NamDangKy
		{
			set {this._NamDangKy = value;}
			get {return this._NamDangKy;}
		}
		public DateTime NgayDangKy
		{
			set {this._NgayDangKy = value;}
			get {return this._NgayDangKy;}
		}
		public string MaDoanhNghiep
		{
			set {this._MaDoanhNghiep = value;}
			get {return this._MaDoanhNghiep;}
		}
		public string TenDoanhNghiep
		{
			set {this._TenDoanhNghiep = value;}
			get {return this._TenDoanhNghiep;}
		}
		public string MaDaiLyTTHQ
		{
			set {this._MaDaiLyTTHQ = value;}
			get {return this._MaDaiLyTTHQ;}
		}
		public string TenDaiLyTTHQ
		{
			set {this._TenDaiLyTTHQ = value;}
			get {return this._TenDaiLyTTHQ;}
		}
		public string TenDonViDoiTac
		{
			set {this._TenDonViDoiTac = value;}
			get {return this._TenDonViDoiTac;}
		}
		public string ChiTietDonViDoiTac
		{
			set {this._ChiTietDonViDoiTac = value;}
			get {return this._ChiTietDonViDoiTac;}
		}
		public string SoGiayPhep
		{
			set {this._SoGiayPhep = value;}
			get {return this._SoGiayPhep;}
		}
		public DateTime NgayGiayPhep
		{
			set {this._NgayGiayPhep = value;}
			get {return this._NgayGiayPhep;}
		}
		public DateTime NgayHetHanGiayPhep
		{
			set {this._NgayHetHanGiayPhep = value;}
			get {return this._NgayHetHanGiayPhep;}
		}
		public string SoHopDong
		{
			set {this._SoHopDong = value;}
			get {return this._SoHopDong;}
		}
		public DateTime NgayHopDong
		{
			set {this._NgayHopDong = value;}
			get {return this._NgayHopDong;}
		}
		public DateTime NgayHetHanHopDong
		{
			set {this._NgayHetHanHopDong = value;}
			get {return this._NgayHetHanHopDong;}
		}
		public string SoHoaDonThuongMai
		{
			set {this._SoHoaDonThuongMai = value;}
			get {return this._SoHoaDonThuongMai;}
		}
		public DateTime NgayHoaDonThuongMai
		{
			set {this._NgayHoaDonThuongMai = value;}
			get {return this._NgayHoaDonThuongMai;}
		}
		public string PTVT_ID
		{
			set {this._PTVT_ID = value;}
			get {return this._PTVT_ID;}
		}
		public string SoHieuPTVT
		{
			set {this._SoHieuPTVT = value;}
			get {return this._SoHieuPTVT;}
		}
		public DateTime NgayDenPTVT
		{
			set {this._NgayDenPTVT = value;}
			get {return this._NgayDenPTVT;}
		}
		public string QuocTichPTVT_ID
		{
			set {this._QuocTichPTVT_ID = value;}
			get {return this._QuocTichPTVT_ID;}
		}
		public string LoaiVanDon
		{
			set {this._LoaiVanDon = value;}
			get {return this._LoaiVanDon;}
		}
		public string SoVanDon
		{
			set {this._SoVanDon = value;}
			get {return this._SoVanDon;}
		}
		public DateTime NgayVanDon
		{
			set {this._NgayVanDon = value;}
			get {return this._NgayVanDon;}
		}
		public string NuocXK_ID
		{
			set {this._NuocXK_ID = value;}
			get {return this._NuocXK_ID;}
		}
		public string NuocNK_ID
		{
			set {this._NuocNK_ID = value;}
			get {return this._NuocNK_ID;}
		}
		public string DiaDiemXepHang
		{
			set {this._DiaDiemXepHang = value;}
			get {return this._DiaDiemXepHang;}
		}
		public string CuaKhau_ID
		{
			set {this._CuaKhau_ID = value;}
			get {return this._CuaKhau_ID;}
		}
		public string DKGH_ID
		{
			set {this._DKGH_ID = value;}
			get {return this._DKGH_ID;}
		}
		public string NguyenTe_ID
		{
			set {this._NguyenTe_ID = value;}
			get {return this._NguyenTe_ID;}
		}
		public decimal TyGiaTinhThue
		{
			set {this._TyGiaTinhThue = value;}
			get {return this._TyGiaTinhThue;}
		}
		public decimal TyGiaUSD
		{
			set {this._TyGiaUSD = value;}
			get {return this._TyGiaUSD;}
		}
		public string PTTT_ID
		{
			set {this._PTTT_ID = value;}
			get {return this._PTTT_ID;}
		}
		public short SoHang
		{
			set {this._SoHang = value;}
			get {return this._SoHang;}
		}
		public short SoLuongPLTK
		{
			set {this._SoLuongPLTK = value;}
			get {return this._SoLuongPLTK;}
		}
		public string TenChuHang
		{
			set {this._TenChuHang = value;}
			get {return this._TenChuHang;}
		}
		public decimal SoContainer20
		{
			set {this._SoContainer20 = value;}
			get {return this._SoContainer20;}
		}
		public decimal SoContainer40
		{
			set {this._SoContainer40 = value;}
			get {return this._SoContainer40;}
		}
		public decimal SoKien
		{
			set {this._SoKien = value;}
			get {return this._SoKien;}
		}
		public decimal TrongLuong
		{
			set {this._TrongLuong = value;}
			get {return this._TrongLuong;}
		}
		public decimal TongTriGiaKhaiBao
		{
			set {this._TongTriGiaKhaiBao = value;}
			get {return this._TongTriGiaKhaiBao;}
		}
		public decimal TongTriGiaTinhThue
		{
			set {this._TongTriGiaTinhThue = value;}
			get {return this._TongTriGiaTinhThue;}
		}
		public string LoaiToKhaiGiaCong
		{
			set {this._LoaiToKhaiGiaCong = value;}
			get {return this._LoaiToKhaiGiaCong;}
		}
		public decimal LePhiHaiQuan
		{
			set {this._LePhiHaiQuan = value;}
			get {return this._LePhiHaiQuan;}
		}
		public decimal PhiBaoHiem
		{
			set {this._PhiBaoHiem = value;}
			get {return this._PhiBaoHiem;}
		}
		public decimal PhiVanChuyen
		{
			set {this._PhiVanChuyen = value;}
			get {return this._PhiVanChuyen;}
		}
		public decimal PhiXepDoHang
		{
			set {this._PhiXepDoHang = value;}
			get {return this._PhiXepDoHang;}
		}
		public decimal PhiKhac
		{
			set {this._PhiKhac = value;}
			get {return this._PhiKhac;}
		}
		public string Xuat_NPL_SP
		{
			set {this._Xuat_NPL_SP = value;}
			get {return this._Xuat_NPL_SP;}
		}
		public string ThanhLy
		{
			set {this._ThanhLy = value;}
			get {return this._ThanhLy;}
		}
		public DateTime NGAY_THN_THX
		{
			set {this._NGAY_THN_THX = value;}
			get {return this._NGAY_THN_THX;}
		}
		public string MaDonViUT
		{
			set {this._MaDonViUT = value;}
			get {return this._MaDonViUT;}
		}
        public string TrangThaiThanhKhoan
        {
            set { this._TrangThaiThanhKhoan= value; }
            get { return this._TrangThaiThanhKhoan; }
        }
        public double TrongLuongNet
        {
            set { this._TrongLuongNet = value; }
            get { return this._TrongLuongNet; }
        }
        public decimal SoTienKhoan
        {
            set { this._SoTienKhoan = value; }
            get { return this._SoTienKhoan; }
        }
		
       //Bổ sung
        public int  TrangThai
        {
            set { this._TrangThai   = value; }
            get { return this._TrangThai; }
        }
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
        public void LoadHMDCollection()
        {
            HangMauDich hmd = new HangMauDich();
            hmd.MaHaiQuan = Globals.trimMaHaiQuan(this.MaHaiQuan);
            hmd.MaLoaiHinh = this.MaLoaiHinh;
            hmd.SoToKhai = this.SoToKhai;
            hmd.NamDangKy = this.NamDangKy;
            this.HMDCollection = hmd.SelectCollectionBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy();
        }
		public bool Load()
		{
			string spName = "p_SXXK_ToKhaiMauDich_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) this._Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) this._NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) this.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) this.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) this.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
			}
            reader.Close();
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------

        public DataSet GetThongBao(uint hanthanhkhoan,uint thoigiantk)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            string sql = "SELECT *, (@HanThanhKhoan - DATEDIFF(day, NgayDangKy, GETDATE())) as songay " +
                        "FROM         t_SXXK_ToKhaiMauDich " +
                        "WHERE     (275 - DATEDIFF(day, NgayDangKy, GETDATE()) <=@ThoiGianTK) AND ThanhLy!='H' AND" +
                        " maloaihinh like 'NSX%'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HanThanhKhoan",SqlDbType.Int, hanthanhkhoan);
            db.AddInParameter(dbCommand, "@ThoiGianTK", SqlDbType.Int, thoigiantk);
            return this.db.ExecuteDataSet(dbCommand);              
        }

		public DataSet SelectAll()
        {
            string spName = "p_SXXK_ToKhaiMauDich_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ToKhaiMauDich_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ToKhaiMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ToKhaiMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
        public IDataReader SelectReaderDSTKNChuaThanhLy(string maDoanhNghiep,string  maHaiQuan, DateTime maxDate)
        {
            string spName = "p_SXXK_GetDSTKNChuaThanhLy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@maxDate", SqlDbType.DateTime, maxDate);

            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKXChuaThanhLy(string maDoanhNghiep, string maHaiQuan)
        {
            string spName = "p_SXXK_GetDSTKXChuaThanhLy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKXChuaThanhLyMinDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate)
        {
            string spName = "p_SXXK_GetDSTKXChuaThanhLyMinDate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@MinDate", SqlDbType.DateTime, minDate);

            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKXChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            string spName = "p_SXXK_GetDSTKXChuaThanhLyDate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, minDate);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, maxDate);
            
            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKNChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            string spName = "p_SXXK_GetDSTKNChuaThanhLyDate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, minDate);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, maxDate);

            return this.db.ExecuteReader(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		public ToKhaiMauDichCollection SelectCollectionAll()
		{
			ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				ToKhaiMauDich entity = new ToKhaiMauDich();
				
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity.Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                collection.Add(entity);                
			}
            reader.Close();
            return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public ToKhaiMauDichCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				ToKhaiMauDich entity = new ToKhaiMauDich();
				
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity.Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));

                collection.Add(entity);
			}
			return collection;			
		}
        public ToKhaiMauDichCollection GetDSTKNChuaThanhLy(string maDoanhNghiep, string maHaiQuan, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKNChuaThanhLy(maDoanhNghiep, maHaiQuan, maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKXChuaThanhLy(string maDoanhNghiep, string maHaiQuan)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLy(maDoanhNghiep, maHaiQuan);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKXChuaThanhLyMinDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLyMinDate(maDoanhNghiep, maHaiQuan, minDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKXChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLyByDate(maDoanhNghiep, maHaiQuan, minDate,maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKNChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKNChuaThanhLyByDate(maDoanhNghiep, maHaiQuan, minDate, maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            return collection;
        }
		#endregion
        
        //---------------------------------------------------------------------------------------------

        public DataSet SelectToKhai_HangMauDich_GroupByDoiTac(DateTime FromDate, DateTime ToDate, string NuocNK_ID)
        {
            string spName = "p_SXXK_SelectToKhai_HangMauDich_GroupByDoiTac";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@FromDate", SqlDbType.DateTime, FromDate);
            this.db.AddInParameter(dbCommand, "@ToDate", SqlDbType.DateTime, ToDate);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.NVarChar, NuocNK_ID);
            return this.db.ExecuteDataSet(dbCommand);
        }
        
        //---------------------------------------------------------------------------------------------

        public DataSet SelectToKhai_HangMauDich_By(DateTime FromDate, DateTime ToDate, string NuocNK_ID )
        {
            string spName = "p_SXXK_SelectToKhai_HangMauDich_By";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@FromDate", SqlDbType.DateTime, FromDate);
            this.db.AddInParameter(dbCommand, "@ToDate", SqlDbType.DateTime, ToDate);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.NVarChar, NuocNK_ID);
            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public DataSet SelectTenNuocToKhaiXK()
        {
            string spName = "p_SXXK_SelectTenNuocToKhaiXK";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

		//---------------------------------------------------------------------------------------------
        public DataSet SelectTriGiaHangToKhaiXK_ByDate(DateTime FromDate, DateTime ToDate)
        {
            string spName = "p_SXXK_SelectTriGiaHangToKhaiXK";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@FromDate", SqlDbType.DateTime, FromDate);
            this.db.AddInParameter(dbCommand, "@ToDate", SqlDbType.DateTime, ToDate);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public DataSet SelectTriGiaHangDPToKhaiXK_ByDate(DateTime FromDate, DateTime ToDate)
        {
            string spName = "p_SXXK_SelectTriGiaHangDPToKhaiXK";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@FromDate", SqlDbType.DateTime, FromDate);
            this.db.AddInParameter(dbCommand, "@ToDate", SqlDbType.DateTime, ToDate);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------


        public DataSet SelectTriGiaHang_NuocToKhaiXK_ByDate(DateTime FromDate, DateTime ToDate)
        {
            string spName = "p_SXXK_SelectTriGiaHang/NuocToKhaiXK";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@FromDate", SqlDbType.DateTime, FromDate);
            this.db.AddInParameter(dbCommand, "@ToDate", SqlDbType.DateTime, ToDate);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public DataSet SelectTriGiaHangDP_NuocToKhaiXK_ByDate(DateTime FromDate, DateTime ToDate)
        {
            string spName = "p_SXXK_SelectTriGiaHangDP/NuocToKhaiXK";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@FromDate", SqlDbType.DateTime, FromDate);
            this.db.AddInParameter(dbCommand, "@ToDate", SqlDbType.DateTime, ToDate);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ToKhaiMauDich_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
			this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
			this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
			this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
			this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
			this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
			this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
			this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
			this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
			this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
			this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
			this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
			this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
			this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
			this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
			this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
			this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
			this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
			this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
			this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
			this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
			this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
			this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
			this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
			this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
			this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
			this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
			this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
			this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
			this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
			this.db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
			this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
			this.db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
			this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            this.db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
			if (transaction != null)
			{
				return this.db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return this.db.ExecuteNonQuery(dbCommand);
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(ToKhaiMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ToKhaiMauDich item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, ToKhaiMauDichCollection collection)
        {
            foreach (ToKhaiMauDich item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.

        //**************** InsertUpdateFull() ******************
        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                  // this.InsertUpdateTransaction(transaction);
                    //this.UpdateTransaction(transaction);
                    BLL.SXXK.ToKhai.ToKhaiMauDich tkMD = new Company.KD.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkMD.MaDoanhNghiep = this.MaDoanhNghiep;
                    tkMD.PhanLuong = "";
                    tkMD.MaHaiQuan = Globals.trimMaHaiQuan(this.MaHaiQuan);
                    tkMD.MaLoaiHinh = this.MaLoaiHinh;
                    tkMD.NamDangKy = (short)this.NgayDangKy.Year;
                    //tkMD.NamDangKy = this.NamDangKy ;
                    tkMD.NgayDangKy = this.NgayDangKy;
                    tkMD.SoToKhai = this.SoToKhai;
                    tkMD.TenDoanhNghiep = this.TenDoanhNghiep;//
                    tkMD.TenDaiLyTTHQ = this.TenDaiLyTTHQ;//
                    tkMD.MaDaiLyTTHQ = this.MaDaiLyTTHQ; //
                    tkMD.TenDonViDoiTac = this.TenDonViDoiTac;
                    tkMD.ChiTietDonViDoiTac = this.ChiTietDonViDoiTac;//
                    tkMD.NgayGiayPhep = this.NgayGiayPhep;
                    tkMD.NgayHetHanGiayPhep = this.NgayHetHanGiayPhep;
                    tkMD.SoHopDong = this.SoHopDong;
                    tkMD.NgayHopDong = this.NgayHopDong;
                    tkMD.NgayHetHanHopDong = this.NgayHetHanHopDong;
                    tkMD.SoHoaDonThuongMai = this.SoHoaDonThuongMai;
                    tkMD.NgayHoaDonThuongMai = this.NgayHoaDonThuongMai;
                    tkMD.PTVT_ID = this.PTVT_ID;
                    tkMD.SoHieuPTVT = this.SoHieuPTVT;
                    tkMD.NgayDenPTVT = this.NgayDenPTVT;
                    tkMD.SoVanDon = this.SoVanDon; //
                    tkMD.LoaiVanDon = this.LoaiVanDon;
                    tkMD.NgayVanDon = this.NgayVanDon;
                    tkMD.NuocXK_ID = this.NuocXK_ID;
                    tkMD.NuocNK_ID = this.NuocNK_ID;
                    tkMD.DiaDiemXepHang = this.DiaDiemXepHang;
                    tkMD.DKGH_ID = this.DKGH_ID;
                    tkMD.CuaKhau_ID = this.CuaKhau_ID;
                    tkMD.NguyenTe_ID = this.NguyenTe_ID;
                    tkMD.TyGiaTinhThue = this.TyGiaTinhThue;
                    tkMD.TyGiaUSD = this.TyGiaUSD;
                    tkMD.PTTT_ID = this.PTTT_ID;
                    tkMD.SoHang = this.SoHang;
                    tkMD.SoLuongPLTK = this.SoLuongPLTK;
                    tkMD.TenChuHang = this.TenChuHang;
                    tkMD.SoContainer20 = this.SoContainer20;
                    tkMD.SoContainer40 = this.SoContainer40;
                    tkMD.SoKien = this.SoKien;
                    tkMD.TrongLuong = this.TrongLuong;
                    tkMD.TongTriGiaKhaiBao = this.TongTriGiaKhaiBao;
                    tkMD.TongTriGiaTinhThue = this.TongTriGiaTinhThue;
                    tkMD.LoaiToKhaiGiaCong = this.LoaiToKhaiGiaCong;
                    tkMD.LePhiHaiQuan = this.LePhiHaiQuan;
                    tkMD.PhiKhac = this.PhiKhac;//
                    tkMD.PhiBaoHiem = this.PhiBaoHiem;
                    tkMD.PhiVanChuyen = this.PhiVanChuyen;
                    tkMD.ThanhLy = "";
                    tkMD.Xuat_NPL_SP = this.Xuat_NPL_SP ;//edit
                    tkMD.ChungTu = this.ChungTu ;       // edit
                    tkMD.NGAY_THN_THX = this.NGAY_THN_THX;//
                    tkMD.TrangThaiThanhKhoan = "D";                   
                    tkMD.MaDonViUT = this.MaDonViUT;//
                    tkMD.NgayHoanThanh = this.NgayDangKy;//
                    tkMD.TrangThai = 1;
                    tkMD.InsertUpdateTransaction(transaction);
                    if (this.HMDCollection.Count == 0)
                        this.LoadHMDCollection();
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        BLL.SXXK.ToKhai.HangMauDich hmdTK = new Company.KD.BLL.SXXK.ToKhai.HangMauDich();
                        hmdTK.DonGiaKB = hmd.DonGiaKB;
                        hmdTK.DonGiaTT = hmd.DonGiaTT;
                        hmdTK.DVT_ID = hmd.DVT_ID;
                        hmdTK.MaHaiQuan = Globals.trimMaHaiQuan(this.MaHaiQuan);
                        hmdTK.MaHS = hmd.MaHS;
                        hmdTK.MaLoaiHinh = this.MaLoaiHinh;
                        hmdTK.MaPhu = hmd.MaPhu;
                        hmdTK.MienThue = hmd.MienThue;
                        hmdTK.NamDangKy = (short)this.NgayDangKy.Year;                       
                        hmdTK.NuocXX_ID = hmd.NuocXX_ID;
                        hmdTK.PhuThu = hmd.PhuThu;
                        hmdTK.SoLuong = hmd.SoLuong;
                        hmdTK.SoThuTuHang = (short)hmd.SoThuTuHang;
                        hmdTK.SoToKhai = this.SoToKhai;
                        hmdTK.TenHang = hmd.TenHang;
                        hmdTK.ThueGTGT = hmd.ThueGTGT;
                        hmdTK.ThueSuatGTGT = hmd.ThueSuatGTGT;
                        hmdTK.ThueSuatTTDB = hmd.ThueSuatTTDB;
                        hmdTK.ThueSuatXNK = hmd.ThueSuatXNK;
                        hmdTK.ThueTTDB = hmd.ThueTTDB;
                        hmdTK.ThueXNK = hmd.ThueXNK;
                        hmdTK.TriGiaKB = hmd.TriGiaKB;
                        hmdTK.TriGiaKB_VND = hmd.TriGiaKB_VND;
                        hmdTK.TriGiaThuKhac = hmd.TriGiaThuKhac;
                        hmdTK.TriGiaTT = hmd.TriGiaTT;
                        hmdTK.TyLeThuKhac = hmd.TyLeThuKhac;

                        hmdTK.InsertUpdateTransaction(transaction);                       
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        //***********************************
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ToKhaiMauDich_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
			this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
			this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
			this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
			this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
			this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
			this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
			this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
			this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
			this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
			this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
			this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
			this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
			this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
			this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
			this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
			this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
			this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
			this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
			this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
			this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
			this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
			this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
			this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
			this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
			this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
			this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
			this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
			this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
			this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
			this.db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
			this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
			this.db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
			this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            this.db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(ToKhaiMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ToKhaiMauDich item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ToKhaiMauDich_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
			this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
			this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
			this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
			this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
			this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
			this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
			this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
			this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
			this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
			this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
			this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
			this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
			this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
			this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
			this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
			this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
			this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
			this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
			this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
			this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
			this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
			this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
			this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
			this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
			this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
			this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
			this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
			this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
			this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
			this.db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
			this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
			this.db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
			this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            this.db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(ToKhaiMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiMauDich item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ToKhaiMauDich_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(ToKhaiMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiMauDich item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(ToKhaiMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ToKhaiMauDich item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion

        #region WS

        //public void DongBoDuLieuHaiQuan()
        //{
        //    WS.KDT.KDTService kdt = new Company.KD.BLL.WS.KDT.KDTService();
        //    string[] stXML = kdt.ToKhaiMauDich_Request(Globals.trimMaHaiQuan(this.MaHaiQuan), this.MaLoaiHinh, this.NamDangKy, this.SoToKhai);
        //    //load thong tin to khai
        //    DataSet ds = new DataSet();
        //    System.IO.StringReader xmlSR = new System.IO.StringReader(stXML[2]);
        //    ds.ReadXmlSchema(xmlSR);
        //    xmlSR = new System.IO.StringReader(stXML[0]);
        //    ds.ReadXml(xmlSR);
        //    DataTable dt = ds.Tables[0];
        //    //load thong tin hang hoa
        //    DataSet dsHang = new DataSet();
        //    xmlSR = new System.IO.StringReader(stXML[3]);
        //    dsHang.ReadXmlSchema(xmlSR);
        //    xmlSR = new System.IO.StringReader(stXML[1]);
        //    dsHang.ReadXml(xmlSR);
        //    DataTable dtHang = dsHang.Tables[0];
        //    //cap nhap thong tin to khai
        //    if (dt.Rows.Count > 0)
        //    {
        //        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //        using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //        {
        //            connection.Open();
        //            SqlTransaction transaction = connection.BeginTransaction();
        //            try
        //            {
        //                this.MaLoaiHinh = Convert.ToString(dt.Rows[0]["Ma_LH"]);
        //                this.MaDaiLyTTHQ = Convert.ToString(dt.Rows[0]["MA_DVKT"]);
        //                //this.TenDaiLyTTHQ = Convert.ToString(dt.Rows[0]["TenDLTTHQ"]);
        //                this.MaDonViUT = Convert.ToString(dt.Rows[0]["MA_DVUT"]);
        //                this.TenDonViDoiTac = Convert.ToString(dt.Rows[0]["DV_DT"]);
        //                this.SoGiayPhep = Convert.ToString(dt.Rows[0]["So_GP"]);
        //                this.SoToKhai = Convert.ToInt32(dt.Rows[0]["SOTK"]);
        //                this.NgayDangKy = Convert.ToDateTime(dt.Rows[0]["Ngay_DK"]);
        //                this.NamDangKy = (short)this.NgayDangKy.Year;
        //                try
        //                {
        //                    this.NGAY_THN_THX = Convert.ToDateTime(dt.Rows[0]["NGAY_THN_THX"]);
        //                }
        //                catch { this.NGAY_THN_THX = new DateTime(1900, 1, 1); }                          
        //                if (dt.Rows[0]["Ngay_GP"].ToString() != "")
        //                    this.NgayGiayPhep = Convert.ToDateTime(dt.Rows[0]["Ngay_GP"].ToString());
        //                if (dt.Rows[0]["Ngay_HHGP"].ToString() != "")
        //                    this.NgayHetHanGiayPhep = Convert.ToDateTime(dt.Rows[0]["Ngay_GP"].ToString());
        //                this.SoHopDong = Convert.ToString(dt.Rows[0]["So_HD"].ToString());
        //                if (dt.Rows[0]["Ngay_HD"].ToString() != "")
        //                    this.NgayHopDong = Convert.ToDateTime(dt.Rows[0]["Ngay_HD"].ToString());
        //                if (dt.Rows[0]["Ngay_HHHD"].ToString() != "")
        //                    this.NgayHetHanHopDong = Convert.ToDateTime(dt.Rows[0]["Ngay_HHHD"].ToString());
        //                this.SoHoaDonThuongMai = Convert.ToString(dt.Rows[0]["So_HDTM"]);
        //                if (dt.Rows[0]["Ngay_HDTM"].ToString() != "")
        //                    this.NgayHoaDonThuongMai = Convert.ToDateTime(dt.Rows[0]["Ngay_HDTM".ToString()]);
        //                this.PTVT_ID = Convert.ToString(dt.Rows[0]["Ma_PTVT"]);
        //                this.SoHieuPTVT = Convert.ToString(dt.Rows[0]["Ten_PTVT"]);
        //                if (dt.Rows[0]["NgayDen"].ToString() != "")
        //                    this.NgayDenPTVT = Convert.ToDateTime(dt.Rows[0]["NgayDen"].ToString());
        //                this.LoaiVanDon = Convert.ToString(dt.Rows[0]["Van_Don"]);
        //                if (dt.Rows[0]["Ngay_VanDon"].ToString() != "")
        //                    this.NgayVanDon = Convert.ToDateTime(dt.Rows[0]["Ngay_VanDon"].ToString());
        //                this.NuocXK_ID = Convert.ToString(dt.Rows[0]["Nuoc_XK"]);
        //                this.NuocNK_ID = Convert.ToString(dt.Rows[0]["Nuoc_NK"]);
        //                this.DiaDiemXepHang = Convert.ToString(dt.Rows[0]["CangNN"]);
        //                this.DKGH_ID = Convert.ToString(dt.Rows[0]["Ma_GH"]);
        //                this.CuaKhau_ID = Convert.ToString(dt.Rows[0]["Ma_CK"]);
        //                this.NguyenTe_ID = Convert.ToString(dt.Rows[0]["Ma_NT"]);
        //                this.TyGiaTinhThue = Convert.ToDecimal(dt.Rows[0]["TyGia_VND"].ToString());
        //                this.TyGiaUSD = Convert.ToDecimal(dt.Rows[0]["TyGia_USD"].ToString());
        //                this.PTTT_ID = Convert.ToString(dt.Rows[0]["Ma_PTTT"]);
        //                if (dt.Rows[0]["SoHang"].ToString() != "")
        //                    this.SoHang = Convert.ToInt16(dt.Rows[0]["SoHang"].ToString());
        //                if (dt.Rows[0]["So_PLTK"].ToString() != "")
        //                    this.SoLuongPLTK = Convert.ToInt16(dt.Rows[0]["So_PLTK"].ToString());
        //                this.TenChuHang = Convert.ToString(dt.Rows[0]["TenCH"]);
        //                if (dt.Rows[0]["So_Container"].ToString() != "")
        //                    this.SoContainer20 = Convert.ToDecimal(dt.Rows[0]["So_Container"].ToString());
        //                if (dt.Rows[0]["So_Container40"].ToString() != "")
        //                    this.SoContainer40 = Convert.ToDecimal(dt.Rows[0]["So_Container40"].ToString());
        //                if (dt.Rows[0]["So_Kien"].ToString() != "")
        //                    this.SoKien = Convert.ToDecimal(dt.Rows[0]["So_Kien"].ToString());
        //                if (dt.Rows[0]["Tr_Luong"].ToString() != "")
        //                    this.TrongLuong = Convert.ToDecimal(dt.Rows[0]["Tr_Luong"].ToString());
        //                if (dt.Rows[0]["TongTGKB"].ToString() != "")
        //                    this.TongTriGiaKhaiBao = Convert.ToDecimal(dt.Rows[0]["TongTGKB"].ToString());
        //                if (dt.Rows[0]["TongTGTT"].ToString() != "")
        //                    this.TongTriGiaTinhThue = Convert.ToDecimal(dt.Rows[0]["TongTGTT"]);
        //                //this.LoaiToKhaiGiaCong = Convert.ToString(dt.Rows[0]["LoaiTKGC"]);
        //                this.MaDoanhNghiep = Convert.ToString(dt.Rows[0]["Ma_DV"]);
        //                if (dt.Rows[0]["LePhi_HQ"].ToString() != "")
        //                    this.LePhiHaiQuan = Convert.ToDecimal(dt.Rows[0]["LePhi_HQ"].ToString());
        //                if (dt.Rows[0]["Phi_BH"].ToString() != "")
        //                    this.PhiBaoHiem = Convert.ToDecimal(dt.Rows[0]["Phi_BH"].ToString());
        //                if (dt.Rows[0]["Phi_VC"].ToString() != "")
        //                    this.PhiVanChuyen = Convert.ToDecimal(dt.Rows[0]["Phi_VC"].ToString());
        //                this.ThanhLy = dt.Rows[0]["THANH_LY"].ToString();
        //                this.Xuat_NPL_SP = dt.Rows[0]["XUAT_NPL_SP"].ToString();
        //                this.TrangThaiThanhKhoan = dt.Rows[0]["TTTK"].ToString();
        //                this.ChungTu = dt.Rows[0]["GIAYTO"].ToString();
        //                if(dt.Rows[0]["NGAY_HOANTHANH"].ToString()!="")
        //                    this.NgayHoanThanh = Convert.ToDateTime(dt.Rows[0]["NGAY_HOANTHANH"].ToString());
        //                this.PhanLuong = stXML[4];
        //                this.InsertUpdateTransaction(transaction);
        //                //cap nnhat hang mau dich
        //                short stt = 1;
        //                //xoa hang cu di
        //                HangMauDich hmdDelete = new HangMauDich();
        //                if (this.HMDCollection == null || this.HMDCollection.Count == 0)
        //                    this.LoadHMDCollection();
        //                hmdDelete.DeleteCollection(this.HMDCollection, transaction);
        //                //tao moi hang hoa cua to khai
        //                foreach (DataRow row in dtHang.Rows)
        //                {
        //                    HangMauDich hmd = new HangMauDich();
        //                    hmd.MaHaiQuan = Globals.trimMaHaiQuan(this.MaHaiQuan);
        //                    hmd.MaLoaiHinh = this.MaLoaiHinh;
        //                    hmd.NamDangKy = this.NamDangKy;
        //                    hmd.SoToKhai = this.SoToKhai;
        //                    hmd.MaPhu = row["MA_NPL_SP"].ToString();
        //                    hmd.TenHang = row["Ten_Hang"].ToString();
        //                    hmd.MaHS = row["Ma_Hang"].ToString();
        //                    if (hmd.MaHS.Length < 10)
        //                    {
        //                        for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
        //                            hmd.MaHS += "0";
        //                    }
        //                    hmd.DVT_ID = row["Ma_DVT"].ToString();
        //                    hmd.NuocXX_ID = row["Nuoc_XX"].ToString();
        //                    hmd.SoLuong = Convert.ToDecimal(row["Luong"].ToString());
        //                    if (row["DGia_KB"].ToString() != "")
        //                        hmd.DonGiaKB = Convert.ToDouble(row["DGia_KB"].ToString());
        //                    if (row["DGia_TT"].ToString() != "")
        //                        hmd.DonGiaTT = Convert.ToDouble(row["DGia_TT"].ToString());
        //                    hmd.TriGiaKB = Convert.ToDouble(row["TriGia_KB"].ToString());
        //                    hmd.TriGiaTT = Convert.ToDouble(row["TriGia_TT"].ToString());
        //                    hmd.TriGiaKB_VND = Convert.ToDouble(row["TGKB_VND"].ToString());
        //                    if(row["TS_XNK"].ToString()!="")
        //                        hmd.ThueSuatXNK = Convert.ToDouble(row["TS_XNK"].ToString());
        //                    if (row["TS_TTDB"].ToString() != "")
        //                        hmd.ThueSuatTTDB = Convert.ToDouble(row["TS_TTDB"].ToString());
        //                    if (row["TS_VAT"].ToString() != "")
        //                        hmd.ThueSuatGTGT = Convert.ToDouble(row["TS_VAT"].ToString());
        //                    if (row["Thue_XNK"].ToString() != "")
        //                        hmd.ThueXNK = Convert.ToDouble(row["Thue_XNK"].ToString());
        //                    if (row["Thue_TTDB"].ToString() != "")
        //                        hmd.ThueTTDB = Convert.ToDouble(row["Thue_TTDB"].ToString());
        //                    if (row["Thue_VAT"].ToString() != "")
        //                        hmd.ThueGTGT = Convert.ToDouble(row["Thue_VAT"].ToString());
        //                    if (row["Phu_Thu"].ToString() != "")
        //                        hmd.PhuThu = Convert.ToDouble(row["Phu_Thu"].ToString());
        //                    if (row["MienThue"].ToString() != "")
        //                        hmd.MienThue = Convert.ToDouble(row["MienThue"].ToString());
        //                    if (row["TyLe_ThuKhac"].ToString() != "")
        //                        hmd.TyLeThuKhac = Convert.ToDouble(row["TyLe_ThuKhac"].ToString());
        //                    if (row["TriGia_ThuKhac"].ToString() != "")
        //                        hmd.TriGiaThuKhac = Convert.ToDouble(row["TriGia_ThuKhac"].ToString());
        //                    hmd.SoThuTuHang = stt++;
        //                    hmd.InsertUpdateTransaction(transaction);
        //                }
        //                transaction.Commit();
        //            }
        //            catch (Exception ex)
        //           { 
        //                transaction.Rollback();
        //                throw new Exception(ex.Message);
        //            }
        //            finally
        //            {
        //                connection.Close();
        //            }
        //        }
        //    }
        //}

        public static long DongBoDuLieuHaiQuanAll(string mahaiquan, string madv,DataSet ds,DataSet dsHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            DataRow row2=null;
            //if (ds.Tables[0].Rows.Count > 0)
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        if (ds != null)
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            ToKhaiMauDich tkmd = new ToKhaiMauDich();
                            tkmd.MaLoaiHinh = Convert.ToString(row["Ma_LH"]);
                            tkmd.MaDaiLyTTHQ = Convert.ToString(row["MA_DVKT"]);
                            tkmd.MaHaiQuan = mahaiquan;
                            //tkmd.TenDaiLyTTHQ = Convert.ToString(row[0]["TenDLTTHQ"]);
                            tkmd.MaDonViUT = Convert.ToString(row["MA_DVUT"]);
                            tkmd.TenDonViDoiTac = Convert.ToString(row["DV_DT"]);
                            tkmd.SoGiayPhep = Convert.ToString(row["So_GP"]);
                            tkmd.SoToKhai = Convert.ToInt32(row["SOTK"]);
                            tkmd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                            tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;
                            try
                            {
                                tkmd.NGAY_THN_THX = Convert.ToDateTime(row["NGAY_THN_THX"]);
                            }
                            catch { tkmd.NGAY_THN_THX = new DateTime(1900, 1, 1); }
                            if (row["Ngay_GP"].ToString() != "")
                                tkmd.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                            try
                            {
                                if (row["Ngay_HHGP"].ToString() != "")
                                    tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                            }
                            catch(Exception exx) { }
                            tkmd.SoHopDong = Convert.ToString(row["So_HD"].ToString());
                            if (row["Ngay_HD"].ToString() != "")
                                tkmd.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                            if (row["Ngay_HHHD"].ToString() != "")
                                tkmd.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                            tkmd.SoHoaDonThuongMai = Convert.ToString(row["So_HDTM"]);
                            if (row["Ngay_HDTM"].ToString() != "")
                                tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM".ToString()]);
                            tkmd.PTVT_ID = Convert.ToString(row["Ma_PTVT"]);
                            tkmd.SoHieuPTVT = Convert.ToString(row["Ten_PTVT"]);
                            if (row["NgayDen"].ToString() != "")
                                tkmd.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                            tkmd.LoaiVanDon = Convert.ToString(row["Van_Don"]);
                            if (row["Ngay_VanDon"].ToString() != "")
                                tkmd.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                            tkmd.NuocXK_ID = Convert.ToString(row["Nuoc_XK"]);
                            tkmd.NuocNK_ID = Convert.ToString(row["Nuoc_NK"]);
                            tkmd.DiaDiemXepHang = Convert.ToString(row["CangNN"]);
                            tkmd.DKGH_ID = Convert.ToString(row["Ma_GH"]);
                            tkmd.CuaKhau_ID = Convert.ToString(row["Ma_CK"]);
                            tkmd.NguyenTe_ID = Convert.ToString(row["Ma_NT"]);
                            tkmd.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                            tkmd.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                            tkmd.PTTT_ID = Convert.ToString(row["Ma_PTTT"]);
                            if (row["SoHang"].ToString() != "")
                                tkmd.SoHang = Convert.ToInt16(row["SoHang"].ToString());
                            if (row["So_PLTK"].ToString() != "")
                                tkmd.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                            tkmd.TenChuHang = Convert.ToString(row["TenCH"]);
                            if (row["So_Container"].ToString() != "")
                                tkmd.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                            if (row["So_Container40"].ToString() != "")
                                tkmd.SoContainer40 = Convert.ToDecimal(row["So_Container40"].ToString());
                            if (row["So_Kien"].ToString() != "")
                                tkmd.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                            if (row["Tr_Luong"].ToString() != "")
                                tkmd.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                            if (row["TongTGKB"].ToString() != "")
                                tkmd.TongTriGiaKhaiBao = Convert.ToDecimal(row["TongTGKB"].ToString());
                            if (row["TongTGTT"].ToString() != "")
                                tkmd.TongTriGiaTinhThue = Convert.ToDecimal(row["TongTGTT"]);
                            //tkmd.LoaiToKhaiGiaCong = Convert.ToString(row[0]["LoaiTKGC"]);
                            tkmd.MaDoanhNghiep = Convert.ToString(row["Ma_DV"]);
                            if (row["LePhi_HQ"].ToString() != "")
                                tkmd.LePhiHaiQuan = Convert.ToDecimal(row["LePhi_HQ"].ToString());
                            if (row["Phi_BH"].ToString() != "")
                                tkmd.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                            if (row["Phi_VC"].ToString() != "")
                                tkmd.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                            tkmd.ThanhLy = row["THANH_LY"].ToString();
                            tkmd.Xuat_NPL_SP = row["XUAT_NPL_SP"].ToString();
                            tkmd.TrangThaiThanhKhoan = row["TTTK"].ToString();
                            tkmd.ChungTu = row["GIAYTO"].ToString();
                            if (row["NGAY_HOANTHANH"].ToString() != "")
                                tkmd.NgayHoanThanh = Convert.ToDateTime(row["NGAY_HOANTHANH"].ToString());                                                   
                            tkmd.InsertUpdateTransaction(transaction);                            
                        }
                        // xoa hang cu                        
                        //cap nnhat hang mau dich    
                        short stt = 1;
                        string mahq = "";
                        string maloaihinh = "";
                        short namdk = 0;
                        int sotk = 0;
                        if(dsHang!=null)
                        foreach (DataRow row in dsHang.Tables[0].Rows)
                        {
                            row2=row;
                            
                            HangMauDich hmd = new HangMauDich();
                            hmd.MaHaiQuan = row["MA_HQ"].ToString();
                            hmd.MaLoaiHinh = row["MA_LH"].ToString();
                            hmd.NamDangKy = (short)Convert.ToInt32(row["NAMDK"].ToString());
                            hmd.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                            if (hmd.SoToKhai == 1313)
                            {
                            }
                            //if (mahq != hmd.MaHaiQuan || maloaihinh != hmd.MaLoaiHinh || sotk != hmd.SoToKhai || namdk != hmd.NamDangKy)
                            //{
                            //    hmd.DeleteByForeigKeyTransaction(transaction);
                            //    stt = 1;
                            //    mahq = hmd.MaHaiQuan;
                            //    maloaihinh = hmd.MaLoaiHinh;
                            //    sotk = hmd.SoToKhai;
                            //    namdk = hmd.NamDangKy;
                            //}
                            hmd.MaPhu = row["MA_NPL_SP"].ToString();
                            hmd.TenHang = row["Ten_Hang"].ToString();
                            hmd.MaHS = row["MA_HANGKB"].ToString();
                            if (hmd.MaHS.Length < 10)
                            {
                                for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                                    hmd.MaHS += "0";
                            }
                            hmd.DVT_ID = row["Ma_DVT"].ToString();
                            hmd.NuocXX_ID = row["Nuoc_XX"].ToString();
                            hmd.SoLuong = Convert.ToDecimal(row["Luong"]);
                            try
                            {
                                hmd.DonGiaKB = Convert.ToDouble(row["DGia_KB"]);
                            }
                            catch (Exception exx) { }
                            try
                            {
                                hmd.DonGiaTT = Convert.ToDouble(row["DGia_TT"]);
                            }
                            catch (Exception exx) { }
                            hmd.TriGiaKB = Convert.ToDouble(row["TriGia_KB"]);
                            hmd.TriGiaTT = Convert.ToDouble(row["TriGia_TT"]);
                            hmd.TriGiaKB_VND = Convert.ToDouble(row["TGKB_VND"]);
                            if (row["TS_XNK"].ToString() != "")
                                hmd.ThueSuatXNK = Convert.ToDouble(row["TS_XNK"]);
                            if (row["TS_TTDB"].ToString() != "")
                                hmd.ThueSuatTTDB = Convert.ToDouble(row["TS_TTDB"]);
                            if (row["TS_VAT"].ToString() != "")
                                hmd.ThueSuatGTGT = Convert.ToDouble(row["TS_VAT"]);
                            if (row["Thue_XNK"].ToString() != "")
                                hmd.ThueXNK = Convert.ToDouble(row["Thue_XNK"]);
                            if (row["Thue_TTDB"].ToString() != "")
                                hmd.ThueTTDB = Convert.ToDouble(row["Thue_TTDB"]);
                            if (row["Thue_VAT"].ToString() != "")
                                hmd.ThueGTGT = Convert.ToDouble(row["Thue_VAT"]);
                            if (row["Phu_Thu"].ToString() != "")
                                hmd.PhuThu = Convert.ToDouble(row["Phu_Thu"]);
                            if (row["MienThue"].ToString() != "")
                                hmd.MienThue = Convert.ToDouble(row["MienThue"]);
                            if (row["TyLe_ThuKhac"].ToString() != "")
                                hmd.TyLeThuKhac = Convert.ToDouble(row["TyLe_ThuKhac"]);
                            if (row["TriGia_ThuKhac"].ToString() != "")
                                hmd.TriGiaThuKhac = Convert.ToDouble(row["TriGia_ThuKhac"]);
                            hmd.SoThuTuHang = Convert.ToInt16(row["STTHANG"]);
                            hmd.InsertUpdateTransaction(transaction);                            
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return 1;//;ds.Tables[0].Rows.Count;
        }
        #endregion
        public ToKhaiMauDichCollection SelectCollectionNhacNhoDynamic(string whereCondition, string orderByExpression)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));                
                collection.Add(entity);
            }
            return collection;
        }
        public static void getDanhSachNhacNho()
        {
            string where = "";
        }
        public void UpdateMaHang(string maMoi, string maCu,decimal soluong)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string sql = "UPDATE t_SXXK_HangMauDich SET MaPhu = '" + maMoi + "' , SoLuong=@SoLuong WHERE " +
                                 "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                                 "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + Globals.trimMaHaiQuan(this.MaHaiQuan) + "' AND MaPhu = '" + maCu +"'";
                    SqlCommand sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
                    db.ExecuteNonQuery(sqlCmd, transaction);

                    //sql = "UPDATE t_SXXK_HangMauDichDieuChinh SET Ma_NPL_SP = '" + maMoi + "', Luong=@SoLuong WHERE " +
                    //     "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                    //     "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + Globals.trimMaHaiQuan(this.MaHaiQuan) + "' AND Ma_NPL_SP = '" + maCu + "'";
                    //sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    //db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
                    //db.ExecuteNonQuery(sqlCmd, transaction);

                    //sql = "UPDATE t_SXXK_ThanhLy_NPLNhapTon SET MaNPL = '" + maMoi + "' , Luong=@SoLuong WHERE " +
                    //      "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                    //      "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + Globals.trimMaHaiQuan(this.MaHaiQuan) + "' AND MaNPL = '" + maCu + "'";
                    //sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    //db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
                    //db.ExecuteNonQuery(sqlCmd, transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void UpdateMaHang(string maMoi, string maCu, decimal soluong,SqlTransaction transaction )
        {           
                    string sql = "UPDATE t_SXXK_HangMauDich SET MaPhu = '" + maMoi + "' , SoLuong=@SoLuong WHERE " +
                                 "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                                 "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + Globals.trimMaHaiQuan(this.MaHaiQuan) + "' AND MaPhu = '" + maCu + "'";
                    SqlCommand sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
                    db.ExecuteNonQuery(sqlCmd, transaction);

                    //sql = "UPDATE t_SXXK_HangMauDichDieuChinh SET Ma_NPL_SP = '" + maMoi + "', Luong=@SoLuong WHERE " +
                    //     "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                    //     "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + Globals.trimMaHaiQuan(this.MaHaiQuan) + "' AND Ma_NPL_SP = '" + maCu + "'";
                    //sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    //db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
                    //db.ExecuteNonQuery(sqlCmd, transaction);

                    //sql = "UPDATE t_SXXK_ThanhLy_NPLNhapTon SET MaNPL = '" + maMoi + "' , Luong=@SoLuong WHERE " +
                    //      "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                    //      "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + Globals.trimMaHaiQuan(this.MaHaiQuan) + "' AND MaNPL = '" + maCu + "'";
                    //sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    //db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
                    //db.ExecuteNonQuery(sqlCmd, transaction);        
        }
        public bool InsertUpdateFullHang()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {                   
                    this.UpdateTransaction(transaction);
                    HangMauDich hangDelete = new HangMauDich();
                    hangDelete.SoToKhai = this.SoToKhai;
                    hangDelete.MaLoaiHinh = this.MaLoaiHinh;
                    hangDelete.MaHaiQuan = Globals.trimMaHaiQuan(this.MaHaiQuan);
                    hangDelete.NamDangKy = this.NamDangKy;
                    hangDelete.DeleteBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy(transaction);                    
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        if (hmd.MaPhu != hmd.MaTMP)
                            this.UpdateMaHang(hmd.MaPhu, hmd.MaTMP, hmd.SoLuong, transaction);                        
                        hmd.InsertTransaction(transaction);                                                                       
                    }
                    
                    
                    transaction.Commit();
                    
                }
                catch (Exception ex)
                {
                    transaction.Rollback();                    
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }
        public static long UpdateSTTHang(DataSet dsHang)
        {
            //HangMauDich hmdIndatabase = new HangMauDich();
            // DataSet dsHangInDatabase=hmdIndatabase.SelectAll();
            // SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            // using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            // {
            //     connection.Open();
            //     SqlTransaction transaction = connection.BeginTransaction();
            //     try
            //     {
            //         if (dsHang != null)
            //             foreach (DataRow row in dsHang.Tables[0].Rows)
            //             {
            //                 foreach (DataRow rowIndatabase in dsHangInDatabase.Tables[0].Rows)
            //                 {
            //                     if (row["MA_HQ"].ToString().Trim() == rowIndatabase["MaHaiQuan"].ToString().Trim() && row["MA_LH"].ToString().Trim() == rowIndatabase["MaLoaiHinh"].ToString().Trim()
            //                         && row["NAMDK"].ToString().Trim() == rowIndatabase["NamDangKy"].ToString().Trim() && row["SOTK"].ToString() == rowIndatabase["SoToKhai"].ToString() &&
            //                          row["MA_NPL_SP"].ToString().Trim().ToUpper() == rowIndatabase["MaPhu"].ToString().Trim().ToUpper())
            //                     {
            //                         HangMauDich hmd = new HangMauDich();
            //                         hmd.MaHaiQuan = row["MA_HQ"].ToString();
            //                         hmd.MaLoaiHinh = row["MA_LH"].ToString();
            //                         hmd.NamDangKy = (short)Convert.ToInt32(row["NAMDK"].ToString());
            //                         hmd.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
            //                         hmd.MaPhu = row["MA_NPL_SP"].ToString();
            //                         hmd.SoThuTuHang = Convert.ToInt16(row["STTHANG"]);
            //                         hmd.UpdateSTTHangTransaction(transaction);
            //                         break;
            //                     }
            //                 }
            //             }
            //         transaction.Commit();
            //     }
            //     catch (Exception ex)
            //     {
            //         transaction.Rollback();
            //         throw new Exception(ex.Message);
            //     }
            //     finally
            //     {
            //         connection.Close();
            //     }
            //}
            return 1;
        }

        public static long DongBoDuLieuHaiQuanToKhaiMoi(string mahaiquan, string madv, DataSet ds, DataSet dsHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            ToKhaiMauDich tkmdGet = new ToKhaiMauDich();
            DataSet dsToKhaiCu = tkmdGet.SelectDynamic("madoanhnghiep='" + madv + "' and mahaiquan='" + mahaiquan + "'", "");

            //if (ds.Tables[0].Rows.Count > 0)
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        if (ds != null)
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                                tkmd.MaLoaiHinh = Convert.ToString(row["Ma_LH"]);
                                tkmd.MaDaiLyTTHQ = Convert.ToString(row["MA_DVKT"]);
                                tkmd.MaHaiQuan = mahaiquan;
                                //tkmd.TenDaiLyTTHQ = Convert.ToString(row[0]["TenDLTTHQ"]);
                                tkmd.MaDonViUT = Convert.ToString(row["MA_DVUT"]);
                                tkmd.TenDonViDoiTac = Convert.ToString(row["DV_DT"]);
                                tkmd.SoGiayPhep = Convert.ToString(row["So_GP"]);
                                tkmd.SoToKhai = Convert.ToInt32(row["SOTK"]);
                                tkmd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                                tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;
                                tkmd.MaDoanhNghiep = Convert.ToString(row["Ma_DV"]);                               
                                try
                                {
                                    tkmd.NGAY_THN_THX = Convert.ToDateTime(row["NGAY_THN_THX"]);
                                }
                                catch { tkmd.NGAY_THN_THX = new DateTime(1900, 1, 1); }
                                if (row["Ngay_GP"].ToString() != "")
                                    tkmd.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                                try
                                {
                                    if (row["Ngay_HHGP"].ToString() != "")
                                        tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                                }
                                catch (Exception exx) { }
                                tkmd.SoHopDong = Convert.ToString(row["So_HD"].ToString());
                                if (row["Ngay_HD"].ToString() != "")
                                    tkmd.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                                if (row["Ngay_HHHD"].ToString() != "")
                                    tkmd.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                                tkmd.SoHoaDonThuongMai = Convert.ToString(row["So_HDTM"]);
                                if (row["Ngay_HDTM"].ToString() != "")
                                    tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM".ToString()]);
                                tkmd.PTVT_ID = Convert.ToString(row["Ma_PTVT"]);
                                tkmd.SoHieuPTVT = Convert.ToString(row["Ten_PTVT"]);
                                if (row["NgayDen"].ToString() != "")
                                    tkmd.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                                tkmd.LoaiVanDon = Convert.ToString(row["Van_Don"]);
                                if (row["Ngay_VanDon"].ToString() != "")
                                    tkmd.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                                tkmd.NuocXK_ID = Convert.ToString(row["Nuoc_XK"]);
                                tkmd.NuocNK_ID = Convert.ToString(row["Nuoc_NK"]);
                                tkmd.DiaDiemXepHang = Convert.ToString(row["CangNN"]);
                                tkmd.DKGH_ID = Convert.ToString(row["Ma_GH"]);
                                tkmd.CuaKhau_ID = Convert.ToString(row["Ma_CK"]);
                                tkmd.NguyenTe_ID = Convert.ToString(row["Ma_NT"]);
                                tkmd.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                                tkmd.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                                tkmd.PTTT_ID = Convert.ToString(row["Ma_PTTT"]);
                                if (row["SoHang"].ToString() != "")
                                    tkmd.SoHang = Convert.ToInt16(row["SoHang"].ToString());
                                if (row["So_PLTK"].ToString() != "")
                                    tkmd.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                                tkmd.TenChuHang = Convert.ToString(row["TenCH"]);
                                if (row["So_Container"].ToString() != "")
                                    tkmd.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                                if (row["So_Container40"].ToString() != "")
                                    tkmd.SoContainer40 = Convert.ToDecimal(row["So_Container40"].ToString());
                                if (row["So_Kien"].ToString() != "")
                                    tkmd.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                                if (row["Tr_Luong"].ToString() != "")
                                    tkmd.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                                if (row["TongTGKB"].ToString() != "")
                                    tkmd.TongTriGiaKhaiBao = Convert.ToDecimal(row["TongTGKB"].ToString());
                                if (row["TongTGTT"].ToString() != "")
                                    tkmd.TongTriGiaTinhThue = Convert.ToDecimal(row["TongTGTT"]);
                                //tkmd.LoaiToKhaiGiaCong = Convert.ToString(row[0]["LoaiTKGC"]);
                              
                                if (row["LePhi_HQ"].ToString() != "")
                                    tkmd.LePhiHaiQuan = Convert.ToDecimal(row["LePhi_HQ"].ToString());
                                if (row["Phi_BH"].ToString() != "")
                                    tkmd.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                                if (row["Phi_VC"].ToString() != "")
                                    tkmd.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                                tkmd.ThanhLy = row["THANH_LY"].ToString();
                                tkmd.Xuat_NPL_SP = row["XUAT_NPL_SP"].ToString();
                                tkmd.TrangThaiThanhKhoan = row["TTTK"].ToString();
                                tkmd.ChungTu = row["GIAYTO"].ToString();
                                if (row["NGAY_HOANTHANH"].ToString() != "")
                                    tkmd.NgayHoanThanh = Convert.ToDateTime(row["NGAY_HOANTHANH"].ToString());
                                try
                                {
                                    DataRow[] rowTTDM = dsToKhaiCu.Tables[0].Select("MaLoaiHinh='" + tkmd.MaLoaiHinh + "' and NamDangKy=" + tkmd.NamDangKy + " and MaHaiQuan='" + tkmd.MaHaiQuan + "' and  SoToKhai=" + tkmd.SoToKhai);
                                    if (rowTTDM == null || rowTTDM.Length == 0)
                                        tkmd.InsertTransaction(transaction);
                                }
                                catch { }
                            }
                        // xoa hang cu                        
                        //cap nnhat hang mau dich    
                        short stt = 1;
                        string mahq = "";
                        string maloaihinh = "";
                        short namdk = 0;
                        int sotk = 0;
                        if (dsHang != null)
                            foreach (DataRow row in dsHang.Tables[0].Rows)
                            {                                
                                HangMauDich hmd = new HangMauDich();
                                hmd.MaHaiQuan = row["MA_HQ"].ToString();
                                hmd.MaLoaiHinh = row["MA_LH"].ToString();
                                hmd.NamDangKy = (short)Convert.ToInt32(row["NAMDK"].ToString());
                                hmd.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());                             
                                hmd.MaPhu = row["MA_NPL_SP"].ToString();
                                hmd.TenHang = row["Ten_Hang"].ToString();
                                hmd.MaHS = row["MA_HANGKB"].ToString();
                                if (hmd.MaHS.Length < 10)
                                {
                                    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                                        hmd.MaHS += "0";
                                }
                                hmd.DVT_ID = row["Ma_DVT"].ToString();
                                hmd.NuocXX_ID = row["Nuoc_XX"].ToString();
                                hmd.SoLuong = Convert.ToDecimal(row["Luong"]);
                                try
                                {
                                    hmd.DonGiaKB = Convert.ToDouble(row["DGia_KB"]);
                                }
                                catch (Exception exx) { }
                                try
                                {
                                    hmd.DonGiaTT = Convert.ToDouble(row["DGia_TT"]);
                                }
                                catch (Exception exx) { }
                                hmd.TriGiaKB = Convert.ToDouble(row["TriGia_KB"]);
                                hmd.TriGiaTT = Convert.ToDouble(row["TriGia_TT"]);
                                hmd.TriGiaKB_VND = Convert.ToDouble(row["TGKB_VND"]);
                                if (row["TS_XNK"].ToString() != "")
                                    hmd.ThueSuatXNK = Convert.ToDouble(row["TS_XNK"]);
                                if (row["TS_TTDB"].ToString() != "")
                                    hmd.ThueSuatTTDB = Convert.ToDouble(row["TS_TTDB"]);
                                if (row["TS_VAT"].ToString() != "")
                                    hmd.ThueSuatGTGT = Convert.ToDouble(row["TS_VAT"]);
                                if (row["Thue_XNK"].ToString() != "")
                                    hmd.ThueXNK = Convert.ToDouble(row["Thue_XNK"]);
                                if (row["Thue_TTDB"].ToString() != "")
                                    hmd.ThueTTDB = Convert.ToDouble(row["Thue_TTDB"]);
                                if (row["Thue_VAT"].ToString() != "")
                                    hmd.ThueGTGT = Convert.ToDouble(row["Thue_VAT"]);
                                if (row["Phu_Thu"].ToString() != "")
                                    hmd.PhuThu = Convert.ToDouble(row["Phu_Thu"]);
                                if (row["MienThue"].ToString() != "")
                                    hmd.MienThue = Convert.ToDouble(row["MienThue"]);
                                if (row["TyLe_ThuKhac"].ToString() != "")
                                    hmd.TyLeThuKhac = Convert.ToDouble(row["TyLe_ThuKhac"]);
                                if (row["TriGia_ThuKhac"].ToString() != "")
                                    hmd.TriGiaThuKhac = Convert.ToDouble(row["TriGia_ThuKhac"]);
                                hmd.SoThuTuHang = Convert.ToInt16(row["STTHANG"]);
                                try
                                {
                                    DataRow[] rowTTDM = dsToKhaiCu.Tables[0].Select("MaLoaiHinh='" + hmd.MaLoaiHinh + "' and NamDangKy=" + hmd.NamDangKy + " and MaHaiQuan='" + hmd.MaHaiQuan + "' and  SoToKhai=" + hmd.SoToKhai);
                                    if (rowTTDM == null || rowTTDM.Length == 0)
                                    hmd.InsertTransaction(transaction);
                                }
                                catch { }
                            }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return 1;//;ds.Tables[0].Rows.Count;
        }
        public DataTable GetToKhaiSapHetHanTK(int hanThanhKhoan, int thoiGianThanhKhoan)
        {
            string sql = "SELECT a.SoToKhai, a.MaLoaiHinh,b.NgayDangKy, a.NamDangKy, a.MaHaiQuan, a.MaNPL, c.Ten as TenNPL, a.Luong, a.Ton , a.ThueXNK, (a.Ton / a.Luong)*a.ThueXNK as ThueTon ,"+
	                            "N'Còn ' + Cast(@HanThanhKhoan - DATEDIFF(day, NgayDangKy, GETDATE()) as nvarchar(10)) + N' ngày' as SoNgay "+
                        "FROM dbo.t_SXXK_ThanhLy_NPLNhapTon a INNER JOIN t_SXXK_ToKhaiMauDich b "+
                        "ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh "+
                        "AND a.NamDangKY = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan "+
                        "INNER JOIN t_SXXK_NguyenPhuLieu c ON a.MaNPL = c.Ma " +
                        "WHERE (@HanThanhKhoan - DATEDIFF(day, b.NgayDangKy, GETDATE())<= @ThoiGianTK) "+
	                            "AND (@HanThanhKhoan - DATEDIFF(day, b.NgayDangKy, GETDATE()) >= 0) "+
	                            "AND b.ThanhLy != 'H' AND a.MaLoaiHinh LIKE 'NSX%' ";

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HanThanhKhoan", SqlDbType.Int, hanThanhKhoan);
            db.AddInParameter(dbCommand, "@ThoiGianTK", SqlDbType.Int, thoiGianThanhKhoan);
            return this.db.ExecuteDataSet(dbCommand).Tables[0];  
        }
        public bool InsertUpdateFullHang(HangMauDich hmd,bool create)//true la tao moi
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {                 
                    {
                        if (hmd.MaPhu != hmd.MaTMP)
                            this.UpdateMaHang(hmd.MaPhu, hmd.MaTMP, hmd.SoLuong, transaction);
                        hmd.InsertUpdateTransaction(transaction);                       
                    }
                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }
	}	

}

 