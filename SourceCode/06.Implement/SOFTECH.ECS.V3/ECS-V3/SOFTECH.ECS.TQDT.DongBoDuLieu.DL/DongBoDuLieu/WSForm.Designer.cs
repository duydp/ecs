﻿namespace DongBoDuLieu
{
    partial class WSForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WSForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ckLuu = new Janus.Windows.EditControls.UICheckBox();
            this.txtMatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnConnect = new Janus.Windows.EditControls.UIButton();
            this.btnAction = new Janus.Windows.EditControls.UIButton();
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvmatKhau = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.ccError = new Company.Controls.CustomValidation.ContainerValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvmatKhau)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnAction);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnConnect);
            this.grbMain.Size = new System.Drawing.Size(335, 171);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ckLuu);
            this.uiGroupBox1.Controls.Add(this.txtMatKhau);
            this.uiGroupBox1.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(6, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(323, 118);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Đồng bộ";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ckLuu
            // 
            this.ckLuu.Location = new System.Drawing.Point(11, 76);
            this.ckLuu.Name = "ckLuu";
            this.ckLuu.Size = new System.Drawing.Size(306, 33);
            this.ckLuu.TabIndex = 4;
            this.ckLuu.Text = "Lưu thông tin cho lần thực hiện sau";
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMatKhau.Location = new System.Drawing.Point(128, 49);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.PasswordChar = '*';
            this.txtMatKhau.Size = new System.Drawing.Size(168, 21);
            this.txtMatKhau.TabIndex = 3;
            this.txtMatKhau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMatKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(128, 22);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(168, 21);
            this.txtMaDoanhNghiep.TabIndex = 1;
            this.txtMaDoanhNghiep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Đăng nhập";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mật khẩu";
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Icon = ((System.Drawing.Icon)(resources.GetObject("btnConnect.Icon")));
            this.btnConnect.Location = new System.Drawing.Point(64, 141);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(92, 24);
            this.btnConnect.TabIndex = 1;
            this.btnConnect.Text = "Thực hiện";
            this.btnConnect.VisualStyleManager = this.vsmMain;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnAction
            // 
            this.btnAction.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAction.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAction.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAction.Icon")));
            this.btnAction.Location = new System.Drawing.Point(162, 141);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(80, 24);
            this.btnAction.TabIndex = 2;
            this.btnAction.Text = "Đóng";
            this.btnAction.VisualStyleManager = this.vsmMain;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaDoanhNghiep;
            this.rfvMa.ErrorMessage = "\"Mã doanh nghiệp không được để trống\"";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvmatKhau
            // 
            this.rfvmatKhau.ControlToValidate = this.txtMatKhau;
            this.rfvmatKhau.ErrorMessage = "\"Chưa nhập mật khẩu\"";
            this.rfvmatKhau.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvmatKhau.Icon")));
            this.rfvmatKhau.Tag = "rfvmatKhau";
            // 
            // ccError
            // 
            this.ccError.ContainerToValidate = this;
            this.ccError.HostingForm = this;
            // 
            // WSForm
            // 
            this.AcceptButton = this.btnConnect;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 171);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(343, 205);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(343, 205);
            this.Name = "WSForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thực hiện giao dịch điện tử";
            this.Load += new System.EventHandler(this.WSForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvmatKhau)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIButton btnConnect;
        private Janus.Windows.EditControls.UIButton btnAction;
        public Janus.Windows.GridEX.EditControls.EditBox txtMatKhau;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvmatKhau;
        private Company.Controls.CustomValidation.ContainerValidator ccError;
        private Janus.Windows.EditControls.UICheckBox ckLuu;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;

    }
}