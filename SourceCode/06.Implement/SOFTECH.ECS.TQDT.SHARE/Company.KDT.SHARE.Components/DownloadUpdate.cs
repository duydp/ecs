﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Threading;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{


    public class DownloadUpdate
    {
        private ManualResetEvent evtDownload = null;
        private ManualResetEvent evtPerDonwload = null;
        private WebClient clientDownload = null;
        private const string dirTemp = "";
        private const string cfg = "Autoupdater.config";
        private string args = string.Empty;
        public DownloadUpdate(string args)
        {
            this.args = args;
        }
        public void DoDownload()
        {

            evtDownload = new ManualResetEvent(true);
            evtDownload.Reset();
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.ProcDownload));
        }
        private void StartUpdate(string arg)
        {
            if (File.Exists("AppAutoUpdate.exe"))
            {
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("AppAutoUpdate.exe",arg);
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                psi.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
                System.Diagnostics.Process.Start(psi);
            }
        }
        private void CreateNode(XmlDocument doc, XmlNode serverNode, XmlNode clientNode)
        {
            if (clientNode == null)
            {
                //serverNode = "aal";
            }
            else
            {
                //foreach (XmlAttribute item in serverNode.Attributes)
                //{
                //    clientNode.Attributes[item.Name]
                //}
            }
        }
        private string getPath(XmlDocument docVersionOld, XmlDocument docVersionNew, string FileName)
        {
            try
            {
                XmlNode nodeVersionOld = docVersionOld.SelectSingleNode("Config/UpdateFileList/LocalFile[@path=\"" + FileName + "\"]");
                XmlNode nodeVersionNew = docVersionNew.SelectSingleNode("UpdateFileList/RemoteFile[@path=\"" + FileName + "\"]");
                if (nodeVersionOld == null)
                {
                    CreateNode(docVersionOld, nodeVersionNew,nodeVersionOld);
                    return FileName;
                }
                else
                    if (string.Compare(nodeVersionOld.Attributes["lastver"].Value, nodeVersionNew.Attributes["lastver"].Value) != 0
                        || string.Compare(nodeVersionOld.Attributes["size"].Value, nodeVersionNew.Attributes["size"].Value) != 0)
                    {
                        CreateNode(docVersionOld, nodeVersionNew, nodeVersionOld);
                        return FileName;
                    }
                    else
                    {
                        return string.Empty;
                    }
            }
            catch
            {
                return FileName;
            }
        }
        private bool UnZip(string fileName)
        {

            if (!File.Exists("unzip.exe")) return false;
            try
            {

                string directoryUnzip = AppDomain.CurrentDomain.BaseDirectory;
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("unzip.exe",
                                                            "-o \"" + directoryUnzip + fileName + "\" -d \"" + directoryUnzip);
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                System.Diagnostics.Process p = System.Diagnostics.Process.Start(psi);
                p.WaitForExit();
                File.SetAttributes(fileName, FileAttributes.Archive);
                File.Delete(directoryUnzip + fileName);
                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return false; }
        }

        private void ProcDownload(object o)
        {
            try
            {
                if (!System.IO.File.Exists(cfg)) Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("Không tồn tại tệp tin {0}", cfg)));
                XmlDocument docVersionOld = new XmlDocument();
                XmlDocument docVersionNew = new XmlDocument();
                string serverLink = string.Empty;

                docVersionOld.Load(cfg);

                List<string> filesDownload = new List<string>();

                XmlNode nodeVersionOld = docVersionOld.SelectSingleNode("Config/ServerUrl");
                if (nodeVersionOld == null) return;

                serverLink = nodeVersionOld.InnerText;
                docVersionNew.Load(serverLink);

                XmlNode nodeVersionNew = docVersionNew.SelectSingleNode("UpdateFileList");
                if (nodeVersionNew == null) return;
                serverLink = nodeVersionNew.Attributes["url"].Value;


                nodeVersionOld = docVersionOld.SelectSingleNode("Config/UpdateFileList");

                if (nodeVersionOld == null)
                {

                    filesDownload.Add("AppAutoUpdate.exe.zip");
                    filesDownload.Add("ECS.AutoUpdater.dll.zip");
                    filesDownload.Add("Logger.dll.zip");
                }
                else
                {
                    string file = getPath(docVersionOld, docVersionNew, "AppAutoUpdate.exe.zip");
                    if (!string.IsNullOrEmpty(file))
                        filesDownload.Add(file);
                    file = getPath(docVersionOld, docVersionNew, "ECS.AutoUpdater.dll.zip");
                    if (!string.IsNullOrEmpty(file))
                        filesDownload.Add(file);
                    file = getPath(docVersionOld, docVersionNew, "Logger.dll.zip");
                    if (!string.IsNullOrEmpty(file))
                        filesDownload.Add(file);

                }

                if (!string.IsNullOrEmpty(dirTemp) && !Directory.Exists(dirTemp))
                    Directory.CreateDirectory(dirTemp);
                evtPerDonwload = new ManualResetEvent(false);
                while (!evtDownload.WaitOne(0, false))
                {
                    if (filesDownload.Count == 0) break;
                    string filename = filesDownload[0];
                    clientDownload = new WebClient();
                    clientDownload.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                    clientDownload.Proxy.Credentials = CredentialCache.DefaultCredentials;
                    clientDownload.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    //clientDownload.DownloadProgressChanged += (object sender, DownloadProgressChangedEventArgs e) =>
                    //{
                    //};
                    clientDownload.DownloadFileCompleted += (object sender, AsyncCompletedEventArgs e) =>
                    {
                        try
                        {
                            evtPerDonwload.Set();
                        }
                        catch { }
                    };
                    evtPerDonwload.Reset();
                    clientDownload.DownloadFileAsync(new Uri(serverLink + "/" + filename), filename, filename);
                    evtPerDonwload.WaitOne();
                    clientDownload.Dispose();
                    clientDownload = null;
                    UnZip(filename);
                    filesDownload.Remove(filename);
                }

                StartUpdate(args);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
