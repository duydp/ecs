﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.GC;
//using Company.BLL.VNACCS;

namespace Company.GC.BLL.VNACC
{
    public class DataVNACCSSync
    {
        public Company.KDT.SHARE.VNACCS.LogMessages.MsgPhanBo msgPB { get; set; }
        public Company.KDT.SHARE.VNACCS.LogMessages.MsgLog msgLog { get; set; }
        public NguyenPhuLieuCollection NplCollection { get; set; }
        public SanPhamCollection SPCollection { get; set; }
        public DinhMucCollection DMCollection { get; set; }
        public string exception { get; set; }
        public string SoToKhai;
        public string TrangThaiXuLy;
        public DateTime NgayDangKy;
        public DataVNACCSSync() { }
        public DataVNACCSSync(string SoToKhai, bool isDaiLy, DateTime ngayDangKy, long HopDong_ID)
        {
            try
            {
                GetDataLog(SoToKhai);
                if (isDaiLy)
                {
                    LoadNPL(SoToKhai, HopDong_ID);
                    LoadSanPham(SoToKhai, HopDong_ID);
                }
                else
                {
                    this.Load_NPL_SP_DM_DN(SoToKhai, HopDong_ID);
                }
                this.SoToKhai = SoToKhai;
                this.NgayDangKy = ngayDangKy;
                exception = null;
            }
            catch (System.Exception ex)
            {
                //Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        public DataVNACCSSync(string SoToKhai, bool isDaiLy, DateTime ngayDangKy, string trangThaiXuLy, long HopDong_ID)
        {
            try
            {
                GetDataLogCTQ(SoToKhai);
                if (isDaiLy)
                {
                    LoadNPL(SoToKhai, HopDong_ID);
                    LoadSanPham(SoToKhai, HopDong_ID);
                }
                else
                {
                    this.Load_NPL_SP_DM_DN(SoToKhai, HopDong_ID);
                }
                this.SoToKhai = SoToKhai;
                this.NgayDangKy = ngayDangKy;
                this.TrangThaiXuLy = trangThaiXuLy;
                exception = null;
            }
            catch (System.Exception ex)
            {
                //Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        public void LoadNPL(string SoToKhai, long HopDong_ID)
        {
            this.NplCollection = new Company.GC.BLL.GC.NguyenPhuLieu().SelectCollectionDynamic(@"Ma IN ( SELECT DISTINCT MaHangHoa FROM t_kdt_vnacc_hangmaudich
  WHERE TKMD_ID = (SELECT TOP 1 ID FROM t_kdt_vnacc_ToKhaiMauDich WHERE SoToKhai like '%" + SoToKhai + "%')) AND  HopDong_ID = " + HopDong_ID + "", null);
        }
        public void LoadSanPham(string SoToKhai, long HopDong_ID)
        {
            this.SPCollection = new Company.GC.BLL.GC.SanPham().SelectCollectionDynamic(@"Ma IN ( SELECT DISTINCT MaHangHoa FROM t_kdt_vnacc_hangmaudich
  WHERE TKMD_ID = (SELECT TOP 1 ID FROM t_kdt_vnacc_ToKhaiMauDich WHERE SoToKhai like '%" + SoToKhai + "%')) AND  HopDong_ID = " + HopDong_ID + "", null);
            if (this.SPCollection != null && this.SPCollection.Count > 0)
            {
                this.DMCollection = new Company.GC.BLL.GC.DinhMuc().SelectCollectionDynamic(@"MaSanPham IN ( SELECT DISTINCT MaHangHoa FROM t_kdt_vnacc_hangmaudich
  WHERE TKMD_ID = (SELECT TOP 1 ID FROM t_kdt_vnacc_ToKhaiMauDich WHERE SoToKhai like '%" + SoToKhai + "%')) AND  HopDong_ID = " + HopDong_ID + "", null);
            }
        }
        private void GetDataLog(string SoToKhai)
        {
            try
            {
                Company.KDT.SHARE.VNACCS.LogMessages.MsgPhanBo phanbo = Company.KDT.SHARE.VNACCS.LogMessages.MsgPhanBo.GetMessThongQuan(SoToKhai);
                if (phanbo != null)
                {
                    Company.KDT.SHARE.VNACCS.LogMessages.MsgLog log = Company.KDT.SHARE.VNACCS.LogMessages.MsgLog.Load(phanbo.Master_ID);
                    if (log != null)
                    {
                        this.msgPB = phanbo;
                        this.msgLog = log;
                        this.msgLog.ID = this.msgPB.ID = 0;
                    }
                    else
                        throw new Exception("Không tìm thấy log messages thông quan của tờ khai " + SoToKhai);
                }
                else
                    throw new Exception("Không tìm thấy log messages thông quan của tờ khai " + SoToKhai);
            }
            catch (System.Exception ex)
            {
                throw new Exception("Lỗi tìm kiếm Msg Thông quan tờ khai " + SoToKhai + ".(" + ex.Message + ")");
            }

        }
        private void GetDataLogCTQ(string SoToKhai)
        {
            try
            {
                Company.KDT.SHARE.VNACCS.LogMessages.MsgPhanBo phanbo = Company.KDT.SHARE.VNACCS.LogMessages.MsgPhanBo.GetMessChuaThongQuan(SoToKhai);
                if (phanbo != null)
                {
                    Company.KDT.SHARE.VNACCS.LogMessages.MsgLog log = Company.KDT.SHARE.VNACCS.LogMessages.MsgLog.Load(phanbo.Master_ID);
                    if (log != null)
                    {
                        this.msgPB = phanbo;
                        this.msgLog = log;
                        this.msgLog.ID = this.msgPB.ID = 0;
                    }
                    else
                        throw new Exception("Không tìm thấy log messages thông quan của tờ khai " + SoToKhai);
                }
                else
                    throw new Exception("Không tìm thấy log messages thông quan của tờ khai " + SoToKhai);
            }
            catch (System.Exception ex)
            {
                throw new Exception("Lỗi tìm kiếm Msg Thông quan tờ khai " + SoToKhai + ".(" + ex.Message + ")");
            }

        }

        public bool insertDataFullToServer(string MaDoanhNghiep,string UserName, int daily)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                DongBoDuLieu_TrackVNACCS track;
                List<DongBoDuLieu_TrackVNACCS> listtrack = DongBoDuLieu_TrackVNACCS.SelectCollectionDynamic("SoToKhai like '%" + this.SoToKhai + "%'", "NgayDongBo desc");
                if (listtrack != null && listtrack.Count > 0)
                {
                    track = listtrack[0];
                }
                else
                    track = new DongBoDuLieu_TrackVNACCS();

                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    track.SoToKhai = SoToKhai;
                    track.NgayDongBo = DateTime.Now;
                    track.NgayDangKy = NgayDangKy;
                    track.TrangThaiDongBoLen = 1;
                    track.TrangThaiDongBoXuong = 0;
                    track.MaDoanhNghiep = MaDoanhNghiep;
                    track.UserName = UserName;
                    track.IDUserDaiLy = daily;
                    if (this.NplCollection != null && this.NplCollection.Count > 0)
                    {
                        string npl = "(";
                        foreach (NguyenPhuLieu item in this.NplCollection)
                        {
                            npl += "'" + item.Ma + "'" + ",";
                        }
                        npl = npl.Substring(0, npl.Length - 1);
                        npl += ")";
                        track.ID_NPL = npl;
                    }
                    else
                        track.ID_NPL = null;
                    if (this.SPCollection != null && this.SPCollection.Count > 0)
                    {
                        string sp = "(";
                        foreach (Company.GC.BLL.GC.SanPham item in this.SPCollection)
                        {
                            sp += "'" + item.Ma + "'" + ",";
                        }
                        sp = sp.Substring(0, sp.Length - 1);
                        sp += ")";
                        track.ID_SanPham = sp;
                    }
                    else
                        track.ID_SanPham = null;
                    if (this.DMCollection != null && this.DMCollection.Count > 0)
                    {
                        string dm = "(";
                        foreach (Company.GC.BLL.GC.DinhMuc item in this.DMCollection)
                        {
                            dm += "'" + item.MaSanPham + "'" + ",";
                        }
                        dm = dm.Substring(0, dm.Length - 1);
                        dm += ")";
                        track.ID_DinhMuc = dm;
                    }
                    else
                        track.ID_DinhMuc = null;
                    Company.KDT.SHARE.VNACCS.LogMessages.MsgPhanBo phanbo = Company.KDT.SHARE.VNACCS.LogMessages.MsgPhanBo.GetMessThongQuan(SoToKhai);
                    Company.KDT.SHARE.VNACCS.LogMessages.MsgLog log = null;
                    if (phanbo != null)
                    {
                        phanbo.CreatedTime = this.msgPB.CreatedTime;
                        phanbo.GhiChu = this.msgPB.GhiChu;
                        phanbo.IndexTag = this.msgPB.IndexTag;
                        phanbo.MaNghiepVu = this.msgPB.MaNghiepVu;
                        phanbo.MessagesInputID = this.msgPB.MessagesInputID;
                        phanbo.messagesTag = this.msgPB.messagesTag;
                        phanbo.RTPTag = this.msgPB.RTPTag;
                        phanbo.SoTiepNhan = this.msgPB.SoTiepNhan;
                        phanbo.TerminalID = this.msgPB.TerminalID;
                        phanbo.GhiChu = this.msgPB.GhiChu;
                        phanbo.TrangThai = this.msgPB.TrangThai;

                        if (phanbo.Master_ID > 0)
                        {
                            log = Company.KDT.SHARE.VNACCS.LogMessages.MsgLog.Load(phanbo.Master_ID);
                            if (log != null)
                            {
                                log.CreatedTime = this.msgLog.CreatedTime;
                                log.InputMessagesID = this.msgLog.InputMessagesID;
                                log.MaNghiepVu = this.msgLog.MaNghiepVu;
                                log.MessagesTag = this.msgLog.MessagesTag;
                                log.NoiDungThongBao = this.msgLog.NoiDungThongBao;
                                log.TieuDeThongBao = this.msgLog.TieuDeThongBao;
                                log.IndexTag = this.msgLog.IndexTag;
                                log.Log_Messages = this.msgLog.Log_Messages;
                            }
                            log = this.msgLog;
                        }
                        else
                            log = this.msgLog;

                    }
                    else
                    {
                        phanbo = this.msgPB;
                        log = this.msgLog;
                    }

                    if (log.ID == 0)
                        phanbo.Master_ID = log.Insert(transaction);
                    else
                    {
                        log.InsertUpdate(transaction);
                        phanbo.Master_ID = log.ID;
                    }
                    phanbo.InsertUpdate(transaction);
                    track.InsertUpdate(transaction);
                    new NguyenPhuLieu().InsertUpdate(this.NplCollection);
                    new SanPham().InsertUpdate(this.SPCollection);
                    new DinhMuc().InsertUpdate(this.DMCollection);
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ret = false;
                    connection.Close();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        private void Load_NPL_SP_DM_DN(string SoToKhai, long HopDong_ID)
        {
            try
            {

                List<DongBoDuLieu_TrackVNACCS> listtrack = DongBoDuLieu_TrackVNACCS.SelectCollectionDynamic("SoToKhai like '%" + SoToKhai + "%'", "NgayDongBo desc");
                if (listtrack != null && listtrack.Count > 0)
                {
                    DongBoDuLieu_TrackVNACCS track = listtrack[0];
                    if (!string.IsNullOrEmpty(track.ID_SanPham))
                    {
                        this.SPCollection = new Company.GC.BLL.GC.SanPham().SelectCollectionDynamic("Ma in " + track.ID_SanPham + "AND HopDong_ID = " + HopDong_ID + "", null);
                    }
                    if (!string.IsNullOrEmpty(track.ID_DinhMuc))
                    {
                        this.DMCollection = new Company.GC.BLL.GC.DinhMuc().SelectCollectionDynamic("MaSanPham in " + track.ID_DinhMuc + "AND HopDong_ID = " + HopDong_ID + "", null);
                    }
                    if (!string.IsNullOrEmpty(track.ID_NPL))
                    {
                        this.NplCollection = new Company.GC.BLL.GC.NguyenPhuLieu().SelectCollectionDynamic("Ma in " + track.ID_NPL + "AND HopDong_ID = " + HopDong_ID + "", null);
                    }
                }
            }
            catch (System.Exception ex)
            {
                // Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        #region dự phòng
        //private bool insertDataFullToClient()
        //{
        //    bool ret;
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        try
        //        {
        //            new NguyenPhuLieu().InsertUpdate(this.NplCollection, transaction);
        //            new SanPham().InsertUpdate(this.SPCollection, transaction);
        //            new DinhMuc().InsertUpdate(this.DMCollection, transaction);

        //            new Company.KDT.SHARE.VNACCS.LogMessages.MsgLog().ProcessMSG(this.msgLog.Log_Messages, true, transaction);
        //            transaction.Commit();
        //            ret = true;

        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //            transaction.Rollback();
        //            ret = false;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }

        //    }
        //    return ret;
        //}
        #endregion  dự phòng
    }
}
