using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.DuLieuChuan
{
	public partial class KDT_ContainerBS : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_id { set; get; }
		public string SoVanDon { set; get; }
		public string SoContainer { set; get; }
		public string SoSeal { set; get; }
        public string CustomsSeal { set; get; }
		public string GhiChu { set; get; }
		public string Code { set; get; }
		public int KVGS { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_ContainerBS> ConvertToCollection(IDataReader reader)
		{
			List<KDT_ContainerBS> collection = new List<KDT_ContainerBS>();
			while (reader.Read())
			{
				KDT_ContainerBS entity = new KDT_ContainerBS();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_id"))) entity.Master_id = reader.GetInt64(reader.GetOrdinal("Master_id"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer"))) entity.SoContainer = reader.GetString(reader.GetOrdinal("SoContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal"))) entity.SoSeal = reader.GetString(reader.GetOrdinal("SoSeal"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomsSeal"))) entity.CustomsSeal = reader.GetString(reader.GetOrdinal("CustomsSeal"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("Code"))) entity.Code = reader.GetString(reader.GetOrdinal("Code"));
				if (!reader.IsDBNull(reader.GetOrdinal("KVGS"))) entity.KVGS = reader.GetInt32(reader.GetOrdinal("KVGS"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_ContainerBS> collection, long id)
        {
            foreach (KDT_ContainerBS item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_ContainerBS VALUES(@Master_id, @SoVanDon, @SoContainer, @SoSeal,@CustomsSeal, @GhiChu, @Code, @KVGS)";
            string update = "UPDATE t_KDT_ContainerBS SET Master_id = @Master_id, SoVanDon = @SoVanDon, SoContainer = @SoContainer, SoSeal = @SoSeal, CustomsSeal = @CustomsSeal, GhiChu = @GhiChu, Code = @Code, KVGS = @KVGS WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ContainerBS WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoContainer", SqlDbType.VarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal", SqlDbType.VarChar, "SoSeal", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@CustomsSeal", SqlDbType.VarChar, "CustomsSeal", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Code", SqlDbType.NVarChar, "Code", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KVGS", SqlDbType.Int, "KVGS", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoContainer", SqlDbType.VarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal", SqlDbType.VarChar, "SoSeal", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@CustomsSeal", SqlDbType.VarChar, "CustomsSeal", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Code", SqlDbType.NVarChar, "Code", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KVGS", SqlDbType.Int, "KVGS", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_ContainerBS VALUES(@Master_id, @SoVanDon, @SoContainer, @SoSeal,@CustomsSeal, @GhiChu, @Code, @KVGS)";
            string update = "UPDATE t_KDT_ContainerBS SET Master_id = @Master_id, SoVanDon = @SoVanDon, SoContainer = @SoContainer, SoSeal = @SoSeal,CustomsSeal=@CustomsSeal, GhiChu = @GhiChu, Code = @Code, KVGS = @KVGS WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ContainerBS WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoContainer", SqlDbType.VarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal", SqlDbType.VarChar, "SoSeal", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@CustomsSeal", SqlDbType.VarChar, "CustomsSeal", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Code", SqlDbType.NVarChar, "Code", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KVGS", SqlDbType.Int, "KVGS", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoContainer", SqlDbType.VarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal", SqlDbType.VarChar, "SoSeal", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@CustomsSeal", SqlDbType.VarChar, "CustomsSeal", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Code", SqlDbType.NVarChar, "Code", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KVGS", SqlDbType.Int, "KVGS", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_ContainerBS Load(long id)
		{
			const string spName = "[dbo].[p_KDT_ContainerBS_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_ContainerBS> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_ContainerBS> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_ContainerBS> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ContainerBS_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ContainerBS_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ContainerBS_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ContainerBS_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_ContainerBS(long master_id, string soVanDon, string soContainer, string soSeal,string customsSeal, string ghiChu, string code, int kVGS)
		{
			KDT_ContainerBS entity = new KDT_ContainerBS();	
			entity.Master_id = master_id;
			entity.SoVanDon = soVanDon;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
            entity.CustomsSeal = customsSeal;
			entity.GhiChu = ghiChu;
			entity.Code = code;
			entity.KVGS = kVGS;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_ContainerBS_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_id", SqlDbType.BigInt, Master_id);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.VarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.VarChar, SoSeal);
            db.AddInParameter(dbCommand, "@CustomsSeal", SqlDbType.VarChar, CustomsSeal);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@Code", SqlDbType.NVarChar, Code);
			db.AddInParameter(dbCommand, "@KVGS", SqlDbType.Int, KVGS);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_ContainerBS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_ContainerBS item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_ContainerBS(long id, long master_id, string soVanDon, string soContainer, string soSeal,string customsSeal, string ghiChu, string code, int kVGS)
		{
			KDT_ContainerBS entity = new KDT_ContainerBS();			
			entity.ID = id;
			entity.Master_id = master_id;
			entity.SoVanDon = soVanDon;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
            entity.CustomsSeal = customsSeal;
			entity.GhiChu = ghiChu;
			entity.Code = code;
			entity.KVGS = kVGS;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_ContainerBS_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_id", SqlDbType.BigInt, Master_id);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.VarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.VarChar, SoSeal);
            db.AddInParameter(dbCommand, "@CustomsSeal", SqlDbType.VarChar, CustomsSeal);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@Code", SqlDbType.NVarChar, Code);
			db.AddInParameter(dbCommand, "@KVGS", SqlDbType.Int, KVGS);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_ContainerBS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_ContainerBS item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_ContainerBS(long id, long master_id, string soVanDon, string soContainer, string soSeal,string customsSeal, string ghiChu, string code, int kVGS)
		{
			KDT_ContainerBS entity = new KDT_ContainerBS();			
			entity.ID = id;
			entity.Master_id = master_id;
			entity.SoVanDon = soVanDon;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
            entity.CustomsSeal = customsSeal;
			entity.GhiChu = ghiChu;
			entity.Code = code;
			entity.KVGS = kVGS;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ContainerBS_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_id", SqlDbType.BigInt, Master_id);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.VarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.VarChar, SoSeal);
            db.AddInParameter(dbCommand, "@CustomsSeal", SqlDbType.VarChar, CustomsSeal);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@Code", SqlDbType.NVarChar, Code);
			db.AddInParameter(dbCommand, "@KVGS", SqlDbType.Int, KVGS);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_ContainerBS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_ContainerBS item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_ContainerBS(long id)
		{
			KDT_ContainerBS entity = new KDT_ContainerBS();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ContainerBS_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_ContainerBS_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_ContainerBS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_ContainerBS item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}