﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;

namespace Company.KD.BLL.KDT
{
    public partial class KetQuaXuLy
    {
        public const string LoaiThongDiep_TuChoiTiepNhan = "Từ chối tiếp nhận";
        public const string LoaiThongDiep_KhaiBaoThanhCong = "Khai báo thành công";
        public const string LoaiThongDiep_ToKhaiDuocCapSo = "Tờ khai được cấp số";
        public const string LoaiThongDiep_ToKhaiDuocPhanLuong = "Tờ khai được phân luồng";
        public const string LoaiThongDiep_ToKhaiSuaDuocDuyet = "Tờ khai sửa được duyệt";
        public const string LoaiThongDiep_ToKhaiChuyenTT = "Tờ khai chuyển trạng thái";
        public const string LoaiThongDiep_ToKhaiNPLNhapTon = "Cập nhật Nguyên phụ liệu nhập tồn";
        public const string LoaiThongDiep_ToKhaiNPLTonThucte = "Cập nhật Nguyên phụ liệu tồn thực tế";
        public const string LoaiThongDiep_AnDinhThueToKhai = "Ấn định thuế tờ khai";

        public const string LoaiChungTu_ToKhai = "TK";

        public static List<KetQuaXuLy> SelectCollectionBy_ItemID(long itemID)
        {
            string whereCondition = string.Format("ItemID = {0}", itemID);
            string orderByExpression = "Ngay ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        public static List<KetQuaXuLy> SelectCollectionBy_ReferenceID(Guid referenceID)
        {
            string whereCondition = string.Format("ReferenceID = '{0}'", referenceID);
            string orderByExpression = "Ngay ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        public static string LayKetQuaXuLy(Guid referenceID, string loaiThongDiep)
        {
            IList<KetQuaXuLy> list = SelectCollectionBy_ReferenceID_LoaiThongDiep(referenceID, loaiThongDiep);
            string msg = string.Empty;

            foreach (var kqxl in list)
            {
                msg += kqxl.NoiDung + "\r\n";
            }
            return msg;
        }

        public static string LayKetQuaXuLy(long itemID, string loaiThongDiep)
        {
            IList<KetQuaXuLy> list = SelectCollectionBy_ReferenceID_LoaiThongDiep(itemID, loaiThongDiep);            
            string msg = string.Empty;

            foreach (var kqxl in list)
            {
                msg += kqxl.NoiDung + "\r\n";
            }
            return msg;
        }

        private static List<KetQuaXuLy> SelectCollectionBy_ReferenceID_LoaiThongDiep(Guid referenceID, string loaiThongDiep)
        {
            string whereCondition = string.Format("ReferenceID = '{0}' AND LoaiThongDiep = N'{1}'", referenceID, loaiThongDiep);
            string orderByExpression = "Ngay ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        private static List<KetQuaXuLy> SelectCollectionBy_ReferenceID_LoaiThongDiep(long itemID, string loaiThongDiep)
        {
            string whereCondition = string.Format("ItemID = {0} AND LoaiThongDiep = N'{1}' AND ID = (SELECT MAX(ID) FROM dbo.t_KDT_KetQuaXuLy)", itemID, loaiThongDiep);
            string orderByExpression = "Ngay ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_KDT_KetQuaXuLy_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, ReferenceID);
            db.AddInParameter(dbCommand, "@ItemID", SqlDbType.BigInt, ItemID);
            db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
            db.AddInParameter(dbCommand, "@LoaiThongDiep", SqlDbType.NVarChar, LoaiThongDiep);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
            db.AddInParameter(dbCommand, "@Ngay", SqlDbType.DateTime, Ngay.Year == 1753 ? DBNull.Value : (object)Ngay);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMD_ID(SqlTransaction transaction, long tKMD_ID, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_KetQuaXuLy_DeleteDynamic]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, "ItemID = " + tKMD_ID);

            return db.ExecuteNonQuery(dbCommand, transaction);
        }

    }
}
