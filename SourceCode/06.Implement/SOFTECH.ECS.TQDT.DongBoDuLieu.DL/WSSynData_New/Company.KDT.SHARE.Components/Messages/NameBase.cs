﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.GoodsItem;

namespace Company.KDT.SHARE.Components
{
    public class NameBase
    {
        /// <summary>
        /// Tên 
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }
        /// <summary>
        /// Mã  
        /// </summary>
        [XmlElement("identity")]
        public string Identity { get; set; }

        /// <summary>
        /// Trạng thái đại lý
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

        /// <summary>
        /// Địa chỉ
        /// </summary>
        [XmlElement("address")]
        public string Address { get; set; }

        ///// <summary>
        ///// Địa chỉ trụ sở chính (Loại)
        ///// </summary>
        //[XmlElement("addressType")]
        //public string AddressType { get; set; }

        ///// <summary>
        ///// Nước đầu tư
        ///// </summary>
        //[XmlElement("investmentCountry")]
        //public string InvestmentCountry { get; set; }

        ///// <summary>
        ///// Ngành nghề sản xuất
        ///// </summary>
        //[XmlElement("industryProduction")]
        //public string IndustryProduction { get; set; }

        ///// <summary>
        ///// Ngày kết thúc năm tài chính
        ///// </summary>
        //[XmlElement("dateFinanceYear")]
        //public string DateFinanceYear { get; set; }

        ///// <summary>
        ///// Loại hình doanh nghiệp
        ///// </summary>
        //[XmlElement("typeOfBusiness")]
        //public string TypeOfBusiness { get; set; }

        ///// <summary>
        ///// Chủ tịch Hội đồng quản trị (hoặc Chủ tịch Hội đồng thành viên)
        ///// </summary>
        //[XmlElement("ChairmanImporter")]
        //public ChairmanImporter ChairmanImporter { get; set; }

        ///// <summary>
        ///// Tổng giám đốc (hoặc Giám đốc):
        ///// </summary>
        //[XmlElement("GeneralDirector")]
        //public GeneralDirector GeneralDirector { get; set; }

        ///// <summary>
        ///// Thông tin các lần kiểm tra
        ///// </summary>
        //[XmlElement("ProductionInspectionHis")]
        //public ProductionInspectionHis ProductionInspectionHis { get; set; }


        ///// <summary>
        ///// Thông tin Doanh nghiệp XNK chi tiết
        ///// </summary>
        //[XmlElement("ImporterDetail")]
        //public NameBase ImporterDetail { get; set; }


        ///// <summary>
        ///// Thông tin Doanh nghiệp XNK chi tiết
        ///// </summary>
        //[XmlElement("StorageOfGood")]
        //public List<NameBase> StorageOfGood { get; set; }



        ///// <summary>
        ///// Thông tin các lần kiểm tra
        ///// </summary>
        //[XmlElement("ContentInspections")]
        //public ContentInspections ContentInspections { get; set; }

        #region Giay ket qua kiem tra
        /// <summary>
        /// ket qua 
        /// </summary>
        [XmlElement("result")]
        public string Result { get; set; }
        #endregion

      
    }
}
