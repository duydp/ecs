using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components.AnDinhThue
{
	public partial class ChiTiet
	{
		#region Properties.
		
		public int ID { set; get; }
		public int IDAnDinhThue { set; get; }
		public string SacThue { set; get; }
		public decimal TienThue { set; get; }
		public string Chuong { set; get; }
		public int Loai { set; get; }
		public int Khoan { set; get; }
		public int Muc { set; get; }
		public int TieuMuc { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ChiTiet Load(int id)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
			ChiTiet entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new ChiTiet();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDAnDinhThue"))) entity.IDAnDinhThue = reader.GetInt32(reader.GetOrdinal("IDAnDinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SacThue"))) entity.SacThue = reader.GetString(reader.GetOrdinal("SacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienThue"))) entity.TienThue = reader.GetDecimal(reader.GetOrdinal("TienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Chuong"))) entity.Chuong = reader.GetString(reader.GetOrdinal("Chuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Loai"))) entity.Loai = reader.GetInt32(reader.GetOrdinal("Loai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Khoan"))) entity.Khoan = reader.GetInt32(reader.GetOrdinal("Khoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Muc"))) entity.Muc = reader.GetInt32(reader.GetOrdinal("Muc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuMuc"))) entity.TieuMuc = reader.GetInt32(reader.GetOrdinal("TieuMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<ChiTiet> SelectCollectionAll()
		{
			List<ChiTiet> collection = new List<ChiTiet>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				ChiTiet entity = new ChiTiet();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDAnDinhThue"))) entity.IDAnDinhThue = reader.GetInt32(reader.GetOrdinal("IDAnDinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SacThue"))) entity.SacThue = reader.GetString(reader.GetOrdinal("SacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienThue"))) entity.TienThue = reader.GetDecimal(reader.GetOrdinal("TienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Chuong"))) entity.Chuong = reader.GetString(reader.GetOrdinal("Chuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Loai"))) entity.Loai = reader.GetInt32(reader.GetOrdinal("Loai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Khoan"))) entity.Khoan = reader.GetInt32(reader.GetOrdinal("Khoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Muc"))) entity.Muc = reader.GetInt32(reader.GetOrdinal("Muc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuMuc"))) entity.TieuMuc = reader.GetInt32(reader.GetOrdinal("TieuMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<ChiTiet> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<ChiTiet> collection = new List<ChiTiet>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				ChiTiet entity = new ChiTiet();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDAnDinhThue"))) entity.IDAnDinhThue = reader.GetInt32(reader.GetOrdinal("IDAnDinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SacThue"))) entity.SacThue = reader.GetString(reader.GetOrdinal("SacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienThue"))) entity.TienThue = reader.GetDecimal(reader.GetOrdinal("TienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Chuong"))) entity.Chuong = reader.GetString(reader.GetOrdinal("Chuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Loai"))) entity.Loai = reader.GetInt32(reader.GetOrdinal("Loai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Khoan"))) entity.Khoan = reader.GetInt32(reader.GetOrdinal("Khoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Muc"))) entity.Muc = reader.GetInt32(reader.GetOrdinal("Muc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuMuc"))) entity.TieuMuc = reader.GetInt32(reader.GetOrdinal("TieuMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<ChiTiet> SelectCollectionBy_IDAnDinhThue(int iDAnDinhThue)
		{
			List<ChiTiet> collection = new List<ChiTiet>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_IDAnDinhThue(iDAnDinhThue);
			while (reader.Read())
			{
				ChiTiet entity = new ChiTiet();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDAnDinhThue"))) entity.IDAnDinhThue = reader.GetInt32(reader.GetOrdinal("IDAnDinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SacThue"))) entity.SacThue = reader.GetString(reader.GetOrdinal("SacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienThue"))) entity.TienThue = reader.GetDecimal(reader.GetOrdinal("TienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Chuong"))) entity.Chuong = reader.GetString(reader.GetOrdinal("Chuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Loai"))) entity.Loai = reader.GetInt32(reader.GetOrdinal("Loai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Khoan"))) entity.Khoan = reader.GetInt32(reader.GetOrdinal("Khoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Muc"))) entity.Muc = reader.GetInt32(reader.GetOrdinal("Muc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuMuc"))) entity.TieuMuc = reader.GetInt32(reader.GetOrdinal("TieuMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_IDAnDinhThue(int iDAnDinhThue)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_SelectBy_IDAnDinhThue]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDAnDinhThue", SqlDbType.Int, iDAnDinhThue);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_IDAnDinhThue(int iDAnDinhThue)
		{
			const string spName = "p_KDT_AnDinhThue_ChiTiet_SelectBy_IDAnDinhThue";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDAnDinhThue", SqlDbType.Int, iDAnDinhThue);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertChiTiet(int iDAnDinhThue, string sacThue, decimal tienThue, string chuong, int loai, int khoan, int muc, int tieuMuc, string ghiChu)
		{
			ChiTiet entity = new ChiTiet();	
			entity.IDAnDinhThue = iDAnDinhThue;
			entity.SacThue = sacThue;
			entity.TienThue = tienThue;
			entity.Chuong = chuong;
			entity.Loai = loai;
			entity.Khoan = khoan;
			entity.Muc = muc;
			entity.TieuMuc = tieuMuc;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@IDAnDinhThue", SqlDbType.Int, IDAnDinhThue);
			db.AddInParameter(dbCommand, "@SacThue", SqlDbType.Char, SacThue);
			db.AddInParameter(dbCommand, "@TienThue", SqlDbType.Decimal, TienThue);
			db.AddInParameter(dbCommand, "@Chuong", SqlDbType.VarChar, Chuong);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.Int, Loai);
			db.AddInParameter(dbCommand, "@Khoan", SqlDbType.Int, Khoan);
			db.AddInParameter(dbCommand, "@Muc", SqlDbType.Int, Muc);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.Int, TieuMuc);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChiTiet item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateChiTiet(int id, int iDAnDinhThue, string sacThue, decimal tienThue, string chuong, int loai, int khoan, int muc, int tieuMuc, string ghiChu)
		{
			ChiTiet entity = new ChiTiet();			
			entity.ID = id;
			entity.IDAnDinhThue = iDAnDinhThue;
			entity.SacThue = sacThue;
			entity.TienThue = tienThue;
			entity.Chuong = chuong;
			entity.Loai = loai;
			entity.Khoan = khoan;
			entity.Muc = muc;
			entity.TieuMuc = tieuMuc;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_AnDinhThue_ChiTiet_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@IDAnDinhThue", SqlDbType.Int, IDAnDinhThue);
			db.AddInParameter(dbCommand, "@SacThue", SqlDbType.Char, SacThue);
			db.AddInParameter(dbCommand, "@TienThue", SqlDbType.Decimal, TienThue);
			db.AddInParameter(dbCommand, "@Chuong", SqlDbType.VarChar, Chuong);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.Int, Loai);
			db.AddInParameter(dbCommand, "@Khoan", SqlDbType.Int, Khoan);
			db.AddInParameter(dbCommand, "@Muc", SqlDbType.Int, Muc);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.Int, TieuMuc);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChiTiet item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateChiTiet(int id, int iDAnDinhThue, string sacThue, decimal tienThue, string chuong, int loai, int khoan, int muc, int tieuMuc, string ghiChu)
		{
			ChiTiet entity = new ChiTiet();			
			entity.ID = id;
			entity.IDAnDinhThue = iDAnDinhThue;
			entity.SacThue = sacThue;
			entity.TienThue = tienThue;
			entity.Chuong = chuong;
			entity.Loai = loai;
			entity.Khoan = khoan;
			entity.Muc = muc;
			entity.TieuMuc = tieuMuc;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@IDAnDinhThue", SqlDbType.Int, IDAnDinhThue);
			db.AddInParameter(dbCommand, "@SacThue", SqlDbType.Char, SacThue);
			db.AddInParameter(dbCommand, "@TienThue", SqlDbType.Decimal, TienThue);
			db.AddInParameter(dbCommand, "@Chuong", SqlDbType.VarChar, Chuong);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.Int, Loai);
			db.AddInParameter(dbCommand, "@Khoan", SqlDbType.Int, Khoan);
			db.AddInParameter(dbCommand, "@Muc", SqlDbType.Int, Muc);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.Int, TieuMuc);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChiTiet item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteChiTiet(int id)
		{
			ChiTiet entity = new ChiTiet();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_IDAnDinhThue(int iDAnDinhThue)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteBy_IDAnDinhThue]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDAnDinhThue", SqlDbType.Int, iDAnDinhThue);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChiTiet item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}