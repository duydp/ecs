using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;

namespace DongBoDuLieu.BLL.GC
{
    public class DM
    {
        public static DataSet GetDataSetDinhMuc(Company.GC.BLL.KDT.GC.HopDong HD,string matKhau)
        {
            
            StringBuilder queryDM = new StringBuilder();
            queryDM.Append("SELECT ");
            queryDM.Append("DM.So_HD			AS SoHopDong, ");
            queryDM.Append("DM.Ma_HQHD		AS MaHaiQuan, ");
            queryDM.Append("DM.DVGC			AS MaDoanhNghiep, ");
            queryDM.Append("DM.Ngay_Ky		AS NgayKy, ");
            queryDM.Append("DM.SPP_Code		AS MaSP, ");
            queryDM.Append("DM.NPLP_Code		AS MaNPL, ");
            queryDM.Append("DM.DMGC		AS DinhMuc, ");
            queryDM.Append("DM.TLHH		AS TyLeHaoHut ,");
            queryDM.Append("DM.Ghi_Chu		AS Ghi_Chu, ");
            queryDM.Append("DM.Mua_VN		AS NPL_TuCungUng, ");
            queryDM.Append("DM.Ten_NPL		AS TenNPL ");
            queryDM.Append("FROM DDMuc DM ");
            queryDM.Append("WHERE DM.Ma_HQHD = '" + HD.MaHaiQuan + "' AND DVGC = '" + HD.MaDoanhNghiep + "' ");
            queryDM.Append(" and So_HD='" + HD.SoHopDong + "'");
            DataSet dsDM = WebServiceDB.getDataBySQL(queryDM.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau); 
            return dsDM;

            //TODO: DinhMuc
            /*
            StringBuilder queryDM = new StringBuilder();
            queryDM.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryDM.Append("SELECT ");
            queryDM.Append("DM.So_HD			AS SoHopDong, ");
            queryDM.Append("DM.Ma_HQHD		AS MaHaiQuan, ");
            queryDM.Append("DM.DVGC			AS MaDoanhNghiep, ");
            queryDM.Append("DM.Ngay_Ky		AS NgayKy, ");
            queryDM.Append("DM.SPP_Code		AS MaSP, ");
            queryDM.Append("DM.NPLP_Code		AS MaNPL, ");
            queryDM.Append("DM.DMGC		AS DinhMuc, ");
            queryDM.Append("DM.TLHH		AS TyLeHaoHut ,");
            queryDM.Append("DM.Ghi_Chu		AS Ghi_Chu, ");
            queryDM.Append("DM.Mua_VN		AS NPL_TuCungUng, ");
            queryDM.Append("DM.Ten_NPL		AS TenNPL ");
            queryDM.Append("FROM DDMuc DM ");
            queryDM.Append("WHERE DM.Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' ");
            queryDM.Append(" and So_HD=''" + HD.SoHopDong + "''");
            queryDM.Append(" ') AS a;");
            DataSet dsDM = WebServiceDB.getDataBySQL(queryDM.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsDM;
            */
        }

        public static DataSet GetDataSetDinhMucOfSanPham(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau, params string[] MaSPCollection)
        {
            /*
            StringBuilder queryDM = new StringBuilder();
            queryDM.Append("SELECT ");
            queryDM.Append("DM.So_HD			AS SoHopDong, ");
            queryDM.Append("DM.Ma_HQHD		AS MaHaiQuan, ");
            queryDM.Append("DM.DVGC			AS MaDoanhNghiep, ");
            queryDM.Append("DM.Ngay_Ky		AS NgayKy, ");
            queryDM.Append("DM.SPP_Code		AS MaSP, ");
            queryDM.Append("DM.NPLP_Code		AS MaNPL, ");
            queryDM.Append("DM.DMGC		AS DinhMuc, ");
            queryDM.Append("DM.TLHH		AS TyLeHaoHut ,");
            queryDM.Append("DM.Ghi_Chu		AS Ghi_Chu, ");
            queryDM.Append("DM.Mua_VN		AS NPL_TuCungUng, ");
            queryDM.Append("DM.Ten_NPL		AS TenNPL ");
            queryDM.Append("FROM DDMuc DM ");
            queryDM.Append("WHERE DM.Ma_HQHD = '" + HD.MaHaiQuan + "' AND DVGC = '" + HD.MaDoanhNghiep + "' ");
            queryDM.Append(" and So_HD='" + HD.SoHopDong + "'");
            queryDM.Append(" and SPP_Code in ( ");
            foreach (string masp in MaSPCollection)
            {
                queryDM.Append("'"+"S"+masp+"',");
            }
            queryDM.Append("''");
            DataSet dsDM = WebServiceDB.getDataBySQL(queryDM.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau); 
            return dsDM;
            */

            StringBuilder queryDM = new StringBuilder();
            queryDM.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryDM.Append("SELECT ");
            queryDM.Append("DM.So_HD			AS SoHopDong, ");
            queryDM.Append("DM.Ma_HQHD		AS MaHaiQuan, ");
            queryDM.Append("DM.DVGC			AS MaDoanhNghiep, ");
            queryDM.Append("DM.Ngay_Ky		AS NgayKy, ");
            queryDM.Append("DM.SPP_Code		AS MaSP, ");
            queryDM.Append("DM.NPLP_Code		AS MaNPL, ");
            queryDM.Append("DM.DMGC		AS DinhMuc, ");
            queryDM.Append("DM.TLHH		AS TyLeHaoHut ,");
            queryDM.Append("DM.Ghi_Chu		AS Ghi_Chu, ");
            queryDM.Append("DM.Mua_VN		AS NPL_TuCungUng, ");
            queryDM.Append("DM.Ten_NPL		AS TenNPL ");
            queryDM.Append("FROM DDMuc DM ");
            queryDM.Append("WHERE DM.Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' ");
            queryDM.Append(" and So_HD=''" + HD.SoHopDong + "''");
            queryDM.Append(" and SPP_Code in ( ");
            foreach (string masp in MaSPCollection)
            {
                queryDM.Append("''" + "S" + masp + "'',");
            }
            queryDM.Append("''''");
            queryDM.Append(" ') AS a;");
            DataSet dsDM = WebServiceDB.getDataBySQL(queryDM.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsDM;
        }

        public static void InsertGhiDeDuLieu(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM,string database)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {                    
                    foreach (DataRow row in dsDM.Tables[0].Rows)
                    {
                        Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                        DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMuc"]);
                        DM.HopDong_ID = HD.ID;
                        DM.MaNguyenPhuLieu = row["MaNPL"].ToString().Substring(1);
                        DM.MaSanPham = row["MaSP"].ToString().Substring(1);
                        DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                        DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);

                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.HopDong_ID = HD.ID;
                        npl.Ma = DM.MaNguyenPhuLieu;
                        npl.Load();
                        DM.TenNPL = npl.Ten;

                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = DM.MaSanPham;
                        sp.LoadKTX();
                        DM.TenSanPham = sp.Ten;

                        DM.InsertTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static void InsertDuLieuMoi(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string masp = "";
                    bool ok = true;
                    foreach (DataRow row in dsDM.Tables[0].Rows)
                    {
                        Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                        DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMuc"]);
                        DM.HopDong_ID = HD.ID;
                        DM.MaNguyenPhuLieu = row["MaNPL"].ToString().Substring(1);
                        DM.MaSanPham = row["MaSP"].ToString().Substring(1);
                        if (DM.MaSanPham.Trim().ToUpper() != masp)
                        {
                            masp = DM.MaSanPham;
                            if (DM.CheckExitsDinhMucSanPham())
                            {
                                ok = false;
                            }
                        }
                        else
                        {
                            if (!ok)
                                continue;
                        }
                        DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                        DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);                       
                        if(ok)
                            DM.InsertTransaction(transaction);                        
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public static DataSet GetDMAtTMP(long Id_HopDong)
        {
            string sql = "select * from t_GC_DinhMuc where HopDong_ID=" + Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }

        public static DataSet GetDMDKAtTMP_TQDT(long Id_HopDong)
        {
            string sql = "select * from dbo.t_KDT_GC_DinhMucDangKy where ID_HopDong=" + Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }

        public static DataSet GetDMAtDN(long Id_HopDong)
        {
            string sql = "select * from t_GC_DinhMuc where HopDong_ID=" + Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }
    }
}
