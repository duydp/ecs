﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Company.KD.BLL.KDT;
using System.Text;
using System;
using System.Security.Cryptography;
using Company.Controls;
using System.Drawing;
using System.IO;
namespace Company.Interface.PhongKhai
{
    public class LoginForm : Form
    {
        private IContainer components = null;

        public int intLogin = 0;
        private DataRowCollection dtb;
        public int intcount = 0;
        public bool config;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton btnLogin;
        private Janus.Windows.GridEX.EditControls.EditBox txtMatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private Janus.Windows.Common.VisualStyleManager vsmMain;
        private HeThongPhongKhai hqch = new HeThongPhongKhai();

        public LoginForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.vsmMain = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.btnLogin = new Janus.Windows.EditControls.UIButton();
            this.txtMatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.SuspendLayout();
            // 
            // uiButton1
            // 
            this.uiButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Location = new System.Drawing.Point(331, 223);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(116, 24);
            this.uiButton1.TabIndex = 38;
            this.uiButton1.Text = "Thoát";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            this.uiButton1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaDoanhNghiep_KeyDown);
            // 
            // vsmMain
            // 
            janusColorScheme1.ActiveCaptionColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(108)))), ((int)(((byte)(0)))));
            janusColorScheme1.ActiveCaptionTextColor = System.Drawing.Color.White;
            janusColorScheme1.ControlColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(164)))), ((int)(((byte)(0)))));
            janusColorScheme1.ControlDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(108)))), ((int)(((byte)(0)))));
            janusColorScheme1.ControlTextColor = System.Drawing.Color.Black;
            janusColorScheme1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(164)))), ((int)(((byte)(0)))));
            janusColorScheme1.GrayTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(108)))), ((int)(((byte)(0)))));
            janusColorScheme1.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(108)))), ((int)(((byte)(0)))));
            janusColorScheme1.HighlightTextColor = System.Drawing.Color.White;
            janusColorScheme1.InfoColor = System.Drawing.Color.White;
            janusColorScheme1.InfoTextColor = System.Drawing.Color.Black;
            janusColorScheme1.MenuColor = System.Drawing.Color.White;
            janusColorScheme1.MenuTextColor = System.Drawing.Color.Black;
            janusColorScheme1.Name = "Scheme0";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme1.UseThemes = false;
            janusColorScheme1.VisualStyle = Janus.Windows.Common.VisualStyle.VS2005;
            janusColorScheme1.WindowColor = System.Drawing.Color.White;
            janusColorScheme1.WindowTextColor = System.Drawing.Color.Black;
            this.vsmMain.ColorSchemes.Add(janusColorScheme1);
            this.vsmMain.DefaultColorScheme = "Scheme0";
            // 
            // btnLogin
            // 
            this.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogin.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Location = new System.Drawing.Point(209, 223);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(116, 24);
            this.btnLogin.TabIndex = 37;
            this.btnLogin.Text = "Đăng nhập";
            this.btnLogin.VisualStyleManager = this.vsmMain;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            this.btnLogin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaDoanhNghiep_KeyDown);
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.txtMatKhau.ButtonFont = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtMatKhau.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtMatKhau.Location = new System.Drawing.Point(184, 186);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.PasswordChar = '*';
            this.txtMatKhau.Size = new System.Drawing.Size(290, 22);
            this.txtMatKhau.TabIndex = 36;
            this.txtMatKhau.TextChanged += new System.EventHandler(this.txtMatKhau_TextChanged);
            this.txtMatKhau.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaDoanhNghiep_KeyDown);
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.txtMaDoanhNghiep.ButtonFont = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtMaDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(184, 124);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(290, 22);
            this.txtMaDoanhNghiep.TabIndex = 35;
            this.txtMaDoanhNghiep.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaDoanhNghiep_KeyDown);
            // 
            // LoginForm
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CancelButton = this.uiButton1;
            this.ClientSize = new System.Drawing.Size(500, 300);
            this.Controls.Add(this.uiButton1);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtMatKhau);
            this.Controls.Add(this.txtMaDoanhNghiep);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng nhập hệ thống";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LoginForm_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaDoanhNghiep_KeyDown);
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private void Login(string strMa, string strPass)
        {

            try
            {              
                dtb = hqch.SelectLogin().Tables[0].Rows;
                string strMaRow = "";
                string strPassRow = "";
                string strValueConfig = "";
                string strKeyConfig = "";
                int intRole = 0;

                foreach (DataRow dr in dtb)
                {
                    strMaRow = dr["MadoanhNghiep"].ToString().Trim();
                    strPassRow = dr["PassWord"].ToString().Trim();
                    strValueConfig = dr["Value_Config"].ToString().Trim();
                    strKeyConfig = dr["Key_Config"].ToString().Trim();
                    intRole = Convert.ToInt16(dr["Role"].ToString().Trim());
                    if (strMa == strMaRow && intRole == 0 && strValueConfig == "1" && strKeyConfig == "CauHinh")
                    {
                        config = true;
                    }
                    else if (strMa == strMaRow && intRole == 0 && strValueConfig == "0" && strKeyConfig == "CauHinh")
                    {
                        config = false;
                    }

                    if (strMa == strMaRow && strPass == strPassRow && intRole == 1)
                    {
                        intLogin = 1;
                        return;
                    }
                    else if (strMa == strMaRow && strPass == strPassRow && intRole == 0)
                    {
                        intLogin = 2;

                        return;
                    }
                    else
                    {
                        intLogin = 3;
                    }


                }
            }
            catch
            {

                intLogin = 4;
                return;
            }

        }
        private void ShowMainForm()
        {
            MainForm mainForm = new MainForm();
            mainForm.ShowDialog();

        }

        private void lblDangky_Click(object sender, System.EventArgs e)
        {
            this.Hide();
            this.ShowAdminForm();
            // this.Close();

        }

        private void ShowAdminForm()
        {

            AdminAccountForm adminAccountForm = new AdminAccountForm();
            adminAccountForm.ShowDialog();
        }

        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }

        private void grbMain_Click(object sender, System.EventArgs e)
        {

        }



        private void LoginForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.BackgroundImage = System.Drawing.Image.FromFile("LOGIN-KINHDOANH.PNG");
                this.BackgroundImageLayout = ImageLayout.Stretch;
            }
            catch (OutOfMemoryException ex1)
            {

            }
            catch (FileNotFoundException ex2)
            {

            }
            catch (ArgumentException ex3)
            {

            }
            catch (Exception ex)
            { 
            
            }
            //lblCucHaiQuan.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
            //lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN.ToUpper();
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.Close();
        }

        private void lblCauHinhCSDL_Click(object sender, EventArgs e)
        {
            this.Hide();
            CauHinhCSDLForm cauHinhCSDLForm = new CauHinhCSDLForm();
            cauHinhCSDLForm.ShowDialog();
            //this.Close();
        }
        private void showFormConfig()
        {
            this.Hide();
            DangKyForm dangKyForm = new DangKyForm();
            dangKyForm.ShowDialog();
        }
        protected MessageBoxControl _MsgBox;
        private void showFormCreatAccount()
        {
            this.Hide();
            CreatAccountForm creatAccountForm = new CreatAccountForm();
            creatAccountForm.ShowDialog();
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog();
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void Login()
        {
            config = false;
            //txtMaDoanhNghiep.Text = txtMaDoanhNghiep.Text.ToLower();
            //txtMatKhau.Text = txtMatKhau.Text.ToLower();
            string strMa = txtMaDoanhNghiep.Text.Trim();
            string strPass = this.GetMD5Value(txtMatKhau.Text.Trim());
            if (strMa == "")
            {
                MessageBox.Show("Chưa nhập Mã doanh nghiệp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtMaDoanhNghiep.Focus();
                return;
            }
            if (txtMatKhau.Text.Trim() == "")
            {
                MessageBox.Show("Chưa nhập mật khẩu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtMatKhau.Focus();
                return;
            }
            //cvError.Validate();
            //if (!cvError.IsValid) return;           
            this.Login(strMa, strPass);
            switch (intLogin)
            {
                case 1:
                        MainForm.isLoginSuccess = true;
                        MainForm.typeLogin = 3;
                        this.Close();

                    break;
                case 2:
                    {

                        try
                        {
                            //Thông tin Doanh Nhiệp
                            Properties.Settings.Default.MA_DON_VI = strMa;
                            if (config)
                            {
                                Properties.Settings.Default.TEN_DON_VI = hqch.SelectedSettingsName(strMa, "CauHinh");
                                Properties.Settings.Default.DIA_CHI = hqch.SelectedSettings(strMa, "DIA_CHI");

                                //Thông tin mặc định chung
                                //string temp = hqch.SelectedSettings(strMa, "TY_GIA_USD");
                                Properties.Settings.Default.TY_GIA_USD = 16000;
                                Properties.Settings.Default.NUOC = hqch.SelectedSettings(strMa, "NUOC");
                                Properties.Settings.Default.CL_THN_THX = hqch.SelectedSettings(strMa, "CL_THN_THX");
                                Properties.Settings.Default.CUA_KHAU = hqch.SelectedSettings(strMa, "CUA_KHAU");
                                Properties.Settings.Default.TEN_DOI_TAC = hqch.SelectedSettings(strMa, "TEN_DOI_TAC");
                                Properties.Settings.Default.DKGH_MAC_DINH = hqch.SelectedSettings(strMa, "DKGH_MAC_DINH");
                                Properties.Settings.Default.DVT_MAC_DINH = hqch.SelectedSettings(strMa, "DVT_MAC_DINH").PadRight(3);
                                Properties.Settings.Default.PTTT_MAC_DINH = hqch.SelectedSettings(strMa, "PTTT_MAC_DINH");
                                Properties.Settings.Default.PTVT_MAC_DINH = hqch.SelectedSettings(strMa, "PTVT_MAC_DINH");
                                Properties.Settings.Default.NGUYEN_TE_MAC_DINH = hqch.SelectedSettings(strMa, "NGUYEN_TE_MAC_DINH");
                                Properties.Settings.Default.LOAI_HINH = hqch.SelectedSettings(strMa, "LOAI_HINH");
                                Properties.Settings.Default.NHOM_LOAI_HINH = hqch.SelectedSettings(strMa, "NHOM_LOAI_HINH");
                                Properties.Settings.Default.NHOM_LOAI_HINH_KHAC_NHAP = hqch.SelectedSettings(strMa, "NHOM_LOAI_HINH_KHAC_NHAP");

                                Properties.Settings.Default.NgaySaoLuu = hqch.SelectedSettings(strMa, "NgaySaoLuu");
                                Properties.Settings.Default.NHAC_NHO_SAO_LUU = hqch.SelectedSettings(strMa, "NHAC_NHO_SAO_LUU");
                                Properties.Settings.Default.MaMID = hqch.SelectedSettings(strMa, "MaMID");
                                Properties.Settings.Default.TuDongTinhThue = hqch.SelectedSettings(strMa, "TuDongTinhThue");
                                Properties.Settings.Default.Save();
                                GlobalSettings.RefreshKey();
                                MainForm.isLoginSuccess = true;
                                MainForm.typeLogin = 1;
                                this.Close();
                            }
                            else
                            {
                                GlobalSettings.TEN_DON_VI = Properties.Settings.Default.TEN_DON_VI = hqch.SelectedSettingsName(strMa, "CauHinh");
                                GlobalSettings.DIA_CHI = "";
                                GlobalSettings.MaMID = "";
                                MainForm.isLoginSuccess = true;
                                MainForm.typeLogin = 2;
                                this.Close();
                            }
                        }
                        catch (Exception ex1)
                        {
                            ShowMessage("Lỗi :" + ex1.Message.ToString(), false);
                        }
                        break;
                    }
                case 3:
                    intcount++;
                    MainForm.isLoginSuccess = false;
                    MessageBox.Show("Đăng nhập không thành công. Lỗi sai mật khẩu hoặc tên đăng nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                default:
                    MainForm.isLoginSuccess = false;
                    MessageBox.Show("Lỗi không kết nối được cơ sở dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;

            }
            //Neu so lan dang nhap khong thanh cong > 5 thi thoat

        }
        private void lblDangNhap_Click(object sender, EventArgs e)
        {

        }
        private void lblThoat_Click(object sender, EventArgs e)
        {

        }

        private void lblCH_DoubleClick(object sender, EventArgs e)
        {
            //CauHinhCSDLForm chForm = new CauHinhCSDLForm();
            //chForm.ShowDialog();
        }

        private void lblDB_DoubleClick(object sender, EventArgs e)
        {
            //CauHinhCSDLHaiQuanForm chHaiQuanForm = new CauHinhCSDLHaiQuanForm();
            //chHaiQuanForm.ShowDialog(); 

        }

        private void txtKeyDownEvent(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.Login();
                    break;
                case Keys.Escape:
                    Application.Exit();
                    break;
            }
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            this.Login();
        }

        private void uiButton1_Click(object sender, EventArgs e)//thoat
        {
            MainForm.isLoginSuccess = false;
            this.Close();
        }

        private void txtMaDoanhNghiep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F10)
            {
                PasswordForm passForm = new PasswordForm();
                passForm.ShowDialog();
                if (passForm.IsPass)
                {
                    CauHinhCSDLHaiQuanForm cauHinhForm = new CauHinhCSDLHaiQuanForm();
                    cauHinhForm.ShowDialog();
                    if (cauHinhForm.Change)
                    {
                        //MainForm.CloseAll();
                        Application.Restart();
                    }
                }
            }
        }

        private void txtMatKhau_TextChanged(object sender, EventArgs e)
        {

        }


    }
}