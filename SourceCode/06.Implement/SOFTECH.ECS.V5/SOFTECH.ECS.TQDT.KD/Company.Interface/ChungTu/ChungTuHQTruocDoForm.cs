﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface
{
    public partial class ChungTuHQTruocDoForm : BaseForm
    {
        public ChungTuHQTruocDoForm()
        {
            InitializeComponent();
        }
        public ChungTuHQTruocDo ChungTuTruoc = null;
        public string Type;
        public long Master_Id;
        private void uiGroupBox3_Click(object sender, EventArgs e)
        {

        }
        private void Set()
        {
            txtGhiChu.Text = ChungTuTruoc.GhiChu;
            donViHaiQuanControl1.Ma = ChungTuTruoc.MaHaiQuan;
            if (ChungTuTruoc.NgayKhaiCT != null && ChungTuTruoc.NgayKhaiCT.Year > 1900)
                dtNgayKhaiCT.Value = ChungTuTruoc.NgayKhaiCT;
            if (ChungTuTruoc.NgayDangKyCT != null && ChungTuTruoc.NgayDangKyCT.Year > 1900)
                dtNgayDKCT.Value = ChungTuTruoc.NgayDangKyCT;
            if (ChungTuTruoc.NgayChungTu != null && ChungTuTruoc.NgayChungTu.Year > 1900)
                dtNgayCT.Value = ChungTuTruoc.NgayChungTu;
            if (ChungTuTruoc.NgayHHChungTu != null && ChungTuTruoc.NgayHHChungTu.Year > 1900)
                dtNgayHHCT.Value = ChungTuTruoc.NgayHHChungTu;
            if (ChungTuTruoc.ThoiHanNop != null && ChungTuTruoc.ThoiHanNop.Year > 1900)
                dtNgayNoCT.Value = ChungTuTruoc.ThoiHanNop;

            txtNguoiDcCap_Ma.Text = ChungTuTruoc.MaNguoiDuocCap;
            txtNguoiDcCap_Ten.Text = ChungTuTruoc.TenNguoiDuocCap;
            txtNguoiPhatHanh_Ma.Text = ChungTuTruoc.MaNguoiPhatHanh;
            txtNguoiPhatHanh_Ten.Text = ChungTuTruoc.TenNguoiPhatHanh;
            txtNguoiKhai_Ma.Text = ChungTuTruoc.MaNguoiKhaiHQ;
            txtNguoiKhai_Ten.Text = ChungTuTruoc.TenNguoiKhaiHQ;
            chkXinNo.Checked = ChungTuTruoc.XinNo;
            txtSoChungTu.Text = ChungTuTruoc.SoChungTu;
            txtSoDKCT.Value = ChungTuTruoc.SoDangKyCT;
        }
        private void Get()
        {
            ChungTuTruoc.Master_ID = Master_Id;
            ChungTuTruoc.Type = Type;
            ChungTuTruoc.MaHaiQuan = donViHaiQuanControl1.Ma;
            ChungTuTruoc.NgayKhaiCT = (dtNgayKhaiCT.Value != null && dtNgayKhaiCT.Value.Year > 1900) ? dtNgayKhaiCT.Value : new DateTime(1900, 1, 1);
            ChungTuTruoc.NgayDangKyCT = (dtNgayDKCT.Value != null && dtNgayDKCT.Value.Year > 1900) ? dtNgayDKCT.Value : new DateTime(1900, 1, 1);
            ChungTuTruoc.NgayChungTu = (dtNgayCT.Value != null && dtNgayCT.Value.Year > 1900) ? dtNgayCT.Value : new DateTime(1900, 1, 1);
            ChungTuTruoc.NgayHHChungTu = (dtNgayHHCT.Value != null && dtNgayHHCT.Value.Year > 1900) ? dtNgayHHCT.Value : new DateTime(1900, 1, 1);
            ChungTuTruoc.ThoiHanNop = (dtNgayNoCT.Value != null && dtNgayNoCT.Value.Year > 1900) ? dtNgayNoCT.Value : new DateTime(1900, 1, 1);

              ChungTuTruoc.MaNguoiDuocCap = txtNguoiDcCap_Ma.Text;
              ChungTuTruoc.TenNguoiDuocCap =txtNguoiDcCap_Ten.Text;
              ChungTuTruoc.MaNguoiPhatHanh=txtNguoiPhatHanh_Ma.Text;
              ChungTuTruoc.TenNguoiPhatHanh=txtNguoiPhatHanh_Ten.Text;
              ChungTuTruoc.MaNguoiKhaiHQ=txtNguoiKhai_Ma.Text;
              ChungTuTruoc.TenNguoiKhaiHQ=txtNguoiKhai_Ten.Text;
              ChungTuTruoc.XinNo=chkXinNo.Checked;
              ChungTuTruoc.SoChungTu=txtSoChungTu.Text;
              ChungTuTruoc.SoDangKyCT = Convert.ToInt32(txtSoDKCT.Value);
              ChungTuTruoc.GhiChu = txtGhiChu.Text;
        }
        private void chkXinNo_CheckedChanged(object sender, EventArgs e)
        {
            dtNgayNoCT.Enabled = chkXinNo.Checked;
        }
        private void ChungTuHQTruocDoForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (ChungTuTruoc != null)
                    Set();
                else
                {
                    donViHaiQuanControl1.Ma = GlobalSettings.MA_HAI_QUAN;
                    ChungTuTruoc = new ChungTuHQTruocDo();
                    ChungTuTruoc.Type = Type;
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);	
            }
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Get();
                
                ShowMessage("Lưu thông tin thành công", false);
                this.DialogResult = DialogResult.Yes;
                this.Close();

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lưu thông tin không thành công. Xảy ra lỗi : " + ex.Message, false);
            }
        }
//         private void btnXoaChungTu_Click(object sender, EventArgs e)
//         {
//             try
//             {
//                 ChungTuTruoc.Delete();
//             }
//             catch (System.Exception ex)
//             {
//                 Logger.LocalLogger.Instance().WriteMessage(ex);	
//             }
//         }
    }
}
