----------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKToKhaiXuat_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh]    Script Date: 02/06/2015 14:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
ALTER PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiXuat_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh]  
 @BangKeHoSoThanhLy_ID bigint  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
SELECT  
 B.ID,  
 B.BangKeHoSoThanhLy_ID,  
 CASE WHEN B.MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=B.SoToKhai)
		ELSE B.SoToKhai END AS SoToKhaiVNACCS,
            B.SoToKhai ,
 B.MaLoaiHinh,  
 B.NamDangKy,  
 B.MaHaiQuan,  
 B.NgayDangKy,  
 B.NgayThucXuat,  
 B.STTHang,  
 --minhnd: thêm số hợp đồng
 Case when A.MaLoaiHinh like '%V%' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = B.SoToKhai)) else (select top 1 SoHopDong from t_SXXK_ToKhaiMauDich where SoToKhai = B.SoToKhai)  end AS GhiChu,  
 --
 A.NgayHoanThanh  
 ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.           
FROM  
 [dbo].[t_KDT_SXXK_BKToKhaiXuat] B  
 INNER JOIN t_SXXK_ToKhaiMauDich A  
 on B.SoToKhai=A.SoToKhai AND B.MaLoaiHinh=A.MaLoaiHinh AND B.NamDangKy=A.NamDangKy and B.MaHaiQuan=A.MaHaiQuan  
 --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
 LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)
WHERE  
 B.BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID  

-----------------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]    Script Date: 02/06/2015 13:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]
    @MaDoanhNghiep VARCHAR(14) ,
    @MaHaiQuan CHAR(6) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
IF(@MaHaiQuan IS not NULL AND @MaHaiQuan <> '')
BEGIN
    SELECT  A.MaHaiQuan ,
            CASE WHEN A.MaLoaiHinh like'%V%' THEN (select Top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
    --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				--else A.MaLoaiHinh end as MaLoaiHinh ,
		    A.MaLoaiHinh,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            --minhnd:thêm số hợp đồng
            Case when A.MaLoaiHinh like '%V%' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = A.SoToKhai)) else (select top 1 SoHopDong from t_SXXK_ToKhaiMauDich where SoToKhai = A.SoToKhai)  end AS GhiChu,
            --
		    e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan        
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'X%'
                  --OR A.MaLoaiHinh LIKE 'XGC%'
                  --OR A.MaLoaiHinh LIKE 'XCX%'
                ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL         
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   MaHaiQuan = @MaHaiQuan
                    AND MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy != 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh       
            
END
ELSE
	BEGIN
		  SELECT  A.MaHaiQuan ,
            CASE WHEN A.MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
    --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				--else A.MaLoaiHinh end as MaLoaiHinh,
				A.MaLoaiHinh,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        --WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan        
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            --AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'X%'
                  --OR A.MaLoaiHinh LIKE 'XGC%'
                  --OR A.MaLoaiHinh LIKE 'XCX%'
                ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL         
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   --MaHaiQuan = @MaHaiQuan AND
                     MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy != 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh       
	END 
	
	
Go

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.7',GETDATE(), N' Cập nhật p_KDT_SXXK_BKToKhaiXuat_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh, p_SXXK_GetDSTKXChuaThanhLyDate')
END	

