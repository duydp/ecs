﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KeyServer;
using System.IO;
using System.Diagnostics;
using KeyServer.User;
using KeySecurity;
using Janus.Windows.GridEX;

namespace LicenseCode
{
    public partial class frmKeyManager : Form
    {
        public List<Key> listKey = new List<Key>();
        private bool isOverWrite = true;
        public List<Products> productId = new List<Products>();
        public User user = new User();

        private ProductKey.ProductKey productServices = new ProductKey.ProductKey();
        public frmKeyManager()
        {
            InitializeComponent();
        }
        private void frmKeyTrial_Load(object sender, EventArgs e)
        {
            clcDateFrom.Value = clcDateFrom.Value.AddDays(-30);
            clcDateTo.Value = clcDateTo.Value.AddDays(1);

            cbbSanPham.DataSource = productId;
            cbbSanPham.DisplayMember = "ID";
            cbbSanPham.ValueMember = "ID";


            productServices.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            productServices.Timeout = 240000;

            if (listKey !=null && listKey.Count >=1)
            {
                BindData();   
            }

            if (listKey.Count >= 1)
            {
                Key keyGen = (Key)gridList.GetRow().DataRow;
                frmLockKey f = new frmLockKey();
                f.keyGen = keyGen;
                f.user = user;
                f.ShowDialog(this);
                Clipboard.SetText(keyGen.ActivedKey);
            }
            else
            {
                LicenseCode.ProductKey.Key key = (LicenseCode.ProductKey.Key)gridList.GetRow().DataRow;
                frmLockKey f = new frmLockKey();
                f.keyLock = key;
                f.user = user;
                f.ShowDialog(this);
                Clipboard.SetText(key.ActivedKey);
            }

        }
        private void BindData()
        {
            try
            {
                gridList.Refetch();
                gridList.DataSource = listKey;
                gridList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string GetSearchWhere()
        {
            try
            {
                String where = " 1=1 ";
                if (cbbCDKeyType.SelectedValue != null)
                    where += " AND Status ='" + cbbCDKeyType.SelectedValue.ToString() + "'";
                if (cbbStatus.SelectedValue != null)
                {
                    String Status = cbbStatus.SelectedValue.ToString();
                    switch (Status)
                    {
                        case "0":
                            where += " AND Year(ActivedDate) > 1900";
                            break;
                        case "1":
                            where += " AND Year(ActivedDate) = 1900";
                            break;
                        default:
                            break;
                    }
                }
                string dateFrom = clcDateFrom.Value.ToString("yyyy-MM-dd 00:00:00");
                string dateTo = clcDateTo.Value.ToString("yyyy-MM-dd 23:59:59");
                where += " AND RequestedDate BETWEEN '" + dateFrom + "' AND '" + dateTo + "'";
                if (cbbSanPham.Value != null)
                    where += " AND ProductID ='" + cbbSanPham.Value.ToString() + "'";
                if (cbbDoanhNghiep.Value != null)
                    where += " AND CustomerCode ='" + cbbDoanhNghiep.Value.ToString() + "'";
                return where;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                gridList.Refetch();
                gridList.DataSource = productServices.SelectCollectionDynamic(user.UserName, KeyCode.ConvertToBase64(user.PassWord),GetSearchWhere());
                gridList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách CD KEY " + DateTime.Now.ToString("dd/MM/yyyy").Replace("/","_") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";

                if (sfNPL.ShowDialog() == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = gridList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (MessageBoxControl.ShowMessageConfirm("Bạn có muốn mở file này không?") == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = gridList.SelectedItems;
                if (gridList.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (MessageBoxControl.ShowMessageConfirm("Bạn có muốn xóa CD Key này không?") == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        LicenseCode.ProductKey.Key key = (LicenseCode.ProductKey.Key)i.GetRow().DataRow;
                        productServices.Delete(user.UserName, KeyCode.ConvertToBase64(user.PassWord), key);
                    }
                }
                btnSearch_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string Status = e.Row.Cells["Status"].Value.ToString();
                    switch (Status)
                    {
                        case "0":
                            e.Row.Cells["Status"].Text = "Trial";
                            break;
                        case "1":
                            e.Row.Cells["Status"].Text = "License";
                            break;
                        case "LOCK":
                            e.Row.Cells["Status"].Text = "Lock";
                            break;
                        case "-1":
                            e.Row.Cells["Status"].Text = "Wait For Approve";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void gridList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (listKey.Count >=1)
                    {
                       Key keyGen = (Key)gridList.GetRow().DataRow;
                       frmLockKey f = new frmLockKey();
                       f.keyGen = keyGen;
                       f.user = user;
                       f.ShowDialog(this);
                    }
                    else
                    {
                        LicenseCode.ProductKey.Key key = (LicenseCode.ProductKey.Key)gridList.GetRow().DataRow;
                        frmLockKey f = new frmLockKey();
                        f.keyLock = key;
                        f.user = user;
                        f.ShowDialog(this);
                    }
                }
            }
            catch (Exception ex)
            {
                
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void LoadDoanhNghiep()
        {
            string result = string.Empty;
            try
            {
                result = productServices.CustomerLoadByProduct(user.UserName, KeyCode.ConvertToBase64(user.PassWord), cbbSanPham.Text.Trim());
                if (!string.IsNullOrEmpty(result))
                {
                    List<Customer> listCu = KeyCode.Deserialize<List<Customer>>(result);
                    cbbDoanhNghiep.DataSource = listCu;
                    cbbDoanhNghiep.ValueMember = "CustomerCode";
                    cbbDoanhNghiep.DisplayMember = "CustomerName";
                }

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbSanPham_ValueChanged(object sender, EventArgs e)
        {
            LoadDoanhNghiep();
            btnSearch_Click(null,null);
        }

    }
}
