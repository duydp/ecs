﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logger;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using System.Diagnostics;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class VNACC_AutoFeedBackVouchers : Company.Interface.BaseForm
    {
        public KDT_VNACCS_License license = new KDT_VNACCS_License();
        public KDT_VNACCS_License_Detail licenseDetail = new KDT_VNACCS_License_Detail();
        public KDT_VNACCS_ContractDocument contractDocument = new KDT_VNACCS_ContractDocument();
        public KDT_VNACCS_ContractDocument_Detail contractDocumentDetail = new KDT_VNACCS_ContractDocument_Detail();
        public KDT_VNACCS_CommercialInvoice commercialInvoice = new KDT_VNACCS_CommercialInvoice();
        public KDT_VNACCS_CommercialInvoice_Detail commercialInvoiceDetail = new KDT_VNACCS_CommercialInvoice_Detail();
        public KDT_VNACCS_CertificateOfOrigin certificateOfOrigin = new KDT_VNACCS_CertificateOfOrigin();
        public KDT_VNACCS_CertificateOfOrigin_Detail certificateOfOriginDetail = new KDT_VNACCS_CertificateOfOrigin_Detail();
        public KDT_VNACCS_BillOfLading billOfLading = new KDT_VNACCS_BillOfLading();
        public KDT_VNACCS_BillOfLading_Detail billOfLadingDetail = new KDT_VNACCS_BillOfLading_Detail();
        public KDT_VNACCS_Container_Detail container = new KDT_VNACCS_Container_Detail();
        public KDT_VNACCS_AdditionalDocument additionalDocument = new KDT_VNACCS_AdditionalDocument();
        public KDT_VNACCS_AdditionalDocument_Detail additionalDocumentDetail = new KDT_VNACCS_AdditionalDocument_Detail();
        public KDT_VNACCS_OverTime overTime = new KDT_VNACCS_OverTime();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public string LoaiChungTu = "";
        public bool isFeedBack = true;
        private bool isStop;
        public DataTable dtFeedBack;
        public VNACC_AutoFeedBackVouchers()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void VNACC_AutoFeedBackVouchers_Load(object sender, EventArgs e)
        {
            LoadData();
            dtFeedBack = new DataTable();

            DataColumn dtColSoToKhai = new DataColumn("SoToKhai");
            dtColSoToKhai.DataType = typeof(System.String);
            DataColumn dtColNgayDangKy = new DataColumn("NgayDangKy");
            dtColNgayDangKy.DataType = typeof(System.DateTime);
            DataColumn dtColSoTN = new DataColumn("SoTN");
            dtColSoTN.DataType = typeof(System.String);
            DataColumn dtColNgayTN = new DataColumn("NgayTN");
            dtColNgayTN.DataType = typeof(System.DateTime);
            DataColumn dtColLoaiChungTu = new DataColumn("LoaiChungTu");
            dtColLoaiChungTu.DataType = typeof(System.String);

            dtFeedBack.Columns.Add(dtColSoToKhai);
            dtFeedBack.Columns.Add(dtColNgayDangKy);
            dtFeedBack.Columns.Add(dtColSoTN);
            dtFeedBack.Columns.Add(dtColNgayTN);
            dtFeedBack.Columns.Add(dtColLoaiChungTu);

        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private void LoadData()
        {
            try
            {
                grList.Refresh();
                grList.DataSource = KDT_VNACC_ToKhaiMauDich.GetAllVouchers().Tables[0];
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void DoWork(object obj)
        {
            try
            {

                SetError(string.Empty);
                Janus.Windows.GridEX.GridEXRow[] listChecked = grList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    LoaiChungTu = item.Cells["LoaiChungTu"].Value.ToString();
                    long ID = Convert.ToInt64(item.Cells["ID"].Value.ToString());
                    switch (LoaiChungTu)
                    {
                        case "CertificateOfOrigin":
                            certificateOfOrigin = KDT_VNACCS_CertificateOfOrigin.Load(ID);
                            certificateOfOrigin.CertificateOfOriginCollection = KDT_VNACCS_CertificateOfOrigin_Detail.SelectCollectionBy_CertificateOfOrigin_ID(certificateOfOrigin.ID);
                            Feedback_CertificateOfOrigin();
                            break;
                        case "BillOfLading":
                            billOfLading = KDT_VNACCS_BillOfLading.Load(ID);
                            billOfLading.BillOfLadingCollection = KDT_VNACCS_BillOfLading_Detail.SelectCollectionBy_BillOfLading_ID(billOfLading.ID);
                            Feedback_BillOfLading();
                            break;
                        case "ContractDocument":
                            contractDocument = KDT_VNACCS_ContractDocument.Load(ID);
                            contractDocument.ContractDocumentCollection = KDT_VNACCS_ContractDocument_Detail.SelectCollectionBy_ContractDocument_ID(contractDocument.ID);
                            Feedback_ContractDocument();
                            break;
                        case "CommercialInvoice":
                            commercialInvoice = KDT_VNACCS_CommercialInvoice.Load(ID);
                            commercialInvoice.CommercialInvoiceCollection = KDT_VNACCS_CommercialInvoice_Detail.SelectCollectionBy_CommercialInvoice_ID(commercialInvoice.ID);
                            Feedback_CommercialInvoice();
                            break;
                        case "License":
                            license = KDT_VNACCS_License.Load(ID);
                            license.LicenseCollection = KDT_VNACCS_License_Detail.SelectCollectionBy_License_ID(license.ID);
                            Feedback_License();
                            break;
                        case "Container":
                            container = KDT_VNACCS_Container_Detail.Load(ID);
                            Feedback_Container();
                            break;
                        case "AdditionalDocument":
                            additionalDocument = KDT_VNACCS_AdditionalDocument.Load(ID);
                            additionalDocument.AdditionalDocumentCollection = KDT_VNACCS_AdditionalDocument_Detail.SelectCollectionBy_AdditionalDocument_ID(additionalDocument.ID);
                            Feedback_AdditionalDocumentn();
                            break;
                    }
                    SetProcessBar((i * 100 / listChecked.Length));
                }
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        LoadData();
                        btnThucHien.Enabled = true; btnDong.Enabled = true;
                        btnStop.Enabled = true;

                    }));
                }
                else
                {
                    LoadData();
                    btnThucHien.Enabled = true; btnDong.Enabled = true; btnStop.Enabled = true;
                }
                SetProcessBar(0);
                SetError("Hoàn thành");

            }

            catch (System.Exception ex)
            {
                SetError(string.Empty);
                LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnExportExcel_Click(object sender, EventArgs e)
        {

        }

        private void btnThucHien_Click(object sender, EventArgs e)
        {
            btnThucHien.Enabled = false;
            btnDong.Enabled = false;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }
        private void BinDataFeedBack()
        {
            try
            {
                grListResult.Refresh();
                grListResult.DataSource = dtFeedBack;
                grListResult.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void Feedback_License()
        {
            isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = license.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.License,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.License,
                };
                subjectBase.Type = DeclarationIssuer.License;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = license.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(license.MaHQ.Trim())),
                                                  Identity = license.MaHQ
                                              }, subjectBase, null);
                if (license.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(license.MaHQ));
                    msgSend.To.Identity = license.MaHQ;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (!isStop)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            if (msgInfor.Contains("Cấp số tiếp nhận khai báo"))
                            {
                                DataRow dtRow = dtFeedBack.NewRow();
                                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(license.TKMD_ID);
                                dtRow["SoToKhai"] = TKMD.SoToKhai.ToString();
                                dtRow["NgayDangKy"] = TKMD.NgayDangKy;
                                dtRow["SoTN"] = license.SoTN.ToString();
                                dtRow["NgayTN"] = license.NgayTN;
                                dtRow["LoaiChungTu"] = "License";
                                dtFeedBack.Rows.Add(dtRow);
                                BinDataFeedBack();
                            }
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        isFeedBack = false;
                    }
                }
            }

        }
        public void Feedback_ContractDocument()
        {
            isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = contractDocument.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.ContractDocument,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.ContractDocument,
                };
                subjectBase.Type = DeclarationIssuer.ContractDocument;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = contractDocument.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(contractDocument.MaHQ.Trim())),
                                                  Identity = contractDocument.MaHQ
                                              }, subjectBase, null);
                if (contractDocument.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(contractDocument.MaHQ));
                    msgSend.To.Identity = contractDocument.MaHQ;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (!isStop)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            if (msgInfor.Contains("Cấp số tiếp nhận khai báo"))
                            {
                                DataRow dtRow = dtFeedBack.NewRow();
                                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(contractDocument.TKMD_ID);
                                dtRow["SoToKhai"] = TKMD.SoToKhai.ToString();
                                dtRow["NgayDangKy"] = TKMD.NgayDangKy;
                                dtRow["SoTN"] = contractDocument.SoTN.ToString();
                                dtRow["NgayTN"] = contractDocument.NgayTN;
                                dtRow["LoaiChungTu"] = "ContractDocument";
                                dtFeedBack.Rows.Add(dtRow);
                                BinDataFeedBack();
                            }
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        isFeedBack = false;
                    }
                }
            }

        }
        public void Feedback_CommercialInvoice()
        {
            isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = commercialInvoice.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.CommercialInvoice,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.CommercialInvoice,
                };
                subjectBase.Type = DeclarationIssuer.CommercialInvoice;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = commercialInvoice.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(commercialInvoice.MaHQ.Trim())),
                                                  Identity = commercialInvoice.MaHQ
                                              }, subjectBase, null);
                if (commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(commercialInvoice.MaHQ));
                    msgSend.To.Identity = commercialInvoice.MaHQ;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (!isStop)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            if (msgInfor.Contains("Cấp số tiếp nhận khai báo"))
                            {
                                DataRow dtRow = dtFeedBack.NewRow();
                                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(commercialInvoice.TKMD_ID);
                                dtRow["SoToKhai"] = TKMD.SoToKhai.ToString();
                                dtRow["NgayDangKy"] = TKMD.NgayDangKy;
                                dtRow["SoTN"] = commercialInvoice.SoTN.ToString();
                                dtRow["NgayTN"] = commercialInvoice.NgayTN;
                                dtRow["LoaiChungTu"] = "CommercialInvoice";
                                dtFeedBack.Rows.Add(dtRow);
                                BinDataFeedBack();
                            }
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        isFeedBack = false;
                    }
                }
            }

        }
        public void Feedback_CertificateOfOrigin()
        {
            isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = certificateOfOrigin.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.CertificateOfOrigin,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.CertificateOfOrigin,
                };
                subjectBase.Type = DeclarationIssuer.CertificateOfOrigin;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = certificateOfOrigin.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(certificateOfOrigin.MaHQ.Trim())),
                                                  Identity = certificateOfOrigin.MaHQ
                                              }, subjectBase, null);
                if (certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(certificateOfOrigin.MaHQ));
                    msgSend.To.Identity = certificateOfOrigin.MaHQ;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (!isStop)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            if (msgInfor.Contains("Cấp số tiếp nhận khai báo"))
                            {
                                DataRow dtRow = dtFeedBack.NewRow();
                                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(certificateOfOrigin.TKMD_ID);
                                dtRow["SoToKhai"] = TKMD.SoToKhai.ToString();
                                dtRow["NgayDangKy"] = TKMD.NgayDangKy;
                                dtRow["SoTN"] = certificateOfOrigin.SoTN.ToString();
                                dtRow["NgayTN"] = certificateOfOrigin.NgayTN;
                                dtRow["LoaiChungTu"] = "CertificateOfOrigin";
                                dtFeedBack.Rows.Add(dtRow);
                                BinDataFeedBack();
                            }
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        isFeedBack = false;
                    }

                }
            }

        }
        public void Feedback_BillOfLading()
        {
            isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = billOfLading.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.BillOfLading,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.BillOfLading,
                };
                subjectBase.Type = DeclarationIssuer.BillOfLading;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = billOfLading.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(billOfLading.MaHQ.Trim())),
                                                  Identity = billOfLading.MaHQ
                                              }, subjectBase, null);
                if (billOfLading.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(billOfLading.MaHQ));
                    msgSend.To.Identity = billOfLading.MaHQ;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (!isStop)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            if (msgInfor.Contains("Cấp số tiếp nhận khai báo"))
                            {
                                DataRow dtRow = dtFeedBack.NewRow();
                                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(billOfLading.TKMD_ID);
                                dtRow["SoToKhai"] = TKMD.SoToKhai.ToString();
                                dtRow["NgayDangKy"] = TKMD.NgayDangKy;
                                dtRow["SoTN"] = billOfLading.SoTN.ToString();
                                dtRow["NgayTN"] = billOfLading.NgayTN;
                                dtRow["LoaiChungTu"] = "BillOfLading";
                                dtFeedBack.Rows.Add(dtRow);
                                BinDataFeedBack();
                            }
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        isFeedBack = false;
                    }
                }
            }

        }
        public void Feedback_Container()
        {
            isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = container.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.Containers,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.Containers,
                };
                subjectBase.Type = DeclarationIssuer.Containers;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = container.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(container.MaHQ.Trim())),
                                                  Identity = container.MaHQ
                                              }, subjectBase, null);
                if (container.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(container.MaHQ));
                    msgSend.To.Identity = container.MaHQ;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (!isStop)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            if (msgInfor.Contains("Cấp số tiếp nhận khai báo"))
                            {
                                DataRow dtRow = dtFeedBack.NewRow();
                                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(container.TKMD_ID);
                                dtRow["SoToKhai"] = TKMD.SoToKhai.ToString();
                                dtRow["NgayDangKy"] = TKMD.NgayDangKy;
                                dtRow["SoTN"] = container.SoTN.ToString();
                                dtRow["NgayTN"] = container.NgayTN;
                                dtRow["LoaiChungTu"] = "Container";
                                dtFeedBack.Rows.Add(dtRow);
                                BinDataFeedBack();
                            }
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        isFeedBack = false;
                    }
                }
            }

        }
        public void Feedback_AdditionalDocumentn()
        {
            isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = additionalDocument.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.AdditionalDocument,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.AdditionalDocument,
                };
                subjectBase.Type = DeclarationIssuer.AdditionalDocument;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = additionalDocument.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(additionalDocument.MaHQ.Trim())),
                                                  Identity = additionalDocument.MaHQ
                                              }, subjectBase, null);
                if (additionalDocument.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(additionalDocument.MaHQ));
                    msgSend.To.Identity = additionalDocument.MaHQ;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (!isStop)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            if (msgInfor.Contains("Cấp số tiếp nhận khai báo"))
                            {
                                DataRow dtRow = dtFeedBack.NewRow();
                                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(additionalDocument.TKMD_ID);
                                dtRow["SoToKhai"] = TKMD.SoToKhai.ToString();
                                dtRow["NgayDangKy"] = TKMD.NgayDangKy;
                                dtRow["SoTN"] = additionalDocument.SoTN.ToString();
                                dtRow["NgayTN"] = additionalDocument.NgayTN;
                                dtRow["LoaiChungTu"] = "AdditionalDocument";
                                dtFeedBack.Rows.Add(dtRow);
                                BinDataFeedBack();
                            }
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        isFeedBack = false;
                    }
                }
            }

        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            switch (LoaiChungTu)
            {
                case "License":
                    feedbackContent = SingleMessage.LicenseSendHandler(license, ref msgInfor, e);
                    break;
                case "ContractDocument":
                    feedbackContent = SingleMessage.ContractDocumentSendHandler(contractDocument, ref msgInfor, e);
                    break;
                case "CommercialInvoice":
                    feedbackContent = SingleMessage.CommercialInvoiceSendHandler(commercialInvoice, ref msgInfor, e);
                    break;
                case "CertificateOfOrigin":
                    feedbackContent = SingleMessage.CertificateOfOriginSendHandler(certificateOfOrigin, ref msgInfor, e);
                    break;
                case "BillOfLading":
                    feedbackContent = SingleMessage.BillOfLadingSendHandler(billOfLading, ref msgInfor, e);
                    break;
                case "Container":
                    feedbackContent = SingleMessage.ContainersSendHandler(container, ref msgInfor, e);
                    break;
                case "AdditionalDocument":
                    feedbackContent = SingleMessage.AdditionalDocumentSendHandler(additionalDocument, ref msgInfor, e);
                    break;
                default:
                    break;
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    btnStop.Enabled = true;
                    btnThucHien.Enabled = true;
                    isFeedBack = false;
                    isStop = true;
                }));
            }
            else
            {
                btnStop.Enabled = true;
                btnThucHien.Enabled = true;
                isFeedBack = false;
                isStop = true;
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
                {
                    string pl = e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString();
                    if (pl == "1")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Xanh";
                    else if (pl == "2")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Vàng";
                    else if (pl == "3")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Đỏ";
                    else
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Chưa phân luồng";
                }
            }
        }
    }
}
