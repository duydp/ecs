using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class MEC : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ECN { get; set; }
public PropertiesAttribute JKN { get; set; }
public PropertiesAttribute CH { get; set; }
public PropertiesAttribute CHB { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute EPP { get; set; }
public PropertiesAttribute EPA { get; set; }
public PropertiesAttribute EPT { get; set; }
public PropertiesAttribute CGC { get; set; }
public PropertiesAttribute CGN { get; set; }
public PropertiesAttribute CGP { get; set; }
public PropertiesAttribute CGA { get; set; }
public PropertiesAttribute CAT { get; set; }
public PropertiesAttribute CAC { get; set; }
public PropertiesAttribute CAS { get; set; }
public PropertiesAttribute CGK { get; set; }
public PropertiesAttribute AWB { get; set; }
public PropertiesAttribute NO { get; set; }
public PropertiesAttribute GW { get; set; }
public PropertiesAttribute ST { get; set; }
public PropertiesAttribute DSC { get; set; }
public PropertiesAttribute PSC { get; set; }
public PropertiesAttribute FCD { get; set; }
public PropertiesAttribute FKK { get; set; }
public PropertiesAttribute SKK { get; set; }
public PropertiesAttribute CMN { get; set; }
public PropertiesAttribute NT { get; set; }
public PropertiesAttribute REF { get; set; }

public MEC()
        {
ECN = new PropertiesAttribute(12, typeof(int));
JKN = new PropertiesAttribute(1, typeof(string));
CH = new PropertiesAttribute(6, typeof(string));
CHB = new PropertiesAttribute(2, typeof(string));
EPC = new PropertiesAttribute(13, typeof(string));
EPN = new PropertiesAttribute(300, typeof(string));
EPP = new PropertiesAttribute(7, typeof(string));
EPA = new PropertiesAttribute(300, typeof(string));
EPT = new PropertiesAttribute(20, typeof(string));
CGC = new PropertiesAttribute(13, typeof(string));
CGN = new PropertiesAttribute(70, typeof(string));
CGP = new PropertiesAttribute(9, typeof(string));
CGA = new PropertiesAttribute(35, typeof(string));
CAT = new PropertiesAttribute(35, typeof(string));
CAC = new PropertiesAttribute(35, typeof(string));
CAS = new PropertiesAttribute(35, typeof(string));
CGK = new PropertiesAttribute(2, typeof(string));
AWB = new PropertiesAttribute(20, typeof(string));
NO = new PropertiesAttribute(6, typeof(int));
GW = new PropertiesAttribute(8, typeof(int));
ST = new PropertiesAttribute(7, typeof(string));
DSC = new PropertiesAttribute(5, typeof(string));
PSC = new PropertiesAttribute(3, typeof(string));
FCD = new PropertiesAttribute(3, typeof(string));
FKK = new PropertiesAttribute(20, typeof(int));
SKK = new PropertiesAttribute(8, typeof(int));
CMN = new PropertiesAttribute(200, typeof(string));
NT = new PropertiesAttribute(300, typeof(string));
REF = new PropertiesAttribute(20, typeof(string));
TongSoByte = 1553;
}

}public partial class EnumGroupID
    {
}

}
