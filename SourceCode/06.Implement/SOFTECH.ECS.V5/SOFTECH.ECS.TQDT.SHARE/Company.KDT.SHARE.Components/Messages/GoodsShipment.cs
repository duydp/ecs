﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages;
namespace Company.KDT.SHARE.Components
{
    public class GoodsShipment
    {
        /// <summary>
        /// Mã nước nhập khẩu   
        /// </summary>
        [XmlElement("importationCountry")]
        public string ImportationCountry { set; get; }
        /// <summary>
        /// Mã nước xuất khẩu   
        /// </summary>
        [XmlElement("exportationCountry")]
        public string ExportationCountry { get; set; }
        /// <summary>
        /// Người giao hàng
        /// </summary>
        [XmlElement("Consignor")]
        public NameBase Consignor { set; get; }
        /// <summary>
        /// Người nhận hàng
        /// </summary>
        [XmlElement("Consignee")]
        public NameBase Consignee { set; get; }
        [XmlElement("Consignment")]
        public Consignment Consignment { get; set; }
        /// <summary>
        /// Người nhận hàng trung gian
        /// </summary>
        [XmlElement("NotifyParty")]
        public NameBase NotifyParty { set; get; }
        /// <summary>
        /// Địa điểm giao hàng
        /// </summary>
        [XmlElement("DeliveryDestination")]
        public DeliveryDestination DeliveryDestination { get; set; }
        /// <summary>
        /// Cửa khẩu nhập
        /// </summary>
        [XmlElement("EntryCustomsOffice")]
        public LocationNameBase EntryCustomsOffice { get; set; }
        /// <summary>
        /// cửa khẩu xuất
        /// </summary>
        [XmlElement("ExitCustomsOffice")]
        public LocationNameBase ExitCustomsOffice { get; set; }
        /// <summary>
        /// Người nhập khẩu
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }
        /// <summary>
        /// Điều kiện giao hàng
        /// </summary>
        [XmlElement("TradeTerm")]
        public TradeTerm TradeTerm { get; set; }
        /// <summary>
        /// Hàng khai báo
        /// </summary>
        [XmlElement("CustomsGoodsItem")]
        public List<CustomsGoodsItem> CustomsGoodsItems { get; set; }
        /// <summary>
        /// Người xuất khẩu
        /// </summary>
        [XmlElement("Exporter")]
        public NameBase Exporter { get; set; }
        /// <summary>
        /// Hãng sản xuất
        /// </summary>
        [XmlElement("Manufacturer")]
        public NameBase Manufacturer { get; set; }
      
        /// <summary>
        /// Chi tiết hàng, thuế
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }
        
        /// <summary>
        /// Nguyên phụ liệu cung ứng
        /// Dùng cho tờ khai xuất gia công
        /// </summary>
        [XmlElement("Supply")]
        public List<Supply> Supplys { get; set; }

        [XmlElement("TransportLine")]
        public DeliveryDestination TransportLine { get; set; }

        /// <summary>
        /// Phụ lục hợp đồng tờ khai gia công chuyển tiếp
        /// </summary>
        [XmlElement("SubContructReference")]
        public SubContructReference SubContructReference { get; set; }
    }

}
