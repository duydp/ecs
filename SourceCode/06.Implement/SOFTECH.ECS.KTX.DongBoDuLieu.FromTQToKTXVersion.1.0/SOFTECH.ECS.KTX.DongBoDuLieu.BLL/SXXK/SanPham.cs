using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Company.BLL.SXXK;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;

namespace DongBoDuLieu.BLL.SXXK
{
    public class SP
    {
        public static DataSet GetALLDanhSachSP(string MaHaiQuan, string MaDoanhNghiep, string matKhau, bool isPhongKhai)
        {
            string query = "SELECT SP.Ma_HQ AS MaHaiQuan, SP.Ma_SP AS Ma, SP.Ten_SP AS Ten, SP.Ma_HS AS MaHS, SP.MA_DVT AS DVT_ID FROM SSP SP WHERE SP.MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDoanhNghiep + "'";
            DataSet ds = null;
            if (!isPhongKhai)
                ds = WebServiceDB.getDataBySQL(query, MaHaiQuan, "SXXK", MaDoanhNghiep, matKhau);
            else
                ds = WebServiceDB.getDataBySQLDatabase(query, MaHaiQuan, "SXXK");
            return ds;
        }

        public static DataSet GetALLDanhSachSPLocal(string MaHaiQuan, string MaDoanhNghiep, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = "SELECT SP.MaHaiQuan AS MaHaiQuan, SP.Ma AS Ma, SP.Ten AS Ten, SP.MaHS AS MaHS, SP.DVT_ID AS DVT_ID FROM dbo.t_SXXK_SanPham SP WHERE SP.MaHaiQuan = '" + MaHaiQuan + "' AND sp.MaDoanhNghiep = '" + MaDoanhNghiep + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.Ma FROM [" + databaseNameTarget + "].dbo.t_SXXK_SanPham temp WHERE temp.Ma = SP.Ma )";
            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);
            return ds;
        }

        public static DataSet GetALLDanhSachSPLocalTQDT(string MaHaiQuan, string MaDoanhNghiep, bool trangThaiDaDuyet, DateTime tuNgay, DateTime denNgay, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = "SELECT  SP.Master_ID,  " +
                            "SPDK.SoTiepNhan, SPDK.NgayTiepNhan, SPDK.TrangThaiXuLy,  " +
                            "SP.Ma AS Ma,  " +
                            "SP.Ten AS Ten,  " +
                            "SP.MaHS AS MaHS,  " +
                            "SP.DVT_ID AS DVT_ID " +
                            "FROM    dbo.t_KDT_SXXK_SanPham SP " +
                            "INNER JOIN dbo.t_KDT_SXXK_SanPhamDangKy SPDK ON SP.Master_ID = SPDK.ID " +
                            "WHERE   SPDK.MaHaiQuan = '" + MaHaiQuan + "' " +
                            "AND SPDK.MaDoanhNghiep = '" + MaDoanhNghiep + "' ";
            if (trangThaiDaDuyet)
                query += " AND SPDK.TrangThaiXuLy = 1";
            if (tuNgay > DateTime.MinValue)
                query += " and SPDK.NgayTiepNhan >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND SPDK.NgayTiepNhan <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.Ma FROM [" + databaseNameTarget + "].dbo.t_KDT_SXXK_SanPham temp WHERE temp.Ma = SP.Ma )";
            query += " ORDER BY SPDK.NgayTiepNhan";

            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);

            return ds;
        }

        public static void InsertDuLieu(string MaHaiQuan, string MaDoanhNghiep, DataSet ds)
        {
            SanPhamCollection collection = new SanPhamCollection();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                SanPham sp = new SanPham();
                sp.MaHaiQuan = MaHaiQuan;
                sp.MaDoanhNghiep = MaDoanhNghiep;
                sp.Ma = row["Ma"].ToString().Trim();
                sp.Ten = row["Ten"].ToString().Trim();
                sp.MaHS = row["MaHS"].ToString().Trim();
                int k = sp.MaHS.Length;
                //if (sp.MaHS.Length < 10)
                //{
                //    for (int i = 1; i <= 10 - k; ++i)
                //        sp.MaHS += "0";
                //}
                sp.DVT_ID = row["DVT_ID"].ToString();
                collection.Add(sp);

            }
            SanPham spDaDuyet = new SanPham();
            spDaDuyet.InsertUpdate(collection);
        }

        public static void InsertDuLieu(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, string connectionString)
        {
            SanPhamCollection collection = new SanPhamCollection();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                SanPham sp = new SanPham();
                sp.MaHaiQuan = MaHaiQuan;
                sp.MaDoanhNghiep = MaDoanhNghiep;
                sp.Ma = row["Ma"].ToString().Trim();
                sp.Ten = row["Ten"].ToString().Trim();
                sp.MaHS = row["MaHS"].ToString().Trim();
                int k = sp.MaHS.Length;
                //if (sp.MaHS.Length < 10)
                //{
                //    for (int i = 1; i <= 10 - k; ++i)
                //        sp.MaHS += "0";
                //}
                sp.DVT_ID = row["DVT_ID"].ToString();
                collection.Add(sp);

            }

            InsertUpdate(collection, connectionString);
        }

        //---------------------------------------------------------------------------------------------

        public static void InsertDuLieuTQDT(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, string connectionString)
        {
            try
            {
                Company.BLL.KDT.SXXK.SanPhamDangKyCollection kdtSPDKCollection = new Company.BLL.KDT.SXXK.SanPhamDangKyCollection();

                //Create collection NguyenPhuLieuDangKy
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Company.BLL.KDT.SXXK.SanPhamDangKy spDK = Company.BLL.KDT.SXXK.SanPhamDangKy.Load(long.Parse(row["Master_ID"].ToString()));

                    if (spDK != null)
                    {
                        //Load NPL cua NPL DK.
                        spDK.LoadSPCollection();

                        kdtSPDKCollection.Add(spDK);
                    }
                }

                //Insert data NguyenPhuLieuDangKy
                int i = 0;
                foreach (Company.BLL.KDT.SXXK.SanPhamDangKy item in kdtSPDKCollection)
                {
                    i += 1;
                    item.InsertUpdateFull(connectionString, i);
                }

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //---------------------------------------------------------------------------------------------

        public static bool InsertUpdate(SanPhamCollection collection, string connectionString)
        {
            bool ret;

            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    int i = 0;
                    bool ret01 = true;
                    foreach (SanPham item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction, db) <= 0)
                        {
                            ret01 = false;
                            break;
                        }

                        //TODO: HungTQ, Update 27/04/2010.
                        i += 1;
                        Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("{6}.SXXK San pham: MaHaiQuan={4}, MaDoanhNghiep={5}, Ma={0}, Ten={1}, DVT={2}, MaHS={3} ", item.Ma, item.Ten, item.DVT_ID, item.MaHS, item.MaHaiQuan, item.MaDoanhNghiep, i)));

                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

    }
}
