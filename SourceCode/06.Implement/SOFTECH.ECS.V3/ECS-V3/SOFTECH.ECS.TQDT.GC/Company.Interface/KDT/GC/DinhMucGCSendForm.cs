﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using System.Data;
using Company.GC.BLL.KDT;
using System.IO;
using Company.Interface.KDT.SXXK;
using Company.Interface.GC;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
namespace Company.Interface.KDT.GC
{
    public partial class DinhMucGCSendForm : BaseForm
    {
        public DinhMucDangKy dmDangKy = new DinhMucDangKy() { TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO};
        HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        //-----------------------------------------------------------------------------------------
        public DinhMucGCSendForm()
        {
            InitializeComponent();

        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }

        //-----------------------------------------------------------------------------------------
        private void DinhMucGCSendForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();

            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            dgList.Tables[0].Columns["NPL_TuCungUng"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            if(HD != null)
            {
                HD.ID = dmDangKy.ID_HopDong;
                HD = HopDong.Load(HD.ID);

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                txtSoHopDong.Text = HD.SoHopDong;
                dmDangKy.LoadCollection();

            }
            txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
            dgList.DataSource = dmDangKy.DMCollection;

            if (dmDangKy.ID > 0)
            {
                Company.GC.BLL.KDT.SXXK.MsgSend msg = new Company.GC.BLL.KDT.SXXK.MsgSend();
                msg.master_id = this.dmDangKy.ID;
                msg.LoaiHS = LoaiKhaiBao.DinhMuc;
                if (msg.Load())
                {
                    lblTrangThai.Text = setText("Chờ xác nhận ", "Wait for confirmation");
                    cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                }
            }
            else
            {
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            setCommandStatus();
        }
        public void setCommandStatus()
        {
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = false;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                lblTrangThai.Text = dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Wait for approval") :
                   setText("Chờ hủy", "Wait for cancel");
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? Janus.Windows.UI.InheritableBoolean.True :
                    Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                lblTrangThai.Text = dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET ? setText("Không phê duyệt", "Not approval") :
                    setText("Đã hủy", "Canceled");
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = true;
            }
            else
            {
                lblTrangThai.Text = dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO? setText("Chưa khai báo", "Not declared yet"): setText("Sửa định mức","");
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True; 
            }
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Bind dữ liệu vào combobox hợp đồng
        /// </summary>
        private void BindHopDong()
        {

        }
        private void LaySoTiepNhanDT()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            {
                sendXML.LoaiHS = "DMHD";
                sendXML.master_id = dmDangKy.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //ShowMessage("Định mức không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    return;
                }
            }

            WSForm wsForm = new WSForm();
            string password = "";
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = dmDangKy.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        else
                        {
                            HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", dmDangKy.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmDangKy.SoTiepNhan, false);
                    lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                    txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702042");
                    //ShowMessage("Đã hủy hợp đồng này", false);
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                    txtSoTiepNhan.Text = "";
                }
                else if (sendXML.func == 2)
                {
                    if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                        lblTrangThai.Text = setText("Đã duyệt", "Approved");
                    }
                    else if (dmDangKy.TrangThaiXuLy == (int)TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                        //ShowMessage("Hải quan chưa xử lý danh sách sản phẩm này!", false);
                    }
                    else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                        lblTrangThai.Text = setText("Không phê duyệt", " Not approved");
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin định mức hợp đồng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            try
            {

                sendXML.LoaiHS = "DMHD";
                sendXML.master_id = dmDangKy.ID;
                sendXML.Load();

                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 12/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, dmDangKy.GUIDSTR));

                ////Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI.Trim();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI");

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN + " ";
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = dmDangKy.GUIDSTR.Trim().ToUpper();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = dmDangKy.LayPhanHoi(pass, xml.InnerXml);
                //xmlCurrent = dmDangKy.TQDTLayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        else
                        {
                            HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", dmDangKy.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmDangKy.SoTiepNhan, false);
                    lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                    txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702041");
                    //ShowMessage("Đã hủy khai báo định mức này", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {

                        string mess = "";

                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số chứng từ là",, false);
                        lblTrangThai.Text = setText("Đã duyệt", "Approved");
                    }
                    else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                        //ShowMessage("Hải quan chưa xử lý danh sách sản phẩm này!", false);
                    }
                    //DATLMQ bổ sung nhận dữ liệu từ chối
                    else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        ShowMessage("Hải quan từ chối tiếp nhận.\nLý do: " + FontConverter.TCVN2Unicode(DinhMucDangKy.strTuChoi), false);
                        txtSoTiepNhan.Text = "";
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin định mức hợp đồng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        /// 
        private void send()
        {

            if (dmDangKy.DMCollection.Count == 0)
            {
                showMsg("MSG_2702043");
                //ShowMessage("Chưa nhập thông tin dịnh mức.", false);
                return;
            }
            HD = HopDong.Load(HD.ID);

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                showMsg("MSG_240235");
                //ShowMessage("Hợp đồng này chưa được khai báo", false);
                return;
            }
            WSForm wsForm = new WSForm();
            string password = "";
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = dmDangKy.WSSend(password);
                this.Cursor = Cursors.Default;

                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "DMHD";
                sendXML.master_id = dmDangKy.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo định mức . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        public void Huy()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "DMHD";
            sendXML.master_id = dmDangKy.ID;
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                //ShowMessage("Định mức đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            WSForm wsForm = new WSForm();
            string password = "";
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = dmDangKy.WSCancel(password);
                this.Cursor = Cursors.Default;

                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "DMHD";
                sendXML.master_id = dmDangKy.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy thông tin định mức . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới định mức.
        /// </summary>
        private void add()
        {
            DinhMucGCEditForm f = new DinhMucGCEditForm();
            f.dmdk = dmDangKy;
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }


        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                //e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                Company.GC.BLL.KDT.GC.DinhMuc dm = (Company.GC.BLL.KDT.GC.DinhMuc)e.Row.DataRow;
                DinhMucGCEditForm f = new DinhMucGCEditForm();
                f.dmdk = dmDangKy;
                f.DMDetail = dm;
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY || dmDangKy.TrangThaiXuLy==TrangThaiXuLy.DA_DUYET)
                    f.OpenType = OpenFormType.View;
                else
                    f.OpenType = OpenFormType.Edit;
                f.ShowDialog();
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void save()
        {
            try
            {
                HD = HopDong.Load(HD.ID);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                showMsg("MSG_240205");
                //ShowMessage("Không có hợp đồng này", false);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (dmDangKy.DMCollection.Count > 0)
                {
                    // Master.                   
                    //this.dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.dmDangKy.MaHaiQuan = HD.MaHaiQuan;
                    // this.dmDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;

                    if (string.IsNullOrEmpty(this.dmDangKy.GUIDSTR))
                        this.dmDangKy.GUIDSTR = Guid.NewGuid().ToString();

                    dmDangKy.InsertUpdateFull();

                    #region Lưu log thao tác
                    if (this.OpenType == OpenFormType.Insert)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", dmDangKy.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.DinhMuc);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                        else
                        {
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                            log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.DinhMuc;
                            log.ID_DK = dmDangKy.ID;
                            log.GUIDSTR_DK = "";
                            log.UserNameKhaiBao = GlobalSettings.UserLog;
                            log.NgayKhaiBao = DateTime.Now;
                            log.UserNameSuaDoi = GlobalSettings.UserLog;
                            log.NgaySuaDoi = DateTime.Now;
                            log.GhiChu = "";
                            log.IsDelete = false;
                            log.Insert();
                        }
                    }
                    else if (this.OpenType == OpenFormType.Edit)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", dmDangKy.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.DinhMuc);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                    }
                    #endregion

                    if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        showMsg("MSG_SAV02");
                    //ShowMessage("Cập nhật thành công", false);

                    setCommandStatus();
                }
                else
                {
                    showMsg("MSG_2702044");
                    //ShowMessage("Bạn chưa chọn định mức", false);

                }
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //MessageBox.Show("Có lỗi:" + ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void btnInDMSP_Click()
        {
            if (dmDangKy.DMCollection.Count > 0 && dmDangKy.ID > 0)
            {
                Report.ReportViewDinhMucKDTForm reportForm = new Company.Interface.Report.ReportViewDinhMucKDTForm();
                reportForm.DMDangKy = dmDangKy;
                reportForm.Show();
            }
        }
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdSend":
                    this.SendV3();
                    break;
                case "HuyKhaiBao":
                    this.CanceldV3();
                    //this.Huy();
                    break;
                case "NhanDuLieu":
                    this.FeedBackV3();
                    // this.NhanDuLieu12();
                    break;
                case "XacNhan":
                    this.FeedBackV3();
                    //this.LaySoTiepNhanDT();
                    break;
                case "cmdThemExcel":
                    this.addExcel();
                    break;
                case "cmdCopyDM":
                    this.copyDM();
                    break;
                case "DinhMuc":
                    this.btnInDMSP_Click();
                    break;
                case "TaoMoiDM":
                    this.TaoDanhSachDMMoi();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdSuaDM":
                    this.ChuyenTrangThaiSua();
                    break;
            }
        }
        private void inPhieuTN()
        {
            if (this.dmDangKy.SoTiepNhan == 0) return;
            Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
            phieuTN.phieu = "ĐỊNH MỨC";
            phieuTN.soTN = this.dmDangKy.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.dmDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = dmDangKy.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void TaoDanhSachDMMoi()
        {
            foreach (DinhMuc dm in dmDangKy.DMCollection)
            {
                if (dm.ID == 0)
                {
                    dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    dmDangKy.MaHaiQuan = HD.MaHaiQuan;
                    if (this.ShowMessage("Bạn có muốn lưu định mức hiện tại lại không?", true) == "Yes")
                    {
                        try
                        {
                            dmDangKy.InsertUpdateFull();
                        }
                        catch (Exception ex)
                        {
                            showMsg("MSG_2702004", ex.Message);
                        }
                    }
                    else
                        break;
                }
            }
            dmDangKy.DMCollection.Clear();
            dmDangKy.ID = 0;
            dmDangKy.NgayTiepNhan = new DateTime(1900, 1, 1);
            dmDangKy.SoTiepNhan = 0;
            dmDangKy.TrangThaiXuLy = -1;
            txtSoTiepNhan.Text = "0";
            try
            {
                dgList.DataSource = dmDangKy.DMCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            setCommandStatus();
        }
        private void copyDM()
        {
            DinhMucGCRegistedForm f = new DinhMucGCRegistedForm();
            f.ShowDialog();
            if (f.idHD != 0)
            {
                long idHopDong = f.idHD;
                DinhMuc dm = new DinhMuc();
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                Company.GC.BLL.GC.SanPhamCollection spcol = new Company.GC.BLL.GC.SanPhamCollection();
                spcol = sp.SelectCollectionBy_HopDong_ID_ChuaCoDM();
                DinhMucCollection dmCollection = new DinhMucCollection();
                dmCollection = dm.copyDinhMuc(idHopDong, spcol);
                if (dmCollection.Count == 0)
                {
                    showMsg("MSG_2702045");
                    //ShowMessage("Không có định mức sản phẩm trong hợp đồng này.", false);
                }
                else
                {
                    dmDangKy.DMCollection = dmCollection;
                    dgList.DataSource = dmDangKy.DMCollection;
                }
            }
        }
        private void addExcel()
        {
            ImportKDTDMForm f = new ImportKDTDMForm();
            f.HD = this.HD;
            f.dmDangKy = this.dmDangKy;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void NhanDuLieu12()
        {
            var sendXml = new Company.GC.BLL.KDT.SXXK.MsgSend { LoaiHS = "DMHD", master_id = dmDangKy.ID };
            if (sendXml.Load())
            {
                showMsg("MSG_WRN05");
                //ShowMessage("Định mức đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            var HD = new HopDong { ID = dmDangKy.ID_HopDong };
            HD = HopDong.Load(HD.ID);

            if (HD.TrangThaiXuLy != 1)
            {
                showMsg("MSG_2702046");
                //ShowMessage("Hợp đồng của định mức này chưa được duyệt.\nHãy nhận trạng thái của hợp đồng trước.", false);
                return;
            }
            var wsForm = new WSForm();
            string password = "";
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                //DATLMQ comment 09/01/2011
                //xmlCurrent = dmDangKy.WSDownLoad(password);
                this.Cursor = Cursors.Default;

                sendXml = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXml.LoaiHS = "DMHD";
                sendXml.master_id = dmDangKy.ID;
                sendXml.msg = xmlCurrent;
                sendXml.func = 2;
                xmlCurrent = "";
                sendXml.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận thông tin định mức hợp đồng . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.deleteDM();

        }

        private void DinhMucGCSendForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void btnXoa_Click_1(object sender, EventArgs e)
        {
            Company.GC.BLL.KDT.GC.DinhMucCollection dmDeleteColl = new DinhMucCollection();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn muốn xóa các định mức đã chọn?", true) == "Yes")
            {
                foreach (GridEXSelectedItem item in items)
                {
                    if (item.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.DinhMuc dmDelte = (Company.GC.BLL.KDT.GC.DinhMuc)item.GetRow().DataRow;
                        if (dmDelte.ID > 0) dmDelte.Delete(HD.ID);
                        dmDeleteColl.Add(dmDelte);
                    }

                }
                foreach (DinhMuc dm in dmDeleteColl)
                {
                    dmDangKy.DMCollection.Remove(dm);

                }
                try
                {
                    dgList.DataSource = dmDangKy.DMCollection;
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
                showMsg("MSG_2702001");
                //ShowMessage("Cập nhật thành công.", false);
            }

        }
        private void deleteDM()
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa định mức này không?", true) == "Yes")
            {
                GridEXRow[] items = dgList.GetCheckedRows();
                foreach (GridEXRow row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.DinhMuc dmDelte = (Company.GC.BLL.KDT.GC.DinhMuc)row.DataRow;
                        if (dmDelte.ID > 0)
                            dmDelte.Delete(HD.ID);
                    }
                }
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = dmDangKy.ID;
            form.DeclarationIssuer = DeclarationIssuer.GC_DINH_MUC;
            form.ShowDialog(this);
        }
        #region V3 - 13/03/2012
        private void SendV3()
        {
            if (dmDangKy.DMCollection.Count == 0)
            {
                showMsg("MSG_2702043");
                //ShowMessage("Chưa nhập thông tin dịnh mức.", false);
                return;
            }
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                this.save();
            HD = HopDong.Load(HD.ID);

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                showMsg("MSG_240235");
                //ShowMessage("Hợp đồng này chưa được khai báo", false);
                return;
            }
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (sendXML.Load())
            {
                ShowMessage("Khai báo đã được gửi đến hải quan. Nhấn nút [Lấy phản hồi] để nhận thông tin từ hải quan", false);
                return;
            }
            try
            {
                dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                Company.KDT.SHARE.Components.GC_DinhMuc dm = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferDinhMuc(dmDangKy, HD);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = dmDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                                     Identity = dmDangKy.MaHaiQuan
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_DINH_MUC,
                                    Function = dm.Function,
                                    Reference = dmDangKy.GUIDSTR,
                                }
                                ,
                                dm);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                        sendForm.Message.XmlSaveMessage(dmDangKy.ID, MessageTitle.KhaiBaoDinhMucSua);
                    else
                        sendForm.Message.XmlSaveMessage(dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoDinhMuc);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                    sendXML.master_id = dmDangKy.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    setCommandStatus();
                    dmDangKy.Update();
                    FeedBackV3();
                    XacNhan.Enabled = XacNhan1.Enabled = NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                {
                    setCommandStatus();
                    sendForm.Message.XmlSaveMessage(dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQTuChoiDinhMuc);
                    ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(dmDangKy.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhMucSendHandler(dmDangKy, ref msgInfor, e);

        }
        private void FeedBackV3()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Khai báo chưa được gửi đến hải quan. Nhấn nút [Khai báo] để gửi thông tin đến hải quan", false);
                return;
            }
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_DINH_MUC,
                Reference = dmDangKy.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_DINH_MUC,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = dmDangKy.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan.Trim()),
                                              Identity = dmDangKy.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    {
                        setCommandStatus();
                    }
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                }
            }
        }
        private void CanceldV3()
        {
            //Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            //sendXML.master_id = dmDangKy.ID;
            //if (sendXML.Load())
            //{
            //    showMsg("MSG_WRN05");
            //    //ShowMessage("Định mức đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
            //    return;
            //}
            Company.KDT.SHARE.Components.DeclarationBase dinhmuc = Company.GC.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.GC_DINH_MUC, dmDangKy.GUIDSTR, dmDangKy.SoTiepNhan, dmDangKy.MaHaiQuan, dmDangKy.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = dmDangKy.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                                 Identity = dmDangKy.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_DINH_MUC,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = dmDangKy.GUIDSTR,
                            }
                            ,
                            dinhmuc);
            SendMessageForm sendForm = new SendMessageForm();
            dmDangKy.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                sendForm.Message.XmlSaveMessage(dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.HQHuyKhaiBaoToKhai);
                //sendXML.func = 3;
                //sendXML.msg = msgSend;
                //sendXML.InsertUpdate();
                dmDangKy.Update();
                FeedBackV3();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }

        #endregion
        private void ChuyenTrangThaiSua()
        {
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                if (ShowMessage("Bạn có muốn chuyển định mức sang trạng thái sửa\r\n" + "Số tiếp nhận: " + dmDangKy.SoTiepNhan.ToString(), true) == "Yes")
                {
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    dmDangKy.DeleteDinhMucGC();
                    Company.KDT.SHARE.Components.Globals.SaveMessage("", dmDangKy.ID, dmDangKy.GUIDSTR, (MessageTypes)int.Parse(DeclarationIssuer.GC_DINH_MUC), MessageFunctions.SuaToKhai, "Chuyển trạng thái sửa", "Chuyển khai báo định mức sang trạng thái sửa");
                }
            }
            else
                ShowMessage("Định mức chưa được duyệt", false);
            this.setCommandStatus();
        }

    }
}