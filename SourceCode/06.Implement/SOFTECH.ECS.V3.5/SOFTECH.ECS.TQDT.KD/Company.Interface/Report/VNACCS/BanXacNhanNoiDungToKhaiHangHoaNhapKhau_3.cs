using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanXacNhanNoiDungToKhaiHangHoaNhapKhau_3 : DevExpress.XtraReports.UI.XtraReport
    {
        public int spl;
        public BanXacNhanNoiDungToKhaiHangHoaNhapKhau_3()
        {
            InitializeComponent();
        }

      
        public void BindingReport(VAD1AC0 vad1ac)
        {
            lblTongSoTrang.Text =  vad1ac.B12.GetValue().ToString().ToUpper();
            lblSoToKhai.Text = vad1ac.ICN.GetValue().ToString().ToUpper();

            lblSoToKhaiDauTien.Text = vad1ac.FIC.GetValue().ToString().ToUpper();
            lblSoNhanhToKhaiChiaNho.Text = vad1ac.BNO.GetValue().ToString().ToUpper();
            lblTongSoToKhaiChiaNho.Text = vad1ac.DNO.GetValue().ToString().ToUpper();
            lblSoToKhaiTamNhapTaiXuat.Text = vad1ac.TDN.GetValue().ToString().ToUpper();
            lblMaPhanLoaiKiemTra.Text = vad1ac.A06.GetValue().ToString().ToUpper();
            lblMaLoaiHinh.Text = vad1ac.ICB.GetValue().ToString().ToUpper();
            lblMaPhanLoaiHangHoa.Text = vad1ac.CCC.GetValue().ToString().ToUpper();
            lblMaHieuPhuongThucVanChuyen.Text = vad1ac.MTC.GetValue().ToString().ToUpper();
            lblPhanLoaiCaNhanToChuc.Text = vad1ac.SKB.GetValue().ToString().ToUpper();
            lblMaSoHangHoaDaiDienToKhai.Text = vad1ac.A00.GetValue().ToString().ToUpper();
            lblTenCoQuanHaiQuanTiepNhanToKhai.Text = vad1ac.A07.GetValue().ToString().ToUpper();
            lblMaBoPhanXuLyToKhai.Text = vad1ac.CHB.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ac.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDangKy.Text = Convert.ToDateTime(vad1ac.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDangKy.Text = "";
            }
            if (vad1ac.AD1.GetValue().ToString().ToUpper() != "" && vad1ac.AD1.GetValue().ToString().ToUpper() != "0")
            {
                lblGioDangKy.Text = vad1ac.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ac.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayThayDoiDangKy.Text = Convert.ToDateTime(vad1ac.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayThayDoiDangKy.Text = "";
            }
            if (vad1ac.AD3.GetValue().ToString().ToUpper() != "" && vad1ac.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioThayDoiDangKy.Text = vad1ac.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioThayDoiDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ac.RED.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(vad1ac.RED.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanTaiNhapTaiXuat.Text = "";
            }
            lblBieuThiTHHetHan.Text = vad1ac.AAA.GetValue().ToString().ToUpper();
            //BindingReportHang(vad1ac.HangMD[spl]);
            DataTable dt = new DataTable();
            if (vad1ac.HangMD != null && vad1ac.HangMD.Count > 0)
            {
                int STT = 0;
                
                dt.Columns.Add("SoDong", typeof(string));
                dt.Columns.Add("MaSoHH", typeof(string));
                dt.Columns.Add("MaQuanLyRieng", typeof(string));
                dt.Columns.Add("MaPhanLoaiTaiXN", typeof(string));
                dt.Columns.Add("MoTaHH", typeof(string));
                dt.Columns.Add("SoLuong1", typeof(string));
                dt.Columns.Add("MaDVT1", typeof(string));
                dt.Columns.Add("SoKhoanMucDC1", typeof(string));
                dt.Columns.Add("SoKhoanMucDC2", typeof(string));
                dt.Columns.Add("SoKhoanMucDC3", typeof(string));
                dt.Columns.Add("SoKhoanMucDC4", typeof(string));
                dt.Columns.Add("SoKhoanMucDC5", typeof(string));
                dt.Columns.Add("SoLuong2", typeof(string));
                dt.Columns.Add("MaDVT2", typeof(string));
                dt.Columns.Add("TriGiaHD", typeof(string));
                dt.Columns.Add("DonGiaHD", typeof(string));
                dt.Columns.Add("MaDongTienCuaDonGia", typeof(string));
                dt.Columns.Add("DonViCuaDonGiaVaSoLuong", typeof(string));
                dt.Columns.Add("TriGiaTinhThueS", typeof(string));
                dt.Columns.Add("MaDongTienCuaTGTT", typeof(string));
                dt.Columns.Add("TriGiaTinhThueM", typeof(string));
                dt.Columns.Add("SoLuongTinhThue", typeof(string));
                dt.Columns.Add("MaDonViTinhChuanDanhThue", typeof(string));
                dt.Columns.Add("DonGiaTinhThue", typeof(string));
                dt.Columns.Add("DonViSoLuongTrongDonGiaTinhThue", typeof(string));
                dt.Columns.Add("MaPhanLoaiThueSuat", typeof(string));
                dt.Columns.Add("ThueSuatThueNhapKhau", typeof(string));
                dt.Columns.Add("PhanLoaiNhapThueSuat", typeof(string));
                dt.Columns.Add("MaXacDinhMucThueNhapKhau", typeof(string));
                dt.Columns.Add("SoTienThueNK", typeof(string));
                dt.Columns.Add("MaNuocXuatXu", typeof(string));
                dt.Columns.Add("TenNoiXuatXu", typeof(string));
                dt.Columns.Add("MaBieuThueNK", typeof(string));
                dt.Columns.Add("SoTienGiamThueNK", typeof(string));
                dt.Columns.Add("MaNgoaiHanNgach", typeof(string));
                dt.Columns.Add("SoThuTuDongHangToKhai", typeof(string));
                dt.Columns.Add("SoDKDanhMucMienThue", typeof(string));
                dt.Columns.Add("SoDongTuongUngDMMienThue", typeof(string));
                dt.Columns.Add("MaMienGiamThueNK", typeof(string));
                dt.Columns.Add("DieuKhoanMienGiam", typeof(string));
                dt.Columns.Add("TenKhoanMucThueVaThuKhac1", typeof(string));
                dt.Columns.Add("MaApDungThueSuatThueVaThuKhac1", typeof(string));
                dt.Columns.Add("TriGiaTinhThueVaThuKhac1", typeof(string));
                dt.Columns.Add("SoLuongTinhThueVaThuKhac1", typeof(string));
                dt.Columns.Add("MaDonViTinhChuanDanhThueVaThuKhac1", typeof(string));
                dt.Columns.Add("ThueSuatThueVaThuKhac1", typeof(string));
                dt.Columns.Add("SoTienThueVaThuKhac1", typeof(string));
                dt.Columns.Add("MaMienGiamThueVaThuKhac1", typeof(string));
                dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac1", typeof(string));
                dt.Columns.Add("SoTienGiamThueVaThuKhac1", typeof(string));
                dt.Columns.Add("TenKhoanMucThueVaThuKhac2", typeof(string));
                dt.Columns.Add("MaApDungThueSuatThueVaThuKhac2", typeof(string));
                dt.Columns.Add("TriGiaTinhThueVaThuKhac2", typeof(string));
                dt.Columns.Add("SoLuongTinhThueVaThuKhac2", typeof(string));
                dt.Columns.Add("MaDonViTinhChuanDanhThueVaThuKhac2", typeof(string));
                dt.Columns.Add("ThueSuatThueVaThuKhac2", typeof(string));
                dt.Columns.Add("SoTienThueVaThuKhac2", typeof(string));
                dt.Columns.Add("MaMienGiamThueVaThuKhac2", typeof(string));
                dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac2", typeof(string));
                dt.Columns.Add("SoTienGiamThueVaThuKhac2", typeof(string));
                dt.Columns.Add("TenKhoanMucThueVaThuKhac3", typeof(string));
                dt.Columns.Add("MaApDungThueSuatThueVaThuKhac3", typeof(string));
                dt.Columns.Add("TriGiaTinhThueVaThuKhac3", typeof(string));
                dt.Columns.Add("SoLuongTinhThueVaThuKhac3", typeof(string));
                dt.Columns.Add("MaDonViTinhChuanDanhThueVaThuKhac3", typeof(string));
                dt.Columns.Add("ThueSuatThueVaThuKhac3", typeof(string));
                dt.Columns.Add("SoTienThueVaThuKhac3", typeof(string));
                dt.Columns.Add("MaMienGiamThueVaThuKhac3", typeof(string));
                dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac3", typeof(string));
                dt.Columns.Add("SoTienGiamThueVaThuKhac3", typeof(string));
                dt.Columns.Add("TenKhoanMucThueVaThuKhac4", typeof(string));
                dt.Columns.Add("MaApDungThueSuatThueVaThuKhac4", typeof(string));
                dt.Columns.Add("TriGiaTinhThueVaThuKhac4", typeof(string));
                dt.Columns.Add("SoLuongTinhThueVaThuKhac4", typeof(string));
                dt.Columns.Add("MaDonViTinhChuanDanhThueVaThuKhac4", typeof(string));
                dt.Columns.Add("ThueSuatThueVaThuKhac4", typeof(string));
                dt.Columns.Add("SoTienThueVaThuKhac4", typeof(string));
                dt.Columns.Add("MaMienGiamThueVaThuKhac4", typeof(string));
                dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac4", typeof(string));
                dt.Columns.Add("SoTienGiamThueVaThuKhac4", typeof(string));
                dt.Columns.Add("TenKhoanMucThueVaThuKhac5", typeof(string));
                dt.Columns.Add("MaApDungThueSuatThueVaThuKhac5", typeof(string));
                dt.Columns.Add("TriGiaTinhThueVaThuKhac5", typeof(string));
                dt.Columns.Add("SoLuongTinhThueVaThuKhac5", typeof(string));
                dt.Columns.Add("MaDonViTinhChuanDanhThueVaThuKhac5", typeof(string));
                dt.Columns.Add("ThueSuatThueVaThuKhac5", typeof(string));
                dt.Columns.Add("SoTienThueVaThuKhac5", typeof(string));
                dt.Columns.Add("MaMienGiamThueVaThuKhac5", typeof(string));
                dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac5", typeof(string));
                dt.Columns.Add("SoTienGiamThueVaThuKhac5", typeof(string));

                for (int i = 0; i < vad1ac.HangMD.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();

                    dr["SoDong"] = vad1ac.HangMD[i].B25.GetValue().ToString().Trim();
                    //dr["STT"] = STT.ToString().Trim();
                    dr["MaSoHH"] = vad1ac.HangMD[i].CMD.GetValue().ToString().Trim();
                    dr["MaQuanLyRieng"] = vad1ac.HangMD[i].GZC.GetValue().ToString().Trim();
                    dr["MaPhanLoaiTaiXN"] = vad1ac.HangMD[i].B29.GetValue().ToString().Trim();
                    dr["MoTaHH"] = vad1ac.HangMD[i].CMN.GetValue().ToString().Trim();
                    dr["SoLuong1"] = vad1ac.HangMD[i].QN1.GetValue().ToString().Trim();
                    dr["MaDVT1"] = vad1ac.HangMD[i].QT1.GetValue().ToString().Trim();
                    for (int j = 0; j < 5; j++)
                    {
                        dr["SoKhoanMucDC" + (j + 1)] = vad1ac.HangMD[i].VN_.listAttribute[0].GetValueCollection(j).ToString().Trim();
                    }
                    dr["SoLuong2"] = vad1ac.HangMD[i].QN2.GetValue().ToString().Trim();
                    dr["MaDVT2"] = vad1ac.HangMD[i].QT2.GetValue().ToString().Trim();
                    dr["TriGiaHD"] = vad1ac.HangMD[i].BPR.GetValue().ToString().Trim();
                    dr["DonGiaHD"] = vad1ac.HangMD[i].UPR.GetValue().ToString().Trim();
                    dr["MaDongTienCuaDonGia"] = vad1ac.HangMD[i].UPC.GetValue().ToString().Trim();
                    dr["DonViCuaDonGiaVaSoLuong"] = vad1ac.HangMD[i].TSC.GetValue().ToString().Trim();
                    dr["TriGiaTinhThueS"] = vad1ac.HangMD[i].B36.GetValue().ToString().Trim();
                    dr["MaDongTienCuaTGTT"] = vad1ac.HangMD[i].B50.GetValue().ToString().Trim();
                    dr["TriGiaTinhThueM"] = vad1ac.HangMD[i].B51.GetValue().ToString().Trim();
                    dr["SoLuongTinhThue"] = vad1ac.HangMD[i].B37.GetValue().ToString().Trim();
                    dr["MaDonViTinhChuanDanhThue"] = vad1ac.HangMD[i].B38.GetValue().ToString().Trim();
                    dr["DonGiaTinhThue"] = vad1ac.HangMD[i].KKT.GetValue().ToString().Trim();
                    dr["DonViSoLuongTrongDonGiaTinhThue"] = vad1ac.HangMD[i].KKS.GetValue().ToString().Trim();
                    dr["MaPhanLoaiThueSuat"] = vad1ac.HangMD[i].B42.GetValue().ToString().Trim();
                    dr["ThueSuatThueNhapKhau"] = vad1ac.HangMD[i].B43.GetValue().ToString().Trim();
                    dr["PhanLoaiNhapThueSuat"] = vad1ac.HangMD[i].SKB.GetValue().ToString().Trim();
                    dr["MaXacDinhMucThueNhapKhau"] = vad1ac.HangMD[i].SPD.GetValue().ToString().Trim();
                    dr["SoTienThueNK"] = vad1ac.HangMD[i].B47.GetValue().ToString().Trim();
                    dr["MaNuocXuatXu"] = vad1ac.HangMD[i].OR.GetValue().ToString().Trim();
                    dr["TenNoiXuatXu"] = vad1ac.HangMD[i].B56.GetValue().ToString().Trim();
                    dr["MaBieuThueNK"] = vad1ac.HangMD[i].ORS.GetValue().ToString().Trim();
                    dr["SoTienGiamThueNK"] = vad1ac.HangMD[i].B49.GetValue().ToString().Trim();
                    dr["MaNgoaiHanNgach"] = vad1ac.HangMD[i].KWS.GetValue().ToString().Trim();
                    dr["SoThuTuDongHangToKhai"] = vad1ac.HangMD[i].TDL.GetValue().ToString().Trim();
                    dr["SoDKDanhMucMienThue"] = vad1ac.HangMD[i].TXN.GetValue().ToString().Trim();
                    dr["SoDongTuongUngDMMienThue"] = vad1ac.HangMD[i].TXR.GetValue().ToString().Trim();
                    dr["MaMienGiamThueNK"] = vad1ac.HangMD[i].RE.GetValue().ToString().Trim();
                    dr["DieuKhoanMienGiam"] = vad1ac.HangMD[i].B59.GetValue().ToString().Trim();
                    for (int j = 0; j < 5; j++)
                    {
                        dr["TenKhoanMucThueVaThuKhac" + (j + 1)] = vad1ac.HangMD[i].KQ1.listAttribute[0].GetValueCollection(j).ToString().Trim();
                        dr["MaApDungThueSuatThueVaThuKhac" + (j + 1)] = vad1ac.HangMD[i].KQ1.listAttribute[1].GetValueCollection(j).ToString().Trim();
                        dr["TriGiaTinhThueVaThuKhac" + (j + 1)] = vad1ac.HangMD[i].KQ1.listAttribute[2].GetValueCollection(j).ToString().Trim();
                        dr["SoLuongTinhThueVaThuKhac" + (j + 1)] = vad1ac.HangMD[i].KQ1.listAttribute[3].GetValueCollection(j).ToString().Trim();
                        dr["MaDonViTinhChuanDanhThueVaThuKhac" + (j + 1)] = vad1ac.HangMD[i].KQ1.listAttribute[4].GetValueCollection(j).ToString().Trim();
                        dr["ThueSuatThueVaThuKhac" + (j + 1)] = vad1ac.HangMD[i].KQ1.listAttribute[5].GetValueCollection(j).ToString().Trim();
                        dr["SoTienThueVaThuKhac" + (j + 1)] = vad1ac.HangMD[i].KQ1.listAttribute[6].GetValueCollection(j).ToString().Trim();
                        dr["MaMienGiamThueVaThuKhac" + (j + 1)] = vad1ac.HangMD[i].KQ1.listAttribute[7].GetValueCollection(j).ToString().Trim();
                        dr["DieuKhoanMienGiamThueVaThuKhac" + (j + 1)] = vad1ac.HangMD[i].KQ1.listAttribute[8].GetValueCollection(j).ToString().Trim();
                        dr["SoTienGiamThueVaThuKhac" + (j + 1)] = vad1ac.HangMD[i].KQ1.listAttribute[9].GetValueCollection(j).ToString().Trim();                    
                    }
                    dt.Rows.Add(dr);
                }
                BindingReportHang(dt);
              
               
            }      
 
        }

        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSoDong.DataBindings.Add("Text", DetailReport.DataSource, "SoDong");
            lblMaSoHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaSoHH");
            lblMaQuanLyRieng.DataBindings.Add("Text", DetailReport.DataSource, "MaQuanLyRieng");
            lblMaPhanLoaiTaiXacNhanGia.DataBindings.Add("Text", DetailReport.DataSource, "MaPhanLoaiTaiXN");
            lblMoTaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MoTaHH");
            lblSoLuong1.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong1");
            lblMaDonViTinh1.DataBindings.Add("Text", DetailReport.DataSource, "MaDVT1");
            lblSoMucKhaiKhoangDieuChinh1.DataBindings.Add("Text", DetailReport.DataSource, "SoKhoanMucDC1");
            lblSoMucKhaiKhoangDieuChinh2.DataBindings.Add("Text", DetailReport.DataSource, "SoKhoanMucDC2");
            lblSoMucKhaiKhoangDieuChinh3.DataBindings.Add("Text", DetailReport.DataSource, "SoKhoanMucDC3");
            lblSoMucKhaiKhoangDieuChinh4.DataBindings.Add("Text", DetailReport.DataSource, "SoKhoanMucDC4");
            lblSoMucKhaiKhoangDieuChinh5.DataBindings.Add("Text", DetailReport.DataSource, "SoKhoanMucDC5");
            lblSoLuong2.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong2");
            lblMaDonViTinh2.DataBindings.Add("Text", DetailReport.DataSource, "MaDVT2");
            lblTriGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaHD");
            lblDonGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "DonGiaHD");
            lblMaDongTienCuaDonGia.DataBindings.Add("Text", DetailReport.DataSource, "MaDongTienCuaDonGia");
            lblDonViCuaDonGiaVaSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "DonViCuaDonGiaVaSoLuong");
            lblTriGiaTinhThueS.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueS");
            lblMaDongTienCuaGiaTinhThue.DataBindings.Add("Text", DetailReport.DataSource, "MaDongTienCuaTGTT");
            lblTriGiaTinhThueM.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueM");
            lblSoLuongTinhThue.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThue");
            lblMaDonViTinhChuanDanhThue.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThue");
            lblDonGiaTinhThue.DataBindings.Add("Text", DetailReport.DataSource, "DonGiaTinhThue");
            lblDonViSoLuongTrongDonGiaTinhThue.DataBindings.Add("Text", DetailReport.DataSource, "DonViSoLuongTrongDonGiaTinhThue");
            lblMaPhanLoaiThueSuat.DataBindings.Add("Text", DetailReport.DataSource, "MaPhanLoaiThueSuat");
            lblThueSuatThueNhapKhau.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueNhapKhau");
            lblPhanLoaiNhapThueSuat.DataBindings.Add("Text", DetailReport.DataSource, "PhanLoaiNhapThueSuat");
            lblMaXacDinhMucThueNhapKhau.DataBindings.Add("Text", DetailReport.DataSource, "MaXacDinhMucThueNhapKhau");
            lblSoTienThueNK.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueNK");
            lblMaNuocXuatXu.DataBindings.Add("Text", DetailReport.DataSource, "MaNuocXuatXu");
            lblTenNoiXuatXu.DataBindings.Add("Text", DetailReport.DataSource, "TenNoiXuatXu");
            lblMaBieuThueNK.DataBindings.Add("Text", DetailReport.DataSource, "MaBieuThueNK");
            lblSoTienGiamThueNK.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueNK");
            lblMaNgoaiHanNgach.DataBindings.Add("Text", DetailReport.DataSource, "MaNgoaiHanNgach");
            lblSoThuTuDongHangToKhai.DataBindings.Add("Text", DetailReport.DataSource, "SoThuTuDongHangToKhai");
            lblSoDKDanhMucMienThue.DataBindings.Add("Text", DetailReport.DataSource, "SoDKDanhMucMienThue");
            lblSoDongTuongUngDMMienThue.DataBindings.Add("Text", DetailReport.DataSource, "SoDongTuongUngDMMienThue");
            lblMaMienGiamThueNK.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueNK");
            lblDieuKhoanMienGiam.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiam");
            lblTenKhoanMucThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucThueVaThuKhac1");
            lblMaApDungThueSuatThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "MaApDungThueSuatThueVaThuKhac1");
            lblTriGiaTinhThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueVaThuKhac1");
            lblSoLuongTinhThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThueVaThuKhac1");
            lblMaDonViTinhChuanDanhThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThueVaThuKhac1");
            lblThueSuatThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueVaThuKhac1");
            lblSoTienThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueVaThuKhac1");
            lblMaMienGiamThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueVaThuKhac1");
            lblDieuKhoanMienGiamThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiamThueVaThuKhac1");
            lblSoTienGiamThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueVaThuKhac1");
            lblTenKhoanMucThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucThueVaThuKhac2");
            lblMaApDungThueSuatThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "MaApDungThueSuatThueVaThuKhac2");
            lblTriGiaTinhThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueVaThuKhac2");
            lblSoLuongTinhThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThueVaThuKhac2");
            lblMaDonViTinhChuanDanhThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThueVaThuKhac2");
            lblThueSuatThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueVaThuKhac2");
            lblSoTienThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueVaThuKhac2");
            lblMaMienGiamThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueVaThuKhac2");
            lblDieuKhoanMienGiamThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiamThueVaThuKhac2");
            lblSoTienGiamThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueVaThuKhac2");
            lblTenKhoanMucThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucThueVaThuKhac3");
            lblMaApDungThueSuatThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "MaApDungThueSuatThueVaThuKhac3");
            lblTriGiaTinhThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueVaThuKhac3");
            lblSoLuongTinhThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThueVaThuKhac3");
            lblMaDonViTinhChuanDanhThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThueVaThuKhac3");
            lblThueSuatThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueVaThuKhac3");
            lblSoTienThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueVaThuKhac3");
            lblMaMienGiamThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueVaThuKhac3");
            lblDieuKhoanMienGiamThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiamThueVaThuKhac3");
            lblSoTienGiamThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueVaThuKhac3");
            lblTenKhoanMucThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucThueVaThuKhac4");
            lblMaApDungThueSuatThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "MaApDungThueSuatThueVaThuKhac4");
            lblTriGiaTinhThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueVaThuKhac4");
            lblSoLuongTinhThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThueVaThuKhac4");
            lblMaDonViTinhChuanDanhThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThueVaThuKhac4");
            lblThueSuatThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueVaThuKhac4");
            lblSoTienThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueVaThuKhac4");
            lblMaMienGiamThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueVaThuKhac4");
            lblDieuKhoanMienGiamThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiamThueVaThuKhac4");
            lblSoTienGiamThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueVaThuKhac4");
            lblTenKhoanMucThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucThueVaThuKhac5");
            lblMaApDungThueSuatThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "MaApDungThueSuatThueVaThuKhac5");
            lblTriGiaTinhThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueVaThuKhac5");
            lblSoLuongTinhThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThueVaThuKhac5");
            lblMaDonViTinhChuanDanhThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThueVaThuKhac5");
            lblThueSuatThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueVaThuKhac5");
            lblSoTienThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueVaThuKhac5");
            lblMaMienGiamThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueVaThuKhac5");
            lblDieuKhoanMienGiamThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiamThueVaThuKhac5");
            lblSoTienGiamThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueVaThuKhac5");
       

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }

        decimal SoTTHang = 0;

    

        private void lblSoTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 3).ToString();
        }


      
    }
}
