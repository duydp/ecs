using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class ThongBaoPheDuyetKhaiBaoVanChuyen_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public ThongBaoPheDuyetKhaiBaoVanChuyen_1()
        {
            InitializeComponent();
        }

        public void BindingReport(VAS5050 vas504)
        {
            int SoCont = vas504.HangHoa.Count;
            if (SoCont <= 20)
                TongSoTrang.Text = System.Convert.ToDecimal(5).ToString();
            else
            {
                TongSoTrang.Text = System.Convert.ToDecimal(5 + Math.Round((decimal)SoCont / 20, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblTenThongTinXuat.Text = vas504.AB.GetValue().ToString();
            lblMaCoBaoYeuCauXN.Text = vas504.KA.GetValue().ToString().ToUpper();
            lblTenCoBaoYeuCauXN.Text = vas504.KB.GetValue().ToString().ToUpper();
            lblCoQuanHQ.Text = vas504.AD.GetValue().ToString().ToUpper();
            lblSoToKhaiVC.Text = vas504.AE.GetValue().ToString().ToUpper();
            lblCoBaoXNK.Text = vas504.ED.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vas504.AF.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayLapTK.Text = Convert.ToDateTime(vas504.AF.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayLapTK.Text = "";
            }
            lblMaNguoiKhai.Text = vas504.AG.GetValue().ToString().ToUpper();
            lblTenNguoiKhai.Text = vas504.AH.GetValue().ToString().ToUpper();
            lblDiaChiNguoiKhai.Text = vas504.AI.GetValue().ToString().ToUpper();
            lblMaNguoiVC.Text = vas504.AL.GetValue().ToString().ToUpper();
            lblTenNguoiVC.Text = vas504.AM.GetValue().ToString().ToUpper();
            lblDiaChiNguoiVC.Text = vas504.AN.GetValue().ToString().ToUpper();
            lblSoHopDongVC.Text = vas504.AO.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vas504.AP.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHopDongVC.Text = Convert.ToDateTime(vas504.AP.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHopDongVC.Text = "";
            }
            if (Convert.ToDateTime(vas504.AQ.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHetHanHD.Text = Convert.ToDateTime(vas504.AQ.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHetHanHD.Text = "";
            }
            lblMaPhuongTienVC1.Text = vas504.AR.GetValue().ToString().ToUpper();
            lblTenPhuongTienVC1.Text = vas504.AS.GetValue().ToString().ToUpper();
            lblMaMucDichVC.Text = vas504.AT.GetValue().ToString().ToUpper();
            lblTenMucDichVC.Text = vas504.AU.GetValue().ToString().ToUpper();
            lblLoaiHinhVanTai.Text = vas504.AV.GetValue().ToString().ToUpper();
            lblTenLoaiHinhVanTai.Text = vas504.AW.GetValue().ToString().ToUpper();
            lblMaDiaDiemXepHang.Text = vas504.AX.GetValue().ToString().ToUpper();
            lblMaViTriXepHang.Text = vas504.AY.GetValue().ToString().ToUpper();
            lblMaCangXepHang.Text = vas504.AZ.GetValue().ToString().ToUpper();
            lblMaXacDinhCangXHKoCo.Text = vas504.BA.GetValue().ToString().ToUpper();
            lblDiaDemXepHang.Text = vas504.ZJ.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vas504.KE.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDiDDXH.Text = Convert.ToDateTime(vas504.KE.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDiDDXH.Text = "";
            }
            for (int i = 0; i < vas504.KC.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaDiaDiemTrungChuyen1.Text = vas504.KC.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenDiaDiemTrungChuyen1.Text = vas504.KC.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        //lblNgayDenDiaDiemTrungChuyen1.Text = vas504.KC.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vas504.KC.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDenDiaDiemTrungChuyen1.Text = Convert.ToDateTime(vas504.KC.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDenDiaDiemTrungChuyen1.Text = "";
                        }
                        if (Convert.ToDateTime(vas504.KC.listAttribute[3].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDiDiaDiemTrungChuyen1.Text = Convert.ToDateTime(vas504.KC.listAttribute[3].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDiDiaDiemTrungChuyen1.Text = "";
                        }
                        //lblNgayDiDiaDiemTrungChuyen1.Text = vas504.KC.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaDiaDiemTrungChuyen2.Text = vas504.KC.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenDiaDiemTrungChuyen2.Text = vas504.KC.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vas504.KC.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDenDiaDiemTrungChuyen2.Text = Convert.ToDateTime(vas504.KC.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDenDiaDiemTrungChuyen2.Text = "";
                        }
                        if (Convert.ToDateTime(vas504.KC.listAttribute[3].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDiDiaDiemTrungChuyen2.Text = Convert.ToDateTime(vas504.KC.listAttribute[3].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDiDiaDiemTrungChuyen2.Text = "";
                        }
                        //lblNgayDenDiaDiemTrungChuyen2.Text = vas504.KC.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        //lblNgayDiDiaDiemTrungChuyen2.Text = vas504.KC.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaDiaDiemTrungChuyen3.Text = vas504.KC.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenDiaDiemTrungChuyen3.Text = vas504.KC.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vas504.KC.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDenDiaDiemTrungChuyen3.Text = Convert.ToDateTime(vas504.KC.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDenDiaDiemTrungChuyen3.Text = "";
                        }
                        if (Convert.ToDateTime(vas504.KC.listAttribute[3].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDiDiaDiemTrungChuyen3.Text = Convert.ToDateTime(vas504.KC.listAttribute[3].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDiDiaDiemTrungChuyen3.Text = "";
                        }
                        //lblNgayDenDiaDiemTrungChuyen3.Text = vas504.KC.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        //lblNgayDiDiaDiemTrungChuyen3.Text = vas504.KC.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;

                }
            }
            lblMaDiaDiemDoHang.Text = vas504.BC.GetValue().ToString().ToUpper();
            lblViTriDoHang.Text = vas504.BD.GetValue().ToString().ToUpper();
            lblCangDoHang.Text = vas504.BE.GetValue().ToString().ToUpper();
            lblMaXacDinhCangDHKoCo.Text = vas504.BF.GetValue().ToString().ToUpper();
            lblDiaDiemDoHang.Text = vas504.BG.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vas504.KH.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDenDDDH.Text = Convert.ToDateTime(vas504.KH.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDenDDDH.Text = "";
            }
            lblTuyenDuongVC.Text = vas504.BH.GetValue().ToString().ToUpper();
            lblLoaiBaoLanh.Text = vas504.FA.GetValue().ToString().ToUpper();
            lblSoTienBaoLanh.Text = vas504.FB.GetValue().ToString().ToUpper();
            lblSoLuongCotTrongTK.Text = vas504.BI.GetValue().ToString().ToUpper();
            lblSoLuongContainer.Text = vas504.BJ.GetValue().ToString().ToUpper();
            lblGhiChu1.Text = vas504.BK.GetValue().ToString().ToUpper();


        }
    }
}
