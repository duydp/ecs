﻿namespace Company.Interface.Controls
{
    partial class XemBangTongHopChungTuThanhToan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XemBangTongHopChungTuThanhToan));
            Janus.Windows.Common.SuperTipSettings superTipSettings2 = new Janus.Windows.Common.SuperTipSettings();
            Janus.Windows.Common.JanusColorScheme janusColorScheme2 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.GridEX.GridEXLayout cboSoHoSo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.btnXemBangTongHopCTTT = new Janus.Windows.EditControls.UIButton();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.cboSoHoSo = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.janusSuperTip1 = new Janus.Windows.Common.JanusSuperTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.cboSoHoSo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnXemBangTongHopCTTT
            // 
            this.btnXemBangTongHopCTTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemBangTongHopCTTT.Image = ((System.Drawing.Image)(resources.GetObject("btnXemBangTongHopCTTT.Image")));
            this.btnXemBangTongHopCTTT.Location = new System.Drawing.Point(327, 2);
            this.btnXemBangTongHopCTTT.Name = "btnXemBangTongHopCTTT";
            this.btnXemBangTongHopCTTT.Size = new System.Drawing.Size(155, 23);
            superTipSettings2.HeaderText = "Chứng từ thanh toán";
            superTipSettings2.ImageListProvider = null;
            superTipSettings2.Text = "Xem bảng tổng hợp chứng từ thanh toán";
            this.janusSuperTip1.SetSuperTip(this.btnXemBangTongHopCTTT, superTipSettings2);
            this.btnXemBangTongHopCTTT.TabIndex = 1;
            this.btnXemBangTongHopCTTT.Text = "Xem bảng tổng hợp...";
            this.btnXemBangTongHopCTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemBangTongHopCTTT.Click += new System.EventHandler(this.btnXemBangTongHopCTTT_Click);
            // 
            // visualStyleManager1
            // 
            janusColorScheme2.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme2.Name = "Ofice2003";
            janusColorScheme2.Office2007CustomColor = System.Drawing.Color.Empty;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme2);
            this.visualStyleManager1.DefaultColorScheme = "Ofice2003";
            // 
            // cboSoHoSo
            // 
            this.cboSoHoSo.DataMember = "ID";
            cboSoHoSo_DesignTimeLayout.LayoutString = resources.GetString("cboSoHoSo_DesignTimeLayout.LayoutString");
            this.cboSoHoSo.DesignTimeLayout = cboSoHoSo_DesignTimeLayout;
            this.cboSoHoSo.DisplayMember = "SoHoSo";
            this.cboSoHoSo.Location = new System.Drawing.Point(56, 3);
            this.cboSoHoSo.Name = "cboSoHoSo";
            this.cboSoHoSo.SelectedIndex = -1;
            this.cboSoHoSo.SelectedItem = null;
            this.cboSoHoSo.Size = new System.Drawing.Size(265, 20);
            this.cboSoHoSo.TabIndex = 0;
            this.cboSoHoSo.ValueMember = "ID";
            this.cboSoHoSo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // janusSuperTip1
            // 
            this.janusSuperTip1.AutoPopDelay = 2000;
            this.janusSuperTip1.ImageList = null;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Số hồ sơ:";
            // 
            // XemBangTongHopChungTuThanhToan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboSoHoSo);
            this.Controls.Add(this.btnXemBangTongHopCTTT);
            this.Name = "XemBangTongHopChungTuThanhToan";
            this.Size = new System.Drawing.Size(485, 28);
            this.Load += new System.EventHandler(this.XemBangTongHopChungTuThanhToan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cboSoHoSo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnXemBangTongHopCTTT;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cboSoHoSo;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private Janus.Windows.Common.JanusSuperTip janusSuperTip1;
        private System.Windows.Forms.Label label1;
    }
}
