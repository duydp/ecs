-- Drop Existing Procedures
IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Insert]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6),
	@SoDinhMuc int,
	@NgayDangKy datetime,
	@NgayApDung datetime,
	@NgayHetHan datetime,
	@ThanhLy int,
	@LenhSanXuat_ID bigint
AS
INSERT INTO [dbo].[t_SXXK_ThongTinDinhMuc]
(
	[MaSanPham],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[NgayHetHan],
	[ThanhLy],
	[LenhSanXuat_ID]
)
VALUES
(
	@MaSanPham,
	@MaDoanhNghiep,
	@MaHaiQuan,
	@SoDinhMuc,
	@NgayDangKy,
	@NgayApDung,
	@NgayHetHan,
	@ThanhLy,
	@LenhSanXuat_ID
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Update]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6),
	@SoDinhMuc int,
	@NgayDangKy datetime,
	@NgayApDung datetime,
	@NgayHetHan datetime,
	@ThanhLy int,
	@LenhSanXuat_ID bigint
AS

UPDATE
	[dbo].[t_SXXK_ThongTinDinhMuc]
SET
	[SoDinhMuc] = @SoDinhMuc,
	[NgayDangKy] = @NgayDangKy,
	[NgayApDung] = @NgayApDung,
	[NgayHetHan] = @NgayHetHan,
	[ThanhLy] = @ThanhLy
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_InsertUpdate]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6),
	@SoDinhMuc int,
	@NgayDangKy datetime,
	@NgayApDung datetime,
	@NgayHetHan datetime,
	@ThanhLy int,
	@LenhSanXuat_ID bigint
AS
IF EXISTS(SELECT [MaSanPham], [MaDoanhNghiep], [MaHaiQuan], [LenhSanXuat_ID] FROM [dbo].[t_SXXK_ThongTinDinhMuc] WHERE [MaSanPham] = @MaSanPham AND [MaDoanhNghiep] = @MaDoanhNghiep AND [MaHaiQuan] = @MaHaiQuan AND [LenhSanXuat_ID] = @LenhSanXuat_ID)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_ThongTinDinhMuc] 
		SET
			[SoDinhMuc] = @SoDinhMuc,
			[NgayDangKy] = @NgayDangKy,
			[NgayApDung] = @NgayApDung,
			[NgayHetHan] = @NgayHetHan,
			[ThanhLy] = @ThanhLy
		WHERE
			[MaSanPham] = @MaSanPham
			AND [MaDoanhNghiep] = @MaDoanhNghiep
			AND [MaHaiQuan] = @MaHaiQuan
			AND [LenhSanXuat_ID] = @LenhSanXuat_ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_ThongTinDinhMuc]
	(
			[MaSanPham],
			[MaDoanhNghiep],
			[MaHaiQuan],
			[SoDinhMuc],
			[NgayDangKy],
			[NgayApDung],
			[NgayHetHan],
			[ThanhLy],
			[LenhSanXuat_ID]
	)
	VALUES
	(
			@MaSanPham,
			@MaDoanhNghiep,
			@MaHaiQuan,
			@SoDinhMuc,
			@NgayDangKy,
			@NgayApDung,
			@NgayHetHan,
			@ThanhLy,
			@LenhSanXuat_ID
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Delete]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6),
	@LenhSanXuat_ID bigint
AS

DELETE FROM 
	[dbo].[t_SXXK_ThongTinDinhMuc]
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_ThongTinDinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Load]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6),
	@LenhSanXuat_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaSanPham],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[NgayHetHan],
	[ThanhLy],
	[LenhSanXuat_ID]
FROM
	[dbo].[t_SXXK_ThongTinDinhMuc]
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaSanPham],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[NgayHetHan],
	[ThanhLy],
	[LenhSanXuat_ID]
FROM [dbo].[t_SXXK_ThongTinDinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_SelectAll]









AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaSanPham],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[NgayHetHan],
	[ThanhLy],
	[LenhSanXuat_ID]
FROM
	[dbo].[t_SXXK_ThongTinDinhMuc]	

GO


IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'MaDinhDanh'
          AND Object_ID = Object_ID(N'dbo.t_SXXK_DinhMuc'))
BEGIN
    ALTER TABLE dbo.t_SXXK_DinhMuc ADD MaDinhDanh nvarchar(50) NULL
END
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Insert]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam bit,
	@MaDinhDanh nvarchar(50),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@LenhSanXuat_ID bigint,
	@GuidString nvarchar(50)
AS
INSERT INTO [dbo].[t_SXXK_DinhMuc]
(
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam],
	[MaDinhDanh],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[LenhSanXuat_ID],
	[GuidString]
)
VALUES
(
	@MaHaiQuan,
	@MaDoanhNghiep,
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@DinhMucChung,
	@GhiChu,
	@IsFromVietNam,
	@MaDinhDanh,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThaiXuLy,
	@LenhSanXuat_ID,
	@GuidString
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Update]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam bit,
	@MaDinhDanh nvarchar(50),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@LenhSanXuat_ID bigint,
	@GuidString nvarchar(50)
AS

UPDATE
	[dbo].[t_SXXK_DinhMuc]
SET
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[DinhMucChung] = @DinhMucChung,
	[GhiChu] = @GhiChu,
	[IsFromVietNam] = @IsFromVietNam,
	[MaDinhDanh] = @MaDinhDanh,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidString] = @GuidString
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_InsertUpdate]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam bit,
	@MaDinhDanh nvarchar(50),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@LenhSanXuat_ID bigint,
	@GuidString nvarchar(50)
AS
IF EXISTS(SELECT [MaHaiQuan], [MaDoanhNghiep], [MaSanPham], [MaNguyenPhuLieu], [LenhSanXuat_ID] FROM [dbo].[t_SXXK_DinhMuc] WHERE [MaHaiQuan] = @MaHaiQuan AND [MaDoanhNghiep] = @MaDoanhNghiep AND [MaSanPham] = @MaSanPham AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu AND [LenhSanXuat_ID] = @LenhSanXuat_ID)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_DinhMuc] 
		SET
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[DinhMucChung] = @DinhMucChung,
			[GhiChu] = @GhiChu,
			[IsFromVietNam] = @IsFromVietNam,
			[MaDinhDanh] = @MaDinhDanh,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidString] = @GuidString
		WHERE
			[MaHaiQuan] = @MaHaiQuan
			AND [MaDoanhNghiep] = @MaDoanhNghiep
			AND [MaSanPham] = @MaSanPham
			AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
			AND [LenhSanXuat_ID] = @LenhSanXuat_ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_DinhMuc]
	(
			[MaHaiQuan],
			[MaDoanhNghiep],
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[DinhMucChung],
			[GhiChu],
			[IsFromVietNam],
			[MaDinhDanh],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThaiXuLy],
			[LenhSanXuat_ID],
			[GuidString]
	)
	VALUES
	(
			@MaHaiQuan,
			@MaDoanhNghiep,
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@DinhMucChung,
			@GhiChu,
			@IsFromVietNam,
			@MaDinhDanh,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThaiXuLy,
			@LenhSanXuat_ID,
			@GuidString
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Delete]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@LenhSanXuat_ID bigint
AS

DELETE FROM 
	[dbo].[t_SXXK_DinhMuc]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Load]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@LenhSanXuat_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam],
	[MaDinhDanh],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[LenhSanXuat_ID],
	[GuidString]
FROM
	[dbo].[t_SXXK_DinhMuc]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam],
	[MaDinhDanh],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[LenhSanXuat_ID],
	[GuidString]
FROM [dbo].[t_SXXK_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectAll]















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam],
	[MaDinhDanh],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[LenhSanXuat_ID],
	[GuidString]
FROM
	[dbo].[t_SXXK_DinhMuc]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '42.1') = 0
	BEGIN
		INSERT INTO dbo.t_HaiQuan_Version VALUES('42.1',GETDATE(), N'CẬP NHẬT PROC t_SXXK_ThongTinDinhMuc, t_SXXK_DinhMuc')
	END	