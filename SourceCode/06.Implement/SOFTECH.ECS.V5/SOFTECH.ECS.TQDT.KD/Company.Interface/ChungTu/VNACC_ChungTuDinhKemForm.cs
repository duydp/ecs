﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.LogMessages;

namespace Company.Interface
{
    public partial class VNACC_ChungTuDinhKemForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ChungTuDinhKem ChungTuKem = new KDT_VNACC_ChungTuDinhKem();
        public List<KDT_VNACC_ChungTuDinhKem_ChiTiet> ListCTDK = new List<KDT_VNACC_ChungTuDinhKem_ChiTiet>();
        public bool isAddNew = true;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public ELoaiThongTin LoaiChungTu = ELoaiThongTin.MSB;

        public VNACC_ChungTuDinhKemForm(ELoaiThongTin loaiChungTu)
        {
            InitializeComponent();
            ChungTuKem.TrangThaiXuLy = "-1";
            LoaiChungTu = loaiChungTu;

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EOthers.MSB.ToString());
        }

        /// <summary>
        /// Tinh tong dung luong
        /// </summary>
        /// <param name="FileInBytes"></param>
        /// <returns></returns>
        private string CalculateFileSize(decimal FileInBytes)
        {
            string strSize = "00";
            if (FileInBytes < 1024)
                strSize = FileInBytes + " B";//Byte
            else if (FileInBytes > 1024 & FileInBytes < 1048576)
                strSize = Math.Round((FileInBytes / 1024), 2) + " KB";//Kilobyte
            else if (FileInBytes > 1048576 & FileInBytes < 107341824)
                strSize = Math.Round((FileInBytes / 1024) / 1024, 2) + " MB";//Megabyte
            else if (FileInBytes > 107341824 & FileInBytes < 1099511627776)
                strSize = Math.Round(((FileInBytes / 1024) / 1024) / 1024, 2) + " GB";//Gigabyte
            else
                strSize = Math.Round((((FileInBytes / 1024) / 1024) / 1024) / 1024, 2) + " TB";//Terabyte
            return strSize;
        }

        /// <summary>
        /// Lay tong dung luong cac file dinh kem
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private void HienThiTongDungLuong(List<KDT_VNACC_ChungTuDinhKem_ChiTiet> list)
        {
            long size = 0;

            for (int i = 0; i < list.Count; i++)
            {
                size += Convert.ToInt64(list[i].FileSize);
            }

            //hien thi tong dung luong file            
            lblTongDungLuong.Text = CalculateFileSize(size);
            lblLuuY.Text = CalculateFileSize(GlobalSettings.FileSize);
        }

        private void BindData()
        {
            dgList.DataSource = ListCTDK;
            try { dgList.Refetch(); }
            catch { }

            //Cap nhat thong tin tong dung luong file.
            HienThiTongDungLuong(ListCTDK);
        }

        private void VNACC_ChungTuDinhKemForm_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SetIDControl();

                uiTabPageMSB.Text = uiTabPageHYS.Text = "Thông tin chung";
                if (LoaiChungTu == ELoaiThongTin.MSB)
                {
                    uiTabPageMSB.TabVisible = true;
                    uiTabPageHYS.TabVisible = false;
                }
                else if (LoaiChungTu == ELoaiThongTin.HYS)
                {
                    uiTabPageMSB.TabVisible = false;
                    uiTabPageHYS.TabVisible = true;
                }

                //Thiet lap nut Khai bao, Phan hoi mac dinh
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = false;

                if (ChungTuKem.ID > 0)
                {
                    SetChungTuKem();

                    ListCTDK = KDT_VNACC_ChungTuDinhKem_ChiTiet.SelectCollectionBy_ChungTuKemID(ChungTuKem.ID);

                    BindData();
                }
                else
                {
                    //Cap nhat thong tin tong dung luong file.
                    HienThiTongDungLuong(ListCTDK);
                }

                if (ChungTuKem.SoTiepNhan > 0 && ChungTuKem.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString())
                {
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    txtSoTiepNhan.Text = ChungTuKem.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Text = ChungTuKem.NgayTiepNhan.ToShortDateString();
                }
                else if (ChungTuKem.SoTiepNhan > 0 && ChungTuKem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString())
                {
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = false;
                    txtSoTiepNhan.Text = ChungTuKem.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Text = ChungTuKem.NgayTiepNhan.ToShortDateString();
                }

                txtTieuDe.Focus();

                // Thiết lập trạng thái các nút trên form.
                // HUNGTQ, Update 07/06/2010.
                //SetButtonStateCHUNGTUKEM(TKMD, ChungTuKem);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetChungTuKem()
        {
            if (LoaiChungTu == ELoaiThongTin.MSB)
            {
                GetChungTuKem_MSB();
            }
            else if (LoaiChungTu == ELoaiThongTin.HYS)
            {
                GetChungTuKem_HYS();
            }
        }

        private void SetChungTuKem()
        {
            if (LoaiChungTu == ELoaiThongTin.MSB)
            {
                SetChungTuKem_MSB();
            }
            else if (LoaiChungTu == ELoaiThongTin.HYS)
            {
                SetChungTuKem_HYS();
            }
        }

        private void GetChungTuKem_MSB()
        {
            ChungTuKem.LoaiChungTu = LoaiChungTu.ToString();
            //Cap nhat lai danh sach file dinh kem chi tiet
            ChungTuKem.ChungTuDinhKemChiTietCollection = ListCTDK;

            ChungTuKem.TieuDe = txtTieuDe.Text.Trim();
            ChungTuKem.GhiChu = txtGhiChu.Text;
            ChungTuKem.CoQuanHaiQuan = ucCoQuanHaiQuan.Code;
            ChungTuKem.NhomXuLyHoSo = ucNhomXuLyHoSo.CustomsSubSectionCode;
            ChungTuKem.NhomXuLyHoSoID = Convert.ToInt32(ucNhomXuLyHoSo.Code);
            ChungTuKem.SoToKhai = Convert.ToInt32(txtSoToKhai.Value);
        }

        private void SetChungTuKem_MSB()
        {
            txtGhiChu.Text = ChungTuKem.GhiChu;
            txtTieuDe.Text = ChungTuKem.TieuDe;
            ucCoQuanHaiQuan.Code = ChungTuKem.CoQuanHaiQuan;
            ucNhomXuLyHoSo.Code = ChungTuKem.NhomXuLyHoSoID.ToString();
            txtSoToKhai.Value = ChungTuKem.SoToKhai;

            ChungTuKem.LoadListChungTuDinhKem_ChiTiet();
            ListCTDK = ChungTuKem.ChungTuDinhKemChiTietCollection;
        }

        private void GetChungTuKem_HYS()
        {
            ChungTuKem.LoaiChungTu = LoaiChungTu.ToString();
            //Cap nhat lai danh sach file dinh kem chi tiet
            ChungTuKem.ChungTuDinhKemChiTietCollection = ListCTDK;

            ChungTuKem.SoToKhai = Convert.ToInt32(txtSoToKhai_HYS.Value);
            ChungTuKem.PhanLoaiThuTucKhaiBao = ucPhanLoaiThuTucKhaiBao.Code;
            ChungTuKem.TenThuTucKhaiBao = ucPhanLoaiThuTucKhaiBao.Name_VN;
            ChungTuKem.CoQuanHaiQuan = ucCoQuanHaiQuan_HYS.Code;
            //ChungTuKem.NhomXuLyHoSo = ucNhomXuLyHoSo.CustomsSubSectionCode;
            //ChungTuKem.NhomXuLyHoSoID = Convert.ToInt32(ucNhomXuLyHoSo.Code);
            ChungTuKem.NgayKhaiBao = dtNgayKhaiBao.Value;
            ChungTuKem.TrangThaiKhaiBao = ucTrangThaiKhaiBao.Code;
            ChungTuKem.NgaySuaCuoiCung = dtNgaySuaCuoiCung.Value;
            ChungTuKem.TenNguoiKhaiBao = txtTenNguoiKhaiBao.Text;
            ChungTuKem.DiaChiNguoiKhaiBao = txtDiaChiNguoiKhaiBao.Text;
            ChungTuKem.SoDienThoaiNguoiKhaiBao = txtSoDienThoaiNguoiKhaiBao.Text;
            ChungTuKem.SoDeLayTepDinhKem = Convert.ToInt32(txtSoDeLayTepDinhKem.Value);
            ChungTuKem.SoQuanLyTrongNoiBoDoanhNghiep = txtSoQuanLyTrongNoiBoDoanhNghiep.Text;
            ChungTuKem.GhiChu = txtGhiChu_HYS.Text;
            ChungTuKem.NgayHoanThanhKiemTraHoSo = dtNgayHoanThanhKiemTraHoSo.Value;
            ChungTuKem.GhiChuHaiQuan = txtGhiChuHaiQuan.Text;
        }

        private void SetChungTuKem_HYS()
        {
            txtSoToKhai_HYS.Value = ChungTuKem.SoToKhai;
            ucPhanLoaiThuTucKhaiBao.Code = ChungTuKem.PhanLoaiThuTucKhaiBao;
            ucPhanLoaiThuTucKhaiBao.Name_VN = ChungTuKem.TenThuTucKhaiBao;
            ucCoQuanHaiQuan_HYS.Code = ChungTuKem.CoQuanHaiQuan;
            ucNhomXuLyHoSo_HYS.Code = ChungTuKem.NhomXuLyHoSoID.ToString();
            dtNgayKhaiBao.Value = ChungTuKem.NgayKhaiBao;
            ucTrangThaiKhaiBao.Code = ChungTuKem.TrangThaiKhaiBao;
            dtNgaySuaCuoiCung.Value = ChungTuKem.NgaySuaCuoiCung;
            txtTenNguoiKhaiBao.Text = ChungTuKem.TenNguoiKhaiBao;
            txtDiaChiNguoiKhaiBao.Text = ChungTuKem.DiaChiNguoiKhaiBao;
            txtSoDienThoaiNguoiKhaiBao.Text = ChungTuKem.SoDienThoaiNguoiKhaiBao;
            txtSoDeLayTepDinhKem.Value = ChungTuKem.SoDeLayTepDinhKem.ToString();
            txtSoQuanLyTrongNoiBoDoanhNghiep.Text = ChungTuKem.SoQuanLyTrongNoiBoDoanhNghiep;
            txtGhiChu_HYS.Text = ChungTuKem.GhiChu;
            dtNgayHoanThanhKiemTraHoSo.Value = ChungTuKem.NgayHoanThanhKiemTraHoSo;
            txtGhiChuHaiQuan.Text = ChungTuKem.GhiChuHaiQuan;

            ChungTuKem.LoadListChungTuDinhKem_ChiTiet();
            ListCTDK = ChungTuKem.ChungTuDinhKemChiTietCollection;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<KDT_VNACC_ChungTuDinhKem_ChiTiet> HangGiayPhepDetailCollection = new List<KDT_VNACC_ChungTuDinhKem_ChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        KDT_VNACC_ChungTuDinhKem_ChiTiet hgpDetail = new KDT_VNACC_ChungTuDinhKem_ChiTiet();
                        hgpDetail = (KDT_VNACC_ChungTuDinhKem_ChiTiet)i.GetRow().DataRow;

                        if (hgpDetail == null) continue;

                        //Detele from DB.
                        if (hgpDetail.ID > 0)
                            hgpDetail.Delete();

                        //Remove out Collction
                        ListCTDK.Remove(hgpDetail);
                    }
                }
            }

            BindData();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ListCTDK == null || ListCTDK.Count == 0)
                {
                    ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                    uiTabPageFile.Selected = true;
                    return;
                }

                GetChungTuKem();

                isAddNew = ChungTuKem.ID == 0;
                ChungTuKem.InsertUpdateFull();

                BindData();

                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "Image files (*.jpg;*.jpeg)|*.jpg;*.jpeg|PDF files (*.pdf)|*.pdf";
            openFileDialog1.Multiselect = true;

            try
            {
                if (openFileDialog1.ShowDialog(this) != DialogResult.Cancel)
                {
                    long size = 0;

                    for (int k = 0; k < openFileDialog1.FileNames.Length; k++)
                    {
                        System.IO.FileInfo fin = new System.IO.FileInfo(openFileDialog1.FileNames[k]);

                        if (fin.Extension.ToUpper() != ".jpg".ToUpper() && fin.Extension.ToUpper() != ".jpeg".ToUpper() && fin.Extension.ToUpper() != ".pdf".ToUpper())
                        {
                            ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                        }
                        else
                        {
                            System.IO.FileStream fs = new System.IO.FileStream(openFileDialog1.FileNames[k], System.IO.FileMode.Open, System.IO.FileAccess.Read);

                            //Cap nhat tong dung luong file.
                            size = 0;

                            for (int i = 0; i < ListCTDK.Count; i++)
                            {
                                size += Convert.ToInt64(ListCTDK[i].FileSize);
                            }

                            //+ them dung luong file moi chuan bi them vao danh sach
                            size += fs.Length;

                            //Kiem tra dung luong file
                            if (size > GlobalSettings.FileSize)
                            {
                                this.ShowMessage(string.Format("Tổng dung lượng cho phép {0}\r\nDung lượng bạn nhập {1} đã vượt mức cho phép.", CalculateFileSize(GlobalSettings.FileSize), CalculateFileSize(size)), false);
                                return;
                            }

                            byte[] data = new byte[fs.Length];
                            fs.Read(data, 0, data.Length);
                            filebase64 = System.Convert.ToBase64String(data);
                            filesize = fs.Length;

                            /*
                             * truoc khi them moi file dinh kem vao danh sach, phai kiem tra tong dung luong co hop len khong?.
                             * Neu > dung luong choh phep -> Hien thi thong bao va khong them vao danh sach.
                             */
                            KDT_VNACC_ChungTuDinhKem_ChiTiet ctctiet = new KDT_VNACC_ChungTuDinhKem_ChiTiet();
                            ctctiet.ChungTuKemID = ChungTuKem.ID;
                            ctctiet.FileName = fin.Name;
                            ctctiet.FileSize = filesize;
                            ctctiet.NoiDung = data;

                            ListCTDK.Add(ctctiet);
                            dgList.DataSource = ListCTDK;
                            try
                            {
                                dgList.Refetch();
                            }
                            catch { dgList.Refresh(); }

                            lblTongDungLuong.Text = CalculateFileSize(size);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXemFile_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //kiem tra thu da co muc temp?. Neu chua co -> tao 1 thu muc temp de chua cac file tam thoi.
                string path = Application.StartupPath + "\\Temp";

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                //Giai ma basecode64 -> file & luu tam vao thu muc Temp moi tao.
                if (dgList.GetRow() == null)
                {
                    ShowMessage("Chưa chọn file để xem", false);
                    return;
                }
                KDT_VNACC_ChungTuDinhKem_ChiTiet fileData = (KDT_VNACC_ChungTuDinhKem_ChiTiet)dgList.GetRow().DataRow;
                string fileName = path + "\\" + fileData.FileName;

                //Ghi file
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }

                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                fs.Write(fileData.NoiDung, 0, fileData.NoiDung.Length);
                fs.Close();

                System.Diagnostics.Process.Start(fileName);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoaChungTu_Click(object sender, EventArgs e)
        {
            if (ChungTuKem.ID > 0)
            {
                if (ShowMessage("Bạn có muốn xóa chứng từ này không?", true) == "Yes")
                {
                    //Xoa chung tu chi tiet
                    ChungTuKem.LoadListChungTuDinhKem_ChiTiet();

                    for (int i = 0; i < ChungTuKem.ChungTuDinhKemChiTietCollection.Count; i++)
                    {
                        ChungTuKem.ChungTuDinhKemChiTietCollection[i].Delete();
                    }

                    //Xoa chung tu trong DB
                    ChungTuKem.Delete();
                }
            }

            //Dong form sau khi xoa chung tu.
            this.Close();
        }

        private void ucCoQuanHaiQuan_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                ucNhomXuLyHoSo.WhereCondition = string.Format("CustomsCode like '{0}%'", ucCoQuanHaiQuan.Code);
                ucNhomXuLyHoSo.ReLoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                ucCoQuanHaiQuan.TagCode = "CHN"; //Cơ quan hải quan
                ucNhomXuLyHoSo.TagName = "CHB"; //Nhóm xử lý hồ sơ 
                txtTieuDe.Tag = "SUB"; //Tiêu đề
                txtSoToKhai.Tag = "ICN"; //Số tờ khai
                txtGhiChu.Tag = "TUS"; //Ghi chú

                ucCoQuanHaiQuan_HYS.TagCode = "CH"; //Cơ quan hải quan
                ucNhomXuLyHoSo_HYS.TagName = "CHB"; //Nhóm xử lý hồ sơ 
                ucPhanLoaiThuTucKhaiBao.TagCode = "TSB"; //Phân loại thủ tục khai báo
                txtSoDienThoaiNguoiKhaiBao.Tag = "STL"; //Số điện thoại người khai báo
                txtSoQuanLyTrongNoiBoDoanhNghiep.Tag = "REF"; //Số quản lý trong nội bộ doanh nghiệp
                txtGhiChu_HYS.Tag = "KIJ"; //Ghi chú

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdKhaiBao":
                    SendVnaccsIDC(false);
                    break;
                case "cmdLayPhanHoi":
                    break;
            }
        }

        #region Send VNACCS

        /// <summary>
        /// Khai báo thông tin
        /// </summary>
        /// <param name="KhaiBaoSua"></param>
        private void SendVnaccsIDC(bool KhaiBaoSua)
        {
            try
            {
                if (ChungTuKem.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }

                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến Hải quan? ", true) == "Yes")
                {
                    ChungTuKem.InputMessageID = HelperVNACCS.NewInputMSGID(); //Tạo mới GUID ID
                    ChungTuKem.InsertUpdateFull(); //Lưu thông tin GUID ID vừa tạo

                    MessagesSend msg = null; //Form khai báo
                    //MSB saa = VNACCMaperFromObject.MSBMapper(ChungTuKem); //Set Mapper
                    //if (saa == null)
                    //{
                    //    this.ShowMessage("Lỗi khi tạo messages !", false);
                    //    return;
                    //}
                    //msg = MessagesSend.Load<MSB>(saa, ChungTuKem.InputMessageID);

                    MsgLog.SaveMessages(msg, ChungTuKem.ID, EnumThongBao.SendMess, ""); //Lưu thông tin trước khai báo
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true; //Có khai báo
                    f.isRep = true; //Có nhận phản hồi
                    f.inputMSGID = ChungTuKem.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result) //Có kêt quả trả về
                    {
                        string ketqua = "Khai báo thông tin thành công";

                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal soTiepNhan = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số chứng từ: " + soTiepNhan;

                                ChungTuKem.SoTiepNhan = soTiepNhan;
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }

                        TuDongCapNhatThongTin();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #endregion

        #region Cập nhật thông tin

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            ProcessMessages.GetDataResult_ChungTuDinhKem(msgResult, "", ChungTuKem);
            ChungTuKem.InsertUpdateFull();
            SetChungTuKem();
            //setCommandStatus();
        }

        private void TuDongCapNhatThongTin()
        {
            if (ChungTuKem != null && ChungTuKem.SoTiepNhan > 0 && ChungTuKem.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' And MessagesInputID = '{1}'", ChungTuKem.SoTiepNhan.ToString(), ChungTuKem.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion

    }
}

