﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Transactions;
using System.Globalization;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Nodes.GC;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
    public partial class HopDong
    {
        public List<HopDongChiTiet> ListHangMDOfHopDong = new List<HopDongChiTiet>();

        public void LoadListHangDMOfHopDong()
        {
            ListHangMDOfHopDong = (List<HopDongChiTiet>)HopDongChiTiet.SelectCollectionBy_HopDongTM_ID(this.ID);
        }

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);
                        foreach (HopDongChiTiet item in ListHangMDOfHopDong)
                        {
                            item.HopDongTM_ID = this.ID;
                            if (id == 0)
                                item.ID = 0;

                            if (item.ID == 0)
                                item.Insert(transaction);
                            else
                                item.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this.ID = id;
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public static XmlNode ConvertCollectionHopDongToXML_TKX(XmlDocument doc, List<HopDong> HopDongThuongMaiCollection, long ID_TK, List<HangChuyenTiep> listHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            if (HopDongThuongMaiCollection == null || HopDongThuongMaiCollection.Count == 0)
            {
                HopDongThuongMaiCollection = (List<HopDong>)HopDong.SelectCollectionBy_ID_TK(ID_TK);
            }
            XmlElement CHUNG_TU_HOPDONG = doc.CreateElement("CHUNG_TU_HOPDONG");
            foreach (HopDong hopdongTM in HopDongThuongMaiCollection)
            {
                XmlElement hopdongTMItem = doc.CreateElement("CHUNG_TU_HOPDONG.ITEM");
                CHUNG_TU_HOPDONG.AppendChild(hopdongTMItem);

                XmlAttribute SO_CT = doc.CreateAttribute("SO_CT");
                SO_CT.Value = FontConverter.Unicode2TCVN(hopdongTM.SoHopDongTM.Trim());
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = doc.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = hopdongTM.NgayHopDongTM.ToString("yyyy-MM-dd");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute THOI_HAN_TT = doc.CreateAttribute("THOI_HAN_TT");
                THOI_HAN_TT.Value = hopdongTM.ThoiHanThanhToan.ToString("yyyy-MM-dd");
                hopdongTMItem.Attributes.Append(THOI_HAN_TT);

                XmlAttribute MA_GH = doc.CreateAttribute("MA_GH");
                MA_GH.Value = hopdongTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute DIADIEM_GH = doc.CreateAttribute("DIADIEM_GH");
                DIADIEM_GH.Value = FontConverter.Unicode2TCVN(hopdongTM.DiaDiemGiaoHang);
                hopdongTMItem.Attributes.Append(DIADIEM_GH);

                XmlAttribute MA_PTTT = doc.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = hopdongTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = doc.CreateAttribute("MA_NT");
                MA_NT.Value = hopdongTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);

                XmlAttribute TONGTRIGIA = doc.CreateAttribute("TONGTRIGIA");
                TONGTRIGIA.Value = BaseClass.Round(hopdongTM.TongTriGia, 9);
                hopdongTMItem.Attributes.Append(TONGTRIGIA);

                XmlAttribute MA_DV_DT = doc.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = hopdongTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = doc.CreateAttribute("TEN_DV_DT");
                TEN_DV_DT.Value = FontConverter.Unicode2TCVN(hopdongTM.TenDonViMua.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute MA_DV = doc.CreateAttribute("MA_DV");
                MA_DV.Value = hopdongTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = doc.CreateAttribute("TEN_DV");
                TEN_DV.Value = FontConverter.Unicode2TCVN(hopdongTM.TenDonViBan.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute GHI_CHU = doc.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = FontConverter.Unicode2TCVN(hopdongTM.ThongTinKhac.Trim());
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (hopdongTM.ListHangMDOfHopDong == null || hopdongTM.ListHangMDOfHopDong.Count == 0)
                {
                    hopdongTM.LoadListHangDMOfHopDong();
                }

                foreach (HopDongChiTiet hangHopDong in hopdongTM.ListHangMDOfHopDong)
                {
                    HangChuyenTiep hmd = BaseClass.GetHangChuyenTiepByIDHCT(hangHopDong.HMD_ID, listHang);

                    if (hmd == null) continue;

                    //Cap nhat thong tin hang thay doi cua rieng giay phep.
                    if (hmd != null)
                    {
                        hmd.MaHS = hangHopDong.MaHS;
                        hmd.MaHang = hangHopDong.MaPhu;
                        hmd.TenHang = hangHopDong.TenHang;
                        hmd.ID_NuocXX = hangHopDong.NuocXX_ID;
                        hmd.ID_DVT = hangHopDong.DVT_ID;
                        hmd.SoLuong = hangHopDong.SoLuong;
                        hmd.DonGia = (decimal)hangHopDong.DonGiaKB;
                        hmd.TriGia = (decimal)hangHopDong.TriGiaKB;
                    }

                    XmlElement hangItem = doc.CreateElement("CHUNG_TU_HOPDONG.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = doc.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaHang.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = doc.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = doc.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute MA_DVT = doc.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.ID_DVT.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = doc.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = doc.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGia, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = doc.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGia, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GHI_CHUhang = doc.CreateAttribute("GHI_CHU");
                    GHI_CHUhang.Value = FontConverter.Unicode2TCVN(hangHopDong.GhiChu != null ? hangHopDong.GhiChu.Trim() : "");
                    hangItem.Attributes.Append(GHI_CHUhang);

                    XmlAttribute NUOC_XXHang = doc.CreateAttribute("NUOC_XX");
                    NUOC_XXHang.Value = hmd.ID_NuocXX;
                    hangItem.Attributes.Append(NUOC_XXHang);
                }
            }
            if (CHUNG_TU_HOPDONG.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_HOPDONG;
        }

        public static XmlNode ConvertCollectionHopDongToXML_TKN(XmlDocument doc, List<HopDong> HopDongThuongMaiCollection, long ID_TK, List<HangChuyenTiep> listHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            if (HopDongThuongMaiCollection == null || HopDongThuongMaiCollection.Count == 0)
            {
                HopDongThuongMaiCollection = (List<HopDong>)HopDong.SelectCollectionBy_ID_TK(ID_TK);
            }
            XmlElement CHUNG_TU_HOPDONG = doc.CreateElement("CHUNG_TU_HOPDONG");
            foreach (HopDong hopdongTM in HopDongThuongMaiCollection)
            {
                XmlElement hopdongTMItem = doc.CreateElement("CHUNG_TU_HOPDONG.ITEM");
                CHUNG_TU_HOPDONG.AppendChild(hopdongTMItem);

                XmlAttribute SO_CT = doc.CreateAttribute("SO_CT");
                SO_CT.Value = FontConverter.Unicode2TCVN(hopdongTM.SoHopDongTM.Trim());
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = doc.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = hopdongTM.NgayHopDongTM.ToString("yyyy-MM-dd");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute THOI_HAN_TT = doc.CreateAttribute("THOI_HAN_TT");
                THOI_HAN_TT.Value = hopdongTM.ThoiHanThanhToan.ToString("yyyy-MM-dd");
                hopdongTMItem.Attributes.Append(THOI_HAN_TT);

                XmlAttribute MA_GH = doc.CreateAttribute("MA_GH");
                MA_GH.Value = hopdongTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute DIADIEM_GH = doc.CreateAttribute("DIADIEM_GH");
                DIADIEM_GH.Value = FontConverter.Unicode2TCVN(hopdongTM.DiaDiemGiaoHang);
                hopdongTMItem.Attributes.Append(DIADIEM_GH);

                XmlAttribute MA_PTTT = doc.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = hopdongTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = doc.CreateAttribute("MA_NT");
                MA_NT.Value = hopdongTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);

                XmlAttribute TONGTRIGIA = doc.CreateAttribute("TONGTRIGIA");
                TONGTRIGIA.Value = BaseClass.Round(hopdongTM.TongTriGia, 9);
                hopdongTMItem.Attributes.Append(TONGTRIGIA);

                XmlAttribute MA_DV = doc.CreateAttribute("MA_DV");
                MA_DV.Value = hopdongTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = doc.CreateAttribute("TEN_DV");
                TEN_DV.Value = FontConverter.Unicode2TCVN(hopdongTM.TenDonViMua.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute MA_DV_DT = doc.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = hopdongTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = doc.CreateAttribute("TEN_DV_DT");
                TEN_DV_DT.Value = FontConverter.Unicode2TCVN(hopdongTM.TenDonViBan.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute GHI_CHU = doc.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = FontConverter.Unicode2TCVN(hopdongTM.ThongTinKhac.Trim());
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (hopdongTM.ListHangMDOfHopDong == null || hopdongTM.ListHangMDOfHopDong.Count == 0)
                {
                    hopdongTM.LoadListHangDMOfHopDong();
                }

                foreach (HopDongChiTiet hangHopDong in hopdongTM.ListHangMDOfHopDong)
                {
                    HangChuyenTiep hmd = BaseClass.GetHangChuyenTiepByIDHCT(hangHopDong.HMD_ID, listHang);

                    if (hmd == null) continue;

                    //Cap nhat thong tin hang thay doi cua rieng giay phep.
                    if (hmd != null)
                    {
                        hmd.MaHS = hangHopDong.MaHS;
                        hmd.MaHang = hangHopDong.MaPhu;
                        hmd.TenHang = hangHopDong.TenHang;
                        hmd.ID_NuocXX = hangHopDong.NuocXX_ID;
                        hmd.ID_DVT = hangHopDong.DVT_ID;
                        hmd.SoLuong = hangHopDong.SoLuong;
                        hmd.DonGia = (decimal)hangHopDong.DonGiaKB;
                        hmd.TriGia = (decimal)hangHopDong.TriGiaKB;
                    }

                    XmlElement hangItem = doc.CreateElement("CHUNG_TU_HOPDONG.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = doc.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaHang.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = doc.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = doc.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute MA_DVT = doc.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.ID_DVT.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = doc.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = doc.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGia, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = doc.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGia, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GHI_CHUhang = doc.CreateAttribute("GHI_CHU");
                    GHI_CHUhang.Value = FontConverter.Unicode2TCVN(hangHopDong.GhiChu != null ? hangHopDong.GhiChu.Trim() : "");
                    hangItem.Attributes.Append(GHI_CHUhang);

                    XmlAttribute NUOC_XXHang = doc.CreateAttribute("NUOC_XX");
                    NUOC_XXHang.Value = hmd.ID_NuocXX;
                    hangItem.Attributes.Append(NUOC_XXHang);
                }
            }
            if (CHUNG_TU_HOPDONG.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_HOPDONG;
        }

        public string WSSend(string password, string maHQ, long soTK, string maLH, int namDK, long ID_TK, List<HangChuyenTiep> listHang)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];
            nodeTO_KHAI.ChildNodes[0].Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.ChildNodes[0].Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.ChildNodes[0].Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.ChildNodes[0].Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<HopDong> listHoaDonThuongMai = new List<HopDong>();
            listHoaDonThuongMai.Add(this);
            nodeDulieu.AppendChild(HopDong.ConvertCollectionHopDongToXML_TKN(doc, listHoaDonThuongMai, ID_TK, listHang));


            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public bool WSKhaiBaoBoSungHopDong(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long ID_TK, string maDoanhNghiep, List<HopDong> list, MessageTypes messageType, MessageFunctions messageFunction, List<HangChuyenTiep> listHang)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep;
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep;

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan;
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan;

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            GuidStr = nodeReference.InnerText = System.Guid.NewGuid().ToString();
            this.Update();
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep;
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep;

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan;
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan;

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";


            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<HopDong> listHopDongThuongMai = new List<HopDong>();
            listHopDongThuongMai.Add(this);
            xmlNodeDulieu.AppendChild(HopDong.ConvertCollectionHopDongToXML_TKN(xmlDocument, listHopDongThuongMai, ID_TK, listHang));

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                // Lưu message.
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ToKhaiNhap;
                message.MessageFunction = MessageFunctions.KhaiBao;
                message.MessageFrom = maDoanhNghiep;
                message.MessageTo = maHaiQuan.Trim();
                message.MessageContent = xmlDocument.InnerXml;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungHopDong;
                message.NoiDungThongBao = string.Empty;
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                    XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);

                    // Lưu message.
                    Message message = new Message();
                    message.ItemID = this.ID;
                    message.ReferenceID = new Guid(this.GuidStr);
                    message.MessageType = MessageTypes.ThongTin;
                    message.MessageFunction = MessageFunctions.LayPhanHoi;
                    message.MessageFrom = maHaiQuan.Trim();
                    message.MessageTo = maDoanhNghiep;
                    message.MessageContent = xmlDocument.InnerXml;
                    message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungHopDong;
                    message.NoiDungThongBao = noiDung;// string.Empty;
                    message.CreatedTime = DateTime.Now;
                    message.Insert();

                    return true;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return false;

        }

        /// <summary>
        /// Lấy số tiếp nhận khai báo.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLaySoTiepNhan(string password)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungHopDongThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Nhận phản hồi thông quan điện tử sau khi khai báo đã có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLayPhanHoi(string password)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }
            if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.TuChoiTiepNhan;
                string lydo = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), lydo);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Cập lại số tiếp nhận
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.Update();

                return true;
            }
            else if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungHopDongThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }
            else if (xmlNodeResult.Attributes["SOTN"] != null)
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungHopDongDuocChapNhan;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã được Hải quan chấp nhận.", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }
            return false;
        }

        public static XmlNode ConvertCollectionHopDongToXML(XmlDocument doc, List<HopDong> HopDongThuongMaiCollection, long ID_TK, List<HangChuyenTiep> listHang)
        {
            if (HopDongThuongMaiCollection == null || HopDongThuongMaiCollection.Count == 0)
            {
                HopDongThuongMaiCollection = (List<HopDong>)HopDong.SelectCollectionBy_ID_TK(ID_TK);
            }
            XmlElement ContractDocuments = doc.CreateElement(NodeHopDong.ContractDocuments);
            foreach (HopDong hopdongTM in HopDongThuongMaiCollection)
            {
                XmlElement ContractDocument = doc.CreateElement(NodeHopDong.ContractDocument);
                ContractDocuments.AppendChild(ContractDocument);
                //
                XmlElement reference = doc.CreateElement(NodeHopDong.reference);// Số HĐ
                ContractDocument.AppendChild(reference);
                reference.InnerText = hopdongTM.SoHopDongTM;
                XmlElement issue = doc.CreateElement(NodeHopDong.issue);
                ContractDocument.AppendChild(issue);
                issue.InnerText = hopdongTM.NgayHopDongTM.ToString(NgayThang.yyyyMMdd);
                XmlElement expire = doc.CreateElement(NodeHopDong.expire);
                ContractDocument.AppendChild(expire);
                expire.InnerText = hopdongTM.ThoiHanThanhToan.ToString(NgayThang.yyyyMMdd);
                //
                XmlElement Payment = doc.CreateElement(NodeHopDong.Payment);
                ContractDocument.AppendChild(Payment);
                XmlElement method = doc.CreateElement(NodeHopDong.method);
                Payment.AppendChild(method);
                method.InnerText = hopdongTM.PTTT_ID;
                //Điều kiện giao hàng
                XmlElement TradeTerm = doc.CreateElement(NodeHopDong.TradeTerm);
                ContractDocument.AppendChild(TradeTerm);
                XmlElement condition = doc.CreateElement(NodeHopDong.condition);
                TradeTerm.AppendChild(condition);
                condition.InnerText = hopdongTM.DKGH_ID;
                //
                XmlElement DeliveryDestination = doc.CreateElement(NodeHopDong.DeliveryDestination);
                ContractDocument.AppendChild(DeliveryDestination);
                XmlElement line = doc.CreateElement(NodeHopDong.line);
                DeliveryDestination.AppendChild(line);
                line.InnerText = hopdongTM.DiaDiemGiaoHang;
                //
                XmlElement CurrencyExchange = doc.CreateElement(NodeHopDong.CurrencyExchange);
                ContractDocument.AppendChild(CurrencyExchange);
                XmlElement currencyType = doc.CreateElement(NodeHopDong.currencyType);
                CurrencyExchange.AppendChild(currencyType);
                currencyType.InnerText = hopdongTM.NguyenTe_ID;
                //
                XmlElement totalValue = doc.CreateElement(NodeHopDong.totalValue);
                ContractDocument.AppendChild(totalValue);
                totalValue.InnerText = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hopdongTM.TongTriGia,9);
                //
                XmlElement Buyer = doc.CreateElement(NodeHopDong.Buyer);
                ContractDocument.AppendChild(Buyer);
                XmlElement name = doc.CreateElement(NodeHopDong.name);
                Buyer.AppendChild(name);
                name.InnerText = hopdongTM.TenDonViMua;
                XmlElement identity = doc.CreateElement(NodeHopDong.identity);
                Buyer.AppendChild(identity);
                identity.InnerText = hopdongTM.MaDonViMua;
                //  
                XmlElement Seller = doc.CreateElement(NodeHopDong.Seller);
                ContractDocument.AppendChild(Seller);
                XmlElement nameSeller = doc.CreateElement(NodeHopDong.name);
                Seller.AppendChild(nameSeller);
                nameSeller.InnerText = hopdongTM.TenDonViBan;
                XmlElement identitySeller = doc.CreateElement(NodeHopDong.identity);
                Seller.AppendChild(identitySeller);
                identitySeller.InnerText = hopdongTM.MaDonViBan;
                //
                XmlElement AdditionalInformation = doc.CreateElement(NodeHopDong.AdditionalInformation);
                ContractDocument.AppendChild(AdditionalInformation);
                XmlElement content = doc.CreateElement(NodeHopDong.content);
                AdditionalInformation.AppendChild(content);
                //Thông tin hàng hóa
                if (hopdongTM.ListHangMDOfHopDong == null || hopdongTM.ListHangMDOfHopDong.Count == 0)
                {
                    hopdongTM.LoadListHangDMOfHopDong();
                }

                foreach (HopDongChiTiet hangHopDong in hopdongTM.ListHangMDOfHopDong)
                {
                    XmlElement ContractItem = doc.CreateElement(NodeHopDong.ContractItem);
                    ContractDocument.AppendChild(ContractItem);
                    XmlElement unitPrice = doc.CreateElement(NodeHopDong.unitPrice);
                    ContractItem.AppendChild(unitPrice);
                    unitPrice.InnerText = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hangHopDong.DonGiaKB, 9);
                    XmlElement statisticalValue = doc.CreateElement(NodeHopDong.statisticalValue);
                    ContractItem.AppendChild(statisticalValue);
                    statisticalValue.InnerText = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hangHopDong.TriGiaKB, 9);
                    //Thông tin hàng hóa
                    XmlElement Commodity = doc.CreateElement(NodeHopDong.Commodity);
                    ContractItem.AppendChild(Commodity);
                    XmlElement description = doc.CreateElement(NodeHopDong.description);
                    Commodity.AppendChild(description);
                    description.InnerText = hangHopDong.TenHang;
                    XmlElement identification = doc.CreateElement(NodeHopDong.identification);
                    Commodity.AppendChild(identification);
                    identification.InnerText = hangHopDong.MaPhu;
                    XmlElement tariffClassification = doc.CreateElement(NodeHopDong.tariffClassification);
                    Commodity.AppendChild(tariffClassification);
                    tariffClassification.InnerText = hangHopDong.MaHS;
                    //
                    XmlElement Origin = doc.CreateElement(NodeHopDong.Origin);
                    ContractItem.AppendChild(Origin);
                    XmlElement originCountry = doc.CreateElement(NodeHopDong.originCountry);
                    Origin.AppendChild(originCountry);
                    originCountry.InnerText = hangHopDong.NuocXX_ID;
                    //
                    XmlElement GoodsMeasure = doc.CreateElement(NodeHopDong.GoodsMeasure);
                    ContractItem.AppendChild(GoodsMeasure);
                    XmlElement quantity = doc.CreateElement(NodeHopDong.quantity);
                    GoodsMeasure.AppendChild(quantity);
                    quantity.InnerText = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hangHopDong.SoLuong,9);
                    XmlElement measureUnit = doc.CreateElement(NodeHopDong.measureUnit);
                    GoodsMeasure.AppendChild(measureUnit);
                    measureUnit.InnerText = hangHopDong.DVT_ID;
                    //
                    XmlElement AdditionalInformationContractItem = doc.CreateElement(NodeHopDong.AdditionalInformation);
                    ContractItem.AppendChild(AdditionalInformationContractItem);
                    XmlElement contentContractItem = doc.CreateElement(NodeHopDong.content);
                    AdditionalInformationContractItem.AppendChild(contentContractItem);
                }
                
            }
            if (ContractDocuments.ChildNodes.Count == 0)
                return null;
            return ContractDocuments;
        }
    }
}
