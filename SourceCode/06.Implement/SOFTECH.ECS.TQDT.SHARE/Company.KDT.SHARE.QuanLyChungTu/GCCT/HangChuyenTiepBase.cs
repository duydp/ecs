using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
    public partial class HangChuyenTiep
    {
        #region Private members.

        protected long _ID;
        protected long _Master_ID;
        protected int _SoThuTuHang;
        protected string _MaHang = String.Empty;
        protected string _TenHang = String.Empty;
        protected string _MaHS = String.Empty;
        protected decimal _SoLuong;
        protected string _ID_NuocXX = String.Empty;
        protected string _ID_DVT = String.Empty;
        protected decimal _DonGia;
        protected decimal _TriGia;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public long Master_ID
        {
            set { this._Master_ID = value; }
            get { return this._Master_ID; }
        }
        public int SoThuTuHang
        {
            set { this._SoThuTuHang = value; }
            get { return this._SoThuTuHang; }
        }
        public string MaHang
        {
            set { this._MaHang = value; }
            get { return this._MaHang; }
        }
        public string TenHang
        {
            set { this._TenHang = value; }
            get { return this._TenHang; }
        }
        public string MaHS
        {
            set { this._MaHS = value; }
            get { return this._MaHS; }
        }
        public decimal SoLuong
        {
            set { this._SoLuong = value; }
            get { return this._SoLuong; }
        }
        public string ID_NuocXX
        {
            set { this._ID_NuocXX = value; }
            get { return this._ID_NuocXX; }
        }
        public string ID_DVT
        {
            set { this._ID_DVT = value; }
            get { return this._ID_DVT; }
        }
        public decimal DonGia
        {
            set { this._DonGia = value; }
            get { return this._DonGia; }
        }
        public decimal TriGia
        {
            set { this._TriGia = value; }
            get { return this._TriGia; }
        }

        #endregion

    }
}