﻿using System;
using Company.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.CX;

namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuDangKyDetailForm_CX : BaseForm
    {
        public HangDuaVaoDangKy NPLDangKy = new HangDuaVaoDangKy();

        //-----------------------------------------------------------------------------------------
        public NguyenPhuLieuDangKyDetailForm_CX()
        {
            InitializeComponent();
        }

        #region Private methods.

        //-----------------------------------------------------------------------------------------
        private void NguyenPhuLieuDangKyDetailForm_Load(object sender, EventArgs e)
        {
            this.NPLDangKy.loadHang();
            dgList.DataSource = this.NPLDangKy.DanhSachHangDuaVao;
            switch (this.NPLDangKy.TrangThaiXuLy)
            {
                case -1: 
                   // lblTrangThai.Text = "Chưa gửi thông tin";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chưa gửi thông tin";
                    }
                    else
                    {
                        lblTrangThai.Text = "Information has not yet sent";
                    }
                    break;
                case 0:
                   // lblTrangThai.Text = "Chờ duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait to approval";
                    }
                    break;
                case 1:
                    //lblTrangThai.Text = "Đã duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đã duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Approved";
                    }
                    break;
                case 2:
                    //lblTrangThai.Text = "Không phê duyệt";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Không phê duyệt";
                    }
                    else
                    {
                        lblTrangThai.Text = "Not Approved";
                    }
                    break;
                case 11:
                    //lblTrangThai.Text = "Chờ Hủy";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ Hủy";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait to Canceled";
                    }
                    break;
                case 10:
                    //lblTrangThai.Text = "Đã Hủy";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đã hủy";
                    }
                    else
                    {
                        lblTrangThai.Text = "Canceled";
                    }
                    break;

            }
         
            txtSoTiepNhan.Text = this.NPLDangKy.SoTiepNhan.ToString();
            txtMaHaiQuan.Text = this.NPLDangKy.MaHaiQuan;
            txtTenHaiQuan.Text = DonViHaiQuan.GetName(this.NPLDangKy.MaHaiQuan);
            if (this.OpenType == OpenFormType.View)
            {
                TopRebar1.Visible = false;
                dgList.AllowDelete = InheritableBoolean.False;
            }
            else
            {
                TopRebar1.Visible = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            //MsgSend msg = new MsgSend();
            //msg.master_id = this.NPLDangKy.ID;
            //msg.LoaiHS = "NPL";
            //if (msg.Load())
            //{
            //    //lblTrangThai.Text = "Chưa xác nhận thông tin tới hải quan";
            //    if (GlobalSettings.NGON_NGU == "0")
            //    {
            //        lblTrangThai.Text = "Chưa xác nhận thông tin tới hải quan";
            //    }
            //    else
            //    {
            //        lblTrangThai.Text = "Not Confirm information to Customs";
            //    }
            //    TopRebar1.Visible = false;
            //    dgList.AllowDelete = InheritableBoolean.False;
            //}
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.KhaiDienTu)))
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    TopRebar1.Visible = false;
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới nguyên phụ liệu.
        /// </summary>
        private void add()
        {
            NguyenPhuLieuEditForm_CX f = new NguyenPhuLieuEditForm_CX();
            f.OpenType = OpenFormType.Insert;
            f.MaHaiQuan = txtMaHaiQuan.Text;
            f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            f.NPLCollection = this.NPLDangKy.DanhSachHangDuaVao;
            f.nplDangKy = NPLDangKy;
            f.ShowDialog(this);

            if (f.NPLDetail != null)
            {
                HangDuaVao npl = f.NPLDetail;
                npl.Master_ID = this.NPLDangKy.ID;
                this.NPLDangKy.DanhSachHangDuaVao.Add(npl);
                npl.Insert();
//                 BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
//                 nplSXXK.DVT_ID = npl.DVT_ID;
//                 nplSXXK.Ma = npl.Ma;
//                 nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
//                 nplSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
//                 nplSXXK.MaHS = npl.MaHS;
//                 nplSXXK.Ten = npl.Ten;
//                 nplSXXK.InsertUpdate();
                dgList.Refetch();
            }
        }

        //-----------------------------------------------------------------------------------------

        //private void upload()
        //{
        //    try
        //    {
        //        this.Cursor = Cursors.WaitCursor;
        //        string[] danhsachDaDangKy = new string[0];

        //        WSForm wsForm = new WSForm();
        //        wsForm.ShowDialog(this);
        //        if (!wsForm.IsReady) return;

        //        int ret = this.NPLDangKy.WSUpload(ref danhsachDaDangKy);

        //        // Thực hiện kiểm tra.
        //        switch(ret)
        //        {
        //            case 0:
        //                ShowMessage("Cập nhật không thành công!\nCó lỗi hệ thống.", false);
        //                this.setNeedUpload(true);
        //                break;
        //            case 1:
        //                ShowMessage("Cập nhật thành công!", false);
        //                this.setNeedUpload(false);
        //                break;
        //            case 2:
        //                if (ShowMessage("Cập nhật không thành công!\nDo chứng từ này đã được duyệt rồi.\nVậy bạn có muốn cập nhật dữ liệu từ Hải quan không?", true) == "Yes")
        //                {
        //                    this.NPLDangKy.WSDownload();
        //                }
        //                this.setNeedUpload(false);
        //                break;
        //            case 3:
        //                ShowMessage("Cập nhật không thành công!\nCó một số nguyên phụ liệu đã được đăng ký rồi (các dòng màu đỏ).\nVui lòng kiểm tra lại!", false);
        //                this.updateRowsOnGrid(danhsachDaDangKy);
        //                this.setNeedUpload(true);
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        //-----------------------------------------------------------------------------------------


        ///// <summary>
        ///// Gửi thông tin đăng ký đến Hải quan.
        ///// </summary>
        //private void send()
        //{
        //    try
        //    {
        //        this.Cursor = Cursors.WaitCursor;
        //        this.NPLDangKy.LoadNPLCollection();
        //        if (this.NPLDangKy.NPLCollection.Count == 0)
        //        {
        //            ShowMessage("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
        //            this.Cursor = Cursors.Default;
        //            return;
        //        }

        //        string[] danhsachDaDangKy = new string[0];

        //        WSForm wsForm = new WSForm();
        //        wsForm.ShowDialog(this);
        //        if (!wsForm.IsReady) return;

        //        this.NPLDangKy.WSSend(ref danhsachDaDangKy);

        //        // Thực hiện kiểm tra.
        //        if (this.NPLDangKy.SoTiepNhan == 0)
        //        {
        //            this.updateRowsOnGrid(danhsachDaDangKy);
        //            ShowMessage("Đăng ký không thành công!\nDo có các nguyên phụ liệu đã được đăng ký rồi.\nVui lòng kiểm tra lại các dòng màu đỏ!", false);
        //        }
        //        else
        //        {
        //            ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + this.NPLDangKy.SoTiepNhan, false);
        //            cmdAdd.Enabled = cmdSend.Enabled = InheritableBoolean.False;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        ///// <summary>
        ///// Hủy thông tin đăng ký đến Hải quan.
        ///// </summary>
        //private void cancel()
        //{
        //    try
        //    {
        //        this.Cursor = Cursors.WaitCursor;
        //        WSForm wsForm = new WSForm();
        //        wsForm.ShowDialog(this);
        //        if (!wsForm.IsReady) return;

        //        bool result = this.NPLDangKy.WSCancel();
        //        if (result)
        //        {
        //            ShowMessage("Hủy thông tin đăng ký thành công!", false);
        //        }
        //        else
        //        {
        //            ShowMessage("Hủy thông tin đăng ký không thành công!", false);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        //private void setNeedUpload(bool status)
        //{
        //    this.needUpload = status;
        //    if (status)
        //        cmdUpload1.Enabled = InheritableBoolean.True;
        //    else
        //        cmdUpload1.Enabled = InheritableBoolean.False;
        //}

        //-----------------------------------------------------------------------------------------

        
        #endregion

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (this.OpenType != OpenFormType.View)
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieuEditForm_CX f = new NguyenPhuLieuEditForm_CX();
                        f.MaHaiQuan = txtMaHaiQuan.Text;
                        f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        f.NPLDetail = (HangDuaVao) e.Row.DataRow;
                        f.NPLCollection = this.NPLDangKy.DanhSachHangDuaVao;
                        f.NPLCollection.RemoveAt(i.Position);
                        f.OpenType = OpenFormType.Edit;
                        f.nplDangKy = NPLDangKy;
                        f.ShowDialog(this);
                        if (f.NPLDetail != null)
                        {
                            this.NPLDangKy.DanhSachHangDuaVao.Insert(i.Position, f.NPLDetail);                            
                        }
                    }
                    dgList.Refetch();
                    break;
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAdd":
                    this.add();
                    break;
            }
        }

    
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
           // if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            {                
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangDuaVao npl = (HangDuaVao)i.GetRow().DataRow;
                        if (npl.ID > 0)
                        {
                            npl.Delete();
                        }
                    }
                }               
            }
            else
            {
                e.Cancel = true;
            }            
        }
    }
}