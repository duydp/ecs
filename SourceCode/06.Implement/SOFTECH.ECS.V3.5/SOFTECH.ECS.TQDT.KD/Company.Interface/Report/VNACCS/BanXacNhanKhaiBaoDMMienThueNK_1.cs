using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
using Company.KDT.SHARE.VNACCS.Messages.Recived;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanXacNhanKhaiBaoDMMienThueNK_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public BanXacNhanKhaiBaoDMMienThueNK_1()
        {
            InitializeComponent();
        }
        public void BindingReport(VAD8010 vad8010,string maNV)
        {
            lblTenThongTinXuat.Text = EnumThongBao.GetTenNV(maNV);
            int HangHoa = vad8010.HangHoa.Count;
            if (HangHoa <= 8)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 8, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblSoDanhMuc.Text = vad8010.A01.GetValue().ToString().ToUpper();
            lblSoQuanLyDanhSachMT.Text = vad8010.A02.GetValue().ToString().ToUpper();
            lblPhanLoaiXNK.Text = vad8010.A03.GetValue().ToString().ToUpper();
            lblCoQuanHQ.Text = vad8010.A04.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad8010.A05.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(vad8010.A05.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            if (vad8010.A06.GetValue().ToString().ToUpper() != "" && vad8010.A06.GetValue().ToString().ToUpper() != "0")
            {
                lblThoiDiemKB.Text = vad8010.A06.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad8010.A06.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad8010.A06.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblThoiDiemKB.Text = "";
            }
            if (Convert.ToDateTime(vad8010.A07.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaySuaDoi.Text = Convert.ToDateTime(vad8010.A07.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaySuaDoi.Text = "";
            }
            if (vad8010.A08.GetValue().ToString().ToUpper() != "" && vad8010.A08.GetValue().ToString().ToUpper() != "0")
            {
                lblThoiDiemSuaDoi.Text = vad8010.A08.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad8010.A08.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad8010.A08.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblThoiDiemSuaDoi.Text = "";
            }
            if (Convert.ToDateTime(vad8010.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHoanThanhKT.Text = Convert.ToDateTime(vad8010.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHoanThanhKT.Text = "";
            }
            if (vad8010.A10.GetValue().ToString().ToUpper() != "" && vad8010.A10.GetValue().ToString().ToUpper() != "0")
            {
                lblThoiDiemHoanThanhKT.Text = vad8010.A10.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad8010.A10.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad8010.A10.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblThoiDiemHoanThanhKT.Text = "";
            }
            lblMaNguoiKhai.Text = vad8010.A11.GetValue().ToString().ToUpper();
            lblTenNguoiKhai.Text = vad8010.A12.GetValue().ToString().ToUpper();
            lblDiaChiNguoiKhai.Text = vad8010.A13.GetValue().ToString().ToUpper();
            lblSoDienThoaiNK.Text = vad8010.A14.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad8010.A15.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanMienThue.Text = Convert.ToDateTime(vad8010.A15.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanMienThue.Text = "";
            }
            lblTenDuAnDauTu.Text = vad8010.A16.GetValue().ToString().ToUpper();
            lblDiaDiemXayDungDuAn.Text = vad8010.A17.GetValue().ToString().ToUpper();
            lblMucTieuDuAn.Text = vad8010.A18.GetValue().ToString().ToUpper();
            lblMaMienGiamKhongChiuThueXNK.Text = vad8010.A19.GetValue().ToString().ToUpper();
            lblDieuKhoanMienGiam.Text = vad8010.A20.GetValue().ToString().ToUpper();
            lblPhamViDK.Text = vad8010.A21.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad8010.A22.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuKienXNK.Text = Convert.ToDateTime(vad8010.A22.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuKienXNK.Text = "";
            }
            lblGiayPhepDauTu_GCNSo.Text = vad8010.A23.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad8010.A24.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayChungNhan.Text = Convert.ToDateTime(vad8010.A24.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayChungNhan.Text = "";
            }
            lblCapBoi.Text = vad8010.A25.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad8010.DA1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblGiayPhepDauTu_GCNDieuChinhLan1.Text = vad8010.DA1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblChungNhanDCSo1.Text = vad8010.DA1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayChungNhanDC1.Text = Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayChungNhanDC1.Text = "";
                        }
                        lblDieuChinhBoi1.Text = vad8010.DA1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblGiayPhepDauTu_GCNDieuChinhLan2.Text = vad8010.DA1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblChungNhanDCSo2.Text = vad8010.DA1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayChungNhanDC2.Text = Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayChungNhanDC2.Text = "";
                        }
                        lblDieuChinhBoi2.Text = vad8010.DA1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblGiayPhepDauTu_GCNDieuChinhLan3.Text = vad8010.DA1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblChungNhanDCSo3.Text = vad8010.DA1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayChungNhanDC3.Text = Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayChungNhanDC3.Text = "";
                        }
                        lblDieuChinhBoi3.Text = vad8010.DA1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblGiayPhepDauTu_GCNDieuChinhLan4.Text = vad8010.DA1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblChungNhanDCSo4.Text = vad8010.DA1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayChungNhanDC4.Text = Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayChungNhanDC4.Text = "";
                        }
                        lblDieuChinhBoi4.Text = vad8010.DA1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblGiayPhepDauTu_GCNDieuChinhLan5.Text = vad8010.DA1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblChungNhanDCSo5.Text = vad8010.DA1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayChungNhanDC5.Text = Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayChungNhanDC5.Text = "";
                        }
                        lblDieuChinhBoi5.Text = vad8010.DA1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
        }
        public void BindingReport(VAD8020 vad8010,string maNV)
        {
            lblTenThongTinXuat.Text = EnumThongBao.GetTenNV(maNV);
            int HangHoa = vad8010.HangHoa.Count;
            if (HangHoa <= 8)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 8, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblSoDanhMuc.Text = vad8010.A01.GetValue().ToString().ToUpper();
            lblSoQuanLyDanhSachMT.Text = vad8010.A02.GetValue().ToString().ToUpper();
            lblPhanLoaiXNK.Text = vad8010.A03.GetValue().ToString().ToUpper();
            lblCoQuanHQ.Text = vad8010.A04.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad8010.A05.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(vad8010.A05.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            if (vad8010.A06.GetValue().ToString().ToUpper() != "" && vad8010.A06.GetValue().ToString().ToUpper() != "0")
            {
                lblThoiDiemKB.Text = vad8010.A06.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad8010.A06.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad8010.A06.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblThoiDiemKB.Text = "";
            }
            if (Convert.ToDateTime(vad8010.A07.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaySuaDoi.Text = Convert.ToDateTime(vad8010.A07.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaySuaDoi.Text = "";
            }
            if (vad8010.A08.GetValue().ToString().ToUpper() != "" && vad8010.A08.GetValue().ToString().ToUpper() != "0")
            {
                lblThoiDiemSuaDoi.Text = vad8010.A08.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad8010.A08.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad8010.A08.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblThoiDiemSuaDoi.Text = "";
            }
            if (Convert.ToDateTime(vad8010.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHoanThanhKT.Text = Convert.ToDateTime(vad8010.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHoanThanhKT.Text = "";
            }
            if (vad8010.A10.GetValue().ToString().ToUpper() != "" && vad8010.A10.GetValue().ToString().ToUpper() != "0")
            {
                lblThoiDiemHoanThanhKT.Text = vad8010.A10.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad8010.A10.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad8010.A10.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblThoiDiemHoanThanhKT.Text = "";
            }
            lblMaNguoiKhai.Text = vad8010.A11.GetValue().ToString().ToUpper();
            lblTenNguoiKhai.Text = vad8010.A12.GetValue().ToString().ToUpper();
            lblDiaChiNguoiKhai.Text = vad8010.A13.GetValue().ToString().ToUpper();
            lblSoDienThoaiNK.Text = vad8010.A14.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad8010.A15.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanMienThue.Text = Convert.ToDateTime(vad8010.A15.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanMienThue.Text = "";
            }
            lblTenDuAnDauTu.Text = vad8010.A16.GetValue().ToString().ToUpper();
            lblDiaDiemXayDungDuAn.Text = vad8010.A17.GetValue().ToString().ToUpper();
            lblMucTieuDuAn.Text = vad8010.A18.GetValue().ToString().ToUpper();
            lblMaMienGiamKhongChiuThueXNK.Text = vad8010.A19.GetValue().ToString().ToUpper();
            lblDieuKhoanMienGiam.Text = vad8010.A20.GetValue().ToString().ToUpper();
            lblPhamViDK.Text = vad8010.A21.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad8010.A22.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuKienXNK.Text = Convert.ToDateTime(vad8010.A22.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuKienXNK.Text = "";
            }
            lblGiayPhepDauTu_GCNSo.Text = vad8010.A23.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad8010.A24.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayChungNhan.Text = Convert.ToDateTime(vad8010.A24.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayChungNhan.Text = "";
            }
            lblCapBoi.Text = vad8010.A25.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad8010.DA1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblGiayPhepDauTu_GCNDieuChinhLan1.Text = vad8010.DA1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblChungNhanDCSo1.Text = vad8010.DA1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayChungNhanDC1.Text = Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayChungNhanDC1.Text = "";
                        }
                        lblDieuChinhBoi1.Text = vad8010.DA1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblGiayPhepDauTu_GCNDieuChinhLan2.Text = vad8010.DA1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblChungNhanDCSo2.Text = vad8010.DA1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayChungNhanDC2.Text = Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayChungNhanDC2.Text = "";
                        }
                        lblDieuChinhBoi2.Text = vad8010.DA1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblGiayPhepDauTu_GCNDieuChinhLan3.Text = vad8010.DA1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblChungNhanDCSo3.Text = vad8010.DA1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayChungNhanDC3.Text = Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayChungNhanDC3.Text = "";
                        }
                        lblDieuChinhBoi3.Text = vad8010.DA1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblGiayPhepDauTu_GCNDieuChinhLan4.Text = vad8010.DA1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblChungNhanDCSo4.Text = vad8010.DA1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayChungNhanDC4.Text = Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayChungNhanDC4.Text = "";
                        }
                        lblDieuChinhBoi4.Text = vad8010.DA1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblGiayPhepDauTu_GCNDieuChinhLan5.Text = vad8010.DA1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblChungNhanDCSo5.Text = vad8010.DA1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayChungNhanDC5.Text = Convert.ToDateTime(vad8010.DA1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayChungNhanDC5.Text = "";
                        }
                        lblDieuChinhBoi5.Text = vad8010.DA1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }

    }
}
