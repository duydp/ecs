﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Janus.Windows.GridEX;
//#if KD_V3 || KD_V4
//using Company.KD.BLL;
//using Company.KD.BLL.KDT;
//#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
//#elif GC_V3 || GC_V4
//using Company.GC.BLL;
//using Company.GC.BLL.KDT;
//#endif
using Company.KDT.SHARE.Components;

namespace SaoChepChuyenDaDuyetMaHQKhac
{
    public partial class CopyAndTransfer : Form
    {
        //#if KD_V3 || KD_V4
        //        public ToKhaiMauDichCollection TKMDColl = new ToKhaiMauDichCollection();
        //#elif SXXK_V3 || SXXK_V4
        public ToKhaiMauDichCollection TKMDColl = new ToKhaiMauDichCollection();
        //#elif GC_V3 || GC_V4
        //        public ToKhaiMauDichCollection TKMDColl = new ToKhaiMauDichCollection();
        //#endif

        public CopyAndTransfer()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            try
            {
                string where = "1 = 1";

                where += string.Format(" AND MaHaiQuan = '{0}' and MaDoanhNghiep='{1}'", GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                where += " AND YEAR(NgayDangKy) = " + numNamDangKy.Value.ToString();

                TKMDColl = new ToKhaiMauDich().SelectCollectionDynamic(where, "NgayDangKy, SoToKhai asc");
                dgList.DataSource = this.TKMDColl;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
            {
                case -1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    break;
                case 0:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    break;
                case 1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                    break;
                case 2:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    break;
                case 5:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Sửa tờ khai";
                    break;
                case 10:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                    break;
                case 11:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                    break;
                default:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chưa xác định";
                    break;
            }
        }

        private void btnThuchien_Click(object sender, EventArgs e)
        {
            try
            {
                if (!GlobalSettings.ValidateNull(donViHaiQuanControl1, errorProvider1, "Mã chi cục Hải quan"))
                {
                    return;
                }

                GridEXRow[] items = dgList.GetCheckedRows();

                if (items.Length <= 0)
                {
                    Helper.Controls.MessageBoxControlV.ShowMessage("Chưa có bản ghi nào được chọn.", false);
                    return;
                }

                if (Helper.Controls.MessageBoxControlH.ShowMessage("Bạn có chắc chắn muốn thực hiển sao chép và chuyển các tờ khai từ chi cục HQ cũ '" + GlobalSettings.MA_HAI_QUAN + "' đến HQ mới '" + donViHaiQuanControl1.Ma + "' không?.", true) == DialogResult.No)
                {
                    return;
                }

                string maHQ = donViHaiQuanControl1.Ma;
                string error = "";

                foreach (GridEXRow row in items)
                {
                    if (row.IsChecked && row.RowType == RowType.Record)
                    {
                        ToKhaiMauDich obj = (ToKhaiMauDich)row.DataRow;

                        if (obj.SoToKhai == 0 || obj.NgayDangKy.Equals(new DateTime(1900, 1, 1)))
                        {
                            row.Cells["TrangThaiXuLy"].Text = "Không hợp lệ";
                            continue;
                        }

                        //#if SXXK_V3 || SXXK_V4
                        obj.LoadHMDCollection();
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkDaDangKy = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                        tkDaDangKy.MaHaiQuan = maHQ;
                        tkDaDangKy.NamDangKy = (short)obj.NgayDangKy.Year;
                        tkDaDangKy.MaLoaiHinh = obj.MaLoaiHinh;
                        tkDaDangKy.SoToKhai = obj.SoToKhai;
                        tkDaDangKy.NgayDangKy = obj.NgayDangKy;
                        if (obj.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            obj.NgayTiepNhan = obj.NgayDangKy;

                        if (!tkDaDangKy.Load())
                        {
                            error = SaoChepToKhai(row, maHQ);

                            row.Cells["TrangThaiXuLy"].Text = row.Cells["TrangThaiXuLy"].ToolTipText = error.Trim().Length > 0 ? error : "Chuyển trạng thái thành công";
                        }
                        else
                        {
                            row.Cells["TrangThaiXuLy"].Text = row.Cells["TrangThaiXuLy"].ToolTipText = "Thông tin tờ khai này trùng trong danh sách đăng ký!";
                        }
                        Company.KDT.SHARE.Components.Globals.XmlSaveMessage(string.Empty, obj.ID, Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay, row.Cells["TrangThaiXuLy"].Text);
                        //#endif
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(ex.Message, false);
            }
        }

        private void btnHuybo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CopyAndTransfer_Load(object sender, EventArgs e)
        {
            try
            {
                string PhanHe = "";
                //#if KD_V3 || KD_V4
                //                PhanHe = "KD";
                //#elif SXXK_V3 || SXXK_V4
                PhanHe = "SXXK";
                //#elif GC_V3 || GC_V4
                //                PhanHe = "GC";
                //#endif
                string subject = string.Format("{0} - ECS.TQDT.{1}.V4", GlobalSettings.TEN_DON_VI, PhanHe);
                this.Text += (subject.Length != 0 ? subject : "");

                lblInfo.Text = string.Format("{0} - {1}/ {2} - {3}/ {4}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI, GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME);

                WSForm2 wsform2 = new WSForm2();
                wsform2.ShowDialog();
                if (WSForm2.IsSuccess == true)
                {
                    this.Show();

                    if (wsform2 != null)
                        wsform2.Hide();

                    BindData();
                }
                else
                    this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnReLoad_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void numNamDangKy_Leave(object sender, EventArgs e)
        {
            if (numNamDangKy.Text.Trim().Length == 0)
            {
                numNamDangKy.Text = DateTime.Now.Year.ToString();
            }
        }

        private string SaoChepToKhai(GridEXRow drow, string maHQNew)
        {
            try
            {
                if (!(drow != null && drow.RowType == RowType.Record))
                    return "Dữ liệu tờ khai rỗng.";

                //Chuyen to khai vao theo doi
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.ID = ((ToKhaiMauDich)drow.DataRow).ID;
                tkmd.Load();
                tkmd.LoadHMDCollection();
                //tkmd.LoadChungTuHaiQuan(); //Khong lay thong tin chung tu dinh kem

                if (tkmd.HMDCollection.Count == 0)
                    return "Tờ khai không có thông tin hàng.";

                foreach (HangMauDich hmd in tkmd.HMDCollection)
                {
                    hmd.ID = 0;
                }

                tkmd.MaHaiQuan = maHQNew;

                tkmd.InsertUpdate();

                //Chuyen to khai vao da dang ky
                tkmd.TransgferDataToSXXK();

                return "";
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return ex.Message; }
        }
    }
}
