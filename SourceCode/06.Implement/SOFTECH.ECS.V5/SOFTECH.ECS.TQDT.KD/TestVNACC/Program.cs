﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace TestVNACC
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new TestTaiHeThongVNACCS());
            //Application.Run(new FormTestControl());
            Application.Run(new GenClassfrm());
        }
    }
}
