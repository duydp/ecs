using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class VanDon : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string SoVanDon { set; get; }
		public DateTime NgayVanDon { set; get; }
		public bool HangRoi { set; get; }
		public string SoHieuPTVT { set; get; }
		public DateTime NgayDenPTVT { set; get; }
		public string MaHangVT { set; get; }
		public string TenHangVT { set; get; }
		public string TenPTVT { set; get; }
		public string QuocTichPTVT { set; get; }
		public string NuocXuat_ID { set; get; }
		public string MaNguoiNhanHang { set; get; }
		public string TenNguoiNhanHang { set; get; }
		public string MaNguoiGiaoHang { set; get; }
		public string TenNguoiGiaoHang { set; get; }
		public string CuaKhauNhap_ID { set; get; }
		public string CuaKhauXuat { set; get; }
		public string MaNguoiNhanHangTrungGian { set; get; }
		public string TenNguoiNhanHangTrungGian { set; get; }
		public string MaCangXepHang { set; get; }
		public string TenCangXepHang { set; get; }
		public string MaCangDoHang { set; get; }
		public string TenCangDoHang { set; get; }
		public long TKMD_ID { set; get; }
		public string DKGH_ID { set; get; }
		public string DiaDiemGiaoHang { set; get; }
		public string NoiDi { set; get; }
		public string SoHieuChuyenDi { set; get; }
		public DateTime NgayKhoiHanh { set; get; }
		public int TongSoKien { set; get; }
		public string LoaiKien { set; get; }
		public string SoHieuKien { set; get; }
		public string DiaDiemChuyenTai { set; get; }
		public string LoaiVanDon { set; get; }
		public string MoTaKhac { set; get; }
		public string ID_NuocPhatHanh { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<VanDon> ConvertToCollection(IDataReader reader)
		{
			IList<VanDon> collection = new List<VanDon>();
			while (reader.Read())
			{
				VanDon entity = new VanDon();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("HangRoi"))) entity.HangRoi = reader.GetBoolean(reader.GetOrdinal("HangRoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangVT"))) entity.MaHangVT = reader.GetString(reader.GetOrdinal("MaHangVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangVT"))) entity.TenHangVT = reader.GetString(reader.GetOrdinal("TenHangVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenPTVT"))) entity.TenPTVT = reader.GetString(reader.GetOrdinal("TenPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT"))) entity.QuocTichPTVT = reader.GetString(reader.GetOrdinal("QuocTichPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXuat_ID"))) entity.NuocXuat_ID = reader.GetString(reader.GetOrdinal("NuocXuat_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauNhap_ID"))) entity.CuaKhauNhap_ID = reader.GetString(reader.GetOrdinal("CuaKhauNhap_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauXuat"))) entity.CuaKhauXuat = reader.GetString(reader.GetOrdinal("CuaKhauXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHangTrungGian"))) entity.MaNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("MaNguoiNhanHangTrungGian"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHangTrungGian"))) entity.TenNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("TenNguoiNhanHangTrungGian"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCangXepHang"))) entity.MaCangXepHang = reader.GetString(reader.GetOrdinal("MaCangXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCangXepHang"))) entity.TenCangXepHang = reader.GetString(reader.GetOrdinal("TenCangXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCangDoHang"))) entity.TenCangDoHang = reader.GetString(reader.GetOrdinal("TenCangDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDi"))) entity.NoiDi = reader.GetString(reader.GetOrdinal("NoiDi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoKien"))) entity.TongSoKien = reader.GetInt32(reader.GetOrdinal("TongSoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKien"))) entity.LoaiKien = reader.GetString(reader.GetOrdinal("LoaiKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuKien"))) entity.SoHieuKien = reader.GetString(reader.GetOrdinal("SoHieuKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemChuyenTai"))) entity.DiaDiemChuyenTai = reader.GetString(reader.GetOrdinal("DiaDiemChuyenTai"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTaKhac"))) entity.MoTaKhac = reader.GetString(reader.GetOrdinal("MoTaKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocPhatHanh"))) entity.ID_NuocPhatHanh = reader.GetString(reader.GetOrdinal("ID_NuocPhatHanh"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<VanDon> collection, long id)
        {
            foreach (VanDon item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VanDon VALUES(@SoVanDon, @NgayVanDon, @HangRoi, @SoHieuPTVT, @NgayDenPTVT, @MaHangVT, @TenHangVT, @TenPTVT, @QuocTichPTVT, @NuocXuat_ID, @MaNguoiNhanHang, @TenNguoiNhanHang, @MaNguoiGiaoHang, @TenNguoiGiaoHang, @CuaKhauNhap_ID, @CuaKhauXuat, @MaNguoiNhanHangTrungGian, @TenNguoiNhanHangTrungGian, @MaCangXepHang, @TenCangXepHang, @MaCangDoHang, @TenCangDoHang, @TKMD_ID, @DKGH_ID, @DiaDiemGiaoHang, @NoiDi, @SoHieuChuyenDi, @NgayKhoiHanh, @TongSoKien, @LoaiKien, @SoHieuKien, @DiaDiemChuyenTai, @LoaiVanDon, @MoTaKhac, @ID_NuocPhatHanh)";
            string update = "UPDATE t_KDT_VanDon SET SoVanDon = @SoVanDon, NgayVanDon = @NgayVanDon, HangRoi = @HangRoi, SoHieuPTVT = @SoHieuPTVT, NgayDenPTVT = @NgayDenPTVT, MaHangVT = @MaHangVT, TenHangVT = @TenHangVT, TenPTVT = @TenPTVT, QuocTichPTVT = @QuocTichPTVT, NuocXuat_ID = @NuocXuat_ID, MaNguoiNhanHang = @MaNguoiNhanHang, TenNguoiNhanHang = @TenNguoiNhanHang, MaNguoiGiaoHang = @MaNguoiGiaoHang, TenNguoiGiaoHang = @TenNguoiGiaoHang, CuaKhauNhap_ID = @CuaKhauNhap_ID, CuaKhauXuat = @CuaKhauXuat, MaNguoiNhanHangTrungGian = @MaNguoiNhanHangTrungGian, TenNguoiNhanHangTrungGian = @TenNguoiNhanHangTrungGian, MaCangXepHang = @MaCangXepHang, TenCangXepHang = @TenCangXepHang, MaCangDoHang = @MaCangDoHang, TenCangDoHang = @TenCangDoHang, TKMD_ID = @TKMD_ID, DKGH_ID = @DKGH_ID, DiaDiemGiaoHang = @DiaDiemGiaoHang, NoiDi = @NoiDi, SoHieuChuyenDi = @SoHieuChuyenDi, NgayKhoiHanh = @NgayKhoiHanh, TongSoKien = @TongSoKien, LoaiKien = @LoaiKien, SoHieuKien = @SoHieuKien, DiaDiemChuyenTai = @DiaDiemChuyenTai, LoaiVanDon = @LoaiVanDon, MoTaKhac = @MoTaKhac, ID_NuocPhatHanh = @ID_NuocPhatHanh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VanDon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HangRoi", SqlDbType.Bit, "HangRoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuPTVT", SqlDbType.VarChar, "SoHieuPTVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDenPTVT", SqlDbType.DateTime, "NgayDenPTVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangVT", SqlDbType.VarChar, "MaHangVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangVT", SqlDbType.NVarChar, "TenHangVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPTVT", SqlDbType.NVarChar, "TenPTVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuocTichPTVT", SqlDbType.VarChar, "QuocTichPTVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXuat_ID", SqlDbType.VarChar, "NuocXuat_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, "MaNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, "MaNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, "TenNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, "CuaKhauNhap_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CuaKhauXuat", SqlDbType.NVarChar, "CuaKhauXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, "MaNguoiNhanHangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, "TenNguoiNhanHangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangXepHang", SqlDbType.VarChar, "MaCangXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCangXepHang", SqlDbType.NVarChar, "TenCangXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangDoHang", SqlDbType.VarChar, "MaCangDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCangDoHang", SqlDbType.NVarChar, "TenCangDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DKGH_ID", SqlDbType.VarChar, "DKGH_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, "DiaDiemGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDi", SqlDbType.NVarChar, "NoiDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, "SoHieuChuyenDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhoiHanh", SqlDbType.DateTime, "NgayKhoiHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoKien", SqlDbType.Int, "TongSoKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKien", SqlDbType.NVarChar, "LoaiKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuKien", SqlDbType.NVarChar, "SoHieuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemChuyenTai", SqlDbType.NVarChar, "DiaDiemChuyenTai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiVanDon", SqlDbType.NVarChar, "LoaiVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTaKhac", SqlDbType.NVarChar, "MoTaKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_NuocPhatHanh", SqlDbType.VarChar, "ID_NuocPhatHanh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HangRoi", SqlDbType.Bit, "HangRoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuPTVT", SqlDbType.VarChar, "SoHieuPTVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDenPTVT", SqlDbType.DateTime, "NgayDenPTVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangVT", SqlDbType.VarChar, "MaHangVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangVT", SqlDbType.NVarChar, "TenHangVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPTVT", SqlDbType.NVarChar, "TenPTVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuocTichPTVT", SqlDbType.VarChar, "QuocTichPTVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXuat_ID", SqlDbType.VarChar, "NuocXuat_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, "MaNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, "MaNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, "TenNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, "CuaKhauNhap_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CuaKhauXuat", SqlDbType.NVarChar, "CuaKhauXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, "MaNguoiNhanHangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, "TenNguoiNhanHangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangXepHang", SqlDbType.VarChar, "MaCangXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCangXepHang", SqlDbType.NVarChar, "TenCangXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangDoHang", SqlDbType.VarChar, "MaCangDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCangDoHang", SqlDbType.NVarChar, "TenCangDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DKGH_ID", SqlDbType.VarChar, "DKGH_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, "DiaDiemGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDi", SqlDbType.NVarChar, "NoiDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, "SoHieuChuyenDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhoiHanh", SqlDbType.DateTime, "NgayKhoiHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoKien", SqlDbType.Int, "TongSoKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKien", SqlDbType.NVarChar, "LoaiKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuKien", SqlDbType.NVarChar, "SoHieuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemChuyenTai", SqlDbType.NVarChar, "DiaDiemChuyenTai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiVanDon", SqlDbType.NVarChar, "LoaiVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTaKhac", SqlDbType.NVarChar, "MoTaKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_NuocPhatHanh", SqlDbType.VarChar, "ID_NuocPhatHanh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VanDon VALUES(@SoVanDon, @NgayVanDon, @HangRoi, @SoHieuPTVT, @NgayDenPTVT, @MaHangVT, @TenHangVT, @TenPTVT, @QuocTichPTVT, @NuocXuat_ID, @MaNguoiNhanHang, @TenNguoiNhanHang, @MaNguoiGiaoHang, @TenNguoiGiaoHang, @CuaKhauNhap_ID, @CuaKhauXuat, @MaNguoiNhanHangTrungGian, @TenNguoiNhanHangTrungGian, @MaCangXepHang, @TenCangXepHang, @MaCangDoHang, @TenCangDoHang, @TKMD_ID, @DKGH_ID, @DiaDiemGiaoHang, @NoiDi, @SoHieuChuyenDi, @NgayKhoiHanh, @TongSoKien, @LoaiKien, @SoHieuKien, @DiaDiemChuyenTai, @LoaiVanDon, @MoTaKhac, @ID_NuocPhatHanh)";
            string update = "UPDATE t_KDT_VanDon SET SoVanDon = @SoVanDon, NgayVanDon = @NgayVanDon, HangRoi = @HangRoi, SoHieuPTVT = @SoHieuPTVT, NgayDenPTVT = @NgayDenPTVT, MaHangVT = @MaHangVT, TenHangVT = @TenHangVT, TenPTVT = @TenPTVT, QuocTichPTVT = @QuocTichPTVT, NuocXuat_ID = @NuocXuat_ID, MaNguoiNhanHang = @MaNguoiNhanHang, TenNguoiNhanHang = @TenNguoiNhanHang, MaNguoiGiaoHang = @MaNguoiGiaoHang, TenNguoiGiaoHang = @TenNguoiGiaoHang, CuaKhauNhap_ID = @CuaKhauNhap_ID, CuaKhauXuat = @CuaKhauXuat, MaNguoiNhanHangTrungGian = @MaNguoiNhanHangTrungGian, TenNguoiNhanHangTrungGian = @TenNguoiNhanHangTrungGian, MaCangXepHang = @MaCangXepHang, TenCangXepHang = @TenCangXepHang, MaCangDoHang = @MaCangDoHang, TenCangDoHang = @TenCangDoHang, TKMD_ID = @TKMD_ID, DKGH_ID = @DKGH_ID, DiaDiemGiaoHang = @DiaDiemGiaoHang, NoiDi = @NoiDi, SoHieuChuyenDi = @SoHieuChuyenDi, NgayKhoiHanh = @NgayKhoiHanh, TongSoKien = @TongSoKien, LoaiKien = @LoaiKien, SoHieuKien = @SoHieuKien, DiaDiemChuyenTai = @DiaDiemChuyenTai, LoaiVanDon = @LoaiVanDon, MoTaKhac = @MoTaKhac, ID_NuocPhatHanh = @ID_NuocPhatHanh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VanDon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HangRoi", SqlDbType.Bit, "HangRoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuPTVT", SqlDbType.VarChar, "SoHieuPTVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDenPTVT", SqlDbType.DateTime, "NgayDenPTVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangVT", SqlDbType.VarChar, "MaHangVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangVT", SqlDbType.NVarChar, "TenHangVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPTVT", SqlDbType.NVarChar, "TenPTVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuocTichPTVT", SqlDbType.VarChar, "QuocTichPTVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXuat_ID", SqlDbType.VarChar, "NuocXuat_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, "MaNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, "MaNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, "TenNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, "CuaKhauNhap_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CuaKhauXuat", SqlDbType.NVarChar, "CuaKhauXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, "MaNguoiNhanHangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, "TenNguoiNhanHangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangXepHang", SqlDbType.VarChar, "MaCangXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCangXepHang", SqlDbType.NVarChar, "TenCangXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangDoHang", SqlDbType.VarChar, "MaCangDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCangDoHang", SqlDbType.NVarChar, "TenCangDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DKGH_ID", SqlDbType.VarChar, "DKGH_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, "DiaDiemGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDi", SqlDbType.NVarChar, "NoiDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, "SoHieuChuyenDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhoiHanh", SqlDbType.DateTime, "NgayKhoiHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoKien", SqlDbType.Int, "TongSoKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKien", SqlDbType.NVarChar, "LoaiKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuKien", SqlDbType.NVarChar, "SoHieuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemChuyenTai", SqlDbType.NVarChar, "DiaDiemChuyenTai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiVanDon", SqlDbType.NVarChar, "LoaiVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTaKhac", SqlDbType.NVarChar, "MoTaKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_NuocPhatHanh", SqlDbType.VarChar, "ID_NuocPhatHanh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HangRoi", SqlDbType.Bit, "HangRoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuPTVT", SqlDbType.VarChar, "SoHieuPTVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDenPTVT", SqlDbType.DateTime, "NgayDenPTVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangVT", SqlDbType.VarChar, "MaHangVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangVT", SqlDbType.NVarChar, "TenHangVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPTVT", SqlDbType.NVarChar, "TenPTVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuocTichPTVT", SqlDbType.VarChar, "QuocTichPTVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXuat_ID", SqlDbType.VarChar, "NuocXuat_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, "MaNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, "MaNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, "TenNguoiGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, "CuaKhauNhap_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CuaKhauXuat", SqlDbType.NVarChar, "CuaKhauXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, "MaNguoiNhanHangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, "TenNguoiNhanHangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangXepHang", SqlDbType.VarChar, "MaCangXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCangXepHang", SqlDbType.NVarChar, "TenCangXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangDoHang", SqlDbType.VarChar, "MaCangDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCangDoHang", SqlDbType.NVarChar, "TenCangDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DKGH_ID", SqlDbType.VarChar, "DKGH_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, "DiaDiemGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDi", SqlDbType.NVarChar, "NoiDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, "SoHieuChuyenDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhoiHanh", SqlDbType.DateTime, "NgayKhoiHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoKien", SqlDbType.Int, "TongSoKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKien", SqlDbType.NVarChar, "LoaiKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuKien", SqlDbType.NVarChar, "SoHieuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemChuyenTai", SqlDbType.NVarChar, "DiaDiemChuyenTai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiVanDon", SqlDbType.NVarChar, "LoaiVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTaKhac", SqlDbType.NVarChar, "MoTaKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_NuocPhatHanh", SqlDbType.VarChar, "ID_NuocPhatHanh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VanDon Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VanDon_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<VanDon> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<VanDon> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<VanDon> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<VanDon> SelectCollectionBy_TKMD_ID(long tKMD_ID)
		{
            IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_VanDon_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VanDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VanDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "p_KDT_VanDon_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertVanDon(string soVanDon, DateTime ngayVanDon, bool hangRoi, string soHieuPTVT, DateTime ngayDenPTVT, string maHangVT, string tenHangVT, string tenPTVT, string quocTichPTVT, string nuocXuat_ID, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string cuaKhauNhap_ID, string cuaKhauXuat, string maNguoiNhanHangTrungGian, string tenNguoiNhanHangTrungGian, string maCangXepHang, string tenCangXepHang, string maCangDoHang, string tenCangDoHang, long tKMD_ID, string dKGH_ID, string diaDiemGiaoHang, string noiDi, string soHieuChuyenDi, DateTime ngayKhoiHanh, int tongSoKien, string loaiKien, string soHieuKien, string diaDiemChuyenTai, string loaiVanDon, string moTaKhac, string iD_NuocPhatHanh)
		{
			VanDon entity = new VanDon();	
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.HangRoi = hangRoi;
			entity.SoHieuPTVT = soHieuPTVT;
			entity.NgayDenPTVT = ngayDenPTVT;
			entity.MaHangVT = maHangVT;
			entity.TenHangVT = tenHangVT;
			entity.TenPTVT = tenPTVT;
			entity.QuocTichPTVT = quocTichPTVT;
			entity.NuocXuat_ID = nuocXuat_ID;
			entity.MaNguoiNhanHang = maNguoiNhanHang;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.MaNguoiGiaoHang = maNguoiGiaoHang;
			entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
			entity.CuaKhauNhap_ID = cuaKhauNhap_ID;
			entity.CuaKhauXuat = cuaKhauXuat;
			entity.MaNguoiNhanHangTrungGian = maNguoiNhanHangTrungGian;
			entity.TenNguoiNhanHangTrungGian = tenNguoiNhanHangTrungGian;
			entity.MaCangXepHang = maCangXepHang;
			entity.TenCangXepHang = tenCangXepHang;
			entity.MaCangDoHang = maCangDoHang;
			entity.TenCangDoHang = tenCangDoHang;
			entity.TKMD_ID = tKMD_ID;
			entity.DKGH_ID = dKGH_ID;
			entity.DiaDiemGiaoHang = diaDiemGiaoHang;
			entity.NoiDi = noiDi;
			entity.SoHieuChuyenDi = soHieuChuyenDi;
			entity.NgayKhoiHanh = ngayKhoiHanh;
			entity.TongSoKien = tongSoKien;
			entity.LoaiKien = loaiKien;
			entity.SoHieuKien = soHieuKien;
			entity.DiaDiemChuyenTai = diaDiemChuyenTai;
			entity.LoaiVanDon = loaiVanDon;
			entity.MoTaKhac = moTaKhac;
			entity.ID_NuocPhatHanh = iD_NuocPhatHanh;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VanDon_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
			db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
			db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object) NgayDenPTVT);
			db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
			db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
			db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
			db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
			db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
			db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
			db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
			db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.NVarChar, TenCangXepHang);
			db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
			db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.NVarChar, TenCangDoHang);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
			db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
			db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
			db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
			db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object) NgayKhoiHanh);
			db.AddInParameter(dbCommand, "@TongSoKien", SqlDbType.Int, TongSoKien);
			db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
			db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
			db.AddInParameter(dbCommand, "@DiaDiemChuyenTai", SqlDbType.NVarChar, DiaDiemChuyenTai);
			db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, LoaiVanDon);
			db.AddInParameter(dbCommand, "@MoTaKhac", SqlDbType.NVarChar, MoTaKhac);
			db.AddInParameter(dbCommand, "@ID_NuocPhatHanh", SqlDbType.VarChar, ID_NuocPhatHanh);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<VanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VanDon item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVanDon(long id, string soVanDon, DateTime ngayVanDon, bool hangRoi, string soHieuPTVT, DateTime ngayDenPTVT, string maHangVT, string tenHangVT, string tenPTVT, string quocTichPTVT, string nuocXuat_ID, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string cuaKhauNhap_ID, string cuaKhauXuat, string maNguoiNhanHangTrungGian, string tenNguoiNhanHangTrungGian, string maCangXepHang, string tenCangXepHang, string maCangDoHang, string tenCangDoHang, long tKMD_ID, string dKGH_ID, string diaDiemGiaoHang, string noiDi, string soHieuChuyenDi, DateTime ngayKhoiHanh, int tongSoKien, string loaiKien, string soHieuKien, string diaDiemChuyenTai, string loaiVanDon, string moTaKhac, string iD_NuocPhatHanh)
		{
			VanDon entity = new VanDon();			
			entity.ID = id;
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.HangRoi = hangRoi;
			entity.SoHieuPTVT = soHieuPTVT;
			entity.NgayDenPTVT = ngayDenPTVT;
			entity.MaHangVT = maHangVT;
			entity.TenHangVT = tenHangVT;
			entity.TenPTVT = tenPTVT;
			entity.QuocTichPTVT = quocTichPTVT;
			entity.NuocXuat_ID = nuocXuat_ID;
			entity.MaNguoiNhanHang = maNguoiNhanHang;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.MaNguoiGiaoHang = maNguoiGiaoHang;
			entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
			entity.CuaKhauNhap_ID = cuaKhauNhap_ID;
			entity.CuaKhauXuat = cuaKhauXuat;
			entity.MaNguoiNhanHangTrungGian = maNguoiNhanHangTrungGian;
			entity.TenNguoiNhanHangTrungGian = tenNguoiNhanHangTrungGian;
			entity.MaCangXepHang = maCangXepHang;
			entity.TenCangXepHang = tenCangXepHang;
			entity.MaCangDoHang = maCangDoHang;
			entity.TenCangDoHang = tenCangDoHang;
			entity.TKMD_ID = tKMD_ID;
			entity.DKGH_ID = dKGH_ID;
			entity.DiaDiemGiaoHang = diaDiemGiaoHang;
			entity.NoiDi = noiDi;
			entity.SoHieuChuyenDi = soHieuChuyenDi;
			entity.NgayKhoiHanh = ngayKhoiHanh;
			entity.TongSoKien = tongSoKien;
			entity.LoaiKien = loaiKien;
			entity.SoHieuKien = soHieuKien;
			entity.DiaDiemChuyenTai = diaDiemChuyenTai;
			entity.LoaiVanDon = loaiVanDon;
			entity.MoTaKhac = moTaKhac;
			entity.ID_NuocPhatHanh = iD_NuocPhatHanh;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VanDon_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
			db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
			db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object) NgayDenPTVT);
			db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
			db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
			db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
			db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
			db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
			db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
			db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
			db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.NVarChar, TenCangXepHang);
			db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
			db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.NVarChar, TenCangDoHang);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
			db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
			db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
			db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
			db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object) NgayKhoiHanh);
			db.AddInParameter(dbCommand, "@TongSoKien", SqlDbType.Int, TongSoKien);
			db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
			db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
			db.AddInParameter(dbCommand, "@DiaDiemChuyenTai", SqlDbType.NVarChar, DiaDiemChuyenTai);
			db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, LoaiVanDon);
			db.AddInParameter(dbCommand, "@MoTaKhac", SqlDbType.NVarChar, MoTaKhac);
			db.AddInParameter(dbCommand, "@ID_NuocPhatHanh", SqlDbType.VarChar, ID_NuocPhatHanh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<VanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VanDon item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVanDon(long id, string soVanDon, DateTime ngayVanDon, bool hangRoi, string soHieuPTVT, DateTime ngayDenPTVT, string maHangVT, string tenHangVT, string tenPTVT, string quocTichPTVT, string nuocXuat_ID, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string cuaKhauNhap_ID, string cuaKhauXuat, string maNguoiNhanHangTrungGian, string tenNguoiNhanHangTrungGian, string maCangXepHang, string tenCangXepHang, string maCangDoHang, string tenCangDoHang, long tKMD_ID, string dKGH_ID, string diaDiemGiaoHang, string noiDi, string soHieuChuyenDi, DateTime ngayKhoiHanh, int tongSoKien, string loaiKien, string soHieuKien, string diaDiemChuyenTai, string loaiVanDon, string moTaKhac, string iD_NuocPhatHanh)
		{
			VanDon entity = new VanDon();			
			entity.ID = id;
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.HangRoi = hangRoi;
			entity.SoHieuPTVT = soHieuPTVT;
			entity.NgayDenPTVT = ngayDenPTVT;
			entity.MaHangVT = maHangVT;
			entity.TenHangVT = tenHangVT;
			entity.TenPTVT = tenPTVT;
			entity.QuocTichPTVT = quocTichPTVT;
			entity.NuocXuat_ID = nuocXuat_ID;
			entity.MaNguoiNhanHang = maNguoiNhanHang;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.MaNguoiGiaoHang = maNguoiGiaoHang;
			entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
			entity.CuaKhauNhap_ID = cuaKhauNhap_ID;
			entity.CuaKhauXuat = cuaKhauXuat;
			entity.MaNguoiNhanHangTrungGian = maNguoiNhanHangTrungGian;
			entity.TenNguoiNhanHangTrungGian = tenNguoiNhanHangTrungGian;
			entity.MaCangXepHang = maCangXepHang;
			entity.TenCangXepHang = tenCangXepHang;
			entity.MaCangDoHang = maCangDoHang;
			entity.TenCangDoHang = tenCangDoHang;
			entity.TKMD_ID = tKMD_ID;
			entity.DKGH_ID = dKGH_ID;
			entity.DiaDiemGiaoHang = diaDiemGiaoHang;
			entity.NoiDi = noiDi;
			entity.SoHieuChuyenDi = soHieuChuyenDi;
			entity.NgayKhoiHanh = ngayKhoiHanh;
			entity.TongSoKien = tongSoKien;
			entity.LoaiKien = loaiKien;
			entity.SoHieuKien = soHieuKien;
			entity.DiaDiemChuyenTai = diaDiemChuyenTai;
			entity.LoaiVanDon = loaiVanDon;
			entity.MoTaKhac = moTaKhac;
			entity.ID_NuocPhatHanh = iD_NuocPhatHanh;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VanDon_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
			db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
			db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object) NgayDenPTVT);
			db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
			db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
			db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
			db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
			db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
			db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
			db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
			db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
			db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.NVarChar, TenCangXepHang);
			db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
			db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.NVarChar, TenCangDoHang);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
			db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
			db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
			db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
			db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object) NgayKhoiHanh);
			db.AddInParameter(dbCommand, "@TongSoKien", SqlDbType.Int, TongSoKien);
			db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
			db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
			db.AddInParameter(dbCommand, "@DiaDiemChuyenTai", SqlDbType.NVarChar, DiaDiemChuyenTai);
			db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, LoaiVanDon);
			db.AddInParameter(dbCommand, "@MoTaKhac", SqlDbType.NVarChar, MoTaKhac);
			db.AddInParameter(dbCommand, "@ID_NuocPhatHanh", SqlDbType.VarChar, ID_NuocPhatHanh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<VanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VanDon item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVanDon(long id)
		{
			VanDon entity = new VanDon();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VanDon_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_VanDon_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VanDon_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<VanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VanDon item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}