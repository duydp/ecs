﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class ContractDocuments
    {
        /// <summary>
        /// Thông tin về hợp đồng
        /// </summary>
       [XmlElement("ContractDocument")]
       public List<ContractDocument> ContractDocument { get; set; }
    }
}
