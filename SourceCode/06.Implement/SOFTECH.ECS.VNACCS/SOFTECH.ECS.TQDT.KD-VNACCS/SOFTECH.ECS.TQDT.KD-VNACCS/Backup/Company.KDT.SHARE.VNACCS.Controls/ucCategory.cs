﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System.Reflection;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Data.Filtering;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public partial class ucCategory : ucBase
    {
        public Color ColorControl { get; set; }
        public string Where { get; set; }
        private bool _showColumnCode = true;
        public bool ShowColumnCode
        {
            get { return _showColumnCode; }
            set { _showColumnCode = value; }
        }

        private bool _showColumnName = true;
        public bool ShowColumnName
        {
            get { return _showColumnName; }
            set { _showColumnName = value; }
        }

        private ECategory _categoryType = ECategory.E001;
        /// <summary>
        /// Xác định loại danh mục.
        /// </summary>
        public ECategory CategoryType
        {
            get { return _categoryType; }
            set { _categoryType = value; }
        }

        private string _referenceDB;
        /// <summary>
        /// Xác định mã loại danh mục
        /// </summary>
        public string ReferenceDB
        {
            get
            {
                _referenceDB = CategoryType.ToString();

                return _referenceDB;
            }
            //set { _referenceDB = value; }
        }

        /// <summary>
        /// Mã
        /// </summary>
        public string Code
        {
            get
            {
                if (SetValidate)
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.EditValue.ToString().Trim();
                    }
                }
                else
                    return cboCategory.EditValue != null ? cboCategory.EditValue.ToString().Trim() : "";
            }
            set
            {
                if (CategoryType == ECategory.A014 && value != "")
                    cboCategory.EditValue = Convert.ToInt32(value);
                else
                    cboCategory.EditValue = value;
            }
        }

        /// <summary>
        /// Tên Tiếng việt
        /// </summary>
        public string Name_VN
        {
            get
            {
                if (SetValidate)
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Text.Trim();
                    }
                }
                else
                    return cboCategory.Text.Trim();
            }
            set { cboCategory.Text = value; }
        }

        public string TagName
        {
            get { return cboCategory.Tag != null ? cboCategory.Tag.ToString() : ""; }
            set { cboCategory.Tag = value; }
        }

        private EImportExport _importType = EImportExport.I;
        /// <summary>
        /// Xác định loại Nhập/ Xuất
        /// </summary>
        public EImportExport ImportType
        {
            get { return _importType; }
            set { _importType = value; }
        }

        /// <summary>
        /// NumberOfKeyItems: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm cặp khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class ContainerSize
        /// </summary>
        public string NumberOfKeyItems
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Properties.GetRowByKeyValue("NumberOfKeyItems").ToString().Trim();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        /// <summary>
        /// CountryCode: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm cặp khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class CityUNLOCODE
        /// </summary>
        public string CountryCode
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Properties.GetRowByKeyValue("CountryCode").ToString().Trim();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        /// <summary>
        /// CustomsSubSectionCode: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class Nhóm xử lý hồ sơ (Customs-Sub Section)
        /// </summary>
        public string CustomsSubSectionCode
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Properties.GetRowByKeyValue("CustomsSubSectionCode").ToString().Trim();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        private bool _setValidate = false;
        public bool SetValidate
        {
            get { return _setValidate; }
            set { _setValidate = value; }
        }

        private bool _isValidate = false;
        public bool IsValidate
        {
            get
            {
                return SetValidate ? (_isValidate = ValidateControl(false)) : true;
            }
            set { _isValidate = value; }
        }

        private bool _setOnlyWarning = false;
        public bool SetOnlyWarning
        {
            get { return _setOnlyWarning; }
            set { _setOnlyWarning = value; }
        }

        private bool _isOnlyWarning = false;
        public bool IsOnlyWarning
        {
            get
            {
                return SetOnlyWarning ? (_isOnlyWarning = ValidateControl(true)) : false;
            }
            set
            {
                _isOnlyWarning = false;
            }
        }

        private bool _isUpperCase = false;
        public bool IsUpperCase
        {
            set
            {
                if (value == true)
                {
                    cboCategory.Properties.CharacterCasing = CharacterCasing.Upper;
                }
                else
                {
                    cboCategory.Properties.CharacterCasing = CharacterCasing.Normal;
                }
            }
            get
            {
                return _isUpperCase;
            }
        }
        public bool _isTrue=true;
        public bool IsTrue {
            get
            {
                return _isTrue;
            }
            set
            {
                _isTrue = value;
            
            }
        }
        public string _isString;
        public string IsString 
        {
            get
            {
                return _isString;
            }
            set
            {
                _isString = value;
            } 
        }
        DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit lookUpProperties;
        public ucCategory()
        {
            InitializeComponent();
            lookUpProperties = cboCategory.Properties;
            lookUpProperties.ImmediatePopup = true;
            cboCategory.Properties.NullText = "";

            cboCategory.Enter += new EventHandler(cboCategory_Enter);
        }

        private void ucMaLoaiHinhIDA_Load(object sender, EventArgs e)
        {
            LoadData(false);
            if (ColorControl != null)
                this.cboCategory.BackColor = ColorControl;
        }

        string valueMember = "Code";
        string displayMember = "Name";
        private void LoadData(bool isReload)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string whereCondition = "";
                string orderBy = "";

                string valueMemberCaption = "Mã";
                string displayMemberCaption = "Tên";
                whereCondition += base.WhereCondition != "" ? base.WhereCondition + " And " : "";

                switch (CategoryType)
                {
                    //case ECategory.A001:
                    //    break;

                    case ECategory.A014: //Nhóm xử lý hồ sơ
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%' and ImportExportClassification = '{1}'", ReferenceDB.ToString(), ImportType.ToString());
                        orderBy = "CustomsSubSectionCode asc";
                        valueMember = "ID"; //"CustomsSubSectionCode";
                        displayMember = "Name";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCustomsSubSections = VNACC_Category_CustomsSubSection.SelectCollectionBy(ucBase.dataCustomsSubSectionGlobals, ReferenceDB.ToString(), ImportType.ToString());
                        VNACC_Category_CustomsSubSection ItemA014 = new VNACC_Category_CustomsSubSection();
                        dataCustomsSubSections.Add(ItemA014);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCustomsSubSections;
                        #endregion
                        break;

                    case ECategory.A015: // Mã nước(Country, coded), Mã nước xuất xứ
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "NationCode asc";
                        valueMember = "NationCode";
                        displayMember = "CountryShortName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataNations = VNACC_Category_Nation.SelectCollectionBy(ucBase.dataNationGlobals, ReferenceDB.ToString());
                        VNACC_Category_Nation ItemA015 = new VNACC_Category_Nation();
                        dataNations.Add(ItemA015);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataNations;
                        #endregion
                        break;
                    case ECategory.A016: // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
                        #region Load data
                        whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        //orderBy = "CityCode asc";
                        valueMember = "LOCODE";
                        displayMember = "CityNameOrStateName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";
                                                //Create obj and load data
                        dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionAllMinimize2(Where);
                        dataCitys.Add(new VNACC_Category_CityUNLOCODE { LOCODE = "", CityNameOrStateName = "" });
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCitys;
                        #endregion
                        break;

                    //case ECategory.A017:
                    //    break;
                    //case ECategory.A021:
                    //    break;

                    case ECategory.A038: //Cơ quan Hải quan
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "CustomsCode asc";
                        valueMember = "CustomsCode";
                        displayMember = "CustomsOfficeNameInVietnamese";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCustomsOffices = VNACC_Category_CustomsOffice.SelectCollectionBy(ucBase.dataCustomsOfficeGlobals, ReferenceDB.ToString());
                        VNACC_Category_CustomsOffice ItemA038 = new VNACC_Category_CustomsOffice();
                        dataCustomsOffices.Add(ItemA038);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCustomsOffices;
                        #endregion
                        break;

                    case ECategory.A202: // Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp), Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "BondedAreaCode asc";
                        valueMember = "BondedAreaCode";
                        displayMember = "BondedAreaName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCargos = VNACC_Category_Cargo.SelectCollectionBy(ucBase.dataCargoGlobals, ReferenceDB.ToString());
                        VNACC_Category_Cargo ItemA202 = new VNACC_Category_Cargo();
                        dataCargos.Add(ItemA202);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCargos;
                        #endregion
                        break;
                

                    case ECategory.A301: //Container size
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "ContainerSizeCode asc";
                        valueMember = "ContainerSizeCode";
                        displayMember = "ContainerSizeName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataContainerSizes = VNACC_Category_ContainerSize.SelectCollectionBy(ucBase.dataContainerSizeGlobals, ReferenceDB.ToString());
                        VNACC_Category_ContainerSize ItemA301 = new VNACC_Category_ContainerSize();
                        dataContainerSizes.Add(ItemA301);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataContainerSizes;
                        #endregion
                        break;

                    //case ECategory.B314:
                    //    break;
                    case ECategory.A305: //Vị trí điểm neo đậu
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "CodeHQ asc";
                        valueMember = "Code";
                        displayMember = "MoTa_HeThong";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataBerth = VNACC_Category_Berth.SelectCollectionBy(ucBase.dataBerthGlobals, ReferenceDB.ToString());
                        VNACC_Category_Berth ItemA305 = new VNACC_Category_Berth();
                        dataBerth.Add(ItemA305);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataBerth;
                        #endregion
                        break;
                    case ECategory.A316: //Mã đơn vị tính (Packages unit code)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "NumberOfPackagesUnitCode asc";
                        valueMember = "NumberOfPackagesUnitCode";
                        displayMember = "NumberOfPackagesUnitName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataPackagesUnits = VNACC_Category_PackagesUnit.SelectCollectionBy(ucBase.dataPackagesUnitGlobals, ReferenceDB.ToString());
                        VNACC_Category_PackagesUnit ItemA316 = new VNACC_Category_PackagesUnit();
                        dataPackagesUnits.Add(ItemA316);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataPackagesUnits;
                        #endregion
                        break;

                    case ECategory.A404: //Mã biểu thuế nhập khẩu
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "TaxCode asc";
                        valueMember = "TaxCode";
                        displayMember = "TaxName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataTaxClassificationCodes = VNACC_Category_TaxClassificationCode.SelectCollectionBy(ucBase.dataTaxClassificationCodeGlobals, ReferenceDB.ToString());
                        VNACC_Category_TaxClassificationCode ItemA404 = new VNACC_Category_TaxClassificationCode();
                        dataTaxClassificationCodes.Add(ItemA404);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataTaxClassificationCodes;
                        #endregion
                        break;

                    case ECategory.A501: // Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "Code asc";
                        valueMember = "Code";
                        displayMember = "Name";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataQuantityUnits = VNACC_Category_QuantityUnit.SelectCollectionBy(ucBase.dataQuantityUnitGlobals, ReferenceDB.ToString());
                        VNACC_Category_QuantityUnit ItemA501 = new VNACC_Category_QuantityUnit();
                        dataQuantityUnits.Add(ItemA501);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataQuantityUnits;
                        #endregion
                        break;
                    case ECategory.A232: // Mã HS 4 số
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "MaHang asc";
                        valueMember = "MaHang";
                        displayMember = "MoTa_VN";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";
                        List<HaiQuan_MaHS> listMaHS = new List<HaiQuan_MaHS>();
                        List<HaiQuan_MaHS> listMaHS2 = new List<HaiQuan_MaHS>();
                        //Create obj and load data
                        listMaHS = KDT.SHARE.Components.DuLieuChuan.HaiQuan_MaHS.SelectCollectionDynamic(" MaHang like '%0000' ", "");
                        foreach (HaiQuan_MaHS item in listMaHS)
                        {
                            item.MaHang = item.MaHang.Substring(0, 4);
                            listMaHS2.Add(item);
                        }
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = listMaHS2;
                        #endregion
                        break;

                    case ECategory.A506: //Mã số hàng hóa (HS)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "HSCode asc";
                        valueMember = "HSCode";
                        displayMember = "";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataHSCodes = VNACC_Category_HSCode.SelectCollectionBy(ucBase.dataHSCodeGlobals, ReferenceDB.ToString());
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataHSCodes;
                        #endregion
                        break;

                    //case ECategory.A515:
                    //    break;
                    //case ECategory.A517:
                    //    break;

                    case ECategory.A527: //Mã đồng tiền
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "CurrencyCode asc";
                        valueMember = "CurrencyCode";
                        displayMember = "CurrencyName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCurrencyExchanges = VNACC_Category_CurrencyExchange.SelectCollectionBy(ucBase.dataCurrencyExchangeGlobals, ReferenceDB.ToString());
                        VNACC_Category_CurrencyExchange ItemA527 = new VNACC_Category_CurrencyExchange();
                        dataCurrencyExchanges.Add(ItemA527);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCurrencyExchanges;
                        #endregion
                        break;

                    case ECategory.A546: //Loại chứng từ đính kèm
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "ApplicationProcedureType asc";
                        valueMember = "ApplicationProcedureType";
                        displayMember = "ApplicationProcedureName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataApplicationProcedureTypes = VNACC_Category_ApplicationProcedureType.SelectCollectionBy(ucBase.dataApplicationProcedureTypeGlobals, ReferenceDB.ToString());
                        VNACC_Category_ApplicationProcedureType ItemA546 = new VNACC_Category_ApplicationProcedureType();
                        dataApplicationProcedureTypes.Add(ItemA546);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataApplicationProcedureTypes;
                        #endregion
                        break;

                    //case ECategory.B501:
                    //    break;
                    //case ECategory.B530:
                    //    break;
                    //case ECategory.B700:
                    //    break;
                    //case ECategory.B701:
                    //    break;

                    case ECategory.A601: //Mã địa điểm dỡ hàng (Stations)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "StationCode asc";
                        valueMember = "StationCode";
                        displayMember = "StationName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataStations = VNACC_Category_Station.SelectCollectionBy(ucBase.dataStationGlobals, ReferenceDB.ToString());
                        VNACC_Category_Station ItemA601 = new VNACC_Category_Station();
                        dataStations.Add(ItemA601);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataStations;
                        #endregion
                        break;

                    case ECategory.A620: //Mã địa điểm dỡ hàng (Border gate)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "BorderGateCode asc";
                        valueMember = "BorderGateCode";
                        displayMember = "BorderGateName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataBorderGates = VNACC_Category_BorderGate.SelectCollectionBy(ucBase.dataBorderGateGlobals, ReferenceDB.ToString());
                        VNACC_Category_BorderGate ItemA620 = new VNACC_Category_BorderGate();
                        dataBorderGates.Add(ItemA620);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataBorderGates;
                        #endregion
                        break;
                    case ECategory.A214://Mã hãng hàng không
                    case ECategory.A621: //Transport means (Vehicle)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "TransportMeansCode asc";
                        valueMember = "TransportMeansCode";
                        displayMember = "TransportMeansName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataTransportMeans = VNACC_Category_TransportMean.SelectCollectionBy(ucBase.dataTransportMeanGlobals, ReferenceDB.ToString());
                        VNACC_Category_TransportMean ItemA621 = new VNACC_Category_TransportMean();
                        dataTransportMeans.Add(ItemA621);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataTransportMeans;
                        #endregion
                        break;

                    case ECategory.A700: //OGA User
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "OfficeOfApplicationCode asc";
                        valueMember = "OfficeOfApplicationCode";
                        displayMember = "OfficeOfApplicationName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataOGAUsers = VNACC_Category_OGAUser.SelectCollectionBy(ucBase.dataOGAUserGlobals, ReferenceDB.ToString());
                        VNACC_Category_OGAUser ItemA700 = new VNACC_Category_OGAUser();
                        dataOGAUsers.Add(ItemA700);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataOGAUsers;
                        #endregion
                        break;

                    case ECategory.A312://Mã mục đích vận chuyển
                    case ECategory.A317://mã Loại hình vận tải
                    case ECategory.A308://mã đánh dấu hàng hóa
                    case ECategory.A519: //Mã văn bản pháp quy khác
                    case ECategory.A520: //Mã miễn / Giảm / Không chịu thuế nhập khẩu
                    case ECategory.A521: //Mã miễn / Giảm / Không chịu thuế và thu khác
                    case ECategory.A522: //Mã áp dụng thuế suất / Mức thuế và thu khác
                    case ECategory.A402: //Mã xác định mức thuế nhập khẩu theo lượng(ma ap dụng mưc thue tuyet doi)
                    case ECategory.A528: //Phân loại giấy phép nhập khẩu
                    case ECategory.A401: //Mã tên khoản điều chỉnh
                    case ECategory.A557: //Mã phân loại phí vận chuyển                    
                    case ECategory.B524: //Trạng thái khai báo VNACC
                    case ECategory.A553: //Mã điều kiện giá hóa đơn
                    case ECategory.E001: //Mã loại hình nhập
                    case ECategory.E002: //Mã loại hình xuất
                    case ECategory.E003:
                    case ECategory.E004:
                    case ECategory.E005:
                    case ECategory.E006:
                    case ECategory.E007:
                    case ECategory.E008:
                    case ECategory.E009:
                    case ECategory.E010:
                    case ECategory.E011:
                    case ECategory.E012:
                    case ECategory.E013:
                    case ECategory.E014:
                    case ECategory.E015:
                    case ECategory.E016:
                    case ECategory.E017:
                    case ECategory.E018:
                    case ECategory.E019:
                    case ECategory.E020:
                    case ECategory.E021:
                    case ECategory.E022:
                    case ECategory.E023:
                    case ECategory.E024:
                    case ECategory.E025:
                    case ECategory.E026:
                    case ECategory.E027:
                    case ECategory.E028:
                    case ECategory.E029:
                    case ECategory.E030:
                    case ECategory.E031:
                    case ECategory.E032:
                    case ECategory.E033:
                    case ECategory.E035://Ma ngan hang
                    case ECategory.E036://Ma loại phương tiện vận chuyển
                    case ECategory.E037://Loai Manifect(Hàng hóa)
                    case ECategory.E038://Loai Manifect(Hàng hóa)

                        #region Load data
                        //whereCondition += string.Format("ReferenceDB = '{0}'", ReferenceDB.ToString());
                        orderBy = "Code asc";
                        valueMember = "Code";
                        displayMember = "Name_VN";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        datas = VNACC_Category_Common.SelectCollectionBy(ucBase.dataGlobals, ReferenceDB.ToString());
                        VNACC_Category_Common ItemEmpty = new VNACC_Category_Common();
                        datas.Add(ItemEmpty);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = datas;
                        #endregion
                        break;
                }

                //SET COLUMN
                //cboCategory.Properties.Columns.Clear();
                // The field providing the editor's display text.
                if (ShowColumnCode && ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = displayMember;
                    cboCategory.Properties.ValueMember = valueMember;
                }
                else if (ShowColumnCode && !ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = valueMember;
                    cboCategory.Properties.ValueMember = valueMember;
                }
                else if (!ShowColumnCode && ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = displayMember;
                    cboCategory.Properties.ValueMember = displayMember;
                }

                // Add two columns to the dropdown.
                //LookUpColumnInfoCollection coll = new LookUpColumnInfoCollection();
                //// A column to display the Format field's values.
                //coll.Add(new LookUpColumnInfo(valueMember, valueMemberCaption, 0));
                //if (displayMember != "")
                //    coll.Add(new LookUpColumnInfo(displayMember, displayMemberCaption, 0));

                //if (CategoryType == ECategory.A014)
                //    coll.Add(new LookUpColumnInfo("CustomsSubSectionCode", "Mã", 0));

                //if (CategoryType == ECategory.A016)
                //    coll.Add(new LookUpColumnInfo("CountryCode", "Mã nước", 0));

                lookUpProperties.View.PopulateColumns(cboCategory.Properties.DataSource);
                for (int i = 0; i < lookUpProperties.View.Columns.Count; i++)
                {
                    if (lookUpProperties.View.Columns[i].FieldName != valueMember && lookUpProperties.View.Columns[i].FieldName != displayMember)
                    {
                        lookUpProperties.View.Columns[i].Visible = false;
                    }

                }
                lookUpProperties.View.Columns[valueMember].Caption = valueMemberCaption;
                lookUpProperties.View.Columns[displayMember].Caption = displayMemberCaption;

                //  Set column widths according to their contents and resize the popup, if required.
                cboCategory.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                // Enable auto completion search mode.
                //cboCategory.Properties.SearchMode = SearchMode.AutoComplete; //Luu y: khi thiet lap thuoc tinh nay, khi chon Name co Code null se tu dong lay dong dau tien trong danh sach.
                // Specify the column against which to perform the search.
                //cboCategory.Properties.AutoSearchColumnIndex = 1;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }
        }

        /// <summary>
        /// Tải mới lại dữ liệu danh mục
        /// </summary>
        public void ReLoadData()
        {
            LoadData(true);
        }

        /// <summary>
        /// Validate or only warning
        /// </summary>
        /// <param name="isOnlyWarning"></param>
        /// <returns></returns>
        private bool ValidateControl(bool isOnlyWarning)
        {
            bool isValid = true;

            isValid &= ValidationControl.ValidateNullLookUpEditDevExpress(cboCategory, dxErrorProvider, "", isOnlyWarning);

            if (isValid)
            {
                isValid &= ValidationControl.ValidateChooseLookUpEditDevExpress(cboCategory, dxErrorProvider, "", isOnlyWarning);
            }

            return isValid;
        }

        public delegate void EditValueChangedHandle(object sender, EventArgs e);
        public event EditValueChangedHandle EditValueChanged;
        private void cboCategory_EditValueChanged(object sender, EventArgs e)
        {
            if (EditValueChanged != null)
            {
                EditValueChanged(sender, e);
            }
        }

        public delegate void EnterHandle(object sender, EventArgs e);
        public event EnterHandle OnEnter;
        void cboCategory_Enter(object sender, EventArgs e)
        {
            if (OnEnter != null)
            {
                OnEnter(sender, e);
            }
        }

        #region Khanhhn


        private void AddNewObject<T>(string valueMember, T obj, object value)
        {
            PropertyInfo pro = typeof(T).GetProperty(valueMember);
            if (pro != null)
                pro.SetValue(obj, value, null);
            if (obj != null)
                (cboCategory.Properties.DataSource as List<T>).Add(obj);
        }

        private void cboCategory_ProcessNewValue(object sender, ProcessNewValueEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.DisplayValue.ToString().Trim()) && MessageBox.Show(this, e.DisplayValue.ToString() +
      "' không có trong danh sách, bạn có muốn thêm vào?",
      "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                string valueMember = string.Empty;
                string value = e.DisplayValue.ToString();
                switch (CategoryType)
                {
                    case ECategory.A014: //Nhóm xử lý hồ sơ
                        #region  Add Item
                        valueMember = "ID"; //"CustomsSubSectionCode";
                        VNACC_Category_CustomsSubSection ItemA014 = new VNACC_Category_CustomsSubSection();
                        AddNewObject<VNACC_Category_CustomsSubSection>(valueMember, ItemA014, value);
                        #endregion
                        break;

                    case ECategory.A015: // Mã nước(Country, coded), Mã nước xuất xứ
                        #region  Add Item
                        valueMember = "NationCode";
                        VNACC_Category_Nation ItemA015 = new VNACC_Category_Nation();
                        AddNewObject<VNACC_Category_Nation>(valueMember, ItemA015, value);
                        #endregion
                        break;
                    case ECategory.A016: // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
                        #region  Add Item
                        valueMember = "CityCode";
                        VNACC_Category_CityUNLOCODE ItemA016 = new VNACC_Category_CityUNLOCODE();
                        AddNewObject<VNACC_Category_CityUNLOCODE>(valueMember, ItemA016, value);
                        #endregion
                        break;
                    case ECategory.A038: //Cơ quan Hải quan
                        #region  Add Item
                        valueMember = "CustomsCode";
                        VNACC_Category_CustomsOffice ItemA038 = new VNACC_Category_CustomsOffice();
                        AddNewObject<VNACC_Category_CustomsOffice>(valueMember, ItemA038, value);
                        #endregion
                        break;

                    case ECategory.A202: // Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp), Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                        #region  Add Item
                        valueMember = "BondedAreaCode";
                        VNACC_Category_Cargo ItemA202 = new VNACC_Category_Cargo();
                        AddNewObject<VNACC_Category_Cargo>(valueMember, ItemA202, value);
                        #endregion
                        break;

                    case ECategory.A301: //Container size
                        #region  Add Item
                        valueMember = "ContainerSizeCode";
                        VNACC_Category_ContainerSize ItemA301 = new VNACC_Category_ContainerSize();
                        AddNewObject<VNACC_Category_ContainerSize>(valueMember, ItemA301, value);
                        #endregion
                        break;
                    case ECategory.A316: //Mã đơn vị tính (Packages unit code)
                        #region  Add Item
                        valueMember = "NumberOfPackagesUnitCode";
                        VNACC_Category_PackagesUnit ItemA316 = new VNACC_Category_PackagesUnit();
                        AddNewObject<VNACC_Category_PackagesUnit>(valueMember, ItemA316, value);
                        #endregion
                        break;
                    case ECategory.A404: //Mã biểu thuế nhập khẩu
                        #region  Add Item
                        valueMember = "TaxCode";
                        VNACC_Category_TaxClassificationCode ItemA404 = new VNACC_Category_TaxClassificationCode();
                        AddNewObject<VNACC_Category_TaxClassificationCode>(valueMember, ItemA404, value);
                        #endregion
                        break;

                    case ECategory.A501: // Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng
                        #region  Add Item
                        valueMember = "Code";
                        VNACC_Category_QuantityUnit ItemA501 = new VNACC_Category_QuantityUnit();
                        AddNewObject<VNACC_Category_QuantityUnit>(valueMember, ItemA501, value);
                        #endregion
                        break;

                    case ECategory.A506: //Mã số hàng hóa (HS)
                        #region  Add Item
                        valueMember = "HSCode";
                        VNACC_Category_HSCode ItemA506 = new VNACC_Category_HSCode();
                        AddNewObject<VNACC_Category_HSCode>(valueMember, ItemA506, value);
                        #endregion
                        break;
                    case ECategory.A527: //Mã đồng tiền
                        #region  Add Item
                        valueMember = "CurrencyCode";
                        VNACC_Category_CurrencyExchange ItemA527 = new VNACC_Category_CurrencyExchange();
                        AddNewObject<VNACC_Category_CurrencyExchange>(valueMember, ItemA527, value);
                        #endregion
                        break;
                    case ECategory.A546: //Loại chứng từ đính kèm
                        #region  Add Item
                        valueMember = "ApplicationProcedureType";
                        VNACC_Category_ApplicationProcedureType ItemA546 = new VNACC_Category_ApplicationProcedureType();
                        AddNewObject<VNACC_Category_ApplicationProcedureType>(valueMember, ItemA546, value);
                        #endregion
                        break;
                    case ECategory.A601: //Mã địa điểm dỡ hàng (Stations)
                        #region  Add Item
                        valueMember = "StationCode";
                        VNACC_Category_Station ItemA601 = new VNACC_Category_Station();
                        AddNewObject<VNACC_Category_Station>(valueMember, ItemA601, value);
                        #endregion
                        break;

                    case ECategory.A620: //Mã địa điểm dỡ hàng (Border gate)
                        #region  Add Item
                        valueMember = "BorderGateCode";
                        VNACC_Category_BorderGate ItemA620 = new VNACC_Category_BorderGate();
                        AddNewObject<VNACC_Category_BorderGate>(valueMember, ItemA620, value);
                        #endregion
                        break;
                    case ECategory.A214: //Mã hãng hàng không
                        #region  Add Item
                        valueMember = "TransportMeansCode";
                        VNACC_Category_TransportMean ItemA214 = new VNACC_Category_TransportMean();
                        AddNewObject<VNACC_Category_TransportMean>(valueMember, ItemA214, value);
                        #endregion
                        break;
                    case ECategory.A621: //Transport means (Vehicle)
                        #region  Add Item
                        valueMember = "TransportMeansCode";
                        VNACC_Category_TransportMean ItemA621 = new VNACC_Category_TransportMean();
                        AddNewObject<VNACC_Category_TransportMean>(valueMember, ItemA621, value);
                        #endregion
                        break;

                    case ECategory.A700: //OGA User
                        #region  Add Item
                        valueMember = "OfficeOfApplicationCode";
                        VNACC_Category_OGAUser ItemA700 = new VNACC_Category_OGAUser();
                        AddNewObject<VNACC_Category_OGAUser>(valueMember, ItemA700, value);
                        #endregion
                        break;

                    case ECategory.A401: //Mã tên khoản điều chỉnh
                    case ECategory.A557: //Mã phân loại phí vận chuyển                    
                    case ECategory.B524: //Trạng thái khai báo VNACC
                    case ECategory.A553://Mã điều kiện giá hóa đơn
                    case ECategory.A519://Mã văn bản pháp quy khác
                    case ECategory.E001:
                    case ECategory.E002:
                    case ECategory.E003:
                    case ECategory.E004:
                    case ECategory.E005:
                    case ECategory.E006:
                    case ECategory.E007:
                    case ECategory.E008:
                    case ECategory.E009:
                    case ECategory.E010:
                    case ECategory.E011:
                    case ECategory.E012:
                    case ECategory.E013:
                    case ECategory.E014:
                    case ECategory.E015:
                    case ECategory.E016:
                    case ECategory.E017:
                    case ECategory.E018:
                    case ECategory.E019:
                    case ECategory.E020:
                    case ECategory.E021:
                    case ECategory.E022:
                    case ECategory.E023:
                    case ECategory.E024:
                    case ECategory.E025:
                    case ECategory.E026:
                    case ECategory.E027:
                    case ECategory.E028:
                    case ECategory.E029:
                    case ECategory.E030:
                    case ECategory.E031:
                    case ECategory.E032:
                    case ECategory.E033:
                    case ECategory.E034:

                        #region  Add Item
                        valueMember = "Code";
                        VNACC_Category_Common ItemEmpty = new VNACC_Category_Common();
                        AddNewObject<VNACC_Category_Common>(valueMember, ItemEmpty, value);
                        #endregion
                        break;
                }

                e.Handled = true;
            }
        }


        private void FilterLookup(object sender)
        {
            Text += " ! ";
            GridLookUpEdit edit = sender as GridLookUpEdit;
            GridView gridView = edit.Properties.View as GridView;
            FieldInfo fi = gridView.GetType().GetField("extraFilter", BindingFlags.NonPublic | BindingFlags.Instance);
            Text = edit.AutoSearchText;
            BinaryOperator op1 = new BinaryOperator(valueMember, "%" + edit.AutoSearchText + "%", BinaryOperatorType.Like);
            BinaryOperator op2 = new BinaryOperator(displayMember, "%" + edit.AutoSearchText + "%", BinaryOperatorType.Like);
            string filterCondition = new GroupOperator(GroupOperatorType.Or, new CriteriaOperator[] { op1, op2 }).ToString();
            fi.SetValue(gridView, filterCondition);

            MethodInfo mi = gridView.GetType().GetMethod("ApplyColumnsFilterEx", BindingFlags.NonPublic | BindingFlags.Instance);
            mi.Invoke(gridView, null);
        }
        void gridLookUpEdit1_Popup(object sender, EventArgs e)
        {
            FilterLookup(sender);

        }

        private void gridLookUpEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            this.BeginInvoke(new System.Windows.Forms.MethodInvoker(delegate
            {
                FilterLookup(sender);
            }));
        }
        #endregion

    }
}
