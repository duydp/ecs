﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.GC.BLL
{
    public class Globals
    {
        public static Company.GC.BLL.KDT.GC.HopDong hopDongGC = new KDT.GC.HopDong();

        #region GIA CONG

        public static List<Company.GC.BLL.KDT.GC.HopDong> GetDanhSachHopDongDaDangKy(string maDoanhNghiep, string maHaiQuan, string soHopDong, int namDangKy)
        {
            List<Company.GC.BLL.KDT.GC.HopDong> hdCollectionDK = new List<Company.GC.BLL.KDT.GC.HopDong>();
            string where = "1=1";

            where += " AND TrangThaiXuLy = 1";

            if (maDoanhNghiep != "")
                where += string.Format(" AND MaDoanhNghiep = '{0}'", maDoanhNghiep);

            if (maHaiQuan != "")
                where += string.Format(" AND MaHaiQuan = '{0}' ", maHaiQuan);

            if (soHopDong.Trim().Length > 0)
                where += string.Format(" AND SoHopDong LIKE '%{0}%' ", soHopDong.Trim());

            if (namDangKy > 0)
                where += string.Format(" AND Year(NgayKy) = '{0}' ", namDangKy);

            hdCollectionDK = GetDanhSachHopDongDaDangKy(where, "NgayKy DESC");

            return hdCollectionDK;
        }

        public static List<Company.GC.BLL.KDT.GC.HopDong> GetDanhSachHopDongDaDangKy(string where, string orderBy)
        {
            return Company.GC.BLL.KDT.GC.HopDong.SelectCollectionDynamic(where, orderBy);
        }

        #endregion
    }
}
