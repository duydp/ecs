namespace Company.Interface.SXXK.BangKe
{
    partial class BK10Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList2_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK10Form));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.grpThongTinNPL = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.txtDonViTinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.calendarCombo1 = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.txtLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cmdSave = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cvLuong = new Company.Controls.CustomValidation.CompareValidator();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList2 = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinNPL)).BeginInit();
            this.grpThongTinNPL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList2)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.cmdSave);
            this.grbMain.Controls.Add(this.grpThongTinNPL);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Size = new System.Drawing.Size(557, 365);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(11, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(255, 14);
            this.label5.TabIndex = 1;
            this.label5.Text = "Danh sách nguyên phụ liệu tự cung ứng";
            // 
            // grpThongTinNPL
            // 
            this.grpThongTinNPL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpThongTinNPL.BackColor = System.Drawing.Color.Transparent;
            this.grpThongTinNPL.Controls.Add(this.btnSave);
            this.grpThongTinNPL.Controls.Add(this.txtDonViTinh);
            this.grpThongTinNPL.Controls.Add(this.calendarCombo1);
            this.grpThongTinNPL.Controls.Add(this.label9);
            this.grpThongTinNPL.Controls.Add(this.txtSoHoaDon);
            this.grpThongTinNPL.Controls.Add(this.label8);
            this.grpThongTinNPL.Controls.Add(this.label7);
            this.grpThongTinNPL.Controls.Add(this.txtMaHS);
            this.grpThongTinNPL.Controls.Add(this.label6);
            this.grpThongTinNPL.Controls.Add(this.btnAdd);
            this.grpThongTinNPL.Controls.Add(this.txtLuong);
            this.grpThongTinNPL.Controls.Add(this.label14);
            this.grpThongTinNPL.Controls.Add(this.label12);
            this.grpThongTinNPL.Controls.Add(this.txtTenNPL);
            this.grpThongTinNPL.Controls.Add(this.txtMaNPL);
            this.grpThongTinNPL.Controls.Add(this.label4);
            this.grpThongTinNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpThongTinNPL.Location = new System.Drawing.Point(12, 10);
            this.grpThongTinNPL.Name = "grpThongTinNPL";
            this.grpThongTinNPL.Size = new System.Drawing.Size(533, 147);
            this.grpThongTinNPL.TabIndex = 0;
            this.grpThongTinNPL.Text = "Thông tin NPL xuất tự cung ứng";
            this.grpThongTinNPL.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grpThongTinNPL.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(397, 111);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(71, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtDonViTinh
            // 
            this.txtDonViTinh.Location = new System.Drawing.Point(348, 56);
            this.txtDonViTinh.Name = "txtDonViTinh";
            this.txtDonViTinh.ReadOnly = true;
            this.txtDonViTinh.Size = new System.Drawing.Size(100, 21);
            this.txtDonViTinh.TabIndex = 7;
            this.txtDonViTinh.VisualStyleManager = this.vsmMain;
            // 
            // calendarCombo1
            // 
            this.calendarCombo1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.calendarCombo1.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.calendarCombo1.DropDownCalendar.Name = "";
            this.calendarCombo1.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.calendarCombo1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarCombo1.Location = new System.Drawing.Point(348, 83);
            this.calendarCombo1.Name = "calendarCombo1";
            this.calendarCombo1.Nullable = true;
            this.calendarCombo1.NullButtonText = "Xóa";
            this.calendarCombo1.ShowNullButton = true;
            this.calendarCombo1.Size = new System.Drawing.Size(100, 21);
            this.calendarCombo1.TabIndex = 11;
            this.calendarCombo1.TodayButtonText = "Hôm nay";
            this.calendarCombo1.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.calendarCombo1.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(259, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Ngày hóa đơn";
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.Location = new System.Drawing.Point(96, 83);
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(126, 21);
            this.txtSoHoaDon.TabIndex = 9;
            this.txtSoHoaDon.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(21, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Số hóa đơn";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(259, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Đơn vị tính";
            // 
            // txtMaHS
            // 
            this.txtMaHS.Location = new System.Drawing.Point(96, 56);
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.ReadOnly = true;
            this.txtMaHS.Size = new System.Drawing.Size(126, 21);
            this.txtMaHS.TabIndex = 5;
            this.txtMaHS.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Mã HS";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAdd.Icon")));
            this.btnAdd.Location = new System.Drawing.Point(310, 111);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(81, 23);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.VisualStyleManager = this.vsmMain;
            this.btnAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // txtLuong
            // 
            this.txtLuong.DecimalDigits = 3;
            this.txtLuong.Location = new System.Drawing.Point(171, 112);
            this.txtLuong.Name = "txtLuong";
            this.txtLuong.Size = new System.Drawing.Size(132, 21);
            this.txtLuong.TabIndex = 13;
            this.txtLuong.Text = "0.000";
            this.txtLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtLuong.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(45, 117);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(120, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Lượng NPL cung ứng";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(259, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Tên NPL";
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.Location = new System.Drawing.Point(348, 29);
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.ReadOnly = true;
            this.txtTenNPL.Size = new System.Drawing.Size(160, 21);
            this.txtTenNPL.TabIndex = 3;
            this.txtTenNPL.VisualStyleManager = this.vsmMain;
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaNPL.Location = new System.Drawing.Point(96, 29);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(126, 21);
            this.txtMaNPL.TabIndex = 1;
            this.txtMaNPL.VisualStyleManager = this.vsmMain;
            this.txtMaNPL.TextChanged += new System.EventHandler(this.txtMaNPL_TextChanged);
            this.txtMaNPL.ButtonClick += new System.EventHandler(this.txtMaNPL_ButtonClick);
            this.txtMaNPL.Leave += new System.EventHandler(this.txtMaNPL_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mã NPL";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this.grpThongTinNPL;
            this.lvsError.SetErrorCaption(this.cvError, "Thông báo");
            this.lvsError.SetErrorMessage(this.cvError, "Có một số lỗi sau:");
            this.cvError.HostingForm = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaNPL;
            this.rfvMa.ErrorMessage = "\"Mã NPL\" không được để trống.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave.Icon")));
            this.cmdSave.Location = new System.Drawing.Point(347, 337);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(116, 23);
            this.cmdSave.TabIndex = 3;
            this.cmdSave.Text = "Lưu bảng kê";
            this.cmdSave.VisualStyleManager = this.vsmMain;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(469, 337);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // cvLuong
            // 
            this.cvLuong.ControlToValidate = this.txtLuong;
            this.cvLuong.ErrorMessage = "\"Lượng NPL tự cung ứng\" không hợp lệ.";
            this.cvLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvLuong.Icon")));
            this.cvLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvLuong.ValueToCompare = "0";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgList2);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(12, 188);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(533, 143);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgList2
            // 
            this.dgList2.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList2.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList2.AlternatingColors = true;
            this.dgList2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList2.AutomaticSort = false;
            this.dgList2.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList2.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList2_DesignTimeLayout.LayoutString = resources.GetString("dgList2_DesignTimeLayout.LayoutString");
            this.dgList2.DesignTimeLayout = dgList2_DesignTimeLayout;
            this.dgList2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList2.GroupByBoxVisible = false;
            this.dgList2.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList2.ImageList = this.ImageList1;
            this.dgList2.Location = new System.Drawing.Point(2, 8);
            this.dgList2.Name = "dgList2";
            this.dgList2.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList2.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList2.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList2.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList2.Size = new System.Drawing.Size(530, 133);
            this.dgList2.TabIndex = 0;
            this.dgList2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList2.VisualStyleManager = this.vsmMain;
            this.dgList2.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList2_RowDoubleClick);
            // 
            // BK10Form
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(557, 365);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK10Form";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Bảng kê nguyên phụ liệu tự cung ứng";
            this.Load += new System.EventHandler(this.BK10Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinNPL)).EndInit();
            this.grpThongTinNPL.ResumeLayout(false);
            this.grpThongTinNPL.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox grpThongTinNPL;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuong;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDon;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.CalendarCombo.CalendarCombo calendarCombo1;
        private Janus.Windows.EditControls.UIButton cmdSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Company.Controls.CustomValidation.CompareValidator cvLuong;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonViTinh;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgList2;
        private Janus.Windows.EditControls.UIButton btnSave;
    }
}