GO
DELETE FROM dbo.t_HaiQuan_PhuongThucThanhToan
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('CAD' ,N'Trả tiền lấy chứng từ',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('CASH' ,N'Tiền mặt',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('DA' ,N'Nhờ thu chấp nhận chứng từ',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('DP' ,N'Nhờ thu kèm chứng từ',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('GV' ,N'Góp vốn',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('H-D-H' ,N'Hàng đổi hàng',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('H-T-N' ,N'Hàng trả nợ',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('HPH' ,N'Hối phiếu',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('LC' ,N'Tín dụng thư',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('LDDT' ,N'Liên doanh đầu tư',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('OA' ,N'Mở tài khoản thanh toán',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('TTR' ,N'Điện chuyển tiền',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('KC' ,N'Khác',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('BIENMAU' ,N'Biên mậu',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('CANTRU' ,N'Cấn trừ',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('CHEQUE' ,N'Séc',GETDATE(),GETDATE())
INSERT dbo.t_HaiQuan_PhuongThucThanhToan( ID ,GhiChu,DateCreated,DateModified) VALUES  ('KHONGTT' ,N'Không thanh toán',GETDATE(),GETDATE())

GO

IF OBJECT_ID(N'[dbo].[p_KDT_UpdateGUIDSTR]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_UpdateGUIDSTR]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]

IF OBJECT_ID(N'[dbo].[t_KDT_GC_PhuKien_TTHopDong]') IS NOT NULL
	DROP TABLE [dbo].[t_KDT_GC_PhuKien_TTHopDong]
GO

CREATE TABLE [dbo].[t_KDT_GC_PhuKien_TTHopDong]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
LoaiPhuKien_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_GC_LoaiPhuKien(ID),
[PTTT_ID] [nvarchar] (10) NULL,
[NguyenTe_ID] [char] (3)  NULL,
[NuocThue_ID] [char] (3)  NULL,
[MaDoanhNghiep] [varchar] (14)  NOT NULL,
[TenDoanhNghiep] [nvarchar] (80)  NULL,
[DiaChiDoanhNghiep] [nvarchar] (256) NULL,
[DonViDoiTac] [nvarchar] (100)  NULL,
[TenDonViDoiTac] [nvarchar] (80)  NULL,
[DiaChiDoiTac] [nvarchar] (256) NULL,
[TongTriGiaSP] [float] NULL,
[TongTriGiaTienCong] [float] NULL,
[GhiChu] [nvarchar] (2000) NULL
) ON [PRIMARY]
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKien_TTHopDong_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKien_TTHopDong_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKien_TTHopDong_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKien_TTHopDong_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKien_TTHopDong_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKien_TTHopDong_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectBy_LoaiPhuKien_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectBy_LoaiPhuKien_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKien_TTHopDong_DeleteBy_LoaiPhuKien_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_DeleteBy_LoaiPhuKien_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKien_TTHopDong_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_Insert]
	@LoaiPhuKien_ID bigint,
	@PTTT_ID nvarchar(10),
	@NguyenTe_ID char(3),
	@NuocThue_ID char(3),
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(80),
	@DiaChiDoanhNghiep nvarchar(256),
	@DonViDoiTac nvarchar(100),
	@TenDonViDoiTac nvarchar(80),
	@DiaChiDoiTac nvarchar(256),
	@TongTriGiaSP float,
	@TongTriGiaTienCong float,
	@GhiChu nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_PhuKien_TTHopDong]
(
	[LoaiPhuKien_ID],
	[PTTT_ID],
	[NguyenTe_ID],
	[NuocThue_ID],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[DiaChiDoanhNghiep],
	[DonViDoiTac],
	[TenDonViDoiTac],
	[DiaChiDoiTac],
	[TongTriGiaSP],
	[TongTriGiaTienCong],
	[GhiChu]
)
VALUES 
(
	@LoaiPhuKien_ID,
	@PTTT_ID,
	@NguyenTe_ID,
	@NuocThue_ID,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@DiaChiDoanhNghiep,
	@DonViDoiTac,
	@TenDonViDoiTac,
	@DiaChiDoiTac,
	@TongTriGiaSP,
	@TongTriGiaTienCong,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKien_TTHopDong_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_Update]
	@ID bigint,
	@LoaiPhuKien_ID bigint,
	@PTTT_ID nvarchar(10),
	@NguyenTe_ID char(3),
	@NuocThue_ID char(3),
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(80),
	@DiaChiDoanhNghiep nvarchar(256),
	@DonViDoiTac nvarchar(100),
	@TenDonViDoiTac nvarchar(80),
	@DiaChiDoiTac nvarchar(256),
	@TongTriGiaSP float,
	@TongTriGiaTienCong float,
	@GhiChu nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_GC_PhuKien_TTHopDong]
SET
	[LoaiPhuKien_ID] = @LoaiPhuKien_ID,
	[PTTT_ID] = @PTTT_ID,
	[NguyenTe_ID] = @NguyenTe_ID,
	[NuocThue_ID] = @NuocThue_ID,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[DiaChiDoanhNghiep] = @DiaChiDoanhNghiep,
	[DonViDoiTac] = @DonViDoiTac,
	[TenDonViDoiTac] = @TenDonViDoiTac,
	[DiaChiDoiTac] = @DiaChiDoiTac,
	[TongTriGiaSP] = @TongTriGiaSP,
	[TongTriGiaTienCong] = @TongTriGiaTienCong,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKien_TTHopDong_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_InsertUpdate]
	@ID bigint,
	@LoaiPhuKien_ID bigint,
	@PTTT_ID nvarchar(10),
	@NguyenTe_ID char(3),
	@NuocThue_ID char(3),
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(80),
	@DiaChiDoanhNghiep nvarchar(256),
	@DonViDoiTac nvarchar(100),
	@TenDonViDoiTac nvarchar(80),
	@DiaChiDoiTac nvarchar(256),
	@TongTriGiaSP float,
	@TongTriGiaTienCong float,
	@GhiChu nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_PhuKien_TTHopDong] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_PhuKien_TTHopDong] 
		SET
			[LoaiPhuKien_ID] = @LoaiPhuKien_ID,
			[PTTT_ID] = @PTTT_ID,
			[NguyenTe_ID] = @NguyenTe_ID,
			[NuocThue_ID] = @NuocThue_ID,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[DiaChiDoanhNghiep] = @DiaChiDoanhNghiep,
			[DonViDoiTac] = @DonViDoiTac,
			[TenDonViDoiTac] = @TenDonViDoiTac,
			[DiaChiDoiTac] = @DiaChiDoiTac,
			[TongTriGiaSP] = @TongTriGiaSP,
			[TongTriGiaTienCong] = @TongTriGiaTienCong,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_PhuKien_TTHopDong]
		(
			[LoaiPhuKien_ID],
			[PTTT_ID],
			[NguyenTe_ID],
			[NuocThue_ID],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[DiaChiDoanhNghiep],
			[DonViDoiTac],
			[TenDonViDoiTac],
			[DiaChiDoiTac],
			[TongTriGiaSP],
			[TongTriGiaTienCong],
			[GhiChu]
		)
		VALUES 
		(
			@LoaiPhuKien_ID,
			@PTTT_ID,
			@NguyenTe_ID,
			@NuocThue_ID,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@DiaChiDoanhNghiep,
			@DonViDoiTac,
			@TenDonViDoiTac,
			@DiaChiDoiTac,
			@TongTriGiaSP,
			@TongTriGiaTienCong,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKien_TTHopDong_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_PhuKien_TTHopDong]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKien_TTHopDong_DeleteBy_LoaiPhuKien_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_DeleteBy_LoaiPhuKien_ID]
	@LoaiPhuKien_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_PhuKien_TTHopDong]
WHERE
	[LoaiPhuKien_ID] = @LoaiPhuKien_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKien_TTHopDong_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_PhuKien_TTHopDong] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKien_TTHopDong_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiPhuKien_ID],
	[PTTT_ID],
	[NguyenTe_ID],
	[NuocThue_ID],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[DiaChiDoanhNghiep],
	[DonViDoiTac],
	[TenDonViDoiTac],
	[DiaChiDoiTac],
	[TongTriGiaSP],
	[TongTriGiaTienCong],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_PhuKien_TTHopDong]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectBy_LoaiPhuKien_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectBy_LoaiPhuKien_ID]
	@LoaiPhuKien_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiPhuKien_ID],
	[PTTT_ID],
	[NguyenTe_ID],
	[NuocThue_ID],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[DiaChiDoanhNghiep],
	[DonViDoiTac],
	[TenDonViDoiTac],
	[DiaChiDoiTac],
	[TongTriGiaSP],
	[TongTriGiaTienCong],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_PhuKien_TTHopDong]
WHERE
	[LoaiPhuKien_ID] = @LoaiPhuKien_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiPhuKien_ID],
	[PTTT_ID],
	[NguyenTe_ID],
	[NuocThue_ID],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[DiaChiDoanhNghiep],
	[DonViDoiTac],
	[TenDonViDoiTac],
	[DiaChiDoiTac],
	[TongTriGiaSP],
	[TongTriGiaTienCong],
	[GhiChu]
FROM [dbo].[t_KDT_GC_PhuKien_TTHopDong] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKien_TTHopDong_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiPhuKien_ID],
	[PTTT_ID],
	[NguyenTe_ID],
	[NuocThue_ID],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[DiaChiDoanhNghiep],
	[DonViDoiTac],
	[TenDonViDoiTac],
	[DiaChiDoiTac],
	[TongTriGiaSP],
	[TongTriGiaTienCong],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_PhuKien_TTHopDong]	

GO

GO
CREATE PROCEDURE [dbo].[p_KDT_UpdateGUIDSTR]
 @table varchar(max),  
 @guidstr nvarchar(max),  
 @where varchar(max)   
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
DECLARE @SQL nvarchar(3250)      
      
SET @SQL = ' UPDATE '+@table+' SET GUIDSTR =N'+@guidstr+'  WHERE ' + @where      
              
EXEC sp_executesql @SQL   
GO

------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]      
-- Database: ECS_GC      
-- Author: Dang Phuoc Duy    
-- Time created: Tuesday, October 20, 2015    
------------------------------------------------------------------------------------------------------------------------      
      
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]      
 @WhereCondition NVARCHAR(500),      
 @OrderByExpression NVARCHAR(250) = NULL      
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
DECLARE @SQL nvarchar(3250)      
      
SET @SQL = 'SELECT      
  tkmd.ID,      
 [SoTiepNhan],      
 [NgayTiepNhan],      
 [MaHaiQuan],      
 [SoToKhai],      
 [MaLoaiHinh],      
 [NgayDangKy],      
 [MaDoanhNghiep],      
 [TenDoanhNghiep],      
 [MaDaiLyTTHQ],      
 [TenDaiLyTTHQ],      
 [TenDonViDoiTac],      
 [ChiTietDonViDoiTac],      
 [SoGiayPhep],      
 [NgayGiayPhep],      
 [NgayHetHanGiayPhep],      
 [SoHopDong],      
 [NgayHopDong],      
 [NgayHetHanHopDong],      
 [SoHoaDonThuongMai],      
 [NgayHoaDonThuongMai],      
 [PTVT_ID],      
 [SoHieuPTVT],      
 [NgayDenPTVT],      
 [QuocTichPTVT_ID],      
 [LoaiVanDon],      
 [SoVanDon],      
 [NgayVanDon],      
 [NuocXK_ID],      
 [NuocNK_ID],      
 [DiaDiemXepHang],      
 [CuaKhau_ID],      
 [DKGH_ID],      
 [NguyenTe_ID],      
 [TyGiaTinhThue],      
 [TyGiaUSD],      
 [PTTT_ID],      
 [SoHang],      
 [SoLuongPLTK],      
 [TenChuHang],      
 [ChucVu],      
 [SoContainer20],      
 [SoContainer40],      
 [SoKien],      
 [TongTriGiaKhaiBao],      
 [TongTriGiaTinhThue],      
 [LoaiToKhaiGiaCong],      
 [LePhiHaiQuan],      
 [PhiBaoHiem],      
 [PhiVanChuyen],      
 [PhiXepDoHang],      
 [PhiKhac],      
 [CanBoDangKy],      
 [QuanLyMay],      
 [TrangThaiXuLy],      
 [LoaiHangHoa],      
 [GiayTo],      
 [PhanLuong],      
 [MaDonViUT],      
 [TenDonViUT],      
 [TrongLuongNet],      
 [SoTienKhoan],      
 [IDHopDong],      
 [MaMid],      
 [Ngay_THN_THX],      
 [TrangThaiPhanBo],      
 [GUIDSTR],      
 [DeXuatKhac],      
 [LyDoSua],      
 [ActionStatus],      
 [GuidReference],      
 [NamDK],      
 [HUONGDAN] ,    
 hmd.SoThuTuHang,    
 hmd.MaPhu,  
 hmd.MaHS,    
 hmd.TenHang,    
 hmd.NuocXX_ID,    
 hmd.SoLuong,    
 hmd.DonGiaKB,    
 hmd.TriGiaKB,    
 hmd.DonGiaTT,    
 hmd.TriGiaTT,    
 CASE WHEN tkmd.ID <>0 THEN (SELECT dvt.Ten FROM dbo.t_HaiQuan_DonViTinh dvt WHERE dvt.ID=hmd.DVT_ID) ELSE NULL END AS DVT    
 FROM [dbo].[t_KDT_ToKhaiMauDich] tkmd INNER JOIN dbo.t_KDT_HangMauDich hmd ON hmd.TKMD_ID = tkmd.ID  WHERE ' + @WhereCondition      
      
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0      
BEGIN      
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression      
END      
      
EXEC sp_executesql @SQL      

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '28.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('28.0',GETDATE(), N'CẬP NHẬT PHỤ KIỆN HỢP ĐỒNG VÀ UPDATE GUIDSTRING')
END