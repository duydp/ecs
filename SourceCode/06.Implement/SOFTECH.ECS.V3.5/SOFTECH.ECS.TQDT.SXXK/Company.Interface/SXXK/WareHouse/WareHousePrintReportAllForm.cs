﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.Interface.Report.WareHouse;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Preview;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHousePrintReportAllForm : Company.Interface.BaseForm
    {
        public ReportViewPhieuXuatKho PhieuXuatKho = new ReportViewPhieuXuatKho();
        public ReportViewPhieuXuatKho PhieuXuatKhoAll = new ReportViewPhieuXuatKho();
        public ReportViewPhieuNhapKho PhieuNhapKho = new ReportViewPhieuNhapKho();
        public ReportViewPhieuNhapKho PhieuNhapKhoAll = new ReportViewPhieuNhapKho();
        public List<T_KHOKETOAN_PHIEUXNKHO> PhieuXNKCollection;
        public WareHousePrintReportAllForm()
        {
            InitializeComponent();
        }

        private void WareHousePrintReportAllForm_Load(object sender, EventArgs e)
        {
            int i = 0;
            foreach (T_KHOKETOAN_PHIEUXNKHO item in PhieuXNKCollection)
            {
                    cbPage.Items.Add(string.Format("SỐ CHỨNG TỪ : {0} - NGÀY CHỨNG TỪ : {1}", item.SOCT, item.NGAYCT.ToString("dd/MM/yyyy")));               
            }
            cbPage.Items.Add("---------------TẤT CẢ CHỨNG TỪ----------------");
            if (cbPage.Items.Count > 0)
                cbPage.SelectedIndex = 0;
            this.ViewPhanBo();
        }
        private void ViewPhanBo()
        {
            if (PhieuXNKCollection.Count >=1)
            {
                foreach (T_KHOKETOAN_PHIEUXNKHO item in PhieuXNKCollection)
                {
                    if (item.LOAICHUNGTU == "N")
                    {
                        printControl1.PrintingSystem = this.PhieuNhapKho.PrintingSystem;
                        PhieuNhapKho.PhieuXNKho = PhieuXNKCollection[0];
                        PhieuNhapKho.BindReport();
                        this.PhieuNhapKho.CreateDocument();
                    }
                    else
                    {
                        printControl1.PrintingSystem = this.PhieuXuatKho.PrintingSystem;
                        PhieuXuatKho.PhieuXNKho = PhieuXNKCollection[0];
                        PhieuXuatKho.BindReport();
                        this.PhieuXuatKho.CreateDocument();
                    }
                }   
            }
        }

        private void cbPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbPage.SelectedIndex < this.cbPage.Items.Count - 1)
            {
                if (PhieuXNKCollection[this.cbPage.SelectedIndex].LOAICHUNGTU=="N")
                {
                    PhieuNhapKho = new ReportViewPhieuNhapKho();
                    PhieuNhapKho.PhieuXNKho = PhieuXNKCollection[this.cbPage.SelectedIndex];
                    PhieuNhapKho.BindReport();
                    printControl1.PrintingSystem = this.PhieuNhapKho.PrintingSystem;
                    this.PhieuNhapKho.CreateDocument();
                }
                else
                {
                    PhieuXuatKho = new ReportViewPhieuXuatKho();
                    PhieuNhapKho.PhieuXNKho = PhieuXNKCollection[this.cbPage.SelectedIndex];
                    PhieuNhapKho.BindReport();
                    printControl1.PrintingSystem = this.PhieuNhapKho.PrintingSystem;
                    this.PhieuNhapKho.CreateDocument();
                }
            }
            else
            {
                if (PhieuXNKCollection[0].LOAICHUNGTU =="N")
                {
                    PhieuNhapKhoAll.PhieuXNKho = PhieuXNKCollection[0];
                    this.PhieuNhapKhoAll.BindReport();
                    this.PhieuNhapKhoAll.CreateDocument();

                    int pages = this.PhieuNhapKhoAll.Pages.Count;
                    for (int i = 1; i < this.cbPage.Items.Count - 1; i++)
                    {
                        PhieuNhapKho = new ReportViewPhieuNhapKho();

                        this.PhieuNhapKho.PhieuXNKho = PhieuXNKCollection[i];
                        this.PhieuNhapKho.BindReport();
                        this.PhieuNhapKho.CreateDocument();
                        this.PhieuNhapKhoAll.Pages.AddRange(this.PhieuNhapKho.Pages);
                    }
                    pages = this.PhieuNhapKhoAll.Pages.Count;
                    printControl1.PrintingSystem = this.PhieuNhapKhoAll.PrintingSystem;
                }
                else
                {
                    PhieuXuatKhoAll.PhieuXNKho = PhieuXNKCollection[0];
                    this.PhieuXuatKhoAll.BindReport();
                    this.PhieuXuatKhoAll.CreateDocument();

                    int pages = this.PhieuXuatKhoAll.Pages.Count;
                    for (int i = 1; i < this.cbPage.Items.Count - 1; i++)
                    {
                        PhieuXuatKho = new ReportViewPhieuXuatKho();

                        this.PhieuXuatKho.PhieuXNKho = PhieuXNKCollection[i];
                        this.PhieuXuatKho.BindReport();
                        this.PhieuXuatKho.CreateDocument();
                        this.PhieuXuatKhoAll.Pages.AddRange(this.PhieuXuatKho.Pages);
                    }
                    pages = this.PhieuXuatKhoAll.Pages.Count;
                    printControl1.PrintingSystem = this.PhieuXuatKhoAll.PrintingSystem;
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }
    }
}
