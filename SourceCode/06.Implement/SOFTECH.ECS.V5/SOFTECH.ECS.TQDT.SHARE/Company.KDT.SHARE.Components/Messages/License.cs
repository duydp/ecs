﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class License : IssueBase
    {
        /// <summary>
        /// Mã kiểu
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        /// <summary>
        /// Mã kiểu
        /// </summary>
        [XmlElement("category")]
        public string Category { get; set; }

        /// <summary>
        /// Hạn
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }
        /// <summary>
        /// Ghi chú
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }

        /// <summary>
        /// Hàng trong giấy phép
        /// </summary>
        [XmlElement("GoodsItem")]
        public List<GoodsItem> GoodItems { get; set; }
        /// <summary>
        /// Số giấy phép an..35
        /// </summary>
        [XmlElement("numberLicense")]
        public string NumberLicense { get; set; }
        /// <summary>
        /// Ngày giấy phép
        /// </summary>
        [XmlElement("dateLicense")]
        public string DateLicense { get; set; }
        /// <summary>
        /// Ngày hết hạn giấy phép
        /// </summary>
        [XmlElement("expireDate")]
        public string ExpireDate { get; set; }
        /// <summary>
        /// Tổ chức cấp phép an..256
        /// </summary>
        [XmlElement("adminitrativeOrgan")]
        public string AdminitrativeOrgan { get; set; }
    }
}
