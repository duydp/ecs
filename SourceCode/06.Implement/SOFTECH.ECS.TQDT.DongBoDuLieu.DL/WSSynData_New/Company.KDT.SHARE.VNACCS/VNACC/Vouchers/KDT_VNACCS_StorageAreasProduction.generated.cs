﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_StorageAreasProduction : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public int TrangThaiXuLy { set; get; }
		public long SoTN { set; get; }
		public DateTime NgayTN { set; get; }
		public string MaHQ { set; get; }
		public string TenDoanhNghiep { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string DiaChiTruSoChinh { set; get; }
		public decimal LoaiDiaChiTruSoChinh { set; get; }
		public string NuocDauTu { set; get; }
		public string NganhNgheSanXuat { set; get; }
		public string TenDoanhNghiepTKTD { set; get; }
		public string MaDoanhNghiepTKTD { set; get; }
		public string LyDoChuyenDoi { set; get; }
		public string SoCMNDCT { set; get; }
		public DateTime NgayCapGiayPhepCT { set; get; }
		public string NoiCapGiayPhepCT { set; get; }
		public string NoiDangKyHKTTCT { set; get; }
		public string SoDienThoaiCT { set; get; }
		public string SoCMNDGD { set; get; }
		public DateTime NgayCapGiayPhepGD { set; get; }
		public string NoiCapGiayPhepGD { set; get; }
		public string NoiDangKyHKTTGD { set; get; }
		public string SoDienThoaiGD { set; get; }
		public decimal DaDuocCQHQKT { set; get; }
		public decimal BiXuPhatVeBuonLau { set; get; }
		public decimal BiXuPhatVeTronThue { set; get; }
		public decimal BiXuPhatVeKeToan { set; get; }
		public decimal ThoiGianSanXuat { set; get; }
		public decimal SoLuongSanPham { set; get; }
		public decimal BoPhanQuanLy { set; get; }
		public decimal SoLuongCongNhan { set; get; }
		public string TenCongTyMe { set; get; }
		public string MaCongTyMe { set; get; }
		public decimal SoLuongThanhVien { set; get; }
		public decimal SoLuongChiNhanh { set; get; }
		public decimal SoLuongThanhVienCTM { set; get; }
		public string GhiChuKhac { set; get; }
		public string GuidStr { set; get; }
		public DateTime NgayKetThucNamTC { set; get; }
		public int LoaiHinhDN { set; get; }
		public int LoaiSua { set; get; }
		public decimal SoLuongSoHuu { set; get; }
		public decimal SoLuongDiThue { set; get; }
		public decimal SoLuongKhac { set; get; }
		public decimal TongSoLuong { set; get; }

        public List<KDT_VNACCS_ContentInspection> ContentInspectionCollection = new List<KDT_VNACCS_ContentInspection>();
        public List<KDT_VNACCS_ManufactureFactory> ManufactureFactoryCollection = new List<KDT_VNACCS_ManufactureFactory>();
        public List<KDT_VNACCS_Career> CareerCollection = new List<KDT_VNACCS_Career>();
        //public List<KDT_VNACCS_Careery> CareeryCollection = new List<KDT_VNACCS_Careery>();
        public List<KDT_VNACCS_HoldingCompany> HoldingCompanyCollection = new List<KDT_VNACCS_HoldingCompany>();
        public List<KDT_VNACCS_AffiliatedMemberCompany> AffiliatedMemberCompanyCollection = new List<KDT_VNACCS_AffiliatedMemberCompany>();
        public List<KDT_VNACCS_MemberCompany> MemberCompanyCollection = new List<KDT_VNACCS_MemberCompany>();

        public List<KDT_VNACCS_StorageOfGood> StorageOfGoodCollection = new List<KDT_VNACCS_StorageOfGood>();
        public List<KDT_VNACCS_StorageAreasProduction_AttachedFile> AttachedFileGoodCollection = new List<KDT_VNACCS_StorageAreasProduction_AttachedFile>();


        public List<KDT_VNACCS_OutsourcingManufactureFactory> OutsourcingManufactureFactoryCollection = new List<KDT_VNACCS_OutsourcingManufactureFactory>();

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_StorageAreasProduction> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_StorageAreasProduction> collection = new List<KDT_VNACCS_StorageAreasProduction>();
			while (reader.Read())
			{
				KDT_VNACCS_StorageAreasProduction entity = new KDT_VNACCS_StorageAreasProduction();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTN"))) entity.SoTN = reader.GetInt64(reader.GetOrdinal("SoTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTN"))) entity.NgayTN = reader.GetDateTime(reader.GetOrdinal("NgayTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQ"))) entity.MaHQ = reader.GetString(reader.GetOrdinal("MaHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiTruSoChinh"))) entity.DiaChiTruSoChinh = reader.GetString(reader.GetOrdinal("DiaChiTruSoChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiDiaChiTruSoChinh"))) entity.LoaiDiaChiTruSoChinh = reader.GetDecimal(reader.GetOrdinal("LoaiDiaChiTruSoChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocDauTu"))) entity.NuocDauTu = reader.GetString(reader.GetOrdinal("NuocDauTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NganhNgheSanXuat"))) entity.NganhNgheSanXuat = reader.GetString(reader.GetOrdinal("NganhNgheSanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiepTKTD"))) entity.TenDoanhNghiepTKTD = reader.GetString(reader.GetOrdinal("TenDoanhNghiepTKTD"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiepTKTD"))) entity.MaDoanhNghiepTKTD = reader.GetString(reader.GetOrdinal("MaDoanhNghiepTKTD"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoChuyenDoi"))) entity.LyDoChuyenDoi = reader.GetString(reader.GetOrdinal("LyDoChuyenDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoCMNDCT"))) entity.SoCMNDCT = reader.GetString(reader.GetOrdinal("SoCMNDCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapGiayPhepCT"))) entity.NgayCapGiayPhepCT = reader.GetDateTime(reader.GetOrdinal("NgayCapGiayPhepCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiCapGiayPhepCT"))) entity.NoiCapGiayPhepCT = reader.GetString(reader.GetOrdinal("NoiCapGiayPhepCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDangKyHKTTCT"))) entity.NoiDangKyHKTTCT = reader.GetString(reader.GetOrdinal("NoiDangKyHKTTCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiCT"))) entity.SoDienThoaiCT = reader.GetString(reader.GetOrdinal("SoDienThoaiCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoCMNDGD"))) entity.SoCMNDGD = reader.GetString(reader.GetOrdinal("SoCMNDGD"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapGiayPhepGD"))) entity.NgayCapGiayPhepGD = reader.GetDateTime(reader.GetOrdinal("NgayCapGiayPhepGD"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiCapGiayPhepGD"))) entity.NoiCapGiayPhepGD = reader.GetString(reader.GetOrdinal("NoiCapGiayPhepGD"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDangKyHKTTGD"))) entity.NoiDangKyHKTTGD = reader.GetString(reader.GetOrdinal("NoiDangKyHKTTGD"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiGD"))) entity.SoDienThoaiGD = reader.GetString(reader.GetOrdinal("SoDienThoaiGD"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaDuocCQHQKT"))) entity.DaDuocCQHQKT = reader.GetDecimal(reader.GetOrdinal("DaDuocCQHQKT"));
				if (!reader.IsDBNull(reader.GetOrdinal("BiXuPhatVeBuonLau"))) entity.BiXuPhatVeBuonLau = reader.GetDecimal(reader.GetOrdinal("BiXuPhatVeBuonLau"));
				if (!reader.IsDBNull(reader.GetOrdinal("BiXuPhatVeTronThue"))) entity.BiXuPhatVeTronThue = reader.GetDecimal(reader.GetOrdinal("BiXuPhatVeTronThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("BiXuPhatVeKeToan"))) entity.BiXuPhatVeKeToan = reader.GetDecimal(reader.GetOrdinal("BiXuPhatVeKeToan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianSanXuat"))) entity.ThoiGianSanXuat = reader.GetDecimal(reader.GetOrdinal("ThoiGianSanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongSanPham"))) entity.SoLuongSanPham = reader.GetDecimal(reader.GetOrdinal("SoLuongSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("BoPhanQuanLy"))) entity.BoPhanQuanLy = reader.GetDecimal(reader.GetOrdinal("BoPhanQuanLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCongNhan"))) entity.SoLuongCongNhan = reader.GetDecimal(reader.GetOrdinal("SoLuongCongNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCongTyMe"))) entity.TenCongTyMe = reader.GetString(reader.GetOrdinal("TenCongTyMe"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCongTyMe"))) entity.MaCongTyMe = reader.GetString(reader.GetOrdinal("MaCongTyMe"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongThanhVien"))) entity.SoLuongThanhVien = reader.GetDecimal(reader.GetOrdinal("SoLuongThanhVien"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongChiNhanh"))) entity.SoLuongChiNhanh = reader.GetDecimal(reader.GetOrdinal("SoLuongChiNhanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongThanhVienCTM"))) entity.SoLuongThanhVienCTM = reader.GetDecimal(reader.GetOrdinal("SoLuongThanhVienCTM"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuKhac"))) entity.GhiChuKhac = reader.GetString(reader.GetOrdinal("GhiChuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThucNamTC"))) entity.NgayKetThucNamTC = reader.GetDateTime(reader.GetOrdinal("NgayKetThucNamTC"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHinhDN"))) entity.LoaiHinhDN = reader.GetInt32(reader.GetOrdinal("LoaiHinhDN"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiSua"))) entity.LoaiSua = reader.GetInt32(reader.GetOrdinal("LoaiSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongSoHuu"))) entity.SoLuongSoHuu = reader.GetDecimal(reader.GetOrdinal("SoLuongSoHuu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDiThue"))) entity.SoLuongDiThue = reader.GetDecimal(reader.GetOrdinal("SoLuongDiThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongKhac"))) entity.SoLuongKhac = reader.GetDecimal(reader.GetOrdinal("SoLuongKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoLuong"))) entity.TongSoLuong = reader.GetDecimal(reader.GetOrdinal("TongSoLuong"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_StorageAreasProduction> collection, long id)
        {
            foreach (KDT_VNACCS_StorageAreasProduction item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_StorageAreasProduction VALUES(@TrangThaiXuLy, @SoTN, @NgayTN, @MaHQ, @TenDoanhNghiep, @MaDoanhNghiep, @DiaChiTruSoChinh, @LoaiDiaChiTruSoChinh, @NuocDauTu, @NganhNgheSanXuat, @TenDoanhNghiepTKTD, @MaDoanhNghiepTKTD, @LyDoChuyenDoi, @SoCMNDCT, @NgayCapGiayPhepCT, @NoiCapGiayPhepCT, @NoiDangKyHKTTCT, @SoDienThoaiCT, @SoCMNDGD, @NgayCapGiayPhepGD, @NoiCapGiayPhepGD, @NoiDangKyHKTTGD, @SoDienThoaiGD, @DaDuocCQHQKT, @BiXuPhatVeBuonLau, @BiXuPhatVeTronThue, @BiXuPhatVeKeToan, @ThoiGianSanXuat, @SoLuongSanPham, @BoPhanQuanLy, @SoLuongCongNhan, @TenCongTyMe, @MaCongTyMe, @SoLuongThanhVien, @SoLuongChiNhanh, @SoLuongThanhVienCTM, @GhiChuKhac, @GuidStr, @NgayKetThucNamTC, @LoaiHinhDN, @LoaiSua, @SoLuongSoHuu, @SoLuongDiThue, @SoLuongKhac, @TongSoLuong)";
            string update = "UPDATE t_KDT_VNACCS_StorageAreasProduction SET TrangThaiXuLy = @TrangThaiXuLy, SoTN = @SoTN, NgayTN = @NgayTN, MaHQ = @MaHQ, TenDoanhNghiep = @TenDoanhNghiep, MaDoanhNghiep = @MaDoanhNghiep, DiaChiTruSoChinh = @DiaChiTruSoChinh, LoaiDiaChiTruSoChinh = @LoaiDiaChiTruSoChinh, NuocDauTu = @NuocDauTu, NganhNgheSanXuat = @NganhNgheSanXuat, TenDoanhNghiepTKTD = @TenDoanhNghiepTKTD, MaDoanhNghiepTKTD = @MaDoanhNghiepTKTD, LyDoChuyenDoi = @LyDoChuyenDoi, SoCMNDCT = @SoCMNDCT, NgayCapGiayPhepCT = @NgayCapGiayPhepCT, NoiCapGiayPhepCT = @NoiCapGiayPhepCT, NoiDangKyHKTTCT = @NoiDangKyHKTTCT, SoDienThoaiCT = @SoDienThoaiCT, SoCMNDGD = @SoCMNDGD, NgayCapGiayPhepGD = @NgayCapGiayPhepGD, NoiCapGiayPhepGD = @NoiCapGiayPhepGD, NoiDangKyHKTTGD = @NoiDangKyHKTTGD, SoDienThoaiGD = @SoDienThoaiGD, DaDuocCQHQKT = @DaDuocCQHQKT, BiXuPhatVeBuonLau = @BiXuPhatVeBuonLau, BiXuPhatVeTronThue = @BiXuPhatVeTronThue, BiXuPhatVeKeToan = @BiXuPhatVeKeToan, ThoiGianSanXuat = @ThoiGianSanXuat, SoLuongSanPham = @SoLuongSanPham, BoPhanQuanLy = @BoPhanQuanLy, SoLuongCongNhan = @SoLuongCongNhan, TenCongTyMe = @TenCongTyMe, MaCongTyMe = @MaCongTyMe, SoLuongThanhVien = @SoLuongThanhVien, SoLuongChiNhanh = @SoLuongChiNhanh, SoLuongThanhVienCTM = @SoLuongThanhVienCTM, GhiChuKhac = @GhiChuKhac, GuidStr = @GuidStr, NgayKetThucNamTC = @NgayKetThucNamTC, LoaiHinhDN = @LoaiHinhDN, LoaiSua = @LoaiSua, SoLuongSoHuu = @SoLuongSoHuu, SoLuongDiThue = @SoLuongDiThue, SoLuongKhac = @SoLuongKhac, TongSoLuong = @TongSoLuong WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_StorageAreasProduction WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiTruSoChinh", SqlDbType.NVarChar, "DiaChiTruSoChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiDiaChiTruSoChinh", SqlDbType.Decimal, "LoaiDiaChiTruSoChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocDauTu", SqlDbType.NVarChar, "NuocDauTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NganhNgheSanXuat", SqlDbType.NVarChar, "NganhNgheSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiepTKTD", SqlDbType.NVarChar, "TenDoanhNghiepTKTD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiepTKTD", SqlDbType.NVarChar, "MaDoanhNghiepTKTD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoChuyenDoi", SqlDbType.NVarChar, "LyDoChuyenDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCMNDCT", SqlDbType.NVarChar, "SoCMNDCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapGiayPhepCT", SqlDbType.DateTime, "NgayCapGiayPhepCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiCapGiayPhepCT", SqlDbType.NVarChar, "NoiCapGiayPhepCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDangKyHKTTCT", SqlDbType.NVarChar, "NoiDangKyHKTTCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiCT", SqlDbType.NVarChar, "SoDienThoaiCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCMNDGD", SqlDbType.NVarChar, "SoCMNDGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapGiayPhepGD", SqlDbType.DateTime, "NgayCapGiayPhepGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiCapGiayPhepGD", SqlDbType.NVarChar, "NoiCapGiayPhepGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDangKyHKTTGD", SqlDbType.NVarChar, "NoiDangKyHKTTGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiGD", SqlDbType.NVarChar, "SoDienThoaiGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaDuocCQHQKT", SqlDbType.Decimal, "DaDuocCQHQKT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BiXuPhatVeBuonLau", SqlDbType.Decimal, "BiXuPhatVeBuonLau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BiXuPhatVeTronThue", SqlDbType.Decimal, "BiXuPhatVeTronThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BiXuPhatVeKeToan", SqlDbType.Decimal, "BiXuPhatVeKeToan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianSanXuat", SqlDbType.Decimal, "ThoiGianSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongSanPham", SqlDbType.Decimal, "SoLuongSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BoPhanQuanLy", SqlDbType.Decimal, "BoPhanQuanLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCongNhan", SqlDbType.Decimal, "SoLuongCongNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCongTyMe", SqlDbType.NVarChar, "TenCongTyMe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCongTyMe", SqlDbType.NVarChar, "MaCongTyMe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThanhVien", SqlDbType.Decimal, "SoLuongThanhVien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongChiNhanh", SqlDbType.Decimal, "SoLuongChiNhanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThanhVienCTM", SqlDbType.Decimal, "SoLuongThanhVienCTM", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetThucNamTC", SqlDbType.DateTime, "NgayKetThucNamTC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHinhDN", SqlDbType.Int, "LoaiHinhDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiSua", SqlDbType.Int, "LoaiSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongSoHuu", SqlDbType.Decimal, "SoLuongSoHuu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDiThue", SqlDbType.Decimal, "SoLuongDiThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongKhac", SqlDbType.Decimal, "SoLuongKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoLuong", SqlDbType.Decimal, "TongSoLuong", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiTruSoChinh", SqlDbType.NVarChar, "DiaChiTruSoChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiDiaChiTruSoChinh", SqlDbType.Decimal, "LoaiDiaChiTruSoChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocDauTu", SqlDbType.NVarChar, "NuocDauTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NganhNgheSanXuat", SqlDbType.NVarChar, "NganhNgheSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiepTKTD", SqlDbType.NVarChar, "TenDoanhNghiepTKTD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiepTKTD", SqlDbType.NVarChar, "MaDoanhNghiepTKTD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoChuyenDoi", SqlDbType.NVarChar, "LyDoChuyenDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCMNDCT", SqlDbType.NVarChar, "SoCMNDCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapGiayPhepCT", SqlDbType.DateTime, "NgayCapGiayPhepCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiCapGiayPhepCT", SqlDbType.NVarChar, "NoiCapGiayPhepCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDangKyHKTTCT", SqlDbType.NVarChar, "NoiDangKyHKTTCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiCT", SqlDbType.NVarChar, "SoDienThoaiCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCMNDGD", SqlDbType.NVarChar, "SoCMNDGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapGiayPhepGD", SqlDbType.DateTime, "NgayCapGiayPhepGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiCapGiayPhepGD", SqlDbType.NVarChar, "NoiCapGiayPhepGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDangKyHKTTGD", SqlDbType.NVarChar, "NoiDangKyHKTTGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiGD", SqlDbType.NVarChar, "SoDienThoaiGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaDuocCQHQKT", SqlDbType.Decimal, "DaDuocCQHQKT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BiXuPhatVeBuonLau", SqlDbType.Decimal, "BiXuPhatVeBuonLau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BiXuPhatVeTronThue", SqlDbType.Decimal, "BiXuPhatVeTronThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BiXuPhatVeKeToan", SqlDbType.Decimal, "BiXuPhatVeKeToan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianSanXuat", SqlDbType.Decimal, "ThoiGianSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongSanPham", SqlDbType.Decimal, "SoLuongSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BoPhanQuanLy", SqlDbType.Decimal, "BoPhanQuanLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCongNhan", SqlDbType.Decimal, "SoLuongCongNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCongTyMe", SqlDbType.NVarChar, "TenCongTyMe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCongTyMe", SqlDbType.NVarChar, "MaCongTyMe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThanhVien", SqlDbType.Decimal, "SoLuongThanhVien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongChiNhanh", SqlDbType.Decimal, "SoLuongChiNhanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThanhVienCTM", SqlDbType.Decimal, "SoLuongThanhVienCTM", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetThucNamTC", SqlDbType.DateTime, "NgayKetThucNamTC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHinhDN", SqlDbType.Int, "LoaiHinhDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiSua", SqlDbType.Int, "LoaiSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongSoHuu", SqlDbType.Decimal, "SoLuongSoHuu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDiThue", SqlDbType.Decimal, "SoLuongDiThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongKhac", SqlDbType.Decimal, "SoLuongKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoLuong", SqlDbType.Decimal, "TongSoLuong", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_StorageAreasProduction VALUES(@TrangThaiXuLy, @SoTN, @NgayTN, @MaHQ, @TenDoanhNghiep, @MaDoanhNghiep, @DiaChiTruSoChinh, @LoaiDiaChiTruSoChinh, @NuocDauTu, @NganhNgheSanXuat, @TenDoanhNghiepTKTD, @MaDoanhNghiepTKTD, @LyDoChuyenDoi, @SoCMNDCT, @NgayCapGiayPhepCT, @NoiCapGiayPhepCT, @NoiDangKyHKTTCT, @SoDienThoaiCT, @SoCMNDGD, @NgayCapGiayPhepGD, @NoiCapGiayPhepGD, @NoiDangKyHKTTGD, @SoDienThoaiGD, @DaDuocCQHQKT, @BiXuPhatVeBuonLau, @BiXuPhatVeTronThue, @BiXuPhatVeKeToan, @ThoiGianSanXuat, @SoLuongSanPham, @BoPhanQuanLy, @SoLuongCongNhan, @TenCongTyMe, @MaCongTyMe, @SoLuongThanhVien, @SoLuongChiNhanh, @SoLuongThanhVienCTM, @GhiChuKhac, @GuidStr, @NgayKetThucNamTC, @LoaiHinhDN, @LoaiSua, @SoLuongSoHuu, @SoLuongDiThue, @SoLuongKhac, @TongSoLuong)";
            string update = "UPDATE t_KDT_VNACCS_StorageAreasProduction SET TrangThaiXuLy = @TrangThaiXuLy, SoTN = @SoTN, NgayTN = @NgayTN, MaHQ = @MaHQ, TenDoanhNghiep = @TenDoanhNghiep, MaDoanhNghiep = @MaDoanhNghiep, DiaChiTruSoChinh = @DiaChiTruSoChinh, LoaiDiaChiTruSoChinh = @LoaiDiaChiTruSoChinh, NuocDauTu = @NuocDauTu, NganhNgheSanXuat = @NganhNgheSanXuat, TenDoanhNghiepTKTD = @TenDoanhNghiepTKTD, MaDoanhNghiepTKTD = @MaDoanhNghiepTKTD, LyDoChuyenDoi = @LyDoChuyenDoi, SoCMNDCT = @SoCMNDCT, NgayCapGiayPhepCT = @NgayCapGiayPhepCT, NoiCapGiayPhepCT = @NoiCapGiayPhepCT, NoiDangKyHKTTCT = @NoiDangKyHKTTCT, SoDienThoaiCT = @SoDienThoaiCT, SoCMNDGD = @SoCMNDGD, NgayCapGiayPhepGD = @NgayCapGiayPhepGD, NoiCapGiayPhepGD = @NoiCapGiayPhepGD, NoiDangKyHKTTGD = @NoiDangKyHKTTGD, SoDienThoaiGD = @SoDienThoaiGD, DaDuocCQHQKT = @DaDuocCQHQKT, BiXuPhatVeBuonLau = @BiXuPhatVeBuonLau, BiXuPhatVeTronThue = @BiXuPhatVeTronThue, BiXuPhatVeKeToan = @BiXuPhatVeKeToan, ThoiGianSanXuat = @ThoiGianSanXuat, SoLuongSanPham = @SoLuongSanPham, BoPhanQuanLy = @BoPhanQuanLy, SoLuongCongNhan = @SoLuongCongNhan, TenCongTyMe = @TenCongTyMe, MaCongTyMe = @MaCongTyMe, SoLuongThanhVien = @SoLuongThanhVien, SoLuongChiNhanh = @SoLuongChiNhanh, SoLuongThanhVienCTM = @SoLuongThanhVienCTM, GhiChuKhac = @GhiChuKhac, GuidStr = @GuidStr, NgayKetThucNamTC = @NgayKetThucNamTC, LoaiHinhDN = @LoaiHinhDN, LoaiSua = @LoaiSua, SoLuongSoHuu = @SoLuongSoHuu, SoLuongDiThue = @SoLuongDiThue, SoLuongKhac = @SoLuongKhac, TongSoLuong = @TongSoLuong WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_StorageAreasProduction WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiTruSoChinh", SqlDbType.NVarChar, "DiaChiTruSoChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiDiaChiTruSoChinh", SqlDbType.Decimal, "LoaiDiaChiTruSoChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocDauTu", SqlDbType.NVarChar, "NuocDauTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NganhNgheSanXuat", SqlDbType.NVarChar, "NganhNgheSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiepTKTD", SqlDbType.NVarChar, "TenDoanhNghiepTKTD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiepTKTD", SqlDbType.NVarChar, "MaDoanhNghiepTKTD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoChuyenDoi", SqlDbType.NVarChar, "LyDoChuyenDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCMNDCT", SqlDbType.NVarChar, "SoCMNDCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapGiayPhepCT", SqlDbType.DateTime, "NgayCapGiayPhepCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiCapGiayPhepCT", SqlDbType.NVarChar, "NoiCapGiayPhepCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDangKyHKTTCT", SqlDbType.NVarChar, "NoiDangKyHKTTCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiCT", SqlDbType.NVarChar, "SoDienThoaiCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCMNDGD", SqlDbType.NVarChar, "SoCMNDGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapGiayPhepGD", SqlDbType.DateTime, "NgayCapGiayPhepGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiCapGiayPhepGD", SqlDbType.NVarChar, "NoiCapGiayPhepGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDangKyHKTTGD", SqlDbType.NVarChar, "NoiDangKyHKTTGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiGD", SqlDbType.NVarChar, "SoDienThoaiGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaDuocCQHQKT", SqlDbType.Decimal, "DaDuocCQHQKT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BiXuPhatVeBuonLau", SqlDbType.Decimal, "BiXuPhatVeBuonLau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BiXuPhatVeTronThue", SqlDbType.Decimal, "BiXuPhatVeTronThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BiXuPhatVeKeToan", SqlDbType.Decimal, "BiXuPhatVeKeToan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianSanXuat", SqlDbType.Decimal, "ThoiGianSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongSanPham", SqlDbType.Decimal, "SoLuongSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BoPhanQuanLy", SqlDbType.Decimal, "BoPhanQuanLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCongNhan", SqlDbType.Decimal, "SoLuongCongNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCongTyMe", SqlDbType.NVarChar, "TenCongTyMe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCongTyMe", SqlDbType.NVarChar, "MaCongTyMe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThanhVien", SqlDbType.Decimal, "SoLuongThanhVien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongChiNhanh", SqlDbType.Decimal, "SoLuongChiNhanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThanhVienCTM", SqlDbType.Decimal, "SoLuongThanhVienCTM", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetThucNamTC", SqlDbType.DateTime, "NgayKetThucNamTC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHinhDN", SqlDbType.Int, "LoaiHinhDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiSua", SqlDbType.Int, "LoaiSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongSoHuu", SqlDbType.Decimal, "SoLuongSoHuu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDiThue", SqlDbType.Decimal, "SoLuongDiThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongKhac", SqlDbType.Decimal, "SoLuongKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoLuong", SqlDbType.Decimal, "TongSoLuong", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiTruSoChinh", SqlDbType.NVarChar, "DiaChiTruSoChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiDiaChiTruSoChinh", SqlDbType.Decimal, "LoaiDiaChiTruSoChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocDauTu", SqlDbType.NVarChar, "NuocDauTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NganhNgheSanXuat", SqlDbType.NVarChar, "NganhNgheSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiepTKTD", SqlDbType.NVarChar, "TenDoanhNghiepTKTD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiepTKTD", SqlDbType.NVarChar, "MaDoanhNghiepTKTD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoChuyenDoi", SqlDbType.NVarChar, "LyDoChuyenDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCMNDCT", SqlDbType.NVarChar, "SoCMNDCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapGiayPhepCT", SqlDbType.DateTime, "NgayCapGiayPhepCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiCapGiayPhepCT", SqlDbType.NVarChar, "NoiCapGiayPhepCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDangKyHKTTCT", SqlDbType.NVarChar, "NoiDangKyHKTTCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiCT", SqlDbType.NVarChar, "SoDienThoaiCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCMNDGD", SqlDbType.NVarChar, "SoCMNDGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapGiayPhepGD", SqlDbType.DateTime, "NgayCapGiayPhepGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiCapGiayPhepGD", SqlDbType.NVarChar, "NoiCapGiayPhepGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDangKyHKTTGD", SqlDbType.NVarChar, "NoiDangKyHKTTGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiGD", SqlDbType.NVarChar, "SoDienThoaiGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaDuocCQHQKT", SqlDbType.Decimal, "DaDuocCQHQKT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BiXuPhatVeBuonLau", SqlDbType.Decimal, "BiXuPhatVeBuonLau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BiXuPhatVeTronThue", SqlDbType.Decimal, "BiXuPhatVeTronThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BiXuPhatVeKeToan", SqlDbType.Decimal, "BiXuPhatVeKeToan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianSanXuat", SqlDbType.Decimal, "ThoiGianSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongSanPham", SqlDbType.Decimal, "SoLuongSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BoPhanQuanLy", SqlDbType.Decimal, "BoPhanQuanLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCongNhan", SqlDbType.Decimal, "SoLuongCongNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCongTyMe", SqlDbType.NVarChar, "TenCongTyMe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCongTyMe", SqlDbType.NVarChar, "MaCongTyMe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThanhVien", SqlDbType.Decimal, "SoLuongThanhVien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongChiNhanh", SqlDbType.Decimal, "SoLuongChiNhanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThanhVienCTM", SqlDbType.Decimal, "SoLuongThanhVienCTM", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetThucNamTC", SqlDbType.DateTime, "NgayKetThucNamTC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHinhDN", SqlDbType.Int, "LoaiHinhDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiSua", SqlDbType.Int, "LoaiSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongSoHuu", SqlDbType.Decimal, "SoLuongSoHuu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDiThue", SqlDbType.Decimal, "SoLuongDiThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongKhac", SqlDbType.Decimal, "SoLuongKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoLuong", SqlDbType.Decimal, "TongSoLuong", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_StorageAreasProduction Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_StorageAreasProduction> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_StorageAreasProduction> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_StorageAreasProduction> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_StorageAreasProduction(int trangThaiXuLy, long soTN, DateTime ngayTN, string maHQ, string tenDoanhNghiep, string maDoanhNghiep, string diaChiTruSoChinh, decimal loaiDiaChiTruSoChinh, string nuocDauTu, string nganhNgheSanXuat, string tenDoanhNghiepTKTD, string maDoanhNghiepTKTD, string lyDoChuyenDoi, string soCMNDCT, DateTime ngayCapGiayPhepCT, string noiCapGiayPhepCT, string noiDangKyHKTTCT, string soDienThoaiCT, string soCMNDGD, DateTime ngayCapGiayPhepGD, string noiCapGiayPhepGD, string noiDangKyHKTTGD, string soDienThoaiGD, decimal daDuocCQHQKT, decimal biXuPhatVeBuonLau, decimal biXuPhatVeTronThue, decimal biXuPhatVeKeToan, decimal thoiGianSanXuat, decimal soLuongSanPham, decimal boPhanQuanLy, decimal soLuongCongNhan, string tenCongTyMe, string maCongTyMe, decimal soLuongThanhVien, decimal soLuongChiNhanh, decimal soLuongThanhVienCTM, string ghiChuKhac, string guidStr, DateTime ngayKetThucNamTC, int loaiHinhDN, int loaiSua, decimal soLuongSoHuu, decimal soLuongDiThue, decimal soLuongKhac, decimal tongSoLuong)
		{
			KDT_VNACCS_StorageAreasProduction entity = new KDT_VNACCS_StorageAreasProduction();	
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.MaHQ = maHQ;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.DiaChiTruSoChinh = diaChiTruSoChinh;
			entity.LoaiDiaChiTruSoChinh = loaiDiaChiTruSoChinh;
			entity.NuocDauTu = nuocDauTu;
			entity.NganhNgheSanXuat = nganhNgheSanXuat;
			entity.TenDoanhNghiepTKTD = tenDoanhNghiepTKTD;
			entity.MaDoanhNghiepTKTD = maDoanhNghiepTKTD;
			entity.LyDoChuyenDoi = lyDoChuyenDoi;
			entity.SoCMNDCT = soCMNDCT;
			entity.NgayCapGiayPhepCT = ngayCapGiayPhepCT;
			entity.NoiCapGiayPhepCT = noiCapGiayPhepCT;
			entity.NoiDangKyHKTTCT = noiDangKyHKTTCT;
			entity.SoDienThoaiCT = soDienThoaiCT;
			entity.SoCMNDGD = soCMNDGD;
			entity.NgayCapGiayPhepGD = ngayCapGiayPhepGD;
			entity.NoiCapGiayPhepGD = noiCapGiayPhepGD;
			entity.NoiDangKyHKTTGD = noiDangKyHKTTGD;
			entity.SoDienThoaiGD = soDienThoaiGD;
			entity.DaDuocCQHQKT = daDuocCQHQKT;
			entity.BiXuPhatVeBuonLau = biXuPhatVeBuonLau;
			entity.BiXuPhatVeTronThue = biXuPhatVeTronThue;
			entity.BiXuPhatVeKeToan = biXuPhatVeKeToan;
			entity.ThoiGianSanXuat = thoiGianSanXuat;
			entity.SoLuongSanPham = soLuongSanPham;
			entity.BoPhanQuanLy = boPhanQuanLy;
			entity.SoLuongCongNhan = soLuongCongNhan;
			entity.TenCongTyMe = tenCongTyMe;
			entity.MaCongTyMe = maCongTyMe;
			entity.SoLuongThanhVien = soLuongThanhVien;
			entity.SoLuongChiNhanh = soLuongChiNhanh;
			entity.SoLuongThanhVienCTM = soLuongThanhVienCTM;
			entity.GhiChuKhac = ghiChuKhac;
			entity.GuidStr = guidStr;
			entity.NgayKetThucNamTC = ngayKetThucNamTC;
			entity.LoaiHinhDN = loaiHinhDN;
			entity.LoaiSua = loaiSua;
			entity.SoLuongSoHuu = soLuongSoHuu;
			entity.SoLuongDiThue = soLuongDiThue;
			entity.SoLuongKhac = soLuongKhac;
			entity.TongSoLuong = tongSoLuong;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@DiaChiTruSoChinh", SqlDbType.NVarChar, DiaChiTruSoChinh);
			db.AddInParameter(dbCommand, "@LoaiDiaChiTruSoChinh", SqlDbType.Decimal, LoaiDiaChiTruSoChinh);
			db.AddInParameter(dbCommand, "@NuocDauTu", SqlDbType.NVarChar, NuocDauTu);
			db.AddInParameter(dbCommand, "@NganhNgheSanXuat", SqlDbType.NVarChar, NganhNgheSanXuat);
			db.AddInParameter(dbCommand, "@TenDoanhNghiepTKTD", SqlDbType.NVarChar, TenDoanhNghiepTKTD);
			db.AddInParameter(dbCommand, "@MaDoanhNghiepTKTD", SqlDbType.NVarChar, MaDoanhNghiepTKTD);
			db.AddInParameter(dbCommand, "@LyDoChuyenDoi", SqlDbType.NVarChar, LyDoChuyenDoi);
			db.AddInParameter(dbCommand, "@SoCMNDCT", SqlDbType.NVarChar, SoCMNDCT);
			db.AddInParameter(dbCommand, "@NgayCapGiayPhepCT", SqlDbType.DateTime, NgayCapGiayPhepCT.Year <= 1753 ? DBNull.Value : (object) NgayCapGiayPhepCT);
			db.AddInParameter(dbCommand, "@NoiCapGiayPhepCT", SqlDbType.NVarChar, NoiCapGiayPhepCT);
			db.AddInParameter(dbCommand, "@NoiDangKyHKTTCT", SqlDbType.NVarChar, NoiDangKyHKTTCT);
			db.AddInParameter(dbCommand, "@SoDienThoaiCT", SqlDbType.NVarChar, SoDienThoaiCT);
			db.AddInParameter(dbCommand, "@SoCMNDGD", SqlDbType.NVarChar, SoCMNDGD);
			db.AddInParameter(dbCommand, "@NgayCapGiayPhepGD", SqlDbType.DateTime, NgayCapGiayPhepGD.Year <= 1753 ? DBNull.Value : (object) NgayCapGiayPhepGD);
			db.AddInParameter(dbCommand, "@NoiCapGiayPhepGD", SqlDbType.NVarChar, NoiCapGiayPhepGD);
			db.AddInParameter(dbCommand, "@NoiDangKyHKTTGD", SqlDbType.NVarChar, NoiDangKyHKTTGD);
			db.AddInParameter(dbCommand, "@SoDienThoaiGD", SqlDbType.NVarChar, SoDienThoaiGD);
			db.AddInParameter(dbCommand, "@DaDuocCQHQKT", SqlDbType.Decimal, DaDuocCQHQKT);
			db.AddInParameter(dbCommand, "@BiXuPhatVeBuonLau", SqlDbType.Decimal, BiXuPhatVeBuonLau);
			db.AddInParameter(dbCommand, "@BiXuPhatVeTronThue", SqlDbType.Decimal, BiXuPhatVeTronThue);
			db.AddInParameter(dbCommand, "@BiXuPhatVeKeToan", SqlDbType.Decimal, BiXuPhatVeKeToan);
			db.AddInParameter(dbCommand, "@ThoiGianSanXuat", SqlDbType.Decimal, ThoiGianSanXuat);
			db.AddInParameter(dbCommand, "@SoLuongSanPham", SqlDbType.Decimal, SoLuongSanPham);
			db.AddInParameter(dbCommand, "@BoPhanQuanLy", SqlDbType.Decimal, BoPhanQuanLy);
			db.AddInParameter(dbCommand, "@SoLuongCongNhan", SqlDbType.Decimal, SoLuongCongNhan);
			db.AddInParameter(dbCommand, "@TenCongTyMe", SqlDbType.NVarChar, TenCongTyMe);
			db.AddInParameter(dbCommand, "@MaCongTyMe", SqlDbType.NVarChar, MaCongTyMe);
			db.AddInParameter(dbCommand, "@SoLuongThanhVien", SqlDbType.Decimal, SoLuongThanhVien);
			db.AddInParameter(dbCommand, "@SoLuongChiNhanh", SqlDbType.Decimal, SoLuongChiNhanh);
			db.AddInParameter(dbCommand, "@SoLuongThanhVienCTM", SqlDbType.Decimal, SoLuongThanhVienCTM);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@NgayKetThucNamTC", SqlDbType.DateTime, NgayKetThucNamTC.Year <= 1753 ? DBNull.Value : (object) NgayKetThucNamTC);
			db.AddInParameter(dbCommand, "@LoaiHinhDN", SqlDbType.Int, LoaiHinhDN);
			db.AddInParameter(dbCommand, "@LoaiSua", SqlDbType.Int, LoaiSua);
			db.AddInParameter(dbCommand, "@SoLuongSoHuu", SqlDbType.Decimal, SoLuongSoHuu);
			db.AddInParameter(dbCommand, "@SoLuongDiThue", SqlDbType.Decimal, SoLuongDiThue);
			db.AddInParameter(dbCommand, "@SoLuongKhac", SqlDbType.Decimal, SoLuongKhac);
			db.AddInParameter(dbCommand, "@TongSoLuong", SqlDbType.Decimal, TongSoLuong);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_StorageAreasProduction> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_StorageAreasProduction item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_StorageAreasProduction(long id, int trangThaiXuLy, long soTN, DateTime ngayTN, string maHQ, string tenDoanhNghiep, string maDoanhNghiep, string diaChiTruSoChinh, decimal loaiDiaChiTruSoChinh, string nuocDauTu, string nganhNgheSanXuat, string tenDoanhNghiepTKTD, string maDoanhNghiepTKTD, string lyDoChuyenDoi, string soCMNDCT, DateTime ngayCapGiayPhepCT, string noiCapGiayPhepCT, string noiDangKyHKTTCT, string soDienThoaiCT, string soCMNDGD, DateTime ngayCapGiayPhepGD, string noiCapGiayPhepGD, string noiDangKyHKTTGD, string soDienThoaiGD, decimal daDuocCQHQKT, decimal biXuPhatVeBuonLau, decimal biXuPhatVeTronThue, decimal biXuPhatVeKeToan, decimal thoiGianSanXuat, decimal soLuongSanPham, decimal boPhanQuanLy, decimal soLuongCongNhan, string tenCongTyMe, string maCongTyMe, decimal soLuongThanhVien, decimal soLuongChiNhanh, decimal soLuongThanhVienCTM, string ghiChuKhac, string guidStr, DateTime ngayKetThucNamTC, int loaiHinhDN, int loaiSua, decimal soLuongSoHuu, decimal soLuongDiThue, decimal soLuongKhac, decimal tongSoLuong)
		{
			KDT_VNACCS_StorageAreasProduction entity = new KDT_VNACCS_StorageAreasProduction();			
			entity.ID = id;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.MaHQ = maHQ;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.DiaChiTruSoChinh = diaChiTruSoChinh;
			entity.LoaiDiaChiTruSoChinh = loaiDiaChiTruSoChinh;
			entity.NuocDauTu = nuocDauTu;
			entity.NganhNgheSanXuat = nganhNgheSanXuat;
			entity.TenDoanhNghiepTKTD = tenDoanhNghiepTKTD;
			entity.MaDoanhNghiepTKTD = maDoanhNghiepTKTD;
			entity.LyDoChuyenDoi = lyDoChuyenDoi;
			entity.SoCMNDCT = soCMNDCT;
			entity.NgayCapGiayPhepCT = ngayCapGiayPhepCT;
			entity.NoiCapGiayPhepCT = noiCapGiayPhepCT;
			entity.NoiDangKyHKTTCT = noiDangKyHKTTCT;
			entity.SoDienThoaiCT = soDienThoaiCT;
			entity.SoCMNDGD = soCMNDGD;
			entity.NgayCapGiayPhepGD = ngayCapGiayPhepGD;
			entity.NoiCapGiayPhepGD = noiCapGiayPhepGD;
			entity.NoiDangKyHKTTGD = noiDangKyHKTTGD;
			entity.SoDienThoaiGD = soDienThoaiGD;
			entity.DaDuocCQHQKT = daDuocCQHQKT;
			entity.BiXuPhatVeBuonLau = biXuPhatVeBuonLau;
			entity.BiXuPhatVeTronThue = biXuPhatVeTronThue;
			entity.BiXuPhatVeKeToan = biXuPhatVeKeToan;
			entity.ThoiGianSanXuat = thoiGianSanXuat;
			entity.SoLuongSanPham = soLuongSanPham;
			entity.BoPhanQuanLy = boPhanQuanLy;
			entity.SoLuongCongNhan = soLuongCongNhan;
			entity.TenCongTyMe = tenCongTyMe;
			entity.MaCongTyMe = maCongTyMe;
			entity.SoLuongThanhVien = soLuongThanhVien;
			entity.SoLuongChiNhanh = soLuongChiNhanh;
			entity.SoLuongThanhVienCTM = soLuongThanhVienCTM;
			entity.GhiChuKhac = ghiChuKhac;
			entity.GuidStr = guidStr;
			entity.NgayKetThucNamTC = ngayKetThucNamTC;
			entity.LoaiHinhDN = loaiHinhDN;
			entity.LoaiSua = loaiSua;
			entity.SoLuongSoHuu = soLuongSoHuu;
			entity.SoLuongDiThue = soLuongDiThue;
			entity.SoLuongKhac = soLuongKhac;
			entity.TongSoLuong = tongSoLuong;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_StorageAreasProduction_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@DiaChiTruSoChinh", SqlDbType.NVarChar, DiaChiTruSoChinh);
			db.AddInParameter(dbCommand, "@LoaiDiaChiTruSoChinh", SqlDbType.Decimal, LoaiDiaChiTruSoChinh);
			db.AddInParameter(dbCommand, "@NuocDauTu", SqlDbType.NVarChar, NuocDauTu);
			db.AddInParameter(dbCommand, "@NganhNgheSanXuat", SqlDbType.NVarChar, NganhNgheSanXuat);
			db.AddInParameter(dbCommand, "@TenDoanhNghiepTKTD", SqlDbType.NVarChar, TenDoanhNghiepTKTD);
			db.AddInParameter(dbCommand, "@MaDoanhNghiepTKTD", SqlDbType.NVarChar, MaDoanhNghiepTKTD);
			db.AddInParameter(dbCommand, "@LyDoChuyenDoi", SqlDbType.NVarChar, LyDoChuyenDoi);
			db.AddInParameter(dbCommand, "@SoCMNDCT", SqlDbType.NVarChar, SoCMNDCT);
			db.AddInParameter(dbCommand, "@NgayCapGiayPhepCT", SqlDbType.DateTime, NgayCapGiayPhepCT.Year <= 1753 ? DBNull.Value : (object) NgayCapGiayPhepCT);
			db.AddInParameter(dbCommand, "@NoiCapGiayPhepCT", SqlDbType.NVarChar, NoiCapGiayPhepCT);
			db.AddInParameter(dbCommand, "@NoiDangKyHKTTCT", SqlDbType.NVarChar, NoiDangKyHKTTCT);
			db.AddInParameter(dbCommand, "@SoDienThoaiCT", SqlDbType.NVarChar, SoDienThoaiCT);
			db.AddInParameter(dbCommand, "@SoCMNDGD", SqlDbType.NVarChar, SoCMNDGD);
			db.AddInParameter(dbCommand, "@NgayCapGiayPhepGD", SqlDbType.DateTime, NgayCapGiayPhepGD.Year <= 1753 ? DBNull.Value : (object) NgayCapGiayPhepGD);
			db.AddInParameter(dbCommand, "@NoiCapGiayPhepGD", SqlDbType.NVarChar, NoiCapGiayPhepGD);
			db.AddInParameter(dbCommand, "@NoiDangKyHKTTGD", SqlDbType.NVarChar, NoiDangKyHKTTGD);
			db.AddInParameter(dbCommand, "@SoDienThoaiGD", SqlDbType.NVarChar, SoDienThoaiGD);
			db.AddInParameter(dbCommand, "@DaDuocCQHQKT", SqlDbType.Decimal, DaDuocCQHQKT);
			db.AddInParameter(dbCommand, "@BiXuPhatVeBuonLau", SqlDbType.Decimal, BiXuPhatVeBuonLau);
			db.AddInParameter(dbCommand, "@BiXuPhatVeTronThue", SqlDbType.Decimal, BiXuPhatVeTronThue);
			db.AddInParameter(dbCommand, "@BiXuPhatVeKeToan", SqlDbType.Decimal, BiXuPhatVeKeToan);
			db.AddInParameter(dbCommand, "@ThoiGianSanXuat", SqlDbType.Decimal, ThoiGianSanXuat);
			db.AddInParameter(dbCommand, "@SoLuongSanPham", SqlDbType.Decimal, SoLuongSanPham);
			db.AddInParameter(dbCommand, "@BoPhanQuanLy", SqlDbType.Decimal, BoPhanQuanLy);
			db.AddInParameter(dbCommand, "@SoLuongCongNhan", SqlDbType.Decimal, SoLuongCongNhan);
			db.AddInParameter(dbCommand, "@TenCongTyMe", SqlDbType.NVarChar, TenCongTyMe);
			db.AddInParameter(dbCommand, "@MaCongTyMe", SqlDbType.NVarChar, MaCongTyMe);
			db.AddInParameter(dbCommand, "@SoLuongThanhVien", SqlDbType.Decimal, SoLuongThanhVien);
			db.AddInParameter(dbCommand, "@SoLuongChiNhanh", SqlDbType.Decimal, SoLuongChiNhanh);
			db.AddInParameter(dbCommand, "@SoLuongThanhVienCTM", SqlDbType.Decimal, SoLuongThanhVienCTM);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@NgayKetThucNamTC", SqlDbType.DateTime, NgayKetThucNamTC.Year <= 1753 ? DBNull.Value : (object) NgayKetThucNamTC);
			db.AddInParameter(dbCommand, "@LoaiHinhDN", SqlDbType.Int, LoaiHinhDN);
			db.AddInParameter(dbCommand, "@LoaiSua", SqlDbType.Int, LoaiSua);
			db.AddInParameter(dbCommand, "@SoLuongSoHuu", SqlDbType.Decimal, SoLuongSoHuu);
			db.AddInParameter(dbCommand, "@SoLuongDiThue", SqlDbType.Decimal, SoLuongDiThue);
			db.AddInParameter(dbCommand, "@SoLuongKhac", SqlDbType.Decimal, SoLuongKhac);
			db.AddInParameter(dbCommand, "@TongSoLuong", SqlDbType.Decimal, TongSoLuong);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_StorageAreasProduction> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_StorageAreasProduction item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_StorageAreasProduction(long id, int trangThaiXuLy, long soTN, DateTime ngayTN, string maHQ, string tenDoanhNghiep, string maDoanhNghiep, string diaChiTruSoChinh, decimal loaiDiaChiTruSoChinh, string nuocDauTu, string nganhNgheSanXuat, string tenDoanhNghiepTKTD, string maDoanhNghiepTKTD, string lyDoChuyenDoi, string soCMNDCT, DateTime ngayCapGiayPhepCT, string noiCapGiayPhepCT, string noiDangKyHKTTCT, string soDienThoaiCT, string soCMNDGD, DateTime ngayCapGiayPhepGD, string noiCapGiayPhepGD, string noiDangKyHKTTGD, string soDienThoaiGD, decimal daDuocCQHQKT, decimal biXuPhatVeBuonLau, decimal biXuPhatVeTronThue, decimal biXuPhatVeKeToan, decimal thoiGianSanXuat, decimal soLuongSanPham, decimal boPhanQuanLy, decimal soLuongCongNhan, string tenCongTyMe, string maCongTyMe, decimal soLuongThanhVien, decimal soLuongChiNhanh, decimal soLuongThanhVienCTM, string ghiChuKhac, string guidStr, DateTime ngayKetThucNamTC, int loaiHinhDN, int loaiSua, decimal soLuongSoHuu, decimal soLuongDiThue, decimal soLuongKhac, decimal tongSoLuong)
		{
			KDT_VNACCS_StorageAreasProduction entity = new KDT_VNACCS_StorageAreasProduction();			
			entity.ID = id;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.MaHQ = maHQ;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.DiaChiTruSoChinh = diaChiTruSoChinh;
			entity.LoaiDiaChiTruSoChinh = loaiDiaChiTruSoChinh;
			entity.NuocDauTu = nuocDauTu;
			entity.NganhNgheSanXuat = nganhNgheSanXuat;
			entity.TenDoanhNghiepTKTD = tenDoanhNghiepTKTD;
			entity.MaDoanhNghiepTKTD = maDoanhNghiepTKTD;
			entity.LyDoChuyenDoi = lyDoChuyenDoi;
			entity.SoCMNDCT = soCMNDCT;
			entity.NgayCapGiayPhepCT = ngayCapGiayPhepCT;
			entity.NoiCapGiayPhepCT = noiCapGiayPhepCT;
			entity.NoiDangKyHKTTCT = noiDangKyHKTTCT;
			entity.SoDienThoaiCT = soDienThoaiCT;
			entity.SoCMNDGD = soCMNDGD;
			entity.NgayCapGiayPhepGD = ngayCapGiayPhepGD;
			entity.NoiCapGiayPhepGD = noiCapGiayPhepGD;
			entity.NoiDangKyHKTTGD = noiDangKyHKTTGD;
			entity.SoDienThoaiGD = soDienThoaiGD;
			entity.DaDuocCQHQKT = daDuocCQHQKT;
			entity.BiXuPhatVeBuonLau = biXuPhatVeBuonLau;
			entity.BiXuPhatVeTronThue = biXuPhatVeTronThue;
			entity.BiXuPhatVeKeToan = biXuPhatVeKeToan;
			entity.ThoiGianSanXuat = thoiGianSanXuat;
			entity.SoLuongSanPham = soLuongSanPham;
			entity.BoPhanQuanLy = boPhanQuanLy;
			entity.SoLuongCongNhan = soLuongCongNhan;
			entity.TenCongTyMe = tenCongTyMe;
			entity.MaCongTyMe = maCongTyMe;
			entity.SoLuongThanhVien = soLuongThanhVien;
			entity.SoLuongChiNhanh = soLuongChiNhanh;
			entity.SoLuongThanhVienCTM = soLuongThanhVienCTM;
			entity.GhiChuKhac = ghiChuKhac;
			entity.GuidStr = guidStr;
			entity.NgayKetThucNamTC = ngayKetThucNamTC;
			entity.LoaiHinhDN = loaiHinhDN;
			entity.LoaiSua = loaiSua;
			entity.SoLuongSoHuu = soLuongSoHuu;
			entity.SoLuongDiThue = soLuongDiThue;
			entity.SoLuongKhac = soLuongKhac;
			entity.TongSoLuong = tongSoLuong;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@DiaChiTruSoChinh", SqlDbType.NVarChar, DiaChiTruSoChinh);
			db.AddInParameter(dbCommand, "@LoaiDiaChiTruSoChinh", SqlDbType.Decimal, LoaiDiaChiTruSoChinh);
			db.AddInParameter(dbCommand, "@NuocDauTu", SqlDbType.NVarChar, NuocDauTu);
			db.AddInParameter(dbCommand, "@NganhNgheSanXuat", SqlDbType.NVarChar, NganhNgheSanXuat);
			db.AddInParameter(dbCommand, "@TenDoanhNghiepTKTD", SqlDbType.NVarChar, TenDoanhNghiepTKTD);
			db.AddInParameter(dbCommand, "@MaDoanhNghiepTKTD", SqlDbType.NVarChar, MaDoanhNghiepTKTD);
			db.AddInParameter(dbCommand, "@LyDoChuyenDoi", SqlDbType.NVarChar, LyDoChuyenDoi);
			db.AddInParameter(dbCommand, "@SoCMNDCT", SqlDbType.NVarChar, SoCMNDCT);
			db.AddInParameter(dbCommand, "@NgayCapGiayPhepCT", SqlDbType.DateTime, NgayCapGiayPhepCT.Year <= 1753 ? DBNull.Value : (object) NgayCapGiayPhepCT);
			db.AddInParameter(dbCommand, "@NoiCapGiayPhepCT", SqlDbType.NVarChar, NoiCapGiayPhepCT);
			db.AddInParameter(dbCommand, "@NoiDangKyHKTTCT", SqlDbType.NVarChar, NoiDangKyHKTTCT);
			db.AddInParameter(dbCommand, "@SoDienThoaiCT", SqlDbType.NVarChar, SoDienThoaiCT);
			db.AddInParameter(dbCommand, "@SoCMNDGD", SqlDbType.NVarChar, SoCMNDGD);
			db.AddInParameter(dbCommand, "@NgayCapGiayPhepGD", SqlDbType.DateTime, NgayCapGiayPhepGD.Year <= 1753 ? DBNull.Value : (object) NgayCapGiayPhepGD);
			db.AddInParameter(dbCommand, "@NoiCapGiayPhepGD", SqlDbType.NVarChar, NoiCapGiayPhepGD);
			db.AddInParameter(dbCommand, "@NoiDangKyHKTTGD", SqlDbType.NVarChar, NoiDangKyHKTTGD);
			db.AddInParameter(dbCommand, "@SoDienThoaiGD", SqlDbType.NVarChar, SoDienThoaiGD);
			db.AddInParameter(dbCommand, "@DaDuocCQHQKT", SqlDbType.Decimal, DaDuocCQHQKT);
			db.AddInParameter(dbCommand, "@BiXuPhatVeBuonLau", SqlDbType.Decimal, BiXuPhatVeBuonLau);
			db.AddInParameter(dbCommand, "@BiXuPhatVeTronThue", SqlDbType.Decimal, BiXuPhatVeTronThue);
			db.AddInParameter(dbCommand, "@BiXuPhatVeKeToan", SqlDbType.Decimal, BiXuPhatVeKeToan);
			db.AddInParameter(dbCommand, "@ThoiGianSanXuat", SqlDbType.Decimal, ThoiGianSanXuat);
			db.AddInParameter(dbCommand, "@SoLuongSanPham", SqlDbType.Decimal, SoLuongSanPham);
			db.AddInParameter(dbCommand, "@BoPhanQuanLy", SqlDbType.Decimal, BoPhanQuanLy);
			db.AddInParameter(dbCommand, "@SoLuongCongNhan", SqlDbType.Decimal, SoLuongCongNhan);
			db.AddInParameter(dbCommand, "@TenCongTyMe", SqlDbType.NVarChar, TenCongTyMe);
			db.AddInParameter(dbCommand, "@MaCongTyMe", SqlDbType.NVarChar, MaCongTyMe);
			db.AddInParameter(dbCommand, "@SoLuongThanhVien", SqlDbType.Decimal, SoLuongThanhVien);
			db.AddInParameter(dbCommand, "@SoLuongChiNhanh", SqlDbType.Decimal, SoLuongChiNhanh);
			db.AddInParameter(dbCommand, "@SoLuongThanhVienCTM", SqlDbType.Decimal, SoLuongThanhVienCTM);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@NgayKetThucNamTC", SqlDbType.DateTime, NgayKetThucNamTC.Year <= 1753 ? DBNull.Value : (object) NgayKetThucNamTC);
			db.AddInParameter(dbCommand, "@LoaiHinhDN", SqlDbType.Int, LoaiHinhDN);
			db.AddInParameter(dbCommand, "@LoaiSua", SqlDbType.Int, LoaiSua);
			db.AddInParameter(dbCommand, "@SoLuongSoHuu", SqlDbType.Decimal, SoLuongSoHuu);
			db.AddInParameter(dbCommand, "@SoLuongDiThue", SqlDbType.Decimal, SoLuongDiThue);
			db.AddInParameter(dbCommand, "@SoLuongKhac", SqlDbType.Decimal, SoLuongKhac);
			db.AddInParameter(dbCommand, "@TongSoLuong", SqlDbType.Decimal, TongSoLuong);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_StorageAreasProduction> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_StorageAreasProduction item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_StorageAreasProduction(long id)
		{
			KDT_VNACCS_StorageAreasProduction entity = new KDT_VNACCS_StorageAreasProduction();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_StorageAreasProduction> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_StorageAreasProduction item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
                try
                {
                    if (this.ID == 0)
                    {
                        this.TrangThaiXuLy = -1;
                        this.ID = this.Insert();
                    }
                    else
                    {
                        this.Update();
                    }

                    foreach (KDT_VNACCS_ManufactureFactory item in ManufactureFactoryCollection)
                    {
                        item.StorageAreasProduction_ID = this.ID;
                        item.InsertUpdateFull();
                    }
                    foreach (KDT_VNACCS_Career item in CareerCollection)
                    {
                        item.StorageAreasProduction_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                    foreach (KDT_VNACCS_StorageOfGood item in StorageOfGoodCollection)
                    {
                        item.StorageAreasProduction_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                    foreach (KDT_VNACCS_ContentInspection item in ContentInspectionCollection)
                    {
                        item.StorageAreasProduction_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }

                    foreach (KDT_VNACCS_AffiliatedMemberCompany item in AffiliatedMemberCompanyCollection)
                    {
                        item.StorageAreasProduction_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                    foreach (KDT_VNACCS_MemberCompany item in MemberCompanyCollection)
                    {
                        item.StorageAreasProduction_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                    foreach (KDT_VNACCS_HoldingCompany item in HoldingCompanyCollection)
                    {
                        item.StorageAreasProduction_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                    foreach (KDT_VNACCS_StorageAreasProduction_AttachedFile item in AttachedFileGoodCollection)
                    {
                        item.StorageAreasProduction_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                    foreach (KDT_VNACCS_OutsourcingManufactureFactory item in OutsourcingManufactureFactoryCollection)
                    {
                        item.StorageAreasProduction_ID = this.ID;
                        item.InsertUpdateFull();
                    }
                }
                catch (Exception ex)
                {
                    //transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    //connection.Close();
                }
        }
	}	
}