﻿using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Collections.Generic;
using System;
using Company.BLL.SXXK.ToKhai;

namespace Company.BLL.SXXK
{
    public partial class PhanBoToKhaiXuat
    {
        public List<PhanBoToKhaiNhap> PhanBoToKhaiNhapCollection=new List<PhanBoToKhaiNhap>();
        
        ToKhai.ToKhaiMauDich TKMD;
        public void LoadPhanBoToKhaiNhapCollection()
        {
            PhanBoToKhaiNhapCollection = (List<PhanBoToKhaiNhap>)PhanBoToKhaiNhap.SelectCollectionBy_TKXuat_ID(this.ID);
        }
        public void LoadPhanBoToKhaiNhapCollection(SqlTransaction transaction)
        {
            PhanBoToKhaiNhapCollection = (List<PhanBoToKhaiNhap>)PhanBoToKhaiNhap.SelectCollectionBy_TKXuat_ID(this.ID, transaction);
        }
        public void LoadToKhaiMD()
        {
             TKMD= new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
             TKMD.SoToKhai = (int)this.SoToKhaiXuat;
             TKMD.MaHaiQuan = this.MaHaiQuanXuat;
             TKMD.MaLoaiHinh = this.MaLoaiHinhXuat;
             TKMD.NamDangKy = (short)this.NamDangKyXuat;
             TKMD.Load();
        }

        public void InsertUpdateFull(IList<PhanBoToKhaiXuat> pbToKhaiXuatCollection,DataTable tableTon)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    try
                    {
                        TKMD.TrangThai = 0;
                        TKMD.UpdateTransaction(transaction);
                        foreach (PhanBoToKhaiXuat pbToKhaiXuat in pbToKhaiXuatCollection)
                        {
                            if (pbToKhaiXuat.ID == 0)
                                pbToKhaiXuat.Insert(transaction);
                            else
                                pbToKhaiXuat.Update(transaction);
                            foreach (PhanBoToKhaiNhap pbTKNhap in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                            {
                                pbTKNhap.TKXuat_ID = pbToKhaiXuat.ID;                                
                                if (pbTKNhap.ID == 0)
                                    pbTKNhap.Insert(transaction);
                                else
                                    pbTKNhap.Update(transaction);
                            }
                        }
                        foreach (DataRow row in tableTon.Rows)
                        {
                            NPLNhapTonThucTe npl = new NPLNhapTonThucTe();
                            npl.Luong = Convert.ToDecimal(row["Luong"]);
                            npl.MaDoanhNghiep =(row["MaDoanhNghiep"].ToString());
                            npl.MaHaiQuan = (row["MaHaiQuan"].ToString());
                            npl.MaLoaiHinh = (row["MaLoaiHinh"].ToString());
                            npl.MaNPL = (row["MaNPL"].ToString());
                            npl.NamDangKy = Convert.ToInt16(row["NamDangKy"]);
                            npl.NgayDangKy = Convert.ToDateTime(row["NgayDangKy"]);
                            npl.PhuThu = Convert.ToDouble(row["PhuThu"]);
                            npl.SoToKhai = Convert.ToInt32(row["SoToKhai"]);                            
                            npl.ThueCLGia = Convert.ToDouble(row["ThueCLGia"]);
                            npl.ThueTTDB = Convert.ToDouble(row["ThueTTDB"]);
                            npl.ThueVAT = Convert.ToDouble(row["ThueVAT"]);
                            npl.ThueXNK = Convert.ToDouble(row["ThueXNK"]);                            
                            npl.Ton = Convert.ToDecimal(row["Ton"]);
                            npl.ThueXNKTon = Convert.ToDouble(row["ThueXNK"]) * Convert.ToDouble(npl.Ton/npl.Luong);
                            npl.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public void InsertUpdateFull(SqlTransaction transaction)
        {
            if (this.ID == 0)
                this.Insert(transaction);
            else
                this.Update(transaction);
            foreach (PhanBoToKhaiNhap pbTKNhap in this.PhanBoToKhaiNhapCollection)
            {
                pbTKNhap.TKXuat_ID = this.ID;
                if (pbTKNhap.ID == 0)
                    pbTKNhap.Insert(transaction);
                else
                    pbTKNhap.Update(transaction);
            }            
        }

        public void InsertUpdateFullLaiChoToKhaiTaiXuat(SqlTransaction transaction,DataTable tableTon)
        {
            if (this.ID == 0)
                this.Insert(transaction);
            else
                this.Update(transaction);
            foreach (PhanBoToKhaiNhap pbTKNhap in this.PhanBoToKhaiNhapCollection)
            {
                pbTKNhap.TKXuat_ID = this.ID;
                if (pbTKNhap.ID == 0)
                    pbTKNhap.Insert(transaction);
                else
                    pbTKNhap.Update(transaction);
            }
            foreach (DataRow row in tableTon.Rows)
            {
                NPLNhapTonThucTe npl = new NPLNhapTonThucTe();
                npl.Luong = Convert.ToDecimal(row["Luong"]);
                npl.MaDoanhNghiep = (row["MaDoanhNghiep"].ToString());
                npl.MaHaiQuan = (row["MaHaiQuan"].ToString());
                npl.MaLoaiHinh = (row["MaLoaiHinh"].ToString());
                npl.MaNPL = (row["MaNPL"].ToString());
                npl.NamDangKy = Convert.ToInt16(row["NamDangKy"]);
                npl.NgayDangKy = Convert.ToDateTime(row["NgayDangKy"]);
                npl.PhuThu = Convert.ToDouble(row["PhuThu"]);
                npl.SoToKhai = Convert.ToInt32(row["SoToKhai"]);
                npl.ThueCLGia = Convert.ToDouble(row["ThueCLGia"]);
                npl.ThueTTDB = Convert.ToDouble(row["ThueTTDB"]);
                npl.ThueVAT = Convert.ToDouble(row["ThueVAT"]);
                npl.ThueXNK = Convert.ToDouble(row["ThueXNK"]);
                npl.Ton = Convert.ToDecimal(row["Ton"]);
                npl.ThueXNKTon = Convert.ToDouble(row["ThueXNK"]) * Convert.ToDouble(npl.Ton / npl.Luong);
                npl.Update(transaction);
            }
        }

        public void PhanBoChoToKhaiXuat(int SoThapPhanNPL, int chenhLechNgay)
        {
            if (TKMD == null)
            {
                this.LoadToKhaiMD();
                TKMD.LoadHMDCollection();
                TKMD.HMDCollection.Sort(new SortHangMauDich());
            }

            DateTime ngayThucXuat = TKMD.NgayDangKy;
            if (TKMD.NGAY_THN_THX.Year > 1900)
                ngayThucXuat = TKMD.NGAY_THN_THX;
            ngayThucXuat = ngayThucXuat.AddDays((-1) * chenhLechNgay);
            DataTable tableTK = NPLNhapTonThucTe.SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndTonHonO(this.MaHaiQuanXuat, this.MaDoanhNghiep, ngayThucXuat, SoThapPhanNPL,chenhLechNgay).Tables[0];

            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
           
            foreach (Company.BLL.SXXK.ToKhai.HangMauDich HMD in TKMD.HMDCollection)
            {
                PhanBoToKhaiXuat pbToKhaiXuat = new PhanBoToKhaiXuat();
                pbToKhaiXuat.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                pbToKhaiXuat.MaHaiQuanXuat = TKMD.MaHaiQuan;
                pbToKhaiXuat.MaLoaiHinhXuat = TKMD.MaLoaiHinh;
                pbToKhaiXuat.MaSP = HMD.MaPhu.Trim();
                pbToKhaiXuat.NamDangKyXuat = TKMD.NamDangKy;
                pbToKhaiXuat.SoLuongXuat = HMD.SoLuong;
                pbToKhaiXuat.SoToKhaiXuat = TKMD.SoToKhai;
                PhanBoToKhaiXuatCollection.Add(pbToKhaiXuat);


                DataView dtDinhMuc = DinhMuc.getDinhMucOfSanPham(HMD.MaPhu.Trim(), this.MaHaiQuanXuat, this.MaDoanhNghiep,HMD.SoLuong,SoThapPhanNPL).Tables[0].DefaultView;
                if (dtDinhMuc.Count == 0)
                {
                    throw new Exception("Sản phẩm : " + HMD.MaPhu.Trim()+" chưa có định mức trong hệ thống nên không thể phân bổ được.Hãy nhập định mức rồi phân bổ lại.");
                }
                dtDinhMuc.Sort="MaNguyenPhuLieu";                
                foreach (DataRowView row in dtDinhMuc)
                {                    
                    string MaNPL = row["MaNguyenPhuLieu"].ToString();
                   
                    decimal NhuCau = Convert.ToDecimal(row["SoLuong"].ToString());
                    DataRow[] RowToKhaiCollection = tableTK.Select("MaNPL='" + MaNPL + "' and Ton>0 ", "NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan");
                    if (RowToKhaiCollection.Length == 0)
                    {
                        PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                        pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(row["DinhMucChung"]);
                        pbToKhaiNhap.LuongCungUng = Convert.ToDouble(NhuCau);
                        pbToKhaiNhap.MaDoanhNghiep = this.MaDoanhNghiep;
                        pbToKhaiNhap.MaNPL = MaNPL;
                        pbToKhaiXuat.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                        continue;
                    }
                    foreach (DataRow rowTK in RowToKhaiCollection)
                    {
                        PhanBoToKhaiNhap pbToKhaiNhap=new PhanBoToKhaiNhap();
                        decimal LuongTon = Convert.ToDecimal(rowTK["Ton"].ToString());
                        if (LuongTon > 0)
                        {
                            pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(row["DinhMucChung"]);
                            pbToKhaiNhap.LuongCungUng = 0;
                            pbToKhaiNhap.LuongTonDau =Convert.ToDouble(LuongTon);
                            bool ok = true;//true la du --> thoat vong lap//false la thieu phai dung to khai khac
                            if (NhuCau > LuongTon)
                            {                                
                                pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(LuongTon);
                                NhuCau -= LuongTon;
                                rowTK["Ton"] = 0;
                                ok = false;
                                pbToKhaiNhap.LuongTonCuoi = 0;
                            }
                            else
                            {
                                pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(NhuCau);
                                rowTK["Ton"] = (LuongTon - NhuCau);
                                NhuCau = 0;
                                pbToKhaiNhap.LuongTonCuoi = Convert.ToDouble(rowTK["Ton"]);
                            }
                            pbToKhaiNhap.MaDoanhNghiep = rowTK["MaDoanhNghiep"].ToString();
                            pbToKhaiNhap.MaHaiQuanNhap = rowTK["MaHaiQuan"].ToString();
                            pbToKhaiNhap.MaLoaiHinhNhap = rowTK["MaLoaiHinh"].ToString();
                            pbToKhaiNhap.MaNPL = MaNPL;
                            pbToKhaiNhap.NamDangKyNhap = Convert.ToInt16(rowTK["NamDangKy"]);
                            pbToKhaiNhap.SoToKhaiNhap = Convert.ToInt32(rowTK["SoToKhai"]);
                            //if(pbToKhaiNhap.SoToKhaiNhap==)
                            //{

                            //}
                            pbToKhaiXuat.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                            if (ok)
                                break;
                        }
                    }
                    if (NhuCau > 0)
                    {
                        pbToKhaiXuat.PhanBoToKhaiNhapCollection[pbToKhaiXuat.PhanBoToKhaiNhapCollection.Count - 1].LuongCungUng = Convert.ToDouble(NhuCau);
                    }
                }

            }
            this.InsertUpdateFull(PhanBoToKhaiXuatCollection, tableTK);
        }
        public void PhanBoChoToKhaiXuat(SqlTransaction transaction,int SoThapPhanNPL,int chenhLechNgay)
        {
            //DataTable tableTK = NPLNhapTonThucTe.SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndTonHonO(this.MaHaiQuanXuat, this.MaDoanhNghiep,transaction).Tables[0];
            DataView dtDinhMuc = DinhMuc.getDinhMucOfSanPham(this.MaSP, this.MaHaiQuanXuat, this.MaDoanhNghiep, this.SoLuongXuat,SoThapPhanNPL,transaction).Tables[0].DefaultView;
            if (dtDinhMuc.Count == 0)
            {
                throw new Exception("Sản phẩm : " + this.MaSP + " chưa có định mức trong hệ thống nên không thể phân bổ được.Hãy nhập định mức rồi phân bổ lại.");
            }
            dtDinhMuc.Sort = "MaNguyenPhuLieu";
            foreach (DataRowView row in dtDinhMuc)
            {
                string MaNPL = row["MaNguyenPhuLieu"].ToString();
                decimal NhuCau = Convert.ToDecimal(row["SoLuong"].ToString());
                IList<NPLNhapTonThucTe> RowToKhaiCollection = NPLNhapTonThucTe.SelectCollectionBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(this.MaHaiQuanXuat, this.MaDoanhNghiep, MaNPL, this.TKMD.NgayDangKy, SoThapPhanNPL, chenhLechNgay, transaction);
                if (RowToKhaiCollection.Count == 0)
                {
                    PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                    pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(row["DinhMucChung"]);
                    pbToKhaiNhap.LuongCungUng = Convert.ToDouble(NhuCau);
                    pbToKhaiNhap.MaDoanhNghiep = this.MaDoanhNghiep;
                    pbToKhaiNhap.MaNPL = MaNPL;
                    this.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                    continue;
                }
                foreach (NPLNhapTonThucTe rowTK in RowToKhaiCollection)
                {
                    PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                    decimal LuongTon = Convert.ToDecimal(rowTK.Ton);
                    if (LuongTon > 0)
                    {
                        pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(row["DinhMucChung"]);
                        pbToKhaiNhap.LuongCungUng = 0;
                        pbToKhaiNhap.LuongTonDau = Convert.ToDouble(LuongTon);
                        bool ok = true;//true la du --> thoat vong lap//false la thieu phai dung to khai khac
                        if (NhuCau > LuongTon)
                        {
                            pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(LuongTon);
                            NhuCau -= LuongTon;
                            rowTK.Ton= 0;
                            ok = false;
                            pbToKhaiNhap.LuongTonCuoi = 0;
                        }
                        else
                        {
                            pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(NhuCau);
                            rowTK.Ton = (LuongTon - NhuCau);
                            NhuCau = 0;
                            pbToKhaiNhap.LuongTonCuoi = Convert.ToDouble(rowTK.Ton);
                        }
                        rowTK.Update(transaction);
                        pbToKhaiNhap.MaDoanhNghiep = rowTK.MaDoanhNghiep;
                        pbToKhaiNhap.MaHaiQuanNhap = rowTK.MaHaiQuan;
                        pbToKhaiNhap.MaLoaiHinhNhap = rowTK.MaLoaiHinh;
                        pbToKhaiNhap.MaNPL = MaNPL;
                        pbToKhaiNhap.NamDangKyNhap = Convert.ToInt16(rowTK.NamDangKy);
                        pbToKhaiNhap.SoToKhaiNhap = Convert.ToInt32(rowTK.SoToKhai);
                        this.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                        if (ok)
                            break;
                    }
                }
                if (NhuCau > 0)
                {
                    this.PhanBoToKhaiNhapCollection[this.PhanBoToKhaiNhapCollection.Count - 1].LuongCungUng = Convert.ToDouble(NhuCau);
                }
            }

            this.InsertUpdateFull(transaction);
        }

        public void PhanBoChoToKhaiTaiXuat(IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection,int SoThapPhanNPL)
        {
            if (TKMD == null)
            {
                this.LoadToKhaiMD();
                TKMD.LoadHMDCollection();
                TKMD.HMDCollection.Sort(new SortHangMauDich());
            }


            DataTable tableTK = NPLNhapTonThucTe.SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndTonHonO(this.MaHaiQuanXuat, this.MaDoanhNghiep, TKMD.NgayDangKy, SoThapPhanNPL, 0).Tables[0];


            foreach (PhanBoToKhaiXuat pbToKhaiXuat in PhanBoToKhaiXuatCollection)
            {
                foreach (PhanBoToKhaiNhap pbToKhaiNhap in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                {
                    DataRow rowToKhaiNhap = tableTK.Select("SoToKhai=" + pbToKhaiNhap.SoToKhaiNhap + " and MaLoaiHinh='" + pbToKhaiNhap.MaLoaiHinhNhap + "' and NamDangKy=" + pbToKhaiNhap.NamDangKyNhap + " and MaHaiQuan='" + pbToKhaiNhap.MaHaiQuanNhap + "' and MaNPL='" + pbToKhaiNhap.MaNPL.Trim() + "'")[0];
                    pbToKhaiNhap.LuongTonDau = Convert.ToDouble(rowToKhaiNhap["Ton"]);
                    pbToKhaiNhap.LuongTonCuoi = pbToKhaiNhap.LuongTonDau - pbToKhaiNhap.LuongPhanBo;
                    rowToKhaiNhap["Ton"] = pbToKhaiNhap.LuongTonCuoi;
                }
            }
            InsertUpdateFull(PhanBoToKhaiXuatCollection, tableTK);
        }
        public void PhanBoLaiChoToKhaiTaiXuat(SqlTransaction transaction,int SoThapPhanNPL)
        {
            PhanBoToKhaiNhap.DeleteBy_TKXuat_ID(this.ID, transaction);
            DataTable tableTK = NPLNhapTonThucTe.SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(this.MaHaiQuanXuat, this.MaDoanhNghiep, this.MaSP, transaction).Tables[0];

            foreach (PhanBoToKhaiNhap pbToKhaiNhap in this.PhanBoToKhaiNhapCollection)
            {
                DataRow rowToKhaiNhap = tableTK.Select("SoToKhai=" + pbToKhaiNhap.SoToKhaiNhap + " and MaLoaiHinh='" + pbToKhaiNhap.MaLoaiHinhNhap + "' and NamDangKy=" + pbToKhaiNhap.NamDangKyNhap + " and MaHaiQuan='" + pbToKhaiNhap.MaHaiQuanNhap + "' and MaNPL='" + pbToKhaiNhap.MaNPL.Trim() + "'")[0];
                pbToKhaiNhap.LuongTonDau = Convert.ToDouble(rowToKhaiNhap["Ton"]);
                pbToKhaiNhap.LuongTonCuoi = pbToKhaiNhap.LuongTonDau - pbToKhaiNhap.LuongPhanBo;
                rowToKhaiNhap["Ton"] = pbToKhaiNhap.LuongTonCuoi;
                pbToKhaiNhap.ID = 0;
            }

            this.InsertUpdateFullLaiChoToKhaiTaiXuat(transaction, tableTK);
        }
        public static IList<PhanBoToKhaiXuat> SelectCollectionDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction)
        {
            IList<PhanBoToKhaiXuat> collection = new List<PhanBoToKhaiXuat>();

            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression, transaction);
            while (reader.Read())
            {
                PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression,SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand,transaction);
        }
        public static IList<PhanBoToKhaiXuat> SelectPhanBoToKhaiXuatOfTKMD(string MaHaiQuan, string MaLoaiHinh, int SoToKhai, short NamDangKy)
        {
            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            PhanBoToKhaiXuatCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("SoToKhaiXuat=" + SoToKhai + " and NamDangKyXuat=" + NamDangKy + " and MaLoaiHinhXuat='" + MaLoaiHinh + "' and MaHaiQuanXuat='" + MaHaiQuan + "'", "");
            foreach (PhanBoToKhaiXuat pbTKXuat in PhanBoToKhaiXuatCollection)
            {
                pbTKXuat.LoadPhanBoToKhaiNhapCollection();
            }
            return PhanBoToKhaiXuatCollection;
        }
        public static IList<PhanBoToKhaiXuat> SelectPhanBoToKhaiXuatOfTKMD(string MaHaiQuan, string MaLoaiHinh, int SoToKhai, short NamDangKy,SqlTransaction transaction)
        {
            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            PhanBoToKhaiXuatCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("SoToKhaiXuat=" + SoToKhai + " and NamDangKyXuat=" + NamDangKy + " and MaLoaiHinhXuat='" + MaLoaiHinh + "' and MaHaiQuanXuat='" + MaHaiQuan + "'", "",transaction);
            foreach (PhanBoToKhaiXuat pbTKXuat in PhanBoToKhaiXuatCollection)
            {
                pbTKXuat.LoadPhanBoToKhaiNhapCollection(transaction);
            }
            return PhanBoToKhaiXuatCollection;
        }

        public static DataSet SelectViewPhanBoToKhaiXuatOfTKMD(string MaHaiQuan, string MaLoaiHinh, int SoToKhai, short NamDangKy)
        {

            string spName = "SELECT a.*, (SoLuongXuat * DinhMucChung) as NhuCau, b.DVT_ID AS DVT_NPL, c.DVT_ID AS DVT_SP "+
                            "from v_SXXK_PhanBoToKhai a "+
                            "INNER JOIN t_SXXK_NguyenPhuLieu b ON a.MaNPL = b.Ma " +
                            "INNER JOIN t_SXXK_SanPham c ON a.MaSP = c.Ma WHERE a.SoToKhaiXuat=" + SoToKhai + " and a.NamDangKyXuat=" + NamDangKy + " and a.MaLoaiHinhXuat='" + MaLoaiHinh + "' and a.MaHaiQuanXuat='" + MaHaiQuan + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);
            
        }

        public static DataSet SelectViewPhanBoToKhaiXuatOfTKMDAndMaSP(string MaHaiQuan, string MaLoaiHinh, int SoToKhai, short NamDangKy,string MaSP)
        {
            string spName = "select *,(SoLuongXuat * DinhMucChung) as NhuCau from v_SXXK_PhanBoToKhai where SoToKhaiXuat=" + SoToKhai + " and NamDangKyXuat=" + NamDangKy + " and MaLoaiHinhXuat='" + MaLoaiHinh + "' and MaHaiQuanXuat='" + MaHaiQuan + "' and MaSP='" + MaSP + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);            
        }
        public static DataSet SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL(string MaHaiQuan, string MaLoaiHinh, int SoToKhai, short NamDangKy, string MaNPL)
        {
            string spName = "select SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo from v_SXXK_PhanBoToKhai where SoToKhaiXuat=" + SoToKhai + " and NamDangKyXuat=" + NamDangKy + " and MaLoaiHinhXuat='" + MaLoaiHinh + "' and MaHaiQuanXuat='" + MaHaiQuan + "' and MaNPL='" + MaNPL + "'" +
                 " GROUP BY SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }

        public void DeletePhanBo(SqlTransaction transaction)
        {
            foreach (PhanBoToKhaiNhap pbToKhaiNhap in this.PhanBoToKhaiNhapCollection)
            {
                if (pbToKhaiNhap.SoToKhaiNhap > 0)
                {
                    NPLNhapTonThucTe nplTon = NPLNhapTonThucTe.Load(pbToKhaiNhap.SoToKhaiNhap, pbToKhaiNhap.MaLoaiHinhNhap, (short)pbToKhaiNhap.NamDangKyNhap, pbToKhaiNhap.MaHaiQuanNhap, pbToKhaiNhap.MaNPL, transaction);
                    if (nplTon == null)
                    {
                        throw new Exception("Không có tờ khai : " + pbToKhaiNhap.SoToKhaiNhap + "/" + pbToKhaiNhap.MaLoaiHinhNhap + "/" + pbToKhaiNhap.NamDangKyNhap +"không có trong hệ thống.");
                    }
                    nplTon.Ton += Convert.ToDecimal(pbToKhaiNhap.LuongPhanBo);
                    nplTon.ThueXNKTon = Convert.ToDouble(nplTon.Ton / nplTon.Luong) * nplTon.ThueXNK;
                    nplTon.Update(transaction);
                }
                pbToKhaiNhap.Delete(transaction);
            }
            this.Delete(transaction);
        }
        public static void DeletePhanBoToKhaiXuat(IList<PhanBoToKhaiXuat> pbToKhaiXuatCollection)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (PhanBoToKhaiXuat item in pbToKhaiXuatCollection)
                        {
                            item.DeletePhanBo(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
 
        }
        //cap nhat lai luong ton truoc khi phan bo lai cho to khai //la tat co cac to khai da dc phan bo truoc do cap nhat lai
        public void CapNhatLuongTon(SqlTransaction transaction)
        {
            foreach (PhanBoToKhaiNhap pbToKhaiNhap in this.PhanBoToKhaiNhapCollection)
            {
                if (pbToKhaiNhap.SoToKhaiNhap > 0)
                {
                    NPLNhapTonThucTe nplTon = NPLNhapTonThucTe.Load(pbToKhaiNhap.SoToKhaiNhap, pbToKhaiNhap.MaLoaiHinhNhap, (short)pbToKhaiNhap.NamDangKyNhap, pbToKhaiNhap.MaHaiQuanNhap, pbToKhaiNhap.MaNPL, transaction);
                    if (nplTon != null)
                    {
                        nplTon.Ton += Convert.ToDecimal(pbToKhaiNhap.LuongPhanBo);
                        nplTon.ThueXNKTon = Convert.ToDouble(nplTon.Ton / nplTon.Luong) * nplTon.ThueXNK;
                        nplTon.Update(transaction);
                    }
                }
                pbToKhaiNhap.Delete(transaction);
            }
            //this.Delete(transaction);
        }

        public void CapNhatLuongTonTKTaiXuat(SqlTransaction transaction)
        {
            foreach (PhanBoToKhaiNhap pbToKhaiNhap in this.PhanBoToKhaiNhapCollection)
            {
                NPLNhapTonThucTe nplTon = NPLNhapTonThucTe.Load(pbToKhaiNhap.SoToKhaiNhap, pbToKhaiNhap.MaLoaiHinhNhap, (short)pbToKhaiNhap.NamDangKyNhap, pbToKhaiNhap.MaHaiQuanNhap, pbToKhaiNhap.MaNPL, transaction);
                if (nplTon != null)
                {
                    nplTon.Ton += Convert.ToDecimal(pbToKhaiNhap.LuongPhanBo);
                    nplTon.ThueXNKTon = Convert.ToDouble(nplTon.Ton / nplTon.Luong) * nplTon.ThueXNK;
                    nplTon.Update(transaction);
                }
                pbToKhaiNhap.Delete(transaction);
            }
            //this.Delete(transaction);
        }
        //phan bo lai cho mot to khai xuat da dc phan bo truoc doi idmax>0//ko co trong bang phan bo thi ko goi ham nay
        //phan bo lai cho to khai nay thi tuc la phan bo lai cho nhung to khai da phan bo khac  o sau no.
        //public static void PhanBoLaiChoToKhaiXuat1(IList<PhanBoToKhaiXuat> pbTKXCollection)
        //{
        //    long IdMax = 0;
            
        //    foreach (PhanBoToKhaiXuat pbtkx in pbTKXCollection)
        //    {
        //        if (pbtkx.ID > IdMax)
        //            IdMax = pbtkx.ID;
        //        if (pbtkx.PhanBoToKhaiNhapCollection.Count == 0)
        //            pbtkx.LoadPhanBoToKhaiNhapCollection();
        //    }
        //    IList<PhanBoToKhaiXuat> pbToKhaiXuatKhacCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("id>"+IdMax, "");
        //    foreach (PhanBoToKhaiXuat pbtkx in pbToKhaiXuatKhacCollection)
        //    {
        //        pbtkx.LoadPhanBoToKhaiNhapCollection();
        //    }
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        using (SqlTransaction transaction = connection.BeginTransaction())
        //        {
        //            try
        //            {
        //                foreach (PhanBoToKhaiXuat pbtkx in pbTKXCollection)
        //                {
        //                    ToKhai.ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //                    TKMD.SoToKhai = pbtkx.SoToKhaiXuat;
        //                    TKMD.MaLoaiHinh = pbtkx.MaLoaiHinhXuat;
        //                    TKMD.NamDangKy =(short) pbtkx.NamDangKyXuat;
        //                    TKMD.MaHaiQuan = pbtkx.MaHaiQuanXuat;
        //                    TKMD.Load(transaction);
        //                    if (TKMD.Xuat_NPL_SP == "S")
        //                        pbtkx.CapNhatLuongTon(transaction);
        //                    else
        //                        pbtkx.CapNhatLuongTonTKTaiXuat(transaction);
        //                }
        //                foreach (PhanBoToKhaiXuat pbtkx in pbToKhaiXuatKhacCollection)
        //                {
        //                    ToKhai.ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //                    TKMD.SoToKhai = pbtkx.SoToKhaiXuat;
        //                    TKMD.MaLoaiHinh = pbtkx.MaLoaiHinhXuat;
        //                    TKMD.NamDangKy = (short)pbtkx.NamDangKyXuat;
        //                    TKMD.MaHaiQuan = pbtkx.MaHaiQuanXuat;
        //                    TKMD.Load(transaction);
        //                    if (TKMD.Xuat_NPL_SP == "S")
        //                        pbtkx.CapNhatLuongTon(transaction);
        //                    else
        //                        pbtkx.CapNhatLuongTonTKTaiXuat(transaction);
        //                }
        //                foreach (PhanBoToKhaiXuat pbtkx in pbTKXCollection)
        //                {
        //                    ToKhai.ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //                    TKMD.SoToKhai = pbtkx.SoToKhaiXuat;
        //                    TKMD.MaLoaiHinh = pbtkx.MaLoaiHinhXuat;
        //                    TKMD.NamDangKy = (short)pbtkx.NamDangKyXuat;
        //                    TKMD.MaHaiQuan = pbtkx.MaHaiQuanXuat;
        //                    TKMD.Load(transaction);
        //                    if (TKMD.Xuat_NPL_SP == "S")
        //                        pbtkx.PhanBoChoToKhaiXuat(transaction);
        //                    else
        //                        pbtkx.PhanBoLaiChoToKhaiTaiXuat(transaction);
                            
        //                }
        //                foreach (PhanBoToKhaiXuat pbtkx in pbToKhaiXuatKhacCollection)
        //                {
        //                    ToKhai.ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //                    TKMD.SoToKhai = pbtkx.SoToKhaiXuat;
        //                    TKMD.MaLoaiHinh = pbtkx.MaLoaiHinhXuat;
        //                    TKMD.NamDangKy = (short)pbtkx.NamDangKyXuat;
        //                    TKMD.MaHaiQuan = pbtkx.MaHaiQuanXuat;
        //                    TKMD.Load(transaction);
        //                    if (TKMD.Xuat_NPL_SP == "S")
        //                        pbtkx.PhanBoChoToKhaiXuat(transaction);
        //                    else
        //                        pbtkx.PhanBoLaiChoToKhaiTaiXuat(transaction);
        //                }
        //                transaction.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw new Exception("Error at DeleteCollection method: " + ex.Message);
        //            }
        //            finally
        //            {
        //                connection.Close();
        //            }
        //        }
        //    }
        //}

        public static long SelectToKhaiMauDichMinDaPhanBo(string MaHaiQuanXuat, string MaDoanhNghiep)
        {
            string sql = "select min(id) from t_SXXK_PhanBoToKhaiXuat where MaHaiQuanXuat=@MaHaiQuan and MaDoanhNghiep=@MaDoanhNghiep";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuanXuat);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            return Convert.ToInt32(db.ExecuteScalar(dbCommand));    
        }
        public static long SelectToKhaiMauDichMinDaPhanBo(SqlTransaction transaction,string MaHaiQuanXuat, string MaDoanhNghiep)
        {
            string sql = "select min(id) from t_SXXK_PhanBoToKhaiXuat where MaHaiQuanXuat=@MaHaiQuan and MaDoanhNghiep=@MaDoanhNghiep";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuanXuat);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            return Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
        }
        
    }

    public class SortHangMauDich : IComparer
    {
        int IComparer.Compare(object hang1, object hang2)
        {
            Company.BLL.SXXK.ToKhai.HangMauDich h1 = (Company.BLL.SXXK.ToKhai.HangMauDich)hang1;
            Company.BLL.SXXK.ToKhai.HangMauDich h2 = (Company.BLL.SXXK.ToKhai.HangMauDich)hang2;
            if (h1.MaPhu.CompareTo(h2.MaPhu) == 1)
                return 1;
            else if (h1.MaPhu.CompareTo(h2.MaPhu) == 0)
                return 0;
            else
                return -1;
        }
    }

  
}