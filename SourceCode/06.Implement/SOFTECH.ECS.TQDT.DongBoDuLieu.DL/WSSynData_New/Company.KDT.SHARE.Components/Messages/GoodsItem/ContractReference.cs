﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.GoodsItem;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages;

namespace Company.KDT.SHARE.Components
{
   public class ContractReference
    {
        /// <summary>
        /// Số thứ tự
        /// </summary>
       [XmlElement("sequence")]
       public decimal Sequence { get; set; }

        /// <summary>
        /// Số hợp đồng
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày hợp đồng
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Mã hải quan tiếp nhận
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }

        /// <summary>
        /// Ngày hết hạn
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }

        /// <summary>
        /// AdditionalInformation
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public Company.KDT.SHARE.Components.AdditionalDocument AdditionalInformation { get; set; }

        /// <summary>
        /// Thông tin về máy móc thiết bị
        /// </summary>
        [XmlElement("GoodsItems")]
        public GoodsItems GoodsItems { get; set; }
    }
}
