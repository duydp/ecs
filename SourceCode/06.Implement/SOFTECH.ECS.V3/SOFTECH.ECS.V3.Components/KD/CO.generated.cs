﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.KD
{
    public partial class CO
    {
        #region Properties.

        public long ID { set; get; }
        public string SoCO { set; get; }
        public DateTime NgayCO { set; get; }
        public string ToChucCap { set; get; }
        public string NuocCapCO { set; get; }
        public string MaNuocXKTrenCO { set; get; }
        public string MaNuocNKTrenCO { set; get; }
        public string TenDiaChiNguoiXK { set; get; }
        public string TenDiaChiNguoiNK { set; get; }
        public string LoaiCO { set; get; }
        public string ThongTinMoTaChiTiet { set; get; }
        public string MaDoanhNghiep { set; get; }
        public string NguoiKy { set; get; }
        public int NoCo { set; get; }
        public DateTime ThoiHanNop { set; get; }
        public long TKMD_ID { set; get; }
        public string GuidStr { set; get; }
        public int LoaiKB { set; get; }
        public long SoTiepNhan { set; get; }
        public DateTime NgayTiepNhan { set; get; }
        public int TrangThai { set; get; }
        public int NamTiepNhan { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<CO> ConvertToCollection(IDataReader reader)
        {
            IList<CO> collection = new List<CO>();
            while (reader.Read())
            {
                CO entity = new CO();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKy"))) entity.NguoiKy = reader.GetString(reader.GetOrdinal("NguoiKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetInt32(reader.GetOrdinal("NoCo"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static CO Load(long id)
        {
            const string spName = "[dbo].[p_KDT_CO_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<CO> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<CO> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<CO> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<CO> SelectCollectionBy_TKMD_ID(long tKMD_ID)
        {
            IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_CO_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_CO_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_CO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_CO_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_CO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "p_KDT_CO_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertCO(string soCO, DateTime ngayCO, string toChucCap, string nuocCapCO, string maNuocXKTrenCO, string maNuocNKTrenCO, string tenDiaChiNguoiXK, string tenDiaChiNguoiNK, string loaiCO, string thongTinMoTaChiTiet, string maDoanhNghiep, string nguoiKy, int noCo, DateTime thoiHanNop, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan)
        {
            CO entity = new CO();
            entity.SoCO = soCO;
            entity.NgayCO = ngayCO;
            entity.ToChucCap = toChucCap;
            entity.NuocCapCO = nuocCapCO;
            entity.MaNuocXKTrenCO = maNuocXKTrenCO;
            entity.MaNuocNKTrenCO = maNuocNKTrenCO;
            entity.TenDiaChiNguoiXK = tenDiaChiNguoiXK;
            entity.TenDiaChiNguoiNK = tenDiaChiNguoiNK;
            entity.LoaiCO = loaiCO;
            entity.ThongTinMoTaChiTiet = thongTinMoTaChiTiet;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.NguoiKy = nguoiKy;
            entity.NoCo = noCo;
            entity.ThoiHanNop = thoiHanNop;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_CO_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, SoCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, NgayCO.Year <= 1753 ? DBNull.Value : (object)NgayCO);
            db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
            db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
            db.AddInParameter(dbCommand, "@MaNuocXKTrenCO", SqlDbType.VarChar, MaNuocXKTrenCO);
            db.AddInParameter(dbCommand, "@MaNuocNKTrenCO", SqlDbType.VarChar, MaNuocNKTrenCO);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiXK", SqlDbType.NVarChar, TenDiaChiNguoiXK);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiNK", SqlDbType.NVarChar, TenDiaChiNguoiNK);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, LoaiCO);
            db.AddInParameter(dbCommand, "@ThongTinMoTaChiTiet", SqlDbType.NVarChar, ThongTinMoTaChiTiet);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NguoiKy", SqlDbType.NVarChar, NguoiKy);
            db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Int, NoCo);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1753 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<CO> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CO item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateCO(long id, string soCO, DateTime ngayCO, string toChucCap, string nuocCapCO, string maNuocXKTrenCO, string maNuocNKTrenCO, string tenDiaChiNguoiXK, string tenDiaChiNguoiNK, string loaiCO, string thongTinMoTaChiTiet, string maDoanhNghiep, string nguoiKy, int noCo, DateTime thoiHanNop, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan)
        {
            CO entity = new CO();
            entity.ID = id;
            entity.SoCO = soCO;
            entity.NgayCO = ngayCO;
            entity.ToChucCap = toChucCap;
            entity.NuocCapCO = nuocCapCO;
            entity.MaNuocXKTrenCO = maNuocXKTrenCO;
            entity.MaNuocNKTrenCO = maNuocNKTrenCO;
            entity.TenDiaChiNguoiXK = tenDiaChiNguoiXK;
            entity.TenDiaChiNguoiNK = tenDiaChiNguoiNK;
            entity.LoaiCO = loaiCO;
            entity.ThongTinMoTaChiTiet = thongTinMoTaChiTiet;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.NguoiKy = nguoiKy;
            entity.NoCo = noCo;
            entity.ThoiHanNop = thoiHanNop;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_CO_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, SoCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, NgayCO.Year <= 1753 ? DBNull.Value : (object)NgayCO);
            db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
            db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
            db.AddInParameter(dbCommand, "@MaNuocXKTrenCO", SqlDbType.VarChar, MaNuocXKTrenCO);
            db.AddInParameter(dbCommand, "@MaNuocNKTrenCO", SqlDbType.VarChar, MaNuocNKTrenCO);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiXK", SqlDbType.NVarChar, TenDiaChiNguoiXK);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiNK", SqlDbType.NVarChar, TenDiaChiNguoiNK);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, LoaiCO);
            db.AddInParameter(dbCommand, "@ThongTinMoTaChiTiet", SqlDbType.NVarChar, ThongTinMoTaChiTiet);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NguoiKy", SqlDbType.NVarChar, NguoiKy);
            db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Int, NoCo);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1753 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<CO> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CO item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateCO(long id, string soCO, DateTime ngayCO, string toChucCap, string nuocCapCO, string maNuocXKTrenCO, string maNuocNKTrenCO, string tenDiaChiNguoiXK, string tenDiaChiNguoiNK, string loaiCO, string thongTinMoTaChiTiet, string maDoanhNghiep, string nguoiKy, int noCo, DateTime thoiHanNop, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan)
        {
            CO entity = new CO();
            entity.ID = id;
            entity.SoCO = soCO;
            entity.NgayCO = ngayCO;
            entity.ToChucCap = toChucCap;
            entity.NuocCapCO = nuocCapCO;
            entity.MaNuocXKTrenCO = maNuocXKTrenCO;
            entity.MaNuocNKTrenCO = maNuocNKTrenCO;
            entity.TenDiaChiNguoiXK = tenDiaChiNguoiXK;
            entity.TenDiaChiNguoiNK = tenDiaChiNguoiNK;
            entity.LoaiCO = loaiCO;
            entity.ThongTinMoTaChiTiet = thongTinMoTaChiTiet;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.NguoiKy = nguoiKy;
            entity.NoCo = noCo;
            entity.ThoiHanNop = thoiHanNop;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_CO_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, SoCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, NgayCO.Year <= 1753 ? DBNull.Value : (object)NgayCO);
            db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
            db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
            db.AddInParameter(dbCommand, "@MaNuocXKTrenCO", SqlDbType.VarChar, MaNuocXKTrenCO);
            db.AddInParameter(dbCommand, "@MaNuocNKTrenCO", SqlDbType.VarChar, MaNuocNKTrenCO);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiXK", SqlDbType.NVarChar, TenDiaChiNguoiXK);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiNK", SqlDbType.NVarChar, TenDiaChiNguoiNK);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, LoaiCO);
            db.AddInParameter(dbCommand, "@ThongTinMoTaChiTiet", SqlDbType.NVarChar, ThongTinMoTaChiTiet);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NguoiKy", SqlDbType.NVarChar, NguoiKy);
            db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Int, NoCo);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1753 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<CO> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CO item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteCO(long id)
        {
            CO entity = new CO();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_CO_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_CO_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_CO_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<CO> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CO item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}