﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_BCXuatNhapTon : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long STT { set; get; }
		public DateTime TuNgay { set; get; }
		public DateTime DenNgay { set; get; }
		public long HopDong_ID { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string MaNPL { set; get; }
		public string TenNPL { set; get; }
		public string TenDVT_NPL { set; get; }
		public long SoToKhaiNhap { set; get; }
		public DateTime NgayDangKyNhap { set; get; }
		public DateTime NgayHoanThanhNhap { set; get; }
		public string MaLoaiHinhNhap { set; get; }
		public decimal LuongNhap { set; get; }
		public string MaSP { set; get; }
		public string TenSP { set; get; }
		public long SoToKhaiXuat { set; get; }
		public DateTime NgayDangKyXuat { set; get; }
		public DateTime NgayHoanThanhXuat { set; get; }
		public string MaLoaiHinhXuat { set; get; }
		public decimal LuongSPXuat { set; get; }
		public string TenDVT_SP { set; get; }
		public decimal DinhMuc { set; get; }
		public decimal LuongNPLSuDung { set; get; }
		public long SoToKhaiTaiXuat { set; get; }
		public DateTime NgayTaiXuat { set; get; }
		public decimal LuongNPLTaiXuat { set; get; }
		public decimal LuongTonCuoi { set; get; }
		public string ThanhKhoanTiep { set; get; }
		public string ChuyenMucDichKhac { set; get; }
		public double DonGiaTT { set; get; }
		public decimal TyGiaTT { set; get; }
		public decimal ThueSuat { set; get; }
		public double ThueXNK { set; get; }
		public double ThueXNKTon { set; get; }
		public DateTime NgayThucXuat { set; get; }
		public int SoThuTuHang { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_BCXuatNhapTon> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_BCXuatNhapTon> collection = new List<KDT_GC_BCXuatNhapTon>();
			while (reader.Read())
			{
				KDT_GC_BCXuatNhapTon entity = new KDT_GC_BCXuatNhapTon();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt64(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TuNgay"))) entity.TuNgay = reader.GetDateTime(reader.GetOrdinal("TuNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("DenNgay"))) entity.DenNgay = reader.GetDateTime(reader.GetOrdinal("DenNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) entity.TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt64(reader.GetOrdinal("SoToKhaiNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhNhap"))) entity.NgayHoanThanhNhap = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSP"))) entity.TenSP = reader.GetString(reader.GetOrdinal("TenSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt64(reader.GetOrdinal("SoToKhaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhXuat"))) entity.NgayHoanThanhXuat = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongSPXuat"))) entity.LuongSPXuat = reader.GetDecimal(reader.GetOrdinal("LuongSPXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_SP"))) entity.TenDVT_SP = reader.GetString(reader.GetOrdinal("TenDVT_SP"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetDecimal(reader.GetOrdinal("DinhMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) entity.LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiTaiXuat"))) entity.SoToKhaiTaiXuat = reader.GetInt64(reader.GetOrdinal("SoToKhaiTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTaiXuat"))) entity.NgayTaiXuat = reader.GetDateTime(reader.GetOrdinal("NgayTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTaiXuat"))) entity.LuongNPLTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongNPLTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDecimal(reader.GetOrdinal("LuongTonCuoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhKhoanTiep"))) entity.ThanhKhoanTiep = reader.GetString(reader.GetOrdinal("ThanhKhoanTiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChuyenMucDichKhac"))) entity.ChuyenMucDichKhac = reader.GetString(reader.GetOrdinal("ChuyenMucDichKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDouble(reader.GetOrdinal("DonGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTT"))) entity.TyGiaTT = reader.GetDecimal(reader.GetOrdinal("TyGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) entity.ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayThucXuat"))) entity.NgayThucXuat = reader.GetDateTime(reader.GetOrdinal("NgayThucXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_BCXuatNhapTon> collection, long id)
        {
            foreach (KDT_GC_BCXuatNhapTon item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_BCXuatNhapTon VALUES(@STT, @TuNgay, @DenNgay, @HopDong_ID, @MaDoanhNghiep, @MaNPL, @TenNPL, @TenDVT_NPL, @SoToKhaiNhap, @NgayDangKyNhap, @NgayHoanThanhNhap, @MaLoaiHinhNhap, @LuongNhap, @MaSP, @TenSP, @SoToKhaiXuat, @NgayDangKyXuat, @NgayHoanThanhXuat, @MaLoaiHinhXuat, @LuongSPXuat, @TenDVT_SP, @DinhMuc, @LuongNPLSuDung, @SoToKhaiTaiXuat, @NgayTaiXuat, @LuongNPLTaiXuat, @LuongTonCuoi, @ThanhKhoanTiep, @ChuyenMucDichKhac, @DonGiaTT, @TyGiaTT, @ThueSuat, @ThueXNK, @ThueXNKTon, @NgayThucXuat, @SoThuTuHang)";
            string update = "UPDATE t_KDT_GC_BCXuatNhapTon SET STT = @STT, TuNgay = @TuNgay, DenNgay = @DenNgay, HopDong_ID = @HopDong_ID, MaDoanhNghiep = @MaDoanhNghiep, MaNPL = @MaNPL, TenNPL = @TenNPL, TenDVT_NPL = @TenDVT_NPL, SoToKhaiNhap = @SoToKhaiNhap, NgayDangKyNhap = @NgayDangKyNhap, NgayHoanThanhNhap = @NgayHoanThanhNhap, MaLoaiHinhNhap = @MaLoaiHinhNhap, LuongNhap = @LuongNhap, MaSP = @MaSP, TenSP = @TenSP, SoToKhaiXuat = @SoToKhaiXuat, NgayDangKyXuat = @NgayDangKyXuat, NgayHoanThanhXuat = @NgayHoanThanhXuat, MaLoaiHinhXuat = @MaLoaiHinhXuat, LuongSPXuat = @LuongSPXuat, TenDVT_SP = @TenDVT_SP, DinhMuc = @DinhMuc, LuongNPLSuDung = @LuongNPLSuDung, SoToKhaiTaiXuat = @SoToKhaiTaiXuat, NgayTaiXuat = @NgayTaiXuat, LuongNPLTaiXuat = @LuongNPLTaiXuat, LuongTonCuoi = @LuongTonCuoi, ThanhKhoanTiep = @ThanhKhoanTiep, ChuyenMucDichKhac = @ChuyenMucDichKhac, DonGiaTT = @DonGiaTT, TyGiaTT = @TyGiaTT, ThueSuat = @ThueSuat, ThueXNK = @ThueXNK, ThueXNKTon = @ThueXNKTon, NgayThucXuat = @NgayThucXuat, SoThuTuHang = @SoThuTuHang WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_BCXuatNhapTon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.BigInt, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDVT_NPL", SqlDbType.VarChar, "TenDVT_NPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiNhap", SqlDbType.BigInt, "SoToKhaiNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyNhap", SqlDbType.DateTime, "NgayDangKyNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, "NgayHoanThanhNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinhNhap", SqlDbType.Char, "MaLoaiHinhNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhap", SqlDbType.Decimal, "LuongNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSP", SqlDbType.NVarChar, "TenSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiXuat", SqlDbType.BigInt, "SoToKhaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyXuat", SqlDbType.DateTime, "NgayDangKyXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, "NgayHoanThanhXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinhXuat", SqlDbType.Char, "MaLoaiHinhXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSPXuat", SqlDbType.Decimal, "LuongSPXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDVT_SP", SqlDbType.VarChar, "TenDVT_SP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMuc", SqlDbType.Decimal, "DinhMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNPLSuDung", SqlDbType.Decimal, "LuongNPLSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiTaiXuat", SqlDbType.BigInt, "SoToKhaiTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTaiXuat", SqlDbType.DateTime, "NgayTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, "LuongNPLTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonCuoi", SqlDbType.Decimal, "LuongTonCuoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThanhKhoanTiep", SqlDbType.NVarChar, "ThanhKhoanTiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuyenMucDichKhac", SqlDbType.NVarChar, "ChuyenMucDichKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTT", SqlDbType.Float, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGiaTT", SqlDbType.Money, "TyGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueXNK", SqlDbType.Float, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueXNKTon", SqlDbType.Float, "ThueXNKTon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayThucXuat", SqlDbType.DateTime, "NgayThucXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.BigInt, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDVT_NPL", SqlDbType.VarChar, "TenDVT_NPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiNhap", SqlDbType.BigInt, "SoToKhaiNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyNhap", SqlDbType.DateTime, "NgayDangKyNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, "NgayHoanThanhNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinhNhap", SqlDbType.Char, "MaLoaiHinhNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhap", SqlDbType.Decimal, "LuongNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSP", SqlDbType.NVarChar, "TenSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiXuat", SqlDbType.BigInt, "SoToKhaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyXuat", SqlDbType.DateTime, "NgayDangKyXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, "NgayHoanThanhXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinhXuat", SqlDbType.Char, "MaLoaiHinhXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSPXuat", SqlDbType.Decimal, "LuongSPXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDVT_SP", SqlDbType.VarChar, "TenDVT_SP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMuc", SqlDbType.Decimal, "DinhMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNPLSuDung", SqlDbType.Decimal, "LuongNPLSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiTaiXuat", SqlDbType.BigInt, "SoToKhaiTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTaiXuat", SqlDbType.DateTime, "NgayTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, "LuongNPLTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonCuoi", SqlDbType.Decimal, "LuongTonCuoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThanhKhoanTiep", SqlDbType.NVarChar, "ThanhKhoanTiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuyenMucDichKhac", SqlDbType.NVarChar, "ChuyenMucDichKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTT", SqlDbType.Float, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGiaTT", SqlDbType.Money, "TyGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueXNK", SqlDbType.Float, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueXNKTon", SqlDbType.Float, "ThueXNKTon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayThucXuat", SqlDbType.DateTime, "NgayThucXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_BCXuatNhapTon VALUES(@STT, @TuNgay, @DenNgay, @HopDong_ID, @MaDoanhNghiep, @MaNPL, @TenNPL, @TenDVT_NPL, @SoToKhaiNhap, @NgayDangKyNhap, @NgayHoanThanhNhap, @MaLoaiHinhNhap, @LuongNhap, @MaSP, @TenSP, @SoToKhaiXuat, @NgayDangKyXuat, @NgayHoanThanhXuat, @MaLoaiHinhXuat, @LuongSPXuat, @TenDVT_SP, @DinhMuc, @LuongNPLSuDung, @SoToKhaiTaiXuat, @NgayTaiXuat, @LuongNPLTaiXuat, @LuongTonCuoi, @ThanhKhoanTiep, @ChuyenMucDichKhac, @DonGiaTT, @TyGiaTT, @ThueSuat, @ThueXNK, @ThueXNKTon, @NgayThucXuat, @SoThuTuHang)";
            string update = "UPDATE t_KDT_GC_BCXuatNhapTon SET STT = @STT, TuNgay = @TuNgay, DenNgay = @DenNgay, HopDong_ID = @HopDong_ID, MaDoanhNghiep = @MaDoanhNghiep, MaNPL = @MaNPL, TenNPL = @TenNPL, TenDVT_NPL = @TenDVT_NPL, SoToKhaiNhap = @SoToKhaiNhap, NgayDangKyNhap = @NgayDangKyNhap, NgayHoanThanhNhap = @NgayHoanThanhNhap, MaLoaiHinhNhap = @MaLoaiHinhNhap, LuongNhap = @LuongNhap, MaSP = @MaSP, TenSP = @TenSP, SoToKhaiXuat = @SoToKhaiXuat, NgayDangKyXuat = @NgayDangKyXuat, NgayHoanThanhXuat = @NgayHoanThanhXuat, MaLoaiHinhXuat = @MaLoaiHinhXuat, LuongSPXuat = @LuongSPXuat, TenDVT_SP = @TenDVT_SP, DinhMuc = @DinhMuc, LuongNPLSuDung = @LuongNPLSuDung, SoToKhaiTaiXuat = @SoToKhaiTaiXuat, NgayTaiXuat = @NgayTaiXuat, LuongNPLTaiXuat = @LuongNPLTaiXuat, LuongTonCuoi = @LuongTonCuoi, ThanhKhoanTiep = @ThanhKhoanTiep, ChuyenMucDichKhac = @ChuyenMucDichKhac, DonGiaTT = @DonGiaTT, TyGiaTT = @TyGiaTT, ThueSuat = @ThueSuat, ThueXNK = @ThueXNK, ThueXNKTon = @ThueXNKTon, NgayThucXuat = @NgayThucXuat, SoThuTuHang = @SoThuTuHang WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_BCXuatNhapTon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.BigInt, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDVT_NPL", SqlDbType.VarChar, "TenDVT_NPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiNhap", SqlDbType.BigInt, "SoToKhaiNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyNhap", SqlDbType.DateTime, "NgayDangKyNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, "NgayHoanThanhNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinhNhap", SqlDbType.Char, "MaLoaiHinhNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhap", SqlDbType.Decimal, "LuongNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSP", SqlDbType.NVarChar, "TenSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiXuat", SqlDbType.BigInt, "SoToKhaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyXuat", SqlDbType.DateTime, "NgayDangKyXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, "NgayHoanThanhXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinhXuat", SqlDbType.Char, "MaLoaiHinhXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSPXuat", SqlDbType.Decimal, "LuongSPXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDVT_SP", SqlDbType.VarChar, "TenDVT_SP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMuc", SqlDbType.Decimal, "DinhMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNPLSuDung", SqlDbType.Decimal, "LuongNPLSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiTaiXuat", SqlDbType.BigInt, "SoToKhaiTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTaiXuat", SqlDbType.DateTime, "NgayTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, "LuongNPLTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonCuoi", SqlDbType.Decimal, "LuongTonCuoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThanhKhoanTiep", SqlDbType.NVarChar, "ThanhKhoanTiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuyenMucDichKhac", SqlDbType.NVarChar, "ChuyenMucDichKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTT", SqlDbType.Float, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGiaTT", SqlDbType.Money, "TyGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueXNK", SqlDbType.Float, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueXNKTon", SqlDbType.Float, "ThueXNKTon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayThucXuat", SqlDbType.DateTime, "NgayThucXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.BigInt, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDVT_NPL", SqlDbType.VarChar, "TenDVT_NPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiNhap", SqlDbType.BigInt, "SoToKhaiNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyNhap", SqlDbType.DateTime, "NgayDangKyNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, "NgayHoanThanhNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinhNhap", SqlDbType.Char, "MaLoaiHinhNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhap", SqlDbType.Decimal, "LuongNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSP", SqlDbType.NVarChar, "TenSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiXuat", SqlDbType.BigInt, "SoToKhaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyXuat", SqlDbType.DateTime, "NgayDangKyXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, "NgayHoanThanhXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinhXuat", SqlDbType.Char, "MaLoaiHinhXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSPXuat", SqlDbType.Decimal, "LuongSPXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDVT_SP", SqlDbType.VarChar, "TenDVT_SP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMuc", SqlDbType.Decimal, "DinhMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNPLSuDung", SqlDbType.Decimal, "LuongNPLSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiTaiXuat", SqlDbType.BigInt, "SoToKhaiTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTaiXuat", SqlDbType.DateTime, "NgayTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, "LuongNPLTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonCuoi", SqlDbType.Decimal, "LuongTonCuoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThanhKhoanTiep", SqlDbType.NVarChar, "ThanhKhoanTiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuyenMucDichKhac", SqlDbType.NVarChar, "ChuyenMucDichKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTT", SqlDbType.Float, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGiaTT", SqlDbType.Money, "TyGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueXNK", SqlDbType.Float, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueXNKTon", SqlDbType.Float, "ThueXNKTon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayThucXuat", SqlDbType.DateTime, "NgayThucXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_BCXuatNhapTon Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_BCXuatNhapTon_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_BCXuatNhapTon> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_BCXuatNhapTon> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_BCXuatNhapTon> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_BCXuatNhapTon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_BCXuatNhapTon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_BCXuatNhapTon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_BCXuatNhapTon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_BCXuatNhapTon(long sTT, DateTime tuNgay, DateTime denNgay, long hopDong_ID, string maDoanhNghiep, string maNPL, string tenNPL, string tenDVT_NPL, long soToKhaiNhap, DateTime ngayDangKyNhap, DateTime ngayHoanThanhNhap, string maLoaiHinhNhap, decimal luongNhap, string maSP, string tenSP, long soToKhaiXuat, DateTime ngayDangKyXuat, DateTime ngayHoanThanhXuat, string maLoaiHinhXuat, decimal luongSPXuat, string tenDVT_SP, decimal dinhMuc, decimal luongNPLSuDung, long soToKhaiTaiXuat, DateTime ngayTaiXuat, decimal luongNPLTaiXuat, decimal luongTonCuoi, string thanhKhoanTiep, string chuyenMucDichKhac, double donGiaTT, decimal tyGiaTT, decimal thueSuat, double thueXNK, double thueXNKTon, DateTime ngayThucXuat, int soThuTuHang)
		{
			KDT_GC_BCXuatNhapTon entity = new KDT_GC_BCXuatNhapTon();	
			entity.STT = sTT;
			entity.TuNgay = tuNgay;
			entity.DenNgay = denNgay;
			entity.HopDong_ID = hopDong_ID;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.TenDVT_NPL = tenDVT_NPL;
			entity.SoToKhaiNhap = soToKhaiNhap;
			entity.NgayDangKyNhap = ngayDangKyNhap;
			entity.NgayHoanThanhNhap = ngayHoanThanhNhap;
			entity.MaLoaiHinhNhap = maLoaiHinhNhap;
			entity.LuongNhap = luongNhap;
			entity.MaSP = maSP;
			entity.TenSP = tenSP;
			entity.SoToKhaiXuat = soToKhaiXuat;
			entity.NgayDangKyXuat = ngayDangKyXuat;
			entity.NgayHoanThanhXuat = ngayHoanThanhXuat;
			entity.MaLoaiHinhXuat = maLoaiHinhXuat;
			entity.LuongSPXuat = luongSPXuat;
			entity.TenDVT_SP = tenDVT_SP;
			entity.DinhMuc = dinhMuc;
			entity.LuongNPLSuDung = luongNPLSuDung;
			entity.SoToKhaiTaiXuat = soToKhaiTaiXuat;
			entity.NgayTaiXuat = ngayTaiXuat;
			entity.LuongNPLTaiXuat = luongNPLTaiXuat;
			entity.LuongTonCuoi = luongTonCuoi;
			entity.ThanhKhoanTiep = thanhKhoanTiep;
			entity.ChuyenMucDichKhac = chuyenMucDichKhac;
			entity.DonGiaTT = donGiaTT;
			entity.TyGiaTT = tyGiaTT;
			entity.ThueSuat = thueSuat;
			entity.ThueXNK = thueXNK;
			entity.ThueXNKTon = thueXNKTon;
			entity.NgayThucXuat = ngayThucXuat;
			entity.SoThuTuHang = soThuTuHang;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_BCXuatNhapTon_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, STT);
			db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.Year <= 1753 ? DBNull.Value : (object) TuNgay);
			db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.Year <= 1753 ? DBNull.Value : (object) DenNgay);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, TenDVT_NPL);
			db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.BigInt, SoToKhaiNhap);
			db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, NgayDangKyNhap.Year <= 1753 ? DBNull.Value : (object) NgayDangKyNhap);
			db.AddInParameter(dbCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, NgayHoanThanhNhap.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhNhap);
			db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, MaLoaiHinhNhap);
			db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, LuongNhap);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, TenSP);
			db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.BigInt, SoToKhaiXuat);
			db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, NgayDangKyXuat.Year <= 1753 ? DBNull.Value : (object) NgayDangKyXuat);
			db.AddInParameter(dbCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, NgayHoanThanhXuat.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhXuat);
			db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, MaLoaiHinhXuat);
			db.AddInParameter(dbCommand, "@LuongSPXuat", SqlDbType.Decimal, LuongSPXuat);
			db.AddInParameter(dbCommand, "@TenDVT_SP", SqlDbType.VarChar, TenDVT_SP);
			db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, DinhMuc);
			db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, LuongNPLSuDung);
			db.AddInParameter(dbCommand, "@SoToKhaiTaiXuat", SqlDbType.BigInt, SoToKhaiTaiXuat);
			db.AddInParameter(dbCommand, "@NgayTaiXuat", SqlDbType.DateTime, NgayTaiXuat.Year <= 1753 ? DBNull.Value : (object) NgayTaiXuat);
			db.AddInParameter(dbCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, LuongNPLTaiXuat);
			db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Decimal, LuongTonCuoi);
			db.AddInParameter(dbCommand, "@ThanhKhoanTiep", SqlDbType.NVarChar, ThanhKhoanTiep);
			db.AddInParameter(dbCommand, "@ChuyenMucDichKhac", SqlDbType.NVarChar, ChuyenMucDichKhac);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Float, DonGiaTT);
			db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Money, TyGiaTT);
			db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, ThueXNKTon);
			db.AddInParameter(dbCommand, "@NgayThucXuat", SqlDbType.DateTime, NgayThucXuat.Year <= 1753 ? DBNull.Value : (object) NgayThucXuat);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_BCXuatNhapTon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_BCXuatNhapTon item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_BCXuatNhapTon(long id, long sTT, DateTime tuNgay, DateTime denNgay, long hopDong_ID, string maDoanhNghiep, string maNPL, string tenNPL, string tenDVT_NPL, long soToKhaiNhap, DateTime ngayDangKyNhap, DateTime ngayHoanThanhNhap, string maLoaiHinhNhap, decimal luongNhap, string maSP, string tenSP, long soToKhaiXuat, DateTime ngayDangKyXuat, DateTime ngayHoanThanhXuat, string maLoaiHinhXuat, decimal luongSPXuat, string tenDVT_SP, decimal dinhMuc, decimal luongNPLSuDung, long soToKhaiTaiXuat, DateTime ngayTaiXuat, decimal luongNPLTaiXuat, decimal luongTonCuoi, string thanhKhoanTiep, string chuyenMucDichKhac, double donGiaTT, decimal tyGiaTT, decimal thueSuat, double thueXNK, double thueXNKTon, DateTime ngayThucXuat, int soThuTuHang)
		{
			KDT_GC_BCXuatNhapTon entity = new KDT_GC_BCXuatNhapTon();			
			entity.ID = id;
			entity.STT = sTT;
			entity.TuNgay = tuNgay;
			entity.DenNgay = denNgay;
			entity.HopDong_ID = hopDong_ID;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.TenDVT_NPL = tenDVT_NPL;
			entity.SoToKhaiNhap = soToKhaiNhap;
			entity.NgayDangKyNhap = ngayDangKyNhap;
			entity.NgayHoanThanhNhap = ngayHoanThanhNhap;
			entity.MaLoaiHinhNhap = maLoaiHinhNhap;
			entity.LuongNhap = luongNhap;
			entity.MaSP = maSP;
			entity.TenSP = tenSP;
			entity.SoToKhaiXuat = soToKhaiXuat;
			entity.NgayDangKyXuat = ngayDangKyXuat;
			entity.NgayHoanThanhXuat = ngayHoanThanhXuat;
			entity.MaLoaiHinhXuat = maLoaiHinhXuat;
			entity.LuongSPXuat = luongSPXuat;
			entity.TenDVT_SP = tenDVT_SP;
			entity.DinhMuc = dinhMuc;
			entity.LuongNPLSuDung = luongNPLSuDung;
			entity.SoToKhaiTaiXuat = soToKhaiTaiXuat;
			entity.NgayTaiXuat = ngayTaiXuat;
			entity.LuongNPLTaiXuat = luongNPLTaiXuat;
			entity.LuongTonCuoi = luongTonCuoi;
			entity.ThanhKhoanTiep = thanhKhoanTiep;
			entity.ChuyenMucDichKhac = chuyenMucDichKhac;
			entity.DonGiaTT = donGiaTT;
			entity.TyGiaTT = tyGiaTT;
			entity.ThueSuat = thueSuat;
			entity.ThueXNK = thueXNK;
			entity.ThueXNKTon = thueXNKTon;
			entity.NgayThucXuat = ngayThucXuat;
			entity.SoThuTuHang = soThuTuHang;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_BCXuatNhapTon_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, STT);
			db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.Year <= 1753 ? DBNull.Value : (object) TuNgay);
			db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.Year <= 1753 ? DBNull.Value : (object) DenNgay);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, TenDVT_NPL);
			db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.BigInt, SoToKhaiNhap);
			db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, NgayDangKyNhap.Year <= 1753 ? DBNull.Value : (object) NgayDangKyNhap);
			db.AddInParameter(dbCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, NgayHoanThanhNhap.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhNhap);
			db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, MaLoaiHinhNhap);
			db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, LuongNhap);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, TenSP);
			db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.BigInt, SoToKhaiXuat);
			db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, NgayDangKyXuat.Year <= 1753 ? DBNull.Value : (object) NgayDangKyXuat);
			db.AddInParameter(dbCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, NgayHoanThanhXuat.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhXuat);
			db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, MaLoaiHinhXuat);
			db.AddInParameter(dbCommand, "@LuongSPXuat", SqlDbType.Decimal, LuongSPXuat);
			db.AddInParameter(dbCommand, "@TenDVT_SP", SqlDbType.VarChar, TenDVT_SP);
			db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, DinhMuc);
			db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, LuongNPLSuDung);
			db.AddInParameter(dbCommand, "@SoToKhaiTaiXuat", SqlDbType.BigInt, SoToKhaiTaiXuat);
			db.AddInParameter(dbCommand, "@NgayTaiXuat", SqlDbType.DateTime, NgayTaiXuat.Year <= 1753 ? DBNull.Value : (object) NgayTaiXuat);
			db.AddInParameter(dbCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, LuongNPLTaiXuat);
			db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Decimal, LuongTonCuoi);
			db.AddInParameter(dbCommand, "@ThanhKhoanTiep", SqlDbType.NVarChar, ThanhKhoanTiep);
			db.AddInParameter(dbCommand, "@ChuyenMucDichKhac", SqlDbType.NVarChar, ChuyenMucDichKhac);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Float, DonGiaTT);
			db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Money, TyGiaTT);
			db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, ThueXNKTon);
			db.AddInParameter(dbCommand, "@NgayThucXuat", SqlDbType.DateTime, NgayThucXuat.Year <= 1753 ? DBNull.Value : (object) NgayThucXuat);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_BCXuatNhapTon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_BCXuatNhapTon item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_BCXuatNhapTon(long id, long sTT, DateTime tuNgay, DateTime denNgay, long hopDong_ID, string maDoanhNghiep, string maNPL, string tenNPL, string tenDVT_NPL, long soToKhaiNhap, DateTime ngayDangKyNhap, DateTime ngayHoanThanhNhap, string maLoaiHinhNhap, decimal luongNhap, string maSP, string tenSP, long soToKhaiXuat, DateTime ngayDangKyXuat, DateTime ngayHoanThanhXuat, string maLoaiHinhXuat, decimal luongSPXuat, string tenDVT_SP, decimal dinhMuc, decimal luongNPLSuDung, long soToKhaiTaiXuat, DateTime ngayTaiXuat, decimal luongNPLTaiXuat, decimal luongTonCuoi, string thanhKhoanTiep, string chuyenMucDichKhac, double donGiaTT, decimal tyGiaTT, decimal thueSuat, double thueXNK, double thueXNKTon, DateTime ngayThucXuat, int soThuTuHang)
		{
			KDT_GC_BCXuatNhapTon entity = new KDT_GC_BCXuatNhapTon();			
			entity.ID = id;
			entity.STT = sTT;
			entity.TuNgay = tuNgay;
			entity.DenNgay = denNgay;
			entity.HopDong_ID = hopDong_ID;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.TenDVT_NPL = tenDVT_NPL;
			entity.SoToKhaiNhap = soToKhaiNhap;
			entity.NgayDangKyNhap = ngayDangKyNhap;
			entity.NgayHoanThanhNhap = ngayHoanThanhNhap;
			entity.MaLoaiHinhNhap = maLoaiHinhNhap;
			entity.LuongNhap = luongNhap;
			entity.MaSP = maSP;
			entity.TenSP = tenSP;
			entity.SoToKhaiXuat = soToKhaiXuat;
			entity.NgayDangKyXuat = ngayDangKyXuat;
			entity.NgayHoanThanhXuat = ngayHoanThanhXuat;
			entity.MaLoaiHinhXuat = maLoaiHinhXuat;
			entity.LuongSPXuat = luongSPXuat;
			entity.TenDVT_SP = tenDVT_SP;
			entity.DinhMuc = dinhMuc;
			entity.LuongNPLSuDung = luongNPLSuDung;
			entity.SoToKhaiTaiXuat = soToKhaiTaiXuat;
			entity.NgayTaiXuat = ngayTaiXuat;
			entity.LuongNPLTaiXuat = luongNPLTaiXuat;
			entity.LuongTonCuoi = luongTonCuoi;
			entity.ThanhKhoanTiep = thanhKhoanTiep;
			entity.ChuyenMucDichKhac = chuyenMucDichKhac;
			entity.DonGiaTT = donGiaTT;
			entity.TyGiaTT = tyGiaTT;
			entity.ThueSuat = thueSuat;
			entity.ThueXNK = thueXNK;
			entity.ThueXNKTon = thueXNKTon;
			entity.NgayThucXuat = ngayThucXuat;
			entity.SoThuTuHang = soThuTuHang;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_BCXuatNhapTon_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, STT);
			db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.Year <= 1753 ? DBNull.Value : (object) TuNgay);
			db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.Year <= 1753 ? DBNull.Value : (object) DenNgay);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, TenDVT_NPL);
			db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.BigInt, SoToKhaiNhap);
			db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, NgayDangKyNhap.Year <= 1753 ? DBNull.Value : (object) NgayDangKyNhap);
			db.AddInParameter(dbCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, NgayHoanThanhNhap.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhNhap);
			db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, MaLoaiHinhNhap);
			db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, LuongNhap);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, TenSP);
			db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.BigInt, SoToKhaiXuat);
			db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, NgayDangKyXuat.Year <= 1753 ? DBNull.Value : (object) NgayDangKyXuat);
			db.AddInParameter(dbCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, NgayHoanThanhXuat.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhXuat);
			db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, MaLoaiHinhXuat);
			db.AddInParameter(dbCommand, "@LuongSPXuat", SqlDbType.Decimal, LuongSPXuat);
			db.AddInParameter(dbCommand, "@TenDVT_SP", SqlDbType.VarChar, TenDVT_SP);
			db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, DinhMuc);
			db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, LuongNPLSuDung);
			db.AddInParameter(dbCommand, "@SoToKhaiTaiXuat", SqlDbType.BigInt, SoToKhaiTaiXuat);
			db.AddInParameter(dbCommand, "@NgayTaiXuat", SqlDbType.DateTime, NgayTaiXuat.Year <= 1753 ? DBNull.Value : (object) NgayTaiXuat);
			db.AddInParameter(dbCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, LuongNPLTaiXuat);
			db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Decimal, LuongTonCuoi);
			db.AddInParameter(dbCommand, "@ThanhKhoanTiep", SqlDbType.NVarChar, ThanhKhoanTiep);
			db.AddInParameter(dbCommand, "@ChuyenMucDichKhac", SqlDbType.NVarChar, ChuyenMucDichKhac);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Float, DonGiaTT);
			db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Money, TyGiaTT);
			db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, ThueXNKTon);
			db.AddInParameter(dbCommand, "@NgayThucXuat", SqlDbType.DateTime, NgayThucXuat.Year <= 1753 ? DBNull.Value : (object) NgayThucXuat);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_BCXuatNhapTon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_BCXuatNhapTon item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_BCXuatNhapTon(long id)
		{
			KDT_GC_BCXuatNhapTon entity = new KDT_GC_BCXuatNhapTon();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_BCXuatNhapTon_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_BCXuatNhapTon_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_BCXuatNhapTon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_BCXuatNhapTon item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
        public int DeleteDynamicTransaction(SqlTransaction transaction, string MaDoanhNghiep, long HopDong_ID)
        {
            string spName = "DELETE FROM t_KDT_GC_BCXuatNhapTon Where MaDoanhNghiep ='" + MaDoanhNghiep + "' AND HopDong_ID =" + HopDong_ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public void InsertTransaction(SqlTransaction transaction, List<KDT_GC_BCXuatNhapTon> collection)
        {
            foreach (KDT_GC_BCXuatNhapTon item in collection)
            {
                item.Insert(transaction);
            }
        }
	}	
}