﻿namespace Company.Interface
{
    partial class TKMD_CacKhoanDieuChinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgListCacKhoanDieuChinhKhac_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TKMD_CacKhoanDieuChinh));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbMaPhanLoaiTKTG = new Janus.Windows.EditControls.UIComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSoDangKyBHTH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtGiaCoSoDieuChinhTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NguyenTe_GiaCoSoDieuChinhTriGia = new Company.Interface.Controls.NguyenTeControl();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListCacKhoanDieuChinhKhac = new Janus.Windows.GridEX.GridEX();
            this.label3 = new System.Windows.Forms.Label();
            this.cbMaPhiVanChuyen = new Janus.Windows.EditControls.UIComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPhiVC = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.NguyenTe_PhiVC = new Company.Interface.Controls.NguyenTeControl();
            this.label6 = new System.Windows.Forms.Label();
            this.cbMaPhanLoaiPhiBaoHiem = new Janus.Windows.EditControls.UIComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.NguyenTe_PhiBH = new Company.Interface.Controls.NguyenTeControl();
            this.label9 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtChiTietKhaiTG = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTongHeSoPB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListCacKhoanDieuChinhKhac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(629, 604);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.NguyenTe_GiaCoSoDieuChinhTriGia);
            this.uiGroupBox1.Controls.Add(this.txtGiaCoSoDieuChinhTriGia);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.cbMaPhanLoaiTKTG);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(629, 117);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông tin tờ khai trị giá";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // cbMaPhanLoaiTKTG
            // 
            this.cbMaPhanLoaiTKTG.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbMaPhanLoaiTKTG.DisplayMember = "ID";
            this.cbMaPhanLoaiTKTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMaPhanLoaiTKTG.Location = new System.Drawing.Point(113, 23);
            this.cbMaPhanLoaiTKTG.Name = "cbMaPhanLoaiTKTG";
            this.cbMaPhanLoaiTKTG.Size = new System.Drawing.Size(506, 21);
            this.cbMaPhanLoaiTKTG.TabIndex = 8;
            this.cbMaPhanLoaiTKTG.ValueMember = "ID";
            this.cbMaPhanLoaiTKTG.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 13);
            this.label14.TabIndex = 7;
            this.label14.Text = "Mã phân loại TKTG";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSoDangKyBHTH
            // 
            this.txtSoDangKyBHTH.DecimalDigits = 0;
            this.txtSoDangKyBHTH.Location = new System.Drawing.Point(113, 75);
            this.txtSoDangKyBHTH.Name = "txtSoDangKyBHTH";
            this.txtSoDangKyBHTH.Size = new System.Drawing.Size(116, 21);
            this.txtSoDangKyBHTH.TabIndex = 10;
            this.txtSoDangKyBHTH.Text = "0";
            this.txtSoDangKyBHTH.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoDangKyBHTH.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(9, 80);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Số đăng ký BHTH";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(280, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Giá cơ sở để điều chỉnh trị giá";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGiaCoSoDieuChinhTriGia
            // 
            this.txtGiaCoSoDieuChinhTriGia.DecimalDigits = 4;
            this.txtGiaCoSoDieuChinhTriGia.Location = new System.Drawing.Point(432, 57);
            this.txtGiaCoSoDieuChinhTriGia.Name = "txtGiaCoSoDieuChinhTriGia";
            this.txtGiaCoSoDieuChinhTriGia.Size = new System.Drawing.Size(187, 21);
            this.txtGiaCoSoDieuChinhTriGia.TabIndex = 10;
            this.txtGiaCoSoDieuChinhTriGia.Text = "0.0000";
            this.txtGiaCoSoDieuChinhTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtGiaCoSoDieuChinhTriGia.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(306, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Mã tiền tệ của giá cơ sở";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NguyenTe_GiaCoSoDieuChinhTriGia
            // 
            this.NguyenTe_GiaCoSoDieuChinhTriGia.BackColor = System.Drawing.Color.Transparent;
            this.NguyenTe_GiaCoSoDieuChinhTriGia.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.NguyenTe_GiaCoSoDieuChinhTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NguyenTe_GiaCoSoDieuChinhTriGia.Location = new System.Drawing.Point(432, 85);
            this.NguyenTe_GiaCoSoDieuChinhTriGia.Ma = "";
            this.NguyenTe_GiaCoSoDieuChinhTriGia.Name = "NguyenTe_GiaCoSoDieuChinhTriGia";
            this.NguyenTe_GiaCoSoDieuChinhTriGia.ReadOnly = false;
            this.NguyenTe_GiaCoSoDieuChinhTriGia.Size = new System.Drawing.Size(199, 22);
            this.NguyenTe_GiaCoSoDieuChinhTriGia.TabIndex = 11;
            this.NguyenTe_GiaCoSoDieuChinhTriGia.VisualStyleManager = null;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.NguyenTe_PhiVC);
            this.uiGroupBox2.Controls.Add(this.cbMaPhiVanChuyen);
            this.uiGroupBox2.Controls.Add(this.txtPhiVC);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 123);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(629, 92);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Phí vận chuyển";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.NguyenTe_PhiBH);
            this.uiGroupBox3.Controls.Add(this.cbMaPhanLoaiPhiBaoHiem);
            this.uiGroupBox3.Controls.Add(this.txtSoDangKyBHTH);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.txtPhiBaoHiem);
            this.uiGroupBox3.Controls.Add(this.label13);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 221);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(629, 106);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.Text = "Phí bảo hiểm";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgListCacKhoanDieuChinhKhac
            // 
            dgListCacKhoanDieuChinhKhac_DesignTimeLayout.LayoutString = resources.GetString("dgListCacKhoanDieuChinhKhac_DesignTimeLayout.LayoutString");
            this.dgListCacKhoanDieuChinhKhac.DesignTimeLayout = dgListCacKhoanDieuChinhKhac_DesignTimeLayout;
            this.dgListCacKhoanDieuChinhKhac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListCacKhoanDieuChinhKhac.GroupByBoxVisible = false;
            this.dgListCacKhoanDieuChinhKhac.Location = new System.Drawing.Point(3, 17);
            this.dgListCacKhoanDieuChinhKhac.Name = "dgListCacKhoanDieuChinhKhac";
            this.dgListCacKhoanDieuChinhKhac.Size = new System.Drawing.Size(623, 137);
            this.dgListCacKhoanDieuChinhKhac.TabIndex = 2;
            this.dgListCacKhoanDieuChinhKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListCacKhoanDieuChinhKhac.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mã phân loại phí VC";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbMaPhiVanChuyen
            // 
            this.cbMaPhiVanChuyen.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbMaPhiVanChuyen.DisplayMember = "ID";
            this.cbMaPhiVanChuyen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMaPhiVanChuyen.Location = new System.Drawing.Point(113, 20);
            this.cbMaPhiVanChuyen.Name = "cbMaPhiVanChuyen";
            this.cbMaPhiVanChuyen.Size = new System.Drawing.Size(506, 21);
            this.cbMaPhiVanChuyen.TabIndex = 8;
            this.cbMaPhiVanChuyen.ValueMember = "ID";
            this.cbMaPhiVanChuyen.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Mã tiền tệ của phí";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(346, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Phí vận chuyển";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPhiVC
            // 
            this.txtPhiVC.DecimalDigits = 4;
            this.txtPhiVC.Location = new System.Drawing.Point(432, 52);
            this.txtPhiVC.Name = "txtPhiVC";
            this.txtPhiVC.Size = new System.Drawing.Size(187, 21);
            this.txtPhiVC.TabIndex = 10;
            this.txtPhiVC.Text = "0.0000";
            this.txtPhiVC.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtPhiVC.VisualStyleManager = this.vsmMain;
            // 
            // NguyenTe_PhiVC
            // 
            this.NguyenTe_PhiVC.BackColor = System.Drawing.Color.Transparent;
            this.NguyenTe_PhiVC.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.NguyenTe_PhiVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NguyenTe_PhiVC.Location = new System.Drawing.Point(113, 52);
            this.NguyenTe_PhiVC.Ma = "";
            this.NguyenTe_PhiVC.Name = "NguyenTe_PhiVC";
            this.NguyenTe_PhiVC.ReadOnly = false;
            this.NguyenTe_PhiVC.Size = new System.Drawing.Size(199, 22);
            this.NguyenTe_PhiVC.TabIndex = 11;
            this.NguyenTe_PhiVC.VisualStyleManager = null;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Mã phân loại phí BH";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbMaPhanLoaiPhiBaoHiem
            // 
            this.cbMaPhanLoaiPhiBaoHiem.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbMaPhanLoaiPhiBaoHiem.DisplayMember = "ID";
            this.cbMaPhanLoaiPhiBaoHiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMaPhanLoaiPhiBaoHiem.Location = new System.Drawing.Point(113, 20);
            this.cbMaPhanLoaiPhiBaoHiem.Name = "cbMaPhanLoaiPhiBaoHiem";
            this.cbMaPhanLoaiPhiBaoHiem.Size = new System.Drawing.Size(503, 21);
            this.cbMaPhanLoaiPhiBaoHiem.TabIndex = 8;
            this.cbMaPhanLoaiPhiBaoHiem.ValueMember = "ID";
            this.cbMaPhanLoaiPhiBaoHiem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Mã tiền tệ của phí";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(343, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Phí bảo hiểm";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.DecimalDigits = 4;
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(429, 47);
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(187, 21);
            this.txtPhiBaoHiem.TabIndex = 10;
            this.txtPhiBaoHiem.Text = "0.0000";
            this.txtPhiBaoHiem.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            // 
            // NguyenTe_PhiBH
            // 
            this.NguyenTe_PhiBH.BackColor = System.Drawing.Color.Transparent;
            this.NguyenTe_PhiBH.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.NguyenTe_PhiBH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NguyenTe_PhiBH.Location = new System.Drawing.Point(113, 47);
            this.NguyenTe_PhiBH.Ma = "";
            this.NguyenTe_PhiBH.Name = "NguyenTe_PhiBH";
            this.NguyenTe_PhiBH.ReadOnly = false;
            this.NguyenTe_PhiBH.Size = new System.Drawing.Size(196, 22);
            this.NguyenTe_PhiBH.TabIndex = 11;
            this.NguyenTe_PhiBH.VisualStyleManager = null;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(235, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "(Nếu có)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.dgListCacKhoanDieuChinhKhac);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 447);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(629, 157);
            this.uiGroupBox4.TabIndex = 3;
            this.uiGroupBox4.Text = "Các khoản điều chỉnh khác";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.txtChiTietKhaiTG);
            this.uiGroupBox5.Controls.Add(this.label10);
            this.uiGroupBox5.Controls.Add(this.txtTongHeSoPB);
            this.uiGroupBox5.Controls.Add(this.label11);
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 333);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(629, 106);
            this.uiGroupBox5.TabIndex = 4;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Chi tiết khai trị giá";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtChiTietKhaiTG
            // 
            this.txtChiTietKhaiTG.Location = new System.Drawing.Point(113, 11);
            this.txtChiTietKhaiTG.Multiline = true;
            this.txtChiTietKhaiTG.Name = "txtChiTietKhaiTG";
            this.txtChiTietKhaiTG.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtChiTietKhaiTG.Size = new System.Drawing.Size(492, 59);
            this.txtChiTietKhaiTG.TabIndex = 8;
            this.txtChiTietKhaiTG.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(9, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Tổng hệ số phân bổ";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTongHeSoPB
            // 
            this.txtTongHeSoPB.DecimalDigits = 4;
            this.txtTongHeSoPB.Location = new System.Drawing.Point(113, 76);
            this.txtTongHeSoPB.Name = "txtTongHeSoPB";
            this.txtTongHeSoPB.Size = new System.Drawing.Size(196, 21);
            this.txtTongHeSoPB.TabIndex = 10;
            this.txtTongHeSoPB.Text = "0.0000";
            this.txtTongHeSoPB.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTongHeSoPB.VisualStyleManager = this.vsmMain;
            // 
            // TKMD_CacKhoanDieuChinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 604);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "TKMD_CacKhoanDieuChinh";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Các khoản điều chỉnh giá";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListCacKhoanDieuChinhKhac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIComboBox cbMaPhanLoaiTKTG;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoDangKyBHTH;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtGiaCoSoDieuChinhTriGia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Company.Interface.Controls.NguyenTeControl NguyenTe_GiaCoSoDieuChinhTriGia;
        private Janus.Windows.GridEX.GridEX dgListCacKhoanDieuChinhKhac;
        private Janus.Windows.EditControls.UIComboBox cbMaPhiVanChuyen;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIComboBox cbMaPhanLoaiPhiBaoHiem;
        private System.Windows.Forms.Label label6;
        private Company.Interface.Controls.NguyenTeControl NguyenTe_PhiVC;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiVC;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Company.Interface.Controls.NguyenTeControl NguyenTe_PhiBH;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiBaoHiem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtChiTietKhaiTG;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongHeSoPB;
        private System.Windows.Forms.Label label11;
    }
}