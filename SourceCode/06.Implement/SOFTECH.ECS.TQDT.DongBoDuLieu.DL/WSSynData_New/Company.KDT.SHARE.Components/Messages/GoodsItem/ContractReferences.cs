﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
   public class ContractReferences
    {
        /// <summary>
        /// Thông tin các hợp đồng
        /// </summary>
       [XmlElement("ContractReference")]
       public List<ContractReference> ContractReference { get; set; }

    }
}
