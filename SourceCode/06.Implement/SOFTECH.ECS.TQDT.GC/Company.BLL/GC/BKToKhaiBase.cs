using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.GC
{
    public partial class BKToKhai : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected long _SoTK;
        protected DateTime _NgayTK = new DateTime(1900, 01, 01);
        protected DateTime _Ngay_DK = new DateTime(1900, 01, 01);
        protected string _DVHQ = String.Empty;
        protected string _CKXN = String.Empty;
        protected string _DVT = String.Empty;
        protected decimal _Luong;
        protected string _CanBoDuyet = String.Empty;
        protected string _GhiChu = String.Empty;
        protected string _VT = String.Empty;
        protected string _MaLoaiHinh = String.Empty;
        protected long _IDHopDong;
        protected int _STT;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public long SoTK
        {
            set { this._SoTK = value; }
            get { return this._SoTK; }
        }
        public DateTime NgayTK
        {
            set { this._NgayTK = value; }
            get { return this._NgayTK; }
        }
        public DateTime Ngay_DK
        {
            set { this._Ngay_DK = value; }
            get { return this._Ngay_DK; }
        }
        public string DVHQ
        {
            set { this._DVHQ = value; }
            get { return this._DVHQ; }
        }
        public string CKXN
        {
            set { this._CKXN = value; }
            get { return this._CKXN; }
        }
        public string DVT
        {
            set { this._DVT = value; }
            get { return this._DVT; }
        }
        public decimal Luong
        {
            set { this._Luong = value; }
            get { return this._Luong; }
        }
        public string CanBoDuyet
        {
            set { this._CanBoDuyet = value; }
            get { return this._CanBoDuyet; }
        }
        public string GhiChu
        {
            set { this._GhiChu = value; }
            get { return this._GhiChu; }
        }
        public string VT
        {
            set { this._VT = value; }
            get { return this._VT; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public long IDHopDong
        {
            set { this._IDHopDong = value; }
            get { return this._IDHopDong; }
        }
        public int STT
        {
            set { this._STT = value; }
            get { return this._STT; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_GC_BKToKhai_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTK"))) this._SoTK = reader.GetInt64(reader.GetOrdinal("SoTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTK"))) this._NgayTK = reader.GetDateTime(reader.GetOrdinal("NgayTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_DK"))) this._Ngay_DK = reader.GetDateTime(reader.GetOrdinal("Ngay_DK"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVHQ"))) this._DVHQ = reader.GetString(reader.GetOrdinal("DVHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("CKXN"))) this._CKXN = reader.GetString(reader.GetOrdinal("CKXN"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) this._DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) this._Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) this._CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("VT"))) this._VT = reader.GetString(reader.GetOrdinal("VT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) this._IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) this._STT = reader.GetInt32(reader.GetOrdinal("STT"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_GC_BKToKhai_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_GC_BKToKhai_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_BKToKhai_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_BKToKhai_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public BKToKhaiCollection SelectCollectionAll()
        {
            BKToKhaiCollection collection = new BKToKhaiCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                BKToKhai entity = new BKToKhai();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTK"))) entity.SoTK = reader.GetInt64(reader.GetOrdinal("SoTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTK"))) entity.NgayTK = reader.GetDateTime(reader.GetOrdinal("NgayTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_DK"))) entity.Ngay_DK = reader.GetDateTime(reader.GetOrdinal("Ngay_DK"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVHQ"))) entity.DVHQ = reader.GetString(reader.GetOrdinal("DVHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("CKXN"))) entity.CKXN = reader.GetString(reader.GetOrdinal("CKXN"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) entity.CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("VT"))) entity.VT = reader.GetString(reader.GetOrdinal("VT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public BKToKhaiCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            BKToKhaiCollection collection = new BKToKhaiCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                BKToKhai entity = new BKToKhai();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTK"))) entity.SoTK = reader.GetInt64(reader.GetOrdinal("SoTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTK"))) entity.NgayTK = reader.GetDateTime(reader.GetOrdinal("NgayTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_DK"))) entity.Ngay_DK = reader.GetDateTime(reader.GetOrdinal("Ngay_DK"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVHQ"))) entity.DVHQ = reader.GetString(reader.GetOrdinal("DVHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("CKXN"))) entity.CKXN = reader.GetString(reader.GetOrdinal("CKXN"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) entity.CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("VT"))) entity.VT = reader.GetString(reader.GetOrdinal("VT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKToKhai_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, this._SoTK);
            this.db.AddInParameter(dbCommand, "@NgayTK", SqlDbType.DateTime, this._NgayTK);
            this.db.AddInParameter(dbCommand, "@Ngay_DK", SqlDbType.DateTime, this._Ngay_DK);
            this.db.AddInParameter(dbCommand, "@DVHQ", SqlDbType.NChar, this._DVHQ);
            this.db.AddInParameter(dbCommand, "@CKXN", SqlDbType.NVarChar, this._CKXN);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.NVarChar, this._CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@VT", SqlDbType.NVarChar, this._VT);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(BKToKhaiCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BKToKhai item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, BKToKhaiCollection collection)
        {
            foreach (BKToKhai item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKToKhai_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, this._SoTK);
            this.db.AddInParameter(dbCommand, "@NgayTK", SqlDbType.DateTime, this._NgayTK);
            this.db.AddInParameter(dbCommand, "@Ngay_DK", SqlDbType.DateTime, this._Ngay_DK);
            this.db.AddInParameter(dbCommand, "@DVHQ", SqlDbType.NChar, this._DVHQ);
            this.db.AddInParameter(dbCommand, "@CKXN", SqlDbType.NVarChar, this._CKXN);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.NVarChar, this._CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@VT", SqlDbType.NVarChar, this._VT);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(BKToKhaiCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BKToKhai item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKToKhai_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, this._SoTK);
            this.db.AddInParameter(dbCommand, "@NgayTK", SqlDbType.DateTime, this._NgayTK);
            this.db.AddInParameter(dbCommand, "@Ngay_DK", SqlDbType.DateTime, this._Ngay_DK);
            this.db.AddInParameter(dbCommand, "@DVHQ", SqlDbType.NChar, this._DVHQ);
            this.db.AddInParameter(dbCommand, "@CKXN", SqlDbType.NVarChar, this._CKXN);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.NVarChar, this._CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@VT", SqlDbType.NVarChar, this._VT);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(BKToKhaiCollection collection, SqlTransaction transaction)
        {
            foreach (BKToKhai item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKToKhai_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(BKToKhaiCollection collection, SqlTransaction transaction)
        {
            foreach (BKToKhai item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(BKToKhaiCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BKToKhai item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion
    }
}