SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[t_VNACCS_CapSoToKhai]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_VNACCS_CapSoToKhai]'
GO
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[t_VNACCS_CapSoToKhai] ALTER COLUMN [SoTKVNACCS] [decimal] (18, 0) NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_VNACCS_CapSoToKhai_Insert]'
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE Name = N'SoTKGiay' AND Object_ID = Object_ID(N't_VNACCS_CapSoToKhai'))
BEGIN
	ALTER TABLE dbo.t_VNACCS_CapSoToKhai ADD
	[SoTKGiay] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
END
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Insert]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Insert]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Insert]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Insert]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(18, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255),
	@SoTkGiay varchar(12)
AS
INSERT INTO [dbo].[t_VNACCS_CapSoToKhai]
(
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1],
	[SoTKGiay]
)
VALUES
(
	@SoTK,
	@MaLoaiHinh,
	@NamDangKy,
	@SoTKVNACCS,
	@SoTKVNACCSFull,
	@SoTKDauTien,
	@SoNhanhTK,
	@TongSoTKChiaNho,
	@SoTKTNTX,
	@Temp1,
	@SoTkGiay
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_VNACCS_CapSoToKhai_Update]'
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Update]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Update]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Update]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Update]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(18, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255),
	@SoTKGiay varchar(12)
AS

UPDATE
	[dbo].[t_VNACCS_CapSoToKhai]
SET
	[NamDangKy] = @NamDangKy,
	[SoTKVNACCS] = @SoTKVNACCS,
	[SoTKVNACCSFull] = @SoTKVNACCSFull,
	[SoTKDauTien] = @SoTKDauTien,
	[SoNhanhTK] = @SoNhanhTK,
	[TongSoTKChiaNho] = @TongSoTKChiaNho,
	[SoTKTNTX] = @SoTKTNTX,
	[Temp1] = @Temp1,
	[SoTKGiay] = @SoTKGiay
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]'
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(18, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255),
	@SoTKGiay varchar(12)
AS
IF EXISTS(SELECT [SoTK], [MaLoaiHinh] FROM [dbo].[t_VNACCS_CapSoToKhai] WHERE [SoTK] = @SoTK AND [MaLoaiHinh] = @MaLoaiHinh)
	BEGIN
		UPDATE
			[dbo].[t_VNACCS_CapSoToKhai] 
		SET
			[NamDangKy] = @NamDangKy,
			[SoTKVNACCS] = @SoTKVNACCS,
			[SoTKVNACCSFull] = @SoTKVNACCSFull,
			[SoTKDauTien] = @SoTKDauTien,
			[SoNhanhTK] = @SoNhanhTK,
			[TongSoTKChiaNho] = @TongSoTKChiaNho,
			[SoTKTNTX] = @SoTKTNTX,
			[Temp1] = @Temp1,
			[SoTKGiay] = @SoTKGiay
		WHERE
			[SoTK] = @SoTK
			AND [MaLoaiHinh] = @MaLoaiHinh
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACCS_CapSoToKhai]
	(
			[SoTK],
			[MaLoaiHinh],
			[NamDangKy],
			[SoTKVNACCS],
			[SoTKVNACCSFull],
			[SoTKDauTien],
			[SoNhanhTK],
			[TongSoTKChiaNho],
			[SoTKTNTX],
			[Temp1],
			[SoTKGiay]
	)
	VALUES
	(
			@SoTK,
			@MaLoaiHinh,
			@NamDangKy,
			@SoTKVNACCS,
			@SoTKVNACCSFull,
			@SoTKDauTien,
			@SoNhanhTK,
			@TongSoTKChiaNho,
			@SoTKTNTX,
			@Temp1,
			@SoTKGiay
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_VNACCS_CapSoToKhai_Load]'
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Load]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Load]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Load]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Load]
	@SoTK int,
	@MaLoaiHinh varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1],
	[SoTKGiay]
FROM
	[dbo].[t_VNACCS_CapSoToKhai]
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_VNACCS_CapSoToKhai_SelectAll]'
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_SelectAll]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectAll]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1],
	[SoTKGiay]
FROM
	[dbo].[t_VNACCS_CapSoToKhai]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]'
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1],
	[SoTKGiay]
FROM [dbo].[t_VNACCS_CapSoToKhai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1],
	[SoTKGiay]
FROM [dbo].[t_VNACCS_CapSoToKhai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[t_VNACCS_CapSoToKhai]'
GO
GO
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO


GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '21.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('21.7',GETDATE(), N'Fix lỗi. Dropping constraints')
END	