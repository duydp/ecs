
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_GC_GiamSatTieuHuyBoSung]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_GC_GiamSatTieuHuyBoSung](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GSTH_ID] [bigint] NOT NULL,
	[SoThamChieuPLHD] varchar(50) NULL,
	[SoDangKyPLHD] varchar(50) NULL,
	[NgayDangKyPLHD] [datetime] NULL,
	[GhiChu] [nvarchar](150) NULL,
 CONSTRAINT [PK_t_KDT_GC_GiamSatTieuHuyBoSung] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectBy_GSTH_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectBy_GSTH_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_DeleteBy_GSTH_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_DeleteBy_GSTH_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 14, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Insert]
	@GSTH_ID bigint,
	@SoThamChieuPLHD varchar(50),
	@SoDangKyPLHD varchar(50),
	@NgayDangKyPLHD datetime,
	@GhiChu nvarchar(150),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_GiamSatTieuHuyBoSung]
(
	[GSTH_ID],
	[SoThamChieuPLHD],
	[SoDangKyPLHD],
	[NgayDangKyPLHD],
	[GhiChu]
)
VALUES 
(
	@GSTH_ID,
	@SoThamChieuPLHD,
	@SoDangKyPLHD,
	@NgayDangKyPLHD,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 14, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Update]
	@ID bigint,
	@GSTH_ID bigint,
	@SoThamChieuPLHD varchar(50),
	@SoDangKyPLHD varchar(50),
	@NgayDangKyPLHD datetime,
	@GhiChu nvarchar(150)
AS

UPDATE
	[dbo].[t_KDT_GC_GiamSatTieuHuyBoSung]
SET
	[GSTH_ID] = @GSTH_ID,
	[SoThamChieuPLHD] = @SoThamChieuPLHD,
	[SoDangKyPLHD] = @SoDangKyPLHD,
	[NgayDangKyPLHD] = @NgayDangKyPLHD,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 14, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_InsertUpdate]
	@ID bigint,
	@GSTH_ID bigint,
	@SoThamChieuPLHD varchar(50),
	@SoDangKyPLHD varchar(50),
	@NgayDangKyPLHD datetime,
	@GhiChu nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_GiamSatTieuHuyBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_GiamSatTieuHuyBoSung] 
		SET
			[GSTH_ID] = @GSTH_ID,
			[SoThamChieuPLHD] = @SoThamChieuPLHD,
			[SoDangKyPLHD] = @SoDangKyPLHD,
			[NgayDangKyPLHD] = @NgayDangKyPLHD,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_GiamSatTieuHuyBoSung]
		(
			[GSTH_ID],
			[SoThamChieuPLHD],
			[SoDangKyPLHD],
			[NgayDangKyPLHD],
			[GhiChu]
		)
		VALUES 
		(
			@GSTH_ID,
			@SoThamChieuPLHD,
			@SoDangKyPLHD,
			@NgayDangKyPLHD,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 14, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_GiamSatTieuHuyBoSung]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_DeleteBy_GSTH_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 14, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_DeleteBy_GSTH_ID]
	@GSTH_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_GiamSatTieuHuyBoSung]
WHERE
	[GSTH_ID] = @GSTH_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 14, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_GiamSatTieuHuyBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 14, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GSTH_ID],
	[SoThamChieuPLHD],
	[SoDangKyPLHD],
	[NgayDangKyPLHD],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_GiamSatTieuHuyBoSung]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectBy_GSTH_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 14, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectBy_GSTH_ID]
	@GSTH_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GSTH_ID],
	[SoThamChieuPLHD],
	[SoDangKyPLHD],
	[NgayDangKyPLHD],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_GiamSatTieuHuyBoSung]
WHERE
	[GSTH_ID] = @GSTH_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 14, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GSTH_ID],
	[SoThamChieuPLHD],
	[SoDangKyPLHD],
	[NgayDangKyPLHD],
	[GhiChu]
FROM [dbo].[t_KDT_GC_GiamSatTieuHuyBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 14, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GSTH_ID],
	[SoThamChieuPLHD],
	[SoDangKyPLHD],
	[NgayDangKyPLHD],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_GiamSatTieuHuyBoSung]	

GO


UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '8.3'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO

