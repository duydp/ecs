﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.WareHouse
{
	public partial class T_KHOKETOAN_DINHMUC : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MASP { set; get; }
		public string TENSP { set; get; }
		public string DVTSP { set; get; }
		public string MANPL { set; get; }
		public string TENNPL { set; get; }
		public string DVTNPL { set; get; }
		public decimal DMSD { set; get; }
		public decimal TLHH { set; get; }
		public decimal DMCHUNG { set; get; }
		public string GHICHU { set; get; }
		public long HOPDONG_ID { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KHOKETOAN_DINHMUC> ConvertToCollection(IDataReader reader)
		{
			List<T_KHOKETOAN_DINHMUC> collection = new List<T_KHOKETOAN_DINHMUC>();
			while (reader.Read())
			{
				T_KHOKETOAN_DINHMUC entity = new T_KHOKETOAN_DINHMUC();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MASP"))) entity.MASP = reader.GetString(reader.GetOrdinal("MASP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENSP"))) entity.TENSP = reader.GetString(reader.GetOrdinal("TENSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTSP"))) entity.DVTSP = reader.GetString(reader.GetOrdinal("DVTSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("MANPL"))) entity.MANPL = reader.GetString(reader.GetOrdinal("MANPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENNPL"))) entity.TENNPL = reader.GetString(reader.GetOrdinal("TENNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTNPL"))) entity.DVTNPL = reader.GetString(reader.GetOrdinal("DVTNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("DMSD"))) entity.DMSD = reader.GetDecimal(reader.GetOrdinal("DMSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("TLHH"))) entity.TLHH = reader.GetDecimal(reader.GetOrdinal("TLHH"));
				if (!reader.IsDBNull(reader.GetOrdinal("DMCHUNG"))) entity.DMCHUNG = reader.GetDecimal(reader.GetOrdinal("DMCHUNG"));
				if (!reader.IsDBNull(reader.GetOrdinal("GHICHU"))) entity.GHICHU = reader.GetString(reader.GetOrdinal("GHICHU"));
				if (!reader.IsDBNull(reader.GetOrdinal("HOPDONG_ID"))) entity.HOPDONG_ID = reader.GetInt64(reader.GetOrdinal("HOPDONG_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KHOKETOAN_DINHMUC> collection, long id)
        {
            foreach (T_KHOKETOAN_DINHMUC item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KHOKETOAN_DINHMUC VALUES(@MASP, @TENSP, @DVTSP, @MANPL, @TENNPL, @DVTNPL, @DMSD, @TLHH, @DMCHUNG, @GHICHU, @HOPDONG_ID)";
            string update = "UPDATE T_KHOKETOAN_DINHMUC SET MASP = @MASP, TENSP = @TENSP, DVTSP = @DVTSP, MANPL = @MANPL, TENNPL = @TENNPL, DVTNPL = @DVTNPL, DMSD = @DMSD, TLHH = @TLHH, DMCHUNG = @DMCHUNG, GHICHU = @GHICHU, HOPDONG_ID = @HOPDONG_ID WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_DINHMUC WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MASP", SqlDbType.NVarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTSP", SqlDbType.NVarChar, "DVTSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTNPL", SqlDbType.NVarChar, "DVTNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DMSD", SqlDbType.Decimal, "DMSD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TLHH", SqlDbType.Decimal, "TLHH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DMCHUNG", SqlDbType.Decimal, "DMCHUNG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MASP", SqlDbType.NVarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTSP", SqlDbType.NVarChar, "DVTSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTNPL", SqlDbType.NVarChar, "DVTNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DMSD", SqlDbType.Decimal, "DMSD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TLHH", SqlDbType.Decimal, "TLHH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DMCHUNG", SqlDbType.Decimal, "DMCHUNG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KHOKETOAN_DINHMUC VALUES(@MASP, @TENSP, @DVTSP, @MANPL, @TENNPL, @DVTNPL, @DMSD, @TLHH, @DMCHUNG, @GHICHU, @HOPDONG_ID)";
            string update = "UPDATE T_KHOKETOAN_DINHMUC SET MASP = @MASP, TENSP = @TENSP, DVTSP = @DVTSP, MANPL = @MANPL, TENNPL = @TENNPL, DVTNPL = @DVTNPL, DMSD = @DMSD, TLHH = @TLHH, DMCHUNG = @DMCHUNG, GHICHU = @GHICHU, HOPDONG_ID = @HOPDONG_ID WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_DINHMUC WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MASP", SqlDbType.NVarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTSP", SqlDbType.NVarChar, "DVTSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTNPL", SqlDbType.NVarChar, "DVTNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DMSD", SqlDbType.Decimal, "DMSD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TLHH", SqlDbType.Decimal, "TLHH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DMCHUNG", SqlDbType.Decimal, "DMCHUNG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MASP", SqlDbType.NVarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTSP", SqlDbType.NVarChar, "DVTSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTNPL", SqlDbType.NVarChar, "DVTNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DMSD", SqlDbType.Decimal, "DMSD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TLHH", SqlDbType.Decimal, "TLHH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DMCHUNG", SqlDbType.Decimal, "DMCHUNG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KHOKETOAN_DINHMUC Load(long id)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_DINHMUC_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KHOKETOAN_DINHMUC> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KHOKETOAN_DINHMUC> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KHOKETOAN_DINHMUC> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_DINHMUC_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_DINHMUC_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_DINHMUC_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_DINHMUC_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KHOKETOAN_DINHMUC(string mASP, string tENSP, string dVTSP, string mANPL, string tENNPL, string dVTNPL, decimal dMSD, decimal tLHH, decimal dMCHUNG, string gHICHU, long hOPDONG_ID)
		{
			T_KHOKETOAN_DINHMUC entity = new T_KHOKETOAN_DINHMUC();	
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVTSP = dVTSP;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVTNPL = dVTNPL;
			entity.DMSD = dMSD;
			entity.TLHH = tLHH;
			entity.DMCHUNG = dMCHUNG;
			entity.GHICHU = gHICHU;
			entity.HOPDONG_ID = hOPDONG_ID;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KHOKETOAN_DINHMUC_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.NVarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVTSP", SqlDbType.NVarChar, DVTSP);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.NVarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVTNPL", SqlDbType.NVarChar, DVTNPL);
			db.AddInParameter(dbCommand, "@DMSD", SqlDbType.Decimal, DMSD);
			db.AddInParameter(dbCommand, "@TLHH", SqlDbType.Decimal, TLHH);
			db.AddInParameter(dbCommand, "@DMCHUNG", SqlDbType.Decimal, DMCHUNG);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KHOKETOAN_DINHMUC> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_DINHMUC item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KHOKETOAN_DINHMUC(long id, string mASP, string tENSP, string dVTSP, string mANPL, string tENNPL, string dVTNPL, decimal dMSD, decimal tLHH, decimal dMCHUNG, string gHICHU, long hOPDONG_ID)
		{
			T_KHOKETOAN_DINHMUC entity = new T_KHOKETOAN_DINHMUC();			
			entity.ID = id;
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVTSP = dVTSP;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVTNPL = dVTNPL;
			entity.DMSD = dMSD;
			entity.TLHH = tLHH;
			entity.DMCHUNG = dMCHUNG;
			entity.GHICHU = gHICHU;
			entity.HOPDONG_ID = hOPDONG_ID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KHOKETOAN_DINHMUC_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.NVarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVTSP", SqlDbType.NVarChar, DVTSP);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.NVarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVTNPL", SqlDbType.NVarChar, DVTNPL);
			db.AddInParameter(dbCommand, "@DMSD", SqlDbType.Decimal, DMSD);
			db.AddInParameter(dbCommand, "@TLHH", SqlDbType.Decimal, TLHH);
			db.AddInParameter(dbCommand, "@DMCHUNG", SqlDbType.Decimal, DMCHUNG);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KHOKETOAN_DINHMUC> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_DINHMUC item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KHOKETOAN_DINHMUC(long id, string mASP, string tENSP, string dVTSP, string mANPL, string tENNPL, string dVTNPL, decimal dMSD, decimal tLHH, decimal dMCHUNG, string gHICHU, long hOPDONG_ID)
		{
			T_KHOKETOAN_DINHMUC entity = new T_KHOKETOAN_DINHMUC();			
			entity.ID = id;
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVTSP = dVTSP;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVTNPL = dVTNPL;
			entity.DMSD = dMSD;
			entity.TLHH = tLHH;
			entity.DMCHUNG = dMCHUNG;
			entity.GHICHU = gHICHU;
			entity.HOPDONG_ID = hOPDONG_ID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_DINHMUC_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.NVarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVTSP", SqlDbType.NVarChar, DVTSP);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.NVarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVTNPL", SqlDbType.NVarChar, DVTNPL);
			db.AddInParameter(dbCommand, "@DMSD", SqlDbType.Decimal, DMSD);
			db.AddInParameter(dbCommand, "@TLHH", SqlDbType.Decimal, TLHH);
			db.AddInParameter(dbCommand, "@DMCHUNG", SqlDbType.Decimal, DMCHUNG);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KHOKETOAN_DINHMUC> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_DINHMUC item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KHOKETOAN_DINHMUC(long id)
		{
			T_KHOKETOAN_DINHMUC entity = new T_KHOKETOAN_DINHMUC();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_DINHMUC_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_DINHMUC_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KHOKETOAN_DINHMUC> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_DINHMUC item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}