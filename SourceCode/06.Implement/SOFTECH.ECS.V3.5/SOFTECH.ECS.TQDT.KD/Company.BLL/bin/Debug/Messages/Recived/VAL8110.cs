﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS.Messages.Recived
{
    public partial class VAL8110 : BasicVNACC
    {
        public List<VAL8110_HANG> HangMD { get; set; }
        public void LoadVAL8110(string strResult)
        {
            try
            {

                this.GetObject<VAL8110>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAL8110, false, VAL8110.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAL8110.TongSoByte);
                while (true)
                {
                    VAL8110_HANG vad1agHang = new VAL8110_HANG();
                    vad1agHang.GetObject<VAL8110_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAL8110_HANG", false, VAL8110_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAL8110_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAL8110_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAL8110_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAL8110_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }

}
