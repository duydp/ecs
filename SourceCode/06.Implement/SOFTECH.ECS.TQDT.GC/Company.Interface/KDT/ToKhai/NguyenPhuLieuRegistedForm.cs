using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;

namespace Company.Interface.KDT.ToKhai
{
    public partial class NguyenPhuLieuRegistedGCForm : BaseForm
    {
        public NguyenPhuLieu NguyenPhuLieuSelected = new NguyenPhuLieu();
        public Company.GC.BLL.KDT.ToKhaiMauDich TKMD;
        public HangMauDichCollection HMDCollection;
        public NguyenPhuLieuRegistedGCForm()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            NguyenPhuLieuSelected.HopDong_ID = TKMD.IDHopDong;
            dgList.DataSource = NguyenPhuLieuSelected.SelectCollectionBy_HopDong_ID();
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll();          
        }

        //-----------------------------------------------------------------------------------------

        private void NguyenPhuLieuRegistedForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            BindData();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------


        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool KiemTraMaHang(string MaHang)
        {
            foreach (Company.GC.BLL.KDT.HangMauDich HMD in HMDCollection)
            {
                if (HMD.MaPhu.Trim().ToUpper() == MaHang.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                Company.GC.BLL.GC.NguyenPhuLieu NPL = (Company.GC.BLL.GC.NguyenPhuLieu)row.DataRow;
                if (KiemTraMaHang(NPL.Ma))
                {
                    string st = showMsg("MSG_WRN31", NPL.Ma, true);
                    //string st = MLMessages("Mặt hàng " + NPL.Ma + " đã có nên sẽ được bỏ qua.Bạn có muốn tiếp tục không ?","MSG_WRN31",NPL.Ma, true);
                    if (st == "Yes")
                    {
                        continue;
                    }
                    else
                        return;
                }
                else
                {
                    HangMauDich HMD = new HangMauDich();
                    HMD.MaPhu = NPL.Ma;
                    HMD.TenHang = NPL.Ten;
                    HMD.MaHS = NPL.MaHS;
                    HMD.DVT_ID = NPL.DVT_ID;
                    HMDCollection.Add(HMD);
                }
            }
            this.Close();
        }

        //-----------------------------------------------------------------------------------------
    }
}