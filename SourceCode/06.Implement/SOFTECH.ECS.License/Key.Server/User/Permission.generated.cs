using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace KeyServer.User
{
	public partial class Permission
	{
		#region Properties.
		
		public int ID { set; get; }
		public int Master_ID { set; get; }
		public bool Admin { set; get; }
		public bool GenKey { set; get; }
		public bool GetLicense { set; get; }
		public bool GetTrial { set; get; }
		public bool UpdateNotices { set; get; }
		public bool TeamLead { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static Permission Load(int id)
		{
			const string spName = "[dbo].[p_LM_Permission_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
			Permission entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new Permission();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt32(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Admin"))) entity.Admin = reader.GetBoolean(reader.GetOrdinal("Admin"));
				if (!reader.IsDBNull(reader.GetOrdinal("GenKey"))) entity.GenKey = reader.GetBoolean(reader.GetOrdinal("GenKey"));
				if (!reader.IsDBNull(reader.GetOrdinal("GetLicense"))) entity.GetLicense = reader.GetBoolean(reader.GetOrdinal("GetLicense"));
				if (!reader.IsDBNull(reader.GetOrdinal("GetTrial"))) entity.GetTrial = reader.GetBoolean(reader.GetOrdinal("GetTrial"));
				if (!reader.IsDBNull(reader.GetOrdinal("UpdateNotices"))) entity.UpdateNotices = reader.GetBoolean(reader.GetOrdinal("UpdateNotices"));
				if (!reader.IsDBNull(reader.GetOrdinal("TeamLead"))) entity.TeamLead = reader.GetBoolean(reader.GetOrdinal("TeamLead"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<Permission> SelectCollectionAll()
		{
			List<Permission> collection = new List<Permission>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				Permission entity = new Permission();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt32(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Admin"))) entity.Admin = reader.GetBoolean(reader.GetOrdinal("Admin"));
				if (!reader.IsDBNull(reader.GetOrdinal("GenKey"))) entity.GenKey = reader.GetBoolean(reader.GetOrdinal("GenKey"));
				if (!reader.IsDBNull(reader.GetOrdinal("GetLicense"))) entity.GetLicense = reader.GetBoolean(reader.GetOrdinal("GetLicense"));
				if (!reader.IsDBNull(reader.GetOrdinal("GetTrial"))) entity.GetTrial = reader.GetBoolean(reader.GetOrdinal("GetTrial"));
				if (!reader.IsDBNull(reader.GetOrdinal("UpdateNotices"))) entity.UpdateNotices = reader.GetBoolean(reader.GetOrdinal("UpdateNotices"));
				if (!reader.IsDBNull(reader.GetOrdinal("TeamLead"))) entity.TeamLead = reader.GetBoolean(reader.GetOrdinal("TeamLead"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<Permission> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<Permission> collection = new List<Permission>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				Permission entity = new Permission();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt32(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Admin"))) entity.Admin = reader.GetBoolean(reader.GetOrdinal("Admin"));
				if (!reader.IsDBNull(reader.GetOrdinal("GenKey"))) entity.GenKey = reader.GetBoolean(reader.GetOrdinal("GenKey"));
				if (!reader.IsDBNull(reader.GetOrdinal("GetLicense"))) entity.GetLicense = reader.GetBoolean(reader.GetOrdinal("GetLicense"));
				if (!reader.IsDBNull(reader.GetOrdinal("GetTrial"))) entity.GetTrial = reader.GetBoolean(reader.GetOrdinal("GetTrial"));
				if (!reader.IsDBNull(reader.GetOrdinal("UpdateNotices"))) entity.UpdateNotices = reader.GetBoolean(reader.GetOrdinal("UpdateNotices"));
				if (!reader.IsDBNull(reader.GetOrdinal("TeamLead"))) entity.TeamLead = reader.GetBoolean(reader.GetOrdinal("TeamLead"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<Permission> SelectCollectionBy_Master_ID(int master_ID)
		{
			List<Permission> collection = new List<Permission>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_Master_ID(master_ID);
			while (reader.Read())
			{
				Permission entity = new Permission();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt32(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Admin"))) entity.Admin = reader.GetBoolean(reader.GetOrdinal("Admin"));
				if (!reader.IsDBNull(reader.GetOrdinal("GenKey"))) entity.GenKey = reader.GetBoolean(reader.GetOrdinal("GenKey"));
				if (!reader.IsDBNull(reader.GetOrdinal("GetLicense"))) entity.GetLicense = reader.GetBoolean(reader.GetOrdinal("GetLicense"));
				if (!reader.IsDBNull(reader.GetOrdinal("GetTrial"))) entity.GetTrial = reader.GetBoolean(reader.GetOrdinal("GetTrial"));
				if (!reader.IsDBNull(reader.GetOrdinal("UpdateNotices"))) entity.UpdateNotices = reader.GetBoolean(reader.GetOrdinal("UpdateNotices"));
				if (!reader.IsDBNull(reader.GetOrdinal("TeamLead"))) entity.TeamLead = reader.GetBoolean(reader.GetOrdinal("TeamLead"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Master_ID(int master_ID)
		{
			const string spName = "[dbo].[p_LM_Permission_SelectBy_Master_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.Int, master_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LM_Permission_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Permission_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LM_Permission_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Permission_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_Master_ID(int master_ID)
		{
			const string spName = "p_LM_Permission_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.Int, master_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertPermission(int master_ID, bool admin, bool genKey, bool getLicense, bool getTrial, bool updateNotices, bool teamLead)
		{
			Permission entity = new Permission();	
			entity.Master_ID = master_ID;
			entity.Admin = admin;
			entity.GenKey = genKey;
			entity.GetLicense = getLicense;
			entity.GetTrial = getTrial;
			entity.UpdateNotices = updateNotices;
			entity.TeamLead = teamLead;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LM_Permission_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.Int, Master_ID);
			db.AddInParameter(dbCommand, "@Admin", SqlDbType.Bit, Admin);
			db.AddInParameter(dbCommand, "@GenKey", SqlDbType.Bit, GenKey);
			db.AddInParameter(dbCommand, "@GetLicense", SqlDbType.Bit, GetLicense);
			db.AddInParameter(dbCommand, "@GetTrial", SqlDbType.Bit, GetTrial);
			db.AddInParameter(dbCommand, "@UpdateNotices", SqlDbType.Bit, UpdateNotices);
			db.AddInParameter(dbCommand, "@TeamLead", SqlDbType.Bit, TeamLead);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<Permission> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Permission item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdatePermission(int id, int master_ID, bool admin, bool genKey, bool getLicense, bool getTrial, bool updateNotices, bool teamLead)
		{
			Permission entity = new Permission();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.Admin = admin;
			entity.GenKey = genKey;
			entity.GetLicense = getLicense;
			entity.GetTrial = getTrial;
			entity.UpdateNotices = updateNotices;
			entity.TeamLead = teamLead;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LM_Permission_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.Int, Master_ID);
			db.AddInParameter(dbCommand, "@Admin", SqlDbType.Bit, Admin);
			db.AddInParameter(dbCommand, "@GenKey", SqlDbType.Bit, GenKey);
			db.AddInParameter(dbCommand, "@GetLicense", SqlDbType.Bit, GetLicense);
			db.AddInParameter(dbCommand, "@GetTrial", SqlDbType.Bit, GetTrial);
			db.AddInParameter(dbCommand, "@UpdateNotices", SqlDbType.Bit, UpdateNotices);
			db.AddInParameter(dbCommand, "@TeamLead", SqlDbType.Bit, TeamLead);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<Permission> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Permission item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdatePermission(int id, int master_ID, bool admin, bool genKey, bool getLicense, bool getTrial, bool updateNotices, bool teamLead)
		{
			Permission entity = new Permission();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.Admin = admin;
			entity.GenKey = genKey;
			entity.GetLicense = getLicense;
			entity.GetTrial = getTrial;
			entity.UpdateNotices = updateNotices;
			entity.TeamLead = teamLead;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Permission_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.Int, Master_ID);
			db.AddInParameter(dbCommand, "@Admin", SqlDbType.Bit, Admin);
			db.AddInParameter(dbCommand, "@GenKey", SqlDbType.Bit, GenKey);
			db.AddInParameter(dbCommand, "@GetLicense", SqlDbType.Bit, GetLicense);
			db.AddInParameter(dbCommand, "@GetTrial", SqlDbType.Bit, GetTrial);
			db.AddInParameter(dbCommand, "@UpdateNotices", SqlDbType.Bit, UpdateNotices);
			db.AddInParameter(dbCommand, "@TeamLead", SqlDbType.Bit, TeamLead);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<Permission> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Permission item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeletePermission(int id)
		{
			Permission entity = new Permission();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Permission_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_Master_ID(int master_ID)
		{
			const string spName = "[dbo].[p_LM_Permission_DeleteBy_Master_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.Int, master_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LM_Permission_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<Permission> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Permission item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}