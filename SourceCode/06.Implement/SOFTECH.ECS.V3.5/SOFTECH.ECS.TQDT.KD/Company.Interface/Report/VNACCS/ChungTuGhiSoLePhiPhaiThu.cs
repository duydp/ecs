using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class ChungTuGhiSoLePhiPhaiThu : DevExpress.XtraReports.UI.XtraReport
    {
        public ChungTuGhiSoLePhiPhaiThu()
        {
            InitializeComponent();
        }
        public void BindReport(VAF8030 vaf803)
        {
            //xrLabel33.Text = "Trong thời hạn từ ngày 01 đến ngày 10 của tháng sau, đơn vị có trách nhiệm nộp đủ số tiền lệ phí phải nộp tại bộ phận thu thuế của cơ quan Hải quan nơi làm thủ tục ";
            lblTencqHQ.Text = vaf803.CSM.GetValue().ToString();
            lblTenchicucHQ.Text = vaf803.CBN.GetValue().ToString();
            lblSochungtu.Text = vaf803.SPN.GetValue().ToString();
            lblTendvXNK.Text = vaf803.IEN.GetValue().ToString();
            lblMadvXNK.Text = vaf803.IEC.GetValue().ToString();
            lblMabuuchinh.Text = vaf803.PCD.GetValue().ToString();
            lblDiachinguoiXNK.Text = vaf803.ADB.GetValue().ToString();
            lblSodtXNK.Text = vaf803.TEL.GetValue().ToString();
            lblSotokhai.Text = vaf803.ICN.GetValue().ToString();
            if (Convert.ToDateTime(vaf803.DDC.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgaydangkytokhai.Text = Convert.ToDateTime(vaf803.DDC.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgaydangkytokhai.Text = "";
            lblMaphanloai.Text = vaf803.DKC.GetValue().ToString();
            lblSotaikhoan.Text = vaf803.TAN.GetValue().ToString();
            lblTenkhobac.Text = vaf803.TNM.GetValue().ToString();
            if (Convert.ToDateTime(vaf803.DOP.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayphathanh.Text = Convert.ToDateTime(vaf803.DOP.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayphathanh.Text = "";

        }

    }
}
