using System;
using System.Collections.Generic;
using System.Text;

namespace VNPTCrypt
{
    public interface ISignedObject
    {
        byte[] OriginalData { get;set;}
        byte[] SignedData { get;set;}                
        byte[] CertData{get;set;}
        int HashType { get;set;}
        string Errormessage { get;}        
    }
}
