using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class HuyToKhai
	{
		#region Properties.
		
		public long ID { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int NamTiepNhan { set; get; }
		public int TrangThai { set; get; }
		public string Guid { set; get; }
		public string LyDoHuy { set; get; }
		public long TKMD_ID { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HuyToKhai Load(long id)
		{
			const string spName = "[dbo].[p_KDT_HuyToKhai_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			HuyToKhai entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new HuyToKhai();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Guid"))) entity.Guid = reader.GetString(reader.GetOrdinal("Guid"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoHuy"))) entity.LyDoHuy = reader.GetString(reader.GetOrdinal("LyDoHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<HuyToKhai> SelectCollectionAll()
		{
			List<HuyToKhai> collection = new List<HuyToKhai>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				HuyToKhai entity = new HuyToKhai();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Guid"))) entity.Guid = reader.GetString(reader.GetOrdinal("Guid"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoHuy"))) entity.LyDoHuy = reader.GetString(reader.GetOrdinal("LyDoHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<HuyToKhai> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<HuyToKhai> collection = new List<HuyToKhai>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				HuyToKhai entity = new HuyToKhai();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Guid"))) entity.Guid = reader.GetString(reader.GetOrdinal("Guid"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoHuy"))) entity.LyDoHuy = reader.GetString(reader.GetOrdinal("LyDoHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<HuyToKhai> SelectCollectionBy_TKMD_ID(long tKMD_ID)
		{
			List<HuyToKhai> collection = new List<HuyToKhai>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_TKMD_ID(tKMD_ID);
			while (reader.Read())
			{
				HuyToKhai entity = new HuyToKhai();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Guid"))) entity.Guid = reader.GetString(reader.GetOrdinal("Guid"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoHuy"))) entity.LyDoHuy = reader.GetString(reader.GetOrdinal("LyDoHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_HuyToKhai_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_HuyToKhai_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_HuyToKhai_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_HuyToKhai_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_HuyToKhai_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "p_KDT_HuyToKhai_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHuyToKhai(long soTiepNhan, DateTime ngayTiepNhan, int namTiepNhan, int trangThai, string guid, string lyDoHuy, long tKMD_ID)
		{
			HuyToKhai entity = new HuyToKhai();	
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.NamTiepNhan = namTiepNhan;
			entity.TrangThai = trangThai;
			entity.Guid = guid;
			entity.LyDoHuy = lyDoHuy;
			entity.TKMD_ID = tKMD_ID;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_HuyToKhai_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@Guid", SqlDbType.VarChar, Guid);
			db.AddInParameter(dbCommand, "@LyDoHuy", SqlDbType.NVarChar, LyDoHuy);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HuyToKhai> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HuyToKhai item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHuyToKhai(long id, long soTiepNhan, DateTime ngayTiepNhan, int namTiepNhan, int trangThai, string guid, string lyDoHuy, long tKMD_ID)
		{
			HuyToKhai entity = new HuyToKhai();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.NamTiepNhan = namTiepNhan;
			entity.TrangThai = trangThai;
			entity.Guid = guid;
			entity.LyDoHuy = lyDoHuy;
			entity.TKMD_ID = tKMD_ID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_HuyToKhai_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@Guid", SqlDbType.VarChar, Guid);
			db.AddInParameter(dbCommand, "@LyDoHuy", SqlDbType.NVarChar, LyDoHuy);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HuyToKhai> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HuyToKhai item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHuyToKhai(long id, long soTiepNhan, DateTime ngayTiepNhan, int namTiepNhan, int trangThai, string guid, string lyDoHuy, long tKMD_ID)
		{
			HuyToKhai entity = new HuyToKhai();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.NamTiepNhan = namTiepNhan;
			entity.TrangThai = trangThai;
			entity.Guid = guid;
			entity.LyDoHuy = lyDoHuy;
			entity.TKMD_ID = tKMD_ID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_HuyToKhai_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@Guid", SqlDbType.VarChar, Guid);
			db.AddInParameter(dbCommand, "@LyDoHuy", SqlDbType.NVarChar, LyDoHuy);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HuyToKhai> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HuyToKhai item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHuyToKhai(long id)
		{
			HuyToKhai entity = new HuyToKhai();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_HuyToKhai_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_HuyToKhai_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_HuyToKhai_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HuyToKhai> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HuyToKhai item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}