﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace KeySecurity
{
    #region LicenseStatus
    public enum LicenseStatus
    {
        Licensed,
        TrialVersion,
        Expired,
        MachineHashMismatch,
        NotFound,
        Invalid,
        InternalError,
        Lock,
    }
    #endregion
    [XmlRoot("Root")]
    public class KeyInfo
    {
        /// <summary>
        /// Mã máy dùng để phân biệt khi copy sang máy khác.
        /// </summary>
        public string MachineCode { get; set; }
        #region Server
        /// <summary>
        /// Số serial
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Ngày hết hạn
        /// </summary>
        public DateTime ExpiredDate { get; set; }
        /// <summary>
        /// Số ngày sử dụng còn lại
        /// </summary>
        public int TrialDays { get; set; }
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string ProductId { get; set; }
        public string Command { get; set; }
        public string Notice { get; set; }
        /// <summary>
        /// CustomerField10
        /// </summary>
        public bool IsShow { get; set; }
        #endregion
        public KeyInfo()
        {
        }
    }
}
