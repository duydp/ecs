-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_Container_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_Container_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_Container_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_Container_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_Container_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_Container_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_Container_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_Container_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_Container_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_Container_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_Container_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_Container_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_Container_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_Container_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_Container_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_Container_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_Container_SelectBy_VanDon_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_Container_SelectBy_VanDon_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_Container_DeleteBy_VanDon_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_Container_DeleteBy_VanDon_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_Insert]
	@VanDon_ID bigint,
	@SoHieu varchar(50),
	@LoaiContainer varchar(1),
	@Seal_No varchar(50),
	@Trang_thai int,
	@TrongLuong float,
	@TrongLuongNet float,
	@SoKien float,
	@DiaDiemDongHang nvarchar(50),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_Container]
(
	[VanDon_ID],
	[SoHieu],
	[LoaiContainer],
	[Seal_No],
	[Trang_thai],
	[TrongLuong],
	[TrongLuongNet],
	[SoKien],
	[DiaDiemDongHang]
)
VALUES 
(
	@VanDon_ID,
	@SoHieu,
	@LoaiContainer,
	@Seal_No,
	@Trang_thai,
	@TrongLuong,
	@TrongLuongNet,
	@SoKien,
	@DiaDiemDongHang
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_Update]
	@ID bigint,
	@VanDon_ID bigint,
	@SoHieu varchar(50),
	@LoaiContainer varchar(1),
	@Seal_No varchar(50),
	@Trang_thai int,
	@TrongLuong float,
	@TrongLuongNet float,
	@SoKien float,
	@DiaDiemDongHang nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_Container]
SET
	[VanDon_ID] = @VanDon_ID,
	[SoHieu] = @SoHieu,
	[LoaiContainer] = @LoaiContainer,
	[Seal_No] = @Seal_No,
	[Trang_thai] = @Trang_thai,
	[TrongLuong] = @TrongLuong,
	[TrongLuongNet] = @TrongLuongNet,
	[SoKien] = @SoKien,
	[DiaDiemDongHang] = @DiaDiemDongHang
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_InsertUpdate]
	@ID bigint,
	@VanDon_ID bigint,
	@SoHieu varchar(50),
	@LoaiContainer varchar(1),
	@Seal_No varchar(50),
	@Trang_thai int,
	@TrongLuong float,
	@TrongLuongNet float,
	@SoKien float,
	@DiaDiemDongHang nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_Container] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_Container] 
		SET
			[VanDon_ID] = @VanDon_ID,
			[SoHieu] = @SoHieu,
			[LoaiContainer] = @LoaiContainer,
			[Seal_No] = @Seal_No,
			[Trang_thai] = @Trang_thai,
			[TrongLuong] = @TrongLuong,
			[TrongLuongNet] = @TrongLuongNet,
			[SoKien] = @SoKien,
			[DiaDiemDongHang] = @DiaDiemDongHang
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_Container]
		(
			[VanDon_ID],
			[SoHieu],
			[LoaiContainer],
			[Seal_No],
			[Trang_thai],
			[TrongLuong],
			[TrongLuongNet],
			[SoKien],
			[DiaDiemDongHang]
		)
		VALUES 
		(
			@VanDon_ID,
			@SoHieu,
			@LoaiContainer,
			@Seal_No,
			@Trang_thai,
			@TrongLuong,
			@TrongLuongNet,
			@SoKien,
			@DiaDiemDongHang
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_Container]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_DeleteBy_VanDon_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_DeleteBy_VanDon_ID]
	@VanDon_ID bigint
AS

DELETE FROM [dbo].[t_KDT_Container]
WHERE
	[VanDon_ID] = @VanDon_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_Container] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[VanDon_ID],
	[SoHieu],
	[LoaiContainer],
	[Seal_No],
	[Trang_thai],
	[TrongLuong],
	[TrongLuongNet],
	[SoKien],
	[DiaDiemDongHang]
FROM
	[dbo].[t_KDT_Container]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_SelectBy_VanDon_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_SelectBy_VanDon_ID]
	@VanDon_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[VanDon_ID],
	[SoHieu],
	[LoaiContainer],
	[Seal_No],
	[Trang_thai],
	[TrongLuong],
	[TrongLuongNet],
	[SoKien],
	[DiaDiemDongHang]
FROM
	[dbo].[t_KDT_Container]
WHERE
	[VanDon_ID] = @VanDon_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[VanDon_ID],
	[SoHieu],
	[LoaiContainer],
	[Seal_No],
	[Trang_thai],
	[TrongLuong],
	[TrongLuongNet],
	[SoKien],
	[DiaDiemDongHang]
FROM [dbo].[t_KDT_Container] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[VanDon_ID],
	[SoHieu],
	[LoaiContainer],
	[Seal_No],
	[Trang_thai],
	[TrongLuong],
	[TrongLuongNet],
	[SoKien],
	[DiaDiemDongHang]
FROM
	[dbo].[t_KDT_Container]	

GO

