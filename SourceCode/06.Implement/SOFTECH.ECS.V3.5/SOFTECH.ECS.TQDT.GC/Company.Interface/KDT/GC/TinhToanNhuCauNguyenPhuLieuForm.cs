﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using Company.Interface.KDT.SXXK;
using Infragistics.Excel;
using System.Diagnostics;
using System.Collections.Generic;

namespace Company.Interface.KDT.GC
{
    public partial class TinhToanNhuCauNguyenPhuLieuForm : BaseForm
    {
        DataTable tabelNPL = new DataTable();
        public Company.GC.BLL.KDT.GC.HopDong HD;
        public TinhToanNhuCauNguyenPhuLieuForm()
        {
            InitializeComponent();
        }

        private void BindData()
        {
            tabelNPL = new DataTable();
            tabelNPL.Columns.Add("MaNPL");
            tabelNPL.Columns.Add("TenNPL");
            tabelNPL.Columns.Add("NhuCauDM", typeof(decimal));
            tabelNPL.Columns.Add("NhuCauThucTe", typeof(decimal));
            tabelNPL.Columns.Add("ChenhLech", typeof(decimal));
            Company.GC.BLL.GC.NguyenPhuLieuCollection NPLCollection = HD.GetNPL();
            SanPhamCollection SPCollection = HD.GetSP();
            string st = "";
            foreach (SanPham SP in SPCollection)
            {
                DataTable tableDinhMuc = DinhMuc.getDinhMuc(HD.ID, SP.Ma, SP.SoLuongDangKy, GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.DinhMuc);
                if (tableDinhMuc.Rows.Count == 0)
                {
                    //string Relsult = showMsg("MSG_0203005", SP.Ma, true);
                    st += "Sản phẩm : " + SP.Ma +"; ";
                }
                foreach (DataRow row in tableDinhMuc.Rows)
                {
                    string MaNPL = row["MaNguyenPhuLieu"].ToString();
                    decimal Nhucau = Convert.ToDecimal(row["NhuCau"]);
                    DataRow[] rowNPLCollection = tabelNPL.Select("MaNPL='" + MaNPL + "'");
                    DataRow rowNPL = null;
                    if (rowNPLCollection.Length == 0)
                    {
                        rowNPL = tabelNPL.NewRow();
                        rowNPL["MaNPL"] = MaNPL;
                        rowNPL["TenNPL"] = row["TenNPL"].ToString();
                        rowNPL["NhuCauDM"] = Nhucau;
                        tabelNPL.Rows.Add(rowNPL);
                    }
                    else
                    {
                        rowNPL = rowNPLCollection[0];
                        rowNPL["NhuCauDM"] = Convert.ToDecimal(rowNPL["NhuCauDM"]) + Nhucau;
                    }
                }
            }
            try
            {
                foreach (NguyenPhuLieu npl in NPLCollection)
                {
                    DataRow[] rowCollection = tabelNPL.Select("MaNPL='" + npl.Ma + "'");
                    if (rowCollection.Length == 0)
                    {
                        DataRow row = tabelNPL.NewRow();
                        row["MaNPL"] = npl.Ma.ToString();
                        row["TenNPL"] = npl.Ten.ToString();
                        row["NhuCauDM"] = 0;
                        row["NhuCauThucTe"] = npl.SoLuongDangKy;
                        row["ChenhLech"] = npl.SoLuongDangKy;
                        tabelNPL.Rows.Add(row);
                    }
                    else
                    {
                        DataRow row = rowCollection[0];
                        row["NhuCauThucTe"] = npl.SoLuongDangKy;
                        row["ChenhLech"] = npl.SoLuongDangKy - Convert.ToDecimal(row["NhuCauDM"]);
                    }
                }
                dgNguyenPhuLieu.DataSource = tabelNPL;
                dgNguyenPhuLieu.Refetch();
                if (st != "")
                {
                    ShowMessage(st+" chưa có định mức.", false);
                }
            }
            catch
            {
                dgNguyenPhuLieu.Refresh();
            }
        }
      
        private void LoaiSanPhamGCEditForm_Load(object sender, EventArgs e)
        {
            dgNguyenPhuLieu.Tables[0].Columns["TongNhuCau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgNguyenPhuLieu.Tables[0].Columns["NhuCauThucTe"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            BindData();
            saveFileDialog1.InitialDirectory = Application.StartupPath;
        }
       

        private void dgNguyenPhuLieu_LoadingRow(object sender, RowLoadEventArgs e)
        {
        }
        private void XuatExcelNPL()
        {
            if (tabelNPL == null || tabelNPL.Rows.Count == 0)
            {
                showMsg("MSG_0203006");
                //ShowMessage("Không có dữ liệu để xuất ra file Excel.", false);
                return;
            }
            string fileName = "";
            DialogResult rs = saveFileDialog1.ShowDialog();
            if (rs == DialogResult.OK)
            {
                fileName = saveFileDialog1.FileName;
            }
            else
                return;

            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets.Add("TongNhuCauNPL");

            WorksheetRowCollection wsrc = ws.Rows;
            WorksheetRow wsr0 = ws.Rows[0];
            ws.Columns[0].Width = 1200;
            ws.Columns[1].Width = 8000;
            ws.Columns[2].Width = 2500;
            ws.Columns[3].Width = 1500;
            wsr0.Cells[0].Value = "Mã NPL";
            wsr0.Cells[1].Value = "Tên NPL";            
            wsr0.Cells[2].Value = "Tổng nhu cầu";

            int i = 1;
            foreach (DataRow row in tabelNPL.Rows)
            {
                WorksheetRow wsr = ws.Rows[i++];
                wsr.Cells[0].Value = row["MaNPL"].ToString();
                wsr.Cells[1].Value = row["TenNPL"].ToString();
                wsr.Cells[2].Value = row["NhuCauDM"];
            }

            wb.Save(fileName);
            //wb = Workbook.Load(fileName);
            if (showMsg("MSG_MAL08", true) == "Yes")
            {
                System.Diagnostics.Process.Start(fileName);
            }

        }
        private void ImportNPLThucTe()
        {
            ImportNhuCauNPLThucTeExcelForm f = new ImportNhuCauNPLThucTeExcelForm();
            f.tableNPL = tabelNPL;
            f.HD = this.HD;
            f.ShowDialog();
            try
            {
                tabelNPL = f.tableNPL;
                dgNguyenPhuLieu.DataSource = tabelNPL;
                dgNguyenPhuLieu.Refetch();
            }
            catch
            {
                dgNguyenPhuLieu.Refresh();
            }
        }
        private void cmMainHDGC_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "XuatExcel" : XuatExcelNPL();break;
                case "NhapNPL" : ImportNPLThucTe();break;
                case "cmdXuatExcelDinhMuc": XuatExcelDinhMuc(); break;
                    
            }

           

        }

        private void XuatExcelDinhMuc()
        {

            Company.GC.BLL.GC.NguyenPhuLieuCollection NPLCollection = HD.GetNPL();
            SanPhamCollection SPCollection = HD.GetSP();

            Workbook wb = new Workbook();
            Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
            int soSheet = (SPCollection.Count - 1) / 50 + 1;
            for (int t = 0; t < soSheet; t++)
            {
                Worksheet ws = wb.Worksheets.Add("DinhMuc"+t);

                WorksheetRowCollection wsrc = ws.Rows;
                WorksheetRow wsr0 = ws.Rows[0];
                WorksheetRow wsr1 = ws.Rows[1];
                WorksheetRow wsr2 = ws.Rows[2];
                wsr0.Cells[1].Value = "DANH SÁCH ĐỊNH MỨC SẢN PHẨM GIA CÔNG";
                wsr0.Height = 1000;
                wsr0.Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                ws.Columns[0].Width = 1200;
                ws.Columns[1].Width = 8000;
                ws.Columns[2].Width = 2500;
                ws.Columns[3].Width = 1500;
                wsr1.Cells[0].Value = "STT";
                wsr1.Cells[1].Value = "Tên NPL";
                wsr1.Cells[2].Value = "Mã NPL";
                wsr1.Cells[3].Value = "ĐVT";
                wsr1.Height = 800;
                wsr1.CellFormat.WrapText = ExcelDefaultableBoolean.True;
                ws.MergedCellsRegions.Add(1, 0, 2, 0);
                ws.MergedCellsRegions.Add(1, 1, 2, 1);
                ws.MergedCellsRegions.Add(1, 2, 2, 2);
                ws.MergedCellsRegions.Add(1, 3, 2, 3);

                SanPhamCollection SPCols = new SanPhamCollection();
                for (int k = t * 50; k < (t + 1) * 50; k++)
                {
                    if(k<SPCollection.Count)
                        SPCols.Add(SPCollection[k]);
                }
                int temp = 0;
                int temp1 = 0;
                for (int i = 0; i < SPCols.Count; i++)
                {
                    SanPham SP = SPCols[i];
                    temp = i * 3 + 4;
                    temp1 = i + 4;
                    wsr1.Cells[temp].Value = SP.Ten + "\n" + SP.Ma + "\nSố lượng: " + SP.SoLuongDangKy;
                    WorksheetCellComment com = new WorksheetCellComment();
                    com.Text = new FormattedString(SP.Ma);
                    com.Author = SP.SoLuongDangKy + "";
                    wsr1.Cells[temp].Comment = com;
                    ws.MergedCellsRegions.Add(1, temp, 1, temp + 2);
                    wsr2.Cells[temp1].Value = "ĐM";
                    wsr2.Cells[temp1 + 1].Value = "TLHH";
                    wsr2.Cells[temp1 + 2].Value = "NC";

                }

                for (int j = 0; j < NPLCollection.Count; j++)
                {
                    temp = 4;
                    NguyenPhuLieu NPL = NPLCollection[j];
                    WorksheetRow wsr = ws.Rows[j + 3];
                    wsr.Cells[0].Value = j + 1;
                    wsr.Cells[1].Value = NPL.Ten;
                    wsr.Cells[2].Value = NPL.Ma;
                    wsr.Cells[3].Value = DonViTinh_GetName(NPL.DVT_ID);
                    for (int i = 0; i < SPCols.Count; i++)
                    {
                        DM.MaNguyenPhuLieu = NPL.Ma;
                        DM.MaSanPham = wsr1.Cells[temp].Comment.Text.UnformattedString;
                        DM.HopDong_ID = this.HD.ID;
                        if (DM.Load())
                        {
                            wsr.Cells[temp].Value = DM.DinhMucSuDung;
                            temp++;
                            wsr.Cells[temp].Value = DM.TyLeHaoHut;
                            temp++;
                            wsr.Cells[temp].Value = DM.DinhMucSuDung * (100 + DM.TyLeHaoHut) / 100 * Convert.ToDecimal(wsr1.Cells[temp - 2].Comment.Author);
                            temp++;
                        }
                        else
                        {
                            wsr.Cells[temp].Value = "-";
                            temp++;
                            wsr.Cells[temp].Value = "-";
                            temp++;
                            wsr.Cells[temp].Value = "-";
                            temp++;
                        }
                    }
                }

            }
            string fileName = "";
            DialogResult rs = saveFileDialog1.ShowDialog();
            if (rs == DialogResult.OK)
            {
                fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
            }

            //wb = Workbook.Load(fileName);
            if (showMsg("MSG_MAL08", true) == "Yes")
            {
                Process.Start(fileName);
            }
            
        }

     
    }
}