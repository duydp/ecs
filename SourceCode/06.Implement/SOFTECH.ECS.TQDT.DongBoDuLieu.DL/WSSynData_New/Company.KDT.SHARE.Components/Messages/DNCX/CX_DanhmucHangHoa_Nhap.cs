﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class CX_DanhmucHangHoa_Nhap:DeclarationBase
    {
        /// <summary>
        /// Thông tin danh mục hàng hóa
        /// </summary>
       [XmlElement("IncomingGoodsItem")]
       public CustomsGoodsItem IncomingGoodsItems { get; set; }
    }
}
