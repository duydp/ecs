-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Insert]
	@MaNghiepVu varchar(5),
	@TieuDeThongBao nvarchar(255),
	@NoiDungThongBao nvarchar(max),
	@Log_Messages nvarchar(max),
	@Item_id bigint,
	@CreatedTime datetime,
	@MessagesTag varchar(26),
	@InputMessagesID varchar(10),
	@IndexTag nchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_MsgLog]
(
	[MaNghiepVu],
	[TieuDeThongBao],
	[NoiDungThongBao],
	[Log_Messages],
	[Item_id],
	[CreatedTime],
	[MessagesTag],
	[InputMessagesID],
	[IndexTag]
)
VALUES 
(
	@MaNghiepVu,
	@TieuDeThongBao,
	@NoiDungThongBao,
	@Log_Messages,
	@Item_id,
	@CreatedTime,
	@MessagesTag,
	@InputMessagesID,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Update]
	@ID bigint,
	@MaNghiepVu varchar(5),
	@TieuDeThongBao nvarchar(255),
	@NoiDungThongBao nvarchar(max),
	@Log_Messages nvarchar(max),
	@Item_id bigint,
	@CreatedTime datetime,
	@MessagesTag varchar(26),
	@InputMessagesID varchar(10),
	@IndexTag nchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_MsgLog]
SET
	[MaNghiepVu] = @MaNghiepVu,
	[TieuDeThongBao] = @TieuDeThongBao,
	[NoiDungThongBao] = @NoiDungThongBao,
	[Log_Messages] = @Log_Messages,
	[Item_id] = @Item_id,
	[CreatedTime] = @CreatedTime,
	[MessagesTag] = @MessagesTag,
	[InputMessagesID] = @InputMessagesID,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_InsertUpdate]
	@ID bigint,
	@MaNghiepVu varchar(5),
	@TieuDeThongBao nvarchar(255),
	@NoiDungThongBao nvarchar(max),
	@Log_Messages nvarchar(max),
	@Item_id bigint,
	@CreatedTime datetime,
	@MessagesTag varchar(26),
	@InputMessagesID varchar(10),
	@IndexTag nchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_MsgLog] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_MsgLog] 
		SET
			[MaNghiepVu] = @MaNghiepVu,
			[TieuDeThongBao] = @TieuDeThongBao,
			[NoiDungThongBao] = @NoiDungThongBao,
			[Log_Messages] = @Log_Messages,
			[Item_id] = @Item_id,
			[CreatedTime] = @CreatedTime,
			[MessagesTag] = @MessagesTag,
			[InputMessagesID] = @InputMessagesID,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_MsgLog]
		(
			[MaNghiepVu],
			[TieuDeThongBao],
			[NoiDungThongBao],
			[Log_Messages],
			[Item_id],
			[CreatedTime],
			[MessagesTag],
			[InputMessagesID],
			[IndexTag]
		)
		VALUES 
		(
			@MaNghiepVu,
			@TieuDeThongBao,
			@NoiDungThongBao,
			@Log_Messages,
			@Item_id,
			@CreatedTime,
			@MessagesTag,
			@InputMessagesID,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_MsgLog]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_MsgLog] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNghiepVu],
	[TieuDeThongBao],
	[NoiDungThongBao],
	[Log_Messages],
	[Item_id],
	[CreatedTime],
	[MessagesTag],
	[InputMessagesID],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_MsgLog]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaNghiepVu],
	[TieuDeThongBao],
	[NoiDungThongBao],
	[Log_Messages],
	[Item_id],
	[CreatedTime],
	[MessagesTag],
	[InputMessagesID],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_MsgLog] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNghiepVu],
	[TieuDeThongBao],
	[NoiDungThongBao],
	[Log_Messages],
	[Item_id],
	[CreatedTime],
	[MessagesTag],
	[InputMessagesID],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_MsgLog]	

GO

