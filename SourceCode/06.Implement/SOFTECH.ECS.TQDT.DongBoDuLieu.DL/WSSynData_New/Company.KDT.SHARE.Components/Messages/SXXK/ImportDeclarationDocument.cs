﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages;
namespace Company.KDT.SHARE.Components
{
    public class ImportDeclarationDocument : DeclarationBase
    {
        [XmlElement("DeclarationDocument")]
        public List<DeclarationDocument> DeclarationDocuments { get; set; }

        [XmlElement("PreviousCustomsDocument")]
        public PreviousCustomsDocument PreviousCustomsDocument { get; set; }

    }
}
