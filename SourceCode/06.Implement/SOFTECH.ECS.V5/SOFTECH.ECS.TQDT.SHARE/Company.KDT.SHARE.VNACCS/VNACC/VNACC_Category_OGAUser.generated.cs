using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_OGAUser : ICloneable
	{
		#region Properties.
		
		public string ResultCode { set; get; }
		public int PGNumber { set; get; }
		public string TableID { set; get; }
		public string ProcessClassification { set; get; }
		public int CreatorClassification { set; get; }
		public string NumberOfKeyItems { set; get; }
		public string OfficeOfApplicationCode { set; get; }
		public string OfficeOfApplicationName { set; get; }
		public string DestinationCode_1 { set; get; }
		public string DestinationCode_2 { set; get; }
		public string DestinationCode_3 { set; get; }
		public string DestinationCode_4 { set; get; }
		public string DestinationCode_5 { set; get; }
		public string DestinationCode_6 { set; get; }
		public string DestinationCode_7 { set; get; }
		public string DestinationCode_8 { set; get; }
		public string DestinationCode_9 { set; get; }
		public string DestinationCode_10 { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACC_Category_OGAUser> ConvertToCollection(IDataReader reader)
		{
			List<VNACC_Category_OGAUser> collection = new List<VNACC_Category_OGAUser>();
			while (reader.Read())
			{
				VNACC_Category_OGAUser entity = new VNACC_Category_OGAUser();
				if (!reader.IsDBNull(reader.GetOrdinal("ResultCode"))) entity.ResultCode = reader.GetString(reader.GetOrdinal("ResultCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("PGNumber"))) entity.PGNumber = reader.GetInt32(reader.GetOrdinal("PGNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProcessClassification"))) entity.ProcessClassification = reader.GetString(reader.GetOrdinal("ProcessClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatorClassification"))) entity.CreatorClassification = reader.GetInt32(reader.GetOrdinal("CreatorClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("NumberOfKeyItems"))) entity.NumberOfKeyItems = reader.GetString(reader.GetOrdinal("NumberOfKeyItems"));
				if (!reader.IsDBNull(reader.GetOrdinal("OfficeOfApplicationCode"))) entity.OfficeOfApplicationCode = reader.GetString(reader.GetOrdinal("OfficeOfApplicationCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("OfficeOfApplicationName"))) entity.OfficeOfApplicationName = reader.GetString(reader.GetOrdinal("OfficeOfApplicationName"));
				if (!reader.IsDBNull(reader.GetOrdinal("DestinationCode_1"))) entity.DestinationCode_1 = reader.GetString(reader.GetOrdinal("DestinationCode_1"));
				if (!reader.IsDBNull(reader.GetOrdinal("DestinationCode_2"))) entity.DestinationCode_2 = reader.GetString(reader.GetOrdinal("DestinationCode_2"));
				if (!reader.IsDBNull(reader.GetOrdinal("DestinationCode_3"))) entity.DestinationCode_3 = reader.GetString(reader.GetOrdinal("DestinationCode_3"));
				if (!reader.IsDBNull(reader.GetOrdinal("DestinationCode_4"))) entity.DestinationCode_4 = reader.GetString(reader.GetOrdinal("DestinationCode_4"));
				if (!reader.IsDBNull(reader.GetOrdinal("DestinationCode_5"))) entity.DestinationCode_5 = reader.GetString(reader.GetOrdinal("DestinationCode_5"));
				if (!reader.IsDBNull(reader.GetOrdinal("DestinationCode_6"))) entity.DestinationCode_6 = reader.GetString(reader.GetOrdinal("DestinationCode_6"));
				if (!reader.IsDBNull(reader.GetOrdinal("DestinationCode_7"))) entity.DestinationCode_7 = reader.GetString(reader.GetOrdinal("DestinationCode_7"));
				if (!reader.IsDBNull(reader.GetOrdinal("DestinationCode_8"))) entity.DestinationCode_8 = reader.GetString(reader.GetOrdinal("DestinationCode_8"));
				if (!reader.IsDBNull(reader.GetOrdinal("DestinationCode_9"))) entity.DestinationCode_9 = reader.GetString(reader.GetOrdinal("DestinationCode_9"));
				if (!reader.IsDBNull(reader.GetOrdinal("DestinationCode_10"))) entity.DestinationCode_10 = reader.GetString(reader.GetOrdinal("DestinationCode_10"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
        public static List<VNACC_Category_OGAUser> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<VNACC_Category_OGAUser> collection = new List<VNACC_Category_OGAUser>();
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                VNACC_Category_OGAUser entity = new VNACC_Category_OGAUser();
                entity.ResultCode = dataSet.Tables[0].Rows[i]["ResultCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ResultCode"].ToString() : String.Empty;
                entity.PGNumber = dataSet.Tables[0].Rows[i]["PGNumber"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["PGNumber"].ToString()) : 0;
                entity.TableID = dataSet.Tables[0].Rows[i]["TableID"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["TableID"].ToString() : "A700A";
                entity.ProcessClassification = dataSet.Tables[0].Rows[i]["ProcessClassification"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ProcessClassification"].ToString() : String.Empty;
                entity.CreatorClassification = dataSet.Tables[0].Rows[i]["CreatorClassification"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["CreatorClassification"].ToString()) : 0;
                entity.NumberOfKeyItems = dataSet.Tables[0].Rows[i]["NumberOfKeyItems"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["NumberOfKeyItems"].ToString() : String.Empty;
                entity.OfficeOfApplicationCode = dataSet.Tables[0].Rows[i]["OfficeOfApplicationCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["OfficeOfApplicationCode"].ToString() : String.Empty;
                entity.OfficeOfApplicationName = dataSet.Tables[0].Rows[i]["OfficeOfApplicationName"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["OfficeOfApplicationName"].ToString() : String.Empty;
                entity.DestinationCode_1 = dataSet.Tables[0].Rows[i]["DestinationCode_1"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["DestinationCode_1"].ToString() : String.Empty;
                entity.DestinationCode_2 = dataSet.Tables[0].Rows[i]["DestinationCode_2"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["DestinationCode_2"].ToString() : String.Empty;
                entity.DestinationCode_3 = dataSet.Tables[0].Rows[i]["DestinationCode_3"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["DestinationCode_3"].ToString() : String.Empty;
                entity.DestinationCode_4 = dataSet.Tables[0].Rows[i]["DestinationCode_4"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["DestinationCode_4"].ToString() : String.Empty;
                entity.DestinationCode_5 = dataSet.Tables[0].Rows[i]["DestinationCode_5"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["DestinationCode_5"].ToString() : String.Empty;
                entity.DestinationCode_6 = dataSet.Tables[0].Rows[i]["DestinationCode_6"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["DestinationCode_6"].ToString() : String.Empty;
                entity.DestinationCode_7 = dataSet.Tables[0].Rows[i]["DestinationCode_7"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["DestinationCode_7"].ToString() : String.Empty;
                entity.DestinationCode_8 = dataSet.Tables[0].Rows[i]["DestinationCode_8"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["DestinationCode_8"].ToString() : String.Empty;
                entity.DestinationCode_9 = dataSet.Tables[0].Rows[i]["DestinationCode_9"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["DestinationCode_9"].ToString() : String.Empty;
                entity.DestinationCode_10 = dataSet.Tables[0].Rows[i]["DestinationCode_10"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["DestinationCode_10"].ToString() : String.Empty;
                entity.Notes = dataSet.Tables[0].Rows[i]["Notes"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["Notes"].ToString() : String.Empty;
                entity.InputMessageID = dataSet.Tables[0].Rows[i]["InputMessageID"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["InputMessageID"].ToString() : String.Empty;
                entity.MessageTag = dataSet.Tables[0].Rows[i]["MessageTag"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["MessageTag"].ToString() : String.Empty;
                entity.IndexTag = dataSet.Tables[0].Rows[i]["IndexTag"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["IndexTag"].ToString() : String.Empty;

                collection.Add(entity);
            }
            return collection;
        }
		public static bool Find(List<VNACC_Category_OGAUser> collection, string officeOfApplicationCode)
        {
            foreach (VNACC_Category_OGAUser item in collection)
            {
                if (item.OfficeOfApplicationCode == officeOfApplicationCode)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACC_Category_OGAUser VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @OfficeOfApplicationCode, @OfficeOfApplicationName, @DestinationCode_1, @DestinationCode_2, @DestinationCode_3, @DestinationCode_4, @DestinationCode_5, @DestinationCode_6, @DestinationCode_7, @DestinationCode_8, @DestinationCode_9, @DestinationCode_10, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_OGAUser SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, OfficeOfApplicationName = @OfficeOfApplicationName, DestinationCode_1 = @DestinationCode_1, DestinationCode_2 = @DestinationCode_2, DestinationCode_3 = @DestinationCode_3, DestinationCode_4 = @DestinationCode_4, DestinationCode_5 = @DestinationCode_5, DestinationCode_6 = @DestinationCode_6, DestinationCode_7 = @DestinationCode_7, DestinationCode_8 = @DestinationCode_8, DestinationCode_9 = @DestinationCode_9, DestinationCode_10 = @DestinationCode_10, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE OfficeOfApplicationCode = @OfficeOfApplicationCode";
            string delete = "DELETE FROM t_VNACC_Category_OGAUser WHERE OfficeOfApplicationCode = @OfficeOfApplicationCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, "OfficeOfApplicationCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@OfficeOfApplicationName", SqlDbType.NVarChar, "OfficeOfApplicationName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_1", SqlDbType.VarChar, "DestinationCode_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_2", SqlDbType.VarChar, "DestinationCode_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_3", SqlDbType.VarChar, "DestinationCode_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_4", SqlDbType.VarChar, "DestinationCode_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_5", SqlDbType.VarChar, "DestinationCode_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_6", SqlDbType.VarChar, "DestinationCode_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_7", SqlDbType.VarChar, "DestinationCode_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_8", SqlDbType.VarChar, "DestinationCode_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_9", SqlDbType.VarChar, "DestinationCode_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_10", SqlDbType.VarChar, "DestinationCode_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, "OfficeOfApplicationCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@OfficeOfApplicationName", SqlDbType.NVarChar, "OfficeOfApplicationName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_1", SqlDbType.VarChar, "DestinationCode_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_2", SqlDbType.VarChar, "DestinationCode_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_3", SqlDbType.VarChar, "DestinationCode_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_4", SqlDbType.VarChar, "DestinationCode_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_5", SqlDbType.VarChar, "DestinationCode_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_6", SqlDbType.VarChar, "DestinationCode_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_7", SqlDbType.VarChar, "DestinationCode_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_8", SqlDbType.VarChar, "DestinationCode_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_9", SqlDbType.VarChar, "DestinationCode_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_10", SqlDbType.VarChar, "DestinationCode_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, "OfficeOfApplicationCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACC_Category_OGAUser VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @OfficeOfApplicationCode, @OfficeOfApplicationName, @DestinationCode_1, @DestinationCode_2, @DestinationCode_3, @DestinationCode_4, @DestinationCode_5, @DestinationCode_6, @DestinationCode_7, @DestinationCode_8, @DestinationCode_9, @DestinationCode_10, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_OGAUser SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, OfficeOfApplicationName = @OfficeOfApplicationName, DestinationCode_1 = @DestinationCode_1, DestinationCode_2 = @DestinationCode_2, DestinationCode_3 = @DestinationCode_3, DestinationCode_4 = @DestinationCode_4, DestinationCode_5 = @DestinationCode_5, DestinationCode_6 = @DestinationCode_6, DestinationCode_7 = @DestinationCode_7, DestinationCode_8 = @DestinationCode_8, DestinationCode_9 = @DestinationCode_9, DestinationCode_10 = @DestinationCode_10, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE OfficeOfApplicationCode = @OfficeOfApplicationCode";
            string delete = "DELETE FROM t_VNACC_Category_OGAUser WHERE OfficeOfApplicationCode = @OfficeOfApplicationCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, "OfficeOfApplicationCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@OfficeOfApplicationName", SqlDbType.NVarChar, "OfficeOfApplicationName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_1", SqlDbType.VarChar, "DestinationCode_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_2", SqlDbType.VarChar, "DestinationCode_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_3", SqlDbType.VarChar, "DestinationCode_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_4", SqlDbType.VarChar, "DestinationCode_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_5", SqlDbType.VarChar, "DestinationCode_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_6", SqlDbType.VarChar, "DestinationCode_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_7", SqlDbType.VarChar, "DestinationCode_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_8", SqlDbType.VarChar, "DestinationCode_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_9", SqlDbType.VarChar, "DestinationCode_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DestinationCode_10", SqlDbType.VarChar, "DestinationCode_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, "OfficeOfApplicationCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@OfficeOfApplicationName", SqlDbType.NVarChar, "OfficeOfApplicationName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_1", SqlDbType.VarChar, "DestinationCode_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_2", SqlDbType.VarChar, "DestinationCode_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_3", SqlDbType.VarChar, "DestinationCode_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_4", SqlDbType.VarChar, "DestinationCode_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_5", SqlDbType.VarChar, "DestinationCode_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_6", SqlDbType.VarChar, "DestinationCode_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_7", SqlDbType.VarChar, "DestinationCode_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_8", SqlDbType.VarChar, "DestinationCode_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_9", SqlDbType.VarChar, "DestinationCode_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DestinationCode_10", SqlDbType.VarChar, "DestinationCode_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, "OfficeOfApplicationCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACC_Category_OGAUser Load(string officeOfApplicationCode)
		{
			const string spName = "[dbo].[p_VNACC_Category_OGAUser_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, officeOfApplicationCode);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACC_Category_OGAUser> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACC_Category_OGAUser> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACC_Category_OGAUser> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_OGAUser_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_OGAUser_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_OGAUser_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_OGAUser_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertVNACC_Category_OGAUser(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string officeOfApplicationName, string destinationCode_1, string destinationCode_2, string destinationCode_3, string destinationCode_4, string destinationCode_5, string destinationCode_6, string destinationCode_7, string destinationCode_8, string destinationCode_9, string destinationCode_10, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_OGAUser entity = new VNACC_Category_OGAUser();	
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.OfficeOfApplicationName = officeOfApplicationName;
			entity.DestinationCode_1 = destinationCode_1;
			entity.DestinationCode_2 = destinationCode_2;
			entity.DestinationCode_3 = destinationCode_3;
			entity.DestinationCode_4 = destinationCode_4;
			entity.DestinationCode_5 = destinationCode_5;
			entity.DestinationCode_6 = destinationCode_6;
			entity.DestinationCode_7 = destinationCode_7;
			entity.DestinationCode_8 = destinationCode_8;
			entity.DestinationCode_9 = destinationCode_9;
			entity.DestinationCode_10 = destinationCode_10;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACC_Category_OGAUser_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, OfficeOfApplicationCode);
			db.AddInParameter(dbCommand, "@OfficeOfApplicationName", SqlDbType.NVarChar, OfficeOfApplicationName);
			db.AddInParameter(dbCommand, "@DestinationCode_1", SqlDbType.VarChar, DestinationCode_1);
			db.AddInParameter(dbCommand, "@DestinationCode_2", SqlDbType.VarChar, DestinationCode_2);
			db.AddInParameter(dbCommand, "@DestinationCode_3", SqlDbType.VarChar, DestinationCode_3);
			db.AddInParameter(dbCommand, "@DestinationCode_4", SqlDbType.VarChar, DestinationCode_4);
			db.AddInParameter(dbCommand, "@DestinationCode_5", SqlDbType.VarChar, DestinationCode_5);
			db.AddInParameter(dbCommand, "@DestinationCode_6", SqlDbType.VarChar, DestinationCode_6);
			db.AddInParameter(dbCommand, "@DestinationCode_7", SqlDbType.VarChar, DestinationCode_7);
			db.AddInParameter(dbCommand, "@DestinationCode_8", SqlDbType.VarChar, DestinationCode_8);
			db.AddInParameter(dbCommand, "@DestinationCode_9", SqlDbType.VarChar, DestinationCode_9);
			db.AddInParameter(dbCommand, "@DestinationCode_10", SqlDbType.VarChar, DestinationCode_10);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACC_Category_OGAUser> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_OGAUser item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACC_Category_OGAUser(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string officeOfApplicationCode, string officeOfApplicationName, string destinationCode_1, string destinationCode_2, string destinationCode_3, string destinationCode_4, string destinationCode_5, string destinationCode_6, string destinationCode_7, string destinationCode_8, string destinationCode_9, string destinationCode_10, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_OGAUser entity = new VNACC_Category_OGAUser();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.OfficeOfApplicationCode = officeOfApplicationCode;
			entity.OfficeOfApplicationName = officeOfApplicationName;
			entity.DestinationCode_1 = destinationCode_1;
			entity.DestinationCode_2 = destinationCode_2;
			entity.DestinationCode_3 = destinationCode_3;
			entity.DestinationCode_4 = destinationCode_4;
			entity.DestinationCode_5 = destinationCode_5;
			entity.DestinationCode_6 = destinationCode_6;
			entity.DestinationCode_7 = destinationCode_7;
			entity.DestinationCode_8 = destinationCode_8;
			entity.DestinationCode_9 = destinationCode_9;
			entity.DestinationCode_10 = destinationCode_10;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACC_Category_OGAUser_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, OfficeOfApplicationCode);
			db.AddInParameter(dbCommand, "@OfficeOfApplicationName", SqlDbType.NVarChar, OfficeOfApplicationName);
			db.AddInParameter(dbCommand, "@DestinationCode_1", SqlDbType.VarChar, DestinationCode_1);
			db.AddInParameter(dbCommand, "@DestinationCode_2", SqlDbType.VarChar, DestinationCode_2);
			db.AddInParameter(dbCommand, "@DestinationCode_3", SqlDbType.VarChar, DestinationCode_3);
			db.AddInParameter(dbCommand, "@DestinationCode_4", SqlDbType.VarChar, DestinationCode_4);
			db.AddInParameter(dbCommand, "@DestinationCode_5", SqlDbType.VarChar, DestinationCode_5);
			db.AddInParameter(dbCommand, "@DestinationCode_6", SqlDbType.VarChar, DestinationCode_6);
			db.AddInParameter(dbCommand, "@DestinationCode_7", SqlDbType.VarChar, DestinationCode_7);
			db.AddInParameter(dbCommand, "@DestinationCode_8", SqlDbType.VarChar, DestinationCode_8);
			db.AddInParameter(dbCommand, "@DestinationCode_9", SqlDbType.VarChar, DestinationCode_9);
			db.AddInParameter(dbCommand, "@DestinationCode_10", SqlDbType.VarChar, DestinationCode_10);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACC_Category_OGAUser> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_OGAUser item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACC_Category_OGAUser(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string officeOfApplicationCode, string officeOfApplicationName, string destinationCode_1, string destinationCode_2, string destinationCode_3, string destinationCode_4, string destinationCode_5, string destinationCode_6, string destinationCode_7, string destinationCode_8, string destinationCode_9, string destinationCode_10, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_OGAUser entity = new VNACC_Category_OGAUser();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.OfficeOfApplicationCode = officeOfApplicationCode;
			entity.OfficeOfApplicationName = officeOfApplicationName;
			entity.DestinationCode_1 = destinationCode_1;
			entity.DestinationCode_2 = destinationCode_2;
			entity.DestinationCode_3 = destinationCode_3;
			entity.DestinationCode_4 = destinationCode_4;
			entity.DestinationCode_5 = destinationCode_5;
			entity.DestinationCode_6 = destinationCode_6;
			entity.DestinationCode_7 = destinationCode_7;
			entity.DestinationCode_8 = destinationCode_8;
			entity.DestinationCode_9 = destinationCode_9;
			entity.DestinationCode_10 = destinationCode_10;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_OGAUser_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, OfficeOfApplicationCode);
			db.AddInParameter(dbCommand, "@OfficeOfApplicationName", SqlDbType.NVarChar, OfficeOfApplicationName);
			db.AddInParameter(dbCommand, "@DestinationCode_1", SqlDbType.VarChar, DestinationCode_1);
			db.AddInParameter(dbCommand, "@DestinationCode_2", SqlDbType.VarChar, DestinationCode_2);
			db.AddInParameter(dbCommand, "@DestinationCode_3", SqlDbType.VarChar, DestinationCode_3);
			db.AddInParameter(dbCommand, "@DestinationCode_4", SqlDbType.VarChar, DestinationCode_4);
			db.AddInParameter(dbCommand, "@DestinationCode_5", SqlDbType.VarChar, DestinationCode_5);
			db.AddInParameter(dbCommand, "@DestinationCode_6", SqlDbType.VarChar, DestinationCode_6);
			db.AddInParameter(dbCommand, "@DestinationCode_7", SqlDbType.VarChar, DestinationCode_7);
			db.AddInParameter(dbCommand, "@DestinationCode_8", SqlDbType.VarChar, DestinationCode_8);
			db.AddInParameter(dbCommand, "@DestinationCode_9", SqlDbType.VarChar, DestinationCode_9);
			db.AddInParameter(dbCommand, "@DestinationCode_10", SqlDbType.VarChar, DestinationCode_10);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACC_Category_OGAUser> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_OGAUser item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACC_Category_OGAUser(string officeOfApplicationCode)
		{
			VNACC_Category_OGAUser entity = new VNACC_Category_OGAUser();
			entity.OfficeOfApplicationCode = officeOfApplicationCode;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_OGAUser_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@OfficeOfApplicationCode", SqlDbType.VarChar, OfficeOfApplicationCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACC_Category_OGAUser> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_OGAUser item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}