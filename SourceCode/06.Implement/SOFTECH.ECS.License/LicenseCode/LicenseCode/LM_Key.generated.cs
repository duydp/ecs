﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace LicenseCode
{
	public partial class LM_Key : ICloneable
	{
		#region Properties.
		
		public Guid Id { set; get; }
		public Guid ModuleID { set; get; }
		public string SerialNumber { set; get; }
		public string ActivedKey { set; get; }
		public string ProductId { set; get; }
		public int Days { set; get; }
		public int RecordCount { set; get; }
		public string Status { set; get; }
		public string CustomerCode { set; get; }
		public string CustomerName { set; get; }
		public string CustomerAddress { set; get; }
		public string CustomerPhone { set; get; }
		public string CustomerEmail { set; get; }
		public string CustomerDescription { set; get; }
		public string MachineCode { set; get; }
		public DateTime ActivedDate { set; get; }
		public DateTime ExpiredDate { set; get; }
		public string RequestedBy { set; get; }
		public DateTime RequestedDate { set; get; }
		public DateTime LastCheckStatus { set; get; }
		public string Command { set; get; }
		public string Notice { set; get; }
		public string Notes { set; get; }
		public string CustomerField1 { set; get; }
		public string CustomerField2 { set; get; }
		public string CustomerField3 { set; get; }
		public string CustomerField4 { set; get; }
		public string CustomerField5 { set; get; }
		public string CustomerField6 { set; get; }
		public string CustomerField7 { set; get; }
		public string CustomerField8 { set; get; }
		public string CustomerField9 { set; get; }
		public string CustomerField10 { set; get; }
		public DateTime CreatedDate { set; get; }
		public string CreatedBy { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<LM_Key> ConvertToCollection(IDataReader reader)
		{
			List<LM_Key> collection = new List<LM_Key>();
			while (reader.Read())
			{
				LM_Key entity = new LM_Key();
				if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetGuid(reader.GetOrdinal("Id"));
				if (!reader.IsDBNull(reader.GetOrdinal("ModuleID"))) entity.ModuleID = reader.GetGuid(reader.GetOrdinal("ModuleID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SerialNumber"))) entity.SerialNumber = reader.GetString(reader.GetOrdinal("SerialNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActivedKey"))) entity.ActivedKey = reader.GetString(reader.GetOrdinal("ActivedKey"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProductId"))) entity.ProductId = reader.GetString(reader.GetOrdinal("ProductId"));
				if (!reader.IsDBNull(reader.GetOrdinal("Days"))) entity.Days = reader.GetInt32(reader.GetOrdinal("Days"));
				if (!reader.IsDBNull(reader.GetOrdinal("RecordCount"))) entity.RecordCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
				if (!reader.IsDBNull(reader.GetOrdinal("Status"))) entity.Status = reader.GetString(reader.GetOrdinal("Status"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerCode"))) entity.CustomerCode = reader.GetString(reader.GetOrdinal("CustomerCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerName"))) entity.CustomerName = reader.GetString(reader.GetOrdinal("CustomerName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerAddress"))) entity.CustomerAddress = reader.GetString(reader.GetOrdinal("CustomerAddress"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerPhone"))) entity.CustomerPhone = reader.GetString(reader.GetOrdinal("CustomerPhone"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerEmail"))) entity.CustomerEmail = reader.GetString(reader.GetOrdinal("CustomerEmail"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerDescription"))) entity.CustomerDescription = reader.GetString(reader.GetOrdinal("CustomerDescription"));
				if (!reader.IsDBNull(reader.GetOrdinal("MachineCode"))) entity.MachineCode = reader.GetString(reader.GetOrdinal("MachineCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActivedDate"))) entity.ActivedDate = reader.GetDateTime(reader.GetOrdinal("ActivedDate"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExpiredDate"))) entity.ExpiredDate = reader.GetDateTime(reader.GetOrdinal("ExpiredDate"));
				if (!reader.IsDBNull(reader.GetOrdinal("RequestedBy"))) entity.RequestedBy = reader.GetString(reader.GetOrdinal("RequestedBy"));
				if (!reader.IsDBNull(reader.GetOrdinal("RequestedDate"))) entity.RequestedDate = reader.GetDateTime(reader.GetOrdinal("RequestedDate"));
				if (!reader.IsDBNull(reader.GetOrdinal("LastCheckStatus"))) entity.LastCheckStatus = reader.GetDateTime(reader.GetOrdinal("LastCheckStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("Command"))) entity.Command = reader.GetString(reader.GetOrdinal("Command"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notice"))) entity.Notice = reader.GetString(reader.GetOrdinal("Notice"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField1"))) entity.CustomerField1 = reader.GetString(reader.GetOrdinal("CustomerField1"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField2"))) entity.CustomerField2 = reader.GetString(reader.GetOrdinal("CustomerField2"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField3"))) entity.CustomerField3 = reader.GetString(reader.GetOrdinal("CustomerField3"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField4"))) entity.CustomerField4 = reader.GetString(reader.GetOrdinal("CustomerField4"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField5"))) entity.CustomerField5 = reader.GetString(reader.GetOrdinal("CustomerField5"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField6"))) entity.CustomerField6 = reader.GetString(reader.GetOrdinal("CustomerField6"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField7"))) entity.CustomerField7 = reader.GetString(reader.GetOrdinal("CustomerField7"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField8"))) entity.CustomerField8 = reader.GetString(reader.GetOrdinal("CustomerField8"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField9"))) entity.CustomerField9 = reader.GetString(reader.GetOrdinal("CustomerField9"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField10"))) entity.CustomerField10 = reader.GetString(reader.GetOrdinal("CustomerField10"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatedBy"))) entity.CreatedBy = reader.GetString(reader.GetOrdinal("CreatedBy"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<LM_Key> collection, Guid id)
        {
            foreach (LM_Key item in collection)
            {
                if (item.Id == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_LM_Keys VALUES(@Id, @ModuleID, @SerialNumber, @ActivedKey, @ProductId, @Days, @RecordCount, @Status, @CustomerCode, @CustomerName, @CustomerAddress, @CustomerPhone, @CustomerEmail, @CustomerDescription, @MachineCode, @ActivedDate, @ExpiredDate, @RequestedBy, @RequestedDate, @LastCheckStatus, @Command, @Notice, @Notes, @CustomerField1, @CustomerField2, @CustomerField3, @CustomerField4, @CustomerField5, @CustomerField6, @CustomerField7, @CustomerField8, @CustomerField9, @CustomerField10, @CreatedDate, @CreatedBy)";
            string update = "UPDATE t_LM_Keys SET ModuleID = @ModuleID, SerialNumber = @SerialNumber, ActivedKey = @ActivedKey, ProductId = @ProductId, Days = @Days, RecordCount = @RecordCount, Status = @Status, CustomerCode = @CustomerCode, CustomerName = @CustomerName, CustomerAddress = @CustomerAddress, CustomerPhone = @CustomerPhone, CustomerEmail = @CustomerEmail, CustomerDescription = @CustomerDescription, MachineCode = @MachineCode, ActivedDate = @ActivedDate, ExpiredDate = @ExpiredDate, RequestedBy = @RequestedBy, RequestedDate = @RequestedDate, LastCheckStatus = @LastCheckStatus, Command = @Command, Notice = @Notice, Notes = @Notes, CustomerField1 = @CustomerField1, CustomerField2 = @CustomerField2, CustomerField3 = @CustomerField3, CustomerField4 = @CustomerField4, CustomerField5 = @CustomerField5, CustomerField6 = @CustomerField6, CustomerField7 = @CustomerField7, CustomerField8 = @CustomerField8, CustomerField9 = @CustomerField9, CustomerField10 = @CustomerField10, CreatedDate = @CreatedDate, CreatedBy = @CreatedBy WHERE Id = @Id";
            string delete = "DELETE FROM t_LM_Keys WHERE Id = @Id";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@Id", SqlDbType.UniqueIdentifier, "Id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ModuleID", SqlDbType.UniqueIdentifier, "ModuleID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SerialNumber", SqlDbType.VarChar, "SerialNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActivedKey", SqlDbType.VarChar, "ActivedKey", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProductId", SqlDbType.VarChar, "ProductId", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Days", SqlDbType.Int, "Days", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@RecordCount", SqlDbType.Int, "RecordCount", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Status", SqlDbType.VarChar, "Status", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerPhone", SqlDbType.NVarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerEmail", SqlDbType.NVarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerDescription", SqlDbType.NVarChar, "CustomerDescription", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MachineCode", SqlDbType.NVarChar, "MachineCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActivedDate", SqlDbType.DateTime, "ActivedDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExpiredDate", SqlDbType.DateTime, "ExpiredDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@RequestedBy", SqlDbType.VarChar, "RequestedBy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@RequestedDate", SqlDbType.DateTime, "RequestedDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Command", SqlDbType.NVarChar, "Command", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notice", SqlDbType.NVarChar, "Notice", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField1", SqlDbType.NVarChar, "CustomerField1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField2", SqlDbType.NVarChar, "CustomerField2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField3", SqlDbType.NVarChar, "CustomerField3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField4", SqlDbType.NVarChar, "CustomerField4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField5", SqlDbType.NVarChar, "CustomerField5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField6", SqlDbType.NVarChar, "CustomerField6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField7", SqlDbType.NVarChar, "CustomerField7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField8", SqlDbType.NVarChar, "CustomerField8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField9", SqlDbType.NVarChar, "CustomerField9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField10", SqlDbType.NVarChar, "CustomerField10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedBy", SqlDbType.VarChar, "CreatedBy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@Id", SqlDbType.UniqueIdentifier, "Id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ModuleID", SqlDbType.UniqueIdentifier, "ModuleID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SerialNumber", SqlDbType.VarChar, "SerialNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActivedKey", SqlDbType.VarChar, "ActivedKey", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProductId", SqlDbType.VarChar, "ProductId", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Days", SqlDbType.Int, "Days", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@RecordCount", SqlDbType.Int, "RecordCount", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Status", SqlDbType.VarChar, "Status", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerPhone", SqlDbType.NVarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerEmail", SqlDbType.NVarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerDescription", SqlDbType.NVarChar, "CustomerDescription", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MachineCode", SqlDbType.NVarChar, "MachineCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActivedDate", SqlDbType.DateTime, "ActivedDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExpiredDate", SqlDbType.DateTime, "ExpiredDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@RequestedBy", SqlDbType.VarChar, "RequestedBy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@RequestedDate", SqlDbType.DateTime, "RequestedDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Command", SqlDbType.NVarChar, "Command", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notice", SqlDbType.NVarChar, "Notice", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField1", SqlDbType.NVarChar, "CustomerField1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField2", SqlDbType.NVarChar, "CustomerField2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField3", SqlDbType.NVarChar, "CustomerField3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField4", SqlDbType.NVarChar, "CustomerField4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField5", SqlDbType.NVarChar, "CustomerField5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField6", SqlDbType.NVarChar, "CustomerField6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField7", SqlDbType.NVarChar, "CustomerField7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField8", SqlDbType.NVarChar, "CustomerField8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField9", SqlDbType.NVarChar, "CustomerField9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField10", SqlDbType.NVarChar, "CustomerField10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedBy", SqlDbType.VarChar, "CreatedBy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@Id", SqlDbType.UniqueIdentifier, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_LM_Keys VALUES(@Id, @ModuleID, @SerialNumber, @ActivedKey, @ProductId, @Days, @RecordCount, @Status, @CustomerCode, @CustomerName, @CustomerAddress, @CustomerPhone, @CustomerEmail, @CustomerDescription, @MachineCode, @ActivedDate, @ExpiredDate, @RequestedBy, @RequestedDate, @LastCheckStatus, @Command, @Notice, @Notes, @CustomerField1, @CustomerField2, @CustomerField3, @CustomerField4, @CustomerField5, @CustomerField6, @CustomerField7, @CustomerField8, @CustomerField9, @CustomerField10, @CreatedDate, @CreatedBy)";
            string update = "UPDATE t_LM_Keys SET ModuleID = @ModuleID, SerialNumber = @SerialNumber, ActivedKey = @ActivedKey, ProductId = @ProductId, Days = @Days, RecordCount = @RecordCount, Status = @Status, CustomerCode = @CustomerCode, CustomerName = @CustomerName, CustomerAddress = @CustomerAddress, CustomerPhone = @CustomerPhone, CustomerEmail = @CustomerEmail, CustomerDescription = @CustomerDescription, MachineCode = @MachineCode, ActivedDate = @ActivedDate, ExpiredDate = @ExpiredDate, RequestedBy = @RequestedBy, RequestedDate = @RequestedDate, LastCheckStatus = @LastCheckStatus, Command = @Command, Notice = @Notice, Notes = @Notes, CustomerField1 = @CustomerField1, CustomerField2 = @CustomerField2, CustomerField3 = @CustomerField3, CustomerField4 = @CustomerField4, CustomerField5 = @CustomerField5, CustomerField6 = @CustomerField6, CustomerField7 = @CustomerField7, CustomerField8 = @CustomerField8, CustomerField9 = @CustomerField9, CustomerField10 = @CustomerField10, CreatedDate = @CreatedDate, CreatedBy = @CreatedBy WHERE Id = @Id";
            string delete = "DELETE FROM t_LM_Keys WHERE Id = @Id";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@Id", SqlDbType.UniqueIdentifier, "Id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ModuleID", SqlDbType.UniqueIdentifier, "ModuleID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SerialNumber", SqlDbType.VarChar, "SerialNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActivedKey", SqlDbType.VarChar, "ActivedKey", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProductId", SqlDbType.VarChar, "ProductId", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Days", SqlDbType.Int, "Days", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@RecordCount", SqlDbType.Int, "RecordCount", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Status", SqlDbType.VarChar, "Status", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerPhone", SqlDbType.NVarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerEmail", SqlDbType.NVarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerDescription", SqlDbType.NVarChar, "CustomerDescription", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MachineCode", SqlDbType.NVarChar, "MachineCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActivedDate", SqlDbType.DateTime, "ActivedDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExpiredDate", SqlDbType.DateTime, "ExpiredDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@RequestedBy", SqlDbType.VarChar, "RequestedBy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@RequestedDate", SqlDbType.DateTime, "RequestedDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Command", SqlDbType.NVarChar, "Command", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notice", SqlDbType.NVarChar, "Notice", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField1", SqlDbType.NVarChar, "CustomerField1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField2", SqlDbType.NVarChar, "CustomerField2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField3", SqlDbType.NVarChar, "CustomerField3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField4", SqlDbType.NVarChar, "CustomerField4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField5", SqlDbType.NVarChar, "CustomerField5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField6", SqlDbType.NVarChar, "CustomerField6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField7", SqlDbType.NVarChar, "CustomerField7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField8", SqlDbType.NVarChar, "CustomerField8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField9", SqlDbType.NVarChar, "CustomerField9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField10", SqlDbType.NVarChar, "CustomerField10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedBy", SqlDbType.VarChar, "CreatedBy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@Id", SqlDbType.UniqueIdentifier, "Id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ModuleID", SqlDbType.UniqueIdentifier, "ModuleID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SerialNumber", SqlDbType.VarChar, "SerialNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActivedKey", SqlDbType.VarChar, "ActivedKey", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProductId", SqlDbType.VarChar, "ProductId", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Days", SqlDbType.Int, "Days", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@RecordCount", SqlDbType.Int, "RecordCount", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Status", SqlDbType.VarChar, "Status", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerPhone", SqlDbType.NVarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerEmail", SqlDbType.NVarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerDescription", SqlDbType.NVarChar, "CustomerDescription", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MachineCode", SqlDbType.NVarChar, "MachineCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActivedDate", SqlDbType.DateTime, "ActivedDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExpiredDate", SqlDbType.DateTime, "ExpiredDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@RequestedBy", SqlDbType.VarChar, "RequestedBy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@RequestedDate", SqlDbType.DateTime, "RequestedDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Command", SqlDbType.NVarChar, "Command", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notice", SqlDbType.NVarChar, "Notice", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField1", SqlDbType.NVarChar, "CustomerField1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField2", SqlDbType.NVarChar, "CustomerField2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField3", SqlDbType.NVarChar, "CustomerField3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField4", SqlDbType.NVarChar, "CustomerField4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField5", SqlDbType.NVarChar, "CustomerField5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField6", SqlDbType.NVarChar, "CustomerField6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField7", SqlDbType.NVarChar, "CustomerField7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField8", SqlDbType.NVarChar, "CustomerField8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField9", SqlDbType.NVarChar, "CustomerField9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField10", SqlDbType.NVarChar, "CustomerField10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedBy", SqlDbType.VarChar, "CreatedBy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@Id", SqlDbType.UniqueIdentifier, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static LM_Key Load(Guid id)
		{
			const string spName = "[dbo].[p_LM_Key_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id", SqlDbType.UniqueIdentifier, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<LM_Key> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<LM_Key> SelectCollectionAll(Guid moduleID)
		{
			IDataReader reader = SelectReaderAll(moduleID);
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<LM_Key> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll(Guid moduleID)
        {
            const string spName = "[dbo].[p_LM_Key_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ModuleID", SqlDbType.UniqueIdentifier, moduleID);
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Key_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll(Guid moduleID)
        {
            const string spName = "[dbo].[p_LM_Key_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddInParameter(dbCommand, "@ModuleID", SqlDbType.UniqueIdentifier, moduleID);
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Key_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertLM_Key(Guid id, Guid moduleID, string serialNumber, string activedKey, string productId, int days, int recordCount, string status, string customerCode, string customerName, string customerAddress, string customerPhone, string customerEmail, string customerDescription, string machineCode, DateTime activedDate, DateTime expiredDate, string requestedBy, DateTime requestedDate, DateTime lastCheckStatus, string command, string notice, string notes, string customerField1, string customerField2, string customerField3, string customerField4, string customerField5, string customerField6, string customerField7, string customerField8, string customerField9, string customerField10, DateTime createdDate, string createdBy)
		{
			LM_Key entity = new LM_Key();	
            entity.Id = id;
			entity.ModuleID = moduleID;
            entity.SerialNumber = serialNumber;
            entity.ActivedKey = activedKey;
			entity.ProductId = productId;
			entity.Days = days;
			entity.RecordCount = recordCount;
			entity.Status = status;
			entity.CustomerCode = customerCode;
			entity.CustomerName = customerName;
			entity.CustomerAddress = customerAddress;
			entity.CustomerPhone = customerPhone;
			entity.CustomerEmail = customerEmail;
			entity.CustomerDescription = customerDescription;
			entity.MachineCode = machineCode;
			entity.ActivedDate = activedDate;
			entity.ExpiredDate = expiredDate;
			entity.RequestedBy = requestedBy;
			entity.RequestedDate = requestedDate;
			entity.LastCheckStatus = lastCheckStatus;
			entity.Command = command;
			entity.Notice = notice;
			entity.Notes = notes;
			entity.CustomerField1 = customerField1;
			entity.CustomerField2 = customerField2;
			entity.CustomerField3 = customerField3;
			entity.CustomerField4 = customerField4;
			entity.CustomerField5 = customerField5;
			entity.CustomerField6 = customerField6;
			entity.CustomerField7 = customerField7;
			entity.CustomerField8 = customerField8;
			entity.CustomerField9 = customerField9;
			entity.CustomerField10 = customerField10;
			entity.CreatedDate = createdDate;
			entity.CreatedBy = createdBy;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LM_Key_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Id", SqlDbType.UniqueIdentifier, Id);
			db.AddInParameter(dbCommand, "@ModuleID", SqlDbType.UniqueIdentifier, ModuleID);
			db.AddInParameter(dbCommand, "@SerialNumber", SqlDbType.VarChar, SerialNumber);
			db.AddInParameter(dbCommand, "@ActivedKey", SqlDbType.VarChar, ActivedKey);
			db.AddInParameter(dbCommand, "@ProductId", SqlDbType.VarChar, ProductId);
			db.AddInParameter(dbCommand, "@Days", SqlDbType.Int, Days);
			db.AddInParameter(dbCommand, "@RecordCount", SqlDbType.Int, RecordCount);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.VarChar, Status);
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerAddress", SqlDbType.NVarChar, CustomerAddress);
			db.AddInParameter(dbCommand, "@CustomerPhone", SqlDbType.NVarChar, CustomerPhone);
			db.AddInParameter(dbCommand, "@CustomerEmail", SqlDbType.NVarChar, CustomerEmail);
			db.AddInParameter(dbCommand, "@CustomerDescription", SqlDbType.NVarChar, CustomerDescription);
			db.AddInParameter(dbCommand, "@MachineCode", SqlDbType.NVarChar, MachineCode);
			db.AddInParameter(dbCommand, "@ActivedDate", SqlDbType.DateTime, ActivedDate.Year <= 1753 ? DBNull.Value : (object) ActivedDate);
			db.AddInParameter(dbCommand, "@ExpiredDate", SqlDbType.DateTime, ExpiredDate.Year <= 1753 ? DBNull.Value : (object) ExpiredDate);
			db.AddInParameter(dbCommand, "@RequestedBy", SqlDbType.VarChar, RequestedBy);
			db.AddInParameter(dbCommand, "@RequestedDate", SqlDbType.DateTime, RequestedDate.Year <= 1753 ? DBNull.Value : (object) RequestedDate);
			db.AddInParameter(dbCommand, "@LastCheckStatus", SqlDbType.DateTime, LastCheckStatus.Year <= 1753 ? DBNull.Value : (object) LastCheckStatus);
			db.AddInParameter(dbCommand, "@Command", SqlDbType.NVarChar, Command);
			db.AddInParameter(dbCommand, "@Notice", SqlDbType.NVarChar, Notice);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@CustomerField1", SqlDbType.NVarChar, CustomerField1);
			db.AddInParameter(dbCommand, "@CustomerField2", SqlDbType.NVarChar, CustomerField2);
			db.AddInParameter(dbCommand, "@CustomerField3", SqlDbType.NVarChar, CustomerField3);
			db.AddInParameter(dbCommand, "@CustomerField4", SqlDbType.NVarChar, CustomerField4);
			db.AddInParameter(dbCommand, "@CustomerField5", SqlDbType.NVarChar, CustomerField5);
			db.AddInParameter(dbCommand, "@CustomerField6", SqlDbType.NVarChar, CustomerField6);
			db.AddInParameter(dbCommand, "@CustomerField7", SqlDbType.NVarChar, CustomerField7);
			db.AddInParameter(dbCommand, "@CustomerField8", SqlDbType.NVarChar, CustomerField8);
			db.AddInParameter(dbCommand, "@CustomerField9", SqlDbType.NVarChar, CustomerField9);
			db.AddInParameter(dbCommand, "@CustomerField10", SqlDbType.NVarChar, CustomerField10);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year <= 1753 ? DBNull.Value : (object) CreatedDate);
			db.AddInParameter(dbCommand, "@CreatedBy", SqlDbType.VarChar, CreatedBy);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<LM_Key> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Key item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateLM_Key(Guid id, Guid moduleID, string serialNumber, string activedKey, string productId, int days, int recordCount, string status, string customerCode, string customerName, string customerAddress, string customerPhone, string customerEmail, string customerDescription, string machineCode, DateTime activedDate, DateTime expiredDate, string requestedBy, DateTime requestedDate, DateTime lastCheckStatus, string command, string notice, string notes, string customerField1, string customerField2, string customerField3, string customerField4, string customerField5, string customerField6, string customerField7, string customerField8, string customerField9, string customerField10, DateTime createdDate, string createdBy)
		{
			LM_Key entity = new LM_Key();			
			entity.Id = id;
			entity.ModuleID = moduleID;
			entity.SerialNumber = serialNumber;
			entity.ActivedKey = activedKey;
			entity.ProductId = productId;
			entity.Days = days;
			entity.RecordCount = recordCount;
			entity.Status = status;
			entity.CustomerCode = customerCode;
			entity.CustomerName = customerName;
			entity.CustomerAddress = customerAddress;
			entity.CustomerPhone = customerPhone;
			entity.CustomerEmail = customerEmail;
			entity.CustomerDescription = customerDescription;
			entity.MachineCode = machineCode;
			entity.ActivedDate = activedDate;
			entity.ExpiredDate = expiredDate;
			entity.RequestedBy = requestedBy;
			entity.RequestedDate = requestedDate;
			entity.LastCheckStatus = lastCheckStatus;
			entity.Command = command;
			entity.Notice = notice;
			entity.Notes = notes;
			entity.CustomerField1 = customerField1;
			entity.CustomerField2 = customerField2;
			entity.CustomerField3 = customerField3;
			entity.CustomerField4 = customerField4;
			entity.CustomerField5 = customerField5;
			entity.CustomerField6 = customerField6;
			entity.CustomerField7 = customerField7;
			entity.CustomerField8 = customerField8;
			entity.CustomerField9 = customerField9;
			entity.CustomerField10 = customerField10;
			entity.CreatedDate = createdDate;
			entity.CreatedBy = createdBy;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LM_Key_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Id", SqlDbType.UniqueIdentifier, Id);
			db.AddInParameter(dbCommand, "@ModuleID", SqlDbType.UniqueIdentifier, ModuleID);
			db.AddInParameter(dbCommand, "@SerialNumber", SqlDbType.VarChar, SerialNumber);
			db.AddInParameter(dbCommand, "@ActivedKey", SqlDbType.VarChar, ActivedKey);
			db.AddInParameter(dbCommand, "@ProductId", SqlDbType.VarChar, ProductId);
			db.AddInParameter(dbCommand, "@Days", SqlDbType.Int, Days);
			db.AddInParameter(dbCommand, "@RecordCount", SqlDbType.Int, RecordCount);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.VarChar, Status);
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerAddress", SqlDbType.NVarChar, CustomerAddress);
			db.AddInParameter(dbCommand, "@CustomerPhone", SqlDbType.NVarChar, CustomerPhone);
			db.AddInParameter(dbCommand, "@CustomerEmail", SqlDbType.NVarChar, CustomerEmail);
			db.AddInParameter(dbCommand, "@CustomerDescription", SqlDbType.NVarChar, CustomerDescription);
			db.AddInParameter(dbCommand, "@MachineCode", SqlDbType.NVarChar, MachineCode);
			db.AddInParameter(dbCommand, "@ActivedDate", SqlDbType.DateTime, ActivedDate.Year <= 1753 ? DBNull.Value : (object) ActivedDate);
			db.AddInParameter(dbCommand, "@ExpiredDate", SqlDbType.DateTime, ExpiredDate.Year <= 1753 ? DBNull.Value : (object) ExpiredDate);
			db.AddInParameter(dbCommand, "@RequestedBy", SqlDbType.VarChar, RequestedBy);
			db.AddInParameter(dbCommand, "@RequestedDate", SqlDbType.DateTime, RequestedDate.Year <= 1753 ? DBNull.Value : (object) RequestedDate);
			db.AddInParameter(dbCommand, "@LastCheckStatus", SqlDbType.DateTime, LastCheckStatus.Year <= 1753 ? DBNull.Value : (object) LastCheckStatus);
			db.AddInParameter(dbCommand, "@Command", SqlDbType.NVarChar, Command);
			db.AddInParameter(dbCommand, "@Notice", SqlDbType.NVarChar, Notice);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@CustomerField1", SqlDbType.NVarChar, CustomerField1);
			db.AddInParameter(dbCommand, "@CustomerField2", SqlDbType.NVarChar, CustomerField2);
			db.AddInParameter(dbCommand, "@CustomerField3", SqlDbType.NVarChar, CustomerField3);
			db.AddInParameter(dbCommand, "@CustomerField4", SqlDbType.NVarChar, CustomerField4);
			db.AddInParameter(dbCommand, "@CustomerField5", SqlDbType.NVarChar, CustomerField5);
			db.AddInParameter(dbCommand, "@CustomerField6", SqlDbType.NVarChar, CustomerField6);
			db.AddInParameter(dbCommand, "@CustomerField7", SqlDbType.NVarChar, CustomerField7);
			db.AddInParameter(dbCommand, "@CustomerField8", SqlDbType.NVarChar, CustomerField8);
			db.AddInParameter(dbCommand, "@CustomerField9", SqlDbType.NVarChar, CustomerField9);
			db.AddInParameter(dbCommand, "@CustomerField10", SqlDbType.NVarChar, CustomerField10);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year <= 1753 ? DBNull.Value : (object) CreatedDate);
			db.AddInParameter(dbCommand, "@CreatedBy", SqlDbType.VarChar, CreatedBy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<LM_Key> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Key item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateLM_Key(Guid id, Guid moduleID, string serialNumber, string activedKey, string productId, int days, int recordCount, string status, string customerCode, string customerName, string customerAddress, string customerPhone, string customerEmail, string customerDescription, string machineCode, DateTime activedDate, DateTime expiredDate, string requestedBy, DateTime requestedDate, DateTime lastCheckStatus, string command, string notice, string notes, string customerField1, string customerField2, string customerField3, string customerField4, string customerField5, string customerField6, string customerField7, string customerField8, string customerField9, string customerField10, DateTime createdDate, string createdBy)
		{
			LM_Key entity = new LM_Key();			
			entity.Id = id;
			entity.ModuleID = moduleID;
			entity.SerialNumber = serialNumber;
			entity.ActivedKey = activedKey;
			entity.ProductId = productId;
			entity.Days = days;
			entity.RecordCount = recordCount;
			entity.Status = status;
			entity.CustomerCode = customerCode;
			entity.CustomerName = customerName;
			entity.CustomerAddress = customerAddress;
			entity.CustomerPhone = customerPhone;
			entity.CustomerEmail = customerEmail;
			entity.CustomerDescription = customerDescription;
			entity.MachineCode = machineCode;
			entity.ActivedDate = activedDate;
			entity.ExpiredDate = expiredDate;
			entity.RequestedBy = requestedBy;
			entity.RequestedDate = requestedDate;
			entity.LastCheckStatus = lastCheckStatus;
			entity.Command = command;
			entity.Notice = notice;
			entity.Notes = notes;
			entity.CustomerField1 = customerField1;
			entity.CustomerField2 = customerField2;
			entity.CustomerField3 = customerField3;
			entity.CustomerField4 = customerField4;
			entity.CustomerField5 = customerField5;
			entity.CustomerField6 = customerField6;
			entity.CustomerField7 = customerField7;
			entity.CustomerField8 = customerField8;
			entity.CustomerField9 = customerField9;
			entity.CustomerField10 = customerField10;
			entity.CreatedDate = createdDate;
			entity.CreatedBy = createdBy;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Key_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Id", SqlDbType.UniqueIdentifier, Id);
			db.AddInParameter(dbCommand, "@ModuleID", SqlDbType.UniqueIdentifier, ModuleID);
			db.AddInParameter(dbCommand, "@SerialNumber", SqlDbType.VarChar, SerialNumber);
			db.AddInParameter(dbCommand, "@ActivedKey", SqlDbType.VarChar, ActivedKey);
			db.AddInParameter(dbCommand, "@ProductId", SqlDbType.VarChar, ProductId);
			db.AddInParameter(dbCommand, "@Days", SqlDbType.Int, Days);
			db.AddInParameter(dbCommand, "@RecordCount", SqlDbType.Int, RecordCount);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.VarChar, Status);
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerAddress", SqlDbType.NVarChar, CustomerAddress);
			db.AddInParameter(dbCommand, "@CustomerPhone", SqlDbType.NVarChar, CustomerPhone);
			db.AddInParameter(dbCommand, "@CustomerEmail", SqlDbType.NVarChar, CustomerEmail);
			db.AddInParameter(dbCommand, "@CustomerDescription", SqlDbType.NVarChar, CustomerDescription);
			db.AddInParameter(dbCommand, "@MachineCode", SqlDbType.NVarChar, MachineCode);
			db.AddInParameter(dbCommand, "@ActivedDate", SqlDbType.DateTime, ActivedDate.Year <= 1753 ? DBNull.Value : (object) ActivedDate);
			db.AddInParameter(dbCommand, "@ExpiredDate", SqlDbType.DateTime, ExpiredDate.Year <= 1753 ? DBNull.Value : (object) ExpiredDate);
			db.AddInParameter(dbCommand, "@RequestedBy", SqlDbType.VarChar, RequestedBy);
			db.AddInParameter(dbCommand, "@RequestedDate", SqlDbType.DateTime, RequestedDate.Year <= 1753 ? DBNull.Value : (object) RequestedDate);
			db.AddInParameter(dbCommand, "@LastCheckStatus", SqlDbType.DateTime, LastCheckStatus.Year <= 1753 ? DBNull.Value : (object) LastCheckStatus);
			db.AddInParameter(dbCommand, "@Command", SqlDbType.NVarChar, Command);
			db.AddInParameter(dbCommand, "@Notice", SqlDbType.NVarChar, Notice);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@CustomerField1", SqlDbType.NVarChar, CustomerField1);
			db.AddInParameter(dbCommand, "@CustomerField2", SqlDbType.NVarChar, CustomerField2);
			db.AddInParameter(dbCommand, "@CustomerField3", SqlDbType.NVarChar, CustomerField3);
			db.AddInParameter(dbCommand, "@CustomerField4", SqlDbType.NVarChar, CustomerField4);
			db.AddInParameter(dbCommand, "@CustomerField5", SqlDbType.NVarChar, CustomerField5);
			db.AddInParameter(dbCommand, "@CustomerField6", SqlDbType.NVarChar, CustomerField6);
			db.AddInParameter(dbCommand, "@CustomerField7", SqlDbType.NVarChar, CustomerField7);
			db.AddInParameter(dbCommand, "@CustomerField8", SqlDbType.NVarChar, CustomerField8);
			db.AddInParameter(dbCommand, "@CustomerField9", SqlDbType.NVarChar, CustomerField9);
			db.AddInParameter(dbCommand, "@CustomerField10", SqlDbType.NVarChar, CustomerField10);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year <= 1753 ? DBNull.Value : (object) CreatedDate);
			db.AddInParameter(dbCommand, "@CreatedBy", SqlDbType.VarChar, CreatedBy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<LM_Key> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Key item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteLM_Key(Guid id)
		{
			LM_Key entity = new LM_Key();
			entity.Id = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Key_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id", SqlDbType.UniqueIdentifier, Id);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LM_Key_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<LM_Key> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Key item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}