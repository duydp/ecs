/*
Run this script on:

        192.168.72.151.ECS_TQDT_GC_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.151.ECS_TQDT_GC_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 06/19/2012 10:18:07 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[t_KDT_AnDinhThue]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue] DROP
CONSTRAINT [FK_t_KDT_AnDinhThue_t_KDT_ToKhaiMauDich]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_AnDinhThue]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [TKMD_ID] [bigint] NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [TKMD_Ref] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [SoQuyetDinh] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [NgayQuyetDinh] [datetime] NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [NgayHetHan] [datetime] NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [TaiKhoanKhoBac] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [TenKhoBac] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_DBDL_Status]'
GO
CREATE TABLE [dbo].[t_KDT_DBDL_Status]
(
[GUIDSTR] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MSG_STATUS] [int] NULL,
[MSG_TYPE] [int] NULL,
[CREATE_TIME] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_GC_DBDL_Status] on [dbo].[t_KDT_DBDL_Status]'
GO
ALTER TABLE [dbo].[t_KDT_DBDL_Status] ADD CONSTRAINT [PK_t_KDT_GC_DBDL_Status] PRIMARY KEY CLUSTERED  ([GUIDSTR])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_Insert]
	@GUIDSTR nvarchar(36),
	@MSG_STATUS int,
	@MSG_TYPE int,
	@CREATE_TIME datetime
AS
INSERT INTO [dbo].[t_KDT_DBDL_Status]
(
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
)
VALUES
(
	@GUIDSTR,
	@MSG_STATUS,
	@MSG_TYPE,
	@CREATE_TIME
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_Update]
	@GUIDSTR nvarchar(36),
	@MSG_STATUS int,
	@MSG_TYPE int,
	@CREATE_TIME datetime
AS

UPDATE
	[dbo].[t_KDT_DBDL_Status]
SET
	[MSG_STATUS] = @MSG_STATUS,
	[MSG_TYPE] = @MSG_TYPE,
	[CREATE_TIME] = @CREATE_TIME
WHERE
	[GUIDSTR] = @GUIDSTR

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_InsertUpdate]
	@GUIDSTR nvarchar(36),
	@MSG_STATUS int,
	@MSG_TYPE int,
	@CREATE_TIME datetime
AS
IF EXISTS(SELECT [GUIDSTR] FROM [dbo].[t_KDT_DBDL_Status] WHERE [GUIDSTR] = @GUIDSTR)
	BEGIN
		UPDATE
			[dbo].[t_KDT_DBDL_Status] 
		SET
			[MSG_STATUS] = @MSG_STATUS,
			[MSG_TYPE] = @MSG_TYPE,
			[CREATE_TIME] = @CREATE_TIME
		WHERE
			[GUIDSTR] = @GUIDSTR
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_DBDL_Status]
	(
			[GUIDSTR],
			[MSG_STATUS],
			[MSG_TYPE],
			[CREATE_TIME]
	)
	VALUES
	(
			@GUIDSTR,
			@MSG_STATUS,
			@MSG_TYPE,
			@CREATE_TIME
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_Delete]
	@GUIDSTR nvarchar(36)
AS

DELETE FROM 
	[dbo].[t_KDT_DBDL_Status]
WHERE
	[GUIDSTR] = @GUIDSTR

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_Load]
	@GUIDSTR nvarchar(36)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
FROM
	[dbo].[t_KDT_DBDL_Status]
WHERE
	[GUIDSTR] = @GUIDSTR
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
FROM
	[dbo].[t_KDT_DBDL_Status]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_DBDL_KetQuaXuLy]'
GO
CREATE TABLE [dbo].[t_KDT_DBDL_KetQuaXuLy]
(
[ID] [bigint] NOT NULL,
[SoToKhai] [bigint] NULL,
[MaDoanhNghiep] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayDongBo] [datetime] NULL,
[TrangThai] [int] NULL,
[GhiChu] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUIDSTR] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_DBDL_KetQuaXuLy] on [dbo].[t_KDT_DBDL_KetQuaXuLy]'
GO
ALTER TABLE [dbo].[t_KDT_DBDL_KetQuaXuLy] ADD CONSTRAINT [PK_t_KDT_DBDL_KetQuaXuLy] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_DBDL_KetQuaXuLy]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Insert]
	@ID bigint,
	@SoToKhai bigint,
	@MaDoanhNghiep nvarchar(15),
	@NgayDongBo datetime,
	@TrangThai int,
	@GhiChu nvarchar(max),
	@GUIDSTR nvarchar(36)
AS
INSERT INTO [dbo].[t_KDT_DBDL_KetQuaXuLy]
(
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
)
VALUES
(
	@ID,
	@SoToKhai,
	@MaDoanhNghiep,
	@NgayDongBo,
	@TrangThai,
	@GhiChu,
	@GUIDSTR
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_InsertUpdate]
	@ID bigint,
	@SoToKhai bigint,
	@MaDoanhNghiep nvarchar(15),
	@NgayDongBo datetime,
	@TrangThai int,
	@GhiChu nvarchar(max),
	@GUIDSTR nvarchar(36)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_DBDL_KetQuaXuLy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_DBDL_KetQuaXuLy] 
		SET
			[SoToKhai] = @SoToKhai,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[NgayDongBo] = @NgayDongBo,
			[TrangThai] = @TrangThai,
			[GhiChu] = @GhiChu,
			[GUIDSTR] = @GUIDSTR
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_DBDL_KetQuaXuLy]
	(
			[ID],
			[SoToKhai],
			[MaDoanhNghiep],
			[NgayDongBo],
			[TrangThai],
			[GhiChu],
			[GUIDSTR]
	)
	VALUES
	(
			@ID,
			@SoToKhai,
			@MaDoanhNghiep,
			@NgayDongBo,
			@TrangThai,
			@GhiChu,
			@GUIDSTR
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_DBDL_KetQuaXuLy]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_DBDL_KetQuaXuLy]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Update]
	@ID bigint,
	@SoToKhai bigint,
	@MaDoanhNghiep nvarchar(15),
	@NgayDongBo datetime,
	@TrangThai int,
	@GhiChu nvarchar(max),
	@GUIDSTR nvarchar(36)
AS

UPDATE
	[dbo].[t_KDT_DBDL_KetQuaXuLy]
SET
	[SoToKhai] = @SoToKhai,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[NgayDongBo] = @NgayDongBo,
	[TrangThai] = @TrangThai,
	[GhiChu] = @GhiChu,
	[GUIDSTR] = @GUIDSTR
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_NhomSanPham_InsertUpdate_ByKTX]'
GO
   
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_InsertUpdate]  
-- Database: ECS_TQDT_GC_V3  
-- Author: Ngo Thanh Tung  
-- Time created: Tuesday, March 06, 2012  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_InsertUpdate_ByKTX]  
 @HopDong_ID bigint,  
 @MaSanPham varchar(12),  
 @SoLuong numeric(18, 5),  
 @GiaGiaCong money,  
 @STTHang int,  
 @TenSanPham nvarchar(250)
AS  
IF EXISTS(SELECT [HopDong_ID], [MaSanPham] FROM [dbo].[t_KDT_GC_NhomSanPham] WHERE [HopDong_ID] = @HopDong_ID AND [MaSanPham] = @MaSanPham)  
 BEGIN  
  UPDATE  
   [dbo].[t_KDT_GC_NhomSanPham]   
  SET  
   [SoLuong] = @SoLuong,  
   [GiaGiaCong] = @GiaGiaCong,  
   [STTHang] = @STTHang,  
   [TenSanPham] = @TenSanPham
  WHERE  
   [HopDong_ID] = @HopDong_ID  
   AND [MaSanPham] = @MaSanPham  
 END  
ELSE  
 BEGIN  
 INSERT INTO [dbo].[t_KDT_GC_NhomSanPham]  
 (  
   [HopDong_ID],  
   [MaSanPham],  
   [SoLuong],  
   [GiaGiaCong],  
   [STTHang],  
   [TenSanPham]
 )  
 VALUES  
 (  
   @HopDong_ID,  
   @MaSanPham,  
   @SoLuong,  
   @GiaGiaCong,  
   @STTHang,  
   @TenSanPham
 )   
 END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_ThietBi_InsertUpdate_ByKTX]'
GO
   
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_InsertUpdate]  
-- Database: ECS_TQDT_GC_V3  
-- Author: Ngo Thanh Tung  
-- Time created: Tuesday, March 06, 2012  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_InsertUpdate_ByKTX]  
 @HopDong_ID bigint,  
 @Ma varchar(30),  
 @Ten nvarchar(80),  
 @MaHS varchar(12),  
 @DVT_ID char(3),  
 @SoLuongDangKy numeric(18, 5),  
 @NuocXX_ID char(3),  
 @DonGia float,  
 @TriGia float,  
 @NguyenTe_ID char(3),  
 @STTHang int,  
 @TinhTrang nvarchar(50)
AS  
IF EXISTS(SELECT [HopDong_ID], [Ma] FROM [dbo].[t_KDT_GC_ThietBi] WHERE [HopDong_ID] = @HopDong_ID AND [Ma] = @Ma)  
 BEGIN  
  UPDATE  
   [dbo].[t_KDT_GC_ThietBi]   
  SET  
   [Ten] = @Ten,  
   [MaHS] = @MaHS,  
   [DVT_ID] = @DVT_ID,  
   [SoLuongDangKy] = @SoLuongDangKy,  
   [NuocXX_ID] = @NuocXX_ID,  
   [DonGia] = @DonGia,  
   [TriGia] = @TriGia,  
   [NguyenTe_ID] = @NguyenTe_ID,  
   [STTHang] = @STTHang,  
   [TinhTrang] = @TinhTrang
  WHERE  
   [HopDong_ID] = @HopDong_ID  
   AND [Ma] = @Ma  
 END  
ELSE  
 BEGIN  
 INSERT INTO [dbo].[t_KDT_GC_ThietBi]  
 (  
   [HopDong_ID],  
   [Ma],  
   [Ten],  
   [MaHS],  
   [DVT_ID],  
   [SoLuongDangKy],  
   [NuocXX_ID],  
   [DonGia],  
   [TriGia],  
   [NguyenTe_ID],  
   [STTHang],  
   [TinhTrang]
 )  
 VALUES  
 (  
   @HopDong_ID,  
   @Ma,  
   @Ten,  
   @MaHS,  
   @DVT_ID,  
   @SoLuongDangKy,  
   @NuocXX_ID,  
   @DonGia,  
   @TriGia,  
   @NguyenTe_ID,  
   @STTHang,  
   @TinhTrang
 )   
 END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_HCT_HD]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_HCT_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong]
-- Database: HaiQuanGC
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 08, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong]
	@ID_HopDong bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	*
FROM
	[dbo].[t_KDT_GC_DinhMucDangKy]
WHERE
	[ID_HopDong] = @ID_HopDong

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_HMD_HD]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_HMD_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_NPL]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_NPL]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_PhuKienDangKy_SelectBy_HopDong_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKienDangKy_SelectBy_HopDong_ID]
-- Database: ECS_GIACONG
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 14, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_PhuKienDangKy_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
[SoTiepNhan]
	,[NgayTiepNhan]
      ,[TrangThaiXuLy]
      ,[NgayPhuKien]
      ,[NguoiDuyet]
      ,[VanBanChoPhep]
      ,[GhiChu]
      ,[SoPhuKien]
      ,[HopDong_ID]
      ,[ID]
      ,[MaHaiQuan]
      ,[MaDoanhNghiep]
      ,[GUIDSTR]
      ,[DeXuatKhac]
      ,[LyDoSua]
      ,[ActionStatus]
      ,[GuidReference]
      ,[NamDK]
      ,[HUONGDAN]
      ,[PhanLuong]
      ,[HuongdanPL]
FROM
	[dbo].[t_KDT_GC_PhuKienDangKy]
WHERE
	[HopDong_ID] = @HopDong_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_GC_BC03HSTK_GC_TT117]'
GO
-- =============================================          
-- Author:  LanNT          
-- Create date:           
-- Description:   Modify 18/06/2012    
-- =============================================          
          
ALTER PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117]           
 -- Add the parameters for the stored procedure here          
 @IDHopDong BIGINT,          
 @MaHaiQuan CHAR(6),          
 @MaDoanhNghiep VARCHAR(14)           
           
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
 -- p_GC_BC03HSTK_GC_TT117 219, 'C34C','0400101556'          
-- SET @IDHopDong=306 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',          
-- declare @IDHopDong BIGINT, @MaHaiQuan CHAR(6), @MaDoanhNghiep VARCHAR(14);  set @IDHopDong=  306;set @MaHaiQuan='C34C'; Set @MaDoanhNghiep='0400101556';          
          
    --      
     /*ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT'*/    
     -- CONVERT(varchar(50), ToKhai.SoToKhai) + '/' + v_HaiQuan_MaLoaiHinh.Ten_VT AS SoToKhai  
SELECT    ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT', ToKhai.SoToKhai, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, '-' AS TongCong, ToKhai.IDHopDong  
FROM         (  
          
      SELECT     CONVERT(varchar(50), t_KDT_ToKhaiMauDich.SoToKhai) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai,   
              LTRIM(RTRIM(t_KDT_ToKhaiMauDich.MaLoaiHinh)) AS MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong,   
              t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.DVT_ID  
       FROM         t_KDT_ToKhaiMauDich INNER JOIN  
              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN  
              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID  
       WHERE     (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND   
        (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND  
        (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND   
        (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N') AND  
        (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND   
        (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep) AND  
        (NOT (t_KDT_ToKhaiMauDich.MaLoaiHinh IN ('XGC18,XGC19,XGC20')))  
                         
                                                                                                                        
                       UNION  
                         
       SELECT     CONVERT(varchar(50), t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) + '/' + t_HaiQuan_LoaiPhieuChuyenTiep.ID AS SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,   
                      t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_HangChuyenTiep.MaHang, t_KDT_GC_HangChuyenTiep.TenHang,   
                      t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_HangChuyenTiep.ID_DVT  
FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN  
                      t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID INNER JOIN  
                      t_HaiQuan_LoaiPhieuChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = t_HaiQuan_LoaiPhieuChuyenTiep.ID  
WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND   
                      (LTRIM(RTRIM(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh)) IN ('PHPLX', 'XGC18', 'PHSPX','PHTBX')) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N')           
       )   
                      AS ToKhai INNER JOIN  
                      t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID  
WHERE     (ToKhai.IDHopDong = @IDHopDong)  
ORDER BY ToKhai.MaHang  
  
END     

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_GC_BC03HSTK_GC_TT117_Tong]'
GO

-- =============================================      
-- Author:  LanNT      
-- Create date:       
-- Description:   Modify 18/06/2012    
-- =============================================      
      
ALTER PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117_Tong]       
 -- Add the parameters for the stored procedure here      
 @IDHopDong BIGINT,      
 @MaHaiQuan CHAR(6),      
 @MaDoanhNghiep VARCHAR(14)       
       
AS      
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
 -- p_GC_BC03HSTK_GC_TT117 219, 'C34C','0400101556'          
-- SET @IDHopDong=306 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',          
-- declare @IDHopDong BIGINT, @MaHaiQuan CHAR(6), @MaDoanhNghiep VARCHAR(14);  set @IDHopDong=  306;set @MaHaiQuan='C34C'; Set @MaDoanhNghiep='0400101556';          
          
    --      
     /*ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT'*/    
     -- CONVERT(varchar(50), ToKhai.SoToKhai) + '/' + v_HaiQuan_MaLoaiHinh.Ten_VT AS SoToKhai  
SELECT   ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT',  ToKhai.SoToKhai, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, '-' AS SoLuong, SUM(ToKhai.SoLuong) AS TongCong,   
                      ToKhai.IDHopDong  
FROM         (SELECT     CONVERT(varchar(50), t_KDT_ToKhaiMauDich.SoToKhai) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai,   
                                              LTRIM(RTRIM(t_KDT_ToKhaiMauDich.MaLoaiHinh)) AS MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong,   
                                              t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.DVT_ID  
                       FROM          t_KDT_ToKhaiMauDich INNER JOIN  
                                              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN  
                                              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID  
                       WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND   
                                              (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N') AND   
                                              (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep) AND   
                                              (NOT (t_KDT_ToKhaiMauDich.MaLoaiHinh IN ('XGC18,XGC19,XGC20')))  
                       UNION  
                       SELECT     CONVERT(varchar(50), t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) + '/' + t_HaiQuan_LoaiPhieuChuyenTiep.ID AS SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,   
                      t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_HangChuyenTiep.MaHang, t_KDT_GC_HangChuyenTiep.TenHang,   
                      t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_HangChuyenTiep.ID_DVT  
FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN  
                      t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID INNER JOIN  
                      t_HaiQuan_LoaiPhieuChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = t_HaiQuan_LoaiPhieuChuyenTiep.ID  
WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND   
                      (LTRIM(RTRIM(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh)) IN ('PHPLX', 'XGC18', 'PHSPX', 'PHTBX')) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N')  
                                             )   
                      AS ToKhai INNER JOIN  
    t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID  
GROUP BY ToKhai.SoToKhai, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten, ToKhai.IDHopDong  
HAVING      (ToKhai.IDHopDong = @IDHopDong)  
ORDER BY ToKhai.MaHang  
  
END     
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_DBDL_Status] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
FROM [dbo].[t_KDT_DBDL_Status] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_DBDL_KetQuaXuLy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
FROM [dbo].[t_KDT_DBDL_KetQuaXuLy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicDongBoDuLieu]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]
-- Database: ECS_GCTQ
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 06, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicDongBoDuLieu]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT     t_KDT_DBDL_Status.MSG_STATUS, t_KDT_ToKhaiMauDich.*
FROM         t_KDT_ToKhaiMauDich LEFT OUTER JOIN
                      t_KDT_DBDL_Status ON t_KDT_ToKhaiMauDich.GUIDSTR = t_KDT_DBDL_Status.GUIDSTR WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamicDongBoDuLieu]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]
-- Database: ECS_GCTQ
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 06, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamicDongBoDuLieu]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT     t_KDT_GC_DBDL_Status.MSG_STATUS, t_KDT_GC_ToKhaiChuyenTiep.*
FROM         t_KDT_GC_ToKhaiChuyenTiep LEFT OUTER JOIN
                      t_KDT_GC_DBDL_Status ON t_KDT_GC_ToKhaiChuyenTiep.GUIDSTR = t_KDT_GC_DBDL_Status.GUIDSTR WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLTuCungUng]'
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[p_NPLTuCungUng]  
@MaDoanhNghiep varchar(50),  
@HopDongID int,
@NamTN int  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 -- p_NPLTuCungUng '0400101556',23,676  
 SET NOCOUNT ON;   
  
--Declare @MaDoanhNghiep varchar(50);  
--set @MaDoanhNghiep='0100101308'  
Declare @cmdSql varchar(8000);  
  
set @cmdSql='SELECT     t_KDT_GC_HopDong.ID AS HopDongID, t_KDT_GC_HopDong.SoHopDong, t_KDT_ToKhaiMauDich.ID AS ToKhaiID, t_KDT_ToKhaiMauDich.SoToKhai, 1 AS IsTKMD,   
                      t_KDT_GC_NPLCungUng.MaHang, t_KDT_GC_NPLCungUng.TenHang, t_KDT_GC_NPLCungUng.MaHS, t_KDT_GC_NPLCungUng.TongLuong,   
                      t_KDT_GC_NPLCungUng.DVT_ID, t_KDT_GC_HopDong.MaDoanhNghiep  
FROM         t_KDT_GC_HopDong INNER JOIN  
                      t_KDT_ToKhaiMauDich ON t_KDT_GC_HopDong.ID = t_KDT_ToKhaiMauDich.IDHopDong INNER JOIN  
                      t_KDT_GC_NPLCungUng ON t_KDT_ToKhaiMauDich.ID = t_KDT_GC_NPLCungUng.TKMDID  
WHERE     (t_KDT_GC_HopDong.MaDoanhNghiep like ''%'+@MaDoanhNghiep+'%'') AND   
                      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1)'  
 if(@NamTN<>0)                   
 set @cmdSql =@cmdSql+ ' AND (YEAR(t_KDT_ToKhaiMauDich.NgayTiepNhan) =' +@NamTN+')';  
 EXEC sp_executesql @cmdSql;  
print @cmdSql; 
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.1', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.1', getdate(), null)
	end  
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_AnDinhThue]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ADD
CONSTRAINT [FK_t_KDT_AnDinhThue_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
