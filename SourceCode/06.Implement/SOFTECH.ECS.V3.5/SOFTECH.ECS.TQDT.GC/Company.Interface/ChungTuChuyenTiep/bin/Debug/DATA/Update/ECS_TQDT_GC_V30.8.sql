GO
ALTER TABLE T_KDT_GC_BCXNT_NguyenPhuLieu
ALTER COLUMN [DVT] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Insert]
	@STTHang int,
	@HopDong_ID bigint,
	@MaNPL varchar(30),
	@TenNPL nvarchar(80),
	@MaHS varchar(12),
	@DVT char(10),
	@LuongNhap numeric(18, 6),
	@LuongXuat numeric(18, 6),
	@LuongCungUng numeric(18, 6),
	@LuongTon numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@TrangThai int,
	@TuNgay datetime,
	@DenNgay datetime
AS
INSERT INTO [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
(
	[STTHang],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT],
	[LuongNhap],
	[LuongXuat],
	[LuongCungUng],
	[LuongTon],
	[TongNhuCau],
	[TrangThai],
	[TuNgay],
	[DenNgay]
)
VALUES
(
	@STTHang,
	@HopDong_ID,
	@MaNPL,
	@TenNPL,
	@MaHS,
	@DVT,
	@LuongNhap,
	@LuongXuat,
	@LuongCungUng,
	@LuongTon,
	@TongNhuCau,
	@TrangThai,
	@TuNgay,
	@DenNgay
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Update]
	@STTHang int,
	@HopDong_ID bigint,
	@MaNPL varchar(30),
	@TenNPL nvarchar(80),
	@MaHS varchar(12),
	@DVT char(10),
	@LuongNhap numeric(18, 6),
	@LuongXuat numeric(18, 6),
	@LuongCungUng numeric(18, 6),
	@LuongTon numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@TrangThai int,
	@TuNgay datetime,
	@DenNgay datetime
AS

UPDATE
	[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
SET
	[STTHang] = @STTHang,
	[TenNPL] = @TenNPL,
	[MaHS] = @MaHS,
	[DVT] = @DVT,
	[LuongNhap] = @LuongNhap,
	[LuongXuat] = @LuongXuat,
	[LuongCungUng] = @LuongCungUng,
	[LuongTon] = @LuongTon,
	[TongNhuCau] = @TongNhuCau,
	[TrangThai] = @TrangThai,
	[TuNgay] = @TuNgay,
	[DenNgay] = @DenNgay
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaNPL] = @MaNPL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_InsertUpdate]
	@STTHang int,
	@HopDong_ID bigint,
	@MaNPL varchar(30),
	@TenNPL nvarchar(80),
	@MaHS varchar(12),
	@DVT char(10),
	@LuongNhap numeric(18, 6),
	@LuongXuat numeric(18, 6),
	@LuongCungUng numeric(18, 6),
	@LuongTon numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@TrangThai int,
	@TuNgay datetime,
	@DenNgay datetime
AS
IF EXISTS(SELECT [HopDong_ID], [MaNPL] FROM [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu] WHERE [HopDong_ID] = @HopDong_ID AND [MaNPL] = @MaNPL)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu] 
		SET
			[STTHang] = @STTHang,
			[TenNPL] = @TenNPL,
			[MaHS] = @MaHS,
			[DVT] = @DVT,
			[LuongNhap] = @LuongNhap,
			[LuongXuat] = @LuongXuat,
			[LuongCungUng] = @LuongCungUng,
			[LuongTon] = @LuongTon,
			[TongNhuCau] = @TongNhuCau,
			[TrangThai] = @TrangThai,
			[TuNgay] = @TuNgay,
			[DenNgay] = @DenNgay
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [MaNPL] = @MaNPL
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
	(
			[STTHang],
			[HopDong_ID],
			[MaNPL],
			[TenNPL],
			[MaHS],
			[DVT],
			[LuongNhap],
			[LuongXuat],
			[LuongCungUng],
			[LuongTon],
			[TongNhuCau],
			[TrangThai],
			[TuNgay],
			[DenNgay]
	)
	VALUES
	(
			@STTHang,
			@HopDong_ID,
			@MaNPL,
			@TenNPL,
			@MaHS,
			@DVT,
			@LuongNhap,
			@LuongXuat,
			@LuongCungUng,
			@LuongTon,
			@TongNhuCau,
			@TrangThai,
			@TuNgay,
			@DenNgay
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Delete]
	@HopDong_ID bigint,
	@MaNPL varchar(30)
AS

DELETE FROM 
	[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaNPL] = @MaNPL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Load]
	@HopDong_ID bigint,
	@MaNPL varchar(30)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[STTHang],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT],
	[LuongNhap],
	[LuongXuat],
	[LuongCungUng],
	[LuongTon],
	[TongNhuCau],
	[TrangThai],
	[TuNgay],
	[DenNgay]
FROM
	[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaNPL] = @MaNPL
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[STTHang],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT],
	[LuongNhap],
	[LuongXuat],
	[LuongCungUng],
	[LuongTon],
	[TongNhuCau],
	[TrangThai],
	[TuNgay],
	[DenNgay]
FROM
	[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[STTHang],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT],
	[LuongNhap],
	[LuongXuat],
	[LuongCungUng],
	[LuongTon],
	[TongNhuCau],
	[TrangThai],
	[TuNgay],
	[DenNgay]
FROM [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[STTHang],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT],
	[LuongNhap],
	[LuongXuat],
	[LuongCungUng],
	[LuongTon],
	[TongNhuCau],
	[TrangThai],
	[TuNgay],
	[DenNgay]
FROM
	[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]	

GO

 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.8',GETDATE(), N'CẬP NHẬT PROCEDURE TỔNG HỢP DỮ LIỆU')
END
