﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Messages.SmartCA
{
    public class RequestMessage
    {
        public string RequestID { get; set; }
        public string ServiceID { get; set; }
        public string FunctionName { get; set; }
        public SignParameter Parameter { get; set; }
    }
    public class SignParameter
    {
        public string CertID { get; set; }
        public string ServiceGroupID { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; }
        public string ContentType { get; set; }
        public string DataBase64 { get; set; }
    }
    public class SignResponse
    {
        public string TranID { get; set; }
        public int ResponseCode { get; set; }
        public string Message { get; set; }
        public string SignedData { get; set; }
    }
    public class AccessTokenSmartCA
    {
        //Yêu cầu giá trị là authorization_code
        public string grant_type { get; set; }

        //Định danh ứng dụng client
        public string client_id { get; set; }

        //Chuỗi giá trị bí mật tương ứng với ứng dụng client
        public string client_secret { get; set; }

        ////Giá trị trả về từ bước 3
        //public int Code { get; set; }

        ////Callback url để dịch gọi lại gửi giá trị access_token (yêu cầu cấu hình trước)
        //public string Redirect_Uri { get; set; }

        //Personal ID đã gửi vào trong email người dùng
        public string username { get; set; }

        //Mật khẩu đăng nhập tài khoản người dùng
        public string password { get; set; }

    }
    public class CredentialInfoSmartCA
    {
        //(Required) ID credential của thuê bao
        public string CredentialId { get; set; }

        //(Required) Kiểu trả về chứng thư số của thuê bao
        //-	none: Không trả về trong kết quả
        //-	single: Chỉ trả về chứng thư số của thuê bao
        //-	chain: Trả về danh sách chứng thư bao gồm của thuê bao và của CA
        public string Certificates { get; set; }

        //(Optional) Có trả về thông tin chứng thư hoặc không
        public bool CertInfo { get; set; }

        //(Optional) Có trả về thông tin kiểu xác thực của thuê bao hoặc không
        public bool AuthInfo { get; set; }
    }
    public class RequestCertificateSmartCA
    {
        public string credentialId { get; set; }
        public string certificates { get; set; }
        public bool certInfo { get; set; }
        public bool authInfo { get; set; }
    }
    public class RequestCredentialSmartCA
    {
    }
    public class SignSmartCA
    {
        //(Required) ID credential của thuê bao
        public string credentialId { get; set; }

        //(Optional) ID giao dịch phía đối tác
        public string refTranId { get; set; }

        //(Optional) Sau khi thuê bao xác nhận giao dịch ký số, SmartCA sẽ gọi URL này 01 lần duy nhất ở server side to server side. Đối tác cần build URL này để nhận kết quả từ SmartCA (HTTP POST).
        //Dữ liệu định dạng application/json:
        //{
        //tranId = (string)“ID giao dịch ký số của SmartCA”, refTranId = (string)<ID giao dịch phía đối tác>, status = (byte)<Trạng thái giao dịch ký số>
        //}
        public string notifyUrl { get; set; }

        //(Optional) Mô tả thông tin giao dịch
        public string description { get; set; }

        //(Required) Danh sách file yêu cầu ký số (tối đa 10)
        public List<DataSign> datas { get; set; }
    }
    public class SignHashSmartCA
    {
        //(Required) ID credential của thuê bao
        public string credentialId { get; set; }

        //(Optional) ID giao dịch phía đối tác
        public string refTranId { get; set; }

        //(Optional) Sau khi thuê bao xác nhận giao dịch ký số, SmartCA sẽ gọi URL này 01 lần duy nhất ở server side to server side. Đối tác cần build URL này để nhận kết quả từ SmartCA (HTTP POST).
        //Dữ liệu định dạng application/json:
        //{
        //tranId = (string)“ID giao dịch ký số của SmartCA”, refTranId = (string)<ID giao dịch phía đối tác>, status = (byte)<Trạng thái giao dịch ký số>
        //}
        public string notifyUrl { get; set; }

        //(Optional) Mô tả thông tin giao dịch
        public string description { get; set; }

        //(Required) Danh sách hash yêu cầu ký số (tối đa 50)
        public List<DataSignHash> datas { get; set; }
    }
    public class DataSign
    {
        public string dataBase64 { get; set; }
        public string name { get; set; }
    }
    public class DataSignHash
    {
        public string hash { get; set; }
        public string name { get; set; }
    }

    public class TransactionInfoSmartCA
    {
        //(Required) ID giao dịch trả về từ api signhash
        public string tranId { get; set; }
    }
}
