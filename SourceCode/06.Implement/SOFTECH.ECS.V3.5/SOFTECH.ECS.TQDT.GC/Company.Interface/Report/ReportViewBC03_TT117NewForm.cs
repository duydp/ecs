using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Company.Interface.Report.GC;
using Company.GC.BLL.KDT.GC;
using Company.Interface;

namespace Company.Interface.Report
{
    public partial class ReportViewBC03_TT117NewForm : DevExpress.XtraEditors.XtraForm
    {
        public ReportViewBC03_TT117NewForm()
        {
            InitializeComponent();
        }
        public HopDong HD;
        private void SapXep_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void printControl1_Load(object sender, EventArgs e)
        {
            
        }

        private void ReportViewBC03_TT117NewForm_Load(object sender, EventArgs e)
        {
            repositoryItemComboBox1.Items.Add("Mã hàng");
            repositoryItemComboBox1.Items.Add("Số tờ khai");
            repositoryItemComboBox1.Items.Add("Tên hàng");
            SapXep.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
        }
        private void loadReport(DataTable dt)
        {
            BangKe03_HSTK_GC_TT117 BC03 = new BangKe03_HSTK_GC_TT117();
            BC03.HD = HD;
            Company.GC.BLL.GC.NguyenPhuLieu  NPL = new Company.GC.BLL.GC.NguyenPhuLieu();

            DataSet dataSource = NPL.BaoCaoBC03HSTK_GC_TT117(this.HD.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

            if (dataSource != null && dataSource.Tables[0].Rows.Count == 0)
            {
                dataSource.Tables.Clear();
            }

        }
    }
}