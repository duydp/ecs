﻿namespace Company.Interface.Report.SXXK
{
    partial class ToKhaiTriGiaA4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiTriGiaA4));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.winControlContainer6 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label6 = new System.Windows.Forms.Label();
            this.winControlContainer5 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label5 = new System.Windows.Forms.Label();
            this.winControlContainer4 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer3 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label3 = new System.Windows.Forms.Label();
            this.winControlContainer2 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.lblInPL = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanga5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanga6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanga7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanga8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangb5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangb6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangb7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangb8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangc5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangc6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangc7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangc8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangd5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangd6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangd7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangd8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayNGK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanga1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanga2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanga3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanga4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangb1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangb2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangb3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangb4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangc1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangc2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangc3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangc4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangd1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangd2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangd3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHangd4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHang174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSLPTKTG = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSTK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTrangthai = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayXK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkYes2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkNo2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkYes3 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkNo3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkYes42 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkNo42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkYes41 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkNo41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMoiQuanHe = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.chkYes52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkNo52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkNo51 = new DevExpress.XtraReports.UI.XRLabel();
            this.chkYes51 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblKemTheoSTK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTiepNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrKD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.winControlContainer6,
            this.winControlContainer5,
            this.winControlContainer4,
            this.xrLabel47,
            this.xrLabel40,
            this.xrLabel39,
            this.xrLabel6,
            this.winControlContainer3,
            this.winControlContainer2,
            this.winControlContainer1,
            this.lblInPL,
            this.xrTable9,
            this.xrTable7,
            this.xrTable5,
            this.xrTable3,
            this.xrTable2,
            this.xrLabel13,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrTable1,
            this.xrLabel67,
            this.xrLabel66,
            this.xrLabel65,
            this.lblNgayNGK,
            this.xrLabel63,
            this.xrLabel61,
            this.xrLabel62,
            this.xrLabel60,
            this.xrTable6,
            this.xrTable12,
            this.xrTable10,
            this.xrTable8,
            this.lblSLPTKTG,
            this.lblSTK,
            this.xrTable4,
            this.lblKemTheoSTK,
            this.xrLabel2,
            this.xrLabel1,
            this.xrLabel23,
            this.lblSoTiepNhan});
            this.Detail.HeightF = 2268F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // winControlContainer6
            // 
            this.winControlContainer6.BackColor = System.Drawing.Color.Transparent;
            this.winControlContainer6.BorderColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.winControlContainer6.BorderWidth = 1;
            this.winControlContainer6.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.winControlContainer6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer6.LocationFloat = new DevExpress.Utils.PointFloat(33F, 1258F);
            this.winControlContainer6.Name = "winControlContainer6";
            this.winControlContainer6.SizeF = new System.Drawing.SizeF(25F, 59F);
            this.winControlContainer6.WinControl = this.label6;
            // 
            // label6
            // 
            this.label6.Image = ((System.Drawing.Image)(resources.GetObject("label6.Image")));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 57);
            this.label6.TabIndex = 5;
            this.label6.Text = " ";
            // 
            // winControlContainer5
            // 
            this.winControlContainer5.BackColor = System.Drawing.Color.Transparent;
            this.winControlContainer5.BorderColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.winControlContainer5.BorderWidth = 1;
            this.winControlContainer5.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.winControlContainer5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer5.LocationFloat = new DevExpress.Utils.PointFloat(33F, 1342F);
            this.winControlContainer5.Name = "winControlContainer5";
            this.winControlContainer5.SizeF = new System.Drawing.SizeF(25F, 266F);
            this.winControlContainer5.WinControl = this.label5;
            // 
            // label5
            // 
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 255);
            this.label5.TabIndex = 4;
            this.label5.Text = " ";
            // 
            // winControlContainer4
            // 
            this.winControlContainer4.BackColor = System.Drawing.Color.Transparent;
            this.winControlContainer4.BorderColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.winControlContainer4.BorderWidth = 1;
            this.winControlContainer4.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.winControlContainer4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer4.LocationFloat = new DevExpress.Utils.PointFloat(33F, 1633F);
            this.winControlContainer4.Name = "winControlContainer4";
            this.winControlContainer4.SizeF = new System.Drawing.SizeF(25F, 117F);
            this.winControlContainer4.WinControl = this.label4;
            // 
            // label4
            // 
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 112);
            this.label4.TabIndex = 3;
            this.label4.Text = " ";
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel47.BorderWidth = 1;
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(25F, 1626F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(41F, 11F);
            this.xrLabel47.Text = " ";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel40.BorderWidth = 1;
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(25F, 1326F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(41F, 11F);
            this.xrLabel40.Text = " ";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel39.BorderWidth = 1;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(17F, 889F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(41F, 11F);
            this.xrLabel39.Text = " ";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel6.BorderWidth = 1;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(18F, 612F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(41F, 11F);
            this.xrLabel6.Text = " ";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // winControlContainer3
            // 
            this.winControlContainer3.BackColor = System.Drawing.Color.Transparent;
            this.winControlContainer3.BorderColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.winControlContainer3.BorderWidth = 1;
            this.winControlContainer3.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.winControlContainer3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer3.LocationFloat = new DevExpress.Utils.PointFloat(25F, 900F);
            this.winControlContainer3.Name = "winControlContainer3";
            this.winControlContainer3.SizeF = new System.Drawing.SizeF(25F, 125F);
            this.winControlContainer3.WinControl = this.label3;
            // 
            // label3
            // 
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 120);
            this.label3.TabIndex = 2;
            this.label3.Text = " ";
            // 
            // winControlContainer2
            // 
            this.winControlContainer2.BackColor = System.Drawing.Color.Transparent;
            this.winControlContainer2.BorderColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.winControlContainer2.BorderWidth = 1;
            this.winControlContainer2.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.winControlContainer2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer2.LocationFloat = new DevExpress.Utils.PointFloat(25F, 542F);
            this.winControlContainer2.Name = "winControlContainer2";
            this.winControlContainer2.SizeF = new System.Drawing.SizeF(25F, 66F);
            this.winControlContainer2.WinControl = this.label2;
            // 
            // label2
            // 
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 63);
            this.label2.TabIndex = 1;
            this.label2.Text = " ";
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.BackColor = System.Drawing.Color.Transparent;
            this.winControlContainer1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.winControlContainer1.BorderWidth = 1;
            this.winControlContainer1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.winControlContainer1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.winControlContainer1.LocationFloat = new DevExpress.Utils.PointFloat(25F, 633F);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.SizeF = new System.Drawing.SizeF(25F, 225F);
            this.winControlContainer1.WinControl = this.label1;
            // 
            // label1
            // 
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 216);
            this.label1.TabIndex = 0;
            this.label1.Text = " ";
            // 
            // lblInPL
            // 
            this.lblInPL.BorderWidth = 0;
            this.lblInPL.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInPL.LocationFloat = new DevExpress.Utils.PointFloat(424F, 567F);
            this.lblInPL.Name = "lblInPL";
            this.lblInPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblInPL.SizeF = new System.Drawing.SizeF(367F, 75F);
            this.lblInPL.Text = " ";
            this.lblInPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(23F, 1169F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow53,
            this.xrTableRow54});
            this.xrTable9.SizeF = new System.Drawing.SizeF(776F, 83F);
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell214});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow53.Weight = 0.39759036144578314;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell214.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell214.Text = " PHẦN XÁC ĐỊNH TRỊ GIÁ TÍNH THUẾ";
            this.xrTableCell214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell214.Weight = 1;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell215,
            this.xrTableCell216,
            this.xrTableCell217,
            this.xrTableCell218,
            this.xrTableCell219});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow54.Weight = 0.60240963855421692;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell215.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell215.Text = " 6. Số thứ tự mặt hàng trong tờ khai hàng hóa nhập khẩu";
            this.xrTableCell215.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell215.Weight = 0.520618556701031;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell216.Text = " Mặt hàng số 5";
            this.xrTableCell216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell216.Weight = 0.12242268041237113;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell217.Text = " Mặt hàng số 6";
            this.xrTableCell217.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell217.Weight = 0.11855670103092783;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell218.Text = " Mặt hàng số 7";
            this.xrTableCell218.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell218.Weight = 0.11855670103092783;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell219.Text = " Mặt hàng số 8";
            this.xrTableCell219.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell219.Weight = 0.11984536082474227;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(65F, 1327F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41,
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow52});
            this.xrTable7.SizeF = new System.Drawing.SizeF(734F, 300F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell154,
            this.lblHang105,
            this.lblHang106,
            this.lblHang107,
            this.lblHang108});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow41.Weight = 0.083333333333333329;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell154.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell154.Text = " 10. Chi phia hoa hồng bán hàng/phí môi giới";
            this.xrTableCell154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell154.Weight = 0.49318801089918257;
            // 
            // lblHang105
            // 
            this.lblHang105.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang105.Name = "lblHang105";
            this.lblHang105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang105.Text = " ";
            this.lblHang105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang105.Weight = 0.12942779291553133;
            // 
            // lblHang106
            // 
            this.lblHang106.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang106.Name = "lblHang106";
            this.lblHang106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang106.Text = " ";
            this.lblHang106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang106.Weight = 0.12534059945504086;
            // 
            // lblHang107
            // 
            this.lblHang107.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang107.Name = "lblHang107";
            this.lblHang107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang107.Text = " ";
            this.lblHang107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang107.Weight = 0.12534059945504086;
            // 
            // lblHang108
            // 
            this.lblHang108.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang108.Name = "lblHang108";
            this.lblHang108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang108.Text = " ";
            this.lblHang108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang108.Weight = 0.12670299727520437;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159,
            this.lblHang115,
            this.lblHang116,
            this.lblHang117,
            this.lblHang118});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow42.Weight = 0.083333333333333329;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell159.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell159.Text = " 11. Chi phí gắn liền với hàng hóa";
            this.xrTableCell159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell159.Weight = 0.49318801089918257;
            // 
            // lblHang115
            // 
            this.lblHang115.Name = "lblHang115";
            this.lblHang115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang115.Text = " ";
            this.lblHang115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang115.Weight = 0.12942779291553133;
            // 
            // lblHang116
            // 
            this.lblHang116.Name = "lblHang116";
            this.lblHang116.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang116.Text = " ";
            this.lblHang116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang116.Weight = 0.12534059945504086;
            // 
            // lblHang117
            // 
            this.lblHang117.Name = "lblHang117";
            this.lblHang117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang117.Text = " ";
            this.lblHang117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang117.Weight = 0.12534059945504086;
            // 
            // lblHang118
            // 
            this.lblHang118.Name = "lblHang118";
            this.lblHang118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang118.Text = " ";
            this.lblHang118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang118.Weight = 0.12670299727520437;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164,
            this.lblHang125,
            this.lblHang126,
            this.lblHang127,
            this.lblHang128});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow43.Weight = 0.083333333333333329;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell164.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell164.Text = " 12. Chi phí đóng gói";
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell164.Weight = 0.49318801089918257;
            // 
            // lblHang125
            // 
            this.lblHang125.Name = "lblHang125";
            this.lblHang125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang125.Text = " ";
            this.lblHang125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang125.Weight = 0.12942779291553133;
            // 
            // lblHang126
            // 
            this.lblHang126.Name = "lblHang126";
            this.lblHang126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang126.Text = " ";
            this.lblHang126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang126.Weight = 0.12534059945504086;
            // 
            // lblHang127
            // 
            this.lblHang127.Name = "lblHang127";
            this.lblHang127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang127.Text = " ";
            this.lblHang127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang127.Weight = 0.12534059945504086;
            // 
            // lblHang128
            // 
            this.lblHang128.Name = "lblHang128";
            this.lblHang128.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang128.Text = " ";
            this.lblHang128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang128.Weight = 0.12670299727520437;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell169,
            this.lblHang135,
            this.lblHang136,
            this.lblHang137,
            this.lblHang138});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow44.Weight = 0.083333333333333329;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell169.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell169.Text = " 13. Các khoản trợ giúp người mua, cung cấp miễn phí hoặc giảm giá";
            this.xrTableCell169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell169.Weight = 0.49318801089918257;
            // 
            // lblHang135
            // 
            this.lblHang135.Name = "lblHang135";
            this.lblHang135.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang135.Text = " ";
            this.lblHang135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang135.Weight = 0.12942779291553133;
            // 
            // lblHang136
            // 
            this.lblHang136.Name = "lblHang136";
            this.lblHang136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang136.Text = " ";
            this.lblHang136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang136.Weight = 0.12534059945504086;
            // 
            // lblHang137
            // 
            this.lblHang137.Name = "lblHang137";
            this.lblHang137.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang137.Text = " ";
            this.lblHang137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang137.Weight = 0.12534059945504086;
            // 
            // lblHang138
            // 
            this.lblHang138.Name = "lblHang138";
            this.lblHang138.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang138.Text = " ";
            this.lblHang138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang138.Weight = 0.12670299727520437;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell174,
            this.lblHanga5,
            this.lblHanga6,
            this.lblHanga7,
            this.lblHanga8});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow45.Weight = 0.083333333333333329;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell174.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell174.Text = " a. Nguyên vật liệu, bộ phận cấu thành, phụ tùng, chi tiết tương tự";
            this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell174.Weight = 0.49318801089918257;
            // 
            // lblHanga5
            // 
            this.lblHanga5.Name = "lblHanga5";
            this.lblHanga5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanga5.Text = " ";
            this.lblHanga5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHanga5.Weight = 0.12942779291553133;
            // 
            // lblHanga6
            // 
            this.lblHanga6.Name = "lblHanga6";
            this.lblHanga6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanga6.Text = " ";
            this.lblHanga6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHanga6.Weight = 0.12534059945504086;
            // 
            // lblHanga7
            // 
            this.lblHanga7.Name = "lblHanga7";
            this.lblHanga7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanga7.Text = " ";
            this.lblHanga7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHanga7.Weight = 0.12534059945504086;
            // 
            // lblHanga8
            // 
            this.lblHanga8.Name = "lblHanga8";
            this.lblHanga8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanga8.Text = " ";
            this.lblHanga8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHanga8.Weight = 0.12670299727520437;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell179,
            this.lblHangb5,
            this.lblHangb6,
            this.lblHangb7,
            this.lblHangb8});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow46.Weight = 0.083333333333333329;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell179.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell179.Text = " b. Vật liệu, nhiên liệu, năng lượng tiêu hao";
            this.xrTableCell179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell179.Weight = 0.49318801089918257;
            // 
            // lblHangb5
            // 
            this.lblHangb5.Name = "lblHangb5";
            this.lblHangb5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangb5.Text = " ";
            this.lblHangb5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangb5.Weight = 0.12942779291553133;
            // 
            // lblHangb6
            // 
            this.lblHangb6.Name = "lblHangb6";
            this.lblHangb6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangb6.Text = " ";
            this.lblHangb6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangb6.Weight = 0.12534059945504086;
            // 
            // lblHangb7
            // 
            this.lblHangb7.Name = "lblHangb7";
            this.lblHangb7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangb7.Text = " ";
            this.lblHangb7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangb7.Weight = 0.12534059945504086;
            // 
            // lblHangb8
            // 
            this.lblHangb8.Name = "lblHangb8";
            this.lblHangb8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangb8.Text = " ";
            this.lblHangb8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangb8.Weight = 0.12670299727520437;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell184,
            this.lblHangc5,
            this.lblHangc6,
            this.lblHangc7,
            this.lblHangc8});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow47.Weight = 0.083333333333333329;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell184.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell184.Text = " c. Công cụ, dụng cụ, khuôn ráp, khuôn đúc, khuôn mẫu, chi tiết tương tự";
            this.xrTableCell184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell184.Weight = 0.49318801089918257;
            // 
            // lblHangc5
            // 
            this.lblHangc5.Name = "lblHangc5";
            this.lblHangc5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangc5.Text = " ";
            this.lblHangc5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangc5.Weight = 0.12942779291553133;
            // 
            // lblHangc6
            // 
            this.lblHangc6.Name = "lblHangc6";
            this.lblHangc6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangc6.Text = " ";
            this.lblHangc6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangc6.Weight = 0.12534059945504086;
            // 
            // lblHangc7
            // 
            this.lblHangc7.Name = "lblHangc7";
            this.lblHangc7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangc7.Text = " ";
            this.lblHangc7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangc7.Weight = 0.12534059945504086;
            // 
            // lblHangc8
            // 
            this.lblHangc8.Name = "lblHangc8";
            this.lblHangc8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangc8.Text = " ";
            this.lblHangc8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangc8.Weight = 0.12670299727520437;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell189,
            this.lblHangd5,
            this.lblHangd6,
            this.lblHangd7,
            this.lblHangd8});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow48.Weight = 0.083333333333333329;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell189.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell189.Text = " d. Bản vẻ/thiết kế kỹ thuật/triển khai, thiết kế mỹ thuật, thi công mẫu,sơ đồ ph" +
                "ác thảo, sản phẩm và dịch vụ tương tự";
            this.xrTableCell189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell189.Weight = 0.49318801089918257;
            // 
            // lblHangd5
            // 
            this.lblHangd5.Name = "lblHangd5";
            this.lblHangd5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangd5.Text = " ";
            this.lblHangd5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangd5.Weight = 0.12942779291553133;
            // 
            // lblHangd6
            // 
            this.lblHangd6.Name = "lblHangd6";
            this.lblHangd6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangd6.Text = " ";
            this.lblHangd6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangd6.Weight = 0.12534059945504086;
            // 
            // lblHangd7
            // 
            this.lblHangd7.Name = "lblHangd7";
            this.lblHangd7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangd7.Text = " ";
            this.lblHangd7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangd7.Weight = 0.12534059945504086;
            // 
            // lblHangd8
            // 
            this.lblHangd8.Name = "lblHangd8";
            this.lblHangd8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangd8.Text = " ";
            this.lblHangd8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangd8.Weight = 0.12670299727520437;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell194,
            this.lblHang145,
            this.lblHang146,
            this.lblHang147,
            this.lblHang148});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow49.Weight = 0.083333333333333329;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell194.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell194.Text = " 14. Tiền bản quyền, phí giấy phép";
            this.xrTableCell194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell194.Weight = 0.49318801089918257;
            // 
            // lblHang145
            // 
            this.lblHang145.Name = "lblHang145";
            this.lblHang145.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang145.Text = " ";
            this.lblHang145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang145.Weight = 0.12942779291553133;
            // 
            // lblHang146
            // 
            this.lblHang146.Name = "lblHang146";
            this.lblHang146.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang146.Text = " ";
            this.lblHang146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang146.Weight = 0.12534059945504086;
            // 
            // lblHang147
            // 
            this.lblHang147.Name = "lblHang147";
            this.lblHang147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang147.Text = " ";
            this.lblHang147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang147.Weight = 0.12534059945504086;
            // 
            // lblHang148
            // 
            this.lblHang148.Name = "lblHang148";
            this.lblHang148.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang148.Text = " ";
            this.lblHang148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang148.Weight = 0.12670299727520437;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell199,
            this.lblHang155,
            this.lblHang156,
            this.lblHang157,
            this.lblHang158});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow50.Weight = 0.083333333333333329;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell199.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell199.Text = " 15. Tiền thu được phải trả sau khi định đoạt sử dụng hàng hóa";
            this.xrTableCell199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell199.Weight = 0.49318801089918257;
            // 
            // lblHang155
            // 
            this.lblHang155.Name = "lblHang155";
            this.lblHang155.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang155.Text = " ";
            this.lblHang155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang155.Weight = 0.12942779291553133;
            // 
            // lblHang156
            // 
            this.lblHang156.Name = "lblHang156";
            this.lblHang156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang156.Text = " ";
            this.lblHang156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang156.Weight = 0.12534059945504086;
            // 
            // lblHang157
            // 
            this.lblHang157.Name = "lblHang157";
            this.lblHang157.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang157.Text = " ";
            this.lblHang157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang157.Weight = 0.12534059945504086;
            // 
            // lblHang158
            // 
            this.lblHang158.Name = "lblHang158";
            this.lblHang158.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang158.Text = " ";
            this.lblHang158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang158.Weight = 0.12670299727520437;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell204,
            this.lblHang165,
            this.lblHang166,
            this.lblHang167,
            this.lblHang168});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow51.Weight = 0.083333333333333329;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell204.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell204.Text = " 16. Chi phí vận tải, bốc xếp, chuyển hàng";
            this.xrTableCell204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell204.Weight = 0.49318801089918257;
            // 
            // lblHang165
            // 
            this.lblHang165.Name = "lblHang165";
            this.lblHang165.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang165.Text = " ";
            this.lblHang165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang165.Weight = 0.12942779291553133;
            // 
            // lblHang166
            // 
            this.lblHang166.Name = "lblHang166";
            this.lblHang166.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang166.Text = " ";
            this.lblHang166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang166.Weight = 0.12534059945504086;
            // 
            // lblHang167
            // 
            this.lblHang167.Name = "lblHang167";
            this.lblHang167.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang167.Text = " ";
            this.lblHang167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang167.Weight = 0.12534059945504086;
            // 
            // lblHang168
            // 
            this.lblHang168.Name = "lblHang168";
            this.lblHang168.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang168.Text = " ";
            this.lblHang168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang168.Weight = 0.12670299727520437;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell209,
            this.lblHang175,
            this.lblHang176,
            this.lblHang177,
            this.lblHang178});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow52.Weight = 0.083333333333333329;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell209.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell209.Text = " 17. Chi phí bảo hiểm hàng hóa";
            this.xrTableCell209.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell209.Weight = 0.49318801089918257;
            // 
            // lblHang175
            // 
            this.lblHang175.Name = "lblHang175";
            this.lblHang175.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang175.Text = " ";
            this.lblHang175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang175.Weight = 0.12942779291553133;
            // 
            // lblHang176
            // 
            this.lblHang176.Name = "lblHang176";
            this.lblHang176.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang176.Text = " ";
            this.lblHang176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang176.Weight = 0.12534059945504086;
            // 
            // lblHang177
            // 
            this.lblHang177.Name = "lblHang177";
            this.lblHang177.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang177.Text = " ";
            this.lblHang177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang177.Weight = 0.12534059945504086;
            // 
            // lblHang178
            // 
            this.lblHang178.Name = "lblHang178";
            this.lblHang178.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang178.Text = " ";
            this.lblHang178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang178.Weight = 0.12670299727520437;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(65F, 1627F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow36,
            this.xrTableRow37,
            this.xrTableRow39});
            this.xrTable5.SizeF = new System.Drawing.SizeF(734F, 125F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell82,
            this.lblHang185,
            this.lblHang186,
            this.lblHang187,
            this.lblHang188});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow28.Weight = 0.2;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell82.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell82.Text = " 18. Phí bảo hiểm, vận tải hàng hóa trong nội địa";
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell82.Weight = 0.49318801089918257;
            // 
            // lblHang185
            // 
            this.lblHang185.Name = "lblHang185";
            this.lblHang185.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang185.Text = " ";
            this.lblHang185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang185.Weight = 0.12942779291553133;
            // 
            // lblHang186
            // 
            this.lblHang186.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang186.Name = "lblHang186";
            this.lblHang186.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang186.Text = " ";
            this.lblHang186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang186.Weight = 0.12534059945504086;
            // 
            // lblHang187
            // 
            this.lblHang187.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang187.Name = "lblHang187";
            this.lblHang187.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang187.Text = " ";
            this.lblHang187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang187.Weight = 0.12534059945504086;
            // 
            // lblHang188
            // 
            this.lblHang188.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang188.Name = "lblHang188";
            this.lblHang188.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang188.Text = " ";
            this.lblHang188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang188.Weight = 0.12670299727520437;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.lblHang195,
            this.lblHang196,
            this.lblHang197,
            this.lblHang198});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow29.Weight = 0.2;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell106.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell106.Text = " 19. Chi phí phát sinh sau khi nhập khẩu";
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell106.Weight = 0.49318801089918257;
            // 
            // lblHang195
            // 
            this.lblHang195.Name = "lblHang195";
            this.lblHang195.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang195.Text = " ";
            this.lblHang195.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang195.Weight = 0.12942779291553133;
            // 
            // lblHang196
            // 
            this.lblHang196.Name = "lblHang196";
            this.lblHang196.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang196.Text = " ";
            this.lblHang196.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang196.Weight = 0.12534059945504086;
            // 
            // lblHang197
            // 
            this.lblHang197.Name = "lblHang197";
            this.lblHang197.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang197.Text = " ";
            this.lblHang197.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang197.Weight = 0.12534059945504086;
            // 
            // lblHang198
            // 
            this.lblHang198.Name = "lblHang198";
            this.lblHang198.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang198.Text = " ";
            this.lblHang198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang198.Weight = 0.12670299727520437;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell115,
            this.lblHang205,
            this.lblHang206,
            this.lblHang207,
            this.lblHang208});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow36.Weight = 0.2;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell115.Text = " 20. Tiền lãi phải trả do việc thanh toán tiền mua hàng";
            this.xrTableCell115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell115.Weight = 0.49318801089918257;
            // 
            // lblHang205
            // 
            this.lblHang205.Name = "lblHang205";
            this.lblHang205.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang205.Text = " ";
            this.lblHang205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang205.Weight = 0.12942779291553133;
            // 
            // lblHang206
            // 
            this.lblHang206.Name = "lblHang206";
            this.lblHang206.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang206.Text = " ";
            this.lblHang206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang206.Weight = 0.12534059945504086;
            // 
            // lblHang207
            // 
            this.lblHang207.Name = "lblHang207";
            this.lblHang207.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang207.Text = " ";
            this.lblHang207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang207.Weight = 0.12534059945504086;
            // 
            // lblHang208
            // 
            this.lblHang208.Name = "lblHang208";
            this.lblHang208.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang208.Text = " ";
            this.lblHang208.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang208.Weight = 0.12670299727520437;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell131,
            this.lblHang215,
            this.lblHang216,
            this.lblHang217,
            this.lblHang218});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow37.Weight = 0.2;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell131.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell131.Text = " 21. Các khoản thuế, phí, lệ phí phải trả";
            this.xrTableCell131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell131.Weight = 0.49318801089918257;
            // 
            // lblHang215
            // 
            this.lblHang215.Name = "lblHang215";
            this.lblHang215.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang215.Text = " ";
            this.lblHang215.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang215.Weight = 0.12942779291553133;
            // 
            // lblHang216
            // 
            this.lblHang216.Name = "lblHang216";
            this.lblHang216.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang216.Text = " ";
            this.lblHang216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang216.Weight = 0.12534059945504086;
            // 
            // lblHang217
            // 
            this.lblHang217.Name = "lblHang217";
            this.lblHang217.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang217.Text = " ";
            this.lblHang217.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang217.Weight = 0.12534059945504086;
            // 
            // lblHang218
            // 
            this.lblHang218.Name = "lblHang218";
            this.lblHang218.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang218.Text = " ";
            this.lblHang218.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang218.Weight = 0.12670299727520437;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149,
            this.lblHang225,
            this.lblHang226,
            this.lblHang227,
            this.lblHang228});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow39.Weight = 0.2;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell149.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell149.Text = " 22. Không giảm giá ";
            this.xrTableCell149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell149.Weight = 0.49318801089918257;
            // 
            // lblHang225
            // 
            this.lblHang225.Name = "lblHang225";
            this.lblHang225.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang225.Text = " ";
            this.lblHang225.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang225.Weight = 0.12942779291553133;
            // 
            // lblHang226
            // 
            this.lblHang226.Name = "lblHang226";
            this.lblHang226.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang226.Text = " ";
            this.lblHang226.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang226.Weight = 0.12534059945504086;
            // 
            // lblHang227
            // 
            this.lblHang227.Name = "lblHang227";
            this.lblHang227.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang227.Text = " ";
            this.lblHang227.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang227.Weight = 0.12534059945504086;
            // 
            // lblHang228
            // 
            this.lblHang228.Name = "lblHang228";
            this.lblHang228.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang228.Text = " ";
            this.lblHang228.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang228.Weight = 0.12670299727520437;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(65F, 1252F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14});
            this.xrTable3.SizeF = new System.Drawing.SizeF(734F, 75F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.lblHang75,
            this.lblHang76,
            this.lblHang77,
            this.lblHang78});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow12.Weight = 0.33333333333333331;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell34.Text = " 7. Giá mua ghi trên hóa đơn";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell34.Weight = 0.49318801089918257;
            // 
            // lblHang75
            // 
            this.lblHang75.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang75.Name = "lblHang75";
            this.lblHang75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang75.Text = " ";
            this.lblHang75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang75.Weight = 0.12942779291553133;
            // 
            // lblHang76
            // 
            this.lblHang76.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang76.Name = "lblHang76";
            this.lblHang76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang76.Text = " ";
            this.lblHang76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang76.Weight = 0.12534059945504086;
            // 
            // lblHang77
            // 
            this.lblHang77.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang77.Name = "lblHang77";
            this.lblHang77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang77.Text = " ";
            this.lblHang77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang77.Weight = 0.12534059945504086;
            // 
            // lblHang78
            // 
            this.lblHang78.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang78.Name = "lblHang78";
            this.lblHang78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang78.Text = " ";
            this.lblHang78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang78.Weight = 0.12670299727520437;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.lblHang85,
            this.lblHang86,
            this.lblHang87,
            this.lblHang88});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow13.Weight = 0.33333333333333331;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell42.Text = " 8. Khoản thanh toán gián tiếp";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell42.Weight = 0.49318801089918257;
            // 
            // lblHang85
            // 
            this.lblHang85.Name = "lblHang85";
            this.lblHang85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang85.Text = " ";
            this.lblHang85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang85.Weight = 0.12942779291553133;
            // 
            // lblHang86
            // 
            this.lblHang86.Name = "lblHang86";
            this.lblHang86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang86.Text = " ";
            this.lblHang86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang86.Weight = 0.12534059945504086;
            // 
            // lblHang87
            // 
            this.lblHang87.Name = "lblHang87";
            this.lblHang87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang87.Text = " ";
            this.lblHang87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang87.Weight = 0.12534059945504086;
            // 
            // lblHang88
            // 
            this.lblHang88.Name = "lblHang88";
            this.lblHang88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang88.Text = " ";
            this.lblHang88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang88.Weight = 0.12670299727520437;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell62,
            this.lblHang95,
            this.lblHang96,
            this.lblHang97,
            this.lblHang98});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow14.Weight = 0.33333333333333331;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell62.Text = " 9. Khoản tiền, trả trước, ứng trước, đặt cọc";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell62.Weight = 0.49318801089918257;
            // 
            // lblHang95
            // 
            this.lblHang95.Name = "lblHang95";
            this.lblHang95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang95.Text = " ";
            this.lblHang95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang95.Weight = 0.12942779291553133;
            // 
            // lblHang96
            // 
            this.lblHang96.Name = "lblHang96";
            this.lblHang96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang96.Text = " ";
            this.lblHang96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang96.Weight = 0.12534059945504086;
            // 
            // lblHang97
            // 
            this.lblHang97.Name = "lblHang97";
            this.lblHang97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang97.Text = " ";
            this.lblHang97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang97.Weight = 0.12534059945504086;
            // 
            // lblHang98
            // 
            this.lblHang98.Name = "lblHang98";
            this.lblHang98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang98.Text = " ";
            this.lblHang98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang98.Weight = 0.12670299727520437;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(140F, 1752F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow11});
            this.xrTable2.SizeF = new System.Drawing.SizeF(659F, 73F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.lblHang235,
            this.lblHang236,
            this.lblHang237,
            this.lblHang238});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 0.50684931506849318;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell19.Text = " 23. Trị giá  tính thuế nguyên tệ = 7+8+...+17-18-19-...22 ";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell19.Weight = 0.43550834597875571;
            // 
            // lblHang235
            // 
            this.lblHang235.Name = "lblHang235";
            this.lblHang235.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang235.Text = " ";
            this.lblHang235.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang235.Weight = 0.1441578148710167;
            // 
            // lblHang236
            // 
            this.lblHang236.Name = "lblHang236";
            this.lblHang236.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang236.Text = " ";
            this.lblHang236.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang236.Weight = 0.13960546282245828;
            // 
            // lblHang237
            // 
            this.lblHang237.Name = "lblHang237";
            this.lblHang237.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang237.Text = " ";
            this.lblHang237.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang237.Weight = 0.13960546282245828;
            // 
            // lblHang238
            // 
            this.lblHang238.Name = "lblHang238";
            this.lblHang238.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang238.Text = " ";
            this.lblHang238.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang238.Weight = 0.14112291350531109;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.lblHang245,
            this.lblHang246,
            this.lblHang247,
            this.lblHang248});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow11.Weight = 0.49315068493150682;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell28.Text = " 24. Trị giá tính thuế bằng đồng Việt Nam";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell28.Weight = 0.43550834597875571;
            // 
            // lblHang245
            // 
            this.lblHang245.Name = "lblHang245";
            this.lblHang245.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang245.Text = " ";
            this.lblHang245.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang245.Weight = 0.1441578148710167;
            // 
            // lblHang246
            // 
            this.lblHang246.Name = "lblHang246";
            this.lblHang246.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang246.Text = " ";
            this.lblHang246.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang246.Weight = 0.13960546282245828;
            // 
            // lblHang247
            // 
            this.lblHang247.Name = "lblHang247";
            this.lblHang247.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang247.Text = " ";
            this.lblHang247.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang247.Weight = 0.13960546282245828;
            // 
            // lblHang248
            // 
            this.lblHang248.Name = "lblHang248";
            this.lblHang248.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang248.Text = " ";
            this.lblHang248.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang248.Weight = 0.14112291350531109;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel13.BorderWidth = 1;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(23F, 1752F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(117F, 73F);
            this.xrLabel13.Text = " IV. Trị giá tính thuế";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.BorderWidth = 1;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(23F, 1252F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(42F, 504F);
            this.xrLabel10.Text = " ";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.BorderWidth = 1;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(17F, 533F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(41F, 510F);
            this.xrLabel9.Text = " ";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.BorderWidth = 1;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(17F, 1039F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(116F, 75F);
            this.xrLabel8.Text = " VI. Trị giá tính thuế";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(17F, 455F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15,
            this.xrTableRow16});
            this.xrTable1.SizeF = new System.Drawing.SizeF(775F, 83F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow15.Weight = 0.39759036144578314;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell37.Text = " PHẦN XÁC ĐỊNH TRỊ GIÁ TÍNH THUẾ";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell37.Weight = 1;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell40,
            this.xrTableCell31,
            this.xrTableCell43,
            this.xrTableCell44});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow16.Weight = 0.60240963855421692;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell38.Text = " 6. Số thứ tự mặt hàng trong tờ khai hàng hóa nhập khẩu";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell38.Weight = 0.52;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell40.Text = " Mặt hàng số 1";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell40.Weight = 0.12258064516129032;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell31.Text = " Mặt hàng số 2";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell31.Weight = 0.11870967741935484;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell43.Text = " Mặt hàng số 3";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell43.Weight = 0.11870967741935484;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell44.Text = " Mặt hàng số 4";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell44.Weight = 0.12;
            // 
            // xrLabel67
            // 
            this.xrLabel67.BorderWidth = 0;
            this.xrLabel67.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(600F, 2175F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(134F, 17F);
            this.xrLabel67.Text = "(Ký,ghi rõ họ tên)";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel66
            // 
            this.xrLabel66.BorderWidth = 0;
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(350F, 2175F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(167F, 17F);
            this.xrLabel66.Text = "(Ký, ghi rõ họ tên)";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel65
            // 
            this.xrLabel65.BorderWidth = 0;
            this.xrLabel65.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(25F, 2167F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(267F, 42F);
            this.xrLabel65.Text = "(Người khai hải quan ghi rõ họ tên, chức danh,ký, đóng dấu)";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayNGK
            // 
            this.lblNgayNGK.BorderWidth = 0;
            this.lblNgayNGK.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayNGK.LocationFloat = new DevExpress.Utils.PointFloat(33F, 1858F);
            this.lblNgayNGK.Name = "lblNgayNGK";
            this.lblNgayNGK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayNGK.SizeF = new System.Drawing.SizeF(267F, 17F);
            this.lblNgayNGK.Text = "Ngày........... tháng..........năm 20...";
            this.lblNgayNGK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel63
            // 
            this.xrLabel63.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel63.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(557F, 1858F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(242F, 367F);
            this.xrLabel63.Text = " 27. Ghi chép của công chức hải quan kiểm tra, xác định trị giá tính thuế";
            this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel61
            // 
            this.xrLabel61.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel61.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(308F, 1825F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(491F, 33F);
            this.xrLabel61.Text = "DÀNH RIÊNG CHO CÔNG CHỨC HẢI QUAN";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel62.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(308F, 1858F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(250F, 367F);
            this.xrLabel62.Text = " 26. Ghi chép của công chức tiếp nhận tờ khai";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel60.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(23F, 1825F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(285F, 400F);
            this.xrLabel60.Text = " 25. Tôi xin cam đoan và chịu trách nhiệm trước pháp luật về những nội dung khai " +
                "báo trên tờ khai này";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(133F, 1039F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34,
            this.xrTableRow35});
            this.xrTable6.SizeF = new System.Drawing.SizeF(659F, 75F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.lblHang231,
            this.lblHang232,
            this.lblHang233,
            this.lblHang234});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow34.Weight = 0.50666666666666671;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell117.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell117.Text = " 23. Trị giá  tính thuế nguyên tệ = 7+8+...+17-18-19-...22 ";
            this.xrTableCell117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell117.Weight = 0.43550834597875571;
            // 
            // lblHang231
            // 
            this.lblHang231.Name = "lblHang231";
            this.lblHang231.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang231.Text = " ";
            this.lblHang231.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang231.Weight = 0.1441578148710167;
            // 
            // lblHang232
            // 
            this.lblHang232.Name = "lblHang232";
            this.lblHang232.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang232.Text = " ";
            this.lblHang232.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang232.Weight = 0.13960546282245828;
            // 
            // lblHang233
            // 
            this.lblHang233.Name = "lblHang233";
            this.lblHang233.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang233.Text = " ";
            this.lblHang233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang233.Weight = 0.13960546282245828;
            // 
            // lblHang234
            // 
            this.lblHang234.Name = "lblHang234";
            this.lblHang234.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang234.Text = " ";
            this.lblHang234.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang234.Weight = 0.14112291350531109;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142,
            this.lblHang241,
            this.lblHang242,
            this.lblHang243,
            this.lblHang244});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow35.Weight = 0.49333333333333335;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell142.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell142.Text = " 24. Trị giá tính thuế bằng đồng Việt Nam";
            this.xrTableCell142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell142.Weight = 0.43550834597875571;
            // 
            // lblHang241
            // 
            this.lblHang241.Name = "lblHang241";
            this.lblHang241.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang241.Text = " ";
            this.lblHang241.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang241.Weight = 0.1441578148710167;
            // 
            // lblHang242
            // 
            this.lblHang242.Name = "lblHang242";
            this.lblHang242.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang242.Text = " ";
            this.lblHang242.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang242.Weight = 0.13960546282245828;
            // 
            // lblHang243
            // 
            this.lblHang243.Name = "lblHang243";
            this.lblHang243.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang243.Text = " ";
            this.lblHang243.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang243.Weight = 0.13960546282245828;
            // 
            // lblHang244
            // 
            this.lblHang244.Name = "lblHang244";
            this.lblHang244.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang244.Text = " ";
            this.lblHang244.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang244.Weight = 0.14112291350531109;
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable12.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(58F, 538F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9,
            this.xrTableRow38,
            this.xrTableRow40});
            this.xrTable12.SizeF = new System.Drawing.SizeF(734F, 75F);
            this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.lblHang71,
            this.lblHang72,
            this.lblHang73,
            this.lblHang74});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 0.33333333333333331;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.Text = " 7. Giá mua ghi trên hóa đơn";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell15.Weight = 0.49318801089918257;
            // 
            // lblHang71
            // 
            this.lblHang71.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang71.Name = "lblHang71";
            this.lblHang71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang71.Text = " ";
            this.lblHang71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang71.Weight = 0.12942779291553133;
            // 
            // lblHang72
            // 
            this.lblHang72.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang72.Name = "lblHang72";
            this.lblHang72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang72.Text = " ";
            this.lblHang72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang72.Weight = 0.12534059945504086;
            // 
            // lblHang73
            // 
            this.lblHang73.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang73.Name = "lblHang73";
            this.lblHang73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang73.Text = " ";
            this.lblHang73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang73.Weight = 0.12534059945504086;
            // 
            // lblHang74
            // 
            this.lblHang74.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang74.Name = "lblHang74";
            this.lblHang74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang74.Text = " ";
            this.lblHang74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang74.Weight = 0.12670299727520437;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell56,
            this.lblHang81,
            this.lblHang82,
            this.lblHang83,
            this.lblHang84});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow38.Weight = 0.33333333333333331;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell56.Text = " 8. Khoản thanh toán gián tiếp";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell56.Weight = 0.49318801089918257;
            // 
            // lblHang81
            // 
            this.lblHang81.Name = "lblHang81";
            this.lblHang81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang81.Text = " ";
            this.lblHang81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang81.Weight = 0.12942779291553133;
            // 
            // lblHang82
            // 
            this.lblHang82.Name = "lblHang82";
            this.lblHang82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang82.Text = " ";
            this.lblHang82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang82.Weight = 0.12534059945504086;
            // 
            // lblHang83
            // 
            this.lblHang83.Name = "lblHang83";
            this.lblHang83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang83.Text = " ";
            this.lblHang83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang83.Weight = 0.12534059945504086;
            // 
            // lblHang84
            // 
            this.lblHang84.Name = "lblHang84";
            this.lblHang84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang84.Text = " ";
            this.lblHang84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang84.Weight = 0.12670299727520437;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.lblHang91,
            this.lblHang92,
            this.lblHang93,
            this.lblHang94});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow40.Weight = 0.33333333333333331;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell86.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell86.Text = " 9. Khoản tiền, trả trước, ứng trước, đặt cọc";
            this.xrTableCell86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell86.Weight = 0.49318801089918257;
            // 
            // lblHang91
            // 
            this.lblHang91.Name = "lblHang91";
            this.lblHang91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang91.Text = " ";
            this.lblHang91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang91.Weight = 0.12942779291553133;
            // 
            // lblHang92
            // 
            this.lblHang92.Name = "lblHang92";
            this.lblHang92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang92.Text = " ";
            this.lblHang92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang92.Weight = 0.12534059945504086;
            // 
            // lblHang93
            // 
            this.lblHang93.Name = "lblHang93";
            this.lblHang93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang93.Text = " ";
            this.lblHang93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang93.Weight = 0.12534059945504086;
            // 
            // lblHang94
            // 
            this.lblHang94.Name = "lblHang94";
            this.lblHang94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang94.Text = " ";
            this.lblHang94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang94.Weight = 0.12670299727520437;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(58F, 914F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33});
            this.xrTable10.SizeF = new System.Drawing.SizeF(734F, 125F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.lblHang181,
            this.lblHang182,
            this.lblHang183,
            this.lblHang184});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.2;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.Text = " 18. Phí bảo hiểm, vận tải hàng hóa trong nội địa";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell10.Weight = 0.49318801089918257;
            // 
            // lblHang181
            // 
            this.lblHang181.Name = "lblHang181";
            this.lblHang181.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang181.Text = " ";
            this.lblHang181.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang181.Weight = 0.12942779291553133;
            // 
            // lblHang182
            // 
            this.lblHang182.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang182.Name = "lblHang182";
            this.lblHang182.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang182.Text = " ";
            this.lblHang182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang182.Weight = 0.12534059945504086;
            // 
            // lblHang183
            // 
            this.lblHang183.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang183.Name = "lblHang183";
            this.lblHang183.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang183.Text = " ";
            this.lblHang183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang183.Weight = 0.12534059945504086;
            // 
            // lblHang184
            // 
            this.lblHang184.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang184.Name = "lblHang184";
            this.lblHang184.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang184.Text = " ";
            this.lblHang184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang184.Weight = 0.12670299727520437;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell118,
            this.lblHang191,
            this.lblHang192,
            this.lblHang193,
            this.lblHang194});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow30.Weight = 0.2;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell118.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell118.Text = " 19. Chi phí phát sinh sau khi nhập khẩu";
            this.xrTableCell118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell118.Weight = 0.49318801089918257;
            // 
            // lblHang191
            // 
            this.lblHang191.Name = "lblHang191";
            this.lblHang191.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang191.Text = " ";
            this.lblHang191.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang191.Weight = 0.12942779291553133;
            // 
            // lblHang192
            // 
            this.lblHang192.Name = "lblHang192";
            this.lblHang192.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang192.Text = " ";
            this.lblHang192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang192.Weight = 0.12534059945504086;
            // 
            // lblHang193
            // 
            this.lblHang193.Name = "lblHang193";
            this.lblHang193.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang193.Text = " ";
            this.lblHang193.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang193.Weight = 0.12534059945504086;
            // 
            // lblHang194
            // 
            this.lblHang194.Name = "lblHang194";
            this.lblHang194.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang194.Text = " ";
            this.lblHang194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang194.Weight = 0.12670299727520437;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell124,
            this.lblHang201,
            this.lblHang202,
            this.lblHang203,
            this.lblHang204});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow31.Weight = 0.2;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell124.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell124.Text = " 20. Tiền lãi phải trả do việc thanh toán tiền mua hàng";
            this.xrTableCell124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell124.Weight = 0.49318801089918257;
            // 
            // lblHang201
            // 
            this.lblHang201.Name = "lblHang201";
            this.lblHang201.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang201.Text = " ";
            this.lblHang201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang201.Weight = 0.12942779291553133;
            // 
            // lblHang202
            // 
            this.lblHang202.Name = "lblHang202";
            this.lblHang202.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang202.Text = " ";
            this.lblHang202.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang202.Weight = 0.12534059945504086;
            // 
            // lblHang203
            // 
            this.lblHang203.Name = "lblHang203";
            this.lblHang203.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang203.Text = " ";
            this.lblHang203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang203.Weight = 0.12534059945504086;
            // 
            // lblHang204
            // 
            this.lblHang204.Name = "lblHang204";
            this.lblHang204.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang204.Text = " ";
            this.lblHang204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang204.Weight = 0.12670299727520437;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell130,
            this.lblHang211,
            this.lblHang212,
            this.lblHang213,
            this.lblHang214});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow32.Weight = 0.2;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell130.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell130.Text = " 21. Các khoản thuế, phí, lệ phí phải trả";
            this.xrTableCell130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell130.Weight = 0.49318801089918257;
            // 
            // lblHang211
            // 
            this.lblHang211.Name = "lblHang211";
            this.lblHang211.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang211.Text = " ";
            this.lblHang211.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang211.Weight = 0.12942779291553133;
            // 
            // lblHang212
            // 
            this.lblHang212.Name = "lblHang212";
            this.lblHang212.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang212.Text = " ";
            this.lblHang212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang212.Weight = 0.12534059945504086;
            // 
            // lblHang213
            // 
            this.lblHang213.Name = "lblHang213";
            this.lblHang213.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang213.Text = " ";
            this.lblHang213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang213.Weight = 0.12534059945504086;
            // 
            // lblHang214
            // 
            this.lblHang214.Name = "lblHang214";
            this.lblHang214.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang214.Text = " ";
            this.lblHang214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang214.Weight = 0.12670299727520437;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136,
            this.lblHang221,
            this.lblHang222,
            this.lblHang223,
            this.lblHang224});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow33.Weight = 0.2;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell136.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell136.Text = " 22. Không giảm giá ";
            this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell136.Weight = 0.49318801089918257;
            // 
            // lblHang221
            // 
            this.lblHang221.Name = "lblHang221";
            this.lblHang221.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang221.Text = " ";
            this.lblHang221.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang221.Weight = 0.12942779291553133;
            // 
            // lblHang222
            // 
            this.lblHang222.Name = "lblHang222";
            this.lblHang222.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang222.Text = " ";
            this.lblHang222.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang222.Weight = 0.12534059945504086;
            // 
            // lblHang223
            // 
            this.lblHang223.Name = "lblHang223";
            this.lblHang223.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang223.Text = " ";
            this.lblHang223.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang223.Weight = 0.12534059945504086;
            // 
            // lblHang224
            // 
            this.lblHang224.Name = "lblHang224";
            this.lblHang224.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang224.Text = " ";
            this.lblHang224.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang224.Weight = 0.12670299727520437;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(58F, 614F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow25,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow26,
            this.xrTableRow27});
            this.xrTable8.SizeF = new System.Drawing.SizeF(734F, 300F);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.lblHang101,
            this.lblHang102,
            this.lblHang103,
            this.lblHang104});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 0.083333333333333329;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.Text = " 10. Chi phí hoa hồng bán hàng/phí môi giới";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell11.Weight = 0.49318801089918257;
            // 
            // lblHang101
            // 
            this.lblHang101.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang101.Name = "lblHang101";
            this.lblHang101.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang101.Text = " ";
            this.lblHang101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang101.Weight = 0.12942779291553133;
            // 
            // lblHang102
            // 
            this.lblHang102.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang102.Name = "lblHang102";
            this.lblHang102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang102.Text = " ";
            this.lblHang102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang102.Weight = 0.12534059945504086;
            // 
            // lblHang103
            // 
            this.lblHang103.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang103.Name = "lblHang103";
            this.lblHang103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang103.Text = " ";
            this.lblHang103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang103.Weight = 0.12534059945504086;
            // 
            // lblHang104
            // 
            this.lblHang104.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lblHang104.Name = "lblHang104";
            this.lblHang104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang104.Text = " ";
            this.lblHang104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang104.Weight = 0.12670299727520437;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.lblHang111,
            this.lblHang112,
            this.lblHang113,
            this.lblHang114});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow17.Weight = 0.083333333333333329;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell45.Text = " 11. Chi phí gắn liền với hàng hóa";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell45.Weight = 0.49318801089918257;
            // 
            // lblHang111
            // 
            this.lblHang111.Name = "lblHang111";
            this.lblHang111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang111.Text = " ";
            this.lblHang111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang111.Weight = 0.12942779291553133;
            // 
            // lblHang112
            // 
            this.lblHang112.Name = "lblHang112";
            this.lblHang112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang112.Text = " ";
            this.lblHang112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang112.Weight = 0.12534059945504086;
            // 
            // lblHang113
            // 
            this.lblHang113.Name = "lblHang113";
            this.lblHang113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang113.Text = " ";
            this.lblHang113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang113.Weight = 0.12534059945504086;
            // 
            // lblHang114
            // 
            this.lblHang114.Name = "lblHang114";
            this.lblHang114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang114.Text = " ";
            this.lblHang114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang114.Weight = 0.12670299727520437;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.lblHang121,
            this.lblHang122,
            this.lblHang123,
            this.lblHang124});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow18.Weight = 0.083333333333333329;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell51.Text = " 12. Chi phí đóng gói";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell51.Weight = 0.49318801089918257;
            // 
            // lblHang121
            // 
            this.lblHang121.Name = "lblHang121";
            this.lblHang121.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang121.Text = " ";
            this.lblHang121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang121.Weight = 0.12942779291553133;
            // 
            // lblHang122
            // 
            this.lblHang122.Name = "lblHang122";
            this.lblHang122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang122.Text = " ";
            this.lblHang122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang122.Weight = 0.12534059945504086;
            // 
            // lblHang123
            // 
            this.lblHang123.Name = "lblHang123";
            this.lblHang123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang123.Text = " ";
            this.lblHang123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang123.Weight = 0.12534059945504086;
            // 
            // lblHang124
            // 
            this.lblHang124.Name = "lblHang124";
            this.lblHang124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang124.Text = " ";
            this.lblHang124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang124.Weight = 0.12670299727520437;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.lblHang131,
            this.lblHang132,
            this.lblHang133,
            this.lblHang134});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow19.Weight = 0.083333333333333329;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell57.Text = " 13. Các khoản trợ giúp người mua, cung cấp miễn phí hoặc giảm giá";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell57.Weight = 0.49318801089918257;
            // 
            // lblHang131
            // 
            this.lblHang131.Name = "lblHang131";
            this.lblHang131.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang131.Text = " ";
            this.lblHang131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang131.Weight = 0.12942779291553133;
            // 
            // lblHang132
            // 
            this.lblHang132.Name = "lblHang132";
            this.lblHang132.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang132.Text = " ";
            this.lblHang132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang132.Weight = 0.12534059945504086;
            // 
            // lblHang133
            // 
            this.lblHang133.Name = "lblHang133";
            this.lblHang133.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang133.Text = " ";
            this.lblHang133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang133.Weight = 0.12534059945504086;
            // 
            // lblHang134
            // 
            this.lblHang134.Name = "lblHang134";
            this.lblHang134.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang134.Text = " ";
            this.lblHang134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang134.Weight = 0.12670299727520437;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.lblHanga1,
            this.lblHanga2,
            this.lblHanga3,
            this.lblHanga4});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow20.Weight = 0.083333333333333329;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell63.Text = " a. Nguyên vật liệu, bộ phận cấu thành, phụ tùng, chi tiết tương tự";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell63.Weight = 0.49318801089918257;
            // 
            // lblHanga1
            // 
            this.lblHanga1.Name = "lblHanga1";
            this.lblHanga1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanga1.Text = " ";
            this.lblHanga1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHanga1.Weight = 0.12942779291553133;
            // 
            // lblHanga2
            // 
            this.lblHanga2.Name = "lblHanga2";
            this.lblHanga2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanga2.Text = " ";
            this.lblHanga2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHanga2.Weight = 0.12534059945504086;
            // 
            // lblHanga3
            // 
            this.lblHanga3.Name = "lblHanga3";
            this.lblHanga3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanga3.Text = " ";
            this.lblHanga3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHanga3.Weight = 0.12534059945504086;
            // 
            // lblHanga4
            // 
            this.lblHanga4.Name = "lblHanga4";
            this.lblHanga4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanga4.Text = " ";
            this.lblHanga4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHanga4.Weight = 0.12670299727520437;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.lblHangb1,
            this.lblHangb2,
            this.lblHangb3,
            this.lblHangb4});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow21.Weight = 0.083333333333333329;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell20.Text = " b. Vật liệu, nhiên liệu, năng lượng tiêu hao";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell20.Weight = 0.49318801089918257;
            // 
            // lblHangb1
            // 
            this.lblHangb1.Name = "lblHangb1";
            this.lblHangb1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangb1.Text = " ";
            this.lblHangb1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangb1.Weight = 0.12942779291553133;
            // 
            // lblHangb2
            // 
            this.lblHangb2.Name = "lblHangb2";
            this.lblHangb2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangb2.Text = " ";
            this.lblHangb2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangb2.Weight = 0.12534059945504086;
            // 
            // lblHangb3
            // 
            this.lblHangb3.Name = "lblHangb3";
            this.lblHangb3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangb3.Text = " ";
            this.lblHangb3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangb3.Weight = 0.12534059945504086;
            // 
            // lblHangb4
            // 
            this.lblHangb4.Name = "lblHangb4";
            this.lblHangb4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangb4.Text = " ";
            this.lblHangb4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangb4.Weight = 0.12670299727520437;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell75,
            this.lblHangc1,
            this.lblHangc2,
            this.lblHangc3,
            this.lblHangc4});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow22.Weight = 0.083333333333333329;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell75.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell75.Text = " c. Công cụ, dụng cụ, khuôn ráp, khuôn đúc, khuôn mẫu, chi tiết tương tự";
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell75.Weight = 0.49318801089918257;
            // 
            // lblHangc1
            // 
            this.lblHangc1.Name = "lblHangc1";
            this.lblHangc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangc1.Text = " ";
            this.lblHangc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangc1.Weight = 0.12942779291553133;
            // 
            // lblHangc2
            // 
            this.lblHangc2.Name = "lblHangc2";
            this.lblHangc2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangc2.Text = " ";
            this.lblHangc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangc2.Weight = 0.12534059945504086;
            // 
            // lblHangc3
            // 
            this.lblHangc3.Name = "lblHangc3";
            this.lblHangc3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangc3.Text = " ";
            this.lblHangc3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangc3.Weight = 0.12534059945504086;
            // 
            // lblHangc4
            // 
            this.lblHangc4.Name = "lblHangc4";
            this.lblHangc4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangc4.Text = " ";
            this.lblHangc4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangc4.Weight = 0.12670299727520437;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.lblHangd1,
            this.lblHangd2,
            this.lblHangd3,
            this.lblHangd4});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow25.Weight = 0.083333333333333329;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell93.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell93.Text = " d. Bản vẻ/thiết kế kỹ thuật/triển khai, thiết kế mỹ thuật, thi công mẫu,sơ đồ ph" +
                "ác thảo, sản phẩm và dịch vụ tương tự";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell93.Weight = 0.49318801089918257;
            // 
            // lblHangd1
            // 
            this.lblHangd1.Name = "lblHangd1";
            this.lblHangd1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangd1.Text = " ";
            this.lblHangd1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangd1.Weight = 0.12942779291553133;
            // 
            // lblHangd2
            // 
            this.lblHangd2.Name = "lblHangd2";
            this.lblHangd2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangd2.Text = " ";
            this.lblHangd2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangd2.Weight = 0.12534059945504086;
            // 
            // lblHangd3
            // 
            this.lblHangd3.Name = "lblHangd3";
            this.lblHangd3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangd3.Text = " ";
            this.lblHangd3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangd3.Weight = 0.12534059945504086;
            // 
            // lblHangd4
            // 
            this.lblHangd4.Name = "lblHangd4";
            this.lblHangd4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHangd4.Text = " ";
            this.lblHangd4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHangd4.Weight = 0.12670299727520437;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.lblHang141,
            this.lblHang142,
            this.lblHang143,
            this.lblHang144});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow23.Weight = 0.083333333333333329;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell81.Text = " 14. Tiền bản quyền, phí giấy phép";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell81.Weight = 0.49318801089918257;
            // 
            // lblHang141
            // 
            this.lblHang141.Name = "lblHang141";
            this.lblHang141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang141.Text = " ";
            this.lblHang141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang141.Weight = 0.12942779291553133;
            // 
            // lblHang142
            // 
            this.lblHang142.Name = "lblHang142";
            this.lblHang142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang142.Text = " ";
            this.lblHang142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang142.Weight = 0.12534059945504086;
            // 
            // lblHang143
            // 
            this.lblHang143.Name = "lblHang143";
            this.lblHang143.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang143.Text = " ";
            this.lblHang143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang143.Weight = 0.12534059945504086;
            // 
            // lblHang144
            // 
            this.lblHang144.Name = "lblHang144";
            this.lblHang144.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang144.Text = " ";
            this.lblHang144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang144.Weight = 0.12670299727520437;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell87,
            this.lblHang151,
            this.lblHang152,
            this.lblHang153,
            this.lblHang154});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow24.Weight = 0.083333333333333329;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell87.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell87.Text = " 15. Tiền thu được phải trả sau khi định đoạt sử dụng hàng hóa";
            this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell87.Weight = 0.49318801089918257;
            // 
            // lblHang151
            // 
            this.lblHang151.Name = "lblHang151";
            this.lblHang151.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang151.Text = " ";
            this.lblHang151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang151.Weight = 0.12942779291553133;
            // 
            // lblHang152
            // 
            this.lblHang152.Name = "lblHang152";
            this.lblHang152.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang152.Text = " ";
            this.lblHang152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang152.Weight = 0.12534059945504086;
            // 
            // lblHang153
            // 
            this.lblHang153.Name = "lblHang153";
            this.lblHang153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang153.Text = " ";
            this.lblHang153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang153.Weight = 0.12534059945504086;
            // 
            // lblHang154
            // 
            this.lblHang154.Name = "lblHang154";
            this.lblHang154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang154.Text = " ";
            this.lblHang154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang154.Weight = 0.12670299727520437;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.lblHang161,
            this.lblHang162,
            this.lblHang163,
            this.lblHang164});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow26.Weight = 0.083333333333333329;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell99.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell99.Text = " 16. Chi phí vận tải, bốc xếp, chuyển hàng";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell99.Weight = 0.49318801089918257;
            // 
            // lblHang161
            // 
            this.lblHang161.Name = "lblHang161";
            this.lblHang161.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang161.Text = " ";
            this.lblHang161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang161.Weight = 0.12942779291553133;
            // 
            // lblHang162
            // 
            this.lblHang162.Name = "lblHang162";
            this.lblHang162.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang162.Text = " ";
            this.lblHang162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang162.Weight = 0.12534059945504086;
            // 
            // lblHang163
            // 
            this.lblHang163.Name = "lblHang163";
            this.lblHang163.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang163.Text = " ";
            this.lblHang163.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang163.Weight = 0.12534059945504086;
            // 
            // lblHang164
            // 
            this.lblHang164.Name = "lblHang164";
            this.lblHang164.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang164.Text = " ";
            this.lblHang164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang164.Weight = 0.12670299727520437;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.lblHang171,
            this.lblHang172,
            this.lblHang173,
            this.lblHang174});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow27.Weight = 0.083333333333333329;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell105.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell105.Text = " 17. Chi phí bảo hiểm hàng hóa";
            this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell105.Weight = 0.49318801089918257;
            // 
            // lblHang171
            // 
            this.lblHang171.Name = "lblHang171";
            this.lblHang171.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang171.Text = " ";
            this.lblHang171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang171.Weight = 0.12942779291553133;
            // 
            // lblHang172
            // 
            this.lblHang172.Name = "lblHang172";
            this.lblHang172.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang172.Text = " ";
            this.lblHang172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang172.Weight = 0.12534059945504086;
            // 
            // lblHang173
            // 
            this.lblHang173.Name = "lblHang173";
            this.lblHang173.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang173.Text = " ";
            this.lblHang173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang173.Weight = 0.12534059945504086;
            // 
            // lblHang174
            // 
            this.lblHang174.Name = "lblHang174";
            this.lblHang174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHang174.Text = " ";
            this.lblHang174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblHang174.Weight = 0.12670299727520437;
            // 
            // lblSLPTKTG
            // 
            this.lblSLPTKTG.Font = new System.Drawing.Font("Times New Roman", 9.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSLPTKTG.LocationFloat = new DevExpress.Utils.PointFloat(200F, 92F);
            this.lblSLPTKTG.Name = "lblSLPTKTG";
            this.lblSLPTKTG.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSLPTKTG.SizeF = new System.Drawing.SizeF(375F, 17F);
            this.lblSLPTKTG.StylePriority.UseFont = false;
            this.lblSLPTKTG.Text = "Số lượng phụ lục tờ khai trị giá..........tờ ";
            this.lblSLPTKTG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSTK
            // 
            this.lblSTK.Font = new System.Drawing.Font("Times New Roman", 9.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTK.LocationFloat = new DevExpress.Utils.PointFloat(200F, 67F);
            this.lblSTK.Name = "lblSTK";
            this.lblSTK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTK.SizeF = new System.Drawing.SizeF(375F, 17F);
            this.lblSTK.StylePriority.UseFont = false;
            this.lblSTK.Text = "Tờ số.............../...............số";
            this.lblSTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(17F, 114F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow6});
            this.xrTable4.SizeF = new System.Drawing.SizeF(775F, 341F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTrangthai});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 0.093841642228739;
            // 
            // lblTrangthai
            // 
            this.lblTrangthai.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayXK,
            this.xrLabel3});
            this.lblTrangthai.Name = "lblTrangthai";
            this.lblTrangthai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrangthai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTrangthai.Weight = 1;
            // 
            // lblNgayXK
            // 
            this.lblNgayXK.BorderWidth = 0;
            this.lblNgayXK.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayXK.LocationFloat = new DevExpress.Utils.PointFloat(225F, 11F);
            this.lblNgayXK.Name = "lblNgayXK";
            this.lblNgayXK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayXK.SizeF = new System.Drawing.SizeF(383F, 17F);
            this.lblNgayXK.Text = "Ngày............ tháng............ năm 20......";
            this.lblNgayXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BorderWidth = 0;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 11F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(150F, 18F);
            this.xrLabel3.Text = " 1. Ngày xuất khẩu :";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 0.093841642228739;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.Text = "ĐIỀU KIỆN ÁP DỤNG TRỊ GIÁ GIAO DỊCH";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 1;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 0.14369501466275661;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.Text = "\r\n  2. Người mua có đầy đủ quyền định đoạt, quyền định đoạt sau khi nhập khẩu kôn" +
                "g ?";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.8490322580645161;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.chkYes2,
            this.xrLabel5,
            this.chkNo2});
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.Text = " ";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell5.Weight = 0.15096774193548387;
            // 
            // xrLabel11
            // 
            this.xrLabel11.BorderWidth = 0;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(17F, 30F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(25F, 11F);
            this.xrLabel11.Text = "Có";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // chkYes2
            // 
            this.chkYes2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkYes2.BorderWidth = 1;
            this.chkYes2.CanGrow = false;
            this.chkYes2.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkYes2.LocationFloat = new DevExpress.Utils.PointFloat(18F, 11F);
            this.chkYes2.Name = "chkYes2";
            this.chkYes2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkYes2.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkYes2.Text = " ";
            this.chkYes2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BorderWidth = 0;
            this.xrLabel5.CanGrow = false;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(50F, 28F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(50F, 17F);
            this.xrLabel5.Text = "Không";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // chkNo2
            // 
            this.chkNo2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkNo2.CanGrow = false;
            this.chkNo2.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNo2.LocationFloat = new DevExpress.Utils.PointFloat(50F, 11F);
            this.chkNo2.Name = "chkNo2";
            this.chkNo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkNo2.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkNo2.Text = " ";
            this.chkNo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 0.14369501466275661;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.Text = "\r\n 3. Việc mua bán hàng hay giá cả có phụ thuộc vào điều kiện nào dẫn đến việc kh" +
                "ông xác định được trị giá của hàng hóa nhập khẩu không ?";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 0.8490322580645161;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel42,
            this.chkYes3,
            this.chkNo3,
            this.xrLabel36});
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.Text = " ";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell2.Weight = 0.15096774193548387;
            // 
            // xrLabel42
            // 
            this.xrLabel42.BorderWidth = 0;
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(17F, 28F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(25F, 11F);
            this.xrLabel42.Text = "Có";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // chkYes3
            // 
            this.chkYes3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkYes3.BorderWidth = 1;
            this.chkYes3.CanGrow = false;
            this.chkYes3.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkYes3.LocationFloat = new DevExpress.Utils.PointFloat(17F, 8F);
            this.chkYes3.Name = "chkYes3";
            this.chkYes3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkYes3.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkYes3.Text = " ";
            this.chkYes3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // chkNo3
            // 
            this.chkNo3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkNo3.CanGrow = false;
            this.chkNo3.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNo3.LocationFloat = new DevExpress.Utils.PointFloat(50F, 8F);
            this.chkNo3.Name = "chkNo3";
            this.chkNo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkNo3.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkNo3.Text = " ";
            this.chkNo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel36
            // 
            this.xrLabel36.BorderWidth = 0;
            this.xrLabel36.CanGrow = false;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(50F, 25F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(50F, 17F);
            this.xrLabel36.Text = "Không";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 0.26392961876832843;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.CanShrink = true;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.Text = resources.GetString("xrTableCell6.Text");
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 0.8490322580645161;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel53,
            this.chkYes42,
            this.chkNo42,
            this.xrLabel50,
            this.xrLabel49,
            this.chkYes41,
            this.chkNo41,
            this.xrLabel46});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.Text = " ";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 0.15096774193548387;
            // 
            // xrLabel53
            // 
            this.xrLabel53.BorderWidth = 0;
            this.xrLabel53.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(17F, 62F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(25F, 16F);
            this.xrLabel53.Text = "Có";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // chkYes42
            // 
            this.chkYes42.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkYes42.BorderWidth = 1;
            this.chkYes42.CanGrow = false;
            this.chkYes42.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkYes42.LocationFloat = new DevExpress.Utils.PointFloat(17F, 43F);
            this.chkYes42.Name = "chkYes42";
            this.chkYes42.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkYes42.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkYes42.Text = " ";
            this.chkYes42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // chkNo42
            // 
            this.chkNo42.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkNo42.CanGrow = false;
            this.chkNo42.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNo42.LocationFloat = new DevExpress.Utils.PointFloat(50F, 43F);
            this.chkNo42.Name = "chkNo42";
            this.chkNo42.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkNo42.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkNo42.Text = " ";
            this.chkNo42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel50
            // 
            this.xrLabel50.BorderWidth = 0;
            this.xrLabel50.CanGrow = false;
            this.xrLabel50.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(50F, 61F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(50F, 17F);
            this.xrLabel50.Text = "Không";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel49
            // 
            this.xrLabel49.BorderWidth = 0;
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(17F, 27F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(25F, 11F);
            this.xrLabel49.Text = "Có";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // chkYes41
            // 
            this.chkYes41.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkYes41.BorderWidth = 1;
            this.chkYes41.CanGrow = false;
            this.chkYes41.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkYes41.LocationFloat = new DevExpress.Utils.PointFloat(17F, 8F);
            this.chkYes41.Name = "chkYes41";
            this.chkYes41.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkYes41.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkYes41.Text = " ";
            this.chkYes41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // chkNo41
            // 
            this.chkNo41.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkNo41.CanGrow = false;
            this.chkNo41.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNo41.LocationFloat = new DevExpress.Utils.PointFloat(50F, 8F);
            this.chkNo41.Name = "chkNo41";
            this.chkNo41.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkNo41.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkNo41.Text = " ";
            this.chkNo41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel46
            // 
            this.xrLabel46.BorderWidth = 0;
            this.xrLabel46.CanGrow = false;
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(50F, 24F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(50F, 17F);
            this.xrLabel46.Text = "Không";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell9});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 0.26099706744868034;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel48,
            this.lblMoiQuanHe,
            this.xrLabel51});
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 0.8490322580645161;
            // 
            // xrLabel48
            // 
            this.xrLabel48.BorderWidth = 0;
            this.xrLabel48.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(8F, 9F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(400F, 17F);
            this.xrLabel48.Text = "5. Người mua và người bán có mối quan hệ đặc biệt hay không ?     ";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblMoiQuanHe
            // 
            this.lblMoiQuanHe.BorderWidth = 0;
            this.lblMoiQuanHe.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMoiQuanHe.LocationFloat = new DevExpress.Utils.PointFloat(17F, 34F);
            this.lblMoiQuanHe.Name = "lblMoiQuanHe";
            this.lblMoiQuanHe.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMoiQuanHe.SizeF = new System.Drawing.SizeF(400F, 17F);
            this.lblMoiQuanHe.Text = "Nếu có, nêu rõ quan hệ đó.";
            this.lblMoiQuanHe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel51
            // 
            this.xrLabel51.BorderWidth = 0;
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(17F, 51F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(400F, 17F);
            this.xrLabel51.Text = "Mối quan hệ đặc biệt có ảnh hưởng đến trị giá giao dịch không ?";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.chkYes52,
            this.xrLabel68,
            this.xrLabel59,
            this.chkNo52,
            this.xrLabel57,
            this.xrLabel56,
            this.chkNo51,
            this.chkYes51});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.Text = " ";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell9.Weight = 0.15096774193548387;
            // 
            // chkYes52
            // 
            this.chkYes52.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkYes52.BorderWidth = 1;
            this.chkYes52.CanGrow = false;
            this.chkYes52.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkYes52.LocationFloat = new DevExpress.Utils.PointFloat(17F, 42F);
            this.chkYes52.Name = "chkYes52";
            this.chkYes52.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkYes52.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkYes52.Text = " ";
            this.chkYes52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel68
            // 
            this.xrLabel68.BorderWidth = 0;
            this.xrLabel68.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(17F, 61F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(25F, 11F);
            this.xrLabel68.Text = "Có";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel59
            // 
            this.xrLabel59.BorderWidth = 0;
            this.xrLabel59.CanGrow = false;
            this.xrLabel59.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(50F, 58F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(50F, 17F);
            this.xrLabel59.Text = "Không";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // chkNo52
            // 
            this.chkNo52.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkNo52.CanGrow = false;
            this.chkNo52.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNo52.LocationFloat = new DevExpress.Utils.PointFloat(50F, 42F);
            this.chkNo52.Name = "chkNo52";
            this.chkNo52.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkNo52.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkNo52.Text = " ";
            this.chkNo52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel57
            // 
            this.xrLabel57.BorderWidth = 0;
            this.xrLabel57.CanGrow = false;
            this.xrLabel57.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(50F, 23F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(50F, 17F);
            this.xrLabel57.Text = "Không";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel56
            // 
            this.xrLabel56.BorderWidth = 0;
            this.xrLabel56.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(17F, 27F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(25F, 11F);
            this.xrLabel56.Text = "Có";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // chkNo51
            // 
            this.chkNo51.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkNo51.CanGrow = false;
            this.chkNo51.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNo51.LocationFloat = new DevExpress.Utils.PointFloat(50F, 8F);
            this.chkNo51.Name = "chkNo51";
            this.chkNo51.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkNo51.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkNo51.Text = " ";
            this.chkNo51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // chkYes51
            // 
            this.chkYes51.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.chkYes51.BorderWidth = 1;
            this.chkYes51.CanGrow = false;
            this.chkYes51.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkYes51.LocationFloat = new DevExpress.Utils.PointFloat(17F, 8F);
            this.chkYes51.Name = "chkYes51";
            this.chkYes51.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.chkYes51.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.chkYes51.Text = " ";
            this.chkYes51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblKemTheoSTK
            // 
            this.lblKemTheoSTK.Font = new System.Drawing.Font("Times New Roman", 9.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKemTheoSTK.LocationFloat = new DevExpress.Utils.PointFloat(42F, 43F);
            this.lblKemTheoSTK.Name = "lblKemTheoSTK";
            this.lblKemTheoSTK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblKemTheoSTK.SizeF = new System.Drawing.SizeF(708F, 17F);
            this.lblKemTheoSTK.StylePriority.UseFont = false;
            this.lblKemTheoSTK.StylePriority.UseTextAlignment = false;
            this.lblKemTheoSTK.Text = "Kèm theo tờ khai hàng hóa nhập khẩu HQ/2009-TKDTNK Số.........../NK/........../.." +
                "........Ngày........../........20";
            this.lblKemTheoSTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(675F, 92F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(116F, 17F);
            this.xrLabel2.Text = "HQ/2008-TGTT";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(217F, 8F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(450F, 25F);
            this.xrLabel1.Text = "TỜ KHAI TRỊ GIÁ TÍNH THUẾ HÀNG NHẬP KHẨU";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(25F, 8F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(175F, 25F);
            this.xrLabel23.Text = "HẢI QUAN VIỆT NAM";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblSoTiepNhan.LocationFloat = new DevExpress.Utils.PointFloat(683F, 8F);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTiepNhan.SizeF = new System.Drawing.SizeF(109F, 25F);
            this.lblSoTiepNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoHopDong
            // 
            this.lblSoHopDong.CanGrow = false;
            this.lblSoHopDong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoHopDong.LocationFloat = new DevExpress.Utils.PointFloat(186F, 18F);
            this.lblSoHopDong.Multiline = true;
            this.lblSoHopDong.Name = "lblSoHopDong";
            this.lblSoHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoHopDong.SizeF = new System.Drawing.SizeF(100F, 33F);
            this.lblSoHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHHHopDong
            // 
            this.lblNgayHHHopDong.LocationFloat = new DevExpress.Utils.PointFloat(186F, 58F);
            this.lblNgayHHHopDong.Name = "lblNgayHHHopDong";
            this.lblNgayHHHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHHHopDong.SizeF = new System.Drawing.SizeF(66F, 17F);
            this.lblNgayHHHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayHopDong
            // 
            this.lblNgayHopDong.LocationFloat = new DevExpress.Utils.PointFloat(183F, 42F);
            this.lblNgayHopDong.Name = "lblNgayHopDong";
            this.lblNgayHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHopDong.SizeF = new System.Drawing.SizeF(83F, 16F);
            // 
            // lblSoGiayPhep
            // 
            this.lblSoGiayPhep.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoGiayPhep.LocationFloat = new DevExpress.Utils.PointFloat(3F, 33F);
            this.lblSoGiayPhep.Name = "lblSoGiayPhep";
            this.lblSoGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoGiayPhep.SizeF = new System.Drawing.SizeF(91F, 33F);
            // 
            // lblNgayGiayPhep
            // 
            this.lblNgayGiayPhep.LocationFloat = new DevExpress.Utils.PointFloat(95F, 33F);
            this.lblNgayGiayPhep.Name = "lblNgayGiayPhep";
            this.lblNgayGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayGiayPhep.SizeF = new System.Drawing.SizeF(91F, 17F);
            // 
            // lblNgayHHGiayPhep
            // 
            this.lblNgayHHGiayPhep.LocationFloat = new DevExpress.Utils.PointFloat(103F, 58F);
            this.lblNgayHHGiayPhep.Name = "lblNgayHHGiayPhep";
            this.lblNgayHHGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHHGiayPhep.SizeF = new System.Drawing.SizeF(75F, 17F);
            this.lblNgayHHGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.BorderWidth = 0;
            this.lblTenDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoanhNghiep.LocationFloat = new DevExpress.Utils.PointFloat(17F, 30F);
            this.lblTenDoanhNghiep.Multiline = true;
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep.SizeF = new System.Drawing.SizeF(353F, 58F);
            this.lblTenDoanhNghiep.Text = "CÔNG TY CỔ PHẦN DỆT MAY 19/3\r\n60 MẸ NHU THANH KHÊ ĐÀ NẴNG\r\nMID CODE: VNMARTEX478D" +
                "AN";
            this.lblTenDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.BorderWidth = 0;
            this.lblMaDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDoanhNghiep.LocationFloat = new DevExpress.Utils.PointFloat(111F, 2F);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep.SizeF = new System.Drawing.SizeF(260F, 24F);
            // 
            // xrLabel12
            // 
            this.xrLabel12.BorderWidth = 0;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(102F, 17F);
            this.xrLabel12.Text = "5. Loại hình: ";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.BorderWidth = 0;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(8F, 4F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(767F, 17F);
            this.xrLabel7.Text = "ĐIỀU KIỆN ÁP DỤNG TRỊ GIÁ GIAO DỊCH";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.Transparent;
            this.xrControlStyle1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrControlStyle1.BorderWidth = 1;
            this.xrControlStyle1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrControlStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // xrLabel31
            // 
            this.xrLabel31.BorderWidth = 0;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(8F, 5F);
            this.xrLabel31.Multiline = true;
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(266F, 34F);
            this.xrLabel31.Text = "23. Trị giá tính thuế nguyên tệ                                          = 7+8+ ." +
                ".. +17-18-19-...22";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrKD
            // 
            this.xrKD.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrKD.CanGrow = false;
            this.xrKD.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrKD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 10F);
            this.xrKD.Name = "xrKD";
            this.xrKD.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrKD.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrKD.Text = "X";
            this.xrKD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel14
            // 
            this.xrLabel14.BorderWidth = 0;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(83F, 10F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(58F, 17F);
            this.xrLabel14.Text = "Không";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.BorderWidth = 0;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(29F, 9F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(25F, 17F);
            this.xrLabel4.Text = "Có";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel15.CanGrow = false;
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(61F, 11F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel15.Text = "X";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel16.CanGrow = false;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(8F, 10F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel16.Text = "X";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BorderWidth = 0;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(33F, 10F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(25F, 17F);
            this.xrLabel17.Text = "Có";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel18.CanGrow = false;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(58F, 10F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel18.Text = "X";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.BorderWidth = 0;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(83F, 10F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(58F, 17F);
            this.xrLabel19.Text = "Không";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel20.CanGrow = false;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(8F, 34F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel20.Text = "X";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.BorderWidth = 0;
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(33F, 34F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(25F, 17F);
            this.xrLabel21.Text = "Có";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel22.CanGrow = false;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(58F, 34F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel22.Text = "X";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.BorderWidth = 0;
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(83F, 34F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(58F, 17F);
            this.xrLabel24.Text = "Không";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel25.CanGrow = false;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel25.Text = "X";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.BorderWidth = 0;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(33F, 8F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(25F, 17F);
            this.xrLabel26.Text = "Có";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel27.CanGrow = false;
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(58F, 8F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel27.Text = "X";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel28
            // 
            this.xrLabel28.BorderWidth = 0;
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(83F, 8F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(58F, 17F);
            this.xrLabel28.Text = "Không";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.BorderWidth = 0;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(83F, 33F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(58F, 17F);
            this.xrLabel29.Text = "Không";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel30.CanGrow = false;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(58F, 33F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel30.Text = "X";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel32
            // 
            this.xrLabel32.BorderWidth = 0;
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(33F, 33F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(25F, 17F);
            this.xrLabel32.Text = "Có";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel33.CanGrow = false;
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(8F, 33F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel33.Text = "X";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel34
            // 
            this.xrLabel34.BorderWidth = 0;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(83F, 5F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(58F, 17F);
            this.xrLabel34.Text = "Không";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel35.CanGrow = false;
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel35.Text = "X";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel37
            // 
            this.xrLabel37.BorderWidth = 0;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(33F, 6F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(25F, 17F);
            this.xrLabel37.Text = "Có";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel38.CanGrow = false;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(58F, 8F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(13F, 13F);
            this.xrLabel38.Text = "X";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel41
            // 
            this.xrLabel41.BorderWidth = 0;
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(5F, 9F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(83F, 17F);
            this.xrLabel41.Text = "Mặt hàng số ...";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.BorderWidth = 0;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(0F, 8F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(83F, 17F);
            this.xrLabel43.Text = "Mặt hàng số ...";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel44
            // 
            this.xrLabel44.BorderWidth = 0;
            this.xrLabel44.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(0F, 8F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(83F, 17F);
            this.xrLabel44.Text = "Mặt hàng số ...";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel45
            // 
            this.xrLabel45.BorderWidth = 0;
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(0F, 8F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(83F, 17F);
            this.xrLabel45.Text = "Mặt hàng số ...";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 4F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 18F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ToKhaiTriGiaA4
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Margins = new System.Drawing.Printing.Margins(5, 15, 4, 18);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblSoHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblSoTiepNhan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblKemTheoSTK;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell lblTrangthai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lblSTK;
        private DevExpress.XtraReports.UI.XRLabel lblSLPTKTG;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRLabel lblNgayXK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell lblHang111;
        private DevExpress.XtraReports.UI.XRTableCell lblHang112;
        private DevExpress.XtraReports.UI.XRTableCell lblHang113;
        private DevExpress.XtraReports.UI.XRTableCell lblHang114;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblHang121;
        private DevExpress.XtraReports.UI.XRTableCell lblHang122;
        private DevExpress.XtraReports.UI.XRTableCell lblHang123;
        private DevExpress.XtraReports.UI.XRTableCell lblHang124;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell lblHang131;
        private DevExpress.XtraReports.UI.XRTableCell lblHang132;
        private DevExpress.XtraReports.UI.XRTableCell lblHang133;
        private DevExpress.XtraReports.UI.XRTableCell lblHang134;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell lblHanga1;
        private DevExpress.XtraReports.UI.XRTableCell lblHanga2;
        private DevExpress.XtraReports.UI.XRTableCell lblHanga3;
        private DevExpress.XtraReports.UI.XRTableCell lblHanga4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell lblHangb1;
        private DevExpress.XtraReports.UI.XRTableCell lblHangb2;
        private DevExpress.XtraReports.UI.XRTableCell lblHangb3;
        private DevExpress.XtraReports.UI.XRTableCell lblHangb4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell lblHangc1;
        private DevExpress.XtraReports.UI.XRTableCell lblHangc2;
        private DevExpress.XtraReports.UI.XRTableCell lblHangc3;
        private DevExpress.XtraReports.UI.XRTableCell lblHangc4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell lblHangd1;
        private DevExpress.XtraReports.UI.XRTableCell lblHangd2;
        private DevExpress.XtraReports.UI.XRTableCell lblHangd3;
        private DevExpress.XtraReports.UI.XRTableCell lblHangd4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell lblHang141;
        private DevExpress.XtraReports.UI.XRTableCell lblHang142;
        private DevExpress.XtraReports.UI.XRTableCell lblHang143;
        private DevExpress.XtraReports.UI.XRTableCell lblHang144;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell lblHang151;
        private DevExpress.XtraReports.UI.XRTableCell lblHang152;
        private DevExpress.XtraReports.UI.XRTableCell lblHang153;
        private DevExpress.XtraReports.UI.XRTableCell lblHang154;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell lblHang161;
        private DevExpress.XtraReports.UI.XRTableCell lblHang162;
        private DevExpress.XtraReports.UI.XRTableCell lblHang163;
        private DevExpress.XtraReports.UI.XRTableCell lblHang164;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell lblHang171;
        private DevExpress.XtraReports.UI.XRTableCell lblHang172;
        private DevExpress.XtraReports.UI.XRTableCell lblHang173;
        private DevExpress.XtraReports.UI.XRTableCell lblHang174;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell lblHang191;
        private DevExpress.XtraReports.UI.XRTableCell lblHang192;
        private DevExpress.XtraReports.UI.XRTableCell lblHang193;
        private DevExpress.XtraReports.UI.XRTableCell lblHang194;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell lblHang201;
        private DevExpress.XtraReports.UI.XRTableCell lblHang202;
        private DevExpress.XtraReports.UI.XRTableCell lblHang203;
        private DevExpress.XtraReports.UI.XRTableCell lblHang204;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell lblHang211;
        private DevExpress.XtraReports.UI.XRTableCell lblHang212;
        private DevExpress.XtraReports.UI.XRTableCell lblHang213;
        private DevExpress.XtraReports.UI.XRTableCell lblHang214;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell lblHang221;
        private DevExpress.XtraReports.UI.XRTableCell lblHang222;
        private DevExpress.XtraReports.UI.XRTableCell lblHang223;
        private DevExpress.XtraReports.UI.XRTableCell lblHang224;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell lblHang81;
        private DevExpress.XtraReports.UI.XRTableCell lblHang82;
        private DevExpress.XtraReports.UI.XRTableCell lblHang83;
        private DevExpress.XtraReports.UI.XRTableCell lblHang84;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell lblHang91;
        private DevExpress.XtraReports.UI.XRTableCell lblHang92;
        private DevExpress.XtraReports.UI.XRTableCell lblHang93;
        private DevExpress.XtraReports.UI.XRTableCell lblHang94;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell lblHang231;
        private DevExpress.XtraReports.UI.XRTableCell lblHang232;
        private DevExpress.XtraReports.UI.XRTableCell lblHang233;
        private DevExpress.XtraReports.UI.XRTableCell lblHang234;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell lblHang241;
        private DevExpress.XtraReports.UI.XRTableCell lblHang242;
        private DevExpress.XtraReports.UI.XRTableCell lblHang243;
        private DevExpress.XtraReports.UI.XRTableCell lblHang244;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRLabel xrKD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel lblNgayNGK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell lblHang181;
        private DevExpress.XtraReports.UI.XRTableCell lblHang182;
        private DevExpress.XtraReports.UI.XRTableCell lblHang183;
        private DevExpress.XtraReports.UI.XRTableCell lblHang184;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell lblHang101;
        private DevExpress.XtraReports.UI.XRTableCell lblHang102;
        private DevExpress.XtraReports.UI.XRTableCell lblHang103;
        private DevExpress.XtraReports.UI.XRTableCell lblHang104;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell lblHang71;
        private DevExpress.XtraReports.UI.XRTableCell lblHang72;
        private DevExpress.XtraReports.UI.XRTableCell lblHang73;
        private DevExpress.XtraReports.UI.XRTableCell lblHang74;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell lblHang105;
        private DevExpress.XtraReports.UI.XRTableCell lblHang106;
        private DevExpress.XtraReports.UI.XRTableCell lblHang107;
        private DevExpress.XtraReports.UI.XRTableCell lblHang108;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell lblHang115;
        private DevExpress.XtraReports.UI.XRTableCell lblHang116;
        private DevExpress.XtraReports.UI.XRTableCell lblHang117;
        private DevExpress.XtraReports.UI.XRTableCell lblHang118;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell lblHang125;
        private DevExpress.XtraReports.UI.XRTableCell lblHang126;
        private DevExpress.XtraReports.UI.XRTableCell lblHang127;
        private DevExpress.XtraReports.UI.XRTableCell lblHang128;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell lblHang135;
        private DevExpress.XtraReports.UI.XRTableCell lblHang136;
        private DevExpress.XtraReports.UI.XRTableCell lblHang137;
        private DevExpress.XtraReports.UI.XRTableCell lblHang138;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell lblHanga5;
        private DevExpress.XtraReports.UI.XRTableCell lblHanga6;
        private DevExpress.XtraReports.UI.XRTableCell lblHanga7;
        private DevExpress.XtraReports.UI.XRTableCell lblHanga8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell lblHangb5;
        private DevExpress.XtraReports.UI.XRTableCell lblHangb6;
        private DevExpress.XtraReports.UI.XRTableCell lblHangb7;
        private DevExpress.XtraReports.UI.XRTableCell lblHangb8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell lblHangc5;
        private DevExpress.XtraReports.UI.XRTableCell lblHangc6;
        private DevExpress.XtraReports.UI.XRTableCell lblHangc7;
        private DevExpress.XtraReports.UI.XRTableCell lblHangc8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell lblHangd5;
        private DevExpress.XtraReports.UI.XRTableCell lblHangd6;
        private DevExpress.XtraReports.UI.XRTableCell lblHangd7;
        private DevExpress.XtraReports.UI.XRTableCell lblHangd8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell lblHang145;
        private DevExpress.XtraReports.UI.XRTableCell lblHang146;
        private DevExpress.XtraReports.UI.XRTableCell lblHang147;
        private DevExpress.XtraReports.UI.XRTableCell lblHang148;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell lblHang155;
        private DevExpress.XtraReports.UI.XRTableCell lblHang156;
        private DevExpress.XtraReports.UI.XRTableCell lblHang157;
        private DevExpress.XtraReports.UI.XRTableCell lblHang158;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell lblHang165;
        private DevExpress.XtraReports.UI.XRTableCell lblHang166;
        private DevExpress.XtraReports.UI.XRTableCell lblHang167;
        private DevExpress.XtraReports.UI.XRTableCell lblHang168;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell lblHang175;
        private DevExpress.XtraReports.UI.XRTableCell lblHang176;
        private DevExpress.XtraReports.UI.XRTableCell lblHang177;
        private DevExpress.XtraReports.UI.XRTableCell lblHang178;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell lblHang185;
        private DevExpress.XtraReports.UI.XRTableCell lblHang186;
        private DevExpress.XtraReports.UI.XRTableCell lblHang187;
        private DevExpress.XtraReports.UI.XRTableCell lblHang188;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell lblHang195;
        private DevExpress.XtraReports.UI.XRTableCell lblHang196;
        private DevExpress.XtraReports.UI.XRTableCell lblHang197;
        private DevExpress.XtraReports.UI.XRTableCell lblHang198;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell lblHang205;
        private DevExpress.XtraReports.UI.XRTableCell lblHang206;
        private DevExpress.XtraReports.UI.XRTableCell lblHang207;
        private DevExpress.XtraReports.UI.XRTableCell lblHang208;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell lblHang215;
        private DevExpress.XtraReports.UI.XRTableCell lblHang216;
        private DevExpress.XtraReports.UI.XRTableCell lblHang217;
        private DevExpress.XtraReports.UI.XRTableCell lblHang218;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell lblHang225;
        private DevExpress.XtraReports.UI.XRTableCell lblHang226;
        private DevExpress.XtraReports.UI.XRTableCell lblHang227;
        private DevExpress.XtraReports.UI.XRTableCell lblHang228;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell lblHang75;
        private DevExpress.XtraReports.UI.XRTableCell lblHang76;
        private DevExpress.XtraReports.UI.XRTableCell lblHang77;
        private DevExpress.XtraReports.UI.XRTableCell lblHang78;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell lblHang85;
        private DevExpress.XtraReports.UI.XRTableCell lblHang86;
        private DevExpress.XtraReports.UI.XRTableCell lblHang87;
        private DevExpress.XtraReports.UI.XRTableCell lblHang88;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell lblHang95;
        private DevExpress.XtraReports.UI.XRTableCell lblHang96;
        private DevExpress.XtraReports.UI.XRTableCell lblHang97;
        private DevExpress.XtraReports.UI.XRTableCell lblHang98;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell lblHang235;
        private DevExpress.XtraReports.UI.XRTableCell lblHang236;
        private DevExpress.XtraReports.UI.XRTableCell lblHang237;
        private DevExpress.XtraReports.UI.XRTableCell lblHang238;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell lblHang245;
        private DevExpress.XtraReports.UI.XRTableCell lblHang246;
        private DevExpress.XtraReports.UI.XRTableCell lblHang247;
        private DevExpress.XtraReports.UI.XRTableCell lblHang248;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRLabel chkNo2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel chkYes2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel chkNo3;
        private DevExpress.XtraReports.UI.XRLabel chkYes3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel chkNo41;
        private DevExpress.XtraReports.UI.XRLabel chkYes41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel chkNo42;
        private DevExpress.XtraReports.UI.XRLabel chkYes42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel chkYes51;
        private DevExpress.XtraReports.UI.XRLabel chkNo51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel chkNo52;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel chkYes52;
        private DevExpress.XtraReports.UI.XRLabel lblInPL;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer2;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer3;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer5;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer4;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer6;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel lblMoiQuanHe;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;

    }
}
