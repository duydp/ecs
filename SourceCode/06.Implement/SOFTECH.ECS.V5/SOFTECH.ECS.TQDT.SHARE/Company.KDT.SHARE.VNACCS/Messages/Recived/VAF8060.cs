using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAF8060 : BasicVNACC
    {
        public List<VAF8060_HANG> HangMD { get; set; }
        public void LoadVAF8060(string strResult)
        {
            try
            {

                this.GetObject<VAF8060>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAF8060, false, VAF8060.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAF8060.TongSoByte);
                while (true)
                {
                    VAF8060_HANG vad1agHang = new VAF8060_HANG();
                    vad1agHang.GetObject<VAF8060_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAF8060_HANG", false, VAF8060_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAF8060_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAF8060_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAF8060_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAF8060_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }


    }
}
