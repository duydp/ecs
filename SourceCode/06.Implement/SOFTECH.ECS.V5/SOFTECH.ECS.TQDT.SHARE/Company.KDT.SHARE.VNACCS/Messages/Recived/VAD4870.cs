using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAD4870 : BasicVNACC
    {
       // public List<VAF5110_HANG> HangMD { get; set; }
        public void LoadVAD4870(string strResult)
        {
            try
            {

                this.GetObject<VAD4870>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD4870, false, VAD4870.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAD4870.TongSoByte);
               
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }

}
