IF OBJECT_ID(N'dbo.t_View_DinhMucChuaKhaiBao') IS NOT NULL
	DROP VIEW dbo.t_View_DinhMucChuaKhaiBao   
GO
CREATE VIEW dbo.t_View_DinhMucChuaKhaiBao
AS
    SELECT
T.MaSanPham,
T.MaNguyenPhuLieu,
T.DinhMucSuDung + T.DinhMucSuDung*T.TyLeHaoHut / 100 AS DinhMucChung,
T.HopDong_ID
FROM
(
SELECT ID ,
       MaSanPham ,
       MaNguyenPhuLieu ,
       DVT_ID ,
       DinhMucSuDung ,
       TyLeHaoHut ,
       GhiChu ,
       STTHang ,
       Master_ID ,
       NPL_TuCungUng ,
       TenSanPham ,
       TenNPL ,
       MaDDSX,
	   CASE WHEN Master_ID <> 0 THEN (SELECT TOP 1 ID_HopDong FROM dbo.t_KDT_GC_DinhMucDangKy WHERE ID = Master_ID) ELSE Master_ID END AS HopDong_ID
	    FROM dbo.t_KDT_GC_DinhMuc ) T
GO 

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDongAnMaSP]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDongAnMaSP]    
GO
	
-----------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong]    
-- Database: ECS_TQDT_GC_V4_TAT    
-----------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDongAnMaSP]    
 @ID_HopDong bigint    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
SELECT     
 [SoTiepNhan],    
 [TrangThaiXuLy],    
 [NgayTiepNhan],    
 [ID_HopDong],    
 [MaHaiQuan],    
 [MaDoanhNghiep],    
 [GUIDSTR],    
 [DeXuatKhac],    
 [LyDoSua],    
 [ActionStatus],    
 [GuidReference],    
 [NamTN],    
 [HUONGDAN],    
 [PhanLuong],    
 [Huongdan_PL],    
 [LoaiDinhMuc],  
 MaSanPham    
FROM    
 [dbo].[t_KDT_GC_DinhMucDangKy]  INNER JOIN dbo.t_KDT_GC_DinhMuc ON t_KDT_GC_DinhMuc.Master_ID = t_KDT_GC_DinhMucDangKy.ID  
WHERE  TrangThaiXuLy NOT IN (10) AND
 [ID_HopDong] = @ID_HopDong    
GO
GO

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_Load_TB]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_Load_TB]   
GO	    
CREATE PROCEDURE [dbo].[p_GC_ThietBi_Load_TB]    
 @HopDong_ID bigint,    
 @Ma varchar(50)    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
SELECT    
 [HopDong_ID],    
 [Ma],    
 [Ten],    
 [MaHS],    
 [DVT_ID],    
 [SoLuongDangKy],    
 [SoLuongDaNhap],    
 [NuocXX_ID],    
 [TinhTrang],    
 [DonGia],    
 [TriGia],    
 [NguyenTe_ID],    
 [STTHang],    
 [TrangThai]    
FROM    
 [dbo].[t_GC_ThietBi]    
WHERE    
 [HopDong_ID] = @HopDong_ID    
 AND [Ma] = @Ma    

GO

  
IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Update_New]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Update_New]   
GO	     
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Update_New]    
-- Database: ECS_TQDT_GC_V3    
-- Author: Ngo Thanh Tung    
-- Time created: Tuesday, March 06, 2012    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Update_New]    
 @HopDong_ID bigint,    
 @Ma varchar(50),  
 @MaMoi varchar(50),    
 @Ten nvarchar(255),    
 @MaHS varchar(12),    
 @DVT_ID char(3),    
 @SoLuongDangKy numeric(18, 5),    
 @NuocXX_ID char(3),    
 @DonGia float,    
 @TriGia float,    
 @NguyenTe_ID char(3),    
 @STTHang int,    
 @TinhTrang nvarchar(50),    
 @GhiChu nvarchar(256)    
AS    
    
UPDATE    
 [dbo].[t_KDT_GC_ThietBi]    
SET    
 [Ten] = @Ten,  
 [Ma] = @MaMoi ,     
 [MaHS] = @MaHS,    
 [DVT_ID] = @DVT_ID,    
 [SoLuongDangKy] = @SoLuongDangKy,    
 [NuocXX_ID] = @NuocXX_ID,    
 [DonGia] = @DonGia,    
 [TriGia] = @TriGia,    
 [NguyenTe_ID] = @NguyenTe_ID,    
 [STTHang] = @STTHang,    
 [TinhTrang] = @TinhTrang,    
 [GhiChu] = @GhiChu    
WHERE    
 [HopDong_ID] = @HopDong_ID    
 AND [Ma] = @Ma    
      
GO

  
IF OBJECT_ID(N'[dbo].[p_GC_HangMau_Update_New]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_HangMau_Update_New]   
GO	     
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_GC_HangMau_Update_New]    
-- Database: ECS_TQDT_GC_V3    
-- Author: Ngo Thanh Tung    
-- Time created: Wednesday, March 14, 2012    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_GC_HangMau_Update_New]    
 @HopDong_ID bigint,    
 @Ma varchar(50),  
 @MaMoi varchar(50),   
 @Ten nvarchar(255),    
 @MaHS varchar(12),    
 @DVT_ID char(3),    
 @SoLuongDangKy numeric(18, 6),    
 @SoLuongDaNhap numeric(18, 6),    
 @SoLuongDaDung numeric(18, 6),    
 @SoLuongCungUng numeric(18, 6),    
 @TongNhuCau numeric(18, 6),    
 @STTHang int,    
 @TrangThai int,    
 @DonGia numeric(18, 5)    
AS    
    
UPDATE    
 [dbo].[t_GC_HangMau]    
SET    
 [Ten] = @Ten,    
 [Ma] = @MaMoi ,   
 [MaHS] = @MaHS,    
 [DVT_ID] = @DVT_ID,    
 [SoLuongDangKy] = @SoLuongDangKy,    
 [SoLuongDaNhap] = @SoLuongDaNhap,    
 [SoLuongDaDung] = @SoLuongDaDung,    
 [SoLuongCungUng] = @SoLuongCungUng,    
 [TongNhuCau] = @TongNhuCau,    
 [STTHang] = @STTHang,    
 [TrangThai] = @TrangThai,    
 [DonGia] = @DonGia    
WHERE    
 [HopDong_ID] = @HopDong_ID    
 AND [Ma] = @Ma    
  
GO

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Update_New]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Update_New]   
GO	

------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Update_New]    
-- Database: ECS_TQDT_GC_V3    
-- Author: Ngo Thanh Tung    
-- Time created: Monday, March 05, 2012    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Update_New]    
 @HopDong_ID bigint,    
 @Ma varchar(50),  
 @MaMoi varchar(50),    
 @Ten nvarchar(255),    
 @MaHS varchar(12),    
 @DVT_ID char(3),    
 @SoLuongDangKy numeric(18, 5),    
 @STTHang int,    
 @GhiChu nvarchar(1000)    
AS    
    
UPDATE    
 [dbo].[t_KDT_GC_HangMau]    
SET    
 [Ten] = @Ten,  
 [Ma] = @MaMoi ,    
 [MaHS] = @MaHS,    
 [DVT_ID] = @DVT_ID,    
 [SoLuongDangKy] = @SoLuongDangKy,    
 [STTHang] = @STTHang,    
 [GhiChu] = @GhiChu    
WHERE    
 [HopDong_ID] = @HopDong_ID    
 AND [Ma] = @Ma    

GO           
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.4',GETDATE(), N'CẬP NHẬT VIEW XỬ LÝ HỢP ĐỒNG VỚI ĐỊNH MỨC CHƯA KHAI BÁO')
END

GO