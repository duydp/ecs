namespace Company.Interface.Report.VNACCS
{
    partial class PhanLoaiKiemTraThucTe_VAD4870
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhanLoaiKiemTraThucTe_VAD4870));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoVanDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoMAWB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemLuuKho = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbltenDiaDiemLuuKho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaSoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTTrongLuongHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTauDuKienChatHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenTauDuKienChatHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiXuatKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiXuatKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoKyHieu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoContainer = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoCuaContainerBiKiemHoa1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoCuaContainerBiKiemHoa2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoCuaContainerBiKiemHoa3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoCuaContainerBiKiemHoa4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoCuaContainerBiKiemHoa5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoBaoCoHon5ContainerBiKiemHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemGiaoHangDich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoQuanLyNoiBoDoanhNghiep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiKiemTra = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenPhanLoaiKiemTra = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiKiemTraBanDau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiKiemHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNoiDungPhanLoaiKiemHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiPhuTrach = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNguoiChuyenLuongKiemHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemThucHienKiemHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDiaDiemThucHienKiemHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNoiDungChiDinhKiemHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhuongPhapChiDinhKiemHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenPhuongPhapChiDinhKiemHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoBaoKiemHoaToanBo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanHaiQuan = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNhomXuLyHoSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiKiemTraThucTe = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.xrTable1});
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Detail.HeightF = 557.1476F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(3.689894F, 35.15625F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow5,
            this.xrTableRow4,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow18,
            this.xrTableRow17,
            this.xrTableRow19});
            this.xrTable2.SizeF = new System.Drawing.SizeF(755.3101F, 445.0193F);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.lblSoVanDon,
            this.xrTableCell15,
            this.lblSoMAWB});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.Text = "Số vận đơn";
            this.xrTableCell12.Weight = 0.37423551195529225;
            // 
            // lblSoVanDon
            // 
            this.lblSoVanDon.Name = "lblSoVanDon";
            this.lblSoVanDon.Text = "XXXXXXXXX1XXXXXXXX2XXXXXXXX3XXXXE";
            this.lblSoVanDon.Weight = 1.4208358021324476;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.Text = "Số MAWB";
            this.xrTableCell15.Weight = 0.34514527069215684;
            // 
            // lblSoMAWB
            // 
            this.lblSoMAWB.Name = "lblSoMAWB";
            this.lblSoMAWB.Text = "XXXXXXXX1XXXXXXXXE";
            this.lblSoMAWB.Weight = 0.85978341522010315;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.lblMaDiaDiemLuuKho,
            this.lbltenDiaDiemLuuKho,
            this.xrTableCell19});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.Text = "Địa điểm lưu kho hàng chờ thông quan dự kiến";
            this.xrTableCell16.Weight = 1.2206463057035986;
            // 
            // lblMaDiaDiemLuuKho
            // 
            this.lblMaDiaDiemLuuKho.Name = "lblMaDiaDiemLuuKho";
            this.lblMaDiaDiemLuuKho.Text = "XXXE";
            this.lblMaDiaDiemLuuKho.Weight = 0.3166735597195115;
            // 
            // lbltenDiaDiemLuuKho
            // 
            this.lbltenDiaDiemLuuKho.Name = "lbltenDiaDiemLuuKho";
            this.lbltenDiaDiemLuuKho.Text = "XXXXXXXX1XXXXXXXXE";
            this.lbltenDiaDiemLuuKho.Weight = 1.0826638824569914;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.38001625211989881;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.lblSoLuong,
            this.xrTableCell29,
            this.lblDonGiaSoLuong,
            this.xrTableCell28,
            this.lblTrongLuongHang,
            this.xrTableCell31,
            this.lblDVTTrongLuongHang});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.Text = "Số lượng";
            this.xrTableCell24.Weight = 0.276774095424296;
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.StylePriority.UseTextAlignment = false;
            this.lblSoLuong.Text = "12,345,678";
            this.lblSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuong.Weight = 0.37191950032772314;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "-";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 0.10612380940149538;
            // 
            // lblDonGiaSoLuong
            // 
            this.lblDonGiaSoLuong.Name = "lblDonGiaSoLuong";
            this.lblDonGiaSoLuong.Text = "XXE";
            this.lblDonGiaSoLuong.Weight = 0.32135917756489712;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.Text = "Tổng trọng lượng hàng (Gross)";
            this.xrTableCell28.Weight = 0.96191170864079412;
            // 
            // lblTrongLuongHang
            // 
            this.lblTrongLuongHang.Name = "lblTrongLuongHang";
            this.lblTrongLuongHang.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongHang.Text = "1.234.567.891";
            this.lblTrongLuongHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongHang.Weight = 0.35281823226660292;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "-";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell31.Weight = 0.068498051376239516;
            // 
            // lblDVTTrongLuongHang
            // 
            this.lblDVTTrongLuongHang.Name = "lblDVTTrongLuongHang";
            this.lblDVTTrongLuongHang.Text = "XXE";
            this.lblDVTTrongLuongHang.Weight = 0.54059542499795166;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.lblMaTauDuKienChatHang,
            this.lblTenTauDuKienChatHang,
            this.xrTableCell23});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.Text = "Mã tàu dự kiến chất hàng";
            this.xrTableCell20.Weight = 0.82197715514590541;
            // 
            // lblMaTauDuKienChatHang
            // 
            this.lblMaTauDuKienChatHang.Name = "lblMaTauDuKienChatHang";
            this.lblMaTauDuKienChatHang.Text = "XXXXXXXXE";
            this.lblMaTauDuKienChatHang.Weight = 0.42419621701163857;
            // 
            // lblTenTauDuKienChatHang
            // 
            this.lblTenTauDuKienChatHang.Name = "lblTenTauDuKienChatHang";
            this.lblTenTauDuKienChatHang.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXX3XXXXE";
            this.lblTenTauDuKienChatHang.Weight = 1.7141077745991658;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.039718853243289942;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.lblMaNguoiXuatKhau,
            this.xrTableCell35});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.Text = "Người xuất khẩu";
            this.xrTableCell32.Weight = 0.509634913546468;
            // 
            // lblMaNguoiXuatKhau
            // 
            this.lblMaNguoiXuatKhau.Name = "lblMaNguoiXuatKhau";
            this.lblMaNguoiXuatKhau.StylePriority.UseTextAlignment = false;
            this.lblMaNguoiXuatKhau.Text = "XXXXXXXXX1XXE";
            this.lblMaNguoiXuatKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMaNguoiXuatKhau.Weight = 0.77845337781273782;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Weight = 1.711911708640794;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.lblTenNguoiXuatKhau});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 2.5643122916813557;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 0.50963485160280464;
            // 
            // lblTenNguoiXuatKhau
            // 
            this.lblTenNguoiXuatKhau.Name = "lblTenNguoiXuatKhau";
            this.lblTenNguoiXuatKhau.StylePriority.UseTextAlignment = false;
            this.lblTenNguoiXuatKhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiXuatKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTenNguoiXuatKhau.Weight = 2.4903651483971951;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.lblSoKyHieu});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 3.1036479025133947;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.Text = "Số ký hiệu";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell34.Weight = 0.509634913546468;
            // 
            // lblSoKyHieu
            // 
            this.lblSoKyHieu.Name = "lblSoKyHieu";
            this.lblSoKyHieu.StylePriority.UseTextAlignment = false;
            this.lblSoKyHieu.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXXE";
            this.lblSoKyHieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoKyHieu.Weight = 2.4903650864535321;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.lblTongSoContainer,
            this.xrTableCell43,
            this.xrTableCell44});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.9999170515709217;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.Text = "Tổng số Container";
            this.xrTableCell37.Weight = 0.50963492475256456;
            // 
            // lblTongSoContainer
            // 
            this.lblTongSoContainer.Name = "lblTongSoContainer";
            this.lblTongSoContainer.Text = "123";
            this.lblTongSoContainer.Weight = 1.4903650752474353;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Weight = 0.5;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 0.5;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.lblSoCuaContainerBiKiemHoa1,
            this.lblSoCuaContainerBiKiemHoa2,
            this.lblSoCuaContainerBiKiemHoa3,
            this.lblSoCuaContainerBiKiemHoa4,
            this.lblSoCuaContainerBiKiemHoa5});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.9999170515709217;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.Text = "Số của Container bị kiểm hóa";
            this.xrTableCell40.Weight = 0.81376247576870742;
            // 
            // lblSoCuaContainerBiKiemHoa1
            // 
            this.lblSoCuaContainerBiKiemHoa1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoCuaContainerBiKiemHoa1.Name = "lblSoCuaContainerBiKiemHoa1";
            this.lblSoCuaContainerBiKiemHoa1.StylePriority.UseFont = false;
            this.lblSoCuaContainerBiKiemHoa1.Text = "XXXXXXXXX1XE";
            this.lblSoCuaContainerBiKiemHoa1.Weight = 0.43241094675477848;
            // 
            // lblSoCuaContainerBiKiemHoa2
            // 
            this.lblSoCuaContainerBiKiemHoa2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoCuaContainerBiKiemHoa2.Name = "lblSoCuaContainerBiKiemHoa2";
            this.lblSoCuaContainerBiKiemHoa2.StylePriority.UseFont = false;
            this.lblSoCuaContainerBiKiemHoa2.Text = "XXXXXXXXX1XE";
            this.lblSoCuaContainerBiKiemHoa2.Weight = 0.43565657628449905;
            // 
            // lblSoCuaContainerBiKiemHoa3
            // 
            this.lblSoCuaContainerBiKiemHoa3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoCuaContainerBiKiemHoa3.Name = "lblSoCuaContainerBiKiemHoa3";
            this.lblSoCuaContainerBiKiemHoa3.StylePriority.UseFont = false;
            this.lblSoCuaContainerBiKiemHoa3.Text = "XXXXXXXXX1XE";
            this.lblSoCuaContainerBiKiemHoa3.Weight = 0.43989646235675928;
            // 
            // lblSoCuaContainerBiKiemHoa4
            // 
            this.lblSoCuaContainerBiKiemHoa4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoCuaContainerBiKiemHoa4.Name = "lblSoCuaContainerBiKiemHoa4";
            this.lblSoCuaContainerBiKiemHoa4.StylePriority.UseFont = false;
            this.lblSoCuaContainerBiKiemHoa4.Text = "XXXXXXXXX1XE";
            this.lblSoCuaContainerBiKiemHoa4.Weight = 0.43692238544947187;
            // 
            // lblSoCuaContainerBiKiemHoa5
            // 
            this.lblSoCuaContainerBiKiemHoa5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoCuaContainerBiKiemHoa5.Name = "lblSoCuaContainerBiKiemHoa5";
            this.lblSoCuaContainerBiKiemHoa5.StylePriority.UseFont = false;
            this.lblSoCuaContainerBiKiemHoa5.Text = "XXXXXXXXX1XE";
            this.lblSoCuaContainerBiKiemHoa5.Weight = 0.44135115338578412;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.lblCoBaoCoHon5ContainerBiKiemHoa,
            this.xrTableCell53,
            this.xrTableCell54});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.9999170515709217;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.Text = "Cờ báo có hơn 5 Container bị kiểm hóa";
            this.xrTableCell49.Weight = 1.0763724827738872;
            // 
            // lblCoBaoCoHon5ContainerBiKiemHoa
            // 
            this.lblCoBaoCoHon5ContainerBiKiemHoa.Name = "lblCoBaoCoHon5ContainerBiKiemHoa";
            this.lblCoBaoCoHon5ContainerBiKiemHoa.Text = "X";
            this.lblCoBaoCoHon5ContainerBiKiemHoa.Weight = 1.0167107988872755;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Weight = 0.46556556495305335;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Weight = 0.44135115338578412;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.lblMaDiaDiemGiaoHangDich,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.9999170515709217;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.Text = "Mã địa điểm giao hàng/đích";
            this.xrTableCell55.Weight = 0.76005638919950236;
            // 
            // lblMaDiaDiemGiaoHangDich
            // 
            this.lblMaDiaDiemGiaoHangDich.Name = "lblMaDiaDiemGiaoHangDich";
            this.lblMaDiaDiemGiaoHangDich.Text = "XXXXE";
            this.lblMaDiaDiemGiaoHangDich.Weight = 0.39660646479958511;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Weight = 0.46788030095328892;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Weight = 0.46854012670878642;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.46556556495305335;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Weight = 0.44135115338578412;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.lblSoQuanLyNoiBoDoanhNghiep,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.9999170515709217;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.Text = "Số quản lý nội bộ doanh nghiệp";
            this.xrTableCell50.Weight = 0.82197725400565191;
            // 
            // lblSoQuanLyNoiBoDoanhNghiep
            // 
            this.lblSoQuanLyNoiBoDoanhNghiep.Name = "lblSoQuanLyNoiBoDoanhNghiep";
            this.lblSoQuanLyNoiBoDoanhNghiep.Text = "XXXXXXXXX1XXXXXXXXE";
            this.lblSoQuanLyNoiBoDoanhNghiep.Weight = 0.8025659009467242;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Weight = 0.46854012670878642;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.46556556495305335;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Weight = 0.44135115338578412;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.lblMaPhanLoaiKiemTra,
            this.lblTenPhanLoaiKiemTra,
            this.xrTableCell67,
            this.lblPhanLoaiKiemTraBanDau,
            this.xrTableCell70});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.9999170515709217;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseFont = false;
            this.xrTableCell65.Text = "Phân loại kiểm tra";
            this.xrTableCell65.Weight = 0.54823102971570181;
            // 
            // lblMaPhanLoaiKiemTra
            // 
            this.lblMaPhanLoaiKiemTra.Name = "lblMaPhanLoaiKiemTra";
            this.lblMaPhanLoaiKiemTra.Text = "X";
            this.lblMaPhanLoaiKiemTra.Weight = 0.093002726414971829;
            // 
            // lblTenPhanLoaiKiemTra
            // 
            this.lblTenPhanLoaiKiemTra.Name = "lblTenPhanLoaiKiemTra";
            this.lblTenPhanLoaiKiemTra.Text = "WWWWWWWWW1WWWE";
            this.lblTenPhanLoaiKiemTra.Weight = 0.79521229838577023;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.Text = "Phân loại kiểm tra ban đầu";
            this.xrTableCell67.Weight = 0.65663722587662421;
            // 
            // lblPhanLoaiKiemTraBanDau
            // 
            this.lblPhanLoaiKiemTraBanDau.Name = "lblPhanLoaiKiemTraBanDau";
            this.lblPhanLoaiKiemTraBanDau.Text = "XXE";
            this.lblPhanLoaiKiemTraBanDau.Weight = 0.46556556622114803;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Weight = 0.44135115338578412;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.lblMaPhanLoaiKiemHoa,
            this.lblNoiDungPhanLoaiKiemHoa,
            this.xrTableCell75});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.9999170515709217;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.Text = "Phân loại kiểm hóa";
            this.xrTableCell51.Weight = 0.54823105263444383;
            // 
            // lblMaPhanLoaiKiemHoa
            // 
            this.lblMaPhanLoaiKiemHoa.Name = "lblMaPhanLoaiKiemHoa";
            this.lblMaPhanLoaiKiemHoa.Text = "X";
            this.lblMaPhanLoaiKiemHoa.Weight = 0.093002741328866656;
            // 
            // lblNoiDungPhanLoaiKiemHoa
            // 
            this.lblNoiDungPhanLoaiKiemHoa.Name = "lblNoiDungPhanLoaiKiemHoa";
            this.lblNoiDungPhanLoaiKiemHoa.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWE";
            this.lblNoiDungPhanLoaiKiemHoa.Weight = 1.9174150647529387;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Weight = 0.44135114128375108;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.lblMaPhanLoaiPhuTrach,
            this.xrTableCell82,
            this.xrTableCell74,
            this.lblNguoiChuyenLuongKiemHoa});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.9999170515709217;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseFont = false;
            this.xrTableCell68.Text = "Phân loại phụ trách";
            this.xrTableCell68.Weight = 0.54823105263444383;
            // 
            // lblMaPhanLoaiPhuTrach
            // 
            this.lblMaPhanLoaiPhuTrach.Name = "lblMaPhanLoaiPhuTrach";
            this.lblMaPhanLoaiPhuTrach.Text = "X";
            this.lblMaPhanLoaiPhuTrach.Weight = 0.093002741328866656;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Weight = 0.89608602228527823;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseFont = false;
            this.xrTableCell74.Text = "Người chuyển luồng kiểm hóa";
            this.xrTableCell74.Weight = 0.797472950490858;
            // 
            // lblNguoiChuyenLuongKiemHoa
            // 
            this.lblNguoiChuyenLuongKiemHoa.Name = "lblNguoiChuyenLuongKiemHoa";
            this.lblNguoiChuyenLuongKiemHoa.Text = "XXXXXXXE";
            this.lblNguoiChuyenLuongKiemHoa.Weight = 0.66520723326055353;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.lblMaDiaDiemThucHienKiemHoa,
            this.lblTenDiaDiemThucHienKiemHoa,
            this.xrTableCell86});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.9999170515709217;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseFont = false;
            this.xrTableCell83.Text = "Địa điểm thực hiện kiểm hóa";
            this.xrTableCell83.Weight = 0.76005638868891279;
            // 
            // lblMaDiaDiemThucHienKiemHoa
            // 
            this.lblMaDiaDiemThucHienKiemHoa.Name = "lblMaDiaDiemThucHienKiemHoa";
            this.lblMaDiaDiemThucHienKiemHoa.Text = "XXE";
            this.lblMaDiaDiemThucHienKiemHoa.Weight = 0.25424126465431379;
            // 
            // lblTenDiaDiemThucHienKiemHoa
            // 
            this.lblTenDiaDiemThucHienKiemHoa.Name = "lblTenDiaDiemThucHienKiemHoa";
            this.lblTenDiaDiemThucHienKiemHoa.Text = "XXXXXXXX1XXXXXXXXE";
            this.lblTenDiaDiemThucHienKiemHoa.Weight = 1.5443512053730224;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 0.44135114128375108;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell78,
            this.lblNoiDungChiDinhKiemHoa});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 5.9625628502115307;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseFont = false;
            this.xrTableCell78.StylePriority.UseTextAlignment = false;
            this.xrTableCell78.Text = "Nội dung chỉ định kiểm hóa";
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell78.Weight = 0.73141293696867016;
            // 
            // lblNoiDungChiDinhKiemHoa
            // 
            this.lblNoiDungChiDinhKiemHoa.Name = "lblNoiDungChiDinhKiemHoa";
            this.lblNoiDungChiDinhKiemHoa.StylePriority.UseTextAlignment = false;
            this.lblNoiDungChiDinhKiemHoa.Text = resources.GetString("lblNoiDungChiDinhKiemHoa.Text");
            this.lblNoiDungChiDinhKiemHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblNoiDungChiDinhKiemHoa.Weight = 2.26858706303133;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell87,
            this.lblMaPhuongPhapChiDinhKiemHoa,
            this.lblTenPhuongPhapChiDinhKiemHoa,
            this.xrTableCell89,
            this.lblCoBaoKiemHoaToanBo});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.99992253003313691;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseFont = false;
            this.xrTableCell87.Text = "Phương pháp chỉ định kiểm hóa";
            this.xrTableCell87.Weight = 0.79829525468334073;
            // 
            // lblMaPhuongPhapChiDinhKiemHoa
            // 
            this.lblMaPhuongPhapChiDinhKiemHoa.Name = "lblMaPhuongPhapChiDinhKiemHoa";
            this.lblMaPhuongPhapChiDinhKiemHoa.Text = "X";
            this.lblMaPhuongPhapChiDinhKiemHoa.Weight = 0.0691736913747897;
            // 
            // lblTenPhuongPhapChiDinhKiemHoa
            // 
            this.lblTenPhuongPhapChiDinhKiemHoa.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenPhuongPhapChiDinhKiemHoa.Name = "lblTenPhuongPhapChiDinhKiemHoa";
            this.lblTenPhuongPhapChiDinhKiemHoa.StylePriority.UseFont = false;
            this.lblTenPhuongPhapChiDinhKiemHoa.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WE";
            this.lblTenPhuongPhapChiDinhKiemHoa.Weight = 1.3596943953058711;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseFont = false;
            this.xrTableCell89.Text = "Cờ báo kiểm hóa toàn bộ";
            this.xrTableCell89.Weight = 0.63189374087284356;
            // 
            // lblCoBaoKiemHoaToanBo
            // 
            this.lblCoBaoKiemHoaToanBo.Name = "lblCoBaoKiemHoaToanBo";
            this.lblCoBaoKiemHoaToanBo.Text = "X";
            this.lblCoBaoKiemHoaToanBo.Weight = 0.14094291776315526;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(3.689894F, 9.99999F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(745.3101F, 14.99998F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.lblSoToKhai,
            this.xrTableCell7,
            this.lblMaLoaiHinh,
            this.xrTableCell8,
            this.lblMaPhanLoaiHangHoa,
            this.lblMaPhanLoaiVanChuyen,
            this.xrTableCell2,
            this.lblMaNguoiKhai,
            this.xrTableCell6,
            this.lblCoQuanHaiQuan,
            this.lblNhomXuLyHoSo});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "Số tờ khai";
            this.xrTableCell4.Weight = 0.27677408510035206;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Text = "NNNNNNNN1NE";
            this.lblSoToKhai.Weight = 0.47804332521513448;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Text = "Mã loại hình";
            this.xrTableCell7.Weight = 0.33599705367030241;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaLoaiHinh.Weight = 0.14620959420004134;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "-";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.051064251240277317;
            // 
            // lblMaPhanLoaiHangHoa
            // 
            this.lblMaPhanLoaiHangHoa.Name = "lblMaPhanLoaiHangHoa";
            this.lblMaPhanLoaiHangHoa.Text = "X";
            this.lblMaPhanLoaiHangHoa.Weight = 0.040085851904181263;
            // 
            // lblMaPhanLoaiVanChuyen
            // 
            this.lblMaPhanLoaiVanChuyen.Name = "lblMaPhanLoaiVanChuyen";
            this.lblMaPhanLoaiVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiVanChuyen.Text = "X";
            this.lblMaPhanLoaiVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMaPhanLoaiVanChuyen.Weight = 0.131571811010131;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.Text = "Mã người khai";
            this.xrTableCell2.Weight = 0.40851413380954527;
            // 
            // lblMaNguoiKhai
            // 
            this.lblMaNguoiKhai.Name = "lblMaNguoiKhai";
            this.lblMaNguoiKhai.Text = "XXXXE";
            this.lblMaNguoiKhai.Weight = 0.23902152323432474;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.Text = "Cơ quan Hải quan";
            this.xrTableCell6.Weight = 0.45858827108298;
            // 
            // lblCoQuanHaiQuan
            // 
            this.lblCoQuanHaiQuan.Name = "lblCoQuanHaiQuan";
            this.lblCoQuanHaiQuan.Text = "XXXXXE";
            this.lblCoQuanHaiQuan.Weight = 0.27195647346795976;
            // 
            // lblNhomXuLyHoSo
            // 
            this.lblNhomXuLyHoSo.Name = "lblNhomXuLyHoSo";
            this.lblNhomXuLyHoSo.Text = "XE";
            this.lblNhomXuLyHoSo.Weight = 0.16217362606477029;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.lblPhanLoaiKiemTraThucTe});
            this.TopMargin.HeightF = 92F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(107.1118F, 20.83333F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(556.2115F, 27.395F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Thông báo yêu cầu kiểm tra thực tế hàng hóa";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblPhanLoaiKiemTraThucTe
            // 
            this.lblPhanLoaiKiemTraThucTe.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblPhanLoaiKiemTraThucTe.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiKiemTraThucTe.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 55.93834F);
            this.lblPhanLoaiKiemTraThucTe.Name = "lblPhanLoaiKiemTraThucTe";
            this.lblPhanLoaiKiemTraThucTe.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiKiemTraThucTe.SizeF = new System.Drawing.SizeF(526.0032F, 19.06166F);
            this.lblPhanLoaiKiemTraThucTe.StylePriority.UseBorders = false;
            this.lblPhanLoaiKiemTraThucTe.StylePriority.UseFont = false;
            this.lblPhanLoaiKiemTraThucTe.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiKiemTraThucTe.Text = "CHỨNG TỪ GHI SỐ THUẾ PHẢI THU";
            this.lblPhanLoaiKiemTraThucTe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PhanLoaiKiemTraThucTe_VAD4870
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(52, 36, 92, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiKiemTraThucTe;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell lblNhomXuLyHoSo;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell lblSoVanDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell lblSoMAWB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemLuuKho;
        private DevExpress.XtraReports.UI.XRTableCell lbltenDiaDiemLuuKho;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTauDuKienChatHang;
        private DevExpress.XtraReports.UI.XRTableCell lblTenTauDuKienChatHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanHaiQuan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaSoLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTTrongLuongHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiXuatKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiXuatKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell lblSoKyHieu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoContainer;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell lblSoCuaContainerBiKiemHoa1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoCuaContainerBiKiemHoa2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoCuaContainerBiKiemHoa3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoCuaContainerBiKiemHoa4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoCuaContainerBiKiemHoa5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell lblCoBaoCoHon5ContainerBiKiemHoa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemGiaoHangDich;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell lblSoQuanLyNoiBoDoanhNghiep;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiKiemTra;
        private DevExpress.XtraReports.UI.XRTableCell lblTenPhanLoaiKiemTra;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiKiemTraBanDau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiKiemHoa;
        private DevExpress.XtraReports.UI.XRTableCell lblNoiDungPhanLoaiKiemHoa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiPhuTrach;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoiChuyenLuongKiemHoa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemThucHienKiemHoa;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDiaDiemThucHienKiemHoa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell lblNoiDungChiDinhKiemHoa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhuongPhapChiDinhKiemHoa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell lblCoBaoKiemHoaToanBo;
        private DevExpress.XtraReports.UI.XRTableCell lblTenPhuongPhapChiDinhKiemHoa;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiHangHoa;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
    }
}
