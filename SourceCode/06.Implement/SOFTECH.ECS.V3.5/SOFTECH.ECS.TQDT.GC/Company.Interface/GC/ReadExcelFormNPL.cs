﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
#if GC_V4
using Company.GC.BLL.GC;
#endif
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.IO;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.DuLieuChuan;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
#endif
namespace Company.Interface.GC
{
    public partial class ReadExcelFormNPL : BaseForm
    {
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem ;
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New goodItemNew = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New();
        public string LoaiHangHoa = "";

#if GC_V4
        public T_GC_NPL_QUYETOAN NPLQT;
        public List<T_GC_NPL_QUYETOAN> ListNPLQuyetToan = new List<T_GC_NPL_QUYETOAN>();
        public KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail HopDong;
        public KDT_VNACCS_BaoCaoQuyetToan_TT39 BCQT = new KDT_VNACCS_BaoCaoQuyetToan_TT39();
#endif
        public string MaNPL;
        public bool ImportSuccess = false;
        public ReadExcelFormNPL()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.ShowDialog(this);
                txtFilePath.Text = sf.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    MLMessages("Lỗi khi chọn file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc sheet. Doanh nghiệp hãy kiểm tra lại tên sheet trước khi chọn file.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnReadFile_Click(object sender, EventArgs e)
        {
            #region New
            int beginRow = Convert.ToInt32(txtRowIndex.Value) - 1;
            if (beginRow < 0)
            {
                error.SetError(txtRowIndex, "DÒNG BẮT ĐẦU PHẢI LỚN HƠN 0");
                error.SetIconPadding(txtRowIndex, 8);
                return;

            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"", "MSG_EXC01", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char STTColumn = Convert.ToChar(txtSTT.Text.Trim());
            int STTCol = ConvertCharToInt(STTColumn);

            char MaHHColumn = Convert.ToChar(txtMaHangHoa.Text.Trim());
            int MaHHCol = ConvertCharToInt(MaHHColumn);

            char TenHHColumn = Convert.ToChar(txtTenHangHoa.Text.Trim());
            int TenHHCol = ConvertCharToInt(TenHHColumn);

            char DVTColumn = Convert.ToChar(txtDVT.Text.Trim());
            int DVTCol = ConvertCharToInt(DVTColumn);

            char TonDKColumn = Convert.ToChar(txtTonDauKy.Text.Trim());
            int TonDKCol = ConvertCharToInt(TonDKColumn);

            char NhapTKColumn = Convert.ToChar(txtNhapTrongKy.Text.Trim());
            int NhapTKCol = ConvertCharToInt(NhapTKColumn);

            char TaiXuatColumn = Convert.ToChar(txtTaiXuat.Text.Trim());
            int TaiXuatCol = ConvertCharToInt(TaiXuatColumn);

            char ChuyenMDSDColumn = Convert.ToChar(txtChuyenMDSD.Text.Trim());
            int ChuyenMDSDCol = ConvertCharToInt(ChuyenMDSDColumn);

            char XuatKhacColumn = Convert.ToChar(txtXuatKhac.Text.Trim());
            int XuatKhacCol = ConvertCharToInt(XuatKhacColumn);

            char XuatTKColumn = Convert.ToChar(txtXuatTrongKy.Text.Trim());
            int XuatTKCol = ConvertCharToInt(XuatTKColumn);

            char TonCKColumn = Convert.ToChar(txtTonCuoiKy.Text.Trim());
            int TonCKCol = ConvertCharToInt(TonCKColumn);

            char GhiChuColumn = Convert.ToChar(txtGhiChu.Text.Trim());
            int GhiChuCol = ConvertCharToInt(GhiChuColumn);

            char SoHopDongColumn = Convert.ToChar(txtSoHopDong.Text.Trim());
            int SoHopDongCol = ConvertCharToInt(SoHopDongColumn);

            string errorTotal = "";
            string errorSTT = "";
            string errorMaHangHoa = "";
            string errorMaHangHoaExits = "";
            string errorTenHangHoa = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorDVTCheck = "";
            string errorTonDauKy = "";
            string errorNhapTrongKy = "";
            string errorTaiXuat = "";
            string errorChuyenMDSD = "";
            string errorXuatKhac = "";
            string errorXuatTrongKy = "";
            string errorTonCuoiKy = "";

            string errorSoHopDong = "";
            string errorSoHopDongExits = "";

            string errorTonDauKyValid = "";
            string errorNhapTrongKyValid = "";
            string errorTaiXuatValid = "";
            string errorChuyenMDSDValid = "";
            string errorXuatKhacValid = "";
            string errorXuatTrongKyValid = "";
            string errorTonCuoiKyValid = "";
            string errorToTalTonCuoiKy = "";

#if GC_V4
            List<GC_NguyenPhuLieu> NPLCollection = new List<GC_NguyenPhuLieu>();
            List<GC_SanPham> SPCollection = new List<GC_SanPham>();
            if (!chkMutilHD.Checked)
            {
                Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
                HD.LoadHD(HopDong.ID_HopDong);
                NPLCollection = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(HD.ID);
                SPCollection = GC_SanPham.SelectCollectionBy_HopDong_ID(HD.ID);

                foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in HopDong.HangHoaCollection)
                {
                    if (item.ID > 0)
                        item.Delete();
                }
                HopDong.HangHoaCollection.Clear();
            }
            else
            {
                foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in BCQT.HopDongCollection)
                {
                    item.HangHoaCollection = KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail.SelectCollectionBy_Contract_ID(item.ID);
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail ite in item.HangHoaCollection)
                    {
                        if (ite.ID > 0)
                            ite.Delete();
                    }
                    item.HangHoaCollection.Clear();
                }
            }

#endif
#if SXXK_V4
            List<SXXK_NguyenPhuLieu> NPLCollection = SXXK_NguyenPhuLieu.SelectCollectionAll();
            List<SXXK_SanPham> SPCollection = SXXK_SanPham.SelectCollectionAll();
            foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in goodItemNew.GoodItemsCollection)
            {
                if (item.ID > 0)
                    item.Delete();
            }
            goodItemNew.GoodItemsCollection.Clear();
#endif
            List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> HangHoaCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail>();
            List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> HHCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New>();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
#if SXXK_V4
                        bool isAdd = true;
                        bool isExits = false;
                        KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New GoodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
                        try
                        {
                            GoodItem.STT = Convert.ToInt32(wsr.Cells[STTCol].Value);
                            if (GoodItem.STT.ToString().Length == 0)
                            {
                                errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.STT + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.STT + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            GoodItem.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                            if (GoodItem.MaHangHoa.Trim().Length == 0)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                //// KIỂM TRA MÃ NPL HOẶC SP ĐÃ ĐĂNG KÝ
                                if (LoaiHangHoa == "NPL")
                                {
                                    foreach (SXXK_NguyenPhuLieu item in NPLCollection)
                                    {
                                        if (item.Ma == GoodItem.MaHangHoa)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (SXXK_SanPham item in SPCollection)
                                    {
                                        if (item.Ma == GoodItem.MaHangHoa)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            GoodItem.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                            if (GoodItem.TenHangHoa.Trim().Length == 0)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TenHangHoa + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TenHangHoa + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            GoodItem.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                            isExits = false;
                            if (GoodItem.DVT.Trim().Length == 0)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                //KIỂM TRA ĐVT ĐÚNG CHUẨN VNACCS
                                List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                {
                                    if (item.Code == GoodItem.DVT)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                                if (isExits)
                                {
                                    //isExits = false;
                                    //KIỂM TRA ĐVT VỚI ĐVT ĐÃ ĐĂNG KÝ CỦA NPL HOẶC SẢN PHẨM
                                    if (LoaiHangHoa == "NPL")
                                    {
                                        foreach (SXXK_NguyenPhuLieu item in NPLCollection)
                                        {
                                            if (item.Ma == GoodItem.MaHangHoa)
                                            {
                                                if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == GoodItem.DVT)
                                                {
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (SXXK_SanPham item in SPCollection)
                                        {
                                            if (item.Ma == GoodItem.MaHangHoa)
                                            {
                                                if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == GoodItem.DVT)
                                                {
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorDVTCheck += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]-[" + GoodItem.DVT + "]\n";
                                        isAdd = false;
                                    }
                                }
                                else
                                {
                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            GoodItem.TonDauKy = Convert.ToDecimal(wsr.Cells[TonDKCol].Value);
                            if (GoodItem.TonDauKy.ToString().Trim().Length == 0)
                            {
                                errorTonDauKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonDauKy + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(GoodItem.TonDauKy, 4) != GoodItem.TonDauKy)
                                {
                                    errorTonDauKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonDauKy + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorTonDauKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonDauKy + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            GoodItem.NhapTrongKy = Convert.ToDecimal(wsr.Cells[NhapTKCol].Value);
                            if (GoodItem.NhapTrongKy.ToString().Trim().Length == 0)
                            {
                                errorNhapTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.NhapTrongKy + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(GoodItem.NhapTrongKy, 4) != GoodItem.NhapTrongKy)
                                {
                                    errorNhapTrongKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.NhapTrongKy + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorNhapTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.NhapTrongKy + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            GoodItem.TaiXuat = Convert.ToDecimal(wsr.Cells[TaiXuatCol].Value);
                            if (GoodItem.TaiXuat.ToString().Trim().Length == 0)
                            {
                                errorTaiXuat += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TaiXuat + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(GoodItem.TaiXuat, 4) != GoodItem.TaiXuat)
                                {
                                    errorTaiXuatValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TaiXuat + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorTaiXuat += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TaiXuat + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            GoodItem.ChuyenMDSD = Convert.ToDecimal(wsr.Cells[ChuyenMDSDCol].Value);
                            if (GoodItem.ChuyenMDSD.ToString().Trim().Length == 0)
                            {
                                errorChuyenMDSD += "[" + (wsr.Index + 1) + "]-[" + GoodItem.ChuyenMDSD + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(GoodItem.ChuyenMDSD, 4) != GoodItem.ChuyenMDSD)
                                {
                                    errorChuyenMDSDValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.ChuyenMDSD + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorChuyenMDSD += "[" + (wsr.Index + 1) + "]-[" + GoodItem.ChuyenMDSD + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            GoodItem.XuatKhac = Convert.ToDecimal(wsr.Cells[XuatKhacCol].Value);
                            if (GoodItem.XuatKhac.ToString().Trim().Length == 0)
                            {
                                errorXuatKhac += "[" + (wsr.Index + 1) + "]-[" + GoodItem.XuatKhac + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(GoodItem.XuatKhac, 4) != GoodItem.XuatKhac)
                                {
                                    errorXuatKhacValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.XuatKhac + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorXuatKhac += "[" + (wsr.Index + 1) + "]-[" + GoodItem.XuatKhac + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            GoodItem.XuatTrongKy = Convert.ToDecimal(wsr.Cells[XuatTKCol].Value);
                            if (GoodItem.XuatTrongKy.ToString().Trim().Length == 0)
                            {
                                errorXuatTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.XuatTrongKy + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(GoodItem.XuatTrongKy, 4) != GoodItem.XuatTrongKy)
                                {
                                    errorXuatTrongKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.XuatTrongKy + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorXuatTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.XuatTrongKy + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            GoodItem.TonCuoiKy = Convert.ToDecimal(wsr.Cells[TonCKCol].Value);
                            if (GoodItem.TonCuoiKy.ToString().Trim().Length == 0)
                            {
                                errorTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonCuoiKy + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(GoodItem.TonCuoiKy, 4) != GoodItem.TonCuoiKy)
                                {
                                    errorTonCuoiKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonCuoiKy + "]\n";
                                    isAdd = false;
                                }
                                if (GoodItem.TonDauKy + GoodItem.NhapTrongKy - GoodItem.TaiXuat - GoodItem.ChuyenMDSD - GoodItem.XuatKhac - GoodItem.XuatTrongKy != GoodItem.TonCuoiKy)
                                {
                                    errorToTalTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonDauKy + "]-[" + GoodItem.NhapTrongKy + "]-[" + GoodItem.TaiXuat + "]-[" + GoodItem.ChuyenMDSD + "]-[" + GoodItem.NhapTrongKy + "]-[" + GoodItem.XuatTrongKy + "]-[" + GoodItem.TonCuoiKy + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TonCuoiKy + "]\n";
                            isAdd = false;
                        }
                        GoodItem.GhiChu = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();
                        if (isAdd)
                            HHCollection.Add(GoodItem);
#elif GC_V4
                        bool isAdd = true;
                        bool isExits = false;
                        if (chkMutilHD.Checked)
                        {
                            string SoHopDong = "";
                            foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail items in BCQT.HopDongCollection)
                            {
                                try
                                {
                                    SoHopDong = (wsr.Cells[SoHopDongCol].Value).ToString().Trim();
                                }
                                catch (Exception)
                                {
                                    errorSoHopDong += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                    isAdd = false;
                                }
                                if (SoHopDong.ToString().Length == 0)
                                {
                                    errorSoHopDong += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (items.SoHopDong == SoHopDong)
                                    {
                                        isExits = true;
                                        NPLCollection = new List<GC_NguyenPhuLieu>();
                                        NPLCollection = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(items.ID_HopDong);
                                        SPCollection = new List<GC_SanPham>();
                                        SPCollection = GC_SanPham.SelectCollectionBy_HopDong_ID(items.ID_HopDong);

                                        KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail GoodItem = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
                                        GoodItem.Contract_ID = items.ID;
                                        try
                                        {
                                            GoodItem.STT = Convert.ToInt32(wsr.Cells[STTCol].Value);
                                            if (GoodItem.STT.ToString().Length == 0)
                                            {
                                                errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.STT + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.STT + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItem.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                            if (GoodItem.MaHangHoa.Trim().Length == 0)
                                            {
                                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                // KIỂM TRA MÃ NPL HOẶC SP ĐÃ ĐĂNG KÝ
                                                if (LoaiHangHoa == "NPL")
                                                {
                                                    foreach (GC_NguyenPhuLieu item in NPLCollection)
                                                    {
                                                        if (item.Ma == GoodItem.MaHangHoa)
                                                        {
                                                            isExits = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    foreach (GC_SanPham item in SPCollection)
                                                    {
                                                        if (item.Ma == GoodItem.MaHangHoa)
                                                        {
                                                            isExits = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItem.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                            if (GoodItem.TenHangHoa.Trim().Length == 0)
                                            {
                                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TenHangHoa + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TenHangHoa + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            isExits = false;
                                            GoodItem.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim().ToUpper();
                                            if (GoodItem.DVT.Trim().Length == 0)
                                            {
                                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                //KIỂM TRA ĐVT ĐÚNG CHUẨN VNACCS
                                                List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == GoodItem.DVT)
                                                    {
                                                        isExits = true;
                                                    }
                                                }
                                                if (isExits)
                                                {
                                                    //isExits = false;
                                                    ////KIỂM TRA ĐVT VỚI ĐVT ĐÃ ĐĂNG KÝ CỦA NPL HOẶC SẢN PHẨM
                                                    if (LoaiHangHoa == "NPL")
                                                    {
                                                        foreach (GC_NguyenPhuLieu item in NPLCollection)
                                                        {
                                                            if (item.Ma == GoodItem.MaHangHoa)
                                                            {
                                                                if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == GoodItem.DVT)
                                                                {
                                                                    isExits = true;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        foreach (GC_SanPham item in SPCollection)
                                                        {
                                                            if (item.Ma == GoodItem.MaHangHoa)
                                                            {
                                                                if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == GoodItem.DVT)
                                                                {
                                                                    isExits = true;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (!isExits)
                                                    {
                                                        errorDVTCheck += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]-[" + GoodItem.DVT + "]\n";
                                                        isAdd = false;
                                                    }
                                                }
                                                else
                                                {
                                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItem.LuongTonDauKy = Convert.ToDecimal(wsr.Cells[TonDKCol].Value);
                                            if (GoodItem.LuongTonDauKy.ToString().Trim().Length == 0)
                                            {
                                                errorTonDauKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonDauKy + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItem.LuongTonDauKy, 4) != GoodItem.LuongTonDauKy)
                                                {
                                                    errorTonDauKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonDauKy + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorTonDauKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonDauKy + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItem.LuongNhapTrongKy = Convert.ToDecimal(wsr.Cells[NhapTKCol].Value);
                                            if (GoodItem.LuongNhapTrongKy.ToString().Trim().Length == 0)
                                            {
                                                errorNhapTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongNhapTrongKy + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItem.LuongNhapTrongKy, 4) != GoodItem.LuongNhapTrongKy)
                                                {
                                                    errorNhapTrongKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongNhapTrongKy + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorNhapTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongNhapTrongKy + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItem.LuongTaiXuat = Convert.ToDecimal(wsr.Cells[TaiXuatCol].Value);
                                            if (GoodItem.LuongTaiXuat.ToString().Trim().Length == 0)
                                            {
                                                errorTaiXuat += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTaiXuat + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItem.LuongTaiXuat, 4) != GoodItem.LuongTaiXuat)
                                                {
                                                    errorTaiXuatValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTaiXuat + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorTaiXuat += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTaiXuat + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItem.LuongChuyenMucDichSD = Convert.ToDecimal(wsr.Cells[ChuyenMDSDCol].Value);
                                            if (GoodItem.LuongChuyenMucDichSD.ToString().Trim().Length == 0)
                                            {
                                                errorChuyenMDSD += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongChuyenMucDichSD + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItem.LuongChuyenMucDichSD, 4) != GoodItem.LuongChuyenMucDichSD)
                                                {
                                                    errorChuyenMDSDValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongChuyenMucDichSD + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorChuyenMDSD += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongChuyenMucDichSD + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItem.LuongXuatKhac = Convert.ToDecimal(wsr.Cells[XuatKhacCol].Value);
                                            if (GoodItem.LuongXuatKhac.ToString().Trim().Length == 0)
                                            {
                                                errorXuatKhac += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhac + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItem.LuongXuatKhac, 4) != GoodItem.LuongXuatKhac)
                                                {
                                                    errorXuatKhacValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhac + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorXuatKhac += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhac + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItem.LuongXuatKhau = Convert.ToDecimal(wsr.Cells[XuatTKCol].Value);
                                            if (GoodItem.LuongXuatKhau.ToString().Trim().Length == 0)
                                            {
                                                errorXuatTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhau + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItem.LuongXuatKhau, 4) != GoodItem.LuongXuatKhau)
                                                {
                                                    errorXuatTrongKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhau + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorXuatTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhau + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItem.LuongTonCuoiKy = Convert.ToDecimal(wsr.Cells[TonCKCol].Value);
                                            if (GoodItem.LuongTonCuoiKy.ToString().Trim().Length == 0)
                                            {
                                                errorTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonCuoiKy + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItem.LuongTonCuoiKy, 4) != GoodItem.LuongTonCuoiKy)
                                                {
                                                    errorTonCuoiKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonCuoiKy + "]\n";
                                                    isAdd = false;
                                                }
                                                if (GoodItem.LuongTonDauKy + GoodItem.LuongNhapTrongKy - GoodItem.LuongTaiXuat - GoodItem.LuongChuyenMucDichSD - GoodItem.LuongXuatKhac - GoodItem.LuongXuatKhau != GoodItem.LuongTonCuoiKy)
                                                {
                                                    errorToTalTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonDauKy + "]-[" + GoodItem.LuongNhapTrongKy + "]-[" + GoodItem.LuongTaiXuat + "]-[" + GoodItem.LuongChuyenMucDichSD + "]-[" + GoodItem.LuongXuatKhac + "]-[" + GoodItem.LuongXuatKhau + "]-[" + GoodItem.LuongTonCuoiKy + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonCuoiKy + "]\n";
                                            isAdd = false;
                                        }
                                        GoodItem.GhiChu = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();
                                        if (isAdd)
                                            items.HangHoaCollection.Add(GoodItem);
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorSoHopDongExits += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                isAdd = false;
                            }
                        }
                        else
                        {
                            KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail GoodItem = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
                            GoodItem.Contract_ID = HopDong.ID;
                            try
                            {
                                GoodItem.STT = Convert.ToInt32(wsr.Cells[STTCol].Value);
                                if (GoodItem.STT.ToString().Length == 0)
                                {
                                    errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.STT + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.STT + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = true;
                                GoodItem.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (GoodItem.MaHangHoa.Trim().Length == 0)
                                {
                                    errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    #region
                                    ////KIỂM TRA MÃ NPL HOẶC SP ĐÃ ĐĂNG KÝ
                                    if (LoaiHangHoa == "NPL")
                                    {
                                        foreach (GC_NguyenPhuLieu item in NPLCollection)
                                        {
                                            if (item.Ma == GoodItem.MaHangHoa)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (GC_SanPham item in SPCollection)
                                        {
                                            if (item.Ma == GoodItem.MaHangHoa)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                                if (!isExits)
                                {
                                    errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItem.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (GoodItem.TenHangHoa.Trim().Length == 0)
                                {
                                    errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TenHangHoa + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItem.TenHangHoa + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItem.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim().ToUpper();
                                isExits = false;
                                if (GoodItem.DVT.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA ĐVT ĐÚNG CHUẨN VNACCS
                                    List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
                                    foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                    {
                                        if (item.Code == GoodItem.DVT)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (isExits)
                                    {
                                        //isExits = false;
                                        //KIỂM TRA ĐVT VỚI ĐVT ĐÃ ĐĂNG KÝ CỦA NPL HOẶC SẢN PHẨM
                                        if (LoaiHangHoa == "NPL")
                                        {
                                            foreach (GC_NguyenPhuLieu item in NPLCollection)
                                            {
                                                if (item.Ma == GoodItem.MaHangHoa)
                                                {
                                                    if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == GoodItem.DVT)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (GC_SanPham item in SPCollection)
                                            {
                                                if (item.Ma == GoodItem.MaHangHoa)
                                                {
                                                    if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == GoodItem.DVT)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorDVTCheck += "[" + (wsr.Index + 1) + "]-[" + GoodItem.MaHangHoa + "]-[" + GoodItem.DVT + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    else
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItem.DVT + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = false;
                                GoodItem.LuongTonDauKy = Convert.ToDecimal(wsr.Cells[TonDKCol].Value);
                                if (GoodItem.LuongTonDauKy.ToString().Trim().Length == 0)
                                {
                                    errorTonDauKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonDauKy + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItem.LuongTonDauKy, 4) != GoodItem.LuongTonDauKy)
                                    {
                                        errorTonDauKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonDauKy + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorTonDauKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonDauKy + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItem.LuongNhapTrongKy = Convert.ToDecimal(wsr.Cells[NhapTKCol].Value);
                                if (GoodItem.LuongNhapTrongKy.ToString().Trim().Length == 0)
                                {
                                    errorNhapTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongNhapTrongKy + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItem.LuongNhapTrongKy, 4) != GoodItem.LuongNhapTrongKy)
                                    {
                                        errorNhapTrongKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongNhapTrongKy + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorNhapTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongNhapTrongKy + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItem.LuongTaiXuat = Convert.ToDecimal(wsr.Cells[TaiXuatCol].Value);
                                if (GoodItem.LuongTaiXuat.ToString().Trim().Length == 0)
                                {
                                    errorTaiXuat += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTaiXuat + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItem.LuongTaiXuat, 4) != GoodItem.LuongTaiXuat)
                                    {
                                        errorTaiXuatValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTaiXuat + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorTaiXuat += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTaiXuat + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItem.LuongChuyenMucDichSD = Convert.ToDecimal(wsr.Cells[ChuyenMDSDCol].Value);
                                if (GoodItem.LuongChuyenMucDichSD.ToString().Trim().Length == 0)
                                {
                                    errorChuyenMDSD += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongChuyenMucDichSD + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItem.LuongChuyenMucDichSD, 4) != GoodItem.LuongChuyenMucDichSD)
                                    {
                                        errorChuyenMDSDValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongChuyenMucDichSD + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorChuyenMDSD += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongChuyenMucDichSD + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItem.LuongXuatKhac = Convert.ToDecimal(wsr.Cells[XuatKhacCol].Value);
                                if (GoodItem.LuongXuatKhac.ToString().Trim().Length == 0)
                                {
                                    errorXuatKhac += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhac + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItem.LuongXuatKhac, 4) != GoodItem.LuongXuatKhac)
                                    {
                                        errorXuatKhacValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhac + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorXuatKhac += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhac + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItem.LuongXuatKhau = Convert.ToDecimal(wsr.Cells[XuatTKCol].Value);
                                if (GoodItem.LuongXuatKhau.ToString().Trim().Length == 0)
                                {
                                    errorXuatTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhau + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItem.LuongXuatKhau, 4) != GoodItem.LuongXuatKhau)
                                    {
                                        errorXuatTrongKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhau + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorXuatTrongKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongXuatKhau + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItem.LuongTonCuoiKy = Convert.ToDecimal(wsr.Cells[TonCKCol].Value);
                                if (GoodItem.LuongTonCuoiKy.ToString().Trim().Length == 0)
                                {
                                    errorTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonCuoiKy + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItem.LuongTonCuoiKy, 4) != GoodItem.LuongTonCuoiKy)
                                    {
                                        errorTonCuoiKyValid += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonCuoiKy + "]\n";
                                        isAdd = false;
                                    }
                                    if (GoodItem.LuongTonDauKy + GoodItem.LuongNhapTrongKy - GoodItem.LuongTaiXuat - GoodItem.LuongChuyenMucDichSD - GoodItem.LuongXuatKhac - GoodItem.LuongXuatKhau != GoodItem.LuongTonCuoiKy)
                                    {
                                        errorToTalTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonDauKy + "]-[" + GoodItem.LuongNhapTrongKy + "]-[" + GoodItem.LuongTaiXuat + "]-[" + GoodItem.LuongChuyenMucDichSD + "]-[" + GoodItem.LuongXuatKhac + "]-[" + GoodItem.LuongXuatKhau + "]-[" + GoodItem.LuongTonCuoiKy + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItem.LuongTonCuoiKy + "]\n";
                                isAdd = false;
                            }
                            GoodItem.GhiChu = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();
                            if (isAdd)
                                HangHoaCollection.Add(GoodItem);
                        }                        
#endif
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorSTT))
                errorTotal += "\n - [STT] -[SỐ THỨ TỰ] : \n" + errorSTT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHangHoaExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaExits + " CHƯA ĐƯỢC ĐĂNG KÝ \n";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ. ĐVT PHẢI LÀ ĐVT VNACCS (VÍ DỤ : CÁI/CHIẾC LÀ : PCE)\n";
            if (!String.IsNullOrEmpty(errorDVTCheck))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] - [ĐVT] : \n" + errorDVTCheck + " KHÁC VỚI ĐVT ĐÃ ĐĂNG KÝ CỦA NPL HOẶC SẢN PHẨM  \n";
            if (!String.IsNullOrEmpty(errorTonDauKy))
                errorTotal += "\n - [STT] -[TỒN ĐẦU KỲ] : \n" + errorTonDauKy + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorTonDauKyValid))
                errorTotal += "\n - [STT] -[TỒN ĐẦU KỲ] : \n" + errorTonDauKyValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorNhapTrongKy))
                errorTotal += "\n - [STT] -[NHẬP TRONG KỲ] : " + errorNhapTrongKy + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorNhapTrongKyValid))
                errorTotal += "\n - [STT] -[NHẬP TRONG KỲ] : \n" + errorNhapTrongKyValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorTaiXuat))
                errorTotal += "\n - [STT] -[TÁI XUẤT] : \n" + errorTaiXuat + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorTaiXuatValid))
                errorTotal += "\n - [STT] -[XUẤT TRONG KỲ] : \n" + errorTaiXuatValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorChuyenMDSD))
                errorTotal += "\n - [STT] -[CHUYỂN MỤC ĐÍCH SD] : \n" + errorChuyenMDSD + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorChuyenMDSDValid))
                errorTotal += "\n - [STT] -[CHUYỂN MỤC ĐÍCH SD] : \n" + errorChuyenMDSDValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorXuatKhac))
                errorTotal += "\n - [STT] -[XUẤT KHÁC] : \n" + errorXuatKhac + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorXuatKhacValid))
                errorTotal += "\n - [STT] -[XUẤT KHÁC] : \n" + errorXuatKhacValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorTonCuoiKy))
                errorTotal += "\n - [STT] -[TỒN CUỐI KỲ] : \n" + errorTonCuoiKy + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorTonCuoiKyValid))
                errorTotal += "\n - [STT] -[TỒN CUỐI KỲ] : \n" + errorTonCuoiKyValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorToTalTonCuoiKy))
                errorTotal += "\n - [STT] -[ [TỒN ĐẦU KỲ] + [NHẬP TRONG KỲ] - [TÁI XUẤT] - [CHUYỂN MĐSD] - [XUẤT KHÁC] - [XUẤT TRONG KỲ] KHÔNG BẰNG [TỒN CUỐI KỲ] ] : \n" + errorToTalTonCuoiKy;

#if GC_V4
            if (!String.IsNullOrEmpty(errorSoHopDong))
                errorTotal += "\n - [STT] -[SỐ HỢP ĐỒNG] : \n" + errorSoHopDong + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorSoHopDongExits))
                errorTotal += "\n - [STT] -[SỐ HỢP ĐỒNG] : \n" + errorSoHopDongExits + " CHƯA ĐƯỢC ĐĂNG KÝ \n";
#endif

            if (String.IsNullOrEmpty(errorTotal))
            {                
#if GC_V4
                if (!chkMutilHD.Checked)
                {
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in HangHoaCollection)
                    {
                        HopDong.HangHoaCollection.Add(item);
                    }   
                }
#elif SXXK_V4
                foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New goodItem in HHCollection)
                {
                    goodItemNew.GoodItemsCollection.Add(goodItem);
                }
#endif
                ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
                ImportSuccess = true;
            }
            else
            {
                if (chkMutilHD.Checked)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "LỖI NHẬP TỪ EXCEL ." + errorTotal, false);
                    return;
                }
                else
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                    return;
                }
            }
            #endregion
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkFileExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GoodItem_New();
            }

            catch (Exception ex) 
            { 
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
