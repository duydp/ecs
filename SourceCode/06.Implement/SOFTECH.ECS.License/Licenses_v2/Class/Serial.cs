﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Activation.Server
{
    public partial class Serial
    {
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll_Extend()
        {
            const string spName = "[dbo].[p_LICENSE_Serial_SelectAll_Extend]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteDataSet(dbCommand);
        }
    }
}
