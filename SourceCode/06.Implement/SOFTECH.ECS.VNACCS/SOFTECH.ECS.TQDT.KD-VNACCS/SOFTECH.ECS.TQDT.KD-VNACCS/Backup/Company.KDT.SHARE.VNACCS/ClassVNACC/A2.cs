﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.LogMessages;

namespace Company.KDT.SHARE.VNACCS.ClassVNACC
{
    public class A2 : BasicVNACC
    {
        
        public PropertiesAttribute MaDieuKhienGD { set; get; } //3
        public PropertiesAttribute MaNghiepVu { get; set; } // 5 

        public PropertiesAttribute OutputMSGCode { set; get; } // 7
        public PropertiesAttribute VungDuTru_1 { set; get; } // 14
        public PropertiesAttribute NguoiSuDung_Ma { get; set; } // 5
        public PropertiesAttribute NguoiSuDung_ID { get; set; } // 3
        public PropertiesAttribute NguoiSuDung_Pass { get; set; } // 8
        public PropertiesAttribute TerminalID { get; set; } // 6
        public PropertiesAttribute NguoiSuDung_DiaChi { set; get; } // 64
        
        public PropertiesAttribute VungDuTru_2 { set; get; } // 64
        public PropertiesAttribute RTPTag { get; set; } // 30
        public PropertiesAttribute VungDuTru_3 { set; get; } //10

        public PropertiesAttribute MessagesTag { get; set; } // 26

        public PropertiesAttribute MSGControlCode { set; get; } // 8

        public PropertiesAttribute InputMessagesID { set; get; } // 10
        public PropertiesAttribute IndexTag { get; set; } // 100
        
        public PropertiesAttribute VungDuTru_4 { set; get; } // 1
        public PropertiesAttribute SystemID { get; set; } // 1

        public PropertiesAttribute ScreenCode { set; get; } // 3
        public PropertiesAttribute VungDuTru_5 { set; get; } // 24
        public PropertiesAttribute MessagesLength { set; get; } // 6


        public A2 ()
        {
            MaNghiepVu = new PropertiesAttribute(5);
            MaNghiepVu.SetValue("?A2");
            OutputMSGCode = new PropertiesAttribute(7);

            RTPTag = new PropertiesAttribute(30);
            RTPTag.SetValue(MsgPhanBo.GetRTPTagPhanBoA2(GlobalVNACC.TerminalID));


            NguoiSuDung_Ma = new PropertiesAttribute(5);
            NguoiSuDung_Ma.SetValue(GlobalVNACC.NguoiSuDung_Ma);

            NguoiSuDung_ID = new PropertiesAttribute(3);
            NguoiSuDung_ID.SetValue(GlobalVNACC.NguoiSuDung_ID);

            NguoiSuDung_Pass = new PropertiesAttribute(8);
            NguoiSuDung_Pass.SetValue(GlobalVNACC.NguoiSuDung_Pass);

            TerminalID = new PropertiesAttribute(6);
            TerminalID.SetValue(GlobalVNACC.TerminalID);

            NguoiSuDung_DiaChi = new PropertiesAttribute(64);
            NguoiSuDung_DiaChi.SetValue(GlobalVNACC.NguoiSuDung_DiaChi);

            this.MessagesTag = new PropertiesAttribute(26);
            this.MSGControlCode = new PropertiesAttribute(8);


            this.VungDuTru_1 = new PropertiesAttribute(14);
            this.VungDuTru_2 = new PropertiesAttribute(64);
            this.VungDuTru_3 = new PropertiesAttribute(10);
            this.VungDuTru_4 = new PropertiesAttribute(1);
            this.VungDuTru_5 = new PropertiesAttribute(24);

            MaDieuKhienGD = new PropertiesAttribute(3);
            IndexTag = new PropertiesAttribute(100);

            SystemID = new PropertiesAttribute(1);
            SystemID.SetValue("1");

            this.MessagesLength = new PropertiesAttribute(6, typeof(int), 0);
            this.MessagesLength.SetValue(400);

            InputMessagesID = new PropertiesAttribute(10);
            this.ScreenCode = new PropertiesAttribute(3);
        }

        public string LoadRTPTTag()
        {
            try
            {
                IList<MsgPhanBo> listmsg = MsgPhanBo.SelectCollectionDynamic("TerminalID = '" + GlobalVNACC.TerminalID + "' AND GhiChu is null", "Master_ID desc");
                if (listmsg != null && listmsg.Count > 0)
                {
                    return listmsg[0].RTPTag;
                }
                else
                    return string.Empty;
                
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return string.Empty;    
            }
        }
        public StringBuilder A2BuidEdiMessages(StringBuilder strB)
        {
            return this.BuildEdiMessages<A2>(strB, false, GlobalVNACC.PathConfig, "A2");
        }

    }
}
