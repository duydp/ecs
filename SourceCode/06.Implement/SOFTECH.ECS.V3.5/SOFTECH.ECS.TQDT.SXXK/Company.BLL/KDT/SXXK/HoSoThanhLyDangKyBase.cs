using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
    public partial class HoSoThanhLyDangKy
    {
        #region Private members.

        protected long _ID;
        protected long _SoTiepNhan;
        protected string _MaHaiQuanTiepNhan = String.Empty;
        protected short _NamTiepNhan;
        protected DateTime _NgayTiepNhan = new DateTime(1900, 01, 01);
        protected string _MaDoanhNghiep = String.Empty;
        protected int _TrangThaiXuLy;
        protected int _TrangThaiThanhKhoan;
        protected int _SoHoSo;
        protected DateTime _NgayBatDau = new DateTime(1900, 01, 01);
        protected DateTime _NgayKetThuc = new DateTime(1900, 01, 01);
        protected int _LanThanhLy;
        protected string _UserName = String.Empty;
        protected string _SoQuyetDinh = String.Empty;
        protected DateTime _NgayQuyetDinh = new DateTime(1900, 01, 01);
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public long SoTiepNhan
        {
            set { this._SoTiepNhan = value; }
            get { return this._SoTiepNhan; }
        }
        public string MaHaiQuanTiepNhan
        {
            set { this._MaHaiQuanTiepNhan = value; }
            get { return this._MaHaiQuanTiepNhan; }
        }
        public short NamTiepNhan
        {
            set { this._NamTiepNhan = value; }
            get { return this._NamTiepNhan; }
        }
        public DateTime NgayTiepNhan
        {
            set { this._NgayTiepNhan = value; }
            get { return this._NgayTiepNhan; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }
        public int TrangThaiXuLy
        {
            set { this._TrangThaiXuLy = value; }
            get { return this._TrangThaiXuLy; }
        }
        public int TrangThaiThanhKhoan
        {
            set { this._TrangThaiThanhKhoan = value; }
            get { return this._TrangThaiThanhKhoan; }
        }
        public int SoHoSo
        {
            set { this._SoHoSo = value; }
            get { return this._SoHoSo; }
        }
        public DateTime NgayBatDau
        {
            set { this._NgayBatDau = value; }
            get { return this._NgayBatDau; }
        }
        public DateTime NgayKetThuc
        {
            set { this._NgayKetThuc = value; }
            get { return this._NgayKetThuc; }
        }
        public int LanThanhLy
        {
            set { this._LanThanhLy = value; }
            get { return this._LanThanhLy; }
        }
        public string UserName
        {
            set { this._UserName = value; }
            get { return this._UserName; }
        }
        public string SoQuyetDinh
        {
            set { this._SoQuyetDinh = value; }
            get { return this._SoQuyetDinh; }
        }
        public DateTime NgayQuyetDinh
        {
            set { this._NgayQuyetDinh = value; }
            get { return this._NgayQuyetDinh; }
        }
        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.
        public bool LoadTransaction(SqlTransaction transaction)
        {
            string spName = "SELECT * FROM T_KDT_SXXK_HoSoThanhLyDangKy where ID=@id";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@id", SqlDbType.Int, this.ID);

            IDataReader reader = db.ExecuteReader(dbCommand,transaction);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) this._MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) this._NamTiepNhan = reader.GetInt16(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) this._TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoSo"))) this._SoHoSo = reader.GetInt32(reader.GetOrdinal("SoHoSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) this._NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThuc"))) this._NgayKetThuc = reader.GetDateTime(reader.GetOrdinal("NgayKetThuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) this._UserName = reader.GetString(reader.GetOrdinal("UserName"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) this._SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) this._NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
                reader.Close();
                return true;

            }
            reader.Close();
            return false;
        }
        public bool Load()
        {
            string spName = "SELECT * FROM T_KDT_SXXK_HoSoThanhLyDangKy where ID=@id";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@id", SqlDbType.Int, this.ID);

            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) this._MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) this._NamTiepNhan = reader.GetInt16(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) this._TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoSo"))) this._SoHoSo = reader.GetInt32(reader.GetOrdinal("SoHoSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) this._NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThuc"))) this._NgayKetThuc = reader.GetDateTime(reader.GetOrdinal("NgayKetThuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) this._UserName = reader.GetString(reader.GetOrdinal("UserName"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) this._SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) this._NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
                reader.Close();
                return true;

            }
            reader.Close();
            return false;
        }
        public bool LoadByLanThanhLy()
        {
            string spName = "SELECT * FROM T_KDT_SXXK_HoSoThanhLyDangKy where LanThanhLy=@LanThanhLy";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this.LanThanhLy);

            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) this._MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) this._NamTiepNhan = reader.GetInt16(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) this._TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoSo"))) this._SoHoSo = reader.GetInt32(reader.GetOrdinal("SoHoSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) this._NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThuc"))) this._NgayKetThuc = reader.GetDateTime(reader.GetOrdinal("NgayKetThuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) this._UserName = reader.GetString(reader.GetOrdinal("UserName"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) this._SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) this._NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
                reader.Close();
                return true;

            }
            reader.Close();
            return false;
        }
        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_HoSoThanhLyDangKy_SelectAll";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_HoSoThanhLyDangKy_SelectAll";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_HoSoThanhLyDangKy_SelectDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_HoSoThanhLyDangKy_SelectDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public HoSoThanhLyDangKyCollection SelectCollectionAll()
        {
            HoSoThanhLyDangKyCollection collection = new HoSoThanhLyDangKyCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                HoSoThanhLyDangKy entity = new HoSoThanhLyDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt16(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoSo"))) entity.SoHoSo = reader.GetInt32(reader.GetOrdinal("SoHoSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) entity.NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThuc"))) entity.NgayKetThuc = reader.GetDateTime(reader.GetOrdinal("NgayKetThuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public HoSoThanhLyDangKyCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            HoSoThanhLyDangKyCollection collection = new HoSoThanhLyDangKyCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                HoSoThanhLyDangKy entity = new HoSoThanhLyDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt16(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoSo"))) entity.SoHoSo = reader.GetInt32(reader.GetOrdinal("SoHoSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) entity.NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThuc"))) entity.NgayKetThuc = reader.GetDateTime(reader.GetOrdinal("NgayKetThuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) entity.SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) entity.NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_HoSoThanhLyDangKy_Insert";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, this._MaHaiQuanTiepNhan);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.SmallInt, this._NamTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, this._TrangThaiThanhKhoan);
            db.AddInParameter(dbCommand, "@SoHoSo", SqlDbType.Int, this._SoHoSo);
            db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, this._NgayBatDau);
            db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, this._NgayKetThuc);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, this._UserName);
            db.AddInParameter(dbCommand, "@SoQuyetDinh", SqlDbType.NVarChar, this._SoQuyetDinh);
            db.AddInParameter(dbCommand, "@NgayQuyetDinh", SqlDbType.DateTime, this._NgayQuyetDinh);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(HoSoThanhLyDangKyCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HoSoThanhLyDangKy item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, HoSoThanhLyDangKyCollection collection)
        {
            foreach (HoSoThanhLyDangKy item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, this._MaHaiQuanTiepNhan);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.SmallInt, this._NamTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, this._TrangThaiThanhKhoan);
            db.AddInParameter(dbCommand, "@SoHoSo", SqlDbType.Int, this._SoHoSo);
            db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, this._NgayBatDau);
            db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, this._NgayKetThuc);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, this._UserName);
            db.AddInParameter(dbCommand, "@SoQuyetDinh", SqlDbType.NVarChar, this._SoQuyetDinh);
            db.AddInParameter(dbCommand, "@NgayQuyetDinh", SqlDbType.DateTime, this._NgayQuyetDinh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(HoSoThanhLyDangKyCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HoSoThanhLyDangKy item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_HoSoThanhLyDangKy_Update";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, this._MaHaiQuanTiepNhan);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.SmallInt, this._NamTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, this._TrangThaiThanhKhoan);
            db.AddInParameter(dbCommand, "@SoHoSo", SqlDbType.Int, this._SoHoSo);
            db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, this._NgayBatDau);
            db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, this._NgayKetThuc);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, this._UserName);
            db.AddInParameter(dbCommand, "@SoQuyetDinh", SqlDbType.NVarChar, this._SoQuyetDinh);
            db.AddInParameter(dbCommand, "@NgayQuyetDinh", SqlDbType.DateTime, this._NgayQuyetDinh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(HoSoThanhLyDangKyCollection collection, SqlTransaction transaction)
        {
            foreach (HoSoThanhLyDangKy item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_HoSoThanhLyDangKy_Delete";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            new NPLNhapTon().DeleteDynamicTransaction(transaction, " LanThanhLy = " + this.LanThanhLy);
            new BCThueXNK().DeleteDynamicTransaction(transaction, this.LanThanhLy, this.MaDoanhNghiep);
            new BCXuatNhapTon().DeleteDynamicTransaction(transaction, this.LanThanhLy, this.MaDoanhNghiep);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(HoSoThanhLyDangKyCollection collection, SqlTransaction transaction)
        {
            foreach (HoSoThanhLyDangKy item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(HoSoThanhLyDangKyCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HoSoThanhLyDangKy item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion
    }
}