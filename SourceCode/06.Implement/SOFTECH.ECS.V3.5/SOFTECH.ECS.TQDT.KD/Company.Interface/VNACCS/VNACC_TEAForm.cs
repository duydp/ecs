﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.Interface.VNACCS;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.Components;
namespace Company.Interface
{
    public partial class VNACC_TEAForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_TEA TEA = new KDT_VNACCS_TEA();

        public VNACC_TEAForm()
        {
            
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.TEA.ToString());
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    showHang();
                    break;
                case "cmdNguoiXNK":
                    showNguoiXNK();
                    break;
                case "cmdDieuChinh":
                    showDieuChinh();
                    break;
                case "cmdLuu":
                    Save();
                    break;

                case "cmdKhaiBao":
                    sendvnaccs(false);
                    break;
                case "cmdKetQuaHQ":
                    ShowKetQuaTraVe();
                    break;
                case "cmdKetQuaXuLy":
                    ShowKetQuaXuLy();
                    break;
            }
        }
        private void ShowKetQuaXuLy()
        {
            try
            {
                VNACC_KetQuaXuLy f = new VNACC_KetQuaXuLy(TEA.ID, "TEA");
                //f.master_id = TKMD.ID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void showHang()
        {
            try
            {
                VNACC_TEA_HangForm f = new VNACC_TEA_HangForm();
                f.TEA = this.TEA;
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void showNguoiXNK()
        {
            try
            {
                VNACC_TEA_NguoiXNK f = new VNACC_TEA_NguoiXNK();
                f.TEA = this.TEA;
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void showDieuChinh()
        {
            try
            {
                VNACC_TEA_DieuChinhForm f = new VNACC_TEA_DieuChinhForm();
                f.TEA = this.TEA;
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        private void GetTEA()
        {
            try
            {
                TEA.SoDanhMucMienThue = Convert.ToDecimal(txtSoDanhMucMienThue.Text);
                TEA.PhanLoaiXuatNhapKhau = cbbPhanLoaiXuatNhapKhau.SelectedValue.ToString();
                TEA.CoQuanHaiQuan = ctrCoQuanHaiQuan.Code;
                TEA.DiaChiCuaNguoiKhai = txtDiaChiCuaNguoiKhai.Text;
                TEA.SDTCuaNguoiKhai = txtSDTCuaNguoiKhai.Text;
                TEA.ThoiHanMienThue = clcThoiHanMienThue.Value;
                TEA.TenDuAnDauTu = txtTenDuAnDauTu.Text;
                TEA.DiaDiemXayDungDuAn = txtDiaDiemXayDungDuAn.Text;
                TEA.MucTieuDuAn = txtMucTieuDuAn.Text;
                TEA.MaMienGiam = txtMaMienGiam.Code;
                TEA.PhamViDangKyDMMT = txtPhamViDangKyDMMT.Text;
                TEA.NgayDuKienXNK = clcNgayDuKienXNK.Value;
                TEA.GP_GCNDauTuSo = txtGP_GCNDauTuSo.Text;
                TEA.NgayChungNhan = clcNgayChungNhan.Value;
                TEA.CapBoi = txtCapBoi.Text;
                TEA.GhiChu = txtGhiChu.Text;
                TEA.CamKetSuDung = txtCamKetSuDung.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void SetTEA()
        {
            try
            {
                errorProvider.Clear();
                txtSoDanhMucMienThue.Text = TEA.SoDanhMucMienThue.ToString();
                cbbPhanLoaiXuatNhapKhau.SelectedValue = TEA.PhanLoaiXuatNhapKhau;
                ctrCoQuanHaiQuan.Code = TEA.CoQuanHaiQuan;
                txtDiaChiCuaNguoiKhai.Text = TEA.DiaChiCuaNguoiKhai;
                txtSDTCuaNguoiKhai.Text = TEA.SDTCuaNguoiKhai;
                clcThoiHanMienThue.Value = TEA.ThoiHanMienThue;
                txtTenDuAnDauTu.Text = TEA.TenDuAnDauTu;
                txtDiaDiemXayDungDuAn.Text = TEA.DiaDiemXayDungDuAn;
                txtMucTieuDuAn.Text = TEA.MucTieuDuAn;
                txtMaMienGiam.Code = TEA.MaMienGiam;
                txtPhamViDangKyDMMT.Text = TEA.PhamViDangKyDMMT;
                clcNgayDuKienXNK.Value = TEA.NgayDuKienXNK;
                txtGP_GCNDauTuSo.Text = TEA.GP_GCNDauTuSo;
                clcNgayChungNhan.Value = TEA.NgayChungNhan;
                txtCapBoi.Text = TEA.CapBoi;
                txtGhiChu.Text = TEA.GhiChu;
                txtCamKetSuDung.Text = TEA.CamKetSuDung;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void VNACC_TEAForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.SetIDControl();
                // Hai quan
                string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS; //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
                //if (TEA.ID != null)
                //{
                //    SetTEA();

                //}

                SetTEA();
                LoadGrid();
                TuDongCapNhatThongTin();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ShowKetQuaTraVe()
        {
            try
            {
                SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
                f.isSend = false;
                f.isRep = true;
                f.SoToKhai = txtSoDanhMucMienThue.Text;
                f.inputMSGID = TEA.InputMessageID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void setCommandStatus()
        {
            if (TEA.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TEA.TrangThaiXuLy == null)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBao.Enabled = cmdKhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdNguoiXNK.Enabled = cmdNguoiXNK1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdThemHang.Enabled = cmdThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdDieuChinh.Enabled = cmdDieuChinh1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                this.OpenType = OpenFormType.Edit;
            }
            else if (TEA.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBao.Enabled = cmdKhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdNguoiXNK.Enabled = cmdNguoiXNK1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdThemHang.Enabled = cmdThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdDieuChinh.Enabled = cmdDieuChinh1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                this.OpenType = OpenFormType.View;

            }
            else if (TEA.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBao.Enabled = cmdKhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdNguoiXNK.Enabled = cmdNguoiXNK1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdThemHang.Enabled = cmdThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdDieuChinh.Enabled = cmdDieuChinh1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                this.OpenType = OpenFormType.View;
            }

        }
        private void Save()
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                if (TEA.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam)
                {
                    if (ShowMessage("Tờ khai đã khai báo, bạn có muốn lưu lại tờ khai để khai báo lại ?", true) == "Yes")
                    {
                        TEA.TrangThaiXuLy = EnumTrangThaiXuLy.ChuaKhaiBao;
                    }
                    else
                        return;
                }

                GetTEA();
                if (TEA.HangCollection.Count == 0)
                {
                    ShowMessage("Chưa nhập thông tin hàng", false);
                }
                else
                {

                    TEA.TrangThaiXuLy = EnumTrangThaiXuLy.ChuaKhaiBao;

                    TEA.InsertUpdateFul();
                    ShowMessage("Lưu tờ khai thành công", false);

                }
                this.Cursor = Cursors.Default;           
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }
        private void LoadGrid()
        {
            try
            {
                grDieuChinh.DataSource = TEA.DieuChinhCollection;
                grHang.DataSource = TEA.HangCollection;
                grNguoiXNK.DataSource = TEA.NguoiXNKCollection;
                grDieuChinh.Refetch();
                grHang.Refetch();
                grNguoiXNK.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
       
        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                txtSoDanhMucMienThue.Tag = "TEN"; //Số danh mục miễn thuế
                cbbPhanLoaiXuatNhapKhau.Tag = "IEC"; //Phân loại xuất nhập khẩu
                ctrCoQuanHaiQuan.TagCode = "CH"; //Cơ quan Hải quan
                txtDiaChiCuaNguoiKhai.Tag = "IMA"; //Địa chỉ của người khai
                txtSDTCuaNguoiKhai.Tag = "IMT"; //Số điện thoại của người khai
                clcThoiHanMienThue.TagName = "TED"; //Thời hạn miễn thuế
                txtTenDuAnDauTu.Tag = "IPN"; //Tên dự án đầu tư
                txtDiaDiemXayDungDuAn.Tag = "PPC"; //Địa điểm xây dựng dự án
                txtMucTieuDuAn.Tag = "PO"; //Mục tiêu dự án
                txtMaMienGiam.Tag = "RE"; //Mã miễn / Giảm / Không chịu thuế xuất nhập khẩu
                txtPhamViDangKyDMMT.Tag = "SEL"; //Phạm vi đăng ký DMMT
                clcNgayDuKienXNK.TagName = "PTI"; //Ngày dự kiến xuất/nhập khẩu
                txtGP_GCNDauTuSo.Tag = "ICN"; //Giấy phép đầu tư hoặc Giấy chứng nhận đầu tư số
                clcNgayChungNhan.TagName = "DOC"; //Ngày chứng nhận
                txtCapBoi.Tag = "IB"; //Cấp bởi
                txtGhiChu.Tag = "IR"; //Ghi chú (dành cho người khai)
                txtCamKetSuDung.Tag = "CRP"; //Cam kết sử dụng đúng mục đích

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        //private void SendVnaccs(bool KhaiBaoSua)
        //{
        //    try
        //    {
        //        if (TEA.ID == 0)
        //        {
        //            this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
        //            return;
        //        }
        //        if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
        //        {
        //            TEA tea = VNACCMaperFromObject.TEAMapper(TEA);
        //            if (tea == null)
        //            {
        //                this.ShowMessage("Lỗi khi tạo messages !", false);
        //                return;
        //            }
        //            MessagesSend msg;
        //            if (!KhaiBaoSua)
        //            {
        //                TEA.InputMessageID = HelperVNACCS.NewInputMSGID();
        //                TEA.InsertUpdateFul();
        //                msg = MessagesSend.Load<TEA>(tea, TEA.InputMessageID);

        //            }
        //            else
        //            {
        //                msg = MessagesSend.Load<TEA>(tea, true, EnumNghiepVu.TEA, TEA.InputMessageID);
        //            }

        //            MsgLog.SaveMessages(msg, TEA.ID, EnumThongBao.SendMess, "");
        //            SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
        //            f.isSend = true;
        //            f.isRep = false;
        //            f.inputMSGID = TEA.InputMessageID;
        //            f.ShowDialog(this);
        //            if (f.DialogResult == DialogResult.Cancel)
        //            {
        //                this.ShowMessage(f.msgFeedBack, false);
        //            }
        //            else if (f.DialogResult == DialogResult.No)
        //            {
        //            }
        //            else
        //            {
        //                string ketqua = "Khai báo thông tin thành công";
        //                CapNhatThongTin(f.feedback.ResponseData);
        //                if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
        //                {
        //                    try
        //                    {
        //                        decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
        //                        ketqua = ketqua + Environment.NewLine;
        //                        ketqua += "Số tờ khai: " + SoToKhai;
        //                    }
        //                    catch (System.Exception ex)
        //                    {
        //                        ketqua += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
        //                        Logger.LocalLogger.Instance().WriteMessage(ex);
        //                    }
        //                }
        //                this.ShowMessageTQDT(ketqua, false);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.ShowMessage(ex.Message, false);
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //    }
        //}
        #region Cập nhật thông tin
        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            CapNhatThongTin(msgResult, string.Empty);
            setCommandStatus();
        }
        private void CapNhatThongTin(ReturnMessages msgResult, string MaNV)
        {
            try
            {
                ProcessMessages.GetDataResult_TEA(msgResult, MaNV, TEA);
                //TEA.InsertUpdateFull();
                //SetToKhai();
                //setCommandStatus();
                VAD8010 vad8010 = new VAD8010();
                vad8010.LoadVAD8010(msgResult.Body.ToString());

                TEA.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoChinhThuc;
                TEA.SoDanhMucMienThue = System.Convert.ToDecimal(vad8010.A01.GetValue().ToString());

                TEA.InsertUpdateFul();  
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void TuDongCapNhatThongTin()
        {
            if (TEA != null && TEA.ID > 0 && !string.IsNullOrEmpty(TEA.InputMessageID))
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("MessagesInputID = '{0}'", TEA.InputMessageID), null);
               
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        //msgPb.Delete();
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem;
                        msgPb.Update();//Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }
        #endregion
        public void sendvnaccs(bool KhaiBaoSua)
        {
            try
            {
                if (TEA.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {

                    TEA tea = VNACCMaperFromObject.TEAMapper(TEA);
                    if (tea == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        TEA.InputMessageID = HelperVNACCS.NewInputMSGID();
                        TEA.InsertUpdateFul();
                        msg = MessagesSend.Load<TEA>(tea, TEA.InputMessageID);

                    }
                    else
                    {
                        msg = MessagesSend.Load<TEA>(tea, true, EnumNghiepVu.TEA, TEA.InputMessageID);
                    }
                    
                    MsgLog.SaveMessages(msg, TEA.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = true;
                    f.inputMSGID = TEA.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result)
                    {
                        TuDongCapNhatThongTin();
                        SetTEA();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                   

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }


    }
}
