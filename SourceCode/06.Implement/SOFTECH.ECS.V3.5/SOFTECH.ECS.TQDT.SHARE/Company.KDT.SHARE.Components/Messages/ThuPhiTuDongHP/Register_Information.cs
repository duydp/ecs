﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
    [XmlRoot("Declaration")]
  public  class Register_Information
    {
        /// <summary>
        /// Loại chứng từ (= 500) 
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// Số tham chiếu tờ khai
        /// 
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày khai báo
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Chức năng (khai báo= 8, sửa=5)
        /// </summary>
        [XmlElement("function")]
        public string Function { get; set; }

        /// <summary>
        ///Đơn vị khai báo
        /// </summary>
        [XmlElement("Company")]
        public Company Company { get; set; }

        /// <summary>
        ///Đơn vị khai báo
        /// </summary>
        [XmlElement("SignDigital")]
        public SignDigital SignDigital { get; set; }

    }
}
