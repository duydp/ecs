using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Management;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;
using Company.KD.BLL.Utils;
using System.Data;
using System.Xml;
using Company.KDT.SHARE.Components.Utils;

namespace Company.Interface
{
    internal static class Program
    {
        public static bool isActivated = false;
        public static License lic = null;

        [STAThread]
        private static void Main()
        {
            try
            {

                try
                {
                   
                   // Company.KDT.SHARE.Components.Helpers.DemoSend();
                    lic = new Company.KDT.SHARE.Components.Utils.License();
                    loadLicense();
       
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + "\nChương trình có lỗi. Vui lòng cài đặt lại.");
                    Application.Exit();
                    return;
                }

                //isActivated = checkRegistered();
                //isActivated = true;

                string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
                ApplicationSettings settings = new ApplicationSettings();
                settings.ReadXml(settingFileName);
                GlobalSettings.RefreshKey();
                CultureInfo culture = null;
                DevExpress.UserSkins.OfficeSkins.Register();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                if (Properties.Settings.Default.NgonNgu == "0")
                    culture = new CultureInfo("vi-VN");
                else
                    culture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;


                Application.Run(new MainForm());

                //Application.Run(new HangTon());
            }
            catch (Exception exx)
            {
                Logger.LocalLogger.Instance().WriteMessage(exx);
                MessageBox.Show(" Lỗi : " + exx.Message);
            }
        }

        //-----------------------------------------------------------------------------------------------
        #region Activate Software
        private static void loadLicense()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                int numInConf = int.Parse(lic.DecryptString(config.AppSettings.Settings["DateTr"].Value));
                if (!lic.checkExistsLicense())
                {
                    lic.generTrial();
                    lic.Load();
                    if (numInConf < int.Parse(lic.dateTrial))
                    {
                        lic.dateTrial = numInConf.ToString();
                        lic.Save();
                    }
                }
                else lic.Load();
                isActivated = !string.IsNullOrEmpty(lic.codeActivate);

                //if (numInConf > int.Parse(lic.dateTrial))
                //{
                //    config.AppSettings.Settings.Remove("DateTr");
                //    config.AppSettings.Settings.Add("DateTr", lic.EncryptString(lic.dateTrial));
                //    config.Save(ConfigurationSaveMode.Full);
                //    ConfigurationManager.RefreshSection("appSettings");
                //}
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static string getProcessorID()
        {
            string CPUID = "BoardID:";
            ManagementObjectSearcher srch = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
            foreach (ManagementObject obj in srch.Get())
            {
                CPUID = obj.Properties["SerialNumber"].Value.ToString();
            }
            return License.md5String(CPUID);
        }

        public static string getCodeToActivate()
        {
            return License.md5String(getProcessorID() + "SOFTECH.ECS.KD");
        }

        private static bool checkRegistered()
        {
            bool value = false;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (lic.codeActivate == getCodeToActivate())
            {
                CultureInfo infoVN = new CultureInfo("vi-VN");
                if (DateTime.Parse(lic.dayExpires, infoVN.DateTimeFormat).Date > DateTime.Now.Date)
                {
                    value = true;
                }
            }
            return value;
        }
        #endregion
        //-----------------------------------------------------------------------------------------------
    }
}
