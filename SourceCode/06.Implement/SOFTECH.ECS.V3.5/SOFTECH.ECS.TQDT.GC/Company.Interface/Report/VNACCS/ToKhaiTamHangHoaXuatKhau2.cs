using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.KDT.SHARE.VNACCS;
namespace Company.Interface.Report.VNACCS
{
    public partial class ToKhaiTamHangHoaXuatKhau2 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiTamHangHoaXuatKhau2()
        {
            InitializeComponent();
        }
        public void BindReport(VAE1LD0 Vae)
        {
            //lblSoTrang.Text = "2/" + Vae.K69.GetValue().ToString();
            int HangHoa = Vae.HangMD.Count;
            if (HangHoa <= 2)
            {
                lblSoTrang.Text = "2/" + System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblSoTrang.Text = "2/" + System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblSotokhai.Text = Vae.ECN.GetValue().ToString();
            lblSotokhaidautien.Text = Vae.FIC.GetValue().ToString();
            lblSonhanhtokhaichianho.Text = Vae.BNO.GetValue().ToString();
            lblTongsotokhaichianho.Text = Vae.DNO.GetValue().ToString();
            lblSotokhaitamnhaptaixuat.Text = Vae.TDN.GetValue().ToString();
            lblMaphanloaikiemtra.Text = Vae.K07.GetValue().ToString();
            lblMaloaihinh.Text = Vae.ECB.GetValue().ToString();
            lblMaphanloaihanghoa.Text = Vae.CCC.GetValue().ToString();
            lblMahieuphuongthucvanchuyen.Text = Vae.MTC.GetValue().ToString();
            lblMasothuedaidien.Text = Vae.K01.GetValue().ToString();
            lblTencoquanhaiquantiepnhantokhai.Text = Vae.K08.GetValue().ToString();
            lblNhomxulyhoso.Text = Vae.CHB.GetValue().ToString();
            if
            (Convert.ToDateTime(Vae.K10.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaydangky.Text = Convert.ToDateTime(Vae.K10.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydangky.Text = "";
            }


            if (Vae.AD1.GetValue().ToString() != "" && Vae.AD1.GetValue().ToString() != "0")
            {
                lblGiodangky.Text = Vae.AD1.GetValue().ToString().Substring(0, 2) + ":" + Vae.AD1.GetValue().ToString().Substring(2, 2) + ":" + Vae.AD1.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiodangky.Text = "";
            }


            if
            (Convert.ToDateTime(Vae.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaythaydoidangky.Text = Convert.ToDateTime(Vae.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaythaydoidangky.Text = "";
            }

            if (Vae.AD3.GetValue().ToString() != "" && Vae.AD3.GetValue().ToString() != "0")
            {
                lblGiothaydoidangky.Text = Vae.AD3.GetValue().ToString().Substring(0, 2) + ":" + Vae.AD3.GetValue().ToString().Substring(2, 2) + ":" + Vae.AD3.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiothaydoidangky.Text = "";
            }


            if
            (Convert.ToDateTime(Vae.RID.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoihantainhapxuat.Text = Convert.ToDateTime(Vae.RID.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoihantainhapxuat.Text = "";
            }

            lblBieuthitruonghophethan.Text = Vae.AAA.GetValue().ToString();
            //lblNgaydukienden.Text = Convert.ToDateTime(Vae.ADT.GetValue()).ToString("dd/MM/yyyy");

            lblTendiachixephang.Text = Vae.VN.GetValue().ToString();
            lblDiachixephang.Text = Vae.K72.GetValue().ToString();

            lblPhanloai.Text = Vae.CCM.GetValue().ToString();

            for (int i = 0; i < Vae.VC_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaxephang1.Text = Vae.VC_.listAttribute[0].GetValueCollection(i).ToString();
                        break;
                    case 1:
                        lblMaxephang2.Text = Vae.VC_.listAttribute[0].GetValueCollection(i).ToString();
                        break;
                    case 2:
                        lblMaxephang3.Text = Vae.VC_.listAttribute[0].GetValueCollection(i).ToString();
                        break;
                    case 3:
                        lblMaxephang4.Text = Vae.VC_.listAttribute[0].GetValueCollection(i).ToString();

                        break;
                    case 4:
                        lblMaxephang5.Text = Vae.VC_.listAttribute[0].GetValueCollection(i).ToString();
                        break;

                }

            }

            BindReportContainer(Vae);
            BindReportChiThiHQ(Vae);

        }
        public void BindReportChiThiHQ(VAE1LD0 Vae)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("Ten", typeof(string));
                dt.Columns.Add("NoiDung", typeof(string));
                dt.Columns.Add("STT", typeof(string));
                int STT = 0;

                for (int i = 0; i < Vae.D__.listAttribute[0].ListValue.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    if
                      (Convert.ToDateTime(Vae.D__.listAttribute[0].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                    {
                        dr["Date"] = Convert.ToDateTime(Vae.D__.listAttribute[0].GetValueCollection(i)).ToString("dd/MM/yyyy");
                    }


                    dr["Ten"] = Vae.D__.listAttribute[1].GetValueCollection(i).ToString();
                    dr["STT"] = STT.ToString();
                    dr["NoiDung"] = Vae.D__.listAttribute[2].GetValueCollection(i).ToString();
                    dt.Rows.Add(dr);
                }

                while (dt.Rows.Count < 10)
                {

                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["Date"] = "";
                    dr["Ten"] = "";
                    dr["NoiDung"] = "";
                    dr["STT"] = STT.ToString();
                    dt.Rows.Add(dr);
                }
                DetailReport1.DataSource = dt;
                lblNgaychithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "Date");
                lblTenchithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "Ten");
                lblNoidungchithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "NoiDung");
                lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport1.DataSource, "STT");
            }
            catch (Exception e)
            {


            }

        }

        public void BindReportContainer(VAE1LD0 Vae)
        {

            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("STT1", typeof(string));
                dt.Columns.Add("SoCont1", typeof(string));
                dt.Columns.Add("STT2", typeof(string));
                dt.Columns.Add("SoCont2", typeof(string));
                dt.Columns.Add("STT3", typeof(string));
                dt.Columns.Add("SoCont3", typeof(string));
                dt.Columns.Add("STT4", typeof(string));
                dt.Columns.Add("SoCont4", typeof(string));
                dt.Columns.Add("STT5", typeof(string));
                dt.Columns.Add("SoCont5", typeof(string));
                for (int i = 1; i < 11; i++)
                {
                    DataRow dr = dt.NewRow();
                    int r = 1;
                    for (int j = (i - 1) * 5; j < i * 5; j++)
                    {

                        dr["STT" + r] = (j + 1).ToString();
                        dr["SoCont" + r] = Vae.C__.listAttribute[0].GetValueCollection(j).ToString();
                        r = r + 1;

                    }
                    dt.Rows.Add(dr);

                }

                DetailReport.DataSource = dt;


                lblSTT1.DataBindings.Add("Text", DetailReport.DataSource, "STT1");
                lblSocontainer1.DataBindings.Add("Text", DetailReport.DataSource, "SoCont1");
                lblSTT2.DataBindings.Add("Text", DetailReport.DataSource, "STT2");
                lblSocontainer2.DataBindings.Add("Text", DetailReport.DataSource, "SoCont2");
                lblSTT3.DataBindings.Add("Text", DetailReport.DataSource, "STT3");
                lblSocontainer3.DataBindings.Add("Text", DetailReport.DataSource, "SoCont3");
                lblSTT4.DataBindings.Add("Text", DetailReport.DataSource, "STT4");
                lblSocontainer4.DataBindings.Add("Text", DetailReport.DataSource, "SoCont4");
                lblSTT5.DataBindings.Add("Text", DetailReport.DataSource, "STT5");
                lblSocontainer5.DataBindings.Add("Text", DetailReport.DataSource, "SoCont5");

            }
            catch (Exception ex)
            {


            }




        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
        }
    }
}
