namespace Company.Interface.ProdInfo
{
    partial class frmRegister_old
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegister_old));
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtCDKey = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtCodeActivate = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnActivate = new Janus.Windows.EditControls.UIButton();
            this.btnCancel = new Janus.Windows.EditControls.UIButton();
            this.radioCDKey = new System.Windows.Forms.RadioButton();
            this.radioFile = new System.Windows.Forms.RadioButton();
            this.txtFile = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.txtFile);
            this.grbMain.Controls.Add(this.radioFile);
            this.grbMain.Controls.Add(this.radioCDKey);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.pictureBox2);
            this.grbMain.Controls.Add(this.txtCDKey);
            this.grbMain.Controls.Add(this.txtCodeActivate);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.pictureBox1);
            this.grbMain.Controls.Add(this.btnActivate);
            this.grbMain.Controls.Add(this.btnCancel);
            this.grbMain.Size = new System.Drawing.Size(497, 256);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(147, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(349, 18);
            this.label5.TabIndex = 23;
            this.label5.Text = "Khi kích hoạt phần mềm bằng CD Key, vui lòng kiểm tra kết nối internet.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(13, 110);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(128, 128);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 22;
            this.pictureBox2.TabStop = false;
            // 
            // txtCDKey
            // 
            this.txtCDKey.BackColor = System.Drawing.Color.White;
            this.txtCDKey.Location = new System.Drawing.Point(224, 172);
            this.txtCDKey.MaxLength = 25;
            this.txtCDKey.Name = "txtCDKey";
            this.txtCDKey.Size = new System.Drawing.Size(245, 21);
            this.txtCDKey.TabIndex = 20;
            this.txtCDKey.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.txtCDKey.VisualStyleManager = this.vsmMain;
            // 
            // txtCodeActivate
            // 
            this.txtCodeActivate.BackColor = System.Drawing.SystemColors.Info;
            this.txtCodeActivate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodeActivate.Location = new System.Drawing.Point(224, 122);
            this.txtCodeActivate.MaxLength = 25;
            this.txtCodeActivate.Name = "txtCodeActivate";
            this.txtCodeActivate.ReadOnly = true;
            this.txtCodeActivate.Size = new System.Drawing.Size(245, 21);
            this.txtCodeActivate.TabIndex = 21;
            this.txtCodeActivate.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.txtCodeActivate.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(177, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "CD key:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(151, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Mã kích hoạt:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(497, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // btnActivate
            // 
            this.btnActivate.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnActivate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivate.Image = ((System.Drawing.Image)(resources.GetObject("btnActivate.Image")));
            this.btnActivate.Location = new System.Drawing.Point(313, 232);
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.Size = new System.Drawing.Size(87, 23);
            this.btnActivate.TabIndex = 15;
            this.btnActivate.Text = "Kích hoạt";
            this.btnActivate.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnActivate.Click += new System.EventHandler(this.btnActivate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(404, 232);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 23);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Thoát";
            this.btnCancel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // radioCDKey
            // 
            this.radioCDKey.AutoSize = true;
            this.radioCDKey.BackColor = System.Drawing.Color.Transparent;
            this.radioCDKey.Checked = true;
            this.radioCDKey.ForeColor = System.Drawing.Color.Black;
            this.radioCDKey.Location = new System.Drawing.Point(224, 149);
            this.radioCDKey.Name = "radioCDKey";
            this.radioCDKey.Size = new System.Drawing.Size(88, 17);
            this.radioCDKey.TabIndex = 24;
            this.radioCDKey.TabStop = true;
            this.radioCDKey.Text = "Nhập CD Key";
            this.radioCDKey.UseVisualStyleBackColor = false;
            this.radioCDKey.CheckedChanged += new System.EventHandler(this.radioCDKey_CheckedChanged);
            // 
            // radioFile
            // 
            this.radioFile.AutoSize = true;
            this.radioFile.BackColor = System.Drawing.Color.Transparent;
            this.radioFile.ForeColor = System.Drawing.Color.Black;
            this.radioFile.Location = new System.Drawing.Point(318, 149);
            this.radioFile.Name = "radioFile";
            this.radioFile.Size = new System.Drawing.Size(115, 17);
            this.radioFile.TabIndex = 24;
            this.radioFile.TabStop = true;
            this.radioFile.Text = "Chọn tệp kích hoạt";
            this.radioFile.UseVisualStyleBackColor = false;
            // 
            // txtFile
            // 
            this.txtFile.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFile.Enabled = false;
            this.txtFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFile.Location = new System.Drawing.Point(224, 199);
            this.txtFile.Name = "txtFile";
            this.txtFile.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(245, 21);
            this.txtFile.TabIndex = 25;
            this.txtFile.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtFile.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtFile.VisualStyleManager = this.vsmMain;
            this.txtFile.ButtonClick += new System.EventHandler(this.txtFile_ButtonClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(147, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Tệp kích hoạt:";
            // 
            // frmRegister
            // 
            this.AcceptButton = this.btnActivate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(497, 256);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRegister";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Kích hoạt phần mềm";
            this.Load += new System.EventHandler(this.frmRegister_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtCDKey;
        private Janus.Windows.GridEX.EditControls.EditBox txtCodeActivate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Janus.Windows.EditControls.UIButton btnActivate;
        private Janus.Windows.EditControls.UIButton btnCancel;
        private System.Windows.Forms.RadioButton radioFile;
        private System.Windows.Forms.RadioButton radioCDKey;
        private Janus.Windows.GridEX.EditControls.EditBox txtFile;
        private System.Windows.Forms.Label label3;
    }
}