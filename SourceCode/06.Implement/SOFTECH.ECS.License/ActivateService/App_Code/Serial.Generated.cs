using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Entity
{
    public partial class Serial
    {
        #region Private members.

        protected string _Code = string.Empty;
        protected string _LicenseID = string.Empty;
        protected string _ProductID = string.Empty;
        protected string _EmployeeID = string.Empty;
        protected DateTime _CreateDate;
        protected bool _Status;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string Code
        {
            set { this._Code = value; }
            get { return this._Code; }
        }

        public string LicenseID
        {
            set { this._LicenseID = value; }
            get { return this._LicenseID; }
        }

        public string ProductID
        {
            set { this._ProductID = value; }
            get { return this._ProductID; }
        }

        public string EmployeeID
        {
            set { this._EmployeeID = value; }
            get { return this._EmployeeID; }
        }

        public DateTime CreateDate
        {
            set { this._CreateDate = value; }
            get { return this._CreateDate; }
        }

        public bool Status
        {
            set { this._Status = value; }
            get { return this._Status; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static Serial Load(string code)
        {
            const string spName = "[dbo].[p_LICENSE_Serial_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Code", SqlDbType.VarChar, code);
            Serial entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new Serial();
                if (!reader.IsDBNull(reader.GetOrdinal("Code"))) entity.Code = reader.GetString(reader.GetOrdinal("Code"));
                if (!reader.IsDBNull(reader.GetOrdinal("LicenseID"))) entity.LicenseID = reader.GetString(reader.GetOrdinal("LicenseID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ProductID"))) entity.ProductID = reader.GetString(reader.GetOrdinal("ProductID"));
                if (!reader.IsDBNull(reader.GetOrdinal("EmployeeID"))) entity.EmployeeID = reader.GetString(reader.GetOrdinal("EmployeeID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreateDate"))) entity.CreateDate = reader.GetDateTime(reader.GetOrdinal("CreateDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("Status"))) entity.Status = reader.GetBoolean(reader.GetOrdinal("Status"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_LicenseID(string licenseID)
        {
            const string spName = "[dbo].[p_LICENSE_Serial_SelectBy_LicenseID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, licenseID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static DataSet SelectBy_EmployeeID(string employeeID)
        {
            const string spName = "[dbo].[p_LICENSE_Serial_SelectBy_EmployeeID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, employeeID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static DataSet SelectBy_ProductID(string productID)
        {
            const string spName = "[dbo].[p_LICENSE_Serial_SelectBy_ProductID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, productID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_Serial_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_Serial_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_Serial_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_Serial_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_LicenseID(string licenseID)
        {
            const string spName = "p_LICENSE_Serial_SelectBy_LicenseID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, licenseID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static IDataReader SelectReaderBy_EmployeeID(string employeeID)
        {
            const string spName = "p_LICENSE_Serial_SelectBy_EmployeeID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, employeeID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static IDataReader SelectReaderBy_ProductID(string productID)
        {
            const string spName = "p_LICENSE_Serial_SelectBy_ProductID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, productID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static string InsertSerial(string licenseID, string productID, string employeeID, DateTime createDate, bool status)
        {
            Serial entity = new Serial();
            entity.LicenseID = licenseID;
            entity.ProductID = productID;
            entity.EmployeeID = employeeID;
            entity.CreateDate = createDate;
            entity.Status = status;
            return entity.Insert();
        }

        public string Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public string Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_Serial_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Code", SqlDbType.VarChar, Code);
            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, LicenseID);
            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, ProductID);
            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, EmployeeID);
            db.AddInParameter(dbCommand, "@CreateDate", SqlDbType.DateTime, CreateDate);
            db.AddInParameter(dbCommand, "@Status", SqlDbType.Bit, Status);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._Code = (string)db.GetParameterValue(dbCommand, "@Code");
                return this._Code;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._Code = (string)db.GetParameterValue(dbCommand, "@Code");
                return this._Code;
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateSerial(string code, string licenseID, string productID, string employeeID, DateTime createDate, bool status)
        {
            Serial entity = new Serial();
            entity.LicenseID = licenseID;
            entity.ProductID = productID;
            entity.EmployeeID = employeeID;
            entity.CreateDate = createDate;
            entity.Status = status;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_LICENSE_Serial_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Code", SqlDbType.VarChar, Code);
            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, LicenseID);
            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, ProductID);
            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, EmployeeID);
            db.AddInParameter(dbCommand, "@CreateDate", SqlDbType.DateTime, CreateDate);
            db.AddInParameter(dbCommand, "@Status", SqlDbType.Bit, Status);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateSerial(string code, string licenseID, string productID, string employeeID, DateTime createDate, bool status)
        {
            Serial entity = new Serial();
            entity.Code = code;
            entity.LicenseID = licenseID;
            entity.ProductID = productID;
            entity.EmployeeID = employeeID;
            entity.CreateDate = createDate;
            entity.Status = status;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_Serial_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Code", SqlDbType.VarChar, Code);
            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, LicenseID);
            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, ProductID);
            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, EmployeeID);
            db.AddInParameter(dbCommand, "@CreateDate", SqlDbType.DateTime, CreateDate);
            db.AddInParameter(dbCommand, "@Status", SqlDbType.Bit, Status);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteSerial(string code)
        {
            Serial entity = new Serial();
            entity.Code = code;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_Serial_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Code", SqlDbType.VarChar, Code);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_LicenseID(string licenseID)
        {
            string spName = "[dbo].[p_LICENSE_Serial_DeleteBy_LicenseID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, licenseID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_EmployeeID(string employeeID)
        {
            string spName = "[dbo].[p_LICENSE_Serial_DeleteBy_EmployeeID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, employeeID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_ProductID(string productID)
        {
            string spName = "[dbo].[p_LICENSE_Serial_DeleteBy_ProductID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, productID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        #endregion
    }
}