using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using System.Xml.Serialization;

namespace Company.Interface.GC
{
    public partial class DinhMucGCRegistedForm : BaseForm
    {
        public bool IsBrowseForm = false;
        Company.GC.BLL.GC.DinhMuc dinhmuc = new Company.GC.BLL.GC.DinhMuc();
        public Company.GC.BLL.GC.DinhMucCollection DMCollection = new Company.GC.BLL.GC.DinhMucCollection();
        public long idHD = 0;
        public DinhMucGCRegistedForm()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            string where = "";
            this.idHD = 0;
            try
            {
                this.idHD = Convert.ToInt64(cbHopDong.Value);
            }
            catch
            {
                //ShowMessage("Không có hợp đồng này trong hệ thống.", false);
                showMsg("MSG_240205");
                return;
            }
            where += string.Format(" 1 = 1 ");
            if (txtMaSP.Text.Trim().Length > 0)
            {
                where += string.Format(" And MaSanPham like'%{0}%' ", txtMaSP.Text.Trim());
            }
            if (cbHopDong.Text.Trim() != "")
                where += string.Format(" And HopDong_ID = {0} ", this.idHD);
            else
            {
                where += string.Format(" 1 = 1 ");
                return;
            }

            //DATLMQ bổ sung tìm kiếm định mức theo Người khai báo 01/07/2011
            if (cbUserKB.SelectedValue != null && cbUserKB.SelectedIndex != 0)
            {
                try
                {
                    DMCollection = new Company.GC.BLL.GC.DinhMuc().GetDMFromUserName(cbUserKB.SelectedItem.Value.ToString(), txtMaSP.Text.Trim(), this.idHD);
                    dgList.DataSource = this.DMCollection;
                    return;
                }
                catch (Exception ex)
                {
                    ShowMessage("Có lỗi trong quá trình tìm kiếm dữ liệu!\r\nChi tiết lỗi: " + ex.Message, false);
                    return;
                }
            }

            Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
            this.DMCollection = dm.SelectCollectionDynamic(where, "");
            dgList.DataSource = this.DMCollection;
        }

        private void setDataToComboUserKB()
        {
            DataTable dt = new Company.QuanTri.User().SelectAll().Tables[0];
            DataRow dr = dt.NewRow();
            dr["USER_NAME"] = -1;
            dr["HO_TEN"] = "--Tất cả--";
            dt.Rows.InsertAt(dr, 0);
            cbUserKB.DataSource = dt;
            cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
            cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
        }

        private void BindHopDong()
        {
            Company.GC.BLL.KDT.GC.HopDong hopdong = new Company.GC.BLL.KDT.GC.HopDong();
            DataTable dt;

            string where = string.Format("MaDoanhNghiep='{0}' and TrangThaiXuLy=1", GlobalSettings.MA_DON_VI);
            dt = HopDong.SelectDynamic(where, "").Tables[0];

            cbHopDong.DataSource = dt;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            try
            {
                cbHopDong.SelectedIndex = 0;
            }
            catch { }
        }
        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];

            // Đơn vị Hải quan.            
        }

        //-----------------------------------------------------------------------------------------

        private void DinhMucGCRegistedForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            dgList.Tables[0].Columns["NPL_TuCungUng"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            this.khoitao_DuLieuChuan();
            setDataToComboUserKB();
            cbUserKB.SelectedIndex = 0;
            BindHopDong();
            BindData();
        }

        //-----------------------------------------------------------------------------------------

        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {


        }

        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            BindHopDong();
            this.BindData();

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                Company.GC.BLL.GC.DinhMuc DM = (Company.GC.BLL.GC.DinhMuc)e.Row.DataRow;
                if (DM.TenNPL != null && DM.TenNPL.Trim().Length == 0)
                {
                    Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                    NPL.HopDong_ID = DM.HopDong_ID;
                    NPL.Ma = DM.MaNguyenPhuLieu;
                    NPL.Load();
                    DM.TenNPL = NPL.Ten;
                }

                if (DM.TenSanPham == null || DM.TenSanPham.Trim().Length == 0)
                {
                    Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
                    SP.HopDong_ID = DM.HopDong_ID;
                    SP.Ma = DM.MaSanPham;
                    SP.Load();
                    DM.TenSanPham = SP.Ten;
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.dinhmuc.InsertUpdate(this.DMCollection);
                //ShowMessage("Lưu định mức thành công.", false);
                showMsg("MSG_2702001");
            }
            catch (Exception ex)
            {
                //ShowMessage("Có lỗi khi lưu định mức.", false);
                showMsg("MSG_SAV022");
                //Message("MSG_SAV01", "", false);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        Company.GC.BLL.GC.DinhMuc dm = (Company.GC.BLL.GC.DinhMuc)i.GetRow().DataRow;
                        dm.Delete();
                    }
                }
                BindData();
            }

        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            if (showMsg("MSG_2702005", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa các định mức này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        Company.GC.BLL.GC.DinhMuc dm = (Company.GC.BLL.GC.DinhMuc)i.GetRow().DataRow;
                        dm.Delete();

                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DinhMucGCDaduyetEditForm f = new DinhMucGCDaduyetEditForm();
            HopDong HD = new HopDong();
            HD.ID = this.idHD;
            HD = HopDong.Load(HD.ID);

            f.HD = HD;
            f.ShowDialog();
            BindData();
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Globals.ExportData(dgList);
        }

        //-----------------------------------------------------------------------------------------
    }
}