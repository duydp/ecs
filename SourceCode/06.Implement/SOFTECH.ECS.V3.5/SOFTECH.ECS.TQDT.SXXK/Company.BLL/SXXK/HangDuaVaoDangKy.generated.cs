using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.BLL.SXXK
{
	public partial class HangDuaVaoDangKy : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public string MaHaiQuan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string MaDaiLy { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string GUIDSTR { set; get; }
		public string DeXuatKhac { set; get; }
		public string LyDoSua { set; get; }
		public int ActionStatus { set; get; }
		public string GuidReference { set; get; }
		public short NamDK { set; get; }
		public string HUONGDAN { set; get; }
		public string PhanLuong { set; get; }
		public string Huongdan_PL { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<HangDuaVaoDangKy> ConvertToCollection(IDataReader reader)
		{
			IList<HangDuaVaoDangKy> collection = new List<HangDuaVaoDangKy>();
			while (reader.Read())
			{
				HangDuaVaoDangKy entity = new HangDuaVaoDangKy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<HangDuaVaoDangKy> collection, long id)
        {
            foreach (HangDuaVaoDangKy item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_CX_HangDuaVaoDangKy VALUES(@SoTiepNhan, @NgayTiepNhan, @MaHaiQuan, @MaDoanhNghiep, @MaDaiLy, @TrangThaiXuLy, @GUIDSTR, @DeXuatKhac, @LyDoSua, @ActionStatus, @GuidReference, @NamDK, @HUONGDAN, @PhanLuong, @Huongdan_PL)";
            string update = "UPDATE t_KDT_CX_HangDuaVaoDangKy SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, MaDaiLy = @MaDaiLy, TrangThaiXuLy = @TrangThaiXuLy, GUIDSTR = @GUIDSTR, DeXuatKhac = @DeXuatKhac, LyDoSua = @LyDoSua, ActionStatus = @ActionStatus, GuidReference = @GuidReference, NamDK = @NamDK, HUONGDAN = @HUONGDAN, PhanLuong = @PhanLuong, Huongdan_PL = @Huongdan_PL WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_CX_HangDuaVaoDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLy", SqlDbType.VarChar, "MaDaiLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamDK", SqlDbType.SmallInt, "NamDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Huongdan_PL", SqlDbType.NVarChar, "Huongdan_PL", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLy", SqlDbType.VarChar, "MaDaiLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamDK", SqlDbType.SmallInt, "NamDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Huongdan_PL", SqlDbType.NVarChar, "Huongdan_PL", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_CX_HangDuaVaoDangKy VALUES(@SoTiepNhan, @NgayTiepNhan, @MaHaiQuan, @MaDoanhNghiep, @MaDaiLy, @TrangThaiXuLy, @GUIDSTR, @DeXuatKhac, @LyDoSua, @ActionStatus, @GuidReference, @NamDK, @HUONGDAN, @PhanLuong, @Huongdan_PL)";
            string update = "UPDATE t_KDT_CX_HangDuaVaoDangKy SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, MaDaiLy = @MaDaiLy, TrangThaiXuLy = @TrangThaiXuLy, GUIDSTR = @GUIDSTR, DeXuatKhac = @DeXuatKhac, LyDoSua = @LyDoSua, ActionStatus = @ActionStatus, GuidReference = @GuidReference, NamDK = @NamDK, HUONGDAN = @HUONGDAN, PhanLuong = @PhanLuong, Huongdan_PL = @Huongdan_PL WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_CX_HangDuaVaoDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLy", SqlDbType.VarChar, "MaDaiLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamDK", SqlDbType.SmallInt, "NamDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Huongdan_PL", SqlDbType.NVarChar, "Huongdan_PL", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLy", SqlDbType.VarChar, "MaDaiLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamDK", SqlDbType.SmallInt, "NamDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Huongdan_PL", SqlDbType.NVarChar, "Huongdan_PL", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HangDuaVaoDangKy Load(long id)
		{
			const string spName = "[dbo].[p_KDT_CX_HangDuaVaoDangKy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<HangDuaVaoDangKy> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<HangDuaVaoDangKy> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<HangDuaVaoDangKy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_CX_HangDuaVaoDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_CX_HangDuaVaoDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_CX_HangDuaVaoDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_CX_HangDuaVaoDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHangDuaVaoDangKy(long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, string maDoanhNghiep, string maDaiLy, int trangThaiXuLy, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL)
		{
			HangDuaVaoDangKy entity = new HangDuaVaoDangKy();	
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaDaiLy = maDaiLy;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_CX_HangDuaVaoDangKy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HangDuaVaoDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangDuaVaoDangKy item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHangDuaVaoDangKy(long id, long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, string maDoanhNghiep, string maDaiLy, int trangThaiXuLy, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL)
		{
			HangDuaVaoDangKy entity = new HangDuaVaoDangKy();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaDaiLy = maDaiLy;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_CX_HangDuaVaoDangKy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HangDuaVaoDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangDuaVaoDangKy item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHangDuaVaoDangKy(long id, long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, string maDoanhNghiep, string maDaiLy, int trangThaiXuLy, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL)
		{
			HangDuaVaoDangKy entity = new HangDuaVaoDangKy();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaDaiLy = maDaiLy;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_CX_HangDuaVaoDangKy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HangDuaVaoDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangDuaVaoDangKy item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHangDuaVaoDangKy(long id)
		{
			HangDuaVaoDangKy entity = new HangDuaVaoDangKy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_CX_HangDuaVaoDangKy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_CX_HangDuaVaoDangKy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HangDuaVaoDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangDuaVaoDangKy item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}