GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO

DELETE FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521'

GO
--------------------------------------BẢNG MÃ MIỄN THUẾ NHẬP KHẨU BỔ SUNG DÙNG TRONG VNACCS----------------------------------------------
BEGIN
   IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='BM')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'BM' ,N'Hàng hóa được miễn thuế tự vệ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='BM' ,Name_VN = N'Hàng hóa được miễn thuế tự vệ' WHERE ReferenceDB='A521' AND Code='BM'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='GM')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'GM' ,N'Hàng hóa được miễn thuế chống bán phá giá')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='GM' ,Name_VN = N'Hàng hóa được miễn thuế chống bán phá giá' WHERE ReferenceDB='A521' AND Code='GM'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='CM')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'CM' ,N'Hàng hóa được miễn thuế chống trợ cấp')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='CM' ,Name_VN = N'Hàng hóa được miễn thuế chống trợ cấp' WHERE ReferenceDB='A521' AND Code='CM'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='PM')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'PM' ,N'Hàng hóa được miễn thuế chống phân biệt đối xử')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='PM' ,Name_VN = N'Hàng hóa được miễn thuế chống phân biệt đối xử' WHERE ReferenceDB='A521' AND Code='PM'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='DM')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'DM' ,N'Mã dự trữ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='DM' ,Name_VN = N'Mã dự trữ' WHERE ReferenceDB='A521' AND Code='DM'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='EM')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'EM' ,N'Mã dự trữ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='EM' ,Name_VN = N'Mã dự trữ' WHERE ReferenceDB='A521' AND Code='EM'
END
 
END
--------------------------------------BẢNG MÃ ĐỐI TƯỢNG KHÔNG CHỊU THUẾ NHẬP KHẨU BỔ SUNG DÙNG TRONG VNACCS  ----------------------------------------------
BEGIN
    IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='BK')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'BK' ,N'Hàng hóa thuộc đối tượng không chịu thuế tự vệ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='BK' ,Name_VN = N'Hàng hóa thuộc đối tượng không chịu thuế tự vệ' WHERE ReferenceDB='A521' AND Code='BK'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='GK')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'GK' ,N'Hàng hóa thuộc đối tượng không chịu thuế chống bán phá giá')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='GK' ,Name_VN = N'Hàng hóa thuộc đối tượng không chịu thuế chống bán phá giá' WHERE ReferenceDB='A521' AND Code='GK'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='CK')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'CK' ,N'Hàng hóa thuộc đối tượng không chịu thuế chống trợ cấp')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='CK' ,Name_VN = N'Hàng hóa thuộc đối tượng không chịu thuế chống trợ cấp' WHERE ReferenceDB='A521' AND Code='CK'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='PK')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'PK' ,N'Hàng hóa thuộc đối tượng không chịu thuế chống phân biệt đối xử')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='PK' ,Name_VN = N'Hàng hóa thuộc đối tượng không chịu thuế chống phân biệt đối xử' WHERE ReferenceDB='A521' AND Code='PK'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='DK')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'DK' ,N'Mã dự trữ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='DK' ,Name_VN = N'Mã dự trữ' WHERE ReferenceDB='A521' AND Code='DK'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='EK')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'EK' ,N'Mã dự trữ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='EK' ,Name_VN = N'Mã dự trữ' WHERE ReferenceDB='A521' AND Code='EK'
END

END
--------------------------------------BẢNG MÃ ĐỐI TƯỢNG KHÔNG CHỊU THUẾ TIÊU THỤ ĐẶC BIỆT DÙNG TRONG VNACCS ----------------------------------------------
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK010')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK010' ,N'Hàng viện trợ nhân đạo, viện trợ không hoàn lại')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK010' ,Name_VN = N'Hàng viện trợ nhân đạo, viện trợ không hoàn lại' WHERE ReferenceDB='A521' AND Code='TK010'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK020')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK020' ,N'Hàng quà biếu, tặng')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK020' ,Name_VN = N'Hàng quà biếu, tặng' WHERE ReferenceDB='A521' AND Code='TK020'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK030')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK030' ,N'Hàng quá cảnh, mượn đường, chuyển khẩu')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK030' ,Name_VN = N'Hàng quá cảnh, mượn đường, chuyển khẩu' WHERE ReferenceDB='A521' AND Code='TK030'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK040')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK040' ,N'Hàng tạm nhập tái xuất, tạm xuất tái nhập')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK040' ,Name_VN = N'Hàng tạm nhập tái xuất, tạm xuất tái nhập' WHERE ReferenceDB='A521' AND Code='TK040'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK050')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK050' ,N'Hàng theo tiêu chuẩn miễn trừ ngoại giao')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK050' ,Name_VN = N'Hàng theo tiêu chuẩn miễn trừ ngoại giao' WHERE ReferenceDB='A521' AND Code='TK050'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK060')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK060' ,N'Hàng NK để bán tại cửa hàng miễn thuế')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK060' ,Name_VN = N'Hàng NK để bán tại cửa hàng miễn thuế' WHERE ReferenceDB='A521' AND Code='TK060'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK070')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK070' ,N'Hàng đưa vào khu phi thuế quan')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK070' ,Name_VN = N'Hàng đưa vào khu phi thuế quan' WHERE ReferenceDB='A521' AND Code='TK070'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK080')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK080' ,N'Tàu bay, du thuyền ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK080' ,Name_VN = N'Tàu bay, du thuyền ' WHERE ReferenceDB='A521' AND Code='TK080'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK090')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK090' ,N'Xe ô tô ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK090' ,Name_VN = N'Xe ô tô ' WHERE ReferenceDB='A521' AND Code='TK090'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK100')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK100' ,N'Nap-ta, condensate, chế phẩm tái hợp... ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK100' ,Name_VN = N'Nap-ta, condensate, chế phẩm tái hợp... ' WHERE ReferenceDB='A521' AND Code='TK100'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK110')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK110' ,N'Điều hòa nhiệt độ không chịu thuế TTĐB')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK110' ,Name_VN = N'Điều hòa nhiệt độ không chịu thuế TTĐB' WHERE ReferenceDB='A521' AND Code='TK110'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='TK300')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'TK300' ,N'Hàng hóa khác')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TK300' ,Name_VN = N'Hàng hóa khác' WHERE ReferenceDB='A521' AND Code='TK300'
END
    
END
--------------------------------------BẢNG MÃ ĐỐI TƯỢNG KHÔNG CHỊU THUẾ GIÁ TRỊ GIA TĂNG DÙNG TRONG VNACCS ----------------------------------------------
BEGIN
   IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK010')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK010' ,N'SP trồng trọt, chăn nuôi, thủy sản nuôi trồng, đánh bắt')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK010' ,Name_VN = N'SP trồng trọt, chăn nuôi, thủy sản nuôi trồng, đánh bắt' WHERE ReferenceDB='A521' AND Code='VK010'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK020')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK020' ,N'Giống vật nuôi, giống cây trồng')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK020' ,Name_VN = N'Giống vật nuôi, giống cây trồng' WHERE ReferenceDB='A521' AND Code='VK020'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK030')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK030' ,N'Muối ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK030' ,Name_VN = N'Muối ' WHERE ReferenceDB='A521' AND Code='VK030'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK040')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK040' ,N'Báo, tạp chí, bản tin, sách, tranh, ảnh, áp phích')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK040' ,Name_VN = N'Báo, tạp chí, bản tin, sách, tranh, ảnh, áp phích' WHERE ReferenceDB='A521' AND Code='VK040'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK050')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK050' ,N'Hàng NK phục vụ nghiên cứu khoa học, phát triển công nghệ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK050' ,Name_VN = N'Hàng NK phục vụ nghiên cứu khoa học, phát triển công nghệ' WHERE ReferenceDB='A521' AND Code='VK050'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK060')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK060' ,N'Hàng NK phục vụ tìm kiếm, thăm dò, phát triển mỏ dầu, khí đốt')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK060' ,Name_VN = N'Hàng NK phục vụ tìm kiếm, thăm dò, phát triển mỏ dầu, khí đốt' WHERE ReferenceDB='A521' AND Code='VK060'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK070')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK070' ,N'Tàu bay, dàn khoan, tàu thủy')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK070' ,Name_VN = N'Tàu bay, dàn khoan, tàu thủy' WHERE ReferenceDB='A521' AND Code='VK070'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK080')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK080' ,N'Vũ khí, khí tài chuyên dùng phục vụ quốc phòng an ninh')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK080' ,Name_VN = N'Vũ khí, khí tài chuyên dùng phục vụ quốc phòng an ninh' WHERE ReferenceDB='A521' AND Code='VK080'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK091')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK091' ,N'Hàng viện trợ nhân đạo, viện trợ không hoàn lại')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK091' ,Name_VN = N'Hàng viện trợ nhân đạo, viện trợ không hoàn lại' WHERE ReferenceDB='A521' AND Code='VK091'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK092')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK092' ,N'Quà biếu, quà tặng')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK092' ,Name_VN = N'Quà biếu, quà tặng' WHERE ReferenceDB='A521' AND Code='VK092'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK093')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK093' ,N'Đồ dùng theo tiêu chuẩn miễn trừ ngoại giao')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK093' ,Name_VN = N'Đồ dùng theo tiêu chuẩn miễn trừ ngoại giao' WHERE ReferenceDB='A521' AND Code='VK093'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK100')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK100' ,N'Hàng chuyển khẩu, quá cảnh')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK100' ,Name_VN = N'Hàng chuyển khẩu, quá cảnh' WHERE ReferenceDB='A521' AND Code='VK100'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK110')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK110' ,N'Hàng tạm nhập tái xuất, tạm xuất tái nhập')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK110' ,Name_VN = N'Hàng tạm nhập tái xuất, tạm xuất tái nhập' WHERE ReferenceDB='A521' AND Code='VK110'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK120')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK120' ,N'Nguyên liệu NK để SX, gia công hàng XK')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK120' ,Name_VN = N'Nguyên liệu NK để SX, gia công hàng XK' WHERE ReferenceDB='A521' AND Code='VK120'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK130')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK130' ,N'Hàng mua bán giữa nước ngoài với khu PTQ, giữa các khu PTQ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK130' ,Name_VN = N'Hàng mua bán giữa nước ngoài với khu PTQ, giữa các khu PTQ' WHERE ReferenceDB='A521' AND Code='VK130'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK140')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK140' ,N'Vàng ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK140' ,Name_VN = N'Vàng ' WHERE ReferenceDB='A521' AND Code='VK140'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK150')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK150' ,N'SP nhân tạo thay thế bộ phận cơ thể người bệnh')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK150' ,Name_VN = N'SP nhân tạo thay thế bộ phận cơ thể người bệnh' WHERE ReferenceDB='A521' AND Code='VK150'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK160')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK160' ,N'Dụng cụ chuyên dùng cho người tàn tật')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK160' ,Name_VN = N'Dụng cụ chuyên dùng cho người tàn tật' WHERE ReferenceDB='A521' AND Code='VK160'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK170')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK170' ,N'Hàng gửi qua dịch vụ chuyển phát nhanh')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK170' ,Name_VN = N'Hàng gửi qua dịch vụ chuyển phát nhanh' WHERE ReferenceDB='A521' AND Code='VK170'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK180')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK180' ,N'Hàng hóa bán tại cửa hàng miễn thuế')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK180' ,Name_VN = N'Hàng hóa bán tại cửa hàng miễn thuế' WHERE ReferenceDB='A521' AND Code='VK180'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK190')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK190' ,N'Hàng nông sản do VN hỗ trợ đầu tư, trồng tại Campuchia')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK190' ,Name_VN = N'Hàng nông sản do VN hỗ trợ đầu tư, trồng tại Campuchia' WHERE ReferenceDB='A521' AND Code='VK190'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK200')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK200' ,N'Phân bón')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK200' ,Name_VN = N'Phân bón' WHERE ReferenceDB='A521' AND Code='VK200'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK210')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK210' ,N'Máy móc, thiết bị chuyên dùng phục vụ cho sản xuất nông nghiệp')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK210' ,Name_VN = N'Máy móc, thiết bị chuyên dùng phục vụ cho sản xuất nông nghiệp' WHERE ReferenceDB='A521' AND Code='VK210'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK220')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK220' ,N'Thức ăn gia súc, gia cầm và thức ăn cho vật nuôi khác')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK220' ,Name_VN = N'Thức ăn gia súc, gia cầm và thức ăn cho vật nuôi khác' WHERE ReferenceDB='A521' AND Code='VK220'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK230')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK230' ,N'Tàu đánh bắt xa bờ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK230' ,Name_VN = N'Tàu đánh bắt xa bờ' WHERE ReferenceDB='A521' AND Code='VK230'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='VK900')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'VK900' ,N'Hàng hóa khác')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VK900' ,Name_VN = N'Hàng hóa khác' WHERE ReferenceDB='A521' AND Code='VK900'
END
 
END
--------------------------------------BẢNG MÃ ĐỐI TƯỢNG KHÔNG CHỊU THUẾ BẢO VỆ MÔI TRƯỜNG DÙNG TRONG VNACCS ----------------------------------------------
BEGIN
    IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='MK010')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'MK010' ,N'Hàng vận chuyển quá cảnh, chuyển khẩu')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MK010' ,Name_VN = N'Hàng vận chuyển quá cảnh, chuyển khẩu' WHERE ReferenceDB='A521' AND Code='MK010'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='MK020')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'MK020' ,N'Hàng tạm nhập tái xuất')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MK020' ,Name_VN = N'Hàng tạm nhập tái xuất' WHERE ReferenceDB='A521' AND Code='MK020'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='MK030')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'MK030' ,N'Bao bì đóng gói sẵn hàng hóa')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MK030' ,Name_VN = N'Bao bì đóng gói sẵn hàng hóa' WHERE ReferenceDB='A521' AND Code='MK030'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='MK040')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'MK040' ,N'Túi ni lông thân thiện với môi trường')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MK040' ,Name_VN = N'Túi ni lông thân thiện với môi trường' WHERE ReferenceDB='A521' AND Code='MK040'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A521' AND Code='MK100')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A521' ,'MK100' ,N'Hàng hóa khác')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MK100' ,Name_VN = N'Hàng hóa khác' WHERE ReferenceDB='A521' AND Code='MK100'
END

END
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '33.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('33.0',GETDATE(), N'BẢNG MÃ MIỄN GIẢM KHÔNG CHỊU THUẾ VÀ THU KHÁC DÙNG TRONG VNACCS')
END