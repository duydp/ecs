using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
    public partial class SanPham
    {
        public int DeleteByNhomSanPhamTransaction(SqlTransaction transaction, string manhomsp)
        {
            string spName = "delete from t_KDT_GC_SanPham where NhomSanPham_ID=@ID and HopDong_ID=@HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, manhomsp);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this.HopDong_ID);

            if (transaction != null)
                db.ExecuteNonQuery(dbCommand, transaction);
            else
                db.ExecuteNonQuery(dbCommand);

            spName = "delete from t_GC_SanPham where NhomSanPham_ID=@ID and HopDong_ID=@HopDong_ID";
            dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            // XOA BEN gc
            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, manhomsp);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this.HopDong_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public static bool CheckExitSPinDinhMuc(string MaSanPham, string MaHaiQuan, string MaDoanhNghiep, long idHopDong)
        {
            string sql = " SELECT  MaSanPham FROM t_KDT_GC_DinhMuc inner join t_KDT_GC_DinhMucDangKy " +
                         " on t_KDT_GC_DinhMuc.Master_ID =t_KDT_GC_DinhMucDangKy.ID " +
                         " where  MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan and MaSanPham=@MaSanPham and ID_HopDong=" + idHopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            return db.ExecuteScalar(dbCommand) != null;

        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_SanPham_InsertUpdate_ByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@NhomSanPham_ID", SqlDbType.VarChar, this._NhomSanPham_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int DeleteBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_SanPham_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        public int InsertUpdateTransactionTQDT(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_SanPham_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@NhomSanPham_ID", SqlDbType.VarChar, this._NhomSanPham_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public SanPhamCollection SelectCollectionBy_HopDong_ID_ByKTX(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 28/06/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_SanPham_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            SanPhamCollection collection = new SanPhamCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                SanPham entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) entity.NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }


    }
}
