﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.SXXK;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.KDT.SXXK
{
    public partial class SaoChepDinhMucSXXKForm : Company.Interface.BaseForm
    {
        public Company.BLL.SXXK.DinhMuc dm = new Company.BLL.SXXK.DinhMuc();
        public Company.BLL.SXXK.DinhMucCollection DMCollectionCopy = new Company.BLL.SXXK.DinhMucCollection();
        public Company.BLL.SXXK.DinhMucCollection DMCollectionRemove = new Company.BLL.SXXK.DinhMucCollection();
        public KDT_LenhSanXuat_SP SP = new KDT_LenhSanXuat_SP();

        public SaoChepDinhMucSXXKForm()
        {
            InitializeComponent();
        }

        private void SaoChepDinhMucSXXKForm_Load(object sender, EventArgs e)
        {
            this.Text = "SAO CHÉP ĐỊNH MỨC CHO MÃ SẢN PHẨM : " + SP.MaSanPham.ToString();
            BindData();
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = dm.SelectCollectionDynamic(" MaSanPham NOT IN ('" + SP.MaSanPham.ToString() + "') AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAdd = true;
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    isAdd = true;
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    Company.BLL.SXXK.DinhMuc dinhmuc = (Company.BLL.SXXK.DinhMuc)item.DataRow;
                    //KIỂM TRA CHỌN TRÙNG NHIỀU MÃ NPL KHI CHỌN
                    foreach (Company.BLL.SXXK.DinhMuc it in DMCollectionCopy)
                    {
                        if (it.MaNguyenPhuLieu == dinhmuc.MaNguyenPhuLieu)
                        {
                            if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC CHỌN TRÊN LƯỚI .\nBẠN CÓ MUỐN SAO CHÉP THAY THẾ BẰNG ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                            {
                                it.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                it.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                                isAdd = false;
                            }
                            else if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC CHỌN TRÊN LƯỚI .\nBẠN CÓ GHỘP ĐỊNH MỨC CỦA MÃ NPL NÀY KHÔNG ?", true) == "Yes")
                            {
                                it.DinhMucSuDung = it.DinhMucSuDung + dinhmuc.DinhMucSuDung;
                                it.TyLeHaoHut = it.TyLeHaoHut + dinhmuc.TyLeHaoHut;
                                isAdd = false;
                            }
                            else
                            {
                                isAdd = false;
                            }
                        }
                    }
                    if (isAdd)
                        DMCollectionCopy.Add(dinhmuc);
                }
                Company.BLL.SXXK.DinhMucCollection DMSPCollection = new Company.BLL.SXXK.DinhMucCollection();
                DMSPCollection = dm.SelectCollectionDynamic(" MaSanPham ='" + SP.MaSanPham.ToString() + "' AND LenhSanXuat_ID =" + SP.LenhSanXuat_ID + "", "");
                if (DMCollectionCopy.Count > 0)
                {
                    //KIỂM TRA TRÙNG MÃ NPL ĐÃ NHẬP ĐỊNH MỨC TRƯỚC ĐÓ
                    foreach (Company.BLL.SXXK.DinhMuc ite in DMSPCollection)
                    {
                        foreach (Company.BLL.SXXK.DinhMuc dinhmuc in DMCollectionCopy)
                        {
                            if (ite.MaSanPHam == SP.MaSanPham && ite.MaNguyenPhuLieu == dinhmuc.MaNguyenPhuLieu)
                            {
                                if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC .\nBẠN CÓ MUỐN SAO CHÉP THAY THẾ BẰNG ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                                {
                                    ite.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                    ite.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                                    DMCollectionRemove.Add(dinhmuc);
                                }
                                else if ((ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC .\nBẠN CÓ MUỐN GHỘP ĐỊNH MỨC CỦA MÃ NPL KHÔNG ?", true) == "Yes"))
                                {
                                    ite.DinhMucSuDung = ite.DinhMucSuDung + dinhmuc.DinhMucSuDung;
                                    ite.TyLeHaoHut = ite.TyLeHaoHut + dinhmuc.TyLeHaoHut;
                                    DMCollectionRemove.Add(dinhmuc);
                                }
                            }
                        }
                    }
                    foreach (Company.BLL.SXXK.DinhMuc item in DMCollectionRemove)
                    {
                        DMCollectionCopy.Remove(item);
                    }
                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                KDT_LenhSanXuat lenhhXS = new KDT_LenhSanXuat();
                lenhhXS = KDT_LenhSanXuat.Load(Convert.ToInt64(e.Row.Cells["LenhSanXuat_ID"].Value));
                if (lenhhXS != null)
                    e.Row.Cells["LenhSanXuat_ID"].Text = lenhhXS.SoLenhSanXuat;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
