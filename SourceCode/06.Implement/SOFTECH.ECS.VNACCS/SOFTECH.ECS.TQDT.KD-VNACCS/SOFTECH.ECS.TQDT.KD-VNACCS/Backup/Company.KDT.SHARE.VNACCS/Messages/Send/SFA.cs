﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class SFA : BasicVNACC
    {
        public List<SFA_HANGHOA> ListHangHoa = new List<SFA_HANGHOA>();

        public StringBuilder BuilEdiMessagesSFA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<SFA>(StrBuild, true, GlobalVNACC.PathConfig, "SFA");
            foreach (SFA_HANGHOA item in ListHangHoa)
            {
                item.BuildEdiMessages<SFA_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "SFA_HANGHOA");
            }
            return str;
        }

    }
}
