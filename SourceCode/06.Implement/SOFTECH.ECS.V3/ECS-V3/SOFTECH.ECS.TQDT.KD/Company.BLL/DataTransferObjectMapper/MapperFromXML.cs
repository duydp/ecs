﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.Components;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.QuanLyChungTu;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
using GlobalsShare = Company.KDT.SHARE.Components.Globals;

namespace Company.KD.BLL.DataTransferObjectMapper
{
    public class MapperFromXML
    {
        static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        static string sfmtDate = "yyyy-MM-dd";
        /// <summary>
        /// Chuyển dữ liệu từ KD_ToKhaiMauDich DTO sang ToKhaiMauDich BO
        /// </summary>
        /// <param name="tokhaimaudich">ToKhaiMauDich</param>
        /// <returns>ToKhai</returns>
        #region TransferOb tờ KD
        public static ToKhaiMauDich ToDataTransferObjectFromXML(ToKhai tokhai)
        {

            bool isToKhaiNhap = tokhai.NatureOfTransaction.Substring(0, 1).Equals("N");
            //bool isToKhaiSua = tkmd.SoToKhai != 0 && tkmd.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            //if (isToKhaiSua)
                //tkmd.SoTiepNhan = KhaiBaoSua.SoTNSua(tkmd.SoTiepNhan, tkmd.SoToKhai, tkmd.NamDK, tkmd.MaLoaiHinh,
                        //tkmd.MaHaiQuan, tkmd.MaDoanhNghiep);
            #region Header
            ToKhaiMauDich tkmd = new ToKhaiMauDich()
            {
                //Issuer = isToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT,
                GUIDSTR = tokhai.Reference,
                NgayTiepNhan = Convert.ToDateTime(tokhai.Issue),
                //Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                //IssueLocation = string.Empty,
                //Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                // Số tiếp nhận
                SoTiepNhan = (long)Convert.ToInt64(tokhai.CustomsReference),
                //Ngày dang ký chứng thư
                NgayDangKy = Convert.ToDateTime(tokhai.Acceptance),
                // Ðon vị hải quan khai báo
                MaHaiQuan = tokhai.DeclarationOffice,
                // Số hàng
                //GoodsItem = Helpers.FormatNumeric(tkmd.SoHang),
                SoHang = (short)Convert.ToInt32(tokhai.GoodsItem),
                //LoadingList = Helpers.FormatNumeric(tkmd.SoLuongPLTK),
                SoLuongPLTK = (short)Convert.ToInt32(tokhai.LoadingList),
                // Khối luợng và khối luợng tịnh
                //TotalGrossMass = Helpers.FormatNumeric(tkmd.TrongLuong, 3),
                TrongLuong = Convert.ToDecimal(tokhai.TotalGrossMass),
                //TotalNetGrossMass = Helpers.FormatNumeric(tkmd.TrongLuongNet, 3),
                TrongLuongNet = Convert.ToDouble(tokhai.TotalNetGrossMass),
                // Mã Loại Hình
                  MaLoaiHinh = tokhai.NatureOfTransaction,
                // Phuong thức thanh toán
                 PTTT_ID = tokhai.PaymentMethod,



                #region Install element
                //Agents = new List<Agent>(),

                //Nguyên tệ
                //CurrencyExchange = new CurrencyExchange { CurrencyType = tkmd.NguyenTe_ID, Rate = Helpers.FormatNumeric(tkmd.TyGiaTinhThue, 3) },
                NguyenTe_ID = tokhai.CurrencyExchange.CurrencyType,
                TyGiaTinhThue = Convert.ToDecimal(tokhai.CurrencyExchange.Rate),
                //Số kiện
                //DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tkmd.SoKien) },
                SoKien = Convert.ToDecimal(tokhai.DeclarationPackaging.Quantity),
                //Neu có day du 1 trong 3 chứng từ :  Giấy phép, Hợp đồng, Vận đơn 
                //AdditionalDocuments = new List<AdditionalDocument>(),

                // Hóa Ðon thuong mại
                //Invoice = new Invoice { Issue = tkmd.NgayHoaDonThuongMai.Year <= 1900 ? string.Empty : tkmd.NgayHoaDonThuongMai.ToString(sfmtDate), Reference = tkmd.SoHoaDonThuongMai, Type = AdditionalDocumentType.HOA_DON_THUONG_MAI },
                NgayHoaDonThuongMai = String.IsNullOrEmpty(tokhai.Invoice.Issue) ? new DateTime(1900, 01, 01) : Convert.ToDateTime(tokhai.Invoice.Issue),
                SoHoaDonThuongMai = tokhai.Invoice.Reference,

                // Doanh Nghiệp Xuất khẩu
                //Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = "." } :
                //new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() },

                TenDonViDoiTac = isToKhaiNhap ? tokhai.Exporter.Name : tokhai.Importer.Name,

                //Doanh nghiệp nhập khẩu
                //Importer = !isToKhaiNhap ? new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() } :
                //new NameBase { Name = tkmd.TenDonViDoiTac, Identity = tkmd.MaDoanhNghiep.Trim() },

                // Nguười gởi (Tên chủ hàng)
                //RepresentativePerson = new RepresentativePerson { ContactFunction = ".", Name = tkmd.TenChuHang },
                TenChuHang = tokhai.RepresentativePerson.Name,

                // Đề xuất khác | Mã loại thông tin - Ghi chú khác
                //AdditionalInformations = new List<AdditionalInformation>(),
                
                // GoodsShipmet Thông tin hàng hóa
                //GoodsShipment = new GoodsShipment(),
                /*
                License = new List<License>(),
                ContractDocument = new List<ContractDocument>(),
                CommercialInvoices = new List<CommercialInvoice>(),
                CertificateOfOrigins = new List<CertificateOfOrigin>(),
                CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
                AttachDocumentItem = new List<AttachDocumentItem>(),
                AdditionalDocumentEx = new List<AdditionalDocument>(),*/
                #endregion Nrr

            };
            //tokhai.AdditionalInformations.Add(
            //    new AdditionalInformation
            //    {
            //        Statement = "001",
            //        Content = new Content() { Text = tkmd.DeXuatKhac }
            //    }
            //    );
            tkmd.DeXuatKhac = tokhai.AdditionalInformations[0].Content.Text;
            tkmd.LyDoSua = tokhai.AdditionalInformations.Count>1? tokhai.AdditionalInformations[1].Content.Text:string.Empty;
            //if (isToKhaiSua)
            //    tokhai.AdditionalInformations.Add(new AdditionalInformation()
            //    {
            //        Statement = "005",
            //        Content = new Content() { Text = tkmd.LyDoSua }
            //    });


            #endregion Header

            #region Agents Đại lý khai
            //tokhai.Agents = AgentsFrom(tkmd);
            AgentsFromXML(tokhai.Agents, ref tkmd);
            #endregion

            #region AdditionalDocument Hợp đồng - Giấy phép - Vận đơn ngoài tờ khai
            
            if(tokhai.AdditionalDocuments != null)
                foreach (AdditionalDocument item in tokhai.AdditionalDocuments)
                {
                    switch (item.Type)
                    {
                        case AdditionalDocumentType.HOP_DONG:
                            tkmd.NgayHopDong = Convert.ToDateTime(item.Issue);
                            tkmd.SoHopDong = item.Reference;
                            tkmd.NgayHetHanHopDong = Convert.ToDateTime(item.Expire);
                            break;
                        case AdditionalDocumentType.BILL_OF_LADING_ORIGIN:
                            tkmd.NgayVanDon = Convert.ToDateTime(item.Issue);
                            tkmd.SoVanDon = item.Reference;
                            break;
                        default:
                            tkmd.NgayGiayPhep = Convert.ToDateTime(item.Issue);
                            tkmd.SoGiayPhep = item.Reference;
                            tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(item.Expire);
                            break;
                    }
                }
            #endregion

            #region GoodsShipment thông tin về hàng hóa
            tkmd.NuocNK_ID = isToKhaiNhap ? string.Empty : tokhai.GoodsShipment.ImportationCountry;
            tkmd.NuocXK_ID = isToKhaiNhap ? tokhai.GoodsShipment.ExportationCountry : string.Empty;
            tkmd.CuaKhau_ID = isToKhaiNhap ? tokhai.GoodsShipment.EntryCustomsOffice.Code : tokhai.GoodsShipment.ExitCustomsOffice.Code;
            tkmd.DKGH_ID = tokhai.GoodsShipment.TradeTerm.Condition;
            #endregion GoodsShipment
            tkmd.InsertUpdateFullAll();
            #region CustomGoodsItem Danh sách hàng khai báo
            tkmd.HMDCollection = new List<HangMauDich>();
            tkmd.TKTGCollection = new ToKhaiTriGiaCollection();
            tkmd.TKTGPP23Collection = new ToKhaiTriGiaPP23Collection();
            int soluonghang = 0;
            soluonghang = tokhai.GoodsShipment.CustomsGoodsItems.Count;
            tkmd.PhiVanChuyen = Convert.ToDecimal(tokhai.GoodsShipment.CustomsGoodsItems[0].CustomsValuation.FreightCharge) * soluonghang;
            foreach (CustomsGoodsItem item in tokhai.GoodsShipment.CustomsGoodsItems)
            {
                HangMauDich hmd = new HangMauDich();
                hmd.TriGiaKB = Convert.ToDouble(item.CustomsValue.Trim());
                hmd.SoThuTuHang = Convert.ToInt16(item.Sequence.Trim());
                hmd.TriGiaTT = Convert.ToDouble(item.StatisticalValue.Trim());
                hmd.DonGiaKB = Convert.ToDouble(item.UnitPrice.Trim());
                hmd.DonGiaTT = Convert.ToDouble(item.StatisticalUnitPrice.Trim());
                hmd.TenHangSX = item.Manufacturer.Name.Trim();
                hmd.MaHangSX = item.Manufacturer.Identity.Trim();
                hmd.NuocXX_ID = item.Origin.OriginCountry.Trim();
                hmd.SoLuong = Convert.ToDecimal(item.GoodsMeasure.Quantity.Trim());
                hmd.DVT_ID = item.GoodsMeasure.MeasureUnit.Trim();
                hmd.TenHang = item.Commodity.Description.Trim();
                hmd.MaPhu = item.Commodity.Identification.Trim();
                hmd.MaHS = item.Commodity.TariffClassification.Trim();
                hmd.MaHSMoRong = item.Commodity.TariffClassificationExtension;
                hmd.NhanHieu = item.Commodity.Brand.Trim();
                hmd.QuyCachPhamChat = item.Commodity.Grade.Trim();
                hmd.ThanhPhan = item.Commodity.Ingredients.Trim();
                hmd.Model = item.Commodity.ModelNumber.Trim();
                #region Thuế
                foreach (DutyTaxFee tax in item.Commodity.DutyTaxFee)
                {
                    switch (tax.Type)
                    {
                        case DutyTaxFeeType.THUE_XNK:
                            hmd.ThueXNK = Convert.ToDouble(tax.AdValoremTaxBase);
                            hmd.ThueSuatXNK = Convert.ToDouble(tax.Tax);
                            break;
                        case DutyTaxFeeType.THUE_VAT:
                            hmd.ThueGTGT = Convert.ToDouble(tax.AdValoremTaxBase);
                            hmd.ThueSuatGTGT = Convert.ToDouble(tax.Tax);
                            break;
                        case DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET:
                            hmd.ThueTTDB = Convert.ToDouble(tax.AdValoremTaxBase);
                            hmd.ThueSuatTTDB = Convert.ToDouble(tax.Tax);
                            break;
                        case DutyTaxFeeType.THUE_KHAC:
                            hmd.TyLeThuKhac = Convert.ToDouble(tax.Tax);
                            break;
                        case DutyTaxFeeType.THUE_CHENH_LECH_GIA:
                            hmd.PhuThu = Convert.ToDouble(tax.AdValoremTaxBase);
                            break;
                    }
                }
                #endregion
                if (!string.IsNullOrEmpty(item.CustomsValuation.Method))
                {
                    #region Tờ khai trị giá PP 1

                    if (Convert.ToInt16(item.CustomsValuation.Method.Trim()) == 1 && item.AdditionalInformations != null)
                    {
                        #region hàng trị giá

                        HangTriGia hangtg = new HangTriGia();
                        hangtg.TenHang = item.Commodity.Description.Trim();
                        foreach (ValuationAdjustment htg in item.ValuationAdjustments)
                        {
                            switch (htg.Addition)
                            {
                                case ValuationAdjustmentAddition.Gia_hoa_don:
                                    hangtg.GiaTrenHoaDon = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Thanh_toan_gian_tiep:
                                    hangtg.KhoanThanhToanGianTiep = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Tien_tra_truoc:
                                    hangtg.TraTruoc = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Phi_hoa_hong:
                                    hangtg.HoaHong = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Phi_bao_bi:
                                    hangtg.ChiPhiBaoBi = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Phi_dong_goi:
                                    hangtg.ChiPhiDongGoi = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Khoan_tro_giup:
                                    hangtg.TroGiup = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Tro_giup_NVL:
                                    hangtg.NguyenLieu = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Tro_giup_NL:
                                    hangtg.VatLieu = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Tro_giup_cong_cu:
                                    hangtg.CongCu = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Tro_giup_thiet_ke:
                                    hangtg.ThietKe = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Tien_ban_quyen:
                                    hangtg.BanQuyen = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Tien_phai_tra:
                                    hangtg.TienTraSuDung = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Phi_van_tai:
                                    hangtg.ChiPhiVanChuyen = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Phi_bao_hiem:
                                    hangtg.PhiBaoHiem = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Phi_VT_BH_noi_dia:
                                    hangtg.ChiPhiNoiDia = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Phi_phat_sinh:
                                    hangtg.ChiPhiPhatSinh = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Tien_lai:
                                    hangtg.TienLai = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Thue_phi_le_phi:
                                    hangtg.TienThue = Convert.ToDouble(htg.Amount);
                                    break;
                                case ValuationAdjustmentAddition.Khoan_Giam_Gia:
                                    hangtg.GiamGia = Convert.ToDouble(htg.Amount);
                                    break;
                            }
                        }
                        #endregion
                        #region tờ khai trị giá

                        ToKhaiTriGia tktg = new ToKhaiTriGia();
                        foreach (AdditionalInformation trigia in item.AdditionalInformations)
                        {
                            switch (trigia.Statement)
                            {
                                case AdditionalInformationStatement.TO_SO:
                                    tktg.ToSo = (long)Convert.ToInt64(trigia.Content.Text.Trim());
                                    break;
                                case AdditionalInformationStatement.NGAY_XK:
                                    tktg.NgayXuatKhau = Convert.ToDateTime(trigia.Content.Text.Trim());
                                    break;
                                case AdditionalInformationStatement.QUYEN_SD:
                                    tktg.QuyenSuDung = Convert.ToInt16(trigia.Content.Text.Trim()) == 0 ? false : true;
                                    break;
                                case AdditionalInformationStatement.KHONG_XD:
                                    tktg.KhongXacDinh = Convert.ToInt16(trigia.Content.Text.Trim()) == 0 ? false : true;
                                    break;
                                case AdditionalInformationStatement.TRA_THEM:
                                    tktg.TraThem = Convert.ToInt16(trigia.Content.Text.Trim()) == 0 ? false : true;
                                    break;
                                case AdditionalInformationStatement.TIEN_TRA_16:
                                    tktg.TienTra = Convert.ToInt16(trigia.Content.Text.Trim()) == 0 ? false : true;
                                    break;
                                case AdditionalInformationStatement.CO_QHDB:
                                    tktg.CoQuanHeDacBiet = Convert.ToInt16(trigia.Content.Text.Trim()) == 0 ? false : true;
                                    break;
                                case AdditionalInformationStatement.KIEU_QHDB:
                                    tktg.KieuQuanHe = trigia.Content.Text.Trim();
                                    break;
                                case AdditionalInformationStatement.ANH_HUONG_QH:
                                    tktg.AnhHuongQuanHe = Convert.ToInt16(trigia.Content.Text.Trim()) == 0 ? false : true;
                                    break;

                            }
                        }
                        #endregion
                        foreach (ToKhaiTriGia tktg2 in tkmd.TKTGCollection)
                        {
                            if (tktg2.ToSo == tktg.ToSo && tktg2.NgayXuatKhau == tktg.NgayXuatKhau)
                            {
                                tktg2.HTGCollection.Add(hangtg);
                                tktg2.TKMD_ID = tkmd.ID;
                                tktg2.InsertUpdateFull();
                            }
                            else
                            {
                                tktg.HTGCollection.Add(hangtg);
                                tktg.TKMD_ID = tkmd.ID;
                                tktg.InsertUpdateFull();
                                tkmd.TKTGCollection.Add(tktg);
                            }

                        }
                    }
                    #endregion
                    #region Tờ khai trị giá PP2-3
                    if (Convert.ToInt16(item.CustomsValuation.Method.Trim()) > 1 && item.AdditionalInformations != null)
                    {
                        ToKhaiTriGiaPP23 tktgpp23 = new ToKhaiTriGiaPP23();
                        tktgpp23.TenHang = hmd.TenHang;
                        tktgpp23.STTHang = hmd.SoThuTuHang;
                        tktgpp23.MaToKhaiTriGia = Convert.ToInt16(item.CustomsValuation.Method.Trim());
                        foreach (AdditionalInformation trigia in item.AdditionalInformations)
                        {
                            switch (trigia.Statement.Trim().Substring(0, 1))
                            {
                                case AdditionalInformationStatement.LYDO_KAD_PP1:
                                    tktgpp23.LyDo = trigia.Content.Text;
                                    break;
                                case AdditionalInformationStatement.NGAY_XK23:
                                    tktgpp23.NgayXuat = Convert.ToDateTime(trigia.Content.Text.Trim());
                                    break;
                                case AdditionalInformationStatement.STTHANG_TT:
                                    tktgpp23.STTHang = Convert.ToInt16(trigia.Content.Text.Trim());
                                    break;
                                case AdditionalInformationStatement.SOTK_TT:
                                    tktgpp23.SoTKHangTT = (long)Convert.ToInt64(trigia.Content.Text.Trim());
                                    break;
                                case AdditionalInformationStatement.NGAY_NK_TT:
                                    tktgpp23.NgayDangKyHangTT = Convert.ToDateTime(trigia.Content.Text.Trim());
                                    break;
                                case AdditionalInformationStatement.MA_HQ_TT:
                                    tktgpp23.MaHaiQuanHangTT = trigia.Content.Text.Trim();
                                    break;
                                case AdditionalInformationStatement.NGAY_XK_TT:
                                    tktgpp23.NgayXuatTT = Convert.ToDateTime(trigia.Content.Text.Trim());
                                    break;
                                case AdditionalInformationStatement.GIAI_TRINH:
                                    tktgpp23.GiaiTrinh = trigia.Content.Text.Trim();
                                    break;
                                case AdditionalInformationStatement.Ma_LH:
                                    tktgpp23.MaLoaiHinhHangTT = trigia.Content.Text.Trim();
                                    break;
                                case AdditionalInformationStatement.HANG_TUONG_TU:
                                    tktgpp23.TenHangTT = trigia.Content.Text.Trim();
                                    break;
                            }
                        }
                        foreach (ValuationAdjustment trigia in item.ValuationAdjustments)
                        {
                            switch (trigia.Addition.Trim().Substring(0, 1))
                            {
                                case ValuationAdjustmentAddition.Tri_gia_hang_TT:
                                    tktgpp23.TriGiaNguyenTeHangTT = Convert.ToDouble(trigia.Amount.Trim());
                                    break;
                                case ValuationAdjustmentAddition.DCC_cap_do_TM:
                                    if (Convert.ToDouble(trigia.Amount.Trim()) > 0)
                                        tktgpp23.DieuChinhCongThuongMai = Convert.ToDouble(trigia.Amount.Trim());
                                    else
                                        tktgpp23.DieuChinhTruCapDoThuongMai = Convert.ToDouble(trigia.Amount.Trim());
                                    break;
                                case ValuationAdjustmentAddition.DCC_so_luong:
                                    if (Convert.ToDouble(trigia.Amount.Trim()) > 0)
                                        tktgpp23.DieuChinhCongSoLuong = Convert.ToDouble(trigia.Amount.Trim());
                                    else
                                        tktgpp23.DieuChinhTruSoLuong = Convert.ToDouble(trigia.Amount.Trim());
                                    break;
                                case ValuationAdjustmentAddition.DCC_khoan_khac:
                                    if (Convert.ToDouble(trigia.Amount.Trim()) > 0)
                                        tktgpp23.DieuChinhCongKhoanGiamGiaKhac = Convert.ToDouble(trigia.Amount.Trim());
                                    else
                                        tktgpp23.DieuChinhTruKhoanGiamGiaKhac = Convert.ToDouble(trigia.Amount.Trim());
                                    break;
                                case ValuationAdjustmentAddition.DCC_phi_van_tai:
                                    if (Convert.ToDouble(trigia.Amount.Trim()) > 0)
                                        tktgpp23.DieuChinhCongChiPhiVanTai = Convert.ToDouble(trigia.Amount.Trim());
                                    else
                                        tktgpp23.DieuChinhTruChiPhiVanTai = Convert.ToDouble(trigia.Amount.Trim());
                                    break;
                                case ValuationAdjustmentAddition.DCC_phi_bao_hiem:
                                    if (Convert.ToDouble(trigia.Amount.Trim()) > 0)
                                        tktgpp23.DieuChinhCongChiPhiBaoHiem = Convert.ToDouble(trigia.Amount.Trim());
                                    else
                                        tktgpp23.DieuChinhTruChiPhiBaoHiem = Convert.ToDouble(trigia.Amount.Trim());
                                    break;
                            }
                        }
                        tkmd.TKTGPP23Collection.Add(tktgpp23);
                        tktgpp23.TKMD_ID = tkmd.ID;
                        tktgpp23.InsertUpdate();
                    }
                    #endregion
                }
                tkmd.HMDCollection.Add(hmd);
            }
#endregion
            tkmd.InsertUpdateFullAll();
            //////////////////////////////////////////////////////////////////////////
            #region CO + Giấy phép + hóa đơn + hợp đồng + đề nghị chuyển cửa khẩu + chứng từ kèm
            if (tokhai.CertificateOfOrigins != null && tokhai.CertificateOfOrigins.Count>0)
            {
                foreach (CertificateOfOrigin Co in tokhai.CertificateOfOrigins)
                {
                    CertificateOfOriginFromXML(Co, tkmd);
                }
            }
            if (tokhai.License != null && tokhai.License.Count > 0)
            {
                foreach (License lic in tokhai.License)
                {
                    LicenseFromXML(lic, tkmd);
                }
            }
            if (tokhai.ContractDocument != null && tokhai.ContractDocument.Count > 0)
            {
                foreach (ContractDocument cotract in tokhai.ContractDocument)
                {
                    ContractFromXML(cotract, tkmd);
                }
            }
            if (tokhai.CommercialInvoices != null && tokhai.CommercialInvoices.Count > 0)
            {
                foreach (CommercialInvoice invoice in tokhai.CommercialInvoices)
                {
                    CommercialInvoiceFromXML(invoice, tkmd);
                }
            }
            if (tokhai.CustomsOfficeChangedRequest != null && tokhai.CustomsOfficeChangedRequest.Count > 0)
            {
                foreach (CustomsOfficeChangedRequest dncck in tokhai.CustomsOfficeChangedRequest)
                {
                    CustomsOfficeChangedRequestFromXML(dncck, tkmd.ID);
                }
            }
            if (tokhai.AttachDocumentItem != null && tokhai.AttachDocumentItem.Count > 0)
            {
                foreach (AttachDocumentItem att in tokhai.AttachDocumentItem)
                {
                    AttachDocumentItemFromXML(att, tkmd.ID);
                }
            }
            #endregion
            return tkmd;
        }
        #endregion
        #region Data Mapper
        private static void AgentsFromXML(List<Agent> listAgent,ref ToKhaiMauDich tkmd)
        {
            #region Agents Đại lý khai
            foreach (Agent item in listAgent)
            {
                switch (item.Status)
                {
                    case AgentsStatus.NGUOIKHAI_HAIQUAN:
                        tkmd.TenDaiLyTTHQ = item.Name;
                        tkmd.MaDaiLyTTHQ = item.Identity;
                	break;
                    case AgentsStatus.UYTHAC:
                        tkmd.TenDonViUT = item.Name;
                        tkmd.MaDonViUT = item.Identity;
                    break;
                    case AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE:
                        tkmd.TenDoanhNghiep = item.Name;
                        tkmd.MaDoanhNghiep = item.Identity;
                    break;
                }
            }
            #endregion
        }
        private static void CertificateOfOriginFromXML(CertificateOfOrigin CoFromXML, ToKhaiMauDich tkmd)
        {
            #region Điền thông tin CO
            CO co = new CO()
            {
                SoCO = CoFromXML.Reference,
                LoaiCO = CoFromXML.Type,
                ToChucCap = CoFromXML.Issuer,
                NgayCO = Convert.ToDateTime(CoFromXML.Issue.Trim()),
                NuocCapCO = CoFromXML.IssueLocation,
                NguoiKy = CoFromXML.Representative,
                TenDiaChiNguoiXK = CoFromXML.ExporterEx.Name,
                TenDiaChiNguoiNK = CoFromXML.ImporterEx.Name,
                MaNuocXKTrenCO = CoFromXML.ExportationCountryEx.Code,
                MaNuocNKTrenCO = CoFromXML.ImportationCountryEx.Code,
                CangXepHang = CoFromXML.LoadingLocation.Code,
                NgayKhoiHanh = Convert.ToDateTime(CoFromXML.LoadingLocation.Loading.Trim()),
                CangDoHang = CoFromXML.UnloadingLocation.Code,
                NoCo = Convert.ToInt16(CoFromXML.IsDebt),
                ThoiHanNop = Convert.ToDateTime(CoFromXML.Submit.Trim()),
                TKMD_ID = tkmd.ID,
                MaDoanhNghiep = tkmd.MaDoanhNghiep,
            };
            #endregion CO
            #region Hàng trong CO
            foreach (GoodsItem item in CoFromXML.GoodsItems)
            {
                HangCoDetail hangCo = new HangCoDetail();
                hangCo.SoThuTuHang = Convert.ToInt16(item.Sequence.Trim());
                hangCo.TriGiaKB = Convert.ToDouble(item.StatisticalValue.Trim());
                hangCo.MaNguyenTe = item.CurrencyExchange.CurrencyType;
                hangCo.SoLuong = Convert.ToDecimal(item.ConsignmentItemPackaging.Quantity.Trim());
                hangCo.LoaiKien = item.ConsignmentItemPackaging.Type;
                hangCo.SoHieuKien = item.ConsignmentItemPackaging.MarkNumber;
                hangCo.TenHang = item.Commodity.Description;
                hangCo.MaPhu = item.Commodity.Identification;
                hangCo.MaHS = item.Commodity.TariffClassification;
                hangCo.TrongLuong = Convert.ToDouble(item.GoodsMeasure.GrossMass.Trim());
                hangCo.DVT_ID = item.GoodsMeasure.MeasureUnit;
                hangCo.NuocXX_ID = item.Origin.OriginCountry;
                hangCo.SoHoaDon = item.Invoice.Reference;
                hangCo.NgayHoaDon = Convert.ToDateTime(item.Invoice.Issue.Trim());
                foreach (HangMauDich hang in tkmd.HMDCollection)
                {
                    if(hangCo.MaPhu == hang.MaPhu)
                    {
                        hangCo.HMD_ID = hang.ID;
                        break;
                    }
                }
                co.ListHMDofCo.Add(hangCo);
                
            }
            #endregion
            co.InsertUpdateFull();
        }
        private static void AttachDocumentItemFromXML(AttachDocumentItem attDoc, long tkmdID)
        {
            ChungTuKem ctk = new ChungTuKem();
            ctk.NGAY_CT = Convert.ToDateTime(attDoc.Issue.Trim());
            ctk.SO_CT = attDoc.Reference;
            ctk.DIENGIAI = attDoc.Description;
            foreach (AttachedFile file in attDoc.AttachedFiles)
            {
                ChungTuKemChiTiet chitiet = new ChungTuKemChiTiet();
                chitiet.FileName = file.FileName;
                chitiet.NoiDung = System.Convert.FromBase64String(file.Content.Text);
                ctk.listCTChiTiet.Add(chitiet);
            }
            ctk.TKMDID = tkmdID;
            ctk.InsertUpdateFull(ctk.listCTChiTiet);
        }
        private static void LicenseFromXML(License lic,ToKhaiMauDich tkmd)
        {
            GiayPhep giayphep = new GiayPhep()
            {
                NguoiCap = lic.Issuer,
                SoGiayPhep = lic.Reference,
                NgayGiayPhep = Convert.ToDateTime(lic.Issue.Trim()),
                NoiCap = lic.IssueLocation,
                NgayHetHan = Convert.ToDateTime(lic.Expire.Trim()),
                ThongTinKhac = lic.AdditionalInformation.Content.Text,
                TKMD_ID = tkmd.ID,
            };
            foreach (GoodsItem item in lic.GoodItems)
            {
                HangGiayPhepDetail hang = new HangGiayPhepDetail();
                hang.SoThuTuHang = Convert.ToInt16(item.Sequence.Trim());
                hang.TriGiaKB = Convert.ToDouble(item.StatisticalValue.Trim());
                hang.MaNguyenTe = item.CurrencyExchange.CurrencyType;
                hang.TenHang = item.Commodity.Description;
                hang.MaPhu = item.Commodity.Identification;
                hang.MaHS = item.Commodity.TariffClassification;
                hang.DVT_ID = item.GoodsMeasure.MeasureUnit;
                hang.SoLuong = Convert.ToDecimal(item.GoodsMeasure.Quantity.Trim());
                hang.GhiChu = item.AdditionalInformation.Content.Text;
                foreach (HangMauDich hmd in tkmd.HMDCollection)
                {
                    if (hmd.MaPhu == hang.MaPhu)
                    {
                        hang.HMD_ID = hmd.ID;
                        break;
                    }
                }
                giayphep.ListHMDofGiayPhep.Add(hang);
            }
            giayphep.InsertUpdateFull();
        }
        private static void ContractFromXML(ContractDocument contractDoc, ToKhaiMauDich tkmd)
        {
            HopDongThuongMai hdTM = new HopDongThuongMai()
            {
                SoHopDongTM = contractDoc.Reference,
                NgayHopDongTM = Convert.ToDateTime(contractDoc.Issue.Trim()),
                ThoiHanThanhToan = Convert.ToDateTime(contractDoc.Expire.Trim()),
                PTTT_ID = contractDoc.Payment.Method,
                DKGH_ID = contractDoc.TradeTerm.Condition,
                DiaDiemGiaoHang = contractDoc.DeliveryDestination.Line,
                NguyenTe_ID = contractDoc.CurrencyExchange.CurrencyType.Trim(),
                TongTriGia = Convert.ToDecimal(contractDoc.TotalValue.Trim()),
                TenDonViMua = contractDoc.Buyer.Name,
                MaDonViMua = contractDoc.Buyer.Identity,
                TenDonViBan = contractDoc.Seller.Name,
                MaDonViBan = contractDoc.Seller.Identity,
                ThongTinKhac = string.IsNullOrEmpty(contractDoc.AdditionalInformations[0].Content.Text) ? "" : contractDoc.AdditionalInformations[0].Content.Text,
                TKMD_ID = tkmd.ID,
            };

            foreach (ContractItem hang in contractDoc.ContractItems)
            {
                HopDongThuongMaiDetail hangHD = new HopDongThuongMaiDetail();
                hangHD.DonGiaKB = Convert.ToDouble(hang.unitPrice.Trim());
                hangHD.TriGiaKB = Convert.ToDouble(hang.statisticalValue.Trim());
                hangHD.TenHang = hang.Commodity.Description;
                hangHD.MaPhu = hang.Commodity.Identification;
                hangHD.MaHS = hang.Commodity.TariffClassification;
                hangHD.NuocXX_ID = hang.Origin.OriginCountry;
                hangHD.SoLuong = Convert.ToDecimal(hang.GoodsMeasure.Quantity.Trim());
                hangHD.DVT_ID = hang.GoodsMeasure.MeasureUnit.Trim();
                hangHD.GhiChu = string.IsNullOrEmpty(hang.AdditionalInformation.Content.Text) ? "" : hang.AdditionalInformation.Content.Text;
                foreach (HangMauDich hmd in tkmd.HMDCollection)
                {
                    if (hmd.MaPhu == hangHD.MaPhu)
                    {
                        hangHD.HMD_ID = hmd.ID;
                        break;
                    }
                }
                hdTM.ListHangMDOfHopDong.Add(hangHD);
            }
            hdTM.InsertUpdateFull();
        }
        private static void ReceiptFromXML(Receipt receipt, ToKhaiMauDich tkmd)
        {
            GiayNopTien giayNT = new GiayNopTien()
            {
                SoLenh = receipt.Reference,
                NgayPhatLenh = Convert.ToDateTime(receipt.Issue.Trim()),
                TenNguoiNop = receipt.Payer.Name,
                SoCMNDNguoiNop = receipt.Payer.Identity,
                DiaChi = receipt.Payer.AddressGNT.Line,
                TenDonViNop = receipt.TaxPayer.Name,
                MaDonViNop = receipt.TaxPayer.Identity,
                SoTKNop = receipt.TaxPayer.Account.Number,
                TenNganHangNop = receipt.TaxPayer.Account.Bank.Name,
                MaNganHangNop = receipt.TaxPayer.Account.Bank.Identity,
                TenDonViNhan = receipt.Payee.Name,
                MaDonViNhan = receipt.Payee.Identity,
                SoTKNhan = receipt.Payee.Account.Number,
                TenNganHangNhan = receipt.Payee.Account.Bank.Name,
                MaNganHangNhan = receipt.Payee.Account.Bank.Identity,
                GhiChu = receipt.AdditionalInformation.Content.Text,
                ChungTuCollection = new List<GiayNopTienChungTu>(),
                ChiTietCollection = new List<GiayNopTienChiTiet>(),
            };
            giayNT.TKMD_ID = tkmd.ID;
            giayNT.InsertUpdate();
            foreach (AdditionalDocument add in receipt.AdditionalDocument)
            {
                GiayNopTienChungTu chungtu = new GiayNopTienChungTu();
                chungtu.LoaiChungTu = add.Type;
                chungtu.SoChungTu = add.Reference;
                chungtu.NgayPhatHanh = Convert.ToDateTime(add.Issue.Trim());
                chungtu.TenChungTu = add.Name;
                chungtu.GiayNopTien_ID = giayNT.ID;
                giayNT.InsertUpdate();
                giayNT.ChungTuCollection.Add(chungtu);

            }
            foreach (DutyTaxFee tax in receipt.DutyTaxFee)
            {
                GiayNopTienChiTiet chitiet = new GiayNopTienChiTiet();
                chitiet.SoTien = Convert.ToDouble(tax.AdValoremTaxBase.Trim());
                chitiet.DieuChinhGiam = Convert.ToDouble(tax.Deduct.Trim());
                chitiet.SacThue = Convert.ToInt16(tax.Type);
                foreach (AdditionalInformation info in tax.AdditionalInformations)
                {
                    switch (info.Statement)
                    {
                        case "211":
                            chitiet.MaChuong = info.Content.Text;
                            break;
                        case "212":
                            chitiet.Loai = info.Content.Text;
                            break;
                        case "213":
                            chitiet.Khoan = info.Content.Text;
                            break;
                        case "214":
                            chitiet.Muc = info.Content.Text;
                            break;
                        case "215":
                            chitiet.TieuMuc = info.Content.Text;
                            break;
                    }
                }
                chitiet.GiayNopTien_ID = giayNT.ID;
                chitiet.InsertUpdate();
                giayNT.ChiTietCollection.Add(chitiet);
            }
            
        }
        /// <summary>
        /// Đề nghị chuyển cửa khẩu
        /// </summary>
        /// <param name="dnChuyenCuaKhau"></param>
        /// <returns></returns>
        private static void CustomsOfficeChangedRequestFromXML(CustomsOfficeChangedRequest dncck,long tkmdID)
        {
            DeNghiChuyenCuaKhau denghicck = new DeNghiChuyenCuaKhau
            {
                SoVanDon = dncck.AdditionalDocument.Reference,
                NgayVanDon = Convert.ToDateTime(dncck.AdditionalDocument.Issue.Trim()),
                ThongTinKhac = dncck.AdditionalInformation.Content.Text,
                DiaDiemKiemTra = dncck.AdditionalInformation.ExaminationPlace,
                ThoiGianDen = Convert.ToDateTime(dncck.AdditionalInformation.Time.Trim()),
                TuyenDuong = dncck.AdditionalInformation.Route,
                TKMD_ID = tkmdID
            };
            denghicck.InsertUpdate();
        }
        private static void CommercialInvoiceFromXML(CommercialInvoice hoadonTM,ToKhaiMauDich tkmd)
        {
            HoaDonThuongMai hdtm = new HoaDonThuongMai
            {
                SoHoaDon = hoadonTM.Reference,
                NgayHoaDon = Convert.ToDateTime(hoadonTM.Issue.Trim()),
                TenDonViBan = hoadonTM.Seller.Name,
                MaDonViBan = hoadonTM.Seller.Identity,
                TenDonViMua = hoadonTM.Buyer.Name,
                MaDonViMua = hoadonTM.Buyer.Identity,
                PTTT_ID = hoadonTM.Payment.Method.Trim(),
                NguyenTe_ID = hoadonTM.CurrencyExchange.CurrencyType.Trim(),
                DKGH_ID = hoadonTM.TradeTerm.Condition.Trim(),
                TKMD_ID = tkmd.ID,
            };
            foreach (CommercialInvoiceItem hang in hoadonTM.CommercialInvoiceItems)
            {
                HoaDonThuongMaiDetail hangInHD = new HoaDonThuongMaiDetail();
                hangInHD.SoThuTuHang = Convert.ToInt16(hang.Sequence.Trim());
                hangInHD.DonGiaKB = Convert.ToDouble(hang.UnitPrice.Trim());
                hangInHD.TriGiaKB = Convert.ToDouble(hang.StatisticalValue.Trim());
                hangInHD.NuocXX_ID = hang.Origin.OriginCountry;
                hangInHD.TenHang = hang.Commodity.Description;
                hangInHD.MaPhu = hang.Commodity.Identification;
                hangInHD.MaHS = hang.Commodity.TariffClassification;
                hangInHD.SoLuong = Convert.ToDecimal(hang.GoodsMeasure.Quantity.Trim());
                hangInHD.DVT_ID = hang.GoodsMeasure.MeasureUnit;
                hangInHD.GiaTriDieuChinhTang = Convert.ToDouble(hang.ValuationAdjustment.Addition.Trim());
                hangInHD.GiaiTriDieuChinhGiam = Convert.ToDouble(hang.ValuationAdjustment.Deduction.Trim());
                hangInHD.GhiChu = string.IsNullOrEmpty(hang.AdditionalInformation.Content.Text) ? "" : hang.AdditionalInformation.Content.Text;
                foreach (HangMauDich hmd in tkmd.HMDCollection)
                {
                    if (hmd.MaPhu == hangInHD.MaPhu)
                    {
                        hangInHD.HMD_ID = hmd.ID;
                        break;
                    }
                }

                hdtm.ListHangMDOfHoaDon.Add(hangInHD);
            }
            hdtm.InsertUpdateFull();
        }
        #endregion
    }
}
