﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;

namespace Company.Interface.KDT.GC
{
    public partial class SanPhamGCBoSungForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPKDieuChinhMaSP = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        public bool boolFlag;
        public SanPhamGCBoSungForm()
        {
            InitializeComponent();
        }

       

        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            {
                if (!MaHS.Validate(txtMaHS.Text.Trim(), 10))
                {
                    error.SetIconPadding(txtMaHS, -8);
                    error.SetError(txtMaHS,setText("Mã số HS không hợp lệ.","This value is invalid"));
                    txtMaHS.Focus();
                    return;
                }
                if (Convert.ToDecimal(txtSoLuong.Text) == 0)
                {
                    error.SetError(txtSoLuong,setText( "Số lượng phải lớn hơn 0.","This value must be greater than 0"));
                    return;
                }              
                //if (Convert.ToDecimal(txtDonGia.Text) == 0)
                //{
                //    error.SetError(txtDonGia, setText("Đơn giá phải lớn hơn 0.", "This value must be greater than 0"));
                //    return;
                //}   
                checkExitsSPAndSTTHang();
            }
        }
        private void reset()
        {
            txtMa.Text = "";
            txtMaHS.Text = "";
            txtSoLuong.Text = "0";
            txtTen.Text = "";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3) ;           
            try
            {
                dgList.Refetch();
            }
            catch
            { dgList.Refresh(); }
            HangPK = new HangPhuKien();

          
        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            error.SetError(txtMaHS, null);

        }
        private void checkExitsSPAndSTTHang()
        {
            try
            {
                Company.GC.BLL.GC.SanPham sp = new  Company.GC.BLL.GC.SanPham();
                sp.Ma = txtMa.Text.Trim();
                sp.HopDong_ID = this.HD.ID;
                //bool ok = false;
                if (sp.Load())
                {
                    if (sp.Ma.Trim().ToUpper() != HangPK.MaHang.Trim().ToUpper())
                    {
                        showMsg("MSG_240220");
                        //MLMessages("Sản phẩm này đã được khai báo.","MSG_STN08","", false);
                        return;
                    }
                }
                LoaiPK.HPKCollection.Remove(HangPK);

                foreach (HangPhuKien pkienBoSung in LoaiPK.HPKCollection)
                {
                    if (pkienBoSung.MaHang.Trim().ToUpper() == txtMa.Text.Trim().ToUpper())
                    {
                        showMsg("MSG_240220");
                        //MLMessages("Sản phẩm này đã được khai báo.", "MSG_STN08", "", false);
                        txtMa.Focus();
                        if (HangPK.MaHang.Trim() != "")
                            LoaiPK.HPKCollection.Add(HangPK);
                        return;
                    }
                }
                HangPK.Delete(LoaiPK.MaPhuKien, HD.ID);
                HangPK.ID = 0;
                HangPK.SoLuong = Convert.ToDecimal(txtSoLuong.Text);
                HangPK.TenHang = txtTen.Text.Trim();
                HangPK.MaHang = txtMa.Text.Trim();
                HangPK.MaHS = txtMaHS.Text.Trim();
                HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                HangPK.DonGia = Convert.ToDouble(txtDonGia.Text);
                HangPK.Master_ID = pkdk.ID;
                HangPK.NuocXX_ID = "VN";
                HangPK.NhomSP = cbNhomSP.SelectedValue.ToString();
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                LoaiPK.InsertUpdateBoSungSP(pkdk.HopDong_ID);
                //PhuKienGCSendForm.isEdit = true;
                reset();
                this.setErro();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
            
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                HangPK = (HangPhuKien)e.Row.DataRow;
                txtMa.Text = HangPK.MaHang.Trim();
                txtMaHS.Text = HangPK.MaHS.Trim();
                txtSoLuong.Text = HangPK.SoLuong.ToString();
                txtTen.Text = HangPK.TenHang.Trim();
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                cbNhomSP.SelectedValue = HangPK.NhomSP;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
                e.Row.Cells["TenNhomSanPham"].Text = Company.KDT.SHARE.Components.DuLieuChuan.NhomSanPham.getTenSanPham(e.Row.Cells["NhomSanPham_ID"].Value.ToString());
                e.Row.Cells["NhomSanPham_ID"].Text = Company.KDT.SHARE.Components.DuLieuChuan.NhomSanPham.getTenSanPham(e.Row.Cells["NhomSanPham_ID"].Value.ToString());
            }
        }
  
        private void SanPhamGCBoSungForm_Load(object sender, EventArgs e)
        {
            revMaHS.ValidationExpression = @"\d{8,10}";

            txtMa.Focus();
            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["SoLuongDangKy"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;

            Company.GC.BLL.GC.NhomSanPham nhomSP = new Company.GC.BLL.GC.NhomSanPham ();
            nhomSP.HopDong_ID = this.HD.ID;

            List<Company.GC.BLL.GC.NhomSanPham> LoaiSPCollection = nhomSP.SelectCollectionBy_HopDong_ID();                 
            cbNhomSP.DataSource = LoaiSPCollection;
            cbNhomSP.DisplayMember = "TenSanPham";
            cbNhomSP.ValueMember = "MaSanPham";    
          

            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "S13")
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnAdd.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO|| pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                btnAdd.Enabled = true;
                btnXoa.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                btnAdd.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            //Registered
            if (boolFlag == true)
            {
                btnAdd.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            LoaiPK.MaPhuKien = "S13";
            dgList.DataSource = LoaiPK.HPKCollection; 
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            {

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien,pkdk.HopDong_ID);
                        }
                    }
                }

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void SanPhamGCBoSungForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (LoaiPK.HPKCollection.Count > 0)
            {
                LoaiPK.MaPhuKien = "S13";
                LoaiPK.NoiDung = LoaiPhuKien_GetName("S13");
                pkdk.PKCollection.Remove(LoaiPK);
                pkdk.PKCollection.Add(LoaiPK);
            }
            else
            {
                pkdk.PKCollection.Remove(LoaiPK);
                LoaiPK.Delete(pkdk.HopDong_ID);;
            }
        }

        private void txtMa_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTen_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMaHS_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbDonViTinh_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtSoLuong_Click(object sender, EventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (LoaiPK.HPKCollection.Count <= 0) return;
            HangPhuKienCollection pkColl = new  HangPhuKienCollection();
            pkColl =( HangPhuKienCollection) LoaiPK.HPKCollection.Clone();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {                
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                        }
                        try 
                        {
                            pkColl.Remove(pkDelete);                            
                        }
                        catch { }
                    }
                }
                LoaiPK.HPKCollection = pkColl;
                this.dgList.DataSource = LoaiPK.HPKCollection;
                txtMa.Text = "";
                txtMaHS.Text = "";
                txtTen.Text = "";
                txtSoLuong.Text = "0.000";
                cbDonViTinh.SelectedIndex = 0;
                cbNhomSP.SelectedIndex = 0;


                this.setErro();
                try
                {
                    dgList.Refetch();
                }
                catch { dgList.Refresh(); }
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            Company.Interface.KDT.SXXK.BoSungSanPhamReadExcelForm f = new Company.Interface.KDT.SXXK.BoSungSanPhamReadExcelForm();
            f.PKDangKy = this.pkdk;
            f.LoaiPK = this.LoaiPK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch            {
                dgList.Refresh();
            }
        }

      

        
    }
}