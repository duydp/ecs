namespace Company.Interface
{
    partial class ReadExcelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadExcelForm));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtCLGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTSVAT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTSTTDB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTriGiaTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSheet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtXuatXuColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDVTColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuongColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTSXNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDonGiaColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSelectFile = new Janus.Windows.EditControls.UIButton();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.rfvDVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSP = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSheet = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDonGia = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoLuong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTSXNK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.requiredFieldValidator4 = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.ckTuDongTT = new Janus.Windows.EditControls.UICheckBox();
            this.ckTCVN3 = new Janus.Windows.EditControls.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDonGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTSXNK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requiredFieldValidator4)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.ckTCVN3);
            this.grbMain.Controls.Add(this.ckTuDongTT);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(460, 311);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(174, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cột mã hàng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(262, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cột tên hàng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(353, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cột mã HS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(86, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Cột ĐVT";
            // 
            // txtRow
            // 
            this.txtRow.DecimalDigits = 0;
            this.txtRow.Location = new System.Drawing.Point(87, 40);
            this.txtRow.MaxLength = 6;
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(75, 21);
            this.txtRow.TabIndex = 3;
            this.txtRow.Text = "1";
            this.txtRow.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtRow.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên Sheet";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(172, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Cột số lượng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(262, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Cột đơn giá";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Cột xuất xứ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(84, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Dòng bắt đầu";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtCLGia);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.txtTSVAT);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.txtTSTTDB);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaTT);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtSheet);
            this.uiGroupBox1.Controls.Add(this.txtXuatXuColumn);
            this.uiGroupBox1.Controls.Add(this.txtDVTColumn);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongColumn);
            this.uiGroupBox1.Controls.Add(this.txtTSXNK);
            this.uiGroupBox1.Controls.Add(this.txtDonGiaColumn);
            this.uiGroupBox1.Controls.Add(this.txtMaHSColumn);
            this.uiGroupBox1.Controls.Add(this.txtTenHangColumn);
            this.uiGroupBox1.Controls.Add(this.txtMaHangColumn);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtRow);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(438, 199);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông số cấu hình";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtCLGia
            // 
            this.txtCLGia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCLGia.Location = new System.Drawing.Point(265, 141);
            this.txtCLGia.MaxLength = 1;
            this.txtCLGia.Name = "txtCLGia";
            this.txtCLGia.Size = new System.Drawing.Size(75, 21);
            this.txtCLGia.TabIndex = 27;
            this.txtCLGia.Text = "L";
            this.txtCLGia.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(262, 125);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Cột TLCL giá";
            // 
            // txtTSVAT
            // 
            this.txtTSVAT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTSVAT.Location = new System.Drawing.Point(175, 141);
            this.txtTSVAT.MaxLength = 1;
            this.txtTSVAT.Name = "txtTSVAT";
            this.txtTSVAT.Size = new System.Drawing.Size(79, 21);
            this.txtTSVAT.TabIndex = 25;
            this.txtTSVAT.Text = "K";
            this.txtTSVAT.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(173, 125);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Cột TS VAT";
            // 
            // txtTSTTDB
            // 
            this.txtTSTTDB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTSTTDB.Location = new System.Drawing.Point(86, 141);
            this.txtTSTTDB.MaxLength = 1;
            this.txtTSTTDB.Name = "txtTSTTDB";
            this.txtTSTTDB.Size = new System.Drawing.Size(75, 21);
            this.txtTSTTDB.TabIndex = 23;
            this.txtTSTTDB.Text = "J";
            this.txtTSTTDB.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(84, 125);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Cột TS TTDB";
            // 
            // txtTriGiaTT
            // 
            this.txtTriGiaTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTriGiaTT.Location = new System.Drawing.Point(353, 90);
            this.txtTriGiaTT.MaxLength = 1;
            this.txtTriGiaTT.Name = "txtTriGiaTT";
            this.txtTriGiaTT.Size = new System.Drawing.Size(75, 21);
            this.txtTriGiaTT.TabIndex = 19;
            this.txtTriGiaTT.Text = "H";
            this.txtTriGiaTT.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(350, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Cột TGTT";
            // 
            // txtSheet
            // 
            this.txtSheet.Location = new System.Drawing.Point(13, 40);
            this.txtSheet.Name = "txtSheet";
            this.txtSheet.Size = new System.Drawing.Size(61, 21);
            this.txtSheet.TabIndex = 1;
            this.txtSheet.Text = "Sheet1";
            this.txtSheet.VisualStyleManager = this.vsmMain;
            // 
            // txtXuatXuColumn
            // 
            this.txtXuatXuColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtXuatXuColumn.Location = new System.Drawing.Point(11, 90);
            this.txtXuatXuColumn.MaxLength = 1;
            this.txtXuatXuColumn.Name = "txtXuatXuColumn";
            this.txtXuatXuColumn.Size = new System.Drawing.Size(69, 21);
            this.txtXuatXuColumn.TabIndex = 11;
            this.txtXuatXuColumn.Text = "D";
            this.txtXuatXuColumn.VisualStyleManager = this.vsmMain;
            this.txtXuatXuColumn.TextChanged += new System.EventHandler(this.txtXuatXuColumn_TextChanged);
            // 
            // txtDVTColumn
            // 
            this.txtDVTColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTColumn.Location = new System.Drawing.Point(89, 90);
            this.txtDVTColumn.MaxLength = 1;
            this.txtDVTColumn.Name = "txtDVTColumn";
            this.txtDVTColumn.Size = new System.Drawing.Size(73, 21);
            this.txtDVTColumn.TabIndex = 13;
            this.txtDVTColumn.Text = "E";
            this.txtDVTColumn.VisualStyleManager = this.vsmMain;
            this.txtDVTColumn.TextChanged += new System.EventHandler(this.editBox8_TextChanged);
            // 
            // txtSoLuongColumn
            // 
            this.txtSoLuongColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongColumn.Location = new System.Drawing.Point(175, 90);
            this.txtSoLuongColumn.MaxLength = 1;
            this.txtSoLuongColumn.Name = "txtSoLuongColumn";
            this.txtSoLuongColumn.Size = new System.Drawing.Size(79, 21);
            this.txtSoLuongColumn.TabIndex = 15;
            this.txtSoLuongColumn.Text = "F";
            this.txtSoLuongColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtTSXNK
            // 
            this.txtTSXNK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTSXNK.Location = new System.Drawing.Point(13, 141);
            this.txtTSXNK.MaxLength = 1;
            this.txtTSXNK.Name = "txtTSXNK";
            this.txtTSXNK.Size = new System.Drawing.Size(62, 21);
            this.txtTSXNK.TabIndex = 21;
            this.txtTSXNK.Text = "I";
            this.txtTSXNK.VisualStyleManager = this.vsmMain;
            this.txtTSXNK.TextChanged += new System.EventHandler(this.txtTriGiaColumn_TextChanged);
            // 
            // txtDonGiaColumn
            // 
            this.txtDonGiaColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonGiaColumn.Location = new System.Drawing.Point(265, 90);
            this.txtDonGiaColumn.MaxLength = 1;
            this.txtDonGiaColumn.Name = "txtDonGiaColumn";
            this.txtDonGiaColumn.Size = new System.Drawing.Size(75, 21);
            this.txtDonGiaColumn.TabIndex = 17;
            this.txtDonGiaColumn.Text = "G";
            this.txtDonGiaColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHSColumn
            // 
            this.txtMaHSColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHSColumn.Location = new System.Drawing.Point(353, 40);
            this.txtMaHSColumn.MaxLength = 1;
            this.txtMaHSColumn.Name = "txtMaHSColumn";
            this.txtMaHSColumn.Size = new System.Drawing.Size(75, 21);
            this.txtMaHSColumn.TabIndex = 9;
            this.txtMaHSColumn.Text = "C";
            this.txtMaHSColumn.VisualStyleManager = this.vsmMain;
            this.txtMaHSColumn.TextChanged += new System.EventHandler(this.txtMaHSColumn_TextChanged);
            // 
            // txtTenHangColumn
            // 
            this.txtTenHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangColumn.Location = new System.Drawing.Point(265, 40);
            this.txtTenHangColumn.MaxLength = 1;
            this.txtTenHangColumn.Name = "txtTenHangColumn";
            this.txtTenHangColumn.Size = new System.Drawing.Size(75, 21);
            this.txtTenHangColumn.TabIndex = 7;
            this.txtTenHangColumn.Text = "B";
            this.txtTenHangColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHangColumn
            // 
            this.txtMaHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHangColumn.Location = new System.Drawing.Point(177, 40);
            this.txtMaHangColumn.MaxLength = 1;
            this.txtMaHangColumn.Name = "txtMaHangColumn";
            this.txtMaHangColumn.Size = new System.Drawing.Size(77, 21);
            this.txtMaHangColumn.TabIndex = 5;
            this.txtMaHangColumn.Text = "A";
            this.txtMaHangColumn.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Cột TS XNK";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnSelectFile);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 217);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(433, 53);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFile.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSelectFile.Icon")));
            this.btnSelectFile.Location = new System.Drawing.Point(343, 18);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(75, 22);
            this.btnSelectFile.TabIndex = 1;
            this.btnSelectFile.Text = "Chọn file";
            this.btnSelectFile.VisualStyleManager = this.vsmMain;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(13, 19);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(324, 21);
            this.txtFilePath.TabIndex = 0;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(376, 278);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(68, 23);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(302, 278);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(68, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Ghi";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // rfvDVT
            // 
            this.rfvDVT.ControlToValidate = this.txtDVTColumn;
            this.rfvDVT.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvDVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDVT.Icon")));
            this.rfvDVT.Tag = "rfvDVT";
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "Chưa chọn file Excel cần đọc";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            this.rfvFilePath.Tag = "rfvFilePath";
            // 
            // rfvTenSP
            // 
            this.rfvTenSP.ControlToValidate = this.txtTenHangColumn;
            this.rfvTenSP.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvTenSP.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSP.Icon")));
            this.rfvTenSP.Tag = "rfvTenSP";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMaHSColumn;
            this.rfvMaHS.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // cvError
            // 
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaHangColumn;
            this.rfvMa.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvTenSheet
            // 
            this.rfvTenSheet.ControlToValidate = this.txtSheet;
            this.rfvTenSheet.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvTenSheet.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSheet.Icon")));
            this.rfvTenSheet.Tag = "rfvTenSheet";
            // 
            // rfvDonGia
            // 
            this.rfvDonGia.ControlToValidate = this.txtDonGiaColumn;
            this.rfvDonGia.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvDonGia.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDonGia.Icon")));
            this.rfvDonGia.Tag = "rfvDonGia";
            // 
            // rfvSoLuong
            // 
            this.rfvSoLuong.ControlToValidate = this.txtSoLuongColumn;
            this.rfvSoLuong.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoLuong.Icon")));
            this.rfvSoLuong.Tag = "rfvSoLuong";
            // 
            // rfvTSXNK
            // 
            this.rfvTSXNK.ControlToValidate = this.txtTSXNK;
            this.rfvTSXNK.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvTSXNK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTSXNK.Icon")));
            this.rfvTSXNK.Tag = "rfvTSXNK";
            // 
            // requiredFieldValidator4
            // 
            this.requiredFieldValidator4.ControlToValidate = this.txtMaHangColumn;
            this.requiredFieldValidator4.ErrorMessage = "Thông tin này  không được để trống";
            this.requiredFieldValidator4.Icon = ((System.Drawing.Icon)(resources.GetObject("requiredFieldValidator4.Icon")));
            this.requiredFieldValidator4.Tag = "requiredFieldValidator4";
            // 
            // ckTuDongTT
            // 
            this.ckTuDongTT.BackColor = System.Drawing.Color.Transparent;
            this.ckTuDongTT.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.ckTuDongTT.Location = new System.Drawing.Point(12, 278);
            this.ckTuDongTT.Name = "ckTuDongTT";
            this.ckTuDongTT.Size = new System.Drawing.Size(114, 23);
            this.ckTuDongTT.TabIndex = 2;
            this.ckTuDongTT.Text = "Tự động tính thuế";
            // 
            // ckTCVN3
            // 
            this.ckTCVN3.BackColor = System.Drawing.Color.Transparent;
            this.ckTCVN3.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.ckTCVN3.Location = new System.Drawing.Point(132, 278);
            this.ckTCVN3.Name = "ckTCVN3";
            this.ckTCVN3.Size = new System.Drawing.Size(76, 23);
            this.ckTCVN3.TabIndex = 3;
            this.ckTCVN3.Text = "Font TCVN3";
            // 
            // ReadExcelForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(460, 311);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReadExcelForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đọc danh sách hàng hóa";
            this.Load += new System.EventHandler(this.ReadExcelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDonGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTSXNK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requiredFieldValidator4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnSelectFile;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtXuatXuColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonGiaColumn;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.GridEX.EditControls.EditBox txtSheet;
        private Janus.Windows.GridEX.EditControls.EditBox txtTSXNK;
        private System.Windows.Forms.Label label10;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDVT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSP;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDonGia;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoLuong;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTSXNK;
        private Company.Controls.CustomValidation.RequiredFieldValidator requiredFieldValidator4;
        private Janus.Windows.GridEX.EditControls.EditBox txtCLGia;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.EditBox txtTSVAT;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtTSTTDB;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtTriGiaTT;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UICheckBox ckTuDongTT;
        private Janus.Windows.EditControls.UICheckBox ckTCVN3;
    }
}