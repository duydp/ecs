using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_TK_KhoanDieuChinh : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public int SoTT { set; get; }
		public string MaTenDieuChinh { set; get; }
		public string MaPhanLoaiDieuChinh { set; get; }
		public string MaTTDieuChinhTriGia { set; get; }
		public decimal TriGiaKhoanDieuChinh { set; get; }
		public decimal TongHeSoPhanBo { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_TK_KhoanDieuChinh> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_TK_KhoanDieuChinh> collection = new List<KDT_VNACC_TK_KhoanDieuChinh>();
			while (reader.Read())
			{
				KDT_VNACC_TK_KhoanDieuChinh entity = new KDT_VNACC_TK_KhoanDieuChinh();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTT"))) entity.SoTT = reader.GetInt32(reader.GetOrdinal("SoTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTenDieuChinh"))) entity.MaTenDieuChinh = reader.GetString(reader.GetOrdinal("MaTenDieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiDieuChinh"))) entity.MaPhanLoaiDieuChinh = reader.GetString(reader.GetOrdinal("MaPhanLoaiDieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTDieuChinhTriGia"))) entity.MaTTDieuChinhTriGia = reader.GetString(reader.GetOrdinal("MaTTDieuChinhTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKhoanDieuChinh"))) entity.TriGiaKhoanDieuChinh = reader.GetDecimal(reader.GetOrdinal("TriGiaKhoanDieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongHeSoPhanBo"))) entity.TongHeSoPhanBo = reader.GetDecimal(reader.GetOrdinal("TongHeSoPhanBo"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_TK_KhoanDieuChinh> collection, long id)
        {
            foreach (KDT_VNACC_TK_KhoanDieuChinh item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_TK_KhoanDieuChinh VALUES(@TKMD_ID, @SoTT, @MaTenDieuChinh, @MaPhanLoaiDieuChinh, @MaTTDieuChinhTriGia, @TriGiaKhoanDieuChinh, @TongHeSoPhanBo, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_TK_KhoanDieuChinh SET TKMD_ID = @TKMD_ID, SoTT = @SoTT, MaTenDieuChinh = @MaTenDieuChinh, MaPhanLoaiDieuChinh = @MaPhanLoaiDieuChinh, MaTTDieuChinhTriGia = @MaTTDieuChinhTriGia, TriGiaKhoanDieuChinh = @TriGiaKhoanDieuChinh, TongHeSoPhanBo = @TongHeSoPhanBo, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TK_KhoanDieuChinh WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTT", SqlDbType.Int, "SoTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTenDieuChinh", SqlDbType.VarChar, "MaTenDieuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiDieuChinh", SqlDbType.VarChar, "MaPhanLoaiDieuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTDieuChinhTriGia", SqlDbType.VarChar, "MaTTDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaKhoanDieuChinh", SqlDbType.Decimal, "TriGiaKhoanDieuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongHeSoPhanBo", SqlDbType.Decimal, "TongHeSoPhanBo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTT", SqlDbType.Int, "SoTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTenDieuChinh", SqlDbType.VarChar, "MaTenDieuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiDieuChinh", SqlDbType.VarChar, "MaPhanLoaiDieuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTDieuChinhTriGia", SqlDbType.VarChar, "MaTTDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaKhoanDieuChinh", SqlDbType.Decimal, "TriGiaKhoanDieuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongHeSoPhanBo", SqlDbType.Decimal, "TongHeSoPhanBo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_TK_KhoanDieuChinh VALUES(@TKMD_ID, @SoTT, @MaTenDieuChinh, @MaPhanLoaiDieuChinh, @MaTTDieuChinhTriGia, @TriGiaKhoanDieuChinh, @TongHeSoPhanBo, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_TK_KhoanDieuChinh SET TKMD_ID = @TKMD_ID, SoTT = @SoTT, MaTenDieuChinh = @MaTenDieuChinh, MaPhanLoaiDieuChinh = @MaPhanLoaiDieuChinh, MaTTDieuChinhTriGia = @MaTTDieuChinhTriGia, TriGiaKhoanDieuChinh = @TriGiaKhoanDieuChinh, TongHeSoPhanBo = @TongHeSoPhanBo, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TK_KhoanDieuChinh WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTT", SqlDbType.Int, "SoTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTenDieuChinh", SqlDbType.VarChar, "MaTenDieuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiDieuChinh", SqlDbType.VarChar, "MaPhanLoaiDieuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTDieuChinhTriGia", SqlDbType.VarChar, "MaTTDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaKhoanDieuChinh", SqlDbType.Decimal, "TriGiaKhoanDieuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongHeSoPhanBo", SqlDbType.Decimal, "TongHeSoPhanBo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTT", SqlDbType.Int, "SoTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTenDieuChinh", SqlDbType.VarChar, "MaTenDieuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiDieuChinh", SqlDbType.VarChar, "MaPhanLoaiDieuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTDieuChinhTriGia", SqlDbType.VarChar, "MaTTDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaKhoanDieuChinh", SqlDbType.Decimal, "TriGiaKhoanDieuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongHeSoPhanBo", SqlDbType.Decimal, "TongHeSoPhanBo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_TK_KhoanDieuChinh Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_TK_KhoanDieuChinh> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_TK_KhoanDieuChinh> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_TK_KhoanDieuChinh> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_TK_KhoanDieuChinh(long tKMD_ID, int soTT, string maTenDieuChinh, string maPhanLoaiDieuChinh, string maTTDieuChinhTriGia, decimal triGiaKhoanDieuChinh, decimal tongHeSoPhanBo, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_TK_KhoanDieuChinh entity = new KDT_VNACC_TK_KhoanDieuChinh();	
			entity.TKMD_ID = tKMD_ID;
			entity.SoTT = soTT;
			entity.MaTenDieuChinh = maTenDieuChinh;
			entity.MaPhanLoaiDieuChinh = maPhanLoaiDieuChinh;
			entity.MaTTDieuChinhTriGia = maTTDieuChinhTriGia;
			entity.TriGiaKhoanDieuChinh = triGiaKhoanDieuChinh;
			entity.TongHeSoPhanBo = tongHeSoPhanBo;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoTT", SqlDbType.Int, SoTT);
			db.AddInParameter(dbCommand, "@MaTenDieuChinh", SqlDbType.VarChar, MaTenDieuChinh);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDieuChinh", SqlDbType.VarChar, MaPhanLoaiDieuChinh);
			db.AddInParameter(dbCommand, "@MaTTDieuChinhTriGia", SqlDbType.VarChar, MaTTDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@TriGiaKhoanDieuChinh", SqlDbType.Decimal, TriGiaKhoanDieuChinh);
			db.AddInParameter(dbCommand, "@TongHeSoPhanBo", SqlDbType.Decimal, TongHeSoPhanBo);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_TK_KhoanDieuChinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_KhoanDieuChinh item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_TK_KhoanDieuChinh(long id, long tKMD_ID, int soTT, string maTenDieuChinh, string maPhanLoaiDieuChinh, string maTTDieuChinhTriGia, decimal triGiaKhoanDieuChinh, decimal tongHeSoPhanBo, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_TK_KhoanDieuChinh entity = new KDT_VNACC_TK_KhoanDieuChinh();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoTT = soTT;
			entity.MaTenDieuChinh = maTenDieuChinh;
			entity.MaPhanLoaiDieuChinh = maPhanLoaiDieuChinh;
			entity.MaTTDieuChinhTriGia = maTTDieuChinhTriGia;
			entity.TriGiaKhoanDieuChinh = triGiaKhoanDieuChinh;
			entity.TongHeSoPhanBo = tongHeSoPhanBo;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoTT", SqlDbType.Int, SoTT);
			db.AddInParameter(dbCommand, "@MaTenDieuChinh", SqlDbType.VarChar, MaTenDieuChinh);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDieuChinh", SqlDbType.VarChar, MaPhanLoaiDieuChinh);
			db.AddInParameter(dbCommand, "@MaTTDieuChinhTriGia", SqlDbType.VarChar, MaTTDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@TriGiaKhoanDieuChinh", SqlDbType.Decimal, TriGiaKhoanDieuChinh);
			db.AddInParameter(dbCommand, "@TongHeSoPhanBo", SqlDbType.Decimal, TongHeSoPhanBo);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_TK_KhoanDieuChinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_KhoanDieuChinh item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_TK_KhoanDieuChinh(long id, long tKMD_ID, int soTT, string maTenDieuChinh, string maPhanLoaiDieuChinh, string maTTDieuChinhTriGia, decimal triGiaKhoanDieuChinh, decimal tongHeSoPhanBo, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_TK_KhoanDieuChinh entity = new KDT_VNACC_TK_KhoanDieuChinh();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoTT = soTT;
			entity.MaTenDieuChinh = maTenDieuChinh;
			entity.MaPhanLoaiDieuChinh = maPhanLoaiDieuChinh;
			entity.MaTTDieuChinhTriGia = maTTDieuChinhTriGia;
			entity.TriGiaKhoanDieuChinh = triGiaKhoanDieuChinh;
			entity.TongHeSoPhanBo = tongHeSoPhanBo;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoTT", SqlDbType.Int, SoTT);
			db.AddInParameter(dbCommand, "@MaTenDieuChinh", SqlDbType.VarChar, MaTenDieuChinh);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDieuChinh", SqlDbType.VarChar, MaPhanLoaiDieuChinh);
			db.AddInParameter(dbCommand, "@MaTTDieuChinhTriGia", SqlDbType.VarChar, MaTTDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@TriGiaKhoanDieuChinh", SqlDbType.Decimal, TriGiaKhoanDieuChinh);
			db.AddInParameter(dbCommand, "@TongHeSoPhanBo", SqlDbType.Decimal, TongHeSoPhanBo);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_TK_KhoanDieuChinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_KhoanDieuChinh item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_TK_KhoanDieuChinh(long id)
		{
			KDT_VNACC_TK_KhoanDieuChinh entity = new KDT_VNACC_TK_KhoanDieuChinh();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_TK_KhoanDieuChinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_KhoanDieuChinh item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}