﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Data;
namespace DongBoDuLieu.BLL.TKMD_V3
{
    public class ChuyenCK
    {


        private static DeNghiChuyenCuaKhau ConvertToChuyenCuaKhau(DataRow drCCK)
        {
            DeNghiChuyenCuaKhau CCK = new DeNghiChuyenCuaKhau();

            CCK.DiaDiemKiemTra = drCCK["DiaDiemKH"].ToString();
            CCK.MaDoanhNghiep = drCCK["MaDN"].ToString();
            if (string.IsNullOrEmpty(drCCK["NamDK"].ToString()))
                CCK.NamTiepNhan = DateTime.Now.Year;
            CCK.NamTiepNhan = Convert.ToInt16(drCCK["NamDK"]);
            CCK.SoVanDon = drCCK["SoVanDon"].ToString();
            CCK.ThoiGianDen = Convert.ToDateTime(drCCK["ThoiGianDen"]);
            CCK.ThongTinKhac = drCCK["NoiDung"].ToString();
            CCK.TuyenDuong = drCCK["TuyenDuong"].ToString();
            
            return CCK;
        }
        public static long InsertUpdateCCK(long IDToKhai, DataRow drCCK, SqlTransaction transaction, SqlDatabase db)
        {
            DeNghiChuyenCuaKhau cck = ConvertToChuyenCuaKhau(drCCK);
            List<DeNghiChuyenCuaKhau> listcck = DeNghiChuyenCuaKhau.SelectCollectionDynamic(" TKMD_ID = " + IDToKhai, null, transaction, db);
            if (listcck != null && listcck.Count > 0)
                return 0;
            else
            {
                cck.TKMD_ID = IDToKhai;
                 return cck.InsertTransaction(transaction, db);
            }

        }
    }
}
