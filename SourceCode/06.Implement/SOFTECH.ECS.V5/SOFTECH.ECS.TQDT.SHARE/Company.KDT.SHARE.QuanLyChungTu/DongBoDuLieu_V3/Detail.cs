﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Company.KDT.SHARE.Components;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3
{
    public partial class Detail
    {

        public void TaoDuLieu(MessageHeader thongtinmess, ToKhai tokhai)
        {

            this.GUIDSTR = thongtinmess.Subject.Reference;
            this.SoTiepNhan = (long)Convert.ToInt64(tokhai.CustomsReference.Trim());
            this.SoToKhai = (long)Convert.ToInt64(thongtinmess.Subject.Issuer.Trim());
            this.NgayDangKy = Convert.ToDateTime(tokhai.Acceptance);
            this.PhanHe = thongtinmess.Subject.Function;
            this.MaLoaiHinh = thongtinmess.Subject.Type;
            this.PhanLuong = thongtinmess.Subject.IssueLocation;

            this.MaDoanhNghiep = thongtinmess.To.Identity;
            this.TenDoanhNghiep = thongtinmess.To.Name;

            this.MaDaiLy = thongtinmess.From.Identity;
            this.TenDaiLy = thongtinmess.From.Name;

            this.MaHaiQuan = thongtinmess.Subject.DeclarationOffice;
            this.NgayGui = DateTime.Now;
        }
        public static IDataReader SelectCollection(string phanhe, string MaDoanhNghiep, DateTime TuNgay, DateTime DenNgay)
        {
            const string spName = "[dbo].[p_MSG_DBDL_Detail_LoadCollection]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.ToString());
            db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.ToString());
            db.AddInParameter(dbCommand, "@PhanHe", SqlDbType.Char, phanhe);
            return db.ExecuteReader(dbCommand);
        }
        public static List<Detail> LoadCollectionDetail(string PhanHe, string MaDoanhNghiep, DateTime TuNgay, DateTime DenNgay)
        {
            List<Detail> collection = new List<Detail>();

            SqlDataReader reader = (SqlDataReader)SelectCollection(PhanHe, MaDoanhNghiep, TuNgay, DenNgay);
            while (reader.Read())
            {
                Detail entity = new Detail();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanHe"))) entity.PhanHe = reader.GetString(reader.GetOrdinal("PhanHe"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLy"))) entity.TenDaiLy = reader.GetString(reader.GetOrdinal("TenDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGui"))) entity.NgayGui = reader.GetDateTime(reader.GetOrdinal("NgayGui"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public static int DeleteByGuiStr(string guiStr, SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_MSG_DBDL_Detail_DeleteByGuiStr]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, guiStr);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public static Detail LoadByGuiStr(string GuiStr)
        {
            const string spName = "[dbo].[p_MSG_DBDL_Detail_LoadByGuiStr]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GuiStr);
            Detail entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new Detail();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanHe"))) entity.PhanHe = reader.GetString(reader.GetOrdinal("PhanHe"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLy"))) entity.TenDaiLy = reader.GetString(reader.GetOrdinal("TenDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGui"))) entity.NgayGui = reader.GetDateTime(reader.GetOrdinal("NgayGui"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
            }
            reader.Close();
            return entity;
        }
    }
}
