﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class KDT_LenhSanXuat_SP : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long LenhSanXuat_ID { set; get; }
		public string MaSanPham { set; get; }
		public string TenSanPham { set; get; }
		public string MaHS { set; get; }
		public string DVT_ID { set; get; }
		public string GhiChu { set; get; }
		public int TrangThai { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_LenhSanXuat_SP> ConvertToCollection(IDataReader reader)
		{
			List<KDT_LenhSanXuat_SP> collection = new List<KDT_LenhSanXuat_SP>();
			while (reader.Read())
			{
				KDT_LenhSanXuat_SP entity = new KDT_LenhSanXuat_SP();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuat_ID"))) entity.LenhSanXuat_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuat_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_LenhSanXuat_SP> collection, long id)
        {
            foreach (KDT_LenhSanXuat_SP item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_LenhSanXuat_SP VALUES(@LenhSanXuat_ID, @MaSanPham, @TenSanPham, @MaHS, @DVT_ID, @GhiChu, @TrangThai)";
            string update = "UPDATE t_KDT_LenhSanXuat_SP SET LenhSanXuat_ID = @LenhSanXuat_ID, MaSanPham = @MaSanPham, TenSanPham = @TenSanPham, MaHS = @MaHS, DVT_ID = @DVT_ID, GhiChu = @GhiChu, TrangThai = @TrangThai WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_LenhSanXuat_SP WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_LenhSanXuat_SP VALUES(@LenhSanXuat_ID, @MaSanPham, @TenSanPham, @MaHS, @DVT_ID, @GhiChu, @TrangThai)";
            string update = "UPDATE t_KDT_LenhSanXuat_SP SET LenhSanXuat_ID = @LenhSanXuat_ID, MaSanPham = @MaSanPham, TenSanPham = @TenSanPham, MaHS = @MaHS, DVT_ID = @DVT_ID, GhiChu = @GhiChu, TrangThai = @TrangThai WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_LenhSanXuat_SP WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_LenhSanXuat_SP Load(long id)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_LenhSanXuat_SP> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_LenhSanXuat_SP> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_LenhSanXuat_SP> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_LenhSanXuat_SP> SelectCollectionBy_LenhSanXuat_ID(long lenhSanXuat_ID)
		{
            IDataReader reader = SelectReaderBy_LenhSanXuat_ID(lenhSanXuat_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_LenhSanXuat_ID(long lenhSanXuat_ID)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_SelectBy_LenhSanXuat_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, lenhSanXuat_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_LenhSanXuat_ID(long lenhSanXuat_ID)
		{
			const string spName = "p_KDT_LenhSanXuat_SP_SelectBy_LenhSanXuat_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, lenhSanXuat_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_LenhSanXuat_SP(long lenhSanXuat_ID, string maSanPham, string tenSanPham, string maHS, string dVT_ID, string ghiChu, int trangThai)
		{
			KDT_LenhSanXuat_SP entity = new KDT_LenhSanXuat_SP();	
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			entity.MaSanPham = maSanPham;
			entity.TenSanPham = tenSanPham;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.GhiChu = ghiChu;
			entity.TrangThai = trangThai;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_LenhSanXuat_SP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat_SP item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_LenhSanXuat_SP(long id, long lenhSanXuat_ID, string maSanPham, string tenSanPham, string maHS, string dVT_ID, string ghiChu, int trangThai)
		{
			KDT_LenhSanXuat_SP entity = new KDT_LenhSanXuat_SP();			
			entity.ID = id;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			entity.MaSanPham = maSanPham;
			entity.TenSanPham = tenSanPham;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.GhiChu = ghiChu;
			entity.TrangThai = trangThai;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_LenhSanXuat_SP_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_LenhSanXuat_SP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat_SP item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_LenhSanXuat_SP(long id, long lenhSanXuat_ID, string maSanPham, string tenSanPham, string maHS, string dVT_ID, string ghiChu, int trangThai)
		{
			KDT_LenhSanXuat_SP entity = new KDT_LenhSanXuat_SP();			
			entity.ID = id;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			entity.MaSanPham = maSanPham;
			entity.TenSanPham = tenSanPham;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.GhiChu = ghiChu;
			entity.TrangThai = trangThai;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_LenhSanXuat_SP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat_SP item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_LenhSanXuat_SP(long id)
		{
			KDT_LenhSanXuat_SP entity = new KDT_LenhSanXuat_SP();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_LenhSanXuat_ID(long lenhSanXuat_ID)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_DeleteBy_LenhSanXuat_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, lenhSanXuat_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_SP_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_LenhSanXuat_SP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat_SP item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}