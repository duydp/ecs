﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Danh Sách hợp đồng khai kèm
    /// </summary>
    public class ContractDocument : DeclarationBase
    {
        /// <summary>
        /// Thời hạn
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }


        /// <summary>
        /// Có phải là GC ngược không ( true = 1, false = 0)
        /// </summary>
        [XmlElement("isInverseProcedure")]
        public string isInverseProcedure { get; set; }

        /// <summary>
        /// Thanh toán
        /// </summary>
        [XmlElement("Payment")]
        public Payment Payment { get; set; }
        /// <summary>
        /// Điều kiện giao hàng
        /// </summary>
        [XmlElement("TradeTerm")]
        public TradeTerm TradeTerm { get; set; }
        /// <summary>
        /// Địa Điểm giao hàng
        /// </summary>
        [XmlElement("DeliveryDestination")]
        public DeliveryDestination DeliveryDestination { get; set; }
        /// <summary>
        /// Giá trị hợp đồng
        /// </summary>
        [XmlElement("CustomsValue")]
        public CustomsValue CustomsValue { get; set; }
        /// <summary>
        /// nước thuê gia công
        /// </summary>
        #region phần GC-Hợp Đồng
        [XmlElement("exportationCountry")]
        public string ExportationCountry { get; set; }
        /// <summary>
        /// (Phần GC_Hợp đồng)
        /// </summary>
        [XmlArray("ContractItems")]
        [XmlArrayItem("Item")]
        public List<Item> Items { get; set; }
        /// <summary>
        /// (Phần GC_Hợp đồng)
        /// </summary>
        [XmlArray("Products")]
        [XmlArrayItem("Product")]
        public List<Product> Products { get; set; }
        /// <summary>
        /// (Phần GC_Hợp đồng)
        /// </summary>
        [XmlArray("Materials")]
        [XmlArrayItem("Material")]
        public List<Product> Materials { get; set; }
        /// <summary>
        /// (Phần GC_Hợp đồng)
        /// </summary>
        [XmlArray("Equipments")]
        [XmlArrayItem("Equipment")]
        public List<Equipment> Equipments { get; set; }
        /// <summary>
        /// (Phần GC_Hợp đồng)
        /// </summary>
        [XmlArray("SampleProducts")]
        [XmlArrayItem("SampleProduct")]
        public List<Product> SampleProducts { get; set; }

        #endregion
        /// <summary>
        /// Tổng giá trị
        /// </summary>
        [XmlElement("totalValue")]
        public string TotalValue { get; set; }
        /// <summary>
        /// Người nhập
        /// </summary>
        [XmlElement("Buyer")]
        public NameBase Buyer { get; set; }
        /// <summary>
        /// Người xuất
        /// </summary>
        [XmlElement("Seller")]
        public NameBase Seller { get; set; }       
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("ContractItem")]
        public List<ContractItem> ContractItems { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("Equipment")]
        public List<Equipment> Equipment { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("SampleProduct")]
        public List<Product> SampleProduct { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("Comodity")]
        public Commodity Comodity { get; set; }
        /// <summary>
        /// Trị giá hàng thực xuất khẩu trên tờ khai,n..16
        /// </summary>
        [XmlElement("StatisticalValue")]
        public string StatisticalValue { get; set; }
        /// <summary>
        /// Chứng từ thanh toán
        /// </summary>
        [XmlElement("PaymentDocument")]
        public PaymentDocument PaymentDocument { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("ExportDeclarationDocument")]
        public List<ExportDeclarationDocument> ExportDeclarationDocument { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("Material")]
        public Product Material { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("methodOfProcess")]
        public string MethodOfProcess { get; set; }
        /// nước gia công
        /// </summary>
        [XmlElement("importationCountry")]
        public string ImportationCountry { get; set; }
    }
}
