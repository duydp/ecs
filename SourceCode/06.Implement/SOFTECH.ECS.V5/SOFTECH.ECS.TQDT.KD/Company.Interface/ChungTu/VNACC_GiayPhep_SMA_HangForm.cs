﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class VNACC_GiayPhep_SMA_HangForm : BaseFormHaveGuidPanel
    {
        private KDT_VNACC_HangGiayPhep_SMA HangGiayPhep = null;
        public KDT_VNACC_GiayPhep_SMA GiayPhep_SMA;
        private EGiayPhep GiayPhepType = EGiayPhep.SMA;
        private bool isAddNew = true;

        private DataTable dtHS = new DataTable();

        public VNACC_GiayPhep_SMA_HangForm()
        {
            InitializeComponent();

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_OGAProcedure.SMA.ToString());
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateControl(false))
                    return;

                if (isAddNew)
                {
                    HangGiayPhep = new KDT_VNACC_HangGiayPhep_SMA();

                    GetHangGiayPhep(HangGiayPhep);

                    GiayPhep_SMA.HangCollection.Add(HangGiayPhep);
                }
                else
                {
                    GetHangGiayPhep(HangGiayPhep);
                }

                grdHang.DataSource = GiayPhep_SMA.HangCollection;
                grdHang.Refetch();

                HangGiayPhep = new KDT_VNACC_HangGiayPhep_SMA();
                SetHangGiayPhep(HangGiayPhep);
                isAddNew = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetHangGiayPhep(KDT_VNACC_HangGiayPhep_SMA hangGiayPhep)
        {
            errorProvider.Clear();

            hangGiayPhep.TenThuocQuyCachDongGoi = txtTenThuocQuyCachDongGoi.Text;
            hangGiayPhep.MaSoHangHoa = txtMaSoHangHoa.Text;
            hangGiayPhep.HoatChat = txtHoatChat.Text;
            hangGiayPhep.TieuChuanChatLuong = txtTieuChuanChatLuong.Text;
            hangGiayPhep.SoDangKy = txtSoDangKy.Text;
            hangGiayPhep.HanDung = dtHanDung.Value;
            hangGiayPhep.SoLuong = Convert.ToInt32(txtSoLuong.Value);
            hangGiayPhep.DonVitinhSoLuong = ucDonViTinhSoLuong.Code;
            hangGiayPhep.TenHoatChatGayNghien = txtTenHoatChatGayNghien.Text;
            hangGiayPhep.CongDung = txtCongDung.Text;
            hangGiayPhep.TongSoKhoiLuongHoatChatGayNghien = Convert.ToDecimal(txtTongSoKhoiLuongHoatChatGayNghien.Value);
            hangGiayPhep.DonVitinhKhoiLuong = ucDonViTinhKhoiLuong.Code;
            hangGiayPhep.GiaNhapKhauVND = Convert.ToDecimal(txtGiaNhapKhauVND.Value);
            hangGiayPhep.GiaBanBuonVND = Convert.ToDecimal(txtGiaBanBuonVND.Value);
            hangGiayPhep.GiaBanLeVND = Convert.ToDecimal(txtGiaBanLeVND.Value);
            hangGiayPhep.TenThuongNhanXuatKhau = txtTenThuongNhanXuatKhau.Text;
            hangGiayPhep.MaBuuChinhXuatKhau = txtMaBuuChinhXuatKhau.Text;
            hangGiayPhep.SoNhaTenDuongXuatKhau = txtSoNhaTenDuongXuatKhau.Text;
            hangGiayPhep.PhuongXaXuatKhau = txtPhuongXaXuatKhau.Text;
            hangGiayPhep.QuanHuyenXuatKhau = txtQuanHuyenXuatKhau.Text;
            hangGiayPhep.TinhThanhPhoXuatKhau = txtTinhThanhPhoXuatKhau.Text;
            hangGiayPhep.MaQuocGiaXuatKhau = ucMaQuocGiaXuatKhau.Code;
            hangGiayPhep.TenCongTySanXuat = txtTenCongTySanXuat.Text;
            hangGiayPhep.MaBuuChinhCongTySanXuat = txtMaBuuChinhCongTySanXuat.Text;
            hangGiayPhep.SoNhaTenDuongCongTySanXuat = txtSoNhaTenDuongCongTySanXuat.Text;
            hangGiayPhep.PhuongXaCongTySanXuat = txtPhuongXaCongTySanXuat.Text;
            hangGiayPhep.QuanHuyenCongTySanXuat = txtQuanHuyenCongTySanXuat.Text;
            hangGiayPhep.TinhThanhPhoCongTySanXuat = txtTinhThanhPhoCongTySanXuat.Text;
            hangGiayPhep.MaQuocGiaCongTySanXuat = ucMaQuocGiaCongTySanXuat.Code;
            hangGiayPhep.TenCongTyCungCap = txtTenCongTyCungCap.Text;
            hangGiayPhep.MaBuuChinhCongTyCungCap = txtMaBuuChinhCongTyCungCap.Text;
            hangGiayPhep.SoNhaTenDuongCongTyCungCap = txtSoNhaTenDuongCongTyCungCap.Text;
            hangGiayPhep.PhuongXaCongTyCungCap = txtPhuongXaCongTyCungCap.Text;
            hangGiayPhep.QuanHuyenCongTyCungCap = txtQuanHuyenCongTyCungCap.Text;
            hangGiayPhep.TinhThanhPhoCongTyCungCap = txtTinhThanhPhoCongTyCungCap.Text;
            hangGiayPhep.MaQuocGiaCongTyCungCap = ucMaQuocGiaCongTyCungCap.Code;
            hangGiayPhep.TenDonViUyThac = txtTenDonViUyThac.Text;
            hangGiayPhep.MaBuuChinhDonViUyThac = txtMaBuuChinhDonViUyThac.Text;
            hangGiayPhep.SoNhaTenDuongDonViUyThac = txtSoNhaTenDuongDonViUyThac.Text;
            hangGiayPhep.PhuongXaDonViUyThac = txtPhuongXaDonViUyThac.Text;
            hangGiayPhep.QuanHuyenDonViUyThac = txtQuanHuyenDonViUyThac.Text;
            hangGiayPhep.TinhThanhPhoDonViUyThac = txtTinhThanhPhoDonViUyThac.Text;
            hangGiayPhep.MaQuocGiaDonViUyThac = ucMaQuocGiaDonViUyThac.Code;
            hangGiayPhep.LuongTonKhoKyTruoc = Convert.ToInt32(txtLuongTonKhoKyTruoc.Value);
            hangGiayPhep.DonViTinhLuongTonKyTruoc = ucDonViTinhLuongTonKyTruoc.Code;
            hangGiayPhep.LuongNhapTrongKy = Convert.ToInt32(txtLuongNhapTrongKy.Value);
            hangGiayPhep.TongSo = Convert.ToInt32(txtTongSo.Value);
            hangGiayPhep.TongSoXuatTrongKy = Convert.ToInt32(txtTongSoXuatTrongKy.Value);
            hangGiayPhep.SoLuongTonKhoDenNgay = Convert.ToInt32(txtSoLuongTonKhoDenNgay.Value);
            hangGiayPhep.HuHao = Convert.ToInt32(txtHuHao.Value);
            hangGiayPhep.GhiChuChiTiet = txtGhiChuChiTiet.Text;

        }

        private void SetHangGiayPhep(KDT_VNACC_HangGiayPhep_SMA hangGiayPhep)
        {
            errorProvider.Clear();

            txtTenThuocQuyCachDongGoi.Text = hangGiayPhep.TenThuocQuyCachDongGoi;
            txtMaSoHangHoa.Text = hangGiayPhep.MaSoHangHoa;
            txtHoatChat.Text = hangGiayPhep.HoatChat;
            txtTieuChuanChatLuong.Text = hangGiayPhep.TieuChuanChatLuong;
            txtSoDangKy.Text = hangGiayPhep.SoDangKy;
            dtHanDung.Value = hangGiayPhep.HanDung;
            txtSoLuong.Value = hangGiayPhep.SoLuong;
            ucDonViTinhSoLuong.Code = hangGiayPhep.DonVitinhSoLuong;
            txtTenHoatChatGayNghien.Text = hangGiayPhep.TenHoatChatGayNghien;
            txtCongDung.Text = hangGiayPhep.CongDung;
            txtTongSoKhoiLuongHoatChatGayNghien.Value = hangGiayPhep.TongSoKhoiLuongHoatChatGayNghien;
            ucDonViTinhKhoiLuong.Code = hangGiayPhep.DonVitinhKhoiLuong;
            txtGiaNhapKhauVND.Value = hangGiayPhep.GiaNhapKhauVND;
            txtGiaBanBuonVND.Value = hangGiayPhep.GiaBanBuonVND;
            txtGiaBanLeVND.Value = hangGiayPhep.GiaBanLeVND;
            txtTenThuongNhanXuatKhau.Text = hangGiayPhep.TenThuongNhanXuatKhau;
            txtMaBuuChinhXuatKhau.Text = hangGiayPhep.MaBuuChinhXuatKhau;
            txtSoNhaTenDuongXuatKhau.Text = hangGiayPhep.SoNhaTenDuongXuatKhau;
            txtPhuongXaXuatKhau.Text = hangGiayPhep.PhuongXaXuatKhau;
            txtQuanHuyenXuatKhau.Text = hangGiayPhep.QuanHuyenXuatKhau;
            txtTinhThanhPhoXuatKhau.Text = hangGiayPhep.TinhThanhPhoXuatKhau;
            ucMaQuocGiaXuatKhau.Code = hangGiayPhep.MaQuocGiaXuatKhau;
            txtTenCongTySanXuat.Text = hangGiayPhep.TenCongTySanXuat;
            txtMaBuuChinhCongTySanXuat.Text = hangGiayPhep.MaBuuChinhCongTySanXuat;
            txtSoNhaTenDuongCongTySanXuat.Text = hangGiayPhep.SoNhaTenDuongCongTySanXuat;
            txtPhuongXaCongTySanXuat.Text = hangGiayPhep.PhuongXaCongTySanXuat;
            txtQuanHuyenCongTySanXuat.Text = hangGiayPhep.QuanHuyenCongTySanXuat;
            txtTinhThanhPhoCongTySanXuat.Text = hangGiayPhep.TinhThanhPhoCongTySanXuat;
            ucMaQuocGiaCongTySanXuat.Code = hangGiayPhep.MaQuocGiaCongTySanXuat;
            txtTenCongTyCungCap.Text = hangGiayPhep.TenCongTyCungCap;
            txtMaBuuChinhCongTyCungCap.Text = hangGiayPhep.MaBuuChinhCongTyCungCap;
            txtSoNhaTenDuongCongTyCungCap.Text = hangGiayPhep.SoNhaTenDuongCongTyCungCap;
            txtPhuongXaCongTyCungCap.Text = hangGiayPhep.PhuongXaCongTyCungCap;
            txtQuanHuyenCongTyCungCap.Text = hangGiayPhep.QuanHuyenCongTyCungCap;
            txtTinhThanhPhoCongTyCungCap.Text = hangGiayPhep.TinhThanhPhoCongTyCungCap;
            ucMaQuocGiaCongTyCungCap.Code = hangGiayPhep.MaQuocGiaCongTyCungCap;
            txtTenDonViUyThac.Text = hangGiayPhep.TenDonViUyThac;
            txtMaBuuChinhDonViUyThac.Text = hangGiayPhep.MaBuuChinhDonViUyThac;
            txtSoNhaTenDuongDonViUyThac.Text = hangGiayPhep.SoNhaTenDuongDonViUyThac;
            txtPhuongXaDonViUyThac.Text = hangGiayPhep.PhuongXaDonViUyThac;
            txtQuanHuyenDonViUyThac.Text = hangGiayPhep.QuanHuyenDonViUyThac;
            txtTinhThanhPhoDonViUyThac.Text = hangGiayPhep.TinhThanhPhoDonViUyThac;
            ucMaQuocGiaDonViUyThac.Code = hangGiayPhep.MaQuocGiaDonViUyThac;
            txtLuongTonKhoKyTruoc.Value = hangGiayPhep.LuongTonKhoKyTruoc;
            ucDonViTinhLuongTonKyTruoc.Code = hangGiayPhep.DonViTinhLuongTonKyTruoc;
            txtLuongNhapTrongKy.Value = hangGiayPhep.LuongNhapTrongKy;
            txtTongSo.Value = hangGiayPhep.TongSo;
            txtTongSoXuatTrongKy.Value = hangGiayPhep.TongSoXuatTrongKy;
            txtSoLuongTonKhoDenNgay.Value = hangGiayPhep.SoLuongTonKhoDenNgay;
            txtHuHao.Value = hangGiayPhep.HuHao;
            txtGhiChuChiTiet.Text = hangGiayPhep.GhiChuChiTiet;
        }

        private void VNACC_GiayPhep_SMA_HangForm_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SetIDControl();

                SetMaxLengthControl();

                Khoitao_DuLieuChuan();

                grdHang.DataSource = GiayPhep_SMA.HangCollection;

                ValidateControl(true);

                SetAutoRemoveUnicodeAndUpperCaseControl();

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void Khoitao_DuLieuChuan()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                dtHS = MaHS.SelectAll();
                foreach (DataRow dr in dtHS.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaSoHangHoa.AutoCompleteCustomSource = col;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                List<KDT_VNACC_HangGiayPhep_SMA> hangColl = new List<KDT_VNACC_HangGiayPhep_SMA>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            hangColl.Add((KDT_VNACC_HangGiayPhep_SMA)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangGiayPhep_SMA hmd in hangColl)
                    {
                        if (hmd.ID > 0)
                            hmd.Delete();
                        GiayPhep_SMA.HangCollection.Remove(hmd);
                    }

                    grdHang.DataSource = GiayPhep_SMA.HangCollection;
                    try
                    {
                        grdHang.Refetch();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grdHang_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                // List<KDT_VNACC_HangGiayPhep_SMA> hangColl = new List<KDT_VNACC_HangGiayPhep_SMA>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                HangGiayPhep = (KDT_VNACC_HangGiayPhep_SMA)items[0].GetRow().DataRow;
                SetHangGiayPhep(HangGiayPhep);
                isAddNew = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void txtMaSoHangHoa_Leave(object sender, EventArgs e)
        {
            try
            {
                string MoTa = MaHS.CheckExist(txtMaSoHangHoa.Text);
                if (MoTa == "")
                {
                    txtMaSoHangHoa.Focus();
                    epError.SetIconPadding(txtMaSoHangHoa, -8);
                    epError.SetError(txtMaSoHangHoa, setText("Mã HS không có trong danh mục mã HS.", "This HS is not exist"));
                }
                else
                {
                    epError.SetError(txtMaSoHangHoa, string.Empty);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtTenThuocQuyCachDongGoi.Tag = "MDC"; //Tên thuốc, hàm lượng, dạng bào chế, quy cách đóng gói
                txtMaSoHangHoa.Tag = "HSC"; //Mã số hàng hóa
                txtHoatChat.Tag = "AET"; //Hoạt chất
                txtTieuChuanChatLuong.Tag = "QST"; //Tiêu chuẩn chất lượng
                txtSoDangKy.Tag = "RGN"; //Số đăng ký
                dtHanDung.Tag = "EOM"; //Hạn dùng
                txtSoLuong.Tag = "QT"; //Số lượng 
                ucDonViTinhSoLuong.TagName = "QTU"; //Đơn vị tính số lượng
                txtTenHoatChatGayNghien.Tag = "QEA"; //Tên hoạt chất gây nghiện 
                txtCongDung.Tag = "EFE"; //Công dụng
                txtTongSoKhoiLuongHoatChatGayNghien.Tag = "TW"; //Tổng số khối lượng hoạt chất gây nghiện
                ucDonViTinhKhoiLuong.TagName = "TWU"; //Đơn vị tính
                txtGiaNhapKhauVND.Tag = "IMP"; //Giá nhập khẩu (VNĐ)
                txtGiaBanBuonVND.Tag = "SWP"; //Giá bán buôn (VNĐ)
                txtGiaBanLeVND.Tag = "ESP"; //Giá bán lẻ (VNĐ)
                txtTenThuongNhanXuatKhau.Tag = "EFN"; //Tên của công ty xuất khẩu
                txtMaBuuChinhXuatKhau.Tag = "EXC"; //Mã bưu chính của địa chỉ công ty xuất khẩu
                txtSoNhaTenDuongXuatKhau.Tag = "EFA"; //Địa chỉ của công ty xuất khẩu (Số nhà, tên đường)
                txtPhuongXaXuatKhau.Tag = "EFS"; //Địa chỉ của công ty xuất khẩu (Phường, xã)
                txtQuanHuyenXuatKhau.Tag = "EFD"; //Địa chỉ của công ty xuất khẩu (Quận, huyện, thị xã)
                txtTinhThanhPhoXuatKhau.Tag = "EFC"; //Địa chỉ của công ty xuất khẩu (Tỉnh, thành phố)
                ucMaQuocGiaXuatKhau.TagName = "ECC"; //Địa chỉ của công ty xuất khẩu (Mã quốc gia)
                txtTenCongTySanXuat.Tag = "MFN"; //Tên của công ty sản xuất
                txtMaBuuChinhCongTySanXuat.Tag = "MPC"; //Mã bưu chính của địa chỉ công ty sản xuất
                txtSoNhaTenDuongCongTySanXuat.Tag = "MFA"; //Địa chỉ của công ty sản xuất (Số nhà, tên đường)
                txtPhuongXaCongTySanXuat.Tag = "MFS"; //Địa chỉ của công ty sản xuất (Phường, xã)
                txtQuanHuyenCongTySanXuat.Tag = "MFD"; //Địa chỉ của công ty sản xuất (Quận, huyện, thị xã)
                txtTinhThanhPhoCongTySanXuat.Tag = "MFC"; //Địa chỉ của công ty sản xuất (Tỉnh, thành phố)
                ucMaQuocGiaCongTySanXuat.TagName = "MCC"; //Địa chỉ của công ty sản xuất (Mã quốc gia)
                txtTenCongTyCungCap.Tag = "DBN"; //Tên của công ty cung cấp
                txtMaBuuChinhCongTyCungCap.Tag = "DPC"; //Mã bưu chính của công ty cung cấp
                txtSoNhaTenDuongCongTyCungCap.Tag = "DAB"; //Địa chỉ của công ty cung cấp (Số nhà, tên đường)
                txtPhuongXaCongTyCungCap.Tag = "DAS"; //Địa chỉ của công ty cung cấp (Phường, xã)
                txtQuanHuyenCongTyCungCap.Tag = "DAD"; //Địa chỉ của công ty cung cấp (Quận, huyện, thị xã)
                txtTinhThanhPhoCongTyCungCap.Tag = "DAN"; //Địa chỉ của công ty cung cấp (Tỉnh, thành phố)
                ucMaQuocGiaCongTyCungCap.TagName = "DCC"; //Địa chỉ của công ty cung cấp (Mã quốc gia)
                txtTenDonViUyThac.Tag = "TPN"; //Tên của đơn vị ủy thác
                txtMaBuuChinhDonViUyThac.Tag = "PPC"; //Mã bưu chính của đơn vị ủy thác
                txtSoNhaTenDuongDonViUyThac.Tag = "TPB"; //Địa chỉ của đơn vị ủy thác (Số nhà, tên đường)
                txtPhuongXaDonViUyThac.Tag = "TPS"; //Địa chỉ của đơn vị ủy thác (Phường, xã)
                txtQuanHuyenDonViUyThac.Tag = "TPD"; //Địa chỉ của đơn vị ủy thác (Quận, huyện, thị xã)
                txtTinhThanhPhoDonViUyThac.Tag = "TPC"; //Địa chỉ của đơn vị ủy thác (Tỉnh, thành phố)
                ucMaQuocGiaDonViUyThac.TagName = "TCC"; //Địa chỉ của đơn vị ủy thác (Mã quốc gia)
                txtLuongTonKhoKyTruoc.Tag = "QS"; //Lượng tồn kho kỳ trước
                ucDonViTinhLuongTonKyTruoc.TagName = "QSU"; //Đơn vị tính
                txtLuongNhapTrongKy.Tag = "QI"; //Lượng nhập trong kỳ
                txtTongSo.Tag = "TQ"; //Tổng số
                txtTongSoXuatTrongKy.Tag = "TE"; //Tổng số xuất trong kỳ
                txtSoLuongTonKhoDenNgay.Tag = "ISD"; //Số lượng tồn kho đến ngày 
                txtHuHao.Tag = "DMD"; //Hư hao
                txtGhiChuChiTiet.Tag = "REM"; //Ghi chú (chi tiết)

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateControl(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtTenThuocQuyCachDongGoi.MaxLength = 768;
                txtMaSoHangHoa.MaxLength = 12;
                txtHoatChat.MaxLength = 35;
                txtTieuChuanChatLuong.MaxLength = 35;
                txtSoDangKy.MaxLength = 17;
                //dtHanDung.MaxLength = 8;
                txtSoLuong.MaxLength = 8; txtSoLuong.DecimalDigits = 0;
                //txtDonViTinhSoLuong.MaxLength = 3;
                txtTenHoatChatGayNghien.MaxLength = 70;
                txtCongDung.MaxLength = 50;
                txtTongSoKhoiLuongHoatChatGayNghien.MaxLength = 10;
                //txtDonViTinhKhoiLuong.MaxLength = 3;
                txtGiaNhapKhauVND.MaxLength = 19; txtGiaNhapKhauVND.DecimalDigits = 3;
                txtGiaBanBuonVND.MaxLength = 19; txtGiaBanBuonVND.DecimalDigits = 3;
                txtGiaBanLeVND.MaxLength = 19; txtGiaBanLeVND.DecimalDigits = 3;
                txtTenThuongNhanXuatKhau.MaxLength = 70;
                txtMaBuuChinhXuatKhau.MaxLength = 9;
                txtSoNhaTenDuongXuatKhau.MaxLength = 35;
                txtPhuongXaXuatKhau.MaxLength = 35;
                txtQuanHuyenXuatKhau.MaxLength = 35;
                txtTinhThanhPhoXuatKhau.MaxLength = 35;
                //txtMaQuocGiaXuatKhau.MaxLength = 2;
                txtTenCongTySanXuat.MaxLength = 70;
                txtMaBuuChinhCongTySanXuat.MaxLength = 9;
                txtSoNhaTenDuongCongTySanXuat.MaxLength = 35;
                txtPhuongXaCongTySanXuat.MaxLength = 35;
                txtQuanHuyenCongTySanXuat.MaxLength = 35;
                txtTinhThanhPhoCongTySanXuat.MaxLength = 35;
                //txtMaQuocGiaCongTySanXuat.MaxLength = 2;
                txtTenCongTyCungCap.MaxLength = 70;
                txtMaBuuChinhCongTyCungCap.MaxLength = 9;
                txtSoNhaTenDuongCongTyCungCap.MaxLength = 35;
                txtPhuongXaCongTyCungCap.MaxLength = 35;
                txtQuanHuyenCongTyCungCap.MaxLength = 35;
                txtTinhThanhPhoCongTyCungCap.MaxLength = 35;
                //txtMaQuocGiaCongTyCungCap.MaxLength = 2;
                txtTenDonViUyThac.MaxLength = 70;
                txtMaBuuChinhDonViUyThac.MaxLength = 9;
                txtSoNhaTenDuongDonViUyThac.MaxLength = 35;
                txtPhuongXaDonViUyThac.MaxLength = 35;
                txtQuanHuyenDonViUyThac.MaxLength = 35;
                txtTinhThanhPhoDonViUyThac.MaxLength = 35;
                //txtMaQuocGiaDonViUyThac.MaxLength = 2;
                txtLuongTonKhoKyTruoc.MaxLength = 8; txtLuongTonKhoKyTruoc.DecimalDigits = 0;
                //txtDonViTinhLuongTonKyTruoc.MaxLength = 3;
                txtLuongNhapTrongKy.MaxLength = 8; txtLuongNhapTrongKy.DecimalDigits = 0;
                txtTongSo.MaxLength = 9; txtTongSo.DecimalDigits = 0;
                txtTongSoXuatTrongKy.MaxLength = 9; txtTongSoXuatTrongKy.DecimalDigits = 0;
                txtSoLuongTonKhoDenNgay.MaxLength = 8; txtSoLuongTonKhoDenNgay.DecimalDigits = 0;
                txtHuHao.MaxLength = 8; txtHuHao.DecimalDigits = 0;
                txtGhiChuChiTiet.MaxLength = 105;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            txtMaSoHangHoa.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtHoatChat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTieuChuanChatLuong.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoDangKy.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtDonViTinhSoLuong.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenHoatChatGayNghien.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtCongDung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtDonViTinhKhoiLuong.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenThuongNhanXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinhXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoNhaTenDuongXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtPhuongXaXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtQuanHuyenXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTinhThanhPhoXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaQuocGiaXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenCongTySanXuat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinhCongTySanXuat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoNhaTenDuongCongTySanXuat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtPhuongXaCongTySanXuat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtQuanHuyenCongTySanXuat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTinhThanhPhoCongTySanXuat.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaQuocGiaCongTySanXuat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenCongTyCungCap.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinhCongTyCungCap.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoNhaTenDuongCongTyCungCap.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtPhuongXaCongTyCungCap.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtQuanHuyenCongTyCungCap.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTinhThanhPhoCongTyCungCap.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaQuocGiaCongTyCungCap.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenDonViUyThac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinhDonViUyThac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoNhaTenDuongDonViUyThac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtPhuongXaDonViUyThac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtQuanHuyenDonViUyThac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTinhThanhPhoDonViUyThac.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaQuocGiaDonViUyThac.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtDonViTinhLuongTonKyTruoc.TextChanged += new EventHandler(SetTextChanged_Handler);

            txtMaSoHangHoa.CharacterCasing = CharacterCasing.Upper;
            txtHoatChat.CharacterCasing = CharacterCasing.Upper;
            txtTieuChuanChatLuong.CharacterCasing = CharacterCasing.Upper;
            txtSoDangKy.CharacterCasing = CharacterCasing.Upper;
            ucDonViTinhSoLuong.IsUpperCase = true;
            txtTenHoatChatGayNghien.CharacterCasing = CharacterCasing.Upper;
            txtCongDung.CharacterCasing = CharacterCasing.Upper;
            ucDonViTinhKhoiLuong.IsUpperCase = true;
            txtTenThuongNhanXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtMaBuuChinhXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtSoNhaTenDuongXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtPhuongXaXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtQuanHuyenXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtTinhThanhPhoXuatKhau.CharacterCasing = CharacterCasing.Upper;
            ucMaQuocGiaXuatKhau.IsUpperCase = true;
            txtTenCongTySanXuat.CharacterCasing = CharacterCasing.Upper;
            txtMaBuuChinhCongTySanXuat.CharacterCasing = CharacterCasing.Upper;
            txtSoNhaTenDuongCongTySanXuat.CharacterCasing = CharacterCasing.Upper;
            txtPhuongXaCongTySanXuat.CharacterCasing = CharacterCasing.Upper;
            txtQuanHuyenCongTySanXuat.CharacterCasing = CharacterCasing.Upper;
            txtTinhThanhPhoCongTySanXuat.CharacterCasing = CharacterCasing.Upper;
            ucMaQuocGiaCongTySanXuat.IsUpperCase = true;
            txtTenCongTyCungCap.CharacterCasing = CharacterCasing.Upper;
            txtMaBuuChinhCongTyCungCap.CharacterCasing = CharacterCasing.Upper;
            txtSoNhaTenDuongCongTyCungCap.CharacterCasing = CharacterCasing.Upper;
            txtPhuongXaCongTyCungCap.CharacterCasing = CharacterCasing.Upper;
            txtQuanHuyenCongTyCungCap.CharacterCasing = CharacterCasing.Upper;
            txtTinhThanhPhoCongTyCungCap.CharacterCasing = CharacterCasing.Upper;
            ucMaQuocGiaCongTyCungCap.IsUpperCase = true;
            txtTenDonViUyThac.CharacterCasing = CharacterCasing.Upper;
            txtMaBuuChinhDonViUyThac.CharacterCasing = CharacterCasing.Upper;
            txtSoNhaTenDuongDonViUyThac.CharacterCasing = CharacterCasing.Upper;
            txtPhuongXaDonViUyThac.CharacterCasing = CharacterCasing.Upper;
            txtQuanHuyenDonViUyThac.CharacterCasing = CharacterCasing.Upper;
            txtTinhThanhPhoDonViUyThac.CharacterCasing = CharacterCasing.Upper;
            ucMaQuocGiaDonViUyThac.IsUpperCase = true;
            ucDonViTinhLuongTonKyTruoc.IsUpperCase = true;

        }

    }
}
