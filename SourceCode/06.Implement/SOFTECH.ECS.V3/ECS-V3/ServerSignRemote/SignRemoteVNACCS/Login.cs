﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using ServerRemote;

namespace SignRemoteVNACCS
{
    public partial class Login : Form
    {
        public bool IsSuccess = false;
        public Login()
        {
            InitializeComponent();
        }
        public static string EncryptPassword(string password)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] hashBytes = encoding.GetBytes(password);

            // compute SHA-1 hash.
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] bytePassword = sha1.ComputeHash(hashBytes);
            string cryptPassword = Convert.ToBase64String(bytePassword);

            return cryptPassword;
        }
        public static string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }
        public static long ValidateLogin(string username, string password)
        {
            long newID;
            string cryptPassword = GetMD5Value(password);

            User_DaiLy dataUser = new User_DaiLy();

            if ((newID = User_DaiLy.ValidateLoginAdmin(username, cryptPassword)) > 0)
                return newID;
            else
                return 0;
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                long ID = ValidateLogin(txtUserName.Text,txtPassword.Text);
                if (ID > 0)
                {
                    SignRemoteManagement.IsLoginSuccess = true;
                    Close();
                }
                else
	            {
                    MessageBoxControl.ShowMessage("Tên đăng nhập hoặc mật khẩu không đúng .");
                    SignRemoteManagement.IsLoginSuccess = false;
	            }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ConfigConnectDatabase f = new ConfigConnectDatabase();
            f.Activate();
            f.ShowDialog(this);
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.Activate();
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                btnLogin_Click(null,null);
            }
        }
    }
}
