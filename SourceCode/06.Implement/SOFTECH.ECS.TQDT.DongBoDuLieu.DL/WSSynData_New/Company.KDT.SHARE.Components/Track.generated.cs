using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components
{
	public partial class Track
	{
		#region Properties.
		
		public int ID { set; get; }
		public string TKMD_GUIDSTR { set; get; }
		public int SoToKhai { set; get; }
		public int NamDangKy { set; get; }
		public string MaLoaiHinh { set; get; }
		public string MaHaiQuan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public int IDUserDaiLy { set; get; }
		public DateTime NgayDongBo { set; get; }
		public int TrangThaiDongBoTaiLen { set; get; }
		public int TrangThaiDongBoTaiXuong { set; get; }
		public string GhiChu { set; get; }
		public string UserName { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<Track> ConvertToCollection(IDataReader reader)
		{
			IList<Track> collection = new List<Track>();
			while (reader.Read())
			{
				Track entity = new Track();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_GUIDSTR"))) entity.TKMD_GUIDSTR = reader.GetString(reader.GetOrdinal("TKMD_GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt32(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDUserDaiLy"))) entity.IDUserDaiLy = reader.GetInt32(reader.GetOrdinal("IDUserDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDongBo"))) entity.NgayDongBo = reader.GetDateTime(reader.GetOrdinal("NgayDongBo"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiDongBoTaiLen"))) entity.TrangThaiDongBoTaiLen = reader.GetInt32(reader.GetOrdinal("TrangThaiDongBoTaiLen"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiDongBoTaiXuong"))) entity.TrangThaiDongBoTaiXuong = reader.GetInt32(reader.GetOrdinal("TrangThaiDongBoTaiXuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static Track Load(int id)
		{
			const string spName = "[dbo].[p_DongBoDuLieu_Track_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<Track> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<Track> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<Track> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<Track> SelectCollectionBy_IDUserDaiLy(int iDUserDaiLy)
		{
            IDataReader reader = SelectReaderBy_IDUserDaiLy(iDUserDaiLy);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_IDUserDaiLy(int iDUserDaiLy)
		{
			const string spName = "[dbo].[p_DongBoDuLieu_Track_SelectBy_IDUserDaiLy]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDUserDaiLy", SqlDbType.Int, iDUserDaiLy);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_DongBoDuLieu_Track_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_DongBoDuLieu_Track_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_DongBoDuLieu_Track_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_DongBoDuLieu_Track_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_IDUserDaiLy(int iDUserDaiLy)
		{
			const string spName = "p_DongBoDuLieu_Track_SelectBy_IDUserDaiLy";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDUserDaiLy", SqlDbType.Int, iDUserDaiLy);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertTrack(string tKMD_GUIDSTR, int soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep, int iDUserDaiLy, DateTime ngayDongBo, int trangThaiDongBoTaiLen, int trangThaiDongBoTaiXuong, string ghiChu, string userName)
		{
			Track entity = new Track();	
			entity.TKMD_GUIDSTR = tKMD_GUIDSTR;
			entity.SoToKhai = soToKhai;
			entity.NamDangKy = namDangKy;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.IDUserDaiLy = iDUserDaiLy;
			entity.NgayDongBo = ngayDongBo;
			entity.TrangThaiDongBoTaiLen = trangThaiDongBoTaiLen;
			entity.TrangThaiDongBoTaiXuong = trangThaiDongBoTaiXuong;
			entity.GhiChu = ghiChu;
			entity.UserName = userName;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_DongBoDuLieu_Track_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@TKMD_GUIDSTR", SqlDbType.NVarChar, TKMD_GUIDSTR);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@IDUserDaiLy", SqlDbType.Int, IDUserDaiLy);
			db.AddInParameter(dbCommand, "@NgayDongBo", SqlDbType.DateTime, NgayDongBo.Year <= 1753 ? DBNull.Value : (object) NgayDongBo);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoTaiLen", SqlDbType.Int, TrangThaiDongBoTaiLen);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoTaiXuong", SqlDbType.Int, TrangThaiDongBoTaiXuong);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.NVarChar, UserName);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<Track> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Track item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateTrack(int id, string tKMD_GUIDSTR, int soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep, int iDUserDaiLy, DateTime ngayDongBo, int trangThaiDongBoTaiLen, int trangThaiDongBoTaiXuong, string ghiChu, string userName)
		{
			Track entity = new Track();			
			entity.ID = id;
			entity.TKMD_GUIDSTR = tKMD_GUIDSTR;
			entity.SoToKhai = soToKhai;
			entity.NamDangKy = namDangKy;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.IDUserDaiLy = iDUserDaiLy;
			entity.NgayDongBo = ngayDongBo;
			entity.TrangThaiDongBoTaiLen = trangThaiDongBoTaiLen;
			entity.TrangThaiDongBoTaiXuong = trangThaiDongBoTaiXuong;
			entity.GhiChu = ghiChu;
			entity.UserName = userName;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_DongBoDuLieu_Track_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@TKMD_GUIDSTR", SqlDbType.NVarChar, TKMD_GUIDSTR);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@IDUserDaiLy", SqlDbType.Int, IDUserDaiLy);
			db.AddInParameter(dbCommand, "@NgayDongBo", SqlDbType.DateTime, NgayDongBo.Year <= 1753 ? DBNull.Value : (object) NgayDongBo);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoTaiLen", SqlDbType.Int, TrangThaiDongBoTaiLen);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoTaiXuong", SqlDbType.Int, TrangThaiDongBoTaiXuong);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.NVarChar, UserName);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<Track> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Track item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateTrack(int id, string tKMD_GUIDSTR, int soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep, int iDUserDaiLy, DateTime ngayDongBo, int trangThaiDongBoTaiLen, int trangThaiDongBoTaiXuong, string ghiChu, string userName)
		{
			Track entity = new Track();			
			entity.ID = id;
			entity.TKMD_GUIDSTR = tKMD_GUIDSTR;
			entity.SoToKhai = soToKhai;
			entity.NamDangKy = namDangKy;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.IDUserDaiLy = iDUserDaiLy;
			entity.NgayDongBo = ngayDongBo;
			entity.TrangThaiDongBoTaiLen = trangThaiDongBoTaiLen;
			entity.TrangThaiDongBoTaiXuong = trangThaiDongBoTaiXuong;
			entity.GhiChu = ghiChu;
			entity.UserName = userName;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_DongBoDuLieu_Track_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@TKMD_GUIDSTR", SqlDbType.NVarChar, TKMD_GUIDSTR);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@IDUserDaiLy", SqlDbType.Int, IDUserDaiLy);
			db.AddInParameter(dbCommand, "@NgayDongBo", SqlDbType.DateTime, NgayDongBo.Year <= 1753 ? DBNull.Value : (object) NgayDongBo);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoTaiLen", SqlDbType.Int, TrangThaiDongBoTaiLen);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoTaiXuong", SqlDbType.Int, TrangThaiDongBoTaiXuong);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.NVarChar, UserName);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<Track> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Track item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteTrack(int id)
		{
			Track entity = new Track();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_DongBoDuLieu_Track_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_IDUserDaiLy(int iDUserDaiLy)
		{
			const string spName = "[dbo].[p_DongBoDuLieu_Track_DeleteBy_IDUserDaiLy]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDUserDaiLy", SqlDbType.Int, iDUserDaiLy);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_DongBoDuLieu_Track_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<Track> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Track item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}