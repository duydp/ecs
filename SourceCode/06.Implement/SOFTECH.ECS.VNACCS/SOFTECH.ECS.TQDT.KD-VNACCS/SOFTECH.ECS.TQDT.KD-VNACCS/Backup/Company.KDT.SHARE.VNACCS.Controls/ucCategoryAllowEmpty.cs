﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;
using System.Reflection;
using DevExpress.Data.Filtering;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public partial class ucCategoryAllowEmpty : ucBase
    {
        private string whereCondition = "";
        private string orderBy = "";
        private string valueMember = "Code";
        private string displayMember = "Name";
        private string valueMemberCaption = "Mã";
        private string displayMemberCaption = "Tên";
        public string Where { get; set; }
        private bool _showColumnCode = true;
        public bool ShowColumnCode
        {
            get { return _showColumnCode; }
            set { _showColumnCode = value; }
        }

        private bool _showColumnName = true;
        public bool ShowColumnName
        {
            get { return _showColumnName; }
            set { _showColumnName = value; }
        }

        DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit lookUpProperties;
        public ucCategoryAllowEmpty()
        {
            InitializeComponent();
            lookUpProperties = cboCategory.Properties;
            lookUpProperties.ImmediatePopup = true;
            cboCategory.Properties.NullText = "";
            // cboCategory.Popup += new EventHandler(gridLookUpEdit1_Popup);
        }
        public string Ten
        {
            set 
            {
                if (this.Code.Contains("ZZZ"))
                {
                    txtTen.Text = value;
                    txtTen.Visible = true;
                    cboCategory.Visible = false;
                }
            }
        }
        private ECategory _categoryType = ECategory.E001;
        /// <summary>
        /// Xác định loại danh mục.
        /// </summary>
        public ECategory CategoryType
        {
            get { return _categoryType; }
            set { _categoryType = value; }
        }

        private string _referenceDB;
        /// <summary>
        /// Xác định mã loại danh mục
        /// </summary>
        public string ReferenceDB
        {
            get
            {
                _referenceDB = CategoryType.ToString();

                return _referenceDB;
            }
            //set { _referenceDB = value; }
        }

        /// <summary>
        /// Mã
        /// </summary>
        public string Code
        {
            get
            {
                if (SetValidate)
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return txtCode.Text.Trim();
                    }
                }
                else
                    return txtCode.Text.Trim();
            }
            set { txtCode.Text = value; }
        }

        /// <summary>
        /// Tên Tiếng việt
        /// </summary>
        public string Name_VN
        {
            get
            {
                if (SetValidate)
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        if (cboCategory.EditValue.ToString().Contains("ZZZ"))
                            return txtTen.Text;
                        else
                            return cboCategory.Text.Trim();
                    }
                }
                else
                {
                    if (cboCategory.EditValue.ToString().Contains("ZZZ"))
                        return txtTen.Text;
                    else
                        return cboCategory.Text.Trim();
                }
            }
            set  
            {
                cboCategory.EditValue = value;

                if (value.Contains("ZZZ"))
                {
                    txtTen.Visible = true;
                    cboCategory.Visible = false;
                }
                else
                {
                    txtTen.Visible = false;
                    cboCategory.Visible = true;
                }
            } 
            //cboCategory.SelectedText = value; }
        }

        public string TagCode
        {
            get { return txtCode.Tag != null ? txtCode.Tag.ToString() : ""; }
            set { txtCode.Tag = value; }
        }

        public string TagName
        {
            get { return cboCategory.Tag != null ? cboCategory.Tag.ToString() : ""; }
            set { cboCategory.Tag = value; }
        }

        private EImportExport _importType = EImportExport.I;
        /// <summary>
        /// Xác định loại Nhập/ Xuất
        /// </summary>
        public EImportExport ImportType
        {
            get { return _importType; }
            set { _importType = value; }
        }

        /// <summary>
        /// NumberOfKeyItems: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm cặp khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class ContainerSize
        /// </summary>
        public string NumberOfKeyItems
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        List<VNACC_Category_ContainerSize> results = VNACC_Category_ContainerSize.SelectCollectionDynamic(Code != "" ? "ContainerSizeCode = '" + Code + "'" : "ContainerSizeName = '" + Name_VN + "'", "");
                        return results.Count > 0 ? results[0].NumberOfKeyItems.Trim() : "";
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        /// <summary>
        /// CountryCode: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm cặp khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class CityUNLOCODE
        /// </summary>
        public string CountryCode
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        List<VNACC_Category_CityUNLOCODE> results = VNACC_Category_CityUNLOCODE.SelectCollectionDynamic(Code != "" ? "CityCode = '" + Code + "'" : "CityNameOrStateName = '" + Name_VN + "'", "");
                        return results.Count > 0 ? results[0].CityCode.Trim() : "";
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        private bool _setValidate = false;
        public bool SetValidate
        {
            get { return _setValidate; }
            set { _setValidate = value; }
        }

        private bool _isValidate = false;
        public bool IsValidate
        {
            get
            {
                return SetValidate ? (_isValidate = ValidateControl(false)) : true;
            }
            set { _isValidate = value; }
        }

        private bool _setOnlyWarning = false;
        public bool SetOnlyWarning
        {
            get { return _setOnlyWarning; }
            set { _setOnlyWarning = value; }
        }

        private bool _isOnlyWarning = false;
        public bool IsOnlyWarning
        {
            get
            {
                return SetOnlyWarning ? (_isOnlyWarning = ValidateControl(true)) : false;
            }
            set
            {
                _isOnlyWarning = false;
            }
        }
        public bool IsUpperCase
        {
            set
            {
                if (value == true)
                {
                    txtCode.Properties.CharacterCasing = CharacterCasing.Upper;
                    cboCategory.Properties.CharacterCasing = CharacterCasing.Upper;
                }
                else
                {
                    txtCode.Properties.CharacterCasing = CharacterCasing.Normal;
                    cboCategory.Properties.CharacterCasing = CharacterCasing.Normal;
                }
            }
        }

        /// <summary>
        /// CustomsSubSectionCode: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class Nhóm xử lý hồ sơ (Customs-Sub Section)
        /// </summary>
        public string CustomsSubSectionCode
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        List<VNACC_Category_CustomsSubSection> results = VNACC_Category_CustomsSubSection.SelectCollectionDynamic(Code != "" ? "CustomsSubSectionCode = '" + Code + "'" : "Name = '" + Name_VN + "'", "");
                        return results.Count > 0 ? results[0].Name.Trim() : "";
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }


        private void ucCategoryAllowEmpty_Load(object sender, EventArgs e)
        {
            LoadData(false);

            txtCode.Enter += new EventHandler(txtCode_Enter);
            cboCategory.Enter += new EventHandler(cboCategory_Enter);
        }

        public delegate void EnterHandle(object sender, EventArgs e);
        public event EnterHandle OnEnter;
        void cboCategory_Enter(object sender, EventArgs e)
        {
            if (OnEnter != null)
            {
                OnEnter(sender, e);
            }
        }
        void txtCode_Enter(object sender, EventArgs e)
        {
            if (OnEnter != null)
            {
                OnEnter(sender, e);
            }
        }

        private void LoadData(bool isReload)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                whereCondition = "";
                whereCondition += base.WhereCondition != "" ? base.WhereCondition + " And " : "";
                //DevExpress.XtraEditors.Controls.ComboBoxItemCollection items = cboCategory.Properties.Items;
                //List<string> items = new List<string>();
                cboCategory.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                switch (CategoryType)
                {
                    //case ECategory.A001:
                    //    break;
                        
                    case ECategory.A014: //Nhóm xử lý hồ sơ
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%' and ImportExportClassification = '{1}'", ReferenceDB.ToString(), ImportType.ToString());
                        orderBy = "CustomsSubSectionCode asc";
                        valueMember = "ID"; //"CustomsSubSectionCode";
                        displayMember = "Name";
                        valueMemberCaption = "Id";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCustomsSubSections = VNACC_Category_CustomsSubSection.SelectCollectionBy(ucBase.dataCustomsSubSectionGlobals, ReferenceDB.ToString(), ImportType.ToString());

                        //                         Specify the data source to display in the dropdown.
                        //                                                 items = cboCategory.Properties.Items;
                        //                                                 items.Clear();
                        //                         
                        //                                                 items.BeginUpdate();
                        //                                                 try
                        //                                                 {
                        //                                                     foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsSubSection item in dataCustomsSubSections)
                        //                                                     {
                        //                                                         items.Add(item.Name);
                        //                                                     }
                        //                                                 }
                        //                                                 finally
                        //                                                 {
                        //                                                     items.EndUpdate();
                        //                                                 }
                        cboCategory.Properties.DataSource = dataCustomsSubSections;
                        #endregion
                        break;

                    case ECategory.A015: // Mã nước(Country, coded), Mã nước xuất xứ
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "NationCode asc";
                        valueMember = "NationCode";
                        displayMember = "CountryShortName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataNations = VNACC_Category_Nation.SelectCollectionBy(ucBase.dataNationGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();

                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_Nation item in dataNations)
                        //    {
                        //        items.Add(item.CountryShortName);
                        //    }
                        //}
                        //finally
                        //{
                        //    items.EndUpdate();
                        //}
                        cboCategory.Properties.DataSource = dataNations;
                        #endregion
                        break;
                    case ECategory.A016: // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
                        #region Load data
                        whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        //orderBy = "CityCode asc";
                        valueMember = "LOCODE";
                        displayMember = "CityNameOrStateName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";
                        //Create obj and load data
                        //dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionAllMinimize2(Where);
                        if (Where == "I")
                            dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionBy(ucBase.dataCityGlobals2, ReferenceDB.ToString());
                        else if (!string.IsNullOrEmpty(Where))
                            dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionByCountryCode(ucBase.dataCityGlobals, Where);
                        else
                            dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionBy(ucBase.dataCityGlobals, ReferenceDB.ToString());
                        dataCitys.Add(new VNACC_Category_CityUNLOCODE { LOCODE = "", CityNameOrStateName = "" });
                        cboCategory.Properties.BestFitMode = BestFitMode.None;
                        //Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();

                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE item in dataCitys)
                        //    {
                        //        items.Add(item.CityNameOrStateName);
                        //    }
                        //}
                        //finally
                        //{
                        //    items.EndUpdate();
                        //}
                        
                        cboCategory.Properties.DataSource = dataCitys;
                        #endregion
                        break;
                    case ECategory.A016T: // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
                        #region Load data
                        whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        //orderBy = "CityCode asc";
                        valueMember = "LOCODE";
                        displayMember = "CityNameOrStateName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";
                        //Create obj and load data
                        //dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionAllMinimize2(Where);
                        dataCitys = new List<VNACC_Category_CityUNLOCODE>();
                        //if (Where == "I")
                        //    dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionBy(ucBase.dataCityGlobals2, ReferenceDB.ToString());
                        // if (!string.IsNullOrEmpty(Where))
                        //    dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionByCountryCode(ucBase.dataCityGlobals, Where);
                        //else
                        //    dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionBy(ucBase.dataCityGlobals, ReferenceDB.ToString());
                        //dataCitys.Add(new VNACC_Category_CityUNLOCODE { LOCODE = "", CityNameOrStateName = "" });
                        cboCategory.Properties.BestFitMode = BestFitMode.None;
                        //Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();

                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE item in dataCitys)
                        //    {
                        //        items.Add(item.CityNameOrStateName);
                        //    }
                        //}
                        //finally
                        //{
                        //    items.EndUpdate();
                        //}

                        cboCategory.Properties.DataSource = dataCitys;
                        #endregion
                        break;

                    //case ECategory.A017:
                    //    break;
                    //case ECategory.A021:
                    //    break;

                    case ECategory.A038: //Cơ quan Hải quan
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "CustomsCode asc";
                        valueMember = "CustomsCode";
                        displayMember = "CustomsOfficeNameInVietnamese";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCustomsOffices = VNACC_Category_CustomsOffice.SelectCollectionBy(ucBase.dataCustomsOfficeGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();

                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsOffice item in dataCustomsOffices)
                        //    {
                        //        items.Add(item.CustomsOfficeNameInVietnamese);
                        //    }
                        //}
                        //finally
                        //{
                        //    items.EndUpdate();
                        //}
                        cboCategory.Properties.DataSource = dataCustomsOffices;
                        #endregion
                        break;


                    case ECategory.A202: // Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp), Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "BondedAreaCode asc";
                        valueMember = "BondedAreaCode";
                        displayMember = "BondedAreaName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCargos = VNACC_Category_Cargo.SelectCollectionBy(ucBase.dataCargoGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();

                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_Cargo item in dataCargos)
                        //    {
                        //        items.Add(item.BondedAreaName);
                        //    }
                        //}
                        //finally
                        //{
                        //    items.EndUpdate();
                        //}
                        cboCategory.Properties.DataSource = dataCargos;
                        #endregion
                        break;

                    case ECategory.A301: //Container size
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "ContainerSizeCode asc";
                        valueMember = "ContainerSizeCode";
                        displayMember = "ContainerSizeName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataContainerSizes = VNACC_Category_ContainerSize.SelectCollectionBy(ucBase.dataContainerSizeGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();

                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_ContainerSize item in dataContainerSizes)
                        //    {
                        //        items.Add(item.ContainerSizeName);
                        //    }
                        //}
                        //finally
                        //{
                        //    items.EndUpdate();
                        //}
                        cboCategory.Properties.DataSource = dataContainerSizes;
                        #endregion
                        break;

                    //case ECategory.B314:
                    //    break;

                    case ECategory.A316: //Mã đơn vị tính (Packages unit code)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "NumberOfPackagesUnitCode asc";
                        valueMember = "NumberOfPackagesUnitCode";
                        displayMember = "NumberOfPackagesUnitName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataPackagesUnits = VNACC_Category_PackagesUnit.SelectCollectionBy(ucBase.dataPackagesUnitGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();

                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_PackagesUnit item in dataPackagesUnits)
                        //    {
                        //        items.Add(item.NumberOfPackagesUnitName);
                        //    }
                        //}
                        //finally
                        //{
                        //    items.EndUpdate();
                        //}
                        cboCategory.Properties.DataSource = dataPackagesUnits;
                        #endregion
                        break;

                    case ECategory.A404: //Mã biểu thuế nhập khẩu
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "TaxCode asc";
                        valueMember = "TaxCode";
                        displayMember = "TaxName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataTaxClassificationCodes = VNACC_Category_TaxClassificationCode.SelectCollectionBy(ucBase.dataTaxClassificationCodeGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();
                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_TaxClassificationCode item in dataTaxClassificationCodes)
                        //    {
                        //        items.Add(item.TaxName);
                        //    }
                        //}
                        //finally { items.EndUpdate(); }
                        lookUpProperties.BestFitMode = BestFitMode.BestFitResizePopup;
                        cboCategory.Properties.DataSource = dataTaxClassificationCodes;
                        #endregion
                        break;

                    case ECategory.A501: // Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "Code asc";
                        valueMember = "Code";
                        displayMember = "Name";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataQuantityUnits = VNACC_Category_QuantityUnit.SelectCollectionBy(ucBase.dataQuantityUnitGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();
                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_QuantityUnit item in dataQuantityUnits)
                        //    {
                        //        items.Add(item.Name);
                        //    }
                        //}
                        //finally { items.EndUpdate(); }
                        cboCategory.Properties.DataSource = dataQuantityUnits;
                        #endregion
                        break;

                    case ECategory.A506: //Mã số hàng hóa (HS)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "HSCode asc";
                        valueMember = "HSCode";
                        displayMember = "";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataHSCodes = VNACC_Category_HSCode.SelectCollectionBy(ucBase.dataHSCodeGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();
                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_HSCode item in dataHSCodes)
                        //    {
                        //        items.Add(item.HSCode);
                        //    }
                        //}
                        //finally { items.EndUpdate(); }
                        cboCategory.Properties.DataSource = dataHSCodes;
                        #endregion
                        break;

                    //case ECategory.A515:
                    //    break;
                    //case ECategory.A517:
                    //    break;

                    case ECategory.A527: //Mã đồng tiền
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "CurrencyCode asc";
                        valueMember = "CurrencyCode";
                        displayMember = "CurrencyName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCurrencyExchanges = VNACC_Category_CurrencyExchange.SelectCollectionBy(ucBase.dataCurrencyExchangeGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();
                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_CurrencyExchange item in dataCurrencyExchanges)
                        //    {
                        //        items.Add(item.CurrencyName);
                        //    }
                        //}
                        //finally { items.EndUpdate(); }
                        cboCategory.Properties.DataSource = dataCurrencyExchanges;
                        #endregion
                        break;

                    case ECategory.A546: //Loại chứng từ đính kèm
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "ApplicationProcedureType asc";
                        valueMember = "ApplicationProcedureType";
                        displayMember = "ApplicationProcedureName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataApplicationProcedureTypes = VNACC_Category_ApplicationProcedureType.SelectCollectionBy(ucBase.dataApplicationProcedureTypeGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();
                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_ApplicationProcedureType item in dataApplicationProcedureTypes)
                        //    {
                        //        items.Add(item.ApplicationProcedureName);
                        //    }
                        //}
                        //finally { items.EndUpdate(); }
                        cboCategory.Properties.DataSource = dataApplicationProcedureTypes;
                        #endregion
                        break;

                    //case ECategory.B501:
                    //    break;
                    //case ECategory.B530:
                    //    break;
                    //case ECategory.B700:
                    //    break;
                    //case ECategory.B701:
                    //    break;

                    case ECategory.A601: //Mã địa điểm dỡ hàng (Stations)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "StationCode asc";
                        valueMember = "StationCode";
                        displayMember = "StationName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataStations = VNACC_Category_Station.SelectCollectionBy(ucBase.dataStationGlobals, ReferenceDB.ToString());
                        
                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();
                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_Station item in dataStations)
                        //    {
                        //        items.Add(item.StationName);
                        //    }
                        //}
                        //finally { items.EndUpdate(); }
                        cboCategory.Properties.DataSource = dataStations;
                        #endregion
                        break;

                    case ECategory.A620: //Mã địa điểm dỡ hàng (Border gate)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "BorderGateCode asc";
                        valueMember = "BorderGateCode";
                        displayMember = "BorderGateName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataBorderGates = VNACC_Category_BorderGate.SelectCollectionBy(ucBase.dataBorderGateGlobals, ReferenceDB.ToString());
                       
                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();
                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_BorderGate item in dataBorderGates)
                        //    {
                        //        items.Add(item.BorderGateName);
                        //    }
                        //}
                        //finally { items.EndUpdate(); }
                        cboCategory.Properties.DataSource = dataBorderGates;
                        #endregion
                        break;

                    case ECategory.A621: //Transport means (Vehicle)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "TransportMeansCode asc";
                        valueMember = "TransportMeansCode";
                        displayMember = "TransportMeansName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data                        
                        dataTransportMeans = VNACC_Category_TransportMean.SelectCollectionBy(ucBase.dataTransportMeanGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();
                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_TransportMean item in dataTransportMeans)
                        //    {
                        //        items.Add(item.TransportMeansName);
                        //    }
                        //}
                        //finally { items.EndUpdate(); }
                        cboCategory.Properties.DataSource = dataTransportMeans;
                        #endregion
                        break;

                    case ECategory.A700: //OGA User
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "OfficeOfApplicationCode asc";
                        valueMember = "OfficeOfApplicationCode";
                        displayMember = "OfficeOfApplicationName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataOGAUsers = VNACC_Category_OGAUser.SelectCollectionBy(ucBase.dataOGAUserGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();
                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_OGAUser item in dataOGAUsers)
                        //    {
                        //        items.Add(item.OfficeOfApplicationName);
                        //    }
                        //}
                        //finally { items.EndUpdate(); }
                        cboCategory.Properties.DataSource = dataOGAUsers;
                        #endregion
                        break;

                    case ECategory.A519: //Mã văn bản pháp quy khác
                    case ECategory.A520: //Mã miễn / Giảm / Không chịu thuế nhập khẩu
                    case ECategory.A521: //Mã miễn / Giảm / Không chịu thuế và thu khác
                    case ECategory.A522: //Mã áp dụng thuế suất / Mức thuế và thu khác
                    case ECategory.A402: //Mã xác định mức thuế nhập khẩu theo lượng
                    case ECategory.A528: //Phân loại giấy phép nhập khẩu

                    case ECategory.A401: //Mã tên khoản điều chỉnh
                    case ECategory.A557: //Mã phân loại phí vận chuyển     
                    case ECategory.B524: //Trạng thái khai báo VNACC
                    case ECategory.A553://Mã điều kiện giá hóa đơn
                    case ECategory.E001:
                    case ECategory.E002:
                    case ECategory.E003:
                    case ECategory.E004:
                    case ECategory.E005:
                    case ECategory.E006:
                    case ECategory.E007:
                    case ECategory.E008:
                    case ECategory.E009:
                    case ECategory.E010:
                    case ECategory.E011:
                    case ECategory.E012:
                    case ECategory.E013:
                    case ECategory.E014:
                    case ECategory.E015:
                    case ECategory.E016:
                    case ECategory.E017:
                    case ECategory.E018:
                    case ECategory.E019:
                    case ECategory.E020:
                    case ECategory.E021:
                    case ECategory.E022:
                    case ECategory.E023:
                    case ECategory.E024:
                    case ECategory.E025:
                    case ECategory.E026:
                    case ECategory.E027:
                    case ECategory.E028:
                    case ECategory.E029:
                    case ECategory.E030:
                    case ECategory.E031:
                    case ECategory.E032:
                    case ECategory.E033:
                        #region Load data
                        //whereCondition += string.Format("ReferenceDB = '{0}'", ReferenceDB.ToString());
                        orderBy = "Code asc";
                        valueMember = "Code";
                        displayMember = "Name_VN";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        datas = VNACC_Category_Common.SelectCollectionBy(ucBase.dataGlobals, ReferenceDB.ToString());

                        // Specify the data source to display in the dropdown.
                        //items = cboCategory.Properties.Items;
                        //items.Clear();
                        //items.BeginUpdate();
                        //try
                        //{
                        //    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_Common item in datas)
                        //    {
                        //        items.Add(item.Name_VN);
                        //    }
                        //}
                        //finally { items.EndUpdate(); }
                        cboCategory.Properties.DataSource = datas;
                        #endregion
                        break;
                }

                cboCategory.Properties.DisplayMember = displayMember;
                cboCategory.Properties.ValueMember = valueMember;
                lookUpProperties.View.Columns.Clear();
                try
                {
                    lookUpProperties.View.PopulateColumns(cboCategory.Properties.DataSource);
                }
                catch (Exception ex) { }
                for (int i = 0; i < lookUpProperties.View.Columns.Count; i++)
                {
                    if (lookUpProperties.View.Columns[i].FieldName != valueMember && lookUpProperties.View.Columns[i].FieldName != displayMember)
                    {
                        lookUpProperties.View.Columns[i].Visible = false;
                    }
                }
                lookUpProperties.View.Columns[valueMember].Caption = valueMemberCaption;
                lookUpProperties.View.Columns[displayMember].Caption = displayMemberCaption;
                //cboCategory.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                //lookUpProperties = cboCategory.Properties;
                //lookUpProperties.ImmediatePopup = true;
                //cboCategory.Properties.NullText = "";
                //cboCategory.Popup += new EventHandler(gridLookUpEdit1_Popup);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }
        }

        private string GetCodeOrName(bool isCode)
        {
            string _code = "";
            try
            {
                Cursor = Cursors.WaitCursor;
                whereCondition = "";
                whereCondition += string.Format("{0} = N'{1}'", isCode ? displayMember : valueMember, isCode ? cboCategory.Text : txtCode.Text.Trim());

                switch (CategoryType)
                {
                    //case ECategory.A001:
                    //    break;

                    case ECategory.A014: //Nhóm xử lý hồ sơ                        
                        List<VNACC_Category_CustomsSubSection> result1 = VNACC_Category_CustomsSubSection.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result1.Count > 0 ? (isCode ? result1[0].CustomsSubSectionCode.Trim() : result1[0].Name.Trim()) : "";
                        break;

                    case ECategory.A015: // Mã nước(Country, coded), Mã nước xuất xứ
                        List<VNACC_Category_Nation> result2 = VNACC_Category_Nation.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result2.Count > 0 ? (isCode ? result2[0].NationCode.Trim() : result2[0].CountryShortName.Trim()) : "";
                        break;
                    case ECategory.A016: // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
                        List<VNACC_Category_CityUNLOCODE> result3 = VNACC_Category_CityUNLOCODE.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result3.Count > 0 ? (isCode ? result3[0].CityCode.Trim() : result3[0].CityNameOrStateName.Trim()) : "";
                        break;

                    //case ECategory.A017:
                    //    break;
                    //case ECategory.A021:
                    //    break;

                    case ECategory.A038: //Cơ quan Hải quan
                        List<VNACC_Category_CustomsOffice> result4 = VNACC_Category_CustomsOffice.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result4.Count > 0 ? (isCode ? result4[0].CustomsCode.Trim() : result4[0].CustomsOfficeNameInVietnamese.Trim()) : "";
                        break;

                    case ECategory.A202: // Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp), Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                        List<VNACC_Category_Cargo> result5 = VNACC_Category_Cargo.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result5.Count > 0 ? (isCode ? result5[0].BondedAreaCode.Trim() : result5[0].BondedAreaName.Trim()) : "";
                        break;

                    case ECategory.A301: //Container size
                        List<VNACC_Category_ContainerSize> result6 = VNACC_Category_ContainerSize.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result6.Count > 0 ? (isCode ? result6[0].ContainerSizeCode.Trim() : result6[0].ContainerSizeName.Trim()) : "";
                        break;

                    //case ECategory.B314:
                    //    break;

                    case ECategory.A316: //Mã đơn vị tính (Packages unit code)
                        List<VNACC_Category_PackagesUnit> result7 = VNACC_Category_PackagesUnit.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result7.Count > 0 ? (isCode ? result7[0].NumberOfPackagesUnitCode.Trim() : result7[0].NumberOfPackagesUnitName.Trim()) : "";
                        break;

                    //case ECategory.A402:
                    //    break;

                    case ECategory.A404: //Mã biểu thuế nhập khẩu
                        List<VNACC_Category_TaxClassificationCode> result8 = VNACC_Category_TaxClassificationCode.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result8.Count > 0 ? (isCode ? result8[0].TaxCode.Trim() : result8[0].TaxName.Trim()) : "";
                        break;

                    case ECategory.A501: // Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng
                        List<VNACC_Category_QuantityUnit> result9 = VNACC_Category_QuantityUnit.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result9.Count > 0 ? (isCode ? result9[0].Code.Trim() : result9[0].Name.Trim()) : "";
                        break;

                    case ECategory.A506: //Mã số hàng hóa (HS)
                        List<VNACC_Category_HSCode> result10 = VNACC_Category_HSCode.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result10.Count > 0 ? (isCode ? result10[0].HSCode.Trim() : result10[0].HSCode.Trim()) : "";
                        break;

                    //case ECategory.A515:
                    //    break;
                    //case ECategory.A517:
                    //    break;
                    //case ECategory.A519:
                    //    break;
                    //case ECategory.A520:
                    //    break;
                    //case ECategory.A521:
                    //    break;
                    //case ECategory.A522:
                    //    break;

                    case ECategory.A527: //Mã đồng tiền
                        List<VNACC_Category_CurrencyExchange> result11 = VNACC_Category_CurrencyExchange.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result11.Count > 0 ? (isCode ? result11[0].CurrencyCode.Trim() : result11[0].CurrencyName.Trim()) : "";
                        break;

                    //case ECategory.A528:
                    //    break;

                    case ECategory.A546: //Loại chứng từ đính kèm
                        List<VNACC_Category_ApplicationProcedureType> result12 = VNACC_Category_ApplicationProcedureType.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result12.Count > 0 ? (isCode ? result12[0].ApplicationProcedureType.Trim() : result12[0].ApplicationProcedureName.Trim()) : "";
                        break;

                    //case ECategory.B501:
                    //    break;
                    //case ECategory.B530:
                    //    break;
                    //case ECategory.B700:
                    //    break;
                    //case ECategory.B701:
                    //    break;

                    case ECategory.A601: //Mã địa điểm dỡ hàng (Stations)
                        List<VNACC_Category_Station> result13 = VNACC_Category_Station.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result13.Count > 0 ? (isCode ? result13[0].StationCode.Trim() : result13[0].StationName.Trim()) : "";
                        break;

                    case ECategory.A620: //Mã địa điểm dỡ hàng (Border gate)
                        List<VNACC_Category_BorderGate> result14 = VNACC_Category_BorderGate.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result14.Count > 0 ? (isCode ? result14[0].BorderGateCode.Trim() : result14[0].BorderGateName.Trim()) : "";
                        break;

                    case ECategory.A621: //Transport means (Vehicle)
                        List<VNACC_Category_TransportMean> result15 = VNACC_Category_TransportMean.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result15.Count > 0 ? (isCode ? result15[0].TransportMeansCode.Trim() : result15[0].TransportMeansName.Trim()) : "";
                        break;

                    case ECategory.A700: //OGA User
                        List<VNACC_Category_OGAUser> result16 = VNACC_Category_OGAUser.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result16.Count > 0 ? (isCode ? result16[0].OfficeOfApplicationCode.Trim() : result16[0].OfficeOfApplicationName.Trim()) : "";
                        break;
                    case ECategory.A402: //Mã xác định mức thuế nhập khẩu theo lượng(ma ap dụng mưc thue tuyet doi)
                    case ECategory.A401: //Mã tên khoản điều chỉnh
                    case ECategory.A557: //Mã phân loại phí vận chuyển     
                    case ECategory.B524: //Trạng thái khai báo VNACC
                    case ECategory.E001:
                    case ECategory.E002:
                    case ECategory.E003:
                    case ECategory.E004:
                    case ECategory.E005:
                    case ECategory.E006:
                    case ECategory.E007:
                    case ECategory.E008:
                    case ECategory.E009:
                    case ECategory.E010:
                    case ECategory.E011:
                    case ECategory.E012:
                    case ECategory.E013:
                    case ECategory.E014:
                    case ECategory.E015:
                    case ECategory.E016:
                    case ECategory.E017:
                    case ECategory.E018:
                    case ECategory.E019:
                    case ECategory.E020:
                    case ECategory.E021:
                    case ECategory.E022:
                    case ECategory.E023:
                    case ECategory.E024:
                    case ECategory.E025:
                    case ECategory.E026:
                    case ECategory.E027:
                    case ECategory.E028:
                    case ECategory.E029:
                    case ECategory.E030:
                    case ECategory.E031:
                    case ECategory.E032:
                    case ECategory.E033:
                        List<VNACC_Category_Common> result17 = VNACC_Category_Common.SelectCollectionDynamic(whereCondition, orderBy);
                        _code = result17.Count > 0 ? (isCode ? result17[0].Code.Trim() : result17[0].Name_VN.Trim()) : "";
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return _code;
        }

        /// <summary>
        /// Tải mới lại dữ liệu danh mục
        /// </summary>
        public void ReLoadData()
        {
            LoadData(true);
        }

        private bool ValidateControl(bool isOnlyWarning)
        {
            bool isValid = true;
            bool isValidCode = true;
            bool isValidName = true;

            if (ShowColumnCode)
            {
                isValidCode &= ValidationControl.ValidateNullDevExpress(txtCode, dxErrorProvider, "", isOnlyWarning);
                isValid &= isValidCode;
            }

            if (ShowColumnName)
            {
                isValidName &= ValidationControl.ValidateNullComboBoxDevExpress(cboCategory, dxErrorProvider, "", isOnlyWarning);

                if (isValidName)
                {
                    isValidName &= ValidationControl.ValidateNullComboBoxDevExpress(cboCategory, dxErrorProvider, "", isOnlyWarning);
                }

                isValid &= isValidName;
            }

            return isValid;
        }

        public delegate void EditValueChangedHandle(object sender, EventArgs e);
        public event EditValueChangedHandle EditValueChanged;
        private void cboCategory_EditValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cboCategory.Text))
            {
                //string code = GetCodeOrName(true);
                txtCode.Text = cboCategory.EditValue.ToString();
            }

            if (EditValueChanged != null)
            {
                EditValueChanged(sender, e);
            }
        }

        private void txtCode_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCode.Text))
                Name_VN = txtCode.Text; // GetCodeOrName(false);
        }

        private void txtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCode.Text))
                Name_VN = txtCode.Text;// GetCodeOrName(false);
        }


        #region Khanhhn

        private void FilterLookup(object sender)
        {
            Text += " ! ";
            GridLookUpEdit edit = sender as GridLookUpEdit;
            GridView gridView = edit.Properties.View as GridView;
            FieldInfo fi = gridView.GetType().GetField("extraFilter", BindingFlags.NonPublic | BindingFlags.Instance);
            Text = edit.AutoSearchText;
            BinaryOperator op1 = new BinaryOperator(valueMember, "%" + edit.AutoSearchText + "%", BinaryOperatorType.Like);
            BinaryOperator op2 = new BinaryOperator(displayMember, "%" + edit.AutoSearchText + "%", BinaryOperatorType.Like);
            string filterCondition = new GroupOperator(GroupOperatorType.Or, new CriteriaOperator[] { op1, op2 }).ToString();
            fi.SetValue(gridView, filterCondition);

            MethodInfo mi = gridView.GetType().GetMethod("ApplyColumnsFilterEx", BindingFlags.NonPublic | BindingFlags.Instance);
            mi.Invoke(gridView, null);
        }
        void gridLookUpEdit1_Popup(object sender, EventArgs e)
        {

        }

        private void gridLookUpEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            this.BeginInvoke(new System.Windows.Forms.MethodInvoker(delegate
            {
                FilterLookup(sender);
            }));
        }

        private void AddNewObject<T>(string valueMember, T obj, object value)
        {
            PropertyInfo pro = typeof(T).GetProperty(valueMember);
            if (pro != null)
                pro.SetValue(obj, value, null);
            if (obj != null)
                (cboCategory.Properties.DataSource as List<T>).Add(obj);
        }
        public bool IsRequimentMa = false;
        public bool IsXNKTaiCho = false;
            private void cboCategory_ProcessNewValue(object sender, ProcessNewValueEventArgs e)
        {
            if (IsXNKTaiCho)
            {
                string valueMember = string.Empty;
                string value = e.DisplayValue.ToString();
                if (string.IsNullOrEmpty(value.Trim()))
                    return;
                switch (CategoryType)
                {
                    case ECategory.A016T: // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
                        #region  Add Item
                        valueMember = "CityCode";
                        VNACC_Category_CityUNLOCODE ItemA016 = new VNACC_Category_CityUNLOCODE();
                        //ItemA016.CityCode = IsRequimentMa ? txtCode.Text.Trim() : " ";
                        ItemA016.LOCODE = IsRequimentMa ? string.IsNullOrEmpty(txtCode.Text.Trim()) ? "VNZZZ" : txtCode.Text.Trim() : " ";
                        ItemA016.CityNameOrStateName = value;
                        List<VNACC_Category_CityUNLOCODE> list = (cboCategory.Properties.DataSource as List<VNACC_Category_CityUNLOCODE>);
                        list.Clear();
                        list.Add(ItemA016);
                        cboCategory.Properties.DataSource = list;
                        
                        //AddNewObject<VNACC_Category_CityUNLOCODE>(valueMember, ItemA016, value);
                        cboCategory.EditValue = ItemA016.LOCODE;
                        txtCode.Text = ItemA016.LOCODE;
                        #endregion
                        break;
                }
            }
        }

        #endregion
    }
}
