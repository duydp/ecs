namespace Company.Interface.SXXK
{
    partial class CapNhatHoSoThanhLyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CapNhatHoSoThanhLyForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanNewControl1 = new Company.Interface.Controls.DonViHaiQuanNewControl();
            this.label14 = new System.Windows.Forms.Label();
            this.txtQuy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLanThanhLy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblHinhThuc = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblTrangThaiTK = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSoHSTL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoQuyetDinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ccNgayThanhLy = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label2 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sendBK1 = new System.Windows.Forms.ToolStripMenuItem();
            this.huyBangKe1 = new System.Windows.Forms.ToolStripMenuItem();
            this.xacNhanThongTin1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.Luu1 = new Janus.Windows.UI.CommandBars.UICommand("Luu");
            this.cmdThanhKhoan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThanhKhoan");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdTT38_20151 = new Janus.Windows.UI.CommandBars.UICommand("cmdTT38_2015");
            this.cmdTT79KCX1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTT79KCX");
            this.cmdCanDoiNX1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCanDoiNX");
            this.cmdXemSPXuat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXemSPXuat");
            this.cmdChungTuThanhToan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuThanhToan");
            this.cmdChungTuTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuTT");
            this.cmdTQDT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTQDT");
            this.cmdFixAndRun1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFixAndRun");
            this.cmdNgayTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNgayTK");
            this.cmdDonGiaTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDonGiaTT");
            this.cmdSendNPLCU1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendNPLCU");
            this.cmdUpdateTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateTK");
            this.KhaiBao = new Janus.Windows.UI.CommandBars.UICommand("KhaiBao");
            this.NhanDuLieu = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.Huy = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.Luu = new Janus.Windows.UI.CommandBars.UICommand("Luu");
            this.cmdThanhLy = new Janus.Windows.UI.CommandBars.UICommand("ThanhLy");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdBKToKhaiNhap1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap");
            this.cmdBKToKhaiXuat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat");
            this.cmdBCNPLXuatNhapTon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon");
            this.cmdBCNPLXuatNhapTon_TongNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_TongNPL");
            this.cmdBCThueXNK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCThueXNK");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdBKToKhaiNhap_1961 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_196");
            this.cmdBKToKhaiXuat_1961 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_196");
            this.cmdBCNPLXuatNhapTon_1961 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_196");
            this.cmdBCNPLXuatNhapTon_TongNPL_1961 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_TongNPL_196");
            this.cmdBCThueXNK_1961 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCThueXNK_196");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdBKToKhaiNhap_1281 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_128");
            this.cmdBKToKhaiXuat_1281 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_128");
            this.cmdBCNPLXuatNhapTon_1281 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_128");
            this.cmdBCNPLXuatNhapTon_TongNPL_1281 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_TongNPL_128");
            this.cmdBCThueNK_1281 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCThueNK_128");
            this.cmdBaoCaoTonSauThanhKhoan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBaoCaoTonSauThanhKhoan");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdBKToKhaiNhap_TT381 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_TT38");
            this.cmdBKToKhaiXuat_TT381 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_TT38");
            this.cmdBKToKhaiNhapXinHoan_TT381 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhapXinHoan_TT38");
            this.cmdBKToKhaiNhapKhongThu_TT381 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhapKhongThu_TT38");
            this.cmdBCNPLXuatNhapTon = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon");
            this.cmdBCThueXNK = new Janus.Windows.UI.CommandBars.UICommand("cmdBCThueXNK");
            this.cmdBKToKhaiXuat = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat");
            this.cmdBKToKhaiNhap = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap");
            this.cmdDelete = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.cmdRollback = new Janus.Windows.UI.CommandBars.UICommand("cmdRollback");
            this.cmdClose = new Janus.Windows.UI.CommandBars.UICommand("cmdClose");
            this.cmdThanhKhoan = new Janus.Windows.UI.CommandBars.UICommand("cmdThanhKhoan");
            this.ThanhLy1 = new Janus.Windows.UI.CommandBars.UICommand("ThanhLy");
            this.cmdRollback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdRollback");
            this.cmdClose1 = new Janus.Windows.UI.CommandBars.UICommand("cmdClose");
            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdThanhKhoanTheoMaHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThanhKhoanTheoMaHang");
            this.cmdKhaiDT = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiDT");
            this.KhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("KhaiBao");
            this.Huy1 = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.cmdGuiBK = new Janus.Windows.UI.CommandBars.UICommand("cmdGuiBK");
            this.cmdHuyBK = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyBK");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.cmdCanDoiNX = new Janus.Windows.UI.CommandBars.UICommand("cmdCanDoiNX");
            this.cmdChungTuThanhToan = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuThanhToan");
            this.cmdXuatBaoCao = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatBaoCao");
            this.cmdBKTKNKNL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTKNKNL");
            this.cmdBKTKXKSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTKXKSP");
            this.cmdBCNLSXXK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNLSXXK");
            this.cmdBCXNTNL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCXNTNL");
            this.cmdBCTTNLNK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCTTNLNK");
            this.cmdBKTKNKNL = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTKNKNL");
            this.cmdBKTKXKSP = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTKXKSP");
            this.cmdBCNLSXXK = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNLSXXK");
            this.cmdBCXNTNL = new Janus.Windows.UI.CommandBars.UICommand("cmdBCXNTNL");
            this.cmdBCTTNLNK = new Janus.Windows.UI.CommandBars.UICommand("cmdBCTTNLNK");
            this.cmdFixAndRun = new Janus.Windows.UI.CommandBars.UICommand("cmdFixAndRun");
            this.cmdTT79KCX = new Janus.Windows.UI.CommandBars.UICommand("cmdTT79KCX");
            this.cmdBKToKhaiNhap_KCX1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_KCX");
            this.cmdBKToKhaiXuat_KCX1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_KCX");
            this.cmdBCNPLXuatNhapTon_KCX1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX");
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX_TongNPL");
            this.cmdBCNPLXuatNhapTon_KCX_1281 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX_128");
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL_1281 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX_TongNPL_128");
            this.cmdBCNPLXuatNhapTon_KCX_TT221 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX_TT22");
            this.cmdTQDT = new Janus.Windows.UI.CommandBars.UICommand("cmdTQDT");
            this.KhaiBao2 = new Janus.Windows.UI.CommandBars.UICommand("KhaiBao");
            this.NhanDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.Huy2 = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdChungTuTT = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuTT");
            this.cmdBKToKhaiNhap_KCX = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_KCX");
            this.cmdBKToKhaiXuat_KCX = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_KCX");
            this.cmdBCNPLXuatNhapTon_KCX = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX");
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX_TongNPL");
            this.cmdBCNPLXuatNhapTon_TongNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_TongNPL");
            this.cmdBCNPLXuatNhapTon_196 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_196");
            this.cmdBCThueXNK_196 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCThueXNK_196");
            this.cmdBKToKhaiNhap_196 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_196");
            this.cmdBKToKhaiXuat_196 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_196");
            this.cmdBCNPLXuatNhapTon_TongNPL_196 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_TongNPL_196");
            this.cmdBKToKhaiNhap_128 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_128");
            this.cmdBKToKhaiXuat_128 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_128");
            this.cmdBCThueNK_128 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCThueNK_128");
            this.cmdBCNPLXuatNhapTon_128 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_128");
            this.cmdBCNPLXuatNhapTon_TongNPL_128 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_TongNPL_128");
            this.cmdBCNPLXuatNhapTon_KCX_128 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX_128");
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL_128 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX_TongNPL_128");
            this.cmdBaoCaoTonSauThanhKhoan = new Janus.Windows.UI.CommandBars.UICommand("cmdBaoCaoTonSauThanhKhoan");
            this.cmdBCNPLXuatNhapTon_KCX_TT22 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX_TT22");
            this.cmdHangTonKho = new Janus.Windows.UI.CommandBars.UICommand("cmdHangTonKho");
            this.cmdBKTaiXUat = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTaiXUat");
            this.cmdBKTieuHuy = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTieuHuy");
            this.cmdBangKe = new Janus.Windows.UI.CommandBars.UICommand("cmdBangKe");
            this.cmdHangTonKho1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHangTonKho");
            this.cmdBKTaiXUat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTaiXUat");
            this.cmdBKTieuHuy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTieuHuy");
            this.cmdBKToKhaiNhap_TT38 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_TT38");
            this.cmdBKToKhaiXuat_TT38 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_TT38");
            this.cmdBKToKhaiXNKKhongThuThue_TT38 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXNKKhongThuThue_TT38");
            this.cmdBKToKhaiXNKHoan_TT38 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXNKHoan_TT38");
            this.cmdBKToKhaiNhapXinHoan_TT38 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhapXinHoan_TT38");
            this.cmdBKToKhaiNhapKhongThu_TT38 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhapKhongThu_TT38");
            this.cmdThanhKhoanTheoMaHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThanhKhoanTheoMaHang");
            this.cmdBCNPLXuatNhapTonTheoTK_TT38 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTonTheoTK_TT38");
            this.cmdBCNPLXuatNhapTonTheoNPL_TT38 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTonTheoNPL_TT38");
            this.cmdBCThueNPLNhapKhau_TT38 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCThueNPLNhapKhau_TT38");
            this.cmdTT38_2015 = new Janus.Windows.UI.CommandBars.UICommand("cmdTT38_2015");
            this.cmdBKToKhaiNhap_1962 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_196");
            this.cmdBKToKhaiXuat_1962 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_196");
            this.Separator5 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdBKToKhaiNhap_TT382 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_TT38");
            this.cmdBKToKhaiXuat_TT382 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_TT38");
            this.cmdBKToKhaiNhapXinHoan_TT382 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhapXinHoan_TT38");
            this.cmdBKToKhaiNhapKhongThu_TT382 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhapKhongThu_TT38");
            this.cmdBCNPLXuatNhapTonTheoTK_TT382 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTonTheoTK_TT38");
            this.cmdBCNPLXuatNhapTonTheoNPL_TT382 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTonTheoNPL_TT38");
            this.cmdBCThueNPLNhapKhau_TT382 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCThueNPLNhapKhau_TT38");
            this.cmdXemSPXuat = new Janus.Windows.UI.CommandBars.UICommand("cmdXemSPXuat");
            this.cmdNgayTK = new Janus.Windows.UI.CommandBars.UICommand("cmdNgayTK");
            this.cmdDonGiaTT = new Janus.Windows.UI.CommandBars.UICommand("cmdDonGiaTT");
            this.cmdViewNPLCU = new Janus.Windows.UI.CommandBars.UICommand("cmdViewNPLCU");
            this.cmdSendNPLCU = new Janus.Windows.UI.CommandBars.UICommand("cmdSendNPLCU");
            this.cmdViewNPLCU1 = new Janus.Windows.UI.CommandBars.UICommand("cmdViewNPLCU");
            this.cmdUpdateTK = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateTK");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.btnXoaBK = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkChenhLech = new System.Windows.Forms.LinkLabel();
            this.numChenhlech = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.chkKhaiBaoBangKe = new Janus.Windows.EditControls.UICheckBox();
            this.numSoLuongChungTu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtNgayTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.dtNgayKhaiBao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnThemMoi = new Janus.Windows.EditControls.UIButton();
            this.cbbBangKe = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Dock = System.Windows.Forms.DockStyle.None;
            this.grbMain.Location = new System.Drawing.Point(0, 68);
            this.grbMain.Size = new System.Drawing.Size(1169, 571);
            this.grbMain.Click += new System.EventHandler(this.grbMain_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.donViHaiQuanNewControl1);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.txtQuy);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.txtLanThanhLy);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.lblHinhThuc);
            this.uiGroupBox1.Controls.Add(this.label16);
            this.uiGroupBox1.Controls.Add(this.lblTrangThaiTK);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.txtSoHSTL);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.txtNam);
            this.uiGroupBox1.Controls.Add(this.txtSoQuyetDinh);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.ccNgayThanhLy);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(513, 135);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông tin hồ sơ thanh khoản";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // donViHaiQuanNewControl1
            // 
            this.donViHaiQuanNewControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanNewControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanNewControl1.Location = new System.Drawing.Point(86, 22);
            this.donViHaiQuanNewControl1.Ma = "";
            this.donViHaiQuanNewControl1.MaCuc = "";
            this.donViHaiQuanNewControl1.Name = "donViHaiQuanNewControl1";
            this.donViHaiQuanNewControl1.ReadOnly = true;
            this.donViHaiQuanNewControl1.Size = new System.Drawing.Size(388, 22);
            this.donViHaiQuanNewControl1.TabIndex = 1;
            this.donViHaiQuanNewControl1.Ten = "";
            this.donViHaiQuanNewControl1.VisualStyleManager = null;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(341, 81);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Năm";
            // 
            // txtQuy
            // 
            this.txtQuy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuy.Location = new System.Drawing.Point(225, 77);
            this.txtQuy.MaxLength = 5;
            this.txtQuy.Name = "txtQuy";
            this.txtQuy.Size = new System.Drawing.Size(50, 21);
            this.txtQuy.TabIndex = 7;
            this.txtQuy.Text = "0";
            this.txtQuy.Value = ((long)(0));
            this.txtQuy.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int64;
            this.txtQuy.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(190, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Quý";
            // 
            // txtLanThanhLy
            // 
            this.txtLanThanhLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLanThanhLy.Location = new System.Drawing.Point(86, 77);
            this.txtLanThanhLy.MaxLength = 5;
            this.txtLanThanhLy.Name = "txtLanThanhLy";
            this.txtLanThanhLy.ReadOnly = true;
            this.txtLanThanhLy.Size = new System.Drawing.Size(50, 21);
            this.txtLanThanhLy.TabIndex = 7;
            this.txtLanThanhLy.Text = "0";
            this.txtLanThanhLy.Value = ((long)(0));
            this.txtLanThanhLy.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int64;
            this.txtLanThanhLy.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Lần thanh lý";
            // 
            // lblHinhThuc
            // 
            this.lblHinhThuc.AutoSize = true;
            this.lblHinhThuc.BackColor = System.Drawing.Color.Transparent;
            this.lblHinhThuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHinhThuc.ForeColor = System.Drawing.Color.Blue;
            this.lblHinhThuc.Location = new System.Drawing.Point(289, 108);
            this.lblHinhThuc.Name = "lblHinhThuc";
            this.lblHinhThuc.Size = new System.Drawing.Size(122, 13);
            this.lblHinhThuc.TabIndex = 9;
            this.lblHinhThuc.Text = "Nhập trước - Xuất trước";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(222, 108);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Hình thức";
            // 
            // lblTrangThaiTK
            // 
            this.lblTrangThaiTK.AutoSize = true;
            this.lblTrangThaiTK.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThaiTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThaiTK.ForeColor = System.Drawing.Color.Blue;
            this.lblTrangThaiTK.Location = new System.Drawing.Point(83, 108);
            this.lblTrangThaiTK.Name = "lblTrangThaiTK";
            this.lblTrangThaiTK.Size = new System.Drawing.Size(79, 13);
            this.lblTrangThaiTK.TabIndex = 9;
            this.lblTrangThaiTK.Text = "Đang nhập liệu";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Trạng thái";
            // 
            // txtSoHSTL
            // 
            this.txtSoHSTL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHSTL.Location = new System.Drawing.Point(86, 49);
            this.txtSoHSTL.MaxLength = 5;
            this.txtSoHSTL.Name = "txtSoHSTL";
            this.txtSoHSTL.Size = new System.Drawing.Size(50, 21);
            this.txtSoHSTL.TabIndex = 3;
            this.txtSoHSTL.Text = "0";
            this.txtSoHSTL.Value = ((long)(0));
            this.txtSoHSTL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int64;
            this.txtSoHSTL.VisualStyleManager = this.vsmMain;
            this.txtSoHSTL.Click += new System.EventHandler(this.txtSoHSTL_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(332, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Số QĐ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Số hồ sơ ";
            // 
            // txtNam
            // 
            this.txtNam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNam.Location = new System.Drawing.Point(379, 77);
            this.txtNam.Name = "txtNam";
            this.txtNam.Size = new System.Drawing.Size(46, 21);
            this.txtNam.TabIndex = 3;
            this.txtNam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtNam.VisualStyleManager = this.vsmMain;
            // 
            // txtSoQuyetDinh
            // 
            this.txtSoQuyetDinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoQuyetDinh.Location = new System.Drawing.Point(379, 49);
            this.txtSoQuyetDinh.Name = "txtSoQuyetDinh";
            this.txtSoQuyetDinh.Size = new System.Drawing.Size(95, 21);
            this.txtSoQuyetDinh.TabIndex = 3;
            this.txtSoQuyetDinh.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã hải quan";
            // 
            // ccNgayThanhLy
            // 
            this.ccNgayThanhLy.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayThanhLy.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayThanhLy.DropDownCalendar.Name = "";
            this.ccNgayThanhLy.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayThanhLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayThanhLy.Location = new System.Drawing.Point(225, 49);
            this.ccNgayThanhLy.Name = "ccNgayThanhLy";
            this.ccNgayThanhLy.Nullable = true;
            this.ccNgayThanhLy.NullButtonText = "Xóa";
            this.ccNgayThanhLy.ReadOnly = true;
            this.ccNgayThanhLy.ShowNullButton = true;
            this.ccNgayThanhLy.Size = new System.Drawing.Size(88, 21);
            this.ccNgayThanhLy.TabIndex = 5;
            this.ccNgayThanhLy.TodayButtonText = "Hôm nay";
            this.ccNgayThanhLy.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayThanhLy.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(138, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ngày bắt đầu";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Enabled = false;
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sendBK1,
            this.huyBangKe1,
            this.xacNhanThongTin1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(176, 70);
            // 
            // sendBK1
            // 
            this.sendBK1.Enabled = false;
            this.sendBK1.Name = "sendBK1";
            this.sendBK1.Size = new System.Drawing.Size(175, 22);
            this.sendBK1.Text = "Gửi bảng kê";
            this.sendBK1.Visible = false;
            this.sendBK1.Click += new System.EventHandler(this.sendBK1_Click);
            // 
            // huyBangKe1
            // 
            this.huyBangKe1.Enabled = false;
            this.huyBangKe1.Name = "huyBangKe1";
            this.huyBangKe1.Size = new System.Drawing.Size(175, 22);
            this.huyBangKe1.Text = "Hủy bảng kê";
            this.huyBangKe1.Visible = false;
            this.huyBangKe1.Click += new System.EventHandler(this.huyBangKe1_Click);
            // 
            // xacNhanThongTin1
            // 
            this.xacNhanThongTin1.Enabled = false;
            this.xacNhanThongTin1.Image = ((System.Drawing.Image)(resources.GetObject("xacNhanThongTin1.Image")));
            this.xacNhanThongTin1.Name = "xacNhanThongTin1";
            this.xacNhanThongTin1.Size = new System.Drawing.Size(175, 22);
            this.xacNhanThongTin1.Text = "Xác nhận thông tin";
            this.xacNhanThongTin1.Visible = false;
            this.xacNhanThongTin1.Click += new System.EventHandler(this.xacNhanThongTin1_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "GiayPhep1.Icon.ico");
            this.ImageList1.Images.SetKeyName(5, "baocao.ico");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(1100, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(66, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cmMain
            // 
            this.cmMain.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowMerge = false;
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.KhaiBao,
            this.NhanDuLieu,
            this.Huy,
            this.Luu,
            this.cmdThanhLy,
            this.cmdPrint,
            this.cmdBCNPLXuatNhapTon,
            this.cmdBCThueXNK,
            this.cmdBKToKhaiXuat,
            this.cmdBKToKhaiNhap,
            this.cmdDelete,
            this.cmdRollback,
            this.cmdClose,
            this.cmdThanhKhoan,
            this.cmdKhaiDT,
            this.cmdGuiBK,
            this.cmdHuyBK,
            this.XacNhan,
            this.cmdCanDoiNX,
            this.cmdChungTuThanhToan,
            this.cmdXuatBaoCao,
            this.cmdBKTKNKNL,
            this.cmdBKTKXKSP,
            this.cmdBCNLSXXK,
            this.cmdBCXNTNL,
            this.cmdBCTTNLNK,
            this.cmdFixAndRun,
            this.cmdTT79KCX,
            this.cmdTQDT,
            this.cmdKetQuaXuLy,
            this.cmdChungTuTT,
            this.cmdBKToKhaiNhap_KCX,
            this.cmdBKToKhaiXuat_KCX,
            this.cmdBCNPLXuatNhapTon_KCX,
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL,
            this.cmdBCNPLXuatNhapTon_TongNPL,
            this.cmdBCNPLXuatNhapTon_196,
            this.cmdBCThueXNK_196,
            this.cmdBKToKhaiNhap_196,
            this.cmdBKToKhaiXuat_196,
            this.cmdBCNPLXuatNhapTon_TongNPL_196,
            this.cmdBKToKhaiNhap_128,
            this.cmdBKToKhaiXuat_128,
            this.cmdBCThueNK_128,
            this.cmdBCNPLXuatNhapTon_128,
            this.cmdBCNPLXuatNhapTon_TongNPL_128,
            this.cmdBCNPLXuatNhapTon_KCX_128,
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL_128,
            this.cmdBaoCaoTonSauThanhKhoan,
            this.cmdBCNPLXuatNhapTon_KCX_TT22,
            this.cmdHangTonKho,
            this.cmdBKTaiXUat,
            this.cmdBKTieuHuy,
            this.cmdBangKe,
            this.cmdBKToKhaiNhap_TT38,
            this.cmdBKToKhaiXuat_TT38,
            this.cmdBKToKhaiXNKKhongThuThue_TT38,
            this.cmdBKToKhaiXNKHoan_TT38,
            this.cmdBKToKhaiNhapXinHoan_TT38,
            this.cmdBKToKhaiNhapKhongThu_TT38,
            this.cmdThanhKhoanTheoMaHang,
            this.cmdBCNPLXuatNhapTonTheoTK_TT38,
            this.cmdBCNPLXuatNhapTonTheoNPL_TT38,
            this.cmdBCThueNPLNhapKhau_TT38,
            this.cmdTT38_2015,
            this.cmdXemSPXuat,
            this.cmdNgayTK,
            this.cmdDonGiaTT,
            this.cmdViewNPLCU,
            this.cmdSendNPLCU,
            this.cmdUpdateTK});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.KeepMergeSettings = false;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.ShowShortcutInToolTips = true;
            this.cmMain.ShowToolTipOnMenus = true;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.Luu1,
            this.cmdThanhKhoan1,
            this.cmdPrint1,
            this.cmdTT38_20151,
            this.cmdTT79KCX1,
            this.cmdCanDoiNX1,
            this.cmdXemSPXuat1,
            this.cmdChungTuThanhToan1,
            this.cmdChungTuTT1,
            this.cmdTQDT1,
            this.cmdFixAndRun1,
            this.cmdNgayTK1,
            this.cmdDonGiaTT1,
            this.cmdSendNPLCU1,
            this.cmdUpdateTK1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.ImageList = this.ImageList1;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1169, 68);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.Wrappable = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.uiCommandBar1_CommandClick);
            // 
            // Luu1
            // 
            this.Luu1.Image = ((System.Drawing.Image)(resources.GetObject("Luu1.Image")));
            this.Luu1.Key = "Luu";
            this.Luu1.Name = "Luu1";
            this.Luu1.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.Luu1.Text = "Lưu hồ sơ";
            // 
            // cmdThanhKhoan1
            // 
            this.cmdThanhKhoan1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThanhKhoan1.Image")));
            this.cmdThanhKhoan1.Key = "cmdThanhKhoan";
            this.cmdThanhKhoan1.Name = "cmdThanhKhoan1";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            this.cmdPrint1.Text = "Báo cáo TT 128/2013";
            // 
            // cmdTT38_20151
            // 
            this.cmdTT38_20151.Key = "cmdTT38_2015";
            this.cmdTT38_20151.Name = "cmdTT38_20151";
            this.cmdTT38_20151.Text = "Báo cáo TT 38/2015";
            // 
            // cmdTT79KCX1
            // 
            this.cmdTT79KCX1.Key = "cmdTT79KCX";
            this.cmdTT79KCX1.Name = "cmdTT79KCX1";
            this.cmdTT79KCX1.Text = "Báo cáo TT 128/2013 - KCX";
            // 
            // cmdCanDoiNX1
            // 
            this.cmdCanDoiNX1.Key = "cmdCanDoiNX";
            this.cmdCanDoiNX1.Name = "cmdCanDoiNX1";
            // 
            // cmdXemSPXuat1
            // 
            this.cmdXemSPXuat1.Key = "cmdXemSPXuat";
            this.cmdXemSPXuat1.Name = "cmdXemSPXuat1";
            // 
            // cmdChungTuThanhToan1
            // 
            this.cmdChungTuThanhToan1.Key = "cmdChungTuThanhToan";
            this.cmdChungTuThanhToan1.Name = "cmdChungTuThanhToan1";
            this.cmdChungTuThanhToan1.Text = "Bảng tổng hợp chứng từ thanh toán";
            // 
            // cmdChungTuTT1
            // 
            this.cmdChungTuTT1.Key = "cmdChungTuTT";
            this.cmdChungTuTT1.Name = "cmdChungTuTT1";
            this.cmdChungTuTT1.Text = "Chứng từ thanh toán khai báo";
            // 
            // cmdTQDT1
            // 
            this.cmdTQDT1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTQDT1.Image")));
            this.cmdTQDT1.Key = "cmdTQDT";
            this.cmdTQDT1.Name = "cmdTQDT1";
            // 
            // cmdFixAndRun1
            // 
            this.cmdFixAndRun1.Image = ((System.Drawing.Image)(resources.GetObject("cmdFixAndRun1.Image")));
            this.cmdFixAndRun1.Key = "cmdFixAndRun";
            this.cmdFixAndRun1.Name = "cmdFixAndRun1";
            this.cmdFixAndRun1.Text = "Cập nhật dữ liệu và chạy thanh khoản";
            // 
            // cmdNgayTK1
            // 
            this.cmdNgayTK1.Key = "cmdNgayTK";
            this.cmdNgayTK1.Name = "cmdNgayTK1";
            // 
            // cmdDonGiaTT1
            // 
            this.cmdDonGiaTT1.Key = "cmdDonGiaTT";
            this.cmdDonGiaTT1.Name = "cmdDonGiaTT1";
            // 
            // cmdSendNPLCU1
            // 
            this.cmdSendNPLCU1.Key = "cmdSendNPLCU";
            this.cmdSendNPLCU1.Name = "cmdSendNPLCU1";
            // 
            // cmdUpdateTK1
            // 
            this.cmdUpdateTK1.Key = "cmdUpdateTK";
            this.cmdUpdateTK1.Name = "cmdUpdateTK1";
            // 
            // KhaiBao
            // 
            this.KhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("KhaiBao.Image")));
            this.KhaiBao.Key = "KhaiBao";
            this.KhaiBao.Name = "KhaiBao";
            this.KhaiBao.Text = "Khai báo";
            // 
            // NhanDuLieu
            // 
            this.NhanDuLieu.Image = ((System.Drawing.Image)(resources.GetObject("NhanDuLieu.Image")));
            this.NhanDuLieu.Key = "NhanDuLieu";
            this.NhanDuLieu.Name = "NhanDuLieu";
            this.NhanDuLieu.Text = "Nhận dữ liệu";
            // 
            // Huy
            // 
            this.Huy.Image = ((System.Drawing.Image)(resources.GetObject("Huy.Image")));
            this.Huy.Key = "Huy";
            this.Huy.Name = "Huy";
            this.Huy.Text = "Hủy";
            // 
            // Luu
            // 
            this.Luu.Key = "Luu";
            this.Luu.Name = "Luu";
            this.Luu.Text = "Lưu";
            // 
            // cmdThanhLy
            // 
            this.cmdThanhLy.Image = ((System.Drawing.Image)(resources.GetObject("cmdThanhLy.Image")));
            this.cmdThanhLy.Key = "ThanhLy";
            this.cmdThanhLy.Name = "cmdThanhLy";
            this.cmdThanhLy.Text = "Thanh lý";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdBKToKhaiNhap1,
            this.cmdBKToKhaiXuat1,
            this.cmdBCNPLXuatNhapTon1,
            this.cmdBCNPLXuatNhapTon_TongNPL1,
            this.cmdBCThueXNK1,
            this.Separator1,
            this.cmdBKToKhaiNhap_1961,
            this.cmdBKToKhaiXuat_1961,
            this.cmdBCNPLXuatNhapTon_1961,
            this.cmdBCNPLXuatNhapTon_TongNPL_1961,
            this.cmdBCThueXNK_1961,
            this.Separator2,
            this.cmdBKToKhaiNhap_1281,
            this.cmdBKToKhaiXuat_1281,
            this.cmdBCNPLXuatNhapTon_1281,
            this.cmdBCNPLXuatNhapTon_TongNPL_1281,
            this.cmdBCThueNK_1281,
            this.cmdBaoCaoTonSauThanhKhoan1,
            this.Separator4,
            this.cmdBKToKhaiNhap_TT381,
            this.cmdBKToKhaiXuat_TT381,
            this.cmdBKToKhaiNhapXinHoan_TT381,
            this.cmdBKToKhaiNhapKhongThu_TT381});
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "Báo cáo TT 128/2013";
            // 
            // cmdBKToKhaiNhap1
            // 
            this.cmdBKToKhaiNhap1.Key = "cmdBKToKhaiNhap";
            this.cmdBKToKhaiNhap1.Name = "cmdBKToKhaiNhap1";
            this.cmdBKToKhaiNhap1.Shortcut = System.Windows.Forms.Shortcut.Ctrl1;
            // 
            // cmdBKToKhaiXuat1
            // 
            this.cmdBKToKhaiXuat1.Key = "cmdBKToKhaiXuat";
            this.cmdBKToKhaiXuat1.Name = "cmdBKToKhaiXuat1";
            this.cmdBKToKhaiXuat1.Shortcut = System.Windows.Forms.Shortcut.Ctrl2;
            // 
            // cmdBCNPLXuatNhapTon1
            // 
            this.cmdBCNPLXuatNhapTon1.Key = "cmdBCNPLXuatNhapTon";
            this.cmdBCNPLXuatNhapTon1.Name = "cmdBCNPLXuatNhapTon1";
            this.cmdBCNPLXuatNhapTon1.Shortcut = System.Windows.Forms.Shortcut.Ctrl3;
            // 
            // cmdBCNPLXuatNhapTon_TongNPL1
            // 
            this.cmdBCNPLXuatNhapTon_TongNPL1.Key = "cmdBCNPLXuatNhapTon_TongNPL";
            this.cmdBCNPLXuatNhapTon_TongNPL1.Name = "cmdBCNPLXuatNhapTon_TongNPL1";
            // 
            // cmdBCThueXNK1
            // 
            this.cmdBCThueXNK1.Key = "cmdBCThueXNK";
            this.cmdBCThueXNK1.Name = "cmdBCThueXNK1";
            this.cmdBCThueXNK1.Shortcut = System.Windows.Forms.Shortcut.Ctrl4;
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdBKToKhaiNhap_1961
            // 
            this.cmdBKToKhaiNhap_1961.Key = "cmdBKToKhaiNhap_196";
            this.cmdBKToKhaiNhap_1961.Name = "cmdBKToKhaiNhap_1961";
            this.cmdBKToKhaiNhap_1961.Text = "BC01 - Bảng kê tờ khai nhập khẩu đưa vào thanh khoản (TT 22)";
            // 
            // cmdBKToKhaiXuat_1961
            // 
            this.cmdBKToKhaiXuat_1961.Key = "cmdBKToKhaiXuat_196";
            this.cmdBKToKhaiXuat_1961.Name = "cmdBKToKhaiXuat_1961";
            this.cmdBKToKhaiXuat_1961.Text = "BC02 - Bảng kê tờ khai xuất khẩu đưa vào thanh khoản (TT 22)";
            // 
            // cmdBCNPLXuatNhapTon_1961
            // 
            this.cmdBCNPLXuatNhapTon_1961.Key = "cmdBCNPLXuatNhapTon_196";
            this.cmdBCNPLXuatNhapTon_1961.Name = "cmdBCNPLXuatNhapTon_1961";
            this.cmdBCNPLXuatNhapTon_1961.Text = "BC03 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn (TT 22)";
            // 
            // cmdBCNPLXuatNhapTon_TongNPL_1961
            // 
            this.cmdBCNPLXuatNhapTon_TongNPL_1961.Key = "cmdBCNPLXuatNhapTon_TongNPL_196";
            this.cmdBCNPLXuatNhapTon_TongNPL_1961.Name = "cmdBCNPLXuatNhapTon_TongNPL_1961";
            this.cmdBCNPLXuatNhapTon_TongNPL_1961.Text = "BC03.1 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn - Tổng NPL (TT 22)";
            // 
            // cmdBCThueXNK_1961
            // 
            this.cmdBCThueXNK_1961.Key = "cmdBCThueXNK_196";
            this.cmdBCThueXNK_1961.Name = "cmdBCThueXNK_1961";
            this.cmdBCThueXNK_1961.Text = "BC04 - Báo cáo tính thuế NPL nhập khẩu (TT 22)";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdBKToKhaiNhap_1281
            // 
            this.cmdBKToKhaiNhap_1281.Key = "cmdBKToKhaiNhap_128";
            this.cmdBKToKhaiNhap_1281.Name = "cmdBKToKhaiNhap_1281";
            // 
            // cmdBKToKhaiXuat_1281
            // 
            this.cmdBKToKhaiXuat_1281.Key = "cmdBKToKhaiXuat_128";
            this.cmdBKToKhaiXuat_1281.Name = "cmdBKToKhaiXuat_1281";
            // 
            // cmdBCNPLXuatNhapTon_1281
            // 
            this.cmdBCNPLXuatNhapTon_1281.Key = "cmdBCNPLXuatNhapTon_128";
            this.cmdBCNPLXuatNhapTon_1281.Name = "cmdBCNPLXuatNhapTon_1281";
            // 
            // cmdBCNPLXuatNhapTon_TongNPL_1281
            // 
            this.cmdBCNPLXuatNhapTon_TongNPL_1281.Key = "cmdBCNPLXuatNhapTon_TongNPL_128";
            this.cmdBCNPLXuatNhapTon_TongNPL_1281.Name = "cmdBCNPLXuatNhapTon_TongNPL_1281";
            // 
            // cmdBCThueNK_1281
            // 
            this.cmdBCThueNK_1281.Key = "cmdBCThueNK_128";
            this.cmdBCThueNK_1281.Name = "cmdBCThueNK_1281";
            // 
            // cmdBaoCaoTonSauThanhKhoan1
            // 
            this.cmdBaoCaoTonSauThanhKhoan1.Key = "cmdBaoCaoTonSauThanhKhoan";
            this.cmdBaoCaoTonSauThanhKhoan1.Name = "cmdBaoCaoTonSauThanhKhoan1";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdBKToKhaiNhap_TT381
            // 
            this.cmdBKToKhaiNhap_TT381.Key = "cmdBKToKhaiNhap_TT38";
            this.cmdBKToKhaiNhap_TT381.Name = "cmdBKToKhaiNhap_TT381";
            // 
            // cmdBKToKhaiXuat_TT381
            // 
            this.cmdBKToKhaiXuat_TT381.Key = "cmdBKToKhaiXuat_TT38";
            this.cmdBKToKhaiXuat_TT381.Name = "cmdBKToKhaiXuat_TT381";
            // 
            // cmdBKToKhaiNhapXinHoan_TT381
            // 
            this.cmdBKToKhaiNhapXinHoan_TT381.Key = "cmdBKToKhaiNhapXinHoan_TT38";
            this.cmdBKToKhaiNhapXinHoan_TT381.Name = "cmdBKToKhaiNhapXinHoan_TT381";
            // 
            // cmdBKToKhaiNhapKhongThu_TT381
            // 
            this.cmdBKToKhaiNhapKhongThu_TT381.Key = "cmdBKToKhaiNhapKhongThu_TT38";
            this.cmdBKToKhaiNhapKhongThu_TT381.Name = "cmdBKToKhaiNhapKhongThu_TT381";
            // 
            // cmdBCNPLXuatNhapTon
            // 
            this.cmdBCNPLXuatNhapTon.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon.Image")));
            this.cmdBCNPLXuatNhapTon.Key = "cmdBCNPLXuatNhapTon";
            this.cmdBCNPLXuatNhapTon.Name = "cmdBCNPLXuatNhapTon";
            this.cmdBCNPLXuatNhapTon.Text = "BC03 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn (TT 194)";
            this.cmdBCNPLXuatNhapTon.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdBCThueXNK
            // 
            this.cmdBCThueXNK.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCThueXNK.Image")));
            this.cmdBCThueXNK.Key = "cmdBCThueXNK";
            this.cmdBCThueXNK.Name = "cmdBCThueXNK";
            this.cmdBCThueXNK.Text = "BC04 - Báo cáo tính thuế trên NPL nhập khẩu (TT 194)";
            this.cmdBCThueXNK.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdBKToKhaiXuat
            // 
            this.cmdBKToKhaiXuat.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiXuat.Image")));
            this.cmdBKToKhaiXuat.Key = "cmdBKToKhaiXuat";
            this.cmdBKToKhaiXuat.Name = "cmdBKToKhaiXuat";
            this.cmdBKToKhaiXuat.Text = "BC02 - Bảng kê tờ khai xuất khẩu đưa vào thanh khoản (TT 194)";
            this.cmdBKToKhaiXuat.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdBKToKhaiNhap
            // 
            this.cmdBKToKhaiNhap.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiNhap.Image")));
            this.cmdBKToKhaiNhap.Key = "cmdBKToKhaiNhap";
            this.cmdBKToKhaiNhap.Name = "cmdBKToKhaiNhap";
            this.cmdBKToKhaiNhap.Text = "BC01 - Bảng kê tờ khai nhập khẩu đưa vào thanh khoản (TT 194)";
            this.cmdBKToKhaiNhap.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdDelete
            // 
            this.cmdDelete.Key = "cmdDelete";
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Shortcut = System.Windows.Forms.Shortcut.CtrlDel;
            this.cmdDelete.Text = "Xóa";
            // 
            // cmdRollback
            // 
            this.cmdRollback.Image = ((System.Drawing.Image)(resources.GetObject("cmdRollback.Image")));
            this.cmdRollback.Key = "cmdRollback";
            this.cmdRollback.Name = "cmdRollback";
            this.cmdRollback.Shortcut = System.Windows.Forms.Shortcut.CtrlR;
            this.cmdRollback.Text = "Rollback";
            // 
            // cmdClose
            // 
            this.cmdClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdClose.Image")));
            this.cmdClose.Key = "cmdClose";
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.cmdClose.Text = "Đóng hồ sơ";
            // 
            // cmdThanhKhoan
            // 
            this.cmdThanhKhoan.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThanhLy1,
            this.cmdRollback1,
            this.cmdClose1,
            this.Separator3,
            this.cmdThanhKhoanTheoMaHang1});
            this.cmdThanhKhoan.Key = "cmdThanhKhoan";
            this.cmdThanhKhoan.Name = "cmdThanhKhoan";
            this.cmdThanhKhoan.Text = "Thanh khoản";
            // 
            // ThanhLy1
            // 
            this.ThanhLy1.Key = "ThanhLy";
            this.ThanhLy1.Name = "ThanhLy1";
            this.ThanhLy1.Shortcut = System.Windows.Forms.Shortcut.CtrlE;
            this.ThanhLy1.Text = "Chạy thanh khoản";
            // 
            // cmdRollback1
            // 
            this.cmdRollback1.Key = "cmdRollback";
            this.cmdRollback1.Name = "cmdRollback1";
            // 
            // cmdClose1
            // 
            this.cmdClose1.Key = "cmdClose";
            this.cmdClose1.Name = "cmdClose1";
            // 
            // Separator3
            // 
            this.Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator3.Key = "Separator";
            this.Separator3.Name = "Separator3";
            // 
            // cmdThanhKhoanTheoMaHang1
            // 
            this.cmdThanhKhoanTheoMaHang1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThanhKhoanTheoMaHang1.Image")));
            this.cmdThanhKhoanTheoMaHang1.Key = "cmdThanhKhoanTheoMaHang";
            this.cmdThanhKhoanTheoMaHang1.Name = "cmdThanhKhoanTheoMaHang1";
            this.cmdThanhKhoanTheoMaHang1.Shortcut = System.Windows.Forms.Shortcut.F5;
            this.cmdThanhKhoanTheoMaHang1.Text = "Chạy thanh khoản theo PO (TT38/2015)";
            // 
            // cmdKhaiDT
            // 
            this.cmdKhaiDT.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.KhaiBao1,
            this.Huy1});
            this.cmdKhaiDT.Key = "cmdKhaiDT";
            this.cmdKhaiDT.Name = "cmdKhaiDT";
            this.cmdKhaiDT.Text = "Khai điện tử";
            // 
            // KhaiBao1
            // 
            this.KhaiBao1.Icon = ((System.Drawing.Icon)(resources.GetObject("KhaiBao1.Icon")));
            this.KhaiBao1.Key = "KhaiBao";
            this.KhaiBao1.Name = "KhaiBao1";
            this.KhaiBao1.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.KhaiBao1.Text = "Đăng ký số HS";
            // 
            // Huy1
            // 
            this.Huy1.Icon = ((System.Drawing.Icon)(resources.GetObject("Huy1.Icon")));
            this.Huy1.Key = "Huy";
            this.Huy1.Name = "Huy1";
            this.Huy1.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.Huy1.Text = "Hủy số HS";
            // 
            // cmdGuiBK
            // 
            this.cmdGuiBK.Key = "cmdGuiBK";
            this.cmdGuiBK.Name = "cmdGuiBK";
            this.cmdGuiBK.Text = "Gửi bảng kê";
            // 
            // cmdHuyBK
            // 
            this.cmdHuyBK.Key = "cmdHuyBK";
            this.cmdHuyBK.Name = "cmdHuyBK";
            this.cmdHuyBK.Text = "Huỷ bảng kê";
            // 
            // XacNhan
            // 
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận thông tin";
            // 
            // cmdCanDoiNX
            // 
            this.cmdCanDoiNX.Image = ((System.Drawing.Image)(resources.GetObject("cmdCanDoiNX.Image")));
            this.cmdCanDoiNX.Key = "cmdCanDoiNX";
            this.cmdCanDoiNX.Name = "cmdCanDoiNX";
            this.cmdCanDoiNX.Shortcut = System.Windows.Forms.Shortcut.CtrlV;
            this.cmdCanDoiNX.Text = "Xem cân đối nhập xuất";
            // 
            // cmdChungTuThanhToan
            // 
            this.cmdChungTuThanhToan.Image = ((System.Drawing.Image)(resources.GetObject("cmdChungTuThanhToan.Image")));
            this.cmdChungTuThanhToan.Key = "cmdChungTuThanhToan";
            this.cmdChungTuThanhToan.Name = "cmdChungTuThanhToan";
            this.cmdChungTuThanhToan.Text = "Bảng CTTT";
            // 
            // cmdXuatBaoCao
            // 
            this.cmdXuatBaoCao.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdBKTKNKNL1,
            this.cmdBKTKXKSP1,
            this.cmdBCNLSXXK1,
            this.cmdBCXNTNL1,
            this.cmdBCTTNLNK1});
            this.cmdXuatBaoCao.Key = "cmdXuatBaoCao";
            this.cmdXuatBaoCao.Name = "cmdXuatBaoCao";
            // 
            // cmdBKTKNKNL1
            // 
            this.cmdBKTKNKNL1.Key = "cmdBKTKNKNL";
            this.cmdBKTKNKNL1.Name = "cmdBKTKNKNL1";
            // 
            // cmdBKTKXKSP1
            // 
            this.cmdBKTKXKSP1.Key = "cmdBKTKXKSP";
            this.cmdBKTKXKSP1.Name = "cmdBKTKXKSP1";
            // 
            // cmdBCNLSXXK1
            // 
            this.cmdBCNLSXXK1.Key = "cmdBCNLSXXK";
            this.cmdBCNLSXXK1.Name = "cmdBCNLSXXK1";
            // 
            // cmdBCXNTNL1
            // 
            this.cmdBCXNTNL1.Key = "cmdBCXNTNL";
            this.cmdBCXNTNL1.Name = "cmdBCXNTNL1";
            // 
            // cmdBCTTNLNK1
            // 
            this.cmdBCTTNLNK1.Key = "cmdBCTTNLNK";
            this.cmdBCTTNLNK1.Name = "cmdBCTTNLNK1";
            // 
            // cmdBKTKNKNL
            // 
            this.cmdBKTKNKNL.Key = "cmdBKTKNKNL";
            this.cmdBKTKNKNL.Name = "cmdBKTKNKNL";
            this.cmdBKTKNKNL.Text = "BC 01 - Bảng kê tờ khai nhập khẩu nguyên liệu đưa vào thanh khoản";
            // 
            // cmdBKTKXKSP
            // 
            this.cmdBKTKXKSP.Key = "cmdBKTKXKSP";
            this.cmdBKTKXKSP.Name = "cmdBKTKXKSP";
            this.cmdBKTKXKSP.Text = "BC 02- Bảng kê tờ khai xuất khẩu sản phẩm đưa vào thanh khoản";
            // 
            // cmdBCNLSXXK
            // 
            this.cmdBCNLSXXK.Key = "cmdBCNLSXXK";
            this.cmdBCNLSXXK.Name = "cmdBCNLSXXK";
            this.cmdBCNLSXXK.Text = "BC 03 - Báo cáo nguyên liệu sản xuất hàng xuất khẩu";
            // 
            // cmdBCXNTNL
            // 
            this.cmdBCXNTNL.Key = "cmdBCXNTNL";
            this.cmdBCXNTNL.Name = "cmdBCXNTNL";
            this.cmdBCXNTNL.Text = "BC 04 - Báo cáo xuất nhập tồn nguyên liệu nhập khẩu";
            // 
            // cmdBCTTNLNK
            // 
            this.cmdBCTTNLNK.Key = "cmdBCTTNLNK";
            this.cmdBCTTNLNK.Name = "cmdBCTTNLNK";
            this.cmdBCTTNLNK.Text = "BC 05 - Báo cáo tính thuế nguyên liệu nhập khẩu";
            // 
            // cmdFixAndRun
            // 
            this.cmdFixAndRun.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            this.cmdFixAndRun.Key = "cmdFixAndRun";
            this.cmdFixAndRun.Name = "cmdFixAndRun";
            this.cmdFixAndRun.Text = "Fix du lieu va chay thanh khoan";
            this.cmdFixAndRun.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdTT79KCX
            // 
            this.cmdTT79KCX.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdBKToKhaiNhap_KCX1,
            this.cmdBKToKhaiXuat_KCX1,
            this.cmdBCNPLXuatNhapTon_KCX1,
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL1,
            this.cmdBCNPLXuatNhapTon_KCX_1281,
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL_1281,
            this.cmdBCNPLXuatNhapTon_KCX_TT221});
            this.cmdTT79KCX.Image = ((System.Drawing.Image)(resources.GetObject("cmdTT79KCX.Image")));
            this.cmdTT79KCX.Key = "cmdTT79KCX";
            this.cmdTT79KCX.Name = "cmdTT79KCX";
            this.cmdTT79KCX.Text = "Báo cáo TT 128/2013 - KCX";
            // 
            // cmdBKToKhaiNhap_KCX1
            // 
            this.cmdBKToKhaiNhap_KCX1.Key = "cmdBKToKhaiNhap_KCX";
            this.cmdBKToKhaiNhap_KCX1.Name = "cmdBKToKhaiNhap_KCX1";
            // 
            // cmdBKToKhaiXuat_KCX1
            // 
            this.cmdBKToKhaiXuat_KCX1.Key = "cmdBKToKhaiXuat_KCX";
            this.cmdBKToKhaiXuat_KCX1.Name = "cmdBKToKhaiXuat_KCX1";
            // 
            // cmdBCNPLXuatNhapTon_KCX1
            // 
            this.cmdBCNPLXuatNhapTon_KCX1.Key = "cmdBCNPLXuatNhapTon_KCX";
            this.cmdBCNPLXuatNhapTon_KCX1.Name = "cmdBCNPLXuatNhapTon_KCX1";
            // 
            // cmdBCNPLXuatNhapTon_KCX_TongNPL1
            // 
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL1.Key = "cmdBCNPLXuatNhapTon_KCX_TongNPL";
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL1.Name = "cmdBCNPLXuatNhapTon_KCX_TongNPL1";
            // 
            // cmdBCNPLXuatNhapTon_KCX_1281
            // 
            this.cmdBCNPLXuatNhapTon_KCX_1281.Key = "cmdBCNPLXuatNhapTon_KCX_128";
            this.cmdBCNPLXuatNhapTon_KCX_1281.Name = "cmdBCNPLXuatNhapTon_KCX_1281";
            // 
            // cmdBCNPLXuatNhapTon_KCX_TongNPL_1281
            // 
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL_1281.Key = "cmdBCNPLXuatNhapTon_KCX_TongNPL_128";
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL_1281.Name = "cmdBCNPLXuatNhapTon_KCX_TongNPL_1281";
            // 
            // cmdBCNPLXuatNhapTon_KCX_TT221
            // 
            this.cmdBCNPLXuatNhapTon_KCX_TT221.Key = "cmdBCNPLXuatNhapTon_KCX_TT22";
            this.cmdBCNPLXuatNhapTon_KCX_TT221.Name = "cmdBCNPLXuatNhapTon_KCX_TT221";
            // 
            // cmdTQDT
            // 
            this.cmdTQDT.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.KhaiBao2,
            this.NhanDuLieu1,
            this.Huy2,
            this.cmdKetQuaXuLy1});
            this.cmdTQDT.Image = ((System.Drawing.Image)(resources.GetObject("cmdTQDT.Image")));
            this.cmdTQDT.Key = "cmdTQDT";
            this.cmdTQDT.Name = "cmdTQDT";
            this.cmdTQDT.Text = "Thông quan điện tử";
            // 
            // KhaiBao2
            // 
            this.KhaiBao2.Key = "KhaiBao";
            this.KhaiBao2.Name = "KhaiBao2";
            // 
            // NhanDuLieu1
            // 
            this.NhanDuLieu1.Key = "NhanDuLieu";
            this.NhanDuLieu1.Name = "NhanDuLieu1";
            // 
            // Huy2
            // 
            this.Huy2.Key = "Huy";
            this.Huy2.Name = "Huy2";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaXuLy1.Image")));
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // cmdChungTuTT
            // 
            this.cmdChungTuTT.Image = ((System.Drawing.Image)(resources.GetObject("cmdChungTuTT.Image")));
            this.cmdChungTuTT.Key = "cmdChungTuTT";
            this.cmdChungTuTT.Name = "cmdChungTuTT";
            this.cmdChungTuTT.Text = "Chứng từ thanh toán";
            // 
            // cmdBKToKhaiNhap_KCX
            // 
            this.cmdBKToKhaiNhap_KCX.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiNhap_KCX.Image")));
            this.cmdBKToKhaiNhap_KCX.Key = "cmdBKToKhaiNhap_KCX";
            this.cmdBKToKhaiNhap_KCX.Name = "cmdBKToKhaiNhap_KCX";
            this.cmdBKToKhaiNhap_KCX.Text = "BC01 - Bảng kê tờ khai nhập khẩu đưa vào thanh khoản (KCX)";
            // 
            // cmdBKToKhaiXuat_KCX
            // 
            this.cmdBKToKhaiXuat_KCX.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiXuat_KCX.Image")));
            this.cmdBKToKhaiXuat_KCX.Key = "cmdBKToKhaiXuat_KCX";
            this.cmdBKToKhaiXuat_KCX.Name = "cmdBKToKhaiXuat_KCX";
            this.cmdBKToKhaiXuat_KCX.Text = "BC02 - Bảng kê tờ khai xuất khẩu đưa vào thanh khoản (KCX)";
            // 
            // cmdBCNPLXuatNhapTon_KCX
            // 
            this.cmdBCNPLXuatNhapTon_KCX.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon_KCX.Image")));
            this.cmdBCNPLXuatNhapTon_KCX.Key = "cmdBCNPLXuatNhapTon_KCX";
            this.cmdBCNPLXuatNhapTon_KCX.Name = "cmdBCNPLXuatNhapTon_KCX";
            this.cmdBCNPLXuatNhapTon_KCX.Text = "BC03 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn (KCX - TT 194)";
            this.cmdBCNPLXuatNhapTon_KCX.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdBCNPLXuatNhapTon_KCX_TongNPL
            // 
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon_KCX_TongNPL.Image")));
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL.Key = "cmdBCNPLXuatNhapTon_KCX_TongNPL";
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL.Name = "cmdBCNPLXuatNhapTon_KCX_TongNPL";
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL.Text = "BC03.1 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn - Tổng NPL (KCX - TT 194)";
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdBCNPLXuatNhapTon_TongNPL
            // 
            this.cmdBCNPLXuatNhapTon_TongNPL.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon_TongNPL.Image")));
            this.cmdBCNPLXuatNhapTon_TongNPL.Key = "cmdBCNPLXuatNhapTon_TongNPL";
            this.cmdBCNPLXuatNhapTon_TongNPL.Name = "cmdBCNPLXuatNhapTon_TongNPL";
            this.cmdBCNPLXuatNhapTon_TongNPL.Text = "BC03.1 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn - Tổng NPL (TT 194)";
            this.cmdBCNPLXuatNhapTon_TongNPL.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdBCNPLXuatNhapTon_196
            // 
            this.cmdBCNPLXuatNhapTon_196.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon_196.Image")));
            this.cmdBCNPLXuatNhapTon_196.Key = "cmdBCNPLXuatNhapTon_196";
            this.cmdBCNPLXuatNhapTon_196.Name = "cmdBCNPLXuatNhapTon_196";
            this.cmdBCNPLXuatNhapTon_196.Text = "BC03 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn (TT 196)";
            // 
            // cmdBCThueXNK_196
            // 
            this.cmdBCThueXNK_196.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCThueXNK_196.Image")));
            this.cmdBCThueXNK_196.Key = "cmdBCThueXNK_196";
            this.cmdBCThueXNK_196.Name = "cmdBCThueXNK_196";
            this.cmdBCThueXNK_196.Text = "Báo cáo tính thuế NPL nhập khẩu";
            // 
            // cmdBKToKhaiNhap_196
            // 
            this.cmdBKToKhaiNhap_196.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiNhap_196.Image")));
            this.cmdBKToKhaiNhap_196.Key = "cmdBKToKhaiNhap_196";
            this.cmdBKToKhaiNhap_196.Name = "cmdBKToKhaiNhap_196";
            this.cmdBKToKhaiNhap_196.Text = "Bảng kê tờ khai nhập khẩu đưa vào thanh khoản (TT 196)";
            // 
            // cmdBKToKhaiXuat_196
            // 
            this.cmdBKToKhaiXuat_196.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiXuat_196.Image")));
            this.cmdBKToKhaiXuat_196.Key = "cmdBKToKhaiXuat_196";
            this.cmdBKToKhaiXuat_196.Name = "cmdBKToKhaiXuat_196";
            this.cmdBKToKhaiXuat_196.Text = "Bảng kê tờ khai xuất khẩu đưa vào thanh khoản (TT 196)";
            // 
            // cmdBCNPLXuatNhapTon_TongNPL_196
            // 
            this.cmdBCNPLXuatNhapTon_TongNPL_196.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon_TongNPL_196.Image")));
            this.cmdBCNPLXuatNhapTon_TongNPL_196.Key = "cmdBCNPLXuatNhapTon_TongNPL_196";
            this.cmdBCNPLXuatNhapTon_TongNPL_196.Name = "cmdBCNPLXuatNhapTon_TongNPL_196";
            this.cmdBCNPLXuatNhapTon_TongNPL_196.Text = "Báo cáo Nguyên phụ liệu Xuất Nhập Tồn - Tổng NPL (TT 196)";
            // 
            // cmdBKToKhaiNhap_128
            // 
            this.cmdBKToKhaiNhap_128.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiNhap_128.Image")));
            this.cmdBKToKhaiNhap_128.Key = "cmdBKToKhaiNhap_128";
            this.cmdBKToKhaiNhap_128.Name = "cmdBKToKhaiNhap_128";
            this.cmdBKToKhaiNhap_128.Text = "BC01 - Bảng kê tờ khai nhập khẩu đưa vào thanh khoản (TT 128)";
            // 
            // cmdBKToKhaiXuat_128
            // 
            this.cmdBKToKhaiXuat_128.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiXuat_128.Image")));
            this.cmdBKToKhaiXuat_128.Key = "cmdBKToKhaiXuat_128";
            this.cmdBKToKhaiXuat_128.Name = "cmdBKToKhaiXuat_128";
            this.cmdBKToKhaiXuat_128.Text = "BC02 - Bảng kê tờ khai xuất khẩu đưa vào thanh khoản (TT 128)";
            // 
            // cmdBCThueNK_128
            // 
            this.cmdBCThueNK_128.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCThueNK_128.Image")));
            this.cmdBCThueNK_128.Key = "cmdBCThueNK_128";
            this.cmdBCThueNK_128.Name = "cmdBCThueNK_128";
            this.cmdBCThueNK_128.Text = "BC04 - Báo cáo tính thuế trên NPL nhập khẩu (TT 128)";
            // 
            // cmdBCNPLXuatNhapTon_128
            // 
            this.cmdBCNPLXuatNhapTon_128.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon_128.Image")));
            this.cmdBCNPLXuatNhapTon_128.Key = "cmdBCNPLXuatNhapTon_128";
            this.cmdBCNPLXuatNhapTon_128.Name = "cmdBCNPLXuatNhapTon_128";
            this.cmdBCNPLXuatNhapTon_128.Text = "BC03 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn (TT 128)";
            // 
            // cmdBCNPLXuatNhapTon_TongNPL_128
            // 
            this.cmdBCNPLXuatNhapTon_TongNPL_128.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon_TongNPL_128.Image")));
            this.cmdBCNPLXuatNhapTon_TongNPL_128.Key = "cmdBCNPLXuatNhapTon_TongNPL_128";
            this.cmdBCNPLXuatNhapTon_TongNPL_128.Name = "cmdBCNPLXuatNhapTon_TongNPL_128";
            this.cmdBCNPLXuatNhapTon_TongNPL_128.Text = "BC03.1 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn - Tổng NPL (TT 128)";
            // 
            // cmdBCNPLXuatNhapTon_KCX_128
            // 
            this.cmdBCNPLXuatNhapTon_KCX_128.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon_KCX_128.Image")));
            this.cmdBCNPLXuatNhapTon_KCX_128.Key = "cmdBCNPLXuatNhapTon_KCX_128";
            this.cmdBCNPLXuatNhapTon_KCX_128.Name = "cmdBCNPLXuatNhapTon_KCX_128";
            this.cmdBCNPLXuatNhapTon_KCX_128.Text = "BC03 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn (KCX - TT 128)";
            // 
            // cmdBCNPLXuatNhapTon_KCX_TongNPL_128
            // 
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL_128.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon_KCX_TongNPL_128.Image")));
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL_128.Key = "cmdBCNPLXuatNhapTon_KCX_TongNPL_128";
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL_128.Name = "cmdBCNPLXuatNhapTon_KCX_TongNPL_128";
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL_128.Text = "BC03.1 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn - Tổng NPL (KCX - TT 128)";
            // 
            // cmdBaoCaoTonSauThanhKhoan
            // 
            this.cmdBaoCaoTonSauThanhKhoan.Image = ((System.Drawing.Image)(resources.GetObject("cmdBaoCaoTonSauThanhKhoan.Image")));
            this.cmdBaoCaoTonSauThanhKhoan.Key = "cmdBaoCaoTonSauThanhKhoan";
            this.cmdBaoCaoTonSauThanhKhoan.Name = "cmdBaoCaoTonSauThanhKhoan";
            this.cmdBaoCaoTonSauThanhKhoan.Text = "Báo cáo tồn sau thanh khoản";
            // 
            // cmdBCNPLXuatNhapTon_KCX_TT22
            // 
            this.cmdBCNPLXuatNhapTon_KCX_TT22.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTon_KCX_TT22.Image")));
            this.cmdBCNPLXuatNhapTon_KCX_TT22.Key = "cmdBCNPLXuatNhapTon_KCX_TT22";
            this.cmdBCNPLXuatNhapTon_KCX_TT22.Name = "cmdBCNPLXuatNhapTon_KCX_TT22";
            this.cmdBCNPLXuatNhapTon_KCX_TT22.Text = "BC07 - Bảng tổng hợp hàng hóa sản xuất nhập - xuất - tồn (TT22)";
            // 
            // cmdHangTonKho
            // 
            this.cmdHangTonKho.Image = ((System.Drawing.Image)(resources.GetObject("cmdHangTonKho.Image")));
            this.cmdHangTonKho.Key = "cmdHangTonKho";
            this.cmdHangTonKho.Name = "cmdHangTonKho";
            this.cmdHangTonKho.Text = "Bảng kê hàng tồn kho";
            // 
            // cmdBKTaiXUat
            // 
            this.cmdBKTaiXUat.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKTaiXUat.Image")));
            this.cmdBKTaiXUat.Key = "cmdBKTaiXUat";
            this.cmdBKTaiXUat.Name = "cmdBKTaiXUat";
            this.cmdBKTaiXUat.Text = "Bảng kê tái xuất";
            // 
            // cmdBKTieuHuy
            // 
            this.cmdBKTieuHuy.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKTieuHuy.Image")));
            this.cmdBKTieuHuy.Key = "cmdBKTieuHuy";
            this.cmdBKTieuHuy.Name = "cmdBKTieuHuy";
            this.cmdBKTieuHuy.Text = "Bảng kê tiêu hủy";
            // 
            // cmdBangKe
            // 
            this.cmdBangKe.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHangTonKho1,
            this.cmdBKTaiXUat1,
            this.cmdBKTieuHuy1});
            this.cmdBangKe.Image = ((System.Drawing.Image)(resources.GetObject("cmdBangKe.Image")));
            this.cmdBangKe.Key = "cmdBangKe";
            this.cmdBangKe.Name = "cmdBangKe";
            this.cmdBangKe.Text = "Bảng kê";
            // 
            // cmdHangTonKho1
            // 
            this.cmdHangTonKho1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHangTonKho1.Image")));
            this.cmdHangTonKho1.Key = "cmdHangTonKho";
            this.cmdHangTonKho1.Name = "cmdHangTonKho1";
            // 
            // cmdBKTaiXUat1
            // 
            this.cmdBKTaiXUat1.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKTaiXUat1.Image")));
            this.cmdBKTaiXUat1.Key = "cmdBKTaiXUat";
            this.cmdBKTaiXUat1.Name = "cmdBKTaiXUat1";
            // 
            // cmdBKTieuHuy1
            // 
            this.cmdBKTieuHuy1.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKTieuHuy1.Image")));
            this.cmdBKTieuHuy1.Key = "cmdBKTieuHuy";
            this.cmdBKTieuHuy1.Name = "cmdBKTieuHuy1";
            // 
            // cmdBKToKhaiNhap_TT38
            // 
            this.cmdBKToKhaiNhap_TT38.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiNhap_TT38.Image")));
            this.cmdBKToKhaiNhap_TT38.Key = "cmdBKToKhaiNhap_TT38";
            this.cmdBKToKhaiNhap_TT38.Name = "cmdBKToKhaiNhap_TT38";
            this.cmdBKToKhaiNhap_TT38.Text = "BK Tờ khai nhập đưa vào thanh khoản (TT 38)";
            // 
            // cmdBKToKhaiXuat_TT38
            // 
            this.cmdBKToKhaiXuat_TT38.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiXuat_TT38.Image")));
            this.cmdBKToKhaiXuat_TT38.Key = "cmdBKToKhaiXuat_TT38";
            this.cmdBKToKhaiXuat_TT38.Name = "cmdBKToKhaiXuat_TT38";
            this.cmdBKToKhaiXuat_TT38.Text = "BK Tờ khai xuất đưa vào thanh khoản (TT 38)";
            // 
            // cmdBKToKhaiXNKKhongThuThue_TT38
            // 
            this.cmdBKToKhaiXNKKhongThuThue_TT38.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiXNKKhongThuThue_TT38.Image")));
            this.cmdBKToKhaiXNKKhongThuThue_TT38.Key = "cmdBKToKhaiXNKKhongThuThue_TT38";
            this.cmdBKToKhaiXNKKhongThuThue_TT38.Name = "cmdBKToKhaiXNKKhongThuThue_TT38";
            this.cmdBKToKhaiXNKKhongThuThue_TT38.Text = "BK Tờ Khai XNK Không Thu Thuế (TT 38)";
            // 
            // cmdBKToKhaiXNKHoan_TT38
            // 
            this.cmdBKToKhaiXNKHoan_TT38.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiXNKHoan_TT38.Image")));
            this.cmdBKToKhaiXNKHoan_TT38.Key = "cmdBKToKhaiXNKHoan_TT38";
            this.cmdBKToKhaiXNKHoan_TT38.Name = "cmdBKToKhaiXNKHoan_TT38";
            this.cmdBKToKhaiXNKHoan_TT38.Text = "BK Tờ Khai XNK Hoàn (TT 38)";
            // 
            // cmdBKToKhaiNhapXinHoan_TT38
            // 
            this.cmdBKToKhaiNhapXinHoan_TT38.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiNhapXinHoan_TT38.Image")));
            this.cmdBKToKhaiNhapXinHoan_TT38.Key = "cmdBKToKhaiNhapXinHoan_TT38";
            this.cmdBKToKhaiNhapXinHoan_TT38.Name = "cmdBKToKhaiNhapXinHoan_TT38";
            this.cmdBKToKhaiNhapXinHoan_TT38.Text = "BK Tờ khai nhập xin hoàn (TT 38)";
            // 
            // cmdBKToKhaiNhapKhongThu_TT38
            // 
            this.cmdBKToKhaiNhapKhongThu_TT38.Image = ((System.Drawing.Image)(resources.GetObject("cmdBKToKhaiNhapKhongThu_TT38.Image")));
            this.cmdBKToKhaiNhapKhongThu_TT38.Key = "cmdBKToKhaiNhapKhongThu_TT38";
            this.cmdBKToKhaiNhapKhongThu_TT38.Name = "cmdBKToKhaiNhapKhongThu_TT38";
            this.cmdBKToKhaiNhapKhongThu_TT38.Text = "BK Tờ khai nhập không thu (TT 38)";
            // 
            // cmdThanhKhoanTheoMaHang
            // 
            this.cmdThanhKhoanTheoMaHang.Key = "cmdThanhKhoanTheoMaHang";
            this.cmdThanhKhoanTheoMaHang.Name = "cmdThanhKhoanTheoMaHang";
            this.cmdThanhKhoanTheoMaHang.Text = "Thanh khoản theo mã hàng (TT 38)";
            // 
            // cmdBCNPLXuatNhapTonTheoTK_TT38
            // 
            this.cmdBCNPLXuatNhapTonTheoTK_TT38.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTonTheoTK_TT38.Image")));
            this.cmdBCNPLXuatNhapTonTheoTK_TT38.Key = "cmdBCNPLXuatNhapTonTheoTK_TT38";
            this.cmdBCNPLXuatNhapTonTheoTK_TT38.Name = "cmdBCNPLXuatNhapTonTheoTK_TT38";
            this.cmdBCNPLXuatNhapTonTheoTK_TT38.Text = "BC Xuất nhập tồn theo tờ khai (TT 38)";
            // 
            // cmdBCNPLXuatNhapTonTheoNPL_TT38
            // 
            this.cmdBCNPLXuatNhapTonTheoNPL_TT38.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCNPLXuatNhapTonTheoNPL_TT38.Image")));
            this.cmdBCNPLXuatNhapTonTheoNPL_TT38.Key = "cmdBCNPLXuatNhapTonTheoNPL_TT38";
            this.cmdBCNPLXuatNhapTonTheoNPL_TT38.Name = "cmdBCNPLXuatNhapTonTheoNPL_TT38";
            this.cmdBCNPLXuatNhapTonTheoNPL_TT38.Text = "BC Xuất nhập tồn theo NPL (TT 38)";
            // 
            // cmdBCThueNPLNhapKhau_TT38
            // 
            this.cmdBCThueNPLNhapKhau_TT38.Image = ((System.Drawing.Image)(resources.GetObject("cmdBCThueNPLNhapKhau_TT38.Image")));
            this.cmdBCThueNPLNhapKhau_TT38.Key = "cmdBCThueNPLNhapKhau_TT38";
            this.cmdBCThueNPLNhapKhau_TT38.Name = "cmdBCThueNPLNhapKhau_TT38";
            this.cmdBCThueNPLNhapKhau_TT38.Text = "BC tính thuế trên NPL nhập khẩu (TT 38)";
            // 
            // cmdTT38_2015
            // 
            this.cmdTT38_2015.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdBKToKhaiNhap_1962,
            this.cmdBKToKhaiXuat_1962,
            this.Separator5,
            this.cmdBKToKhaiNhap_TT382,
            this.cmdBKToKhaiXuat_TT382,
            this.cmdBKToKhaiNhapXinHoan_TT382,
            this.cmdBKToKhaiNhapKhongThu_TT382,
            this.cmdBCNPLXuatNhapTonTheoTK_TT382,
            this.cmdBCNPLXuatNhapTonTheoNPL_TT382,
            this.cmdBCThueNPLNhapKhau_TT382});
            this.cmdTT38_2015.Image = ((System.Drawing.Image)(resources.GetObject("cmdTT38_2015.Image")));
            this.cmdTT38_2015.Key = "cmdTT38_2015";
            this.cmdTT38_2015.Name = "cmdTT38_2015";
            this.cmdTT38_2015.Text = "Báo cáo TT38/2015";
            // 
            // cmdBKToKhaiNhap_1962
            // 
            this.cmdBKToKhaiNhap_1962.Key = "cmdBKToKhaiNhap_196";
            this.cmdBKToKhaiNhap_1962.Name = "cmdBKToKhaiNhap_1962";
            // 
            // cmdBKToKhaiXuat_1962
            // 
            this.cmdBKToKhaiXuat_1962.Key = "cmdBKToKhaiXuat_196";
            this.cmdBKToKhaiXuat_1962.Name = "cmdBKToKhaiXuat_1962";
            // 
            // Separator5
            // 
            this.Separator5.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator5.Key = "Separator";
            this.Separator5.Name = "Separator5";
            // 
            // cmdBKToKhaiNhap_TT382
            // 
            this.cmdBKToKhaiNhap_TT382.Key = "cmdBKToKhaiNhap_TT38";
            this.cmdBKToKhaiNhap_TT382.Name = "cmdBKToKhaiNhap_TT382";
            // 
            // cmdBKToKhaiXuat_TT382
            // 
            this.cmdBKToKhaiXuat_TT382.Key = "cmdBKToKhaiXuat_TT38";
            this.cmdBKToKhaiXuat_TT382.Name = "cmdBKToKhaiXuat_TT382";
            // 
            // cmdBKToKhaiNhapXinHoan_TT382
            // 
            this.cmdBKToKhaiNhapXinHoan_TT382.Key = "cmdBKToKhaiNhapXinHoan_TT38";
            this.cmdBKToKhaiNhapXinHoan_TT382.Name = "cmdBKToKhaiNhapXinHoan_TT382";
            // 
            // cmdBKToKhaiNhapKhongThu_TT382
            // 
            this.cmdBKToKhaiNhapKhongThu_TT382.Key = "cmdBKToKhaiNhapKhongThu_TT38";
            this.cmdBKToKhaiNhapKhongThu_TT382.Name = "cmdBKToKhaiNhapKhongThu_TT382";
            // 
            // cmdBCNPLXuatNhapTonTheoTK_TT382
            // 
            this.cmdBCNPLXuatNhapTonTheoTK_TT382.Key = "cmdBCNPLXuatNhapTonTheoTK_TT38";
            this.cmdBCNPLXuatNhapTonTheoTK_TT382.Name = "cmdBCNPLXuatNhapTonTheoTK_TT382";
            // 
            // cmdBCNPLXuatNhapTonTheoNPL_TT382
            // 
            this.cmdBCNPLXuatNhapTonTheoNPL_TT382.Key = "cmdBCNPLXuatNhapTonTheoNPL_TT38";
            this.cmdBCNPLXuatNhapTonTheoNPL_TT382.Name = "cmdBCNPLXuatNhapTonTheoNPL_TT382";
            // 
            // cmdBCThueNPLNhapKhau_TT382
            // 
            this.cmdBCThueNPLNhapKhau_TT382.Key = "cmdBCThueNPLNhapKhau_TT38";
            this.cmdBCThueNPLNhapKhau_TT382.Name = "cmdBCThueNPLNhapKhau_TT382";
            // 
            // cmdXemSPXuat
            // 
            this.cmdXemSPXuat.Image = ((System.Drawing.Image)(resources.GetObject("cmdXemSPXuat.Image")));
            this.cmdXemSPXuat.Key = "cmdXemSPXuat";
            this.cmdXemSPXuat.Name = "cmdXemSPXuat";
            this.cmdXemSPXuat.Text = "Xem cân đối sản phẩm xuất";
            // 
            // cmdNgayTK
            // 
            this.cmdNgayTK.Image = ((System.Drawing.Image)(resources.GetObject("cmdNgayTK.Image")));
            this.cmdNgayTK.Key = "cmdNgayTK";
            this.cmdNgayTK.Name = "cmdNgayTK";
            this.cmdNgayTK.Text = "Ngày đưa vào TK";
            // 
            // cmdDonGiaTT
            // 
            this.cmdDonGiaTT.Image = ((System.Drawing.Image)(resources.GetObject("cmdDonGiaTT.Image")));
            this.cmdDonGiaTT.Key = "cmdDonGiaTT";
            this.cmdDonGiaTT.Name = "cmdDonGiaTT";
            this.cmdDonGiaTT.Text = "Đơn giá TT";
            // 
            // cmdViewNPLCU
            // 
            this.cmdViewNPLCU.Image = ((System.Drawing.Image)(resources.GetObject("cmdViewNPLCU.Image")));
            this.cmdViewNPLCU.Key = "cmdViewNPLCU";
            this.cmdViewNPLCU.Name = "cmdViewNPLCU";
            this.cmdViewNPLCU.Text = "Xem NPL cung ứng";
            // 
            // cmdSendNPLCU
            // 
            this.cmdSendNPLCU.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdViewNPLCU1});
            this.cmdSendNPLCU.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendNPLCU.Image")));
            this.cmdSendNPLCU.Key = "cmdSendNPLCU";
            this.cmdSendNPLCU.Name = "cmdSendNPLCU";
            this.cmdSendNPLCU.Text = "Khai báo NPL cung ứng";
            // 
            // cmdViewNPLCU1
            // 
            this.cmdViewNPLCU1.Key = "cmdViewNPLCU";
            this.cmdViewNPLCU1.Name = "cmdViewNPLCU1";
            // 
            // cmdUpdateTK
            // 
            this.cmdUpdateTK.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateTK.Image")));
            this.cmdUpdateTK.Key = "cmdUpdateTK";
            this.cmdUpdateTK.Name = "cmdUpdateTK";
            this.cmdUpdateTK.Text = "Cập nhật tờ khai sai số liệu";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1169, 68);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Image = ((System.Drawing.Image)(resources.GetObject("uiButton2.Image")));
            this.uiButton2.Location = new System.Drawing.Point(1015, 15);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(83, 23);
            this.uiButton2.TabIndex = 6;
            this.uiButton2.Text = "Xóa hồ sơ";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // btnXoaBK
            // 
            this.btnXoaBK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoaBK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaBK.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaBK.Image")));
            this.btnXoaBK.Location = new System.Drawing.Point(908, 15);
            this.btnXoaBK.Name = "btnXoaBK";
            this.btnXoaBK.Size = new System.Drawing.Size(101, 23);
            this.btnXoaBK.TabIndex = 5;
            this.btnXoaBK.Text = "Xóa bảng kê";
            this.btnXoaBK.VisualStyleManager = this.vsmMain;
            this.btnXoaBK.Click += new System.EventHandler(this.btnXoaBK_Click);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox5.Controls.Add(this.uiGroupBox1);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1169, 146);
            this.uiGroupBox5.TabIndex = 8;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.linkChenhLech);
            this.uiGroupBox4.Controls.Add(this.numChenhlech);
            this.uiGroupBox4.Controls.Add(this.chkKhaiBaoBangKe);
            this.uiGroupBox4.Controls.Add(this.numSoLuongChungTu);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.label15);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox4.Controls.Add(this.txtNgayTiepNhan);
            this.uiGroupBox4.Controls.Add(this.dtNgayKhaiBao);
            this.uiGroupBox4.Controls.Add(this.lblTrangThai);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(516, 8);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(650, 135);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Thông tin khai báo";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // linkChenhLech
            // 
            this.linkChenhLech.AutoSize = true;
            this.linkChenhLech.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkChenhLech.Location = new System.Drawing.Point(303, 77);
            this.linkChenhLech.Name = "linkChenhLech";
            this.linkChenhLech.Size = new System.Drawing.Size(60, 13);
            this.linkChenhLech.TabIndex = 12;
            this.linkChenhLech.TabStop = true;
            this.linkChenhLech.Text = "Chênh lệch";
            // 
            // numChenhlech
            // 
            this.numChenhlech.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numChenhlech.Location = new System.Drawing.Point(377, 72);
            this.numChenhlech.Name = "numChenhlech";
            this.numChenhlech.ReadOnly = true;
            this.numChenhlech.Size = new System.Drawing.Size(41, 21);
            this.numChenhlech.TabIndex = 10;
            this.numChenhlech.Text = "0";
            this.numChenhlech.Value = ((long)(0));
            this.numChenhlech.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int64;
            this.numChenhlech.VisualStyleManager = this.vsmMain;
            // 
            // chkKhaiBaoBangKe
            // 
            this.chkKhaiBaoBangKe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkKhaiBaoBangKe.Location = new System.Drawing.Point(111, 99);
            this.chkKhaiBaoBangKe.Name = "chkKhaiBaoBangKe";
            this.chkKhaiBaoBangKe.Size = new System.Drawing.Size(288, 23);
            this.chkKhaiBaoBangKe.TabIndex = 9;
            this.chkKhaiBaoBangKe.Text = "Chỉ khai báo các tờ khai tham gia thanh khoản";
            // 
            // numSoLuongChungTu
            // 
            this.numSoLuongChungTu.AllowParentheses = Janus.Windows.GridEX.TriState.False;
            this.numSoLuongChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numSoLuongChungTu.FormatString = "#0";
            this.numSoLuongChungTu.Location = new System.Drawing.Point(304, 17);
            this.numSoLuongChungTu.Name = "numSoLuongChungTu";
            this.numSoLuongChungTu.ReadOnly = true;
            this.numSoLuongChungTu.Size = new System.Drawing.Size(114, 21);
            this.numSoLuongChungTu.TabIndex = 1;
            this.numSoLuongChungTu.Text = "0";
            this.numSoLuongChungTu.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numSoLuongChungTu.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(14, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Trạng thái";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(15, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Số tiếp nhận";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(207, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Ngày tiếp nhận";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(14, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Ngày khai báo";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(199, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Số chứng từ kèm";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(111, 46);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(82, 21);
            this.txtSoTiepNhan.TabIndex = 2;
            this.txtSoTiepNhan.Text = "0";
            this.txtSoTiepNhan.Value = ((long)(0));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int64;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtNgayTiepNhan
            // 
            this.txtNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayTiepNhan.Location = new System.Drawing.Point(304, 44);
            this.txtNgayTiepNhan.Name = "txtNgayTiepNhan";
            this.txtNgayTiepNhan.ReadOnly = true;
            this.txtNgayTiepNhan.Size = new System.Drawing.Size(114, 21);
            this.txtNgayTiepNhan.TabIndex = 3;
            this.txtNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // dtNgayKhaiBao
            // 
            this.dtNgayKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayKhaiBao.Location = new System.Drawing.Point(111, 19);
            this.dtNgayKhaiBao.Name = "dtNgayKhaiBao";
            this.dtNgayKhaiBao.ReadOnly = true;
            this.dtNgayKhaiBao.Size = new System.Drawing.Size(82, 21);
            this.dtNgayKhaiBao.TabIndex = 0;
            this.dtNgayKhaiBao.VisualStyleManager = this.vsmMain;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(108, 77);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(142, 13);
            this.lblTrangThai.TabIndex = 4;
            this.lblTrangThai.Text = "Chưa gửi hồ sơ lên Hải quan";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.btnThemMoi);
            this.uiGroupBox3.Controls.Add(this.cbbBangKe);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 146);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1169, 61);
            this.uiGroupBox3.TabIndex = 9;
            this.uiGroupBox3.Text = "Thêm mới bảng kê";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(38, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Bảng kê chưa có";
            // 
            // btnThemMoi
            // 
            this.btnThemMoi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnThemMoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemMoi.Image = ((System.Drawing.Image)(resources.GetObject("btnThemMoi.Image")));
            this.btnThemMoi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnThemMoi.Location = new System.Drawing.Point(966, 28);
            this.btnThemMoi.Name = "btnThemMoi";
            this.btnThemMoi.Size = new System.Drawing.Size(94, 23);
            this.btnThemMoi.TabIndex = 2;
            this.btnThemMoi.Text = "Thêm mới";
            this.btnThemMoi.VisualStyleManager = this.vsmMain;
            this.btnThemMoi.Click += new System.EventHandler(this.btnThemMoi_Click);
            // 
            // cbbBangKe
            // 
            this.cbbBangKe.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbBangKe.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "BK01 - Bảng kê tờ khai nhập khẩu chưa thanh lý";
            uiComboBoxItem1.Value = "DTLTKN";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "BK02 - Bảng kê tờ khai xuất khẩu chưa thanh lý";
            uiComboBoxItem2.Value = "DTLTKX";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "BK03 - Bảng kê nguyên phụ liệu chưa thanh lý";
            uiComboBoxItem3.Value = "DTLNPLCHUATL";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "BK04 - Bảng kê NPL hủy, biếu, tặng";
            uiComboBoxItem4.Value = "DTLNPLXH";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "BK05 - Bảng kê NPL tái xuất";
            uiComboBoxItem5.Value = "DTLNPLTX";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "BK06 - Bảng kê NPL xuất sử dụng tờ khai NKD";
            uiComboBoxItem6.Value = "DTLNPLNKD";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "BK07 - Bảng kê nguyên phụ liệu nộp thuế tiêu thụ nội địa";
            uiComboBoxItem7.Value = "DTLNPLNT";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "BK08 - Bảng kê nguyên phụ liệu xuất theo loại hình XGC";
            uiComboBoxItem8.Value = "DTLNPLXGC";
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "BK09 - Bảng kê nguyên phụ liệu tạm nộp thuế";
            uiComboBoxItem9.Value = "DTLCHITIETNT";
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "BK10 - Bảng kê nguyên phụ liệu tự cung ứng";
            uiComboBoxItem10.Value = "DTLNPLTCU";
            this.cbbBangKe.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9,
            uiComboBoxItem10});
            this.cbbBangKe.Location = new System.Drawing.Point(150, 28);
            this.cbbBangKe.Name = "cbbBangKe";
            this.cbbBangKe.SelectedItemFormatStyle.BackColor = System.Drawing.Color.Silver;
            this.cbbBangKe.SelectedItemFormatStyle.BackColorGradient = System.Drawing.Color.Lavender;
            this.cbbBangKe.SelectedItemFormatStyle.BackgroundGradientMode = Janus.Windows.UI.BackgroundGradientMode.Horizontal;
            this.cbbBangKe.Size = new System.Drawing.Size(806, 21);
            this.cbbBangKe.TabIndex = 1;
            this.cbbBangKe.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnClose);
            this.uiGroupBox6.Controls.Add(this.uiButton2);
            this.uiGroupBox6.Controls.Add(this.btnXoaBK);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 527);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1169, 44);
            this.uiGroupBox6.TabIndex = 10;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dgList);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 207);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1169, 320);
            this.uiGroupBox2.TabIndex = 11;
            this.uiGroupBox2.Text = "Danh sách bảng kê đã khai của hồ sơ thanh khoản";
            this.uiGroupBox2.UseCompatibleTextRendering = true;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.AutomaticSort = false;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(3, 17);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.Size = new System.Drawing.Size(1163, 300);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgList_DeletingRecord);
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // CapNhatHoSoThanhLyForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1169, 639);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CapNhatHoSoThanhLyForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cập nhật hồ sơ thanh khoản";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CapNhatHoSoThanhLyForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoHSTL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayThanhLy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand KhaiBao;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand Luu1;
        private Janus.Windows.UI.CommandBars.UICommand NhanDuLieu;
        private Janus.Windows.UI.CommandBars.UICommand Huy;
        private Janus.Windows.UI.CommandBars.UICommand Luu;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhLy;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCThueXNK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCThueXNK;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTrangThaiTK;
        private Janus.Windows.UI.CommandBars.UICommand cmdRollback;
        private Janus.Windows.UI.CommandBars.UICommand cmdClose;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhKhoan1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhKhoan;
        private Janus.Windows.UI.CommandBars.UICommand ThanhLy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdClose1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiDT;
        private Janus.Windows.UI.CommandBars.UICommand KhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand Huy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdGuiBK;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyBK;
        private Janus.Windows.UI.CommandBars.UICommand cmdRollback1;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sendBK1;
        private System.Windows.Forms.ToolStripMenuItem huyBangKe1;
        private System.Windows.Forms.ToolStripMenuItem xacNhanThongTin1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCanDoiNX1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCanDoiNX;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuThanhToan;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLanThanhLy;
        private System.Windows.Forms.Label label5;
        private Company.Interface.Controls.DonViHaiQuanNewControl donViHaiQuanNewControl1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatBaoCao;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTKNKNL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTKXKSP1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNLSXXK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCXNTNL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCTTNLNK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTKNKNL;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTKXKSP;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNLSXXK;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCXNTNL;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCTTNLNK;
        private Janus.Windows.EditControls.UIButton btnXoaBK;
        private Janus.Windows.UI.CommandBars.UICommand cmdFixAndRun;
        private Janus.Windows.UI.CommandBars.UICommand cmdTT79KCX1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTT79KCX;
        private Janus.Windows.UI.CommandBars.UICommand cmdTQDT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTQDT;
        private Janus.Windows.UI.CommandBars.UICommand KhaiBao2;
        private Janus.Windows.UI.CommandBars.UICommand NhanDuLieu1;
        private Janus.Windows.UI.CommandBars.UICommand Huy2;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy;
        private Janus.Windows.UI.CommandBars.UICommand cmdFixAndRun1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuThanhToan1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuTT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuTT;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_KCX;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_KCX;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_KCX1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_KCX1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX_TongNPL;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_TongNPL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX_TongNPL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_TongNPL;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_1961;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_196;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCThueXNK_1961;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCThueXNK_196;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_1961;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_196;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_1961;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_TongNPL_1961;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_196;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_TongNPL_196;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_128;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_128;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_1281;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_1281;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_1281;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_TongNPL_1281;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCThueNK_1281;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCThueNK_128;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_128;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_TongNPL_128;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX_TongNPL_1281;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX_128;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX_TongNPL_128;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX_1281;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQuyetDinh;
        private Janus.Windows.UI.CommandBars.UICommand cmdBaoCaoTonSauThanhKhoan;
        private Janus.Windows.UI.CommandBars.UICommand cmdBaoCaoTonSauThanhKhoan1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX_TT221;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX_TT22;
        private Janus.Windows.UI.CommandBars.UICommand cmdHangTonKho;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTaiXUat;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtNam;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtQuy;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTieuHuy;
        private Janus.Windows.UI.CommandBars.UICommand cmdBangKe;
        private Janus.Windows.UI.CommandBars.UICommand cmdHangTonKho1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTaiXUat1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTieuHuy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_TT38;
        private Janus.Windows.UI.CommandBars.UICommand Separator4;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_TT38;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXNKKhongThuThue_TT38;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXNKHoan_TT38;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhapXinHoan_TT38;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhapKhongThu_TT38;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhKhoanTheoMaHang;
        private Janus.Windows.UI.CommandBars.UICommand Separator3;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhKhoanTheoMaHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTonTheoTK_TT38;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTonTheoNPL_TT38;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCThueNPLNhapKhau_TT38;
        private System.Windows.Forms.Label lblHinhThuc;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.UI.CommandBars.UICommand cmdTT38_20151;
        private Janus.Windows.UI.CommandBars.UICommand cmdTT38_2015;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_TT382;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_TT382;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhapXinHoan_TT382;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTonTheoTK_TT382;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTonTheoNPL_TT382;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCThueNPLNhapKhau_TT382;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_TT381;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_TT381;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhapXinHoan_TT381;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhapKhongThu_TT381;
        private Janus.Windows.UI.CommandBars.UICommand Separator5;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhapKhongThu_TT382;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_1962;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_1962;
        private Janus.Windows.UI.CommandBars.UICommand cmdXemSPXuat;
        private Janus.Windows.UI.CommandBars.UICommand cmdXemSPXuat1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.LinkLabel linkChenhLech;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numChenhlech;
        private Janus.Windows.EditControls.UICheckBox chkKhaiBaoBangKe;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numSoLuongChungTu;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox dtNgayKhaiBao;
        private System.Windows.Forms.Label lblTrangThai;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIButton btnThemMoi;
        private Janus.Windows.EditControls.UIComboBox cbbBangKe;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.UI.CommandBars.UICommand cmdNgayTK;
        private Janus.Windows.UI.CommandBars.UICommand cmdDonGiaTT;
        private Janus.Windows.UI.CommandBars.UICommand cmdNgayTK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDonGiaTT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendNPLCU1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateTK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdViewNPLCU;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendNPLCU;
        private Janus.Windows.UI.CommandBars.UICommand cmdViewNPLCU1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateTK;
    }
}
