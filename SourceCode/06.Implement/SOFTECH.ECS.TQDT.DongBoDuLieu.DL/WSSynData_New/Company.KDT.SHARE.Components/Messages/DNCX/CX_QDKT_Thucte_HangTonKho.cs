﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class CX_QDKT_Thucte_HangTonKho
    {
       /// <summary>
        /// Thông tin thông báo kiểm tra
       /// </summary>
       [XmlElement("Examination")]
       public Examination Examination { get; set; }
    }
}
