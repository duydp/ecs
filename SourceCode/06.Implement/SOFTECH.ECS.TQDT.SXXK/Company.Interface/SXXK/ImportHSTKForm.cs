using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Company.BLL.SXXK.ThanhKhoan;

namespace Company.Interface.SXXK
{
    public partial class ImportHSTKForm : BaseForm
    {
        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection NPLNTCollection = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection();
        public Company.BLL.KDT.SXXK.HoSoThanhLyDangKy HSTL = new Company.BLL.KDT.SXXK.HoSoThanhLyDangKy();
        public Company.BLL.KDT.SXXK.BKToKhaiXuatCollection bkTKXCollection = new Company.BLL.KDT.SXXK.BKToKhaiXuatCollection();
        public Company.BLL.KDT.SXXK.BKToKhaiNhapCollection bkTKNCollection = new Company.BLL.KDT.SXXK.BKToKhaiNhapCollection();

        public ImportHSTKForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                btnSave.Enabled = false;
                this.HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (this.HSTL.checkHSTLHasCreated(MainForm.EcsQuanTri.Identity.Name))
                {
                    MLMessages("Hồ sơ thanh khoản lần trước đã tạo!", "", "", false);
                }
                else {
                    this.HSTL.MaHaiQuanTiepNhan = GlobalSettings.MA_HAI_QUAN;
                    this.HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.HSTL.TrangThaiXuLy = Company.BLL.TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.HSTL.NgayBatDau = DateTime.Today;
                    this.HSTL.LanThanhLy = this.HSTL.GetLanThanhLyMoiNhat(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                    this.HSTL.UserName = MainForm.EcsQuanTri.Identity.Name;
                    // Insert bảng kê
                    
                    foreach (GridEXRow row in dgListTKXuat.GetRows()){
                        if (row.RowType == RowType.Record)
                        {
                            Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmd = (Company.BLL.SXXK.ToKhai.ToKhaiMauDich)row.DataRow;
                            Company.BLL.KDT.SXXK.BKToKhaiXuat bk = new Company.BLL.KDT.SXXK.BKToKhaiXuat();
                            bk.MaHaiQuan = tkmd.MaHaiQuan;
                            bk.MaLoaiHinh = tkmd.MaLoaiHinh;
                            bk.NamDangKy = tkmd.NamDangKy;
                            bk.NgayDangKy = tkmd.NgayDangKy;
                            bk.NgayThucXuat = tkmd.NGAY_THN_THX;
                            bk.SoToKhai = tkmd.SoToKhai;
                            this.bkTKXCollection.Add(bk);                    
                        }
                    }
                    foreach (GridEXRow row in dgListTKNhap.GetRows())
                    {
                        if (row.RowType == RowType.Record)
                        {
                            Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmd = (BLL.SXXK.ToKhai.ToKhaiMauDich)row.DataRow;
                            Company.BLL.KDT.SXXK.BKToKhaiNhap bk = new Company.BLL.KDT.SXXK.BKToKhaiNhap();
                            bk.MaHaiQuan = tkmd.MaHaiQuan;
                            bk.MaLoaiHinh = tkmd.MaLoaiHinh;
                            bk.NamDangKy = tkmd.NamDangKy;
                            bk.NgayDangKy = tkmd.NgayDangKy;
                            bk.NgayThucNhap = tkmd.NGAY_THN_THX;
                            bk.SoToKhai = tkmd.SoToKhai;
                            bk.UserName = MainForm.EcsQuanTri.Identity.Name;
                            this.bkTKNCollection.Add(bk);
                        }
                    }

                    //Tạo hồ sơ thanh lý
                    int i1 = this.HSTL.getBKToKhaiXuat();
                    if (i1 < 0)
                    {
                        Company.BLL.KDT.SXXK.BangKeHoSoThanhLy bk = new Company.BLL.KDT.SXXK.BangKeHoSoThanhLy();
                        bk.MaBangKe = "DTLTKX";
                        bk.TenBangKe = "DTLTKX";
                        bk.STTHang = this.HSTL.BKCollection.Count + 1;
                        bk.bkTKXColletion = this.bkTKXCollection;
                        this.HSTL.BKCollection.Add(bk);
                    }
                    else
                    {
                        this.HSTL.BKCollection[i1].bkTKXColletion = this.bkTKXCollection;
                    }
                    int i = this.HSTL.getBKToKhaiNhap();
                    if (i < 0)
                    {
                        Company.BLL.KDT.SXXK.BangKeHoSoThanhLy bk = new Company.BLL.KDT.SXXK.BangKeHoSoThanhLy();
                        bk.MaBangKe = "DTLTKN";
                        bk.TenBangKe = "DTLTKN";
                        bk.STTHang = this.HSTL.BKCollection.Count + 1;
                        bk.bkTKNCollection = this.bkTKNCollection;
                        this.HSTL.BKCollection.Add(bk);
                    }
                    else
                    {
                        this.HSTL.BKCollection[i].bkTKNCollection = this.bkTKNCollection;
                    }
                    this.HSTL.LanThanhLy = new Company.BLL.KDT.SXXK.HoSoThanhLyDangKy().GetLanThanhLyMoiNhat(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                    if (this.HSTL.InsertUpdateFull())
                    {
                        MLMessages("Hồ sơ thanh khoản đã tạo thành công.", "MSG_THK83", "", false);
                        show_CapNhatHoSoThanhLyForm(this.HSTL);
                        this.Close();
                    }
                    else
                        MLMessages("Tạo mới hồ sơ không thành công.", "MSG_THK84", "", false);
                }
            }
            catch (Exception ex){   ShowMessage(" " + ex.Message, false);   }
            finally
            {
                btnSave.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }
        private void show_CapNhatHoSoThanhLyForm(Company.BLL.KDT.SXXK.HoSoThanhLyDangKy HSTL)
        {
            CapNhatHoSoThanhLyForm capNhatHSTLForm = new CapNhatHoSoThanhLyForm();
            capNhatHSTLForm.HSTL = HSTL;
            capNhatHSTLForm.MdiParent = this.ParentForm;
            capNhatHSTLForm.Show();
        }
        private void ImportTTDMForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        private void btnImportTKN_Click(object sender, EventArgs e)
        {
            ShowFormImportBangKeTKN();
        }

        private void btnImportTKX_Click(object sender, EventArgs e)
        {
            ShowFormImportBangKeTKX();
        }

        private void btnImportNPLTon_Click(object sender, EventArgs e)
        {
            ShowFormImportNPLNhapTon();
        }

        private void ShowFormImportBangKeTKN()
        {
            try
            {
                ImportHSTKTKNForm tkn = new ImportHSTKTKNForm();
                tkn.maLoaiHinh = "N";
                tkn.ShowDialog();
                this.dgListTKNhap.DataSource = tkn.TKMDReadCollection;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void ShowFormImportBangKeTKX()
        {
            try
            {
                ImportHSTKTKNForm tkn = new ImportHSTKTKNForm();
                tkn.maLoaiHinh = "X";
                tkn.ShowDialog();
                this.dgListTKXuat.DataSource = tkn.TKMDReadCollection;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void ShowFormImportNPLNhapTon()
        {
            try
            {
                ImportHSTK_NPLTonForm nplTon = new ImportHSTK_NPLTonForm();
                nplTon.ShowDialog();
                this.dgListNPLTon.DataSource = nplTon.nplNhapTonCollection;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnCapNhatTon_Click(object sender, EventArgs e)
        {
            bool ok=false;
            foreach (GridEXRow row in dgListNPLTon.GetRows())
            {
                if (row.RowType == RowType.Record)
                {
                    Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (BLL.SXXK.ThanhKhoan.NPLNhapTon)row.DataRow;
                    npl.Ton = Convert.ToDecimal(row.Cells["Ton"].Value);
                    npl.Update();
                    ok = true;
                }
            }
            if (ok)
                MessageBox.Show("Cập nhật thành công");
        }

        
    }
}