using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAD8000 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute JKN { get; set; }
public PropertiesAttribute FIC { get; set; }
public PropertiesAttribute BNO { get; set; }
public PropertiesAttribute DNO { get; set; }
public PropertiesAttribute TDN { get; set; }
public PropertiesAttribute A06 { get; set; }
public PropertiesAttribute A02 { get; set; }
public PropertiesAttribute ADA { get; set; }
public PropertiesAttribute ADB { get; set; }
public PropertiesAttribute A05 { get; set; }
public PropertiesAttribute A00 { get; set; }
public PropertiesAttribute A07 { get; set; }
public PropertiesAttribute A08 { get; set; }
public PropertiesAttribute A09 { get; set; }
public PropertiesAttribute AD1 { get; set; }
public PropertiesAttribute AD2 { get; set; }
public PropertiesAttribute AD3 { get; set; }
public PropertiesAttribute RED { get; set; }
public PropertiesAttribute AAA { get; set; }
public PropertiesAttribute B99 { get; set; }
public GroupAttribute CT { get; set; }

public VAD8000()
        {
ICN = new PropertiesAttribute(12, typeof(int));
JKN = new PropertiesAttribute(1, typeof(string));
FIC = new PropertiesAttribute(12, typeof(string));
BNO = new PropertiesAttribute(2, typeof(int));
DNO = new PropertiesAttribute(2, typeof(int));
TDN = new PropertiesAttribute(12, typeof(int));
A06 = new PropertiesAttribute(3, typeof(string));
A02 = new PropertiesAttribute(3, typeof(string));
ADA = new PropertiesAttribute(1, typeof(string));
ADB = new PropertiesAttribute(1, typeof(string));
A05 = new PropertiesAttribute(1, typeof(string));
A00 = new PropertiesAttribute(4, typeof(string));
A07 = new PropertiesAttribute(10, typeof(string));
A08 = new PropertiesAttribute(2, typeof(string));
A09 = new PropertiesAttribute(8, typeof(DateTime));
AD1 = new PropertiesAttribute(6, typeof(int));
AD2 = new PropertiesAttribute(8, typeof(DateTime));
AD3 = new PropertiesAttribute(6, typeof(int));
RED = new PropertiesAttribute(8, typeof(DateTime));
AAA = new PropertiesAttribute(1, typeof(string));
B99 = new PropertiesAttribute(1, typeof(string));
#region CT
List<PropertiesAttribute> listCT = new List<PropertiesAttribute>();
listCT.Add(new PropertiesAttribute(8, 10, EnumGroupID.VAD8000_D01, typeof(DateTime)));
listCT.Add(new PropertiesAttribute(120, 10, EnumGroupID.VAD8000_T01, typeof(string)));
listCT.Add(new PropertiesAttribute(420, 10, EnumGroupID.VAD8000_I01, typeof(string)));
CT = new GroupAttribute("CT", 10, listCT);
#endregion CT
TongSoByte = 5686;
}

}public partial class EnumGroupID
    {
public static readonly string VAD8000_D01 = "VAD8000_D01";
public static readonly string VAD8000_T01 = "VAD8000_T01";
public static readonly string VAD8000_I01 = "VAD8000_I01";
}

}
