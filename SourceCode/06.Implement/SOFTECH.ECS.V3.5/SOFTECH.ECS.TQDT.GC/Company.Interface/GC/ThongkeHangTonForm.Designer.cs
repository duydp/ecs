﻿namespace Company.Interface.GC
{
    partial class ThongkeHangTonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgListNPLTK_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListNPLTK_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgListPhanBo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListPhanBo_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout cbbToKhai_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTKChuaPhanBo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListTKChuaPhanBo_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongkeHangTonForm));
            Janus.Windows.GridEX.GridEXLayout dgToKhaiDaPhanBo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgToKhaiDaPhanBo_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column1.Image");
            this.uicmdThongke = new Janus.Windows.EditControls.UIButton();
            this.uicmdSelectTK = new Janus.Windows.EditControls.UIButton();
            this.uiTabSanPham = new Janus.Windows.UI.Tab.UITab();
            this.tabQLNPLTon = new Janus.Windows.UI.Tab.UITabPage();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnInNPLTon = new Janus.Windows.EditControls.UIButton();
            this.btnChinhDL = new Janus.Windows.EditControls.UIButton();
            this.btnXuatExcelNPLTon = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListNPLTK = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.btnLayDuLieuCungUng = new Janus.Windows.EditControls.UIButton();
            this.uiButton4 = new Janus.Windows.EditControls.UIButton();
            this.btnChuyenTonSXXK = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccTo = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label9 = new System.Windows.Forms.Label();
            this.ccFrom = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.uiButton3 = new Janus.Windows.EditControls.UIButton();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListPhanBo = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.cmdXemBangNPLCungUng = new Janus.Windows.EditControls.UIButton();
            this.btnXuatExcelNPLCungUng = new Janus.Windows.EditControls.UIButton();
            this.cmdViewBangPhanBoNPL = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbToKhai = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbSanPham = new Janus.Windows.EditControls.UIComboBox();
            this.uiButton8 = new Janus.Windows.EditControls.UIButton();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.contextMenuStripPhanBo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemPhanBo = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.progressBarPhanBo = new System.Windows.Forms.ProgressBar();
            this.btnPhanBo = new Janus.Windows.EditControls.UIButton();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.contextMenuStripDaPhanBo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemBangPhanBo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemBangNPLCungUng = new System.Windows.Forms.ToolStripMenuItem();
            this.xuatExcelNPLCungUngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.xemBangNplCungUngTKMuaVNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnDeletePhanBo = new Janus.Windows.EditControls.UIButton();
            this.progressBarDaPhanBo = new System.Windows.Forms.ProgressBar();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDonViHaiQuan = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.ccToDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.ccFromDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label6 = new System.Windows.Forms.Label();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnChonTK = new Janus.Windows.EditControls.UIButton();
            this.dgTKX = new Janus.Windows.GridEX.GridEX();
            this.gridEXPrintDocument1 = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXPrintDocumentNPL = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXExporterNPL = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchNPL = new Janus.Windows.EditControls.UIButton();
            this.label22 = new System.Windows.Forms.Label();
            this.txtMaNguyenPhuLieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchMa = new Janus.Windows.EditControls.UIButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox12 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchTK = new Janus.Windows.EditControls.UIButton();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSoTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox14 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnSearchDate = new Janus.Windows.EditControls.UIButton();
            this.dgListTKChuaPhanBo = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox15 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchNgayDangKy = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox17 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchSoTKDaPB = new Janus.Windows.EditControls.UIButton();
            this.txtSoTKDaPB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.clcDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.clcTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayDangKy = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dgToKhaiDaPhanBo = new Janus.Windows.GridEX.GridEX();
            this.clcToDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label17 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTabSanPham)).BeginInit();
            this.uiTabSanPham.SuspendLayout();
            this.tabQLNPLTon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLTK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListPhanBo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToKhai)).BeginInit();
            this.uiTabPage4.SuspendLayout();
            this.contextMenuStripPhanBo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.contextMenuStripDaPhanBo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).BeginInit();
            this.uiGroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).BeginInit();
            this.uiGroupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKChuaPhanBo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).BeginInit();
            this.uiGroupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).BeginInit();
            this.uiGroupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhaiDaPhanBo)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.grbMain.Controls.Add(this.uiTabSanPham);
            this.grbMain.Size = new System.Drawing.Size(1097, 518);
            // 
            // uicmdThongke
            // 
            this.uicmdThongke.Location = new System.Drawing.Point(698, 27);
            this.uicmdThongke.Name = "uicmdThongke";
            this.uicmdThongke.Size = new System.Drawing.Size(84, 40);
            this.uicmdThongke.TabIndex = 196;
            this.uicmdThongke.Text = "Thống kê";
            this.uicmdThongke.VisualStyleManager = this.vsmMain;
            // 
            // uicmdSelectTK
            // 
            this.uicmdSelectTK.Location = new System.Drawing.Point(592, 27);
            this.uicmdSelectTK.Name = "uicmdSelectTK";
            this.uicmdSelectTK.Size = new System.Drawing.Size(84, 40);
            this.uicmdSelectTK.TabIndex = 195;
            this.uicmdSelectTK.Text = "Chọn tờ khai";
            this.uicmdSelectTK.VisualStyleManager = this.vsmMain;
            // 
            // uiTabSanPham
            // 
            this.uiTabSanPham.BackColor = System.Drawing.Color.Transparent;
            this.uiTabSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTabSanPham.FlatBorderColor = System.Drawing.Color.Transparent;
            this.uiTabSanPham.Location = new System.Drawing.Point(0, 0);
            this.uiTabSanPham.Name = "uiTabSanPham";
            this.uiTabSanPham.Size = new System.Drawing.Size(1097, 518);
            this.uiTabSanPham.TabIndex = 0;
            this.uiTabSanPham.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tabQLNPLTon,
            this.uiTabPage2,
            this.uiTabPage3,
            this.uiTabPage4,
            this.uiTabPage1});
            this.uiTabSanPham.VisualStyleManager = this.vsmMain;
            this.uiTabSanPham.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.uiTabSanPham_SelectedTabChanged_1);
            // 
            // tabQLNPLTon
            // 
            this.tabQLNPLTon.Controls.Add(this.dgList);
            this.tabQLNPLTon.Controls.Add(this.uiGroupBox2);
            this.tabQLNPLTon.Controls.Add(this.uiGroupBox5);
            this.tabQLNPLTon.Key = "tpQLNPLTon";
            this.tabQLNPLTon.Location = new System.Drawing.Point(1, 21);
            this.tabQLNPLTon.Name = "tabQLNPLTon";
            this.tabQLNPLTon.Size = new System.Drawing.Size(1095, 496);
            this.tabQLNPLTon.TabStop = true;
            this.tabQLNPLTon.Text = "Quản lý nguyên phụ liệu tồn";
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 59);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1095, 396);
            this.dgList.TabIndex = 309;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            this.dgList.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_FormattingRow);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.AutoScrollMinSize = new System.Drawing.Size(500, 0);
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnInNPLTon);
            this.uiGroupBox2.Controls.Add(this.btnChinhDL);
            this.uiGroupBox2.Controls.Add(this.btnXuatExcelNPLTon);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 455);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1095, 41);
            this.uiGroupBox2.TabIndex = 308;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnInNPLTon
            // 
            this.btnInNPLTon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInNPLTon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInNPLTon.Image = ((System.Drawing.Image)(resources.GetObject("btnInNPLTon.Image")));
            this.btnInNPLTon.ImageSize = new System.Drawing.Size(20, 20);
            this.btnInNPLTon.Location = new System.Drawing.Point(1014, 11);
            this.btnInNPLTon.Name = "btnInNPLTon";
            this.btnInNPLTon.Size = new System.Drawing.Size(75, 23);
            this.btnInNPLTon.TabIndex = 191;
            this.btnInNPLTon.Text = "In";
            this.btnInNPLTon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnInNPLTon.Click += new System.EventHandler(this.btnInNPLTon_Click);
            // 
            // btnChinhDL
            // 
            this.btnChinhDL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChinhDL.Image = ((System.Drawing.Image)(resources.GetObject("btnChinhDL.Image")));
            this.btnChinhDL.ImageSize = new System.Drawing.Size(20, 20);
            this.btnChinhDL.Location = new System.Drawing.Point(11, 12);
            this.btnChinhDL.Name = "btnChinhDL";
            this.btnChinhDL.Size = new System.Drawing.Size(132, 23);
            this.btnChinhDL.TabIndex = 192;
            this.btnChinhDL.Text = "Chỉnh dữ liệu lệch";
            this.btnChinhDL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnChinhDL.Click += new System.EventHandler(this.btnChinhDL_Click);
            // 
            // btnXuatExcelNPLTon
            // 
            this.btnXuatExcelNPLTon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuatExcelNPLTon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcelNPLTon.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatExcelNPLTon.Image")));
            this.btnXuatExcelNPLTon.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXuatExcelNPLTon.Location = new System.Drawing.Point(906, 12);
            this.btnXuatExcelNPLTon.Name = "btnXuatExcelNPLTon";
            this.btnXuatExcelNPLTon.Size = new System.Drawing.Size(102, 23);
            this.btnXuatExcelNPLTon.TabIndex = 192;
            this.btnXuatExcelNPLTon.Text = "Xuất Excel";
            this.btnXuatExcelNPLTon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXuatExcelNPLTon.Click += new System.EventHandler(this.btnXuatExcelNPLTon_Click);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.AutoScrollMinSize = new System.Drawing.Size(500, 0);
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.uiGroupBox10);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1095, 59);
            this.uiGroupBox5.TabIndex = 189;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.dgListNPLTK);
            this.uiTabPage2.Controls.Add(this.uiGroupBox1);
            this.uiTabPage2.Controls.Add(this.uiGroupBox3);
            this.uiTabPage2.Key = "tpThongKeLuongTon";
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1095, 496);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thống kê lượng tồn theo tờ khai";
            // 
            // dgListNPLTK
            // 
            this.dgListNPLTK.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPLTK.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPLTK.ColumnAutoResize = true;
            dgListNPLTK_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListNPLTK_DesignTimeLayout_Reference_0.Instance")));
            dgListNPLTK_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListNPLTK_DesignTimeLayout_Reference_0});
            dgListNPLTK_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLTK_DesignTimeLayout.LayoutString");
            this.dgListNPLTK.DesignTimeLayout = dgListNPLTK_DesignTimeLayout;
            this.dgListNPLTK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPLTK.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLTK.GroupByBoxVisible = false;
            this.dgListNPLTK.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLTK.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLTK.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPLTK.Location = new System.Drawing.Point(0, 73);
            this.dgListNPLTK.Name = "dgListNPLTK";
            this.dgListNPLTK.RecordNavigator = true;
            this.dgListNPLTK.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPLTK.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLTK.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPLTK.Size = new System.Drawing.Size(1095, 379);
            this.dgListNPLTK.TabIndex = 310;
            this.dgListNPLTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListNPLTK.VisualStyleManager = this.vsmMain;
            this.dgListNPLTK.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListNPLTK_LoadingRow);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.AutoScrollMinSize = new System.Drawing.Size(500, 0);
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiButton2);
            this.uiGroupBox1.Controls.Add(this.btnLayDuLieuCungUng);
            this.uiGroupBox1.Controls.Add(this.uiButton4);
            this.uiGroupBox1.Controls.Add(this.btnChuyenTonSXXK);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 452);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1095, 44);
            this.uiGroupBox1.TabIndex = 309;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Image = ((System.Drawing.Image)(resources.GetObject("uiButton2.Image")));
            this.uiButton2.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton2.Location = new System.Drawing.Point(1018, 12);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 3;
            this.uiButton2.Text = "In";
            this.uiButton2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton2.Click += new System.EventHandler(this.inDanhSachTon_Click_1);
            // 
            // btnLayDuLieuCungUng
            // 
            this.btnLayDuLieuCungUng.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLayDuLieuCungUng.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayDuLieuCungUng.Image = ((System.Drawing.Image)(resources.GetObject("btnLayDuLieuCungUng.Image")));
            this.btnLayDuLieuCungUng.ImageSize = new System.Drawing.Size(20, 20);
            this.btnLayDuLieuCungUng.Location = new System.Drawing.Point(604, 12);
            this.btnLayDuLieuCungUng.Name = "btnLayDuLieuCungUng";
            this.btnLayDuLieuCungUng.Size = new System.Drawing.Size(154, 23);
            this.btnLayDuLieuCungUng.TabIndex = 5;
            this.btnLayDuLieuCungUng.Text = "Lấy dữ liệu cung ứng";
            this.btnLayDuLieuCungUng.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnLayDuLieuCungUng.Click += new System.EventHandler(this.btnLayDuLieuCungUng_Click);
            // 
            // uiButton4
            // 
            this.uiButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton4.Image = ((System.Drawing.Image)(resources.GetObject("uiButton4.Image")));
            this.uiButton4.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton4.Location = new System.Drawing.Point(910, 12);
            this.uiButton4.Name = "uiButton4";
            this.uiButton4.Size = new System.Drawing.Size(102, 23);
            this.uiButton4.TabIndex = 2;
            this.uiButton4.Text = "Xuất Excel";
            this.uiButton4.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.uiButton4.VisualStyleManager = this.vsmMain;
            this.uiButton4.Click += new System.EventHandler(this.uiButton4_Click);
            // 
            // btnChuyenTonSXXK
            // 
            this.btnChuyenTonSXXK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChuyenTonSXXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChuyenTonSXXK.Image = ((System.Drawing.Image)(resources.GetObject("btnChuyenTonSXXK.Image")));
            this.btnChuyenTonSXXK.ImageSize = new System.Drawing.Size(20, 20);
            this.btnChuyenTonSXXK.Location = new System.Drawing.Point(764, 12);
            this.btnChuyenTonSXXK.Name = "btnChuyenTonSXXK";
            this.btnChuyenTonSXXK.Size = new System.Drawing.Size(140, 23);
            this.btnChuyenTonSXXK.TabIndex = 4;
            this.btnChuyenTonSXXK.Text = "Chuyển tồn SXXK";
            this.btnChuyenTonSXXK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnChuyenTonSXXK.Click += new System.EventHandler(this.btnChuyenTonSXXK_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.ccTo);
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.ccFrom);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtMaNPL);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.uiButton3);
            this.uiGroupBox3.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1095, 73);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ccTo
            // 
            this.ccTo.CustomFormat = "dd/MM/yyyy";
            this.ccTo.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccTo.DropDownCalendar.Name = "";
            this.ccTo.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccTo.Location = new System.Drawing.Point(339, 39);
            this.ccTo.Name = "ccTo";
            this.ccTo.Size = new System.Drawing.Size(177, 21);
            this.ccTo.TabIndex = 12;
            this.ccTo.Value = new System.DateTime(2020, 5, 23, 0, 0, 0, 0);
            this.ccTo.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccTo.TextChanged += new System.EventHandler(this.TimToKhaiTon_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(269, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Đến ngày";
            // 
            // ccFrom
            // 
            this.ccFrom.CustomFormat = "dd/MM/yyyy";
            this.ccFrom.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccFrom.DropDownCalendar.Name = "";
            this.ccFrom.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFrom.Location = new System.Drawing.Point(81, 39);
            this.ccFrom.Name = "ccFrom";
            this.ccFrom.Size = new System.Drawing.Size(166, 21);
            this.ccFrom.TabIndex = 10;
            this.ccFrom.Value = new System.DateTime(2020, 5, 23, 0, 0, 0, 0);
            this.ccFrom.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFrom.TextChanged += new System.EventHandler(this.TimToKhaiTon_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Từ ngày";
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtMaNPL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNPL.Location = new System.Drawing.Point(339, 11);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(267, 21);
            this.txtMaNPL.TabIndex = 5;
            this.txtMaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNPL.TextChanged += new System.EventHandler(this.TimToKhaiTon_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(269, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã NPL";
            // 
            // uiButton3
            // 
            this.uiButton3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton3.Image = ((System.Drawing.Image)(resources.GetObject("uiButton3.Image")));
            this.uiButton3.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.uiButton3.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton3.Location = new System.Drawing.Point(522, 39);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.Size = new System.Drawing.Size(84, 23);
            this.uiButton3.TabIndex = 8;
            this.uiButton3.Text = "Tìm kiếm";
            this.uiButton3.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton3.WordWrap = false;
            this.uiButton3.Click += new System.EventHandler(this.TimToKhaiTon_Click);
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.DecimalDigits = 0;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.FormatString = "#####";
            this.txtSoTiepNhan.Location = new System.Drawing.Point(81, 11);
            this.txtSoTiepNhan.MaxLength = 12;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(166, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTiepNhan.Value = ((ulong)(0ul));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.TextChanged += new System.EventHandler(this.TimToKhaiTon_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Số tờ khai";
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.dgListPhanBo);
            this.uiTabPage3.Controls.Add(this.uiGroupBox7);
            this.uiTabPage3.Controls.Add(this.uiGroupBox6);
            this.uiTabPage3.Key = "tpTheoDoiPhanBo";
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1095, 496);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Theo dõi phân bổ";
            // 
            // dgListPhanBo
            // 
            this.dgListPhanBo.AlternatingColors = true;
            this.dgListPhanBo.AutomaticSort = false;
            this.dgListPhanBo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListPhanBo.ColumnAutoResize = true;
            dgListPhanBo_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListPhanBo_DesignTimeLayout_Reference_0.Instance")));
            dgListPhanBo_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListPhanBo_DesignTimeLayout_Reference_0});
            dgListPhanBo_DesignTimeLayout.LayoutString = resources.GetString("dgListPhanBo_DesignTimeLayout.LayoutString");
            this.dgListPhanBo.DesignTimeLayout = dgListPhanBo_DesignTimeLayout;
            this.dgListPhanBo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListPhanBo.GroupByBoxVisible = false;
            this.dgListPhanBo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListPhanBo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListPhanBo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListPhanBo.Location = new System.Drawing.Point(0, 94);
            this.dgListPhanBo.Name = "dgListPhanBo";
            this.dgListPhanBo.RecordNavigator = true;
            this.dgListPhanBo.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListPhanBo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListPhanBo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListPhanBo.Size = new System.Drawing.Size(1095, 358);
            this.dgListPhanBo.TabIndex = 311;
            this.dgListPhanBo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListPhanBo.VisualStyleManager = this.vsmMain;
            this.dgListPhanBo.EditingCell += new Janus.Windows.GridEX.EditingCellEventHandler(this.dgListPhanBo_EditingCell);
            this.dgListPhanBo.RecordUpdated += new System.EventHandler(this.dgListPhanBo_RecordUpdated);
            this.dgListPhanBo.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListPhanBo_LoadingRow);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.AutoScrollMinSize = new System.Drawing.Size(700, 0);
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.cmdXemBangNPLCungUng);
            this.uiGroupBox7.Controls.Add(this.btnXuatExcelNPLCungUng);
            this.uiGroupBox7.Controls.Add(this.cmdViewBangPhanBoNPL);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 452);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1095, 44);
            this.uiGroupBox7.TabIndex = 310;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // cmdXemBangNPLCungUng
            // 
            this.cmdXemBangNPLCungUng.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdXemBangNPLCungUng.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdXemBangNPLCungUng.Image = ((System.Drawing.Image)(resources.GetObject("cmdXemBangNPLCungUng.Image")));
            this.cmdXemBangNPLCungUng.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.cmdXemBangNPLCungUng.ImageSize = new System.Drawing.Size(20, 20);
            this.cmdXemBangNPLCungUng.Location = new System.Drawing.Point(641, 11);
            this.cmdXemBangNPLCungUng.Name = "cmdXemBangNPLCungUng";
            this.cmdXemBangNPLCungUng.Size = new System.Drawing.Size(221, 23);
            this.cmdXemBangNPLCungUng.TabIndex = 2;
            this.cmdXemBangNPLCungUng.Text = "Xem bảng tổng hợp NPL cung ứng";
            this.cmdXemBangNPLCungUng.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmdXemBangNPLCungUng.WordWrap = false;
            this.cmdXemBangNPLCungUng.Click += new System.EventHandler(this.cmdXemBangNPLCungUng_Click);
            // 
            // btnXuatExcelNPLCungUng
            // 
            this.btnXuatExcelNPLCungUng.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuatExcelNPLCungUng.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcelNPLCungUng.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatExcelNPLCungUng.Image")));
            this.btnXuatExcelNPLCungUng.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnXuatExcelNPLCungUng.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXuatExcelNPLCungUng.Location = new System.Drawing.Point(413, 11);
            this.btnXuatExcelNPLCungUng.Name = "btnXuatExcelNPLCungUng";
            this.btnXuatExcelNPLCungUng.Size = new System.Drawing.Size(221, 23);
            this.btnXuatExcelNPLCungUng.TabIndex = 1;
            this.btnXuatExcelNPLCungUng.Text = "Xuất Excel bảng NPL cung ứng";
            this.btnXuatExcelNPLCungUng.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXuatExcelNPLCungUng.WordWrap = false;
            this.btnXuatExcelNPLCungUng.Click += new System.EventHandler(this.btnXuatExcelNPLCungUng_Click);
            // 
            // cmdViewBangPhanBoNPL
            // 
            this.cmdViewBangPhanBoNPL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdViewBangPhanBoNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdViewBangPhanBoNPL.Image = ((System.Drawing.Image)(resources.GetObject("cmdViewBangPhanBoNPL.Image")));
            this.cmdViewBangPhanBoNPL.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.cmdViewBangPhanBoNPL.ImageSize = new System.Drawing.Size(20, 20);
            this.cmdViewBangPhanBoNPL.Location = new System.Drawing.Point(868, 11);
            this.cmdViewBangPhanBoNPL.Name = "cmdViewBangPhanBoNPL";
            this.cmdViewBangPhanBoNPL.Size = new System.Drawing.Size(221, 23);
            this.cmdViewBangPhanBoNPL.TabIndex = 3;
            this.cmdViewBangPhanBoNPL.Text = "Xem bảng tổng hợp phân bổ NPL";
            this.cmdViewBangPhanBoNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmdViewBangPhanBoNPL.WordWrap = false;
            this.cmdViewBangPhanBoNPL.Click += new System.EventHandler(this.cmdViewBangPhanBoNPL_Click);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.uiGroupBox11);
            this.uiGroupBox6.Controls.Add(this.cbbToKhai);
            this.uiGroupBox6.Controls.Add(this.label10);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.cbSanPham);
            this.uiGroupBox6.Controls.Add(this.uiButton8);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1095, 94);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // cbbToKhai
            // 
            cbbToKhai_DesignTimeLayout.LayoutString = resources.GetString("cbbToKhai_DesignTimeLayout.LayoutString");
            this.cbbToKhai.DesignTimeLayout = cbbToKhai_DesignTimeLayout;
            this.cbbToKhai.Location = new System.Drawing.Point(97, 14);
            this.cbbToKhai.Name = "cbbToKhai";
            this.cbbToKhai.SelectedIndex = -1;
            this.cbbToKhai.SelectedItem = null;
            this.cbbToKhai.Size = new System.Drawing.Size(296, 21);
            this.cbbToKhai.TabIndex = 1;
            this.cbbToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbToKhai.ValueChanged += new System.EventHandler(this.XemPhanBo_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(28, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "ID tờ khai";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(412, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sản phẩm";
            // 
            // cbSanPham
            // 
            this.cbSanPham.Location = new System.Drawing.Point(481, 14);
            this.cbSanPham.Name = "cbSanPham";
            this.cbSanPham.Size = new System.Drawing.Size(206, 21);
            this.cbSanPham.TabIndex = 3;
            this.cbSanPham.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbSanPham.SelectedValueChanged += new System.EventHandler(this.XemPhanBo_Click);
            // 
            // uiButton8
            // 
            this.uiButton8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton8.Image = ((System.Drawing.Image)(resources.GetObject("uiButton8.Image")));
            this.uiButton8.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.uiButton8.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton8.Location = new System.Drawing.Point(703, 14);
            this.uiButton8.Name = "uiButton8";
            this.uiButton8.Size = new System.Drawing.Size(97, 23);
            this.uiButton8.TabIndex = 4;
            this.uiButton8.Text = "Tìm kiếm";
            this.uiButton8.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton8.WordWrap = false;
            this.uiButton8.Click += new System.EventHandler(this.XemPhanBo_Click);
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.dgListTKChuaPhanBo);
            this.uiTabPage4.Controls.Add(this.uiGroupBox12);
            this.uiTabPage4.Controls.Add(this.uiGroupBox8);
            this.uiTabPage4.Key = "tpPhanBoToKhaiXuat";
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1095, 496);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Thực hiện phân bổ cho tờ khai xuất";
            // 
            // contextMenuStripPhanBo
            // 
            this.contextMenuStripPhanBo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemPhanBo});
            this.contextMenuStripPhanBo.Name = "contextMenuStrip2";
            this.contextMenuStripPhanBo.Size = new System.Drawing.Size(119, 26);
            // 
            // toolStripMenuItemPhanBo
            // 
            this.toolStripMenuItemPhanBo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemPhanBo.Image")));
            this.toolStripMenuItemPhanBo.Name = "toolStripMenuItemPhanBo";
            this.toolStripMenuItemPhanBo.Size = new System.Drawing.Size(118, 22);
            this.toolStripMenuItemPhanBo.Text = "Phân bổ";
            this.toolStripMenuItemPhanBo.Click += new System.EventHandler(this.toolStripMenuItemPhanBo_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.progressBarPhanBo);
            this.uiGroupBox8.Controls.Add(this.btnPhanBo);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox8.Location = new System.Drawing.Point(0, 455);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(1095, 41);
            this.uiGroupBox8.TabIndex = 311;
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // progressBarPhanBo
            // 
            this.progressBarPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarPhanBo.BackColor = System.Drawing.SystemColors.Highlight;
            this.progressBarPhanBo.Location = new System.Drawing.Point(11, 14);
            this.progressBarPhanBo.Name = "progressBarPhanBo";
            this.progressBarPhanBo.Size = new System.Drawing.Size(986, 18);
            this.progressBarPhanBo.TabIndex = 282;
            // 
            // btnPhanBo
            // 
            this.btnPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhanBo.Image = ((System.Drawing.Image)(resources.GetObject("btnPhanBo.Image")));
            this.btnPhanBo.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnPhanBo.ImageSize = new System.Drawing.Size(20, 20);
            this.btnPhanBo.Location = new System.Drawing.Point(1003, 11);
            this.btnPhanBo.Name = "btnPhanBo";
            this.btnPhanBo.Size = new System.Drawing.Size(81, 23);
            this.btnPhanBo.TabIndex = 278;
            this.btnPhanBo.Text = "Phân bổ";
            this.btnPhanBo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnPhanBo.WordWrap = false;
            this.btnPhanBo.Click += new System.EventHandler(this.PhanBo_Click);
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgToKhaiDaPhanBo);
            this.uiTabPage1.Controls.Add(this.uiGroupBox15);
            this.uiTabPage1.Controls.Add(this.uiGroupBox9);
            this.uiTabPage1.Key = "tpDanhSachToKhaiDaPhanBo";
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1095, 496);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Danh sách tờ khai đã phân bổ";
            // 
            // contextMenuStripDaPhanBo
            // 
            this.contextMenuStripDaPhanBo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemBangPhanBo,
            this.toolStripMenuItemBangNPLCungUng,
            this.xuatExcelNPLCungUngToolStripMenuItem,
            this.toolStripMenuItem1,
            this.xemBangNplCungUngTKMuaVNToolStripMenuItem});
            this.contextMenuStripDaPhanBo.Name = "contextMenuStrip1";
            this.contextMenuStripDaPhanBo.Size = new System.Drawing.Size(270, 114);
            // 
            // toolStripMenuItemBangPhanBo
            // 
            this.toolStripMenuItemBangPhanBo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemBangPhanBo.Image")));
            this.toolStripMenuItemBangPhanBo.Name = "toolStripMenuItemBangPhanBo";
            this.toolStripMenuItemBangPhanBo.Size = new System.Drawing.Size(269, 22);
            this.toolStripMenuItemBangPhanBo.Text = "Xem bảng phân bổ";
            this.toolStripMenuItemBangPhanBo.Click += new System.EventHandler(this.toolStripMenuItemBangPhanBo_Click);
            // 
            // toolStripMenuItemBangNPLCungUng
            // 
            this.toolStripMenuItemBangNPLCungUng.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemBangNPLCungUng.Image")));
            this.toolStripMenuItemBangNPLCungUng.Name = "toolStripMenuItemBangNPLCungUng";
            this.toolStripMenuItemBangNPLCungUng.Size = new System.Drawing.Size(269, 22);
            this.toolStripMenuItemBangNPLCungUng.Text = "Xem bảng npl cung ứng";
            this.toolStripMenuItemBangNPLCungUng.Click += new System.EventHandler(this.toolStripMenuItemBangNPLCungUng_Click);
            // 
            // xuatExcelNPLCungUngToolStripMenuItem
            // 
            this.xuatExcelNPLCungUngToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xuatExcelNPLCungUngToolStripMenuItem.Image")));
            this.xuatExcelNPLCungUngToolStripMenuItem.Name = "xuatExcelNPLCungUngToolStripMenuItem";
            this.xuatExcelNPLCungUngToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.xuatExcelNPLCungUngToolStripMenuItem.Text = "Xuất Excel NPL cung ứng";
            this.xuatExcelNPLCungUngToolStripMenuItem.Click += new System.EventHandler(this.xuatExcelNPLCungUngToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(269, 22);
            this.toolStripMenuItem1.Text = "Xóa phân bổ và chỉnh dữ liệu";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // xemBangNplCungUngTKMuaVNToolStripMenuItem
            // 
            this.xemBangNplCungUngTKMuaVNToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xemBangNplCungUngTKMuaVNToolStripMenuItem.Image")));
            this.xemBangNplCungUngTKMuaVNToolStripMenuItem.Name = "xemBangNplCungUngTKMuaVNToolStripMenuItem";
            this.xemBangNplCungUngTKMuaVNToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.xemBangNplCungUngTKMuaVNToolStripMenuItem.Text = "Xem bảng NPL cung ứng TK mua VN";
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.label15);
            this.uiGroupBox9.Controls.Add(this.btnDeletePhanBo);
            this.uiGroupBox9.Controls.Add(this.progressBarDaPhanBo);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox9.Location = new System.Drawing.Point(0, 421);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(1095, 75);
            this.uiGroupBox9.TabIndex = 311;
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label15.Location = new System.Drawing.Point(10, 37);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(518, 30);
            this.label15.TabIndex = 280;
            this.label15.Text = "Liệt kê danh sách tờ khai đã phân bổ theo thứ tự phân bổ.\r\nChọn tờ khai xuất để r" +
                "ồi bấm nút xóa phân bổ để xóa phân bổ từ tờ khai đó trở về sau.";
            // 
            // btnDeletePhanBo
            // 
            this.btnDeletePhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeletePhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeletePhanBo.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletePhanBo.Image")));
            this.btnDeletePhanBo.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnDeletePhanBo.Location = new System.Drawing.Point(971, 11);
            this.btnDeletePhanBo.Name = "btnDeletePhanBo";
            this.btnDeletePhanBo.Size = new System.Drawing.Size(113, 23);
            this.btnDeletePhanBo.TabIndex = 279;
            this.btnDeletePhanBo.Text = "Xóa phân bổ";
            this.btnDeletePhanBo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeletePhanBo.WordWrap = false;
            this.btnDeletePhanBo.Click += new System.EventHandler(this.btnDeletePhanBo_Click);
            // 
            // progressBarDaPhanBo
            // 
            this.progressBarDaPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarDaPhanBo.BackColor = System.Drawing.SystemColors.Highlight;
            this.progressBarDaPhanBo.Location = new System.Drawing.Point(9, 14);
            this.progressBarDaPhanBo.Name = "progressBarDaPhanBo";
            this.progressBarDaPhanBo.Size = new System.Drawing.Size(953, 18);
            this.progressBarDaPhanBo.TabIndex = 281;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.ctrDonViHaiQuan);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.ccToDate);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.ccFromDate);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(53, 5);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(686, 96);
            this.uiGroupBox4.TabIndex = 304;
            this.uiGroupBox4.Text = "Tìm kiếm thông tin tờ khai";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // ctrDonViHaiQuan
            // 
            this.ctrDonViHaiQuan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ctrDonViHaiQuan.BackColor = System.Drawing.Color.Transparent;
            this.ctrDonViHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDonViHaiQuan.Location = new System.Drawing.Point(161, 19);
            this.ctrDonViHaiQuan.Ma = "C34C";
            this.ctrDonViHaiQuan.MaCuc = "";
            this.ctrDonViHaiQuan.Name = "ctrDonViHaiQuan";
            this.ctrDonViHaiQuan.ReadOnly = true;
            this.ctrDonViHaiQuan.Size = new System.Drawing.Size(366, 39);
            this.ctrDonViHaiQuan.TabIndex = 274;
            this.ctrDonViHaiQuan.VisualStyleManager = null;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(60, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 273;
            this.label3.Text = "Mã hải quan";
            // 
            // btnSearch
            // 
            this.btnSearch.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSearch.Icon")));
            this.btnSearch.Location = new System.Drawing.Point(546, 50);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(93, 40);
            this.btnSearch.TabIndex = 272;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            // 
            // ccToDate
            // 
            this.ccToDate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccToDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccToDate.DropDownCalendar.Name = "";
            this.ccToDate.DropDownCalendar.Visible = false;
            this.ccToDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccToDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccToDate.Location = new System.Drawing.Point(392, 52);
            this.ccToDate.Name = "ccToDate";
            this.ccToDate.Nullable = true;
            this.ccToDate.NullButtonText = "Xóa";
            this.ccToDate.ShowNullButton = true;
            this.ccToDate.Size = new System.Drawing.Size(120, 21);
            this.ccToDate.TabIndex = 271;
            this.ccToDate.TodayButtonText = "Hôm nay";
            this.ccToDate.Value = new System.DateTime(2020, 5, 23, 0, 0, 0, 0);
            this.ccToDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccToDate.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(325, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 270;
            this.label7.Text = "Đến ngày";
            // 
            // ccFromDate
            // 
            this.ccFromDate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccFromDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccFromDate.DropDownCalendar.Name = "";
            this.ccFromDate.DropDownCalendar.Visible = false;
            this.ccFromDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccFromDate.Location = new System.Drawing.Point(190, 52);
            this.ccFromDate.Name = "ccFromDate";
            this.ccFromDate.Nullable = true;
            this.ccFromDate.NullButtonText = "Xóa";
            this.ccFromDate.ShowNullButton = true;
            this.ccFromDate.Size = new System.Drawing.Size(120, 21);
            this.ccFromDate.TabIndex = 269;
            this.ccFromDate.TodayButtonText = "Hôm nay";
            this.ccFromDate.Value = new System.DateTime(2008, 1, 1, 0, 0, 0, 0);
            this.ccFromDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccFromDate.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(60, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 269;
            this.label6.Text = "Chọn tờ khai từ ngày";
            // 
            // uiButton1
            // 
            this.uiButton1.Location = new System.Drawing.Point(625, 120);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(84, 40);
            this.uiButton1.TabIndex = 303;
            this.uiButton1.Text = "Chọn tờ khai";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            // 
            // btnChonTK
            // 
            this.btnChonTK.Location = new System.Drawing.Point(718, 120);
            this.btnChonTK.Name = "btnChonTK";
            this.btnChonTK.Size = new System.Drawing.Size(74, 40);
            this.btnChonTK.TabIndex = 302;
            this.btnChonTK.Text = "Thống kê";
            this.btnChonTK.VisualStyleManager = this.vsmMain;
            // 
            // dgTKX
            // 
            this.dgTKX.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgTKX.AlternatingColors = true;
            this.dgTKX.AutomaticSort = false;
            this.dgTKX.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKX.ColumnAutoResize = true;
            dgTKX_DesignTimeLayout.LayoutString = resources.GetString("dgTKX_DesignTimeLayout.LayoutString");
            this.dgTKX.DesignTimeLayout = dgTKX_DesignTimeLayout;
            this.dgTKX.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgTKX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKX.GroupByBoxVisible = false;
            this.dgTKX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKX.Location = new System.Drawing.Point(0, 172);
            this.dgTKX.Name = "dgTKX";
            this.dgTKX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKX.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKX.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKX.Size = new System.Drawing.Size(795, 165);
            this.dgTKX.TabIndex = 195;
            this.dgTKX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKX.VisualStyleManager = this.vsmMain;
            // 
            // gridEXPrintDocument1
            // 
            this.gridEXPrintDocument1.CardColumnsPerPage = 1;
            this.gridEXPrintDocument1.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintDocument1.PageHeaderCenter = "DANH SÁCH NGUYÊN PHỤ LIỆU TỒN THỰC TẾ";
            this.gridEXPrintDocument1.PageHeaderFormatStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // gridEXPrintDocumentNPL
            // 
            this.gridEXPrintDocumentNPL.CardColumnsPerPage = 1;
            this.gridEXPrintDocumentNPL.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintDocumentNPL.PageHeaderCenter = "THỐNG KÊ NGUYÊN PHỤ LIỆU TỒN";
            this.gridEXPrintDocumentNPL.PageHeaderFormatStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // gridEXExporterNPL
            // 
            this.gridEXExporterNPL.GridEX = this.dgList;
            // 
            // gridEXExporter1
            // 
            this.gridEXExporter1.GridEX = this.dgListNPLTK;
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.AutoScroll = true;
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.btnSearchNPL);
            this.uiGroupBox10.Controls.Add(this.label22);
            this.uiGroupBox10.Controls.Add(this.txtMaNguyenPhuLieu);
            this.uiGroupBox10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox10.Location = new System.Drawing.Point(3, 6);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(1089, 50);
            this.uiGroupBox10.TabIndex = 287;
            this.uiGroupBox10.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchNPL
            // 
            this.btnSearchNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchNPL.Image")));
            this.btnSearchNPL.Location = new System.Drawing.Point(378, 17);
            this.btnSearchNPL.Name = "btnSearchNPL";
            this.btnSearchNPL.Size = new System.Drawing.Size(89, 23);
            this.btnSearchNPL.TabIndex = 87;
            this.btnSearchNPL.Text = "Tìm kiếm";
            this.btnSearchNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchNPL.Click += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(22, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 13);
            this.label22.TabIndex = 86;
            this.label22.Text = "Mã NPL";
            // 
            // txtMaNguyenPhuLieu
            // 
            this.txtMaNguyenPhuLieu.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNguyenPhuLieu.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNguyenPhuLieu.BackColor = System.Drawing.Color.White;
            this.txtMaNguyenPhuLieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguyenPhuLieu.Location = new System.Drawing.Point(95, 18);
            this.txtMaNguyenPhuLieu.Name = "txtMaNguyenPhuLieu";
            this.txtMaNguyenPhuLieu.Size = new System.Drawing.Size(266, 21);
            this.txtMaNguyenPhuLieu.TabIndex = 85;
            this.txtMaNguyenPhuLieu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguyenPhuLieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNguyenPhuLieu.TextChanged += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.AutoScroll = true;
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.btnSearchMa);
            this.uiGroupBox11.Controls.Add(this.label2);
            this.uiGroupBox11.Controls.Add(this.txtMa);
            this.uiGroupBox11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox11.Location = new System.Drawing.Point(3, 41);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(1089, 50);
            this.uiGroupBox11.TabIndex = 288;
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchMa
            // 
            this.btnSearchMa.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchMa.Image")));
            this.btnSearchMa.Location = new System.Drawing.Point(378, 17);
            this.btnSearchMa.Name = "btnSearchMa";
            this.btnSearchMa.Size = new System.Drawing.Size(89, 23);
            this.btnSearchMa.TabIndex = 87;
            this.btnSearchMa.Text = "Tìm kiếm";
            this.btnSearchMa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchMa.Click += new System.EventHandler(this.btnSearchMa_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 86;
            this.label2.Text = "Mã NPL";
            // 
            // txtMa
            // 
            this.txtMa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMa.BackColor = System.Drawing.Color.White;
            this.txtMa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMa.Location = new System.Drawing.Point(95, 18);
            this.txtMa.Name = "txtMa";
            this.txtMa.Size = new System.Drawing.Size(266, 21);
            this.txtMa.TabIndex = 85;
            this.txtMa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMa.TextChanged += new System.EventHandler(this.btnSearchMa_Click);
            // 
            // uiGroupBox12
            // 
            this.uiGroupBox12.AutoScroll = true;
            this.uiGroupBox12.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox12.Controls.Add(this.uiGroupBox13);
            this.uiGroupBox12.Controls.Add(this.uiGroupBox14);
            this.uiGroupBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox12.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox12.Name = "uiGroupBox12";
            this.uiGroupBox12.Size = new System.Drawing.Size(1095, 57);
            this.uiGroupBox12.TabIndex = 313;
            this.uiGroupBox12.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchTK
            // 
            this.btnSearchTK.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTK.Image")));
            this.btnSearchTK.Location = new System.Drawing.Point(282, 14);
            this.btnSearchTK.Name = "btnSearchTK";
            this.btnSearchTK.Size = new System.Drawing.Size(89, 23);
            this.btnSearchTK.TabIndex = 87;
            this.btnSearchTK.Text = "Tìm kiếm";
            this.btnSearchTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchTK.Click += new System.EventHandler(this.btnSearchTK_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(9, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 86;
            this.label11.Text = "Số tờ khai";
            // 
            // txtSoTK
            // 
            this.txtSoTK.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoTK.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoTK.BackColor = System.Drawing.Color.White;
            this.txtSoTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTK.Location = new System.Drawing.Point(82, 15);
            this.txtSoTK.Name = "txtSoTK";
            this.txtSoTK.Size = new System.Drawing.Size(194, 21);
            this.txtSoTK.TabIndex = 85;
            this.txtSoTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTK.TextChanged += new System.EventHandler(this.btnSearchTK_Click);
            // 
            // uiGroupBox14
            // 
            this.uiGroupBox14.AutoScroll = true;
            this.uiGroupBox14.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox14.Controls.Add(this.btnSearchTK);
            this.uiGroupBox14.Controls.Add(this.txtSoTK);
            this.uiGroupBox14.Controls.Add(this.label11);
            this.uiGroupBox14.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox14.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox14.Name = "uiGroupBox14";
            this.uiGroupBox14.Size = new System.Drawing.Size(442, 46);
            this.uiGroupBox14.TabIndex = 315;
            this.uiGroupBox14.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.AutoScroll = true;
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.clcToDate);
            this.uiGroupBox13.Controls.Add(this.label17);
            this.uiGroupBox13.Controls.Add(this.clcNgayDangKy);
            this.uiGroupBox13.Controls.Add(this.btnSearchDate);
            this.uiGroupBox13.Controls.Add(this.label13);
            this.uiGroupBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox13.Location = new System.Drawing.Point(445, 8);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(647, 46);
            this.uiGroupBox13.TabIndex = 316;
            this.uiGroupBox13.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(13, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 14);
            this.label13.TabIndex = 41;
            this.label13.Text = "Từ ngày";
            // 
            // btnSearchDate
            // 
            this.btnSearchDate.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchDate.Image")));
            this.btnSearchDate.Location = new System.Drawing.Point(401, 13);
            this.btnSearchDate.Name = "btnSearchDate";
            this.btnSearchDate.Size = new System.Drawing.Size(89, 23);
            this.btnSearchDate.TabIndex = 88;
            this.btnSearchDate.Text = "Tìm kiếm";
            this.btnSearchDate.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchDate.Click += new System.EventHandler(this.btnSearchDate_Click);
            // 
            // dgListTKChuaPhanBo
            // 
            this.dgListTKChuaPhanBo.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTKChuaPhanBo.AutomaticSort = false;
            this.dgListTKChuaPhanBo.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListTKChuaPhanBo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTKChuaPhanBo.ColumnAutoResize = true;
            this.dgListTKChuaPhanBo.ContextMenuStrip = this.contextMenuStripPhanBo;
            dgListTKChuaPhanBo_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListTKChuaPhanBo_DesignTimeLayout_Reference_0.Instance")));
            dgListTKChuaPhanBo_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListTKChuaPhanBo_DesignTimeLayout_Reference_0});
            dgListTKChuaPhanBo_DesignTimeLayout.LayoutString = resources.GetString("dgListTKChuaPhanBo_DesignTimeLayout.LayoutString");
            this.dgListTKChuaPhanBo.DesignTimeLayout = dgListTKChuaPhanBo_DesignTimeLayout;
            this.dgListTKChuaPhanBo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTKChuaPhanBo.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKChuaPhanBo.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTKChuaPhanBo.GroupByBoxVisible = false;
            this.dgListTKChuaPhanBo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListTKChuaPhanBo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTKChuaPhanBo.Hierarchical = true;
            this.dgListTKChuaPhanBo.ImageList = this.ImageList1;
            this.dgListTKChuaPhanBo.Location = new System.Drawing.Point(0, 57);
            this.dgListTKChuaPhanBo.Name = "dgListTKChuaPhanBo";
            this.dgListTKChuaPhanBo.RecordNavigator = true;
            this.dgListTKChuaPhanBo.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTKChuaPhanBo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKChuaPhanBo.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKChuaPhanBo.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKChuaPhanBo.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListTKChuaPhanBo.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKChuaPhanBo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTKChuaPhanBo.Size = new System.Drawing.Size(1095, 398);
            this.dgListTKChuaPhanBo.TabIndex = 314;
            this.dgListTKChuaPhanBo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTKChuaPhanBo.VisualStyleManager = this.vsmMain;
            this.dgListTKChuaPhanBo.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListTKChuaPhanBo_LoadingRow);
            this.dgListTKChuaPhanBo.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListTKChuaPhanBo_FormattingRow);
            // 
            // uiGroupBox15
            // 
            this.uiGroupBox15.AutoScroll = true;
            this.uiGroupBox15.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox15.Controls.Add(this.uiGroupBox16);
            this.uiGroupBox15.Controls.Add(this.uiGroupBox17);
            this.uiGroupBox15.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox15.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox15.Name = "uiGroupBox15";
            this.uiGroupBox15.Size = new System.Drawing.Size(1095, 57);
            this.uiGroupBox15.TabIndex = 314;
            this.uiGroupBox15.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.AutoScroll = true;
            this.uiGroupBox16.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox16.Controls.Add(this.clcTuNgay);
            this.uiGroupBox16.Controls.Add(this.clcDenNgay);
            this.uiGroupBox16.Controls.Add(this.label12);
            this.uiGroupBox16.Controls.Add(this.label16);
            this.uiGroupBox16.Controls.Add(this.btnSearchNgayDangKy);
            this.uiGroupBox16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox16.Location = new System.Drawing.Point(445, 8);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(647, 46);
            this.uiGroupBox16.TabIndex = 316;
            this.uiGroupBox16.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchNgayDangKy
            // 
            this.btnSearchNgayDangKy.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchNgayDangKy.Image")));
            this.btnSearchNgayDangKy.Location = new System.Drawing.Point(406, 14);
            this.btnSearchNgayDangKy.Name = "btnSearchNgayDangKy";
            this.btnSearchNgayDangKy.Size = new System.Drawing.Size(89, 23);
            this.btnSearchNgayDangKy.TabIndex = 88;
            this.btnSearchNgayDangKy.Text = "Tìm kiếm";
            this.btnSearchNgayDangKy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchNgayDangKy.Click += new System.EventHandler(this.btnSearchNgayDangKy_Click);
            // 
            // uiGroupBox17
            // 
            this.uiGroupBox17.AutoScroll = true;
            this.uiGroupBox17.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox17.Controls.Add(this.btnSearchSoTKDaPB);
            this.uiGroupBox17.Controls.Add(this.txtSoTKDaPB);
            this.uiGroupBox17.Controls.Add(this.label14);
            this.uiGroupBox17.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox17.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox17.Name = "uiGroupBox17";
            this.uiGroupBox17.Size = new System.Drawing.Size(442, 46);
            this.uiGroupBox17.TabIndex = 315;
            this.uiGroupBox17.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchSoTKDaPB
            // 
            this.btnSearchSoTKDaPB.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchSoTKDaPB.Image")));
            this.btnSearchSoTKDaPB.Location = new System.Drawing.Point(282, 14);
            this.btnSearchSoTKDaPB.Name = "btnSearchSoTKDaPB";
            this.btnSearchSoTKDaPB.Size = new System.Drawing.Size(89, 23);
            this.btnSearchSoTKDaPB.TabIndex = 87;
            this.btnSearchSoTKDaPB.Text = "Tìm kiếm";
            this.btnSearchSoTKDaPB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchSoTKDaPB.Click += new System.EventHandler(this.btnSearchSoTKDaPB_Click);
            // 
            // txtSoTKDaPB
            // 
            this.txtSoTKDaPB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoTKDaPB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoTKDaPB.BackColor = System.Drawing.Color.White;
            this.txtSoTKDaPB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTKDaPB.Location = new System.Drawing.Point(82, 15);
            this.txtSoTKDaPB.Name = "txtSoTKDaPB";
            this.txtSoTKDaPB.Size = new System.Drawing.Size(194, 21);
            this.txtSoTKDaPB.TabIndex = 85;
            this.txtSoTKDaPB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTKDaPB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTKDaPB.TextChanged += new System.EventHandler(this.btnSearchSoTKDaPB_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(9, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 86;
            this.label14.Text = "Số tờ khai";
            // 
            // clcDenNgay
            // 
            this.clcDenNgay.CustomFormat = "dd/MM/yyyy";
            this.clcDenNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDenNgay.DropDownCalendar.Name = "";
            this.clcDenNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcDenNgay.Location = new System.Drawing.Point(294, 15);
            this.clcDenNgay.Name = "clcDenNgay";
            this.clcDenNgay.Size = new System.Drawing.Size(97, 21);
            this.clcDenNgay.TabIndex = 92;
            this.clcDenNgay.Value = new System.DateTime(2020, 5, 23, 0, 0, 0, 0);
            this.clcDenNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.TextChanged += new System.EventHandler(this.btnSearchNgayDangKy_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(210, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 14);
            this.label12.TabIndex = 89;
            this.label12.Text = "Đến ngày :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 14);
            this.label16.TabIndex = 90;
            this.label16.Text = "Từ ngày :";
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.CustomFormat = "dd/MM/yyyy";
            this.clcTuNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcTuNgay.DropDownCalendar.Name = "";
            this.clcTuNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcTuNgay.Location = new System.Drawing.Point(74, 15);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.Size = new System.Drawing.Size(120, 21);
            this.clcTuNgay.TabIndex = 93;
            this.clcTuNgay.Value = new System.DateTime(2020, 5, 23, 0, 0, 0, 0);
            this.clcTuNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.TextChanged += new System.EventHandler(this.btnSearchNgayDangKy_Click);
            // 
            // clcNgayDangKy
            // 
            this.clcNgayDangKy.CustomFormat = "dd/MM/yyyy";
            this.clcNgayDangKy.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayDangKy.DropDownCalendar.Name = "";
            this.clcNgayDangKy.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayDangKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayDangKy.Location = new System.Drawing.Point(81, 14);
            this.clcNgayDangKy.Name = "clcNgayDangKy";
            this.clcNgayDangKy.Size = new System.Drawing.Size(121, 21);
            this.clcNgayDangKy.TabIndex = 92;
            this.clcNgayDangKy.Value = new System.DateTime(2020, 5, 23, 0, 0, 0, 0);
            this.clcNgayDangKy.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dgToKhaiDaPhanBo
            // 
            this.dgToKhaiDaPhanBo.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgToKhaiDaPhanBo.AutomaticSort = false;
            this.dgToKhaiDaPhanBo.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgToKhaiDaPhanBo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgToKhaiDaPhanBo.ColumnAutoResize = true;
            this.dgToKhaiDaPhanBo.ContextMenuStrip = this.contextMenuStripDaPhanBo;
            dgToKhaiDaPhanBo_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgToKhaiDaPhanBo_DesignTimeLayout_Reference_0.Instance")));
            dgToKhaiDaPhanBo_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgToKhaiDaPhanBo_DesignTimeLayout_Reference_0});
            dgToKhaiDaPhanBo_DesignTimeLayout.LayoutString = resources.GetString("dgToKhaiDaPhanBo_DesignTimeLayout.LayoutString");
            this.dgToKhaiDaPhanBo.DesignTimeLayout = dgToKhaiDaPhanBo_DesignTimeLayout;
            this.dgToKhaiDaPhanBo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgToKhaiDaPhanBo.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgToKhaiDaPhanBo.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiDaPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgToKhaiDaPhanBo.GroupByBoxVisible = false;
            this.dgToKhaiDaPhanBo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiDaPhanBo.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgToKhaiDaPhanBo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiDaPhanBo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgToKhaiDaPhanBo.Hierarchical = true;
            this.dgToKhaiDaPhanBo.ImageList = this.ImageList1;
            this.dgToKhaiDaPhanBo.Location = new System.Drawing.Point(0, 57);
            this.dgToKhaiDaPhanBo.Name = "dgToKhaiDaPhanBo";
            this.dgToKhaiDaPhanBo.RecordNavigator = true;
            this.dgToKhaiDaPhanBo.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgToKhaiDaPhanBo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgToKhaiDaPhanBo.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgToKhaiDaPhanBo.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiDaPhanBo.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgToKhaiDaPhanBo.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgToKhaiDaPhanBo.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiDaPhanBo.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgToKhaiDaPhanBo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgToKhaiDaPhanBo.Size = new System.Drawing.Size(1095, 364);
            this.dgToKhaiDaPhanBo.TabIndex = 315;
            this.dgToKhaiDaPhanBo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgToKhaiDaPhanBo.VisualStyleManager = this.vsmMain;
            this.dgToKhaiDaPhanBo.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListTKChuaPhanBo_LoadingRow);
            // 
            // clcToDate
            // 
            this.clcToDate.CustomFormat = "dd/MM/yyyy";
            this.clcToDate.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcToDate.DropDownCalendar.Name = "";
            this.clcToDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcToDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcToDate.Location = new System.Drawing.Point(287, 14);
            this.clcToDate.Name = "clcToDate";
            this.clcToDate.Size = new System.Drawing.Size(108, 21);
            this.clcToDate.TabIndex = 94;
            this.clcToDate.Value = new System.DateTime(2020, 5, 23, 0, 0, 0, 0);
            this.clcToDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(214, 17);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 14);
            this.label17.TabIndex = 93;
            this.label17.Text = "Đến ngày :";
            // 
            // ThongkeHangTonForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1097, 518);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ThongkeHangTonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Quản lý phân bổ";
            this.Load += new System.EventHandler(this.ThongkeHangTonForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTabSanPham)).EndInit();
            this.uiTabSanPham.ResumeLayout(false);
            this.tabQLNPLTon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLTK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListPhanBo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToKhai)).EndInit();
            this.uiTabPage4.ResumeLayout(false);
            this.contextMenuStripPhanBo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.contextMenuStripDaPhanBo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).EndInit();
            this.uiGroupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).EndInit();
            this.uiGroupBox14.ResumeLayout(false);
            this.uiGroupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            this.uiGroupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKChuaPhanBo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).EndInit();
            this.uiGroupBox15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            this.uiGroupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).EndInit();
            this.uiGroupBox17.ResumeLayout(false);
            this.uiGroupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhaiDaPhanBo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton uicmdSelectTK;
        private Janus.Windows.EditControls.UIButton uicmdThongke;
        private Janus.Windows.UI.Tab.UITab uiTabSanPham;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.CalendarCombo.CalendarCombo ccToDate;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFromDate;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton btnChonTK;
        private Janus.Windows.GridEX.GridEX dgTKX;
        private Company.Interface.Controls.DonViHaiQuanControl ctrDonViHaiQuan;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.UI.Tab.UITabPage tabQLNPLTon;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton uiButton3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintDocument1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton uiButton8;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cbSanPham;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.EditControls.UIButton btnPhanBo;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton cmdViewBangPhanBoNPL;
        private Janus.Windows.EditControls.UIButton cmdXemBangNPLCungUng;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.EditControls.UIButton btnDeletePhanBo;
        private Janus.Windows.EditControls.UIButton uiButton4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbToKhai;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripDaPhanBo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemBangPhanBo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemBangNPLCungUng;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripPhanBo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPhanBo;
        private Janus.Windows.EditControls.UIButton btnXuatExcelNPLTon;
        private Janus.Windows.EditControls.UIButton btnInNPLTon;
        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintDocumentNPL;
        private Janus.Windows.EditControls.UIButton btnXuatExcelNPLCungUng;
        private System.Windows.Forms.ToolStripMenuItem xuatExcelNPLCungUngToolStripMenuItem;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.CalendarCombo.CalendarCombo ccTo;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFrom;
        private Janus.Windows.EditControls.UIButton btnChuyenTonSXXK;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private Janus.Windows.EditControls.UIButton btnLayDuLieuCungUng;
        private System.Windows.Forms.ProgressBar progressBarDaPhanBo;
        private System.Windows.Forms.ProgressBar progressBarPhanBo;
        private System.Windows.Forms.ToolStripMenuItem xemBangNplCungUngTKMuaVNToolStripMenuItem;
        private Janus.Windows.EditControls.UIButton btnChinhDL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX dgListNPLTK;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.GridEX.GridEX dgListPhanBo;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporterNPL;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.EditControls.UIButton btnSearchNPL;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguyenPhuLieu;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.EditControls.UIButton btnSearchMa;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMa;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox12;
        private Janus.Windows.EditControls.UIButton btnSearchTK;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTK;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox14;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.EditControls.UIButton btnSearchDate;
        private Janus.Windows.GridEX.GridEX dgListTKChuaPhanBo;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox15;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox16;
        private Janus.Windows.EditControls.UIButton btnSearchNgayDangKy;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox17;
        private Janus.Windows.EditControls.UIButton btnSearchSoTKDaPB;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTKDaPB;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDenNgay;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.CalendarCombo.CalendarCombo clcTuNgay;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayDangKy;
        private Janus.Windows.GridEX.GridEX dgToKhaiDaPhanBo;
        private Janus.Windows.CalendarCombo.CalendarCombo clcToDate;
        private System.Windows.Forms.Label label17;
    }
}