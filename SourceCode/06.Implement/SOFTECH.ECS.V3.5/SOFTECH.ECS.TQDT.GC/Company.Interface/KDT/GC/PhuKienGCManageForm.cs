﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Data;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using Company.Interface.KDT.SXXK;

namespace Company.Interface.KDT.GC
{
    public partial class PhuKienGCManageForm : BaseForm
    {
        PhuKienDangKyCollection collection = new PhuKienDangKyCollection();
        PhuKienDangKy pkDangKy = new PhuKienDangKy();
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        //-----------------------------------------------------------------------------------------
        public PhuKienGCManageForm()
        {
            InitializeComponent();
        }
        private void BindHopDong()
        {
            DataTable dt;
            {
                string where = string.Format("madoanhnghiep='{0}'", GlobalSettings.MA_DON_VI);
                HopDong HD = new HopDong();
                dt = HopDong.SelectDynamic(where, "").Tables[0];
            }
            cbHopDong.DataSource = dt;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            cbHopDong.SelectedIndex = 1;
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            BindHopDong();
        }

        //-----------------------------------------------------------------------------------------
        private void PhuKienGCManageForm_Load(object sender, EventArgs e)
        {
            //An nut Xac nhan
            XacNhanThongTin.Visible = Janus.Windows.UI.InheritableBoolean.False;
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
            this.khoitao_DuLieuChuan();
            cbStatus.SelectedIndex = 0;
            btnSearch_Click(null, null);
            //dgList.DataSource = this.dmDangKy.DMCollection;
        }
        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //e.Row.Cells[].Text = this.LoaiPhuKien_GetName(e.Row.Cells[1].Text); 
            try
            {
                string st = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                if (st == "-1")
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                else if (st == "0")
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                else if (st == "1")
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                else if (st == "10")
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                else if (st == "11")
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                else if (st == "2")
                    e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                else if (st == "-3")
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                if (e.Row.Cells["NgayTiepNhan"].Value != null)
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Value);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }
                if (e.Row.Cells["NgayTiepNhan"].Value != null)
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayPhuKien"].Value);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayPhuKien"].Text = "";
                }
                HopDong hd = new HopDong();
                hd.ID = Convert.ToInt64(e.Row.Cells["HopDong_ID"].Value);
                hd = HopDong.Load(hd.ID);
                e.Row.Cells["HopDong_ID"].Text = hd.SoHopDong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            Company.Interface.KDT.GC.PhuKienGCSendForm pk = new Company.Interface.KDT.GC.PhuKienGCSendForm();
            pk.HD.ID = Convert.ToInt64(e.Row.Cells["HopDong_ID"].Value);
            pk.HD = HopDong.Load(pk.HD.ID);
            pk.pkdk.ID = Convert.ToInt64(e.Row.Cells["ID"].Value);
            pk.pkdk.Load();
            pk.pkdk.LoadCollection();
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien loaiPK in pk.pkdk.PKCollection)
            {
                loaiPK.Load();
                loaiPK.LoadCollection();
            }
            pk.OpenType = OpenFormType.Edit;
            pk.ShowDialog();
            btnSearch_Click(null, null);
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdCSDaDuyet": 
                    ChuyenTrangThai(); 
                    break;
                case "InPhieuTN": 
                    this.inPhieuTN(); 
                    break;
                case "cmdExportExcel": 
                    this.ExportExcel(); 
                    break;
                case "cmdUpdateStatus": 
                    this.UpdateStatus(); 
                    break;
            }
        }
        private void UpdateStatus()
        {
            WSForm2 login = new WSForm2();
            login.ShowDialog(this);
            if (WSForm2.IsSuccess == true)
            {
                if (dgPhuKien.SelectedItems.Count > 0)
                {
                    PhuKienDangKyCollection pkdkColl = new PhuKienDangKyCollection();
                    foreach (GridEXSelectedItem grItem in dgPhuKien.SelectedItems)
                    {
                        pkdkColl.Add((PhuKienDangKy)grItem.GetRow().DataRow);
                    }

                    for (int i = 0; i < pkdkColl.Count; i++)
                    {
                        pkdkColl[i].LoadCollection();
                        UpdateStatusForm f = new UpdateStatusForm();
                        f.PKDK = pkdkColl[i];
                        f.formType = "PK";
                        f.ShowDialog(this);
                    }

                    this.btnSearch_Click(null, null);
                }
                else
                {
                    showMsg("MSG_240233");
                }
            }
        }
        private void ExportExcel()
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Danh sách phụ kiện " + DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sf.Filter = "Excel File | *.xls ";

            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                Janus.Windows.GridEX.Export.GridEXExporter grdExport = new Janus.Windows.GridEX.Export.GridEXExporter();
                grdExport.GridEX = dgPhuKien;

                try
                {
                    System.IO.Stream str = sf.OpenFile();
                    grdExport.Export(str);
                    str.Close();
                    if (ShowMessage("Bạn có muốn mở File này không", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            else
            {
                ShowMessage("Xuất Excel không thành công", false);
            }
        }

        private void inPhieuTN()
        {
            if (!(dgPhuKien.GetRows().Length > 0 && dgPhuKien.GetRow().RowType == RowType.Record)) return;
            GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "PHỤ KIỆN";
            Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    PhuKienDangKy pkDangKySelected = (PhuKienDangKy)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = pkDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = pkDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                    break;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();

        }
        private void ChuyenTrangThai()
        {
            if (dgPhuKien.SelectedItems.Count > 0)
            {
                PhuKienDangKyCollection pkdkColl = new PhuKienDangKyCollection();
                foreach (GridEXSelectedItem grItem in dgPhuKien.SelectedItems)
                {
                    pkdkColl.Add((PhuKienDangKy)grItem.GetRow().DataRow);
                }

                for (int i = 0; i < pkdkColl.Count; i++)
                {
                    if (pkdkColl[i].TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        pkdkColl[i].LoadCollection();

                        string msg = "Bạn có muốn chuyển trạng thái của danh sách được chọn sang đã duyệt không?";
                        msg += "\n\nSố thứ tự của danh sách: " + pkdkColl[i].ID.ToString();
                        msg += "\nCó " + pkdkColl[i].PKCollection.Count.ToString() + " phụ kiện được đăng ký";

                        string[] args = new string[2];
                        args[0] = pkdkColl[i].ID.ToString();
                        args[1] = pkdkColl[i].PKCollection.Count.ToString();
                        if (showMsg("MSG_0203072", args, true) == "Yes")
                        {
                            pkdkColl[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            pkdkColl[i].updateTrangThaiDuLieu();
                            Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty,
                                pkdkColl[i].ID, pkdkColl[i].GUIDSTR, Company.KDT.SHARE.Components.MessageTypes.PhuKien,
                                 Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                                 string.Format("Trước khi chuyển GUIDSTR={0},Trạng thái xử lý {1}", pkdkColl[i].GUIDSTR, pkdkColl[i].TrangThaiXuLy));

                        }                        
                    }
                    else
                    {
                        ShowMessageTQDT("Chỉ những Phụ kiện đã khai báo đến HQ và cấp số TN khai báo và được HQ duyệt mới được phép dùng chức năng này .", false);
                        return;
                    }
                }

                this.btnSearch_Click(null, null);
            }
            else
            {
                showMsg("MSG_240233");
            }
        }
        private bool checkExistHD(string soHD)
        {
            HopDong hopdong = new HopDong();
            List<HopDong> hdcol = new List<HopDong>();
            hdcol = HopDong.SelectCollectionAll();
            foreach (HopDong hd in hdcol)
            {
                if (soHD.Equals(hd.SoHopDong.Trim()))
                    return true;

            }
            return false;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string where = "1=1  and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' ";
                if (txtSoTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and sotiepnhan=" + txtSoTiepNhan.Text;
                }
                if (!String.IsNullOrEmpty(txtSoPhuKien.Text))
                {
                    where += " AND SoPhuKien LIKE '%" + txtSoPhuKien.Text + "%' ";
                }
                if (cbHopDong.Text.Length > 0)
                {
                    if (!checkExistHD(cbHopDong.Text.Trim().ToString()))
                    {
                        showMsg("MSG_2702066");
                        //MLMessages("Không tồn tại số hợp đồng này","MSG_WRN10","", false);
                        return;
                    }
                    where += " and HopDong_ID=" + cbHopDong.Value.ToString();


                }
                if (txtNamTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text;
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcDateFrom.Value;
                    DateTime toDate = clcDateTo.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (ngaytiepnhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                if (cbStatus.Text.Length > 0)
                    where += " and TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();
                collection = pkDangKy.SelectCollectionDynamic(where, "");
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgPhuKien.Refresh();
                dgPhuKien.DataSource = collection;
                dgPhuKien.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }
        private void dgPhuKien_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        PhuKienDangKy pkdkDelete = (PhuKienDangKy)i.GetRow().DataRow;

                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkdkDelete.ID;
                        if (sendXML.Load())
                        {
                            j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                        }
                        else
                        {
                            if (pkdkDelete.ID > 0)
                            {
                                pkdkDelete.Delete();
                            }
                        }
                    }
                }
                btnSearch_Click(null,null);
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (collection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
            int j = 0;
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {

                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        PhuKienDangKy pkdkDelete = (PhuKienDangKy)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkdkDelete.ID;
                        if (sendXML.Load())
                        {
                            j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                        }
                        else
                        {
                            if (pkdkDelete.ID > 0)
                            {
                                pkdkDelete.Delete();
                            }
                        }
                    }
                    
                }
                btnSearch_Click(null,null);
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            if (dgPhuKien.GetRow() != null)
            {
                PhuKienDangKy npl = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = npl.ID;
                form.DeclarationIssuer = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG;
                form.ShowDialog(this);
            }
        }
        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            AutoSendAndFeedbackXMLForm f = new AutoSendAndFeedbackXMLForm();
            f.Type = "PK";
            f.ShowDialog(this);
        }

       
    }
}