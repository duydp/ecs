﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class Container
    {
        //---------------------------------------------------------------------------------------------

        public int InsertUpdateBy(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_KDT_Container_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@VanDon_ID", SqlDbType.BigInt, VanDon_ID);
            db.AddInParameter(dbCommand, "@SoHieu", SqlDbType.VarChar, SoHieu);
            db.AddInParameter(dbCommand, "@LoaiContainer", SqlDbType.VarChar, LoaiContainer);
            db.AddInParameter(dbCommand, "@Seal_No", SqlDbType.VarChar, Seal_No);
            db.AddInParameter(dbCommand, "@Trang_thai", SqlDbType.Int, Trang_thai);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

    }
}
