IF OBJECT_ID('p_KDT_GC_DinhMuc_SelectDistinctDynamicLSX') IS NOT NULL
DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectDistinctDynamicLSX]
GO
IF OBJECT_ID('t_View_DinhMuc_LenhSX') IS NOT NULL
DROP VIEW [dbo].[t_View_DinhMuc_LenhSX]
GO
IF OBJECT_ID('p_KDT_GC_DinhMuc_SelectDynamicSP') IS NOT NULL
DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectDynamicSP]
GO
-----------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_SelectDynamic]    
-- Database: ECS_TQDT_GC_V4_TAT    
-----------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectDistinctDynamicLSX]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT     
 DISTINCT      
 [MaDDSX] ,  
 Master_ID  
FROM [dbo].[t_KDT_GC_DinhMuc]     
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END      
EXEC sp_executesql @SQL    
GO


GO
CREATE VIEW dbo.t_View_DinhMuc_LenhSX    
AS    
SELECT     dbo.t_GC_DinhMuc.HopDong_ID, dbo.t_GC_DinhMuc.MaSanPham, dbo.t_GC_DinhMuc.MaNguyenPhuLieu, dbo.t_GC_DinhMuc.STTHang,     
                      dbo.t_GC_DinhMuc.DinhMucSuDung + dbo.t_GC_DinhMuc.DinhMucSuDung * dbo.t_GC_DinhMuc.TyLeHaoHut / 100 AS DinhMucChung,     
                      dbo.t_GC_NguyenPhuLieu.Ten AS TenNPL, dbo.t_GC_NguyenPhuLieu.DVT_ID, dbo.t_GC_SanPham.Ten AS TenSP,     
                      dbo.t_GC_DinhMuc.DinhMucSuDung, dbo.t_GC_DinhMuc.TyLeHaoHut, dbo.t_KDT_GC_HopDong.DonViDoiTac, dbo.t_KDT_GC_HopDong.DiaChiDoiTac,     
                      dbo.t_KDT_GC_HopDong.NgayDangKy, dbo.t_KDT_GC_HopDong.NgayHetHan, dbo.t_KDT_GC_HopDong.NgayGiaHan,     
                      dbo.t_KDT_GC_HopDong.NgayKy, dbo.t_KDT_GC_HopDong.SoHopDong  , dbo.t_GC_DinhMuc.LenhSanXuat_ID ,t_KDT_LenhSanXuat.TuNgay , t_KDT_LenhSanXuat.DenNgay  
FROM         dbo.t_GC_DinhMuc INNER JOIN    
                      dbo.t_GC_NguyenPhuLieu ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID AND     
                      dbo.t_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma INNER JOIN    
                      dbo.t_GC_SanPham ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_SanPham.HopDong_ID AND     
                      dbo.t_GC_DinhMuc.MaSanPham = dbo.t_GC_SanPham.Ma INNER JOIN    
                      dbo.t_KDT_GC_HopDong ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_KDT_GC_HopDong.ID AND     
                      dbo.t_GC_NguyenPhuLieu.HopDong_ID = dbo.t_KDT_GC_HopDong.ID AND dbo.t_GC_SanPham.HopDong_ID = dbo.t_KDT_GC_HopDong.ID  
       INNER JOIN dbo.t_KDT_LenhSanXuat ON t_KDT_LenhSanXuat.ID = dbo.t_GC_DinhMuc.LenhSanXuat_ID  
      
GO    
-----------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_SelectDynamic]    
-- Database: ECS_TQDT_GC_V4_TAT    
-----------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectDynamicSP]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT     
 DISTINCT    
 [MaSanPham],    
 [TenSanPham],    
 [MaDDSX]  ,  
  Master_ID  
FROM [dbo].[t_KDT_GC_DinhMuc]    
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END      
EXEC sp_executesql @SQL 

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.4',GETDATE(), N'CẬP NHẬT PROCEDURE CẬP NHẬT LỆNH SẢN XUẤT TỪ ĐỊNH MỨC ĐÃ NHẬP TRƯỚC ĐÓ')
END