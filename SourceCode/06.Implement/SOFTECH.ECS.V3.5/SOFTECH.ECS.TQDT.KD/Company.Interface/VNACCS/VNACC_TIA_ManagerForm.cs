﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
namespace Company.Interface
{
    public partial class VNACC_TIA_ManagerForm : BaseForm
    {
        private string where = "";
        private List<KDT_VNACCS_TIA> listTIA = new List<KDT_VNACCS_TIA>();
        private DateTime dayMin = new DateTime(1900, 1, 1);
        public VNACC_TIA_ManagerForm()
        {
            InitializeComponent();

            this.Text = "Theo dõi hàng tạm nhập/ tái xuất";
        }

        private void VNACC_TIA_ManagerForm_Load(object sender, EventArgs e)
        {
            try
            {
                cbbTrangThaiXuLy.SelectedValue = "0";
                //where = "TrangThaiXuLy='-1'";
                //LoadData(where);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        //private void LoadData(string where)
        //{
        //    listTIA = KDT_VNACCS_TIA.SelectCollectionDynamic(where, "");
        //    grList.DataSource = listTIA;
        //}
        private KDT_VNACCS_TIA getTIA(long id)
        {
            try
            {
                foreach (KDT_VNACCS_TIA TIA in listTIA)
                {
                    if (TIA.ID == id)
                    {
                        string query = "Master_ID =" + id;
                        List<KDT_VNACCS_TIA_HangHoa> listHang = new List<KDT_VNACCS_TIA_HangHoa>();
                        listHang = KDT_VNACCS_TIA_HangHoa.SelectCollectionDynamic(query, "ID");
                        foreach (KDT_VNACCS_TIA_HangHoa hang in listHang)
                        {
                            TIA.HangCollection.Add(hang);
                        }
                        return TIA;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return null;
            }
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                if (id != 0)
                {
                    VNACC_TIAForm f = new VNACC_TIAForm();
                    KDT_VNACCS_TIA TIA = getTIA(id);
                    f.TIA = TIA;
                    f.ShowDialog();
                    btnTimKiem_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //string soTN = txtSoGiayPhepDauTu.Text.Trim();
                //string loaiHinh = (cbbPhanLoaiXuatNhap.SelectedValue.ToString() == "0") ? "" : cbbPhanLoaiXuatNhap.SelectedValue.ToString();
                string trangThai = (cbbTrangThaiXuLy.SelectedValue == null) ? "0" : cbbTrangThaiXuLy.SelectedValue.ToString();
                where = String.Empty;
                //if (soTN != "")
                //    where = "GP_GCNDauTuSo='" + soTN + "' and ";
                //if (loaiHinh != "")
                //    where = where + "PhanLoaiXuatNhapKhau='" + loaiHinh + "' and ";
                if (trangThai != "")
                    where = where + "TrangThaiXuLy='" + trangThai + "'";
                listTIA.Clear();
                listTIA = KDT_VNACCS_TIA.SelectCollectionDynamic(where, "ID");

                grList.DataSource = listTIA;
                grList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                this.Cursor = Cursors.Default;
            }
        }

        private void cbbTrangThaiXuLy_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách tờ khai TIA_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (ShowMessage("Bạn có muốn xuất kèm theo danh sách hàng hóa không ?", true) == "No")
                {
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = grList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
                else
                {
                    if (sfNPL.ShowDialog(this) == DialogResult.Yes || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaSoHangHoa", ColumnType.Text, EditType.NoEdit) { Caption = "Mã số hàng hóa" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoLuongBanDau", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng ban đầu" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DVTSoLuongBanDau", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoLuongDaTaiNhapTaiXuat", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng đã tái nhập tái xuất" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DVTSoLuongDaTaiNhapTaiXuat", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });

                        grList.DataSource = KDT_VNACCS_TIA.SelectDynamicFull(where, "ID").Tables[0];
                        gridEXExporter1.GridEX = grList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        grList.Tables[0].Columns.Remove("MaSoHangHoa");
                        grList.Tables[0].Columns.Remove("SoLuongBanDau");
                        grList.Tables[0].Columns.Remove("DVTSoLuongBanDau");
                        grList.Tables[0].Columns.Remove("SoLuongDaTaiNhapTaiXuat");
                        grList.Tables[0].Columns.Remove("DVTSoLuongDaTaiNhapTaiXuat");
                        grList.Refresh();
                        btnTimKiem_Click(null, null);
                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xử lý :\r\n " + ex.Message, false);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACCS_TIA> ItemColl = new List<KDT_VNACCS_TIA>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_TIA)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_TIA item in ItemColl)
                    {
                        item.HangCollection = KDT_VNACCS_TIA_HangHoa.SelectCollectionDynamic("Master_ID =" + item.ID, "ID");
                        if (item.ID > 0)
                            item.DeleteFull();
                    }
                }
                btnTimKiem_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
