﻿using System;
using System.ComponentModel;
using System.Data.OleDb;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using Janus.Windows.UI.StatusBar;
using Khendys.Controls;

namespace HaiQuan.HS
{
	public class HSMainForm :  CommonForm
	{
		private UICommand cmdThoat1;
		private UIPanelManager pmMain;
		private UIPanelGroup pnlMain;
		internal ImageList ilSmall;
		internal ImageList ilMedium;
		internal ImageList ilLarge;
		private UIStatusBar uiStatusBar1;
		private UIPanel pnlDanhMuc;
		private UIPanelInnerContainer pnlDanhMucContainer;
		private ImageList imageList1;
		private TreeView tvMucLuc;
		private UIPanel pnlDescription;
		private UIPanelInnerContainer pnlDescriptionContainer;
		private IContainer components;

		private SearchForm sForm;
		private FavouriteForm fForm;
		private ExportItemsForm eForm;
		private ExRichTextBox rtfDescription;
		private UIPanel pnlFavourite;
		private Janus.Windows.UI.Dock.UIPanel pnlExport;
		private Janus.Windows.UI.Dock.UIPanelInnerContainer pnlExportContainer;
		private UIPanelInnerContainer pnlFavouriteContainer;

		public HSMainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HSMainForm));
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel1 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            this.cmdThoat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.pmMain = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.pnlMain = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.pnlExport = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlExportContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.pnlDanhMuc = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlDanhMucContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.tvMucLuc = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pnlFavourite = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlFavouriteContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.pnlDescription = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlDescriptionContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.rtfDescription = new Khendys.Controls.ExRichTextBox();
            this.ilSmall = new System.Windows.Forms.ImageList(this.components);
            this.ilMedium = new System.Windows.Forms.ImageList(this.components);
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.uiStatusBar1 = new Janus.Windows.UI.StatusBar.UIStatusBar();
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).BeginInit();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlExport)).BeginInit();
            this.pnlExport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDanhMuc)).BeginInit();
            this.pnlDanhMuc.SuspendLayout();
            this.pnlDanhMucContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFavourite)).BeginInit();
            this.pnlFavourite.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDescription)).BeginInit();
            this.pnlDescription.SuspendLayout();
            this.pnlDescriptionContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdThoat1
            // 
            this.cmdThoat1.Key = "cmdThoat";
            this.cmdThoat1.Name = "cmdThoat1";
            // 
            // pmMain
            // 
            this.pmMain.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.pmMain.ContainerControl = this;
            this.pmMain.DefaultPanelSettings.ActiveCaptionMode = Janus.Windows.UI.Dock.ActiveCaptionMode.Never;
            this.pmMain.DefaultPanelSettings.CaptionDisplayMode = Janus.Windows.UI.Dock.PanelCaptionDisplayMode.Text;
            this.pmMain.DefaultPanelSettings.CaptionHeight = 30;
            this.pmMain.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.pmMain.DefaultPanelSettings.DarkCaptionFormatStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.pmMain.DefaultPanelSettings.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.Window;
            this.pmMain.DefaultPanelSettings.TabStateStyles.FormatStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pmMain.DefaultPanelSettings.UseActiveCaptionStyle = false;
            this.pmMain.TabbedMdi = true;
            this.pmMain.TabbedMdiSettings.TabStateStyles.SelectedFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.pmMain.Tag = null;
            this.pnlMain.Id = new System.Guid("96d876c4-e448-4823-b699-fcff62ff56b1");
            this.pnlMain.StaticGroup = true;
            this.pnlExport.Id = new System.Guid("b912ff56-4be3-4485-ae47-1a6c40bebe9a");
            this.pnlMain.Panels.Add(this.pnlExport);
            this.pnlDanhMuc.Id = new System.Guid("2b3e5f09-9a24-4b99-bf7e-8ee886f8383d");
            this.pnlMain.Panels.Add(this.pnlDanhMuc);
            this.pnlFavourite.Id = new System.Guid("99f186a3-3837-45d0-9a80-18f7529c8d98");
            this.pnlMain.Panels.Add(this.pnlFavourite);
            this.pmMain.Panels.Add(this.pnlMain);
            this.pnlDescription.Id = new System.Guid("157e998e-cb90-4f16-9374-295e1dd2aed4");
            this.pmMain.Panels.Add(this.pnlDescription);
            // 
            // Design Time Panel Info:
            // 
            this.pmMain.BeginPanelInfo();
            this.pmMain.AddDockPanelInfo(new System.Guid("96d876c4-e448-4823-b699-fcff62ff56b1"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, Janus.Windows.UI.Dock.PanelDockStyle.Left, true, new System.Drawing.Size(207, 531), true);
            this.pmMain.AddDockPanelInfo(new System.Guid("b912ff56-4be3-4485-ae47-1a6c40bebe9a"), new System.Guid("96d876c4-e448-4823-b699-fcff62ff56b1"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("2b3e5f09-9a24-4b99-bf7e-8ee886f8383d"), new System.Guid("96d876c4-e448-4823-b699-fcff62ff56b1"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("99f186a3-3837-45d0-9a80-18f7529c8d98"), new System.Guid("96d876c4-e448-4823-b699-fcff62ff56b1"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("157e998e-cb90-4f16-9374-295e1dd2aed4"), Janus.Windows.UI.Dock.PanelDockStyle.Top, new System.Drawing.Size(742, 263), true);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("96d876c4-e448-4823-b699-fcff62ff56b1"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(88, 116), new System.Drawing.Size(0, 6), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("2b3e5f09-9a24-4b99-bf7e-8ee886f8383d"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("d5e59413-5184-45bc-bbc5-9b40a268e6ec"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("52dc898d-e5c5-4c3e-964e-6134d41411e2"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("038f8df0-b141-4aac-bb44-6015ce71b26f"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("157e998e-cb90-4f16-9374-295e1dd2aed4"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("99f186a3-3837-45d0-9a80-18f7529c8d98"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("b912ff56-4be3-4485-ae47-1a6c40bebe9a"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.EndPanelInfo();
            // 
            // pnlMain
            // 
            this.pnlMain.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.pnlMain.FloatingLocation = new System.Drawing.Point(88, 116);
            this.pnlMain.FloatingSize = new System.Drawing.Size(0, 6);
            this.pnlMain.GroupStyle = Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator;
            this.pnlMain.Location = new System.Drawing.Point(3, 21);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.SelectedPanel = this.pnlDanhMuc;
            this.pnlMain.Size = new System.Drawing.Size(207, 531);
            this.pnlMain.TabIndex = 4;
            this.pnlMain.Text = "Panel 0";
            this.pnlMain.SelectedPanelChanged += new Janus.Windows.UI.Dock.PanelActionEventHandler(this.pnlMain_SelectedPanelChanged);
            // 
            // pnlExport
            // 
            this.pnlExport.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlExport.Icon")));
            this.pnlExport.InnerContainer = this.pnlExportContainer;
            this.pnlExport.Location = new System.Drawing.Point(0, 0);
            this.pnlExport.Name = "pnlExport";
            this.pnlExport.Size = new System.Drawing.Size(203, 397);
            this.pnlExport.TabIndex = 4;
            this.pnlExport.Text = "Biểu thuế xuất khẩu";
            // 
            // pnlExportContainer
            // 
            this.pnlExportContainer.Location = new System.Drawing.Point(1, 31);
            this.pnlExportContainer.Name = "pnlExportContainer";
            this.pnlExportContainer.Size = new System.Drawing.Size(201, 366);
            this.pnlExportContainer.TabIndex = 0;
            // 
            // pnlDanhMuc
            // 
            this.pnlDanhMuc.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlDanhMuc.Icon")));
            this.pnlDanhMuc.InnerContainer = this.pnlDanhMucContainer;
            this.pnlDanhMuc.Location = new System.Drawing.Point(0, 0);
            this.pnlDanhMuc.Name = "pnlDanhMuc";
            this.pnlDanhMuc.Size = new System.Drawing.Size(203, 397);
            this.pnlDanhMuc.TabIndex = 4;
            this.pnlDanhMuc.Text = "Biểu thuế NK ưu đãi";
            // 
            // pnlDanhMucContainer
            // 
            this.pnlDanhMucContainer.Controls.Add(this.tvMucLuc);
            this.pnlDanhMucContainer.Location = new System.Drawing.Point(1, 31);
            this.pnlDanhMucContainer.Name = "pnlDanhMucContainer";
            this.pnlDanhMucContainer.Size = new System.Drawing.Size(201, 366);
            this.pnlDanhMucContainer.TabIndex = 0;
            // 
            // tvMucLuc
            // 
            this.tvMucLuc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvMucLuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvMucLuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvMucLuc.ImageIndex = 0;
            this.tvMucLuc.ImageList = this.imageList1;
            this.tvMucLuc.Location = new System.Drawing.Point(0, 0);
            this.tvMucLuc.Name = "tvMucLuc";
            this.tvMucLuc.SelectedImageIndex = 1;
            this.tvMucLuc.Size = new System.Drawing.Size(201, 366);
            this.tvMucLuc.TabIndex = 2;
            this.tvMucLuc.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvMucLuc_AfterSelect);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            // 
            // pnlFavourite
            // 
            this.pnlFavourite.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlFavourite.Icon")));
            this.pnlFavourite.InnerContainer = this.pnlFavouriteContainer;
            this.pnlFavourite.Location = new System.Drawing.Point(0, 0);
            this.pnlFavourite.Name = "pnlFavourite";
            this.pnlFavourite.Size = new System.Drawing.Size(203, 397);
            this.pnlFavourite.TabIndex = 4;
            this.pnlFavourite.Text = "Mã số thường dùng";
            // 
            // pnlFavouriteContainer
            // 
            this.pnlFavouriteContainer.Location = new System.Drawing.Point(1, 31);
            this.pnlFavouriteContainer.Name = "pnlFavouriteContainer";
            this.pnlFavouriteContainer.Size = new System.Drawing.Size(201, 366);
            this.pnlFavouriteContainer.TabIndex = 0;
            // 
            // pnlDescription
            // 
            this.pnlDescription.AutoHide = true;
            this.pnlDescription.AutoHideButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.pnlDescription.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.pnlDescription.InnerContainer = this.pnlDescriptionContainer;
            this.pnlDescription.Location = new System.Drawing.Point(247, 59);
            this.pnlDescription.Name = "pnlDescription";
            this.pnlDescription.Size = new System.Drawing.Size(742, 263);
            this.pnlDescription.TabIndex = 4;
            this.pnlDescription.Text = "CHÚ GIẢI";
            // 
            // pnlDescriptionContainer
            // 
            this.pnlDescriptionContainer.Controls.Add(this.rtfDescription);
            this.pnlDescriptionContainer.Location = new System.Drawing.Point(1, 30);
            this.pnlDescriptionContainer.Name = "pnlDescriptionContainer";
            this.pnlDescriptionContainer.Size = new System.Drawing.Size(740, 228);
            this.pnlDescriptionContainer.TabIndex = 0;
            // 
            // rtfDescription
            // 
            this.rtfDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtfDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtfDescription.HiglightColor = Khendys.Controls.RtfColor.White;
            this.rtfDescription.Location = new System.Drawing.Point(0, 0);
            this.rtfDescription.Name = "rtfDescription";
            this.rtfDescription.Size = new System.Drawing.Size(740, 228);
            this.rtfDescription.TabIndex = 1;
            this.rtfDescription.Text = "";
            this.rtfDescription.TextColor = Khendys.Controls.RtfColor.Black;
            // 
            // ilSmall
            // 
            this.ilSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmall.ImageStream")));
            this.ilSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmall.Images.SetKeyName(0, "");
            this.ilSmall.Images.SetKeyName(1, "");
            this.ilSmall.Images.SetKeyName(2, "");
            this.ilSmall.Images.SetKeyName(3, "");
            this.ilSmall.Images.SetKeyName(4, "");
            this.ilSmall.Images.SetKeyName(5, "");
            this.ilSmall.Images.SetKeyName(6, "");
            this.ilSmall.Images.SetKeyName(7, "");
            this.ilSmall.Images.SetKeyName(8, "");
            this.ilSmall.Images.SetKeyName(9, "");
            this.ilSmall.Images.SetKeyName(10, "");
            this.ilSmall.Images.SetKeyName(11, "");
            this.ilSmall.Images.SetKeyName(12, "");
            this.ilSmall.Images.SetKeyName(13, "");
            this.ilSmall.Images.SetKeyName(14, "");
            this.ilSmall.Images.SetKeyName(15, "");
            this.ilSmall.Images.SetKeyName(16, "");
            this.ilSmall.Images.SetKeyName(17, "");
            this.ilSmall.Images.SetKeyName(18, "");
            this.ilSmall.Images.SetKeyName(19, "");
            this.ilSmall.Images.SetKeyName(20, "");
            this.ilSmall.Images.SetKeyName(21, "");
            this.ilSmall.Images.SetKeyName(22, "");
            this.ilSmall.Images.SetKeyName(23, "");
            this.ilSmall.Images.SetKeyName(24, "");
            this.ilSmall.Images.SetKeyName(25, "");
            this.ilSmall.Images.SetKeyName(26, "");
            this.ilSmall.Images.SetKeyName(27, "");
            this.ilSmall.Images.SetKeyName(28, "");
            this.ilSmall.Images.SetKeyName(29, "");
            this.ilSmall.Images.SetKeyName(30, "");
            this.ilSmall.Images.SetKeyName(31, "");
            this.ilSmall.Images.SetKeyName(32, "");
            this.ilSmall.Images.SetKeyName(33, "");
            this.ilSmall.Images.SetKeyName(34, "");
            // 
            // ilMedium
            // 
            this.ilMedium.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilMedium.ImageStream")));
            this.ilMedium.TransparentColor = System.Drawing.Color.Transparent;
            this.ilMedium.Images.SetKeyName(0, "");
            this.ilMedium.Images.SetKeyName(1, "");
            this.ilMedium.Images.SetKeyName(2, "");
            this.ilMedium.Images.SetKeyName(3, "");
            // 
            // ilLarge
            // 
            this.ilLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLarge.ImageStream")));
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLarge.Images.SetKeyName(0, "");
            this.ilLarge.Images.SetKeyName(1, "");
            this.ilLarge.Images.SetKeyName(2, "");
            this.ilLarge.Images.SetKeyName(3, "");
            // 
            // uiStatusBar1
            // 
            this.uiStatusBar1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiStatusBar1.Location = new System.Drawing.Point(0, 555);
            this.uiStatusBar1.Name = "uiStatusBar1";
            uiStatusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel1.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel1.Key = "statusP1";
            uiStatusBarPanel1.PanelType = Janus.Windows.UI.StatusBar.StatusBarPanelType.ControlContainer;
            uiStatusBarPanel1.ProgressBarValue = 0;
            uiStatusBarPanel1.Text = "Bản quyền thuộc công ty cổ phần Softech Đà Nẵng";
            uiStatusBarPanel1.Width = 783;
            this.uiStatusBar1.Panels.AddRange(new Janus.Windows.UI.StatusBar.UIStatusBarPanel[] {
            uiStatusBarPanel1});
            this.uiStatusBar1.PanelsBorderColor = System.Drawing.SystemColors.ControlDark;
            this.uiStatusBar1.Size = new System.Drawing.Size(804, 23);
            this.uiStatusBar1.TabIndex = 7;
            this.uiStatusBar1.PanelClick += new Janus.Windows.UI.StatusBar.StatusBarEventHandler(this.uiStatusBar1_PanelClick);
            // 
            // HSMainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(804, 578);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.uiStatusBar1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "HSMainForm";
            this.Text = "Tra cứu mã số HS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Closed += new System.EventHandler(this.MainForm_Closed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).EndInit();
            this.pnlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlExport)).EndInit();
            this.pnlExport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlDanhMuc)).EndInit();
            this.pnlDanhMuc.ResumeLayout(false);
            this.pnlDanhMucContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlFavourite)).EndInit();
            this.pnlFavourite.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlDescription)).EndInit();
            this.pnlDescription.ResumeLayout(false);
            this.pnlDescriptionContainer.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private void show_SearchForm()
		{
			Form[] forms = this.MdiChildren;
			for (int i = 0; i < forms.Length; i++)
			{
				if (forms[i].Name.ToString().Equals("SearchForm"))
				{
					forms[i].Activate();
					return;
				}
			}
			sForm = new SearchForm();
			sForm.MdiParent = this;
			sForm.Show();
		}

		private void show_FavouriteForm()
		{
			Form[] forms = this.MdiChildren;
			for (int i = 0; i < forms.Length; i++)
			{
				if (forms[i].Name.ToString().Equals("FavouriteForm"))
				{
					forms[i].Activate();
					return;
				}
			}
			fForm = new FavouriteForm();
			fForm.MdiParent = this;
			fForm.Show();
		}

		private void show_ExportItemsForm()
		{
			Form[] forms = this.MdiChildren;
			for (int i = 0; i < forms.Length; i++)
			{
				if (forms[i].Name.ToString().Equals("ExportItemsForm"))
				{
					forms[i].Activate();
					return;
				}
			}
			eForm = new ExportItemsForm();
			eForm.MdiParent = this;
			eForm.Show();
		}


		private void initData()
		{
			// Phần.
			OleDbDataReader reader = new Sections().SelectAll();
			string pID = string.Empty;
			string pName = string.Empty;
			while (reader.Read())
			{
				pID = reader["ID"].ToString();
				pName = reader["Name"].ToString();

				TreeNode pNode = new TreeNode();
				pNode.Text = "PHẦN " + pID + " - " + pName;
				tvMucLuc.Nodes.Add(pNode);

				OleDbDataReader cReader = new Chapter().SelectBySection(pID);
				while (cReader.Read())
				{
					TreeNode cNode = new TreeNode();
					cNode.ImageIndex = 2;
					cNode.SelectedImageIndex = 3;
					cNode.Text = "Chương " + cReader["ID"].ToString().PadLeft(2, '0') + ": " + cReader["Name"].ToString();
					pNode.Nodes.Add(cNode);
				}
			}
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			// Khởi tạo dữ liệu.
			this.initData();
			// 
			this.show_SearchForm();
		}

		private void tvMucLuc_AfterSelect(object sender, TreeViewEventArgs e)
		{
			this.show_SearchForm();
			if (e.Node.Parent == null)
			{
				rtfDescription.LoadFile(string.Format("Sections\\{0}.rtf", e.Node.Index + 1));
				pnlDescription.Text = e.Node.Text;
			}
			else
			{
				TreeNode pNode = e.Node.Parent;

				string chapter = e.Node.Text;
				int pG = chapter.IndexOf('g');
				chapter = chapter.Substring(pG + 2, 2);
				sForm.SearchByChapter(chapter);
				pnlDescription.Text = pNode.Text + " (Chương " + chapter + ")";
				rtfDescription.LoadFile(string.Format("Chapters\\{0}.rtf", chapter));
			}
		}

		private void MainForm_Closed(object sender, System.EventArgs e)
		{
			
		}

		private void pnlMain_SelectedPanelChanged(object sender, Janus.Windows.UI.Dock.PanelActionEventArgs e)
		{
			if (e.Panel.Name == "pnlFavourite")
			{
				this.show_FavouriteForm();
			}
			else if (e.Panel.Name == "pnlDanhMuc")
			{
				this.show_SearchForm();
			}
			else
			{
				this.show_ExportItemsForm();
			}
		}

		private void uiStatusBar1_PanelClick(object sender, Janus.Windows.UI.StatusBar.StatusBarEventArgs e)
		{
			FlashFrom ff = new FlashFrom();
			ff.timer1.Enabled = false;
			ff.ShowDialog(this);
		}
	}
}