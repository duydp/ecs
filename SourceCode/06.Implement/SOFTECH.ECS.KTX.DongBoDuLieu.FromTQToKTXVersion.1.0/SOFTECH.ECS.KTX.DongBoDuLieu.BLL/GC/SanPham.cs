using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;

namespace DongBoDuLieu.BLL.GC
{
    public class SP
    {
        public static DataSet GetDataSetSanPham(Company.GC.BLL.KDT.GC.HopDong HD,string matKhau)
        {
            
            StringBuilder querySP = new StringBuilder();
            querySP.Append("SELECT ");
            querySP.Append("SP.So_HD			AS SoHopDong, ");
            querySP.Append("SP.Ma_HQHD		AS MaHaiQuan, ");
            querySP.Append("SP.DVGC			AS MaDoanhNghiep, ");
            querySP.Append("SP.Ngay_Ky		AS NgayKy, ");
            querySP.Append("SP.P_Code			AS Ma, ");
            querySP.Append("SP.HS_Code		AS MaHS, ");
            querySP.Append("SP.Ten_SP			AS Ten, ");
            querySP.Append("SP.Ma_DVT			AS DVT_ID, ");
            querySP.Append("SP.SL_DK + SP.SL_DC			AS SoLuongDangKy, ");
            querySP.Append("SP.Nhom_SP			AS Nhom_SP ");
            querySP.Append("FROM DSPGC SP ");
            querySP.Append("WHERE SP.Ma_HQHD = '" + HD.MaHaiQuan + "' AND DVGC = '" + HD.MaDoanhNghiep + "' ");
            querySP.Append(" and So_HD='" + HD.SoHopDong + "'");
            DataSet dsSP =WebServiceDB.getDataBySQL(querySP.ToString(),HD.MaHaiQuan,"SLXNK",HD.MaDoanhNghiep,matKhau);
            return dsSP;

            //TODO: SanPham
            /*
            StringBuilder querySP = new StringBuilder();
            querySP.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            querySP.Append("SELECT ");
            querySP.Append("SP.So_HD			AS SoHopDong, ");
            querySP.Append("SP.Ma_HQHD		AS MaHaiQuan, ");
            querySP.Append("SP.DVGC			AS MaDoanhNghiep, ");
            querySP.Append("SP.Ngay_Ky		AS NgayKy, ");
            querySP.Append("SP.P_Code			AS Ma, ");
            querySP.Append("SP.HS_Code		AS MaHS, ");
            querySP.Append("SP.Ten_SP			AS Ten, ");
            querySP.Append("SP.Ma_DVT			AS DVT_ID, ");
            querySP.Append("SP.SL_DK + SP.SL_DC			AS SoLuongDangKy, ");
            querySP.Append("SP.Nhom_SP			AS Nhom_SP ");
            querySP.Append("FROM DSPGC SP ");
            querySP.Append("WHERE SP.Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' ");
            querySP.Append(" and So_HD=''" + HD.SoHopDong + "''");
            querySP.Append(" ') AS a;");
            DataSet dsSP = WebServiceDB.getDataBySQL(querySP.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsSP;
            */
        }

        public static DataSet GetSanPhamAtTMP(long Id_HopDong)
        {
            string sql = "select * from t_GC_SanPham where HopDong_ID=" + Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }

        public static DataSet GetSanPhamAtDN(long Id_HopDong)
        {
            string sql = "select * from t_GC_SanPham where HopDong_ID=" + Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }
    }
}
