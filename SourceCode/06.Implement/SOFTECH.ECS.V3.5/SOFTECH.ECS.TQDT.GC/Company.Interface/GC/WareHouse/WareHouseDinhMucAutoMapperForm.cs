﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseDinhMucAutoMapperForm : Company.Interface.BaseForm
    {
        public HopDong HD;
        public WareHouseDinhMucAutoMapperForm()
        {
            InitializeComponent();
        }

        private void WareHouseDinhMucAutoMapperForm_Load(object sender, EventArgs e)
        {
            BindData();
        }
        private void BindData()
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic(" HOPDONG_ID = " + HD.ID + " AND MASP NOT IN (SELECT MASP FROM T_KHOKETOAN_DINHMUC WHERE HOPDONG_ID = " + HD.ID + " )", "");
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                btnProcess.Enabled = false;
                btnClose.Enabled = false;
                System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private void DoWork(object obj)
        {
            try
            {
                    List<T_KHOKETOAN_SANPHAM> SPCollection = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic(" HOPDONG_ID = " + HD.ID + " AND MASP NOT IN (SELECT MASP FROM T_KHOKETOAN_DINHMUC WHERE HOPDONG_ID = " + HD.ID + " )", "");
                    List<T_KHOKETOAN_DINHMUC> DMTotalCollection = new List<T_KHOKETOAN_DINHMUC>();
                    int i = 1;
                    foreach (T_KHOKETOAN_SANPHAM SP in SPCollection)
                    {
                        //Lấy danh sách SPMap tương ứng với SP trong HĐ
                        List<T_KHOKETOAN_SANPHAM_MAP> SPMapCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic(" HOPDONG_ID = " + HD.ID + " AND MASP='" + SP.MASP + "'", "");
                        decimal TotalTLQD = 0;
                        foreach (T_KHOKETOAN_SANPHAM_MAP SPMap in SPMapCollection)
                        {
                            TotalTLQD += SPMap.TYLEQD;
                            //Lấy định mức sản phẩm trong Hợp đồng tương ứng với MÃ SP Map trong kho kế toán
                            Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                            Company.GC.BLL.GC.DinhMucCollection DMCollection = new Company.GC.BLL.GC.DinhMucCollection();
                            DMCollection = DM.SelectCollectionDynamic("HopDong_ID =" + SPMap.HOPDONG_ID + " and MaSanPham = '" + SPMap.MASPMAP + "'", "");
                            foreach (Company.GC.BLL.GC.DinhMuc item in DMCollection)
                            {
                                //Lấy danh sách NPLMap tưng ứng với NPL trong HĐ
                                List<T_KHOKETOAN_NGUYENPHULIEU_MAP> NPLCollection = new List<T_KHOKETOAN_NGUYENPHULIEU_MAP>();
                                NPLCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionDynamic("HOPDONG_ID = " + item.HopDong_ID + " AND MANPLMAP ='" + item.MaNguyenPhuLieu + "'", "ID");
                                foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP NPLMap in NPLCollection)
                                {
                                    T_KHOKETOAN_DINHMUC DMMap = new T_KHOKETOAN_DINHMUC();
                                    DMMap.MASP = SPMap.MASPMAP;
                                    DMMap.TENSP = SPMap.TENSP;
                                    DMMap.DVTSP = SPMap.DVT;
                                    DMMap.MANPL = NPLMap.MANPL;
                                    DMMap.TENNPL = NPLMap.TENNPL;
                                    DMMap.DVTNPL = NPLMap.DVT;
                                    DMMap.HOPDONG_ID = NPLMap.HOPDONG_ID;
                                    DMMap.DMSD = item.DinhMucSuDung * NPLMap.TYLEQD;
                                    DMMap.TLHH = item.TyLeHaoHut * NPLMap.TYLEQD;
                                    DMMap.DMCHUNG = DMMap.DMSD + (DMMap.DMSD * DMMap.TLHH) / 100;
                                    DMMap.GHICHU = "";
                                    DMTotalCollection.Add(DMMap);
                                }
                            }
                        }
                        SetProcessBar((i * 100 / SPCollection.Count));
                        i++;
                    }
                    foreach (T_KHOKETOAN_DINHMUC item in DMTotalCollection)
                    {
                        item.InsertUpdate();
                    }
                    if (InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            BindData();
                            btnProcess.Enabled = true; btnClose.Enabled = true;

                        }));
                    }
                    else
                    {
                        BindData();
                        btnProcess.Enabled = true; btnClose.Enabled = true;
                    }
                    SetProcessBar(0);
                    SetError("Hoàn thành");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
