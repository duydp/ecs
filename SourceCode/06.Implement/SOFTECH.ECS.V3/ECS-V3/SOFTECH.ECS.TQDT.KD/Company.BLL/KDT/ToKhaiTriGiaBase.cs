using System;
using System.Data;
using System.Data.SqlClient;
#if GC_V3
using Company.GC.BLL;
#elif KD_V3
using Company.KD.BLL.KDT;
#endif
namespace Company.KD.BLL.KDT
{
    public partial class ToKhaiTriGia : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected long _TKMD_ID;
        protected long _ToSo;
        protected string _LoaiToKhaiTriGia = "KB";
        protected DateTime _NgayXuatKhau = new DateTime(1900, 01, 01);
        protected string _CapDoThuongMai="B";
        protected bool _QuyenSuDung;
        protected bool _KhongXacDinh;
        protected bool _TraThem;
        protected bool _TienTra;
        protected bool _CoQuanHeDacBiet;
        protected bool _AnhHuongQuanHe;
        protected string _GhiChep = String.Empty;
        protected DateTime _NgayKhaiBao = new DateTime(1900, 01, 01);
        protected string _NguoiKhaiBao = String.Empty;
        protected string _ChucDanhNguoiKhaiBao = String.Empty;
        protected DateTime _NgayTruyen = new DateTime(1900, 01, 01);
        protected string _KieuQuanHe = String.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public long TKMD_ID
        {
            set { this._TKMD_ID = value; }
            get { return this._TKMD_ID; }
        }
        public long ToSo
        {
            set { this._ToSo = value; }
            get { return this._ToSo; }
        }
        public string LoaiToKhaiTriGia
        {
            set { this._LoaiToKhaiTriGia = value; }
            get { return this._LoaiToKhaiTriGia; }
        }
        public DateTime NgayXuatKhau
        {
            set { this._NgayXuatKhau = value; }
            get { return this._NgayXuatKhau; }
        }
        public string CapDoThuongMai
        {
            set { this._CapDoThuongMai = value; }
            get { return this._CapDoThuongMai; }
        }
        public bool QuyenSuDung
        {
            set { this._QuyenSuDung = value; }
            get { return this._QuyenSuDung; }
        }
        public bool KhongXacDinh
        {
            set { this._KhongXacDinh = value; }
            get { return this._KhongXacDinh; }
        }
        public bool TraThem
        {
            set { this._TraThem = value; }
            get { return this._TraThem; }
        }
        public bool TienTra
        {
            set { this._TienTra = value; }
            get { return this._TienTra; }
        }
        public bool CoQuanHeDacBiet
        {
            set { this._CoQuanHeDacBiet = value; }
            get { return this._CoQuanHeDacBiet; }
        }
        public bool AnhHuongQuanHe
        {
            set { this._AnhHuongQuanHe = value; }
            get { return this._AnhHuongQuanHe; }
        }
        public string GhiChep
        {
            set { this._GhiChep = value; }
            get { return this._GhiChep; }
        }
        public DateTime NgayKhaiBao
        {
            set { this._NgayKhaiBao = value; }
            get { return this._NgayKhaiBao; }
        }
        public string NguoiKhaiBao
        {
            set { this._NguoiKhaiBao = value; }
            get { return this._NguoiKhaiBao; }
        }
        public string ChucDanhNguoiKhaiBao
        {
            set { this._ChucDanhNguoiKhaiBao = value; }
            get { return this._ChucDanhNguoiKhaiBao; }
        }
        public DateTime NgayTruyen
        {
            set { this._NgayTruyen = value; }
            get { return this._NgayTruyen; }
        }
        public string KieuQuanHe
        {
            set { this._KieuQuanHe = value; }
            get { return this._KieuQuanHe; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) this._TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToSo"))) this._ToSo = reader.GetInt64(reader.GetOrdinal("ToSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiTriGia"))) this._LoaiToKhaiTriGia = reader.GetString(reader.GetOrdinal("LoaiToKhaiTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuatKhau"))) this._NgayXuatKhau = reader.GetDateTime(reader.GetOrdinal("NgayXuatKhau"));
                if (!reader.IsDBNull(reader.GetOrdinal("CapDoThuongMai"))) this._CapDoThuongMai = reader.GetString(reader.GetOrdinal("CapDoThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyenSuDung"))) this._QuyenSuDung = reader.GetBoolean(reader.GetOrdinal("QuyenSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("KhongXacDinh"))) this._KhongXacDinh = reader.GetBoolean(reader.GetOrdinal("KhongXacDinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("TraThem"))) this._TraThem = reader.GetBoolean(reader.GetOrdinal("TraThem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienTra"))) this._TienTra = reader.GetBoolean(reader.GetOrdinal("TienTra"));
                if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHeDacBiet"))) this._CoQuanHeDacBiet = reader.GetBoolean(reader.GetOrdinal("CoQuanHeDacBiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("AnhHuongQuanHe"))) this._AnhHuongQuanHe = reader.GetBoolean(reader.GetOrdinal("AnhHuongQuanHe"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChep"))) this._GhiChep = reader.GetString(reader.GetOrdinal("GhiChep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) this._NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKhaiBao"))) this._NguoiKhaiBao = reader.GetString(reader.GetOrdinal("NguoiKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucDanhNguoiKhaiBao"))) this._ChucDanhNguoiKhaiBao = reader.GetString(reader.GetOrdinal("ChucDanhNguoiKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTruyen"))) this._NgayTruyen = reader.GetDateTime(reader.GetOrdinal("NgayTruyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("KieuQuanHe"))) this._KieuQuanHe = reader.GetString(reader.GetOrdinal("KieuQuanHe"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiTriGiaCollection SelectCollectionBy_TKMD_ID()
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            ToKhaiTriGiaCollection collection = new ToKhaiTriGiaCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiTriGia entity = new ToKhaiTriGia();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToSo"))) entity.ToSo = reader.GetInt64(reader.GetOrdinal("ToSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiTriGia"))) entity.LoaiToKhaiTriGia = reader.GetString(reader.GetOrdinal("LoaiToKhaiTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuatKhau"))) entity.NgayXuatKhau = reader.GetDateTime(reader.GetOrdinal("NgayXuatKhau"));
                if (!reader.IsDBNull(reader.GetOrdinal("CapDoThuongMai"))) entity.CapDoThuongMai = reader.GetString(reader.GetOrdinal("CapDoThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyenSuDung"))) entity.QuyenSuDung = reader.GetBoolean(reader.GetOrdinal("QuyenSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("KhongXacDinh"))) entity.KhongXacDinh = reader.GetBoolean(reader.GetOrdinal("KhongXacDinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("TraThem"))) entity.TraThem = reader.GetBoolean(reader.GetOrdinal("TraThem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienTra"))) entity.TienTra = reader.GetBoolean(reader.GetOrdinal("TienTra"));
                if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHeDacBiet"))) entity.CoQuanHeDacBiet = reader.GetBoolean(reader.GetOrdinal("CoQuanHeDacBiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("AnhHuongQuanHe"))) entity.AnhHuongQuanHe = reader.GetBoolean(reader.GetOrdinal("AnhHuongQuanHe"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChep"))) entity.GhiChep = reader.GetString(reader.GetOrdinal("GhiChep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKhaiBao"))) entity.NguoiKhaiBao = reader.GetString(reader.GetOrdinal("NguoiKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucDanhNguoiKhaiBao"))) entity.ChucDanhNguoiKhaiBao = reader.GetString(reader.GetOrdinal("ChucDanhNguoiKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTruyen"))) entity.NgayTruyen = reader.GetDateTime(reader.GetOrdinal("NgayTruyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("KieuQuanHe"))) entity.KieuQuanHe = reader.GetString(reader.GetOrdinal("KieuQuanHe"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_TKMD_ID()
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiTriGiaCollection SelectCollectionAll()
        {
            ToKhaiTriGiaCollection collection = new ToKhaiTriGiaCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                ToKhaiTriGia entity = new ToKhaiTriGia();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToSo"))) entity.ToSo = reader.GetInt64(reader.GetOrdinal("ToSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiTriGia"))) entity.LoaiToKhaiTriGia = reader.GetString(reader.GetOrdinal("LoaiToKhaiTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuatKhau"))) entity.NgayXuatKhau = reader.GetDateTime(reader.GetOrdinal("NgayXuatKhau"));
                if (!reader.IsDBNull(reader.GetOrdinal("CapDoThuongMai"))) entity.CapDoThuongMai = reader.GetString(reader.GetOrdinal("CapDoThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyenSuDung"))) entity.QuyenSuDung = reader.GetBoolean(reader.GetOrdinal("QuyenSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("KhongXacDinh"))) entity.KhongXacDinh = reader.GetBoolean(reader.GetOrdinal("KhongXacDinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("TraThem"))) entity.TraThem = reader.GetBoolean(reader.GetOrdinal("TraThem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienTra"))) entity.TienTra = reader.GetBoolean(reader.GetOrdinal("TienTra"));
                if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHeDacBiet"))) entity.CoQuanHeDacBiet = reader.GetBoolean(reader.GetOrdinal("CoQuanHeDacBiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("AnhHuongQuanHe"))) entity.AnhHuongQuanHe = reader.GetBoolean(reader.GetOrdinal("AnhHuongQuanHe"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChep"))) entity.GhiChep = reader.GetString(reader.GetOrdinal("GhiChep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKhaiBao"))) entity.NguoiKhaiBao = reader.GetString(reader.GetOrdinal("NguoiKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucDanhNguoiKhaiBao"))) entity.ChucDanhNguoiKhaiBao = reader.GetString(reader.GetOrdinal("ChucDanhNguoiKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTruyen"))) entity.NgayTruyen = reader.GetDateTime(reader.GetOrdinal("NgayTruyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("KieuQuanHe"))) entity.KieuQuanHe = reader.GetString(reader.GetOrdinal("KieuQuanHe"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiTriGiaCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            ToKhaiTriGiaCollection collection = new ToKhaiTriGiaCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiTriGia entity = new ToKhaiTriGia();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToSo"))) entity.ToSo = reader.GetInt64(reader.GetOrdinal("ToSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiTriGia"))) entity.LoaiToKhaiTriGia = reader.GetString(reader.GetOrdinal("LoaiToKhaiTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuatKhau"))) entity.NgayXuatKhau = reader.GetDateTime(reader.GetOrdinal("NgayXuatKhau"));
                if (!reader.IsDBNull(reader.GetOrdinal("CapDoThuongMai"))) entity.CapDoThuongMai = reader.GetString(reader.GetOrdinal("CapDoThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyenSuDung"))) entity.QuyenSuDung = reader.GetBoolean(reader.GetOrdinal("QuyenSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("KhongXacDinh"))) entity.KhongXacDinh = reader.GetBoolean(reader.GetOrdinal("KhongXacDinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("TraThem"))) entity.TraThem = reader.GetBoolean(reader.GetOrdinal("TraThem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienTra"))) entity.TienTra = reader.GetBoolean(reader.GetOrdinal("TienTra"));
                if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHeDacBiet"))) entity.CoQuanHeDacBiet = reader.GetBoolean(reader.GetOrdinal("CoQuanHeDacBiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("AnhHuongQuanHe"))) entity.AnhHuongQuanHe = reader.GetBoolean(reader.GetOrdinal("AnhHuongQuanHe"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChep"))) entity.GhiChep = reader.GetString(reader.GetOrdinal("GhiChep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKhaiBao"))) entity.NguoiKhaiBao = reader.GetString(reader.GetOrdinal("NguoiKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucDanhNguoiKhaiBao"))) entity.ChucDanhNguoiKhaiBao = reader.GetString(reader.GetOrdinal("ChucDanhNguoiKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTruyen"))) entity.NgayTruyen = reader.GetDateTime(reader.GetOrdinal("NgayTruyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("KieuQuanHe"))) entity.KieuQuanHe = reader.GetString(reader.GetOrdinal("KieuQuanHe"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@ToSo", SqlDbType.BigInt, this._ToSo);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiTriGia", SqlDbType.VarChar, this._LoaiToKhaiTriGia);
            this.db.AddInParameter(dbCommand, "@NgayXuatKhau", SqlDbType.DateTime, this._NgayXuatKhau);
            this.db.AddInParameter(dbCommand, "@CapDoThuongMai", SqlDbType.Char, this._CapDoThuongMai);
            this.db.AddInParameter(dbCommand, "@QuyenSuDung", SqlDbType.Bit, this._QuyenSuDung);
            this.db.AddInParameter(dbCommand, "@KhongXacDinh", SqlDbType.Bit, this._KhongXacDinh);
            this.db.AddInParameter(dbCommand, "@TraThem", SqlDbType.Bit, this._TraThem);
            this.db.AddInParameter(dbCommand, "@TienTra", SqlDbType.Bit, this._TienTra);
            this.db.AddInParameter(dbCommand, "@CoQuanHeDacBiet", SqlDbType.Bit, this._CoQuanHeDacBiet);
            this.db.AddInParameter(dbCommand, "@AnhHuongQuanHe", SqlDbType.Bit, this._AnhHuongQuanHe);
            this.db.AddInParameter(dbCommand, "@GhiChep", SqlDbType.NVarChar, this._GhiChep);
            this.db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, this._NgayKhaiBao);
            this.db.AddInParameter(dbCommand, "@NguoiKhaiBao", SqlDbType.NVarChar, this._NguoiKhaiBao);
            this.db.AddInParameter(dbCommand, "@ChucDanhNguoiKhaiBao", SqlDbType.NVarChar, this._ChucDanhNguoiKhaiBao);
            this.db.AddInParameter(dbCommand, "@NgayTruyen", SqlDbType.DateTime, this._NgayTruyen);
            this.db.AddInParameter(dbCommand, "@KieuQuanHe", SqlDbType.VarChar, this._KieuQuanHe);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(ToKhaiTriGiaCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiTriGia item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, ToKhaiTriGiaCollection collection)
        {
            foreach (ToKhaiTriGia item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@ToSo", SqlDbType.BigInt, this._ToSo);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiTriGia", SqlDbType.VarChar, this._LoaiToKhaiTriGia);
            this.db.AddInParameter(dbCommand, "@NgayXuatKhau", SqlDbType.DateTime, this._NgayXuatKhau);
            this.db.AddInParameter(dbCommand, "@CapDoThuongMai", SqlDbType.Char, this._CapDoThuongMai);
            this.db.AddInParameter(dbCommand, "@QuyenSuDung", SqlDbType.Bit, this._QuyenSuDung);
            this.db.AddInParameter(dbCommand, "@KhongXacDinh", SqlDbType.Bit, this._KhongXacDinh);
            this.db.AddInParameter(dbCommand, "@TraThem", SqlDbType.Bit, this._TraThem);
            this.db.AddInParameter(dbCommand, "@TienTra", SqlDbType.Bit, this._TienTra);
            this.db.AddInParameter(dbCommand, "@CoQuanHeDacBiet", SqlDbType.Bit, this._CoQuanHeDacBiet);
            this.db.AddInParameter(dbCommand, "@AnhHuongQuanHe", SqlDbType.Bit, this._AnhHuongQuanHe);
            this.db.AddInParameter(dbCommand, "@GhiChep", SqlDbType.VarChar, this._GhiChep);
            this.db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, this._NgayKhaiBao);
            this.db.AddInParameter(dbCommand, "@NguoiKhaiBao", SqlDbType.VarChar, this._NguoiKhaiBao);
            this.db.AddInParameter(dbCommand, "@ChucDanhNguoiKhaiBao", SqlDbType.VarChar, this._ChucDanhNguoiKhaiBao);
            this.db.AddInParameter(dbCommand, "@NgayTruyen", SqlDbType.DateTime, this._NgayTruyen);
            this.db.AddInParameter(dbCommand, "@KieuQuanHe", SqlDbType.VarChar, this._KieuQuanHe);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(ToKhaiTriGiaCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiTriGia item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@ToSo", SqlDbType.BigInt, this._ToSo);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiTriGia", SqlDbType.VarChar, this._LoaiToKhaiTriGia);
            this.db.AddInParameter(dbCommand, "@NgayXuatKhau", SqlDbType.DateTime, this._NgayXuatKhau);
            this.db.AddInParameter(dbCommand, "@CapDoThuongMai", SqlDbType.Char, this._CapDoThuongMai);
            this.db.AddInParameter(dbCommand, "@QuyenSuDung", SqlDbType.Bit, this._QuyenSuDung);
            this.db.AddInParameter(dbCommand, "@KhongXacDinh", SqlDbType.Bit, this._KhongXacDinh);
            this.db.AddInParameter(dbCommand, "@TraThem", SqlDbType.Bit, this._TraThem);
            this.db.AddInParameter(dbCommand, "@TienTra", SqlDbType.Bit, this._TienTra);
            this.db.AddInParameter(dbCommand, "@CoQuanHeDacBiet", SqlDbType.Bit, this._CoQuanHeDacBiet);
            this.db.AddInParameter(dbCommand, "@AnhHuongQuanHe", SqlDbType.Bit, this._AnhHuongQuanHe);
            this.db.AddInParameter(dbCommand, "@GhiChep", SqlDbType.NVarChar, this._GhiChep);
            this.db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, this._NgayKhaiBao);
            this.db.AddInParameter(dbCommand, "@NguoiKhaiBao", SqlDbType.NVarChar, this._NguoiKhaiBao);
            this.db.AddInParameter(dbCommand, "@ChucDanhNguoiKhaiBao", SqlDbType.NVarChar, this._ChucDanhNguoiKhaiBao);
            this.db.AddInParameter(dbCommand, "@NgayTruyen", SqlDbType.DateTime, this._NgayTruyen);
            this.db.AddInParameter(dbCommand, "@KieuQuanHe", SqlDbType.VarChar, this._KieuQuanHe);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(ToKhaiTriGiaCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiTriGia item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(ToKhaiTriGiaCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiTriGia item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(ToKhaiTriGiaCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiTriGia item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int DeleteBy_TKMD_ID()
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_DeleteBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            return this.db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteBy_TKMD_ID(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_ToKhaiTriGia_DeleteBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        #endregion
    }
}