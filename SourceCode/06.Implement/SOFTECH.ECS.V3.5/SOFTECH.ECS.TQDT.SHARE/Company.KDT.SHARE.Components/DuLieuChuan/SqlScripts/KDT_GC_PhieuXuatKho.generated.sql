-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuXuatKho_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Insert]
	@TKMD_ID bigint,
	@NgayXuatKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@Style nvarchar(50),
	@PO nvarchar(50),
	@DonViNhan nvarchar(200),
	@XuatTaiKho nvarchar(200),
	@SoLuong numeric(18, 2),
	@DVTSoLuong varchar(15),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_PhieuXuatKho]
(
	[TKMD_ID],
	[NgayXuatKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[Style],
	[PO],
	[DonViNhan],
	[XuatTaiKho],
	[SoLuong],
	[DVTSoLuong]
)
VALUES 
(
	@TKMD_ID,
	@NgayXuatKho,
	@SoPhieu,
	@SoHopDong,
	@KhachHang,
	@Style,
	@PO,
	@DonViNhan,
	@XuatTaiKho,
	@SoLuong,
	@DVTSoLuong
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayXuatKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@Style nvarchar(50),
	@PO nvarchar(50),
	@DonViNhan nvarchar(200),
	@XuatTaiKho nvarchar(200),
	@SoLuong numeric(18, 2),
	@DVTSoLuong varchar(15)
AS

UPDATE
	[dbo].[t_KDT_GC_PhieuXuatKho]
SET
	[TKMD_ID] = @TKMD_ID,
	[NgayXuatKho] = @NgayXuatKho,
	[SoPhieu] = @SoPhieu,
	[SoHopDong] = @SoHopDong,
	[KhachHang] = @KhachHang,
	[Style] = @Style,
	[PO] = @PO,
	[DonViNhan] = @DonViNhan,
	[XuatTaiKho] = @XuatTaiKho,
	[SoLuong] = @SoLuong,
	[DVTSoLuong] = @DVTSoLuong
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayXuatKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@Style nvarchar(50),
	@PO nvarchar(50),
	@DonViNhan nvarchar(200),
	@XuatTaiKho nvarchar(200),
	@SoLuong numeric(18, 2),
	@DVTSoLuong varchar(15)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_PhieuXuatKho] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_PhieuXuatKho] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[NgayXuatKho] = @NgayXuatKho,
			[SoPhieu] = @SoPhieu,
			[SoHopDong] = @SoHopDong,
			[KhachHang] = @KhachHang,
			[Style] = @Style,
			[PO] = @PO,
			[DonViNhan] = @DonViNhan,
			[XuatTaiKho] = @XuatTaiKho,
			[SoLuong] = @SoLuong,
			[DVTSoLuong] = @DVTSoLuong
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_PhieuXuatKho]
		(
			[TKMD_ID],
			[NgayXuatKho],
			[SoPhieu],
			[SoHopDong],
			[KhachHang],
			[Style],
			[PO],
			[DonViNhan],
			[XuatTaiKho],
			[SoLuong],
			[DVTSoLuong]
		)
		VALUES 
		(
			@TKMD_ID,
			@NgayXuatKho,
			@SoPhieu,
			@SoHopDong,
			@KhachHang,
			@Style,
			@PO,
			@DonViNhan,
			@XuatTaiKho,
			@SoLuong,
			@DVTSoLuong
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_PhieuXuatKho]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_PhieuXuatKho] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayXuatKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[Style],
	[PO],
	[DonViNhan],
	[XuatTaiKho],
	[SoLuong],
	[DVTSoLuong]
FROM
	[dbo].[t_KDT_GC_PhieuXuatKho]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[NgayXuatKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[Style],
	[PO],
	[DonViNhan],
	[XuatTaiKho],
	[SoLuong],
	[DVTSoLuong]
FROM [dbo].[t_KDT_GC_PhieuXuatKho] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayXuatKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[Style],
	[PO],
	[DonViNhan],
	[XuatTaiKho],
	[SoLuong],
	[DVTSoLuong]
FROM
	[dbo].[t_KDT_GC_PhieuXuatKho]	

GO

