﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Management;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using Company.BLL.Utils;
using Company.KDT.SHARE.Components.Utils;

namespace Company.Interface
{
    internal static class Program
    {
        public static bool isActivated = false;
        //public static License lic = null;
        public static bool isExistLicense = true;
        [STAThread]
        private static void Main()
        {
            try
            {                
                try
                {
                    //lic = new License();
                    //lic.EcsSys = "ECS.TQDT.SXXK";
                    //loadLicense();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + "\n Chương trình có lỗi. Vui lòng cài đặt lại.");
                    Application.Exit();
                }
                //isActivated = checkRegistered();
                //isActivated = true;
                DevExpress.UserSkins.OfficeSkins.Register();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                // CultureInfo culture = new CultureInfo("vi-VN");
                // Thread.CurrentThread.CurrentCulture = culture;
                //Thread.CurrentThread.CurrentUICulture = culture;
                if (GlobalSettings.NgayHeThong)
                {
                    if (GlobalSettings.DiaPhuong.Trim() == "")
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieudeNgay", "................." + ", ngày " + DateTime.Now.Day.ToString("00") + " tháng " + DateTime.Now.Month.ToString("00") + " năm " + DateTime.Now.Year.ToString("0000"));
                    else
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieudeNgay", GlobalSettings.DiaPhuong + ", ngày " + DateTime.Now.Day.ToString("00") + " tháng " + DateTime.Now.Month.ToString("00") + " năm " + DateTime.Now.Year.ToString("0000"));
                }

                Company.KDT.SHARE.Components.Globals.AupdateConfigXMLToExe("SXXK");

                GlobalSettings.RefreshKey();

                //TODO: VNACCS--------------------------------------
                Company.KDT.SHARE.VNACCS.Controls.ucBase.InitialData(false);

                CultureInfo culture = null;
                if (GlobalSettings.NGON_NGU == "0")
                    culture = new CultureInfo("vi-VN");
                else
                    culture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;
                DevExpress.UserSkins.BonusSkins.Register();

                DevExpress.LookAndFeel.DefaultLookAndFeel defaultSkin = new DevExpress.LookAndFeel.DefaultLookAndFeel();

                defaultSkin.LookAndFeel.SetSkinStyle("Office 2007 Blue");


                Application.Run(new MainForm()/*new FrmQuanLyHopDongXuatKhau()*/);

            }
            catch (Exception ex) 
            {                
                Logger.LocalLogger.Instance().WriteMessage(ex);      
            }
            
        }

        //-----------------------------------------------------------------------------------------------
//         #region Activate Software
//         private static void loadLicense()
//         {
// //             try
// //             {
// //                 Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
// //                 int numInConf = int.Parse(lic.DecryptString(config.AppSettings.Settings["DateTr"].Value));
// //                 if (!lic.checkExistsLicense())
// //                 {
// //                     lic.generTrial();
// //                     lic.Load();
// //                     if (numInConf < int.Parse(lic.dateTrial))
// //                     {
// //                         lic.dateTrial = numInConf.ToString();
// //                         lic.Save();
// //                     }
// //                 }
// //                 else
// //                 {
// //                     lic.Load();
// //                     //if (numInConf > int.Parse(lic.dateTrial))
// //                     //{                        
// //                     //    config.AppSettings.Settings.Remove("DateTr");
// //                     //    config.AppSettings.Settings.Add("DateTr", lic.EncryptString(lic.dateTrial));
// //                     //    config.Save(ConfigurationSaveMode.Full);
// //                     //    ConfigurationManager.RefreshSection("appSettings");
// //                     //}
// //                 }
// //                 isActivated = !string.IsNullOrEmpty(lic.codeActivate);
// //             }
// //             catch (Exception e)
// //             {
// //                 throw e;
// //             }
//         }
// 
//         public static string getProcessorID()
//         {
//             string CPUID = "BoardID:";
//             ManagementObjectSearcher srch = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
//             foreach (ManagementObject obj in srch.Get())
//             {
//                 CPUID = obj.Properties["SerialNumber"].Value.ToString();
//             }
//             return License.md5String(CPUID);
//         }
// 
//         public static string getCodeToActivate()
//         {
//             return License.md5String(getProcessorID() + "SOFTECH.ECS.SXXK");
//         }
// 
//         private static bool checkRegistered()
//         {
//             bool value = false;
//             Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//             if (lic.codeActivate == getCodeToActivate())
//             {
//                 CultureInfo vn = new CultureInfo("vi-VN");
//                 if (DateTime.Parse(lic.dayExpires, vn.DateTimeFormat).Date > DateTime.Now.Date)
//                 {
//                     value = true;
//                 }
//             }
//             return value;
//         }
// 
//         #endregion
        //-----------------------------------------------------------------------------------------------

    }
}