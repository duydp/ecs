﻿using System;
using Company.KD.BLL;
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;

namespace Company.Interface.SXXK
{
    public partial class NguyenPhuLieuChangeForm : BaseForm
    {
        public Company.KD.BLL.SXXK.HangHoaNhap HangNhap;
        //public Company.KD.BLL.KDT.List<HangMauDich> hangmdcoll;
        private string tempMa = "";
        private string tempTen = "";
        public NguyenPhuLieuChangeForm()
        {
            InitializeComponent();
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {          
           this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            // Kiểm tra tính hợp lệ của mã HS.
          

            if (!cvError.IsValid) return;
                  
            Company.KD.BLL.SXXK.HangHoaNhap nplSXXK = new Company.KD.BLL.SXXK.HangHoaNhap();
            nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            nplSXXK.MaHaiQuan = "";
            nplSXXK.Ma = txtMa.Text.Trim();

            if (nplSXXK.Load() && nplSXXK.Ma.Trim().ToUpper()!=HangNhap.Ma.Trim().ToUpper())
            {
                MLMessages("Hàng nhập này đã được đăng ký.","MSG_STN09","", false);
                return;
            }
          
            nplSXXK.Ten = txtTen.Text.Trim();
            nplSXXK.MaHS = txtMaHS.Text;
            nplSXXK.DVT_ID = cbDonViTinh.SelectedValue.ToString();

           
            try
            {
                if (HangNhap != null)
                    nplSXXK.InsertUpdateFull(HangNhap.Ma);
                else
                    nplSXXK.InsertUpdateFull("");
                MLMessages("Cập nhật thành công","MSG_SAV02","", false);
            }
            catch { ShowMessage("Cập nhật không thành công", false); }

            this.Close();
        }
      
        private void NguyenPhuLieuEditForm_Load(object sender, EventArgs e)
        {

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            if (HangNhap != null)
            {
                //
                tempMa = HangNhap.Ma;
                tempTen = HangNhap.Ten;
                //
                txtMa.Text = HangNhap.Ma;
                txtMaHS.Text = HangNhap.MaHS;
                txtTen.Text = HangNhap.Ten;
                cbDonViTinh.SelectedValue = HangNhap.DVT_ID;
            }
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
             
        }

        private void txtMaHS_TextChanged(object sender, EventArgs e)
        {

        }
    }
}