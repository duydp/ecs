﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class T_KDT_THUPHI_TOKHAI : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaHaiQuan { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string DiemThuPhi { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string TenDoanhNghiep { set; get; }
		public string SoTKNP { set; get; }
		public DateTime NgayTKNP { set; get; }
		public string HinhThucTT { set; get; }
		public string SoTK { set; get; }
		public DateTime NgayTK { set; get; }
		public string MaLoaiHinh { set; get; }
		public string MaHQ { set; get; }
		public string NhomLoaiHinh { set; get; }
		public int MaPTVC { set; get; }
		public string MaDiaDiemLuuKho { set; get; }
		public string GhiChu { set; get; }
		public string GuidStr { set; get; }
		public int LoaiHangHoa { set; get; }
        public List<T_KDT_THUPHI_TOKHAI_DSCONTAINER> ContainerCollection = new List<T_KDT_THUPHI_TOKHAI_DSCONTAINER>();
        public List<T_KDT_THUPHI_TOKHAI_FILE> FileCollection = new List<T_KDT_THUPHI_TOKHAI_FILE>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KDT_THUPHI_TOKHAI> ConvertToCollection(IDataReader reader)
		{
			List<T_KDT_THUPHI_TOKHAI> collection = new List<T_KDT_THUPHI_TOKHAI>();
			while (reader.Read())
			{
				T_KDT_THUPHI_TOKHAI entity = new T_KDT_THUPHI_TOKHAI();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiemThuPhi"))) entity.DiemThuPhi = reader.GetString(reader.GetOrdinal("DiemThuPhi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKNP"))) entity.SoTKNP = reader.GetString(reader.GetOrdinal("SoTKNP"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTKNP"))) entity.NgayTKNP = reader.GetDateTime(reader.GetOrdinal("NgayTKNP"));
				if (!reader.IsDBNull(reader.GetOrdinal("HinhThucTT"))) entity.HinhThucTT = reader.GetString(reader.GetOrdinal("HinhThucTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTK"))) entity.SoTK = reader.GetString(reader.GetOrdinal("SoTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTK"))) entity.NgayTK = reader.GetDateTime(reader.GetOrdinal("NgayTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQ"))) entity.MaHQ = reader.GetString(reader.GetOrdinal("MaHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhomLoaiHinh"))) entity.NhomLoaiHinh = reader.GetString(reader.GetOrdinal("NhomLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPTVC"))) entity.MaPTVC = reader.GetInt32(reader.GetOrdinal("MaPTVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemLuuKho"))) entity.MaDiaDiemLuuKho = reader.GetString(reader.GetOrdinal("MaDiaDiemLuuKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetInt32(reader.GetOrdinal("LoaiHangHoa"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KDT_THUPHI_TOKHAI> collection, long id)
        {
            foreach (T_KDT_THUPHI_TOKHAI item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KDT_THUPHI_TOKHAI VALUES(@MaHaiQuan, @SoTiepNhan, @NgayTiepNhan, @TrangThaiXuLy, @DiemThuPhi, @MaDoanhNghiep, @TenDoanhNghiep, @SoTKNP, @NgayTKNP, @HinhThucTT, @SoTK, @NgayTK, @MaLoaiHinh, @MaHQ, @NhomLoaiHinh, @MaPTVC, @MaDiaDiemLuuKho, @GhiChu, @GuidStr, @LoaiHangHoa)";
            string update = "UPDATE T_KDT_THUPHI_TOKHAI SET MaHaiQuan = @MaHaiQuan, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, DiemThuPhi = @DiemThuPhi, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, SoTKNP = @SoTKNP, NgayTKNP = @NgayTKNP, HinhThucTT = @HinhThucTT, SoTK = @SoTK, NgayTK = @NgayTK, MaLoaiHinh = @MaLoaiHinh, MaHQ = @MaHQ, NhomLoaiHinh = @NhomLoaiHinh, MaPTVC = @MaPTVC, MaDiaDiemLuuKho = @MaDiaDiemLuuKho, GhiChu = @GhiChu, GuidStr = @GuidStr, LoaiHangHoa = @LoaiHangHoa WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_TOKHAI WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.NVarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiemThuPhi", SqlDbType.NVarChar, "DiemThuPhi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKNP", SqlDbType.NVarChar, "SoTKNP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTKNP", SqlDbType.DateTime, "NgayTKNP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HinhThucTT", SqlDbType.NVarChar, "HinhThucTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTK", SqlDbType.NVarChar, "SoTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTK", SqlDbType.DateTime, "NgayTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, "NhomLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPTVC", SqlDbType.Int, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemLuuKho", SqlDbType.NVarChar, "MaDiaDiemLuuKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangHoa", SqlDbType.Int, "LoaiHangHoa", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.NVarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiemThuPhi", SqlDbType.NVarChar, "DiemThuPhi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKNP", SqlDbType.NVarChar, "SoTKNP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTKNP", SqlDbType.DateTime, "NgayTKNP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HinhThucTT", SqlDbType.NVarChar, "HinhThucTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTK", SqlDbType.NVarChar, "SoTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTK", SqlDbType.DateTime, "NgayTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, "NhomLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPTVC", SqlDbType.Int, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemLuuKho", SqlDbType.NVarChar, "MaDiaDiemLuuKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangHoa", SqlDbType.Int, "LoaiHangHoa", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KDT_THUPHI_TOKHAI VALUES(@MaHaiQuan, @SoTiepNhan, @NgayTiepNhan, @TrangThaiXuLy, @DiemThuPhi, @MaDoanhNghiep, @TenDoanhNghiep, @SoTKNP, @NgayTKNP, @HinhThucTT, @SoTK, @NgayTK, @MaLoaiHinh, @MaHQ, @NhomLoaiHinh, @MaPTVC, @MaDiaDiemLuuKho, @GhiChu, @GuidStr, @LoaiHangHoa)";
            string update = "UPDATE T_KDT_THUPHI_TOKHAI SET MaHaiQuan = @MaHaiQuan, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, DiemThuPhi = @DiemThuPhi, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, SoTKNP = @SoTKNP, NgayTKNP = @NgayTKNP, HinhThucTT = @HinhThucTT, SoTK = @SoTK, NgayTK = @NgayTK, MaLoaiHinh = @MaLoaiHinh, MaHQ = @MaHQ, NhomLoaiHinh = @NhomLoaiHinh, MaPTVC = @MaPTVC, MaDiaDiemLuuKho = @MaDiaDiemLuuKho, GhiChu = @GhiChu, GuidStr = @GuidStr, LoaiHangHoa = @LoaiHangHoa WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_TOKHAI WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.NVarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiemThuPhi", SqlDbType.NVarChar, "DiemThuPhi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKNP", SqlDbType.NVarChar, "SoTKNP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTKNP", SqlDbType.DateTime, "NgayTKNP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HinhThucTT", SqlDbType.NVarChar, "HinhThucTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTK", SqlDbType.NVarChar, "SoTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTK", SqlDbType.DateTime, "NgayTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, "NhomLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPTVC", SqlDbType.Int, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemLuuKho", SqlDbType.NVarChar, "MaDiaDiemLuuKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangHoa", SqlDbType.Int, "LoaiHangHoa", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.NVarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiemThuPhi", SqlDbType.NVarChar, "DiemThuPhi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKNP", SqlDbType.NVarChar, "SoTKNP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTKNP", SqlDbType.DateTime, "NgayTKNP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HinhThucTT", SqlDbType.NVarChar, "HinhThucTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTK", SqlDbType.NVarChar, "SoTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTK", SqlDbType.DateTime, "NgayTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, "NhomLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPTVC", SqlDbType.Int, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemLuuKho", SqlDbType.NVarChar, "MaDiaDiemLuuKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangHoa", SqlDbType.Int, "LoaiHangHoa", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KDT_THUPHI_TOKHAI Load(long id)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_TOKHAI_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KDT_THUPHI_TOKHAI> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KDT_THUPHI_TOKHAI> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KDT_THUPHI_TOKHAI> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_TOKHAI_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_TOKHAI_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_TOKHAI_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_TOKHAI_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KDT_THUPHI_TOKHAI(string maHaiQuan, long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string diemThuPhi, string maDoanhNghiep, string tenDoanhNghiep, string soTKNP, DateTime ngayTKNP, string hinhThucTT, string soTK, DateTime ngayTK, string maLoaiHinh, string maHQ, string nhomLoaiHinh, int maPTVC, string maDiaDiemLuuKho, string ghiChu, string guidStr, int loaiHangHoa)
		{
			T_KDT_THUPHI_TOKHAI entity = new T_KDT_THUPHI_TOKHAI();	
			entity.MaHaiQuan = maHaiQuan;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.DiemThuPhi = diemThuPhi;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.SoTKNP = soTKNP;
			entity.NgayTKNP = ngayTKNP;
			entity.HinhThucTT = hinhThucTT;
			entity.SoTK = soTK;
			entity.NgayTK = ngayTK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHQ = maHQ;
			entity.NhomLoaiHinh = nhomLoaiHinh;
			entity.MaPTVC = maPTVC;
			entity.MaDiaDiemLuuKho = maDiaDiemLuuKho;
			entity.GhiChu = ghiChu;
			entity.GuidStr = guidStr;
			entity.LoaiHangHoa = loaiHangHoa;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KDT_THUPHI_TOKHAI_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@DiemThuPhi", SqlDbType.NVarChar, DiemThuPhi);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@SoTKNP", SqlDbType.NVarChar, SoTKNP);
			db.AddInParameter(dbCommand, "@NgayTKNP", SqlDbType.DateTime, NgayTKNP.Year <= 1753 ? DBNull.Value : (object) NgayTKNP);
			db.AddInParameter(dbCommand, "@HinhThucTT", SqlDbType.NVarChar, HinhThucTT);
			db.AddInParameter(dbCommand, "@SoTK", SqlDbType.NVarChar, SoTK);
			db.AddInParameter(dbCommand, "@NgayTK", SqlDbType.DateTime, NgayTK.Year <= 1753 ? DBNull.Value : (object) NgayTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, NhomLoaiHinh);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.Int, MaPTVC);
			db.AddInParameter(dbCommand, "@MaDiaDiemLuuKho", SqlDbType.NVarChar, MaDiaDiemLuuKho);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Int, LoaiHangHoa);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KDT_THUPHI_TOKHAI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_TOKHAI item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KDT_THUPHI_TOKHAI(long id, string maHaiQuan, long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string diemThuPhi, string maDoanhNghiep, string tenDoanhNghiep, string soTKNP, DateTime ngayTKNP, string hinhThucTT, string soTK, DateTime ngayTK, string maLoaiHinh, string maHQ, string nhomLoaiHinh, int maPTVC, string maDiaDiemLuuKho, string ghiChu, string guidStr, int loaiHangHoa)
		{
			T_KDT_THUPHI_TOKHAI entity = new T_KDT_THUPHI_TOKHAI();			
			entity.ID = id;
			entity.MaHaiQuan = maHaiQuan;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.DiemThuPhi = diemThuPhi;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.SoTKNP = soTKNP;
			entity.NgayTKNP = ngayTKNP;
			entity.HinhThucTT = hinhThucTT;
			entity.SoTK = soTK;
			entity.NgayTK = ngayTK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHQ = maHQ;
			entity.NhomLoaiHinh = nhomLoaiHinh;
			entity.MaPTVC = maPTVC;
			entity.MaDiaDiemLuuKho = maDiaDiemLuuKho;
			entity.GhiChu = ghiChu;
			entity.GuidStr = guidStr;
			entity.LoaiHangHoa = loaiHangHoa;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KDT_THUPHI_TOKHAI_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@DiemThuPhi", SqlDbType.NVarChar, DiemThuPhi);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@SoTKNP", SqlDbType.NVarChar, SoTKNP);
			db.AddInParameter(dbCommand, "@NgayTKNP", SqlDbType.DateTime, NgayTKNP.Year <= 1753 ? DBNull.Value : (object) NgayTKNP);
			db.AddInParameter(dbCommand, "@HinhThucTT", SqlDbType.NVarChar, HinhThucTT);
			db.AddInParameter(dbCommand, "@SoTK", SqlDbType.NVarChar, SoTK);
			db.AddInParameter(dbCommand, "@NgayTK", SqlDbType.DateTime, NgayTK.Year <= 1753 ? DBNull.Value : (object) NgayTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, NhomLoaiHinh);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.Int, MaPTVC);
			db.AddInParameter(dbCommand, "@MaDiaDiemLuuKho", SqlDbType.NVarChar, MaDiaDiemLuuKho);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Int, LoaiHangHoa);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KDT_THUPHI_TOKHAI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_TOKHAI item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KDT_THUPHI_TOKHAI(long id, string maHaiQuan, long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string diemThuPhi, string maDoanhNghiep, string tenDoanhNghiep, string soTKNP, DateTime ngayTKNP, string hinhThucTT, string soTK, DateTime ngayTK, string maLoaiHinh, string maHQ, string nhomLoaiHinh, int maPTVC, string maDiaDiemLuuKho, string ghiChu, string guidStr, int loaiHangHoa)
		{
			T_KDT_THUPHI_TOKHAI entity = new T_KDT_THUPHI_TOKHAI();			
			entity.ID = id;
			entity.MaHaiQuan = maHaiQuan;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.DiemThuPhi = diemThuPhi;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.SoTKNP = soTKNP;
			entity.NgayTKNP = ngayTKNP;
			entity.HinhThucTT = hinhThucTT;
			entity.SoTK = soTK;
			entity.NgayTK = ngayTK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHQ = maHQ;
			entity.NhomLoaiHinh = nhomLoaiHinh;
			entity.MaPTVC = maPTVC;
			entity.MaDiaDiemLuuKho = maDiaDiemLuuKho;
			entity.GhiChu = ghiChu;
			entity.GuidStr = guidStr;
			entity.LoaiHangHoa = loaiHangHoa;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_TOKHAI_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@DiemThuPhi", SqlDbType.NVarChar, DiemThuPhi);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@SoTKNP", SqlDbType.NVarChar, SoTKNP);
			db.AddInParameter(dbCommand, "@NgayTKNP", SqlDbType.DateTime, NgayTKNP.Year <= 1753 ? DBNull.Value : (object) NgayTKNP);
			db.AddInParameter(dbCommand, "@HinhThucTT", SqlDbType.NVarChar, HinhThucTT);
			db.AddInParameter(dbCommand, "@SoTK", SqlDbType.NVarChar, SoTK);
			db.AddInParameter(dbCommand, "@NgayTK", SqlDbType.DateTime, NgayTK.Year <= 1753 ? DBNull.Value : (object) NgayTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, NhomLoaiHinh);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.Int, MaPTVC);
			db.AddInParameter(dbCommand, "@MaDiaDiemLuuKho", SqlDbType.NVarChar, MaDiaDiemLuuKho);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Int, LoaiHangHoa);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KDT_THUPHI_TOKHAI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_TOKHAI item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KDT_THUPHI_TOKHAI(long id)
		{
			T_KDT_THUPHI_TOKHAI entity = new T_KDT_THUPHI_TOKHAI();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_TOKHAI_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_TOKHAI_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KDT_THUPHI_TOKHAI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_TOKHAI item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.TrangThaiXuLy = -1;
                        this.ID = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }

                    foreach (T_KDT_THUPHI_TOKHAI_DSCONTAINER item in ContainerCollection)
                    {
                        item.TK_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    foreach (T_KDT_THUPHI_TOKHAI_FILE item in FileCollection)
                    {
                        item.TK_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
	}	
}