﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.GC.BLL.GC;

namespace Company.GC.BLL.KDT.GC
{
    public partial class DinhMuc
    {

        public ThuTucHQTruocDo ThuTucHQTruocDo { get; set; }
        public DonViTinhQuyDoi DVT_QuyDoi { get; set; }

        public static string GetMaDDSX(string MaSanPham, long Master_ID)
        {
            string spName = "SELECT TOP 1 MaDDSX FROM dbo.t_KDT_GC_DinhMuc WHERE MaSanPham = '" + MaSanPham + "' AND Master_ID =" + Master_ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return String.Empty;
            else
                return (String)obj;
        }
        public DataSet SelectDinhMuc()
        {
            string sql = " SELECT  "
                            + " * "
                            + " FROM t_View_DinhMuc ";
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(cmd);

        }
        public DataSet SelectDinhMuc(string whereCondition)
        {
            string sql = " SELECT  "
                            + " * "
                            + " FROM t_View_DinhMuc " + whereCondition;
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(cmd);

        }
        public DataSet SelectDinhMuc1()
        {
            string sql = " SELECT  "
                            + " row_number() over ( order by MaSanPham ) as STT, "
                            + " DVT_ID , "
                            + " MaSanPham, "
                            + " MaNguyenPhuLieu, "
                            + " DinhMucSuDung, "
                            + " TyLeHaoHut, "
                            + " NPL_TuCungUng, "
                            + " TenSanPham ,"
                            + " TenNPL "
                            + " FROM t_GC_DinhMuc";
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(cmd);

        }
        //---------------------

        public DinhMucCollection copyDinhMuc(long idHopDong, Company.GC.BLL.GC.SanPhamCollection spCollection)
        {
            string where = "";
            where += string.Format(" HopDong_ID = {0} ", idHopDong);

            Company.GC.BLL.GC.DinhMucCollection dmGiaCongCollection = new Company.GC.BLL.GC.DinhMucCollection();
            Company.GC.BLL.GC.DinhMuc dmGiaCong = new Company.GC.BLL.GC.DinhMuc();
            dmGiaCong.HopDong_ID = idHopDong;
            DinhMucCollection collection = new DinhMucCollection();

            string ma_sp = "(";
            foreach (Company.GC.BLL.GC.SanPham p in spCollection)
            {
                ma_sp += "'" + p.Ma + "',";
            }
            ma_sp += "'')";
            where += string.Format(" AND MaSanPham IN {0} ", ma_sp);
            dmGiaCongCollection = dmGiaCong.SelectCollectionDynamic(where, "");
            foreach (Company.GC.BLL.GC.DinhMuc dinhmuc in dmGiaCongCollection)
            {
                DinhMuc dm = new DinhMuc();
                dm.MaSanPham = dinhmuc.MaSanPham;
                dm.MaNguyenPhuLieu = dinhmuc.MaNguyenPhuLieu;
                dm.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                dm.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                dm.NPL_TuCungUng = dinhmuc.NPL_TuCungUng;
                collection.Add(dm);
            }
            return collection;
        }

        public DataSet getDinhMucSanPham(string MaSanPham, long ID)
        {
            string sql = " SELECT  "
                            + " * "
                            + " FROM t_KDT_GC_DinhMuc where  MaSanPham=@MaSanPham and Master_ID =@Master_ID"; ;
            DbCommand cmd = db.GetSqlStringCommand(sql);
            this.db.AddInParameter(cmd, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            this.db.AddInParameter(cmd, "@Master_ID", SqlDbType.BigInt, ID);
            return db.ExecuteDataSet(cmd);

        }

        public void Delete(long ID_HopDong)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {

                    GC_DinhMuc dmGC = new GC_DinhMuc();
                    dmGC.MaNguyenPhuLieu = this.MaNguyenPhuLieu;
                    dmGC.MaSanPham = this.MaSanPham;
                    dmGC.HopDong_ID = ID_HopDong;
                    dmGC.LenhSanXuat_ID = String.IsNullOrEmpty(this.MaDDSX) ? 0 : KDT_LenhSanXuat.GetID(this.MaDDSX.ToString(),this.HopDong_ID);
                    dmGC.Delete(transaction);

                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void Delete()
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        // duydp 20/10/2015 Xóa định mức đã đăng ký 
        public int DeleteByMasterID(long Master_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string SQL = "DELETE  FROM dbo.t_KDT_GC_DinhMuc WHERE Master_ID = @Master_ID ";
                    Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
                    DbCommand dbcommand = db1.GetSqlStringCommand(SQL);
                    db1.AddInParameter(dbcommand, "@Master_ID", DbType.Int32, Master_ID);
                    return db1.ExecuteNonQuery(dbcommand, transaction);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_DinhMuc_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this._DinhMucSuDung);
            this.db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this._TyLeHaoHut);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, this._NPL_TuCungUng);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

    }
}