﻿namespace Company.Interface.Report
{
    partial class CauHinhInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CauHinhInForm));
            this.label1 = new System.Windows.Forms.Label();
            this.txtDinhMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDonGiaNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this._lblDonGiaNT = new System.Windows.Forms.Label();
            this._lblTriGiaNT = new System.Windows.Forms.Label();
            this.txtSoLuongHMD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this._lblSoLuongHMD = new System.Windows.Forms.Label();
            this.txtTrongLuongTK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLuongSP = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLuongNPL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtFontTenHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtfontHuongDan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtfontToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this._lblFontToKhai = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.numTimeDelay = new System.Windows.Forms.NumericUpDown();
            this.chkKhongDungBKContainer = new Janus.Windows.EditControls.UICheckBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.chkInCV3742 = new Janus.Windows.EditControls.UICheckBox();
            this.ccbNgayHeThong = new Janus.Windows.EditControls.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeDelay)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.ccbNgayHeThong);
            this.grbMain.Controls.Add(this.chkInCV3742);
            this.grbMain.Controls.Add(this.chkKhongDungBKContainer);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Size = new System.Drawing.Size(397, 495);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(16, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Định mức";
            // 
            // txtDinhMuc
            // 
            this.txtDinhMuc.Location = new System.Drawing.Point(107, 18);
            this.txtDinhMuc.Name = "txtDinhMuc";
            this.txtDinhMuc.Size = new System.Drawing.Size(109, 21);
            this.txtDinhMuc.TabIndex = 1;
            this.txtDinhMuc.Text = "0";
            this.txtDinhMuc.Value = 0;
            this.txtDinhMuc.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtDinhMuc.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(16, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lượng NPL         ";
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(99, 463);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "&Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtDonGiaNT);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaNT);
            this.uiGroupBox1.Controls.Add(this._lblDonGiaNT);
            this.uiGroupBox1.Controls.Add(this._lblTriGiaNT);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongHMD);
            this.uiGroupBox1.Controls.Add(this._lblSoLuongHMD);
            this.uiGroupBox1.Controls.Add(this.txtTrongLuongTK);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtDinhMuc);
            this.uiGroupBox1.Controls.Add(this.txtLuongSP);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.txtLuongNPL);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(397, 219);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Số thập phân";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(228, 135);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "0 - 5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(228, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "0 - 4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(228, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "0 - 6";
            // 
            // txtDonGiaNT
            // 
            this.txtDonGiaNT.Location = new System.Drawing.Point(107, 185);
            this.txtDonGiaNT.Name = "txtDonGiaNT";
            this.txtDonGiaNT.Size = new System.Drawing.Size(109, 21);
            this.txtDonGiaNT.TabIndex = 6;
            this.txtDonGiaNT.Text = "4";
            this.txtDonGiaNT.Value = 4;
            this.txtDonGiaNT.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            // 
            // txtTriGiaNT
            // 
            this.txtTriGiaNT.Location = new System.Drawing.Point(107, 157);
            this.txtTriGiaNT.Name = "txtTriGiaNT";
            this.txtTriGiaNT.Size = new System.Drawing.Size(109, 21);
            this.txtTriGiaNT.TabIndex = 6;
            this.txtTriGiaNT.Text = "4";
            this.txtTriGiaNT.Value = 4;
            this.txtTriGiaNT.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtTriGiaNT.VisualStyleManager = this.vsmMain;
            // 
            // _lblDonGiaNT
            // 
            this._lblDonGiaNT.AutoSize = true;
            this._lblDonGiaNT.BackColor = System.Drawing.Color.Transparent;
            this._lblDonGiaNT.Location = new System.Drawing.Point(18, 190);
            this._lblDonGiaNT.Name = "_lblDonGiaNT";
            this._lblDonGiaNT.Size = new System.Drawing.Size(60, 13);
            this._lblDonGiaNT.TabIndex = 11;
            this._lblDonGiaNT.Text = "Đơn giá NT";
            // 
            // _lblTriGiaNT
            // 
            this._lblTriGiaNT.AutoSize = true;
            this._lblTriGiaNT.BackColor = System.Drawing.Color.Transparent;
            this._lblTriGiaNT.Location = new System.Drawing.Point(18, 162);
            this._lblTriGiaNT.Name = "_lblTriGiaNT";
            this._lblTriGiaNT.Size = new System.Drawing.Size(52, 13);
            this._lblTriGiaNT.TabIndex = 11;
            this._lblTriGiaNT.Text = "Trị giá NT";
            // 
            // txtSoLuongHMD
            // 
            this.txtSoLuongHMD.Location = new System.Drawing.Point(107, 130);
            this.txtSoLuongHMD.Name = "txtSoLuongHMD";
            this.txtSoLuongHMD.Size = new System.Drawing.Size(109, 21);
            this.txtSoLuongHMD.TabIndex = 5;
            this.txtSoLuongHMD.Text = "0";
            this.txtSoLuongHMD.Value = 0;
            this.txtSoLuongHMD.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoLuongHMD.VisualStyleManager = this.vsmMain;
            // 
            // _lblSoLuongHMD
            // 
            this._lblSoLuongHMD.AutoSize = true;
            this._lblSoLuongHMD.BackColor = System.Drawing.Color.Transparent;
            this._lblSoLuongHMD.Location = new System.Drawing.Point(18, 135);
            this._lblSoLuongHMD.Name = "_lblSoLuongHMD";
            this._lblSoLuongHMD.Size = new System.Drawing.Size(74, 13);
            this._lblSoLuongHMD.TabIndex = 11;
            this._lblSoLuongHMD.Text = "Số lượng HMD";
            // 
            // txtTrongLuongTK
            // 
            this.txtTrongLuongTK.Location = new System.Drawing.Point(107, 100);
            this.txtTrongLuongTK.Name = "txtTrongLuongTK";
            this.txtTrongLuongTK.Size = new System.Drawing.Size(109, 21);
            this.txtTrongLuongTK.TabIndex = 4;
            this.txtTrongLuongTK.Text = "0";
            this.txtTrongLuongTK.Value = 0;
            this.txtTrongLuongTK.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtTrongLuongTK.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(16, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Trọng lượng TK";
            // 
            // txtLuongSP
            // 
            this.txtLuongSP.Location = new System.Drawing.Point(107, 72);
            this.txtLuongSP.Name = "txtLuongSP";
            this.txtLuongSP.Size = new System.Drawing.Size(109, 21);
            this.txtLuongSP.TabIndex = 3;
            this.txtLuongSP.Text = "0";
            this.txtLuongSP.Value = 0;
            this.txtLuongSP.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtLuongSP.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(16, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Lượng SP";
            // 
            // txtLuongNPL
            // 
            this.txtLuongNPL.Location = new System.Drawing.Point(107, 45);
            this.txtLuongNPL.Name = "txtLuongNPL";
            this.txtLuongNPL.Size = new System.Drawing.Size(109, 21);
            this.txtLuongNPL.TabIndex = 2;
            this.txtLuongNPL.Text = "0";
            this.txtLuongNPL.Value = 0;
            this.txtLuongNPL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtLuongNPL.VisualStyleManager = this.vsmMain;
            // 
            // txtFontTenHang
            // 
            this.txtFontTenHang.Location = new System.Drawing.Point(107, 13);
            this.txtFontTenHang.Name = "txtFontTenHang";
            this.txtFontTenHang.Size = new System.Drawing.Size(109, 21);
            this.txtFontTenHang.TabIndex = 0;
            this.txtFontTenHang.Text = "0";
            this.txtFontTenHang.Value = 0;
            this.txtFontTenHang.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtFontTenHang.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(19, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Font Tên hàng";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtfontHuongDan);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.txtfontToKhai);
            this.uiGroupBox2.Controls.Add(this._lblFontToKhai);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtFontTenHang);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 219);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(397, 94);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Font";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtfontHuongDan
            // 
            this.txtfontHuongDan.Location = new System.Drawing.Point(107, 67);
            this.txtfontHuongDan.Name = "txtfontHuongDan";
            this.txtfontHuongDan.Size = new System.Drawing.Size(109, 21);
            this.txtfontHuongDan.TabIndex = 0;
            this.txtfontHuongDan.Text = "0";
            this.txtfontHuongDan.Value = 0;
            this.txtfontHuongDan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(18, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Font Hướng Dẫn";
            // 
            // txtfontToKhai
            // 
            this.txtfontToKhai.Location = new System.Drawing.Point(107, 40);
            this.txtfontToKhai.Name = "txtfontToKhai";
            this.txtfontToKhai.Size = new System.Drawing.Size(109, 21);
            this.txtfontToKhai.TabIndex = 0;
            this.txtfontToKhai.Text = "0";
            this.txtfontToKhai.Value = 0;
            this.txtfontToKhai.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtfontToKhai.VisualStyleManager = this.vsmMain;
            // 
            // _lblFontToKhai
            // 
            this._lblFontToKhai.AutoSize = true;
            this._lblFontToKhai.BackColor = System.Drawing.Color.Transparent;
            this._lblFontToKhai.Location = new System.Drawing.Point(18, 45);
            this._lblFontToKhai.Name = "_lblFontToKhai";
            this._lblFontToKhai.Size = new System.Drawing.Size(66, 13);
            this._lblFontToKhai.TabIndex = 13;
            this._lblFontToKhai.Text = "Font Tờ khai";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label35);
            this.uiGroupBox3.Controls.Add(this.label34);
            this.uiGroupBox3.Controls.Add(this.numTimeDelay);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 313);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(397, 50);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(222, 29);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 13);
            this.label35.TabIndex = 41;
            this.label35.Text = "(giây)";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(4, 13);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(100, 32);
            this.label34.TabIndex = 40;
            this.label34.Text = "Thời gian chờ gửi và nhận thông tin";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numTimeDelay
            // 
            this.numTimeDelay.Location = new System.Drawing.Point(107, 21);
            this.numTimeDelay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numTimeDelay.Name = "numTimeDelay";
            this.numTimeDelay.Size = new System.Drawing.Size(109, 21);
            this.numTimeDelay.TabIndex = 0;
            this.numTimeDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chkKhongDungBKContainer
            // 
            this.chkKhongDungBKContainer.BackColor = System.Drawing.Color.Transparent;
            this.chkKhongDungBKContainer.ForeColor = System.Drawing.Color.Blue;
            this.chkKhongDungBKContainer.Location = new System.Drawing.Point(12, 412);
            this.chkKhongDungBKContainer.Name = "chkKhongDungBKContainer";
            this.chkKhongDungBKContainer.Size = new System.Drawing.Size(337, 23);
            this.chkKhongDungBKContainer.TabIndex = 42;
            this.chkKhongDungBKContainer.Text = "    Không dùng bản kê container (in container theo phụ lục hàng)";
            this.chkKhongDungBKContainer.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(190, 463);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(88, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chkInCV3742
            // 
            this.chkInCV3742.BackColor = System.Drawing.Color.Transparent;
            this.chkInCV3742.ForeColor = System.Drawing.Color.Blue;
            this.chkInCV3742.Location = new System.Drawing.Point(12, 434);
            this.chkInCV3742.Name = "chkInCV3742";
            this.chkInCV3742.Size = new System.Drawing.Size(337, 23);
            this.chkInCV3742.TabIndex = 43;
            this.chkInCV3742.Text = "    Dùng phụ lục tờ khai theo CV 3742";
            this.chkInCV3742.VisualStyleManager = this.vsmMain;
            // 
            // ccbNgayHeThong
            // 
            this.ccbNgayHeThong.BackColor = System.Drawing.Color.Transparent;
            this.ccbNgayHeThong.ForeColor = System.Drawing.Color.Blue;
            this.ccbNgayHeThong.Location = new System.Drawing.Point(12, 392);
            this.ccbNgayHeThong.Name = "ccbNgayHeThong";
            this.ccbNgayHeThong.Size = new System.Drawing.Size(337, 23);
            this.ccbNgayHeThong.TabIndex = 43;
            this.ccbNgayHeThong.Text = "    Dùng ngày hệ thống";
            // 
            // CauHinhInForm
            // 
            this.ClientSize = new System.Drawing.Size(397, 495);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "CauHinhInForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cấu hình hệ thống";
            this.Load += new System.EventHandler(this.CauHinhInForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeDelay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDinhMuc;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongSP;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongNPL;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuongTK;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongHMD;
        private System.Windows.Forms.Label _lblSoLuongHMD;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtFontTenHang;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.NumericUpDown numTimeDelay;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaNT;
        private System.Windows.Forms.Label _lblTriGiaNT;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaNT;
        private System.Windows.Forms.Label _lblDonGiaNT;
        private System.Windows.Forms.Timer timer1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtfontToKhai;
        private System.Windows.Forms.Label _lblFontToKhai;
        private Janus.Windows.EditControls.UICheckBox chkKhongDungBKContainer;
        private Janus.Windows.EditControls.UICheckBox chkInCV3742;
        private Janus.Windows.EditControls.UICheckBox ccbNgayHeThong;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtfontHuongDan;
        private System.Windows.Forms.Label label9;
    }
}
