﻿using System;
using System.Drawing;

#if SXXK_V3 || SXXK_V4
using Company.Interface.SXXK;
#elif GC_V3
using Company.Interface.GC;
#elif KD_V3
using Company.Interface;
#endif
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace Company.Interface
{
    public partial class InfoOnline : BaseForm
    {
        public InfoOnline()
        {
            InitializeComponent();
        }
        public void SetText(string text)
        {
            rtfInfo.Text = text;
        }
        public void SetRTF(string sfmtRTF)
        {
            rtfInfo.Rtf = sfmtRTF;
        }
    }
}
