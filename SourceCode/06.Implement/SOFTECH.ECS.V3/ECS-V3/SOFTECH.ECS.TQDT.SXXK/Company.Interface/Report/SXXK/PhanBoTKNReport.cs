﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.BLL.SXXK.ThanhKhoan;

namespace Company.Interface.Report.SXXK
{
    public partial class PhanBoTKNReport : DevExpress.XtraReports.UI.XtraReport
    {
        public int SoToKhai;
        public short NamDangKy;
        public string MaHaiQuan;
        public string MaLoaiHinh;
        public PhanBoTKNReport()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            string formatLuongNPL = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            DataTable dt = new Company.BLL.SXXK.ThanhKhoan.BCThueXNK().GetPhanBoToKhaiNhap(this.SoToKhai, this.MaLoaiHinh, this.NamDangKy, this.MaHaiQuan.Trim()).Tables[0];
            dt.TableName = "PhanBoToKhaiNhap";
            this.DataSource = dt;
            lblTongLuongPhanBo.Summary.Running = SummaryRunning.Group;
            lblTongLuongPhanBo.Summary.Func = SummaryFunc.Sum;
            lblTongLuongPhanBo.Summary.FormatString = formatLuongNPL;
            lblToKhaiNhap.Text = this.SoToKhai + "/" + this.MaHaiQuan.Trim().Trim() + "/" + this.NamDangKy;
            lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ma_NPL_SP");
            lblMaNPL1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ma_NPL_SP");
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STTHang");          
            lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Luong",formatLuongNPL);
            lblLuongNhap1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Luong", formatLuongNPL);
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ten");
            lblDVT1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ten");
            lblDonGiaTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DGIA_KB", "{0:n5}");
            lblDonGiaTT1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DGIA_KB", "{0:n5}");
            lblThueSuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TS_XNK", "{0:n0}");
            lblThueSuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TS_XNK", "{0:n0}");

            lblLanThanhLy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LanThanhLy");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            lblNgayThucXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayThucXuat", "{0:dd/MM/yy}");
            lblLuongPhanBo.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", formatLuongNPL);
            lblTongLuongPhanBo.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", formatLuongNPL);
            lblTongLuongTon.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTon", formatLuongNPL);

        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("SoToKhaiXuat").ToString() == "") e.Cancel = true;
        }

    }
}
