﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SOFTECH ECS TQDT GC v4.0")]
[assembly: AssemblyDescription("Phần mềm Khai báo Hải quan điện tử - Gia công")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Công ty Cổ phần SOFTECH")]
[assembly: AssemblyProduct("SOFTECH ECS TQDT GC v4.0")]
[assembly: AssemblyCopyright("Copyright © Softech 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8BA522FF-33CD-4BCC-BF44-6148A73315B7")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("4.0.2022.1")]
[assembly: AssemblyFileVersion("4.0.2022.1")]
[assembly: NeutralResourcesLanguageAttribute("")]
